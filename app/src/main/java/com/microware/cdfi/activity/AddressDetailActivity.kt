package com.microware.cdfi.activity

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.*
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.fragment_address_detail.*
import kotlinx.android.synthetic.main.tablayoutgroup.*
import kotlinx.android.synthetic.main.tablayout.lay_phone
import kotlinx.android.synthetic.main.white_toolbar.*
import kotlinx.android.synthetic.main.white_toolbar.ivBack
import kotlinx.android.synthetic.main.white_toolbar.ivHome
import kotlinx.android.synthetic.main.white_toolbar.tv_title
import java.util.*

class AddressDetailActivity : AppCompatActivity() {

    var validate: Validate? = null
    var addressEntity: AddressEntity? = null
    var cboAddressViewModel: CboAddressViewmodel? = null
    var shgcode = ""
    var shgName = ""
    var addressGUID = ""
    var sStateCode = 0
    var isVerified = 0
    var sDistrictCode = 0
    var sBlockCode = 0
    var sPanchayatCode = 0
    var sVillageCode = 0
    var locationViewModel: LocationViewModel? = null
    var lookupViewmodel: LookupViewmodel? = null
    var dataPanchayat: List<PanchayatEntity>? = null
    var dataVillage: List<VillageEntity>? = null
    var datastate: List<StateEntity>? = null
    var datadistrcit: List<DistrictEntity>? = null
    var datablock: List<BlockEntity>? = null
    var shgViewmodel: SHGViewmodel? = null
    var addressViewmodel: CboAddressViewmodel? = null
    var phoneViewmodel: CboPhoneViewmodel? = null
    var bank: CboBankViewmodel? = null
    var systemtagViewmodel: SystemtagViewmodel? = null
    var memberDesignationViewmodel: MemberDesignationViewmodel? = null
    var memberviewmodel: Memberviewmodel? = null


    var languageCode: String = ""
    var mSpeechRecognizer: SpeechRecognizer? = null
    var is_listening = false
    var is_lang_change = false
    var mSpeechRecognizerIntent: Intent? = null
    private val REQ_CODE_SPEECH_INPUT = 11
    private var CURRENT_SELECTED_EDITTEXT = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_address_detail)

        validate = Validate(this)
        setLabelText()
        cboAddressViewModel = ViewModelProviders.of(this).get(CboAddressViewmodel::class.java)
        locationViewModel = ViewModelProviders.of(this).get(LocationViewModel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        shgViewmodel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        addressViewmodel = ViewModelProviders.of(this).get(CboAddressViewmodel::class.java)
        phoneViewmodel = ViewModelProviders.of(this).get(CboPhoneViewmodel::class.java)
        bank = ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        systemtagViewmodel = ViewModelProviders.of(this).get(SystemtagViewmodel::class.java)
        memberDesignationViewmodel = ViewModelProviders.of(this).get(MemberDesignationViewmodel::class.java)
        memberviewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)

        shgcode = validate!!.RetriveSharepreferenceString(AppSP.Shgcode)!!
        shgName = validate!!.RetriveSharepreferenceString(AppSP.ShgName)!!
        sStateCode = validate!!.RetriveSharepreferenceInt(AppSP.statecode)
        sDistrictCode = validate!!.RetriveSharepreferenceInt(AppSP.districtcode)
        sBlockCode = validate!!.RetriveSharepreferenceInt(AppSP.blockcode)

        tvCode.text = shgcode
        et_shgcode.setText(shgName)
        spin_state.isEnabled = false
        spin_district.isEnabled = false
        spin_block.isEnabled = false

        ivBack.setOnClickListener {
            var intent = Intent(this, AddressDetailListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

        ivHome.setOnClickListener {
            var intent = Intent(this, ShgListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

//        IvSearch.visibility = View.GONE
        tv_title.text =
            validate!!.RetriveSharepreferenceString(AppSP.ShgName)!! + "(" + validate!!.RetriveSharepreferenceString(AppSP.Shgcode) + ")"
        if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) == 1) {
            ivLock.visibility = View.VISIBLE
            btn_addgray.visibility = View.VISIBLE
            btn_add.visibility = View.GONE

        } else {
            ivLock.visibility = View.GONE
            btn_addgray.visibility = View.GONE
            btn_add.visibility = View.VISIBLE
        }


        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvVector.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        var shglist=shgViewmodel!!.getSHG(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var banklist=bank!!.getBankdatalistcount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var phonelist=phoneViewmodel!!.getuploadlist(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var systemlist=systemtagViewmodel!!.getSystemtagdatalistcount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var memberdesignationcount=memberDesignationViewmodel!!.getMemberDesignationdetaillistcount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))

        if(!shglist.isNullOrEmpty()){
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else{
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))


        }
        if(!banklist.isNullOrEmpty()){
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        }else{
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        if(!phonelist.isNullOrEmpty()){
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        }else{
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        if(!systemlist.isNullOrEmpty()){
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        }else{
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        if(!memberdesignationcount.isNullOrEmpty()){
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        }else{
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        lay_systemTag.setOnClickListener {
            var intent = Intent(this, GroupSystemTagList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_vector.setOnClickListener {
            var intent = Intent(this, BasicDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_phone.setOnClickListener {
            var intent = Intent(this, PhoneDetailListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_bank.setOnClickListener {
            var intent = Intent(this, BankDetailListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_post.setOnClickListener {
            var intent = Intent(this, MemberDesignationActvity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        spin_panchayat?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                sPanchayatCode = validate!!.returnPanchayatID(spin_panchayat, dataPanchayat)
                if (position > 0) {
                    dataVillage = locationViewModel!!.getVillagedata_by_panchayatCode(
                        sStateCode,
                        sDistrictCode,
                        sBlockCode,
                        sPanchayatCode
                    )
                    validate!!.fillVillageSpinner(
                        this@AddressDetailActivity,
                        spin_village,
                        dataVillage
                    )
                    spin_village.setSelection(
                        validate!!.returnVillagepos(
                            sVillageCode,
                            dataVillage
                        )
                    )
                } else {
                    dataVillage = locationViewModel!!.getVillagedata_by_panchayatCode(
                        sStateCode,
                        sDistrictCode,
                        sBlockCode,
                        sPanchayatCode
                    )
                    validate!!.fillVillageSpinner(
                        this@AddressDetailActivity,
                        spin_village,
                        dataVillage
                    )
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }


        btn_add.setOnClickListener {
            if (!validate!!.RetriveSharepreferenceString(AppSP.SHGGUID).isNullOrEmpty()) {
                if (checkValidation() == 1) {
                    if (checkData() == 0) {
                        SaveAddressDetail()
                    }else {
                        validate!!.CustomAlert(
                            LabelSet.getText(
                                "data_saved_successfully",
                                R.string.data_saved_successfully
                            ),
                            this,
                            AddressDetailListActivity::class.java
                        )
                    }
                }
            } else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "insert_Basic_data_first",
                        R.string.insert_Basic_data_first
                    ) ,
                    this,
                    BasicDetailActivity::class.java
                )
            }
        }

     //   setLabelText()
        fillSpinner()
        showData()


        languageCode = CDFIApplication.validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!

        iv_mic_address1_eng.setOnClickListener {
            CURRENT_SELECTED_EDITTEXT = 1
            promptSpeechInput("en")
        }

        iv_mic_address2_eng.setOnClickListener {
            CURRENT_SELECTED_EDITTEXT = 2
            promptSpeechInput("en")
        }

    }

    private fun fillSpinner() {
        datastate = locationViewModel!!.getStateByStateCode()
        datadistrcit = locationViewModel!!.getDistrictData(sStateCode)
        datablock = locationViewModel!!.getBlock_data(sStateCode, sDistrictCode)
        validate!!.fillStateSpinner(this, spin_state, datastate)
        validate!!.fillDistrictSpinner(this, spin_district, datadistrcit)
        validate!!.fillBlockSpinner(this, spin_block, datablock)

        dataPanchayat =
            locationViewModel!!.getPanchayatByPanchayatCode(sStateCode, sDistrictCode, sBlockCode)
        validate!!.fillPanchayatSpinner(this, spin_panchayat, dataPanchayat)

        dataVillage = locationViewModel!!.getVillagedata_by_panchayatCode(
            sStateCode,
            sDistrictCode,
            sBlockCode,
            sPanchayatCode
        )
        validate!!.fillVillageSpinner(this, spin_village, dataVillage)

    }

    private fun showData() {
        var list =
            cboAddressViewModel!!.getAddressdetaildata(validate!!.RetriveSharepreferenceString(AppSP.ADDRESSGUID))
        spin_state.setSelection(validate!!.returnStatepos(sStateCode, datastate))
        spin_district.setSelection(
            validate!!.returnDistrictpos(
                sDistrictCode,
                datadistrcit
            )
        )
        spin_block.setSelection(validate!!.returnBlockpos(sBlockCode, datablock))
        if (!list.isNullOrEmpty() && list.size > 0) {
            isVerified = validate!!.returnIntegerValue(list.get(0).is_verified.toString())
            if(isVerified == 1){
                btn_add.text = LabelSet.getText(
                    "save_verify",
                    R.string.save_verify
                )
            }else {
                btn_add.text = LabelSet.getText(
                    "add_address",
                    R.string.add_address
                )
            }
            spin_panchayat.setSelection(
                validate!!.returnPanchayatpos(
                    validate!!.returnIntegerValue(list.get(0).panchayat_id.toString()),
                    dataPanchayat
                )
            )
            sVillageCode =  validate!!.returnIntegerValue(list.get(0).village_id.toString())
            et_address1.setText(validate!!.returnStringValue(list.get(0).address_line1))
            et_address2.setText(validate!!.returnStringValue(list.get(0).address_line2))
            et_pincode.setText(validate!!.returnStringValue(list.get(0).postal_code.toString()))
            et_landmark.setText(validate!!.returnStringValue(list.get(0).landmark))
        } else {
            spin_panchayat.setSelection(
                validate!!.returnPanchayatpos(
                    validate!!.RetriveSharepreferenceInt(
                        AppSP.panchayatcode
                    ), dataPanchayat
                )
            )
            sVillageCode = validate!!.RetriveSharepreferenceInt(AppSP.villagecode)
        }
    }

    private fun SaveAddressDetail() {
        addressGUID = validate!!.random()

        var villagecode = validate!!.returnVillageID(spin_village, dataVillage)
        var panchayatcode = validate!!.returnPanchayatID(spin_panchayat, dataPanchayat)
        if (validate!!.RetriveSharepreferenceString(AppSP.ADDRESSGUID).isNullOrEmpty()) {
            addressEntity = AddressEntity(0,
                validate!!.returnLongValue(shgcode),
                0,
                validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!,
                addressGUID,
                0,
                validate!!.returnStringValue(et_address1.text.toString()),
                validate!!.returnStringValue(et_address2.text.toString()),
                villagecode,
                0,
                panchayatcode,
                validate!!.RetriveSharepreferenceInt(AppSP.blockcode),
                validate!!.returnStringValue(et_landmark.text.toString()),
                validate!!.RetriveSharepreferenceInt(AppSP.statecode),
                validate!!.RetriveSharepreferenceInt(AppSP.districtcode),
                validate!!.returnIntegerValue(et_pincode.text.toString()),
                1,
                1,
                1,
                validate!!.Daybetweentime(validate!!.currentdatetime),
                validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                0,
                "",
                0,
                0,
                1,1
            )
            cboAddressViewModel!!.insert(addressEntity!!)
            validate!!.SaveSharepreferenceString(AppSP.ADDRESSGUID, addressGUID)
            validate!!.updateLockingFlag(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!,
                shgViewmodel,memberviewmodel)
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this,
                AddressDetailListActivity::class.java
            )
        } else {
            cboAddressViewModel!!.updateAddressDetail(
                validate!!.RetriveSharepreferenceString(AppSP.ADDRESSGUID)!!,
                validate!!.returnStringValue(et_address1.text.toString()),
                validate!!.returnStringValue(et_address2.text.toString()),
                villagecode,
                panchayatcode,
                0,
                et_landmark.text.toString(),
                validate!!.returnIntegerValue(et_pincode.text.toString()),
                1,
                1,
                validate!!.Daybetweentime(validate!!.currentdatetime),
                validate!!.RetriveSharepreferenceString(AppSP.userid)!!
            )
            validate!!.updateLockingFlag(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!,
                shgViewmodel,memberviewmodel)
            validate!!.CustomAlert(
                LabelSet.getText(
                    "updated_successfully",
                    R.string.updated_successfully
                ),
                this,
                AddressDetailListActivity::class.java
            )
        }

    }

    fun checkValidation(): Int {
        var value = 1
        if (et_address1.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                )+" " +LabelSet.getText(
                    "address_1",
                    R.string.address_1
                ),
                    this,
                    et_address1
            )
            value = 0
            return value
        }else if(spin_panchayat.selectedItemPosition == 0){
            validate!!.CustomAlertSpinner(this,spin_panchayat,LabelSet.getText(
                "please_select",
                R.string.please_select
            )+" " +LabelSet.getText(
                "grampanchayat",
                R.string.grampanchayat
            ))
            value = 0
            return value
        }else if(spin_village.selectedItemPosition == 0){
            validate!!.CustomAlertSpinner(this,spin_village,LabelSet.getText(
                "please_select",
                R.string.please_select
            )+" " +LabelSet.getText(
                "city_town_village",
                R.string.city_town_village
            ))
            value = 0
            return value
        } else if (et_pincode.text.toString().length == 0 || (et_pincode.text.toString().length > 0 && validate!!.returnIntegerValue(et_pincode.text.toString())==0) || (et_pincode.text.toString().length > 0 && et_pincode.text.toString().length <6)) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                )+" " +LabelSet.getText(
                    "valid_pincode",
                    R.string.valid_pincode
                ),
                this,
                et_pincode
            )
            value = 0
            return value
        }

        return value

    }


    override fun onBackPressed() {
        var intent = Intent(this, AddressDetailListActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }
    fun setLabelText()
    {
        tv_addressLine.text = LabelSet.getText(
            "addresslin1",
            R.string.addresslin1
        )
        et_address1.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_addressline2.text = LabelSet.getText(
            "addresslin2",
            R.string.addresslin2
        )
        et_address2.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tvState.text = LabelSet.getText("state", R.string.state)
        tvDistrict.text = LabelSet.getText(
            "district",
            R.string.district
        )
        tvBlock.text = LabelSet.getText("block", R.string.block)
        tvPanchayat.text = LabelSet.getText(
            "grampanchayat",
            R.string.grampanchayat
        )
        tv_VillageTown.text = LabelSet.getText(
            "city_town_village",
            R.string.city_town_village
        )
        tvLandmark.text = LabelSet.getText(
            "landmark",
            R.string.landmark
        )
        et_landmark.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tvpincode.text = LabelSet.getText(
            "pincode",
            R.string.pincode
        )
        et_pincode.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        btn_add.text = LabelSet.getText(
            "add_address",
            R.string.add_address
        )
        btn_addgray.text = LabelSet.getText(
            "add_address",
            R.string.add_address
        )

    }

    fun updateLockingFlag():Int {
        var isLocked = 0
        var bankCount =
            shgViewmodel!!.getBankCount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var memberCount =
            memberviewmodel!!.getcount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var officeBearerCount = memberviewmodel!!.getOfficeBearerCount(
            validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),
            listOf(1, 2, 3)
        )
        var signatoryCount =
            shgViewmodel!!.getSignatoryCount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))

        if (bankCount == 0 && memberCount >= 5 && officeBearerCount>=2) {
            isLocked = 0
        }else if(bankCount>0 && memberCount>=5 && officeBearerCount>=2 && signatoryCount>=2){
            isLocked = 1
        }
        return isLocked
    }

    private fun checkData(): Int {
        var value = 1
        var villagecode = validate!!.returnVillageID(spin_village, dataVillage)
        var panchayatcode = validate!!.returnPanchayatID(spin_panchayat, dataPanchayat)
        if (!validate!!.RetriveSharepreferenceString(AppSP.ADDRESSGUID).isNullOrEmpty()) {

            var list =
                cboAddressViewModel!!.getAddressdetaildata(validate!!.RetriveSharepreferenceString(AppSP.ADDRESSGUID))
                if (validate!!.returnStringValue(et_address1.text.toString()) != validate!!.returnStringValue(
                        list?.get(0)?.address_line1
                    )
                ) {

                    value = 0
                } else if (et_address2.text.toString() != validate!!.returnStringValue(list?.get(0)?.address_line2)) {

                    value = 0
                } else if (et_pincode.text.toString() != validate!!.returnStringValue(list?.get(0)?.postal_code.toString())) {

                    value = 0

                } else if (panchayatcode != validate!!.returnIntegerValue(list?.get(0)?.panchayat_id.toString())) {

                    value = 0

                } else if (villagecode != validate!!.returnIntegerValue(list?.get(0)?.village_id.toString())) {

                    value = 0

                }

        } else {
            value = 0

        }
        return value
    }


    fun promptSpeechInput(language: String) {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )
        if (isNetworkConnected()){
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, language)
        }
        else{
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.ENGLISH)
        }
        intent.putExtra(
            RecognizerIntent.EXTRA_PROMPT,
            getString(R.string.speech_prompt)
        )
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT)
        } catch (a: ActivityNotFoundException) {
            Toast.makeText(
                applicationContext,
                getString(R.string.speech_not_supported),
                Toast.LENGTH_SHORT
            ).show()
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (resultCode == RESULT_OK) {
                if (requestCode == 11) {
                    if (resultCode == RESULT_OK && null != data) {
                        val result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                        var textdata = result!![0]
                        if (textdata.isNullOrEmpty()) {
                        } else {
                            if (CURRENT_SELECTED_EDITTEXT == 1) {
                                if (et_address1.hasFocus()) {
                                    var cursorPosition: Int = et_address1.selectionStart
                                    et_address1.text.insert(cursorPosition, textdata)
                                } else {
                                    var getData = et_address1.text.toString()
                                    if (getData.length > 0) {
                                        getData = getData + " " + textdata
                                    } else {
                                        getData = getData + textdata
                                    }
                                    et_address1.setText(getData)
                                }
                            }
                            if (CURRENT_SELECTED_EDITTEXT == 2) {
                                if (et_address2.hasFocus()) {
                                    var cursorPosition: Int = et_address2.selectionStart
                                    et_address2.text.insert(cursorPosition, textdata)
                                } else {
                                    var getData = et_address2.text.toString()
                                    if (getData.length > 0) {
                                        getData = getData + " " + textdata
                                    } else {
                                        getData = getData + textdata
                                    }
                                    et_address2.setText(getData)
                                }
                            }
                        }
                    }
                }

            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
//                Toast.makeText(applicationContext, R.string.cancel, Toast.LENGTH_SHORT)
//                    .show()

            }
        } catch (e: Exception) {
            e.printStackTrace()
//            Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show()
        }
    }
    private fun isNetworkConnected(): Boolean {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager //1
        val networkInfo = connectivityManager.activeNetworkInfo //2
        return networkInfo != null && networkInfo.isConnected //3
    }

}
