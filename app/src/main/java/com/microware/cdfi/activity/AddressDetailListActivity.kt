package com.microware.cdfi.activity

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.CboAddressAdapter
import com.microware.cdfi.entity.AddressEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.fragment_address_detaillist.*
import kotlinx.android.synthetic.main.fragment_address_detaillist.rvList
import kotlinx.android.synthetic.main.tablayoutgroup.*
import kotlinx.android.synthetic.main.white_toolbar.*
import kotlinx.android.synthetic.main.white_toolbar.ivBack
import kotlinx.android.synthetic.main.white_toolbar.ivHome
import kotlinx.android.synthetic.main.white_toolbar.tv_title

class AddressDetailListActivity : AppCompatActivity() {

    var validate: Validate? = null
    var addressEntity: AddressEntity? = null
    var cboAddressViewModel: CboAddressViewmodel? = null
    var locationViewmodel: LocationViewModel? = null
    var lookupViewmodel: LookupViewmodel? = null
    var shgViewmodel: SHGViewmodel? = null
    var addressViewmodel: CboAddressViewmodel? = null
    var phoneViewmodel: CboPhoneViewmodel? = null
    var bank: CboBankViewmodel? = null
    var systemtagViewmodel: SystemtagViewmodel? = null
    var memberDesignationViewmodel: MemberDesignationViewmodel? = null
    var memberviewmodel: Memberviewmodel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_address_detaillist)

        validate = Validate(this)
        cboAddressViewModel = ViewModelProviders.of(this).get(CboAddressViewmodel::class.java)
        locationViewmodel = ViewModelProviders.of(this).get(LocationViewModel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        shgViewmodel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        addressViewmodel = ViewModelProviders.of(this).get(CboAddressViewmodel::class.java)
        phoneViewmodel = ViewModelProviders.of(this).get(CboPhoneViewmodel::class.java)
        bank = ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        systemtagViewmodel = ViewModelProviders.of(this).get(SystemtagViewmodel::class.java)
        memberDesignationViewmodel = ViewModelProviders.of(this).get(MemberDesignationViewmodel::class.java)
        memberviewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)

        ivBack.setOnClickListener {
            var intent = Intent(this, ShgListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

        ivHome.setOnClickListener {
            var intent = Intent(this, ShgListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

//        IvSearch.visibility = View.GONE
        if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) == 1) {
            ivLock.visibility = View.VISIBLE
            addAddressgray.visibility = View.VISIBLE
            addAddress.visibility = View.GONE
        } else {
            ivLock.visibility = View.GONE
            addAddressgray.visibility = View.GONE
            addAddress.visibility = View.VISIBLE
        }

        tv_title.text =
            validate!!.RetriveSharepreferenceString(AppSP.ShgName)!! + "(" + validate!!.RetriveSharepreferenceString(
                AppSP.Shgcode
            ) + ")"

        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvVector.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        var shglist = shgViewmodel!!.getSHG(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var banklist =
            bank!!.getBankdatalistcount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var phonelist =
            phoneViewmodel!!.getuploadlistcount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var systemlist = systemtagViewmodel!!.getSystemtagdatalistcount(
            validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)
        )
        var memberdesignationcount=memberDesignationViewmodel!!.getMemberDesignationdetaillistcount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))

        if (!shglist.isNullOrEmpty()) {
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))


        }
        if (!banklist.isNullOrEmpty()) {
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        } else {
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        if (!phonelist.isNullOrEmpty()) {
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        } else {
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        if (!systemlist.isNullOrEmpty()) {
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        } else {
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        if(!memberdesignationcount.isNullOrEmpty()){
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        }else{
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        lay_systemTag.setOnClickListener {
            var intent = Intent(this, GroupSystemTagList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        lay_systemTag.setOnClickListener {
            var intent = Intent(this, GroupSystemTagList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_vector.setOnClickListener {
            var intent = Intent(this, BasicDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_phone.setOnClickListener {
            var intent = Intent(this, PhoneDetailListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_bank.setOnClickListener {
            var intent = Intent(this, BankDetailListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_post.setOnClickListener {
            var intent = Intent(this, MemberDesignationActvity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        addAddress.setOnClickListener {
            if (!validate!!.RetriveSharepreferenceString(AppSP.SHGGUID).isNullOrEmpty()) {
                validate!!.SaveSharepreferenceString(AppSP.ADDRESSGUID, "")
                var intent = Intent(this, AddressDetailActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "insert_Basic_data_first",
                        R.string.insert_Basic_data_first
                    ),
                    this,
                    BasicDetailActivity::class.java
                )
            }
        }

        if(cboAddressViewModel!!.getVerificationCount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))>0){
            btn_VerifyAddress.visibility = View.VISIBLE
        }else {
            btn_VerifyAddress.visibility = View.GONE
        }
        btn_VerifyAddress.setOnClickListener {
            var isCompleteCount = cboAddressViewModel!!.getCompletionCount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
            if(isCompleteCount==0){
                cboAddressViewModel!!.updateIsVerifed(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),validate!!.Daybetweentime(validate!!.currentdatetime),
                    validate!!.RetriveSharepreferenceString(AppSP.userid)!!)
                validate!!.CustomAlert(LabelSet.getText(
                    "",
                    R.string.verified_successfully
                ),this)
                if(cboAddressViewModel!!.getVerificationCount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))>0){
                    btn_VerifyAddress.visibility = View.VISIBLE
                }else {
                    btn_VerifyAddress.visibility = View.GONE
                }
            }else {
                validate!!.CustomAlert(LabelSet.getText(
                    "",
                    R.string.completerecord
                ),this)
            }
        }
        fillData()

    }



    private fun fillData() {
        cboAddressViewModel!!.getAddressdata(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))!!
            .observe(this, object : Observer<List<AddressEntity>> {
                override fun onChanged(address_list: List<AddressEntity>?) {

                    if (address_list!=null) {
                        rvList.layoutManager = LinearLayoutManager(this@AddressDetailListActivity)
                        rvList.adapter =
                            CboAddressAdapter(this@AddressDetailListActivity, address_list)
                        if (address_list.size>1){
                            addAddress.visibility=View.GONE
                        }else{
                            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) == 1) {
                                addAddressgray.visibility = View.VISIBLE
                                addAddress.visibility = View.GONE
                            } else {
                                addAddressgray.visibility = View.GONE
                                addAddress.visibility = View.VISIBLE
                            }
                            //addAddress.visibility=View.VISIBLE
                        }
                        if(address_list.size>0){
                            lay_noaddress_avialable.visibility = View.GONE
                        } else {
                            lay_noaddress_avialable.visibility = View.VISIBLE
                            tv_noaddress_avialable.text = LabelSet.getText(
                                "no_address_s_avialable",
                                R.string.no_address_s_avialable
                            )
                        }
                    }
                }
            })
    }

    fun getAddressType(kyc_code: String, addressType: Int?): String? {
        var type: String? = null
        type = lookupViewmodel!!.getAddressType(kyc_code, addressType)
        return type
    }

    override fun onBackPressed() {
        var intent = Intent(this, ShgListActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }

    fun getVillageName(villageId: Int?): String? {
        var name: String? = null
        name = locationViewmodel!!.getVillageName(villageId!!)
        return name
    }

    fun CustomAlert(guid:String,iFlag:Int) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = LabelSet.getText(
            "do_u_want_to_delete",
            R.string.do_u_want_to_delete
        )
        mDialogView.txt_dialog_title.text = LabelSet.getText("app_name", R.string.app_name)
        mDialogView.btn_yes.text = LabelSet.getText("yes", R.string.yes)
        mDialogView.btn_no.text = LabelSet.getText("no", R.string.no)
        mDialogView.btn_yes.setOnClickListener {
            if(iFlag==0){
                cboAddressViewModel!!.deleteRecord(guid)
            }else {
                cboAddressViewModel!!.deleteData(guid)

            }
            validate!!.updateLockingFlag(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!,
                shgViewmodel,memberviewmodel)

            fillData()
            mAlertDialog.dismiss()


        }

        mDialogView.btn_no.setOnClickListener {

            mAlertDialog.dismiss()


        }
    }
}
