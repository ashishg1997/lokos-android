package com.microware.cdfi.activity

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.CboBankAdapter
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.fragment_bank_detail_list.*
import kotlinx.android.synthetic.main.fragment_bank_detail_list.rvList
import kotlinx.android.synthetic.main.tablayoutgroup.*
import kotlinx.android.synthetic.main.white_toolbar.*
import kotlinx.android.synthetic.main.white_toolbar.ivBack
import kotlinx.android.synthetic.main.white_toolbar.ivHome
import kotlinx.android.synthetic.main.white_toolbar.tv_title

class BankDetailListActivity : AppCompatActivity() {

    var validate: Validate? = null
    var cboBankEntity: Cbo_bankEntity? = null
    var cboBankViewmodel: CboBankViewmodel? = null
    var bankMasterViewModel : MasterBankViewmodel? = null
    var masterBankBranchViewModel : MasterBankBranchViewModel? = null
    var shgViewmodel: SHGViewmodel? = null
    var addressViewmodel: CboAddressViewmodel? = null
    var phoneViewmodel: CboPhoneViewmodel? = null
    var memberviewmodel: Memberviewmodel? = null
    var bank: CboBankViewmodel? = null
    var systemtagViewmodel: SystemtagViewmodel? = null
    var memberDesignationViewmodel: MemberDesignationViewmodel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_bank_detail_list)
        validate = Validate(this)
        cboBankViewmodel = ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        bankMasterViewModel = ViewModelProviders.of(this).get(MasterBankViewmodel::class.java)
        masterBankBranchViewModel = ViewModelProviders.of(this).get(MasterBankBranchViewModel::class.java)
        shgViewmodel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        addressViewmodel = ViewModelProviders.of(this).get(CboAddressViewmodel::class.java)
        phoneViewmodel = ViewModelProviders.of(this).get(CboPhoneViewmodel::class.java)
        bank = ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        systemtagViewmodel = ViewModelProviders.of(this).get(SystemtagViewmodel::class.java)
        memberDesignationViewmodel = ViewModelProviders.of(this).get(MemberDesignationViewmodel::class.java)
        memberviewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)


        ivBack.setOnClickListener {
            var intent = Intent(this,ShgListActivity::class.java)
            intent.flags=Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

        ivHome.setOnClickListener {
            var intent = Intent(this,ShgListActivity::class.java)
            intent.flags=Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

//        IvSearch.visibility = View.GONE
        if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) == 1) {
            ivLock.visibility = View.VISIBLE
            addBank.visibility = View.GONE
            addBankgray.visibility = View.VISIBLE
        } else {
            ivLock.visibility = View.GONE
            addBank.visibility = View.VISIBLE
            addBankgray.visibility = View.GONE
        }

        tv_title.text =
            validate!!.RetriveSharepreferenceString(AppSP.ShgName)!! + "(" + validate!!.RetriveSharepreferenceString(AppSP.Shgcode) + ")"
        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvVector.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        var shglist=shgViewmodel!!.getSHG(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var addresslist=addressViewmodel!!.getAddressDatalistcount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var phonelist=phoneViewmodel!!.getuploadlistcount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var systemlist=systemtagViewmodel!!.getSystemtagdatalistcount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var memberdesignationcount=memberDesignationViewmodel!!.getMemberDesignationdetaillistcount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))

        if(!shglist.isNullOrEmpty()){
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else{
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))


        }
        if(!addresslist.isNullOrEmpty()){
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        }else{
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        if(!phonelist.isNullOrEmpty()){
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        }else{
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        if(!systemlist.isNullOrEmpty()){
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        }else{
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        if(!memberdesignationcount.isNullOrEmpty()){
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        }else{
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        lay_systemTag.setOnClickListener {
            var intent = Intent(this, GroupSystemTagList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_vector.setOnClickListener {
            var intent = Intent(this, BasicDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_phone.setOnClickListener {
            var intent = Intent(this, PhoneDetailListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_location.setOnClickListener {
            var intent = Intent(this, AddressDetailListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_post.setOnClickListener {
            var intent = Intent(this, MemberDesignationActvity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        addBank.setOnClickListener {
            if (!validate!!.RetriveSharepreferenceString(AppSP.SHGGUID).isNullOrEmpty()) {
                validate!!.SaveSharepreferenceString(AppSP.BankGUID, "")
                validate!!.SaveSharepreferenceInt(AppSP.ShgBankStatus,0)
                var inetnt = Intent(this, BankDetailActivity::class.java)
                startActivity(inetnt)
                finish()
            } else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "insert_Basic_data_first",
                        R.string.insert_Basic_data_first
                    ),
                    this,
                    BasicDetailActivity::class.java
                )
            }
        }

        if(cboBankViewmodel!!.getVerificationCount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))>0){
            btn_VerifyBank.visibility = View.VISIBLE
        }else {
            btn_VerifyBank.visibility = View.GONE
        }
        btn_VerifyBank.setOnClickListener {
            var isCompleteCount = cboBankViewmodel!!.getCompletionCount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
            if(isCompleteCount==0){
                cboBankViewmodel!!.updateIsVerifed(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),validate!!.Daybetweentime(validate!!.currentdatetime),
                    validate!!.RetriveSharepreferenceString(AppSP.userid)!!)
                validate!!.CustomAlert(LabelSet.getText(
                    "",
                    R.string.verified_successfully
                ),this)
                if(cboBankViewmodel!!.getVerificationCount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))>0){
                    btn_VerifyBank.visibility = View.VISIBLE
                }else {
                    btn_VerifyBank.visibility = View.GONE
                }
            }else {
                validate!!.CustomAlert(LabelSet.getText(
                    "",
                    R.string.completerecord
                ),this)
            }
        }
        fillData()

    }

    private fun fillData() {
        cboBankViewmodel!!.getBankdata(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))!!
            .observe(this,object : Observer<List<Cbo_bankEntity>> {
                override fun onChanged(bank_list: List<Cbo_bankEntity>?) {
                    if (bank_list!=null){
                        rvList.layoutManager = LinearLayoutManager(this@BankDetailListActivity)
                        rvList.adapter = CboBankAdapter(this@BankDetailListActivity, bank_list)
                        if(bank_list.size>0){
                            lay_nobank_avialable.visibility = View.GONE
                        } else {
                            lay_nobank_avialable.visibility = View.VISIBLE
                            tv_nobank_avialable.text = LabelSet.getText(
                                "no_bank_s_avialable",
                                R.string.no_bank_s_avialable
                            )
                        }
                    }
                }
            })
    }

    fun getBankName(bankID : Int?): String?{
        var value: String? = null
        value = bankMasterViewModel!!.getBankName(bankID!!)
        return value
    }
    fun getBranchname(branchid : Int): String?{
        var value: String? = null
        value = masterBankBranchViewModel!!.getBranchname(branchid)
        return value
    }

    override fun onBackPressed() {
        var intent = Intent(this,ShgListActivity::class.java)
        intent.flags=Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }

    fun CustomAlert(guid:String,iFlag:Int) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = LabelSet.getText(
            "do_u_want_to_delete",
            R.string.do_u_want_to_delete
        )
        mDialogView.txt_dialog_title.text = LabelSet.getText("app_name", R.string.app_name)
        mDialogView.btn_yes.text = LabelSet.getText("yes", R.string.yes)
        mDialogView.btn_no.text = LabelSet.getText("no", R.string.no)
        mDialogView.btn_yes.setOnClickListener {

            if(iFlag==0){
                cboBankViewmodel!!.deleteRecord(guid)
            }else {
                cboBankViewmodel!!.deleteData(guid)

            }
            validate!!.updateLockingFlag(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!,
                shgViewmodel,memberviewmodel)

            mAlertDialog.dismiss()
        }
        mDialogView.btn_no.setOnClickListener {

            mAlertDialog.dismiss()

        }
    }
}
