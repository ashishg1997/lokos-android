package com.microware.cdfi.activity

import android.Manifest
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.provider.Settings
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.BuildConfig
import com.microware.cdfi.R
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.*
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.buttons.btn_cancel
import kotlinx.android.synthetic.main.buttons.btn_save
import kotlinx.android.synthetic.main.buttons.btn_savegray
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.customselectdialoge.view.*
import kotlinx.android.synthetic.main.fragment_basicdetail.*
import kotlinx.android.synthetic.main.tablayoutgroup.*
import kotlinx.android.synthetic.main.white_toolbar.*
import org.apache.commons.io.IOUtils
import java.io.*
import java.nio.channels.FileChannel
import java.text.SimpleDateFormat
import java.util.*

class BasicDetailActivity : AppCompatActivity() {
    var shgViewModel: SHGViewmodel? = null
    var locationViewModel: LocationViewModel? = null
    var shgEntity: SHGEntity? = null
    var validate: Validate? = null
    var guid = ""
    var shgcode = ""
    var shgName = " "
    var cbotype = 0
    var sStateCode = 0
    var sDistrictCode = 0
    var sBlockCode = 0
    var sPanchayatCode = 0
    var iMeetingOn = 0
    var ifrequency_value = 0
    var sVillageCode = 0
    var iFrequency_db = 0
    var name_book_keeper = ""
    var initial_dt: String = "11-06-2011"
    var dataPanchayat: List<PanchayatEntity>? = null
    var dataVillage: List<VillageEntity>? = null
    var lookupViewmodel: LookupViewmodel? = null
    var fundingViewModel: FundingAgencyViewModel? = null
    var dataspin_name: List<LookupEntity>? = null
    var dataspin_funding: List<FundingEntity>? = null
    var datashgtype: List<LookupEntity>? = null
    var databookkeeper: List<LookupEntity>? = null
    var datarevival: List<LookupEntity>? = null
    var datayes: List<LookupEntity>? = null
    var data_tags: List<LookupEntity>? = null
    var datapvtg: List<LookupEntity>? = null
    var dataspin_federation: List<LookupEntity>? = null
    var dataspin_status: List<LookupEntity>? = null
    var dataspin_pramotedby: List<LookupEntity>? = null
    var dataspin_fortnight: List<LookupEntity>? = null
    var dataspin_monthly: List<LookupEntity>? = null
    var dataspin_monthlydate: List<LookupEntity>? = null
    var dataspin_fortnightdate: List<LookupEntity>? = null
    var dataspin_meetingfreq: List<LookupEntity>? = null
    var dataspin_activity: List<LookupEntity>? = null
    var dataspin_weekday: List<LookupEntity>? = null
    var dataspin_inactiveReason: List<LookupEntity>? = null
    var memberviewmodel: Memberviewmodel? = null
    var memberPhoneViewmodel: MemberPhoneViewmodel? = null
    var dataspin_belong: List<MemberEntity>? = null
    var dataspin_belongmemberphone: List<MemberPhoneDetailEntity>? = null
    var addressViewmodel: CboAddressViewmodel? = null
    var phoneViewmodel: CboPhoneViewmodel? = null
    var bank: CboBankViewmodel? = null
    var systemtagViewmodel: SystemtagViewmodel? = null
    var memberDesignationViewmodel: MemberDesignationViewmodel? = null
    var rgrevivalid = 0
    var isVerified = 0
    var savingfrequencyid = 0
    var REQUEST_TAKE_GALLERY_VIDEO = 1
    var imageName = ""
    var newimageName = false
    var bitmap: Bitmap? = null
    var imageUploadViewmodel: ImageUploadViewmodel? = null
    private var fileUri: Uri? = null
    var mediaFile: File? = null


    var languageCode: String = ""
    var mSpeechRecognizer: SpeechRecognizer? = null
    var is_listening = false
    var is_lang_change = false
    var mSpeechRecognizerIntent: Intent? = null
    private val REQ_CODE_SPEECH_INPUT = 11
    private var CURRENT_SELECTED_EDITTEXT = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_basicdetail)
        validate = Validate(this)

        setLabelText()
        tv_title.text = LabelSet.getText(
            "shgbasicdetails",
            R.string.shgbasicdetails
        )
        shgViewModel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        locationViewModel = ViewModelProviders.of(this).get(LocationViewModel::class.java)
        memberviewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)
        memberPhoneViewmodel = ViewModelProviders.of(this).get(MemberPhoneViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        addressViewmodel = ViewModelProviders.of(this).get(CboAddressViewmodel::class.java)
        phoneViewmodel = ViewModelProviders.of(this).get(CboPhoneViewmodel::class.java)
        fundingViewModel = ViewModelProviders.of(this).get(FundingAgencyViewModel::class.java)
        bank = ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        systemtagViewmodel = ViewModelProviders.of(this).get(SystemtagViewmodel::class.java)
        memberDesignationViewmodel =
            ViewModelProviders.of(this).get(MemberDesignationViewmodel::class.java)
        imageUploadViewmodel = ViewModelProviders.of(this).get(ImageUploadViewmodel::class.java)


        sStateCode = validate!!.RetriveSharepreferenceInt(AppSP.statecode)
        sDistrictCode = validate!!.RetriveSharepreferenceInt(AppSP.districtcode)
        sBlockCode = validate!!.RetriveSharepreferenceInt(AppSP.blockcode)

        if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) == 1) {
            ivLock.visibility = View.VISIBLE
            btn_savegray.visibility = View.VISIBLE
            btn_save.visibility = View.GONE

        } else {
            ivLock.visibility = View.GONE
            btn_savegray.visibility = View.GONE
            btn_save.visibility = View.VISIBLE
        }
        ivBack.setOnClickListener {
            // CustomAlertPartialsave(getString(R.string.doyouwanttosavepartialdata),ShgListActivity::class.java)
            if (CheckValidation() == 1) {
                PartialSaveData()
                var intent = Intent(this, ShgListActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                finish()
            }
        }

        ivHome.setOnClickListener {
            var intent = Intent(this, ShgListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvVector.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        var banklist =
            bank!!.getBankdatalistcount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var addresslist =
            addressViewmodel!!.getAddressDatalistcount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var phonelist =
            phoneViewmodel!!.getuploadlistcount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var systemlist = systemtagViewmodel!!.getSystemtagdatalistcount(
            validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)
        )
        var memberdesignationcount =
            memberDesignationViewmodel!!.getMemberDesignationdetaillistcount(
                validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)
            )

        if (!banklist.isNullOrEmpty()) {
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if (!addresslist.isNullOrEmpty()) {
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        } else {
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        if (!phonelist.isNullOrEmpty()) {
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        } else {
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        if (!systemlist.isNullOrEmpty()) {
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        } else {
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        if (!memberdesignationcount.isNullOrEmpty()) {
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        } else {
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        lay_systemTag.setOnClickListener {
            // CustomAlertPartialsave(getString(R.string.doyouwanttosavepartialdata),GroupSystemTagList::class.java)
            if (CheckValidation() == 1) {
                PartialSaveData()
                var intent = Intent(this, GroupSystemTagList::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_phone.setOnClickListener {
            //            CustomAlertPartialsave(getString(R.string.doyouwanttosavepartialdata),PhoneDetailListActivity::class.java)
            if (CheckValidation() == 1) {
                PartialSaveData()
                var intent = Intent(this, PhoneDetailListActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_location.setOnClickListener {
            //            CustomAlertPartialsave(getString(R.string.doyouwanttosavepartialdata),AddressDetailListActivity::class.java)
            if (CheckValidation() == 1) {
                PartialSaveData()
                var intent = Intent(this, AddressDetailListActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_bank.setOnClickListener {
            //            CustomAlertPartialsave(getString(R.string.doyouwanttosavepartialdata),BankDetailListActivity::class.java)
            if (CheckValidation() == 1) {
                PartialSaveData()
                var intent = Intent(this, BankDetailListActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_post.setOnClickListener {
            //            CustomAlertPartialsave(getString(R.string.doyouwanttosavepartialdata),KYCDetailListActivity::class.java)
            if (CheckValidation() == 1) {
                PartialSaveData()
                var intent = Intent(this, MemberDesignationActvity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        et_date.setOnClickListener {
            validate!!.datePicker(et_date)
        }

        et_formationDate.setOnClickListener {
            validate!!.datePickerwithmindate(
                validate!!.Daybetweentime("01-01-1990"),
                et_formationDate
            )

        }

        et_revivalDate.setOnClickListener {
            var date = validate!!.Daybetweentime(et_formationDate.text.toString())
            if (date > 0) {
                validate!!.datePickerwithmindate(date, et_revivalDate)
            } else {
                validate!!.datePicker(et_revivalDate)
            }

        }
        et_coopt_Date.setOnClickListener {
            var date = validate!!.Daybetweentime(et_formationDate.text.toString())
            if (date > 0) {
                validate!!.datePickerwithmindate(date, et_coopt_Date)
            } else {
                validate!!.datePicker(et_coopt_Date)
            }

        }

        btn_save.setOnClickListener {
            if (CheckValidation() == 1) {
                if (CheckData() == 0) {
                    SaveData()
                } else {
                    validate!!.CustomAlert(
                        LabelSet.getText(
                            "data_saved_successfully",
                            R.string.data_saved_successfully
                        ),
                        this,
                        PhoneDetailListActivity::class.java
                    )
                }
            }
        }

        btn_cancel.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) == 1) {
                var intent = Intent(this, ShgListActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                finish()
            } else {

                CustomAlertPartialsave(
                    LabelSet.getText(
                        "doyouwanttoexit",
                        R.string.doyouwanttoexit
                    ), ShgListActivity::class.java
                )
            }

        }

        rgBookeeper.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->
            var book_keeper_identified = validate!!.GetAnswerTypeRadioButtonID(rgBookeeper)
            if (book_keeper_identified == 1) {
                lay_spinmember.visibility = View.VISIBLE
                lay_bookkeepername.visibility = View.GONE
                lay_bookkeepermobile.visibility = View.VISIBLE
                fillmemberspinner(spin_member, dataspin_belong)
                et_bookkeeper_s_mobile_no.isEnabled = false

            } else if (book_keeper_identified == 2) {
                lay_spinmember.visibility = View.GONE
                lay_bookkeepername.visibility = View.VISIBLE
                lay_bookkeepermobile.visibility = View.VISIBLE
                et_bookkeeper_s_mobile_no.isEnabled = true
                et_bookkeeper_s_mobile_no.setText("")
            } else {
                lay_spinmember.visibility = View.GONE
                lay_bookkeepername.visibility = View.GONE
                lay_bookkeepermobile.visibility = View.GONE
                et_bookkeeper_name.setText("")
                et_bookkeeper_s_mobile_no.setText("")
            }

        })
        rgShgType.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->

            if (checkedId == 2) {
                lay_special.visibility = View.VISIBLE
            } else {
                lay_special.visibility = View.GONE
                lay_tags_other.visibility = View.GONE
                spin_tags.setSelection(0)
                et_OtherTags.setText("")

            }

        })
        rgvolunteer.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->

            if (checkedId == 1) {
                lay_voluntaryROI.visibility = View.VISIBLE
            } else {
                lay_voluntaryROI.visibility = View.GONE
                et_voluntarysavingROI.setText("")
            }

        })
        rgrevival.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->

            if (checkedId == 1) {
                lay_coptdate.visibility = View.GONE
                lay_revivalDate.visibility = View.GONE
                et_coopt_Date.setText("")
                et_revivalDate.setText("")
            } else if (checkedId == 2) {
                lay_coptdate.visibility = View.VISIBLE
                lay_revivalDate.visibility = View.GONE
                et_revivalDate.setText("")
            } else if (checkedId == 3) {
                lay_coptdate.visibility = View.GONE
                et_coopt_Date.setText("")
                lay_revivalDate.visibility = View.VISIBLE
            } else {
                lay_coptdate.visibility = View.GONE
                lay_revivalDate.visibility = View.GONE
            }

        })




        spin_panchayat?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                sPanchayatCode = validate!!.returnPanchayatID(spin_panchayat, dataPanchayat)
                if (position > 0) {
                    dataVillage = locationViewModel!!.getVillagedata_by_panchayatCode(
                        sStateCode,
                        sDistrictCode,
                        sBlockCode,
                        sPanchayatCode
                    )
                    validate!!.fillVillageSpinner(
                        this@BasicDetailActivity,
                        spin_village,
                        dataVillage
                    )
                    spin_village.setSelection(
                        validate!!.returnVillagepos(
                            sVillageCode,
                            dataVillage
                        )
                    )
                } else {
                    dataVillage = locationViewModel!!.getVillagedata_by_panchayatCode(
                        sStateCode,
                        sDistrictCode,
                        sBlockCode,
                        sPanchayatCode
                    )
                    validate!!.fillVillageSpinner(
                        this@BasicDetailActivity,
                        spin_village,
                        dataVillage
                    )
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        et_formationDate.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var promoter = validate!!.returnlookupcode(spin_promotedby, dataspin_pramotedby)
                if (validate!!.returnString_DateValue(et_formationDate.text.toString()).length > 0) {

                    if (validate!!.returnString_DateValue(et_revivalDate.text.toString()).length > 0) {
                        if (validate!!.getDate(
                                et_revivalDate.text.toString(),
                                et_formationDate.text.toString()
                            ) == 1
                        ) {
                            validate!!.CustomAlertEditText(
                                LabelSet.getText(
                                    "formation_revival_date",
                                    R.string.formation_revival_date
                                ),
                                this@BasicDetailActivity,
                                et_formationDate
                            )
                        }
                    }

                    if (validate!!.returnString_DateValue(et_coopt_Date.text.toString()).length > 0) {
                        if (validate!!.getDate(
                                et_coopt_Date.text.toString(),
                                et_formationDate.text.toString()
                            ) == 1
                        ) {
                            validate!!.CustomAlertEditText(
                                LabelSet.getText(
                                    "formation_copt_date",
                                    R.string.formation_copt_date
                                ),
                                this@BasicDetailActivity,
                                et_formationDate
                            )
                        }
                    }
                    if (validate!!.getDate(
                            et_formationDate.text.toString(),
                            initial_dt
                        ) == 1 && promoter == 1
                    ) {
                        validate!!.CustomAlertSpinner(
                            this@BasicDetailActivity,
                            spin_promotedby,
                            LabelSet.getText(
                                "nrlm_allowed",
                                R.string.nrlm_allowed
                            ) + initial_dt
                        )
                    }
                    if (validate!!.getDate(
                            et_formationDate.text.toString(),
                            initial_dt
                        ) == 1 || promoter != 1
                    ) {
                        lay_rgrevival.visibility = View.VISIBLE
                        /* lay_coptdate.visibility = View.VISIBLE
                         lay_revivalDate.visibility = View.VISIBLE*/
                    } else {
                        lay_rgrevival.visibility = View.GONE
                        lay_coptdate.visibility = View.GONE
                        lay_revivalDate.visibility = View.GONE
                        et_coopt_Date.setText("")
                        et_revivalDate.setText("")
                    }
                }
            }
        })

        et_revivalDate.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var promoter = validate!!.returnlookupcode(spin_promotedby, dataspin_pramotedby)
                if (promoter != 0 && promoter != 1 && validate!!.returnString_DateValue(
                        et_formationDate.text.toString()
                    ).length > 0 && validate!!.returnString_DateValue(
                        et_revivalDate.text.toString()
                    ).length > 0
                ) {
                    if (validate!!.getDate(
                            et_revivalDate.text.toString(),
                            et_formationDate.text.toString()
                        ) == 1
                    ) {
                        validate!!.CustomAlertEditText(
                            LabelSet.getText(
                                "revival_formation_date",
                                R.string.revival_formation_date
                            ),
                            this@BasicDetailActivity,
                            et_revivalDate
                        )
                    }
                }
            }
        })

        et_electiontenure.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (validate!!.returnStringValue(et_electiontenure.text.toString()).length >= 2 &&
                    (validate!!.returnIntegerValue(et_electiontenure.text.toString()) < 12 || validate!!.returnIntegerValue(
                        et_electiontenure.text.toString()
                    ) > 36)
                ) {
                    validate!!.CustomAlertEditText(
                        LabelSet.getText(
                            "valid_election_tenure",
                            R.string.valid_election_tenure
                        ),
                        this@BasicDetailActivity,
                        et_electiontenure
                    )
                }
            }
        })

        spin_promotedby.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {
                    var promoter = validate!!.returnlookupcode(spin_promotedby, dataspin_pramotedby)
                    if (validate!!.getDate(
                            et_formationDate.text.toString(),
                            initial_dt
                        ) == 1 && promoter == 1
                    ) {
                        validate!!.CustomAlertSpinner(
                            this@BasicDetailActivity,
                            spin_promotedby,
                            LabelSet.getText(
                                "nrlm_allowed",
                                R.string.nrlm_allowed
                            ) + initial_dt
                        )
                    }
                    if (promoter == 99) {
                        lay_promoter_name.visibility = View.VISIBLE
                        lay_fundingAgency.visibility = View.GONE
                        spin_FundingAgency.setSelection(0)
                    } else if (promoter == 3) {
                        lay_promoter_name.visibility = View.GONE
                        lay_fundingAgency.visibility = View.VISIBLE
                        et_promoter_name.setText("")
                    } else {
                        lay_promoter_name.visibility = View.GONE
                        lay_fundingAgency.visibility = View.GONE
                        spin_FundingAgency.setSelection(0)
                        et_promoter_name.setText("")
                    }
                    if (promoter != 1) {
                        if (validate!!.getDate(
                                et_formationDate.text.toString(),
                                initial_dt
                            ) == 1 || promoter != 1
                        ) {
                            lay_rgrevival.visibility = View.VISIBLE
                            lay_coptdate.visibility = View.VISIBLE
                            datarevival = lookupViewmodel!!.getlookupnotin(
                                51, 1,
                                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
                            )
                            var checkValue = 0
                            if (rgrevivalid == 0) {
                                checkValue = 2
                            } else {
                                checkValue = rgrevivalid
                            }
                            validate!!.fillradio(
                                rgrevival,
                                checkValue,
                                datarevival,
                                this@BasicDetailActivity
                            )
                            //  rgrevivalid=0
                            /*lay_coptdate.visibility = View.VISIBLE
                            lay_revivalDate.visibility = View.VISIBLE*/
                        } else {

                            lay_rgrevival.visibility = View.GONE
                            lay_coptdate.visibility = View.GONE
                            lay_revivalDate.visibility = View.GONE
                            et_coopt_Date.setText("")
                            et_revivalDate.setText("")
                        }
                    } else if (promoter == 1) {
                        lay_rgrevival.visibility = View.VISIBLE
                        datarevival = lookupViewmodel!!.getlookupnotin(
                            51, 2,
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
                        )
                        var checkValue = 0
                        if (rgrevivalid == 0) {
                            checkValue = 1
                        } else {
                            checkValue = rgrevivalid
                        }
                        validate!!.fillradio(
                            rgrevival,
                            checkValue,
                            datarevival,
                            this@BasicDetailActivity
                        )
                        if (rgrevivalid == 3) {
                            lay_revivalDate.visibility = View.VISIBLE
                        } else {
                            lay_revivalDate.visibility = View.GONE
                            et_revivalDate.setText("")
                        }
                        lay_coptdate.visibility = View.GONE
                        et_coopt_Date.setText("")

                    }
                } else {
                    lay_rgrevival.visibility = View.GONE
                    lay_coptdate.visibility = View.GONE
                    lay_revivalDate.visibility = View.GONE
                    lay_promoter_name.visibility = View.GONE
                    lay_fundingAgency.visibility = View.GONE
                    spin_FundingAgency.setSelection(0)
                    et_coopt_Date.setText("")
                    et_revivalDate.setText("")
                    et_promoter_name.setText("")
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        spin_FundingAgency.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {
                    var promotorCode =
                        validate!!.returnFundingcode(spin_FundingAgency, dataspin_funding)
                    if (promotorCode == 99) {
                        lay_promoter_name.visibility = View.VISIBLE
                    } else {
                        lay_promoter_name.visibility = View.GONE
                    }

                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }
        spin_member.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {
                    dataspin_belongmemberphone =
                        memberPhoneViewmodel!!.getphonedetaillistdata(returnmemguid())
                    fillmemberphoneno(dataspin_belongmemberphone)
                } else {
                    dataspin_belongmemberphone = memberPhoneViewmodel!!.getphonedetaillistdata("")
                    fillmemberphoneno(dataspin_belongmemberphone)

                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        spin_tags.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {
                    var tagId = validate!!.returnlookupcode(spin_tags, data_tags)
                    if (tagId == 99) {
                        lay_tags_other.visibility = View.VISIBLE
                    } else {
                        lay_tags_other.visibility = View.GONE
                        et_OtherTags.setText("")
                    }
                } else {
                    lay_tags_other.visibility = View.GONE
                    et_OtherTags.setText("")

                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }
        et_coopt_Date.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var promoter = validate!!.returnlookupcode(spin_promotedby, dataspin_pramotedby)
                if (promoter != 0 && promoter != 1 && validate!!.returnString_DateValue(
                        et_formationDate.text.toString()
                    ).length > 0 && validate!!.returnString_DateValue(
                        et_coopt_Date.text.toString()
                    ).length > 0
                ) {
                    if (validate!!.getDate(
                            et_coopt_Date.text.toString(),
                            et_formationDate.text.toString()
                        ) == 1
                    ) {
                        validate!!.CustomAlertEditText(
                            LabelSet.getText(
                                "coopt_formation_date",
                                R.string.coopt_formation_date
                            ),
                            this@BasicDetailActivity,
                            et_coopt_Date
                        )
                    }
                }
            }
        })
        Imgmember.setOnClickListener {
            CustomSelectAlert()
        }
        tblimage.setOnClickListener {
            if (imageName.isNotEmpty()) {
                if (imageName.contains("pdf")) {
                    if (Build.VERSION.SDK_INT >= 24) {
                        try {
                            val m =
                                StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
                            m.invoke(null)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }
                    val pdfFile = File(
                        getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
                        AppSP.IMAGE_DIRECTORY_NAME + File.separator + imageName
                    )
                    val path = FileProvider.getUriForFile(
                        this,
                        BuildConfig.APPLICATION_ID + ".provider",
                        pdfFile
                    )
                    Log.e("create pdf uri path==>", "" + path)

                    try {
                        val intent = Intent(Intent.ACTION_VIEW)
                        intent.setDataAndType(path, "application/pdf")
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                        startActivity(intent)
                    } catch (e: ActivityNotFoundException) {
                        Toast.makeText(
                            this
                                .applicationContext,
                            "There is no any PDF Viewer",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } else {


                    ShowImage(imageName)
                }
            }

        }
        et_ComsavingRoi.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = validate!!.returnDoubleValue(et_ComsavingRoi.text.toString())
                if (validate!!.returnDoubleValue(s.toString()) > 0 && value > 24) {
                    var installmentamt = "0"
                    et_ComsavingRoi.setText(installmentamt.toString())
                    validate!!.CustomAlertEditText(
                        LabelSet.getText(
                            "please_enter",
                            R.string.please_enter
                        ) + " " + LabelSet.getText(
                            "interest_less_complusory",
                            R.string.interest_less_complusory
                        ), this@BasicDetailActivity,
                        et_ComsavingRoi
                    )
                }

            }

        })
        et_voluntarysavingROI.filters = arrayOf<InputFilter>(DecimalDigitsInputFilter(3, 2))
        et_ComsavingRoi.filters = arrayOf<InputFilter>(DecimalDigitsInputFilter(3, 2))
        fillSpinner()
        fillRadio()
        showData()
        //  setLabelText()

        languageCode = CDFIApplication.validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
        iv_mic_name_eng.setOnClickListener {
            CURRENT_SELECTED_EDITTEXT = 1
            promptSpeechInput("en")
        }

        iv_mic_promoter_name_eng.setOnClickListener {
            CURRENT_SELECTED_EDITTEXT = 2
            promptSpeechInput("en")
        }

        iv_mic_bookkeepername_eng.setOnClickListener {
            CURRENT_SELECTED_EDITTEXT = 3
            promptSpeechInput("en")
        }
    }

    fun returnmemguid(): String {

        var pos = spin_member.selectedItemPosition
        var id = ""

        if (!dataspin_belong.isNullOrEmpty()) {
            if (pos > 0) id = dataspin_belong!!.get(pos - 1).member_guid
        }
        return id
    }

    fun returnPhone(): String {

        var pos = spin_member.selectedItemPosition
        var id = ""

        if (!dataspin_belongmemberphone.isNullOrEmpty()) {
            if (pos > 0) id = dataspin_belongmemberphone!!.get(pos - 1).phone_no!!
        }
        return id
    }

    fun fillmemberphoneno(data: List<MemberPhoneDetailEntity>?) {

        if (!data.isNullOrEmpty()) {
            et_bookkeeper_s_mobile_no.setText(data.get(0).phone_no)
        } else {
            et_bookkeeper_s_mobile_no.setText("")

        }

    }

    private fun fillRadio() {
        datashgtype = lookupViewmodel!!.getlookup(
            31,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        datapvtg = lookupViewmodel!!.getlookup(
            18,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        databookkeeper = lookupViewmodel!!.getlookup(
            44,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        datayes = lookupViewmodel!!.getlookup(
            9,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        datarevival = lookupViewmodel!!.getlookupnotin(
            51, 3,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        validate!!.fillradio(rgShgType, 0, datashgtype, this)
        validate!!.fillradio(rgBookeeper, 0, databookkeeper, this)
        validate!!.fillradio(rgvolunteer, 0, datayes, this)
        validate!!.fillradio(rgrevival, 0, datarevival, this)
    }

    private fun fillSpinner() {

        dataspin_funding =
            fundingViewModel!!.getfundingAgency(validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!)
        dataspin_belong =
            memberviewmodel!!.getAllMemberDeslist(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))

        dataPanchayat =
            locationViewModel!!.getPanchayatByPanchayatCode(sStateCode, sDistrictCode, sBlockCode)
        validate!!.fillPanchayatSpinner(this, spin_panchayat, dataPanchayat)

        data_tags = lookupViewmodel!!.getlookup(
            18,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataVillage = locationViewModel!!.getVillagedata_by_panchayatCode(
            sStateCode,
            sDistrictCode,
            sBlockCode,
            sPanchayatCode
        )
        validate!!.fillVillageSpinner(this, spin_village, dataVillage)

        dataspin_name = lookupViewmodel!!.getlookup(
            1,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_pramotedby = lookupViewmodel!!.getlookup(
            28,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        /*  dataspin_mode = lookupViewmodel!!.getlookup(
              17,
              validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
          )*/
        dataspin_meetingfreq = lookupViewmodel!!.getlookup(
            19,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_weekday = lookupViewmodel!!.getlookup(
            34,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_fortnight = lookupViewmodel!!.getlookup(
            16,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_monthly = lookupViewmodel!!.getlookup(
            15,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_monthlydate = lookupViewmodel!!.getlookup(
            14,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_fortnightdate = lookupViewmodel!!.getlookupbycode_data(
            14,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_activity = lookupViewmodel!!.getlookup(
            47,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_status = lookupViewmodel!!.getlookup(
            5,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_federation = lookupViewmodel!!.getlookup(
            999,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_inactiveReason = lookupViewmodel!!.getlookup(
            96,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        validate!!.fillspinner(this, spin_inactivereason, dataspin_inactiveReason)

        validate!!.fillspinner(this, spin_ParentFedration, dataspin_federation)
        spin_ParentFedration.isEnabled = false
        validate!!.fillspinner(this, spin_promotedby, dataspin_pramotedby)
        validate!!.fillFundingspinner(this, spin_FundingAgency, dataspin_funding)
        validate!!.fillspinner(this, spin_frequency, dataspin_meetingfreq)
        validate!!.fillspinner(this, spin_weekday, dataspin_weekday)
        validate!!.fillspinner(this, spin_primaryActivity, dataspin_activity)
        validate!!.fillspinner(this, spin_secondaryActivity, dataspin_activity)
        validate!!.fillspinner(this, spin_tertiaryActivity, dataspin_activity)
        validate!!.fillspinner(this, spin_status, dataspin_status)
        validate!!.fillspinner(this, spin_savingfrequency, dataspin_meetingfreq)
        validate!!.fillspinner(this, spin_fortnight, dataspin_fortnight)
        validate!!.fillspinner(this, spin_monthly, dataspin_monthly)
        validate!!.fillspinner(this, spin_fortnightdate, dataspin_fortnightdate)
        validate!!.fillspinner(this, spin_monthlydate, dataspin_monthlydate)
        validate!!.fillspinner(this, spin_tags, data_tags)
//        validate!!.fillspinner(this, spin_weeklyday, dataspin_meetingfreq)
//        validate!!.fillspinner(this, spin_monthalyyday, dataspin_meetingfreq)
//        validate!!.fillspinner(this, spin_fortnihtday, dataspin_meetingfreq)

        spin_primaryActivity?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {
                    spin_secondaryActivity.isEnabled = true
                    var p_activity =
                        validate!!.returnlookupcode(spin_primaryActivity, dataspin_activity)
                    if (p_activity == validate!!.returnlookupcode(
                            spin_secondaryActivity,
                            dataspin_activity
                        )
                    ) {
                        validate!!.CustomAlertSpinner(
                            this@BasicDetailActivity,
                            spin_primaryActivity,
                            LabelSet.getText(
                                "primary_secondary_activity",
                                R.string.primary_secondary_activity
                            )
                        )
                    } else if (p_activity == validate!!.returnlookupcode(
                            spin_tertiaryActivity,
                            dataspin_activity
                        )
                    ) {
                        validate!!.CustomAlertSpinner(
                            this@BasicDetailActivity,
                            spin_primaryActivity,
                            LabelSet.getText(
                                "primary_tertiary_activity",
                                R.string.primary_tertiary_activity
                            )
                        )
                    }
                } else {
                    spin_secondaryActivity.isEnabled = false
                    spin_tertiaryActivity.isEnabled = false
                    spin_secondaryActivity.setSelection(0)
                    spin_tertiaryActivity.setSelection(0)
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        spin_secondaryActivity?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {
                    spin_tertiaryActivity.isEnabled = true
                    var s_activity =
                        validate!!.returnlookupcode(spin_secondaryActivity, dataspin_activity)
                    if (s_activity == validate!!.returnlookupcode(
                            spin_primaryActivity,
                            dataspin_activity
                        )
                    ) {
                        validate!!.CustomAlertSpinner(
                            this@BasicDetailActivity,
                            spin_secondaryActivity,
                            LabelSet.getText(
                                "secondary_primary_activity",
                                R.string.secondary_primary_activity
                            )
                        )
                    }
                    if (s_activity == validate!!.returnlookupcode(
                            spin_tertiaryActivity,
                            dataspin_activity
                        )
                    ) {
                        validate!!.CustomAlertSpinner(
                            this@BasicDetailActivity,
                            spin_secondaryActivity,
                            LabelSet.getText(
                                "secondary_tertiary_activity",
                                R.string.secondary_tertiary_activity
                            )
                        )
                    }
                } else {
                    spin_tertiaryActivity.isEnabled = false
                    spin_tertiaryActivity.setSelection(0)
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        spin_tertiaryActivity?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {
                    var t_activity =
                        validate!!.returnlookupcode(spin_tertiaryActivity, dataspin_activity)
                    if (t_activity == validate!!.returnlookupcode(
                            spin_primaryActivity,
                            dataspin_activity
                        )
                    ) {
                        validate!!.CustomAlertSpinner(
                            this@BasicDetailActivity,
                            spin_tertiaryActivity,
                            LabelSet.getText(
                                "tertiary_primary_activity",
                                R.string.tertiary_primary_activity
                            )
                        )
                    } else if (t_activity == validate!!.returnlookupcode(
                            spin_secondaryActivity,
                            dataspin_activity
                        )
                    ) {
                        validate!!.CustomAlertSpinner(
                            this@BasicDetailActivity,
                            spin_tertiaryActivity,
                            LabelSet.getText(
                                "tertiary_secondary_activity",
                                R.string.tertiary_secondary_activity
                            )
                        )
                    }
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }
        spin_frequency?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {
                    var frequency =
                        validate!!.returnlookupcode(spin_frequency, dataspin_meetingfreq)

                    if (frequency == 1) {
                        lay_weekday.visibility = View.VISIBLE
                        lay_fortnight.visibility = View.GONE
                        lay_monthly.visibility = View.GONE
                        lay_fortnightdate.visibility = View.GONE
                        lay_monthlydate.visibility = View.GONE
                        spin_fortnight.setSelection(0)
                        spin_fortnightdate.setSelection(0)
                        spin_monthly.setSelection(0)
                        spin_monthlydate.setSelection(0)
                        spin_weekday.setSelection(
                            validate!!.returnlookupcodepos(
                                iMeetingOn,
                                dataspin_weekday
                            )
                        )
                    } else if (frequency == 2) {
                        lay_fortnight.visibility = View.VISIBLE
                        spin_fortnight.setSelection(
                            validate!!.returnlookupcodepos(
                                ifrequency_value,
                                dataspin_fortnight
                            )
                        )
                        lay_monthly.visibility = View.GONE
                        lay_monthlydate.visibility = View.GONE
                        lay_weekday.visibility = View.GONE
                        spin_monthly.setSelection(0)
                        spin_monthlydate.setSelection(0)
                        spin_weekday.setSelection(0)

                    } else if (frequency == 3) {
                        lay_monthly.visibility = View.VISIBLE
                        lay_fortnight.visibility = View.GONE
                        lay_fortnightdate.visibility = View.GONE
                        lay_monthlydate.visibility = View.GONE
                        lay_weekday.visibility = View.GONE
                        spin_fortnight.setSelection(0)
                        spin_fortnightdate.setSelection(0)
                        spin_monthly.setSelection(0)
                        spin_monthlydate.setSelection(0)
                        spin_weekday.setSelection(0)
                        spin_monthly.setSelection(
                            validate!!.returnlookupcodepos(
                                ifrequency_value,
                                dataspin_monthly
                            )
                        )
                    }


                    if (savingfrequencyid > 0) {
                        if (iFrequency_db == frequency) {
                            spin_savingfrequency.setSelection(
                                validate!!.returnlookupcodepos(
                                    savingfrequencyid,
                                    dataspin_meetingfreq
                                )
                            )
                        } else {
                            savingfrequencyid
                            spin_savingfrequency.setSelection(0)
                        }
                        // savingfrequencyid=0
                    }/*else{
    spin_savingfrequency.setSelection(0)
    }*/
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        spin_savingfrequency?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {
                    // savingfrequencyid--
                    var frequency =
                        validate!!.returnlookupcode(spin_frequency, dataspin_meetingfreq)
                    if (frequency == 2) {
                        var savingfrequency =
                            validate!!.returnlookupcode(spin_savingfrequency, dataspin_meetingfreq)
                        if (savingfrequency == 1) {
                            validate!!.CustomAlert(
                                LabelSet.getText(
                                    "monthlyfortnightly",
                                    R.string.monthlyfortnightly
                                ),
                                this@BasicDetailActivity
                            )
                            spin_savingfrequency.setSelection(0)
                            // savingfrequencyid=0
                        }
                    } else if (frequency == 3) {
                        var savingfrequency =
                            validate!!.returnlookupcode(spin_savingfrequency, dataspin_meetingfreq)
                        if (savingfrequency == 1 || savingfrequency == 2) {
                            validate!!.CustomAlert(
                                LabelSet.getText(
                                    "yuocanselectmonthaly",
                                    R.string.yuocanselectmonthaly
                                ),
                                this@BasicDetailActivity
                            )
                            spin_savingfrequency.setSelection(0)
                            //  savingfrequencyid=0
                        }
                    }
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }
        spin_fortnight?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {
                    var fortnightId =
                        validate!!.returnlookupcode(spin_fortnight, dataspin_fortnight)
                    if (fortnightId == 2 || fortnightId == 3) {
                        lay_weekday.visibility = View.VISIBLE
                        lay_fortnightdate.visibility = View.GONE
                        lay_monthlydate.visibility = View.GONE
                        spin_fortnightdate.setSelection(0)
                        spin_monthlydate.setSelection(0)
                        spin_weekday.setSelection(
                            validate!!.returnlookupcodepos(
                                iMeetingOn,
                                dataspin_weekday
                            )
                        )
                    } else if (fortnightId == 1) {
                        lay_fortnightdate.visibility = View.VISIBLE
                        lay_weekday.visibility = View.GONE
                        lay_monthlydate.visibility = View.GONE
                        spin_monthlydate.setSelection(0)
                        spin_weekday.setSelection(0)
                        spin_fortnightdate.setSelection(
                            validate!!.returnlookupcodepos(
                                iMeetingOn,
                                dataspin_fortnightdate
                            )
                        )
                    }
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        spin_monthly?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {
                    var monthlyId =
                        validate!!.returnlookupcode(spin_monthly, dataspin_monthly)
                    if (monthlyId == 1 || monthlyId == 2 || monthlyId == 3 || monthlyId == 4 || monthlyId == 5) {
                        lay_weekday.visibility = View.VISIBLE
                        lay_fortnightdate.visibility = View.GONE
                        lay_monthlydate.visibility = View.GONE
                        spin_fortnightdate.setSelection(0)
                        spin_monthlydate.setSelection(0)
                        spin_weekday.setSelection(
                            validate!!.returnlookupcodepos(
                                iMeetingOn,
                                dataspin_weekday
                            )
                        )
                    } else if (monthlyId == 6) {
                        lay_monthlydate.visibility = View.VISIBLE
                        lay_weekday.visibility = View.GONE
                        lay_fortnightdate.visibility = View.GONE
                        spin_fortnightdate.setSelection(0)
                        spin_weekday.setSelection(0)
                        spin_monthlydate.setSelection(
                            validate!!.returnlookupcodepos(
                                iMeetingOn,
                                dataspin_monthlydate
                            )
                        )
                    }
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        spin_status.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position > 0) {
                    var status = validate!!.returnlookupcode(spin_status, dataspin_status)
                    if (status == 1) {
                        lay_inactive_reason.visibility = View.GONE
                        spin_inactivereason.setSelection(0)
                    } else {
                        lay_inactive_reason.visibility = View.VISIBLE
                    }
                } else {
                    lay_inactive_reason.visibility = View.GONE
                    spin_inactivereason.setSelection(0)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

        }
    }


    private fun showData() {
        try {
            val list =
                shgViewModel!!.getSHGdetail(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
            if (!list.isNullOrEmpty()) {
                isVerified = validate!!.returnIntegerValue(list.get(0).is_verified.toString())
                if (isVerified == 1) {
                    btn_save.text = LabelSet.getText(
                        "save_verify",
                        R.string.save_verify
                    )

                } else {
                    btn_save.text = LabelSet.getText(
                        "save",
                        R.string.save
                    )
                }
                if (validate!!.returnIntegerValue(list.get(0).panchayat_id.toString()) > 0) {
                    spin_panchayat.setSelection(
                        validate!!.returnPanchayatpos(
                            list.get(0).panchayat_id,
                            dataPanchayat
                        )
                    )
                }
                spin_panchayat.isEnabled = false
                if (validate!!.returnIntegerValue(list.get(0).village_id.toString()) > 0) {
                    sVillageCode = list.get(0).village_id!!
                    spin_village.setSelection(
                        validate!!.returnVillagepos(
                            list.get(0).village_id,
                            dataVillage
                        )
                    )
                }
                spin_village.isEnabled = false
                et_groupname.setText(validate!!.returnStringValue(list.get(0).shg_name))
                et_shgcode.setText(validate!!.returnStringValue(list.get(0).shg_code))

                et_formationDate.setText(
                    validate!!.convertDatetime(
                        validate!!.returnLongValue(
                            list.get(
                                0
                            ).shg_formation_date.toString()
                        )
                    )
                )
                et_revivalDate.setText(
                    validate!!.convertDatetime(
                        validate!!.returnLongValue(
                            list.get(
                                0
                            ).shg_revival_date.toString()
                        )
                    )
                )
                et_coopt_Date.setText(
                    validate!!.convertDatetime(
                        validate!!.returnLongValue(
                            list.get(
                                0
                            ).shg_cooption_date.toString()
                        )
                    )
                )
                if (validate!!.returnLongValue(
                        list.get(
                            0
                        ).shg_cooption_date.toString()
                    ) > 0L
                ) {
                    validate!!.fillradio(rgrevival, 2, datarevival, this)
                    rgrevivalid = 2
                    lay_coptdate.visibility = View.VISIBLE
                    lay_revivalDate.visibility = View.GONE
                } else if (validate!!.returnLongValue(
                        list.get(
                            0
                        ).shg_revival_date.toString()
                    ) > 0L
                ) {
                    validate!!.fillradio(rgrevival, 3, datarevival, this)
                    rgrevivalid = 3
                    lay_coptdate.visibility = View.GONE
                    lay_revivalDate.visibility = View.VISIBLE
                } else {
                    validate!!.fillradio(rgrevival, 1, datarevival, this)
                    rgrevivalid = 1
                    lay_coptdate.visibility = View.GONE
                    lay_revivalDate.visibility = View.GONE
                }
                var amout = validate!!.returnIntegerValue(list.get(0).month_comp_saving.toString())
                et_saving.setText(amout.toString())
                spin_frequency.setSelection(
                    validate!!.returnlookupcodepos(
                        validate!!.returnIntegerValue(list.get(0).meeting_frequency.toString()),
                        dataspin_meetingfreq
                    )
                )
                iFrequency_db = validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(list.get(0).meeting_frequency.toString()),
                    dataspin_meetingfreq
                )
                iMeetingOn = validate!!.returnIntegerValue(list.get(0).meeting_on.toString())
                ifrequency_value =
                    validate!!.returnIntegerValue(list.get(0).meeting_frequency_value.toString())

                spin_savingfrequency.setSelection(
                    validate!!.returnlookupcodepos(
                        validate!!.returnIntegerValue(list.get(0).saving_frequency.toString()),
                        dataspin_meetingfreq
                    )
                )
                savingfrequencyid =
                    validate!!.returnIntegerValue(list.get(0).saving_frequency.toString())
                spin_ParentFedration.setSelection(
                    validate!!.returnlookupcodepos(
                        validate!!.returnIntegerValue(list.get(0).parent_cbo_code.toString()),
                        dataspin_name
                    )
                )
                spin_status.setSelection(
                    validate!!.returnlookupcodepos(
                        validate!!.returnIntegerValue(list.get(0).status.toString()),
                        dataspin_status
                    )
                )
                spin_promotedby.setSelection(
                    validate!!.returnlookupcodepos(
                        validate!!.returnIntegerValue(list.get(0).shg_promoted_by.toString()),
                        dataspin_pramotedby
                    )
                )

                spin_FundingAgency.setSelection(
                    validate!!.returnFundingpos(
                        validate!!.returnIntegerValue(
                            list.get(0).promoter_code.toString()
                        ), dataspin_funding
                    )
                )
                et_promoter_name.setText(validate!!.returnStringValue(list.get(0).promoter_name))
                spin_primaryActivity.setSelection(
                    validate!!.returnlookupcodepos(
                        validate!!.returnIntegerValue(list.get(0).primary_activity.toString()),
                        dataspin_activity
                    )
                )

                spin_secondaryActivity.setSelection(
                    validate!!.returnlookupcodepos(
                        validate!!.returnIntegerValue(list.get(0).secondary_activity.toString()),
                        dataspin_activity
                    )
                )

                spin_tertiaryActivity.setSelection(
                    validate!!.returnlookupcodepos(
                        validate!!.returnIntegerValue(list.get(0).tertiary_activity.toString()),
                        dataspin_activity
                    )
                )
                validate!!.fillradio(
                    rgShgType,
                    validate!!.returnIntegerValue(list.get(0).shg_type_code.toString()),
                    datashgtype,
                    this
                )
                validate!!.fillradio(
                    rgBookeeper,
                    validate!!.returnIntegerValue(list.get(0).bookkeeper_identified.toString()),
                    databookkeeper,
                    this
                )
                if (validate!!.returnIntegerValue(list.get(0).bookkeeper_identified.toString()) == 1) {
                    spin_member.setSelection(
                        setMemberName(
                            validate!!.returnStringValue(list.get(0).bookkeeper_name),
                            dataspin_belong
                        )
                    )
                } else {
                    et_bookkeeper_name.setText(validate!!.returnStringValue(list.get(0).bookkeeper_name))
                }

                spin_tags.setSelection(
                    validate!!.returnlookupcodepos(
                        validate!!.returnIntegerValue(
                            list.get(0).tags.toString()
                        ), data_tags
                    )
                )
                spin_inactivereason.setSelection(
                    validate!!.returnlookupcodepos(
                        validate!!.returnIntegerValue(list.get(0).inactive_reason.toString()),
                        dataspin_inactiveReason
                    )
                )
                validate!!.fillradio(
                    rgvolunteer,
                    validate!!.returnIntegerValue(list.get(0).is_voluntary_saving.toString()),
                    datayes,
                    this
                )
                et_ComsavingRoi.setText(validate!!.returnStringValue(list.get(0).savings_interest.toString()))
                et_voluntarysavingROI.setText(validate!!.returnStringValue(list.get(0).voluntary_savings_interest.toString()))
                et_bookkeeper_s_mobile_no.setText(validate!!.returnStringValue(list.get(0).bookkeeper_mobile.toString()))
                et_electiontenure.setText(validate!!.returnStringValue(list.get(0).election_tenure.toString()))
                et_OtherTags.setText(validate!!.returnStringValue(list.get(0).shg_type_other))
                val mediaStorageDirectory =  File(
                    getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
                    AppSP.IMAGE_DIRECTORY_NAME
                )
                imageName = validate!!.returnStringValue(list.get(0).shg_resolution)
                if (imageName.contains("pdf")) {
                    Imgshow.setImageResource(R.drawable.pdf)
                } else {
                    var mediaFile1 =
                        File(mediaStorageDirectory.path + File.separator + imageName)
                    Picasso.with(this).load(mediaFile1).into(Imgshow)
                }
                /*     spin_mode.setSelection(
                         validate!!.returnlookupcodepos(
                             list.get(0).mode,
                             dataspin_mode
                         )
                     )*/
                lay_status.visibility = View.VISIBLE
                if (isVerified == 1) {
                    btn_save.text = LabelSet.getText(
                        "save_verify",
                        R.string.save_verify
                    )
                    lay_groupname.setBackgroundColor(Color.parseColor("#D8FFD8"))
                    // tv_shgName.setTextColor(Color.parseColor("#006400"))
                    lay_panchayat.setBackgroundColor(Color.parseColor("#D8FFD8"))
                    lay_villge.setBackgroundColor(Color.parseColor("#D8FFD8"))
                    lay_frmationDate.setBackgroundColor(Color.parseColor("#D8FFD8"))
                    lay_promotedby.setBackgroundColor(Color.parseColor("#D8FFD8"))
                    lay_frequency.setBackgroundColor(Color.parseColor("#D8FFD8"))
                    lay_compulsorySaving.setBackgroundColor(Color.parseColor("#D8FFD8"))
                    lay_status.setBackgroundColor(Color.parseColor("#D8FFD8"))
                    lay_bookkeeper.setBackgroundColor(Color.parseColor("#D8FFD8"))
                    lay_revivalDate.setBackgroundColor(Color.parseColor("#D8FFD8"))
                    lay_coptdate.setBackgroundColor(Color.parseColor("#D8FFD8"))
                } else {
                    btn_save.text = LabelSet.getText(
                        "save",
                        R.string.save
                    )
                }
            } else {
                lay_status.visibility = View.GONE
                et_ComsavingRoi.setText("0")
                spin_panchayat.setSelection(
                    validate!!.returnPanchayatpos(
                        validate!!.RetriveSharepreferenceInt(
                            AppSP.panchayatcode
                        ).toInt(), dataPanchayat
                    )
                )
                sVillageCode = validate!!.RetriveSharepreferenceInt(AppSP.villagecode)


            }
        } catch (ex: Exception) {
            Log.d("Excetion value:", ex.toString())
        }
    }

    private fun SaveData() {
        var promotor_name = ""
        var promotor_code: Int? = null
        if (lay_promoter_name.visibility == View.VISIBLE) {
            promotor_name = validate!!.returnStringValue(et_promoter_name.text.toString())
        } else {
            promotor_name = ""
        }
        if (lay_fundingAgency.visibility == View.VISIBLE) {
            promotor_code = validate!!.returnFundingcode(spin_FundingAgency, dataspin_funding)
        } else {
            promotor_code = null
        }
        guid = validate!!.random()
        shgcode = et_shgcode.text.toString()
        shgName = et_groupname.text.toString()
        if (isVerified == 1 || isVerified == 3) {
            isVerified = 3
        } else if (isVerified == 9) {
            isVerified = 9
        } else {
            isVerified = 0
        }
        var book_keeper_name = ""
        if (rgBookeeper.checkedRadioButtonId == 1) {
            book_keeper_name = returnMemberName()
        } else {
            book_keeper_name = et_bookkeeper_name.text.toString()
        }
        var iTag_value = 0
        var shg_type_other = ""
        var panchaytcode = validate!!.returnPanchayatID(spin_panchayat, dataPanchayat)
        var villagecode = validate!!.returnVillageID(spin_village, dataVillage)
        var meetingfrequency = validate!!.returnlookupcode(spin_frequency, dataspin_meetingfreq)
        var meetingfrequencyvalue = 0
        var meetingon = 0
        if (lay_fortnight.visibility == View.VISIBLE) {
            meetingfrequencyvalue = validate!!.returnlookupcode(spin_fortnight, dataspin_fortnight)
        } else if (lay_monthly.visibility == View.VISIBLE) {
            meetingfrequencyvalue = validate!!.returnlookupcode(spin_monthly, dataspin_monthly)
        }
        if (lay_weekday.visibility == View.VISIBLE) {
            meetingon = validate!!.returnlookupcode(spin_weekday, dataspin_weekday)
        } else if (lay_fortnightdate.visibility == View.VISIBLE) {
            meetingon = validate!!.returnlookupcode(spin_fortnightdate, dataspin_fortnightdate)
        } else if (lay_monthlydate.visibility == View.VISIBLE) {
            meetingon = validate!!.returnlookupcode(spin_monthlydate, dataspin_monthlydate)
        }
        if (lay_special.visibility == View.VISIBLE) {
            iTag_value = validate!!.returnlookupcode(spin_tags, data_tags)
        } else {
            iTag_value = 0
        }
        if (lay_tags_other.visibility == View.VISIBLE) {
            shg_type_other = validate!!.returnStringValue(et_OtherTags.text.toString())
        } else {
            shg_type_other = ""
        }
        //     vaif(layr mode = validate!!.returnlookupcode(spin_mode, dataspin_mode)
        var pramotedby = validate!!.returnlookupcode(spin_promotedby, dataspin_pramotedby)
        var federation = validate!!.returnlookupcode(spin_ParentFedration, dataspin_name)
        var priactivity = validate!!.returnlookupcode(spin_primaryActivity, dataspin_activity)
        var secactivity = validate!!.returnlookupcode(spin_secondaryActivity, dataspin_activity)
        var teractivity = validate!!.returnlookupcode(spin_tertiaryActivity, dataspin_activity)
        var status = validate!!.returnlookupcode(spin_status, dataspin_status)

        if (validate!!.RetriveSharepreferenceString(AppSP.SHGGUID).isNullOrEmpty()) {
//
            shgEntity = SHGEntity(
                0,
                guid,
                "0",
                //shgcode
                validate!!.RetriveSharepreferenceInt(AppSP.statecode),
                validate!!.RetriveSharepreferenceInt(AppSP.districtcode),
                validate!!.RetriveSharepreferenceInt(AppSP.blockcode),
                villagecode,
                panchaytcode,
                "",
                shgName,
                "",
                "",
                rgShgType.checkedRadioButtonId,
                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),
                "",
                0,
                validate!!.Daybetweentime(et_formationDate.text.toString()),
                validate!!.Daybetweentime(et_revivalDate.text.toString()),
                pramotedby,
                promotor_code,
                promotor_name,
                0,
                meetingfrequency,
                meetingfrequencyvalue,
                meetingon,
                0,
                validate!!.returnIntegerValue(et_saving.text.toString()),
                0,
                0,
                federation,
                0,
                1,
                1,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                "",
                0,
                "",
                rgBookeeper.checkedRadioButtonId,
                0,
                0,
                0,
                1,
                0,
                "",
                "",
                0,
                "",
                validate!!.Daybetweentime(validate!!.currentdatetime),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                0,
                "",
                priactivity,
                secactivity,
                teractivity,
                0.0,
                0.0,
                validate!!.returnDoubleValue(et_ComsavingRoi.text.toString()),
                validate!!.returnDoubleValue(et_voluntarysavingROI.text.toString()),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                0,
                0,
                0,
                book_keeper_name,
                et_bookkeeper_s_mobile_no.text.toString(),
                validate!!.returnIntegerValue(et_electiontenure.text.toString()),
                rgvolunteer.checkedRadioButtonId,
                validate!!.returnlookupcode(spin_savingfrequency, dataspin_meetingfreq),
                iTag_value,
                "",
                0,
                0,
                0,
                validate!!.Daybetweentime(et_coopt_Date.text.toString()),
                1,
                0,
                imageName, "",
                shg_type_other, 0, 1, 0, 0
            )

            shgViewModel!!.insert(shgEntity!!)
            var image1 = ImageuploadEntity(
                imageName,
                guid,
                "",
                1,
                0
            )
            if (imageName.isNotEmpty() && newimageName) {
                imageUploadViewmodel!!.insert(image1)
            }
            validate!!.SaveSharepreferenceString(AppSP.SHGGUID, guid)
            validate!!.SaveSharepreferenceString(AppSP.Shgcode, shgcode)
            validate!!.SaveSharepreferenceString(AppSP.ShgName, shgName)
            validate!!.SaveSharepreferenceLong(
                AppSP.Formation_dt,
                validate!!.Daybetweentime(et_formationDate.text.toString())
            )
            validate!!.SaveSharepreferenceInt(AppSP.CboType, rgShgType.checkedRadioButtonId)
            validate!!.SaveSharepreferenceInt(AppSP.Tag, iTag_value)
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this,
                PhoneDetailListActivity::class.java
            )
        } else {
            shgViewModel!!.updateBasicDetail(
                validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!,
                "",
                shgName,
                "",
                "",
                rgShgType.checkedRadioButtonId,
                "",
                0,
                validate!!.Daybetweentime(et_formationDate.text.toString()),
                validate!!.Daybetweentime(et_revivalDate.text.toString()),
                validate!!.Daybetweentime(et_coopt_Date.text.toString()),
                pramotedby,
                0,
                meetingfrequency,
                meetingfrequencyvalue,
                meetingon,
                0,
                validate!!.returnIntegerValue(et_saving.text.toString()),
                0,
                0,
                federation,
                0,
                status,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                "",
                0,
                "",
                rgBookeeper.checkedRadioButtonId,
                0,
                0,
                1,
                "",
                "",
                validate!!.Daybetweentime(validate!!.currentdatetime),
                validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                priactivity,
                secactivity,
                teractivity,
                book_keeper_name,
                et_bookkeeper_s_mobile_no.text.toString(),
                validate!!.returnIntegerValue(et_electiontenure.text.toString()),
                validate!!.returnDoubleValue(et_ComsavingRoi.text.toString()),
                validate!!.returnDoubleValue(et_voluntarysavingROI.text.toString()),
                rgvolunteer.checkedRadioButtonId,
                validate!!.returnlookupcode(spin_savingfrequency, dataspin_meetingfreq),
                iTag_value,
                promotor_name,
                promotor_code,
                imageName,
                shg_type_other, isVerified,
                validate!!.returnlookupcode(spin_inactivereason, dataspin_inactiveReason)
            )
            validate!!.SaveSharepreferenceString(AppSP.ShgName, shgName)

            validate!!.updateLockingFlag(
                validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!,
                shgViewModel, memberviewmodel
            )

            var image1 = ImageuploadEntity(
                imageName,
                validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),
                "",
                1,
                0
            )
            if (imageName.isNotEmpty() && newimageName) {
                imageUploadViewmodel!!.insert(image1)
            }
            validate!!.SaveSharepreferenceLong(
                AppSP.Formation_dt,
                validate!!.Daybetweentime(et_formationDate.text.toString())
            )
            CDFIApplication.database?.shgDao()
                ?.updatecompletestatus(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!)


            validate!!.CustomAlert(
                LabelSet.getText(
                    "updated_successfully",
                    R.string.updated_successfully
                ),
                this,
                PhoneDetailListActivity::class.java
            )
        }

    }

    private fun PartialSaveData() {
        if (CheckData() == 0) {

            var promotor_name = ""
            var promotor_code: Int? = null
            if (lay_promoter_name.visibility == View.VISIBLE) {
                promotor_name = validate!!.returnStringValue(et_promoter_name.text.toString())
            } else {
                promotor_name = ""
            }
            if (lay_fundingAgency.visibility == View.VISIBLE) {
                promotor_code = validate!!.returnFundingcode(spin_FundingAgency, dataspin_funding)
            } else {
                promotor_code = null
            }
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (isVerified == 1 || isVerified == 3) {
                    isVerified = 3
                } else if (isVerified == 9) {
                    isVerified = 9
                } else {
                    isVerified = 0
                }
                guid = validate!!.random()
                shgcode = et_shgcode.text.toString()
                shgName = et_groupname.text.toString()
                var book_keeper_name = ""
                if (rgBookeeper.checkedRadioButtonId == 1) {
                    book_keeper_name = returnMemberName()
                } else {
                    book_keeper_name = et_bookkeeper_name.text.toString()
                }
                var iTag_value = 0
                var shg_type_other = ""
                var panchaytcode = validate!!.returnPanchayatID(spin_panchayat, dataPanchayat)
                var villagecode = validate!!.returnVillageID(spin_village, dataVillage)
                var meetingfrequency =
                    validate!!.returnlookupcode(spin_frequency, dataspin_meetingfreq)
                var meetingfrequencyvalue = 0
                var meetingon = 0
                if (lay_fortnight.visibility == View.VISIBLE) {
                    meetingfrequencyvalue =
                        validate!!.returnlookupcode(spin_fortnight, dataspin_fortnight)
                } else if (lay_monthly.visibility == View.VISIBLE) {
                    meetingfrequencyvalue =
                        validate!!.returnlookupcode(spin_monthly, dataspin_monthly)
                }
                if (lay_weekday.visibility == View.VISIBLE) {
                    meetingon = validate!!.returnlookupcode(spin_weekday, dataspin_weekday)
                } else if (lay_fortnightdate.visibility == View.VISIBLE) {
                    meetingon =
                        validate!!.returnlookupcode(spin_fortnightdate, dataspin_fortnightdate)
                } else if (lay_monthlydate.visibility == View.VISIBLE) {
                    meetingon = validate!!.returnlookupcode(spin_monthlydate, dataspin_monthlydate)
                }
                if (lay_special.visibility == View.VISIBLE) {
                    iTag_value = validate!!.returnlookupcode(spin_tags, data_tags)
                } else {
                    iTag_value = 0
                }
                //     vaif(layr mode = validate!!.returnlookupcode(spin_mode, dataspin_mode)
                var pramotedby = validate!!.returnlookupcode(spin_promotedby, dataspin_pramotedby)
                var federation = validate!!.returnlookupcode(spin_ParentFedration, dataspin_name)
                var priactivity =
                    validate!!.returnlookupcode(spin_primaryActivity, dataspin_activity)
                var secactivity =
                    validate!!.returnlookupcode(spin_secondaryActivity, dataspin_activity)
                var teractivity =
                    validate!!.returnlookupcode(spin_tertiaryActivity, dataspin_activity)
                var status = validate!!.returnlookupcode(spin_status, dataspin_name)
                if (lay_tags_other.visibility == View.VISIBLE) {
                    shg_type_other = validate!!.returnStringValue(et_OtherTags.text.toString())
                } else {
                    shg_type_other = ""
                }
                if (validate!!.RetriveSharepreferenceString(AppSP.SHGGUID).isNullOrEmpty()) {
//
                    shgEntity = SHGEntity(
                        0,
                        guid,
                        "0",
                        //shgcode
                        validate!!.RetriveSharepreferenceInt(AppSP.statecode),
                        validate!!.RetriveSharepreferenceInt(AppSP.districtcode),
                        validate!!.RetriveSharepreferenceInt(AppSP.blockcode),
                        villagecode,
                        panchaytcode,
                        "",
                        shgName,
                        "",
                        "",
                        rgShgType.checkedRadioButtonId,
                        validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),
                        "",
                        0,
                        validate!!.Daybetweentime(et_formationDate.text.toString()),
                        validate!!.Daybetweentime(et_revivalDate.text.toString()),
                        pramotedby,
                        promotor_code,
                        promotor_name,
                        0,
                        meetingfrequency,
                        meetingfrequencyvalue,
                        meetingon,
                        0,
                        validate!!.returnIntegerValue(et_saving.text.toString()),
                        0,
                        0,
                        federation,
                        0,
                        1,
                        1,
                        1,
                        1,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        "",
                        0,
                        "",
                        rgBookeeper.checkedRadioButtonId,
                        0,
                        0,
                        0,
                        1,
                        0,
                        "",
                        "",
                        0,
                        "",
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        0,
                        "",
                        priactivity,
                        secactivity,
                        teractivity,
                        0.0,
                        0.0,
                        validate!!.returnDoubleValue(et_ComsavingRoi.text.toString()),
                        validate!!.returnDoubleValue(et_voluntarysavingROI.text.toString()),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        0,
                        0,
                        0,
                        book_keeper_name,
                        et_bookkeeper_s_mobile_no.text.toString(),
                        validate!!.returnIntegerValue(et_electiontenure.text.toString()),
                        rgvolunteer.checkedRadioButtonId,
                        validate!!.returnlookupcode(spin_savingfrequency, dataspin_meetingfreq),
                        iTag_value,
                        "",
                        0,
                        0,
                        0,
                        validate!!.Daybetweentime(et_coopt_Date.text.toString()),
                        0,
                        1,
                        imageName, "",
                        shg_type_other, 0, 1, 0, 0
                    )

                    shgViewModel!!.insert(shgEntity!!)
                    validate!!.SaveSharepreferenceString(AppSP.SHGGUID, guid)
                    validate!!.SaveSharepreferenceString(AppSP.Shgcode, shgcode)
                    validate!!.SaveSharepreferenceString(AppSP.ShgName, shgName)
                    validate!!.SaveSharepreferenceLong(
                        AppSP.Formation_dt,
                        validate!!.Daybetweentime(et_formationDate.text.toString())
                    )
                    validate!!.SaveSharepreferenceInt(AppSP.CboType, rgShgType.checkedRadioButtonId)
                    validate!!.SaveSharepreferenceInt(AppSP.Tag, iTag_value)

                } else {
                    shgViewModel!!.updateBasicDetail(
                        validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!,
                        "",
                        shgName,
                        "",
                        "",
                        rgShgType.checkedRadioButtonId,
                        "",
                        0,
                        validate!!.Daybetweentime(et_formationDate.text.toString()),
                        validate!!.Daybetweentime(et_revivalDate.text.toString()),
                        validate!!.Daybetweentime(et_coopt_Date.text.toString()),
                        pramotedby,
                        0,
                        meetingfrequency,
                        meetingfrequencyvalue,
                        meetingon,
                        0,
                        validate!!.returnIntegerValue(et_saving.text.toString()),
                        0,
                        0,
                        federation,
                        0,
                        1,
                        1,
                        1,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        "",
                        0,
                        "",
                        rgBookeeper.checkedRadioButtonId,
                        0,
                        0,
                        1,
                        "",
                        "",
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                        priactivity,
                        secactivity,
                        teractivity,
                        book_keeper_name,
                        et_bookkeeper_s_mobile_no.text.toString(),
                        validate!!.returnIntegerValue(et_electiontenure.text.toString()),
                        validate!!.returnDoubleValue(et_ComsavingRoi.text.toString()),
                        validate!!.returnDoubleValue(et_voluntarysavingROI.text.toString()),
                        rgvolunteer.checkedRadioButtonId,
                        validate!!.returnlookupcode(spin_savingfrequency, dataspin_meetingfreq),
                        iTag_value,
                        promotor_name, promotor_code, imageName, shg_type_other, isVerified,
                        validate!!.returnlookupcode(spin_inactivereason, dataspin_inactiveReason)
                    )
                    validate!!.SaveSharepreferenceString(AppSP.ShgName, shgName)
                    validate!!.SaveSharepreferenceLong(
                        AppSP.Formation_dt,
                        validate!!.Daybetweentime(et_formationDate.text.toString())
                    )
                    CDFIApplication.database?.shgDao()
                        ?.updatepartialstatus(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!)

                    validate!!.updateLockingFlag(
                        validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!,
                        shgViewModel, memberviewmodel
                    )
                }
            }
        }
    }

    /* fun returnPanchayatID(pos: Int): String {
         var sPos = ""
         var panchayat = ArrayList<PanchayatEntity>()

         panchayat = locationViewModel?.getPanchayatByPanchayatCode(
             sStateCode,
             sDistrictCode,
             sBlockCode
         ) as ArrayList<PanchayatEntity>

         if (panchayat != null && panchayat.size > 0) {
             if (pos > 0) {

                 sPos = panchayat[pos - 1].panchayat_code!!
             }
         }
         return sPos;
     }*/

    private fun CheckValidation(): Int {
        var value = 1

        var monthlyId =
            validate!!.returnlookupcode(spin_monthly, dataspin_monthly)
        var pramotedby = validate!!.returnlookupcode(spin_promotedby, dataspin_pramotedby)
        var meetingfrequency = validate!!.returnlookupcode(spin_frequency, dataspin_meetingfreq)
        //  lay_weekday.visibility = View.VISIBLE
        if (et_groupname.text.toString().trim().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "shg_name",
                    R.string.shg_name
                ),
                this,
                et_groupname
            )
            value = 0
            return value
        } else if (!validate!!.checkname(et_groupname.text.toString())) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "shg_name",
                    R.string.shg_name
                ),
                this,
                et_groupname
            )
            value = 0
            return value
        } else if (spin_panchayat.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_panchayat,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "grampanchayat",
                    R.string.grampanchayat
                )
            )
            value = 0
            return value
        } else if (spin_village.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_village,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "village",
                    R.string.village
                )
            )
            value = 0
            return value
        } else if (et_formationDate.text.toString().trim().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "formation_date",
                    R.string.formation_date
                ),
                this,
                et_formationDate
            )
            value = 0
            return value
        } else if (spin_promotedby.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_promotedby,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "promoted_by",
                    R.string.promoted_by
                )
            )
            value = 0
            return value
        } else if (spin_promotedby.selectedItemPosition == 3 && spin_FundingAgency.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_FundingAgency,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "promoters_name",
                    R.string.promoters_name
                )
            )
            value = 0
            return value
        } else if (lay_promoter_name.visibility == View.VISIBLE && validate!!.returnStringValue(
                et_promoter_name.text.toString()
            ).trim().length == 0
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "promoters_name",
                    R.string.promoters_name
                ),
                this,
                et_promoter_name
            )
            value = 0
            return value
        } else if (pramotedby == 1 && rgrevival.checkedRadioButtonId == 2) {
            validate!!.CustomAlertRadio(
                this,
                LabelSet.getText(
                    "new_can_not_be_selected",
                    R.string.new_can_not_be_selected
                ), rgrevival

            )
            value = 0
            return value
        } else if (lay_revivalDate.visibility == View.VISIBLE && validate!!.returnStringValue(
                et_revivalDate.text.toString()
            ).trim().length == 0
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "revival_date",
                    R.string.revival_date
                ),
                this,
                et_revivalDate
            )
            value = 0
            return value
        } else if (lay_coptdate.visibility == View.VISIBLE && validate!!.returnStringValue(
                et_coopt_Date.text.toString()
            ).trim().length == 0
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "co_optdate",
                    R.string.co_optdate
                ),
                this,
                et_coopt_Date
            )
            value = 0
            return value
        } else if (rgShgType.checkedRadioButtonId == 0) {
            validate!!.CustomAlertRadio(
                this,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "shg_type",
                    R.string.shg_type
                ), rgShgType

            )
            value = 0
            return value
        } else if (validate!!.GetAnswerTypeRadioButtonID(rgShgType) == 2 && spin_tags.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_tags,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText("tags", R.string.tags)

            )
            value = 0
            return value
        } else if (validate!!.returnlookupcode(
                spin_tags,
                data_tags
            ) == 99 && et_OtherTags.text.toString().trim().length == 0
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "other_specify",
                    R.string.other_specify
                ),
                this,
                et_OtherTags
            )
            value = 0
            return value
        } else if (spin_frequency.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_frequency,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "meeting_frequency",
                    R.string.meeting_frequency
                )
            )
            value = 0
            return value
        } else if (spin_savingfrequency.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_savingfrequency,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "saving_frequency",
                    R.string.saving_frequency
                )
            )
            value = 0
            return value
        } else if (validate!!.returnIntegerValue(et_saving.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "cumpulsory_saving",
                    R.string.cumpulsory_saving
                ),
                this,
                et_saving
            )
            value = 0
            return value
        } else if ((meetingfrequency == 1 || meetingfrequency == 2) && validate!!.returnIntegerValue(
                et_saving.text.toString()
            ) > 1000
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "weekly_less_complusory_saving",
                    R.string.weekly_less_complusory_saving
                ),
                this,
                et_saving
            )
            value = 0
            return value
        } else if (meetingfrequency == 3 && validate!!.returnIntegerValue(et_saving.text.toString()) > 2000) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "less_complusory_saving",
                    R.string.less_complusory_saving
                ),
                this,
                et_saving
            )
            value = 0
            return value
        } else if (validate!!.returnDoubleValue(et_ComsavingRoi.text.toString()) > 0 && validate!!.returnDoubleValue(
                et_ComsavingRoi.text.toString()
            ) > 24
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "CompulsorySavingroi",
                    R.string.CompulsorySavingroi
                ),
                this,
                et_ComsavingRoi
            )
            value = 0
            return value
        } else if (validate!!.returnDoubleValue(et_voluntarysavingROI.text.toString()) > 0 && validate!!.returnDoubleValue(
                et_voluntarysavingROI.text.toString()
            ) > 36
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "VoluntarySavingroi",
                    R.string.VoluntarySavingroi
                ),
                this,
                et_voluntarysavingROI
            )
            value = 0
            return value
        } else if (et_bookkeeper_s_mobile_no.text.toString().length > 0 && validate!!.checkmobileno(
                et_bookkeeper_s_mobile_no.text.toString()
            ) == 0 && lay_bookkeepermobile.visibility == View.VISIBLE
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "valid_phone",
                    R.string.valid_phone
                ),
                this,
                et_bookkeeper_s_mobile_no
            )
            value = 0
            return value
        } else if (lay_status.visibility == View.VISIBLE && spin_status.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_status,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "status",
                    R.string.status
                )
            )
            value = 0
            return value
        } else if (validate!!.returnIntegerValue(et_electiontenure.text.toString()) > 0 &&
            (validate!!.returnIntegerValue(et_electiontenure.text.toString()) < 12 || validate!!.returnIntegerValue(
                et_electiontenure.text.toString()
            ) > 36)
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "valid_election_tenure",
                    R.string.valid_election_tenure
                ),
                this,
                et_electiontenure
            )
            value = 0
            return value
        } else if (spin_frequency.selectedItemPosition > 0 && spin_fortnight.selectedItemPosition == 0 && lay_fortnight.visibility == View.VISIBLE
        ) {
            validate!!.CustomAlertSpinner(
                this,
                spin_fortnight,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "fortnight_day",
                    R.string.fortnight_day
                )
            )
            value = 0
            return value
        } else if (spin_frequency.selectedItemPosition > 0 && spin_fortnightdate.selectedItemPosition == 0 && lay_fortnightdate.visibility == View.VISIBLE
        ) {
            validate!!.CustomAlertSpinner(
                this,
                spin_fortnightdate,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText("date", R.string.date)
            )
            value = 0
            return value
        } else if (spin_frequency.selectedItemPosition > 0 && spin_monthly.selectedItemPosition == 0 && lay_monthly.visibility == View.VISIBLE
        ) {
            validate!!.CustomAlertSpinner(
                this,
                spin_monthly,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "monthly_day",
                    R.string.monthly_day
                )
            )
            value = 0
            return value
        } else if (spin_frequency.selectedItemPosition > 0 && spin_weekday.selectedItemPosition == 0 && lay_weekday.visibility == View.VISIBLE
        ) {
            validate!!.CustomAlertSpinner(
                this,
                spin_weekday,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText("day", R.string.day)
            )
            value = 0
            return value
        } else if (spin_frequency.selectedItemPosition > 0 && spin_monthlydate.selectedItemPosition == 0 && lay_monthlydate.visibility == View.VISIBLE) {
            validate!!.CustomAlertSpinner(
                this,
                spin_monthlydate,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText("date", R.string.date)
            )
            value = 0
            return value
        } else if (validate!!.returnlookupcode(
                spin_status,
                dataspin_status
            ) == 2 && spin_inactivereason.selectedItemPosition == 0
        ) {
            validate!!.CustomAlertSpinner(
                this,
                spin_inactivereason,
                LabelSet.getText(
                    "validleavingreason",
                    R.string.validleavingreason
                )
            )
            value = 0
            return value
        }



        return value
    }


    override fun onBackPressed() {
//        CustomAlertPartialsave(getString(R.string.doyouwanttosavepartialdata),ShgListActivity::class.java)
        if (CheckValidation() == 1) {
            PartialSaveData()
            var intent = Intent(this, ShgListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }
    }

    fun returnMemberName(): String {

        var pos = spin_member.selectedItemPosition
        var id = ""

        if (!dataspin_belong.isNullOrEmpty()) {
            if (pos > 0) id = dataspin_belong!!.get(pos - 1).member_name!!
        }
        return id
    }

    fun setMemberName(memberName: String, data: List<MemberEntity>?): Int {


        var pos = 0

        if (!data.isNullOrEmpty()) {
            for (i in data.indices) {
                if (memberName.equals(data.get(i).member_name))
                    pos = i + 1
            }
        }
        return pos
    }


    fun CustomAlertPartialsave(str: String, java: Class<*>) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str
        mDialogView.txt_dialog_title.text = LabelSet.getText("app_name", R.string.app_name)
        mDialogView.btn_yes.text = LabelSet.getText("yes", R.string.yes)
        mDialogView.btn_no.text = LabelSet.getText("no", R.string.no)
        mDialogView.btn_yes.setOnClickListener {
            //            PartialSaveData()
            mAlertDialog.dismiss()
            val intent = Intent(this, java)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)


        }
        mDialogView.btn_no.setOnClickListener {
            mAlertDialog.dismiss()
            if (CheckValidation() == 1) {
                PartialSaveData()
                val intent = Intent(this, java)
                startActivity(intent)
                finish()
            }

        }
    }

    fun setLabelText() {
        tv_groupCode.text = LabelSet.getText(
            "group_code",
            R.string.group_code
        )
        tv_shgName.text = LabelSet.getText(
            "shg_name",
            R.string.shg_name
        )
        et_groupname.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_parenFName.text = LabelSet.getText(
            "parent_federation",
            R.string.parent_federation
        )
        tv_panchayat.text = LabelSet.getText(
            "grampanchayat",
            R.string.grampanchayat
        )
        tv_village.text = LabelSet.getText(
            "village1",
            R.string.village1
        )
        tv_formationdate.text = LabelSet.getText(
            "formation_date",
            R.string.formation_date
        )
        tv_promotedBy.text = LabelSet.getText(
            "promoted_by",
            R.string.promoted_by
        )
        tv_fundingAgency.text = LabelSet.getText(
            "promoters_name",
            R.string.promoters_name
        )
        tv_promotedName.text = LabelSet.getText(
            "promoters_name",
            R.string.promoters_name_other
        )
        et_promoter_name.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_copt_date.text = LabelSet.getText(
            "co_optdate",
            R.string.co_optdate
        )
        tv_revivaldate.text = LabelSet.getText(
            "revival_date",
            R.string.revival_date
        )
        tv_tags.text = LabelSet.getText("tags", R.string.tags)
        tv_tags_other.text = LabelSet.getText(
            "other_specify",
            R.string.other_specify
        )
        tv_meetingFrequency.text = LabelSet.getText(
            "meeting_frequency",
            R.string.meeting_frequency
        )
        tv_FortNight.text = LabelSet.getText(
            "fortnight_day",
            R.string.fortnight_day
        )
        tv_monthlyday.text = LabelSet.getText(
            "monthly_day",
            R.string.monthly_day
        )
        tv_days.text = LabelSet.getText("day", R.string.day)
        tvDate.text = LabelSet.getText("date", R.string.date)
        tvDate1.text = LabelSet.getText("date", R.string.date)
        tvDate2.text = LabelSet.getText("date", R.string.date)
        tvDate3.text = LabelSet.getText("date", R.string.date)
        tv_csf.text = LabelSet.getText(
            "saving_frequency",
            R.string.saving_frequency
        )
        tv_csa.text = LabelSet.getText(
            "cumpulsory_saving",
            R.string.cumpulsory_saving
        )
        et_saving.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_csroi.text = LabelSet.getText(
            "compulsory_roi",
            R.string.compulsory_roi
        )
        tv_voluntarySaving.text = LabelSet.getText(
            "voluntary_saving",
            R.string.voluntary_saving
        )
        et_ComsavingRoi.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_voluntarysavingROI.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_bookkeeper_name.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_bookkeeper_s_mobile_no.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_electiontenure.hint = LabelSet.getText(
            "valid_tenure_12_36",
            R.string.valid_tenure_12_36
        )
//        rbVolYes.setText(LabelSet.getText("yes",R.string.yes))
//        rbVolNo.setText(LabelSet.getText("no",R.string.no))
        tv_vsr.text = LabelSet.getText(
            "voluntary_saving_roi",
            R.string.voluntary_saving_roi
        )
        tv_bookkeeper.text = LabelSet.getText(
            "bookkeeper_identified",
            R.string.bookkeeper_identified
        )
        // tvbookkeeper_name.setText(LabelSet.getText("bookkeeper_name",R.string.bookkeeper_name))
        tv_bookkeeperName.text = LabelSet.getText(
            "bookkeeper_name",
            R.string.bookkeeper_name
        )
        tv_bookkeepermob.text = LabelSet.getText(
            "bookkeeper_s_mobile_no",
            R.string.bookkeeper_s_mobile_no
        )
        tv_Primary.text = LabelSet.getText(
            "primary_activity",
            R.string.primary_activity
        )
        tv_secondary.text = LabelSet.getText(
            "secondary_activity",
            R.string.secondary_activity
        )
        tv_tertiary.text = LabelSet.getText(
            "tertiaryactivity",
            R.string.tertiaryactivity
        )
        tv_electionTenure.text = LabelSet.getText(
            "election_tenure",
            R.string.election_tenure
        )
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_savegray.text = LabelSet.getText(
            "save",
            R.string.save
        )
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
        tvPaasbookPage.text = LabelSet.getText(
            "shg_resolution_copy",
            R.string.shg_resolution_copy
        )
        tv_status.text = LabelSet.getText(
            "status",
            R.string.status
        )
    }

    private fun checkPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        val result1 =
            ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val result2 =
            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission(requestCode: Int) {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ),
            requestCode
        )
    }

    fun captureimage(code: Int) {
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
                m.invoke(null)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//        fileUri = getOutputMediaFileUri(MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE, code)
        fileUri = FileProvider.getUriForFile(
            this, BuildConfig.APPLICATION_ID + ".provider", getOutputMediaFile(
                MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE, code
            )!!
        )
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
        startActivityForResult(intent, code)

    }

    fun getOutputMediaFileUri(type: Int, flag: Int): Uri {
        return Uri.fromFile(getOutputMediaFile(type, flag))
    }

    private fun getOutputMediaFile(type: Int, flag: Int): File? {

        // External sdcard location
        val mediaStorageDir =  File(
            getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
            AppSP.IMAGE_DIRECTORY_NAME
        )
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null
            }
        }

        // Create a media file name
        val timeStamp = SimpleDateFormat(
            "yyyyMMdd_HHmmss",
            Locale.ENGLISH
        ).format(Date())
        imageName = "IMG_$timeStamp,2000.jpg"
        if (type == MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE) {
            mediaFile = File(
                mediaStorageDir.path + File.separator
                        + imageName
            )


        } else {
            return null
        }

        return mediaFile
    }

    private fun isPermissionGranted(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            return Environment.isExternalStorageLegacy()
        } else {
            var readExternalStorage =
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
            return readExternalStorage == PackageManager.PERMISSION_GRANTED
        }
    }

    private fun takePermissions() {

            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                101
            )

    }

    private fun pickPdfFromGallery() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "application/pdf"
        intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        getResult.launch(intent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (resultCode == RESULT_OK) {
                if (requestCode == 100) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                        if (Environment.isExternalStorageManager()) {
                            pickPdfFromGallery()
                        } else {
                            takePermissions()
                        }
                    }
                }

                else if (requestCode == 11) {
                    if (resultCode == RESULT_OK && null != data) {
                        val result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                        var textdata = result!![0]
                        if (textdata.isNullOrEmpty()) {
                        } else {
                            if (CURRENT_SELECTED_EDITTEXT == 1) {
                                if (et_groupname.hasFocus()) {
                                    var cursorPosition: Int = et_groupname.selectionStart
                                    et_groupname.text.insert(cursorPosition, textdata)
                                } else {
                                    var getData = et_groupname.text.toString()
                                    if (getData.length > 0) {
                                        getData =  getData +" " +  textdata
                                    } else {
                                        getData = getData + textdata
                                    }
                                    et_groupname.setText(getData)
                                }
                            }
                            if (CURRENT_SELECTED_EDITTEXT == 2) {
                                if (et_promoter_name.hasFocus()) {
                                    var cursorPosition: Int = et_promoter_name.selectionStart
                                    et_promoter_name.text.insert(cursorPosition, textdata)
                                } else {
                                    var getData = et_promoter_name.text.toString()
                                    if (getData.length > 0) {
                                        getData =  getData +" " +  textdata
                                    } else {
                                        getData = getData + textdata
                                    }
                                    et_promoter_name.setText(getData)
                                }
                            }
                            if (CURRENT_SELECTED_EDITTEXT == 3) {
                                if (et_bookkeeper_name.hasFocus()) {
                                    var cursorPosition: Int = et_bookkeeper_name.selectionStart
                                    et_bookkeeper_name.text.insert(cursorPosition, textdata)
                                } else {
                                    var getData = et_bookkeeper_name.text.toString()
                                    if (getData.length > 0) {
                                        getData =  getData +" " +  textdata
                                    } else {
                                        getData = getData + textdata
                                    }
                                    et_bookkeeper_name.setText(getData)
                                }
                            }


                        }
                    }
                }

                else if (requestCode == 102) {
                    if (data != null) {
                        fileUri = data.data
                        //toast("$fileUri")
                        if (fileUri != null) {
                            //  var realPath = Backup(this).getpath(this, fileUri!!)
                            var mediaStorageDir =  File(
                                getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
                                AppSP.IMAGE_DIRECTORY_NAME
                            )

                            // Create the storage directory if it does not exist
                            if (!mediaStorageDir.exists()) {
                                if (!mediaStorageDir.mkdirs()) {
                                    // Create a media file name
                                } else {
                                    //   Toast.makeText(this,"error",Toast.LENGTH_LONG).show()
                                }
                            }
                            if (mediaStorageDir.canWrite()) {
                                val timeStamp = SimpleDateFormat(
                                    "yyyyMMdd_HHmmss",
                                    Locale.ENGLISH
                                ).format(Date())
                                imageName = "SHG_$timeStamp,2000.pdf"
                                //   var realPath = FileUtils.getPath(this, fileUri!!)
                                var realPath = ImageFilePath1.getPath(
                                    this,
                                    fileUri!!
                                )
                                //  Toast.makeText(this,realPath,Toast.LENGTH_LONG).show()

                                var destination = File(mediaStorageDir, imageName)
                            }
                        }
                    }
                } else {
                    compreesedimage()
                    newimageName = true
                    bitmap = BitmapFactory.decodeFile(fileUri!!.path)
                    if (requestCode == 101 && resultCode == RESULT_OK) {
                        //  ImgFrntpage.setImageBitmap(bitmap)
                        Imgshow.setImageBitmap(bitmap)
                        //tvUploadFiles.setText(imageName)
                    }
                }
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
//                Toast.makeText(applicationContext, R.string.cancel, Toast.LENGTH_SHORT)
//                    .show()

            }
        } catch (e: Exception) {
            e.printStackTrace()
//            Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (grantResults.size > 0) {
            if (requestCode == 101) {
                var readExternalStorage: Boolean =
                    grantResults[0] == PackageManager.PERMISSION_GRANTED
                if (readExternalStorage) {
                    pickPdfFromGallery()
                } else {
                    takePermissions()
                }
            }
        }

    }

    fun compreesedimage() {
        val options = BitmapFactory.Options()
        var bitmap = BitmapFactory.decodeFile(fileUri!!.path, options)

        if (bitmap != null) {
            var actualWidth = options.outWidth
            var actualHeight = options.outHeight
            var mutableBitmap =
                Bitmap.createScaledBitmap(bitmap, actualWidth / 2, actualHeight / 2, false)

            var fOut: OutputStream? = null
            var file: File = File(fileUri!!.path)
            try {
                fOut = FileOutputStream(file)
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }
            mutableBitmap.compress(Bitmap.CompressFormat.JPEG, 50, fOut)
            try {
                fOut!!.flush()
                fOut.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
    }

    private fun ShowImage(name: String?) {
        val inflater = this.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val layout: View = inflater.inflate(R.layout.popupwindow, null, false)
        val window = PopupWindow(
            layout,
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT,
            true
        )
        window.showAtLocation(layout, Gravity.CENTER, 0, 0)
        val cancel: ImageView = layout.findViewById(R.id.cancel)
        val image_preview: ImageView = layout.findViewById(R.id.image_preview)
        val mediaStorageDirectory =  File(
            getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
            AppSP.IMAGE_DIRECTORY_NAME)
        var mediaFile1 = File(mediaStorageDirectory.path + File.separator + name)
        cancel.setOnClickListener {
            window.dismiss()
        }

        if (mediaFile1.exists()) {
            Picasso.with(this).load(mediaFile1).into(image_preview)
        } else {
            window.dismiss()
        }

    }

    fun CustomSelectAlert() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customselectdialoge, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_title.text =
            LabelSet.getText(
                "shg_resolution_copy",
                R.string.shg_resolution_copy
            )
        mDialogView.tvMasterData.text =
            LabelSet.getText("captureimage", R.string.captureimage)
        mDialogView.tvUploadData.text =
            LabelSet.getText("upolad_pdf", R.string.upolad_pdf)


        mDialogView.layout_gallery.setOnClickListener {

            mAlertDialog.dismiss()
            if (isPermissionGranted()) {
                pickPdfFromGallery()
            } else {
                takePermissions()
            }


        }
        mDialogView.layout_camera.setOnClickListener {
            if (!checkPermission()) {
                requestPermission(101)
            } else {
                captureimage(101)
            }
            mAlertDialog.dismiss()


        }
    }

    fun fillmemberspinner(spin: Spinner, data: List<MemberEntity>?) {

        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in data.indices) {
                sValue[i + 1] =
                    data[i].member_name + "(" + getlookupValue(data[i].designation, 1) + ")"
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText(
                "nomemberavailable",
                R.string.nomemberavailable
            )
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }

    }

    fun getlookupValue(keyCode: Int?, flag: Int): String? {
        var name: String? = null
        name = lookupViewmodel!!.getlookupValue(
            flag,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),
            keyCode
        )
        return name
    }

    private fun CheckData(): Int {
        var value = 1
        var promotor_code:Int?=0
        var book_keeper_name = ""
        var meetingfrequencyvalue = 0
        var meetingon = 0
        if (lay_fortnight.visibility == View.VISIBLE) {
            meetingfrequencyvalue =
                validate!!.returnlookupcode(spin_fortnight, dataspin_fortnight)
        } else if (lay_monthly.visibility == View.VISIBLE) {
            meetingfrequencyvalue =
                validate!!.returnlookupcode(spin_monthly, dataspin_monthly)
        }
        if (lay_weekday.visibility == View.VISIBLE) {
            meetingon = validate!!.returnlookupcode(spin_weekday, dataspin_weekday)
        } else if (lay_fortnightdate.visibility == View.VISIBLE) {
            meetingon =
                validate!!.returnlookupcode(spin_fortnightdate, dataspin_fortnightdate)
        } else if (lay_monthlydate.visibility == View.VISIBLE) {
            meetingon = validate!!.returnlookupcode(spin_monthlydate, dataspin_monthlydate)
        }
        var priactivity =
            validate!!.returnlookupcode(spin_primaryActivity, dataspin_activity)
        var secactivity =
            validate!!.returnlookupcode(spin_secondaryActivity, dataspin_activity)
        var teractivity =
            validate!!.returnlookupcode(spin_tertiaryActivity, dataspin_activity)

        if (rgBookeeper.checkedRadioButtonId == 1) {
            book_keeper_name = returnMemberName()
        } else {
            book_keeper_name = et_bookkeeper_name.text.toString()
        }
        var pramotedby = validate!!.returnlookupcode(spin_promotedby, dataspin_pramotedby)
        var meetingfrequency = validate!!.returnlookupcode(spin_frequency, dataspin_meetingfreq)
        if (lay_fundingAgency.visibility == View.VISIBLE) {
            promotor_code = validate!!.returnFundingcode(spin_FundingAgency, dataspin_funding)
        } else {
            promotor_code = null
        }
        if(!validate!!.RetriveSharepreferenceString(AppSP.SHGGUID).isNullOrEmpty()){
        val list =
            shgViewModel!!.getSHGdetail(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))

        if (validate!!.returnStringValue(et_promoter_name.text.toString()) != validate!!.returnStringValue(
                list?.get(0)?.promoter_name
            )
        ) {

            value = 0


        } else if (et_groupname.text.toString() != validate!!.returnStringValue(list?.get(0)?.shg_name)) {

            value = 0

        } else if (et_shgcode.text.toString() != validate!!.returnStringValue(list?.get(0)?.shg_code)) {

            value = 0

        } else if (validate!!.returnPanchayatID(
                spin_panchayat,
                dataPanchayat
            ) != list?.get(0)?.panchayat_id
        ) {

            value = 0

        } else if (validate!!.returnVillageID(
                spin_village,
                dataVillage
            ) != list.get(0).village_id
        ) {

            value = 0

        } else if (validate!!.Daybetweentime(et_formationDate.text.toString()) != validate!!.returnLongValue(
                list.get(0).shg_formation_date.toString()
            )
        ) {

            value = 0

        } else if (validate!!.Daybetweentime(et_revivalDate.text.toString()) != validate!!.returnLongValue(
                list.get(0).shg_revival_date.toString()
            )
        ) {

            value = 0

        } else if (pramotedby != validate!!.returnIntegerValue(list.get(0).shg_promoted_by.toString())) {

            value = 0

        } else if (validate!!.returnIntegerValue(promotor_code.toString()) != validate!!.returnIntegerValue(
                list.get(
                    0
                ).promoter_code.toString()
            )
        ) {

            value = 0

        } else if (et_promoter_name.text.toString() != validate!!.returnStringValue(list.get(0).promoter_name)) {

            value = 0

        } else if (rgShgType.checkedRadioButtonId != validate!!.returnIntegerValue(list.get(0).shg_type_code.toString())) {

            value = 0


        } else if (meetingfrequency != validate!!.returnIntegerValue(list.get(0).meeting_frequency.toString())) {

            value = 0

        } else if (validate!!.returnlookupcode(spin_savingfrequency, dataspin_meetingfreq) != validate!!.returnIntegerValue(
                list.get(
                    0
                ).saving_frequency.toString()
            )
        ) {

            value = 0

        } else if (et_saving.text.toString() != list.get(0).month_comp_saving.toString()) {

            value = 0

        } else if (et_ComsavingRoi.text.toString() != validate!!.returnStringValue(list.get(0).savings_interest.toString())) {

            value = 0

        } else if (rgvolunteer.checkedRadioButtonId != validate!!.returnIntegerValue(list.get(0).is_voluntary_saving.toString())) {

            value = 0

        } else if (et_voluntarysavingROI.text.toString() != validate!!.returnStringValue(list.get(0).voluntary_savings_interest.toString())) {

            value = 0

        } else if (rgBookeeper.checkedRadioButtonId != validate!!.returnIntegerValue(list.get(0).bookkeeper_identified.toString())) {

            value = 0


        } else if (book_keeper_name != validate!!.returnStringValue(list.get(0).bookkeeper_name)) {

            value = 0

        } else if (et_bookkeeper_s_mobile_no.text.toString() != validate!!.returnStringValue(
                list.get(
                    0
                ).bookkeeper_mobile.toString()
            )
        ) {

            value = 0

        } else if (priactivity != validate!!.returnIntegerValue(
                list.get(
                    0
                ).primary_activity.toString()
            )
        ) {

            value = 0

        } else if (secactivity != validate!!.returnIntegerValue(
                list.get(
                    0
                ).secondary_activity.toString()
            )
        ) {

            value = 0

        } else if (teractivity != validate!!.returnIntegerValue(
                list.get(
                    0
                ).tertiary_activity.toString()
            )
        ) {

            value = 0

        } else if (validate!!.returnIntegerValue(et_electiontenure.text.toString()) != validate!!.returnIntegerValue(
                list.get(0).election_tenure.toString())) {

            value = 0

        } else if (validate!!.returnlookupcode(spin_status, dataspin_name) != validate!!.returnIntegerValue(
                list.get(0).status.toString())) {

            value = 0
        } else if (validate!!.returnlookupcode(spin_inactivereason, dataspin_inactiveReason) != validate!!.returnIntegerValue(
                list.get(0).inactive_reason.toString())) {

            value = 0

        } else if (meetingfrequencyvalue != validate!!.returnIntegerValue(
                list.get(0).meeting_frequency_value.toString())) {

            value = 0

        } else if (meetingon != validate!!.returnIntegerValue(
                list.get(0).meeting_on.toString())) {

            value = 0

        } else if (imageName != validate!!.returnStringValue(list.get(0).shg_resolution)) {

            value = 0

        }
    }else {
        value = 0

    }
        return value
    }

    private val getResult =
        registerForActivityResult(
            ActivityResultContracts.StartActivityForResult()
        ) {
            if (it.resultCode == RESULT_OK) {
                val value = it.data!!

                if (value != null) {
                    fileUri = value.data
                    //toast("$fileUri")
                    if (fileUri != null) {
                        val timeStamp = SimpleDateFormat(
                            "yyyyMMdd_HHmmss",
                            Locale.ENGLISH
                        ).format(Date())
                        imageName = "SHG_$timeStamp,2000.pdf"
                        //   var realPath = FileUtils.getPath(this, fileUri!!)

                        val path2 = File(
                            getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
                            AppSP.IMAGE_DIRECTORY_NAME
                        )
                        if (!path2.exists()) {
                            path2.mkdirs()
                        }
                        var destination = File(path2.path + File.separator + imageName)
                        getFileName(fileUri!!,destination)
                    }
                }
            }
        }

    //
    @Throws(IllegalArgumentException::class)
    private fun getFileName(uri: Uri, destination: File) {

        try {
            val parcelFileDescriptor = this.contentResolver.openFileDescriptor(uri, "r", null)

            val inputStream = FileInputStream(parcelFileDescriptor!!.fileDescriptor)

            // Obtain a cursor with information regarding this uri
            val cursor: Cursor? = contentResolver.query(uri, null, null, null, null)
            if (cursor!!.count <= 0) {
                cursor.close()
                throw IllegalArgumentException("Can't obtain file name, cursor is empty")
            }
            cursor.moveToFirst()
            val fileName: String =
                cursor.getString(cursor.getColumnIndexOrThrow(OpenableColumns.DISPLAY_NAME))
            cursor.close()

//            val file = File(this.cacheDir, fileName)
//            var str=file.absolutePath
            val outputStream = FileOutputStream(destination)
            IOUtils.copy(inputStream, outputStream)
            newimageName = true
            Imgshow.setImageResource(R.drawable.pdf)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun promptSpeechInput(language: String) {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )
        if (isNetworkConnected()){
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, language)
        }
        else{
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.ENGLISH)
        }
        intent.putExtra(
            RecognizerIntent.EXTRA_PROMPT,
            getString(R.string.speech_prompt)
        )
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT)
        } catch (a: ActivityNotFoundException) {
            Toast.makeText(
                applicationContext,
                getString(R.string.speech_not_supported),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun isNetworkConnected(): Boolean {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager //1
        val networkInfo = connectivityManager.activeNetworkInfo //2
        return networkInfo != null && networkInfo.isConnected //3
    }
}
