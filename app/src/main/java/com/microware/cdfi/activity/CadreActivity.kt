package com.microware.cdfi.activity

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.CardreCateogaryEntity
import com.microware.cdfi.entity.MemberEntity
import com.microware.cdfi.entity.cadreRoleEntity
import com.microware.cdfi.entity.cadreShgMemberEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_cadre.*
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.buttons.btn_cancel
import kotlinx.android.synthetic.main.buttons.btn_save
import kotlinx.android.synthetic.main.buttons.btn_savegray
import kotlinx.android.synthetic.main.tablayout.*
import kotlinx.android.synthetic.main.white_toolbar.*
import java.util.*

class CadreActivity : AppCompatActivity() {
    var validate: Validate? = null
    var dataspin_belong: List<MemberEntity>? = null
    var dataspin_cadrecategory: List<CardreCateogaryEntity>? = null
    var dataspin_cadrerole: List<cadreRoleEntity>? = null

    var memberviewmodel: Memberviewmodel? = null
    var memberPhoneViewmodel: MemberPhoneViewmodel? = null
    var memberkycviewmodel: MemberKYCViewmodel? = null
    var memberbankviewmodel: MemberBankViewmodel? = null
    var membersystemviewmodel: MemberSystemtagViewmodel? = null
    var cadreCateogaryViewmodel: CardreCateogaryViewmodel? = null
    var cadreRoleViewmodel: CardreRoleViewmodel? = null
    var cadreMemberViewModel: CadreMemberViewModel? = null
    var shgViewmodel: SHGViewmodel? = null
    var cadreGUID = ""
    var shgcode = ""
    var sCategoryCode = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cadre)

        validate = Validate(this)
        memberviewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)
        memberPhoneViewmodel = ViewModelProviders.of(this).get(MemberPhoneViewmodel::class.java)
        memberkycviewmodel = ViewModelProviders.of(this).get(MemberKYCViewmodel::class.java)
        shgViewmodel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        memberbankviewmodel = ViewModelProviders.of(this).get(MemberBankViewmodel::class.java)
        membersystemviewmodel =
            ViewModelProviders.of(this).get(MemberSystemtagViewmodel::class.java)
        cadreCateogaryViewmodel =
            ViewModelProviders.of(this).get(CardreCateogaryViewmodel::class.java)
        cadreRoleViewmodel = ViewModelProviders.of(this).get(CardreRoleViewmodel::class.java)
        cadreMemberViewModel = ViewModelProviders.of(this).get(CadreMemberViewModel::class.java)
        setLabelText()

        ivHome.visibility = View.GONE
        if (validate!!.RetriveSharepreferenceInt(AppSP.MemberLockRecord) == 1) {
            ivLock.visibility = View.VISIBLE
            btn_savegray.visibility = View.VISIBLE
            btn_save.visibility = View.GONE

        } else {
            ivLock.visibility = View.GONE
            btn_savegray.visibility = View.GONE
            btn_save.visibility = View.VISIBLE
        }
        tv_title.text =
            validate!!.RetriveSharepreferenceString(AppSP.memebrname)!! + "(" + validate!!.RetriveSharepreferenceString(
                AppSP.ShgName
            ) + ")"

        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvCader.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
        tvVector.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_cader.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))

        var memberlist =
            memberviewmodel!!.getmember(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
        var memberphonelist =
            memberPhoneViewmodel!!.getphoneDatalistcount(
                validate!!.RetriveSharepreferenceString(
                    AppSP.MEMBERGUID
                )
            )
        var memberbanklist =
            memberbankviewmodel!!.getBankdetaildatalistcount(
                validate!!.RetriveSharepreferenceString(
                    AppSP.MEMBERGUID
                )
            )
        var memberkyclist =
            memberkycviewmodel!!.getKycdetaildatalistcount(
                validate!!.RetriveSharepreferenceString(
                    AppSP.MEMBERGUID
                )
            )
        var membersystetmtaglist =
            membersystemviewmodel!!.getSystemtagdatalistcount(
                validate!!.RetriveSharepreferenceString(
                    AppSP.MEMBERGUID
                )
            )

        if (!memberlist.isNullOrEmpty()) {
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberbanklist.isNullOrEmpty()) {
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberkyclist.isNullOrEmpty()) {
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!membersystetmtaglist.isNullOrEmpty()) {
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberphonelist.isNullOrEmpty()) {
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }

        ivBack.setOnClickListener {
            val intent = Intent(this, CadreListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        lay_vector.setOnClickListener {
            var intent = Intent(this, MemberDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_phone.setOnClickListener {
            var intent = Intent(this, MemeberPhoneListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_bank.setOnClickListener {
            var intent = Intent(this, MemberBankListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_kyc.setOnClickListener {
            var intent = Intent(this, MemberIdListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_location.setOnClickListener {
            var intent = Intent(this, MemberAddressListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        lay_systemTag.setOnClickListener {
            var intent = Intent(this, MemberGroupTagList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        et_leavingDate.setOnClickListener {
            validate!!.datePicker(et_leavingDate)
        }

        et_joiningDate.setOnClickListener {
            var date = validate!!.Daybetweentime(et_joiningDate.text.toString())
            if (date > 0) {
                validate!!.datePickerwithmindate(date, et_joiningDate)
            } else {
                validate!!.datePicker(et_joiningDate)
            }
        }

        spin_category.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                sCategoryCode =
                    validate!!.returncardreCatcode(spin_category, dataspin_cadrecategory)
                if (position > 0) {
                    dataspin_cadrerole = cadreRoleViewmodel!!.getCardreRole(
                        validate!!.RetriveSharepreferenceString(AppSP.Langaugecode), sCategoryCode)
                    fillcardreRolespinner(this@CadreActivity, spin_role, dataspin_cadrerole)
                    spin_role.setSelection(
                        retuncardreRolespos(
                            validate!!.RetriveSharepreferenceInt(AppSP.CadreRoleID),
                            dataspin_cadrerole
                        )
                    )
                } else {
                    dataspin_cadrerole = cadreRoleViewmodel!!.getAllCardreRole(
                        validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
                    fillcardreRolespinner(this@CadreActivity, spin_role, dataspin_cadrerole)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

        }

        spin_role.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                var roleID = returncardreRolecode(spin_role, dataspin_cadrerole)
                if (roleID > 0) {
                    if (position > 0) {
                        validate!!.SaveSharepreferenceInt(AppSP.CadreRoleID, roleID)
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

        }

        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                SaveData()
            }
        }

        btn_cancel.setOnClickListener {
            val intent = Intent(this, ShgListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        fillSpinner()
        showData()

    }

    private fun showData() {
        val listData =
            cadreMemberViewModel!!.getCadredetail(validate!!.RetriveSharepreferenceString(AppSP.CADRE_GUID))
        if (!listData.isNullOrEmpty()) {
            et_leavingDate.setText(validate!!.convertDatetime(validate!!.returnLongValue(listData[0].date_leaving.toString())))
            et_joiningDate.setText(validate!!.convertDatetime(validate!!.returnLongValue(listData[0].date_joining.toString())))
            spin_category.setSelection(
                validate!!.returncardreCatpos(
                    validate!!.returnIntegerValue(listData[0].cadre_cat_code.toString()),
                    dataspin_cadrecategory
                )
            )

            validate!!.SaveSharepreferenceInt(AppSP.CadreRoleID, listData.get(0).cadre_role_code!!)

            spin_role.setSelection(
                retuncardreRolespos(
                    validate!!.returnIntegerValue(
                        listData[0].cadre_role_code.toString()
                    ), dataspin_cadrerole
                )
            )

        }

    }

    private fun SaveData() {
        var save = 0
        cadreGUID = validate!!.random()
        shgcode = validate!!.RetriveSharepreferenceString(AppSP.Shgcode)!!

        if (validate!!.RetriveSharepreferenceString(AppSP.CADRE_GUID).isNullOrEmpty()) {
            var cadreEntity = cadreShgMemberEntity(
                0,
                cadreGUID,
                validate!!.returnLongValue(shgcode),
                validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID),
                0,
                returncardreRolecode(spin_role, dataspin_cadrerole),
                validate!!.returncardreCatcode(spin_category, dataspin_cadrecategory),
                validate!!.Daybetweentime(et_joiningDate.text.toString()),
                validate!!.Daybetweentime(et_leavingDate.text.toString()),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                "",
                0,
                "",
                0,
                1, 1, 1
            )

            cadreMemberViewModel!!.insert(cadreEntity)
            save = 1
        } else {
            cadreMemberViewModel!!.updateCadreData(
                validate!!.RetriveSharepreferenceString(AppSP.CADRE_GUID)!!,
                returncardreRolecode(spin_role, dataspin_cadrerole),
                validate!!.returncardreCatcode(spin_category, dataspin_cadrecategory),
                validate!!.Daybetweentime(et_joiningDate.text.toString()),
                validate!!.Daybetweentime(et_leavingDate.text.toString()),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                1, 1)
            save = 2
        }
        validate!!.updateMemberEditFlag(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),shgViewmodel,memberviewmodel)

        if (save == 1) {
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this, CadreListActivity::class.java
            )
        } else {
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_updated_successfully",
                    R.string.data_updated_successfully
                ),
                this, CadreListActivity::class.java
            )
        }
    }

    private fun fillSpinner() {
        dataspin_belong =
            memberviewmodel!!.getAllMemberlist(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        fillmemberspinner(spin_memberName, dataspin_belong)

        dataspin_cadrecategory =
            cadreCateogaryViewmodel!!.getCateogary(validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
        validate!!.fillcardreCatspinner(this, spin_category, dataspin_cadrecategory)

    }

    fun fillmemberspinner(spin: Spinner, data: List<MemberEntity>?) {

        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in data.indices) {
                sValue[i + 1] = data[i].member_name
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText(
                "nomemberavailable",
                R.string.nomemberavailable
            )
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }

    }

    fun setMember(memberGuid: String, data: List<MemberEntity>?): Int {

        var pos = 0

        if (!data.isNullOrEmpty()) {
            for (i in data.indices) {
                if (memberGuid.equals(data.get(i).member_guid))
                    pos = i + 1
            }
        }
        return pos
    }

    fun returnmemguid(): String {

        var pos = spin_memberName.selectedItemPosition
        var id = ""

        if (!dataspin_belong.isNullOrEmpty()) {
            if (pos > 0) id = dataspin_belong!!.get(pos - 1).member_guid
        }
        return id
    }

    private fun checkValidation(): Int {
        var value = 1

        if (spin_category.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_category,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "category1",
                    R.string.category1
                )
            )
            value = 0
            return value
        } else if (spin_role.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_role,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "role",
                    R.string.role
                )
            )
            value = 0
            return value
        } else if (spin_memberName.selectedItemPosition == 0 && lay_memberName.visibility == View.VISIBLE) {
            validate!!.CustomAlertSpinner(
                this,
                spin_memberName,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "member_name",
                    R.string.member_name
                )
            )
            value = 0
            return value
        } else if (et_joiningDate.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "joiningDate1",
                    R.string.joiningDate1
                ),
                this,
                et_joiningDate
            )
            value = 0
            return value
        } else if (validate!!.getDate(
                et_joiningDate.text.toString(),
                validate!!.convertDatetime(
                    validate!!.RetriveSharepreferenceLong(AppSP.MEMBERJOININGDATE)
                ).toString()
            ) == 1
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "validjoinDate1",
                    R.string.validjoinDate1
                ),
                this,
                et_joiningDate
            )
            value = 0
            return value
        } else if (validate!!.getDate(
                et_leavingDate.text.toString(),
                et_joiningDate.text.toString()
            ) == 1
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "validleavingDate",
                    R.string.validleavingDate
                ),
                this,
                et_leavingDate
            )
            value = 0
            return value
        }

        return value
    }

    private fun setLabelText() {
        tv_category.text = LabelSet.getText("category1", R.string.category1)
        tv_role.text = LabelSet.getText("role", R.string.role)
        tv_memName.text = LabelSet.getText("member_name", R.string.member_name)
        tvJoiningDate.text = LabelSet.getText("joiningDate1", R.string.joiningDate1)
        tvLeavingDate.text = LabelSet.getText("leaving_date1", R.string.leaving_date1)
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_savegray.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText("cancel", R.string.cancel)
    }

    fun fillcardreRolespinner(activity: Activity, spin: Spinner, data: List<cadreRoleEntity>?) {

        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in data.indices) {
                sValue[i + 1] = data[i].cadre_role
            }
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }

    }

    fun retuncardreRolespos(id: Int?, data: List<cadreRoleEntity>?): Int {

        var pos = 0
        if (!data.isNullOrEmpty()) {
            if (id!! > 0) {
                for (i in data.indices) {
                    if (id == data.get(i).cadre_role_code)
                        pos = i + 1
                }
            }
        }
        return pos
    }

    fun returncardreRolecode(spin: Spinner, data: List<cadreRoleEntity>?): Int {

        var pos = spin.selectedItemPosition
        var id = 0

        if (!data.isNullOrEmpty()) {
            if (pos > 0) id = data.get(pos - 1).cadre_role_code!!
        }
        return id
    }

    override fun onBackPressed() {
        val intent = Intent(this, CadreListActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

}