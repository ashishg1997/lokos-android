package com.microware.cdfi.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.CadreAdapter
import com.microware.cdfi.entity.cadreShgMemberEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_cadrelist.*
import kotlinx.android.synthetic.main.tablayout.*
import kotlinx.android.synthetic.main.white_toolbar.*

class CadreListActivity : AppCompatActivity() {

    var validate: Validate? = null

    var memberviewmodel: Memberviewmodel? = null
    var memberPhoneViewmodel: MemberPhoneViewmodel? = null
    var memberkycviewmodel: MemberKYCViewmodel? = null
    var memberbankviewmodel: MemberBankViewmodel? = null
    var membersystemviewmodel: MemberSystemtagViewmodel? = null
    var shgViewmodel: SHGViewmodel? = null
    var cadreMemberViewModel: CadreMemberViewModel? = null
    var cadreCateogaryViewmodel: CardreCateogaryViewmodel? = null
    var cadreRoleViewmodel: CardreRoleViewmodel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cadrelist)

        validate = Validate(this)
        memberviewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)
        memberPhoneViewmodel = ViewModelProviders.of(this).get(MemberPhoneViewmodel::class.java)
        memberkycviewmodel = ViewModelProviders.of(this).get(MemberKYCViewmodel::class.java)
        shgViewmodel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        memberbankviewmodel = ViewModelProviders.of(this).get(MemberBankViewmodel::class.java)
        membersystemviewmodel =
            ViewModelProviders.of(this).get(MemberSystemtagViewmodel::class.java)
        cadreMemberViewModel = ViewModelProviders.of(this).get(CadreMemberViewModel::class.java)
        cadreCateogaryViewmodel = ViewModelProviders.of(this).get(CardreCateogaryViewmodel::class.java)
        cadreRoleViewmodel = ViewModelProviders.of(this).get(CardreRoleViewmodel::class.java)

        ivHome.visibility = View.GONE
        if (validate!!.RetriveSharepreferenceInt(AppSP.MemberLockRecord) == 1) {
            ivLock.visibility = View.VISIBLE

        } else {
            ivLock.visibility = View.GONE
        }
        tv_title.text =
            validate!!.RetriveSharepreferenceString(AppSP.memebrname)!! + "(" + validate!!.RetriveSharepreferenceString(
                AppSP.ShgName
            ) + ")"

        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvCader.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
        tvVector.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_cader.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))

        var memberlist =
            memberviewmodel!!.getmember(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
        var memberphonelist =
            memberPhoneViewmodel!!.getphoneDatalistcount(
                validate!!.RetriveSharepreferenceString(
                    AppSP.MEMBERGUID
                )
            )
        var memberbanklist =
            memberbankviewmodel!!.getBankdetaildatalistcount(
                validate!!.RetriveSharepreferenceString(
                    AppSP.MEMBERGUID
                )
            )
        var memberkyclist =
            memberkycviewmodel!!.getKycdetaildatalistcount(
                validate!!.RetriveSharepreferenceString(
                    AppSP.MEMBERGUID
                )
            )
        var membersystetmtaglist =
            membersystemviewmodel!!.getSystemtagdatalistcount(
                validate!!.RetriveSharepreferenceString(
                    AppSP.MEMBERGUID
                )
            )

        if (!memberlist.isNullOrEmpty()) {
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberbanklist.isNullOrEmpty()) {
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberkyclist.isNullOrEmpty()) {
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!membersystetmtaglist.isNullOrEmpty()) {
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberphonelist.isNullOrEmpty()) {
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }

        ivBack.setOnClickListener {
            val intent = Intent(this, ShgListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        lay_systemTag.setOnClickListener {
            var intent = Intent(this, MemberGroupTagList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }


        lay_vector.setOnClickListener {
            var intent = Intent(this, MemberDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_phone.setOnClickListener {
            var intent = Intent(this, MemeberPhoneListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_bank.setOnClickListener {
            var intent = Intent(this, MemberBankListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_kyc.setOnClickListener {
            var intent = Intent(this, MemberIdListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_location.setOnClickListener {
            var intent = Intent(this, MemberAddressListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        addCader.setOnClickListener {
            if (!validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID).isNullOrEmpty()) {
                validate!!.SaveSharepreferenceString(AppSP.CADRE_GUID, "")
                var intent = Intent(this, CadreActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "add_memeber_data_first",
                        R.string.add_memeber_data_first
                    ), this, MemberDetailActivity::class.java
                )
            }
        }
        setLabelText()
        fillRecycler()

    }

    private fun fillRecycler() {

        cadreMemberViewModel!!.getCadreListdata(
            validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!
        )?.observe(this,object:Observer<List<cadreShgMemberEntity>>{
            override fun onChanged(list: List<cadreShgMemberEntity>?) {
                if (list != null){
                    rvList.apply {
                        layoutManager = LinearLayoutManager(this@CadreListActivity)
                        adapter = CadreAdapter(this@CadreListActivity,list)
                    }
                    if(list.size>0){
                        lay_nocadre_avialable.visibility = View.GONE
                        tblHeader.visibility = View.VISIBLE
                    }else {
                        tblHeader.visibility = View.GONE
                        lay_nocadre_avialable.visibility = View.VISIBLE
                        tv_noCadre.text = LabelSet.getText(
                            "no_cadre_s_avialable",
                            R.string.no_cadre_s_avialable
                        )
                    }
                }
            }

        })
    }

    fun getCategoryName(categoryCode:Int):String{
       var type: String? = null
       type = cadreCateogaryViewmodel!!.getCateogaryName(validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),categoryCode)
        return type
    }

    fun getRoleName(roleCode:Int):String{
        var type: String? = null
        type = cadreRoleViewmodel!!.getRoleName(validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),
            roleCode)
        return type
    }

    override fun onBackPressed() {
        var intent = Intent(this, MemberListActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    private fun setLabelText() {
        tvCategoryType.text = LabelSet.getText("category_type",R.string.category_type)
        tv_cadreType.text = LabelSet.getText("cadre_type",R.string.cadre_type)
        tvJoiningDate.text = LabelSet.getText("joiningDate1",R.string.joiningDate1)
        tv_leavingDate.text = LabelSet.getText("leaving_date1",R.string.leaving_date1)
    }
}