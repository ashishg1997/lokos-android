package com.microware.cdfi.activity

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import com.google.gson.JsonObject
import com.microware.cdfi.R
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.api.response.MastersResponse
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.RoleEntity
import com.microware.cdfi.entity.StateEntity
import com.microware.cdfi.entity.UserEntity
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.LocationViewModel
import com.microware.cdfi.viewModel.ResponseViewModel
import kotlinx.android.synthetic.main.activity_changelanguage.*
import kotlinx.android.synthetic.main.activity_changelanguage.btn_login
import kotlinx.android.synthetic.main.customealertdialogelogin.*
import kotlinx.android.synthetic.main.customealertdialogelogin.view.*
import kotlinx.android.synthetic.main.language_alert.view.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChangeLanguageActivity : AppCompatActivity() {
    var datastate: List<StateEntity>? = null

    var validate: Validate? = null
    var locationViewModel: LocationViewModel? = null
    var responseViewModel: ResponseViewModel? = null
    var lang = 0
    var statemap = HashMap<Int, String>()
    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: ApiInterface? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_changelanguage)
        validate = Validate(this)
        // getstate()
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        locationViewModel = ViewModelProviders.of(this).get(LocationViewModel::class.java)
        responseViewModel = ViewModelProviders.of(this).get(ResponseViewModel::class.java)
        datastate = locationViewModel!!.getStateByStateCode()

//        validate!!.fillStateSpinner(this,spin_state,datastate)

        tveng.setOnClickListener {
            // lang = 1
            /* tvhindi.background = resources.getDrawable(R.drawable.white_round)
             tveng.background = resources.getDrawable(R.drawable.language_selectedbg)
             tvBangali.background = resources.getDrawable(R.drawable.white_round)
             tvmarathi.background = resources.getDrawable(R.drawable.white_round)
             tvtelgu.background = resources.getDrawable(R.drawable.white_round)
             tvtamil.background = resources.getDrawable(R.drawable.white_round)
             tvGujrat.background = resources.getDrawable(R.drawable.white_round)
             tvkannad.background = resources.getDrawable(R.drawable.white_round)
             tvmalyalam.background = resources.getDrawable(R.drawable.white_round)
 */
        }

        tvassamee.setOnClickListener {
            lang = 2
            tveng.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvassamee.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvBangali.background = resources.getDrawable(R.drawable.white_round)
            tvGujrat.background = resources.getDrawable(R.drawable.white_round)
            tvhindi.background = resources.getDrawable(R.drawable.white_round)
            tvkannad.background = resources.getDrawable(R.drawable.white_round)
            tvmalyalam.background = resources.getDrawable(R.drawable.white_round)
            tvmarathi.background = resources.getDrawable(R.drawable.white_round)
            tvoriya.background = resources.getDrawable(R.drawable.white_round)
            tvpunjabi.background = resources.getDrawable(R.drawable.white_round)
            tvtamil.background = resources.getDrawable(R.drawable.white_round)
            tvtelgu.background = resources.getDrawable(R.drawable.white_round)
            tvurdu.background = resources.getDrawable(R.drawable.white_round)


        }
        tvBangali.setOnClickListener {
            lang = 3
            tveng.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvassamee.background = resources.getDrawable(R.drawable.white_round)
            tvBangali.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvGujrat.background = resources.getDrawable(R.drawable.white_round)
            tvhindi.background = resources.getDrawable(R.drawable.white_round)
            tvkannad.background = resources.getDrawable(R.drawable.white_round)
            tvmalyalam.background = resources.getDrawable(R.drawable.white_round)
            tvmarathi.background = resources.getDrawable(R.drawable.white_round)
            tvoriya.background = resources.getDrawable(R.drawable.white_round)
            tvpunjabi.background = resources.getDrawable(R.drawable.white_round)
            tvtamil.background = resources.getDrawable(R.drawable.white_round)
            tvtelgu.background = resources.getDrawable(R.drawable.white_round)
            tvurdu.background = resources.getDrawable(R.drawable.white_round)


        }

        tvGujrat.setOnClickListener {
            lang = 4
            tveng.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvassamee.background = resources.getDrawable(R.drawable.white_round)
            tvBangali.background = resources.getDrawable(R.drawable.white_round)
            tvGujrat.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvhindi.background = resources.getDrawable(R.drawable.white_round)
            tvkannad.background = resources.getDrawable(R.drawable.white_round)
            tvmalyalam.background = resources.getDrawable(R.drawable.white_round)
            tvmarathi.background = resources.getDrawable(R.drawable.white_round)
            tvoriya.background = resources.getDrawable(R.drawable.white_round)
            tvpunjabi.background = resources.getDrawable(R.drawable.white_round)
            tvtamil.background = resources.getDrawable(R.drawable.white_round)
            tvtelgu.background = resources.getDrawable(R.drawable.white_round)
            tvurdu.background = resources.getDrawable(R.drawable.white_round)

        }
        tvhindi.setOnClickListener {
            lang = 5
            tveng.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvassamee.background = resources.getDrawable(R.drawable.white_round)
            tvBangali.background = resources.getDrawable(R.drawable.white_round)
            tvGujrat.background = resources.getDrawable(R.drawable.white_round)
            tvhindi.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvkannad.background = resources.getDrawable(R.drawable.white_round)
            tvmalyalam.background = resources.getDrawable(R.drawable.white_round)
            tvmarathi.background = resources.getDrawable(R.drawable.white_round)
            tvoriya.background = resources.getDrawable(R.drawable.white_round)
            tvpunjabi.background = resources.getDrawable(R.drawable.white_round)
            tvtamil.background = resources.getDrawable(R.drawable.white_round)
            tvtelgu.background = resources.getDrawable(R.drawable.white_round)
            tvurdu.background = resources.getDrawable(R.drawable.white_round)

        }
        tvkannad.setOnClickListener {
            lang = 6
            tveng.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvassamee.background = resources.getDrawable(R.drawable.white_round)
            tvBangali.background = resources.getDrawable(R.drawable.white_round)
            tvGujrat.background = resources.getDrawable(R.drawable.white_round)
            tvhindi.background = resources.getDrawable(R.drawable.white_round)
            tvkannad.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvmalyalam.background = resources.getDrawable(R.drawable.white_round)
            tvmarathi.background = resources.getDrawable(R.drawable.white_round)
            tvoriya.background = resources.getDrawable(R.drawable.white_round)
            tvpunjabi.background = resources.getDrawable(R.drawable.white_round)
            tvtamil.background = resources.getDrawable(R.drawable.white_round)
            tvtelgu.background = resources.getDrawable(R.drawable.white_round)
            tvurdu.background = resources.getDrawable(R.drawable.white_round)

        }
        tvmalyalam.setOnClickListener {
            lang = 7
            tveng.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvassamee.background = resources.getDrawable(R.drawable.white_round)
            tvBangali.background = resources.getDrawable(R.drawable.white_round)
            tvGujrat.background = resources.getDrawable(R.drawable.white_round)
            tvhindi.background = resources.getDrawable(R.drawable.white_round)
            tvkannad.background = resources.getDrawable(R.drawable.white_round)
            tvmalyalam.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvmarathi.background = resources.getDrawable(R.drawable.white_round)
            tvoriya.background = resources.getDrawable(R.drawable.white_round)
            tvpunjabi.background = resources.getDrawable(R.drawable.white_round)
            tvtamil.background = resources.getDrawable(R.drawable.white_round)
            tvtelgu.background = resources.getDrawable(R.drawable.white_round)
            tvurdu.background = resources.getDrawable(R.drawable.white_round)

        }
        tvmarathi.setOnClickListener {
            lang = 8
            tveng.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvassamee.background = resources.getDrawable(R.drawable.white_round)
            tvBangali.background = resources.getDrawable(R.drawable.white_round)
            tvGujrat.background = resources.getDrawable(R.drawable.white_round)
            tvhindi.background = resources.getDrawable(R.drawable.white_round)
            tvkannad.background = resources.getDrawable(R.drawable.white_round)
            tvmalyalam.background = resources.getDrawable(R.drawable.white_round)
            tvmarathi.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvoriya.background = resources.getDrawable(R.drawable.white_round)
            tvpunjabi.background = resources.getDrawable(R.drawable.white_round)
            tvtamil.background = resources.getDrawable(R.drawable.white_round)
            tvtelgu.background = resources.getDrawable(R.drawable.white_round)
            tvurdu.background = resources.getDrawable(R.drawable.white_round)

        }
        tvoriya.setOnClickListener {
            lang = 9
            tveng.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvassamee.background = resources.getDrawable(R.drawable.white_round)
            tvBangali.background = resources.getDrawable(R.drawable.white_round)
            tvGujrat.background = resources.getDrawable(R.drawable.white_round)
            tvhindi.background = resources.getDrawable(R.drawable.white_round)
            tvkannad.background = resources.getDrawable(R.drawable.white_round)
            tvmalyalam.background = resources.getDrawable(R.drawable.white_round)
            tvmarathi.background = resources.getDrawable(R.drawable.white_round)
            tvoriya.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvpunjabi.background = resources.getDrawable(R.drawable.white_round)
            tvtamil.background = resources.getDrawable(R.drawable.white_round)
            tvtelgu.background = resources.getDrawable(R.drawable.white_round)
            tvurdu.background = resources.getDrawable(R.drawable.white_round)

        }
        tvpunjabi.setOnClickListener {
            lang = 10
            tveng.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvassamee.background = resources.getDrawable(R.drawable.white_round)
            tvBangali.background = resources.getDrawable(R.drawable.white_round)
            tvGujrat.background = resources.getDrawable(R.drawable.white_round)
            tvhindi.background = resources.getDrawable(R.drawable.white_round)
            tvkannad.background = resources.getDrawable(R.drawable.white_round)
            tvmalyalam.background = resources.getDrawable(R.drawable.white_round)
            tvmarathi.background = resources.getDrawable(R.drawable.white_round)
            tvoriya.background = resources.getDrawable(R.drawable.white_round)
            tvpunjabi.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvtamil.background = resources.getDrawable(R.drawable.white_round)
            tvtelgu.background = resources.getDrawable(R.drawable.white_round)
            tvurdu.background = resources.getDrawable(R.drawable.white_round)

        }
        tvtamil.setOnClickListener {
            lang = 11
            tveng.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvassamee.background = resources.getDrawable(R.drawable.white_round)
            tvBangali.background = resources.getDrawable(R.drawable.white_round)
            tvGujrat.background = resources.getDrawable(R.drawable.white_round)
            tvhindi.background = resources.getDrawable(R.drawable.white_round)
            tvkannad.background = resources.getDrawable(R.drawable.white_round)
            tvmalyalam.background = resources.getDrawable(R.drawable.white_round)
            tvmarathi.background = resources.getDrawable(R.drawable.white_round)
            tvoriya.background = resources.getDrawable(R.drawable.white_round)
            tvpunjabi.background = resources.getDrawable(R.drawable.white_round)
            tvtamil.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvtelgu.background = resources.getDrawable(R.drawable.white_round)
            tvurdu.background = resources.getDrawable(R.drawable.white_round)

        }
        tvtelgu.setOnClickListener {
            lang = 12
            tveng.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvassamee.background = resources.getDrawable(R.drawable.white_round)
            tvBangali.background = resources.getDrawable(R.drawable.white_round)
            tvGujrat.background = resources.getDrawable(R.drawable.white_round)
            tvhindi.background = resources.getDrawable(R.drawable.white_round)
            tvkannad.background = resources.getDrawable(R.drawable.white_round)
            tvmalyalam.background = resources.getDrawable(R.drawable.white_round)
            tvmarathi.background = resources.getDrawable(R.drawable.white_round)
            tvoriya.background = resources.getDrawable(R.drawable.white_round)
            tvpunjabi.background = resources.getDrawable(R.drawable.white_round)
            tvtamil.background = resources.getDrawable(R.drawable.white_round)
            tvtelgu.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvurdu.background = resources.getDrawable(R.drawable.white_round)

        }
        tvurdu.setOnClickListener {
            lang = 13
            tveng.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvassamee.background = resources.getDrawable(R.drawable.white_round)
            tvBangali.background = resources.getDrawable(R.drawable.white_round)
            tvGujrat.background = resources.getDrawable(R.drawable.white_round)
            tvhindi.background = resources.getDrawable(R.drawable.white_round)
            tvkannad.background = resources.getDrawable(R.drawable.white_round)
            tvmalyalam.background = resources.getDrawable(R.drawable.white_round)
            tvmarathi.background = resources.getDrawable(R.drawable.white_round)
            tvoriya.background = resources.getDrawable(R.drawable.white_round)
            tvpunjabi.background = resources.getDrawable(R.drawable.white_round)
            tvtamil.background = resources.getDrawable(R.drawable.white_round)
            tvtelgu.background = resources.getDrawable(R.drawable.white_round)
            tvurdu.background = resources.getDrawable(R.drawable.language_selectedbg)

        }
        btn_login.setOnClickListener {

if(validate!!.isNetworkConnected(this)){
            if (lang != 0) {
                var language = ""
                var languagecode = ""
                if (lang == 2) {
                    language = LabelSet.getText(
                        "assameelang",
                        R.string.assameelang
                    )
                    languagecode = "as"
                } else if (lang == 3) {
                    language = LabelSet.getText(
                        "banglalang",
                        R.string.banglalang
                    )
                    languagecode = "bn"
                } else if (lang == 4) {
                    language = LabelSet.getText(
                        "gujaratilang",
                        R.string.gujaratilang
                    )
                    languagecode = "gu"
                } else if (lang == 5) {
                    language = LabelSet.getText(
                        "hindilang",
                        R.string.hindilang
                    )
                    languagecode = "hi"
                } else if (lang == 6) {
                    language = LabelSet.getText(
                        "kannadlang",
                        R.string.kannadlang
                    )
                    languagecode = "kn"
                } else if (lang == 7) {
                    language = LabelSet.getText(
                        "malayalamlang",
                        R.string.malayalamlang
                    )
                    languagecode = "ml"
                } else if (lang == 8) {
                    language = LabelSet.getText(
                        "marathilang",
                        R.string.marathilang
                    )
                    languagecode = "mr"
                } else if (lang == 9) {
                    language = LabelSet.getText(
                        "oriyalang",
                        R.string.oriyalang
                    )
                    languagecode = "or"
                } else if (lang == 10) {
                    language = LabelSet.getText(
                        "punjabilang",
                        R.string.punjabilang
                    )
                    languagecode = "pa"
                } else if (lang == 11) {
                    language = LabelSet.getText(
                        "tamillang",
                        R.string.tamillang
                    )
                    languagecode = "ta"
                } else if (lang == 12) {
                    language = LabelSet.getText(
                        "telgulang",
                        R.string.telgulang
                    )
                    languagecode = "te"
                } else if (lang == 13) {
                    language = LabelSet.getText(
                        "urdulang",
                        R.string.urdulang
                    )
                    languagecode = "ur"
                }
                showLangAlertDialog(language, languagecode)
            } else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "pleaseselect",
                        R.string.pleaseselect
                    ),
                    this
                )
            }
} else {
    validate!!.CustomAlert(LabelSet.getText(
        "no_internet_msg",
        R.string.no_internet_msg
    ),this)

}
        }


    }

    fun importlanguage(languagecode: String, language: String?) {
        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText(
                "downlaod_master_data",
                R.string.downlaod_master_data
            )
        )

        val token =  validate!!.returnStringValue(
            AESEncryption.decrypt(
                validate!!.RetriveSharepreferenceString(AppSP.token),validate!!.RetriveSharepreferenceString(AppSP.userid)))

        val call = apiInterface?.getAllMasterDatabylanguagecode(
            languagecode,
            token,
            validate!!.RetriveSharepreferenceString(AppSP.userid)!!

        )
        call?.enqueue(object : Callback<MastersResponse> {

            //  call?.enqueue(object : Callback<List<MastersResponse>>, retrofit2.Callback<List<MastersResponse>> {
            override fun onFailure(
                call: Call<MastersResponse>,
                t: Throwable
            ) {
                t.printStackTrace()
                Toast.makeText(
                    this@ChangeLanguageActivity, t.message,
                    Toast.LENGTH_LONG
                ).show()

                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<MastersResponse>,
                response: Response<MastersResponse>
            ) {
                progressDialog.dismiss()
                if (response.isSuccessful) {
                    when (response.code()) {
                        200 -> {

                            if (response.body()?.mst_State!!.isNotEmpty()) {
                                try {
                                    CDFIApplication.database?.stateDao()
                                        ?.deleteAllState()
                                    CDFIApplication.database?.districtDao()
                                        ?.deleteAllDistrict()
                                    CDFIApplication.database?.blockDao()
                                        ?.deleteAllBlocks()
                                    CDFIApplication.database?.panchayatDao()
                                        ?.deleteAllPanchayat()
                                    CDFIApplication.database?.villageDao()
                                        ?.deleteAllVillage()
                                    CDFIApplication.database?.userDao()
                                        ?.delete()
                                    CDFIApplication.database?.lookupdao()
                                        ?.delete()
                                    CDFIApplication.database?.responseCodeDao()
                                        ?.deleteResponse()
                                    CDFIApplication.database?.subcommitteeMasterDao()
                                        ?.deleteScMasterdata()
                                    CDFIApplication.database?.fundingDao()
                                        ?.deleteAllAgency()
                                    CDFIApplication.database?.mstProductDao()
                                        ?.deleteProducts()
                                    CDFIApplication.database?.fundsDao()
                                        ?.deleteFundType()
                                    CDFIApplication.database?.mstCOADao()
                                        ?.deleteAll()
                                    CDFIApplication.database?.cardreCateogaryDao()
                                        ?.deleteAllCateogary()
                                    CDFIApplication.database?.labelMasterDao()
                                        ?.deleteAllLabel()
                                    CDFIApplication.database?.cardreRoleDao()
                                        ?.deleteAllCardreRole()

                                    CDFIApplication.database?.stateDao()
                                        ?.insertState(
                                            response.body()?.mst_State
                                        )
                                    CDFIApplication.database?.districtDao()
                                        ?.insertDistrict(
                                            response.body()?.mst_District
                                        )
                                    CDFIApplication.database?.blockDao()
                                        ?.insertAllBlock(
                                            response.body()?.mst_Block
                                        )
                                    CDFIApplication.database?.panchayatDao()
                                        ?.insertAllPanchayat(
                                            response.body()?.mstPanchayat
                                        )
                                    if (!response.body()?.mstVillage.isNullOrEmpty()) {
                                        CDFIApplication.database?.villageDao()
                                            ?.insertAllVillage(
                                                response.body()?.mstVillage
                                            )
                                    }
                                    CDFIApplication.database?.responseCodeDao()
                                        ?.insertAllResponse(
                                            response.body()?.mstResponse
                                        )

                                    CDFIApplication.database?.laonProductDao()
                                        ?.insertproductlist(
                                            response.body()?.mstLoanProduct
                                        )

                                    CDFIApplication.database?.fundsDao()
                                        ?.insertFundslist(
                                            response.body()?.mstFundType
                                        )
                                    CDFIApplication.database?.mstCOADao()
                                        ?.insertMstCOAAllData(
                                            response.body()?.mstCoa
                                        )

                                    CDFIApplication.database?.lookupdao()
                                        ?.insertlookup(
                                            response.body()?.mstlookup
                                        )
                                    CDFIApplication.database?.subcommitteeMasterDao()
                                        ?.insertScMasterdata(
                                            response.body()?.subCommitteeMasterList
                                        )

                                    if (!response.body()?.mstbankBranch.isNullOrEmpty()) {
                                        CDFIApplication.database?.masterbankbranchDao()
                                            ?.insertbranch(
                                                response.body()?.mstbankBranch
                                            )
                                    }

                                    if (!response.body()?.mstBankMaster.isNullOrEmpty()) {
                                        CDFIApplication.database?.masterBankDao()
                                            ?.insertbank(
                                                response.body()?.mstBankMaster
                                            )
                                    }

                                    if (!response.body()?.mstlabelMasterList.isNullOrEmpty()) {
                                        CDFIApplication.database?.labelMasterDao()
                                            ?.insertAllLabel(
                                                response.body()?.mstlabelMasterList
                                            )
                                    }

                                    if (!response.body()?.cadreCategoryMasterList.isNullOrEmpty()) {
                                        CDFIApplication.database?.cardreCateogaryDao()
                                            ?.insertAllCateogary(
                                                response.body()?.cadreCategoryMasterList
                                            )
                                    }

                                    if (!response.body()?.cadreRoleMasterlist.isNullOrEmpty()) {
                                        CDFIApplication.database?.cardreRoleDao()
                                            ?.insertAllCardreRole(
                                                response.body()?.cadreRoleMasterlist
                                            )
                                    }

                                    if (!response.body()?.mstFundingMaster.isNullOrEmpty()) {
                                        CDFIApplication.database?.fundingDao()
                                            ?.insertAllAgency(
                                                response.body()?.mstFundingMaster
                                            )
                                    }


                                    var list =
                                        response.body()?.mstuser!!
                                    var userlist =
                                        list

                                    if (!userlist.isNullOrEmpty()) {
                                        var userEntity: UserEntity? =
                                            null
                                        userEntity = UserEntity(
                                            userlist.get(0).userId,
                                            userlist.get(0).userName,
                                            userlist.get(0).designation,
                                            userlist.get(0).emailId,
                                            userlist.get(0).mobileNo
                                        )

                                        CDFIApplication.database?.userDao()
                                            ?.insertuser(
                                                userEntity
                                            )
                                    }
//
                                    var roleid = ""
                                    var userrole =
                                        response.body()?.mstuser!!.get(
                                            0
                                        ).userRoleRightsMap
                                    var userrolemaster =
                                        response.body()?.mstuser!!.get(
                                            0
                                        ).userRoleRightsMap!!.roleMaster
                                    if (userrolemaster != null) {
                                        roleid = userrolemaster.roleId
                                        var roleEntity: RoleEntity? =
                                            null
                                        roleEntity = RoleEntity(
                                            userrolemaster.roleId,
                                            userrolemaster.roleName,
                                            userrolemaster.status,
                                            userrolemaster.categoryId,
                                            userrolemaster.levelId,
                                            userrolemaster.typeId
                                        )

                                        CDFIApplication.database?.roledao()
                                            ?.insertrole(
                                                roleEntity
                                            )
                                    }
                                    if (userrole != null) {

                                        validate!!.SaveSharepreferenceInt(
                                            AppSP.statecode,
                                            userrole.stateId
                                        )

                                        validate!!.SaveSharepreferenceInt(
                                            AppSP.districtcode,
                                            userrole.districtId
                                        )

                                        validate!!.SaveSharepreferenceInt(
                                            AppSP.blockcode,
                                            userrole.blockId
                                        )

                                        if(userrole.panchayatId.contains(","))
                                        {

                                            var panchayatid =
                                                userrole.panchayatId.split(
                                                    ","
                                                )
                                            validate!!.SaveSharepreferenceInt(
                                                AppSP.panchayatcode,
                                                validate!!.returnIntegerValue(
                                                    panchayatid[0]
                                                )
                                            )
                                        }else{
                                            validate!!.SaveSharepreferenceInt(
                                                AppSP.panchayatcode,
                                                validate!!.returnIntegerValue(
                                                    userrole.panchayatId
                                                ))
                                        }
                                        validate!!.SaveSharepreferenceString(
                                            AppSP.Roleid,
                                            roleid
                                        )
                                        if (!userrole.villageId.isNullOrEmpty()) {
                                            if (userrole.villageId!!.contains(
                                                    ","
                                                )
                                            ) {

                                                var villageid =
                                                    userrole.villageId!!.split(
                                                        ","
                                                    )
                                                validate!!.SaveSharepreferenceInt(
                                                    AppSP.villagecode,
                                                    validate!!.returnIntegerValue(
                                                        villageid[0]
                                                    )
                                                )
                                            } else {
                                                validate!!.SaveSharepreferenceInt(
                                                    AppSP.villagecode,
                                                    validate!!.returnIntegerValue(
                                                        userrole.villageId
                                                    )
                                                )
                                            }
                                        }

                                        validate!!.SaveSharepreferenceString(
                                            AppSP.SelectedLanguage,
                                            language!!
                                        )
                                        validate!!.SaveSharepreferenceString(
                                            AppSP.SelectedLanguageCode,
                                            languagecode
                                        )
                                        validate!!.SaveSharepreferenceString(AppSP.Langaugecode, "en")
                                        var intent = Intent(
                                            this@ChangeLanguageActivity,
                                            MainActivityDrawer::class.java
                                        )
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                                        startActivity(intent)
                                        finish()

                                    }

                                } catch (ex: Exception) {
                                    ex.printStackTrace()
                                }
                            }

                        }

                    }

                } else {
                    progressDialog.dismiss()
                    when (response.code()) {
                        403 ->
                            CustomAlertlogin()
                        else -> {
                            var jsonObject1 =
                                JSONObject(response.errorBody()!!.source().readUtf8().toString())

                            var resCode = validate!!.returnIntegerValue(
                                jsonObject1.get("responseCode").toString()
                            )

                            var msg = "" + validate!!.alertMsg(
                                this@ChangeLanguageActivity,
                                responseViewModel,
                                resCode,
                                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                                response.message()
                            )

                            Toast.makeText(
                                this@ChangeLanguageActivity,
                                msg,
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }
                } // if
            }

        })
    }

    fun importtoken(user: String, pass: String) {
        var grant_type = "password"
        var userId = user
        var password = pass
        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText(
                "authenticate_user",
                R.string.authenticate_user
            )
        )
        val callCount = apiInterface?.gettokenforce(
            grant_type,
            userId,
            password, validate!!.RetriveSharepreferenceInt(AppSP.State_Selected), 1,
            validate!!.RetriveSharepreferenceString(AppSP.Roleid)!!
        )
        callCount?.enqueue(object : Callback<JsonObject> {
            override fun onFailure(callCount: Call<JsonObject>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                progressDialog.dismiss()


                if (response.isSuccessful) {

                    var jsonObject1: JSONObject? = null
                    try {
                        jsonObject1 = JSONObject(response.body().toString())

                        val auth = jsonObject1.get("accessToken").toString()
                        if (auth.trim().length > 0) {
                            validate!!.SaveSharepreferenceString(
                                AppSP.token,
                                validate!!.returnStringValue(
                                    AESEncryption.encrypt(
                                        auth,validate!!.RetriveSharepreferenceString(AppSP.userid))))
                        }
                        validate!!.CustomAlert(
                            LabelSet.getText(
                                "userauthenticatedsuccessfully",
                                R.string.userauthenticatedsuccessfully
                            ), this@ChangeLanguageActivity
                        )
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Toast.makeText(
                            this@ChangeLanguageActivity,
                            response.message(),
                            Toast.LENGTH_LONG
                        ).show()


                    }

                } else {
                    var jsonObject1 = JSONObject(response.errorBody()!!.source().readUtf8().toString())

                    var resCode = validate!!.returnIntegerValue(jsonObject1.get("responseCode").toString())

                    var msg = "" + validate!!.alertMsg(this@ChangeLanguageActivity,responseViewModel,resCode,validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                        response.message())

                    Toast.makeText(
                        this@ChangeLanguageActivity,
                        msg,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

        })
    }

    fun CustomAlertlogin() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogelogin, null)
        val mBuilder = androidx.appcompat.app.AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()

        val password =  validate!!.returnStringValue(AESEncryption.decrypt(
            validate!!.RetriveSharepreferenceString(AppSP.Password),validate!!.RetriveSharepreferenceString(AppSP.userid)))

        mDialogView.etUsername.setText(validate!!.RetriveSharepreferenceString(AppSP.userid))
        mDialogView.etPassword.setText(password)
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_dialog_title2.text = LabelSet.getText("app_name",R.string.app_name)
        mDialogView.btn_login.text = LabelSet.getText("log_in",R.string.log_in)
        mDialogView.chk_showHide1.text = LabelSet.getText("show_password",R.string.show_password)
        mDialogView.btn_login.setOnClickListener {
            mAlertDialog.dismiss()
            importtoken(
                mDialogView.etUsername.text.toString(),
                validate!!.returnStringValue(
                    AESEncryption.encrypt(validate!!.returnStringValue(
                        mDialogView.etPassword.text.toString()),mDialogView.etUsername.text.toString()))
            )
        }
        mAlertDialog.chk_showHide1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                mAlertDialog.etPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                mAlertDialog.etPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
        }


    }

    private fun showLangAlertDialog(language: String?, languagecode: String) {
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.language_alert, null)
        val mBuilder = AlertDialog.Builder(this).setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = LabelSet.getText(
            "want_toselect",
            R.string.want_toselect
        ) + language + LabelSet.getText(
            "select_lang",
            R.string.select_lang
        )

        mDialogView.btn_yes.setOnClickListener {
            mAlertDialog.dismiss()
            importlanguage(languagecode, language)
        }

        mDialogView.btn_no.setOnClickListener {
            mAlertDialog.dismiss()
        }
    }

    override fun onBackPressed() {
        var intent = Intent(this, MainActivityDrawer::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        finish()

    }

    fun getstate() {

        statemap.put(0, "0")
        statemap.put(1, "1")
        statemap.put(2, "2")
        statemap.put(3, "3")
        statemap.put(4, "4")
        statemap.put(5, "5")
        statemap.put(6, "6")
        statemap.put(7, "31")
        statemap.put(8, "36")
        statemap.put(9, "7")
        statemap.put(10, "8")
        statemap.put(11, "9")
        statemap.put(12, "10")
        statemap.put(13, "11")
        statemap.put(14, "12")
        statemap.put(15, "32")
        statemap.put(16, "13")
        statemap.put(17, "14")
        statemap.put(18, "35")
        statemap.put(19, "17")
        statemap.put(20, "15")
        statemap.put(21, "16")
        statemap.put(22, "18")
        statemap.put(23, "19")
        statemap.put(24, "20")
        statemap.put(25, "21")
        statemap.put(26, "22")
        statemap.put(27, "23")
        statemap.put(28, "24")
        statemap.put(29, "25")
        statemap.put(30, "26")
        statemap.put(31, "27")
        statemap.put(32, "34")
        statemap.put(33, "28")
        statemap.put(34, "29")
        statemap.put(35, "33")
        statemap.put(36, "30")

    }

}