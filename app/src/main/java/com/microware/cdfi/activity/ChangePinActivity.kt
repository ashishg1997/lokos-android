package com.microware.cdfi.activity

import android.content.Intent
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.microware.cdfi.R
import com.microware.cdfi.utility.AESEncryption
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.mukesh.OnOtpCompletionListener
import kotlinx.android.synthetic.main.change_pin.*

class ChangePinActivity : AppCompatActivity() {
    var validate: Validate? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.requestWindowFeature(Window.FEATURE_NO_TITLE)
//Remove notification bar
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.change_pin)
        validate = Validate(this)
        insert_your.text =
            LabelSet.getText("insert_your", R.string.insert_your) + " " + validate!!.RetriveSharepreferenceString(AppSP.userid)

        confirm_otp.setOtpCompletionListener(OnOtpCompletionListener {
            if(old_pin.text.toString().trim().length>0 && old_pin.text.toString().trim().length==4) {
                if (it.equals(enter_otp.text.toString())) {

                    validate!!.SaveSharepreferenceString(
                        AppSP.Pin,
                        validate!!.returnStringValue(
                            AESEncryption.encrypt(
                                it!!,validate!!.RetriveSharepreferenceString(AppSP.userid)))
                    )
                    val startMain = Intent(this, VillageSelection::class.java)
                    startMain.flags =
                        Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(startMain)
                } else {
                    Toast.makeText(this, "Pin does not match", Toast.LENGTH_SHORT).show()

                }
            }else {
                Toast.makeText(this,  LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                )+" "+LabelSet.getText(
                    "valid_old_pin",
                    R.string.valid_old_pin
                ), Toast.LENGTH_SHORT).show()
                confirm_otp.setText("")
                enter_otp.setText("")
            }
        })
        old_pin.setOtpCompletionListener(OnOtpCompletionListener {

            if (validate!!.returnRoleValue(validate!!.RetriveSharepreferenceString(AppSP.Roleid)) == 310) {

                if (it.equals(
                        validate!!.returnStringValue(AESEncryption.decrypt(validate!!.RetriveSharepreferenceString(AppSP.Pin),
                                validate!!.RetriveSharepreferenceString(AppSP.userid))))) {
                    enter_otp.isEnabled = true
                    confirm_otp.isEnabled = true
                    enter_otp.requestFocus()
                }else {
                    validate!!.CustomAlertOtp(LabelSet.getText(
                        "incorrect_old_pin",
                        R.string.incorrect_old_pin
                    ),this,old_pin)
                    enter_otp.isEnabled = false
                    confirm_otp.isEnabled = false
                }
            }else if (validate!!.returnRoleValue(validate!!.RetriveSharepreferenceString(AppSP.Roleid)) == 410) {

                if (it.equals(
                        validate!!.returnStringValue(AESEncryption.decrypt(validate!!.RetriveSharepreferenceString(AppSP.VoPin),
                                validate!!.RetriveSharepreferenceString(AppSP.userid))))) {
                    enter_otp.isEnabled = true
                    confirm_otp.isEnabled = true
                    enter_otp.requestFocus()
                }else {
                    validate!!.CustomAlertOtp(LabelSet.getText(
                        "incorrect_old_pin",
                        R.string.incorrect_old_pin
                    ),this,old_pin)
                    enter_otp.isEnabled = false
                    confirm_otp.isEnabled = false
                }
            }else if (validate!!.returnRoleValue(validate!!.RetriveSharepreferenceString(AppSP.Roleid)) == 450) {

                if (it.equals(
                        validate!!.returnStringValue(AESEncryption.decrypt(validate!!.RetriveSharepreferenceString(AppSP.VoappPin),
                                validate!!.RetriveSharepreferenceString(AppSP.userid))))) {
                    enter_otp.isEnabled = true
                    confirm_otp.isEnabled = true
                    enter_otp.requestFocus()
                }else {
                    validate!!.CustomAlertOtp(LabelSet.getText(
                        "incorrect_old_pin",
                        R.string.incorrect_old_pin
                    ),this,old_pin)
                    enter_otp.isEnabled = false
                    confirm_otp.isEnabled = false
                }
            }else if (validate!!.returnRoleValue(validate!!.RetriveSharepreferenceString(AppSP.Roleid)) == 510) {

                if (it.equals(validate!!.returnStringValue(AESEncryption.decrypt(validate!!.RetriveSharepreferenceString(AppSP.CLfPin),
                                validate!!.RetriveSharepreferenceString(AppSP.userid))))) {
                    enter_otp.isEnabled = true
                    confirm_otp.isEnabled = true
                    enter_otp.requestFocus()
                }else {
                    validate!!.CustomAlertOtp(LabelSet.getText(
                        "incorrect_old_pin",
                        R.string.incorrect_old_pin
                    ),this,old_pin)
                    enter_otp.isEnabled = false
                    confirm_otp.isEnabled = false
                }
            }
        })
        enter_otp.setOtpCompletionListener(OnOtpCompletionListener {
            confirm_otp.requestFocus()
        })

        setLabelText()
    }

    fun setLabelText()
    {
        lokos_core.text = LabelSet.getText(
            "enter_user_pin2",
            R.string.enter_user_pin2
        )
        insert_your.text = LabelSet.getText(
            "insert_your",
            R.string.insert_your
        )
        tventer_old_pin.text = LabelSet.getText(
            "enter_old_pin",
            R.string.enter_old_pin
        )
        tv_enter_pin.text = LabelSet.getText(
            "enter_pin",
            R.string.enter_pin
        )
        tv_confirm_pin.text = LabelSet.getText(
            "confirm_pin",
            R.string.confirm_pin
        )
        btn_done.text = LabelSet.getText("done", R.string.done)
        tv_forgotpin.text = LabelSet.getText(
            "forgot_pin",
            R.string.forgot_pin
        )
    }
}