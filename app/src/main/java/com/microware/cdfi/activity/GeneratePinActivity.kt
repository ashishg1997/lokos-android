package com.microware.cdfi.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import com.microware.cdfi.R
import com.mukesh.OnOtpCompletionListener
import kotlinx.android.synthetic.main.layout_pin_screen.*

class GeneratePinActivity : AppCompatActivity(), OnOtpCompletionListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.requestWindowFeature(Window.FEATURE_NO_TITLE)
//Remove notification bar
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_generate_pin)
        otp_view.setOtpCompletionListener(this)

    }

    override fun onOtpCompleted(otp: String?) {

    }
}