package com.microware.cdfi.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.entity.SystemTagEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_group_system_tag.*
import kotlinx.android.synthetic.main.tablayoutgroup.*
import kotlinx.android.synthetic.main.white_toolbar.ivBack
import kotlinx.android.synthetic.main.white_toolbar.ivHome
import kotlinx.android.synthetic.main.white_toolbar.tv_title
import kotlinx.android.synthetic.main.whiteicon_toolbar.*

class GroupSystemTag : AppCompatActivity() {

    var validate: Validate? = null
    var shgcode = ""
    var shgName = ""
    var lookupViewmodel: LookupViewmodel? = null
    var systemtagViewmodel: SystemtagViewmodel? = null
    var dataspin_system: List<LookupEntity>? = null

    var systemTagEntity: SystemTagEntity? = null
    var shgViewmodel: SHGViewmodel? = null
    var addressViewmodel: CboAddressViewmodel? = null
    var phoneViewmodel: CboPhoneViewmodel? = null
    var memberviewmodel: Memberviewmodel? = null
    var bank: CboBankViewmodel? = null
    var isVerified = 0
    var memberDesignationViewmodel: MemberDesignationViewmodel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group_system_tag)

        validate = Validate(this)
        setLabelText()
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        systemtagViewmodel = ViewModelProviders.of(this).get(SystemtagViewmodel::class.java)
        shgViewmodel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        addressViewmodel = ViewModelProviders.of(this).get(CboAddressViewmodel::class.java)
        phoneViewmodel = ViewModelProviders.of(this).get(CboPhoneViewmodel::class.java)
        bank = ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        memberDesignationViewmodel = ViewModelProviders.of(this).get(MemberDesignationViewmodel::class.java)
        memberviewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)
        shgcode = validate!!.RetriveSharepreferenceString(AppSP.Shgcode)!!
        shgName = validate!!.RetriveSharepreferenceString(AppSP.ShgName)!!


        tvCode.text = shgcode.toString()
        et_shgcode.setText(shgName)

        ivBack.setOnClickListener {
            var intent = Intent(this, GroupSystemTagList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            finish()
        }

        ivHome.setOnClickListener {
            var intent = Intent(this, ShgListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

//        IvSearch.visibility = View.GONE
        if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) == 1) {
            ivLock.visibility = View.VISIBLE
            btn_addgray.visibility = View.VISIBLE
            btn_add.visibility = View.GONE

        } else {
            ivLock.visibility = View.GONE
            btn_addgray.visibility = View.GONE
            btn_add.visibility = View.VISIBLE
        }
        tv_title.text =
            validate!!.RetriveSharepreferenceString(AppSP.ShgName)!! + "(" + validate!!.RetriveSharepreferenceString(AppSP.Shgcode) + ")"
        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
        tvVector.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        var shglist=shgViewmodel!!.getSHG(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var addresslist=addressViewmodel!!.getAddressDatalist(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var phonelist=phoneViewmodel!!.getuploadlistcount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var banklist=bank!!.getBankdatalistcount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var memberdesignationcount = memberDesignationViewmodel!!.getMemberDesignationdetaillistcount(
            validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)
        )
        if(!shglist.isNullOrEmpty()){
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else{
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))


        }
        if(!addresslist.isNullOrEmpty()){
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        }else{
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        if(!phonelist.isNullOrEmpty()){
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        }else{
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        if(!banklist.isNullOrEmpty()){
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        }else{
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        if(!memberdesignationcount.isNullOrEmpty()){
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        }else{
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        lay_vector.setOnClickListener {
            var intent = Intent(this, BasicDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_phone.setOnClickListener {
            var intent = Intent(this, PhoneDetailListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_bank.setOnClickListener {
            var intent = Intent(this, BankDetailListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        lay_post.setOnClickListener {
            var intent = Intent(this, MemberDesignationActvity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        lay_location.setOnClickListener {
            var intent = Intent(this, AddressDetailListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        btn_add.setOnClickListener {
            if (!validate!!.RetriveSharepreferenceString(AppSP.SHGGUID).isNullOrEmpty()) {
                if (checkValidation() == 1) {
                    SaveGrouptag()
                }
            } else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "insert_Basic_data_first",
                        R.string.insert_Basic_data_first
                    ),
                    this,
                    BasicDetailActivity::class.java
                )
            }
        }
        fillSpinner()
        showData()
        //  setLabelText()
    }
    private fun showData() {
        var list =
            systemtagViewmodel!!.getSystemtag(validate!!.RetriveSharepreferenceString(AppSP.SystemtagGUID))
        if(!list.isNullOrEmpty()){
            if(validate!!.returnIntegerValue(list.get(0).system_type.toString())==1){
                spin_system.isEnabled = false
                et_id.isEnabled = false
            }else {
                spin_system.isEnabled = true
                et_id.isEnabled = true
            }
            spin_system.setSelection(validate!!.returnlookupcodepos(
                validate!!.returnIntegerValue(list.get(0).system_type.toString()),dataspin_system))
            et_id.setText(validate!!.returnStringValue(list.get(0).system_id))

        }
    }

    fun checkValidation(): Int {
        var value = 1

        if (spin_system.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,

                spin_system,
                LabelSet.getText(
                    "pleaseselectsystemid",
                    R.string.pleaseselectsystemid
                )
            )
            value = 0
            return value
        } else if (spin_system.selectedItemPosition>0 && et_id.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "pleaseenetrid",
                    R.string.pleaseenetrid
                ),
                this,
                et_id
            )
            value = 0
            return value
        }


        return value

    }

    private fun fillSpinner() {


        dataspin_system = lookupViewmodel!!.getlookup(
            43,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        validate!!.fillspinner(this, spin_system, dataspin_system)
    }
    private fun SaveGrouptag() {

        var tagguid = validate!!.random()
        var systemid = validate!!.returnlookupcode(spin_system, dataspin_system)

        if (validate!!.RetriveSharepreferenceString(AppSP.SystemtagGUID).isNullOrEmpty()) {
            systemTagEntity = SystemTagEntity(
                0,
                validate!!.returnLongValue(shgcode),
                validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!,
                tagguid,
                systemid,
                validate!!.returnStringValue(et_id.text.toString()),
                1,
                1,
                1,
                1,
                0,
                "",
                validate!!.Daybetweentime(validate!!.currentdatetime),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                0,
                "", 1

            )
            systemtagViewmodel!!.insert(systemTagEntity!!)

            validate!!.SaveSharepreferenceString(AppSP.SystemtagGUID, tagguid)
            validate!!.updateLockingFlag(
                validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!,
                shgViewmodel, memberviewmodel
            )

            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this,
                GroupSystemTagList::class.java
            )
        } else {
            if(checkData()==0){
                systemtagViewmodel!!.updateSystemTagdata(
                    systemid,
                    validate!!.returnStringValue(et_id.text.toString()),
                    validate!!.RetriveSharepreferenceString(AppSP.SystemtagGUID)!!,
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                )
                validate!!.updateLockingFlag(
                    validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!,
                    shgViewmodel, memberviewmodel
                )
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "updated_successfully",
                        R.string.updated_successfully
                    ), this,
                    GroupSystemTagList::class.java
                )
            }else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "updated_successfully",
                        R.string.updated_successfully
                    ), this,
                    GroupSystemTagList::class.java
                )
            }
        }



    }


    override fun onBackPressed() {
        var intent = Intent(this, GroupSystemTagList::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }
    fun setLabelText()
    {
        tvCode.text = LabelSet.getText(
            "shg_code",
            R.string.shg_code
        )
        tvSYtemID.text = LabelSet.getText(
            "system_tag_id",
            R.string.system_tag_id
        )
        tvID.text = LabelSet.getText("id", R.string.id)
        et_id.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        btn_add.text = LabelSet.getText(
            "add_system_tab",
            R.string.add_system_tab
        )
        btn_addgray.text = LabelSet.getText(
            "add_system_tab",
            R.string.add_system_tab
        )
    }

    private fun checkData():Int{
        var value=1
        var list =
            systemtagViewmodel!!.getSystemtag(validate!!.RetriveSharepreferenceString(AppSP.SystemtagGUID))
        if (validate!!.returnlookupcode(spin_system, dataspin_system) != validate!!.returnIntegerValue(list?.get(0)?.system_type.toString())) {

            value = 0
        } else if (validate!!.returnStringValue(et_id.text.toString()) != validate!!.returnStringValue(list?.get(0)?.system_id)) {
            value = 0
        }


        return value
    }
}