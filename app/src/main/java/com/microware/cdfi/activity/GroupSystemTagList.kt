package com.microware.cdfi.activity

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.GroupSystemtagAdapter
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.entity.SystemTagEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_group_system_tag_list.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.btn_no
import kotlinx.android.synthetic.main.customealertdialogepartial.view.btn_yes
import kotlinx.android.synthetic.main.customealertdialogepartial.view.txt_msg
import kotlinx.android.synthetic.main.layout_verification_dialog.view.*
import kotlinx.android.synthetic.main.tablayoutgroup.*
import kotlinx.android.synthetic.main.white_toolbar.ivBack
import kotlinx.android.synthetic.main.white_toolbar.ivHome
import kotlinx.android.synthetic.main.white_toolbar.tv_title
import kotlinx.android.synthetic.main.whiteicon_toolbar.*

class GroupSystemTagList : AppCompatActivity() {

    var validate: Validate? = null
    var lookupViewmodel: LookupViewmodel? = null
    var systemtagViewmodel: SystemtagViewmodel? = null
    var dataspin_system: List<LookupEntity>? = null
    var dataspin_verify: List<LookupEntity>? = null
    var shgViewmodel: SHGViewmodel? = null
    var addressViewmodel: CboAddressViewmodel? = null
    var phoneViewmodel: CboPhoneViewmodel? = null
    var bank: CboBankViewmodel? = null
    var memberDesignationViewmodel: MemberDesignationViewmodel? = null
    var memberviewmodel: Memberviewmodel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group_system_tag_list)

        validate = Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        systemtagViewmodel = ViewModelProviders.of(this).get(SystemtagViewmodel::class.java)
        shgViewmodel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        addressViewmodel = ViewModelProviders.of(this).get(CboAddressViewmodel::class.java)
        phoneViewmodel = ViewModelProviders.of(this).get(CboPhoneViewmodel::class.java)
        bank = ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        memberDesignationViewmodel = ViewModelProviders.of(this).get(MemberDesignationViewmodel::class.java)
        memberviewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)

        ivBack.setOnClickListener {
            var intent = Intent(this, ShgListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            finish()
        }

        ivHome.setOnClickListener {
            var intent = Intent(this, ShgListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

//        IvSearch.visibility = View.GONE
       /* if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) == 1) {
            ivLock.visibility = View.VISIBLE
            addSystemTaggray.visibility = View.VISIBLE
            addSystemTag.visibility = View.GONE
        } else {
            ivLock.visibility = View.GONE
            addSystemTaggray.visibility = View.GONE
            addSystemTag.visibility = View.VISIBLE
        }
*/
        addSystemTag.visibility = View.GONE
        tv_title.text =
            validate!!.RetriveSharepreferenceString(AppSP.ShgName)!! + "(" + validate!!.RetriveSharepreferenceString(AppSP.Shgcode) + ")"
        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
        tvVector.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        var shglist=shgViewmodel!!.getSHG(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var addresslist=addressViewmodel!!.getAddressDatalistcount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var phonelist=phoneViewmodel!!.getuploadlistcount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var banklist=bank!!.getBankdatalistcount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var memberdesignationcount = memberDesignationViewmodel!!.getMemberDesignationdetaillistcount(
            validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)
        )
        if(!shglist.isNullOrEmpty()){
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else{
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))


        }
        if(!addresslist.isNullOrEmpty()){
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        }else{
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        if(!phonelist.isNullOrEmpty()){
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        }else{
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        if(!banklist.isNullOrEmpty()){
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        }else{
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        if(!memberdesignationcount.isNullOrEmpty()){
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        }else{
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        lay_vector.setOnClickListener {
            var intent = Intent(this, BasicDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_phone.setOnClickListener {
            var intent = Intent(this, PhoneDetailListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_bank.setOnClickListener {
            var intent = Intent(this, BankDetailListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        lay_location.setOnClickListener {
            var intent = Intent(this, AddressDetailListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        lay_post.setOnClickListener {
            var intent = Intent(this, MemberDesignationActvity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        addSystemTag.setOnClickListener {
            if (!validate!!.RetriveSharepreferenceString(AppSP.SHGGUID).isNullOrEmpty()) {
                validate!!.SaveSharepreferenceString(AppSP.SystemtagGUID,"")
                var intent = Intent(this, GroupSystemTag::class.java)
                startActivity(intent)
                finish()
            } else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "insert_Basic_data_first",
                        R.string.insert_Basic_data_first
                    ) ,
                    this,
                    BasicDetailActivity::class.java
                )
            }
        }

        if(shgViewmodel!!.getFinalVerification(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))>0){
            btn_Verify.visibility = View.VISIBLE
        }else {
            btn_Verify.visibility = View.GONE
        }

        btn_Verify.setOnClickListener {
            if(sCheckValidation()==1){
                CustomAlertVerification()
            }
        }
        fillData()
    }

    fun sCheckValidation():Int{
        var value = 1
        var isVerify = shgViewmodel!!.getIsVerified(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        if(isVerify==1){
            validate!!.CustomAlert(LabelSet.getText(
                "complete_shg",
                R.string.complete_shg
            ),this)
            value = 0
            return value
        }else if(phoneViewmodel!!.getVerificationCount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))>0){
            validate!!.CustomAlert(LabelSet.getText(
                "complete_phone",
                R.string.complete_phone
            ),this)
            value = 0
            return value
        }else if(addressViewmodel!!.getVerificationCount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))>0){
            validate!!.CustomAlert(LabelSet.getText(
                "complete_address",
                R.string.complete_address
            ),this)
            value = 0
            return value
        }else if(bank!!.getVerificationCount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))>0){
            validate!!.CustomAlert(LabelSet.getText(
                "complete_bank",
                R.string.complete_bank
            ),this)
            value = 0
            return value
        }

        return value
    }
     fun fillData() {
        systemtagViewmodel!!.getSystemtagdata(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))!!
            .observe(this, object : Observer<List<SystemTagEntity>> {
                override fun onChanged(system_list: List<SystemTagEntity>?) {
                    if (system_list!=null) {
                        rvList.layoutManager = LinearLayoutManager(this@GroupSystemTagList)
                        rvList.adapter = GroupSystemtagAdapter(this@GroupSystemTagList, system_list)
                        if(system_list.size>0){
                            lay_nosystem_avialable.visibility=View.GONE
                        }
                    }
                }
            })
    }

    fun returnlookupvalue(id: Int): String {
        dataspin_system = lookupViewmodel!!.getlookup(
            43,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        return validate!!.returnlookupcodevalue(id, dataspin_system)
    }

    override fun onBackPressed() {
        var intent = Intent(this, ShgListActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun CustomAlert(guid:String,iFlag:Int) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = LabelSet.getText(
            "do_u_want_to_delete",
            R.string.do_u_want_to_delete
        )
        mDialogView.txt_dialog_title.text = LabelSet.getText("app_name", R.string.app_name)
        mDialogView.btn_yes.text = LabelSet.getText("yes", R.string.yes)
        mDialogView.btn_no.text = LabelSet.getText("no", R.string.no)
        mDialogView.btn_yes.setOnClickListener {

            if(iFlag == 0) {
                systemtagViewmodel!!.deleteRecord(guid)
            }else {
                systemtagViewmodel!!.deleteSystemtagData(guid)

            }
            validate!!.updateLockingFlag(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!,
                shgViewmodel,memberviewmodel)
            mAlertDialog.dismiss()

        }

        mDialogView.btn_no.setOnClickListener {

            mAlertDialog.dismiss()

        }
    }

    fun CustomAlertVerification() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.layout_verification_dialog, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.tvVerified.text = LabelSet.getText(
            "verification_status",
            R.string.verification_status
        )
        dataspin_verify = lookupViewmodel!!.getlookup(
            56,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        validate!!.fillspinner(this, mDialogView.spin_verified, dataspin_verify)
        mDialogView.txt_dialog_title1.text = LabelSet.getText("app_name", R.string.app_name)
        mDialogView.btn_yes.text = LabelSet.getText("yes", R.string.yes)
        mDialogView.btn_no.text = LabelSet.getText("no", R.string.no)
        mDialogView.btn_yes.setOnClickListener {

            if(mDialogView.spin_verified.selectedItemPosition>0){
                shgViewmodel!!.updateIsVerified(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!,validate!!.returnlookupcode(mDialogView.spin_verified, dataspin_verify),validate!!.Daybetweentime(validate!!.currentdatetime),
                    validate!!.RetriveSharepreferenceString(AppSP.userid)!!)
                if(shgViewmodel!!.getFinalVerification(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))>0){
                    btn_Verify.visibility = View.VISIBLE
                }else {
                    btn_Verify.visibility = View.GONE
                }
                validate!!.CustomAlert(LabelSet.getText(
                    "verified_successfully",
                    R.string.verified_successfully
                ),this)
                mAlertDialog.dismiss()
            }else {
                var msg = LabelSet.getText(
                    "",
                    R.string.select_verification_status
                )
                validate!!.CustomAlertSpinner(this,mDialogView.spin_verified,msg)
            }



        }

        mDialogView.btn_no.setOnClickListener {

            mAlertDialog.dismiss()

        }
    }
}