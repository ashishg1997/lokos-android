package com.microware.cdfi.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.CboKycAdapter
import com.microware.cdfi.entity.Cbo_kycEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.CboKycViewmodel
import com.microware.cdfi.viewModel.LookupViewmodel
import kotlinx.android.synthetic.main.fragment_kyc_detail_list.*
import kotlinx.android.synthetic.main.tablayout.*
import kotlinx.android.synthetic.main.white_toolbar.*

class KYCDetailListActivity : AppCompatActivity() {

    var validate: Validate? = null
    var kycDetailViewModel: CboKycViewmodel? = null
    var lookupViewmodel: LookupViewmodel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_kyc_detail_list)

        validate = Validate(this)
        kycDetailViewModel = ViewModelProviders.of(this).get(CboKycViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)

        ivBack.setOnClickListener {
            var intent = Intent(this,ShgListActivity::class.java)
            intent.flags=Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

        ivHome.setOnClickListener {
            var intent = Intent(this,ShgListActivity::class.java)
            intent.flags=Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

//        IvSearch.visibility = View.GONE
        tv_title.text = validate!!.RetriveSharepreferenceString(AppSP.ShgName)
        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
        tvVector.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))

        lay_systemTag.setOnClickListener {
            var intent = Intent(this, GroupSystemTagList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_vector.setOnClickListener {
            var intent = Intent(this, BasicDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_phone.setOnClickListener {
            var intent = Intent(this, PhoneDetailListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_location.setOnClickListener {
            var intent = Intent(this, AddressDetailListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_bank.setOnClickListener {
            var intent = Intent(this, BankDetailListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        addKyc.setOnClickListener {
            if (!validate!!.RetriveSharepreferenceString(AppSP.SHGGUID).isNullOrEmpty())  {
                validate!!.SaveSharepreferenceString(AppSP.KYCGUID, "")
                var intent = Intent(this, KycDetailActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
            }else {
                validate!!.CustomAlert(
                    getString(R.string.insert_Basic_data_first),
                    this,
                    BasicDetailActivity::class.java
                )
            }
        }

        fillData()
    }

    fun getDocumentTYpeName(kycType: Int?): String?{
        var value:String? = null

        value = lookupViewmodel!!.getAddressType("MEMBERDOCTYPE",kycType)
        return value
    }

    private fun fillData() {
        kycDetailViewModel!!.getKycdetaildata(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))!!
            .observe(this,object : Observer<List<Cbo_kycEntity>>{
            override fun onChanged(kyc_list: List<Cbo_kycEntity>?) {
                if (!kyc_list.isNullOrEmpty() && kyc_list.size > 0 ){
                    rvList.layoutManager = LinearLayoutManager(this@KYCDetailListActivity)
                    rvList.adapter = CboKycAdapter(this@KYCDetailListActivity, kyc_list)
                }
            }
        })
    }
}
