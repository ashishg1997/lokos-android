package com.microware.cdfi.activity

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.PopupWindow
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.BuildConfig
import com.microware.cdfi.R
import com.microware.cdfi.entity.Cbo_kycEntity
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.CboKycViewmodel
import com.microware.cdfi.viewModel.LookupViewmodel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_member_id.*
import kotlinx.android.synthetic.main.fragment_bank_detail.*
import kotlinx.android.synthetic.main.fragment_kyc.*
import kotlinx.android.synthetic.main.fragment_kyc.IvFrntUpload
import kotlinx.android.synthetic.main.fragment_kyc.IvrearUpload
import kotlinx.android.synthetic.main.fragment_kyc.btn_kyc
import kotlinx.android.synthetic.main.fragment_kyc.et_kycno
import kotlinx.android.synthetic.main.fragment_kyc.et_shgcode
import kotlinx.android.synthetic.main.fragment_kyc.et_validform
import kotlinx.android.synthetic.main.fragment_kyc.et_validtill
import kotlinx.android.synthetic.main.fragment_kyc.lay_frontattach
import kotlinx.android.synthetic.main.fragment_kyc.lay_rearattach
import kotlinx.android.synthetic.main.fragment_kyc.lay_validform
import kotlinx.android.synthetic.main.fragment_kyc.lay_validtill
import kotlinx.android.synthetic.main.fragment_kyc.spin_kyctype
import kotlinx.android.synthetic.main.fragment_kyc.tvCode
import kotlinx.android.synthetic.main.tablayout.*
import kotlinx.android.synthetic.main.tablayout.lay_bank
import kotlinx.android.synthetic.main.white_toolbar.*
import java.io.*
import java.text.SimpleDateFormat
import java.util.*

class KycDetailActivity : AppCompatActivity() {

    var validate: Validate? = null
    var cboKycViewmodel: CboKycViewmodel? = null
    var cboKycentity: Cbo_kycEntity? = null
    var frontName = ""
    var rearName = ""
    var bitmap: Bitmap? = null
    var IMAGE_DIRECTORY_NAME = "CDFI"
    private var fileUri: Uri? = null
    var imgPathUpload = ""
    var imgNameUpload = ""
    var mediaFile: File?= null
    var guid = ""
    var shgcode = ""
    var shgName = ""
    var lookupViewmodel: LookupViewmodel? = null
    var dataspin_doctype: List<LookupEntity>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_kyc)

        validate = Validate(this)
        cboKycViewmodel = ViewModelProviders.of(this).get(CboKycViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        shgcode = validate!!.RetriveSharepreferenceString(AppSP.Shgcode)!!
        shgName = validate!!.RetriveSharepreferenceString(AppSP.ShgName)!!
        tvCode.text = shgcode.toString()
        et_shgcode.setText(shgName)

        ivBack.setOnClickListener {
            var intent = Intent(this,KYCDetailListActivity::class.java)
            intent.flags=Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

        ivHome.setOnClickListener {
            var intent = Intent(this,ShgListActivity::class.java)
            intent.flags=Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

//        IvSearch.visibility = View.GONE
        tv_title.text = validate!!.RetriveSharepreferenceString(AppSP.ShgName)

        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
        tvVector.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))

        lay_systemTag.setOnClickListener {
            var intent = Intent(this, GroupSystemTagList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_vector.setOnClickListener {
            var intent = Intent(this, BasicDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_phone.setOnClickListener {
            var intent = Intent(this, PhoneDetailListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_location.setOnClickListener {
            var intent = Intent(this, AddressDetailListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_bank.setOnClickListener {
            var intent = Intent(this, BankDetailListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        et_validform.setOnClickListener {
            validate!!.datePicker(et_validform)
        }

        et_validtill.setOnClickListener {
            validate!!.datePicker(et_validtill)
        }

        lay_frontattach.setOnClickListener {
            if(!checkPermission()){
                requestPermission(101)
            }else{
                captureimage(101)
            }
        }

        lay_rearattach.setOnClickListener {
            if(!checkPermission()){
                requestPermission(102)
            }else {
                captureimage(102)
            }
        }

        IvFrntUpload.setOnClickListener {
                ShowImage(frontName)
        }

        IvrearUpload.setOnClickListener {
            ShowImage(rearName)
        }

        /*rbValidYes.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked){
                lay_validform.visibility = View.VISIBLE
                lay_validtill.visibility = View.VISIBLE
            }else {
                lay_validform.visibility = View.GONE
                lay_validtill.visibility = View.GONE
            }
        }

        rbValidNO.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked){
                lay_validform.visibility = View.GONE
                lay_validtill.visibility = View.GONE
            }else {
                lay_validform.visibility = View.VISIBLE
                lay_validtill.visibility = View.VISIBLE
            }

        }*/

        btn_kyc.setOnClickListener {
            if (!validate!!.RetriveSharepreferenceString(AppSP.SHGGUID).isNullOrEmpty()){
                if(CheckValidation() == 1){
                    SaveKycDetail()
                }
            }else{
                validate!!.CustomAlert(getString(R.string.insert_Basic_data_first),this,BasicDetailActivity::class.java)
            }
        }

        fillSpinner()
        showData()

    }

    private fun fillSpinner() {
        dataspin_doctype = lookupViewmodel!!.getlookupfromkeycode("MEMBERDOCTYPE",validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
        validate!!.fillspinner(this,spin_kyctype,dataspin_doctype)
    }


    private fun showData() {
        var list = cboKycViewmodel!!.getKycdetail(validate!!.RetriveSharepreferenceString(AppSP.KYCGUID))
        if (!list.isNullOrEmpty() && list.size > 0){
            spin_kyctype.setSelection(validate!!.returnlookupcodepos(list.get(0).kyc_type,dataspin_doctype))
            et_kycno.setText(list.get(0).kyc_number)
            frontName = list.get(0).kyc_front_doc_orig_name!!
            rearName = list.get(0).kyc_rear_doc_name!!
            if (!list.get(0).kyc_front_doc_orig_name.isNullOrEmpty()){
                showimage(list.get(0).kyc_front_doc_orig_name!!,IvFrntUpload)
            }
            if (!list.get(0).kyc_rear_doc_name.isNullOrEmpty()){
                showimage(list.get(0).kyc_rear_doc_name!!,IvrearUpload)
            }
        }
    }

    fun showimage(name: String, image: ImageView) {
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
                m.invoke(null)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
        val mediaStorageDir = File(
            getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
            AppSP.IMAGE_DIRECTORY_NAME
        )

        mediaFile = File(mediaStorageDir.path + File.separator + name)
        imgPathUpload = mediaStorageDir.path + File.separator + name
        imgNameUpload = name

        Picasso.with(this).load(mediaFile).into(image)

    }

    fun captureimage(code: Int) {
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
                m.invoke(null)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//        fileUri = getOutputMediaFileUri(MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE, code)
        fileUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", getOutputMediaFile(MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE, code)!!)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
        startActivityForResult(intent, code)

    }

    fun getOutputMediaFileUri(type: Int, flag: Int): Uri {
        return Uri.fromFile(getOutputMediaFile(type, flag))
    }

    private fun getOutputMediaFile(type: Int, flag: Int): File? {

        // External sdcard location
        val mediaStorageDir = File(
            getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
            AppSP.IMAGE_DIRECTORY_NAME
        )
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null
            }
        }

        // Create a media file name
        val timeStamp = SimpleDateFormat(
            "yyyyMMdd_HHmmss",
            Locale.ENGLISH
        ).format(Date())
        if (type == MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE) {
            mediaFile = File(
                mediaStorageDir.path + File.separator
                        + "IMG_" + timeStamp + ".jpg"
            )
            imgPathUpload = mediaStorageDir.path + File.separator + "IMG_" + timeStamp + ".jpg"
            imgNameUpload = timeStamp + ".jpg"
            if (flag == 101) {
                frontName = "IMG_$timeStamp.jpg"
            }
            if (flag == 102){
                rearName = "IMG_$timeStamp.jpg"
            }

        } else {
            return null
        }

        return mediaFile
    }

    private fun SaveKycDetail() {
        guid = validate!!.random()
        var kyc_type = validate!!.returnlookupcode(spin_kyctype,dataspin_doctype)
        if (validate!!.RetriveSharepreferenceString(AppSP.KYCGUID).isNullOrEmpty()){
            cboKycentity = Cbo_kycEntity(

                validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!,
                guid,
                validate!!.returnLongValue(shgcode),
                kyc_type,
                et_kycno.text.toString(),
                frontName,
                "",
                rearName,
                "",
                0,
                0,
                0,
                0,
                "",
                1,
                1,
                1,
                1,
                validate!!.Daybetweentime(validate!!.currentdatetime),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                0,
                "",
                0,
                ""
            ,1,0)
            cboKycViewmodel!!.insert(cboKycentity!!)
            validate!!.SaveSharepreferenceString(AppSP.KYCGUID,guid)
            validate!!.CustomAlert(getString(R.string.data_saved_successfully),this,KYCDetailListActivity::class.java)
        }else{
            cboKycViewmodel!!.updateKycDetail(
                validate!!.RetriveSharepreferenceString(AppSP.KYCGUID)!!,
                kyc_type,
                et_kycno.text.toString(),
                frontName,
                "",
                rearName,
                "",
                0,
                0,
                validate!!.Daybetweentime(validate!!.currentdatetime),
                validate!!.RetriveSharepreferenceString(AppSP.userid)!!,1)

            validate!!.CustomAlert(getString(R.string.updated_successfully),this,KYCDetailListActivity::class.java)
        }
    }

    override
    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            compreesedimage()
            bitmap = BitmapFactory.decodeFile(fileUri!!.path)
            if (requestCode == 101 && resultCode == Activity.RESULT_OK) {
                IvFrntUpload.setImageBitmap(bitmap)
            } else if (requestCode == 102 && resultCode == Activity.RESULT_OK) {
                IvrearUpload.setImageBitmap(bitmap)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun compreesedimage() {
        val options = BitmapFactory.Options()
        var bitmap = BitmapFactory.decodeFile(fileUri!!.path, options)

        if (bitmap != null) {
            var actualWidth = options.outWidth
            var actualHeight = options.outHeight
            var mutableBitmap =
                Bitmap.createScaledBitmap(bitmap, actualWidth / 2, actualHeight / 2, false)

            var fOut: OutputStream? = null
            var file: File = File(fileUri!!.path)
            try {
                fOut = FileOutputStream(file)
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }
            mutableBitmap.compress(Bitmap.CompressFormat.JPEG, 50, fOut)
            try {
                fOut!!.flush()
                fOut.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
    }

    private fun CheckValidation(): Int {
        var value = 1
        if (et_kycno.text.toString().trim().length == 0) {
            validate!!.CustomAlertEditText(
                getString(R.string.Pleaseenetrmandatorydetails),
                this,
                et_kycno
            )
            value = 0
            return value
        }
      /*  else if (et_validform.text.toString().trim().length == 0) {
            validate!!.CustomAlertEditText(
                getString(R.string.Pleaseenetrmandatorydetails),
                this,
                et_validform
            )
            value = 0
            return value
        } else if (et_validtill.text.toString().trim().length == 0) {
            validate!!.CustomAlertEditText(
                getString(R.string.Pleaseenetrmandatorydetails),
                this,
                et_validtill
            )
            value = 0
            return value
        }*/


        return value
    }

    private fun checkPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        val result1 =
            ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val result2 =
            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission(requestCode: Int) {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ),
            requestCode
        )
    }

    override fun onBackPressed() {
        var intent = Intent(this, ShgListActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }

    private fun ShowImage(name: String?) {
        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val layout: View = inflater.inflate(R.layout.popupwindow, null, false)
        val window = PopupWindow(layout, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, true)
        window.showAtLocation(layout, Gravity.CENTER, 0, 0)
        val cancel: ImageView = layout.findViewById(R.id.cancel)
        val image_preview: ImageView = layout.findViewById(R.id.image_preview)
        val mediaStorageDirectory = File(
            getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
            AppSP.IMAGE_DIRECTORY_NAME
        )
      var mediaFile1 = File(mediaStorageDirectory.path + File.separator + name)
        cancel.setOnClickListener {
            window.dismiss()
        }

        Picasso.with(this).load(mediaFile1).into(image_preview)

    }

}
