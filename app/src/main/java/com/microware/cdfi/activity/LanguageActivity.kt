package com.microware.cdfi.activity

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.StateEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.LocationViewModel
import kotlinx.android.synthetic.main.activity_language.*
import kotlinx.android.synthetic.main.language_alert.view.*

class LanguageActivity : AppCompatActivity() {
    var datastate: List<StateEntity>? = null

    var validate: Validate? = null
    var locationViewModel: LocationViewModel? = null
    var lang = 0
    var statemap = HashMap<Int, String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_language)
        validate = Validate(this)
        getstate()
        locationViewModel = ViewModelProviders.of(this).get(LocationViewModel::class.java)
        datastate = locationViewModel!!.getStateByStateCode()

//        validate!!.fillStateSpinner(this,spin_state,datastate)
//

        tveng.setOnClickListener {
            lang = 1
            /* tvhindi.background = resources.getDrawable(R.drawable.white_round)
             tveng.background = resources.getDrawable(R.drawable.language_selectedbg)
             tvBangali.background = resources.getDrawable(R.drawable.white_round)
             tvmarathi.background = resources.getDrawable(R.drawable.white_round)
             tvtelgu.background = resources.getDrawable(R.drawable.white_round)
             tvtamil.background = resources.getDrawable(R.drawable.white_round)
             tvGujrat.background = resources.getDrawable(R.drawable.white_round)
             tvkannad.background = resources.getDrawable(R.drawable.white_round)
             tvmalyalam.background = resources.getDrawable(R.drawable.white_round)
 */
        }

        tvassamee.setOnClickListener {
            lang = 2
            tveng.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvassamee.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvBangali.background = resources.getDrawable(R.drawable.white_round)
            tvGujrat.background = resources.getDrawable(R.drawable.white_round)
            tvhindi.background = resources.getDrawable(R.drawable.white_round)
            tvkannad.background = resources.getDrawable(R.drawable.white_round)
            tvmalyalam.background = resources.getDrawable(R.drawable.white_round)
            tvmarathi.background = resources.getDrawable(R.drawable.white_round)
            tvoriya.background = resources.getDrawable(R.drawable.white_round)
            tvpunjabi.background = resources.getDrawable(R.drawable.white_round)
            tvtamil.background = resources.getDrawable(R.drawable.white_round)
            tvtelgu.background = resources.getDrawable(R.drawable.white_round)
            tvurdu.background = resources.getDrawable(R.drawable.white_round)


        }
        tvBangali.setOnClickListener {
            lang = 3
            tveng.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvassamee.background = resources.getDrawable(R.drawable.white_round)
            tvBangali.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvGujrat.background = resources.getDrawable(R.drawable.white_round)
            tvhindi.background = resources.getDrawable(R.drawable.white_round)
            tvkannad.background = resources.getDrawable(R.drawable.white_round)
            tvmalyalam.background = resources.getDrawable(R.drawable.white_round)
            tvmarathi.background = resources.getDrawable(R.drawable.white_round)
            tvoriya.background = resources.getDrawable(R.drawable.white_round)
            tvpunjabi.background = resources.getDrawable(R.drawable.white_round)
            tvtamil.background = resources.getDrawable(R.drawable.white_round)
            tvtelgu.background = resources.getDrawable(R.drawable.white_round)
            tvurdu.background = resources.getDrawable(R.drawable.white_round)


        }

        tvGujrat.setOnClickListener {
            lang = 4
            tveng.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvassamee.background = resources.getDrawable(R.drawable.white_round)
            tvBangali.background = resources.getDrawable(R.drawable.white_round)
            tvGujrat.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvhindi.background = resources.getDrawable(R.drawable.white_round)
            tvkannad.background = resources.getDrawable(R.drawable.white_round)
            tvmalyalam.background = resources.getDrawable(R.drawable.white_round)
            tvmarathi.background = resources.getDrawable(R.drawable.white_round)
            tvoriya.background = resources.getDrawable(R.drawable.white_round)
            tvpunjabi.background = resources.getDrawable(R.drawable.white_round)
            tvtamil.background = resources.getDrawable(R.drawable.white_round)
            tvtelgu.background = resources.getDrawable(R.drawable.white_round)
            tvurdu.background = resources.getDrawable(R.drawable.white_round)

        }
        tvhindi.setOnClickListener {
            lang = 5
            tveng.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvassamee.background = resources.getDrawable(R.drawable.white_round)
            tvBangali.background = resources.getDrawable(R.drawable.white_round)
            tvGujrat.background = resources.getDrawable(R.drawable.white_round)
            tvhindi.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvkannad.background = resources.getDrawable(R.drawable.white_round)
            tvmalyalam.background = resources.getDrawable(R.drawable.white_round)
            tvmarathi.background = resources.getDrawable(R.drawable.white_round)
            tvoriya.background = resources.getDrawable(R.drawable.white_round)
            tvpunjabi.background = resources.getDrawable(R.drawable.white_round)
            tvtamil.background = resources.getDrawable(R.drawable.white_round)
            tvtelgu.background = resources.getDrawable(R.drawable.white_round)
            tvurdu.background = resources.getDrawable(R.drawable.white_round)

        }
        tvkannad.setOnClickListener {
            lang = 6
            tveng.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvassamee.background = resources.getDrawable(R.drawable.white_round)
            tvBangali.background = resources.getDrawable(R.drawable.white_round)
            tvGujrat.background = resources.getDrawable(R.drawable.white_round)
            tvhindi.background = resources.getDrawable(R.drawable.white_round)
            tvkannad.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvmalyalam.background = resources.getDrawable(R.drawable.white_round)
            tvmarathi.background = resources.getDrawable(R.drawable.white_round)
            tvoriya.background = resources.getDrawable(R.drawable.white_round)
            tvpunjabi.background = resources.getDrawable(R.drawable.white_round)
            tvtamil.background = resources.getDrawable(R.drawable.white_round)
            tvtelgu.background = resources.getDrawable(R.drawable.white_round)
            tvurdu.background = resources.getDrawable(R.drawable.white_round)

        }
        tvmalyalam.setOnClickListener {
            lang = 7
            tveng.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvassamee.background = resources.getDrawable(R.drawable.white_round)
            tvBangali.background = resources.getDrawable(R.drawable.white_round)
            tvGujrat.background = resources.getDrawable(R.drawable.white_round)
            tvhindi.background = resources.getDrawable(R.drawable.white_round)
            tvkannad.background = resources.getDrawable(R.drawable.white_round)
            tvmalyalam.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvmarathi.background = resources.getDrawable(R.drawable.white_round)
            tvoriya.background = resources.getDrawable(R.drawable.white_round)
            tvpunjabi.background = resources.getDrawable(R.drawable.white_round)
            tvtamil.background = resources.getDrawable(R.drawable.white_round)
            tvtelgu.background = resources.getDrawable(R.drawable.white_round)
            tvurdu.background = resources.getDrawable(R.drawable.white_round)

        }
        tvmarathi.setOnClickListener {
            lang = 8
            tveng.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvassamee.background = resources.getDrawable(R.drawable.white_round)
            tvBangali.background = resources.getDrawable(R.drawable.white_round)
            tvGujrat.background = resources.getDrawable(R.drawable.white_round)
            tvhindi.background = resources.getDrawable(R.drawable.white_round)
            tvkannad.background = resources.getDrawable(R.drawable.white_round)
            tvmalyalam.background = resources.getDrawable(R.drawable.white_round)
            tvmarathi.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvoriya.background = resources.getDrawable(R.drawable.white_round)
            tvpunjabi.background = resources.getDrawable(R.drawable.white_round)
            tvtamil.background = resources.getDrawable(R.drawable.white_round)
            tvtelgu.background = resources.getDrawable(R.drawable.white_round)
            tvurdu.background = resources.getDrawable(R.drawable.white_round)

        }
        tvoriya.setOnClickListener {
            lang = 9
            tveng.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvassamee.background = resources.getDrawable(R.drawable.white_round)
            tvBangali.background = resources.getDrawable(R.drawable.white_round)
            tvGujrat.background = resources.getDrawable(R.drawable.white_round)
            tvhindi.background = resources.getDrawable(R.drawable.white_round)
            tvkannad.background = resources.getDrawable(R.drawable.white_round)
            tvmalyalam.background = resources.getDrawable(R.drawable.white_round)
            tvmarathi.background = resources.getDrawable(R.drawable.white_round)
            tvoriya.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvpunjabi.background = resources.getDrawable(R.drawable.white_round)
            tvtamil.background = resources.getDrawable(R.drawable.white_round)
            tvtelgu.background = resources.getDrawable(R.drawable.white_round)
            tvurdu.background = resources.getDrawable(R.drawable.white_round)

        }
        tvpunjabi.setOnClickListener {
            lang = 10
            tveng.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvassamee.background = resources.getDrawable(R.drawable.white_round)
            tvBangali.background = resources.getDrawable(R.drawable.white_round)
            tvGujrat.background = resources.getDrawable(R.drawable.white_round)
            tvhindi.background = resources.getDrawable(R.drawable.white_round)
            tvkannad.background = resources.getDrawable(R.drawable.white_round)
            tvmalyalam.background = resources.getDrawable(R.drawable.white_round)
            tvmarathi.background = resources.getDrawable(R.drawable.white_round)
            tvoriya.background = resources.getDrawable(R.drawable.white_round)
            tvpunjabi.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvtamil.background = resources.getDrawable(R.drawable.white_round)
            tvtelgu.background = resources.getDrawable(R.drawable.white_round)
            tvurdu.background = resources.getDrawable(R.drawable.white_round)

        }
        tvtamil.setOnClickListener {
            lang = 11
            tveng.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvassamee.background = resources.getDrawable(R.drawable.white_round)
            tvBangali.background = resources.getDrawable(R.drawable.white_round)
            tvGujrat.background = resources.getDrawable(R.drawable.white_round)
            tvhindi.background = resources.getDrawable(R.drawable.white_round)
            tvkannad.background = resources.getDrawable(R.drawable.white_round)
            tvmalyalam.background = resources.getDrawable(R.drawable.white_round)
            tvmarathi.background = resources.getDrawable(R.drawable.white_round)
            tvoriya.background = resources.getDrawable(R.drawable.white_round)
            tvpunjabi.background = resources.getDrawable(R.drawable.white_round)
            tvtamil.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvtelgu.background = resources.getDrawable(R.drawable.white_round)
            tvurdu.background = resources.getDrawable(R.drawable.white_round)

        }
        tvtelgu.setOnClickListener {
            lang = 12
            tveng.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvassamee.background = resources.getDrawable(R.drawable.white_round)
            tvBangali.background = resources.getDrawable(R.drawable.white_round)
            tvGujrat.background = resources.getDrawable(R.drawable.white_round)
            tvhindi.background = resources.getDrawable(R.drawable.white_round)
            tvkannad.background = resources.getDrawable(R.drawable.white_round)
            tvmalyalam.background = resources.getDrawable(R.drawable.white_round)
            tvmarathi.background = resources.getDrawable(R.drawable.white_round)
            tvoriya.background = resources.getDrawable(R.drawable.white_round)
            tvpunjabi.background = resources.getDrawable(R.drawable.white_round)
            tvtamil.background = resources.getDrawable(R.drawable.white_round)
            tvtelgu.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvurdu.background = resources.getDrawable(R.drawable.white_round)

        }
        tvurdu.setOnClickListener {
            lang = 13
            tveng.background = resources.getDrawable(R.drawable.language_selectedbg)
            tvassamee.background = resources.getDrawable(R.drawable.white_round)
            tvBangali.background = resources.getDrawable(R.drawable.white_round)
            tvGujrat.background = resources.getDrawable(R.drawable.white_round)
            tvhindi.background = resources.getDrawable(R.drawable.white_round)
            tvkannad.background = resources.getDrawable(R.drawable.white_round)
            tvmalyalam.background = resources.getDrawable(R.drawable.white_round)
            tvmarathi.background = resources.getDrawable(R.drawable.white_round)
            tvoriya.background = resources.getDrawable(R.drawable.white_round)
            tvpunjabi.background = resources.getDrawable(R.drawable.white_round)
            tvtamil.background = resources.getDrawable(R.drawable.white_round)
            tvtelgu.background = resources.getDrawable(R.drawable.white_round)
            tvurdu.background = resources.getDrawable(R.drawable.language_selectedbg)

        }


        btn_login.setOnClickListener {
            var language = LabelSet.getText(
                "hindilang",
                R.string.hindilang
            )
            var languagecode = "hi"
            if (lang == 2) {
                language = LabelSet.getText(
                    "assameelang",
                    R.string.assameelang
                )
                languagecode = "as"
            } else if (lang == 3) {
                language = LabelSet.getText(
                    "banglalang",
                    R.string.banglalang
                )
                languagecode = "bn"
            } else if (lang == 4) {
                language = LabelSet.getText(
                    "gujaratilang",
                    R.string.gujaratilang
                )
                languagecode = "gu"
            } else if (lang == 5) {
                language = LabelSet.getText(
                    "hindilang",
                    R.string.hindilang
                )
                languagecode = "hi"
            } else if (lang == 6) {
                language = LabelSet.getText(
                    "kannadlang",
                    R.string.kannadlang
                )
                languagecode = "kn"
            } else if (lang == 7) {
                language = LabelSet.getText(
                    "malayalamlang",
                    R.string.malayalamlang
                )
                languagecode = "ml"
            } else if (lang == 8) {
                language = LabelSet.getText(
                    "marathilang",
                    R.string.marathilang
                )
                languagecode = "mr"
            } else if (lang == 9) {
                language = LabelSet.getText(
                    "oriyalang",
                    R.string.oriyalang
                )
                languagecode = "or"
            } else if (lang == 10) {
                language = LabelSet.getText(
                    "punjabilang",
                    R.string.punjabilang
                )
                languagecode = "pa"
            } else if (lang == 11) {
                language = LabelSet.getText(
                    "tamillang",
                    R.string.tamillang
                )
                languagecode = "ta"
            } else if (lang == 12) {
                language = LabelSet.getText(
                    "telgulang",
                    R.string.telgulang
                )
                languagecode = "te"
            } else if (lang == 13) {
                language = LabelSet.getText(
                    "urdulang",
                    R.string.urdulang
                )
                languagecode = "ur"
            }

            if (spin_state.selectedItemPosition > 0) {

                showLangAlertDialog(language, languagecode)
            }else {
                validate!!.CustomAlert(LabelSet.getText(
                    "select_state",
                    R.string.select_state
                ), this)
            }
        }

        if(validate!!.RetriveSharepreferenceInt(
                AppSP.State_Selected)>0){
            spin_state.isEnabled=false
            spin_state.setSelection(getKeyByValue(statemap,validate!!.RetriveSharepreferenceInt(
                AppSP.State_Selected)))
        }else{
            spin_state.isEnabled=true
        }


    }
    fun  getKeyByValue(statemap: HashMap<Int, String>, stateid: Int): Int {
        var pos=0
        for (i in 0  until statemap.size) {
            if (stateid==statemap.get(i)!!.toInt()) {
                pos= i
                break
            }
        }
        return pos
    }
    private fun showLangAlertDialog(language: String,languagecode: String) {
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.language_alert, null)
        val mBuilder = AlertDialog.Builder(this).setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = getString(R.string.want_toselect) + language + getString(R.string.select_lang)

        mDialogView.btn_yes.setOnClickListener {
            mAlertDialog.dismiss()

            validate!!.SaveSharepreferenceInt(
                AppSP.State_Selected,
                statemap.get(spin_state.selectedItemPosition)!!.toInt())
            validate!!.SaveSharepreferenceString(
                AppSP.SelectedLanguage,
                language
            )
            validate!!.SaveSharepreferenceString(AppSP.SelectedLanguageCode,languagecode)

            Toast.makeText(this,statemap.get(spin_state.selectedItemPosition)!!.toString(),Toast.LENGTH_LONG).show()

            var intent = Intent(this, LoginActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }

        mDialogView.btn_no.setOnClickListener {
            mAlertDialog.dismiss()
        }
    }

    fun getstate() {

        statemap.put(0, "0")
        statemap.put(1, "1")
        statemap.put(2, "2")
        statemap.put(3, "3")
        statemap.put(4, "4")
        statemap.put(5, "5")
        statemap.put(6, "6")
        statemap.put(7, "31")
        statemap.put(8, "36")
        statemap.put(9, "7")
        statemap.put(10, "8")
        statemap.put(11, "9")
        statemap.put(12, "10")
        statemap.put(13, "11")
        statemap.put(14, "12")
        statemap.put(15, "32")
        statemap.put(16, "13")
        statemap.put(17, "14")
        statemap.put(18, "35")
        statemap.put(19, "17")
        statemap.put(20, "15")
        statemap.put(21, "16")
        statemap.put(22, "18")
        statemap.put(23, "19")
        statemap.put(24, "20")
        statemap.put(25, "21")
        statemap.put(26, "22")
        statemap.put(27, "23")
        statemap.put(28, "24")
        statemap.put(29, "25")
        statemap.put(30, "26")
        statemap.put(31, "27")
        statemap.put(32, "34")
        statemap.put(33, "28")
        statemap.put(34, "29")
        statemap.put(35, "33")
        statemap.put(36, "30")

    }

}