package com.microware.cdfi.activity

import android.Manifest
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.lifecycle.ViewModelProviders
import com.google.gson.JsonObject
import com.microware.cdfi.R
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.api.response.MastersResponse
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.RoleEntity
import com.microware.cdfi.entity.UserEntity
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.ResponseViewModel
import com.microware.cdfi.viewModel.UserViewmodel
import kotlinx.android.synthetic.main.activity_language.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.btn_login
import kotlinx.android.synthetic.main.activity_main.lokos_core
import kotlinx.android.synthetic.main.activity_main.tv_version
import kotlinx.android.synthetic.main.customealertchangerolel.view.*
import kotlinx.android.synthetic.main.fragment_bank_detail.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : AppCompatActivity() {

    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: ApiInterface? = null
    var apiInterface1: ApiInterface? = null
    var responseViewModel: ResponseViewModel? = null
    var userViewModel: UserViewmodel? = null
    var iDownload: Int = 0
    internal var PERMISSION_ALL = 1
    var validate: Validate? = null
    var Rolemap = HashMap<Int, String>()
    internal var PERMISSIONS = arrayOf(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.INTERNET,
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_NETWORK_STATE,
        Manifest.permission.CAMERA
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        validate = Validate(this)
        validate!!.SaveSharepreferenceString(AppSP.Langaugecode, "en")
        if (!hasPermissions(this, *PERMISSIONS)) {
            ActivityCompat
                .requestPermissions(this, PERMISSIONS, PERMISSION_ALL)
        }
        getrole()
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        apiInterface1 = ApiClientConnection.Companion.instance.createApiInterface()
        responseViewModel = ViewModelProviders.of(this).get(ResponseViewModel::class.java)


        btn_login.setOnClickListener {
            if (isNetworkConnected()) {

                var iCheck = sCheckValidation()
                if (iCheck == 1) {
//                    next()
                    validate!!.SaveSharepreferenceString(
                        AppSP.Roleid,
                        Rolemap.get(spin_role.selectedItemPosition).toString()
                    )
                    CDFIApplication.changedb()
                    apiInterface1 = ApiClientConnection.Companion.instance.createApiInterface()
//                    chkdatabase()
                    importMastersVersion()
                }

            } else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "no_internet_msg",
                        R.string.no_internet_msg
                    ), this
                )

            }
        }

        tv_forgot.setOnClickListener {
            //            val startMain = Intent(this, Forgotpassword::class.java)
//            startMain.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
//            startActivity(startMain)
        }

        chk_showHide.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                etPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
            } else {
                etPassword.transformationMethod = PasswordTransformationMethod.getInstance()
            }
        }

        setLabelText()
        setdata()
        tv_version.text = getVersion()
    }

    fun setdata() {

        if (validate!!.RetriveSharepreferenceString(AppSP.userid)!!.length>0) {
            etUsername.setText(validate!!.RetriveSharepreferenceString(AppSP.userid).toString().toUpperCase())
            etPassword.setText(validate!!.returnStringValue(
                AESEncryption.decrypt(
                    validate!!.RetriveSharepreferenceString(AppSP.Password), validate!!.RetriveSharepreferenceString(AppSP.userid).toString().toUpperCase())))
            spin_role.setSelection(getKeyByValue())
            etUsername.isEnabled=false
        }else{
            etUsername.isEnabled=true
        }

    }
    fun  getKeyByValue(): Int {
        var pos=0
        for (i in 0  until Rolemap.size) {
            if (validate!!.RetriveSharepreferenceString(AppSP.Roleid)==Rolemap.get(i)!!) {
                pos= i
                break
            }
        }
        return pos
    }
    fun importMastersVersion() {
        var grant_type = "password"
        var userId = etUsername.text.toString().trim().toUpperCase()
        progressDialog = ProgressDialog.show(
            this, "",
            resources.getString(R.string.authenticate_user)
        )
        try {

            val callCount = apiInterface?.gettokenforce(
                grant_type,
                etUsername.text.toString().trim().toUpperCase(),
                validate!!.returnStringValue(
                    AESEncryption.encrypt(
                        validate!!.returnStringValue(
                            etPassword.text.toString()),etUsername.text.toString().toUpperCase())),
                validate!!.RetriveSharepreferenceInt(AppSP.State_Selected), 1,
                validate!!.RetriveSharepreferenceString(AppSP.Roleid)!!
            )
            callCount?.enqueue(object : Callback<JsonObject> {
                override fun onFailure(callCount: Call<JsonObject>, t: Throwable) {
                    Toast.makeText(this@LoginActivity, t.message, Toast.LENGTH_LONG).show()
                    progressDialog.dismiss()
                }

                override fun onResponse(
                    call: Call<JsonObject>,
                    response: Response<JsonObject>
                ) {
                    if (response.isSuccessful) {
                        when (response.code()) {
                            200 -> {


                                var jsonObject1: JSONObject? = null
                                try {
                                    jsonObject1 = JSONObject(response.body().toString())

                                    val auth = jsonObject1.get("accessToken").toString()
                                    val mobileuser =
                                        validate!!.returnIntegerValue(jsonObject1.optString("mobileWeb"))
                                    if (mobileuser == 2) {
                                        if (auth.trim().length > 0) {
                                            try {
                                                progressDialog.setMessage(
                                                    LabelSet.getText(
                                                        "downlaod_master_data",
                                                        R.string.downlaod_master_data
                                                    )
                                                )
                                                val call =
                                                    apiInterface1?.getAllMasterDatabylanguagecode(
                                                        validate!!.RetriveSharepreferenceString(
                                                            AppSP.SelectedLanguageCode
                                                        ),
                                                        auth,
                                                        etUsername.text.trim().toString().toUpperCase()

                                                    )
                                                call?.enqueue(object : Callback<MastersResponse> {

                                                    //  call?.enqueue(object : Callback<List<MastersResponse>>, retrofit2.Callback<List<MastersResponse>> {
                                                    override fun onFailure(
                                                        call: Call<MastersResponse>,
                                                        t: Throwable
                                                    ) {
                                                //        t.printStackTrace()
                                                        Toast.makeText(
                                                            this@LoginActivity,
                                                            t.message,
                                                            Toast.LENGTH_LONG
                                                        ).show()

                                                        progressDialog.dismiss()
                                                    }

                                                    override fun onResponse(
                                                        call: Call<MastersResponse>,
                                                        response: Response<MastersResponse>
                                                    ) {
                                                        if (response.isSuccessful) {
                                                            when (response.code()) {
                                                                200 -> {

                                                                    if (response.body()?.mst_State!!.isNotEmpty()) {
                                                                        try {
                                                                            CDFIApplication.database?.stateDao()
                                                                                ?.deleteAllState()
                                                                            CDFIApplication.database?.districtDao()
                                                                                ?.deleteAllDistrict()
                                                                            CDFIApplication.database?.blockDao()
                                                                                ?.deleteAllBlocks()
                                                                            CDFIApplication.database?.panchayatDao()
                                                                                ?.deleteAllPanchayat()
                                                                            CDFIApplication.database?.villageDao()
                                                                                ?.deleteAllVillage()
                                                                            CDFIApplication.database?.userDao()
                                                                                ?.delete()
                                                                            CDFIApplication.database?.lookupdao()
                                                                                ?.delete()
                                                                            CDFIApplication.database?.responseCodeDao()
                                                                                ?.deleteResponse()
                                                                            CDFIApplication.database?.subcommitteeMasterDao()
                                                                                ?.deleteScMasterdata()
                                                                            CDFIApplication.database?.fundingDao()
                                                                                ?.deleteAllAgency()
                                                                            CDFIApplication.database?.mstProductDao()
                                                                                ?.deleteProducts()
                                                                            CDFIApplication.database?.fundsDao()
                                                                                ?.deleteFundType()
                                                                            CDFIApplication.database?.mstCOADao()
                                                                                ?.deleteAll()
                                                                            CDFIApplication.database?.mstVOCOADao()
                                                                                ?.deleteAll()
                                                                            CDFIApplication.database?.cardreCateogaryDao()
                                                                                ?.deleteAllCateogary()
                                                                            CDFIApplication.database?.labelMasterDao()
                                                                                ?.deleteAllLabel()
                                                                            CDFIApplication.database?.cardreRoleDao()
                                                                                ?.deleteAllCardreRole()

                                                                            CDFIApplication.database?.stateDao()
                                                                                ?.insertState(
                                                                                    response.body()?.mst_State
                                                                                )
                                                                            CDFIApplication.database?.districtDao()
                                                                                ?.insertDistrict(
                                                                                    response.body()?.mst_District
                                                                                )
                                                                            CDFIApplication.database?.blockDao()
                                                                                ?.insertAllBlock(
                                                                                    response.body()?.mst_Block
                                                                                )
                                                                            CDFIApplication.database?.panchayatDao()
                                                                                ?.insertAllPanchayat(
                                                                                    response.body()?.mstPanchayat
                                                                                )
                                                                            if (!response.body()?.mstVillage.isNullOrEmpty()) {
                                                                                CDFIApplication.database?.villageDao()
                                                                                    ?.insertAllVillage(
                                                                                        response.body()?.mstVillage
                                                                                    )
                                                                            }
                                                                            CDFIApplication.database?.responseCodeDao()
                                                                                ?.insertAllResponse(
                                                                                    response.body()?.mstResponse
                                                                                )

                                                                            CDFIApplication.database?.laonProductDao()
                                                                                ?.insertproductlist(
                                                                                    response.body()?.mstLoanProduct
                                                                                )
                                                                            CDFIApplication.database?.fundsDao()
                                                                                ?.insertFundslist(
                                                                                    response.body()?.mstFundType
                                                                                )
                                                                            CDFIApplication.database?.mstCOADao()
                                                                                ?.insertMstCOAAllData(
                                                                                    response.body()?.mstCoa
                                                                                )
                                                                            if(!response.body()?.mstVoCoa.isNullOrEmpty()) {
                                                                                CDFIApplication.database?.mstVOCOADao()
                                                                                    ?.insertMstCOAAllData(
                                                                                        response.body()?.mstVoCoa
                                                                                    )
                                                                            }
                                                                            CDFIApplication.database?.lookupdao()
                                                                                ?.insertlookup(
                                                                                    response.body()?.mstlookup
                                                                                )
                                                                            CDFIApplication.database?.subcommitteeMasterDao()
                                                                                ?.insertScMasterdata(
                                                                                    response.body()?.subCommitteeMasterList
                                                                                )

                                                                            if (!response.body()?.mstbankBranch.isNullOrEmpty()) {
                                                                                CDFIApplication.database?.masterbankbranchDao()
                                                                                    ?.insertbranch(
                                                                                        response.body()?.mstbankBranch
                                                                                    )
                                                                            }

                                                                            if (!response.body()?.mstBankMaster.isNullOrEmpty()) {
                                                                                CDFIApplication.database?.masterBankDao()
                                                                                    ?.insertbank(
                                                                                        response.body()?.mstBankMaster
                                                                                    )
                                                                            }

                                                                            if (!response.body()?.mstlabelMasterList.isNullOrEmpty()) {
                                                                                CDFIApplication.database?.labelMasterDao()
                                                                                    ?.insertAllLabel(
                                                                                        response.body()?.mstlabelMasterList
                                                                                    )
                                                                            }

                                                                            if (!response.body()?.cadreCategoryMasterList.isNullOrEmpty()) {
                                                                                CDFIApplication.database?.cardreCateogaryDao()
                                                                                    ?.insertAllCateogary(
                                                                                        response.body()?.cadreCategoryMasterList
                                                                                    )
                                                                            }
                                                                            if (!response.body()?.cadreRoleMasterlist.isNullOrEmpty()) {
                                                                                CDFIApplication.database?.cardreRoleDao()
                                                                                    ?.insertAllCardreRole(
                                                                                        response.body()?.cadreRoleMasterlist
                                                                                    )
                                                                            }

                                                                            if (!response.body()?.mstFundingMaster.isNullOrEmpty()) {
                                                                                CDFIApplication.database?.fundingDao()
                                                                                    ?.insertAllAgency(
                                                                                        response.body()?.mstFundingMaster
                                                                                    )
                                                                            }

                                                                            var list =
                                                                                response.body()?.mstuser!!
                                                                            var userlist =
                                                                                list

                                                                            if (!userlist.isNullOrEmpty()) {
                                                                                var userEntity: UserEntity? =
                                                                                    null
                                                                                userEntity =
                                                                                    UserEntity(
                                                                                        userlist.get(
                                                                                            0
                                                                                        ).userId,
                                                                                        userlist.get(
                                                                                            0
                                                                                        ).userName,
                                                                                        userlist.get(
                                                                                            0
                                                                                        ).designation,
                                                                                        userlist.get(
                                                                                            0
                                                                                        ).emailId,
                                                                                        userlist.get(
                                                                                            0
                                                                                        ).mobileNo
                                                                                    )
                                                                                validate!!.SaveSharepreferenceString(
                                                                                    AppSP.userid,
                                                                                    userlist.get(0).userId
                                                                                )

                                                                                validate!!.SaveSharepreferenceString(
                                                                                    AppSP.token,
                                                                                    validate!!.returnStringValue(
                                                                                        AESEncryption.encrypt(
                                                                                            auth,validate!!.RetriveSharepreferenceString(AppSP.userid)))
                                                                                )

                                                                                validate!!.SaveSharepreferenceString(
                                                                                    AppSP.Password,
                                                                                    validate!!.returnStringValue(
                                                                                        AESEncryption.encrypt(
                                                                                            validate!!.returnStringValue(
                                                                                                etPassword.text.toString()
                                                                                            ),validate!!.RetriveSharepreferenceString(AppSP.userid)))
                                                                                )
                                                                                CDFIApplication.database?.userDao()
                                                                                    ?.insertuser(
                                                                                        userEntity
                                                                                    )
                                                                            }
//
                                                                            var roleid = ""
                                                                            var RoleName = ""
                                                                            var userrole =
                                                                                response.body()?.mstuser!!.get(
                                                                                    0
                                                                                ).userRoleRightsMap

                                                                            var userrolemaster =
                                                                                response.body()?.mstuser!!.get(
                                                                                    0
                                                                                ).userRoleRightsMap!!.roleMaster
                                                                            if (userrolemaster != null) {
                                                                                roleid =
                                                                                    userrolemaster.roleId
                                                                                RoleName =
                                                                                    userrolemaster.roleName
                                                                                var roleEntity: RoleEntity? =
                                                                                    null
                                                                                roleEntity =
                                                                                    RoleEntity(
                                                                                        userrolemaster.roleId,
                                                                                        userrolemaster.roleName,
                                                                                        userrolemaster.status,
                                                                                        userrolemaster.categoryId,
                                                                                        userrolemaster.levelId,
                                                                                        userrolemaster.typeId
                                                                                    )

                                                                                CDFIApplication.database?.roledao()
                                                                                    ?.insertrole(
                                                                                        roleEntity
                                                                                    )
                                                                            }
                                                                            if (userrole != null) {

                                                                                validate!!.SaveSharepreferenceInt(
                                                                                    AppSP.statecode,
                                                                                    userrole.stateId
                                                                                )

                                                                                validate!!.SaveSharepreferenceInt(
                                                                                    AppSP.districtcode,
                                                                                    userrole.districtId
                                                                                )

                                                                                validate!!.SaveSharepreferenceInt(
                                                                                    AppSP.blockcode,
                                                                                    userrole.blockId
                                                                                )

                                                                                if (!userrole.panchayatId.isNullOrEmpty()) {
                                                                                    if (userrole.panchayatId.contains(
                                                                                            ","
                                                                                        )
                                                                                    ) {

                                                                                        var panchayatid =
                                                                                            userrole.panchayatId.split(
                                                                                                ","
                                                                                            )
                                                                                        validate!!.SaveSharepreferenceInt(
                                                                                            AppSP.panchayatcode,
                                                                                            validate!!.returnIntegerValue(
                                                                                                panchayatid[0]
                                                                                            )
                                                                                        )
                                                                                    } else {
                                                                                        validate!!.SaveSharepreferenceInt(
                                                                                            AppSP.panchayatcode,
                                                                                            validate!!.returnIntegerValue(
                                                                                                userrole.panchayatId
                                                                                            )
                                                                                        )
                                                                                    }
                                                                                }
                                                                                validate!!.SaveSharepreferenceString(
                                                                                    AppSP.Roleid,
                                                                                    roleid
                                                                                )
                                                                                validate!!.SaveSharepreferenceString(
                                                                                    AppSP.RoleName,
                                                                                    RoleName
                                                                                )
                                                                                if (!userrole.villageId.isNullOrEmpty()) {
                                                                                    if (userrole.villageId!!.contains(
                                                                                            ","
                                                                                        )
                                                                                    ) {

                                                                                        var villageid =
                                                                                            userrole.villageId!!.split(
                                                                                                ","
                                                                                            )
                                                                                        validate!!.SaveSharepreferenceInt(
                                                                                            AppSP.villagecode,
                                                                                            validate!!.returnIntegerValue(
                                                                                                villageid[0]
                                                                                            )
                                                                                        )
                                                                                    } else {
                                                                                        validate!!.SaveSharepreferenceInt(
                                                                                            AppSP.villagecode,
                                                                                            validate!!.returnIntegerValue(
                                                                                                userrole.villageId
                                                                                            )
                                                                                        )
                                                                                    }
                                                                                }

                                                                            }

                                                                            iDownload = 1
                                                                            progressDialog.dismiss()
                                                                            next()
                                                                        } catch (ex: Exception) {
                                                                            ex.printStackTrace()
                                                                            progressDialog.dismiss()
                                                                        }
                                                                    }

                                                                }
                                                                else -> {
                                                                    iDownload = 0
                                                                    progressDialog.dismiss()
                                                                    var resCode = 0
                                                                    var resMsg = ""
                                                                    if (response.errorBody()
                                                                            ?.contentLength() == 0L || response.errorBody()
                                                                            ?.contentLength()!! < 0L
                                                                    ) {
                                                                        resCode = response.code()
                                                                        resMsg = response.message()
                                                                    } else {
                                                                        var jsonObject1 =
                                                                            JSONObject(
                                                                                response.errorBody()!!
                                                                                    .source()
                                                                                    .readUtf8()
                                                                                    .toString()
                                                                            )

                                                                        resCode =
                                                                            validate!!.returnIntegerValue(
                                                                                jsonObject1.optString(
                                                                                    "responseCode"
                                                                                )
                                                                                    .toString()
                                                                            )
                                                                        resMsg =
                                                                            validate!!.returnStringValue(
                                                                                jsonObject1.optString(
                                                                                    "responseMsg"
                                                                                )
                                                                                    .toString()
                                                                            )
                                                                    }
                                                                    validate!!.CustomAlertMsg(
                                                                        this@LoginActivity,
                                                                        responseViewModel,
                                                                        resCode,
                                                                        validate!!.RetriveSharepreferenceString(
                                                                            AppSP.Langaugecode
                                                                        )!!,
                                                                        resMsg
                                                                    )
                                                                }


                                                            }

                                                        } else {
                                                            iDownload = 0
                                                            progressDialog.dismiss()
                                                            var resCode = 0
                                                            var resMsg = ""
                                                            if (response.errorBody()
                                                                    ?.contentLength() == 0L || response.errorBody()
                                                                    ?.contentLength()!! < 0L
                                                            ) {
                                                                resCode = response.code()
                                                                resMsg = response.message()
                                                            } else {
                                                                var jsonObject1 =
                                                                    JSONObject(
                                                                        response.errorBody()!!
                                                                            .source()
                                                                            .readUtf8().toString()
                                                                    )

                                                                resCode =
                                                                    validate!!.returnIntegerValue(
                                                                        jsonObject1.optString("responseCode")
                                                                            .toString()
                                                                    )
                                                                resMsg =
                                                                    validate!!.returnStringValue(
                                                                        jsonObject1.optString("responseMsg")
                                                                            .toString()
                                                                    )
                                                            }
                                                            validate!!.CustomAlertMsg(
                                                                this@LoginActivity,
                                                                responseViewModel,
                                                                resCode,
                                                                validate!!.RetriveSharepreferenceString(
                                                                    AppSP.Langaugecode
                                                                )!!,
                                                                resMsg
                                                            )
                                                        }
                                                    }

                                                })
                                            } catch (ex: Exception) {
                                                ex.printStackTrace()
                                            }
                                        }
                                    } else {
                                        progressDialog.dismiss()
                                        validate!!.CustomAlert(
                                            LabelSet.getText("loginwithmobileuser", R.string.loginwithmobileuser),this@LoginActivity

                                        )
                                    }


                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                    progressDialog.dismiss()
                                }

                            }

                            500 -> {
                                var resCode = 0
                                var resMsg = ""
                                if (response.errorBody()
                                        ?.contentLength() == 0L || response.errorBody()
                                        ?.contentLength()!! < 0L
                                ) {
                                    resCode = response.code()
                                    resMsg = response.message()
                                } else {
                                    var jsonObject1 =
                                        JSONObject(
                                            response.errorBody()!!.source().readUtf8().toString()
                                        )

                                    resCode =
                                        validate!!.returnIntegerValue(
                                            jsonObject1.optString("responseCode").toString()
                                        )
                                    resMsg = validate!!.returnStringValue(
                                        jsonObject1.optString("responseMsg").toString()
                                    )
                                }
                                validate!!.CustomAlertMsg(
                                    this@LoginActivity,
                                    responseViewModel,
                                    resCode,
                                    validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                                    resMsg
                                )
                            }
                            503 -> {
                                var resCode = 0
                                var resMsg = ""
                                if (response.errorBody()
                                        ?.contentLength() == 0L || response.errorBody()
                                        ?.contentLength()!! < 0L
                                ) {
                                    resCode = response.code()
                                    resMsg = response.message()
                                } else {
                                    var jsonObject1 =
                                        JSONObject(
                                            response.errorBody()!!.source().readUtf8().toString()
                                        )

                                    resCode =
                                        validate!!.returnIntegerValue(
                                            jsonObject1.optString("responseCode").toString()
                                        )
                                    resMsg = validate!!.returnStringValue(
                                        jsonObject1.optString("responseMsg").toString()
                                    )
                                }
                                validate!!.CustomAlertMsg(
                                    this@LoginActivity,
                                    responseViewModel,
                                    resCode,
                                    validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                                    resMsg
                                )
                            }
                        }
                    } else {
                        iDownload = 0
                        progressDialog.dismiss()
                        var resCode = 0
                        var resMsg = ""
                        if (response.errorBody()?.contentLength() == 0L || response.errorBody()
                                ?.contentLength()!! < 0L
                        ) {
                            resCode = response.code()
                            resMsg = response.message()
                        } else {
                            var jsonObject1 =
                                JSONObject(response.errorBody()!!.source().readUtf8().toString())

                            resCode =
                                validate!!.returnIntegerValue(
                                    jsonObject1.optString("responseCode").toString()
                                )
                            resMsg = validate!!.returnStringValue(
                                jsonObject1.optString("responseMsg").toString()
                            )
                        }
                        validate!!.CustomAlertMsg(
                            this@LoginActivity, responseViewModel, resCode,
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!, resMsg
                        )
                    }

                }

            })


        } catch (ex: Exception) {
            ex.printStackTrace()
            progressDialog.dismiss()
        }


        fun Context.toast(message: String) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        }


    }

    fun next() {
        val startMain = Intent(this, VerifyPinActivity::class.java)
        startMain.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(startMain)
    }

    fun sCheckValidation(): Int {
        if (spin_role.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_role,
                LabelSet.getText("selectrole", R.string.selectrole)
            )
            return 0
        }
        if (etUsername.text.toString().trim().length == 0) {
            AlertDialog.Builder(this)
                .setTitle(
                    LabelSet.getText(
                        "Invalidcrede" +
                                "ntials",
                        R.string.Invalidcredentials
                    )
                )
                .setMessage(
                    LabelSet.getText(
                        "Invalidcredentials",
                        R.string.Invalidcredentials
                    )
                )
                .setPositiveButton(android.R.string.ok) { _, _ -> }
                .setIcon(android.R.drawable.ic_dialog_alert).show()
            return 0
        }
        if (etPassword.text.toString().trim().length == 0) {
            AlertDialog.Builder(this)
                .setTitle(
                    LabelSet.getText(
                        "Invalidcredentials",
                        R.string.Invalidcredentials
                    )
                )
                .setMessage(
                    LabelSet.getText(
                        "Invalidcredentials",
                        R.string.Invalidcredentials
                    )
                )
                .setPositiveButton(android.R.string.ok) { _, _ -> }
                .setIcon(android.R.drawable.ic_dialog_alert).show()
            return 0
        }
        validate!!.SaveSharepreferenceString(
            AppSP.Roleid,
            Rolemap.get(spin_role.selectedItemPosition).toString()
        )
        CDFIApplication.changedb()
        userViewModel = ViewModelProviders.of(this).get(UserViewmodel::class.java)

        if (userViewModel!!.getUserCount() > 0 && userViewModel!!.getUserCountByUserId(
                etUsername.text.toString().toUpperCase()) == 0
        ) {
            AlertDialog.Builder(this)
                .setTitle(
                    LabelSet.getText(
                        "Invalidcredentials",
                        R.string.Invalidcredentials
                    )
                )
                .setMessage(
                    LabelSet.getText(
                        "Invalidcredentials",
                        R.string.Invalidcredentials
                    )
                )
                .setPositiveButton(android.R.string.ok) { _, _ -> }
                .setIcon(android.R.drawable.ic_dialog_alert).show()
            return 0
        }
        return 1
    }

    fun getrole() {

        Rolemap.put(0, "0")
        Rolemap.put(1, "310")
        Rolemap.put(2, "410")
        Rolemap.put(3, "450")
        Rolemap.put(4, "510")


    }

    private fun showAlert(iCheck: Int) {
        if (iCheck == 2 || iCheck == 3) {
            AlertDialog.Builder(this)
                .setTitle(
                    LabelSet.getText(
                        "Invalidcredentials",
                        R.string.Invalidcredentials
                    )
                )
                .setMessage(
                    LabelSet.getText(
                        "Invalidcredentials",
                        R.string.Invalidcredentials
                    )
                )
                .setPositiveButton(android.R.string.ok) { _, _ -> }
                .setIcon(android.R.drawable.ic_dialog_alert).show()
        } else if (iCheck == 4) {
            AlertDialog.Builder(this)
                .setTitle(
                    LabelSet.getText(
                        "Invalidcredentials",
                        R.string.Invalidcredentials
                    )
                )
                .setMessage(
                    LabelSet.getText(
                        "Invalidcredentials",
                        R.string.Invalidcredentials
                    )
                )
                .setPositiveButton(android.R.string.ok) { _, _ -> }
                .setIcon(android.R.drawable.ic_dialog_alert).show()
        } else if (iCheck == 5) {
            validate!!.CustomAlertSpinner(
                this,
                spin_role,
                LabelSet.getText("selectrole", R.string.selectrole)
            )
        }
    }

    private fun isNetworkConnected(): Boolean {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager //1
        val networkInfo = connectivityManager.activeNetworkInfo //2
        return networkInfo != null && networkInfo.isConnected //3
    }

    fun hasPermissions(context: Context?, vararg permissions: String): Boolean {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
            && context != null && permissions != null
        ) {
            for (permission in permissions) {
                if (ActivityCompat.checkSelfPermission(
                        context,
                        permission
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    return false
                }
            }
        }
        return true
    }

    fun getVersion(): String {
        var mVersionNumber: String
        val mContext = applicationContext
        try {
            val pkg = mContext.packageName
            mVersionNumber = mContext.packageManager
                .getPackageInfo(pkg, 0).versionName
        } catch (e: PackageManager.NameNotFoundException) {
            mVersionNumber = "?"
        }

        return LabelSet.getText("version", R.string.version) + ": " + mVersionNumber
    }

    fun setLabelText() {

        lokos_core.text = LabelSet.getText(
            "lokos_core",
            R.string.lokos_core
        )
        tv_cbomobiletext.text = LabelSet.getText(
            "cbo_mobile_application",
            R.string.cbo_mobile_application
        )
        tv_enterusernpass.text = LabelSet.getText(
            "enter_username_and_password",
            R.string.enter_username_and_password
        )
        etUsername.hint = LabelSet.getText(
            "user_id",
            R.string.user_id
        )
        etPassword.hint = LabelSet.getText(
            "password",
            R.string.password
        )
        //etPassword.setText(LabelSet.getText("chk_showHide",R.string.show_password))
        btn_login.text = LabelSet.getText(
            "log_in",
            R.string.log_in
        )
        tv_forgot.text = LabelSet.getText(
            "forgot_password",
            R.string.forgot_password
        )
    }
}
