package com.microware.cdfi.activity

import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.navigation.NavigationView
import com.microware.cdfi.R
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.fragment.DashboardFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.SHGViewmodel
import com.microware.cdfi.viewModel.UserViewmodel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main_drawer.*
import kotlinx.android.synthetic.main.app_bar_home_drawer.*
import kotlinx.android.synthetic.main.customealertchangerolel.view.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.nav_header_drawer.view.*


class MainActivityDrawer : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    var validate: Validate? = null
    var userViewmodel: UserViewmodel? = null
    var shgViewmodel: SHGViewmodel? = null
    var lastdate: String? = ""
    var Rolemap = HashMap<Int, String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_drawer)
        setSupportActionBar(toolbar)
        validate = Validate(this)
        userViewmodel = ViewModelProviders.of(this).get(UserViewmodel::class.java)
        shgViewmodel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        getrole()
        val toggle = ActionBarDrawerToggle(
            this,
            drawer_layout,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )

        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        val navigationView = findViewById<View>(R.id.nav_view) as NavigationView
        val hView = navigationView.getHeaderView(0)
        val lView = navigationView.menu

        var list = userViewmodel!!.getUserData()



        nav_view.setNavigationItemSelectedListener(this)
//        nav_view.itemIconTintList = nulldate
        replaceFragmenty(
            fragment = DashboardFragment(),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )
        if (validate!!.RetriveSharepreferenceString(AppSP.Langaugecode).equals("en")) {
            btnEnglish.background = resources.getDrawable(R.drawable.border)
            btnHindi.setBackgroundColor(Color.TRANSPARENT)
            btnHindi.text = validate!!.RetriveSharepreferenceString(AppSP.SelectedLanguage)

        } else {
            btnHindi.background = resources.getDrawable(R.drawable.border)
            btnEnglish.setBackgroundColor(Color.TRANSPARENT)
            btnHindi.text = validate!!.RetriveSharepreferenceString(AppSP.SelectedLanguage)


        }
        val version = navigationView.findViewById<View>(R.id.tvVersion) as TextView
        version.text = getVersion()
        if (!list.isNullOrEmpty()) {
            tvtext.text = validate!!.returnStringValue(list.get(0).userName)
            hView.tvname.text = validate!!.returnStringValue(list.get(0).userName)
            hView.tvmob.text = list.get(0).mobileNo
        }



        btnEnglish.setOnClickListener {


            validate!!.SaveSharepreferenceString(AppSP.Langaugecode, "en")
            //  validate!!.changeLang("en", this)
            btnEnglish.background = resources.getDrawable(R.drawable.border)
            btnHindi.setBackgroundColor(Color.TRANSPARENT)


            replaceFragmenty(
                fragment = DashboardFragment(),
                allowStateLoss = true,
                containerViewId = R.id.mainContent
            )
            lView.findItem(R.id.nav_lastsync).title = LabelSet.getText(
                "last_sync",
                R.string.last_sync
            ) + " " + lastdate
            lView.findItem(R.id.nav_home).title = LabelSet.getText(
                "menu_home",
                R.string.menu_home
            )
            lView.findItem(R.id.nav_profile).title = LabelSet.getText(
                "new_shg_profile",
                R.string.new_shg_profile
            )

            lView.findItem(R.id.nav_password).title = LabelSet.getText(
                "change_password",
                R.string.change_password
            )
            lView.findItem(R.id.nav_village).title = LabelSet.getText(
                "set_village_gp",
                R.string.set_village_gp
            )
            lView.findItem(R.id.nav_language).title = LabelSet.getText(
                "change_language",
                R.string.change_language
            )
            lView.findItem(R.id.nav_logout).title = LabelSet.getText(
                "logout",
                R.string.logout
            )
            lView.findItem(R.id.nav_changeuser).title = LabelSet.getText("change_role", R.string.change_role)
        }
        btnHindi.setOnClickListener {
            validate!!.SaveSharepreferenceString(
                AppSP.Langaugecode,
                validate!!.RetriveSharepreferenceString(AppSP.SelectedLanguageCode)!!
            )
            // validate!!.changeLang("hi", this)
            btnHindi.background = resources.getDrawable(R.drawable.border)
            btnEnglish.setBackgroundColor(Color.TRANSPARENT)
            replaceFragmenty(
                fragment = DashboardFragment(),
                allowStateLoss = true,
                containerViewId = R.id.mainContent
            )
            lView.findItem(R.id.nav_lastsync).title = LabelSet.getText(
                "last_sync",
                R.string.last_sync
            ) + " " + lastdate
            lView.findItem(R.id.nav_home).title = LabelSet.getText(
                "menu_home",
                R.string.menu_home
            )
            lView.findItem(R.id.nav_profile).title = LabelSet.getText(
                "new_shg_profile",
                R.string.new_shg_profile
            )

            lView.findItem(R.id.nav_password).title = LabelSet.getText(
                "change_password",
                R.string.change_password
            )
            lView.findItem(R.id.nav_village).title = LabelSet.getText(
                "set_village_gp",
                R.string.set_village_gp
            )
            lView.findItem(R.id.nav_language).title = LabelSet.getText(
                "change_language",
                R.string.change_language
            )
            lView.findItem(R.id.nav_logout).title = LabelSet.getText(
                "logout",
                R.string.logout
            )
            lView.findItem(R.id.nav_changeuser).title = LabelSet.getText("change_role", R.string.change_role)
        }
        if (shgViewmodel!!.getlastdate() > 0) {
            lastdate = validate!!.convertDatetime(shgViewmodel!!.getlastdate())
        } else {
            lastdate = ""
        }

        lView.findItem(R.id.nav_lastsync).title = LabelSet.getText(
            "last_sync",
            R.string.last_sync
        ) + " " + lastdate


        lView.findItem(R.id.nav_home).title = LabelSet.getText(
            "menu_home",
            R.string.menu_home
        )
        lView.findItem(R.id.nav_profile).title = LabelSet.getText(
            "new_shg_profile",
            R.string.new_shg_profile
        )

        lView.findItem(R.id.nav_password).title = LabelSet.getText(
            "change_password",
            R.string.change_password
        )
        lView.findItem(R.id.nav_village).title = LabelSet.getText(
            "set_village_gp",
            R.string.set_village_gp
        )
        lView.findItem(R.id.nav_language).title = LabelSet.getText(
            "change_language",
            R.string.change_language
        )
        lView.findItem(R.id.nav_logout).title = LabelSet.getText(
            "logout",
            R.string.logout
        )
        lView.findItem(R.id.nav_changeuser).title = LabelSet.getText("change_role", R.string.change_role)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_home -> {
                loadFragment(DashboardFragment())
            }
            R.id.nav_profile -> {
                validate!!.SaveSharepreferenceString(AppSP.SHGGUID, "")
                validate!!.SaveSharepreferenceString(AppSP.ShgName, "")
                validate!!.SaveSharepreferenceString(AppSP.Shgcode, "")
                val i = Intent(this, BasicDetailActivity::class.java)
                i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(i)
                finish()
            }


            R.id.nav_village -> {
                val i = Intent(this, VillageSelection::class.java)
                i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(i)
                finish()
            }
            R.id.nav_language -> {
                val i = Intent(this, ChangeLanguageActivity::class.java)
                i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(i)
                finish()
            }
            R.id.nav_password -> {
                val i = Intent(this, Changepassword::class.java)
                i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(i)
                finish()
            }
            R.id.nav_logout -> {
                /*  val i = Intent(this, LoginActivity::class.java)
                  i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                  startActivity(i)
                  finish()*/

                Backup(this).backup(CDFIApplication.database!!.openHelper.databaseName)
                validate!!.restartApplication(this)
           //     validate!!.openApp(this)
            }
            R.id.nav_changeuser -> {
                CustomAlertChangerole()


            }

        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    fun getVersion(): String {
        var mVersionNumber: String
        val mContext = applicationContext
        try {
            val pkg = mContext.packageName
            mVersionNumber = mContext.packageManager
                .getPackageInfo(pkg, 0).versionName
        } catch (e: PackageManager.NameNotFoundException) {
            mVersionNumber = "?"
        }

        return LabelSet.getText("version", R.string.version) + ": " + mVersionNumber
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    fun loadFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.mainContent, fragment)
            .commit()
    }

    fun CustomAlertChangerole() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertchangerolel, null)
        val mBuilder = androidx.appcompat.app.AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.btn_changerole.setOnClickListener {
            if (mDialogView.spin_role.selectedItemPosition > 0) {
                validate!!.SaveSharepreferenceString(
                    AppSP.Roleid,
                    Rolemap.get(mDialogView.spin_role.selectedItemPosition).toString()
                )
                validate!!.SaveSharepreferenceString(
                    AppSP.RoleName,
                    mDialogView.spin_role.getItemAtPosition(mDialogView.spin_role.selectedItemPosition).toString()
                )
                mAlertDialog.dismiss()

                Backup(this).backup(CDFIApplication.database!!.openHelper.databaseName)
                validate!!.restartApplication(this)
           //     validate!!.openApp(this)
            } else {
                validate!!.CustomAlertSpinner(
                    this,
                    mDialogView.spin_role,
                    LabelSet.getText("selectrole", R.string.selectrole)
                )
            }


        }
        mDialogView.btn_cancel.setOnClickListener {
            mAlertDialog.dismiss()


        }
    }

    fun getrole() {

        Rolemap.put(0, "0")
        Rolemap.put(1, "310")
        Rolemap.put(2, "410")
        Rolemap.put(3, "450")
        Rolemap.put(4, "510")


    }
}