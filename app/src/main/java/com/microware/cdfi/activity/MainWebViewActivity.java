package com.microware.cdfi.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import com.microware.cdfi.R;
import com.microware.cdfi.api.AdharModels.Poa;
import com.microware.cdfi.api.AdharModels.Poi;
import com.microware.cdfi.api.AdharModels.SampleEntity;
import com.microware.cdfi.api.ApiInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class MainWebViewActivity extends AppCompatActivity {


    private WebView mWebView;
    private ProgressBar mProgressBar;
    private static final String TEST_PAGE_URL = "https://api.digitallocker.gov.in/public/oauth2/1/authorize?response_type=code&client_id=0CE58F4C&redirect_uri=103.248.60.151&state=postman01&dl_flow=signup";
    //    private static final String TEST_PAGE_URL = "https://www.google.com/";
    Button btnRetry;
    SwipeRefreshLayout finalMySwipeRefreshLayout1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_web_view);
        mWebView = findViewById(R.id.mainView);
        btnRetry = findViewById(R.id.btnRetry);
        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mWebView.setVisibility(View.VISIBLE);

                mWebView.loadUrl(TEST_PAGE_URL);
            }
        });


        //  getAadharData("7e373049b140cc7b86221d61acee5bb79595e736");


        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl(TEST_PAGE_URL);

        //SwipeRefreshLayout

        finalMySwipeRefreshLayout1 = findViewById(R.id.swiperefresh);
        finalMySwipeRefreshLayout1.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // This method performs the actual data-refresh operation.
                // The method calls setRefreshing(false) when it's finished.
                mWebView.loadUrl(mWebView.getUrl());
//                Log.e("current url ", mWebView.getUrl());

            }
        });

        // Get the widgets reference from XML layout
        mProgressBar = findViewById(R.id.pb);
        mProgressBar.setVisibility(View.VISIBLE);
        mWebView.setWebViewClient(new MyWebViewClient());

        mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int newProgress) {
                // Update the progress bar with page loading progress
                mProgressBar.setProgress(newProgress);
                if (newProgress == 100) {
                    // Hide the progressbar
                    mProgressBar.setVisibility(View.GONE);
                }
            }
        });

// string url which you have to load into a web view


    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView webview, String url) {


            webview.loadUrl(url);
//            Log.e("url", url);
            if (url.contains("=") && url.contains("&") && url.contains("&state=postman01") && url.contains("/oauth2/1/103.248.60.151?code")) {
                String[] code = url.split("=")[1].split("&");
                String mainCode = code[0];
                 mWebView.setVisibility(View.GONE);
                getJsonData(mainCode);
            }

            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            mProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            finalMySwipeRefreshLayout1.setRefreshing(false);
            mProgressBar.setVisibility(View.GONE);
        }

        @Override
        public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {
            super.doUpdateVisitedHistory(view, url, isReload);
            String currentUrl = view.getUrl();
//            Log.e("current url ", currentUrl);

        }


    }

    ProgressDialog progressDialog;

    public void getJsonData(String maincode) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS);

        Retrofit retrofit = new Retrofit.Builder()
                .client(okHttpClient.build())
                .baseUrl("https://api.digitallocker.gov.in/public/")

                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();


        ApiInterface clientmm = retrofit.create(ApiInterface.class);


        Call<JsonObject> userdata = null;
        userdata = clientmm.getAuth(maincode, "authorization_code", "0CE58F4C", "9dd841e7cb7fea226b3e", "103.248.60.151");
        // userdata = deploymentInfo.getAuth(body);

        userdata.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                if (response != null && response.isSuccessful()) {
                    try {
//                        Log.e("OTP   ", response.body().toString());
//                        Log.e("OTP   ", "helloooooooooo");


                        JSONObject data = new JSONObject(response.body().toString());
//                        Log.e("OTPjjjjjjjjj   ", data.getString("access_token"));
                        getAadharData(data.getString("access_token"));


                    } catch (Exception e) {
                        e.printStackTrace();
//                        Log.e("SendingOTP Response", e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
            }
        });
    }

    public void getAadharData(final String token) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();

        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl("https://api.digitallocker.gov.in/public/")

                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();
        ApiInterface clientmm = retrofit.create(ApiInterface.class);
        Call<ResponseBody> userdata = null;
        userdata = clientmm.getAllData(token);

        userdata.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                if (response != null && response.isSuccessful()) {
                    try {
                        String data = response.body().source().readUtf8();
//                        Log.e("OTP   ", response.body().source().readByteString().toString());
                        JSONObject jsonObj = new JSONObject();
                        try {
                            StringReader sr = new StringReader(data);
                            XmlMapper xmlMapper = new XmlMapper();
                            Object poppy = xmlMapper.readValue(data, Object.class);
                            ObjectMapper mapper = new ObjectMapper();
                            String json = mapper.writeValueAsString(poppy);


                            Intent intent = new Intent();
                            intent.putExtra("adhardata", json);
                            setResult(201, intent);
                            finish();

                            // Map<String,String> aa=mapper.readValue(json,Map.class);

                            // Log.e("uid ", restaurantObject.getCertificateData().getUidData().getUid());
                            // tvUID.setText("UID  "+restaurantObject.getCertificateData().getUidData().getUid());
                            // Poi poi=restaurantObject.getCertificateData().getUidData().getPoi();
                            // SetInfo(poi);
                            // SetAddress(restaurantObject.getCertificateData().getUidData().getPoa());

                            // Log.e("JSON ", json.toString());
                        } catch (Exception e) {
//                            Log.e("JSON exception", e.getMessage());
                            e.printStackTrace();
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
//                        Log.e("SendingOTP Response", e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent intent = new Intent();
        intent.putExtra("adhardata", "");
        setResult(201, intent);
        finish();
    }


}