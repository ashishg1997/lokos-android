package com.microware.cdfi.activity

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.text.Html
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProviders
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.microware.cdfi.R
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.api.model.SHGMemberUploadModel
import com.microware.cdfi.api.model.SHGUploadModel
import com.microware.cdfi.api.response.ImageUploadResponse
import com.microware.cdfi.api.response.MastersResponse
import com.microware.cdfi.api.response.SHGResponse
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.*
import com.microware.cdfi.utility.*
import com.microware.cdfi.utility.MappingData.*
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_master_sync_new.*
import kotlinx.android.synthetic.main.customealertdialogelogin.*
import kotlinx.android.synthetic.main.dialoge.view.*
import kotlinx.android.synthetic.main.customealertdialogelogin.view.*
import kotlinx.android.synthetic.main.sync_toolbar.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.apache.http.conn.ConnectTimeoutException
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.net.SocketTimeoutException

class MasterSyncActivity : AppCompatActivity() {

    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: ApiInterface? = null
    var apiInterface1: ApiInterface? = null
    var validate: Validate? = null

    var cadreMemberViewModel: CadreMemberViewModel? = null
    var shgViewModel: SHGViewmodel? = null
    var cboPhoneViewmodel: CboPhoneViewmodel? = null
    var cboAddressViewmodel: CboAddressViewmodel? = null
    var cboBankViewmodel: CboBankViewmodel? = null
    var cboSystemtagViewmodel: SystemtagViewmodel? = null
    var memberviewmodel: Memberviewmodel? = null
    var memberAddressViewmodel: MemberAddressViewmodel? = null
    var memberBankViewmodel: MemberBankViewmodel? = null
    var memberKYCViewmodel: MemberKYCViewmodel? = null
    var memberPhoneViewmodel: MemberPhoneViewmodel? = null
    var memberSystemtagViewmodel: MemberSystemtagViewmodel? = null
    var imageUploadViewmodel: ImageUploadViewmodel? = null
    var memberDesignationViewmodel: MemberDesignationViewmodel? = null
    var transactionViewmodel: TransactionViewmodel? = null
    var responseViewModel: ResponseViewModel? = null

    var iDownload = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_master_sync_new)

        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        apiInterface1 = ApiClientConnection.Companion.instance.createApiInterface()

        validate = Validate(this)
        setLabelText()
        cadreMemberViewModel = ViewModelProviders.of(this).get(CadreMemberViewModel::class.java)
        shgViewModel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        cboPhoneViewmodel = ViewModelProviders.of(this).get(CboPhoneViewmodel::class.java)
        cboAddressViewmodel = ViewModelProviders.of(this).get(CboAddressViewmodel::class.java)
        cboBankViewmodel = ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        cboSystemtagViewmodel = ViewModelProviders.of(this).get(SystemtagViewmodel::class.java)
        memberviewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)
        memberAddressViewmodel = ViewModelProviders.of(this).get(MemberAddressViewmodel::class.java)
        memberBankViewmodel = ViewModelProviders.of(this).get(MemberBankViewmodel::class.java)
        memberKYCViewmodel = ViewModelProviders.of(this).get(MemberKYCViewmodel::class.java)
        memberPhoneViewmodel = ViewModelProviders.of(this).get(MemberPhoneViewmodel::class.java)
        responseViewModel = ViewModelProviders.of(this).get(ResponseViewModel::class.java)
        memberDesignationViewmodel =
            ViewModelProviders.of(this).get(MemberDesignationViewmodel::class.java)
        memberSystemtagViewmodel =
            ViewModelProviders.of(this).get(MemberSystemtagViewmodel::class.java)
        imageUploadViewmodel =
            ViewModelProviders.of(this).get(ImageUploadViewmodel::class.java)
        transactionViewmodel =
            ViewModelProviders.of(this).get(TransactionViewmodel::class.java)

        IvHome.setOnClickListener {
            val i = Intent(this, MainActivityDrawer::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)
            finish()
        }
        var shglist = shgViewModel!!.getPendingSHGlistdata()

        tvUploadData.text = Html.fromHtml(
            LabelSet.getText(
                "upload_data",
                R.string.upload_data
            ) + "\n" + LabelSet.getText(
                "SHGrecordtobeuploaded",
                R.string.SHGrecordtobeuploaded
            ) + " (" + shglist!!.size + ")"
        )


        tbl_download.setOnClickListener {
            layout_download.visibility = View.VISIBLE
            layout_upload.visibility = View.GONE
            linearUploadmsg.visibility = View.GONE
            tbl_download.background = getDrawable(R.drawable.light_shg_bg)
            tbl_upload.background = getDrawable(R.drawable.light_gray_bg)
            img_download.setImageResource(R.drawable.ic_white_download)
            img_upload.setImageResource(R.drawable.ic_gray)
            tv_download.setTextColor(getColor(R.color.white))
            tv_upload.setTextColor(getColor(R.color.black))

        }
        tbl_upload.setOnClickListener {
            layout_download.visibility = View.GONE
            linearUploadmsg.visibility = View.VISIBLE
            layout_upload.visibility = View.VISIBLE
            tbl_download.background = getDrawable(R.drawable.light_gray_bg)
            tbl_upload.background = getDrawable(R.drawable.light_shg_bg)
            img_download.setImageResource(R.drawable.ic_downloadgray)
            img_upload.setImageResource(R.drawable.ic_white)
            tv_download.setTextColor(getColor(R.color.black))
            tv_upload.setTextColor(getColor(R.color.white))

        }
        tbl_profileupload.setOnClickListener {
            if (isNetworkConnected()) {
                val shglist = shgViewModel!!.getPendingSHGlistdata()
                if (!shglist.isNullOrEmpty()) {
                    exportDatawithimage(shglist)
                    //exportData(shglist)
                } else {
                    val text = LabelSet.getText(
                        "nothing_upload",
                        R.string.nothing_upload
                    )
                    validate!!.CustomAlert(text, this@MasterSyncActivity)
                }
            } else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "no_internet_msg",
                        R.string.no_internet_msg
                    ), this
                )
            }
        }

        tbl_completedownload.setOnClickListener {
            if (isNetworkConnected()) {

                importSHGlist()
                //    importSHGStatus()
            } else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "no_internet_msg",
                        R.string.no_internet_msg
                    ), this
                )
            }
        }

        tbl_uploadstatus.setOnClickListener {
            if (isNetworkConnected()) {

                importuploadstatus()
            } else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "no_internet_msg",
                        R.string.no_internet_msg
                    ), this
                )
            }
        }
        tbl_approvalownload.setOnClickListener {
            if (isNetworkConnected()) {

                //       importSHGlist()
                importSHGStatus()
            } else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "no_internet_msg",
                        R.string.no_internet_msg
                    ), this
                )
            }
        }
        tbl_masterdownload.setOnClickListener {
            if (isNetworkConnected()) {

                importMastersVersion()

            } else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "no_internet_msg",
                        R.string.no_internet_msg
                    ), this
                )
            }
        }

    }

    private fun isNetworkConnected(): Boolean {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager //1
        val networkInfo = connectivityManager.activeNetworkInfo //2
        return networkInfo != null && networkInfo.isConnected //3
    }


    fun exportDatawithimage(shglist: List<SHGEntity>?) {
        progressDialog = ProgressDialog.show(
            this, LabelSet.getText("app_name", R.string.app_name),
            LabelSet.getText(
                "DataUploading",
                R.string.DataUploading
            )
        )
        progressDialog.setIcon(this.getDrawable(R.drawable.appicon))
        var idownload = 0
        var msg = ""
        var code = 0
        object : Thread() {

            //@SuppressLint("NewApi")
            override fun run() {
                try {


                    for (j in 0..shglist!!.size - 1) {
                        var transactionid = validate!!.randomtransactionno()
                        var shglistmodel = shglist.get(j)
                        var cbophone = cboPhoneViewmodel!!.getuploadlist(shglist.get(j).guid)
                        var cboaddress =
                            cboAddressViewmodel!!.getAddressDatalist(shglist.get(j).guid)
                        var cbobank = cboBankViewmodel!!.getBankdatalist(shglist.get(j).guid)
                        var cbosystemtag =
                            cboSystemtagViewmodel!!.getSystemtagdatalist(shglist.get(j).guid)
                        var shgDesignationList =
                            memberDesignationViewmodel!!.getMemberDesignationdetaillist(
                                shglist.get(j).guid
                            )

                        var memberlist =
                            memberviewmodel!!.getMemberuploadlist(shglist.get(j).guid)
                        var shgUploadModel: SHGUploadModel = SHGUploadModel()
//

                        shgUploadModel = returnObj(shglistmodel)
                        shgUploadModel.cboPhoneNoDetailsList = cbophone
                        shgUploadModel.cboAddressesList = cboaddress
                        shgUploadModel.cboBankDetailsList = cbobank
                        shgUploadModel.cboSystemTagsList = cbosystemtag
                        shgUploadModel.shgDesignationList = shgDesignationList
                        shgUploadModel.transaction_id = transactionid
                        var sHGMemberUploadModelList: List<SHGMemberUploadModel>? = null
                        sHGMemberUploadModelList =
                            returnSHGMemberUploadModelistObj(memberlist)

                        shgUploadModel.memberProfileList =
                            sHGMemberUploadModelList
                        for (i in 0..memberlist!!.size - 1) {
                            var memberaddresslist =
                                memberAddressViewmodel!!.ggetAddressdatalist(
                                    memberlist.get(
                                        i
                                    ).member_guid
                                )
                            var memberbanklist =
                                memberBankViewmodel!!.getBankdetaildatalist(
                                    memberlist.get(
                                        i
                                    ).member_guid
                                )
                            var memberkyclist =
                                memberKYCViewmodel!!.getKycdetaildatalist(memberlist.get(i).member_guid)
                            var memberphonelist =
                                memberPhoneViewmodel!!.getphoneDatalist(memberlist.get(i).member_guid)
                            var membersystetmtaglist =
                                memberSystemtagViewmodel!!.getSystemtagdatalist(
                                    memberlist.get(
                                        i
                                    ).member_guid
                                )
                            var memberCardrelist =
                                cadreMemberViewModel!!.getCadrePendingdata(
                                    memberlist.get(i).member_guid
                                )
                            var memaddress: SHGMemberUploadModel =
                                sHGMemberUploadModelList!!.get(i)
                            memaddress.memberAddressesList = memberaddresslist
                            memaddress.memberBankList = memberbanklist
                            memaddress.memberKYCDetailsList = memberkyclist
                            memaddress.memberPhoneNoDetailsList = memberphonelist
                            memaddress.memberSystemTagsList = membersystetmtaglist
                            memaddress.cadreShgMembersModeslList = memberCardrelist
                            memaddress.transaction_id = transactionid


                        }

                        var gsonbuilder = GsonBuilder()
                        gsonbuilder.serializeNulls()
                        val gson: Gson = gsonbuilder.setPrettyPrinting().create()
                        val json = gson.toJson(shgUploadModel)
                        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                        val token = validate!!.returnStringValue(
                            AESEncryption.decrypt(
                                validate!!.RetriveSharepreferenceString(AppSP.token),
                                validate!!.RetriveSharepreferenceString(AppSP.userid)))

                        val file =
                            RequestBody.create(MediaType.parse("application/json"), json)

                        var call: Call<ImageUploadResponse>? = null

                        val storageDir =
                            getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!.toString()
                        var arr: MultipartBody
                        var sFullImagePath = ""
                        val mediaStorageDir = File(
                            getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
                            AppSP.IMAGE_DIRECTORY_NAME
                        )

                        var imagefile1 = MultipartBody.Builder()
                            .addFormDataPart("shgProfile", "shgProfile", file)
                        var imageList =
                            imageUploadViewmodel!!.getimage_by_guid(shglist.get(j).guid)
                        for (k in 0..imageList!!.size - 1) {
                            if (imageList.get(k).image_name.length > 0) {
                                sFullImagePath =
                                    mediaStorageDir.path + File.separator + imageList.get(
                                        k
                                    ).image_name
                                val file1 = File(sFullImagePath)
                                if (file1.exists()) {
                                    val file3 =
                                        RequestBody.create(
                                            MediaType.parse("multipart/form-data"),
                                            file1
                                        )
                                    imagefile1.addFormDataPart("uploadFiles", file1.name, file3)
                                }
                            }
                        }
                        arr = imagefile1.build()

                        val call1 = apiInterface1!!.postUploadImageBody(
                            "multipart/form-data; boundary=" + arr.boundary(),
                            user,
                            token, arr
                        )
                        val res = call1.execute()

                        if (res!!.isSuccessful) {
                            if (res.body()?.msg.equals("Record Added To Queue")) {
                                try {
                                    CDFIApplication.database?.shgDao()
                                        ?.updateuploadStatus(
                                            shglist.get(j).guid, transactionid,
                                            validate!!.Daybetweentime(validate!!.currentdatetime)
                                        )
                                    transactionViewmodel!!.deleteTransactionByGuid(shglist.get(j).guid)
                                    var transaction = TransactionEntity(
                                        0, transactionid,
                                        shglist.get(j).guid,
                                        "",
                                        1, 0, 0, ""
                                    )
                                    if (shglist.get(j).is_edited == 1 && shglist.get(j).approve_status == 1) {
                                        transactionViewmodel!!.insert(transaction)
                                    }
                                    CDFIApplication.database?.cboBankDao()
                                        ?.updateBankDetailisedit(
                                            shglist.get(j).guid,
                                            validate!!.Daybetweentime(validate!!.currentdatetime)
                                        )
                                    CDFIApplication.database?.cboaddressDao()
                                        ?.updateisedit(
                                            shglist.get(j).guid,
                                            validate!!.Daybetweentime(validate!!.currentdatetime)
                                        )
                                    CDFIApplication.database?.cbophoneDao()
                                        ?.updateisedit(
                                            shglist.get(j).guid,
                                            validate!!.Daybetweentime(validate!!.currentdatetime)
                                        )
                                    CDFIApplication.database?.systemtagDao()
                                        ?.updateisedit(
                                            shglist.get(j).guid,
                                            validate!!.Daybetweentime(validate!!.currentdatetime)
                                        )
                                    CDFIApplication.database?.memberDesignationDao()
                                        ?.updateisedit(
                                            shglist.get(j).guid,
                                            validate!!.Daybetweentime(validate!!.currentdatetime)
                                        )
                                    msg = res.body()?.msg!!
                                    memberviewmodel!!.updateMemberuploadStatus(
                                        shglist.get(j).guid, transactionid,
                                        validate!!.Daybetweentime(validate!!.currentdatetime)
                                    )
                                    for (k in memberlist.indices) {
                                        CDFIApplication.database?.memberaddressDao()
                                            ?.updateisedit(
                                                memberlist.get(k).member_guid,
                                                validate!!.Daybetweentime(validate!!.currentdatetime)
                                            )
                                        CDFIApplication.database?.memberphoneDao()
                                            ?.updateisedit(
                                                memberlist.get(k).member_guid,
                                                validate!!.Daybetweentime(validate!!.currentdatetime)
                                            )
                                        CDFIApplication.database?.memberkycDao()
                                            ?.updateisedit(
                                                memberlist.get(k).member_guid,
                                                validate!!.Daybetweentime(validate!!.currentdatetime)
                                            )
                                        CDFIApplication.database?.memberbanckDao()
                                            ?.updateisedit(
                                                memberlist.get(k).member_guid,
                                                validate!!.Daybetweentime(validate!!.currentdatetime)
                                            )
                                        CDFIApplication.database?.memberSystemtagDao()
                                            ?.updateisedit(
                                                memberlist.get(k).member_guid,
                                                validate!!.Daybetweentime(validate!!.currentdatetime)
                                            )
                                        var kycimage1 = TransactionEntity(
                                            0, transactionid,
                                            shglist.get(j).guid,
                                            memberlist.get(k).member_guid,
                                            2, 0, 0, ""
                                        )
                                        transactionViewmodel!!.insert(kycimage1)

                                    }
                                    progressDialog.setMessage(
                                        LabelSet.getText(
                                            "DataUploading",
                                            R.string.DataUploading
                                        ) + " " + (j + 1) + "/" + shglist.size
                                    )
//                                    progressDialog.dismiss()
                                } catch (ex: Exception) {
                                    ex.printStackTrace()
//                                    progressDialog.dismiss()
                                }
                            }
                        } else {
                            msg = "" + res.code() + " " + res.message()
                            code = res.code()
                            idownload = 1
//                            val text = LabelSet.getText("lokos_core",R.string.lokos_core)R.string.data_saved_successfully)
//                            validate!!.CustomAlert( text,this@MasterSyncActivity)
                        }

                    }



                    runOnUiThread {
                        if (idownload == 0) {
                            progressDialog.dismiss()
                            val text = msg
                            validate!!.CustomAlert(text, this@MasterSyncActivity)
                            var shglist = shgViewModel!!.getPendingSHGlistdata()

                            tvUploadData.text = Html.fromHtml(
                                LabelSet.getText(
                                    "upload_data",
                                    R.string.upload_data
                                ) + "\n" + LabelSet.getText(
                                    "SHGrecordtobeuploaded",
                                    R.string.SHGrecordtobeuploaded
                                ) + " (" + shglist!!.size + ")"
                            )

                        } else if (idownload == -1) {
                            progressDialog.dismiss()
                            val text = LabelSet.getText(
                                "nothing_upload",
                                R.string.nothing_upload
                            )
                            validate!!.CustomAlert(text, this@MasterSyncActivity)
                            Toast.makeText(this@MasterSyncActivity, msg, Toast.LENGTH_LONG).show()
                        } else {
                            progressDialog.dismiss()
                            if (code == 403) {
                                Toast.makeText(this@MasterSyncActivity, msg, Toast.LENGTH_LONG)
                                    .show()
                                CustomAlertlogin()
                            } else {
                                validate!!.CustomAlert(msg, this@MasterSyncActivity)
                                Toast.makeText(this@MasterSyncActivity, msg, Toast.LENGTH_LONG)
                                    .show()
                            }
                        }

                    }

                } catch (bug: SocketTimeoutException) {
                    progressDialog.dismiss()
//                                Toast.makeText(activity!!, "Socket Timeout", Toast.LENGTH_LONG).show();
                } catch (bug: ConnectTimeoutException) {
                    progressDialog.dismiss()
                    // Toast.makeText(getApplicationContext(), "Connection Timeout", Toast.LENGTH_LONG).show();
                } catch (e: Exception) {
                    e.printStackTrace()
                    progressDialog.dismiss()
                }
            }

        }.start()
    }

    override fun onBackPressed() {
        var intent = Intent(this, MainActivityDrawer::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    fun importSHGlist() {
        progressDialog = ProgressDialog.show(
            this, LabelSet.getText("app_name", R.string.app_name),
            LabelSet.getText("DataLoading", R.string.DataLoading)
        )
        progressDialog.setIcon(this.getDrawable(R.drawable.appicon))
        var idownload = 0
        var msg = ""
        var code = 0
        object : Thread() {

            //@SuppressLint("NewApi")
            override fun run() {
                try {

                    val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                    val token = validate!!.returnStringValue(
                        AESEncryption.decrypt(
                            validate!!.RetriveSharepreferenceString(AppSP.token),
                            validate!!.RetriveSharepreferenceString(AppSP.userid)))

                    val call1 = apiInterface?.getShglist(
                        token,
                        user
                    )

                    val res1 = call1?.execute()

                    if (res1!!.isSuccessful) {
                        try {

                            msg = "" + res1.code() + " " + res1.message()

                            if (!res1.body()?.shgProfile!!.isNullOrEmpty()) {
                                CDFIApplication.database?.shgDao()
                                    ?.insertShg(res1.body()?.shgProfile)
                                var shglist = res1.body()?.shgProfile!!
                                for (i in shglist.indices) {
                                    if (isNetworkConnected()) {
                                        getshgdata(
                                            shglist.get(i).shg_id,
                                            progressDialog,
                                            LabelSet.getText(
                                                "DataLoading",
                                                R.string.DataLoading
                                            ) + " " + (i + 1) + "/" + shglist.size
                                        )
                                    } else {
                                        msg = LabelSet.getText(
                                            "no_internet_msg",
                                            R.string.no_internet_msg
                                        )
                                    }
                                    // progressDialog.setTitle(LabelSet.getText("lokos_core",R.string.lokos_core)R.string.DataLoading) + " "+ (i + 1) + "/" + shglist.size)
//
                                }
                            }

                            idownload = 1
                            if (idownload == 1) {
                                importIFSC()
                            }
                            progressDialog.dismiss()

                        } catch (ex: Exception) {
                            ex.printStackTrace()
//                                    progressDialog.dismiss()
                        }

                    } else {
                        var resMsg = ""
                        if (res1.code() == 403) {

                            code = res1.code()
                            msg = res1.message()
                        } else {
                            if (res1.errorBody()?.contentLength() == 0L || res1.errorBody()
                                    ?.contentLength()!! < 0L
                            ) {
                                code = res1.code()
                                resMsg = res1.message()
                            } else {
                                var jsonObject1 =
                                    JSONObject(res1.errorBody()!!.source().readUtf8().toString())

                                code =
                                    validate!!.returnIntegerValue(
                                        jsonObject1.optString("responseCode").toString()
                                    )
                                resMsg = validate!!.returnStringValue(
                                    jsonObject1.optString("responseMsg").toString()
                                )
                            }
                            msg = "" + validate!!.alertMsg(
                                this@MasterSyncActivity,
                                responseViewModel,
                                code,
                                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                                resMsg
                            )

                        }

                    }

                    runOnUiThread {
                        if (idownload == 1) {
                            progressDialog.dismiss()
                            val text = LabelSet.getText(
                                "Datadownloadedsuccessfully",
                                R.string.Datadownloadedsuccessfully
                            )
                            validate!!.CustomAlert(text, this@MasterSyncActivity)
                        } else {
                            progressDialog.dismiss()
                            if (code == 403) {
                                Toast.makeText(this@MasterSyncActivity, msg, Toast.LENGTH_LONG)
                                    .show()
                                CustomAlertlogin()
                            } else {
                                val text = msg
                                validate!!.CustomAlert(text, this@MasterSyncActivity)
                                Toast.makeText(this@MasterSyncActivity, msg, Toast.LENGTH_LONG)
                                    .show()
                            }
                        }

                    }

                } catch (bug: SocketTimeoutException) {
                    progressDialog.dismiss()
//                                Toast.makeText(activity!!, "Socket Timeout", Toast.LENGTH_LONG).show();
                } catch (bug: ConnectTimeoutException) {
                    progressDialog.dismiss()
                    // Toast.makeText(getApplicationContext(), "Connection Timeout", Toast.LENGTH_LONG).show();
                } catch (e: Exception) {
                    e.printStackTrace()
                    progressDialog.dismiss()
                }
            }

        }.start()
    }


    fun importSHGStatus() {
        progressDialog = ProgressDialog.show(
            this, LabelSet.getText("app_name", R.string.app_name),
            LabelSet.getText("DataLoading", R.string.DataLoading)
        )
        progressDialog.setIcon(this.getDrawable(R.drawable.appicon))
        var idownload = 0
        var code = 0
        var msg = ""
        object : Thread() {

            //@SuppressLint("NewApi")
            override fun run() {
                try {

                    val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                    val token = validate!!.returnStringValue(
                        AESEncryption.decrypt(
                            validate!!.RetriveSharepreferenceString(AppSP.token),
                            validate!!.RetriveSharepreferenceString(AppSP.userid))
                    )

                    val call = apiInterface?.getnonSyncShgsByActivationStatus(
                        token,
                        user
                    )

                    val res = call?.execute()

                    if (res!!.isSuccessful) {
                        if (!res.body()?.groupResponseStatus.isNullOrEmpty()) {
                            try {
                                for (i in 0 until res.body()?.groupResponseStatus!!.size) {
                                    shgViewModel!!.updateShgDedupStatus(
                                        res.body()?.groupResponseStatus?.get(i)?.guid!!,
                                        res.body()?.groupResponseStatus?.get(i)?.code!!,
                                        res.body()?.groupResponseStatus?.get(i)?.activationStatus!!,
                                        res.body()?.groupResponseStatus?.get(i)?.approve_status!!,
                                        res.body()?.groupResponseStatus?.get(i)?.checker_remarks!!
                                    )
                                    var shgbankList =
                                        res.body()?.groupResponseStatus!!.get(i)
                                            .bankDownloadStatus
                                    if (!shgbankList.isNullOrEmpty()) {
                                        for (l in shgbankList.indices) {
                                            cboBankViewmodel!!.updateactivotaionstatus(
                                                shgbankList.get(l).guid,
                                                shgbankList.get(l).activationStatus
                                            )
                                        }
                                    }
                                    var memberList = res.body()?.groupResponseStatus!!.get(i)
                                        .memberDownloadStatus
                                    if(!memberList.isNullOrEmpty()){
                                        for (j in 0 until memberList.size) {
                                            memberviewmodel!!.updateMemberDedupStatus(
                                                memberList.get(j).guid,
                                                memberList.get(j).code!!,
                                                memberList.get(j).activationStatus,
                                                memberList.get(j).approve_status,
                                                validate!!.returnStringValue(memberList.get(j).checker_remarks)
                                            )
                                            var memberkycList = res.body()?.groupResponseStatus!!.get(i)
                                                .memberDownloadStatus?.get(i)?.kycDownloadStatus
                                            var memberbankList =
                                                res.body()?.groupResponseStatus!!.get(i)
                                                    .memberDownloadStatus?.get(i)?.kycDownloadStatus
                                            if(!memberkycList.isNullOrEmpty()) {
                                                for (k in memberkycList.indices) {
                                                    memberKYCViewmodel!!.updateactivotaionstatus(
                                                        memberkycList.get(k).guid,
                                                        validate!!.returnIntegerValue(
                                                            memberkycList.get(
                                                                k
                                                            ).activationStatus.toString()
                                                        )
                                                    )
                                                }
                                            }
                                            if(!memberbankList.isNullOrEmpty()) {
                                                for (l in memberbankList.indices) {
                                                    memberBankViewmodel!!.updateactivotaionstatus(
                                                        memberbankList.get(l).guid,
                                                        validate!!.returnIntegerValue(memberbankList.get(l).activationStatus.toString())
                                                    )
                                                }
                                            }
                                        }
                                    }
                                }
//
                            } catch (ex: Exception) {
                                ex.printStackTrace()
//                                    progressDialog.dismiss()
                            }
                        } else {
                            idownload = 0
                        }
                    } else {
                        var resMsg = ""
                        idownload = 1
                        if (res.code() == 403) {

                            code = res.code()
                            msg = res.message()
                        } else {
                            if (res.errorBody()?.contentLength() == 0L || res.errorBody()
                                    ?.contentLength()!! < 0L
                            ) {
                                code = res.code()
                                resMsg = res.message()
                            } else {
                                var jsonObject1 =
                                    JSONObject(res.errorBody()!!.source().readUtf8().toString())

                                code =
                                    validate!!.returnIntegerValue(
                                        jsonObject1.optString("responseCode").toString()
                                    )
                                resMsg = validate!!.returnStringValue(
                                    jsonObject1.optString("responseMsg").toString()
                                )
                            }
                            msg = "" + validate!!.alertMsg(
                                this@MasterSyncActivity,
                                responseViewModel,
                                code,
                                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                                resMsg
                            )

                        }


                    }




                    runOnUiThread {
                        if (idownload == 0) {
                            progressDialog.dismiss()
                            val text = LabelSet.getText(
                                "Datadownloadedsuccessfully",
                                R.string.Datadownloadedsuccessfully
                            )
                            CustomAlert(text)
                        } else {
                            progressDialog.dismiss()
                            if (code == 403) {
                                Toast.makeText(this@MasterSyncActivity, msg, Toast.LENGTH_LONG)
                                    .show()
                                CustomAlertlogin()
                            } else {

                                validate!!.CustomAlert(msg, this@MasterSyncActivity)

                            }

                        }

                    }

                } catch (bug: SocketTimeoutException) {
                    progressDialog.dismiss()
//                                Toast.makeText(activity!!, "Socket Timeout", Toast.LENGTH_LONG).show();
                } catch (bug: ConnectTimeoutException) {
                    progressDialog.dismiss()
                    // Toast.makeText(getApplicationContext(), "Connection Timeout", Toast.LENGTH_LONG).show();
                } catch (e: Exception) {
                    e.printStackTrace()
                    progressDialog.dismiss()
                }
            }

        }.start()
    }

    fun importMastersVersion() {
        var grant_type = "password"
        var userId = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
        var token = validate!!.returnStringValue(
            AESEncryption.decrypt(
                validate!!.RetriveSharepreferenceString(AppSP.token),
                validate!!.RetriveSharepreferenceString(AppSP.userid)))
        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText("DataLoading", R.string.DataLoading)
        )
        try {

            try {

                val call =
                    apiInterface1?.getAllMasterDatabylanguagecode(
                        validate!!.RetriveSharepreferenceString(AppSP.SelectedLanguageCode),
                        token,
                        userId

                    )
                call?.enqueue(object : Callback<MastersResponse> {

                    //  call?.enqueue(object : Callback<List<MastersResponse>>, retrofit2.Callback<List<MastersResponse>> {
                    override fun onFailure(
                        call: Call<MastersResponse>,
                        t: Throwable
                    ) {
                        t.printStackTrace()
                        progressDialog.dismiss()
                    }

                    override fun onResponse(
                        call: Call<MastersResponse>,
                        response: Response<MastersResponse>
                    ) {
                        if (response.isSuccessful) {
                            when (response.code()) {
                                200 -> {

                                    if (response.body()?.mst_State!!.isNotEmpty()) {
                                        try {
                                            CDFIApplication.database?.stateDao()
                                                ?.deleteAllState()
                                            CDFIApplication.database?.districtDao()
                                                ?.deleteAllDistrict()
                                            CDFIApplication.database?.blockDao()
                                                ?.deleteAllBlocks()
                                            CDFIApplication.database?.panchayatDao()
                                                ?.deleteAllPanchayat()
                                            CDFIApplication.database?.villageDao()
                                                ?.deleteAllVillage()
                                            CDFIApplication.database?.userDao()
                                                ?.delete()
                                            CDFIApplication.database?.lookupdao()
                                                ?.delete()
                                            CDFIApplication.database?.responseCodeDao()
                                                ?.deleteResponse()
                                            CDFIApplication.database?.subcommitteeMasterDao()
                                                ?.deleteScMasterdata()
                                            CDFIApplication.database?.fundingDao()
                                                ?.deleteAllAgency()
                                            CDFIApplication.database?.mstProductDao()
                                                ?.deleteProducts()
                                            CDFIApplication.database?.fundsDao()
                                                ?.deleteFundType()
                                            CDFIApplication.database?.mstCOADao()
                                                ?.deleteAll()
                                            CDFIApplication.database?.cardreCateogaryDao()
                                                ?.deleteAllCateogary()
                                            CDFIApplication.database?.labelMasterDao()
                                                ?.deleteAllLabel()
                                            CDFIApplication.database?.cardreRoleDao()
                                                ?.deleteAllCardreRole()

                                            CDFIApplication.database?.stateDao()
                                                ?.insertState(
                                                    response.body()?.mst_State
                                                )
                                            CDFIApplication.database?.districtDao()
                                                ?.insertDistrict(
                                                    response.body()?.mst_District
                                                )
                                            CDFIApplication.database?.blockDao()
                                                ?.insertAllBlock(
                                                    response.body()?.mst_Block
                                                )
                                            CDFIApplication.database?.panchayatDao()
                                                ?.insertAllPanchayat(
                                                    response.body()?.mstPanchayat
                                                )
                                            if (!response.body()?.mstVillage.isNullOrEmpty()) {
                                                CDFIApplication.database?.villageDao()
                                                    ?.insertAllVillage(
                                                        response.body()?.mstVillage
                                                    )
                                            }
                                            CDFIApplication.database?.responseCodeDao()
                                                ?.insertAllResponse(
                                                    response.body()?.mstResponse
                                                )

                                            CDFIApplication.database?.laonProductDao()
                                                ?.insertproductlist(
                                                    response.body()?.mstLoanProduct
                                                )

                                            CDFIApplication.database?.fundsDao()
                                                ?.insertFundslist(
                                                    response.body()?.mstFundType
                                                )
                                            CDFIApplication.database?.mstCOADao()
                                                ?.insertMstCOAAllData(
                                                    response.body()?.mstCoa
                                                )

                                            CDFIApplication.database?.lookupdao()
                                                ?.insertlookup(
                                                    response.body()?.mstlookup
                                                )
                                            CDFIApplication.database?.subcommitteeMasterDao()
                                                ?.insertScMasterdata(
                                                    response.body()?.subCommitteeMasterList
                                                )

                                            if (!response.body()?.mstbankBranch.isNullOrEmpty()) {
                                                CDFIApplication.database?.masterbankbranchDao()
                                                    ?.insertbranch(
                                                        response.body()?.mstbankBranch
                                                    )
                                            }

                                            if (!response.body()?.mstBankMaster.isNullOrEmpty()) {
                                                CDFIApplication.database?.masterBankDao()
                                                    ?.insertbank(
                                                        response.body()?.mstBankMaster
                                                    )
                                            }

                                            if (!response.body()?.mstlabelMasterList.isNullOrEmpty()) {
                                                CDFIApplication.database?.labelMasterDao()
                                                    ?.insertAllLabel(
                                                        response.body()?.mstlabelMasterList
                                                    )
                                            }

                                            if (!response.body()?.cadreCategoryMasterList.isNullOrEmpty()) {
                                                CDFIApplication.database?.cardreCateogaryDao()
                                                    ?.insertAllCateogary(
                                                        response.body()?.cadreCategoryMasterList
                                                    )
                                            }

                                            if (!response.body()?.cadreRoleMasterlist.isNullOrEmpty()) {
                                                CDFIApplication.database?.cardreRoleDao()
                                                    ?.insertAllCardreRole(
                                                        response.body()?.cadreRoleMasterlist
                                                    )
                                            }
                                            if (!response.body()?.mstFundingMaster.isNullOrEmpty()) {
                                                CDFIApplication.database?.fundingDao()
                                                    ?.insertAllAgency(
                                                        response.body()?.mstFundingMaster
                                                    )
                                            }

                                            var list =
                                                response.body()?.mstuser!!
                                            var userlist =
                                                list

                                            if (!userlist.isNullOrEmpty()) {
                                                var userEntity: UserEntity? =
                                                    null
                                                userEntity = UserEntity(
                                                    userlist.get(0).userId,
                                                    userlist.get(0).userName,
                                                    userlist.get(0).designation,
                                                    userlist.get(0).emailId,
                                                    userlist.get(0).mobileNo
                                                )
                                                validate!!.SaveSharepreferenceString(
                                                    AppSP.userid,
                                                    userlist.get(0).userId
                                                )

                                                CDFIApplication.database?.userDao()
                                                    ?.insertuser(
                                                        userEntity
                                                    )
                                            }
//
                                            var userrole =
                                                response.body()?.mstuser!!.get(
                                                    0
                                                ).userRoleRightsMap
                                            var userrolemaster =
                                                response.body()?.mstuser!!.get(
                                                    0
                                                ).userRoleRightsMap!!.roleMaster
                                            if (userrolemaster != null) {
                                                var roleEntity: RoleEntity? =
                                                    null
                                                roleEntity = RoleEntity(
                                                    userrolemaster.roleId,
                                                    userrolemaster.roleName,
                                                    userrolemaster.status,
                                                    userrolemaster.categoryId,
                                                    userrolemaster.levelId,
                                                    userrolemaster.typeId
                                                )

                                                CDFIApplication.database?.roledao()
                                                    ?.insertrole(
                                                        roleEntity
                                                    )
                                            }
                                            if (userrole != null) {

                                                validate!!.SaveSharepreferenceInt(
                                                    AppSP.statecode,
                                                    userrole.stateId
                                                )

                                                validate!!.SaveSharepreferenceInt(
                                                    AppSP.districtcode,
                                                    userrole.districtId
                                                )

                                                validate!!.SaveSharepreferenceInt(
                                                    AppSP.blockcode,
                                                    userrole.blockId
                                                )


                                                if (userrole.panchayatId.contains(",")) {

                                                    var panchayatid =
                                                        userrole.panchayatId.split(
                                                            ","
                                                        )
                                                    validate!!.SaveSharepreferenceInt(
                                                        AppSP.panchayatcode,
                                                        validate!!.returnIntegerValue(
                                                            panchayatid[0]
                                                        )
                                                    )
                                                } else {
                                                    validate!!.SaveSharepreferenceInt(
                                                        AppSP.panchayatcode,
                                                        validate!!.returnIntegerValue(
                                                            userrole.panchayatId
                                                        )
                                                    )
                                                }

                                                if (!userrole.villageId.isNullOrEmpty()) {
                                                    if (userrole.villageId!!.contains(
                                                            ","
                                                        )
                                                    ) {

                                                        var villageid =
                                                            userrole.villageId!!.split(
                                                                ","
                                                            )
                                                        validate!!.SaveSharepreferenceInt(
                                                            AppSP.villagecode,
                                                            validate!!.returnIntegerValue(
                                                                villageid[0]
                                                            )
                                                        )
                                                    } else {
                                                        validate!!.SaveSharepreferenceInt(
                                                            AppSP.villagecode,
                                                            validate!!.returnIntegerValue(
                                                                userrole.villageId
                                                            )
                                                        )
                                                    }
                                                }

                                            }

                                            iDownload = 1
                                            progressDialog.dismiss()
                                            if (iDownload == 1) {
                                                Toast.makeText(
                                                    this@MasterSyncActivity,
                                                    LabelSet.getText(
                                                        "Datadownloadedsuccessfully",
                                                        R.string.Datadownloadedsuccessfully
                                                    ),
                                                    Toast.LENGTH_LONG
                                                ).show()
                                            }
                                        } catch (ex: Exception) {
                                            ex.printStackTrace()
                                            progressDialog.dismiss()
                                        }
                                    }

                                }

                            }

                        } else {
                            iDownload = 0
                            progressDialog.dismiss()
                            var resCode = 0
                            var resMsg = ""
                            when (response.code()) {
                                403 ->
                                    CustomAlertlogin()
                                else -> {
                                    if (response.errorBody()
                                            ?.contentLength() == 0L || response.errorBody()
                                            ?.contentLength()!! < 0L
                                    ) {
                                        resCode = response.code()
                                        resMsg = response.message()
                                    } else {
                                        var jsonObject1 =
                                            JSONObject(
                                                response.errorBody()!!.source().readUtf8()
                                                    .toString()
                                            )

                                        resCode =
                                            validate!!.returnIntegerValue(
                                                jsonObject1.optString("responseCode").toString()
                                            )
                                        resMsg = validate!!.returnStringValue(
                                            jsonObject1.optString("responseMsg").toString()
                                        )
                                    }
                                    var msg = "" + validate!!.alertMsg(
                                        this@MasterSyncActivity,
                                        responseViewModel,
                                        resCode,
                                        validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                                        resMsg
                                    )
                                    Toast.makeText(
                                        this@MasterSyncActivity,
                                        msg,
                                        Toast.LENGTH_LONG
                                    ).show()
                                }
                            }
                        }
                    }

                })
            } catch (ex: Exception) {
                ex.printStackTrace()
            }


        } catch (e: JSONException) {
            e.printStackTrace()
            progressDialog.dismiss()
        }





        fun Context.toast(message: String) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        }


    }

    fun importuploadstatus() {

        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText("DataLoading", R.string.DataLoading)
        )

        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
        val token = validate!!.returnStringValue(
            AESEncryption.decrypt(
                validate!!.RetriveSharepreferenceString(AppSP.token),
                validate!!.RetriveSharepreferenceString(AppSP.userid)
            )
        )
        val callCount = apiInterface?.getdownloadTransactionStatus(
            token,
            user
        )

        callCount?.enqueue(object : Callback<SHGResponse> {
            override fun onFailure(callCount: Call<SHGResponse>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<SHGResponse>,
                response: Response<SHGResponse>
            ) {

                progressDialog.dismiss()
                if (response.isSuccessful) {
                    if (!response.body()?.grouptransactionResponse.isNullOrEmpty()) {
                        try {
                            var list = response.body()?.grouptransactionResponse

                            for (i in list!!.indices) {
                                if (list.get(i).status == 3) {
                                    CDFIApplication.database?.shgDao()
                                        ?.updatetransactionstatus(
                                            list.get(i).transaction_id
                                        )
                                    CDFIApplication.database?.memberDao()
                                        ?.updatemembertransactionstatus(
                                            list.get(i).transaction_id
                                        )
                                }
                                CDFIApplication.database?.transactionDao()
                                    ?.updatetransactionstatus(
                                        list.get(i).transaction_id,
                                        list.get(i).status,
                                        list.get(i).remarks
                                    )
                            }
                            val text = LabelSet.getText(
                                "Datadownloadedsuccessfully",
                                R.string.Datadownloadedsuccessfully
                            )
                            CustomAlert(text)

                        } catch (ex: Exception) {
                            ex.printStackTrace()
                            //                                    progressDialog.dismiss()
                        }
                    }
                } else {
                    progressDialog.dismiss()
                    var resCode = 0
                    var resMsg = ""
                    if (response.code() == 403) {

                        CustomAlertlogin()
                    } else {
                        if (response.errorBody()?.contentLength() == 0L || response.errorBody()
                                ?.contentLength()!! < 0L
                        ) {
                            resCode = response.code()
                            resMsg = response.message()
                        } else {
                            var jsonObject1 =
                                JSONObject(response.errorBody()!!.source().readUtf8().toString())

                            resCode =
                                validate!!.returnIntegerValue(
                                    jsonObject1.optString("responseCode").toString()
                                )
                            resMsg = validate!!.returnStringValue(
                                jsonObject1.optString("responseMsg").toString()
                            )
                        }

                        validate!!.CustomAlertMsg(
                            this@MasterSyncActivity,
                            responseViewModel,
                            resCode,
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!, resMsg
                        )

                    }

                }


            }

        })
    }

    fun importtoken(user: String, pass: String) {
        var grant_type = "password"
        var userId = user
        var password = pass
        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText(
                "authenticate_user",
                R.string.authenticate_user
            )
        )


        val callCount = apiInterface?.gettokenforce(
            grant_type,
            userId,
            password, validate!!.RetriveSharepreferenceInt(AppSP.State_Selected), 1,
            validate!!.RetriveSharepreferenceString(AppSP.Roleid)!!
        )
        callCount?.enqueue(object : Callback<JsonObject> {
            override fun onFailure(callCount: Call<JsonObject>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                progressDialog.dismiss()


                if (response.isSuccessful) {

                    var jsonObject1: JSONObject? = null
                    try {
                        jsonObject1 = JSONObject(response.body().toString())

                        val auth = jsonObject1.get("accessToken").toString()
                        if (auth.trim().length > 0) {
                            validate!!.SaveSharepreferenceString(
                                AppSP.token,
                                validate!!.returnStringValue(
                                    AESEncryption.encrypt(
                                        auth,validate!!.RetriveSharepreferenceString(AppSP.userid))))
                        }
                        validate!!.CustomAlert(
                            LabelSet.getText(
                                "userauthenticatedsuccessfully",
                                R.string.userauthenticatedsuccessfully
                            ), this@MasterSyncActivity
                        )
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        var jsonObject1 =
                            JSONObject(response.errorBody()!!.source().readUtf8().toString())

                        var resCode =
                            validate!!.returnIntegerValue(
                                jsonObject1.optString("responseCode").toString()
                            )
                        validate!!.CustomAlertMsg(
                            this@MasterSyncActivity,
                            responseViewModel,
                            resCode,
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                            validate!!.returnStringValue(
                                jsonObject1.optString("responseMsg").toString()
                            )
                        )
                    }

                } else {
                    var jsonObject1 =
                        JSONObject(response.errorBody()!!.source().readUtf8().toString())

                    var resCode =
                        validate!!.returnIntegerValue(
                            jsonObject1.optString("responseCode").toString()
                        )
                    validate!!.CustomAlertMsg(
                        this@MasterSyncActivity,
                        responseViewModel,
                        resCode,
                        validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                        validate!!.returnStringValue(
                            jsonObject1.optString("responseMsg").toString()
                        )
                    )
                }


            }

        })


    }

    fun CustomAlertlogin() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogelogin, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mDialogView.etUsername.setText(validate!!.RetriveSharepreferenceString(AppSP.userid))
        mDialogView.etPassword.setText(
            validate!!.returnStringValue(
                AESEncryption.decrypt(
                    validate!!.RetriveSharepreferenceString(AppSP.Password),
                    validate!!.RetriveSharepreferenceString(AppSP.userid)
                )
            )
        )
        mDialogView.txt_dialog_title2.text = LabelSet.getText("app_name",R.string.app_name)
        mDialogView.btn_login.text = LabelSet.getText("log_in",R.string.log_in)
        mDialogView.chk_showHide1.text = LabelSet.getText("show_password",R.string.show_password)
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.btn_login.setOnClickListener {
            mAlertDialog.dismiss()
            importtoken(
                mDialogView.etUsername.text.toString(),
                validate!!.returnStringValue(
                    AESEncryption.encrypt(validate!!.returnStringValue(
                        mDialogView.etPassword.text.toString()),mDialogView.etUsername.text.toString()))
            )
        }
        mAlertDialog.chk_showHide1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                mAlertDialog.etPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                mAlertDialog.etPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
        }


    }

    fun getshgdata(shgid: Long, progressDialog: ProgressDialog, str: String) {

        val userId = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
        val token = validate!!.returnStringValue(
            AESEncryption.decrypt(
                validate!!.RetriveSharepreferenceString(AppSP.token),
                validate!!.RetriveSharepreferenceString(AppSP.userid)
            )
        )

        val call = apiInterface?.getSHGData(
            "application/json", token, userId,
            shgid
        )
        var response = call!!.execute()
        if (response.isSuccessful) {
            when (response.code()) {
                200 -> {

                    if (response.body()?.shgUploadModel != null) {
                        try {
                            var shg = response.body()?.shgUploadModel
                            var shgaddress = shg!!.cboAddressesList
                            var shgbank = shg.cboBankDetailsList
                            var shgphone = shg.cboPhoneNoDetailsList
                            var shgDesignationList = shg.shgDesignationList
                            var cboSystemTagsList = shg.cboSystemTagsList
                            var shglist = returnObjentity(shg)
                            var membermodellist = shg.memberProfileList
                            var memberlist = returnmemberentity(membermodellist)
                            if (shglist != null) {
                                CDFIApplication.database?.shgDao()
                                    ?.insertShgData(shglist)
                            }
                            if (!shgaddress.isNullOrEmpty()) {
                                CDFIApplication.database?.cboaddressDao()
                                    ?.insertAddressDetail(
                                        shgaddress
                                    )
                            }

                            if (!shgphone.isNullOrEmpty()) {
                                CDFIApplication.database?.cbophoneDao()
                                    ?.insertPhoneDetail(
                                        shgphone
                                    )
                            }
                            if (!shgbank.isNullOrEmpty()) {
                                CDFIApplication.database?.cboBankDao()
                                    ?.insertBankDetail(
                                        shgbank
                                    )
                            }
                            if (!cboSystemTagsList.isNullOrEmpty()) {
                                CDFIApplication.database?.systemtagDao()
                                    ?.insertSystemtagDetail(
                                        cboSystemTagsList
                                    )
                            }
                            if (!shgDesignationList.isNullOrEmpty()) {
                                CDFIApplication.database?.memberDesignationDao()
                                    ?.insertMemberDesignationList(
                                        shgDesignationList
                                    )
                            }

                            if (!memberlist.isNullOrEmpty()) {
                                CDFIApplication.database?.memberDao()
                                    ?.insertMember(memberlist)
                            }


                            for (i in membermodellist!!.indices) {
                                var memberbanklist = membermodellist.get(i).memberBankList
                                var memberaddresslist =
                                    membermodellist.get(i).memberAddressesList
                                var memberkyclist = membermodellist.get(i).memberKYCDetailsList
                                var memberphonelist =
                                    membermodellist.get(i).memberPhoneNoDetailsList
                                var membertaglist =
                                    membermodellist.get(i).memberSystemTagsList
                                var cadreShgMemberlist =
                                    membermodellist.get(i).cadreShgMembersModeslList
                                if (!cadreShgMemberlist.isNullOrEmpty()) {
                                    CDFIApplication.database?.caderMemberDao()
                                        ?.insertCadre(cadreShgMemberlist)
                                }
                                if (!memberaddresslist.isNullOrEmpty()) {
                                    CDFIApplication.database?.memberaddressDao()
                                        ?.insertAddressDetail(
                                            memberaddresslist
                                        )
                                }

                                if (!memberbanklist.isNullOrEmpty()) {
                                    CDFIApplication.database?.memberbanckDao()
                                        ?.insertBankDetail(
                                            memberbanklist
                                        )
                                }

                                if (!memberkyclist.isNullOrEmpty()) {
                                    CDFIApplication.database?.memberkycDao()
                                        ?.insertKycDetail(
                                            memberkyclist
                                        )
                                }

                                if (!memberphonelist.isNullOrEmpty()) {
                                    CDFIApplication.database?.memberphoneDao()
                                        ?.insertPhoneDetail(
                                            memberphonelist
                                        )
                                }

                                if (!membertaglist.isNullOrEmpty()) {
                                    CDFIApplication.database?.memberSystemtagDao()
                                        ?.insertSystemtagDetail(
                                            membertaglist
                                        )
                                }
                            }

                            progressDialog.setMessage(str)
//
                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }
                    }

                }

            }

        } else {
            // iDownload = 0

        } // if
    }


    fun CustomAlert(str: String) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialoge, null)
        val mBuilder = android.app.AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str
        mDialogView.btn_ok.setOnClickListener {


            mAlertDialog.dismiss()


        }
    }

    fun setLabelText() {
        tv_title.text = LabelSet.getText(
            "profile_sync",
            R.string.profile_sync
        )
        tvUploadData.text = LabelSet.getText(
            "upload_data",
            R.string.upload_data
        )
        tv_uploadstatus.text = LabelSet.getText(
            "upload_stauts",
            R.string.upload_stauts
        )
        tv_approvalstatus.text = LabelSet.getText(
            "approval_status",
            R.string.approval_status
        )
        tv_completestatus.text = LabelSet.getText(
            "complete_data",
            R.string.complete_data
        )
        tv_masterstatus.text = LabelSet.getText(
            "master_data",
            R.string.master_data
        )
        tvUploadDataMsg.text = LabelSet.getText(
            "shg_sync_msg",
            R.string.shg_sync_msg
        )
        tvUploadMsg.text = LabelSet.getText(
            "shg_sync_msg_without_bank",
            R.string.shg_sync_msg_without_bank
        )
        tvUploadMsg4.text = LabelSet.getText(
            "shg_sync_msg_complete",
            R.string.shg_sync_msg_complete
        )
        tvUploadMsg1.text = LabelSet.getText(
            "shg_msg_without_bank",
            R.string.shg_msg_without_bank
        )
        tvUploadMsg2.text = LabelSet.getText(
            "shg_sync_msg_with_bank",
            R.string.shg_sync_msg_with_bank
        )
        tvUploadMsg3.text = LabelSet.getText(
            "shg_msg_with_bank",
            R.string.shg_msg_with_bank
        )
    }

    fun importIFSC() {
        var ifscList = shgViewModel!!.getIfscCode()
        for (i in 0 until ifscList.size) {
            importBankList(ifscList.get(i))
        }
    }

    fun importBankList(ifscCode: String) {
        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
        val token = validate!!.returnStringValue(
            AESEncryption.decrypt(
                validate!!.RetriveSharepreferenceString(AppSP.token),
                validate!!.RetriveSharepreferenceString(AppSP.userid)
            )
        )

        val call1 = apiInterface?.getBankList(
            token,
            user, ifscCode
        )

        var bankResponse = call1?.execute()
        if (bankResponse!!.isSuccessful) {


            var alldata = bankResponse.body()!!.source().readUtf8().toString()
            val objectMapper = ObjectMapper()

            val langList: List<Bank_branchEntity> =
                objectMapper.readValue(
                    alldata,
                    object : TypeReference<List<Bank_branchEntity>>() {})
            if (langList != null && langList.size > 0) {

                CDFIApplication.database?.masterbankbranchDao()
                    ?.insertbranch(
                        langList
                    )

            }

        }


    }
}
