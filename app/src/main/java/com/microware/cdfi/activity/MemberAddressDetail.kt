package com.microware.cdfi.activity

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.view.View
import android.widget.AdapterView
import android.widget.RadioGroup
import android.widget.Spinner
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.*
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_member_address_detail.*
import kotlinx.android.synthetic.main.activity_member_address_detail.btn_add
import kotlinx.android.synthetic.main.activity_member_address_detail.btn_addgray
import kotlinx.android.synthetic.main.activity_member_address_detail.et_address1
import kotlinx.android.synthetic.main.activity_member_address_detail.et_address2
import kotlinx.android.synthetic.main.activity_member_address_detail.et_landmark
import kotlinx.android.synthetic.main.activity_member_address_detail.et_pincode
import kotlinx.android.synthetic.main.activity_member_address_detail.iv_mic_address1_eng
import kotlinx.android.synthetic.main.activity_member_address_detail.iv_mic_address2_eng
import kotlinx.android.synthetic.main.activity_member_address_detail.lay_address1
import kotlinx.android.synthetic.main.activity_member_address_detail.lay_city_town
import kotlinx.android.synthetic.main.activity_member_address_detail.lay_panchayat
import kotlinx.android.synthetic.main.activity_member_address_detail.lay_pincode
import kotlinx.android.synthetic.main.activity_member_address_detail.spin_addresstype
import kotlinx.android.synthetic.main.activity_member_address_detail.spin_block
import kotlinx.android.synthetic.main.activity_member_address_detail.spin_district
import kotlinx.android.synthetic.main.activity_member_address_detail.spin_panchayat
import kotlinx.android.synthetic.main.activity_member_address_detail.spin_state
import kotlinx.android.synthetic.main.activity_member_address_detail.spin_village
import kotlinx.android.synthetic.main.activity_member_address_detail.tvBlock
import kotlinx.android.synthetic.main.activity_member_address_detail.tvCode
import kotlinx.android.synthetic.main.activity_member_address_detail.tvDistrict
import kotlinx.android.synthetic.main.activity_member_address_detail.tvLandmark
import kotlinx.android.synthetic.main.activity_member_address_detail.tvState
import kotlinx.android.synthetic.main.activity_member_address_detail.tv_addressType
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.tablayout.*
import kotlinx.android.synthetic.main.white_toolbar.*
import kotlinx.android.synthetic.main.white_toolbar.tv_title
import java.util.*


class MemberAddressDetail : AppCompatActivity() {

    var validate: Validate? = null
    var addressEntity: member_addressEntity? = null
    var addressViewModel: MemberAddressViewmodel? = null
    var cadreMemberViewModel: CadreMemberViewModel? = null
    var membercode = 0L
    var memberName = ""
    var addressGUID = ""
    var sStateCode = 0
    var sDistrictCode = 0
    var sBlockCode = 0
    var sPanchayatCode = 0
    var sVillageCode = 0
    var isVerified = 0
    var lookupViewmodel: LookupViewmodel? = null
    var locationViewModel: LocationViewModel? = null
    var dataspin_addresstype: List<LookupEntity>? = null
    var dataPanchayat: List<PanchayatEntity>? = null
    var dataVillage: List<VillageEntity>? = null
    var datastate: List<StateEntity>? = null
    var datadistrcit: List<DistrictEntity>? = null
    var datablock: List<BlockEntity>? = null
    var dataddresslocation: List<LookupEntity>? = null

    var memberviewmodel: Memberviewmodel? = null
    var memberPhoneViewmodel: MemberPhoneViewmodel? = null
    var memberkycviewmodel: MemberKYCViewmodel? = null
    var memberbankviewmodel: MemberBankViewmodel? = null
    var membersystemviewmodel: MemberSystemtagViewmodel? = null
    var shgViewmodel: SHGViewmodel? = null


    var languageCode: String = ""
    var mSpeechRecognizer: SpeechRecognizer? = null
    var is_listening = false
    var is_lang_change = false
    var mSpeechRecognizerIntent: Intent? = null
    private val REQ_CODE_SPEECH_INPUT = 11
    private var CURRENT_SELECTED_EDITTEXT = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_member_address_detail)

        validate = Validate(this)
        setLabelText()
        addressViewModel = ViewModelProviders.of(this).get(MemberAddressViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        locationViewModel = ViewModelProviders.of(this).get(LocationViewModel::class.java)
        memberviewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)
        memberPhoneViewmodel = ViewModelProviders.of(this).get(MemberPhoneViewmodel::class.java)
        memberkycviewmodel = ViewModelProviders.of(this).get(MemberKYCViewmodel::class.java)
        shgViewmodel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        memberbankviewmodel = ViewModelProviders.of(this).get(MemberBankViewmodel::class.java)
        cadreMemberViewModel = ViewModelProviders.of(this).get(CadreMemberViewModel::class.java)
        membersystemviewmodel =
            ViewModelProviders.of(this).get(MemberSystemtagViewmodel::class.java)

        membercode = validate!!.RetriveSharepreferenceLong(AppSP.membercode)
        memberName = validate!!.RetriveSharepreferenceString(AppSP.memebrname)!!
        tvCode.text = membercode.toString()
        et_membercode.setText(memberName)

        ivHome.visibility = View.GONE
        if (validate!!.RetriveSharepreferenceInt(AppSP.MemberLockRecord) == 1) {
            ivLock.visibility = View.VISIBLE
            btn_addgray.visibility = View.VISIBLE
            btn_add.visibility = View.GONE

        } else {
            ivLock.visibility = View.GONE
            btn_addgray.visibility = View.GONE
            btn_add.visibility = View.VISIBLE
        }
        tv_title.text =
            validate!!.RetriveSharepreferenceString(AppSP.memebrname)!! + "(" + validate!!.RetriveSharepreferenceString(
                AppSP.ShgName
            ) + ")"
        spin_state.isEnabled = false
        spin_district.isEnabled = false
        spin_block.isEnabled = false

        sStateCode = validate!!.RetriveSharepreferenceInt(AppSP.statecode)
        sDistrictCode = validate!!.RetriveSharepreferenceInt(AppSP.districtcode)
        sBlockCode = validate!!.RetriveSharepreferenceInt(AppSP.blockcode)

        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvCader.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvVector.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_cader.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        var memberlist =
            memberviewmodel!!.getmember(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
        var memberphonelist =
            memberPhoneViewmodel!!.getphoneDatalistcount(
                validate!!.RetriveSharepreferenceString(
                    AppSP.MEMBERGUID
                )
            )
        var memberbanklist =
            memberbankviewmodel!!.getBankdetaildatalistcount(
                validate!!.RetriveSharepreferenceString(
                    AppSP.MEMBERGUID
                )
            )
        var memberkyclist =
            memberkycviewmodel!!.getKycdetaildatalistcount(
                validate!!.RetriveSharepreferenceString(
                    AppSP.MEMBERGUID
                )
            )
        var membersystetmtaglist =
            membersystemviewmodel!!.getSystemtagdatalistcount(
                validate!!.RetriveSharepreferenceString(
                    AppSP.MEMBERGUID
                )
            )

        var cadreshgMemberlist = cadreMemberViewModel!!.getCadreListdata1(
            validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!)
        if (!cadreshgMemberlist.isNullOrEmpty()){
            IvCader.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }

        if (!memberlist.isNullOrEmpty()) {
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberbanklist.isNullOrEmpty()) {
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberkyclist.isNullOrEmpty()) {
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!membersystetmtaglist.isNullOrEmpty()) {
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberphonelist.isNullOrEmpty()) {
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }

        ivBack.setOnClickListener {
            var intent = Intent(this, MemberAddressListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

        lay_vector.setOnClickListener {
            var intent = Intent(this, MemberDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_phone.setOnClickListener {
            var intent = Intent(this, MemeberPhoneListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_bank.setOnClickListener {
            var intent = Intent(this, MemberBankListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_kyc.setOnClickListener {
            var intent = Intent(this, MemberIdListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        lay_Cader.setOnClickListener {
            var intent = Intent(this, CadreListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        btn_add.setOnClickListener {
            if (!validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID).isNullOrEmpty()) {
                if (checkValidation() == 1) {
                    SaveAddressDetail()
                }
            } else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "add_memeber_data_first",
                        R.string.add_memeber_data_first
                    ),
                    this,
                    MemberDetailActivity::class.java
                )
            }
        }

        spin_panchayat?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                sPanchayatCode = validate!!.returnPanchayatID(spin_panchayat, dataPanchayat)
                if (position > 0) {
                    dataVillage = locationViewModel!!.getVillagedata_by_panchayatCode(
                        sStateCode,
                        sDistrictCode,
                        sBlockCode,
                        sPanchayatCode
                    )
                    validate!!.fillVillageSpinner(
                        this@MemberAddressDetail,
                        spin_village,
                        dataVillage
                    )
                    spin_village.setSelection(
                        validate!!.returnVillagepos(
                            sVillageCode,
                            dataVillage
                        )
                    )
                } else {
                    dataVillage = locationViewModel!!.getVillagedata_by_panchayatCode(
                        sStateCode,
                        sDistrictCode,
                        sBlockCode,
                        sPanchayatCode
                    )
                    validate!!.fillVillageSpinner(
                        this@MemberAddressDetail,
                        spin_village,
                        dataVillage
                    )
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        rgaddresslocation.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->

            if (checkedId == 1) {
                lay_city_town.visibility=View.VISIBLE
                lay_panchayat.visibility=View.VISIBLE
            } else {
                lay_city_town.visibility=View.GONE
                lay_panchayat.visibility=View.GONE

            }

        })

        fillSpinner()
        showData()
        //   setLabelText()


        languageCode = CDFIApplication.validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!

        iv_mic_address1_eng.setOnClickListener {
            CURRENT_SELECTED_EDITTEXT = 1
            promptSpeechInput("en")
        }

        iv_mic_address2_eng.setOnClickListener {
            CURRENT_SELECTED_EDITTEXT = 2
            promptSpeechInput("en")
        }
    }


    private fun fillSpinner() {
        datastate = locationViewModel!!.getStateByStateCode()
        datadistrcit = locationViewModel!!.getDistrictData(sStateCode)
        datablock = locationViewModel!!.getBlock_data(sStateCode, sDistrictCode)
        validate!!.fillStateSpinner(this, spin_state, datastate)
        validate!!.fillDistrictSpinner(this, spin_district, datadistrcit)
        validate!!.fillBlockSpinner(this, spin_block, datablock)

        dataPanchayat =
            locationViewModel!!.getPanchayatByPanchayatCode(sStateCode, sDistrictCode, sBlockCode)
        validate!!.fillPanchayatSpinner(this, spin_panchayat, dataPanchayat)

        dataVillage = locationViewModel!!.getVillagedata_by_panchayatCode(
            sStateCode,
            sDistrictCode,
            sBlockCode,
            sPanchayatCode
        )
        validate!!.fillVillageSpinner(this, spin_village, dataVillage)

        dataspin_addresstype = lookupViewmodel!!.getlookup(
            20,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        validate!!.fillspinner(this, spin_addresstype, dataspin_addresstype)

        dataddresslocation = lookupViewmodel!!.getlookup(
            53,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        validate!!.fillradio(rgaddresslocation, 1, dataddresslocation, this)

    }

    private fun showData() {
        var list =
            addressViewModel!!.getAddressdetaildata(validate!!.RetriveSharepreferenceString(AppSP.MemberAddressGUID))
        spin_state.setSelection(validate!!.returnStatepos(sStateCode, datastate))
        spin_district.setSelection(validate!!.returnDistrictpos(sDistrictCode, datadistrcit))


        if (!list.isNullOrEmpty() && list.size > 0) {
            isVerified = validate!!.returnIntegerValue(list.get(0).is_verified.toString())
            if(isVerified == 1){
                btn_add.text = LabelSet.getText(
                    "save_verify",
                    R.string.save_verify
                )
                lay_address1.setBackgroundColor(Color.parseColor("#D8FFD8"))
                lay_panchayat.setBackgroundColor(Color.parseColor("#D8FFD8"))
                lay_city_town.setBackgroundColor(Color.parseColor("#D8FFD8"))
                lay_pincode.setBackgroundColor(Color.parseColor("#D8FFD8"))
            }else {
                btn_add.text = LabelSet.getText(
                    "add_address",
                    R.string.add_address
                )
            }
            spin_addresstype.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(list.get(0).address_type.toString()),
                    dataspin_addresstype
                )
            )
            spin_panchayat.setSelection(
                validate!!.returnPanchayatpos(
                    validate!!.returnIntegerValue(list.get(0).panchayat_id.toString()),
                    dataPanchayat
                )
            )
            sVillageCode = validate!!.returnIntegerValue(list.get(0).village_id.toString())
            et_address1.setText(validate!!.returnStringValue(list.get(0).address_line1))
            et_address2.setText(validate!!.returnStringValue(list.get(0).address_line2))
            et_pincode.setText(validate!!.returnStringValue(list.get(0).postal_code.toString()))
            et_landmark.setText(validate!!.returnStringValue(list.get(0).landmark))
            if (validate!!.returnIntegerValue(list.get(0).address_location.toString()) == 1) {
                lay_city_town.visibility=View.VISIBLE
                lay_panchayat.visibility=View.VISIBLE
            } else {
                lay_city_town.visibility=View.GONE
                lay_panchayat.visibility=View.GONE
            }

            validate!!.fillradio(rgaddresslocation, validate!!.returnIntegerValue(list.get(0).address_location.toString()), dataddresslocation, this)

        } else {


            spin_panchayat.setSelection(
                validate!!.returnPanchayatpos(
                    validate!!.RetriveSharepreferenceInt(
                        AppSP.panchayatcode
                    ), dataPanchayat
                )
            )
            sVillageCode = validate!!.RetriveSharepreferenceInt(AppSP.villagecode)
        }
    }


    private fun SaveAddressDetail() {

        addressGUID = validate!!.random()
        var addressType = validate!!.returnlookupcode(spin_addresstype, dataspin_addresstype)
        var villagecode = validate!!.returnVillageID(spin_village, dataVillage)
        var panchayatcode = validate!!.returnPanchayatID(spin_panchayat, dataPanchayat)

        if (validate!!.RetriveSharepreferenceString(AppSP.MemberAddressGUID).isNullOrEmpty()) {
            addressEntity = member_addressEntity(
                0,
                membercode,
                validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID),
                addressGUID,
                addressType,
                et_address1.text.toString(),
                et_address2.text.toString(),
                villagecode,
                panchayatcode,
                validate!!.RetriveSharepreferenceInt(AppSP.blockcode),
                et_landmark.text.toString(),
                validate!!.RetriveSharepreferenceInt(AppSP.statecode),
                validate!!.RetriveSharepreferenceInt(AppSP.districtcode),
                et_pincode.text.toString(),
                1,
                0,
                1,
                validate!!.Daybetweentime(validate!!.currentdatetime),
                validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                0,
                "",
                0,
                "", 1, 0, 1, rgaddresslocation.checkedRadioButtonId
            )
            addressViewModel!!.insert(addressEntity!!)
            validate!!.SaveSharepreferenceString(AppSP.MemberAddressGUID, addressGUID)
            validate!!.updateMemberEditFlag(
                validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),
                shgViewmodel,
                memberviewmodel
            )

            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ), this, MemberAddressListActivity::class.java
            )
        } else {
            if(checkData()==0){
            addressViewModel!!.updateAddressDetail(
                validate!!.RetriveSharepreferenceString(AppSP.MemberAddressGUID)!!,
                addressType,
                et_address1.text.toString(),
                et_address2.text.toString(),
                villagecode,
                panchayatcode,
                et_landmark.text.toString(),
                validate!!.RetriveSharepreferenceInt(AppSP.statecode),
                validate!!.RetriveSharepreferenceInt(AppSP.districtcode),
                et_pincode.text.toString(),
                1,
                validate!!.Daybetweentime(validate!!.currentdatetime),
                validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                rgaddresslocation.checkedRadioButtonId,
                1
            )
            validate!!.updateMemberEditFlag(
                validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),
                shgViewmodel,
                memberviewmodel
            )

            validate!!.CustomAlert(
                LabelSet.getText(
                    "updated_successfully",
                    R.string.updated_successfully
                ), this, MemberAddressListActivity::class.java
            )
        }else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "updated_successfully",
                        R.string.updated_successfully
                    ), this, MemberAddressListActivity::class.java
                )
        }
    }

    }

    fun checkValidation(): Int {
        var value = 1
        if (spin_addresstype.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this@MemberAddressDetail,
                spin_addresstype,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "address_type",
                    R.string.address_type
                )
            )
            value = 0
            return value
        } else if (et_address1.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "addresslin1",
                    R.string.addresslin1
                ),
                this,
                et_address1
            )
            value = 0
            return value
        } else if (et_pincode.text.toString().length == 0 || (et_pincode.text.toString().length > 0 && validate!!.returnIntegerValue(et_pincode.text.toString())==0) || (et_pincode.text.toString().length > 0 && et_pincode.text.toString().length <6)) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "valid_pincode",
                    R.string.valid_pincode
                ),
                this,
                et_pincode
            )
            value = 0
            return value
        }

        val sp = arrayOf<Spinner>(
            spin_addresstype
        )

        for (i in sp.indices) {
            if (sp[i].selectedItemPosition == 0) {
                validate!!.CustomAlertSpinner(
                    this,
                    sp[i],
                    LabelSet.getText(
                        "Pleaseenetrmandatorydetails",
                        R.string.Pleaseenetrmandatorydetails
                    )
                )
                value = 0
                break
            }
        }

        return value

    }

    override fun onBackPressed() {
        var intent = Intent(this, MemberAddressListActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    fun setLabelText() {
        tv_addressType.text = LabelSet.getText(
            "address_type",
            R.string.address_type
        )
        tv_addresslocation.text = LabelSet.getText(
            "address_location",
            R.string.address_location
        )
        tvAddresLine1.text = LabelSet.getText(
            "address_1",
            R.string.address_1
        )
        et_address1.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_address2.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_landmark.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_pincode.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tvAddresLine2.text = LabelSet.getText(
            "address_2",
            R.string.address_2
        )
        tvState.text = LabelSet.getText("state", R.string.state)
        tvDistrict.text = LabelSet.getText(
            "district",
            R.string.district
        )
        tvBlock.text = LabelSet.getText("block", R.string.block)
        tv_panchayat.text = LabelSet.getText(
            "panchayat",
            R.string.panchayat
        )
        tvVillage.text = LabelSet.getText(
            "city_town_village",
            R.string.city_town_village
        )
        tvLandmark.text = LabelSet.getText(
            "landmark",
            R.string.landmark
        )
        tvPincode.text = LabelSet.getText(
            "pincode",
            R.string.pincode
        )
        btn_add.text = LabelSet.getText(
            "add_address",
            R.string.add_address
        )
        btn_addgray.text = LabelSet.getText(
            "add_address",
            R.string.add_address
        )
    }

    private fun checkData(): Int {
        var value = 1
        var addressType = validate!!.returnlookupcode(spin_addresstype, dataspin_addresstype)
        var villagecode = validate!!.returnVillageID(spin_village, dataVillage)
        var panchayatcode = validate!!.returnPanchayatID(spin_panchayat, dataPanchayat)
            var list =
                addressViewModel!!.getAddressdetaildata(
                    validate!!.RetriveSharepreferenceString(AppSP.MemberAddressGUID))
                if (validate!!.returnStringValue(et_address1.text.toString()) != validate!!.returnStringValue(
                        list?.get(0)?.address_line1)) {

                    value = 0


                } else if (et_address2.text.toString() != validate!!.returnStringValue(list?.get(0)?.address_line2)) {

                    value = 0

                } else if (et_landmark.text.toString() != validate!!.returnStringValue(list?.get(0)?.landmark)) {

                    value = 0
                } else if (et_pincode.text.toString() != validate!!.returnStringValue(list?.get(0)?.postal_code.toString())) {

                    value = 0


                } else if (addressType != validate!!.returnIntegerValue(
                        list?.get(
                            0
                        )?.address_type.toString()
                    )
                ) {

                    value = 0
                } else if (panchayatcode != validate!!.returnIntegerValue(
                        list?.get(
                            0
                        )?.panchayat_id.toString()
                    )
                ) {

                    value = 0
                } else if (villagecode != validate!!.returnIntegerValue(
                        list?.get(
                            0
                        )?.village_id.toString()
                    )
                ) {

                    value = 0
                }else if (rgaddresslocation.checkedRadioButtonId != validate!!.returnIntegerValue(
                        list?.get(
                            0
                        )?.address_location.toString()
                    )
                ) {

                    value = 0

                }

        return value
    }



    fun promptSpeechInput(language: String) {


        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )

        if (isNetworkConnected()){
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, language)
        }
        else{
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.ENGLISH)
        }

        intent.putExtra(
            RecognizerIntent.EXTRA_PROMPT,
            getString(R.string.speech_prompt)
        )
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT)
        } catch (a: ActivityNotFoundException) {
            Toast.makeText(
                applicationContext,
                getString(R.string.speech_not_supported),
                Toast.LENGTH_SHORT
            ).show()
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (resultCode == RESULT_OK) {
                if (requestCode == 11) {
                    if (resultCode == RESULT_OK && null != data) {
                        val result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                        var textdata = result!![0]
                        if (textdata.isNullOrEmpty()) {
                        } else {
                            if (CURRENT_SELECTED_EDITTEXT == 1) {
                                if (et_address1.hasFocus()) {
                                    var cursorPosition: Int = et_address1.selectionStart
                                    et_address1.text.insert(cursorPosition, textdata)
                                } else {
                                    var getData = et_address1.text.toString()
                                    if (getData.length > 0) {
                                        getData = getData + " " + textdata
                                    } else {
                                        getData = getData + textdata
                                    }
                                    et_address1.setText(getData)
                                }
                            }
                            if (CURRENT_SELECTED_EDITTEXT == 2) {
                                if (et_address2.hasFocus()) {
                                    var cursorPosition: Int = et_address2.selectionStart
                                    et_address2.text.insert(cursorPosition, textdata)
                                } else {
                                    var getData = et_address2.text.toString()
                                    if (getData.length > 0) {
                                        getData = getData + " " + textdata
                                    } else {
                                        getData = getData + textdata
                                    }
                                    et_address2.setText(getData)
                                }
                            }
                        }
                    }
                }

            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
//                Toast.makeText(applicationContext, R.string.cancel, Toast.LENGTH_SHORT)
//                    .show()

            }
        } catch (e: Exception) {
            e.printStackTrace()
//            Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show()
        }
    }

    private fun isNetworkConnected(): Boolean {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager //1
        val networkInfo = connectivityManager.activeNetworkInfo //2
        return networkInfo != null && networkInfo.isConnected //3
    }

}
