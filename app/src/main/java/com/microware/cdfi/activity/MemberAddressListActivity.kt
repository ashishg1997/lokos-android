package com.microware.cdfi.activity

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.MemberAddressAdapter
import com.microware.cdfi.entity.member_addressEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_member_address_list.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.tablayout.*
import kotlinx.android.synthetic.main.white_toolbar.*

class MemberAddressListActivity : AppCompatActivity() {

    var validate: Validate? = null
    var addressEntity: member_addressEntity? = null
    var addressViewModel: MemberAddressViewmodel? = null
    var lookupViewmodel: LookupViewmodel? = null
    var locationViewmodel: LocationViewModel? = null
    var memberviewmodel: Memberviewmodel? = null
    var memberPhoneViewmodel: MemberPhoneViewmodel? = null
    var memberkycviewmodel: MemberKYCViewmodel? = null
    var memberbankviewmodel: MemberBankViewmodel? = null
    var membersystemviewmodel: MemberSystemtagViewmodel? = null
    var cadreMemberViewModel: CadreMemberViewModel? = null
    var shgViewmodel: SHGViewmodel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_member_address_list)

        validate = Validate(this)
        addressViewModel = ViewModelProviders.of(this).get(MemberAddressViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        locationViewmodel = ViewModelProviders.of(this).get(LocationViewModel::class.java)
        memberviewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)
        memberPhoneViewmodel = ViewModelProviders.of(this).get(MemberPhoneViewmodel::class.java)
        memberkycviewmodel = ViewModelProviders.of(this).get(MemberKYCViewmodel::class.java)
        memberbankviewmodel = ViewModelProviders.of(this).get(MemberBankViewmodel::class.java)
        membersystemviewmodel = ViewModelProviders.of(this).get(MemberSystemtagViewmodel::class.java)
        shgViewmodel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        cadreMemberViewModel = ViewModelProviders.of(this).get(CadreMemberViewModel::class.java)

        ivHome.visibility = View.GONE
        tv_title.text =
            validate!!.RetriveSharepreferenceString(AppSP.memebrname)!! + "(" + validate!!.RetriveSharepreferenceString(AppSP.ShgName) + ")"
        ivBack.setOnClickListener {
            var intent = Intent(this,MemberListActivity::class.java)
            intent.flags=Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }
        if (validate!!.RetriveSharepreferenceInt(AppSP.MemberLockRecord) == 1) {
            ivLock.visibility = View.VISIBLE
            addAddressgray.visibility = View.VISIBLE
            addAddress.visibility = View.GONE

        } else {
            ivLock.visibility = View.GONE
            addAddressgray.visibility = View.GONE
            addAddress.visibility = View.VISIBLE
        }
        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvCader.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvVector.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimary))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_cader.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        var memberlist =
            memberviewmodel!!.getmember(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
        var memberphonelist =
            memberPhoneViewmodel!!.getphoneDatalistcount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
        var memberbanklist =
            memberbankviewmodel!!.getBankdetaildatalistcount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
        var memberkyclist =
            memberkycviewmodel!!.getKycdetaildatalistcount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
        var membersystetmtaglist =
            membersystemviewmodel!!.getSystemtagdatalistcount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))

        var cadreshgMemberlist = cadreMemberViewModel!!.getCadreListdata1(
            validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!)
        if (!cadreshgMemberlist.isNullOrEmpty()){
            IvCader.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }


        if (!memberlist.isNullOrEmpty()){
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberbanklist.isNullOrEmpty()){
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberkyclist.isNullOrEmpty()){
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!membersystetmtaglist.isNullOrEmpty()){
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberphonelist.isNullOrEmpty()){
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        lay_systemTag.setOnClickListener {
            var intent = Intent(this, MemberGroupTagList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_vector.setOnClickListener {
            var intent = Intent(this,MemberDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_phone.setOnClickListener {
            var intent = Intent(this,MemeberPhoneListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_bank.setOnClickListener {
            var intent = Intent(this,MemberBankListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_kyc.setOnClickListener {
            var intent = Intent(this,MemberIdListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }
        lay_Cader.setOnClickListener {
            var intent = Intent(this, CadreListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        addAddress.setOnClickListener {
            if (!validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID).isNullOrEmpty()) {
                validate!!.SaveSharepreferenceString(AppSP.MemberAddressGUID, "")
                var intent = Intent(this, MemberAddressDetail::class.java)
                startActivity(intent)
                finish()
            }else{
                validate!!.CustomAlert(LabelSet.getText(
                    "add_memeber_data_first",
                    R.string.add_memeber_data_first
                ),this,MemberDetailActivity::class.java)
            }
        }

        if(addressViewModel!!.getVerificationCount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))>0){
            btn_VerifyAddress.visibility = View.VISIBLE
        }else {
            btn_VerifyAddress.visibility = View.GONE
        }
        btn_VerifyAddress.setOnClickListener {
            var isCompleteCount = addressViewModel!!.getCompletionCount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
            if(isCompleteCount==0){
                addressViewModel!!.updateIsVerifed(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID),validate!!.Daybetweentime(validate!!.currentdatetime),
                    validate!!.RetriveSharepreferenceString(AppSP.userid)!!)
                validate!!.CustomAlert(LabelSet.getText(
                    "",
                    R.string.verified_successfully
                ),this)
                if(addressViewModel!!.getVerificationCount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))>0){
                    btn_VerifyAddress.visibility = View.VISIBLE
                }else {
                    btn_VerifyAddress.visibility = View.GONE
                }
            }else {
                validate!!.CustomAlert(LabelSet.getText(
                    "",
                    R.string.completerecord
                ),this)
            }
        }
        fillData()

    }

    private fun fillData() {
        addressViewModel!!.getAddressdata(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))!!
            .observe(this,object : Observer<List<member_addressEntity>> {
                override fun onChanged(address_list: List<member_addressEntity>?) {
                    if (address_list!=null){
                        rvList.layoutManager = LinearLayoutManager(this@MemberAddressListActivity)
                        rvList.adapter = MemberAddressAdapter(this@MemberAddressListActivity, address_list)
                        if (address_list.size>1){
                            addAddress.visibility=View.GONE
                        }else{
                            if (validate!!.RetriveSharepreferenceInt(AppSP.MemberLockRecord) == 1) {
                                ivLock.visibility = View.VISIBLE
                                addAddressgray.visibility = View.VISIBLE
                                addAddress.visibility = View.GONE

                            } else {
                                ivLock.visibility = View.GONE
                                addAddressgray.visibility = View.GONE
                                addAddress.visibility = View.VISIBLE
                            }
                           // addAddress.visibility=View.VISIBLE
                        }
                        if(address_list.size>0){
                            lay_noaddress_avialable.visibility=View.GONE
                        }else {
                            lay_noaddress_avialable.visibility=View.VISIBLE
                            tv_noaddress_avialable.text = LabelSet.getText(
                                "no_address_s_avialable",
                                R.string.no_address_s_avialable
                            )
                        }
                    }
                }
            })
    }

    fun getAddressType(kyc_code: String, addressType: Int?): String?{
        var type:String? = null
        type = lookupViewmodel!!.getAddressType(kyc_code,addressType)
        return type
    }

    fun getLookupValue(id:Int):String{
        var    dataspin_addresstype =lookupViewmodel!!.getlookup(20,validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
        var sValue = ""
        sValue = validate!!.returnlookupcodevalue(id,dataspin_addresstype)
        return sValue
    }

    fun getVillageName(villageId: Int?): String? {
        var name:String? = null
        name = locationViewmodel!!.getVillageName(villageId!!)
        return name
    }

    override fun onBackPressed() {
        var intent = Intent(this,MemberListActivity::class.java)
        intent.flags=Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    fun CustomAlert(guid:String,iFlag:Int) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = LabelSet.getText(
            "do_u_want_to_delete",
            R.string.do_u_want_to_delete
        )
        mDialogView.txt_dialog_title.text = LabelSet.getText("app_name", R.string.app_name)
        mDialogView.btn_yes.text = LabelSet.getText("yes", R.string.yes)
        mDialogView.btn_no.text = LabelSet.getText("no", R.string.no)
        mDialogView.btn_yes.setOnClickListener {

            if(iFlag==0){
                addressViewModel!!.deleteRecord(guid)
            }else {
                addressViewModel!!.deleteData(guid)

            }
            validate!!.updateMemberEditFlag(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),shgViewmodel,memberviewmodel)

            mAlertDialog.dismiss()

        }
        mDialogView.btn_no.setOnClickListener {

            mAlertDialog.dismiss()

        }
    }
}