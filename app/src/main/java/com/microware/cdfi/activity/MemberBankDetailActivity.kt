package com.microware.cdfi.activity

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModelProviders
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.google.gson.JsonObject
import com.microware.cdfi.BuildConfig
import com.microware.cdfi.R
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.*
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_member_bank_detail.*
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.customealertdialogelogin.*
import kotlinx.android.synthetic.main.customealertdialogelogin.view.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.dialog_bankpassbook_name_diff.*
import kotlinx.android.synthetic.main.tablayout.*
import kotlinx.android.synthetic.main.tablayout.lay_phone
import kotlinx.android.synthetic.main.white_toolbar.*
import kotlinx.android.synthetic.main.white_toolbar.tv_title
import okhttp3.ResponseBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*
import java.text.SimpleDateFormat
import java.util.*

class MemberBankDetailActivity : AppCompatActivity() {

    var validate: Validate? = null
    var memberBankEntity: MemberBankAccountEntity? = null
    var memberbankViewmodel: MemberBankViewmodel? = null
    var cboBankViewmodel: CboBankViewmodel? = null
    var shgViewmodel: SHGViewmodel? = null
    var cadreMemberViewModel: CadreMemberViewModel? = null
    var bankGUID = ""
    var membercode = 0L
    var branch_id = 0
    var memberName = ""
    var dataspin_bank: List<BankEntity>? = null
    var dataspin_bankBranch: List<Bank_branchEntity>? = null
    var dataspin_branch: List<Bank_branchEntity>? = null
    var bankMasterViewModel: MasterBankViewmodel? = null
    var imageUploadViewmodel: ImageUploadViewmodel? = null
    var masterbankBranchViewmodel: MasterBankBranchViewModel? = null
    var lookupViewmodel: LookupViewmodel? = null
    var dataspin_name: List<LookupEntity>? = null
    var sBankCode = 0
    var isVerified = 0
    var dataspin_yesno: List<LookupEntity>? = null

    var imageName = ""
    var newimageName = false
    var bitmap: Bitmap? = null

    private var fileUri: Uri? = null
    var imgPathUpload = ""
    var mediaFile: File? = null
    var memberviewmodel: Memberviewmodel? = null
    var memberPhoneViewmodel: MemberPhoneViewmodel? = null
    var memberkycviewmodel: MemberKYCViewmodel? = null
    var memberaddressviewmodel: MemberAddressViewmodel? = null
    var membersystemviewmodel: MemberSystemtagViewmodel? = null
    var apiInterface: ApiInterface? = null
    internal lateinit var progressDialog: ProgressDialog
    var checkdeffName:Boolean? = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_member_bank_detail)
         checkdeffName = true
        validate = Validate(this)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        setLabelText()
        memberbankViewmodel = ViewModelProviders.of(this).get(MemberBankViewmodel::class.java)
        imageUploadViewmodel = ViewModelProviders.of(this).get(ImageUploadViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        bankMasterViewModel = ViewModelProviders.of(this).get(MasterBankViewmodel::class.java)
        masterbankBranchViewmodel =
            ViewModelProviders.of(this).get(MasterBankBranchViewModel::class.java)
        memberviewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)
        memberPhoneViewmodel = ViewModelProviders.of(this).get(MemberPhoneViewmodel::class.java)
        memberkycviewmodel = ViewModelProviders.of(this).get(MemberKYCViewmodel::class.java)
        memberaddressviewmodel = ViewModelProviders.of(this).get(MemberAddressViewmodel::class.java)
        membersystemviewmodel =
            ViewModelProviders.of(this).get(MemberSystemtagViewmodel::class.java)
        cboBankViewmodel = ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        shgViewmodel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        cadreMemberViewModel = ViewModelProviders.of(this).get(CadreMemberViewModel::class.java)

        membercode = validate!!.RetriveSharepreferenceLong(AppSP.membercode)
        memberName = validate!!.RetriveSharepreferenceString(AppSP.memebrname)!!
        tvCode.text = membercode.toString()
        // et_membercode.setText(memberName)

        ivBack.setOnClickListener {
            var intent = Intent(this, MemberBankListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

        ivHome.visibility = View.GONE
        if (validate!!.RetriveSharepreferenceInt(AppSP.MemberLockRecord) == 1 || validate!!.RetriveSharepreferenceInt(AppSP.MemberBankStatus)==2) {
            ivLock.visibility = View.VISIBLE
            btn_addgray.visibility = View.VISIBLE
            btn_add.visibility = View.GONE

        } else {
            ivLock.visibility = View.GONE
            btn_addgray.visibility = View.GONE
            btn_add.visibility = View.VISIBLE
        }
        tv_title.text =
            validate!!.RetriveSharepreferenceString(AppSP.memebrname)!! + "(" + validate!!.RetriveSharepreferenceString(
                AppSP.ShgName
            ) + ")"

        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvCader.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvVector.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_cader.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        var memberlist =
            memberviewmodel!!.getmember(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
        var memberphonelist =
            memberPhoneViewmodel!!.getphoneDatalistcount(
                validate!!.RetriveSharepreferenceString(
                    AppSP.MEMBERGUID
                )
            )
        var memberaddresslist =
            memberaddressviewmodel!!.getAddressdatalistcount(
                validate!!.RetriveSharepreferenceString(
                    AppSP.MEMBERGUID
                )
            )
        var memberkyclist =
            memberkycviewmodel!!.getKycdetaildatalistcount(
                validate!!.RetriveSharepreferenceString(
                    AppSP.MEMBERGUID
                )
            )
        var membersystetmtaglist =
            membersystemviewmodel!!.getSystemtagdatalistcount(
                validate!!.RetriveSharepreferenceString(
                    AppSP.MEMBERGUID
                )
            )

        var cadreshgMemberlist = cadreMemberViewModel!!.getCadreListdata1(
            validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!)
        if (!cadreshgMemberlist.isNullOrEmpty()){
            IvCader.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }

        if (!memberlist.isNullOrEmpty()) {
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberaddresslist.isNullOrEmpty()) {
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberkyclist.isNullOrEmpty()) {
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!membersystetmtaglist.isNullOrEmpty()) {
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberphonelist.isNullOrEmpty()) {
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        lay_systemTag.setOnClickListener {
            var intent = Intent(this, MemberGroupTagList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_vector.setOnClickListener {
            var intent = Intent(this, MemberDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_phone.setOnClickListener {
            var intent = Intent(this, MemeberPhoneListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_location.setOnClickListener {
            var intent = Intent(this, MemberAddressListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_kyc.setOnClickListener {
            var intent = Intent(this, MemberIdListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        lay_Cader.setOnClickListener {
            var intent = Intent(this, CadreListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        et_opdate.setOnClickListener {
            validate!!.datePickerwithmindate(
                validate!!.RetriveSharepreferenceLong(AppSP.MemberDob),
                et_opdate
            )
        }

        /*et_cldate.setOnClickListener {
            validate!!.datePicker(et_cldate)
        }*/

        spin_bankname?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                if (position > 0) {
                    sBankCode = validate!!.returnMasterBankID(spin_bankname, dataspin_bank)
                    bindBankBranchSpinner(sBankCode)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }
        spin_branch?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                if (position > 0) {
                    var branchifsc =
                        validate!!.returnMasterBankBranchifsc(spin_branch, dataspin_bankBranch)
                    et_ifsc.setText(branchifsc)

                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        Imgsearch.setOnClickListener {

            spin_bankname.setSelection(0)
            branch_id = 0
            //spin_branch.setSelection(0)
            if (et_ifsc.text.toString().toUpperCase().trim().length == 11) {
                dataspin_branch =
                    masterbankBranchViewmodel!!.BankBranchlistifsc(
                        et_ifsc.text.toString().toUpperCase()
                    )
                if (!dataspin_branch.isNullOrEmpty()) {
                    branch_id = dataspin_branch!!.get(0).bank_branch_id
                    spin_bankname.setSelection(
                        validate!!.returnMasterBankpos(
                            validate!!.returnIntegerValue(dataspin_branch!!.get(0).bank_id.toString()),
                            dataspin_bank
                        )
                    )
                    bindBankBranchSpinner(validate!!.returnIntegerValue(dataspin_branch!!.get(0).bank_id.toString()))

                } else if (et_ifsc.text.toString().toUpperCase()
                        .trim().length == 11 && isNetworkConnected()
                ) {
//                spin_branch.setSelection(0)
//                spin_bankname.setSelection(0)
                    importBankList(et_ifsc.text.toString().toUpperCase())

                } else {
                    validate!!.CustomAlertEditText(
                        LabelSet.getText(
                            "no_internet_msg",
                            R.string.no_internet_msg
                        ),
                        this,
                        et_ifsc
                    )
                }
            } else {
                validate!!.CustomAlertEditText(
                    LabelSet.getText(
                        "pleaseenetervalidifsc",
                        R.string.pleaseenetervalidifsc
                    ),
                    this,
                    et_ifsc
                )
            }

        }

        btn_add.setOnClickListener {
            if (!validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID).isNullOrEmpty()) {
                if (checkvalidation() == 1) {
                    if (checkData() == 0) {
                        var isdefault = memberbankViewmodel!!.getBankdetaildefaultcount(
                            validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)
                        )
                        if (isdefault == 0) {
                            SaveBankDetail(1)
                        } else {
                            CustomAlertisdefault()
                        }
                    }else {
                        validate!!.CustomAlert(
                            LabelSet.getText(
                                "data_saved_successfully",
                                R.string.data_saved_successfully
                            ),
                            this,
                            MemberBankListActivity::class.java
                        )
                    }
                }
            } else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "add_memeber_data_first",
                        R.string.add_memeber_data_first
                    ),
                    this,
                    MemberDetailActivity::class.java
                )
            }

        }
        ImgFrntpage.setOnClickListener {
            if (!checkPermission()) {
                requestPermission(101)
            } else {
                captureimage(101)
            }

        }
        tblFront.setOnClickListener {
            if (imageName.isNotEmpty()) {
                ShowImage(imageName)
            }
        }
        Imginfo.setOnClickListener {
            if (!memberName.equals(et_nameinbankpassbook.text.toString())) {
                showDialog()

            }

        }
        spin_bankname.setTitle(
            LabelSet.getText(
                "selectbankname",
                R.string.selectbankname
            )
        )
        spin_branch.setTitle(
            LabelSet.getText(
                "selectbranchname",
                R.string.selectbranchname
            )
        )
        fillSpinner()
        fillRadio()
        showData()
        //  setLabelText()
    }

    private fun showDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_bankpassbook_name_diff)

        val tvshdName = dialog.findViewById(R.id.tv_shg_name) as TextView
        val tvpassbokName = dialog.findViewById(R.id.tv_passbooknm_name) as TextView
        val btnOk = dialog.findViewById(R.id.btn_ok) as Button

        dialog.tv_name.text = LabelSet.getText(
            "member_name",
            R.string.member_name
        )
        tvshdName.text = memberName
        tvpassbokName.text = et_nameinbankpassbook.text.toString()
        dialog.tvMemberNamePassbook.text = LabelSet.getText("nameinbankpassbook", R.string.nameinbankpassbook)
        btnOk.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()

    }

    private fun fillRadio() {
        dataspin_yesno = lookupViewmodel!!.getlookup(
            9,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        validate!!.fillradio(rgisDefault, 0, dataspin_yesno, this)
    }

    private fun bindBankBranchSpinner(sBankCode: Int) {
        dataspin_bankBranch = masterbankBranchViewmodel!!.getBankMaster(sBankCode)
        validate!!.fillMasterBankBranchspinner(this, spin_branch, dataspin_bankBranch, branch_id)
        if (branch_id > 0) {
            spin_branch.setSelection(
                validate!!.returnMasterBankBranchpos(
                    branch_id,
                    dataspin_bankBranch
                )
            )
        }
    }

    private fun fillSpinner() {
        dataspin_bank = bankMasterViewModel!!.getBankMaster()
        validate!!.fillMasterBankspinner(this, spin_bankname, dataspin_bank)

        dataspin_name = lookupViewmodel!!.getlookupfromkeycode(
            "MODE",
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        validate!!.fillspinner(this, spin_acctype, dataspin_name)
    }

    private fun showData() {

        var list =
            memberbankViewmodel!!.getBankdata(validate!!.RetriveSharepreferenceString(AppSP.MemberBankGUID))
        if (!list.isNullOrEmpty() && list.size > 0) {
            isVerified = validate!!.returnIntegerValue(list.get(0).is_verified.toString())
            if (isVerified == 1) {
                btn_add.text = LabelSet.getText(
                    "save_verify",
                    R.string.save_verify
                )
                lay_namePasbook.setBackgroundColor(Color.parseColor("#D8FFD8"))
                lay_bankName.setBackgroundColor(Color.parseColor("#D8FFD8"))
                lay_branch.setBackgroundColor(Color.parseColor("#D8FFD8"))
                lay_accountno.setBackgroundColor(Color.parseColor("#D8FFD8"))
                lay_opDate.setBackgroundColor(Color.parseColor("#D8FFD8"))
            } else {
                btn_add.text = LabelSet.getText(
                    "add_bank",
                    R.string.add_bank
                )
            }
            et_nameinbankpassbook.setText(validate!!.returnStringValue(list.get(0).bank_passbook_name))
            et_Accountno.setText(validate!!.returnStringValue(list.get(0).account_no))
            et_ifsc.setText(validate!!.returnStringValue(list.get(0).ifsc_code))
            et_opdate.setText(validate!!.convertDatetime(validate!!.returnLongValue(list.get(0).account_open_date.toString())))
//            et_cldate.setText(validate!!.convertDate(list.get(0).closing_date!!.toInt()))
            // spin_acctype.setSelection(validate!!.returnlookupcodepos(list.get(0).account_type!!.toInt(),dataspin_name))
            spin_bankname.setSelection(
                validate!!.returnMasterBankpos(
                    validate!!.returnIntegerValue(list.get(0).bank_id!!),
                    dataspin_bank
                )
            )
            branch_id = validate!!.returnIntegerValue(list.get(0).mem_branch_code)
            validate!!.fillradio(
                rgisDefault,
                validate!!.returnIntegerValue(list.get(0).is_default_account.toString()),
                dataspin_yesno,
                this
            )

            val mediaStorageDirectory = File(
                getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
                AppSP.IMAGE_DIRECTORY_NAME
            )
            var mediaFile1 =
                File(
                    mediaStorageDirectory.path + File.separator + validate!!.returnStringValue(
                        list.get(
                            0
                        ).passbook_firstpage
                    )
                )
            Picasso.with(this).load(mediaFile1).into(Imgshow)

            imageName = validate!!.returnStringValue(list.get(0).passbook_firstpage.toString())
            // tvUploadFiles.text=imageName
        } else {
            et_nameinbankpassbook.setText(memberName)
        }

    }

    private fun SaveBankDetail(isdefault: Int) {
        bankGUID = validate!!.random()
        if (isVerified == 1) {
            isVerified = 9
        } else {
            isVerified = 0
        }
        var accounttype = validate!!.returnlookupcode(spin_acctype, dataspin_name)
        var bankID = validate!!.returnMasterBankID(spin_bankname, dataspin_bank)
        var branchID = validate!!.returnMasterBankBranchID(spin_branch, dataspin_bankBranch)
        val mediaStorageDirectory = File(
            getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
            AppSP.IMAGE_DIRECTORY_NAME
        )
        if (validate!!.RetriveSharepreferenceString(AppSP.MemberBankGUID).isNullOrEmpty()) {

            if (imageName.length > 0) {
                var file = File(mediaStorageDirectory.path + File.separator + imageName)
                var newFile =
                    File(mediaStorageDirectory.path + File.separator + bankGUID + ",201.jpg")
                if (file.renameTo(newFile)) {

                    println("File move success")
                } else {
                    println("File move failed")
                }
                imageName = bankGUID + ",201.jpg"
            }
            memberBankEntity = MemberBankAccountEntity(
                0,
                membercode, 0,
                validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID),
                bankGUID,
                et_Accountno.text.toString(),
                bankID.toString(),
                0,
                accounttype.toString(),
                validate!!.Daybetweentime(et_opdate.text.toString()),
                isdefault,
                1,
                0,
                "",
                0,
                1,
                1,
                validate!!.Daybetweentime(validate!!.currentdatetime),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                0,
                "",
                0,
                "",
                et_nameinbankpassbook.text.toString(),
                imageName, et_ifsc.text.toString(), branchID.toString(), 1, 1, 1, "", 1
            )
            memberbankViewmodel!!.insert(memberBankEntity!!)
            validate!!.SaveSharepreferenceString(AppSP.MemberBankGUID, bankGUID)
            var kycimage1 = ImageuploadEntity(
                imageName,
                validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),
                validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID),

                3,
                0
            )
            if (imageName.isNotEmpty() && newimageName) {
                imageUploadViewmodel!!.insert(kycimage1)
            }
            CDFIApplication.database?.memberDao()
                ?.updateMemberdedup(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!)
            validate!!.updateMemberEditFlag(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),shgViewmodel,memberviewmodel)

            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this,
                MemberBankListActivity::class.java
            )
        } else {
            if (imageName.length > 0) {
                var file = File(mediaStorageDirectory.path + File.separator + imageName)
                var newFile = File(
                    mediaStorageDirectory.path + File.separator + validate!!.RetriveSharepreferenceString(
                        AppSP.MemberBankGUID
                    )!! + ",201.jpg"
                )
                if (file.renameTo(newFile)) {

                    println("File move success")
                } else {
                    println("File move failed")
                }
                imageName =
                    validate!!.RetriveSharepreferenceString(AppSP.MemberBankGUID)!! + ",201.jpg"
            }
            memberbankViewmodel!!.updateBankDetail(
                validate!!.RetriveSharepreferenceString(AppSP.MemberBankGUID)!!,
                et_Accountno.text.toString(),
                bankID.toString(),
                0,
                accounttype.toString(),
                branchID.toString(),
                validate!!.Daybetweentime(et_opdate.text.toString()).toString(),
                isdefault,
                1,
                "",
                "",
                0,
                et_nameinbankpassbook.text.toString(),
                1,
                validate!!.Daybetweentime(validate!!.currentdatetime),
                validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                imageName,
                1,
                et_ifsc.text.toString()
            )
            var kycimage1 = ImageuploadEntity(
                imageName,
                validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),
                validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID),

                3,
                0
            )
            if (imageName.isNotEmpty() && newimageName) {
                imageUploadViewmodel!!.insert(kycimage1)
            }
            CDFIApplication.database?.memberDao()
                ?.updateMemberdedup(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!)
            validate!!.updateMemberEditFlag(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),shgViewmodel,memberviewmodel)

            validate!!.CustomAlert(
                LabelSet.getText(
                    "updated_successfully",
                    R.string.updated_successfully
                ),
                this,
                MemberBankListActivity::class.java
            )
        }

    }

    private fun checkvalidation(): Int {
        var value = 1

        var isValidAccount = 0
        var bankID = validate!!.returnMasterBankID(spin_bankname, dataspin_bank)
        var branchID = validate!!.returnMasterBankBranchID(spin_branch, dataspin_bankBranch)
        var acc_count = memberbankViewmodel!!.getbank_acc_count(
            validate!!.returnStringValue(et_Accountno.text.toString()),
            validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!
        )
        var shgAcc_count = cboBankViewmodel!!.getbank_acc_count(
            validate!!.returnStringValue(et_Accountno.text.toString()), 0
        )
        var accountLengthAllowed = bankMasterViewModel!!.getAccountLength(bankID)
        var arr = validate!!.returnStringValue(accountLengthAllowed.toString()).split(",")
        var account_length = et_Accountno.text.toString().trim().length
        if (!arr.isNullOrEmpty()) {
            for (i in 0 until arr.size) {
                if (account_length == validate!!.returnIntegerValue(arr[i])) {
                    isValidAccount = 1
                    break
                } else {
                    isValidAccount = 0
                }
            }
        } else {
            isValidAccount = 0
        }


        if (et_nameinbankpassbook.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "nameinbankpassbook",
                    R.string.nameinbankpassbook
                ),
                this,
                et_nameinbankpassbook
            )
            value = 0
            return value
        }else  if(!memberName.equals(et_nameinbankpassbook.text.toString())){
            if(checkdeffName == true){
                showDialog()
                checkdeffName = false
                value = 0
                return value
            }
        } else if (et_ifsc.text.toString().length == 0 || et_ifsc.text.toString().length != 11) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "pleaseenetervalidifsc",
                    R.string.pleaseenetervalidifsc
                ),
                this,
                et_ifsc
            )
            value = 0
            return value
        } else if (spin_bankname.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_bankname,
                LabelSet.getText("selectbank", R.string.selectbank)
            )
            value = 0
            return value
        } else if (spin_branch.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_branch,
                LabelSet.getText(
                    "selectbranch",
                    R.string.selectbranch
                )
            )
            value = 0
            return value
        } else if ((validate!!.returnStringValue(accountLengthAllowed)
                .trim().length > 0 && isValidAccount == 0) || (validate!!.returnStringValue(
                accountLengthAllowed
            ).trim().length == 0 && et_Accountno.text.toString().length < 4)
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "valid_account_no",
                    R.string.valid_account_no
                ),
                this,
                et_Accountno
            )
            value = 0
            return value
        } else if ((validate!!.RetriveSharepreferenceString(AppSP.MemberBankGUID)!!
                .trim().length == 0 && acc_count > 0) ||
            (validate!!.RetriveSharepreferenceString(AppSP.MemberBankGUID)!!
                .trim().length > 0 && acc_count > 1)
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "acc_no_exists",
                    R.string.acc_no_exists
                ), this, et_Accountno
            )
            value = 0
            return value
        } else if (shgAcc_count > 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "acc_no_exists_inshg",
                    R.string.acc_no_exists_inshg
                ), this, et_Accountno
            )
            value = 0
            return value
        } else if (et_opdate.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "account_opening_date",
                    R.string.account_opening_date
                ),
                this,
                et_opdate
            )
            value = 0
            return value
        } else if (imageName.isEmpty()) {
            validate!!.CustomAlert(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "passbook_first_page",
                    R.string.passbook_first_page
                ),
                this
            )
            value = 0
            return value
        } else if ((validate!!.RetriveSharepreferenceString(AppSP.MemberBankGUID)!!
                .trim().length == 0 && acc_count > 0) ||
            (validate!!.RetriveSharepreferenceString(AppSP.MemberBankGUID)!!
                .trim().length > 0 && acc_count > 1)
        ) {
            validate!!.CustomAlert(
                LabelSet.getText(
                    "acc_no_exists",
                    R.string.acc_no_exists
                ), this
            )
            value = 0
            return value
        }

        return value
    }

    override fun onBackPressed() {
        var intent = Intent(this, MemberBankListActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    private fun checkPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        val result1 =
            ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val result2 =
            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission(requestCode: Int) {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ),
            requestCode
        )
    }

    fun captureimage(code: Int) {
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
                m.invoke(null)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//        fileUri = getOutputMediaFileUri(MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE, code)
        fileUri = FileProvider.getUriForFile(
            this, BuildConfig.APPLICATION_ID + ".provider", getOutputMediaFile(
                MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE, code
            )!!
        )
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
        startActivityForResult(intent, code)

    }

    fun getOutputMediaFileUri(type: Int, flag: Int): Uri {
        return Uri.fromFile(getOutputMediaFile(type, flag))
    }

    private fun getOutputMediaFile(type: Int, flag: Int): File? {

        // External sdcard location
        val mediaStorageDir = File(
            getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
            AppSP.IMAGE_DIRECTORY_NAME
        )
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null
            }
        }

        // Create a media file name
        val timeStamp = SimpleDateFormat(
            "yyyyMMdd_HHmmss",
            Locale.ENGLISH
        ).format(Date())
        var seqno = validate!!.RetriveSharepreferenceInt(AppSP.Seqno)
        imageName = "IMG_$timeStamp,2000$seqno.jpg"

        if (type == MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE) {
            mediaFile = File(
                mediaStorageDir.path + File.separator
                        + imageName
            )
            imgPathUpload = mediaStorageDir.path + File.separator + "IMG_" + timeStamp + ".jpg"


        } else {
            return null
        }

        return mediaFile
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            compreesedimage()
            newimageName = true
            bitmap = BitmapFactory.decodeFile(fileUri!!.path)
            if (requestCode == 101 && resultCode == Activity.RESULT_OK) {
                //ImgFrntpage.setImageBitmap(bitmap)
                Imgshow.setImageBitmap(bitmap)
                //  tvUploadFiles.text=imageName
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun compreesedimage() {
        val options = BitmapFactory.Options()
        var bitmap = BitmapFactory.decodeFile(fileUri!!.path, options)

        if (bitmap != null) {
            var actualWidth = options.outWidth
            var actualHeight = options.outHeight
            var mutableBitmap =
                Bitmap.createScaledBitmap(bitmap, actualWidth / 2, actualHeight / 2, false)

            var fOut: OutputStream? = null
            var file: File = File(fileUri!!.path)
            try {
                fOut = FileOutputStream(file)
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }
            mutableBitmap.compress(Bitmap.CompressFormat.JPEG, 50, fOut)
            try {
                fOut!!.flush()
                fOut.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
    }

    private fun ShowImage(name: String?) {
        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val layout: View = inflater.inflate(R.layout.popupwindow, null, false)
        val window = PopupWindow(
            layout,
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT,
            true
        )
        window.showAtLocation(layout, Gravity.CENTER, 0, 0)
        val cancel: ImageView = layout.findViewById(R.id.cancel)
        val image_preview: ImageView = layout.findViewById(R.id.image_preview)
        val mediaStorageDirectory = File(
            getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
            AppSP.IMAGE_DIRECTORY_NAME
        )
        var mediaFile1 = File(mediaStorageDirectory.path + File.separator + name)
        cancel.setOnClickListener {
            window.dismiss()
        }

        if (mediaFile1.exists()) {
            Picasso.with(this).load(mediaFile1).into(image_preview)
        } else {
            window.dismiss()
        }

    }

    fun importBankList(ifscCode: String) {
        var progressDialog: ProgressDialog = ProgressDialog.show(
            this, LabelSet.getText("app_name", R.string.app_name),
            LabelSet.getText(
                "fetchingbankdata",
                R.string.fetchingbankdata
            )
        )
        progressDialog.setIcon(this.getDrawable(R.drawable.appicon))


        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
        val token = validate!!.returnStringValue(
            AESEncryption.decrypt(
                validate!!.RetriveSharepreferenceString(AppSP.token),
                validate!!.RetriveSharepreferenceString(AppSP.userid)
            )
        )

        val call1 = apiInterface?.getBankList(
            token,
            user, ifscCode
        )

        call1?.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(callCount: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
                Toast.makeText(this@MemberBankDetailActivity, t.toString(), Toast.LENGTH_LONG)
                    .show()
            }

            override fun onResponse(
                call: Call<ResponseBody>,
                response: Response<ResponseBody>
            ) {
                progressDialog.dismiss()
                if (response.isSuccessful) {
                    var alldata = response.body()!!.source().readUtf8().toString()
                    val objectMapper = ObjectMapper()

                    val langList: List<Bank_branchEntity> =
                        objectMapper.readValue(
                            alldata,
                            object : TypeReference<List<Bank_branchEntity>>() {})
                    if (langList != null) {
                        if (langList.size == 0) {
                            validate!!.CustomAlert(
                                LabelSet.getText(
                                    "branchnotavialable",
                                    R.string.branchnotavialable
                                ), this@MemberBankDetailActivity
                            )
                        } else {
                            CDFIApplication.database?.masterbankbranchDao()
                                ?.insertbranch(
                                    langList
                                )
                            validate!!.CustomAlert(
                                LabelSet.getText(
                                    "branchdownlodedsuccessfully",
                                    R.string.branchdownlodedsuccessfully
                                ), this@MemberBankDetailActivity
                            )

                        }
                    }



                    dataspin_branch =
                        masterbankBranchViewmodel!!.BankBranchlistifsc(
                            et_ifsc.text.toString().toUpperCase()
                        )
                    if (!dataspin_branch.isNullOrEmpty()) {
                        branch_id = dataspin_branch!!.get(0).bank_branch_id
                        spin_bankname.setSelection(
                            validate!!.returnMasterBankpos(
                                validate!!.returnIntegerValue(dataspin_branch!!.get(0).bank_id.toString()),
                                dataspin_bank
                            )
                        )
                    }


                } else {

                    when (response.code()) {

                        403 ->
                            CustomAlertlogin()
                        else -> {
                            validate!!.CustomAlert(
                                LabelSet.getText(
                                    "branchnotavialable",
                                    R.string.branchnotavialable
                                ), this@MemberBankDetailActivity
                            )
                            Toast.makeText(
                                this@MemberBankDetailActivity,
                                response.message(),
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }

                }
            }
        })


    }


    fun importtoken(user: String, pass: String) {
        var grant_type = "password"
        var userId = user
        var password = pass
        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText(
                "authenticate_user",
                R.string.authenticate_user
            )
        )


        val callCount = apiInterface?.gettokenforce(
            grant_type,
            userId,
            password, validate!!.RetriveSharepreferenceInt(AppSP.State_Selected), 1,
            validate!!.RetriveSharepreferenceString(AppSP.Roleid)!!
        )
        callCount?.enqueue(object : Callback<JsonObject> {
            override fun onFailure(callCount: Call<JsonObject>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                progressDialog.dismiss()


                if (response.isSuccessful) {

                    var jsonObject1: JSONObject? = null
                    try {
                        jsonObject1 = JSONObject(response.body().toString())

                        val auth = jsonObject1.get("accessToken").toString()
                        if (auth.trim().length > 0) {
                            validate!!.SaveSharepreferenceString(
                                AppSP.token,
                                validate!!.returnStringValue(
                                    AESEncryption.encrypt(
                                        auth, validate!!.RetriveSharepreferenceString(AppSP.userid)
                                    )
                                )
                            )
                        }
                        validate!!.CustomAlert(
                            LabelSet.getText(
                                "userauthenticatedsuccessfully",
                                R.string.userauthenticatedsuccessfully
                            ), this@MemberBankDetailActivity
                        )
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Toast.makeText(
                            this@MemberBankDetailActivity,
                            response.message(),
                            Toast.LENGTH_LONG
                        ).show()


                    }

                } else {
                    Toast.makeText(
                        this@MemberBankDetailActivity,
                        response.message(),
                        Toast.LENGTH_LONG
                    )
                        .show()
                }


            }

        })


    }

    fun CustomAlertlogin() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogelogin, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mDialogView.etUsername.setText(validate!!.RetriveSharepreferenceString(AppSP.userid))
        mDialogView.etPassword.setText(
            validate!!.returnStringValue(
                AESEncryption.decrypt(
                    validate!!.RetriveSharepreferenceString(AppSP.Password),
                    validate!!.RetriveSharepreferenceString(AppSP.userid))))
        mDialogView.txt_dialog_title2.text = LabelSet.getText("app_name",R.string.app_name)
        mDialogView.btn_login.text = LabelSet.getText("log_in",R.string.log_in)
        mDialogView.chk_showHide1.text = LabelSet.getText("show_password",R.string.show_password)
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.btn_login.setOnClickListener {
            mAlertDialog.dismiss()
            importtoken(
                mDialogView.etUsername.text.toString(),
                validate!!.returnStringValue(
                    AESEncryption.encrypt(validate!!.returnStringValue(
                        mDialogView.etPassword.text.toString()),mDialogView.etUsername.text.toString()))
            )
        }
        mAlertDialog.chk_showHide1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                mAlertDialog.etPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                mAlertDialog.etPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
        }


    }

    private fun isNetworkConnected(): Boolean {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager //1
        val networkInfo = connectivityManager.activeNetworkInfo //2
        return networkInfo != null && networkInfo.isConnected //3
    }

    fun setLabelText() {
        tvBankPassbook.text = LabelSet.getText(
            "nameinbankpassbook",
            R.string.nameinbankpassbook
        )
        et_nameinbankpassbook.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_ifsc.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_Accountno.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )

        tvifsc_code.text = LabelSet.getText(
            "ifsc_code",
            R.string.ifsc_code
        )
        tvBankName.text = LabelSet.getText(
            "bank_name",
            R.string.bank_name
        )
        tvBankBranch.text = LabelSet.getText(
            "bank_branch",
            R.string.bank_branch
        )
        tvAccountType.text = LabelSet.getText(
            "account_type",
            R.string.account_type
        )
        tvAccountNo.text = LabelSet.getText(
            "account_no",
            R.string.account_no
        )
        tv_AccountOpenDate.text = LabelSet.getText(
            "account_opening_date",
            R.string.account_opening_date
        )
        tvUploadPassbook.text = LabelSet.getText(
            "upload_passbook_first_page",
            R.string.upload_passbook_first_page
        )
        tvISDefault.text = LabelSet.getText(
            "is_default",
            R.string.is_default
        )
//        rdYes.setText(LabelSet.getText("yes",R.string.yes))
//        rdNo.setText(LabelSet.getText("no",R.string.no))
        btn_add.text = LabelSet.getText(
            "add_bank",
            R.string.add_bank
        )
        btn_addgray.text = LabelSet.getText(
            "add_bank",
            R.string.add_bank
        )
    }

    fun CustomAlertisdefault() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = LabelSet.getText(
            "doyouwanttomakedeualt",
            R.string.doyouwanttomakedeualt
        )
        mDialogView.txt_dialog_title.text = LabelSet.getText("app_name", R.string.app_name)
        mDialogView.btn_yes.text = LabelSet.getText("yes", R.string.yes)
        mDialogView.btn_no.text = LabelSet.getText("no", R.string.no)
        mDialogView.btn_yes.setOnClickListener {
            CDFIApplication.database?.memberbanckDao()
                ?.updateisdefault(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!)
            SaveBankDetail(1)
            mAlertDialog.dismiss()


        }
        mDialogView.btn_no.setOnClickListener {
            SaveBankDetail(0)
            mAlertDialog.dismiss()

        }
    }

    private fun checkData(): Int {
        var value = 1
        var bankID = validate!!.returnMasterBankID(spin_bankname, dataspin_bank)
        var branchID = validate!!.returnMasterBankBranchID(spin_branch, dataspin_bankBranch)
        if (!validate!!.RetriveSharepreferenceString(AppSP.MemberBankGUID).isNullOrEmpty()) {

            var list =
                memberbankViewmodel!!.getBankdata(validate!!.RetriveSharepreferenceString(AppSP.MemberBankGUID))
                if (validate!!.returnStringValue(et_nameinbankpassbook.text.toString()) != validate!!.returnStringValue(
                        list?.get(0)?.bank_passbook_name
                    )
                ) {

                    value = 0
                } else if (et_ifsc.text.toString() != validate!!.returnStringValue(list?.get(0)?.ifsc_code)) {

                    value = 0
                } else if (et_Accountno.text.toString() != validate!!.returnStringValue(list?.get(0)?.account_no)) {

                    value = 0

                } else if (validate!!.Daybetweentime(et_opdate.text.toString()) != validate!!.returnLongValue(
                        list?.get(0)?.account_open_date.toString()
                    )
                ) {

                    value = 0
                } else if (bankID != validate!!.returnIntegerValue(
                        list?.get(
                            0
                        )?.bank_id
                    )
                ) {

                    value = 0

                }  else if (branchID != validate!!.returnIntegerValue(list?.get(0)?.mem_branch_code)) {

                    value = 0

                }   else if (imageName != validate!!.returnStringValue(list?.get(0)?.passbook_firstpage.toString())) {

                    value = 0

                } else if (rgisDefault.checkedRadioButtonId != validate!!.returnIntegerValue(
                        list?.get(
                            0
                        )?.is_default_account.toString()
                    )
                ) {

                    value = 0

                }

        } else {
            value = 0

        }
        return value
    }
}
