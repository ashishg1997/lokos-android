package com.microware.cdfi.activity

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.MemberBankAdapter
import com.microware.cdfi.entity.MemberBankAccountEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_member_bank_list.*
import kotlinx.android.synthetic.main.activity_member_list.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.fragment_address_detaillist.rvList
import kotlinx.android.synthetic.main.tablayout.*
import kotlinx.android.synthetic.main.white_toolbar.*

class MemberBankListActivity : AppCompatActivity() {

    var validate: Validate? = null
    var memberBankEntity : MemberBankAccountEntity? = null
    var memberbankViewmodel : MemberBankViewmodel? = null
    var bankMasterViewModel : MasterBankViewmodel? = null
    var masterBankBranchViewModel : MasterBankBranchViewModel? = null
    var memberviewmodel: Memberviewmodel? = null
    var memberPhoneViewmodel: MemberPhoneViewmodel? = null
    var memberkycviewmodel: MemberKYCViewmodel? = null
    var memberaddressviewmodel: MemberAddressViewmodel? = null
    var membersystemviewmodel: MemberSystemtagViewmodel? = null
    var cadreMemberViewModel: CadreMemberViewModel? = null
    var shgViewmodel: SHGViewmodel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_member_bank_list)

        validate = Validate(this)
        memberbankViewmodel = ViewModelProviders.of(this).get(MemberBankViewmodel::class.java)
        bankMasterViewModel = ViewModelProviders.of(this).get(MasterBankViewmodel::class.java)
        masterBankBranchViewModel = ViewModelProviders.of(this).get(MasterBankBranchViewModel::class.java)
        memberviewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)
        memberPhoneViewmodel = ViewModelProviders.of(this).get(MemberPhoneViewmodel::class.java)
        memberkycviewmodel = ViewModelProviders.of(this).get(MemberKYCViewmodel::class.java)
        memberaddressviewmodel = ViewModelProviders.of(this).get(MemberAddressViewmodel::class.java)
        membersystemviewmodel = ViewModelProviders.of(this).get(MemberSystemtagViewmodel::class.java)
        shgViewmodel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        cadreMemberViewModel = ViewModelProviders.of(this).get(CadreMemberViewModel::class.java)

        ivBack.setOnClickListener {
            var intent = Intent(this,MemberListActivity::class.java)
            intent.flags=Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

        ivHome.visibility = View.GONE
        if (validate!!.RetriveSharepreferenceInt(AppSP.MemberLockRecord) == 1) {
            ivLock.visibility = View.VISIBLE
            addBankgray.visibility = View.VISIBLE
            addBank.visibility = View.GONE

        } else {
            ivLock.visibility = View.GONE
            addBankgray.visibility = View.GONE
            addBank.visibility = View.VISIBLE
        }
        tv_title.text =
            validate!!.RetriveSharepreferenceString(AppSP.memebrname)!! + "(" + validate!!.RetriveSharepreferenceString(AppSP.ShgName) + ")"

        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvCader.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvVector.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimary))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_cader.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        var memberlist =
            memberviewmodel!!.getmember(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
        var memberphonelist =
            memberPhoneViewmodel!!.getphoneDatalistcount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
        var memberaddresslist =
            memberaddressviewmodel!!.getAddressdatalistcount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
        var memberkyclist =
            memberkycviewmodel!!.getKycdetaildatalistcount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
        var membersystetmtaglist =
            membersystemviewmodel!!.getSystemtagdatalistcount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))

        var cadreshgMemberlist = cadreMemberViewModel!!.getCadreListdata1(
            validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!)
        if (!cadreshgMemberlist.isNullOrEmpty()){
            IvCader.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }


        if (!memberlist.isNullOrEmpty()){
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberaddresslist.isNullOrEmpty()){
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberkyclist.isNullOrEmpty()){
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!membersystetmtaglist.isNullOrEmpty()){
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberphonelist.isNullOrEmpty()){
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        lay_systemTag.setOnClickListener {
            var intent = Intent(this, MemberGroupTagList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_vector.setOnClickListener {
            var intent = Intent(this,MemberDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_phone.setOnClickListener {
            var intent = Intent(this,MemeberPhoneListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_location.setOnClickListener {
            var intent = Intent(this,MemberAddressListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_kyc.setOnClickListener {
            var intent = Intent(this,MemberIdListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }
        lay_Cader.setOnClickListener {
            var intent = Intent(this, CadreListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        addBank.setOnClickListener {
            if (!validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID).isNullOrEmpty()) {
                validate!!.SaveSharepreferenceInt(AppSP.MemberBankStatus,0)
                validate!!.SaveSharepreferenceString(AppSP.MemberBankGUID, "")
                var intent = Intent(this, MemberBankDetailActivity::class.java)
                startActivity(intent)
                finish()
            }else{
                validate!!.CustomAlert(LabelSet.getText(
                    "add_memeber_data_first",
                    R.string.add_memeber_data_first
                ),this,MemberDetailActivity::class.java)
            }
        }


        if(memberbankViewmodel!!.getVerificationCount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))>0){
            btn_VerifyBank.visibility = View.VISIBLE
        }else {
            btn_VerifyBank.visibility = View.GONE
        }
        btn_VerifyBank.setOnClickListener {
            var isCompleteCount = memberbankViewmodel!!.getCompletionCount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
            if(isCompleteCount==0){
                memberbankViewmodel!!.updateIsVerifed(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID),validate!!.Daybetweentime(validate!!.currentdatetime),
                    validate!!.RetriveSharepreferenceString(AppSP.userid)!!)
                validate!!.CustomAlert(LabelSet.getText(
                    "",
                    R.string.verified_successfully
                ),this)
                if(memberbankViewmodel!!.getVerificationCount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))>0){
                    btn_VerifyBank.visibility = View.VISIBLE
                }else {
                    btn_VerifyBank.visibility = View.GONE
                }
            }else {
                validate!!.CustomAlert(LabelSet.getText(
                    "",
                    R.string.completerecord
                ),this)
            }
        }
        fillData()

    }

    fun getBankName(bankID : Int?): String?{
        var value: String? = null
        value = bankMasterViewModel!!.getBankName(bankID!!)
        return value
    }

    fun getBranchname(branchid : Int): String?{
        var value: String? = null
        value = masterBankBranchViewModel!!.getBranchname(branchid)
        return value
    }

    private fun fillData() {
        memberbankViewmodel!!.getBankdetaildata(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))!!
            .observe(this,object : Observer<List<MemberBankAccountEntity>> {
                override fun onChanged(member_list: List<MemberBankAccountEntity>?) {
                    if (member_list!=null){
                        rvList.layoutManager = LinearLayoutManager(this@MemberBankListActivity)
                        rvList.adapter = MemberBankAdapter(this@MemberBankListActivity, member_list)
                        if(member_list.size>0){
                            lay_nobank_avialable.visibility=View.GONE
                        }else {
                            lay_nobank_avialable.visibility=View.VISIBLE
                            tv_nobank_avialable.text = LabelSet.getText(
                                "no_bank_s_avialable",
                                R.string.no_bank_s_avialable
                            )
                        }
                    }
                }
            })
    }

    override fun onBackPressed() {
        var intent = Intent(this,MemberListActivity::class.java)
        intent.flags=Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    fun CustomAlert(guid:String,iFlag:Int) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = LabelSet.getText(
            "do_u_want_to_delete",
            R.string.do_u_want_to_delete
        )
        mDialogView.txt_dialog_title.text = LabelSet.getText("app_name", R.string.app_name)
        mDialogView.btn_yes.text = LabelSet.getText("yes", R.string.yes)
        mDialogView.btn_no.text = LabelSet.getText("no", R.string.no)
        mDialogView.btn_yes.setOnClickListener {

            if(iFlag==0){
                memberbankViewmodel!!.deleteRecord(guid)
            }else {
                memberbankViewmodel!!.deleteData(guid)

            }
            validate!!.updateMemberEditFlag(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),shgViewmodel,memberviewmodel)

            mAlertDialog.dismiss()

        }
        mDialogView.btn_no.setOnClickListener {

            mAlertDialog.dismiss()

        }
    }
}