package com.microware.cdfi.activity

import android.app.AlertDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.RadioGroup
import android.widget.Spinner
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.ShgMemberListAdapter
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.entity.MemberDesignationEntity
import com.microware.cdfi.entity.MemberEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_member_designation.*
import kotlinx.android.synthetic.main.activity_member_designation.view.*
import kotlinx.android.synthetic.main.member_designation__actvity.rvList
import kotlinx.android.synthetic.main.tablayoutgroup.*
import kotlinx.android.synthetic.main.white_toolbar.*

class MemberDesignationActvity : AppCompatActivity() {

    var validate: Validate? = null
    var shgViewmodel: SHGViewmodel? = null
    var addressViewmodel: CboAddressViewmodel? = null
    var phoneViewmodel: CboPhoneViewmodel? = null
    var memberPhoneViewmodel: MemberPhoneViewmodel? = null
    var bank: CboBankViewmodel? = null
    var systemtagViewmodel: SystemtagViewmodel? = null
    var memberDesignationViewmodel: MemberDesignationViewmodel? = null
    var lookupViewmodel: LookupViewmodel? = null
    var data_post: List<LookupEntity>? = null
    var memberviewmodel: Memberviewmodel? = null

    var datayes: List<LookupEntity>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.member_designation__actvity)
        validate = Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        memberviewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)


        memberDesignationViewmodel =
            ViewModelProviders.of(this).get(MemberDesignationViewmodel::class.java)

        shgViewmodel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        addressViewmodel = ViewModelProviders.of(this).get(CboAddressViewmodel::class.java)
        phoneViewmodel = ViewModelProviders.of(this).get(CboPhoneViewmodel::class.java)
        bank = ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        systemtagViewmodel = ViewModelProviders.of(this).get(SystemtagViewmodel::class.java)
        memberPhoneViewmodel = ViewModelProviders.of(this).get(MemberPhoneViewmodel::class.java)

        ivBack.setOnClickListener {
            var intent = Intent(this, ShgListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }
        tv_title.text =
            validate!!.RetriveSharepreferenceString(AppSP.ShgName)!! + " (" + validate!!.RetriveSharepreferenceString(
                AppSP.Shgcode
            ) + " )"
        if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) == 1) {
            ivLock.visibility = View.VISIBLE
        } else {
            ivLock.visibility = View.GONE
        }
        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvVector.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this, R.color.white))

        var shglist = shgViewmodel!!.getSHG(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var banklist =
            bank!!.getBankdatalistcount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var addresslist = addressViewmodel!!.getAddressDatalistcount(
            validate!!.RetriveSharepreferenceString(
                AppSP.SHGGUID
            )
        )
        var systemlist = systemtagViewmodel!!.getSystemtagdatalistcount(
            validate!!.RetriveSharepreferenceString(
                AppSP.SHGGUID
            )
        )
        var phonelist =
            phoneViewmodel!!.getuploadlistcount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))

        if (!shglist.isNullOrEmpty()) {
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))


        } else {
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))


        }
        if (!banklist.isNullOrEmpty()) {
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        } else {
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        if (!addresslist.isNullOrEmpty()) {
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        } else {
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        if (!phonelist.isNullOrEmpty()) {
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        } else {
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        if (!systemlist.isNullOrEmpty()) {
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        } else {
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        lay_systemTag.setOnClickListener {
            var intent = Intent(this, GroupSystemTagList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_vector.setOnClickListener {
            var intent = Intent(this, BasicDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_location.setOnClickListener {
            var intent = Intent(this, AddressDetailListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_bank.setOnClickListener {
            var intent = Intent(this, BankDetailListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_phone.setOnClickListener {
            var intent = Intent(this, PhoneDetailListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        fillData()
    }

    private fun fillData() {

        data_post = lookupViewmodel!!.getlookup(
            52, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        if (!data_post.isNullOrEmpty() && data_post!!.size > 0) {

            rvList.layoutManager = LinearLayoutManager(this@MemberDesignationActvity)
            rvList.adapter = ShgMemberListAdapter(this@MemberDesignationActvity, data_post)

        }

    }

    override fun onBackPressed() {
        var intent = Intent(this, ShgListActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }


    fun OpenDesignationDialog(memberGUId: String, designationId: Int, PostTitle: String) {
        var memberDesignation: MemberDesignationEntity? = null

        var dataspin_belong: List<MemberEntity>? = null
        var member_joining_date = 0L
        var signatoryvalue = 0
        var lookupviewmodel: LookupViewmodel? = null
        var datastatus: List<LookupEntity>? = null

        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.activity_member_designation, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        lookupviewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        mDialogView.tv_Member.text = LabelSet.getText("name_hi",R.string.name_hi)
        mDialogView.tvstatus.text = LabelSet.getText("status",R.string.status)
        mDialogView.tv_lay_fromDate.text = LabelSet.getText("from_date",R.string.from_date)
        mDialogView.tvToDate.text = LabelSet.getText("to_Date",R.string.to_Date)
        mDialogView.tv_signatory.text = LabelSet.getText("signatory",R.string.signatory)
        mDialogView.btn_addDes.text = LabelSet.getText("add_designation",R.string.add_designation)
        mDialogView.btn_addgray.text = LabelSet.getText("add_designation",R.string.add_designation)
        datastatus = lookupviewmodel.getlookup(
            5,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        validate!!.fillspinner(this, mAlertDialog.spin_status, datastatus)

        mAlertDialog.spin_status?.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {

                override fun onItemSelected(
                    parent: AdapterView<*>, view: View?,
                    position: Int, id: Long
                ) {

                    if (position > 0) {
                        var status =
                            validate!!.returnlookupcode(mAlertDialog.spin_status, datastatus)
                        if (status == 2) {
                            mAlertDialog.et_toDate.isEnabled = true
                            mAlertDialog.et_toDate.setText(
                                validate!!.convertDatetime(
                                    validate!!.Daybetweentime(
                                        validate!!.currentdatetime
                                    )
                                )
                            )
                        } else {
                            mAlertDialog.et_toDate.isEnabled = false
                            mAlertDialog.et_toDate.setText("")
                        }
                    } else {
                        mAlertDialog.et_toDate.setText("")
                    }

                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // TODO Auto-generated method stub

                }
            }

        mAlertDialog.et_toDate.setOnClickListener {
            validate!!.datePickerwithmindate(
                validate!!.Daybetweentime(mAlertDialog.et_fromDate.text.toString()),
                mAlertDialog.et_toDate
            )
        }

        mAlertDialog.et_fromDate.setOnClickListener {
            if (mAlertDialog.spin_Member.selectedItemPosition > 0) {

                var list = memberDesignationViewmodel!!.getMemberDesignationdata(
                    validate!!.RetriveSharepreferenceString(
                        AppSP.SHGGUID
                    ).toString(), designationId
                )
                if (!list.isNullOrEmpty()) {
                    validate!!.datePickerwithmindate(
                        list[0].to_date!!,
                        mAlertDialog.et_fromDate
                    )
                } else {
                    validate!!.datePickerwithmindate(
                        member_joining_date,
                        mAlertDialog.et_fromDate
                    )
                }
            } else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "pleaseselectmember",
                        R.string.pleaseselectmember
                    ), this
                )
            }
        }
        mAlertDialog.lokos_core.text = PostTitle

        datayes = lookupViewmodel!!.getlookup(
            9,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        mAlertDialog.btn_addDes.setOnClickListener {
            var currentmemguid = returnmemguid(
                dataspin_belong!!,
                mAlertDialog.spin_Member
            )
            if (memberDesignation != null) {

                if (memberGUId.equals(currentmemguid)) {
                    if (mAlertDialog.lay_status.visibility == View.VISIBLE && validate!!.returnlookupcode(
                            mAlertDialog.spin_status,
                            datastatus
                        ) == 0
                    ) {
                        validate!!.CustomAlertSpinner(
                            this, mAlertDialog.spin_status,
                            LabelSet.getText(
                                "please_select",
                                R.string.please_select
                            ) + " " + LabelSet.getText(
                                "status",
                                R.string.status
                            )
                        )
                    } else if (validate!!.returnlookupcode(
                            mAlertDialog.spin_status,
                            datastatus
                        ) == 2 && mAlertDialog.et_toDate.text.toString().trim().length == 0
                    ) {
                        validate!!.CustomAlert(
                            LabelSet.getText(
                                "Pleaseentertodate",
                                R.string.Pleaseentertodate
                            ), this
                        )
                    } else {
                        memberDesignation!!.is_edited = 1
                        memberDesignation!!.is_complete = 1
                        memberDesignation!!.is_signatory =
                            mAlertDialog.rgsignatory.checkedRadioButtonId
                        memberDesignation!!.member_guid = currentmemguid
                        memberDesignation!!.from_date =
                            validate!!.Daybetweentime(mAlertDialog.et_fromDate.text.toString())
                        if (mAlertDialog.lay_toDate.visibility == View.VISIBLE) {
                            memberDesignation!!.to_date =
                                validate!!.Daybetweentime(mAlertDialog.et_toDate.text.toString())
                        }
                        if (mAlertDialog.lay_status.visibility == View.VISIBLE) {
                            memberDesignation!!.status =
                                validate!!.returnlookupcode(mAlertDialog.spin_status, datastatus)
                            if (validate!!.returnlookupcode(
                                    mAlertDialog.spin_status,
                                    datastatus
                                ) == 1
                            ) {
                                CDFIApplication.database?.memberDao()
                                    ?.updateMemberpost(currentmemguid, designationId)
                            } else {
                                CDFIApplication.database?.memberDao()
                                    ?.updateMemberpost(currentmemguid, 4)
                            }
                        } else {
                            CDFIApplication.database?.memberDao()
                                ?.updateMemberpost(currentmemguid, designationId)
                        }
                        memberDesignation!!.updated_by =
                            validate!!.RetriveSharepreferenceString(AppSP.userid)
                        memberDesignationViewmodel!!.insert(memberDesignation!!)
                        CDFIApplication.database?.shgDao()
                            ?.updateisedit(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!)
                        validate!!.updateLockingFlag(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!,
                            shgViewmodel,memberviewmodel)
                        mAlertDialog.dismiss()
                        fillData()
                    }
                } else {
                    if (mAlertDialog.spin_Member.selectedItemPosition == 0) {
                        validate!!.CustomAlert(
                            LabelSet.getText(
                                "pleaseselectmember",
                                R.string.pleaseselectmember
                            ), this
                        )
                    } else if (mAlertDialog.et_fromDate.text.toString().length == 0) {
                        validate!!.CustomAlert(
                            LabelSet.getText(
                                "Pleaseenterfromdate",
                                R.string.Pleaseenterfromdate
                            ), this
                        )
                    } else if (memberDesignationViewmodel!!.getMembercount(currentmemguid) > 0) {
                        validate!!.CustomAlert(
                            LabelSet.getText(
                                "thismemberalreadyhaveapost",
                                R.string.thismemberalreadyhaveapost
                            ), this
                        )
                    } else {

                        memberDesignation = MemberDesignationEntity()
                        // memberDesignation!!.cbo_code= shgcode
                        memberDesignation!!.member_designation_guid = validate!!.random()
                        memberDesignation!!.member_guid = currentmemguid
                        memberDesignation!!.cbo_guid =
                            validate!!.RetriveSharepreferenceString(AppSP.SHGGUID).toString()
                        memberDesignation!!.designation = designationId
                        memberDesignation!!.from_date =
                            validate!!.Daybetweentime(mAlertDialog.et_fromDate.text.toString())
                        memberDesignation!!.to_date = 0
                        memberDesignation!!.last_uploaded_date = 0
                        memberDesignation!!.created_date =
                            validate!!.Daybetweentime(validate!!.currentdatetime)
                        memberDesignation!!.updated_date =
                            validate!!.Daybetweentime(validate!!.currentdatetime)
                        memberDesignation!!.created_by =
                            validate!!.RetriveSharepreferenceString(AppSP.userid)
                        memberDesignation!!.status = 1
                        memberDesignation!!.is_active = 1
                        memberDesignation!!.is_edited = 1
                        memberDesignation!!.is_complete = 1
                        memberDesignation!!.entry_source = 1
                        memberDesignation!!.is_signatory =
                            mAlertDialog.rgsignatory.checkedRadioButtonId
                        memberDesignationViewmodel!!.insert(memberDesignation!!)
                        CDFIApplication.database?.memberDesignationDao()
                            ?.updatestatus(memberGUId, memberDesignation!!.from_date!!)
                        CDFIApplication.database?.shgDao()?.updateisedit(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!)
                        validate!!.updateLockingFlag(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!,
                            shgViewmodel,memberviewmodel)
                        CDFIApplication.database?.memberDao()
                            ?.updateMemberpost(currentmemguid, designationId)
                        CDFIApplication.database?.memberDao()
                            ?.updateMemberpost(memberGUId, 4)
                        mAlertDialog.dismiss()
                        fillData()
                    }


                }
            } else {
                if (mAlertDialog.spin_Member.selectedItemPosition == 0) {
                    validate!!.CustomAlert(
                        LabelSet.getText(
                            "pleaseselectmember",
                            R.string.pleaseselectmember
                        ), this
                    )
                } else if (mAlertDialog.et_fromDate.text.toString().length == 0) {
                    validate!!.CustomAlert(
                        LabelSet.getText(
                            "Pleaseenterfromdate",
                            R.string.Pleaseenterfromdate
                        ), this
                    )
                } else if (memberDesignationViewmodel!!.getMembercount(currentmemguid) > 0) {
                    validate!!.CustomAlert(
                        LabelSet.getText(
                            "thismemberalreadyhaveapost",
                            R.string.thismemberalreadyhaveapost
                        ), this
                    )
                } else {

                    memberDesignation = MemberDesignationEntity()
                    // memberDesignation!!.cbo_code= shgcode
                    memberDesignation!!.member_designation_guid = validate!!.random()
                    memberDesignation!!.member_guid = currentmemguid
                    memberDesignation!!.cbo_guid =
                        validate!!.RetriveSharepreferenceString(AppSP.SHGGUID).toString()
                    memberDesignation!!.designation = designationId
                    memberDesignation!!.from_date =
                        validate!!.Daybetweentime(mAlertDialog.et_fromDate.text.toString())
                    memberDesignation!!.to_date = 0
                    memberDesignation!!.last_uploaded_date = 0
                    memberDesignation!!.created_date =
                        validate!!.Daybetweentime(validate!!.currentdatetime)
                    memberDesignation!!.updated_date =
                        validate!!.Daybetweentime(validate!!.currentdatetime)
                    memberDesignation!!.status = 1
                    memberDesignation!!.created_by =
                        validate!!.RetriveSharepreferenceString(AppSP.userid)
                    memberDesignation!!.is_active = 1
                    memberDesignation!!.is_edited = 1
                    memberDesignation!!.is_complete = 1
                    memberDesignation!!.entry_source = 1
                    memberDesignation!!.is_signatory = mAlertDialog.rgsignatory.checkedRadioButtonId
                    memberDesignationViewmodel!!.insert(memberDesignation!!)
                    CDFIApplication.database?.shgDao()
                        ?.updateisedit(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!)
                    validate!!.updateLockingFlag(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!,
                        shgViewmodel,memberviewmodel)
                    CDFIApplication.database?.memberDao()
                        ?.updateMemberpost(currentmemguid, designationId)
                    mAlertDialog.dismiss()
                    fillData()
                }

            }
        }

        dataspin_belong =
            memberviewmodel!!.getAllMemberlist(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        if (dataspin_belong != null)
            validate!!.fillmemberDesspinner(this, mAlertDialog.spin_Member, dataspin_belong)
        mAlertDialog.spin_Member.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {

                override fun onItemSelected(
                    parent: AdapterView<*>, view: View?,
                    position: Int, id: Long
                ) {

                    if (position > 0) {
                        var curr_memguid = returnmemguid(
                            dataspin_belong!!,
                            mAlertDialog.spin_Member
                        )
                        if (!memberGUId.equals(curr_memguid)) {
                            mAlertDialog.lay_status.visibility = View.GONE
                            mAlertDialog.lay_toDate.visibility = View.GONE
                        } else {
                            mAlertDialog.lay_status.visibility = View.VISIBLE
                            mAlertDialog.lay_toDate.visibility = View.VISIBLE
                        }
                        member_joining_date = returnmember_joining(
                            dataspin_belong,
                            mAlertDialog.spin_Member
                        )

                    }

                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // TODO Auto-generated method stub

                }
            }


        memberDesignation = memberDesignationViewmodel!!.getMemberDesignationdata(
            validate!!.RetriveSharepreferenceString(AppSP.SHGGUID).toString(),
            memberGUId
        )
        validate!!.fillradio(mAlertDialog.rgsignatory, 0, datayes, this)
        if (memberDesignation != null) {
            mAlertDialog.spin_Member.setSelection(
                setMember(
                    validate!!.returnStringValue(
                        memberDesignation!!.member_guid
                    ), dataspin_belong
                )
            )
            mAlertDialog.et_fromDate.setText(
                validate!!.convertDatetime(
                    validate!!.returnLongValue(
                        memberDesignation!!.from_date.toString()
                    )
                )
            )
            signatoryvalue = validate!!.returnIntegerValue(
                memberDesignation!!.is_signatory.toString()
            )
            validate!!.fillradio(
                mAlertDialog.rgsignatory, validate!!.returnIntegerValue(
                    memberDesignation!!.is_signatory.toString()
                ), datayes, this
            )
            if (validate!!.returnIntegerValue(memberDesignation!!.status.toString()) > 0) {
                mAlertDialog.spin_status.setSelection(
                    validate!!.returnlookupcodepos(
                        validate!!.returnIntegerValue(
                            memberDesignation!!.status.toString()
                        ), datastatus
                    )
                )
            }
            mAlertDialog.et_toDate.setText(
                validate!!.convertDatetime(
                    validate!!.returnLongValue(
                        memberDesignation!!.to_date.toString()
                    )
                )
            )

        } else {
            mAlertDialog.lay_status.visibility = View.GONE
            mAlertDialog.lay_toDate.visibility = View.GONE
        }
        mAlertDialog.rgsignatory.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->

            if (checkedId == 1) {
                var memberphonelist = memberPhoneViewmodel!!.getphoneDatalistcount(
                    returnmemguid(
                        dataspin_belong!!,
                        mAlertDialog.spin_Member
                    )
                )
                if (!memberphonelist.isNullOrEmpty()) {
                    if (signatoryvalue != 1) {
                        var count = memberDesignationViewmodel!!.getactivesignatorycount(
                            validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!
                        )
                        if (count == 3) {
                            validate!!.fillradio(mAlertDialog.rgsignatory, 2, datayes, this)
                            validate!!.CustomAlert(
                                LabelSet.getText(
                                    "already3signatoryinthisgroup",
                                    R.string.already3signatoryinthisgroup
                                ), this
                            )
                        }
                    }
                } else {
                    validate!!.fillradio(mAlertDialog.rgsignatory, 2, datayes, this)
                    validate!!.CustomAlert(
                        LabelSet.getText(
                            "pleaseupdatephonenointhismember",
                            R.string.pleaseupdatephonenointhismember
                        ), this
                    )
                }
            }

        })

    }


    fun setMember(memberGuid: String, data: List<MemberEntity>?): Int {


        var pos = 0

        if (!data.isNullOrEmpty()) {
            for (i in data.indices) {
                if (memberGuid.equals(data.get(i).member_guid))
                    pos = i + 1
            }
        }
        return pos
    }


    fun returnmemguid(
        dataspin_belong: List<MemberEntity>,
        spinMember: Spinner
    ): String {

        var pos = spinMember.selectedItemPosition
        var id = ""

        if (!dataspin_belong.isNullOrEmpty()) {
            if (pos > 0) id = dataspin_belong.get(pos - 1).member_guid
        }
        return id
    }

    fun returnmember_joining(
        dataspin_belong: List<MemberEntity>,
        spinMember: Spinner
    ): Long {

        var pos = spinMember.selectedItemPosition
        var joining: Long = 0L

        if (!dataspin_belong.isNullOrEmpty()) {
            if (pos > 0) joining = dataspin_belong.get(pos - 1).joining_date!!
        }
        return joining
    }


}