package com.microware.cdfi.activity

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.text.Editable
import android.text.TextWatcher
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.google.zxing.integration.android.IntentIntegrator
import com.gpfreetech.aadhaarkyc.AadhaarParser
import com.microware.cdfi.BuildConfig
import com.microware.cdfi.R
import com.microware.cdfi.api.AdharModels.Poi
import com.microware.cdfi.api.AdharModels.SampleEntity
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.*
import com.microware.cdfi.viewModel.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main_web_view.*
import kotlinx.android.synthetic.main.activity_member_detail.*
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.buttons.btn_cancel
import kotlinx.android.synthetic.main.buttons.btn_save
import kotlinx.android.synthetic.main.customaadharalertdialoge.view.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.designation_dialog_item.view.*
import kotlinx.android.synthetic.main.post_designation_dialog.*
import kotlinx.android.synthetic.main.tablayout.*
import kotlinx.android.synthetic.main.tablayout.lay_bank
import kotlinx.android.synthetic.main.white_toolbar.*
import kotlinx.android.synthetic.main.digitalolocker_data_info.view.*
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import com.gpfreetech.aadhaarkyc.entity.AadhaarUser
import com.microware.cdfi.utility.*

class MemberDetailActivity : AppCompatActivity() {

    var validate: Validate? = null
    var backup: Backup? = null
    var memberViewmodel: Memberviewmodel? = null
    var shgViewmodel: SHGViewmodel? = null
    var imageUploadViewmodel: ImageUploadViewmodel? = null
    var memberEntity: MemberEntity? = null
    var guid = ""
    var isVerified = 0
    var membername = ""
    var membercode = 0L
    var shgcode = ""
    var cboname = ""
    var age_yr = 0
    var age = 0
    var age_comp = 0
    var disability_type = 0
    var seqno = 0
    var kycEntrySource: Int? = 0
    var lookupViewmodel: LookupViewmodel? = null
    var cadreMemberViewModel: CadreMemberViewModel? = null
    var dataspin_name: List<LookupEntity>? = null
    var dataspin_religion: List<LookupEntity>? = null
    var dataradioDisable: List<LookupEntity>? = null
    var dataradio_isDOB: List<LookupEntity>? = null
    var dataradio_head: List<LookupEntity>? = null
    var dataradio_pvtg: List<LookupEntity>? = null
    var dataspin_gender: List<LookupEntity>? = null
    var dataspin_maritial: List<LookupEntity>? = null
    var dataspin_disabletype: List<LookupEntity>? = null
    var dataspin_guardianRelation: List<LookupEntity>? = null
    var dataspin_post: List<LookupEntity>? = null
    var data_Designation: List<LookupEntity>? = null
    var dataspin_education: List<LookupEntity>? = null
    var dataspin_category: List<LookupEntity>? = null
    var dataspin_motherrekation: List<LookupEntity>? = null
    var dataspin_status: List<LookupEntity>? = null
    var dataspin_inactivestatus: List<LookupEntity>? = null
    var dataspin_settlement: List<LookupEntity>? = null
    val LAUNCH_SECOND_ACTIVITY = 1
    var imageName = ""
    var adharno = ""
    var addline1 = ""
    var namelocal = ""
    var addline2 = ""
    var pincode = 0
    var relation_db = 0
    var newimageName = false
    var adharscan = false
    var adharscandigi = false
    var bitmap: Bitmap? = null
    var nameMatch = 0
    private var fileUri: Uri? = null
    var imgPathUpload = ""
    var mediaFile: File? = null
    var membersystemviewmodel: MemberSystemtagViewmodel? = null
    var memberPhoneViewmodel: MemberPhoneViewmodel? = null
    var memberbankviewmodel: MemberBankViewmodel? = null
    var memberaddressviewmodel: MemberAddressViewmodel? = null
    var memberkycviewmodel: MemberKYCViewmodel? = null
    var restaurantObject: SampleEntity? = null
    var flag = false


    var languageCode: String = ""
    var mSpeechRecognizer: SpeechRecognizer? = null
    var is_listening = false
    var is_lang_change = false
    var mSpeechRecognizerIntent: Intent? = null
    private val REQ_CODE_SPEECH_INPUT = 100
    private var CURRENT_SELECTED_EDITTEXT = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_member_detail)
        validate = Validate(this)
        setLabelText()
        ivHome.visibility = View.GONE
        // ivProfile.visibility = View.VISIBLE
        tv_title.text = LabelSet.getText(
            "memberdetail",
            R.string.memberdetail
        )

        ivBack.setOnClickListener {
            //            CustomAlertPartialsave(getString(R.string.doyouwanttosavepartialdata),MemberListActivity::class.java)
            if (checkValidation() == 1) {
                PartialSaveData()
                var intent = Intent(this, MemberListActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                finish()
            }
        }


        backup = Backup(this)
        imageUploadViewmodel = ViewModelProviders.of(this).get(ImageUploadViewmodel::class.java)
        memberViewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        membersystemviewmodel =
            ViewModelProviders.of(this).get(MemberSystemtagViewmodel::class.java)
        memberPhoneViewmodel = ViewModelProviders.of(this).get(MemberPhoneViewmodel::class.java)
        memberbankviewmodel = ViewModelProviders.of(this).get(MemberBankViewmodel::class.java)
        memberaddressviewmodel = ViewModelProviders.of(this).get(MemberAddressViewmodel::class.java)
        memberkycviewmodel = ViewModelProviders.of(this).get(MemberKYCViewmodel::class.java)
        shgViewmodel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        cadreMemberViewModel = ViewModelProviders.of(this).get(CadreMemberViewModel::class.java)

        shgcode = validate!!.RetriveSharepreferenceString(AppSP.Shgcode)!!
        cboname = validate!!.RetriveSharepreferenceString(AppSP.ShgName)!!
        spin_gender.setSelection(
            validate!!.returnlookupcodepos(
                2,
                dataspin_gender
            )
        )
        spin_gender.isEnabled = validate!!.RetriveSharepreferenceInt(AppSP.CboType) != 1

//        membername = et_name.text.toString()

        et_groupcode.setText(shgcode.toString())
        et_groupname.setText(cboname)
        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvCader.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvVector.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_cader.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        var systemtagdatalistlist =
            membersystemviewmodel!!.getSystemtagdatalistcount(
                validate!!.RetriveSharepreferenceString(
                    AppSP.MEMBERGUID
                )
            )
        var memberphonelist =
            memberPhoneViewmodel!!.getphoneDatalistcount(
                validate!!.RetriveSharepreferenceString(
                    AppSP.MEMBERGUID
                )
            )
        var memberaddresslist =
            memberaddressviewmodel!!.getAddressdatalistcount(
                validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)
            )
        var memberbanklist =
            memberbankviewmodel!!.getBankdetaildatalistcount(
                validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)
            )
        var memberkyclist =
            memberkycviewmodel!!.getKycdetaildatalistcount(
                validate!!.RetriveSharepreferenceString(
                    AppSP.MEMBERGUID
                )
            )
        var cadreshgMemberlist = cadreMemberViewModel!!.getCadreListdata1(
            validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!
        )
        if (!cadreshgMemberlist.isNullOrEmpty()) {
            IvCader.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!systemtagdatalistlist.isNullOrEmpty()) {
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberaddresslist.isNullOrEmpty()) {
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberbanklist.isNullOrEmpty()) {
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberkyclist.isNullOrEmpty()) {
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberphonelist.isNullOrEmpty()) {
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (validate!!.RetriveSharepreferenceInt(AppSP.MemberLockRecord) == 1) {
            ivLock.visibility = View.VISIBLE
            btn_savegray.visibility = View.VISIBLE
            btn_save.visibility = View.GONE

        } else {
            ivLock.visibility = View.GONE
            btn_savegray.visibility = View.GONE
            btn_save.visibility = View.VISIBLE
        }
        lay_phone.setOnClickListener {
            //            CustomAlertPartialsave(getString(R.string.doyouwanttosavepartialdata),MemeberPhoneListActivity::class.java)
            if (checkValidation() == 1) {
                PartialSaveData()
                var intent = Intent(this, MemeberPhoneListActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_location.setOnClickListener {
            //            CustomAlertPartialsave(getString(R.string.doyouwanttosavepartialdata),MemberAddressListActivity::class.java)
            if (checkValidation() == 1) {
                PartialSaveData()
                var intent = Intent(this, MemberAddressListActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_bank.setOnClickListener {
            //            CustomAlertPartialsave(getString(R.string.doyouwanttosavepartialdata),MemberBankListActivity::class.java)

            if (checkValidation() == 1) {
                PartialSaveData()
                var intent = Intent(this, MemberBankListActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_kyc.setOnClickListener {
            //            CustomAlertPartialsave(getString(R.string.doyouwanttosavepartialdata),MemberIdListActivity::class.java)
            if (checkValidation() == 1) {
                PartialSaveData()
                var intent = Intent(this, MemberIdListActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_systemTag.setOnClickListener {
            //            CustomAlertPartialsave(getString(R.string.doyouwanttosavepartialdata),MemberGroupTagList::class.java)
            if (checkValidation() == 1) {
                PartialSaveData()
                var intent = Intent(this, MemberGroupTagList::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_Cader.setOnClickListener {
            //            CustomAlertPartialsave(getString(R.string.doyouwanttosavepartialdata),MemberGroupTagList::class.java)
            if (checkValidation() == 1) {
                PartialSaveData()
                var intent = Intent(this, CadreListActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        et_dob.setOnClickListener {
            validate!!.datePicker(et_dob)
        }
        et_dob.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (et_dob.text.toString().trim().length > 0) {
                    // validate!!.SetAnswerTypeRadioButton(rgisDobavailable, 1)
                }
                if (validate!!.returnStringValue(et_dob.text.toString()).length > 0 && validate!!.returnStringValue(
                        et_joiningDate.text.toString()
                    ).length > 0
                ) {
                    var age_value = backup!!.AgeCalculator(
                        et_dob.text.toString(),
                        et_joiningDate.text.toString()
                    )
                    age_yr = validate!!.returnIntegerValue(age_value.split("-")[0])
                    //    et_age.setText(age_yr.toString())
                    if (age_yr < 18 || disability_type == 5) {
                        lay_guardian.visibility = View.VISIBLE
                    } else {
                        lay_guardian.visibility = View.GONE
                        et_guardianName.setText("")
                        et_guardianNamelocal.setText("")
                        spin_relationGuardian.setSelection(0)
                    }
                }
            }
        })

        et_age.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (et_age.text.toString().trim().length > 0
                ) {
                    age = validate!!.returnIntegerValue(et_age.text.toString())
                    if (age < 18 || disability_type == 5) {
                        lay_guardian.visibility = View.VISIBLE
                    } else {
                        lay_guardian.visibility = View.GONE
                        et_guardianName.setText("")
                        et_guardianNamelocal.setText("")
                        spin_relationGuardian.setSelection(0)
                    }
                }
            }
        })

        spin_disabilityType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View?,
                position: Int,
                id: Long
            ) {
                // Display the selected item text on text view
                //    iRelationId = returnID(8,position)
                if (position > 0) {
                    if (et_dob.text.toString().trim().length > 0) {
                        var age_value = backup!!.AgeCalculator(
                            et_dob.text.toString(),
                            et_joiningDate.text.toString()
                        )
                        age_yr = validate!!.returnIntegerValue(age_value.split("-")[0])
                        age_comp = age_yr
                    } else if (et_age.text.toString().trim().length > 0) {
                        age_comp = validate!!.returnIntegerValue(et_age.text.toString())
                    }
                    disability_type =
                        validate!!.returnlookupcode(spin_disabilityType, dataspin_disabletype)
                    if (disability_type == 4 || disability_type == 5 || age_comp < 18) {
                        lay_guardian.visibility = View.VISIBLE
                    } else {
                        lay_guardian.visibility = View.GONE
                        et_guardianName.setText("")
                        et_guardianNamelocal.setText("")
                        spin_relationGuardian.setSelection(0)
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Another interface callback
            }
        }
        et_joiningDate.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (validate!!.returnStringValue(et_dob.text.toString()).length > 0 && validate!!.returnStringValue(
                        et_joiningDate.text.toString()
                    ).length > 0
                ) {
                    var age = backup!!.AgeCalculator(
                        et_dob.text.toString(),
                        et_joiningDate.text.toString()
                    )
                    var age_yr = validate!!.returnIntegerValue(age.split("-")[0])
                    if (age_yr < 18) {
                        lay_guardian.visibility = View.VISIBLE
                    } else {
                        lay_guardian.visibility = View.GONE
                        et_guardianName.setText("")
                        et_guardianNamelocal.setText("")
                        spin_relationGuardian.setSelection(0)
                    }
                }
            }
        })

        et_joiningDate.setOnClickListener {
            validate!!.datePickerwithmindate(
                validate!!.RetriveSharepreferenceLong(AppSP.Formation_dt),
                et_joiningDate
            )
        }

        et_leavingDate.setOnClickListener {
            validate!!.datePicker(et_leavingDate)
        }
        et_ason.setOnClickListener {
            val myCalendar = Calendar.getInstance()
            var date = "01-01-" + myCalendar[Calendar.YEAR]
            validate!!.datePickerwithmindate(validate!!.Daybetweentime(date), et_ason)
        }


        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                if (checkData() == 0) {
                    SaveDetail()
                } else {
                    validate!!.CustomAlert(
                        LabelSet.getText(
                            "data_saved_successfully",
                            R.string.data_saved_successfully
                        ),
                        this,
                        MemeberPhoneListActivity::class.java
                    )
                }
            }
        }

        btn_cancel.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.MemberLockRecord) == 1) {
                var intent = Intent(this, MemberListActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                finish()
            } else {
                CustomAlertPartialsave(
                    LabelSet.getText(
                        "doyouwanttoexit",
                        R.string.doyouwanttoexit
                    ), MemberListActivity::class.java
                )
            }
        }

        spin_socialCategory.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View?,
                position: Int,
                id: Long
            ) {
                // Display the selected item text on text view
                //    iRelationId = returnID(8,position)
                if (position > 0) {
                    var category =
                        validate!!.returnlookupcode(spin_socialCategory, dataspin_category)
                    if (category == 2) {
                        linearPVTG.visibility = View.VISIBLE
                    } else {
                        linearPVTG.visibility = View.GONE
                        SetAnswerTypeRadioReset(rgpvtg, "")
                    }
                } else {
                    linearPVTG.visibility = View.GONE
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Another interface callback
            }
        }

        imgscanadhar.setOnClickListener {
            flag = true
            val integrator = IntentIntegrator(this)
            integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE)
            integrator.setPrompt("Scan a Aadharcard QR Code")
            integrator.setCameraId(0) // Use a specific camera of the device
            integrator.captureActivity = OrientationCaptureActivity::class.java
            integrator.setOrientationLocked(false)
            integrator.setBeepEnabled(false)

            integrator.initiateScan()

        }

        /*    imgscanadhar.setOnClickListener {
                var intent = Intent(this, ScanAadharActivity::class.java)
                startActivityForResult(intent, LAUNCH_SECOND_ACTIVITY)
            }*/

        ivAdd.setOnClickListener {
            AddPostDesignation()
        }

        //      fillSpinner()
        spin_occupation?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {
                    spin_secondaryoccu.isEnabled = true
                    var p_occupation = validate!!.returnlookupcode(spin_occupation, dataspin_name)
                    if (p_occupation == validate!!.returnlookupcode(
                            spin_secondaryoccu,
                            dataspin_name
                        )
                    ) {
                        validate!!.CustomAlertSpinner(
                            this@MemberDetailActivity,
                            spin_occupation,
                            LabelSet.getText(
                                "primary_secondary",
                                R.string.primary_secondary
                            )
                        )
                    }
                    if (p_occupation == validate!!.returnlookupcode(
                            spin_tertiaryoccu,
                            dataspin_name
                        )
                    ) {
                        validate!!.CustomAlertSpinner(
                            this@MemberDetailActivity,
                            spin_occupation,
                            LabelSet.getText(
                                "primary_tertiary",
                                R.string.primary_tertiary
                            )
                        )
                    }
                } else {
                    spin_secondaryoccu.isEnabled = false
                    spin_tertiaryoccu.isEnabled = false
                    spin_secondaryoccu.setSelection(0)
                    spin_tertiaryoccu.setSelection(0)
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        spin_secondaryoccu?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {
                    spin_tertiaryoccu.isEnabled = true
                    var s_occupation =
                        validate!!.returnlookupcode(spin_secondaryoccu, dataspin_name)
                    if (s_occupation == validate!!.returnlookupcode(
                            spin_occupation,
                            dataspin_name
                        )
                    ) {
                        validate!!.CustomAlertSpinner(
                            this@MemberDetailActivity,
                            spin_secondaryoccu,
                            LabelSet.getText(
                                "secondary_primary",
                                R.string.secondary_primary
                            )
                        )
                    } else if (s_occupation == validate!!.returnlookupcode(
                            spin_tertiaryoccu,
                            dataspin_name
                        )
                    ) {
                        validate!!.CustomAlertSpinner(
                            this@MemberDetailActivity,
                            spin_secondaryoccu,
                            LabelSet.getText(
                                "secondary_tertiary",
                                R.string.secondary_tertiary
                            )
                        )
                    }
                } else {
                    spin_tertiaryoccu.isEnabled = false
                    spin_tertiaryoccu.setSelection(0)
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        spin_tertiaryoccu?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {
                    var t_occupation = validate!!.returnlookupcode(spin_tertiaryoccu, dataspin_name)
                    if (t_occupation == validate!!.returnlookupcode(
                            spin_occupation,
                            dataspin_name
                        )
                    ) {
                        validate!!.CustomAlertSpinner(
                            this@MemberDetailActivity,
                            spin_tertiaryoccu,
                            LabelSet.getText(
                                "tertiary_primary",
                                R.string.tertiary_primary
                            )
                        )
                    } else if (t_occupation == validate!!.returnlookupcode(
                            spin_secondaryoccu,
                            dataspin_name
                        )
                    ) {
                        validate!!.CustomAlertSpinner(
                            this@MemberDetailActivity,
                            spin_tertiaryoccu,
                            LabelSet.getText(
                                "tertiary_secondary",
                                R.string.tertiary_secondary
                            )
                        )
                    }
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        spin_status?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {
                    var status = validate!!.returnlookupcode(spin_status, dataspin_status)
                    if (status == 1) {
                        lay_status.visibility = View.GONE
                        et_leavingDate.setText("")
                        spin_inactivereason.setSelection(0)
                        spin_settlementstatus.setSelection(0)
                    } else {
                        lay_status.visibility = View.VISIBLE
                    }
                } else {
                    lay_status.visibility = View.GONE
                    et_leavingDate.setText("")
                    spin_inactivereason.setSelection(0)
                    spin_settlementstatus.setSelection(0)
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        Imgmember.setOnClickListener {
            if (!checkPermission()) {
                requestPermission(101)
            } else {
                captureimage(101)
            }
        }
        tblimage.setOnClickListener {
            if (imageName.isNotEmpty()) {
                ShowImage(imageName)
            }

        }

        imgDigital.setOnClickListener {
            if (validate!!.isNetworkConnected(this)) {
                val intent = Intent(
                    this,
                    MainWebViewActivity::class.java
                )
                startActivityForResult(intent, 201)
            } else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "internetnotavialablepleasrtrywithqr",
                        R.string.internetnotavialablepleasrtrywithqr
                    ),
                    this
                )
            }
        }
        validate!!.disableEmojiInTitle(et_mother_fatherlocal, 100)
        validate!!.disableEmojiInTitle(et_guardianNamelocal, 100)
        validate!!.disableEmojiInTitle(et_namelocal, 100)
        fillSpinner()

        showData()
        //  setLabelText()

        languageCode = CDFIApplication.validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
        iv_mic_name_eng.setOnClickListener {
            CURRENT_SELECTED_EDITTEXT = 1
            promptSpeechInput("en")
        }

        iv_mic_name_local.setOnClickListener {
            CURRENT_SELECTED_EDITTEXT = 2
            promptSpeechInput(languageCode)
        }

        iv_mic_relate_eng.setOnClickListener {
            CURRENT_SELECTED_EDITTEXT = 3
            promptSpeechInput("en")
        }

        iv_mic_relate_local.setOnClickListener {
            CURRENT_SELECTED_EDITTEXT = 4
            promptSpeechInput(languageCode)
        }

        iv_mic_guard_eng.setOnClickListener {
            CURRENT_SELECTED_EDITTEXT = 5
            promptSpeechInput("en")
        }

        iv_mic_guard_local.setOnClickListener {
            CURRENT_SELECTED_EDITTEXT = 6
            promptSpeechInput(languageCode)
        }
    }

    private fun fillSpinner() {
        dataradioDisable = lookupViewmodel!!.getlookup(
            95,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataradio_isDOB = lookupViewmodel!!.getlookup(
            9,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataradio_head = lookupViewmodel!!.getlookup(
            9,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataradio_pvtg = lookupViewmodel!!.getlookup(
            9,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_name = lookupViewmodel!!.getlookup(
            7,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_religion = lookupViewmodel!!.getlookup(
            42,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_status = lookupViewmodel!!.getlookup(
            5,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_settlement = lookupViewmodel!!.getlookup(
            27,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_inactivestatus = lookupViewmodel!!.getlookup(
            41,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_gender = lookupViewmodel!!.getlookup(
            4,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_maritial = lookupViewmodel!!.getlookup(
            26, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_disabletype = lookupViewmodel!!.getlookup(
            46, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_guardianRelation = lookupViewmodel!!.getlookup(
            25,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_post = lookupViewmodel!!.getlookup(
            1, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        data_Designation = lookupViewmodel!!.getlookup(
            52, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_education = lookupViewmodel!!.getlookup(
            8, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_category = lookupViewmodel!!.getlookup(
            2, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_motherrekation = lookupViewmodel!!.getlookup(
            24, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        validate!!.fillspinner(this, spin_relationmother, dataspin_motherrekation)
        validate!!.fillspinner(this, spin_gender, dataspin_gender)
        validate!!.fillspinner(this, spin_martial, dataspin_maritial)
        validate!!.fillspinner(this, spin_disabilityType, dataspin_disabletype)
        validate!!.fillspinner(this, spin_relationGuardian, dataspin_guardianRelation)
        validate!!.fillspinner(this, spin_Post, dataspin_post)
        validate!!.fillspinner(this, spin_education, dataspin_education)
        validate!!.fillspinner(this, spin_occupation, dataspin_name)
        validate!!.fillspinner(this, spin_secondaryoccu, dataspin_name)
        validate!!.fillspinner(this, spin_tertiaryoccu, dataspin_name)
        validate!!.fillspinner(this, spin_socialCategory, dataspin_category)
        validate!!.fillspinner(this, spin_religion, dataspin_religion)
        validate!!.fillspinner(this, spin_inactivereason, dataspin_inactivestatus)
        validate!!.fillspinner(this, spin_status, dataspin_status)
        validate!!.fillspinner(this, spin_settlementstatus, dataspin_settlement)
        validate!!.fillradio(rgDisability, 1, dataradioDisable, this)
        validate!!.fillradio(rgisDobavailable, 0, dataradio_isDOB, this)
        validate!!.fillradio(rgheadofFamily, 0, dataradio_head, this)
        validate!!.fillradio(rgpvtg, 0, dataradio_pvtg, this)
        spin_gender.setSelection(
            validate!!.returnlookupcodepos(
                2,
                dataspin_gender
            )
        )
        spin_gender.isEnabled = validate!!.RetriveSharepreferenceInt(AppSP.CboType) != 1


        rgDisability.setOnCheckedChangeListener(
            RadioGroup.OnCheckedChangeListener { group, checkedId ->
                var disabilityID = rgDisability.checkedRadioButtonId
                if (disabilityID == 2 || disabilityID == 3) {
                    lay_disabilitytype.visibility = View.VISIBLE
                } else {
                    lay_disabilitytype.visibility = View.GONE
                    spin_disabilityType.setSelection(0)
                    if (age_comp > 18) {
                        lay_guardian.visibility = View.GONE
                        et_guardianName.setText("")
                        et_guardianNamelocal.setText("")
                    }
                }
            })

        rgisDobavailable.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->
            // checkedId is the RadioButton selected
            var isDob = rgisDobavailable.checkedRadioButtonId

            if (isDob == 1) {
                lay_dob.visibility = View.VISIBLE
                lay_age.visibility = View.GONE
                et_ason.setText("")
                lay_asOn.visibility = View.GONE
            } else {
                lay_dob.visibility = View.GONE
                lay_age.visibility = View.VISIBLE
                et_ason.setText(validate!!.convertDatetime(validate!!.Daybetweentime(validate!!.currentdatetime)))
                lay_asOn.visibility = View.VISIBLE
            }
        })
        spin_martial?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                var relation =
                    validate!!.returnlookupcode(spin_relationmother, dataspin_motherrekation)

                if (position > 0) {
                    var marital_status =
                        validate!!.returnlookupcode(spin_martial, dataspin_maritial)
                    if (marital_status == 1 && relation_db == 0 && spin_gender.selectedItemPosition == 2) {
                        spin_relationmother.setSelection(3)
                    }

                    if (marital_status == 2 && relation == 3) {
                        validate!!.CustomAlertSpinner(
                            this@MemberDetailActivity, spin_martial,
                            LabelSet.getText(
                                "marital_not_allowed",
                                R.string.marital_not_allowed
                            )
                        )
                    }
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        spin_relationmother?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                var marital_status =
                    validate!!.returnlookupcode(spin_martial, dataspin_maritial)
                if (position > 0) {
                    var relation =
                        validate!!.returnlookupcode(spin_relationmother, dataspin_motherrekation)
                    if (marital_status == 2 && relation == 3) {
                        validate!!.CustomAlertSpinner(
                            this@MemberDetailActivity, spin_relationmother,
                            LabelSet.getText(
                                "relation_not_allowed",
                                R.string.relation_not_allowed
                            )
                        )
                    }
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }
    }

    private fun showData() {
        var list =
            memberViewmodel!!.getMemberdetail(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
        spin_Post.isEnabled = false
        if (!list.isNullOrEmpty() && list.size > 0) {
            isVerified = validate!!.returnIntegerValue(list.get(0).is_verified.toString())
            if (isVerified == 1) {
                btn_save.text = LabelSet.getText(
                    "save_verify",
                    R.string.save_verify
                )
                lay_name.setBackgroundColor(Color.parseColor("#D8FFD8"))
                lay_gender.setBackgroundColor(Color.parseColor("#D8FFD8"))
                lay_isDob.setBackgroundColor(Color.parseColor("#D8FFD8"))
                lay_dob.setBackgroundColor(Color.parseColor("#D8FFD8"))
                lay_joingDate.setBackgroundColor(Color.parseColor("#D8FFD8"))
                lay_motherFather.setBackgroundColor(Color.parseColor("#D8FFD8"))
                lay_post.setBackgroundColor(Color.parseColor("#D8FFD8"))
                lay_socialCategory.setBackgroundColor(Color.parseColor("#D8FFD8"))
                lay_religion.setBackgroundColor(Color.parseColor("#D8FFD8"))
            } else {
                btn_save.text = LabelSet.getText(
                    "save",
                    R.string.save
                )
            }
            kycEntrySource = memberkycviewmodel!!.getKycEntrySource(
                validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID),
                2
            )
            if (kycEntrySource == 1 || kycEntrySource == 2 || kycEntrySource == 4) {
                et_name.isEnabled = false
                spin_gender.isEnabled = false
            } else {
                et_name.isEnabled = true
                spin_gender.isEnabled = true
            }


            et_name.setText(validate!!.returnStringValue(list.get(0).member_name))
            et_membercode.setText(validate!!.returnStringValue(list.get(0).member_code.toString()))
            /* validate!!.SetAnswerTypeRadioButton(
                 rgisDobavailable,
                 validate!!.returnIntegerValue(list?.get(0)?.dob_available.toString())
             )
             validate!!.SetAnswerTypeRadioButton(
                 rgheadofFamily,
                 validate!!.returnIntegerValue(list?.get(0)?.head_house_hold.toString())
             )
             validate!!.SetAnswerTypeRadioButton(
                 rgpvtg,
                 validate!!.returnIntegerValue(list?.get(0)?.tribal_category.toString())
             )*/
            validate!!.fillradio(
                rgisDobavailable,
                validate!!.returnIntegerValue(list.get(0).dob_available.toString()),
                dataradio_isDOB,
                this
            )
            validate!!.fillradio(
                rgheadofFamily,
                validate!!.returnIntegerValue(list.get(0).head_house_hold.toString()),
                dataradio_head,
                this
            )
            validate!!.fillradio(
                rgpvtg,
                validate!!.returnIntegerValue(list.get(0).tribal_category.toString()),
                dataradio_pvtg,
                this
            )
            et_dob.setText(validate!!.convertDatetime(validate!!.returnLongValue(list.get(0).dob.toString())))
            et_joiningDate.setText(validate!!.convertDatetime(validate!!.returnLongValue(list.get(0).joining_date.toString())))
            if (validate!!.returnLongValue(list.get(0).leaving_date.toString()) > 0) {
                et_leavingDate.setText(
                    validate!!.convertDatetime(
                        validate!!.returnLongValue(
                            list.get(
                                0
                            ).leaving_date.toString()
                        )
                    )
                )
            }
            spin_gender.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(list.get(0).gender.toString()),
                    dataspin_gender
                )
            )
            spin_martial.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(list.get(0).marital_status.toString()),
                    dataspin_maritial
                )
            )
            /*  validate!!.SetAnswerTypeRadioButton(
                  rgDisability,
                  validate!!.returnIntegerValue(list?.get(0)?.is_disabled.toString())
              )*/
            validate!!.fillradio(
                rgDisability,
                validate!!.returnIntegerValue(list.get(0).is_disabled.toString()),
                dataradioDisable,
                this
            )

            spin_disabilityType.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(list.get(0).disability_details),
                    dataspin_disabletype
                )
            )
            spin_Post.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(list.get(0).designation.toString()),
                    dataspin_post
                )
            )
            spin_education.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(list.get(0).highest_education_level.toString()),
                    dataspin_education
                )
            )
            spin_socialCategory.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(list.get(0).social_category.toString()),
                    dataspin_category
                )
            )
            spin_religion.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(list.get(0).religion.toString()),
                    dataspin_religion
                )
            )
            spin_status.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(list.get(0).status.toString()),
                    dataspin_status
                )
            )
            if (validate!!.returnIntegerValue(list.get(0).age.toString()) > 0) {
                et_age.setText(validate!!.returnIntegerValue(list.get(0).age.toString()).toString())
            }
            et_mother_father.setText(validate!!.returnStringValue(list.get(0).relation_name))
            et_mother_fatherlocal.setText(validate!!.returnStringValue(list.get(0).relation_name_local))
            et_ason.setText(validate!!.convertDatetime(validate!!.returnLongValue(list.get(0).age_as_on.toString())))
            spin_relationmother.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(list.get(0).father_husband.toString()),
                    dataspin_motherrekation
                )
            )
            et_guardianName.setText(validate!!.returnStringValue(list.get(0).guardian_name))
            et_guardianNamelocal.setText(validate!!.returnStringValue(list.get(0).guardian_name_local))
            spin_relationGuardian.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(list.get(0).guardian_relation.toString()),
                    dataspin_guardianRelation
                )
            )
            spin_inactivereason.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(list.get(0).reason_for_leaving.toString()),
                    dataspin_inactivestatus
                )
            )
            spin_settlementstatus.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(list.get(0).settlement_status.toString()),
                    dataspin_settlement
                )
            )
            spin_occupation.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(list.get(0).primary_occupation.toString()),
                    dataspin_name
                )
            )
            spin_secondaryoccu.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(list.get(0).secondary_occupation.toString()),
                    dataspin_name
                )
            )
            spin_tertiaryoccu.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(list.get(0).tertiary_occupation.toString()),
                    dataspin_name
                )
            )
            val mediaStorageDirectory = File(
                getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
                AppSP.IMAGE_DIRECTORY_NAME
            )
            var mediaFile1 =
                File(mediaStorageDirectory.path + File.separator + list.get(0).member_image)
            Picasso.with(this).load(mediaFile1).into(Imgshow)
            imageName = validate!!.returnStringValue(list.get(0).member_image)
            namelocal = validate!!.returnStringValue(list.get(0).member_name_local)
            if (namelocal.isNotEmpty()) {
                lay_namelocal.visibility = View.VISIBLE
                et_namelocal.setText(namelocal)
            } else {
                lay_namelocal.visibility = View.VISIBLE
            }
            //  tvUploadFiles.setText(imageName)
            seqno = validate!!.returnIntegerValue(list.get(0).seq_no.toString())
            relation_db = validate!!.returnIntegerValue(list.get(0).father_husband.toString())
            spin_status.isEnabled = true
        } else {
            validate!!.fillradio(rgDisability, 1, dataradioDisable, this)
            validate!!.fillradio(rgisDobavailable, 1, dataradio_isDOB, this)
            validate!!.fillradio(rgheadofFamily, 0, dataradio_head, this)
            /*validate!!.fillradio(rgpvtg,0,dataradio_pvtg,this )
            validate!!.SetAnswerTypeRadioButton(
                rgisDobavailable,
                1
            )
            validate!!.SetAnswerTypeRadioButton(
                rgheadofFamily,
                0
            )
            validate!!.SetAnswerTypeRadioButton(
                rgDisability,
                0
            )
*/
            spin_Post.setSelection(
                validate!!.returnlookupcodepos(
                    4,
                    dataspin_post
                )
            )
            et_joiningDate.setText(validate!!.convertDatetime(validate!!.Daybetweentime(validate!!.currentdatetime)))
            spin_status.isEnabled = false
            spin_status.setSelection(
                validate!!.returnlookupcodepos(
                    1,
                    dataspin_status
                )
            )
        }
    }

    private fun SaveDetail() {
        guid = validate!!.random()
        if (isVerified == 1 || isVerified == 3) {
            isVerified = 3
        } else if (isVerified == 9) {
            isVerified = 9
        } else {
            isVerified = 0
        }
        var age = 0
        var ageAson = 0L
        var dob = 0L
        var pvtgID: Int? = 0
        var minor: Int? = 0
        var disabilityID = rgDisability.checkedRadioButtonId
        var headID = rgheadofFamily.checkedRadioButtonId



        if (linearPVTG.visibility == View.VISIBLE) {
            pvtgID = rgpvtg.checkedRadioButtonId
        } else {
            pvtgID = null
        }
        var gender = validate!!.returnlookupcode(spin_gender, dataspin_gender)
        var settlement_status =
            validate!!.returnlookupcode(spin_settlementstatus, dataspin_settlement)
        var martialstatus = validate!!.returnlookupcode(spin_martial, dataspin_maritial)
        var religion = validate!!.returnlookupcode(spin_religion, dataspin_religion)
        var category = validate!!.returnlookupcode(spin_socialCategory, dataspin_category)
        var educationlevel = validate!!.returnlookupcode(spin_education, dataspin_education)
        var disailitytype = validate!!.returnlookupcode(spin_disabilityType, dataspin_disabletype)
        var status = validate!!.returnlookupcode(spin_status, dataspin_status)
        var designation = validate!!.returnlookupcode(spin_Post, dataspin_post)
        var motherrelation =
            validate!!.returnlookupcode(spin_relationmother, dataspin_motherrekation)
        var inactivereasn =
            validate!!.returnlookupcode(spin_inactivereason, dataspin_inactivestatus)
        var guardianrelation =
            validate!!.returnlookupcode(spin_relationGuardian, dataspin_guardianRelation)
        var occupation = validate!!.returnlookupcode(spin_occupation, dataspin_name)
        var secondary_occupation = validate!!.returnlookupcode(spin_secondaryoccu, dataspin_name)
        var tertiary_occupation = validate!!.returnlookupcode(spin_tertiaryoccu, dataspin_name)
        membername = et_name.text.toString()

        if (rgisDobavailable.checkedRadioButtonId == 1) {
            dob = validate!!.Daybetweentime(et_dob.text.toString())
            age = validate!!.getage(validate!!.currentdatetime, et_dob.text.toString())
            ageAson = validate!!.Daybetweentime(validate!!.currentdatetime)
        } else if (rgisDobavailable.checkedRadioButtonId == 0) {
            age = validate!!.returnIntegerValue(et_age.text.toString())
            ageAson = validate!!.Daybetweentime(et_ason.text.toString())
            dob = validate!!.Daybetweentime(validate!!.getdob(et_ason.text.toString(), age))
        }

        if (validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID).isNullOrEmpty()) {
            seqno =
                memberViewmodel!!.getseqno(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)) + 1

            memberEntity = MemberEntity(
                0,
                membercode,
                shgcode,
                validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),
                membercode,
                guid,
                seqno,
                membername,
                validate!!.returnStringValue(et_namelocal.text.toString()),
                motherrelation,
                et_mother_father.text.toString(),
                validate!!.returnStringValue(et_mother_fatherlocal.text.toString()),
                gender,
                martialstatus,
                religion,
                category,
                pvtgID,
                0, "", 0, 0, educationlevel,
                rgisDobavailable.checkedRadioButtonId,
                dob,
                age,
                ageAson, 0, disabilityID, disailitytype.toString(),
                0, occupation, secondary_occupation, tertiary_occupation,
                validate!!.Daybetweentime(et_joiningDate.text.toString()),
                validate!!.Daybetweentime(et_leavingDate.text.toString()),
                inactivereasn, 0,
                et_guardianName.text.toString(),
                validate!!.returnStringValue(et_guardianNamelocal.text.toString()),
                guardianrelation,
                designation,
                status, 0,
                rgheadofFamily.checkedRadioButtonId, 0, 0, 0,
                "", 0, validate!!.Daybetweentime(validate!!.currentdatetime),
                validate!!.RetriveSharepreferenceString(AppSP.userid), 0, "", 1,
                1, 1, 1, 0, "", settlement_status, "", 1, imageName, 0, 1, 0, 1
            )

            memberViewmodel!!.insert(memberEntity!!)
            validate!!.updateMemberEditFlag(
                validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),
                shgViewmodel,
                memberViewmodel
            )
            validate!!.SaveSharepreferenceString(AppSP.MEMBERGUID, guid)
            validate!!.SaveSharepreferenceLong(AppSP.membercode, membercode)
            validate!!.SaveSharepreferenceInt(AppSP.Seqno, seqno)
            validate!!.SaveSharepreferenceString(AppSP.memebrname, membername)
            validate!!.SaveSharepreferenceLong(
                AppSP.MEMBERJOININGDATE,
                validate!!.Daybetweentime(et_joiningDate.text.toString())
            )
            validate!!.SaveSharepreferenceLong(
                AppSP.MemberDob,
                validate!!.returnLongValue(dob.toString())
            )

            var kycimage1 = ImageuploadEntity(
                imageName,
                validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),
                validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID),

                2,
                0
            )
            if (imageName.isNotEmpty() && newimageName) {
                imageUploadViewmodel!!.insert(kycimage1)
            }
            var kycnolist = memberkycviewmodel!!.getKycwithkycno(adharno)
            // if (kycnolist.isNullOrEmpty() && adharscan) {
            if (adharno.isNotEmpty() && adharscan) {
                var entrysource = 0
                if (adharscandigi) {
                    entrysource = 1
                } else {
                    entrysource = 2
                }
                var kycguid = validate!!.random()
                var memberKYCEntity = Member_KYC_Entity(
                    0,
                    membercode,
                    validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!,
                    kycguid,
                    2,
                    adharno, "",
                    "",
                    "",
                    "",
                    "",
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                    0,
                    "",
                    0,
                    "",
                    1,
                    1, entrysource, 1, 1, "", 1, 0
                )
                memberkycviewmodel!!.insert(memberKYCEntity)
                //}
            }
            if (addline1.isNotEmpty() && adharscan) {
                var entrysource = 0
                if (adharscandigi) {
                    entrysource = 1
                } else if (adharscan) {
                    entrysource = 2
                }
                var addressGUID = validate!!.random()
                var addressEntity = member_addressEntity(
                    0,
                    membercode,
                    validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID),
                    addressGUID,
                    1,
                    addline1,
                    addline2,
                    validate!!.RetriveSharepreferenceInt(AppSP.villagecode),
                    validate!!.RetriveSharepreferenceInt(AppSP.panchayatcode),
                    validate!!.RetriveSharepreferenceInt(AppSP.blockcode),
                    "",
                    validate!!.RetriveSharepreferenceInt(AppSP.statecode),
                    validate!!.RetriveSharepreferenceInt(AppSP.districtcode),
                    pincode.toString(),
                    1,
                    entrysource,
                    1,
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                    0,
                    "",
                    0,
                    "", 1, 0, 1, 2
                )
                memberaddressviewmodel!!.insert(addressEntity)
            }

            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this,
                MemeberPhoneListActivity::class.java
            )
        } else {
            memberViewmodel!!.updateMemberDetail(
                validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!,
                seqno,
                membername,
                validate!!.returnStringValue(et_namelocal.text.toString()),
                motherrelation,
                et_mother_father.text.toString(),
                validate!!.returnStringValue(et_mother_fatherlocal.text.toString()),
                gender,
                martialstatus,
                religion,
                category,
                pvtgID,
                0,
                "",
                0,
                0,
                educationlevel,
                rgisDobavailable.checkedRadioButtonId,
                dob,
                age,
                ageAson,
                0,
                rgDisability.checkedRadioButtonId,
                disailitytype.toString(),
                0,
                occupation,
                secondary_occupation,
                tertiary_occupation,
                validate!!.Daybetweentime(et_joiningDate.text.toString()),
                validate!!.Daybetweentime(et_leavingDate.text.toString()),
                inactivereasn,
                0,
                et_guardianName.text.toString(),
                validate!!.returnStringValue(et_guardianNamelocal.text.toString()),
                guardianrelation,
                designation,
                status,
                0,
                rgheadofFamily.checkedRadioButtonId,
                0,
                0,
                0,
                "",
                validate!!.Daybetweentime(validate!!.currentdatetime),
                validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                settlement_status,
                imageName, 1
            )
            validate!!.SaveSharepreferenceString(AppSP.memebrname, membername)
            var kycimage1 = ImageuploadEntity(
                imageName,
                validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),
                validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID),

                2,
                0
            )
            if (imageName.isNotEmpty() && newimageName) {
                imageUploadViewmodel!!.insert(kycimage1)
            }
            insertKycAddressdata()
            validate!!.updateMemberEditFlag(
                validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),
                shgViewmodel,
                memberViewmodel
            )
            validate!!.CustomAlert(
                LabelSet.getText(
                    "updated_successfully",
                    R.string.updated_successfully
                ), this,
                MemeberPhoneListActivity::class.java
            )
        }

    }

    private fun PartialSaveData() {
        if (validate!!.RetriveSharepreferenceInt(AppSP.MemberLockRecord) != 1) {
            if (checkData() == 0) {
                guid = validate!!.random()
                if (isVerified == 1 || isVerified == 3) {
                    isVerified = 3
                } else if (isVerified == 9) {
                    isVerified = 9
                } else {
                    isVerified = 0
                }
                var age = 0
                var ageAson = 0L
                var dob = 0L

                var pvtgID: Int? = 0
                var minor: Int? = 0
                var disabilityID = rgDisability.checkedRadioButtonId
                var headID = rgheadofFamily.checkedRadioButtonId



                if (linearPVTG.visibility == View.VISIBLE) {
                    pvtgID = rgpvtg.checkedRadioButtonId
                } else {
                    pvtgID = null
                }

                var gender = validate!!.returnlookupcode(spin_gender, dataspin_gender)
                var settlement_status =
                    validate!!.returnlookupcode(spin_settlementstatus, dataspin_settlement)
                var martialstatus = validate!!.returnlookupcode(spin_martial, dataspin_maritial)
                var religion = validate!!.returnlookupcode(spin_religion, dataspin_religion)
                var category = validate!!.returnlookupcode(spin_socialCategory, dataspin_category)
                var educationlevel = validate!!.returnlookupcode(spin_education, dataspin_education)
                var disailitytype =
                    validate!!.returnlookupcode(spin_disabilityType, dataspin_disabletype)
                var status = validate!!.returnlookupcode(spin_status, dataspin_status)
                var designation = validate!!.returnlookupcode(spin_Post, dataspin_post)
                var motherrelation =
                    validate!!.returnlookupcode(spin_relationmother, dataspin_motherrekation)
                var inactivereasn =
                    validate!!.returnlookupcode(spin_inactivereason, dataspin_inactivestatus)
                var guardianrelation =
                    validate!!.returnlookupcode(spin_relationGuardian, dataspin_guardianRelation)
                var occupation = validate!!.returnlookupcode(spin_occupation, dataspin_name)
                var secondary_occupation =
                    validate!!.returnlookupcode(spin_secondaryoccu, dataspin_name)
                var tertiary_occupation =
                    validate!!.returnlookupcode(spin_tertiaryoccu, dataspin_name)
                membername = et_name.text.toString()

                if (rgisDobavailable.checkedRadioButtonId == 1) {
                    dob = validate!!.Daybetweentime(et_dob.text.toString())
                    age = validate!!.getage(validate!!.currentdatetime, et_dob.text.toString())
                    ageAson = validate!!.Daybetweentime(validate!!.currentdatetime)
                } else if (rgisDobavailable.checkedRadioButtonId == 0) {
                    age = validate!!.returnIntegerValue(et_age.text.toString())
                    ageAson = validate!!.Daybetweentime(et_ason.text.toString())
                    dob = validate!!.Daybetweentime(validate!!.getdob(et_ason.text.toString(), age))
                }

                if (validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID).isNullOrEmpty()) {
                    seqno =
                        memberViewmodel!!.getseqno(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)) + 1

                    memberEntity = MemberEntity(
                        0,
                        membercode,
                        shgcode,
                        validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),
                        membercode,
                        guid,
                        seqno,
                        membername,
                        validate!!.returnStringValue(et_namelocal.text.toString()),
                        motherrelation,
                        et_mother_father.text.toString(),
                        validate!!.returnStringValue(et_mother_fatherlocal.text.toString()),
                        gender,
                        martialstatus,
                        religion,
                        category,
                        pvtgID,
                        0, "", 0, 0, educationlevel,
                        rgisDobavailable.checkedRadioButtonId,
                        dob,
                        age,
                        ageAson, 0, disabilityID, disailitytype.toString(),
                        0, occupation, secondary_occupation, tertiary_occupation,
                        validate!!.Daybetweentime(et_joiningDate.text.toString()),
                        validate!!.Daybetweentime(et_leavingDate.text.toString()),
                        inactivereasn, 0,
                        et_guardianName.text.toString(),
                        validate!!.returnStringValue(et_guardianNamelocal.text.toString()),
                        guardianrelation,
                        designation,
                        status, 0,
                        rgheadofFamily.checkedRadioButtonId, 0, 0, 0,
                        "", 0, validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid), 0, "", 0,
                        1, 1, 1, 0, "", settlement_status, "", 1, imageName, 0, 1, 1, 1
                    )

                    memberViewmodel!!.insert(memberEntity!!)

                    validate!!.SaveSharepreferenceString(AppSP.MEMBERGUID, guid)
                    validate!!.SaveSharepreferenceLong(AppSP.membercode, membercode)
                    validate!!.SaveSharepreferenceInt(AppSP.Seqno, seqno)
                    validate!!.SaveSharepreferenceString(AppSP.memebrname, membername)
                    validate!!.SaveSharepreferenceLong(
                        AppSP.MEMBERJOININGDATE,
                        validate!!.Daybetweentime(et_joiningDate.text.toString())
                    )

                    validate!!.SaveSharepreferenceLong(
                        AppSP.MemberDob,
                        validate!!.returnLongValue(dob.toString())
                    )

                    var kycimage1 = ImageuploadEntity(
                        imageName,
                        validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),
                        validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID),

                        2,
                        0
                    )
                    if (imageName.isNotEmpty() && newimageName) {
                        imageUploadViewmodel!!.insert(kycimage1)
                    }
                    var kycnolist = memberkycviewmodel!!.getKycwithkycno(adharno)
                    // if (kycnolist.isNullOrEmpty() && adharscan) {
                    if (adharno.isNotEmpty() && adharscan) {
                        var entrysource = 0
                        if (adharscandigi) {
                            entrysource = 1
                        } else {
                            entrysource = 2
                        }
                        var kycguid = validate!!.random()
                        var memberKYCEntity = Member_KYC_Entity(
                            0,
                            membercode,
                            validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!,
                            kycguid,
                            2,
                            adharno, "",
                            "",
                            "",
                            "",
                            "",
                            validate!!.Daybetweentime(validate!!.currentdatetime),
                            validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                            0,
                            "",
                            0,
                            "",
                            1,
                            1, entrysource, 1, 1, "", 1, 0
                        )
                        memberkycviewmodel!!.insert(memberKYCEntity)
                        //}
                    }
                    if (addline1.isNotEmpty() && adharscan) {
                        var entrysource = 0
                        if (adharscandigi) {
                            entrysource = 1
                        } else if (adharscan) {
                            entrysource = 2
                        }
                        var addressGUID = validate!!.random()
                        var addressEntity = member_addressEntity(
                            0,
                            membercode,
                            validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID),
                            addressGUID,
                            1,
                            addline1,
                            addline2,
                            validate!!.RetriveSharepreferenceInt(AppSP.villagecode),
                            validate!!.RetriveSharepreferenceInt(AppSP.panchayatcode),
                            validate!!.RetriveSharepreferenceInt(AppSP.blockcode),
                            "",
                            validate!!.RetriveSharepreferenceInt(AppSP.statecode),
                            validate!!.RetriveSharepreferenceInt(AppSP.districtcode),
                            pincode.toString(),
                            1,
                            entrysource,
                            1,
                            validate!!.Daybetweentime(validate!!.currentdatetime),
                            validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                            0,
                            "",
                            0,
                            "", 1, 0, 1, 2
                        )
                        memberaddressviewmodel!!.insert(addressEntity)
                    }


                } else {
                    memberViewmodel!!.updateMemberDetail(
                        validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!,
                        seqno,
                        membername,
                        validate!!.returnStringValue(et_namelocal.text.toString()),
                        motherrelation,
                        et_mother_father.text.toString(),
                        validate!!.returnStringValue(et_mother_fatherlocal.text.toString()),
                        gender,
                        martialstatus,
                        religion,
                        category,
                        pvtgID,
                        0,
                        "",
                        0,
                        0,
                        educationlevel,
                        rgisDobavailable.checkedRadioButtonId,
                        dob,
                        age,
                        ageAson,
                        0,
                        rgDisability.checkedRadioButtonId,
                        disailitytype.toString(),
                        0,
                        occupation,
                        secondary_occupation,
                        tertiary_occupation,
                        validate!!.Daybetweentime(et_joiningDate.text.toString()),
                        validate!!.Daybetweentime(et_leavingDate.text.toString()),
                        inactivereasn,
                        0,
                        et_guardianName.text.toString(),
                        validate!!.returnStringValue(et_guardianNamelocal.text.toString()),
                        guardianrelation,
                        designation,
                        status,
                        0,
                        rgheadofFamily.checkedRadioButtonId,
                        0,
                        0,
                        0,
                        "",
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                        settlement_status,
                        imageName, 1
                    )
                    validate!!.SaveSharepreferenceString(AppSP.memebrname, membername)
                    var kycimage1 = ImageuploadEntity(
                        imageName,
                        validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),
                        validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID),

                        2,
                        0
                    )
                    if (imageName.isNotEmpty() && newimageName) {
                        imageUploadViewmodel!!.insert(kycimage1)
                    }
                    insertKycAddressdata()
                    /*   if (adharno.isNotEmpty() && adharscan) {
                           var kycguid = validate!!.random()
                           var  memberKYCEntity = Member_KYC_Entity(
                               0,
                               validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!,
                               kycguid,
                               2,
                               adharno, "",
                               "",
                               "",
                               "",
                               "",
                               validate!!.Daybetweentime(validate!!.currentdatetime),
                               validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                               0,
                               "",
                               0,
                               "",
                               1,
                               1, 1, 0, 0, "", 1
                               ,1)
                           memberkycviewmodel!!.insert(memberKYCEntity!!)
                       }*/
                    CDFIApplication.database?.memberDao()
                        ?.updatepartialstatus(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!)


                }
            }
        }

    }

    private fun checkValidation(): Int {

        var value = 1
        if (et_name.text.toString().trim().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "name_including_surname",
                    R.string.name_including_surname
                ),
                this,
                et_name
            )
            value = 0
            return value
        } else if (spin_gender.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this@MemberDetailActivity,
                spin_gender,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "gender",
                    R.string.gender
                )
            )
            value = 0
            return value
        } else if (rgisDobavailable.checkedRadioButtonId == 1 && et_dob.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText("dob", R.string.dob),
                this,
                et_dob
            )
            value = 0
            return value
        } else if (rgisDobavailable.checkedRadioButtonId == 0 && et_age.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText("age", R.string.age),
                this,
                et_age
            )
            value = 0
            return value
        } else if (rgisDobavailable.checkedRadioButtonId == 0 && et_ason.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText("as_on", R.string.as_on),
                this,
                et_ason
            )
            value = 0
            return value
        } else if (et_mother_father.text.toString().trim().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "father_husband",
                    R.string.father_husband
                ),
                this,
                et_mother_father
            )
            value = 0
            return value
        } else if (spin_relationmother.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_relationmother,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "relationwithfather",
                    R.string.relationwithfather
                )
            )
            value = 0
            return value
        } else if (spin_martial.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_martial,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "martial_status",
                    R.string.martial_status
                )
            )
            value = 0
            return value
        }
        /* else if (validate!!.GetAnswerTypeRadioButtonID(rgheadofFamily) == 0) {
             validate!!.CustomAlertRadio(
                 this,
                 LabelSet.getText(
                     "please_select",
                     R.string.please_select
                 ) + " " + LabelSet.getText("head_of_family", R.string.head_of_family),
                 rgheadofFamily
             )
             value = 0
             return value
         } else if (validate!!.GetAnswerTypeRadioButtonID(rgDisability) == 0) {
             validate!!.CustomAlertRadio(
                 this,
                 LabelSet.getText(
                     "please_select",
                     R.string.please_select
                 ) + " " + LabelSet.getText("disability", R.string.disability),
                 rgDisability
             )
             value = 0
             return value
         }*/ else if (rgDisability.checkedRadioButtonId != 1 && spin_disabilityType.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_disabilityType,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "disability_type",
                    R.string.disability_type
                )
            )
            value = 0
            return value
        } else if (lay_guardian.visibility == View.VISIBLE && et_guardianName.text.toString()
                .trim().length == 0
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "guardian_name",
                    R.string.guardian_name
                ),
                this,
                et_guardianName
            )
            value = 0
            return value
        } else if (lay_guardian.visibility == View.VISIBLE && spin_relationGuardian.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_relationGuardian,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "relaion_guardian",
                    R.string.relaion_guardian
                )
            )
            value = 0
            return value
        } else if (et_joiningDate.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "joiningDate",
                    R.string.joiningDate
                ),
                this,
                et_joiningDate
            )
            value = 0
            return value
        } else if (validate!!.getDate(
                et_joiningDate.text.toString(),
                et_dob.text.toString()
            ) == 1
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "validjoiningDate",
                    R.string.validjoiningDate
                ),
                this,
                et_joiningDate
            )
            value = 0
            return value
        } else if (spin_Post.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_Post,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "post_designation",
                    R.string.post_designation
                )
            )
            value = 0
            return value
        } else if (spin_socialCategory.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_socialCategory,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "social_category",
                    R.string.social_category
                )
            )
            value = 0
            return value
        } /*else if (validate!!.returnlookupcode(
                spin_socialCategory,
                dataspin_category
            ) == 2 && validate!!.GetAnswerTypeRadioButtonID(rgpvtg) == 0
        ) {
            validate!!.CustomAlertRadio(
                this,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText("pvtg", R.string.pvtg),
                rgpvtg
            )
            value = 0
            return value
        }*/ else if (spin_religion.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_religion,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "religion",
                    R.string.religion
                )
            )
            value = 0
            return value
        } else if (spin_education.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_education,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "education",
                    R.string.education
                )
            )
            value = 0
            return value
        } else if (validate!!.returnlookupcode(
                spin_status,
                dataspin_status
            ) == 2 && et_leavingDate.text.toString().length == 0
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "validleavingdate",
                    R.string.validleavingdate
                ),
                this,
                et_leavingDate
            )
            value = 0
            return value
        } else if (validate!!.returnlookupcode(
                spin_status,
                dataspin_status
            ) == 2 && spin_inactivereason.selectedItemPosition == 0
        ) {
            validate!!.CustomAlertSpinner(
                this@MemberDetailActivity,
                spin_inactivereason,
                LabelSet.getText(
                    "validleavingreason",
                    R.string.validleavingreason
                )
            )
            value = 0
            return value
        } else if (validate!!.returnlookupcode(
                spin_status,
                dataspin_status
            ) == 2 && spin_settlementstatus.selectedItemPosition == 0
        ) {
            validate!!.CustomAlertSpinner(
                this@MemberDetailActivity,
                spin_settlementstatus,
                LabelSet.getText(
                    "validsettlementstatus",
                    R.string.validsettlementstatus
                )
            )
            value = 0
            return value
        } else if (validate!!.RetriveSharepreferenceInt(AppSP.CboType) == 2) {
            var age = 0

            if (validate!!.RetriveSharepreferenceInt(AppSP.Tag) == 3) {
                if (rgisDobavailable.checkedRadioButtonId == 1) {
                    age = validate!!.getage(validate!!.currentdatetime, et_dob.text.toString())
                    if (age < 55) {
                        validate!!.CustomAlertEditText(
                            LabelSet.getText(
                                "agemustbegreaterthan",
                                R.string.agemustbegreaterthan
                            ),
                            this,
                            et_dob
                        )
                        value = 0
                        return value
                    }
                } else if (rgisDobavailable.checkedRadioButtonId == 0) {
                    age = validate!!.returnIntegerValue(et_age.text.toString())
                    if (age < 55) {
                        validate!!.CustomAlertEditText(
                            LabelSet.getText(
                                "agemustbegreaterthan",
                                R.string.agemustbegreaterthan
                            ),
                            this,

                            et_age
                        )
                        value = 0
                        return value
                    }
                }


            }

        } else if (validate!!.RetriveSharepreferenceInt(AppSP.CboType) == 1) {
            var age = 0

            if (rgisDobavailable.checkedRadioButtonId == 1) {
                age = validate!!.getage(validate!!.currentdatetime, et_dob.text.toString())
                if (age < 18) {
                    validate!!.CustomAlertEditText(
                        LabelSet.getText(
                            "age_womengrp",
                            R.string.age_womengrp
                        ),
                        this,
                        et_dob
                    )
                    value = 0
                    return value
                }
            } else if (rgisDobavailable.checkedRadioButtonId == 0) {
                age = validate!!.returnIntegerValue(et_age.text.toString())
                if (age < 18) {
                    validate!!.CustomAlertEditText(
                        LabelSet.getText(
                            "age_womengrp",
                            R.string.age_womengrp
                        ),
                        this,

                        et_age
                    )
                    value = 0
                    return value
                }
            }


        }

        return value
    }

    fun AddPostDesignation() {
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.post_designation_dialog, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()



        mAlertDialog.btn_close.setOnClickListener {
            mAlertDialog.dismiss()
        }
        mAlertDialog.lyMain.removeAllViews()
        var postList: List<MemberDesignationEntity> =
            CDFIApplication.database?.memberDesignationDao()!!.getMemberDesignationdetail(
                validate!!.RetriveSharepreferenceString(
                    AppSP.MEMBERGUID
                )!!
            )

        for (i in 0..postList.size - 1) {
            val mChildView =
                LayoutInflater.from(this).inflate(R.layout.designation_dialog_item, null)
            mAlertDialog.lyMain.addView(mChildView)
            mChildView.tv_president.text = LabelSet.getText("designation", R.string.designation)
            mChildView.tvFormDate.text = LabelSet.getText("from_date", R.string.from_date)
            mChildView.tvToDatetxt.text = LabelSet.getText("to_Date", R.string.to_Date)
            mChildView.tvdesignationValue.text = returnDesignation(postList.get(i).designation!!)
            mChildView.tvFromdate.text = validate!!.convertDatetime(
                validate!!.returnLongValue(
                    postList.get(i).from_date.toString()
                )
            )
            if (validate!!.returnLongValue(postList.get(i).to_date.toString()) > 0) {
                mChildView.tvToDate.text = validate!!.convertDatetime(
                    validate!!.returnLongValue(
                        postList.get(i).to_date.toString()
                    )
                )

            } else {
                mChildView.tvToDate.text = "Current"
            }
        }


    }

    fun returnDesignation(desID: Int): String {
        var value = ""
        var data = data_Designation!!.filter { it.lookup_code == desID }
        if (data != null && data.size > 0) {
            value = data[0].key_value.toString()
        }
        return value
    }


    fun SetAnswerTypeRadioReset(radioGroup: RadioGroup, data: String) {
        radioGroup.clearCheck()
        for (i in 0 until radioGroup.childCount) {

            val radioButton1 = radioGroup.getChildAt(i) as RadioButton
            radioButton1.isChecked = false

        }
    }

    override fun onBackPressed() {
//        CustomAlertPartialsave(getString(R.string.doyouwanttosavepartialdata),MemberListActivity::class.java)
        if (checkValidation() == 1) {
            PartialSaveData()
            var intent = Intent(this, MemberListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }
    }


    private fun checkPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        val result1 =
            ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val result2 =
            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission(requestCode: Int) {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ),
            requestCode
        )
    }

    fun captureimage(code: Int) {
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
                m.invoke(null)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//        fileUri = getOutputMediaFileUri(MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE, code)
        fileUri = FileProvider.getUriForFile(
            this, BuildConfig.APPLICATION_ID + ".provider", getOutputMediaFile(
                MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE, code
            )!!
        )
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
        startActivityForResult(intent, code)

    }

    fun getOutputMediaFileUri(type: Int, flag: Int): Uri {
        return Uri.fromFile(getOutputMediaFile(type, flag))
    }

    private fun getOutputMediaFile(type: Int, flag: Int): File? {

        // External sdcard location
        val mediaStorageDir = File(
            getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
            AppSP.IMAGE_DIRECTORY_NAME
        )
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null
            }
        }

        // Create a media file name
        val timeStamp = SimpleDateFormat(
            "yyyyMMdd_HHmmss",
            Locale.ENGLISH
        ).format(Date())
        if (validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID).isNullOrEmpty()) {
            seqno =
                memberViewmodel!!.getseqno(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)) + 1
            imageName = "IMG_$timeStamp,1000$seqno.jpg"
        } else {
            imageName = "IMG_$timeStamp,1000$seqno.jpg"
        }
        if (type == MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE) {
            mediaFile = File(
                mediaStorageDir.path + File.separator
                        + imageName
            )
            imgPathUpload = mediaStorageDir.path + File.separator + "IMG_" + timeStamp + ".jpg"


        } else {
            return null
        }

        return mediaFile
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (requestCode == 201) {
                var alldata: String = data!!.getStringExtra("adhardata")!!
                if (alldata.length > 0) {
                    val gson = Gson()
                    restaurantObject = gson.fromJson<SampleEntity>(
                        alldata,
                        SampleEntity::class.java
                    )
                    showAdharDialog(restaurantObject!!)
                    // fillAdharDetail(restaurantObject!!)


                }
            }

            else if (requestCode == 100) {
                if (resultCode == RESULT_OK && null != data) {
                    val result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                    var textdata = result!![0]
                    if (textdata.isNullOrEmpty()) {
                    } else {
                        if (CURRENT_SELECTED_EDITTEXT == 1) {
                            if (et_name.hasFocus()) {
                                var cursorPosition: Int = et_name.selectionStart
                                et_name.text.insert(cursorPosition, textdata)
                            } else {
                                var getData = et_name.text.toString()
                                if (getData.length > 0) {
                                    getData =  getData +" " +  textdata
                                } else {
                                    getData = getData + textdata
                                }
                                et_name.setText(getData)
                            }
                        }
                        if (CURRENT_SELECTED_EDITTEXT == 2) {
                            if (et_namelocal.hasFocus()) {
                                var cursorPosition: Int = et_namelocal.selectionStart
                                et_namelocal.text.insert(cursorPosition, textdata)
                            } else {
                                var getData = et_namelocal.text.toString()
                                if (getData.length > 0) {
                                    getData =  getData +" " +  textdata
                                } else {
                                    getData = getData + textdata
                                }
                                et_namelocal.setText(getData)
                            }
                        }
                        if (CURRENT_SELECTED_EDITTEXT == 3) {
                            if (et_mother_father.hasFocus()) {
                                var cursorPosition: Int = et_mother_father.selectionStart
                                et_mother_father.text.insert(cursorPosition, textdata)
                            } else {
                                var getData = et_mother_father.text.toString()
                                if (getData.length > 0) {
                                    getData =  getData +" " +  textdata
                                } else {
                                    getData = getData + textdata
                                }
                                et_mother_father.setText(getData)
                            }
                        }
                        if (CURRENT_SELECTED_EDITTEXT == 4) {
                            if (et_mother_fatherlocal.hasFocus()) {
                                var cursorPosition: Int = et_mother_fatherlocal.selectionStart
                                et_mother_fatherlocal.text.insert(cursorPosition, textdata)
                            } else {
                                var getData = et_mother_fatherlocal.text.toString()
                                if (getData.length > 0) {
                                    getData = getData + " " +  textdata
                                } else {
                                    getData = getData + textdata
                                }
                                et_mother_fatherlocal.setText(getData)
                            }
                        }
                        if (CURRENT_SELECTED_EDITTEXT == 5) {
                            if (et_guardianName.hasFocus()) {
                                var cursorPosition: Int = et_guardianName.selectionStart
                                et_guardianName.text.insert(cursorPosition, textdata)
                            } else {
                                var getData = et_guardianName.text.toString()
                                if (getData.length > 0) {
                                    getData = getData + " " +  textdata
                                } else {
                                    getData = getData + textdata
                                }
                                et_guardianName.setText(getData)
                            }
                        }
                        if (CURRENT_SELECTED_EDITTEXT == 6) {
                            if (et_guardianNamelocal.hasFocus()) {
                                var cursorPosition: Int = et_guardianNamelocal.selectionStart
                                et_guardianNamelocal.text.insert(cursorPosition, textdata)
                            } else {
                                var getData = et_guardianNamelocal.text.toString()
                                if (getData.length > 0) {
                                    getData = getData + " " + textdata
                                } else {
                                    getData = getData + textdata
                                }
                                et_guardianNamelocal.setText(getData)
                            }
                        }
                    }
                }
            }

            else if (flag) {
                flag = false
                if (resultCode == Activity.RESULT_CANCELED) {
                    Toast.makeText(this, "Cancel", Toast.LENGTH_LONG).show()
                    return
                }

                val scanningResult =
                    IntentIntegrator.parseActivityResult(requestCode, resultCode, data)

                if (scanningResult != null) {
                    val scanContent = scanningResult.contents

                    val aadhaarParser = AadhaarParser.getInstance(this)

                    aadhaarParser.parse(
                        scanContent
                    ) { aadhaarCard ->
                        // aadhaarCard is your user model object
                        try {
                            // process secure QR code
                            CustomAlertAadhar(aadhaarCard)
                        } catch (e: java.lang.Exception) {
                            Toast.makeText(this, "Error in processing QRcode", Toast.LENGTH_LONG)
                                .show()
                        }
                    }

                } else {
                    Toast.makeText(this, "No scan data received!", Toast.LENGTH_LONG).show()
                }
            } else if (requestCode != 201) {
                compreesedimage()
                newimageName = true
                bitmap = BitmapFactory.decodeFile(fileUri!!.path)
                if (requestCode == 101 && resultCode == Activity.RESULT_OK) {
                    Imgshow.setImageBitmap(bitmap)
                    // tvUploadFiles.setText(imageName)
                }
            }
        } catch (e: Exception) {
            validate!!.CustomAlert(
                LabelSet.getText(
                    "eitherretry",
                    R.string.eitherretry
                ), this
            )
            e.printStackTrace()
        }
    }

    fun CustomAlertAadhar(aadhaarCard: AadhaarUser) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customaadharalertdialoge, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_title.text = LabelSet.getText("aadhaardetail", R.string.aadhaardetail)
        mDialogView.btn_ok.text = LabelSet.getText("ok", R.string.ok)
        mDialogView.btn_rescan.text = LabelSet.getText("rescan", R.string.rescan)
        if (aadhaarCard != null) {
            //uid
            mDialogView.tv_set_aadhar.text = aadhaarCard.uid
            if (aadhaarCard.uid.length == 12)
                mDialogView.tv_aadhar.text = "Aadhaar No."
            else
                mDialogView.tv_aadhar.text = "Reference Id"
            //name
            mDialogView.tv_set_name.text = aadhaarCard.name
            //    et_name.setText(aadhaarCard.name)
            //gender
            mDialogView.tv_set_gender.text = aadhaarCard.gender
            // year of birth
            mDialogView.tv_set_dob.text = aadhaarCard.dob
            // care of
            mDialogView.tv_set_address.text = aadhaarCard.address


            /*  validate!!.SetAnswerTypeRadioButton(rgisDobavailable, 1)
              et_dob.isEnabled = false
              spin_gender.isEnabled = false
              et_name.isEnabled = false
              rgisDobavailable.isEnabled = false*/
        } else {
            nameMatch = 0
            validate!!.CustomAlert(LabelSet.getText("name_mismatch", R.string.name_mismatch), this)
        }

        mDialogView.btn_ok.setOnClickListener {

            if (validate!!.RetriveSharepreferenceInt(AppSP.CboType) == 1) {
                if (aadhaarCard.gender.equals("M")) {
                    validate!!.CustomAlert(
                        LabelSet.getText(
                            "youcannotaddmale",
                            R.string.youcannotaddmale
                        ), this
                    )

                } else {
                    //   fillAdharDetail(adharData)
                    fillAdharData(
                        aadhaarCard.uid,
                        aadhaarCard.name,
                        aadhaarCard.gender,
                        aadhaarCard.dob,
                        aadhaarCard.address
                    )
                }
            } else {
                fillAdharData(
                    aadhaarCard.uid,
                    aadhaarCard.name,
                    aadhaarCard.gender,
                    aadhaarCard.dob,
                    aadhaarCard.address
                )
            }
            mAlertDialog.dismiss()


        }
        mDialogView.btn_rescan.setOnClickListener {
            flag = true
            val integrator = IntentIntegrator(this)
            integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE)
            integrator.setPrompt("Scan a Aadharcard QR Code")
            integrator.setCameraId(0) // Use a specific camera of the device
            integrator.captureActivity = OrientationCaptureActivity::class.java
            integrator.setOrientationLocked(false)
            integrator.setBeepEnabled(false)

            integrator.initiateScan()
            mAlertDialog.dismiss()


        }
    }

    private fun fillAdharData(
        uid: String,
        name: String,
        gender: String,
        dob: String,
        address: String
    ) {
        if (et_name.text.toString().trim().length == 0 || (et_name.text.toString()
                .trim().length > 0 && et_name.text.toString().toLowerCase().trim()
                .equals(name.toLowerCase()))
        ) {
            adharscan = true
            adharscandigi = true
            nameMatch = 1
            et_name.setText(name)
            if (gender.contains("M")) {
                spin_gender.setSelection(
                    validate!!.returnlookupcodepos(
                        1,
                        dataspin_gender
                    )
                )
            } else if (gender.contains("F")) {
                spin_gender.setSelection(
                    validate!!.returnlookupcodepos(
                        2,
                        dataspin_gender
                    )
                )
            }

            if (dob.length > 4) {
                if (dob.contains("/")) {
                    et_dob.setText(dob.replace("/", "-"))
                } else {
                    et_dob.setText(dob)
                }
                validate!!.fillradio(rgisDobavailable, 1, dataradio_isDOB, this)
            }

            if (address.length > 0) {
                addline1 = address
            }

            if (uid.length == 12)
                adharno = uid
            else
                adharno = "xxxxxxxx" + uid.substring(0, 4)
        } else {
            nameMatch = 0
            validate!!.CustomAlert(
                LabelSet.getText(
                    "name_mismatch",
                    R.string.name_mismatch
                ), this
            )
        }
    }


    fun fillAdharDetail(adharData: SampleEntity) {
        if (adharData != null) {
            if (adharData.certificateData.uidData.poi != null) {
                var poi: Poi = adharData.certificateData.uidData.poi
                if (et_name.text.toString().trim().length == 0 || (et_name.text.toString()
                        .trim().length > 0 && et_name.text.toString().toLowerCase().trim()
                        .equals(poi.name.toLowerCase()))
                ) {
                    adharscan = true
                    adharscandigi = true
                    adharno = adharData.certificateData.uidData.uid
                    nameMatch = 1
                    et_name.setText(poi.name)
                    if (poi.gender.contains("M")) {
                        spin_gender.setSelection(
                            validate!!.returnlookupcodepos(
                                1,
                                dataspin_gender
                            )
                        )
                    } else if (poi.gender.contains("F")) {
                        spin_gender.setSelection(
                            validate!!.returnlookupcodepos(
                                1,
                                dataspin_gender
                            )
                        )
                    }
                    et_dob.setText(poi.dob.replace("/", "-"))
                    et_dob.isEnabled = false
                    spin_gender.isEnabled = false
                    et_name.isEnabled = false
                    rgisDobavailable.isEnabled = false
                } else {
                    nameMatch = 0
                    validate!!.CustomAlert(
                        LabelSet.getText(
                            "name_mismatch",
                            R.string.name_mismatch
                        ), this
                    )
                }


                if (adharData.certificateData.uidData.poa != null) {
                    var address = adharData.certificateData.uidData.poa
                    pincode = address.pc.toInt()
                    addline1 = ""
                    addline2 = ""
                    namelocal = ""
                    var fathername = ""
                    var address_en: String = ""
                    var addresslin1: String = ""
                    var addresslin2: String = ""
                    var poa = adharData.certificateData.uidData.poa
                    if (poa.co != null) {
                        fathername = poa.co
                        address_en = address_en + "," + poa.co
                        addresslin1 = addresslin1 + "," + poa.co
                    } else if (poa.so != null) {
                        fathername = poa.so
                        address_en = address_en + "," + poa.so
                        addresslin1 = addresslin1 + "," + poa.so
                    }
                    if (poa.house != null) {

                        address_en = address_en + "," + poa.house
                        addresslin1 = addresslin1 + "," + poa.house

                    }
                    if (poa.street != null) {
                        address_en = address_en + "," + poa.street
                        addresslin1 = addresslin1 + "," + poa.street
                    }
                    if (poa.lm != null) {
                        address_en = address_en + "," + poa.lm
                        addresslin1 = addresslin1 + "," + poa.lm
                    }
                    if (poa.loc != null) {
                        address_en = address_en + "," + poa.loc
                        addresslin2 = addresslin2 + "," + poa.loc
                    }
                    if (poa.vtc != null) {
                        address_en = address_en + "," + poa.vtc
                        addresslin2 = addresslin2 + "," + poa.vtc
                    }
                    if (poa.dist != null) {
                        address_en = address_en + "," + poa.dist
                        addresslin2 = addresslin2 + "," + poa.dist
                    }
                    if (poa.state != null) {
                        address_en = address_en + "," + poa.state
                        addresslin2 = addresslin2 + "," + poa.state
                    }
                    if (poa.pc != null) {
                        address_en = address_en + "," + poa.pc
                        pincode = poa.pc.toInt()
                    }
                    addline1 = addresslin1
                    addline2 = addresslin2

                }
            }
        }
    }


    fun compreesedimage() {
        val options = BitmapFactory.Options()
        var bitmap = BitmapFactory.decodeFile(fileUri!!.path, options)

        if (bitmap != null) {
            var actualWidth = options.outWidth
            var actualHeight = options.outHeight
            var mutableBitmap =
                Bitmap.createScaledBitmap(bitmap, actualWidth / 2, actualHeight / 2, false)

            var fOut: OutputStream? = null
            var file: File = File(fileUri!!.path)
            try {
                fOut = FileOutputStream(file)
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }
            mutableBitmap.compress(Bitmap.CompressFormat.JPEG, 50, fOut)
            try {
                fOut!!.flush()
                fOut.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
    }

    private fun ShowImage(name: String?) {
        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val layout: View = inflater.inflate(R.layout.popupwindow, null, false)
        val window = PopupWindow(
            layout,
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT,
            true
        )
        window.showAtLocation(layout, Gravity.CENTER, 0, 0)
        val cancel: ImageView = layout.findViewById(R.id.cancel)
        val image_preview: ImageView = layout.findViewById(R.id.image_preview)
        val mediaStorageDirectory = File(
            getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
            AppSP.IMAGE_DIRECTORY_NAME
        )
        var mediaFile1 = File(mediaStorageDirectory.path + File.separator + name)
        cancel.setOnClickListener {
            window.dismiss()
        }
        if (mediaFile1.exists()) {
            Picasso.with(this).load(mediaFile1).into(image_preview)
        } else {
            window.dismiss()
        }

    }

    fun showAdharDialog(adharData: SampleEntity) {
        if (adharData != null) {
            val mDialogView =
                LayoutInflater.from(this).inflate(R.layout.digitalolocker_data_info, null)
            val mBuilder = AlertDialog.Builder(this)
                .setView(mDialogView)
            val mAlertDialog = mBuilder.show()
            mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            var poi: Poi = adharData.certificateData.uidData.poi
            mDialogView.tvName.text = poi.name
            mDialogView.tvDob.text = poi.dob
            mDialogView.tvgender.text = poi.gender
            //    mDialogView.txt_title.text = getString(R.string.aadhaardetail)
            var address_en: String = ""
            var addresslin1: String = ""
            var addresslin2: String = ""
            var poa = adharData.certificateData.uidData.poa
            adharno = adharData.certificateData.uidData.uid
            if (poa.co != null) {
                address_en = address_en + "," + poa.co
                addresslin1 = addresslin1 + "," + poa.co
            }
            if (poa.house != null) {

                address_en = address_en + "," + poa.house
                addresslin1 = addresslin1 + "," + poa.house

            }
            if (poa.street != null) {
                address_en = address_en + "," + poa.street
                addresslin1 = addresslin1 + "," + poa.street
            }
            if (poa.lm != null) {
                address_en = address_en + "," + poa.lm
                addresslin1 = addresslin1 + "," + poa.lm
            }
            if (poa.loc != null) {
                address_en = address_en + "," + poa.loc
                addresslin2 = addresslin2 + "," + poa.loc
            }
            if (poa.vtc != null) {
                address_en = address_en + "," + poa.vtc
                addresslin2 = addresslin2 + "," + poa.vtc
            }
            if (poa.dist != null) {
                address_en = address_en + "," + poa.dist
                addresslin2 = addresslin2 + "," + poa.dist
            }
            if (poa.state != null) {
                address_en = address_en + "," + poa.state
                addresslin2 = addresslin2 + "," + poa.state
            }
            if (poa.pc != null) {
                address_en = address_en + "," + poa.pc
            }
            mDialogView.tvAddress.text = address_en
            var lData = adharData.certificateData.uidData.lData
            mDialogView.tvName_hi.text = lData.name
            var address_hi: String = ""

            if (lData.co != null) {
                address_hi = address_hi + "," + lData.co
            }
            if (lData.house != null) {
                address_hi = address_hi + "," + lData.house
            }
            if (lData.street != null) {
                address_hi = address_hi + "," + lData.street
            }
            if (lData.lm != null) {
                address_hi = address_hi + "," + lData.lm
            }
            if (lData.loc != null) {
                address_hi = address_hi + "," + lData.loc
            }
            if (lData.vtc != null) {
                address_hi = address_hi + "," + lData.vtc
            }
            if (lData.dist != null) {
                address_hi = address_hi + "," + lData.dist
            }
            if (lData.state != null) {
                address_hi = address_hi + "," + lData.state
            }
            if (lData.pc != null) {
                address_hi = address_hi + "," + lData.pc
            }
            mDialogView.tvAddress_hi.text = address_hi
            mDialogView.btnset.setOnClickListener {
                if (validate!!.RetriveSharepreferenceInt(AppSP.CboType) == 1) {
                    if (poi.gender.equals("M")) {
                        validate!!.CustomAlert(
                            LabelSet.getText(
                                "youcannotaddmale",
                                R.string.youcannotaddmale
                            ), this
                        )

                    } else {
                        fillAdharDetail(adharData)
                    }
                } else {
                    fillAdharDetail(adharData)
                }

                mAlertDialog.dismiss()


            }
            mDialogView.btn_cancel.setOnClickListener {


                mAlertDialog.dismiss()


            }
        }
    }

    fun CustomAlertPartialsave(str: String, java: Class<*>) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = androidx.appcompat.app.AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str
        mDialogView.txt_dialog_title.text = LabelSet.getText("app_name", R.string.app_name)
        mDialogView.btn_yes.text = LabelSet.getText("yes", R.string.yes)
        mDialogView.btn_no.text = LabelSet.getText("no", R.string.no)
        mDialogView.btn_yes.setOnClickListener {
            //  PartialSaveData()
            mAlertDialog.dismiss()
            val intent = Intent(this, java)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)


        }
        mDialogView.btn_no.setOnClickListener {
            mAlertDialog.dismiss()
            PartialSaveData()
            val intent = Intent(this, java)
            startActivity(intent)
            finish()


        }
    }

    fun setLabelText() {
        tv_nameSurname.text = LabelSet.getText(
            "name_including_surname",
            R.string.name_including_surname
        )
        et_name.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_namelocal.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_age.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_ason.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_mother_father.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_guardianName.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_nameSurnamelocal.text = LabelSet.getText(
            "name_in_local",
            R.string.name_in_local
        )
        tv_Gender.text = LabelSet.getText(
            "gender",
            R.string.gender
        )
        tv_memberDate.text = LabelSet.getText(
            "dob_available",
            R.string.dob_available
        )
        tvDOB.text = LabelSet.getText("dob", R.string.dob)
        tvAge.text = LabelSet.getText("age", R.string.age)
        tvAsOnDate.text = LabelSet.getText(
            "as_on",
            R.string.as_on
        )
        tvMemberJoiningDate.text = LabelSet.getText(
            "joiningDate",
            R.string.joiningDate
        )
        tvAadharCard.text = LabelSet.getText(
            "aadhar_Card_scanner",
            R.string.aadhar_Card_scanner
        )
        et_scanaadhar.hint = LabelSet.getText(
            "scan_your_aadhar_card",
            R.string.scan_your_aadhar_card
        )
        tvMartialStatus.text = LabelSet.getText(
            "martial_status",
            R.string.martial_status
        )
        tvMotherFather.text = LabelSet.getText(
            "father_husband",
            R.string.father_husband
        )
        tv_Relation.text = LabelSet.getText(
            "relationwithfather",
            R.string.relationwithfather
        )
        tvFamilyHead.text = LabelSet.getText(
            "head_of_family",
            R.string.head_of_family
        )
        tvDisability.text = LabelSet.getText(
            "disability",
            R.string.disability
        )
        tvDisabilityType.text = LabelSet.getText(
            "disability_type",
            R.string.disability_type
        )
        tvGuardianName.text = LabelSet.getText(
            "guardian_name",
            R.string.guardian_name
        )
        tvRelationWithGuardian.text = LabelSet.getText(
            "relaion_guardian",
            R.string.relaion_guardian
        )
        tvPost.text = LabelSet.getText(
            "post_designation",
            R.string.post_designation
        )
        tvSocialCategory.text = LabelSet.getText(
            "social_category",
            R.string.social_category
        )
        tv_PVTG.text = LabelSet.getText("pvtg", R.string.pvtg)
        tvReligion.text = LabelSet.getText(
            "religion",
            R.string.religion
        )
        tvHighestEducation.text = LabelSet.getText(
            "education",
            R.string.education
        )
        tvPrimaryOccupation.text = LabelSet.getText(
            "occupation",
            R.string.occupation
        )
        tvSecondaryOccupation.text = LabelSet.getText(
            "secondary_occupation",
            R.string.secondary_occupation
        )
        tvTertiaryOccupation.text = LabelSet.getText(
            "tertiaryoccupation",
            R.string.tertiaryoccupation
        )
        tvPaasbookPage.text = LabelSet.getText(
            "member_image",
            R.string.member_image
        )
        tvStatus.text = LabelSet.getText(
            "status",
            R.string.status
        )
        tvInactiveDate.text = LabelSet.getText(
            "leaving_date",
            R.string.leaving_date
        )
        tvInactivePerson.text = LabelSet.getText(
            "inactive_Reason",
            R.string.inactive_Reason
        )
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_savegray.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText("cancel", R.string.cancel)
        tvSettelMent.text = LabelSet.getText(
            "settelment",
            R.string.settelment
        )
    }

    fun insertKycAddressdata() {
        if (nameMatch == 1) {
            if (adharno.isNotEmpty() && adharscan) {
                var entrySource = 0
                if (adharscandigi && (kycEntrySource == 4 || kycEntrySource == 41)) {
                    entrySource = 41
                } else if (adharscan && (kycEntrySource == 4 || kycEntrySource == 42)) {
                    entrySource = 42
                } else if (kycEntrySource == 4) {
                    entrySource = 40
                } else if (adharscandigi) {
                    entrySource = 1
                } else if (adharscan) {
                    entrySource = 2
                }
                var kycGuid = memberkycviewmodel!!.getKycGuidbyKycType(
                    validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID), 2
                )
                if (!kycGuid.isNullOrEmpty()) {
                    memberkycviewmodel!!.updateKycNumber(
                        kycGuid,
                        adharno,
                        1,
                        entrySource,
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                    )
                } else {
                    var kycguid = validate!!.random()
                    var memberKYCEntity = Member_KYC_Entity(
                        0,
                        membercode,
                        validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!,
                        kycguid,
                        2,
                        adharno, "",
                        "",
                        "",
                        "",
                        "",
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                        0,
                        "",
                        0,
                        "",
                        1,
                        1, entrySource, 1, 1, "", 1, 0
                    )
                    memberkycviewmodel!!.insert(memberKYCEntity)
                }
            }
            if (addline1.isNotEmpty() && adharscan) {
                var entrysource = 0
                if (adharscandigi) {
                    entrysource = 1
                } else if (adharscan) {
                    entrysource = 2
                }
                var addressGuid = memberaddressviewmodel!!.getAddressByEntrySource(
                    validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID), entrysource
                )
                if (addressGuid.isNullOrEmpty()) {

                    var addressEntity = member_addressEntity(
                        0,
                        membercode,
                        validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID),
                        validate!!.random(),
                        1,
                        addline1,
                        addline2,
                        validate!!.RetriveSharepreferenceInt(AppSP.villagecode),
                        validate!!.RetriveSharepreferenceInt(AppSP.panchayatcode),
                        validate!!.RetriveSharepreferenceInt(AppSP.blockcode),
                        "",
                        validate!!.RetriveSharepreferenceInt(AppSP.statecode),
                        validate!!.RetriveSharepreferenceInt(AppSP.districtcode),
                        pincode.toString(),
                        1,
                        entrysource,
                        1,
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                        0,
                        "",
                        0,
                        "", 1, 0, 1, 2
                    )
                    memberaddressviewmodel!!.insert(addressEntity)
                } else {

                    memberaddressviewmodel!!.updateAddressdata(
                        addressGuid,
                        addline1,
                        addline2,
                        validate!!.RetriveSharepreferenceInt(AppSP.villagecode),
                        validate!!.RetriveSharepreferenceInt(AppSP.panchayatcode),
                        validate!!.RetriveSharepreferenceInt(AppSP.blockcode),
                        validate!!.RetriveSharepreferenceInt(AppSP.statecode),
                        validate!!.RetriveSharepreferenceInt(AppSP.districtcode),
                        pincode.toString(),
                        1,
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                        2
                    )
                }
            }
        }
    }

    private fun checkData(): Int {
        var value = 1
        if(!validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID).isNullOrEmpty()){
            var list =
                memberViewmodel!!.getMemberdetail(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))

            if (et_name.text.toString() != validate!!.returnStringValue(list?.get(0)?.member_name)) {

                value = 0
                return value
            } else if (et_membercode.text.toString() != validate!!.returnStringValue(list?.get(0)?.member_code.toString())) {

                value = 0
                return value
            } else if (et_namelocal.text.toString() != validate!!.returnStringValue(list?.get(0)?.member_name_local)) {

                value = 0
                return value
            } else if (et_namelocal.text.toString() != validate!!.returnStringValue(list?.get(0)?.member_name_local)) {

                value = 0
                return value
            } else if (spin_gender.selectedItemPosition != validate!!.returnIntegerValue(list?.get(0)?.gender.toString())) {

                value = 0
                return value
            } else if (spin_martial.selectedItemPosition != validate!!.returnIntegerValue(list?.get(0)?.marital_status.toString())) {

                value = 0
                return value
            } else if (spin_relationmother.selectedItemPosition != validate!!.returnIntegerValue(
                    list?.get(
                        0
                    )?.father_husband.toString()
                )
            ) {

                value = 0
                return value
            } else if (rgisDobavailable.checkedRadioButtonId != validate!!.returnIntegerValue(
                    list?.get(
                        0
                    )?.dob_available.toString()
                )
            ) {

                value = 0
                return value
            } else if (validate!!.Daybetweentime(et_dob.text.toString()) != validate!!.returnLongValue(
                    list?.get(0)?.dob.toString()
                )
            ) {

                value = 0
            } else if (validate!!.Daybetweentime(et_ason.text.toString()) != validate!!.returnLongValue(
                    list?.get(0)?.age_as_on.toString()
                )
            ) {

                value = 0
                return value
            } else if (validate!!.Daybetweentime(et_joiningDate.text.toString()) != validate!!.returnLongValue(
                    list?.get(0)?.joining_date.toString()
                )
            ) {

                value = 0
                return value
            } else if (et_age.text.toString() != validate!!.returnIntegerValue(list?.get(0)?.age.toString())
                    .toString()
            ) {

                value = 0
                return value
            } else if (et_age.text.toString() != validate!!.returnIntegerValue(list?.get(0)?.age.toString())
                    .toString()
            ) {

                value = 0
                return value
            } else if (et_mother_father.text.toString() != validate!!.returnStringValue(list?.get(0)?.relation_name)) {

                value = 0
                return value
            } else if (et_mother_fatherlocal.text.toString() != validate!!.returnStringValue(list?.get(0)?.relation_name_local)) {

                value = 0
                return value
            } else if (rgheadofFamily.checkedRadioButtonId != validate!!.returnIntegerValue(list?.get(0)?.head_house_hold.toString())) {

                value = 0
                return value
            } else if (rgDisability.checkedRadioButtonId != validate!!.returnIntegerValue(list?.get(0)?.is_disabled.toString())) {

                value = 0
                return value
            } else if (spin_disabilityType.selectedItemPosition != validate!!.returnIntegerValue(
                    list?.get(
                        0
                    )?.disability_details
                )
            ) {

                value = 0
                return value
            } else if (et_guardianName.text.toString() != validate!!.returnStringValue(list?.get(0)?.guardian_name)) {

                value = 0
                return value
            } else if (et_guardianNamelocal.text.toString() != validate!!.returnStringValue(list?.get(0)?.guardian_name_local)) {

                value = 0
                return value
            } else if (spin_relationGuardian.selectedItemPosition != validate!!.returnIntegerValue(
                    list?.get(
                        0
                    )?.guardian_relation.toString()
                )
            ) {

                value = 0
                return value
            } else if (spin_Post.selectedItemPosition != validate!!.returnIntegerValue(list?.get(0)?.designation.toString())) {

                value = 0
                return value
            } else if (spin_socialCategory.selectedItemPosition != validate!!.returnIntegerValue(
                    list?.get(
                        0
                    )?.social_category.toString()
                )
            ) {

                value = 0
                return value
            } else if (rgpvtg.checkedRadioButtonId != validate!!.returnIntegerValue(list?.get(0)?.tribal_category.toString())) {
                value = 0
                return value
            } else if (spin_religion.selectedItemPosition != validate!!.returnIntegerValue(list?.get(0)?.religion.toString())) {
                value = 0
                return value
            } else if (spin_education.selectedItemPosition != validate!!.returnIntegerValue(list?.get(0)?.highest_education_level.toString())) {
                value = 0
                return value
            } else if (spin_occupation.selectedItemPosition != validate!!.returnIntegerValue(list?.get(0)?.primary_occupation.toString())) {
                value = 0
                return value
            } else if (spin_secondaryoccu.selectedItemPosition != validate!!.returnIntegerValue(
                    list?.get(
                        0
                    )?.secondary_occupation.toString()
                )
            ) {
                value = 0
                return value
            } else if (spin_tertiaryoccu.selectedItemPosition != validate!!.returnIntegerValue(
                    list?.get(
                        0
                    )?.tertiary_occupation.toString()
                )
            ) {
                value = 0
                return value
            } else if (spin_status.selectedItemPosition != validate!!.returnIntegerValue(list?.get(0)?.status.toString())) {
                value = 0
                return value
            } else if (validate!!.Daybetweentime(et_leavingDate.text.toString()) != validate!!.returnLongValue(
                    list?.get(0)?.leaving_date.toString()
                )
            ) {
                value = 0
                return value
            } else if (spin_inactivereason.selectedItemPosition != validate!!.returnIntegerValue(
                    list?.get(
                        0
                    )?.reason_for_leaving.toString()
                )
            ) {
                value = 0
                return value
            } else if (spin_settlementstatus.selectedItemPosition != validate!!.returnIntegerValue(
                    list?.get(
                        0
                    )?.settlement_status.toString()
                )
            ) {
                value = 0
                return value
            }


        }else {
            value = 0
            return value
        }
        return value
    }

    fun promptSpeechInput(language: String) {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )
        if (isNetworkConnected()){
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, language)
        }
        else{
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.ENGLISH)
        }
        intent.putExtra(
            RecognizerIntent.EXTRA_PROMPT,
            getString(R.string.speech_prompt)
        )
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT)
        } catch (a: ActivityNotFoundException) {
            Toast.makeText(
                applicationContext,
                getString(R.string.speech_not_supported),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun isNetworkConnected(): Boolean {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager //1
        val networkInfo = connectivityManager.activeNetworkInfo //2
        return networkInfo != null && networkInfo.isConnected //3
    }
}
