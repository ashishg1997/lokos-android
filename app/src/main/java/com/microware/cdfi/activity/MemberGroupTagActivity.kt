package com.microware.cdfi.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.entity.MemberSystemTagEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_member_group_tag.*
import kotlinx.android.synthetic.main.tablayout.*
import kotlinx.android.synthetic.main.white_toolbar.*

class MemberGroupTagActivity : AppCompatActivity() {
    var validate: Validate? = null
    var membercode = 0L
    var memebrname = ""
    var lookupViewmodel: LookupViewmodel? = null
    var memberSystemtagViewmodel: MemberSystemtagViewmodel? = null
    var cadreMemberViewModel: CadreMemberViewModel? = null
    var dataspin_system: List<LookupEntity>? = null
    var memberSystemTagEntity: MemberSystemTagEntity? = null
    var memberviewmodel: Memberviewmodel? = null
    var memberPhoneViewmodel: MemberPhoneViewmodel? = null
    var memberbankviewmodel: MemberBankViewmodel? = null
    var memberaddressviewmodel: MemberAddressViewmodel? = null
    var memberkycviewmodel: MemberKYCViewmodel? = null
    var shgViewmodel: SHGViewmodel? = null

    var isVerified = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_member_group_tag)
        validate = Validate(this)
        setLabelText()

        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        memberSystemtagViewmodel = ViewModelProviders.of(this).get(MemberSystemtagViewmodel::class.java)
        memberviewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)
        memberPhoneViewmodel = ViewModelProviders.of(this).get(MemberPhoneViewmodel::class.java)
        memberbankviewmodel = ViewModelProviders.of(this).get(MemberBankViewmodel::class.java)
        memberaddressviewmodel = ViewModelProviders.of(this).get(MemberAddressViewmodel::class.java)
        memberkycviewmodel = ViewModelProviders.of(this).get(MemberKYCViewmodel::class.java)
        shgViewmodel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        cadreMemberViewModel = ViewModelProviders.of(this).get(CadreMemberViewModel::class.java)

        membercode = validate!!.RetriveSharepreferenceLong(AppSP.membercode)
        memebrname = validate!!.RetriveSharepreferenceString(AppSP.memebrname)!!
        tvCode.text = membercode.toString()
        et_shgcode.setText(memebrname)
        ivHome.visibility = View.GONE
        if (validate!!.RetriveSharepreferenceInt(AppSP.MemberLockRecord) == 1) {
            ivLock.visibility = View.VISIBLE
            btn_addgray.visibility = View.VISIBLE
            btn_add.visibility = View.GONE

        } else {
            ivLock.visibility = View.GONE
            btn_addgray.visibility = View.GONE
            btn_add.visibility = View.VISIBLE
        }
        ivBack.setOnClickListener {
            var intent = Intent(this,MemberListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            finish()
        }

        ivHome.setOnClickListener {
            var intent = Intent(this,MainActivityDrawer::class.java)
            intent.flags= Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

        tv_title.text =
            validate!!.RetriveSharepreferenceString(AppSP.memebrname)!! + "(" + validate!!.RetriveSharepreferenceString(AppSP.ShgName) + ")"
        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvCader.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
        tvVector.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_cader.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        var memberlist =
            memberviewmodel!!.getmember(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
        var memberphonelist =
            memberPhoneViewmodel!!.getphoneDatalistcount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
        var memberaddresslist =
            memberaddressviewmodel!!.getAddressdatalistcount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
        var memberbanklist =
            memberbankviewmodel!!.getBankdetaildatalistcount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
        var memberkyclist =
            memberkycviewmodel!!.getKycdetaildatalistcount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))

        var cadreshgMemberlist = cadreMemberViewModel!!.getCadreListdata1(
            validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!)
        if (!cadreshgMemberlist.isNullOrEmpty()){
            IvCader.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }

        if (!memberlist.isNullOrEmpty()){
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberaddresslist.isNullOrEmpty()){
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberbanklist.isNullOrEmpty()){
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberkyclist.isNullOrEmpty()){
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberphonelist.isNullOrEmpty()){
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        lay_vector.setOnClickListener {
            var intent = Intent(this, MemberDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_phone.setOnClickListener {
            var intent = Intent(this, MemeberPhoneListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_location.setOnClickListener {
            var intent = Intent(this,MemberAddressListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_bank.setOnClickListener {
            var intent = Intent(this, MemberBankListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_kyc.setOnClickListener {
            var intent = Intent(this, MemberIdListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_Cader.setOnClickListener {
            var intent = Intent(this, CadreListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        btn_add.setOnClickListener {
            if (!validate!!.RetriveSharepreferenceString(AppSP.SHGGUID).isNullOrEmpty()) {
                if (checkValidation() == 1) {
                    Savemembertag()
                }
            } else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "insert_Basic_data_first",
                        R.string.insert_Basic_data_first
                    ),
                    this,
                    BasicDetailActivity::class.java
                )
            }
        }
        spin_Membersystem?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {
                    var tagId =
                        validate!!.returnlookupcode(spin_Membersystem, dataspin_system)
                    if (tagId == 1) {
                        et_id.isEnabled = false
                        et_id.setText("")
                    }else if(tagId == 2){
                        et_id.isEnabled = true
                    }
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
// TODO Auto-generated method stub

            }
        }

        fillSpinner()
        showData()
        //  setLabelText()
    }
    private fun showData() {
        var list =
            memberSystemtagViewmodel!!.getSystemtag(validate!!.RetriveSharepreferenceString(AppSP.MemberSystemtagGUID))
        if(!list.isNullOrEmpty()){
            if(validate!!.returnIntegerValue(list.get(0).system_type.toString())==1){
                spin_Membersystem.isEnabled = false
                et_id.isEnabled = false
            }else {
                spin_Membersystem.isEnabled = true
                et_id.isEnabled = true
            }
            spin_Membersystem.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(list.get(0).system_type.toString()),
                    dataspin_system
                )
            )

            et_id.setText((validate!!.returnStringValue(list.get(0).system_id.toString()))
            )
        }
    }

    fun checkValidation(): Int {
        var systemId = memberSystemtagViewmodel!!.getSystemtagcount( validate!!.returnStringValue(et_id.text.toString()))
        var value = 1
        if (spin_Membersystem.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,

                spin_Membersystem,
                LabelSet.getText(
                    "pleaseselectsystemid",
                    R.string.pleaseselectsystemid
                )
            )
            value = 0
            return value
        } else if (spin_Membersystem.selectedItemPosition > 0 && et_id.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "pleaseenetrid",
                    R.string.pleaseenetrid
                ),
                this,
                et_id
            )
            value = 0
            return value
        }else if ((validate!!.RetriveSharepreferenceString(AppSP.MemberSystemtagGUID).isNullOrEmpty() && systemId> 0) ||
            (!validate!!.RetriveSharepreferenceString(AppSP.MemberSystemtagGUID).isNullOrEmpty() && systemId>1)) {
            validate!!.CustomAlertEditText(LabelSet.getText(
                "pleaseenterdifferenttagid",
                R.string.pleaseenterdifferenttagid
            ),
                this,
                et_id
            )
            value = 0
            return value
        }


        return value

    }


    private fun fillSpinner() {


        dataspin_system = lookupViewmodel!!.getlookup(
            55,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        validate!!.fillspinner(this, spin_Membersystem, dataspin_system)


    }


    private fun Savemembertag() {

        var tagguid = validate!!.random()
        var systemid = validate!!.returnlookupcode(spin_Membersystem, dataspin_system)

        if (validate!!.RetriveSharepreferenceString(AppSP.MemberSystemtagGUID).isNullOrEmpty()) {
            memberSystemTagEntity = MemberSystemTagEntity(0,
                validate!!.returnLongValue(validate!!.RetriveSharepreferenceString(AppSP.Shgcode)),
                validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!,
                membercode,
                tagguid,
                systemid,
                validate!!.returnStringValue(et_id.text.toString()),
                1,
                1,
                1,
                1,
                0,
                "",
                validate!!.Daybetweentime(validate!!.currentdatetime),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                0,
                "",1)
            memberSystemtagViewmodel!!.insert(memberSystemTagEntity!!)
            validate!!.SaveSharepreferenceString(AppSP.MemberSystemtagGUID, tagguid)
            validate!!.updateMemberEditFlag(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),shgViewmodel,memberviewmodel)
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this,
                MemberGroupTagList::class.java
            )
        }else {
            if(checkData()==0){
                memberSystemtagViewmodel!!.updateSystemTagdata(
                    systemid,
                    validate!!.returnStringValue(et_id.text.toString()),
                    validate!!.RetriveSharepreferenceString(AppSP.MemberSystemtagGUID)!!,
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                )
                validate!!.updateMemberEditFlag(
                    validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),
                    shgViewmodel,
                    memberviewmodel
                )

                validate!!.CustomAlert(
                    LabelSet.getText(
                        "updated_successfully",
                        R.string.updated_successfully
                    ), this,
                    MemberGroupTagList::class.java
                )

            }else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "updated_successfully",
                        R.string.updated_successfully
                    ), this,
                    MemberGroupTagList::class.java
                )
            }
        }

    }

    override fun onBackPressed() {
        var intent = Intent(this,MemberGroupTagList::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun  setLabelText()
    {
        tvSystemID.text = LabelSet.getText(
            "system_tag_id",
            R.string.system_tag_id
        )
        tv_ID.text = LabelSet.getText("id", R.string.id)
        btn_add.text = LabelSet.getText("id", R.string.add_system_tab)
        btn_addgray.text = LabelSet.getText("id", R.string.add_system_tab)
    }

    private fun checkData(): Int {
        var value = 1

        var list =
            memberSystemtagViewmodel!!.getSystemtag(validate!!.RetriveSharepreferenceString(AppSP.MemberSystemtagGUID))
        if (validate!!.returnStringValue(et_id.text.toString()) != validate!!.returnStringValue(
                list?.get(0)?.system_id
            )
        ) {

            value = 0

        } else if (validate!!.returnlookupcode(spin_Membersystem, dataspin_system) != validate!!.returnIntegerValue(
                list?.get(0)?.system_type.toString()
            )
        ) {

            value = 0
        }


        return value
    }

}