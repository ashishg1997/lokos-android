package com.microware.cdfi.activity

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.MemberSystemtagAdapter
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.entity.MemberSystemTagEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_member_group_tag_list.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.btn_no
import kotlinx.android.synthetic.main.customealertdialogepartial.view.btn_yes
import kotlinx.android.synthetic.main.layout_verification_dialog.view.*
import kotlinx.android.synthetic.main.tablayout.*
import kotlinx.android.synthetic.main.white_toolbar.*

class MemberGroupTagList : AppCompatActivity() {
    var validate: Validate? = null
    var lookupViewmodel: LookupViewmodel? = null
    var memberSystemtagViewmodel: MemberSystemtagViewmodel? = null
    var dataspin_system: List<LookupEntity>? = null
    var dataspin_verify: List<LookupEntity>? = null
    var memberviewmodel: Memberviewmodel? = null
    var memberPhoneViewmodel: MemberPhoneViewmodel? = null
    var memberbankviewmodel: MemberBankViewmodel? = null
    var memberaddressviewmodel: MemberAddressViewmodel? = null
    var memberkycviewmodel: MemberKYCViewmodel? = null
    var cadreMemberViewModel: CadreMemberViewModel? = null
    var shgViewmodel: SHGViewmodel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_member_group_tag_list)

        validate = Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        memberSystemtagViewmodel =
            ViewModelProviders.of(this).get(MemberSystemtagViewmodel::class.java)
        memberviewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)
        memberPhoneViewmodel = ViewModelProviders.of(this).get(MemberPhoneViewmodel::class.java)
        memberbankviewmodel = ViewModelProviders.of(this).get(MemberBankViewmodel::class.java)
        memberaddressviewmodel = ViewModelProviders.of(this).get(MemberAddressViewmodel::class.java)
        memberkycviewmodel = ViewModelProviders.of(this).get(MemberKYCViewmodel::class.java)
        shgViewmodel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        cadreMemberViewModel = ViewModelProviders.of(this).get(CadreMemberViewModel::class.java)


        ivBack.setOnClickListener {
            var intent = Intent(this, MemberListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            finish()
        }

        ivHome.setOnClickListener {
            var intent = Intent(this, MainActivityDrawer::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }
        if (validate!!.RetriveSharepreferenceInt(AppSP.MemberLockRecord) == 1) {
            ivLock.visibility = View.VISIBLE
            addSystemTaggray.visibility = View.VISIBLE
            addSystemTag.visibility = View.GONE

        } else {
            ivLock.visibility = View.GONE
            addSystemTaggray.visibility = View.GONE
            addSystemTag.visibility = View.VISIBLE
        }
        tv_title.text =
            validate!!.RetriveSharepreferenceString(AppSP.memebrname)!! + "(" + validate!!.RetriveSharepreferenceString(AppSP.ShgName) + ")"
        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvCader.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
        tvVector.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_cader.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        var memberlist =
            memberviewmodel!!.getmember(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
        var memberphonelist =
            memberPhoneViewmodel!!.getphoneDatalistcount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
        var memberaddresslist =
            memberaddressviewmodel!!.getAddressdatalistcount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
        var memberbanklist =
            memberbankviewmodel!!.getBankdetaildatalistcount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
        var memberkyclist =
            memberkycviewmodel!!.getKycdetaildatalistcount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
        var cadreshgMemberlist = cadreMemberViewModel!!.getCadreListdata1(
            validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!)
        if (!cadreshgMemberlist.isNullOrEmpty()){
            IvCader.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }

        if (!memberlist.isNullOrEmpty()){
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberaddresslist.isNullOrEmpty()){
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberbanklist.isNullOrEmpty()){
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberkyclist.isNullOrEmpty()){
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberphonelist.isNullOrEmpty()){
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        lay_vector.setOnClickListener {
            var intent = Intent(this, MemberDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_phone.setOnClickListener {
            var intent = Intent(this, MemeberPhoneListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_location.setOnClickListener {
            var intent = Intent(this, MemberAddressListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_bank.setOnClickListener {
            var intent = Intent(this, MemberBankListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_kyc.setOnClickListener {
            var intent = Intent(this, MemberIdListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        lay_Cader.setOnClickListener {
            var intent = Intent(this, CadreListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        addSystemTag.setOnClickListener {
            if (!validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID).isNullOrEmpty()) {
                validate!!.SaveSharepreferenceString(AppSP.MemberSystemtagGUID,"")
                var intent = Intent(this, MemberGroupTagActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "add_memeber_data_first",
                        R.string.add_memeber_data_first
                    ),
                    this,
                    BasicDetailActivity::class.java
                )
            }
        }

        if(memberviewmodel!!.getFinalVerifyCount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))>0){
            btn_Verify.visibility = View.VISIBLE
        }else {
            btn_Verify.visibility = View.GONE
        }

        btn_Verify.setOnClickListener {
            if(sCheckValidation()==1){
                CustomAlertVerification()
            }
        }
        fillData()
    }

    fun sCheckValidation():Int{
        var value = 1
        var isVerify = memberviewmodel!!.getIsVerified(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        if(isVerify == 1){
            validate!!.CustomAlert(LabelSet.getText(
                "complete_member",
                R.string.complete_member
            ),this)
            value = 0
            return value
        }else if(memberPhoneViewmodel!!.getVerificationCount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))>0){
            validate!!.CustomAlert(LabelSet.getText(
                "complete_phone",
                R.string.complete_phone
            ),this)
            value = 0
            return value
        }else if(memberaddressviewmodel!!.getVerificationCount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))>0){
            validate!!.CustomAlert(LabelSet.getText(
                "complete_address",
                R.string.complete_address
            ),this)
            value = 0
            return value
        }else if(memberbankviewmodel!!.getVerificationCount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))>0){
            validate!!.CustomAlert(LabelSet.getText(
                "complete_bank",
                R.string.complete_bank
            ),this)
            value = 0
            return value
        }

        return value
    }

     fun fillData() {
        memberSystemtagViewmodel!!.getSystemtagdata(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))!!
            .observe(this, object : Observer<List<MemberSystemTagEntity>> {
                override fun onChanged(system_list: List<MemberSystemTagEntity>?) {
                    if (system_list!=null) {
                        rvList.layoutManager = LinearLayoutManager(this@MemberGroupTagList)
                        rvList.adapter =
                            MemberSystemtagAdapter(this@MemberGroupTagList, system_list)
                        if(system_list.size>0){
                            lay_nosystem_avialable.visibility = View.GONE
                        }else {
                            lay_nosystem_avialable.visibility = View.VISIBLE
                            tv_nosystem_avialable.text = LabelSet.getText(
                                "no_system_s_avialable",
                                R.string.no_system_s_avialable
                            )
                        }
                    }
                }
            })
    }

    fun returnlookupvalue(id: Int): String {
        dataspin_system = lookupViewmodel!!.getlookup(
            55,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        return validate!!.returnlookupcodevalue(id, dataspin_system)
    }

    override fun onBackPressed() {
        var intent = Intent(this, MemberListActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun CustomAlert(guid:String,iFlag:Int) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = LabelSet.getText(
            "do_u_want_to_delete",
            R.string.do_u_want_to_delete
        )
        mDialogView.txt_dialog_title.text = LabelSet.getText("app_name", R.string.app_name)
        mDialogView.btn_yes.text = LabelSet.getText("yes", R.string.yes)
        mDialogView.btn_no.text = LabelSet.getText("no", R.string.no)
        mDialogView.btn_yes.setOnClickListener {

            if(iFlag==0){
                memberSystemtagViewmodel!!.deleteRecord(guid)
            }else {
                memberSystemtagViewmodel!!.deleteData(guid)

            }
            validate!!.updateMemberEditFlag(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),shgViewmodel,memberviewmodel)

            mAlertDialog.dismiss()

        }
        mDialogView.btn_no.setOnClickListener {

            mAlertDialog.dismiss()

        }
    }

    fun CustomAlertVerification() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.layout_verification_dialog, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.tvVerified.text = LabelSet.getText(
            "verification_status",
            R.string.verification_status
        )
        dataspin_verify = lookupViewmodel!!.getlookup(
            56,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        validate!!.fillspinner(this, mDialogView.spin_verified, dataspin_verify)
        mDialogView.txt_dialog_title1.text = LabelSet.getText("app_name", R.string.app_name)
        mDialogView.btn_yes.text = LabelSet.getText("yes", R.string.yes)
        mDialogView.btn_no.text = LabelSet.getText("no", R.string.no)
        mDialogView.btn_yes.setOnClickListener {

            if(mDialogView.spin_verified.selectedItemPosition>0){
                memberviewmodel!!.updateIsVerified(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!,validate!!.returnlookupcode(mDialogView.spin_verified, dataspin_verify),validate!!.Daybetweentime(validate!!.currentdatetime),
                    validate!!.RetriveSharepreferenceString(AppSP.userid)!!)
                validate!!.CustomAlert(LabelSet.getText(
                    "",
                    R.string.verified_successfully
                ),this)
                if(memberviewmodel!!.getFinalVerifyCount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))>0){
                    btn_Verify.visibility = View.VISIBLE
                }else {
                    btn_Verify.visibility = View.GONE
                }
                mAlertDialog.dismiss()
            }else {
                var msg = LabelSet.getText(
                    "",
                    R.string.select_verification_status
                )
                validate!!.CustomAlertSpinner(this,mDialogView.spin_verified,msg)
            }



        }

        mDialogView.btn_no.setOnClickListener {

            mAlertDialog.dismiss()

        }
    }
}