package com.microware.cdfi.activity

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Matrix
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.text.InputFilter
import android.text.InputType
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.PopupWindow
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.BuildConfig
import com.microware.cdfi.R
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.ImageuploadEntity
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.entity.Member_KYC_Entity
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_member_id.*
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.tablayout.*
import kotlinx.android.synthetic.main.tablayout.lay_bank
import kotlinx.android.synthetic.main.white_toolbar.*
import java.io.*
import java.text.SimpleDateFormat
import java.util.*

class MemberIdActivity : AppCompatActivity() {

    var validate: Validate? = null
    var memberKYCEntity: Member_KYC_Entity? = null
    var memberkycViewmodel: MemberKYCViewmodel? = null
    var cadreMemberViewModel: CadreMemberViewModel? = null
    var frontName = ""
    var rearName = ""
    var newimagename = false
    var bitmap: Bitmap? = null
    private var fileUri: Uri? = null
    var imgPathUpload = ""
    var imgNameUpload = ""
    var guid = ""
    var membercode = 0L
    var entrysource = 0
    var isVerified = 0
    var memberName = ""
    var IsDigilocker = false
    var fromDigilockerVlaue = ""
    var lookupViewmodel: LookupViewmodel? = null
    var imageUploadViewmodel: ImageUploadViewmodel? = null
    var dataspin_doctype: List<LookupEntity>? = null
    var memberviewmodel: Memberviewmodel? = null
    var memberPhoneViewmodel: MemberPhoneViewmodel? = null
    var memberbankviewmodel: MemberBankViewmodel? = null
    var memberaddressviewmodel: MemberAddressViewmodel? = null
    var membersystemviewmodel: MemberSystemtagViewmodel? = null
    var shgViewmodel: SHGViewmodel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_member_id)
        validate = Validate(this)
        setLabelText()

        memberkycViewmodel = ViewModelProviders.of(this).get(MemberKYCViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        imageUploadViewmodel = ViewModelProviders.of(this).get(ImageUploadViewmodel::class.java)
        memberviewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)
        memberPhoneViewmodel = ViewModelProviders.of(this).get(MemberPhoneViewmodel::class.java)
        memberbankviewmodel = ViewModelProviders.of(this).get(MemberBankViewmodel::class.java)
        memberaddressviewmodel = ViewModelProviders.of(this).get(MemberAddressViewmodel::class.java)
        cadreMemberViewModel = ViewModelProviders.of(this).get(CadreMemberViewModel::class.java)
        shgViewmodel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        membersystemviewmodel =
            ViewModelProviders.of(this).get(MemberSystemtagViewmodel::class.java)

        membercode = validate!!.RetriveSharepreferenceLong(AppSP.membercode)
        memberName = validate!!.RetriveSharepreferenceString(AppSP.memebrname)!!
        tvCode.text = membercode.toString()
        et_membercode.setText(memberName)

        ivHome.visibility = View.GONE
        if (validate!!.RetriveSharepreferenceInt(AppSP.MemberLockRecord) == 1 || validate!!.RetriveSharepreferenceInt(AppSP.MemberKycStatus)==2 ||
            validate!!.RetriveSharepreferenceInt(AppSP.KycEntrySource)== 4 || validate!!.RetriveSharepreferenceInt(AppSP.KycEntrySource)== 40 ||
            validate!!.RetriveSharepreferenceInt(AppSP.KycEntrySource)== 41 || validate!!.RetriveSharepreferenceInt(AppSP.KycEntrySource)== 42) {
            ivLock.visibility = View.VISIBLE
            btn_addgray.visibility = View.VISIBLE
            btn_kyc.visibility = View.GONE

        } else {
            ivLock.visibility = View.GONE
            btn_addgray.visibility = View.GONE
            btn_kyc.visibility = View.VISIBLE
        }
        tv_title.text =
            validate!!.RetriveSharepreferenceString(AppSP.memebrname)!! + "(" + validate!!.RetriveSharepreferenceString(
                AppSP.ShgName
            ) + ")"

        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvCader.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
        tvVector.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_cader.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        var memberlist =
            memberviewmodel!!.getmember(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
        var memberphonelist =
            memberPhoneViewmodel!!.getphoneDatalistcount(
                validate!!.RetriveSharepreferenceString(
                    AppSP.MEMBERGUID
                )
            )
        var memberaddresslist =
            memberaddressviewmodel!!.getAddressdatalistcount(
                validate!!.RetriveSharepreferenceString(
                    AppSP.MEMBERGUID
                )
            )
        var memberbanklist =
            memberbankviewmodel!!.getBankdetaildatalistcount(
                validate!!.RetriveSharepreferenceString(
                    AppSP.MEMBERGUID
                )
            )
        var membersystetmtaglist =
            membersystemviewmodel!!.getSystemtagdatalistcount(
                validate!!.RetriveSharepreferenceString(
                    AppSP.MEMBERGUID
                )
            )

        var cadreshgMemberlist = cadreMemberViewModel!!.getCadreListdata1(
            validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!)
        if (!cadreshgMemberlist.isNullOrEmpty()){
            IvCader.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }

        if (!memberlist.isNullOrEmpty()) {
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberaddresslist.isNullOrEmpty()) {
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberbanklist.isNullOrEmpty()) {
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!membersystetmtaglist.isNullOrEmpty()) {
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberphonelist.isNullOrEmpty()) {
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        ivBack.setOnClickListener {
            var intent = Intent(this, MemberIdListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

        lay_systemTag.setOnClickListener {
            var intent = Intent(this, MemberGroupTagList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_vector.setOnClickListener {
            var intent = Intent(this, MemberDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_phone.setOnClickListener {
            var intent = Intent(this, MemeberPhoneListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_location.setOnClickListener {
            var intent = Intent(this, MemberAddressListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_bank.setOnClickListener {
            var intent = Intent(this, MemberBankListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        lay_Cader.setOnClickListener {
            var intent = Intent(this, CadreListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        et_validform.setOnClickListener {
            validate!!.datePicker(et_validform)
        }

        et_validtill.setOnClickListener {
            validate!!.datePicker(et_validtill)
        }

        lay_frontattach.setOnClickListener {
            if (frontName.isNotEmpty()) {
                ShowImage(frontName)
            }
        }

        lay_rearattach.setOnClickListener {
            if (rearName.isNotEmpty()) {
                ShowImage(rearName)
            }
        }

        IvFrntUpload.setOnClickListener {
            if (!checkPermission()) {
                requestPermission(101)
            } else {
                if (validate!!.returnlookupcode(spin_kyctype, dataspin_doctype) == 1) {
                    captureimage(5000)
                } else if (validate!!.returnlookupcode(spin_kyctype, dataspin_doctype) == 2) {
                    captureimage(3000)
                } else {
                    validate!!.CustomAlert(
                        LabelSet.getText(
                            "pleaseselectid",
                            R.string.pleaseselectid
                        ), this
                    )
                }

            }

        }

        IvrearUpload.setOnClickListener {
            if (!checkPermission()) {
                requestPermission(102)
            } else {
                if (validate!!.returnlookupcode(spin_kyctype, dataspin_doctype) == 1) {
                    captureimage(6000)
                } else if (validate!!.returnlookupcode(spin_kyctype, dataspin_doctype) == 2) {
                    captureimage(4000)
                } else {
                    validate!!.CustomAlert(
                        LabelSet.getText(
                            "pleaseselectid",
                            R.string.pleaseselectid
                        ), this
                    )
                }
            }

        }

        rbkycYes.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                lay_validform.visibility = View.VISIBLE
                lay_validtill.visibility = View.VISIBLE
            } else {
                lay_validform.visibility = View.GONE
                lay_validtill.visibility = View.GONE
            }
        }

        rbkycNo.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                lay_validform.visibility = View.GONE
                lay_validtill.visibility = View.GONE
            } else {
                lay_validform.visibility = View.VISIBLE
                lay_validtill.visibility = View.VISIBLE
            }
        }

        btn_kyc.setOnClickListener {
            if (!validate!!.RetriveSharepreferenceString(AppSP.SHGGUID).isNullOrEmpty()) {
                if (checkValidation() == 1) {
                    SaveKycDetail()
                }
            } else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "add_memeber_data_first",
                        R.string.add_memeber_data_first
                    ),
                    this,
                    BasicDetailActivity::class.java
                )
            }
        }
        spin_kyctype?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position == 2) {
                    et_kycno.inputType = InputType.TYPE_CLASS_NUMBER
                    val filterArray =
                        arrayOfNulls<InputFilter>(1)
                    filterArray[0] = InputFilter.LengthFilter(12)
                    et_kycno.filters = filterArray
                    try {
                        val d: Long = et_kycno.text.toString().toLong()
                    } catch (nfe: NumberFormatException) {
                        if (!et_kycno.text.toString().contains("xxxxxxxx"))
                            et_kycno.setText("")
                    }

                } else {
                    et_kycno.inputType = InputType.TYPE_CLASS_TEXT
                    val filterArray =
                        arrayOfNulls<InputFilter>(1)
                    filterArray[0] = InputFilter.LengthFilter(50)
                    et_kycno.filters = filterArray
                }

            }

        }
        fillSpinner()
        showData()
        // setLabelText()
    }

    private fun fillSpinner() {
        dataspin_doctype = lookupViewmodel!!.getlookup(
            35,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        validate!!.fillspinner(this, spin_kyctype, dataspin_doctype)
    }

    private fun showData() {
        var list =
            memberkycViewmodel!!.getKycdetail(validate!!.RetriveSharepreferenceString(AppSP.MEMBERKYCGUID))
        if (!list.isNullOrEmpty() && list.size > 0) {
            isVerified = validate!!.returnIntegerValue(list.get(0).is_verified.toString())
            if(isVerified == 1){
                btn_kyc.text = LabelSet.getText(
                    "save_verify",
                    R.string.save_verify
                )
                lay_kyctype.setBackgroundColor(Color.parseColor("#D8FFD8"))
                lay_kycno.setBackgroundColor(Color.parseColor("#D8FFD8"))
            }else {
                btn_kyc.text = LabelSet.getText(
                    "add_kyc",
                    R.string.add_kyc
                )
            }
            spin_kyctype.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(list.get(0).kyc_type.toString()),
                    dataspin_doctype
                )
            )
            et_kycno.setText(validate!!.returnStringValue(list.get(0).kyc_number))

            frontName = validate!!.returnStringValue(list.get(0).kyc_front_doc_orig_name)
            rearName = validate!!.returnStringValue(list.get(0).kyc_rear_doc_orig_name)
            val mediaStorageDirectory = File(
                getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
                AppSP.IMAGE_DIRECTORY_NAME
            )

            if (!list.get(0).kyc_front_doc_orig_name.isNullOrEmpty()) {
                tv_uploadFiles.text = frontName
                var mediaFile1 =
                    File(mediaStorageDirectory.path + File.separator + frontName)
                Picasso.with(this).load(mediaFile1).into(ImgFrntUpload)
                //showimage(list.get(0).kyc_front_doc_orig_name!!, IvFrntUpload)
            }
            if (!list.get(0).kyc_rear_doc_orig_name.isNullOrEmpty()) {
                tvUploadFiles1.text = rearName
                var mediaFile1 =
                    File(mediaStorageDirectory.path + File.separator + rearName)
                Picasso.with(this).load(mediaFile1).into(ImgrearUpload)
                // showimage(list.get(0).kyc_rear_doc_orig_name!!, IvrearUpload)
            }
            entrysource = validate!!.returnIntegerValue(list.get(0).entry_source.toString())
            if(entrysource==4 || entrysource==40){
                entrysource = 40
            }
            et_kycno.isEnabled = !(entrysource == 42 && spin_kyctype.selectedItemPosition == 2)
            if (validate!!.returnIntegerValue(list.get(0).entry_source.toString()) == 1) {
                IsDigilocker = true
                fromDigilockerVlaue = validate!!.returnStringValue(list.get(0).kyc_number)
            }

        }
    }

    fun showimage(name: String, image: ImageView) {
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
                m.invoke(null)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
        val mediaStorageDir = File(
            getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
            AppSP.IMAGE_DIRECTORY_NAME
        )

        var mediaFile = File(mediaStorageDir.path + File.separator + name)
        imgPathUpload = mediaStorageDir.path + File.separator + name
        imgNameUpload = name

        Picasso.with(this).load(mediaFile).into(image)

    }

    private fun ShowImage(name: String?) {
        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val layout: View = inflater.inflate(R.layout.popupwindow, null, false)
        val window = PopupWindow(
            layout,
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT,
            true
        )
        window.showAtLocation(layout, Gravity.CENTER, 0, 0)
        val cancel: ImageView = layout.findViewById(R.id.cancel)
        val image_preview: ImageView = layout.findViewById(R.id.image_preview)
        val mediaStorageDirectory = File(
            getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
            AppSP.IMAGE_DIRECTORY_NAME
        )
        var mediaFile1 = File(mediaStorageDirectory.path + File.separator + name)
        cancel.setOnClickListener {
            window.dismiss()
        }

        if (mediaFile1.exists()) {
            Picasso.with(this).load(mediaFile1).into(image_preview)
        } else {
            window.dismiss()
        }

    }

    private fun SaveKycDetail() {

        var kyc_type = validate!!.returnlookupcode(spin_kyctype, dataspin_doctype)
        if (validate!!.RetriveSharepreferenceString(AppSP.MEMBERKYCGUID).isNullOrEmpty()) {
            guid = validate!!.random()
            memberKYCEntity = Member_KYC_Entity(0,
                membercode,
                validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!,
                guid,
                kyc_type,
                et_kycno.text.toString(), "",
                frontName,
                "",
                rearName,
                "",
                validate!!.Daybetweentime(validate!!.currentdatetime),
                validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                0,
                "",
                0,
                "",
                1,
                1, 0, 1, 1, "", 1
                , 1
            )
            memberkycViewmodel!!.insert(memberKYCEntity!!)
            var kycimage1 = ImageuploadEntity(
                frontName,
                validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),
                validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID),
                4,
                0
            )
            var kycimage2 = ImageuploadEntity(
                rearName,
                validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),
                validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID),
                4,
                0
            )
            if (frontName.isNotEmpty() && newimagename) {
                imageUploadViewmodel!!.insert(kycimage1)
            }
            if (rearName.isNotEmpty() && newimagename) {
                imageUploadViewmodel!!.insert(kycimage2)
            }
            validate!!.SaveSharepreferenceString(AppSP.MEMBERKYCGUID, guid)
            CDFIApplication.database?.memberDao()
                ?.updateMemberdedup(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!)
            validate!!.updateMemberEditFlag(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),shgViewmodel,memberviewmodel)

            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this,
                MemberIdListActivity::class.java
            )
        } else {
            if(checkData()==0){
                memberkycViewmodel!!.updateKycDetail(
                    validate!!.RetriveSharepreferenceString(AppSP.MEMBERKYCGUID)!!,
                    kyc_type,
                    et_kycno.text.toString(),
                    frontName,
                    "",
                    rearName,
                    "",
                    entrysource,
                    1,
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    validate!!.RetriveSharepreferenceString(AppSP.userid)!!, 1
                )
                var kycimage1 = ImageuploadEntity(
                    frontName,
                    validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),
                    validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID),
                    4,
                    0
                )
                var kycimage2 = ImageuploadEntity(
                    rearName,
                    validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),
                    validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID),
                    4,
                    0
                )
                if (frontName.isNotEmpty() && newimagename) {
                    imageUploadViewmodel!!.insert(kycimage1)
                }
                if (rearName.isNotEmpty() && newimagename) {
                    imageUploadViewmodel!!.insert(kycimage2)
                }
                CDFIApplication.database?.memberDao()
                    ?.updateMemberdedup(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!)
                validate!!.updateMemberEditFlag(
                    validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),
                    shgViewmodel,
                    memberviewmodel
                )

                validate!!.CustomAlert(
                    LabelSet.getText(
                        "updated_successfully",
                        R.string.updated_successfully
                    ),
                    this,
                    MemberIdListActivity::class.java
                )
            }else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "updated_successfully",
                        R.string.updated_successfully
                    ),
                    this,
                    MemberIdListActivity::class.java
                )
            }
        }

    }

    private fun checkValidation(): Int {
        var value = 1
        var kyc_type = validate!!.returnlookupcode(spin_kyctype, dataspin_doctype)
        val kycCount = memberkycViewmodel!!.getKycCount(
            kyc_type,
            et_kycno.text.toString(),
            validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!
        )

        if (IsDigilocker && spin_kyctype.selectedItemPosition == 2 && et_kycno.text.toString().trim().length == 12) {
            var vale1 = fromDigilockerVlaue
            var vale2 = et_kycno.text.toString().trim()
            var val4 = vale1.substring(vale1.length - 4)
            var val2 = vale2.substring(0, vale2.length - 4)
//            Log.e("new Value", val2 + val4)
            et_kycno.setText(val2 + val4)
        }

        if (spin_kyctype.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this@MemberIdActivity,
                spin_kyctype,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "kyc_id_type",
                    R.string.kyc_id_type
                )
            )
            value = 0
            return value
        } else if (et_kycno.text.toString().trim().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "kyc_id",
                    R.string.kyc_id
                ), this, et_kycno
            )
            value = 0
        } else if (spin_kyctype.selectedItemPosition == 2 && (checkAdhar() < 12 || et_kycno.text.toString().trim().length < 12)) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " valid " + LabelSet.getText(
                    "aadhaarkey",
                    R.string.aadhaarkey
                ), this, et_kycno
            )
            value = 0
        } else if (spin_kyctype.selectedItemPosition == 2 && et_kycno.text.toString().trim().length == 12 && !VerhoeffAlgorithm.validateVerhoeff(
                et_kycno.text.toString().trim()
            )
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " valid " + LabelSet.getText(
                    "aadhaarkey",
                    R.string.aadhaarkey
                ), this, et_kycno
            )
            value = 0
        } else if (frontName.isEmpty()) {
            validate!!.CustomAlert(
                LabelSet.getText("frontimage", R.string.frontimage), this
            )
            value = 0
        } else if (rearName.isEmpty()) {
            validate!!.CustomAlert(
                LabelSet.getText("rearimage", R.string.rearimage), this
            )
            value = 0
        }else if ((validate!!.RetriveSharepreferenceString(AppSP.MEMBERKYCGUID)!!.trim().length==0 && kycCount > 0) ||
            (validate!!.RetriveSharepreferenceString(AppSP.MEMBERKYCGUID)!!.trim().length>0 && kycCount > 1)) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "kyc_exist",
                    R.string.kyc_exist
                ), this,et_kycno
            )
            value = 0
            return value
        }

        return value
    }

    fun checkAdhar(): Int {

        try {
            val d: Long = et_kycno.text.toString().toLong()
        } catch (nfe: NumberFormatException) {

            et_kycno.setText("")
        }
        return et_kycno.text.length
    }

    fun captureimage(code: Int) {
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
                m.invoke(null)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//        fileUri = getOutputMediaFileUri(MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE, code)
        fileUri = FileProvider.getUriForFile(
            this,
            BuildConfig.APPLICATION_ID + ".provider",
            getOutputMediaFile(MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE, code)!!
        )
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
        startActivityForResult(intent, code)

    }

    fun getOutputMediaFileUri(type: Int, flag: Int): Uri {
        return Uri.fromFile(getOutputMediaFile(type, flag))
    }

    private fun getOutputMediaFile(type: Int, flag: Int): File? {

        // External sdcard location
        val mediaStorageDir = File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
            AppSP.IMAGE_DIRECTORY_NAME)
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null
            }
        }

        // Create a media file name
        val timeStamp = SimpleDateFormat(
            "yyyyMMdd_HHmmss",
            Locale.ENGLISH
        ).format(Date())
        var seqno = validate!!.RetriveSharepreferenceInt(AppSP.Seqno)
        var imagename = "IMG_$timeStamp,$flag$seqno.jpg"
        val mediaFile: File
        if (type == MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE) {
            mediaFile = File(
                mediaStorageDir.path + File.separator
                        + imagename
            )
            imgPathUpload = mediaStorageDir.path + File.separator + "IMG_" + timeStamp + ".jpg"
            if (flag == 3000 || flag == 5000) {
                frontName = imagename
            } else if (flag == 4000 || flag == 6000) {
                rearName = imagename
            }

        } else {
            return null
        }

        return mediaFile
    }

    override
    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            compreesedimage()
            newimagename = true
            bitmap = BitmapFactory.decodeFile(fileUri!!.path)

            if ((requestCode == 3000 || requestCode == 5000) && resultCode == Activity.RESULT_OK) {
                // rotateImage(bitmap!!,90F)

                tv_uploadFiles.text = frontName
                ImgFrntUpload.setImageBitmap(bitmap)
            } else if ((requestCode == 4000 || requestCode == 6000) && resultCode == Activity.RESULT_OK) {
                tvUploadFiles1.text = rearName
                ImgrearUpload.setImageBitmap(bitmap)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun rotateImage(source: Bitmap, angle: Float) {
        var matrix = Matrix()
        matrix.postRotate(angle)
        bitmap = Bitmap.createBitmap(
            source, 0, 0, source.width, source.height,
            matrix, true
        )
    }

    fun compreesedimage() {
        val options = BitmapFactory.Options()
        var bitmap = BitmapFactory.decodeFile(fileUri!!.path, options)

        if (bitmap != null) {
            var actualWidth = options.outWidth
            var actualHeight = options.outHeight
            var mutableBitmap =
                Bitmap.createScaledBitmap(bitmap, actualWidth / 2, actualHeight / 2, false)

            var fOut: OutputStream? = null
            var file: File = File(fileUri!!.path)
            try {
                fOut = FileOutputStream(file)
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }
            mutableBitmap.compress(Bitmap.CompressFormat.JPEG, 50, fOut)
            try {
                fOut!!.flush()
                fOut.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
    }


    private fun checkPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        val result1 =
            ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val result2 =
            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission(requestCode: Int) {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ),
            requestCode
        )
    }

    override fun onBackPressed() {
        var intent = Intent(this, MemberIdListActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    fun setLabelText() {
        tvKYCDocType.text = LabelSet.getText(
            "kyc_id_type",
            R.string.kyc_id_type
        )
        tvKYCID.text = LabelSet.getText("kyc_id", R.string.kyc_id)
        et_kycno.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tvFrontPhoto.text = LabelSet.getText(
            "frnt_attcahment",
            R.string.frnt_attcahment
        )
        tvRearPhoto.text = LabelSet.getText(
            "rear_attachment",
            R.string.rear_attachment
        )
        btn_kyc.text = LabelSet.getText("add_kyc", R.string.add_kyc)
        btn_addgray.text = LabelSet.getText("add_kyc", R.string.add_kyc)
    }

    private fun checkData(): Int {
        var value = 1

        var list =
            memberkycViewmodel!!.getKycdetail(validate!!.RetriveSharepreferenceString(AppSP.MEMBERKYCGUID))
        if (validate!!.returnStringValue(et_kycno.text.toString()) != validate!!.returnStringValue(
                list?.get(0)?.kyc_number
            )
        ) {

            value = 0

        } else if (validate!!.returnlookupcode(spin_kyctype, dataspin_doctype) != validate!!.returnIntegerValue(
                list?.get(
                    0
                )?.kyc_type.toString()
            )
        ) {

            value = 0

        }else if(frontName !=validate!!.returnStringValue(list?.get(0)?.kyc_front_doc_orig_name.toString())){
            value = 0
        }else if(rearName !=validate!!.returnStringValue(list?.get(0)?.kyc_rear_doc_orig_name.toString())){
            value = 0
        }


        return value
    }
}
