package com.microware.cdfi.activity

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.PopupWindow
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.MemberListAdapter
import com.microware.cdfi.entity.MemberEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.MemberKYCViewmodel
import com.microware.cdfi.viewModel.Memberviewmodel
import com.microware.cdfi.viewModel.SHGViewmodel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_member_list.*
import kotlinx.android.synthetic.main.activity_shg_list.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.shg_toolbar.*
import java.io.File

class MemberListActivity : AppCompatActivity() {

    var validate: Validate? = null
    var memberviewmodel: Memberviewmodel? = null
    var memberkycviewmodel: MemberKYCViewmodel? = null
    var shgViewmodel: SHGViewmodel? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_member_list)
        tv_title.text = LabelSet.getText(
            "memberlist",
            R.string.memberlist
        )

        validate = Validate(this)
        memberviewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)
        memberkycviewmodel = ViewModelProviders.of(this).get(MemberKYCViewmodel::class.java)
        shgViewmodel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        tv_groupname.text = validate!!.RetriveSharepreferenceString(AppSP.ShgName)

        icBack.setOnClickListener {
            val i = Intent(this, ShgListActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)
            finish()
        }


        tbl_add.setOnClickListener {
            validate!!.SaveSharepreferenceString(AppSP.MEMBERGUID, "")
            validate!!.SaveSharepreferenceInt(AppSP.MemberLockRecord, 0)

            val i = Intent(this, MemberDetailActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)
        }
        chk_all.setOnClickListener {
            if (chk_all.isChecked) {
                fillallData()
            } else {
                fillData()
            }
        }

        if(memberviewmodel!!.getVerificationCount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))>0){
            btn_VerifyMember.visibility = View.VISIBLE
        }else {
            btn_VerifyMember.visibility = View.GONE
        }
        btn_VerifyMember.setOnClickListener {
            var isCompleteCount = memberviewmodel!!.getCompletionCount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
            if(isCompleteCount==0){
                memberviewmodel!!.updateIsVerifed(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),validate!!.Daybetweentime(validate!!.currentdatetime),
                    validate!!.RetriveSharepreferenceString(AppSP.userid)!!)
                validate!!.CustomAlert(LabelSet.getText(
                    "",
                    R.string.verified_successfully
                ),this)
                if(memberviewmodel!!.getVerificationCount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))>0){
                    btn_VerifyMember.visibility = View.VISIBLE
                }else {
                    btn_VerifyMember.visibility = View.GONE
                }
            }else {
                validate!!.CustomAlert(LabelSet.getText(
                    "",
                    R.string.completerecord
                ),this)
            }
        }
        fillData()

    }

    private fun fillData() {
        memberviewmodel!!.getActiveMember(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
            ?.observe(this, object : Observer<List<MemberEntity>> {
                override fun onChanged(memberList: List<MemberEntity>?) {

                    if (memberList != null && memberList.isNotEmpty()) {
                        lay_nomemberavialable.visibility = View.GONE
                        if (memberList.size > 1) {
                            tbl_checkbox.visibility = View.VISIBLE
                        } else {
                            tbl_checkbox.visibility = View.GONE
                        }
                        rvMemberList.layoutManager = LinearLayoutManager(this@MemberListActivity)
                        rvMemberList.adapter =
                            MemberListAdapter(this@MemberListActivity, memberList)
                    } else {
                        tbl_checkbox.visibility = View.GONE
                        lay_nomemberavialable.visibility = View.VISIBLE
                        tv_nomemberavialable.text = LabelSet.getText(
                            "no_member_s_avialable_in_this_group",
                            R.string.no_member_s_avialable_in_this_group
                        )
                    }
                }
            })
    }

    private fun fillallData() {
        memberviewmodel!!.getAllMember(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
            ?.observe(this, object : Observer<List<MemberEntity>> {
                override fun onChanged(memberList: List<MemberEntity>?) {

                    if (memberList != null) {

                        if (memberList.size > 1) {
                            tbl_checkbox.visibility = View.VISIBLE
                        } else {
                            tbl_checkbox.visibility = View.GONE
                        }
                        rvMemberList.layoutManager = LinearLayoutManager(this@MemberListActivity)
                        rvMemberList.adapter =
                            MemberListAdapter(this@MemberListActivity, memberList)
                        if(memberList.size>0){
                            lay_nomemberavialable.visibility = View.GONE
                        } else {
                            tbl_checkbox.visibility = View.GONE
                            lay_nomemberavialable.visibility = View.VISIBLE
                        }
                    }
                }
            })
    }

    override fun onBackPressed() {
        var intent = Intent(this, ShgListActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }

    fun ShowImage(name: String?) {
        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val layout: View = inflater.inflate(R.layout.popupwindow, null, false)
        val window = PopupWindow(
            layout,
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT,
            true
        )
        window.showAtLocation(layout, Gravity.CENTER, 0, 0)
        val cancel: ImageView = layout.findViewById(R.id.cancel)
        val image_preview: ImageView = layout.findViewById(R.id.image_preview)
        val mediaStorageDirectory = File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
            AppSP.IMAGE_DIRECTORY_NAME)

        var mediaFile1 = File(mediaStorageDirectory.path + File.separator + name)
        cancel.setOnClickListener {
            window.dismiss()
        }

        Picasso.with(this).load(mediaFile1).into(image_preview)

    }

    fun returnkycstatus(memberGuid: String): Int {
        var kyclist = memberkycviewmodel!!.getKycdetaildatalistcount(memberGuid)
        var sValue = 0

        if (!kyclist.isNullOrEmpty()) {

            if (validate!!.returnIntegerValue(kyclist.get(0).entry_source.toString()) == 1)
                sValue = validate!!.returnIntegerValue(kyclist.get(0).entry_source.toString())

        }
        return sValue
    }

    fun CustomAlert(guid:String,iFlag:Int) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = LabelSet.getText(
            "do_u_want_to_delete",
            R.string.do_u_want_to_delete
        )
        mDialogView.btn_yes.setOnClickListener {
            if(iFlag==0){
                memberviewmodel!!.deleteMemberByMemberGuid(guid)
                memberviewmodel!!.deleteMemberAddressByMemberGuid(guid)
                memberviewmodel!!.deleteMemberPhoneByMemberGuid(guid)
                memberviewmodel!!.deleteMemberBankByMemberGuid(guid)
                memberviewmodel!!.deleteMemberKycByMemberGuid(guid)
                memberviewmodel!!.deleteMemberSystemTagByMemberGuid(guid)
            }else {
                memberviewmodel!!.deleteMemberDataByMemberGuid(guid)
                memberviewmodel!!.deleteMemberAddressDataByMemberGuid(guid)
                memberviewmodel!!.deleteMemberPhoneDataByMemberGuid(guid)
                memberviewmodel!!.deleteMemberBankDataByMemberGuid(guid)
                memberviewmodel!!.deleteMemberKycDataByMemberGuid(guid)
                memberviewmodel!!.deleteMemberSystemTagDataByMemberGuid(guid)

            }
            validate!!.updateMemberEditFlag(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),shgViewmodel,memberviewmodel)

            fillallData()
            mAlertDialog.dismiss()


        }

        mDialogView.btn_no.setOnClickListener {

            mAlertDialog.dismiss()


        }
    }
}
