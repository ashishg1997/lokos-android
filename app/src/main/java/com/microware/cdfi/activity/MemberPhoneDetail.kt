package com.microware.cdfi.activity

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.entity.MemberPhoneDetailEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_member_phone_detail.*
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.tablayout.*
import kotlinx.android.synthetic.main.white_toolbar.*
import kotlinx.android.synthetic.main.white_toolbar.tv_title

class MemberPhoneDetail : AppCompatActivity() {

    var validate: Validate? = null
    var phoneDetailEntity: MemberPhoneDetailEntity? = null
    var phoneViewmodel: MemberPhoneViewmodel? = null
    var membercode = 0L
    var phoneGUID= ""
    var membername = ""
    var isVerified = 0
    var lookupViewmodel: LookupViewmodel? = null
    var dataspin_memberbelong: List<LookupEntity>? = null
    var dataspin_status: List<LookupEntity>? = null
    var memberviewmodel: Memberviewmodel? = null
    var memberaddressviewmodel: MemberAddressViewmodel? = null
    var memberkycviewmodel: MemberKYCViewmodel? = null
    var memberbankviewmodel: MemberBankViewmodel? = null
    var membersystemviewmodel: MemberSystemtagViewmodel? = null
    var shgViewmodel: SHGViewmodel? = null
    var cadreMemberViewModel: CadreMemberViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_member_phone_detail)
        validate = Validate(this)
        setLabelText()

        phoneViewmodel = ViewModelProviders.of(this).get(MemberPhoneViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        memberviewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)
        memberaddressviewmodel = ViewModelProviders.of(this).get(MemberAddressViewmodel::class.java)
        memberkycviewmodel = ViewModelProviders.of(this).get(MemberKYCViewmodel::class.java)
        memberbankviewmodel = ViewModelProviders.of(this).get(MemberBankViewmodel::class.java)
        membersystemviewmodel = ViewModelProviders.of(this).get(MemberSystemtagViewmodel::class.java)
        shgViewmodel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        cadreMemberViewModel = ViewModelProviders.of(this).get(CadreMemberViewModel::class.java)

        membercode = validate!!.RetriveSharepreferenceLong(AppSP.membercode)
        membername = validate!!.RetriveSharepreferenceString(AppSP.memebrname)!!
        tvCode.text = membercode.toString()
        et_membercode.setText(membername)

        ivHome.visibility = View.GONE
        if (validate!!.RetriveSharepreferenceInt(AppSP.MemberLockRecord) == 1) {
            ivLock.visibility = View.VISIBLE
            btn_addgray.visibility = View.VISIBLE
            btn_add.visibility = View.GONE

        } else {
            ivLock.visibility = View.GONE
            btn_addgray.visibility = View.GONE
            btn_add.visibility = View.VISIBLE
        }
        tv_title.text =
            validate!!.RetriveSharepreferenceString(AppSP.memebrname)!! + "(" + validate!!.RetriveSharepreferenceString(AppSP.ShgName) + ")"
        ivBack.setOnClickListener {
            var intent = Intent(this,MemeberPhoneListActivity::class.java)
            intent.flags=Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvCader.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvVector.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimary))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_cader.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        var memberlist =
            memberviewmodel!!.getmember(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
        var memberaddresslist =
            memberaddressviewmodel!!.getAddressdatalistcount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
        var memberbanklist =
            memberbankviewmodel!!.getBankdetaildatalistcount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
        var memberkyclist =
            memberkycviewmodel!!.getKycdetaildatalistcount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
        var membersystetmtaglist =
            membersystemviewmodel!!.getSystemtagdatalistcount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
        var cadreshgMemberlist = cadreMemberViewModel!!.getCadreListdata1(
            validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!)
        if (!cadreshgMemberlist.isNullOrEmpty()){
            IvCader.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberlist.isNullOrEmpty()){
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberbanklist.isNullOrEmpty()){
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberkyclist.isNullOrEmpty()){
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!membersystetmtaglist.isNullOrEmpty()){
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberaddresslist.isNullOrEmpty()){
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        lay_systemTag.setOnClickListener {
            var intent = Intent(this, MemberGroupTagList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_vector.setOnClickListener {
            var intent = Intent(this,MemberDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_location.setOnClickListener {
            var intent = Intent(this,MemberAddressListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_bank.setOnClickListener {
            var intent = Intent(this,MemberBankListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_kyc.setOnClickListener {
            var intent = Intent(this,MemberIdListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }
        lay_Cader.setOnClickListener {
            var intent = Intent(this, CadreListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        et_validform.setOnClickListener {
            validate!!.datePicker(et_validform)
        }

        et_validtill.setOnClickListener {
            validate!!.datePicker(et_validtill)
        }

        btn_add.setOnClickListener {
            if (!validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID).isNullOrEmpty()){
                if(checkValidation() == 1){
                    SavePhoneDetail()
                }
            }else{
                validate!!.CustomAlert(LabelSet.getText(
                    "add_memeber_data_first",
                    R.string.add_memeber_data_first
                ),this,MemberDetailActivity::class.java)
            }

        }

        fillSpinner()
        showData()
       // setLabelText()
    }

    private fun fillSpinner() {
        dataspin_memberbelong = lookupViewmodel!!.getlookupfromkeycode("RELATION_MB",validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
        dataspin_status = lookupViewmodel!!.getlookupfromkeycode("RELATION_MB",validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))

        validate!!.fillspinner(this,spin_ownership,dataspin_memberbelong)
        validate!!.fillspinner(this,spin_status,dataspin_status)

    }

    private fun showData() {
        var list = phoneViewmodel!!.getphonedetaildata(validate!!.RetriveSharepreferenceString(AppSP.MemberPhoneGUID))
        if (!list.isNullOrEmpty() && list.size > 0){
            isVerified = validate!!.returnIntegerValue(list.get(0).is_verified.toString())
            if(isVerified == 1){
                lay_Mobile.setBackgroundColor(Color.parseColor("#D8FFD8"))
                lay_Composition.setBackgroundColor(Color.parseColor("#D8FFD8"))
                btn_add.text = LabelSet.getText(
                    "save_verify",
                    R.string.save_verify
                )
            }else {
                btn_add.text = LabelSet.getText(
                    "add_phone",
                    R.string.add_phone
                )
            }
            spin_ownership.setSelection(validate!!.returnlookupcodepos(validate!!.returnIntegerValue(list.get(0).phone_ownership),dataspin_memberbelong))
            et_phoneno.setText(validate!!.returnStringValue(list.get(0).phone_no))
            spin_status.setSelection(0)
            if(validate!!.returnLongValue(list.get(0).valid_from.toString())>0) {
                et_validform.setText(validate!!.convertDatetime(list.get(0).valid_from))
            }
            if(validate!!.returnLongValue(list.get(0).valid_till.toString())>0) {
                et_validtill.setText(validate!!.convertDatetime(list.get(0).valid_till))
            }
        }
    }

    private fun SavePhoneDetail() {
        phoneGUID = validate!!.random()

        if (validate!!.RetriveSharepreferenceString(AppSP.MemberPhoneGUID).isNullOrEmpty()) {
            phoneDetailEntity = MemberPhoneDetailEntity(
                0,
                0,
                membercode,
                validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!,
                phoneGUID,
                et_phoneno.text.toString(),
                1,
                validate!!.returnlookupcode(spin_ownership, dataspin_memberbelong).toString(),
                "",
                validate!!.Daybetweentime(validate!!.currentdatetime),
                0,
                1,
                1,
                validate!!.Daybetweentime(validate!!.currentdatetime),
                validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                0,
                "",
                0,
                "", 1, 0, 1, 1, 1, "", 1
            )
            phoneViewmodel!!.insert(phoneDetailEntity!!)
            validate!!.SaveSharepreferenceString(AppSP.MemberPhoneGUID, phoneGUID)
            CDFIApplication.database?.memberDao()
                ?.updateMemberdedup(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!)
            validate!!.updateMemberEditFlag(
                validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),
                shgViewmodel,
                memberviewmodel
            )
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this,
                MemeberPhoneListActivity::class.java
            )
        } else {
            if(checkData()==0){
            phoneViewmodel!!.updatePhoneDetail(
                validate!!.RetriveSharepreferenceString(AppSP.MemberPhoneGUID)!!,
                et_phoneno.text.toString(),
                1,
                validate!!.returnlookupcode(spin_ownership, dataspin_memberbelong).toString(),
                "",
                validate!!.Daybetweentime(validate!!.currentdatetime),
                0,
                1,
                1,
                validate!!.Daybetweentime(validate!!.currentdatetime),
                validate!!.RetriveSharepreferenceString(AppSP.userid)!!, 1
            )
            CDFIApplication.database?.memberDao()
                ?.updateMemberdedup(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!)
            validate!!.updateMemberEditFlag(
                validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),
                shgViewmodel,
                memberviewmodel
            )
            validate!!.CustomAlert(
                LabelSet.getText(
                    "updated_successfully",
                    R.string.updated_successfully
                ),
                this,
                MemeberPhoneListActivity::class.java
            )
        }else {
            validate!!.CustomAlert(
                LabelSet.getText(
                    "updated_successfully",
                    R.string.updated_successfully
                ),
                this,
                MemeberPhoneListActivity::class.java
            )
        }
    }

    }


    private fun checkValidation():Int{
        var value = 1
        val phoneCount = phoneViewmodel!!.getPhoneCount(et_phoneno.text.toString(),
            validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!)
        if (validate!!.checkmobileno(et_phoneno.text.toString()) == 0){
            validate!!.CustomAlertEditText(LabelSet.getText(
                "please_enter",
                R.string.please_enter
            )+" "+LabelSet.getText(
                "valid_phone",
                R.string.valid_phone
            ), this, et_phoneno)
            value = 0
            return value
        }else if (spin_ownership.selectedItemPosition == 0){
            validate!!.CustomAlertSpinner(
                this@MemberPhoneDetail,spin_ownership,LabelSet.getText(
                    "please_select",
                    R.string.please_select
                )+" "+ LabelSet.getText(
                    "belongs_to_memberrelation",
                    R.string.belongs_to_memberrelation
                ))
            value = 0
            return value
        }else if ((validate!!.RetriveSharepreferenceString(AppSP.MemberPhoneGUID)!!.trim().length==0 && phoneCount > 0) ||
            (validate!!.RetriveSharepreferenceString(AppSP.MemberPhoneGUID)!!.trim().length>0 && phoneCount > 1)){
            validate!!.CustomAlertEditText(LabelSet.getText(
                "phone_no_exists",
                R.string.phone_no_exists
            ),this,et_phoneno)
            value = 0
            return value
        }



        return value
    }
    override fun onBackPressed() {
        var intent = Intent(this,MemeberPhoneListActivity::class.java)
        intent.flags=Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    fun setLabelText()
    {
        tvMobNO.text = LabelSet.getText(
            "phone_no",
            R.string.phone_no
        )
        tvMemberBelongs.text = LabelSet.getText(
            "belongs_to_memberrelation",
            R.string.belongs_to_memberrelation
        )
        et_phoneno.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        LabelSet.getText("add_phone", R.string.add_phone)
        btn_addgray.text = LabelSet.getText("add_phone", R.string.add_phone)
    }

    private fun checkData(): Int {
        var value = 1

            var list =
                phoneViewmodel!!.getphonedetaildata(validate!!.RetriveSharepreferenceString(AppSP.MemberPhoneGUID))
                if (validate!!.returnStringValue(et_phoneno.text.toString()) != validate!!.returnStringValue(
                        list?.get(0)?.phone_no
                    )
                ) {

                    value = 0

                } else if (validate!!.returnlookupcode(spin_ownership, dataspin_memberbelong) != validate!!.returnIntegerValue(
                        list?.get(0)?.phone_ownership)) {

                    value = 0
                } else if (validate!!.Daybetweentime(et_validform.text.toString()) != validate!!.returnLongValue(
                        list?.get(0)?.valid_from.toString()
                    )
                ) {

                    value = 0
                } else if (validate!!.Daybetweentime(et_validtill.text.toString()) != validate!!.returnLongValue(
                        list?.get(0)?.valid_till.toString()
                    )
                ) {

                    value = 0

                }


        return value
    }
}
