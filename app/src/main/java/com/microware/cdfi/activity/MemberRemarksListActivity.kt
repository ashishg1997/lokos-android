package com.microware.cdfi.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.MemberRejectionRemarksAdapter
import com.microware.cdfi.entity.MemberEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.layout_rjection_remarks_list.*
import kotlinx.android.synthetic.main.white_toolbar.*

class MemberRemarksListActivity : AppCompatActivity() {

    var memberViewmodel: Memberviewmodel? = null
    var validate: Validate? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_rjection_remarks_list)

        memberViewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)

        validate = Validate(this)

        ivHome.visibility = View.GONE
        tv_title.text = LabelSet.getText(
            "member_rejection_remarks",
            R.string.member_rejection_remarks
        )

        ivBack.setOnClickListener {
            var intent = Intent(this, MemberListActivity::class.java)
            intent.flags= Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }


        fillData()
    }

    override fun onBackPressed() {
        var intent = Intent(this, MemberListActivity::class.java)
        intent.flags= Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    private fun fillData() {
        memberViewmodel!!.getMemberlistdata(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))!!
            .observe(this,object : Observer<List<MemberEntity>> {
                override fun onChanged(memberlist: List<MemberEntity>?) {
                    if (!memberlist.isNullOrEmpty() && memberlist.size > 0 ){
                        rvList.layoutManager = LinearLayoutManager(this@MemberRemarksListActivity)
                        rvList.adapter = MemberRejectionRemarksAdapter(this@MemberRemarksListActivity, memberlist,memberViewmodel!!)
                    }
                }
            })
    }

}