package com.microware.cdfi.activity

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.MemberPhoneAdapter
import com.microware.cdfi.entity.MemberPhoneDetailEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_memeber_phone_list.*
import kotlinx.android.synthetic.main.activity_memeber_phone_list.rvList
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.tablayout.*
import kotlinx.android.synthetic.main.white_toolbar.*

class MemeberPhoneListActivity : AppCompatActivity() {

    var validate: Validate? = null
    var phoneDetailEntity: MemberPhoneDetailEntity? = null
    var phoneViewmodel: MemberPhoneViewmodel? = null
    var lookupViewmodel: LookupViewmodel? = null
    var memberviewmodel: Memberviewmodel? = null
    var memberaddressviewmodel: MemberAddressViewmodel? = null
    var memberkycviewmodel: MemberKYCViewmodel? = null
    var memberbankviewmodel: MemberBankViewmodel? = null
    var membersystemviewmodel: MemberSystemtagViewmodel? = null
    var memberDesignationViewmodel: MemberDesignationViewmodel? = null
    var cadreMemberViewModel: CadreMemberViewModel? = null
    var shgViewmodel: SHGViewmodel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_memeber_phone_list)

        validate = Validate(this)
        phoneViewmodel = ViewModelProviders.of(this).get(MemberPhoneViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        memberviewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)
        memberaddressviewmodel = ViewModelProviders.of(this).get(MemberAddressViewmodel::class.java)
        memberkycviewmodel = ViewModelProviders.of(this).get(MemberKYCViewmodel::class.java)
        memberbankviewmodel = ViewModelProviders.of(this).get(MemberBankViewmodel::class.java)
        shgViewmodel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        cadreMemberViewModel = ViewModelProviders.of(this).get(CadreMemberViewModel::class.java)
        membersystemviewmodel =
            ViewModelProviders.of(this).get(MemberSystemtagViewmodel::class.java)
        memberDesignationViewmodel =
            ViewModelProviders.of(this).get(MemberDesignationViewmodel::class.java)

        ivHome.visibility = View.GONE
        if (validate!!.RetriveSharepreferenceInt(AppSP.MemberLockRecord) == 1) {
            ivLock.visibility = View.VISIBLE
            addphonegray.visibility = View.VISIBLE
            addphone.visibility = View.GONE

        } else {
            ivLock.visibility = View.GONE
            addphonegray.visibility = View.GONE
            addphone.visibility = View.VISIBLE
        }
        tv_title.text =
            validate!!.RetriveSharepreferenceString(AppSP.memebrname)!! + "(" + validate!!.RetriveSharepreferenceString(
                AppSP.ShgName
            ) + ")"
        ivBack.setOnClickListener {
            var intent = Intent(this, MemberListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvCader.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvVector.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_cader.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        var memberlist =
            memberviewmodel!!.getmember(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
        var memberaddresslist =
            memberaddressviewmodel!!.getAddressdatalistcount(
                validate!!.RetriveSharepreferenceString(
                    AppSP.MEMBERGUID
                )
            )
        var memberbanklist =
            memberbankviewmodel!!.getBankdetaildatalistcount(
                validate!!.RetriveSharepreferenceString(
                    AppSP.MEMBERGUID
                )
            )
        var memberkyclist =
            memberkycviewmodel!!.getKycdetaildatalistcount(
                validate!!.RetriveSharepreferenceString(
                    AppSP.MEMBERGUID
                )
            )
        var membersystetmtaglist =
            membersystemviewmodel!!.getSystemtagdatalistcount(
                validate!!.RetriveSharepreferenceString(
                    AppSP.MEMBERGUID
                )
            )
        var cadreshgMemberlist = cadreMemberViewModel!!.getCadreListdata1(
            validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID)!!)
        if (!cadreshgMemberlist.isNullOrEmpty()){
            IvCader.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }

        if (!memberlist.isNullOrEmpty()) {
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberbanklist.isNullOrEmpty()) {
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberkyclist.isNullOrEmpty()) {
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!membersystetmtaglist.isNullOrEmpty()) {
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        if (!memberaddresslist.isNullOrEmpty()) {
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }
        lay_systemTag.setOnClickListener {
            var intent = Intent(this, MemberGroupTagList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_vector.setOnClickListener {
            var intent = Intent(this, MemberDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_location.setOnClickListener {
            var intent = Intent(this, MemberAddressListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_bank.setOnClickListener {
            var intent = Intent(this, MemberBankListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_kyc.setOnClickListener {
            var intent = Intent(this, MemberIdListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        lay_Cader.setOnClickListener {
            var intent = Intent(this, CadreListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        addphone.setOnClickListener {
            if (!validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID).isNullOrEmpty()) {
                validate!!.SaveSharepreferenceString(AppSP.MemberPhoneGUID, "")
                var intnt = Intent(this, MemberPhoneDetail::class.java)
                startActivity(intnt)
                finish()
            } else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "add_memeber_data_first",
                        R.string.add_memeber_data_first
                    ), this, MemberDetailActivity::class.java
                )
            }
        }


        if(phoneViewmodel!!.getVerificationCount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))>0){
            btn_VerifyPhone.visibility = View.VISIBLE
        }else {
            btn_VerifyPhone.visibility = View.GONE
        }
        btn_VerifyPhone.setOnClickListener {
            var isCompleteCount = phoneViewmodel!!.getCompletionCount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
            if(isCompleteCount==0){
                phoneViewmodel!!.updateIsVerifed(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID),validate!!.Daybetweentime(validate!!.currentdatetime),
                    validate!!.RetriveSharepreferenceString(AppSP.userid)!!)
                validate!!.CustomAlert(LabelSet.getText(
                    "",
                    R.string.verified_successfully
                ),this)
                if(phoneViewmodel!!.getVerificationCount(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))>0){
                    btn_VerifyPhone.visibility = View.VISIBLE
                }else {
                    btn_VerifyPhone.visibility = View.GONE
                }
            }else {
                validate!!.CustomAlert(LabelSet.getText(
                    "",
                    R.string.completerecord
                ),this)
            }
        }
        fillData()

    }

    fun getissigntorydata(guid: String): Int {
        return memberDesignationViewmodel!!.getsignatoryMembercount(guid)
    }

    private fun fillData() {
        phoneViewmodel!!.getphoneData(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))!!
            .observe(this, object : Observer<List<MemberPhoneDetailEntity>> {
                override fun onChanged(member_list: List<MemberPhoneDetailEntity>?) {
                    if (member_list != null) {
                        rvList.layoutManager = LinearLayoutManager(this@MemeberPhoneListActivity)
                        rvList.adapter =
                            MemberPhoneAdapter(this@MemeberPhoneListActivity, member_list)
                        if(member_list.size>0){
                            lay_nophone_avialable.visibility=View.GONE
                        }else {
                            lay_nophone_avialable.visibility=View.VISIBLE
                            tv_nophone_avialable.text = LabelSet.getText(
                                "no_phone_s_avialable",
                                R.string.no_phone_s_avialable
                            )
                        }
                    }
                }
            })
    }

    override fun onBackPressed() {
        var intent = Intent(this, MemberListActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    fun getLookupValue(id: Int): String {
        var dataspin_memberbelong = lookupViewmodel!!.getlookupfromkeycode(
            "RELATION_MB",
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        var sValue = ""
        sValue = validate!!.returnlookupcodevalue(id, dataspin_memberbelong)
        return sValue
    }

    fun CustomAlert(guid: String, iFlag: Int) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text =
            LabelSet.getText(
                "do_u_want_to_delete",
                R.string.do_u_want_to_delete
            )
        mDialogView.txt_dialog_title.text = LabelSet.getText("app_name", R.string.app_name)
        mDialogView.btn_yes.text = LabelSet.getText("yes", R.string.yes)
        mDialogView.btn_no.text = LabelSet.getText("no", R.string.no)
        mDialogView.btn_yes.setOnClickListener {

            if (iFlag == 0) {
                phoneViewmodel!!.deleteRecord(guid)
            } else {
                phoneViewmodel!!.deleteData(guid)

            }

            validate!!.updateMemberEditFlag(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),shgViewmodel,memberviewmodel)

            mAlertDialog.dismiss()

        }
        mDialogView.btn_no.setOnClickListener {

            mAlertDialog.dismiss()

        }
    }
}