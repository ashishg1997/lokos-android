package com.microware.cdfi.activity

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.Cbo_phoneEntity
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.entity.MemberEntity
import com.microware.cdfi.entity.MemberPhoneDetailEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.fragment_phone_detail.*
import kotlinx.android.synthetic.main.tablayoutgroup.*
import kotlinx.android.synthetic.main.white_toolbar.ivBack
import kotlinx.android.synthetic.main.white_toolbar.ivHome
import kotlinx.android.synthetic.main.white_toolbar.tv_title
import kotlinx.android.synthetic.main.whiteicon_toolbar.*

class PhoneDetailActivity : AppCompatActivity() {

    var validate: Validate? = null
    var cbophoneDetailEntity: Cbo_phoneEntity? = null
    var cboPhoneViewmodel: CboPhoneViewmodel? = null
    var shgcode = ""
    var shgName = ""
    var phoneGUID= " "
    var lookupViewmodel: LookupViewmodel? = null
    var memberviewmodel: Memberviewmodel? = null
    var memberPhoneViewmodel: MemberPhoneViewmodel? = null
    var dataspin_name: List<LookupEntity>? = null
    var dataspin_belong: List<MemberEntity>? = null
    var dataspin_belongmemberphone: List<MemberPhoneDetailEntity>? = null
    var phone_num = ""
    var shgViewmodel: SHGViewmodel? = null
    var addressViewmodel: CboAddressViewmodel? = null
    var phoneViewmodel: CboPhoneViewmodel? = null
    var bank: CboBankViewmodel? = null
    var systemtagViewmodel: SystemtagViewmodel? = null
    var memberDesignationViewmodel: MemberDesignationViewmodel? = null
    var isVerified = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_phone_detail)
        validate = Validate(this)
        setLabelText()

        memberviewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)
        memberPhoneViewmodel = ViewModelProviders.of(this).get(MemberPhoneViewmodel::class.java)
        cboPhoneViewmodel = ViewModelProviders.of(this).get(CboPhoneViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        shgViewmodel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        addressViewmodel = ViewModelProviders.of(this).get(CboAddressViewmodel::class.java)
        phoneViewmodel = ViewModelProviders.of(this).get(CboPhoneViewmodel::class.java)
        bank = ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        systemtagViewmodel = ViewModelProviders.of(this).get(SystemtagViewmodel::class.java)
        memberDesignationViewmodel = ViewModelProviders.of(this).get(MemberDesignationViewmodel::class.java)
        shgcode = validate!!.RetriveSharepreferenceString(AppSP.Shgcode)!!
        shgName = validate!!.RetriveSharepreferenceString(AppSP.ShgName)!!
        tvCode.text = shgcode.toString()
        et_shgcode.setText(shgName)

        ivBack.setOnClickListener {
            var intent = Intent(this,PhoneDetailListActivity::class.java)
            intent.flags=Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

        ivHome.setOnClickListener {
            var intent = Intent(this,ShgListActivity::class.java)
            intent.flags=Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

//        IvSearch.visibility = View.GONE
        if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) == 1) {
            ivLock.visibility = View.VISIBLE
            btn_addgray.visibility = View.VISIBLE
            btn_add.visibility = View.GONE

        } else {
            ivLock.visibility = View.GONE
            btn_addgray.visibility = View.GONE
            btn_add.visibility = View.VISIBLE
        }

        tv_title.text =
            validate!!.RetriveSharepreferenceString(AppSP.ShgName)!! + "(" + validate!!.RetriveSharepreferenceString(AppSP.Shgcode) + ")"
        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvVector.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimary))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this,R.color.white))

        var shglist=shgViewmodel!!.getSHG(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var banklist=bank!!.getBankdatalistcount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var addresslist=addressViewmodel!!.getAddressDatalistcount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var systemlist=systemtagViewmodel!!.getSystemtagdatalistcount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var memberdesignationcount = memberDesignationViewmodel!!.getMemberDesignationdetaillistcount(
            validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)
        )
        if(!shglist.isNullOrEmpty()){
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))


        }else{
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))


        }
        if(!banklist.isNullOrEmpty()){
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        }else{
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        if(!addresslist.isNullOrEmpty()){
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        }else{
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        if(!systemlist.isNullOrEmpty()){
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        }else{
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        if(!memberdesignationcount.isNullOrEmpty()){
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        }else{
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        lay_systemTag.setOnClickListener {
            var intent = Intent(this, GroupSystemTagList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_vector.setOnClickListener {
            var intent = Intent(this,BasicDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_location.setOnClickListener {
            var intent = Intent(this,AddressDetailListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_bank.setOnClickListener {
            var intent = Intent(this,BankDetailListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_post.setOnClickListener {
            var intent = Intent(this,MemberDesignationActvity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        et_validform.setOnClickListener {
            validate!!.datePicker(et_validform)
        }

        et_validtill.setOnClickListener {
            validate!!.datePicker(et_validtill)
        }

        btn_add.setOnClickListener {
            if (!validate!!.RetriveSharepreferenceString(AppSP.SHGGUID).isNullOrEmpty()){
                if(checkValidation() == 1){
                    SavePhoneDetail()
                }
            }else{
                validate!!.CustomAlert(LabelSet.getText(
                    "insert_Basic_data_first",
                    R.string.insert_Basic_data_first
                ),this,BasicDetailActivity::class.java)
            }

        }
        spin_belongMember?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {
                    dataspin_belongmemberphone = memberPhoneViewmodel!!.getphonedetaillistdata(returnmemguid())
                    fillmemberphoneno(spin_Memberphone, dataspin_belongmemberphone)
                    spin_Memberphone.setSelection(setPhone(phone_num,dataspin_belongmemberphone))
                }else{
                    dataspin_belongmemberphone = memberPhoneViewmodel!!.getphonedetaillistdata("")
                    fillmemberphoneno(spin_Memberphone, dataspin_belongmemberphone)

                }

            }override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }
        fillSpinner()
        showData()
        //  setLabelText()
    }
    fun returnmemguid(): String {

        var pos = spin_belongMember.selectedItemPosition
        var id = ""

        if (!dataspin_belong.isNullOrEmpty()) {
            if (pos > 0) id = dataspin_belong!!.get(pos - 1).member_guid
        }
        return id
    }
    fun fillmemberspinner( spin: Spinner, data: List<MemberEntity>?) {

        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in data.indices) {
                sValue[i + 1] = data[i].member_name+"("+getlookupValue(data[i].designation,1)+")"
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText(
                "nomemberavailable",
                R.string.nomemberavailable
            )
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }

    }
    fun returnPhone(): String {

        var pos = spin_Memberphone.selectedItemPosition
        var id = ""

        if (!dataspin_belongmemberphone.isNullOrEmpty()) {
            if (pos > 0) id = dataspin_belongmemberphone!!.get(pos-1).phone_no!!
        }
        return id
    }

    fun setMember(memberGuid:String,data: List<MemberEntity>?): Int {


        var pos = 0

        if (!data.isNullOrEmpty()) {
            for (i in data.indices) {
                if (memberGuid.equals(data.get(i).member_guid))
                    pos = i + 1
            }
        }
        return pos
    }

    fun setPhone(phone:String,data: List<MemberPhoneDetailEntity>?): Int {

        var pos = 0

        if (!data.isNullOrEmpty()) {
            for (i in data.indices) {
                if (phone.equals(data.get(i).phone_no))
                    pos = i + 1
            }
        }
        return pos
    }
    private fun fillSpinner() {
        dataspin_belong = memberviewmodel!!.getAllMemberDeslist(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        fillmemberspinner(spin_belongMember,dataspin_belong)
        dataspin_name = lookupViewmodel!!.getlookup(1,validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
        validate!!.fillspinner(this,spin_status,dataspin_name)
    }
    fun getlookupValue(keyCode: Int?,flag:Int): String? {
        var name: String? = null
        name = lookupViewmodel!!.getlookupValue(
            flag,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),
            keyCode
        )
        return name
    }
    fun fillmemberphoneno(spin: Spinner, data: List<MemberPhoneDetailEntity>?) {

        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize+1 )
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in data.indices) {
                sValue[i+1] = data[i].phone_no
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue)
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }

    }
    private fun showData() {
        var list = cboPhoneViewmodel!!.getphonedetaildata(validate!!.RetriveSharepreferenceString(AppSP.PhoneGUID))
        if (!list.isNullOrEmpty() && list.size > 0){
            isVerified = validate!!.returnIntegerValue(list.get(0).is_verified.toString())
            if(isVerified == 1){
                btn_add.text = LabelSet.getText(
                    "save_verify",
                    R.string.save_verify
                )
                lay_mobile.setBackgroundColor(Color.parseColor("#D8FFD8"))
                lay_belongs.setBackgroundColor(Color.parseColor("#D8FFD8"))
            }else {
                btn_add.text = LabelSet.getText(
                    "add_phone",
                    R.string.add_phone
                )
            }
            spin_belongMember.setSelection(setMember(validate!!.returnStringValue(list.get(0).member_guid),dataspin_belong))
            phone_num = validate!!.returnStringValue(list.get(0).mobile_no)


        }

    }

    private fun SavePhoneDetail() {

        phoneGUID = validate!!.random()
        var phoneno = spin_Memberphone.selectedItem.toString()
        if (isVerified == 1) {
            isVerified = 9
        } else {
            isVerified = 0
        }

        if (validate!!.RetriveSharepreferenceString(AppSP.PhoneGUID).isNullOrEmpty()) {
            cbophoneDetailEntity = Cbo_phoneEntity(
                0,
                validate!!.returnLongValue(shgcode),
                validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!,
                phoneGUID,
                returnmemguid(),
                returnPhone(),
                0,
                0,
                "",
                validate!!.Daybetweentime(validate!!.currentdatetime),
                0,
                1,
                1,
                1,
                1,
                validate!!.Daybetweentime(validate!!.currentdatetime),
                validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                0,
                "",
                0,
                "", 1, 1, 1
            )
            cboPhoneViewmodel!!.insert(cbophoneDetailEntity!!)
            validate!!.SaveSharepreferenceString(AppSP.PhoneGUID, phoneGUID)
            CDFIApplication.database?.shgDao()
                ?.updatededup(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!)

            validate!!.updateLockingFlag(
                validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!,
                shgViewmodel, memberviewmodel
            )
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ), this, PhoneDetailListActivity::class.java
            )
        } else {
            if(checkData()==0){
            cboPhoneViewmodel!!.updatePhoneDetail(
                validate!!.RetriveSharepreferenceString(AppSP.PhoneGUID)!!,
                returnmemguid(),
                returnPhone(),
                1, "", "",
                validate!!.Daybetweentime(validate!!.currentdatetime),
                0,
                1,
                validate!!.Daybetweentime(validate!!.currentdatetime),
                validate!!.RetriveSharepreferenceString(AppSP.userid)!!, 1
            )
            CDFIApplication.database?.shgDao()
                ?.updatededup(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!)
            validate!!.updateLockingFlag(
                validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!,
                shgViewmodel, memberviewmodel
            )
            validate!!.CustomAlert(
                LabelSet.getText(
                    "updated_successfully",
                    R.string.updated_successfully
                ),
                this,
                PhoneDetailListActivity::class.java
            )
        }else {
            validate!!.CustomAlert(
                LabelSet.getText(
                    "updated_successfully",
                    R.string.updated_successfully
                ),
                this,
                PhoneDetailListActivity::class.java
            )
        }
    }
    }

    fun checkValidation():Int{
        var value = 1
        val phonecount = phoneViewmodel!!.getphone_count(returnPhone(),0)
        if (spin_belongMember.selectedItemPosition == 0){
            validate!!.CustomAlertSpinner(this,spin_belongMember,LabelSet.getText(
                "please_select",
                R.string.please_select
            )+" "+ LabelSet.getText(
                "belongs_to_member",
                R.string.belongs_to_member
            ))
            value = 0
            return value
        }
        if (spin_Memberphone.selectedItemPosition == 0){
            validate!!.CustomAlertSpinner(this,spin_Memberphone,LabelSet.getText(
                "please_select",
                R.string.please_select
            )+" "+ LabelSet.getText("phone_no", R.string.phone_no))
            value = 0
            return value
        }else if ((validate!!.RetriveSharepreferenceString(AppSP.PhoneGUID)!!.trim().length==0 && phonecount > 0) ||
            (validate!!.RetriveSharepreferenceString(AppSP.PhoneGUID)!!.trim().length>0 && phonecount > 1)){
            validate!!.CustomAlertSpinner(this,spin_Memberphone,LabelSet.getText(
                "phone_no_exists",
                R.string.phone_no_exists
            ))
            value = 0
            return value
        }


        return value

    }

    override fun onBackPressed() {
        var intent = Intent(this,PhoneDetailListActivity::class.java)
        intent.flags=Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }

    fun setLabelText()
    {
        tv_belongsto.text = LabelSet.getText(
            "belongs_to_member",
            R.string.belongs_to_member
        )
        tv_mob.text = LabelSet.getText(
            "phone_no",
            R.string.phone_no
        )
        btn_add.text = LabelSet.getText(
            "add_phone",
            R.string.add_phone
        )
        btn_addgray.text = LabelSet.getText(
            "add_phone",
            R.string.add_phone
        )
    }

    private fun checkData(): Int {
        var value = 1
            var list = cboPhoneViewmodel!!.getphonedetaildata(validate!!.RetriveSharepreferenceString(AppSP.PhoneGUID))
                if (returnmemguid() != validate!!.returnStringValue(list?.get(0)?.member_guid)) {

                    value = 0

                }else if (returnPhone() !=validate!!.returnStringValue(list?.get(0)?.mobile_no)) {

                    value = 0

                }


        return value
    }
}
