package com.microware.cdfi.activity

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.CboPhoneAdapter
import com.microware.cdfi.entity.Cbo_phoneEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.fragment_phonedetaillist.*
import kotlinx.android.synthetic.main.tablayoutgroup.*
import kotlinx.android.synthetic.main.white_toolbar.ivBack
import kotlinx.android.synthetic.main.white_toolbar.ivHome
import kotlinx.android.synthetic.main.white_toolbar.tv_title
import kotlinx.android.synthetic.main.whiteicon_toolbar.*

class PhoneDetailListActivity : AppCompatActivity() {

    var validate: Validate? = null
    var phoneDetailEntity : Cbo_phoneEntity? = null
    var cboPhoneViewmodel: CboPhoneViewmodel? = null
    var memberviewmodel: Memberviewmodel? = null
    var shgViewmodel: SHGViewmodel? = null
    var addressViewmodel: CboAddressViewmodel? = null
    var phoneViewmodel: CboPhoneViewmodel? = null
    var bank: CboBankViewmodel? = null
    var systemtagViewmodel: SystemtagViewmodel? = null
    var memberDesignationViewmodel: MemberDesignationViewmodel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_phonedetaillist)

        validate = Validate(this)
        cboPhoneViewmodel = ViewModelProviders.of(this).get(CboPhoneViewmodel::class.java)
        memberviewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)
        shgViewmodel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        addressViewmodel = ViewModelProviders.of(this).get(CboAddressViewmodel::class.java)
        phoneViewmodel = ViewModelProviders.of(this).get(CboPhoneViewmodel::class.java)
        bank = ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        systemtagViewmodel = ViewModelProviders.of(this).get(SystemtagViewmodel::class.java)
        memberDesignationViewmodel = ViewModelProviders.of(this).get(MemberDesignationViewmodel::class.java)
//        IvSearch.visibility = View.GONE
        if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) == 1) {
            ivLock.visibility = View.VISIBLE
            addAddressgray.visibility = View.VISIBLE
            addAddress.visibility = View.GONE
        } else {
            ivLock.visibility = View.GONE
            addAddressgray.visibility = View.GONE
            addAddress.visibility = View.VISIBLE
        }

        tv_title.text =
            validate!!.RetriveSharepreferenceString(AppSP.ShgName)!! + "(" + validate!!.RetriveSharepreferenceString(AppSP.Shgcode) + ")"

        ivBack.setOnClickListener {
            var intent = Intent(this,ShgListActivity::class.java)
            intent.flags=Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

        ivHome.setOnClickListener {
            var intent = Intent(this,ShgListActivity::class.java)
            intent.flags=Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }


        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvVector.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimary))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        var shglist=shgViewmodel!!.getSHG(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var banklist=bank!!.getBankdatalistcount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var addresslist=addressViewmodel!!.getAddressDatalistcount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var systemlist=systemtagViewmodel!!.getSystemtagdatalistcount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var memberdesignationcount = memberDesignationViewmodel!!.getMemberDesignationdetaillistcount(
            validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)
        )
       if(!shglist.isNullOrEmpty()){
           IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

       }else{
           IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

       }
        if(!banklist.isNullOrEmpty()){
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        }else{
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        if(!addresslist.isNullOrEmpty()){
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        }else{
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        if(!systemlist.isNullOrEmpty()){
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        }else{
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        if(!memberdesignationcount.isNullOrEmpty()){
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))

        }else{
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))

        }
        lay_systemTag.setOnClickListener {
            var intent = Intent(this, GroupSystemTagList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_vector.setOnClickListener {
            var intent = Intent(this,BasicDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_location.setOnClickListener {
            var intent = Intent(this,AddressDetailListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_bank.setOnClickListener {
            var intent = Intent(this,BankDetailListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_post.setOnClickListener {
            var intent = Intent(this,MemberDesignationActvity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        addAddress!!.setOnClickListener {
            if (!validate!!.RetriveSharepreferenceString(AppSP.SHGGUID).isNullOrEmpty()) {
                validate!!.SaveSharepreferenceString(AppSP.PhoneGUID, "")
                var inetnt = Intent(this, PhoneDetailActivity::class.java)
                startActivity(inetnt)
                finish()
            }else{
                validate!!.CustomAlert(LabelSet.getText(
                    "insert_Basic_data_first",
                    R.string.insert_Basic_data_first
                ),this,BasicDetailActivity::class.java)
            }
        }

        if(cboPhoneViewmodel!!.getVerificationCount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))>0){
            btn_phoneVerify.visibility = View.VISIBLE
        }else {
            btn_phoneVerify.visibility = View.GONE
        }
        btn_phoneVerify.setOnClickListener {
            var isCompleteCount = cboPhoneViewmodel!!.getCompletionCount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
            if(isCompleteCount==0){
                cboPhoneViewmodel!!.updateIsVerifed(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID),validate!!.Daybetweentime(validate!!.currentdatetime),
                    validate!!.RetriveSharepreferenceString(AppSP.userid)!!)
                validate!!.CustomAlert(LabelSet.getText(
                    "",
                    R.string.verified_successfully
                ),this)
                if(cboPhoneViewmodel!!.getVerificationCount(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))>0){
                    btn_phoneVerify.visibility = View.VISIBLE
                }else {
                    btn_phoneVerify.visibility = View.GONE
                }
            }else {
                validate!!.CustomAlert(LabelSet.getText(
                    "",
                    R.string.completerecord
                ),this)
            }
        }

        fillData()

    }

    private fun fillData() {
        cboPhoneViewmodel!!.getphonedata(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))!!
            .observe(this,object : Observer<List<Cbo_phoneEntity>> {
                override fun onChanged(cbo_list: List<Cbo_phoneEntity>?) {
                    if (cbo_list!=null){
                        rvPhoneList.layoutManager = LinearLayoutManager(this@PhoneDetailListActivity)
                        rvPhoneList.adapter = CboPhoneAdapter(this@PhoneDetailListActivity, cbo_list)
                        if(cbo_list.size>0){
                            lay_nophone_avialable.visibility= View.GONE
                        }else {
                            lay_nophone_avialable.visibility= View.VISIBLE
                            tv_nophone_avialable.text = LabelSet.getText(
                                "no_phone_s_avialable",
                                R.string.no_phone_s_avialable
                            )
                        }
                    }
                }
            })
    }

    override fun onBackPressed() {
        var intent = Intent(this,ShgListActivity::class.java)
        intent.flags=Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }

    fun returnMemberName(memberGuid:String): String {
        var dataspin_belong = memberviewmodel!!.getAllMemberlist(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var sValue = ""

        if (!dataspin_belong.isNullOrEmpty()) {
            for (i in dataspin_belong.indices) {
                if (memberGuid.equals(dataspin_belong.get(i).member_guid))
                    sValue = dataspin_belong.get(i).member_name!!
            }
        }
        return sValue
    }

    fun CustomAlert(guid:String,iFlag:Int) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = LabelSet.getText(
            "do_u_want_to_delete",
            R.string.do_u_want_to_delete
        )
        mDialogView.txt_dialog_title.text = LabelSet.getText("app_name", R.string.app_name)
        mDialogView.btn_yes.text = LabelSet.getText("yes", R.string.yes)
        mDialogView.btn_no.text = LabelSet.getText("no", R.string.no)
        mDialogView.btn_yes.setOnClickListener {

            if(iFlag==0){
                cboPhoneViewmodel!!.deleteRecord(guid)
            }else {
                cboPhoneViewmodel!!.deleteData(guid)

            }

            validate!!.updateLockingFlag(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!,
                shgViewmodel,memberviewmodel)

            mAlertDialog.dismiss()

        }
        mDialogView.btn_no.setOnClickListener {

            mAlertDialog.dismiss()

        }
    }

}
