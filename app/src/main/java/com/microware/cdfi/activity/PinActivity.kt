package com.microware.cdfi.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import com.microware.cdfi.R
import com.microware.cdfi.activity.vo.VoDrawerActivity
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.utility.*
import com.mukesh.OnOtpCompletionListener
import kotlinx.android.synthetic.main.layout_pin_screen.*
import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*

class PinActivity : AppCompatActivity(), OnOtpCompletionListener {
    var validate: Validate? = null
    var backup: Backup? = null

    var wrongPinCount: Int = 0
    var getSec = 0
    var makeMilllis: Long = 180000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.requestWindowFeature(Window.FEATURE_NO_TITLE)
//Remove notification bar
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.layout_pin_screen)
        validate = Validate(this)
        otp_view.setOtpCompletionListener(this)
        insert_your.text = LabelSet.getText(
            "insert_your",
            R.string.insert_your
        ) + " " + validate!!.RetriveSharepreferenceString(AppSP.userid) + " (" + validate!!.RetriveSharepreferenceString(AppSP.RoleName)+ ") "
        tv_forgotpin.setOnClickListener {
            val startMain = Intent(this, LoginActivity::class.java)
            startMain.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(startMain)
        }

        tv_change_pin.setOnClickListener {
            val startMain = Intent(this, ChangePinActivity::class.java)
            startMain.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(startMain)
        }

        insert_your.setOnClickListener {
            backup = Backup(this)
            backup!!.backup(CDFIApplication.database!!.openHelper.databaseName)
            this.finishAffinity()
        }
        setLabelText()

        tv_timer.visibility = View.GONE
        tv_msg.visibility = View.GONE
        pinTimeStamp()
    }


    override fun onOtpCompleted(otp: String?) {
        // Toast.makeText(this, "OnOtpCompletionListener called$otp", Toast.LENGTH_SHORT).show();
        /* var sec = validate!!.currentdatetime
          var totsec = validate!!.Daybetweentime(sec)
          var date1 = validate!!.convertDatetime(totsec*1000)
          var abc = 0*/
        if (validate!!.returnRoleValue(validate!!.RetriveSharepreferenceString(AppSP.Roleid)) == 310) {

            if (otp.equals(validate!!.returnStringValue(
                    AESEncryption.decrypt(
                        validate!!.RetriveSharepreferenceString(AppSP.Pin), validate!!.RetriveSharepreferenceString(AppSP.userid))))) {
                val startMain = Intent(this, MainActivityDrawer::class.java)
                startMain.flags =
                    Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(startMain)
            } else {
                Toast.makeText(this, "Please enter valid pin", Toast.LENGTH_SHORT).show()
                otp_view.setText("")
                wrongPinCount ++
                check()

            }
        } else if (validate!!.returnRoleValue(validate!!.RetriveSharepreferenceString(AppSP.Roleid)) == 410) {
            if (otp.equals(validate!!.returnStringValue(
                    AESEncryption.decrypt(
                        validate!!.RetriveSharepreferenceString(AppSP.VoPin), validate!!.RetriveSharepreferenceString(AppSP.userid))))) {
                validate!!.SaveSharepreferenceInt(AppSP.FormRoleType, 1)
                val startMain = Intent(this, VoDrawerActivity::class.java)
                startMain.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(startMain)
            } else {
                Toast.makeText(this, "Please enter valid pin", Toast.LENGTH_SHORT).show()
                otp_view.setText("")
                wrongPinCount ++
                check()

            }
        }  else if (validate!!.returnRoleValue(validate!!.RetriveSharepreferenceString(AppSP.Roleid)) == 450) {
            if (otp.equals(validate!!.returnStringValue(
                    AESEncryption.decrypt(
                        validate!!.RetriveSharepreferenceString(AppSP.VoappPin), validate!!.RetriveSharepreferenceString(AppSP.userid))))) {
                validate!!.SaveSharepreferenceInt(AppSP.FormRoleType, 1)
                val startMain = Intent(this, VoDrawerActivity::class.java)
                startMain.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(startMain)
            } else {
                Toast.makeText(this, "Please enter valid pin", Toast.LENGTH_SHORT).show()
                otp_view.setText("")
                wrongPinCount ++
                check()

            }
        } else if (validate!!.returnRoleValue(validate!!.RetriveSharepreferenceString(AppSP.Roleid)) == 510) {
            if (otp.equals(validate!!.returnStringValue(
                    AESEncryption.decrypt(
                        validate!!.RetriveSharepreferenceString(AppSP.CLfPin), validate!!.RetriveSharepreferenceString(AppSP.userid))))) {
                validate!!.SaveSharepreferenceInt(AppSP.FormRoleType, 2)
                val startMain = Intent(this, VoDrawerActivity::class.java)
                startMain.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(startMain)
            } else {
                Toast.makeText(this, "Please enter valid pin", Toast.LENGTH_SHORT).show()
                otp_view.setText("")
                wrongPinCount ++
                check()

            }
        } else {
            Toast.makeText(this, "Please enter valid pin", Toast.LENGTH_SHORT).show()
            otp_view.setText("")
            wrongPinCount ++
            check()

        }

    }
    private fun check() {
        if (wrongPinCount == 5) {
            startCountDownTimer()

            val currentTime: Long = System.currentTimeMillis()
            val threeMinutesLater: Long = currentTime + 180000
            validate!!.SaveSharepreferenceLong(AppSP.pinTimerSave, threeMinutesLater)
        }
    }


    private fun startCountDownTimer() {
        tv_timer.visibility = View.VISIBLE
        tv_msg.visibility = View.VISIBLE
        btn_done.isClickable = false
        otp_view.isClickable = false
        otp_view.isEnabled = false
        object : CountDownTimer(makeMilllis, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val f: NumberFormat = DecimalFormat("00")
                val hour = millisUntilFinished / 3600000 % 24
                val min = millisUntilFinished / 60000 % 60
                val sec = millisUntilFinished / 1000 % 60
                tv_timer.text = getString(R.string.min_sec, f.format(min), f.format(sec))

            }

            override fun onFinish() {
                makeMilllis  = 180000
                wrongPinCount = 0
                getSec = 0
                btn_done.isClickable = true
                otp_view.isClickable = true
                otp_view.isEnabled = true
                tv_timer.visibility = View.GONE
                tv_msg.visibility = View.GONE
                validate!!.SaveSharepreferenceLong(AppSP.pinTimerSave, 0)
            }
        }.start()
    }

    fun pinTimeStamp(){

        if (!validate!!.RetriveSharepreferenceLong(AppSP.pinTimerSave).equals(null)) {
            val storetime: Long = validate!!.RetriveSharepreferenceLong(AppSP.pinTimerSave)

            if (System.currentTimeMillis() < storetime) {
                val currentTime: String =
                    SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(Date())
                val simpleDateFormat = SimpleDateFormat("yyyy:MM:dd:HH:mm:ss")
                val dateString = simpleDateFormat.format(storetime)
                var aa: String = String.format("Date: %s", dateString)

                val separateds: List<String> = currentTime.split(":")
                var ctimeHrs = separateds[0]
                var ctimeMin = separateds[1]
                var ctimeSec = separateds[2]

                val separated: List<String> = dateString.split(":")
                var stimeDate = separated[0]
                var stimeaSec = separated[1]
                var stimeSsec = separated[2]
                var stimeHrs = separated[3]
                var stimeMin = separated[4]
                var stimeSec = separated[5]

                if (ctimeHrs >= stimeHrs) {
                    makeMilllis = 0
                    var findHrsdiff = Integer.parseInt(ctimeHrs) - Integer.parseInt(stimeHrs)

                    if (ctimeMin <= stimeMin) {
                        var findMindiff = Integer.parseInt(stimeMin) - Integer.parseInt(ctimeMin)
                        var getMin = findMindiff * 60 * 1000
                        if (ctimeSec >= stimeSec) {
                            var findSecdiff =
                                Integer.parseInt(ctimeSec) - Integer.parseInt(stimeSec)
                            getSec = findSecdiff * 1000

                        }
                        makeMilllis = (getMin + getSec).toLong()
                        startCountDownTimer()

                    }
                }

            }
        }
    }
    fun setLabelText() {
        lokos_core.text = LabelSet.getText(
            "lokos_core",
            R.string.lokos_core
        )
        insert_your.text = LabelSet.getText(
            "insert_your",
            R.string.insert_your
        ) + " " + validate!!.RetriveSharepreferenceString(AppSP.userid)+ " (" + validate!!.RetriveSharepreferenceString(AppSP.RoleName)+ ") "
        btn_done.text = LabelSet.getText("done", R.string.done)
        tv_forgotpin.text = LabelSet.getText(
            "forgot_pin",
            R.string.forgot_pin
        )
        tv_change_pin.text = LabelSet.getText(
            "change_pin",
            R.string.change_pin
        )
        tv_msg.text = LabelSet.getText(
            "wrong_mpin_attempts",
            R.string.wrong_mpin_attempts
        )
    }

}
