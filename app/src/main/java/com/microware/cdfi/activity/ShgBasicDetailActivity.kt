package com.microware.cdfi.activity


import android.app.TabActivity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TabHost
import com.microware.cdfi.R
import com.microware.cdfi.utility.LabelSet
import kotlinx.android.synthetic.main.white_toolbar.*


class ShgBasicDetailActivity : TabActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shg_basic_detail)
        tv_title.text = getString(R.string.shgbasicdetails)
        ivHome.visibility= View.INVISIBLE
        val tabHost = tabHost

        val detail =
            tabHost.newTabSpec("SHG")
                .setIndicator("", resources.getDrawable(R.drawable.tab_selected))
        val detailIntent = Intent(this, BasicDetailActivity::class.java)
        detail.setContent(detailIntent)

        val phonespec =
            tabHost.newTabSpec("Phone")
                .setIndicator("", resources.getDrawable(R.drawable.tab_selected1))
        val phoneIntent = Intent(this, PhoneDetailActivity::class.java)
        phonespec.setContent(phoneIntent)

        val location =
            tabHost.newTabSpec("Address")
                .setIndicator("", resources.getDrawable(R.drawable.tab_selected2))
        val locationIntent = Intent(this, AddressDetailActivity::class.java)
        location.setContent(locationIntent)

        val musem =
            tabHost.newTabSpec("Bank")
                .setIndicator("", resources.getDrawable(R.drawable.tab_selected3))
        val musemIntent = Intent(this, BankDetailActivity::class.java)
        musem.setContent(musemIntent)

        val kyc = tabHost.newTabSpec("KYC")
            .setIndicator("", resources.getDrawable(R.drawable.tab_selected4))
        val kycIntent = Intent(this, KycDetailActivity::class.java)
        kyc.setContent(kycIntent)


        tabHost.addTab(detail)
        tabHost.addTab(phonespec)
        tabHost.addTab(location)
        tabHost.addTab(musem)
        tabHost.addTab(kyc)
        tabHost.setOnTabChangedListener(TabHost.OnTabChangeListener { tabId ->
            if (tabId.equals("SHG")) {
                tv_title.text = LabelSet.getText(
                    "shgbasicdetails",
                    R.string.shgbasicdetails
                )
            } else if (tabId.equals("Phone")) {
                tv_title.text = LabelSet.getText(
                    "phonedetails",
                    R.string.phonedetails
                )
            } else if (tabId.equals("Address")) {
                tv_title.text = LabelSet.getText(
                    "addressdetails",
                    R.string.addressdetails
                )
            } else if (tabId.equals("Bank")) {
                tv_title.text = LabelSet.getText(
                    "bankdetails",
                    R.string.bankdetails
                )
            } else if (tabId.equals("KYC")) {
                tv_title.text = LabelSet.getText(
                    "kycdetails",
                    R.string.kycdetails
                )
            }else{
                tv_title.text = LabelSet.getText(
                    "shgbasicdetails",
                    R.string.shgbasicdetails
                )
            }
        })

    }

}
