package com.microware.cdfi.activity

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.JsonObject
import com.microware.cdfi.R
import com.microware.cdfi.R.layout.activity_shg_list
import com.microware.cdfi.adapter.SHGListAdapter
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.api.response.ShgDownloadResponse
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.*
import com.microware.cdfi.utility.*
import com.microware.cdfi.utility.MappingData.*
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_shg_list.*
import kotlinx.android.synthetic.main.activity_shg_list.spin_village
import kotlinx.android.synthetic.main.customealertdialogelogin.*
import kotlinx.android.synthetic.main.customealertdialogelogin.view.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.shg_toolbar.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ShgListActivity : AppCompatActivity() {
    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: ApiInterface? = null
    var shgviewmodel: SHGViewmodel? = null
    var memberviewmodel: Memberviewmodel? = null
    var locationViewModel: LocationViewModel? = null
    var responseViewModel: ResponseViewModel? = null
    var lookupViewmodel: LookupViewmodel? = null
    var lookupEntity: LookupEntity? = null
    var validate: Validate? = null
    var count = 0
    var activecount = 0
    var sStateCode = 0
    var sDistrictCode = 0
    var sBlockCode = 0
    var sPanchayatCode = 0
    var sVillageID = 0
    var villageData: List<VillageEntity>? = null
    var dataPanchayat: List<PanchayatEntity>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(activity_shg_list)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()

        shgviewmodel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        memberviewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)
        locationViewModel = ViewModelProviders.of(this).get(LocationViewModel::class.java)
        responseViewModel = ViewModelProviders.of(this).get(ResponseViewModel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)

        validate = Validate(this)
        tv_title.text = LabelSet.getText(
            "shg_list",
            R.string.shg_list
        )

        sStateCode = validate!!.RetriveSharepreferenceInt(AppSP.statecode)
        sDistrictCode = validate!!.RetriveSharepreferenceInt(AppSP.districtcode)
        sBlockCode = validate!!.RetriveSharepreferenceInt(AppSP.blockcode)
        sPanchayatCode = validate!!.RetriveSharepreferenceInt(AppSP.panchayatcode)

        // shgviewmodel!!.updateCheckerRemarks(checker_remrks,shgGUID)

        icBack.setOnClickListener {
            val i = Intent(this, MainActivityDrawer::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)
            finish()
        }

        IVSync.setOnClickListener {
            val i = Intent(this, MasterSyncActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)
            finish()
        }

        IvAdd.setOnClickListener {
            validate!!.SaveSharepreferenceString(AppSP.SHGGUID, "")
            validate!!.SaveSharepreferenceString(AppSP.ShgName, "")
            validate!!.SaveSharepreferenceString(AppSP.Shgcode, "")
            validate!!.SaveSharepreferenceInt(AppSP.LockRecord,  0)
            validate!!.SaveSharepreferenceInt(AppSP.CboType,  0)
            validate!!.SaveSharepreferenceInt(AppSP.Tag,  0)

            val i = Intent(this, BasicDetailActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)
            finish()
        }

        sStateCode = validate!!.RetriveSharepreferenceInt(AppSP.statecode)
        sDistrictCode = validate!!.RetriveSharepreferenceInt(AppSP.districtcode)
        sBlockCode = validate!!.RetriveSharepreferenceInt(AppSP.blockcode)
        sVillageID = validate!!.RetriveSharepreferenceInt(AppSP.villagecode)
        sPanchayatCode = validate!!.RetriveSharepreferenceInt(AppSP.panchayatcode)

        fillSpinner()
        ShowData()

        spin_panchayat?.onItemSelectedListener = object:AdapterView.OnItemSelectedListener{
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                sPanchayatCode = validate!!.returnPanchayatID(spin_panchayat,dataPanchayat)
                if (position > 0){
                    validate!!.SaveSharepreferenceInt(AppSP.panchayatcode,sPanchayatCode)
                    bindVillageSpinner(sStateCode,sDistrictCode,sBlockCode,sPanchayatCode)
                    if (sVillageID > 0){
                        spin_village.setSelection(returnVillagepos(sVillageID,villageData))
                    }else{
                        spin_village.setSelection(0)
                    }
                }else{
                    bindVillageSpinner(sStateCode,sDistrictCode,sBlockCode,sPanchayatCode)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

        }

        spin_village?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                var ID = returnVillageID(spin_village, villageData)
                if (sVillageID > 0 && sVillageID == ID){
                    validate!!.SaveSharepreferenceInt(AppSP.villagecode,sVillageID)
                    fillData(sVillageID)
                }else{
                    validate!!.SaveSharepreferenceInt(AppSP.villagecode,ID)
                    fillData(ID)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        setLabelText()
    }

    private fun ShowData() {
        spin_panchayat.setSelection(validate!!.returnPanchayatpos(sPanchayatCode,dataPanchayat))
        spin_village.setSelection(returnVillagepos(sVillageID,villageData))
    }

    private fun fillSpinner() {
        dataPanchayat = locationViewModel!!.getPanchayatByPanchayatCode(sStateCode, sDistrictCode, sBlockCode)
        validate!!.fillPanchayatSpinner(this,spin_panchayat,dataPanchayat)

        bindVillageSpinner(sStateCode, sDistrictCode, sBlockCode,sPanchayatCode)
    }

    fun bindVillageSpinner(
        stateCode: Int,
        districtCode: Int,
        blockCode: Int,
        panchayatCode: Int) {

        villageData = locationViewModel?.getVillagedata_by_panchayatCode(
            stateCode,
            districtCode,
            blockCode,
            panchayatCode
        )

        if (villageData != null) {
            val iGen = villageData!!.size
            val name = arrayOfNulls<String>(iGen)
//            name[0] = LabelSet.getText("all", R.string.all)

            for (i in 0 until villageData!!.size) {
                name[i] = villageData!!.get(i).village_name_en
            }
            val adapter_category = ArrayAdapter<String>(
                this, R.layout.my_spinner_space, name
            )
            adapter_category.setDropDownViewResource(R.layout.my_spinner)
            spin_village?.adapter = adapter_category
        }

    }

    fun returnVillageID(spin: Spinner, data: List<VillageEntity>?): Int {

        var pos = spin.selectedItemPosition
        var id = 0

        if (!data.isNullOrEmpty()) {
                id = data.get(pos).village_id
        }
        return id
    }

    fun returnVillagepos(id: Int?, data: List<VillageEntity>?): Int {

        var pos = 0
        if (!data.isNullOrEmpty()) {
            if (id!! > 0) {
                for (i in data.indices) {
                    if (id == data.get(i).village_id)
                        pos = i
                }
            }
        }
        return pos
    }

    fun getmembercount(shgguid: String): Int {
        return memberviewmodel!!.getcount(shgguid)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun getshgdata(shgid: Long) {
        var iDownload = 0
        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText("DataLoading", R.string.DataLoading)
        )
        val userId = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
        val token = validate!!.returnStringValue(
            AESEncryption.decrypt(
                validate!!.RetriveSharepreferenceString(AppSP.token),
                validate!!.RetriveSharepreferenceString(AppSP.userid)
            ))

        val call = apiInterface?.getSHGData(
            "application/json", token, userId,
            shgid
        )
        call?.enqueue(object : Callback<ShgDownloadResponse> {

            //  call?.enqueue(object : Callback<List<MastersResponse>>, retrofit2.Callback<List<MastersResponse>> {
            override fun onFailure(
                call: Call<ShgDownloadResponse>,
                t: Throwable
            ) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<ShgDownloadResponse>,
                response: Response<ShgDownloadResponse>
            ) {
                if (response.isSuccessful) {
                    when (response.code()) {
                        200 -> {

                            if (response.body()?.shgUploadModel != null) {
                                try {
                                    var shg = response.body()?.shgUploadModel
                                    var shgaddress = shg!!.cboAddressesList
                                    var shgbank = shg.cboBankDetailsList
                                    var shgphone = shg.cboPhoneNoDetailsList
                                    var shglist = returnObjentity(shg)
                                    var membermodellist=shg.memberProfileList
                                    var memberlist = returnmemberentity(membermodellist)
                                    if (shglist!=null) {
                                        CDFIApplication.database?.shgDao()
                                            ?.insertShgData(shglist)
                                    }
                                    if (!shgaddress.isNullOrEmpty()) {
                                        CDFIApplication.database?.cboaddressDao()
                                            ?.insertAddressDetail(shgaddress)
                                    }

                                    if (!shgphone.isNullOrEmpty()) {
                                        CDFIApplication.database?.cbophoneDao()
                                            ?.insertPhoneDetail(
                                                shgphone
                                            )
                                    }
                                    if (!shgbank.isNullOrEmpty()) {
                                        CDFIApplication.database?.cboBankDao()
                                            ?.insertBankDetail(
                                                shgbank
                                            )
                                    }

                                    if (!memberlist.isNullOrEmpty()) {
                                        CDFIApplication.database?.memberDao()
                                            ?.insertMember(memberlist)
                                    }


                                    for(i in membermodellist!!.indices){
                                        var memberbanklist = membermodellist.get(i).memberBankList
                                        var memberaddresslist = membermodellist.get(i).memberAddressesList
                                        var memberkyclist = membermodellist.get(i).memberKYCDetailsList
                                        var memberphonelist = membermodellist.get(i).memberPhoneNoDetailsList
                                        var membertaglist = membermodellist.get(i).memberSystemTagsList
                                        if (!memberaddresslist.isNullOrEmpty()) {
                                            CDFIApplication.database?.memberaddressDao()
                                                ?.insertAddressDetail(
                                                    memberaddresslist
                                                )
                                        }
                                        if (!memberbanklist.isNullOrEmpty()) {
                                            CDFIApplication.database?.memberbanckDao()
                                                ?.insertBankDetail(
                                                    memberbanklist
                                                )
                                        }

                                        if (!memberkyclist.isNullOrEmpty()) {
                                            CDFIApplication.database?.memberkycDao()
                                                ?.insertKycDetail(
                                                    memberkyclist
                                                )
                                        }

                                        if (!memberphonelist.isNullOrEmpty()) {
                                            CDFIApplication.database?.memberphoneDao()
                                                ?.insertPhoneDetail(
                                                    memberphonelist
                                                )
                                        }

                                        if (!membertaglist.isNullOrEmpty()) {
                                            CDFIApplication.database?.memberSystemtagDao()
                                                ?.insertSystemtagDetail(
                                                    membertaglist
                                                )
                                        }
                                    }


                                    iDownload = 1
                                    progressDialog.dismiss()
                                } catch (ex: Exception) {
                                    ex.printStackTrace()
                                    progressDialog.dismiss()
                                }
                            } else {
                                progressDialog.dismiss()
                            }

                        }

                    }

                } else {
                    iDownload = 0
                    progressDialog.dismiss()
                    var resCode = 0
                    var resMsg = ""
                    when (response.code()) {
                        403 ->
                            CustomAlertlogin()
                        else -> {
                            if(response.errorBody()?.contentLength()==0L || response.errorBody()?.contentLength()!! <0L){
                                resCode = response.code()
                                resMsg = response.message()
                            }else {
                                var jsonObject1 =
                                    JSONObject(response.errorBody()!!.source().readUtf8().toString())

                                resCode =
                                    validate!!.returnIntegerValue(
                                        jsonObject1.optString("responseCode").toString()
                                    )
                                resMsg = validate!!.returnStringValue(
                                    jsonObject1.optString("responseMsg").toString()
                                )
                            }

                            var msg = "" + validate!!.alertMsg(
                                this@ShgListActivity,
                                responseViewModel,
                                resCode,
                                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                                resMsg
                            )
                            Toast.makeText(
                                this@ShgListActivity,
                                msg,
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }
                }// if
            }

        })

    }

    override fun onBackPressed() {
        super.onBackPressed()
        val i = Intent(this, MainActivityDrawer::class.java)
        i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(i)
        finish()
    }

    fun fillData(villageid: Int) {
        shgviewmodel!!.getAllSHG(villageid)?.observe(this@ShgListActivity, object : Observer<List<SHGEntity>> {
            override fun onChanged(SHGList: List<SHGEntity>?) {

                if (SHGList!=null) {
                    tblLegend.visibility = View.VISIBLE
                    rvShgList.visibility = View.VISIBLE
                    rvShgList.layoutManager = LinearLayoutManager(this@ShgListActivity)
                    rvShgList.adapter = SHGListAdapter(this@ShgListActivity, SHGList)
                } else {
                    rvShgList.visibility = View.INVISIBLE
                    tblLegend.visibility = View.GONE
                }
                var pendingcount = shgviewmodel!!.getShgpendingCount(villageid)
                count = shgviewmodel!!.getShgCount(villageid)
                activecount = shgviewmodel!!.getShgactiveCount(villageid)

                tv_shg_count.text = "" + activecount + "/" + count.toString()
                tv_activegroup.text = LabelSet.getText(
                    "active_group_1",
                    R.string.active_group_1
                )+" " +"("+" "+ activecount + " "+")"
                tv_inactivegroup.text = LabelSet.getText(
                    "inactive_group_1",
                    R.string.inactive_group_1
                )+" " +"("+" "+ 0 + " "+")"
                tv_pendingapproval.text = LabelSet.getText(
                    "pending_approval_1",
                    R.string.pending_approval_1
                )+" " +"("+" "+ pendingcount + " "+")"
            }
        })
    }

    fun fillDataall() {
        shgviewmodel!!.getAll()?.observe(this@ShgListActivity, object : Observer<List<SHGEntity>> {
            override fun onChanged(SHGList: List<SHGEntity>?) {

                if (SHGList!=null) {
                    tblLegend.visibility = View.VISIBLE
                    rvShgList.visibility = View.VISIBLE
                    rvShgList.layoutManager = LinearLayoutManager(this@ShgListActivity)
                    rvShgList.adapter = SHGListAdapter(this@ShgListActivity, SHGList)
                } else {
                    rvShgList.visibility = View.INVISIBLE
                    tblLegend.visibility = View.GONE
                }
                var pendingcount = shgviewmodel!!.getallShgpendingCount()
                count = shgviewmodel!!.getallShgCount()
                activecount = shgviewmodel!!.getallShgactiveCount()

                tv_shg_count.text = "" + activecount + "/" + count.toString()
                tv_activegroup.text = LabelSet.getText(
                    "active_group_1",
                    R.string.active_group_1
                )+" " +"("+" "+ activecount + " "+")"
                tv_inactivegroup.text = LabelSet.getText(
                    "inactive_group_1",
                    R.string.inactive_group_1
                )+" " +"("+" "+ 0 + " "+")"
                tv_pendingapproval.text = LabelSet.getText(
                    "pending_approval_1",
                    R.string.pending_approval_1
                )+" " +"("+" "+ pendingcount + " "+")"
            }
        })
    }

    fun setLabelText() {
        tv_panchayat.text = LabelSet.getText("panchayat", R.string.panchayat)
        tv_village.text = LabelSet.getText("village1", R.string.village1)
        tv_myshg.text = LabelSet.getText(
            "active_shg",
            R.string.active_shg
        )
        tv_pendingapproval.text = LabelSet.getText(
            "pending_approval_1",
            R.string.pending_approval_1
        )
        tv_activegroup.text = LabelSet.getText(
            "active_group_1",
            R.string.active_group_1
        )
        tv_inactivegroup.text = LabelSet.getText(
            "inactive_group_1",
            R.string.inactive_group_1
        )
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun importtoken(user: String, pass: String) {
        var grant_type = "password"
        var userId = user
        var password = pass
        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText(
                "authenticate_user",
                R.string.authenticate_user
            )
        )
        val callCount = apiInterface?.gettokenforce(
            grant_type,
            userId,
            password, validate!!.RetriveSharepreferenceInt(AppSP.State_Selected), 1,
            validate!!.RetriveSharepreferenceString(AppSP.Roleid)!!)
        callCount?.enqueue(object : Callback<JsonObject> {
            override fun onFailure(callCount: Call<JsonObject>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                progressDialog.dismiss()


                if (response.isSuccessful) {

                    var jsonObject1: JSONObject? = null
                    try {
                        jsonObject1 = JSONObject(response.body().toString())

                        val auth = jsonObject1.get("accessToken").toString()
                        if (auth.trim().length > 0) {
                            validate!!.SaveSharepreferenceString(
                                AppSP.token,
                                validate!!.returnStringValue(
                                    AESEncryption.encrypt(
                                        auth,validate!!.RetriveSharepreferenceString(AppSP.userid)))
                            )
                        }
                        validate!!.CustomAlert(
                            LabelSet.getText(
                                "userauthenticatedsuccessfully",
                                R.string.userauthenticatedsuccessfully
                            ), this@ShgListActivity
                        )
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Toast.makeText(
                            this@ShgListActivity,
                            response.message(),
                            Toast.LENGTH_LONG
                        ).show()


                    }

                } else {
                    Toast.makeText(
                        this@ShgListActivity,
                        response.message(),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun CustomAlertlogin() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogelogin, null)
        val mBuilder = androidx.appcompat.app.AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mDialogView.etUsername.setText(validate!!.RetriveSharepreferenceString(AppSP.userid))
        mDialogView.etPassword.setText(validate!!.returnStringValue(AESEncryption.decrypt(
            validate!!.RetriveSharepreferenceString(AppSP.Password),
            validate!!.RetriveSharepreferenceString(AppSP.userid)
        )))
        mDialogView.txt_dialog_title2.text = LabelSet.getText("app_name",R.string.app_name)
        mDialogView.btn_login.text = LabelSet.getText("log_in",R.string.log_in)
        mDialogView.chk_showHide1.text = LabelSet.getText("show_password",R.string.show_password)
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.btn_login.setOnClickListener {
            mAlertDialog.dismiss()
            importtoken(
                mDialogView.etUsername.text.toString(),
                validate!!.returnStringValue(
                    AESEncryption.encrypt(validate!!.returnStringValue(
                        mDialogView.etPassword.text.toString()),mDialogView.etUsername.text.toString()))
            )
        }
        mAlertDialog.chk_showHide1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                mAlertDialog.etPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                mAlertDialog.etPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
        }


    }

    fun CustomAlert(guid:String,iFlag:Int) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = LabelSet.getText(
            "do_u_want_to_delete",
            R.string.do_u_want_to_delete
        )
        mDialogView.btn_yes.setOnClickListener {
            if(iFlag==0){
                shgviewmodel!!.deleteShgByGuid(guid)
                shgviewmodel!!.deleteBankByShgGuid(guid)
                shgviewmodel!!.deletePhoneByShgGuid(guid)
                shgviewmodel!!.deleteAddressByShgGuid(guid)
                shgviewmodel!!.deleteSystemTagByShgGuid(guid)
                shgviewmodel!!.deleteDesignationByShgGuid(guid)
                shgviewmodel!!.deleteMemberAddressByShgGuid(guid)
                shgviewmodel!!.deleteMemberPhoneByShgGuid(guid)
                shgviewmodel!!.deleteMemberBankByShgGuid(guid)
                shgviewmodel!!.deleteMemberKycByShgGuid(guid)
                shgviewmodel!!.deleteMemberSystemTagByShgGuid(guid)
                shgviewmodel!!.deleteMemberByShgGuid(guid)
            }else {
                shgviewmodel!!.deleteShgDataByGuid(guid)
                shgviewmodel!!.deleteBankDataByShgGuid(guid)
                shgviewmodel!!.deletePhoneDataByShgGuid(guid)
                shgviewmodel!!.deleteAddressDataByShgGuid(guid)
                shgviewmodel!!.deleteSystemTagDataByShgGuid(guid)
                shgviewmodel!!.deleteDesignationDataByShgGuid(guid)
                shgviewmodel!!.deleteMemberAddressDataByShgGuid(guid)
                shgviewmodel!!.deleteMemberPhoneDataByShgGuid(guid)
                shgviewmodel!!.deleteMemberBankDataByShgGuid(guid)
                shgviewmodel!!.deleteMemberKycDataByShgGuid(guid)
                shgviewmodel!!.deleteMemberSystemTagDataByShgGuid(guid)
                shgviewmodel!!.deleteMemberDataByShgGuid(guid)
            }
            if (spin_village.selectedItemPosition > 0) {
                var villageid = returnVillageID(spin_village, villageData)
                fillData(villageid)
            } else {
                fillDataall()
            }
            mAlertDialog.dismiss()


        }

        mDialogView.btn_no.setOnClickListener {

            mAlertDialog.dismiss()

        }
    }


}
