package com.microware.cdfi.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.MemberKycAdapter
import com.microware.cdfi.entity.*
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_member_id_list.*
import kotlinx.android.synthetic.main.activity_shg_member_summery.*
import kotlinx.android.synthetic.main.summary_toolbar.*
import java.io.File

class ShgMemberSummery : AppCompatActivity() {

    var lookupViewmodel: LookupViewmodel? = null
    var validate: Validate? = null
    var bankMasterViewModel: MasterBankViewmodel? = null
    var masterBankBranchViewModel: MasterBankBranchViewModel? = null
    var memberbankViewmodel : MemberBankViewmodel? = null
    var memberviewmodel: Memberviewmodel? = null
    var phoneViewmodel: MemberPhoneViewmodel? = null
    var addressViewModel: MemberAddressViewmodel? = null
    var locationViewmodel: LocationViewModel? = null
    var memberSystemtagViewmodel: MemberSystemtagViewmodel? = null
    var memberkycViewmodel: MemberKYCViewmodel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shg_member_summery)

        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        memberviewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)
        phoneViewmodel = ViewModelProviders.of(this).get(MemberPhoneViewmodel::class.java)
        addressViewModel = ViewModelProviders.of(this).get(MemberAddressViewmodel::class.java)
        bankMasterViewModel = ViewModelProviders.of(this).get(MasterBankViewmodel::class.java)
        locationViewmodel = ViewModelProviders.of(this).get(LocationViewModel::class.java)
        memberbankViewmodel = ViewModelProviders.of(this).get(MemberBankViewmodel::class.java)
        masterBankBranchViewModel = ViewModelProviders.of(this).get(MasterBankBranchViewModel::class.java)
        memberSystemtagViewmodel = ViewModelProviders.of(this).get(MemberSystemtagViewmodel::class.java)
        memberkycViewmodel = ViewModelProviders.of(this).get(MemberKYCViewmodel::class.java)
        validate = Validate(this)

        tv_title.text =
            validate!!.RetriveSharepreferenceString(AppSP.memebrname)!! + "(" + validate!!.RetriveSharepreferenceString(
                AppSP.ShgName) + ")"

        icBack.setOnClickListener {
            var intent = Intent(this, MemberListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

        liSummary.visibility = View.VISIBLE
        ivUpSummary.visibility = View.VISIBLE
        ivDownSummary.visibility = View.GONE

        ivDownSummary.setOnClickListener {
            liSummary.visibility = View.VISIBLE
            ivUpSummary.visibility = View.VISIBLE
            ivDownSummary.visibility = View.GONE
        }

        ivUpSummary.setOnClickListener {
            liSummary.visibility = View.GONE
            ivUpSummary.visibility = View.GONE
            ivDownSummary.visibility = View.VISIBLE
        }

        ivDownBasicDetail.setOnClickListener {
            liBasicDetail.visibility = View.VISIBLE
            ivUpBasicDetail.visibility = View.VISIBLE
            ivDownBasicDetail.visibility = View.GONE
        }

        ivUpBasicDetail.setOnClickListener {
            liBasicDetail.visibility = View.GONE
            ivUpBasicDetail.visibility = View.GONE
            ivDownBasicDetail.visibility = View.VISIBLE
        }

        ivDownMobileDetail.setOnClickListener {
            liMobileDetail.visibility = View.VISIBLE
            ivUpMobileDetail.visibility = View.VISIBLE
            ivDownMobileDetail.visibility = View.GONE
        }
        ivUpMobileDetail.setOnClickListener {
            liMobileDetail.visibility = View.GONE
            ivUpMobileDetail.visibility = View.GONE
            ivDownMobileDetail.visibility = View.VISIBLE
        }
        ivDownAddressDetail.setOnClickListener {
            liAddressDetail.visibility = View.VISIBLE
            ivUpAddressDetail.visibility = View.VISIBLE
            ivDownAddressDetail.visibility = View.GONE
        }
        ivUpAddressDetail.setOnClickListener {
            liAddressDetail.visibility = View.GONE
            ivUpAddressDetail.visibility = View.GONE
            ivDownAddressDetail.visibility = View.VISIBLE
        }
        ivDownBankDetails.setOnClickListener {
            liBankDetails.visibility = View.VISIBLE
            ivUpBankDetails.visibility = View.VISIBLE
            ivDownBankDetails.visibility = View.GONE
        }
        ivUpBankDetails.setOnClickListener {
            liBankDetails.visibility = View.GONE
            ivUpBankDetails.visibility = View.GONE
            ivDownBankDetails.visibility = View.VISIBLE
        }
        ivDownOtherID.setOnClickListener {
            liOtherID.visibility = View.VISIBLE
            ivUpOtherID.visibility = View.VISIBLE
            ivDownOtherID.visibility = View.GONE
        }
        ivUpOtherID.setOnClickListener {
            liOtherID.visibility = View.GONE
            ivUpOtherID.visibility = View.GONE
            ivDownOtherID.visibility = View.VISIBLE
        }

        ivUpKycDetails.setOnClickListener {
            liKycDetails.visibility = View.GONE
            ivUpKycDetails.visibility = View.GONE
            ivDownKycDetails.visibility = View.VISIBLE
        }

        ivDownKycDetails.setOnClickListener {
            liKycDetails.visibility = View.VISIBLE
            ivUpKycDetails.visibility = View.VISIBLE
            ivDownKycDetails.visibility = View.GONE
        }

        showData()

    }

    private fun showData() {
        try {
            memberviewmodel!!.getMemberSummary(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))
                ?.observe(this, object : Observer<List<MemberEntity>> {
                    override fun onChanged(memberList: List<MemberEntity>?) {
                        if (memberList != null && memberList.isNotEmpty()) {
                            var dataspin_gender: List<LookupEntity>? = null
                            var dataradio_isDOB: List<LookupEntity>? = null
                            var dataspin_guardianRelation: List<LookupEntity>? = null
                            var dataspin_status: List<LookupEntity>? = null
                            var dataspin_maritial: List<LookupEntity>? = null
                            var dataspin_disabletype: List<LookupEntity>? = null
                            var dataspin_motherrekation: List<LookupEntity>? = null
                            var dataspin_post: List<LookupEntity>? = null
                            var dataspin_category: List<LookupEntity>? = null
                            var dataspin_religion: List<LookupEntity>? = null
                            var dataspin_education: List<LookupEntity>? = null
                            var dataspin_name: List<LookupEntity>? = null
                            var dataradioDisable: List<LookupEntity>? = null
                            dataradioDisable = lookupViewmodel!!.getlookup(95,validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
                            dataspin_gender = lookupViewmodel!!.getlookup(4, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
                            dataradio_isDOB = lookupViewmodel!!.getlookup(9, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
                            dataspin_guardianRelation = lookupViewmodel!!.getlookup(25, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
                            dataspin_motherrekation = lookupViewmodel!!.getlookup(24, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
                            dataspin_maritial = lookupViewmodel!!.getlookup(26, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
                            dataspin_status = lookupViewmodel!!.getlookup(5, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
                            dataspin_disabletype = lookupViewmodel!!.getlookup(46, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
                            dataspin_post = lookupViewmodel!!.getlookup(1, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
                            dataspin_category = lookupViewmodel!!.getlookup(2, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
                            dataspin_religion = lookupViewmodel!!.getlookup(42, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
                            dataspin_education = lookupViewmodel!!.getlookup(8, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
                            dataspin_name = lookupViewmodel!!.getlookup(7, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))

                            tv_set_name.text = memberList.get(0).member_name
                            tv_set_gender.text = validate!!.returnlookupcodevalue(validate!!.returnIntegerValue(memberList.get(0).gender.toString()), dataspin_gender)
                            tv_set_is_dob_available.text = validate!!.returnlookupcodevalue(memberList.get(0).dob_available, dataradio_isDOB)
                            tv_set_dob.text = validate!!.convertDatetime(validate!!.returnLongValue(memberList.get(0).dob.toString()))
                            tv_set_father_husband.text = memberList.get(0).relation_name
                            tv_set_relation_mother_father_husband.text = validate!!.returnlookupcodevalue(validate!!.returnIntegerValue(memberList.get(0).father_husband.toString()), dataspin_motherrekation)
                            tv_set_martial_status.text = validate!!.returnlookupcodevalue(validate!!.returnIntegerValue(memberList.get(0).marital_status.toString()), dataspin_maritial)

                            tv_set_head_of_family.text = validate!!.returnlookupcodevalue(validate!!.returnIntegerValue(memberList.get(0).head_house_hold.toString()), dataradio_isDOB)
                            tv_set_disability.text = validate!!.returnlookupcodevalue(validate!!.returnIntegerValue(memberList.get(0).is_disabled.toString()), dataradioDisable)
                            tv_set_disability_type.text = validate!!.returnlookupcodevalue(validate!!.returnIntegerValue(memberList.get(0).disability_details), dataspin_disabletype)
                            tv_set_guardian_name.text = validate!!.returnStringValue(memberList.get(0).guardian_name)
                            tv_set_relaion_guardian.text = validate!!.returnlookupcodevalue(validate!!.returnIntegerValue(memberList.get(0).guardian_relation.toString()), dataspin_guardianRelation)
                            tv_set_joiningDate.text = validate!!.convertDatetime(validate!!.returnLongValue(memberList.get(0).joining_date.toString()))
                            tv_set_post_designation.text = validate!!.returnlookupcodevalue(validate!!.returnIntegerValue(memberList.get(0).designation.toString()), dataspin_post)
                            tv_set_social_category.text = validate!!.returnlookupcodevalue(validate!!.returnIntegerValue(memberList.get(0).social_category.toString()), dataspin_category)
                            tv_set_religion.text = validate!!.returnlookupcodevalue(validate!!.returnIntegerValue(memberList.get(0).religion.toString()), dataspin_religion)
                            tv_set_education.text = validate!!.returnlookupcodevalue(validate!!.returnIntegerValue(memberList.get(0).highest_education_level.toString()), dataspin_education)
                            tv_set_primary_occupation.text = validate!!.returnlookupcodevalue(validate!!.returnIntegerValue(memberList.get(0).primary_occupation.toString()), dataspin_name)
                            tv_set_secondary_occupation.text = validate!!.returnlookupcodevalue(validate!!.returnIntegerValue(memberList.get(0).secondary_occupation.toString()), dataspin_name)
                            tv_set_tertiaryoccupation.text = validate!!.returnlookupcodevalue(validate!!.returnIntegerValue(memberList.get(0).tertiary_occupation.toString()), dataspin_name)
                            tv_set_status.text = validate!!.returnlookupcodevalue(validate!!.returnIntegerValue(memberList.get(0).status.toString()), dataspin_status)

                        }
                    }

                })

            phoneViewmodel!!.getphoneData(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))!!
                .observe(this,object : Observer<List<MemberPhoneDetailEntity>> {
                    override fun onChanged(member_list: List<MemberPhoneDetailEntity>?) {
                        if (!member_list.isNullOrEmpty() && member_list.size > 0 ){
                            tv_set_mobile_no.text = validate!!.returnStringValue(member_list.get(0).phone_no)
                            tv_set_mobile_belongs_to.text = getLookupValue(validate
                            !!.returnIntegerValue(member_list.get(0).phone_ownership))
                        }
                    }
                })

            addressViewModel!!.getAddressdata(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))!!
                .observe(this,object : Observer<List<member_addressEntity>> {
                    override fun onChanged(address_list: List<member_addressEntity>?) {
                        if (!address_list.isNullOrEmpty() && address_list.size > 0 ){
                            tv_set_address_type.text = getAddressLookupValue(validate!!.returnIntegerValue(address_list.get(0).address_type.toString()))
                            tv_set_address_1.text = address_list.get(0).address_line1
                            tv_set_address_2.text = address_list.get(0).address_line2
                            tv_set_pincode.text = validate!!.returnStringValue(address_list.get(0).postal_code.toString())
                            tv_set_village_town.text = getVillageName(validate!!.returnIntegerValue(address_list.get(0).village_id.toString()))
                            tv_set_grampanchayat.text = getPanchayatName(validate!!.returnIntegerValue(address_list.get(0).panchayat_id.toString()))
                        }
                    }
                })

           memberbankViewmodel!!.getBankdetaildata(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))!!
                .observe(this,object : Observer<List<MemberBankAccountEntity>> {
                    override fun onChanged(bank_list: List<MemberBankAccountEntity>?) {
                        if (!bank_list.isNullOrEmpty() && bank_list.size > 0 ){
                            var dataspin_yesno: List<LookupEntity>? = null
                            dataspin_yesno = lookupViewmodel!!.getlookup(9, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
                            tv_set_is_default_bank.text = validate!!.returnlookupcodevalue(validate!!.returnIntegerValue(bank_list.get(0).is_default_account.toString()), dataspin_yesno)
                            tv_set_nameinbankpassbook.text = bank_list.get(0).bank_passbook_name
                            tv_set_ifsc_code.text = bank_list.get(0).ifsc_code
                            tv_set_account_no.text = bank_list.get(0).account_no
                            tv_set_account_opening_date.text = validate!!.returnStringValue(bank_list.get(0).account_open_date.toString())
                            val mediaStorageDirectory = File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
                                AppSP.IMAGE_DIRECTORY_NAME)
                            var mediaFile1 = File(mediaStorageDirectory.path + File.separator + validate!!.returnStringValue(
                                bank_list.get(0).passbook_firstpage))
                            Picasso.with(this@ShgMemberSummery).load(mediaFile1).into(iv_set_upload_passbook_first_page)
                            tv_set_bank_name.text = bankMasterViewModel!!.getBankName(validate!!.returnIntegerValue(bank_list.get(0).bank_id)).toString()
                            tv_set_bank_branch.text = masterBankBranchViewModel!!.getBranchname(validate!!.returnIntegerValue(bank_list.get(0).mem_branch_code))

                        }
                    }
                })

            memberSystemtagViewmodel!!.getSystemtagdata(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))!!
                .observe(this, object : Observer<List<MemberSystemTagEntity>> {
                    override fun onChanged(system_list: List<MemberSystemTagEntity>?) {
                        if (!system_list.isNullOrEmpty() && system_list.size > 0) {
                            tv_set_id.text = validate!!.returnStringValue(system_list.get(0).system_id.toString())

                            var dataspin_system: List<LookupEntity>? = null
                            dataspin_system = lookupViewmodel!!.getlookup(43, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
                            tv_set_other_id_type.text = validate!!.returnlookupcodevalue(
                                validate!!.returnIntegerValue(system_list.get(0).system_type.toString()),
                                dataspin_system
                            )
                        }
                    }
                })

            memberkycViewmodel!!.getKycdetaildata(validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID))!!.observe(this,object: Observer<List<Member_KYC_Entity>>{
                override fun onChanged(list: List<Member_KYC_Entity>?) {
                    if (!list.isNullOrEmpty() && list.size > 0){
                        var dataspin_doctype: List<LookupEntity>? = null
                        dataspin_doctype = lookupViewmodel!!.getlookup(35, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
                        tv_set_kyc_id_type.text =
                            validate!!.returnlookupcodevalue(validate!!.returnIntegerValue(list.get(0).kyc_type.toString()), dataspin_doctype)
                        tv_set_kyc_id.text = validate!!.returnStringValue(list.get(0).kyc_number)
                        val mediaStorageDirectory = File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
                            AppSP.IMAGE_DIRECTORY_NAME)
                        var mediaFile = File(mediaStorageDirectory.path + File.separator + validate!!.returnStringValue(
                            list.get(0).kyc_front_doc_orig_name))
                        var mediaFile1 = File(mediaStorageDirectory.path + File.separator + validate!!.returnStringValue(
                            list.get(0).kyc_rear_doc_orig_name))
                        Picasso.with(this@ShgMemberSummery).load(mediaFile).into(iv_set_frontimage)
                        Picasso.with(this@ShgMemberSummery).load(mediaFile1).into(iv_set_rear_attachment)

                    }
                }
            })

        } catch (ex: Exception) {
            ex.printStackTrace()
//            Log.d("Excetion value:", ex.toString())
        }
    }

    fun getBankName(bankID : Int?): String?{
        var value: String? = null
        value = bankMasterViewModel!!.getBankName(bankID!!)
        return value
    }

    fun getVillageName(villageId: Int?): String? {
        var name:String? = null
        name = locationViewmodel!!.getVillageName(villageId!!)
        return name
    }

    fun getPanchayatName(paynchyatID: Int?): String? {
        var name:String? = null
        name = locationViewmodel!!.getPanchayatName(paynchyatID!!)
        return name
    }

    fun getLookupValue(id:Int):String{
        var dataspin_memberbelong = lookupViewmodel!!.getlookupfromkeycode("RELATION_MB",validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
        var sValue = ""
        sValue = validate!!.returnlookupcodevalue(id,dataspin_memberbelong)
        return sValue
    }

    fun getAddressLookupValue(id:Int):String{
        var    dataspin_addresstype =lookupViewmodel!!.getlookup(20,validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
        var sValue = ""
        sValue = validate!!.returnlookupcodevalue(id,dataspin_addresstype)
        return sValue
    }

}