package com.microware.cdfi.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.microware.cdfi.R
import kotlinx.android.synthetic.main.shgsearchbar.*
import kotlinx.android.synthetic.main.white_toolbar.*

class ShgProfileListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shg_profilelist)

        ivHome.setOnClickListener {
            searchbar.visibility = View.VISIBLE
            val down: Animation = AnimationUtils.loadAnimation(this, R.anim.slide_down)
            down.reset()
            searchbar.animation = down
            searchbar.isFocusable = true
        }

        Ivscroll.setOnClickListener {
            searchbar.visibility = View.GONE
            val up: Animation = AnimationUtils.loadAnimation(this, R.anim.slide_up)
            up.reset()
            searchbar.animation = up
            searchbar.isFocusable = true
        }

    }
}
