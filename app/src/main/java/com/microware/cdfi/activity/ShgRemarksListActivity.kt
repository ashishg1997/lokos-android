package com.microware.cdfi.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.ShgRejectionRemarksAdapter
import com.microware.cdfi.entity.SHGEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.layout_rjection_remarks_list.*
import kotlinx.android.synthetic.main.white_toolbar.*

class ShgRemarksListActivity : AppCompatActivity() {

    var shgviewmodel: SHGViewmodel? = null

    var validate: Validate? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_rjection_remarks_list)

        shgviewmodel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)

        validate = Validate(this)

        ivHome.visibility = View.GONE
        tv_title.text =  LabelSet.getText(
            "shg_rejection_remarks",
            R.string.shg_rejection_remarks
        )


        ivBack.setOnClickListener {
            var intent = Intent(this, ShgListActivity::class.java)
            intent.flags= Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }


        fillData()
    }

    override fun onBackPressed() {
        var intent = Intent(this, ShgListActivity::class.java)
        intent.flags= Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    private fun fillData() {
        shgviewmodel!!.getSHGlistdata(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))!!
            .observe(this,object : Observer<List<SHGEntity>> {
                override fun onChanged(shglist: List<SHGEntity>?) {
                    if (!shglist.isNullOrEmpty() && shglist.size > 0 ){
                        rvList.layoutManager = LinearLayoutManager(this@ShgRemarksListActivity)
                        rvList.adapter = ShgRejectionRemarksAdapter(this@ShgRemarksListActivity, shglist,shgviewmodel!!)
                    }
                }
            })
    }

}