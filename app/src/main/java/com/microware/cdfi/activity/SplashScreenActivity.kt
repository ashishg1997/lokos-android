package com.microware.cdfi.activity

import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.Window
import android.view.WindowManager
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.clf.activites.CLFDrawerActivity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.UserViewmodel
import kotlinx.android.synthetic.main.activity_splash_screen.*

class SplashScreenActivity : AppCompatActivity() {
    var validate: Validate? = null
    var userViewmodel: UserViewmodel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_splash_screen)
        validate = Validate(this)
        CDFIApplication.changedb()
        userViewmodel = ViewModelProviders.of(this).get(UserViewmodel::class.java)

//         Handler().postDelayed({
//            var list = userViewmodel!!.getUserData()
//            if (!list.isNullOrEmpty()) {
//                var intent = Intent(this, PinActivity::class.java)
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
//                startActivity(intent)
//                finish()
//            } else {
//                var intent = Intent(this, LanguageActivity::class.java)
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
//                startActivity(intent)
//                finish()
//            }
//        },4000)

        Handler().postDelayed({
//            var list = userViewmodel!!.getUserData()
//            if (!list.isNullOrEmpty()) {
//                var intent = Intent(this, PinActivity::class.java)
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
//                startActivity(intent)
//                finish()
//            } else {
                var intent = Intent(this, CLFDrawerActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                finish()
//            }
        }, 4000)

        // tv_version.setText(getVersion())

    }

    fun getVersion(): String {
        var mVersionNumber: String
        val mContext = applicationContext
        try {
            val pkg = mContext.packageName
            mVersionNumber = mContext.packageManager
                .getPackageInfo(pkg, 0).versionName
        } catch (e: PackageManager.NameNotFoundException) {
            mVersionNumber = "?"
        }

        return getString(R.string.version) + ":" + mVersionNumber
    }
}