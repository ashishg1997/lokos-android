package com.microware.cdfi.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.GroupSystemtagAdapter
import com.microware.cdfi.entity.*
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_group_system_tag_list.*
import kotlinx.android.synthetic.main.activity_summary.*
import kotlinx.android.synthetic.main.fragment_bank_detail_list.*
import kotlinx.android.synthetic.main.fragment_kyc_detail_list.*
import kotlinx.android.synthetic.main.fragment_phonedetaillist.*
import kotlinx.android.synthetic.main.summary_toolbar.*
import java.io.File

class SummaryActivity : AppCompatActivity() {
    var shgViewModel: SHGViewmodel? = null
    var lookupViewmodel: LookupViewmodel? = null
    var cboPhoneViewmodel: CboPhoneViewmodel? = null
    var addressViewmodel: CboAddressViewmodel? = null
    var locationViewmodel: LocationViewModel? = null
    var systemtagViewmodel: SystemtagViewmodel? = null
    var validate: Validate? = null
    var dataspin_doctype: List<LookupEntity>? = null
    var cboBankViewmodel: CboBankViewmodel? = null
    var bankMasterViewModel : MasterBankViewmodel? = null
    var masterBankBranchViewModel : MasterBankBranchViewModel? = null
    var memberviewmodel: Memberviewmodel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_summary)
        shgViewModel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        cboPhoneViewmodel = ViewModelProviders.of(this).get(CboPhoneViewmodel::class.java)
        addressViewmodel = ViewModelProviders.of(this).get(CboAddressViewmodel::class.java)
        locationViewmodel = ViewModelProviders.of(this).get(LocationViewModel::class.java)
        systemtagViewmodel = ViewModelProviders.of(this).get(SystemtagViewmodel::class.java)
        cboBankViewmodel = ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        bankMasterViewModel = ViewModelProviders.of(this).get(MasterBankViewmodel::class.java)
        masterBankBranchViewModel = ViewModelProviders.of(this).get(MasterBankBranchViewModel::class.java)
        memberviewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)
        validate = Validate(this)
        icBack.setOnClickListener {
            var intent = Intent(this, ShgListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

        tv_title.text =
            validate!!.RetriveSharepreferenceString(AppSP.ShgName)!! + "(" + validate!!.RetriveSharepreferenceString(
                AppSP.Shgcode
            ) + ")"

        liSummary.visibility = View.VISIBLE
        ivUpSummary.visibility = View.VISIBLE
        ivDownSummary.visibility = View.GONE

        ivDownSummary.setOnClickListener {
            liSummary.visibility = View.VISIBLE
            ivUpSummary.visibility = View.VISIBLE
            ivDownSummary.visibility = View.GONE
        }
        ivUpSummary.setOnClickListener {
            liSummary.visibility = View.GONE
            ivUpSummary.visibility = View.GONE
            ivDownSummary.visibility = View.VISIBLE
        }
        ivDownBasicDetail.setOnClickListener {
            liBasicDetail.visibility = View.VISIBLE
            ivUpBasicDetail.visibility = View.VISIBLE
            ivDownBasicDetail.visibility = View.GONE
        }
        ivUpBasicDetail.setOnClickListener {
            liBasicDetail.visibility = View.GONE
            ivUpBasicDetail.visibility = View.GONE
            ivDownBasicDetail.visibility = View.VISIBLE
        }
        ivDownMobileDetail.setOnClickListener {
            liMobileDetail.visibility = View.VISIBLE
            ivUpMobileDetail.visibility = View.VISIBLE
            ivDownMobileDetail.visibility = View.GONE
        }
        ivUpMobileDetail.setOnClickListener {
            liMobileDetail.visibility = View.GONE
            ivUpMobileDetail.visibility = View.GONE
            ivDownMobileDetail.visibility = View.VISIBLE
        }
        ivDownAddressDetail.setOnClickListener {
            liAddressDetail.visibility = View.VISIBLE
            ivUpAddressDetail.visibility = View.VISIBLE
            ivDownAddressDetail.visibility = View.GONE
        }
        ivUpAddressDetail.setOnClickListener {
            liAddressDetail.visibility = View.GONE
            ivUpAddressDetail.visibility = View.GONE
            ivDownAddressDetail.visibility = View.VISIBLE
        }
        ivDownBankDetails.setOnClickListener {
            liBankDetails.visibility = View.VISIBLE
            ivUpBankDetails.visibility = View.VISIBLE
            ivDownBankDetails.visibility = View.GONE
        }
        ivUpBankDetails.setOnClickListener {
            liBankDetails.visibility = View.GONE
            ivUpBankDetails.visibility = View.GONE
            ivDownBankDetails.visibility = View.VISIBLE
        }
        ivDownOtherID.setOnClickListener {
            liOtherID.visibility = View.VISIBLE
            ivUpOtherID.visibility = View.VISIBLE
            ivDownOtherID.visibility = View.GONE
        }
        ivUpOtherID.setOnClickListener {
            liOtherID.visibility = View.GONE
            ivUpOtherID.visibility = View.GONE
            ivDownOtherID.visibility = View.VISIBLE
        }
        showData()
    }

    private fun showData() {
        try {
            val list = shgViewModel!!.getSHG(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
            if (!list.isNullOrEmpty()) {

                tv_set_shg_formed.text = validate!!.convertDatetime(list.get(0).shg_formation_date!!)
                tv_set_formation_date.text = validate!!.convertDatetime(list.get(0).shg_formation_date!!)
                tv_set_shg_name.text = validate!!.returnStringValue(list.get(0).shg_name!!)
                var dataspin_pramotedby: List<LookupEntity>? = null
                var datashgtype: List<LookupEntity>? = null
                var dataspin_meetingfreq: List<LookupEntity>? = null
                var dataspin_activity: List<LookupEntity>? = null
                var dataspin_name: List<LookupEntity>? = null
                dataspin_pramotedby = lookupViewmodel!!.getlookup(
                    28,
                    validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
                )
                datashgtype = lookupViewmodel!!.getlookup(
                    31,
                    validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
                )
                dataspin_meetingfreq = lookupViewmodel!!.getlookup(
                    19,
                    validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
                )
                dataspin_activity = lookupViewmodel!!.getlookup(
                    47,
                    validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
                )
                dataspin_name = lookupViewmodel!!.getlookup(
                    1,
                    validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
                )
                tv_set_promoted_by.text = validate!!.returnlookupcodevalue(
                    validate!!.returnIntegerValue(list.get(0).shg_promoted_by.toString()),
                    dataspin_pramotedby
                )
                tv_set_shg_type.text = validate!!.returnlookupcodevalue(
                    validate!!.returnIntegerValue(list.get(0).shg_type_code.toString()),
                    datashgtype
                )
                tv_set_meeting_frequency.text = validate!!.returnlookupcodevalue(
                    validate!!.returnIntegerValue(list.get(0).meeting_frequency.toString()),
                    dataspin_meetingfreq
                )
                tv_set_saving_frequency.text = validate!!.returnlookupcodevalue(
                    validate!!.returnIntegerValue(list.get(0).saving_frequency.toString()),
                    dataspin_meetingfreq
                )

                tv_set_compulsory_saving_amount.text = validate!!.returnStringValue(list.get(0).month_comp_saving.toString())
                tv_set_compulsory_saving_roi.text = validate!!.returnStringValue(list.get(0).savings_interest.toString())
                tv_set_voluntary_roi.text = validate!!.returnStringValue(list.get(0).voluntary_savings_interest.toString())
                tv_set_primary_activity.text = validate!!.returnlookupcodevalue(
                    validate!!.returnIntegerValue(list.get(0).primary_activity.toString()),
                    dataspin_activity
                )
                tv_set_secondary_activity.text = validate!!.returnlookupcodevalue(
                    validate!!.returnIntegerValue(list.get(0).secondary_activity.toString()),
                    dataspin_activity
                )
                tv_set_tertiaryactivity.text = validate!!.returnlookupcodevalue(
                    validate!!.returnIntegerValue(list.get(0).tertiary_activity.toString()),
                    dataspin_activity
                )

            }

            cboPhoneViewmodel!!.getphonedata(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))!!
                .observe(this,object : Observer<List<Cbo_phoneEntity>> {
                    override fun onChanged(cbo_list: List<Cbo_phoneEntity>?) {
                        if (!cbo_list.isNullOrEmpty()){
                            var member_Name = returnMemberName(cbo_list.get(0).member_guid)
                            if (!member_Name.isNullOrEmpty()) {
                                tv_set_mobile_belongs_to.text = member_Name
                            }else{
                                tv_set_mobile_belongs_to.text = member_Name
                            }

                            tv_set_mobile_no.text = validate!!.returnStringValue(cbo_list.get(0).mobile_no)

                        }
                    }
                })

            addressViewmodel!!.getAddressdata(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))!!
                    .observe(this, object : Observer<List<AddressEntity>> {
                        override fun onChanged(t: List<AddressEntity>?) {
                            if (!t.isNullOrEmpty()) {
                                tv_set_address_1.text = t.get(0).address_line1
                                tv_set_address_2.text = t.get(0).address_line2
                                tv_set_pincode.text = validate!!.returnStringValue(t.get(0).postal_code.toString())
                                var villageName =
                                    locationViewmodel!!.getVillageName(validate!!.returnIntegerValue(t.get(0).village_id.toString()))
                                if (!villageName.isNullOrEmpty()) {
                                    tv_set_village_town.text = villageName
                                } else {
                                    tv_set_village_town.text = ""
                                }

                                var panchayatname =
                                    locationViewmodel!!.getPanchayatName(validate!!.returnIntegerValue(t.get(0).panchayat_id.toString()))
                                if (!panchayatname.isNullOrEmpty()) {
                                    tv_set_grampanchayat.text = panchayatname
                                } else {
                                    tv_set_grampanchayat.text = ""
                                }

                            }
                        }

                    })

            cboBankViewmodel!!.getBankdata(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))!!
                .observe(this,object : Observer<List<Cbo_bankEntity>> {
                    override fun onChanged(bank_list: List<Cbo_bankEntity>?) {
                        var dataspin_yesno: List<LookupEntity>? = null
                        if (!bank_list.isNullOrEmpty() && bank_list.size > 0 ){
                            dataspin_yesno = lookupViewmodel!!.getlookup(
                                9,
                                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
                            )
                            tv_set_is_default_bank.text = validate!!.returnlookupcodevalue(
                                validate!!.returnIntegerValue(bank_list.get(0).is_default.toString()),
                                dataspin_yesno
                            )
                            tv_set_nameinbankpassbook.text = bank_list.get(0).bankpassbook_name
                            tv_set_ifsc_code.text = bank_list.get(0).ifsc_code
                            tv_set_account_no.text = bank_list.get(0).account_no
                            tv_set_account_opening_date.text = validate!!.returnStringValue(bank_list.get(0).account_opening_date.toString())
                            val mediaStorageDirectory = File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
                                AppSP.IMAGE_DIRECTORY_NAME)

                            var mediaFile1 = File(mediaStorageDirectory.path + File.separator + validate!!.returnStringValue(
                                bank_list.get(0).passbook_firstpage))
                            Picasso.with(this@SummaryActivity).load(mediaFile1).into(iv_set_upload_passbook_first_page)
                           var bankname = bankMasterViewModel!!.getBankName(validate!!.returnIntegerValue(bank_list.get(0).bank_id.toString()))
                            if (!bankname.isNullOrEmpty()){
                                tv_set_bank_name.text = bankname
                            }else{
                                tv_set_bank_name.text = ""
                            }

                            var bankbranchname = masterBankBranchViewModel!!.getBranchname(validate!!.returnIntegerValue(bank_list.get(0).bank_branch_id.toString()))
                            if (!bankbranchname.isNullOrEmpty()){
                                tv_set_bank_branch.text = bankbranchname
                            }else{
                                tv_set_bank_branch.text = ""
                            }

                        }
                    }
                })

            systemtagViewmodel!!.getSystemtagdata(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))!!
                .observe(this, object : Observer<List<SystemTagEntity>> {
                    override fun onChanged(system_list: List<SystemTagEntity>?) {
                        if (!system_list.isNullOrEmpty() && system_list.size > 0) {
                            tv_set_id.text = validate!!.returnStringValue(system_list.get(0).system_id.toString())

                            var dataspin_system: List<LookupEntity>? = null
                            dataspin_system = lookupViewmodel!!.getlookup(43, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
                            tv_set_other_id_type.text = validate!!.returnlookupcodevalue(
                                validate!!.returnIntegerValue(system_list.get(0).system_type.toString()),
                                dataspin_system
                            )

                        }
                    }
                })

        } catch (ex: Exception) {
            ex.printStackTrace()
//            Log.d("Excetion value:", ex.toString())
        }
    }

    fun returnMemberName(memberGuid:String): String {
        var dataspin_belong = memberviewmodel!!.getAllMemberlist(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID))
        var sValue = ""

        if (!dataspin_belong.isNullOrEmpty()) {
            for (i in dataspin_belong.indices) {
                if (memberGuid.equals(dataspin_belong.get(i).member_guid))
                    sValue = dataspin_belong.get(i).member_name!!
            }
        }
        return sValue
    }


    override fun onBackPressed() {
        var intent = Intent(this, ShgListActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }
}