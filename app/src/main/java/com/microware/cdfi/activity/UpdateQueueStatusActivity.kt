package com.microware.cdfi.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.UpdateQueueStatusAdapter
import com.microware.cdfi.api.model.QueuestatusModel
import com.microware.cdfi.entity.TransactionEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.Memberviewmodel
import com.microware.cdfi.viewModel.SHGViewmodel
import com.microware.cdfi.viewModel.TransactionViewmodel
import kotlinx.android.synthetic.main.layout_rjection_remarks_list.*
import kotlinx.android.synthetic.main.white_toolbar.*

class UpdateQueueStatusActivity : AppCompatActivity() {
    var validate:Validate? = null
    var transactionViewModel : TransactionViewmodel? = null
    var shgviewmodel: SHGViewmodel? = null
    var memberViewmodel: Memberviewmodel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_queue_status)

        validate = Validate(this)
        transactionViewModel = ViewModelProviders.of(this).get(TransactionViewmodel::class.java)
        shgviewmodel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        memberViewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)

        ivHome.visibility = View.GONE
        tv_title.text =  LabelSet.getText(
            "update_queue_status",
            R.string.update_queue_status
        )

        ivBack.setOnClickListener {
            val intent = Intent(this, ShgListActivity::class.java)
            intent.flags= Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }

        fillData()
    }

    private fun fillData() {
        transactionViewModel!!.getUpdateQueueList(validate!!.RetriveSharepreferenceString(AppSP.SHGGUID)!!)
            .observe(this,object : Observer<List<TransactionEntity>> {
                override fun onChanged(list: List<TransactionEntity>?) {
                    if (!list.isNullOrEmpty() && list.size > 0 ){
                        rvList.layoutManager = LinearLayoutManager(this@UpdateQueueStatusActivity)
                        rvList.adapter = UpdateQueueStatusAdapter(this@UpdateQueueStatusActivity, list)
                    }
                }
            })
    }

     fun getshgName(shgGuid:String):String{
        return shgviewmodel!!.getSHGname(shgGuid)
    }

    fun getmemberList(shgGuid:String):List<QueuestatusModel>{
        return memberViewmodel!!.getmemberList(shgGuid)
    }

    override fun onBackPressed() {
        val intent = Intent(this, ShgListActivity::class.java)
        intent.flags= Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

}

