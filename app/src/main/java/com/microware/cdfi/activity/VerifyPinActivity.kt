package com.microware.cdfi.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import com.microware.cdfi.R
import com.microware.cdfi.activity.vo.VoDrawerActivity
import com.microware.cdfi.utility.AESEncryption
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.mukesh.OnOtpCompletionListener
import kotlinx.android.synthetic.main.verifypin_screen.*

class VerifyPinActivity : AppCompatActivity() {
    var validate: Validate? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.requestWindowFeature(Window.FEATURE_NO_TITLE)
//Remove notification bar
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.verifypin_screen)
        validate = Validate(this)
        setLabelText()

        insert_your.text =
            LabelSet.getText(
                "insert_your",
                R.string.insert_your
            ) +  " " + validate!!.RetriveSharepreferenceString(AppSP.userid) + " (" + validate!!.RetriveSharepreferenceString(AppSP.RoleName)+ ") "


        confirm_otp.setOtpCompletionListener(OnOtpCompletionListener {
            if (it.equals(enter_otp.text.toString())) {

                if (validate!!.returnRoleValue(validate!!.RetriveSharepreferenceString(AppSP.Roleid)) == 310) {
                    validate!!.SaveSharepreferenceString(
                        AppSP.Pin,
                        validate!!.returnStringValue(
                            AESEncryption.encrypt(
                                it!!,validate!!.RetriveSharepreferenceString(AppSP.userid)))
                    )
                    val startMain = Intent(this, MainActivityDrawer::class.java)
                    startMain.flags =
                        Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(startMain)
                } else if (validate!!.returnRoleValue(validate!!.RetriveSharepreferenceString(AppSP.Roleid)) == 410) {
                    validate!!.SaveSharepreferenceInt(AppSP.FormRoleType, 1)
                    validate!!.SaveSharepreferenceString(
                        AppSP.VoPin,
                        validate!!.returnStringValue(
                            AESEncryption.encrypt(
                                it!!,validate!!.RetriveSharepreferenceString(AppSP.userid)))
                    )
                    val startMain = Intent(this, VoDrawerActivity::class.java)
                    startMain.flags =
                        Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(startMain)
                } else if (validate!!.returnRoleValue(validate!!.RetriveSharepreferenceString(AppSP.Roleid)) == 450) {
                    validate!!.SaveSharepreferenceString(
                        AppSP.VoappPin,
                        validate!!.returnStringValue(
                            AESEncryption.encrypt(
                                it!!,validate!!.RetriveSharepreferenceString(AppSP.userid)))
                    )
                    val startMain = Intent(this, VoDrawerActivity::class.java)
                    startMain.flags =
                        Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(startMain)
                } else if (validate!!.returnRoleValue(validate!!.RetriveSharepreferenceString(AppSP.Roleid)) == 510) {

                    validate!!.SaveSharepreferenceInt(AppSP.FormRoleType, 2)

                    validate!!.SaveSharepreferenceString(AppSP.CLfPin, it!!)
                    validate!!.SaveSharepreferenceString(
                        AppSP.CLfPin,
                        validate!!.returnStringValue(
                            AESEncryption.encrypt(
                                it,validate!!.RetriveSharepreferenceString(AppSP.userid)))
                    )
                    val startMain = Intent(this, VoDrawerActivity::class.java)
                    startMain.flags =
                        Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(startMain)
                }
            } else {
                Toast.makeText(this, "Pin does not match", Toast.LENGTH_SHORT).show()

            }
        })
        enter_otp.setOtpCompletionListener(OnOtpCompletionListener {
            confirm_otp.requestFocus()
        })

        /* btn_save.setOnClickListener {
             val startMain = Intent(this, ShgListActivity::class.java)
             startMain.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
             startActivity(startMain)
         }*/
        // setLabelText()
    }

    fun setLabelText() {
        lokos_core.text = LabelSet.getText(
            "enter_user_pin2",
            R.string.enter_user_pin2
        )
        insert_your.text = LabelSet.getText(
            "insert_your",
            R.string.insert_your
        ) + " " + validate!!.RetriveSharepreferenceString(AppSP.userid)
        tv_pinquotes.text = LabelSet.getText(
            "enter_pin",
            R.string.enter_pin
        )
        tv_cnfrmPin.text = LabelSet.getText(
            "confirm_pin",
            R.string.confirm_pin
        )

    }
    /* override fun onOtpCompleted(otp: String?) {
         // Toast.makeText(this, "OnOtpCompletionListener called$otp", Toast.LENGTH_SHORT).show();

     }
 */

}
