package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.*
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_adjustment.*
import kotlinx.android.synthetic.main.activity_adjustment.et_amount
import kotlinx.android.synthetic.main.activity_adjustment.tv_amount
import kotlinx.android.synthetic.main.buttons.btn_cancel
import kotlinx.android.synthetic.main.buttons.btn_save

class AdjustmentActivity : AppCompatActivity() {
    var coaviewmodel: MstCOAViewmodel? = null
    var generateMeetingViewmodel: GenerateMeetingViewmodel? = null
    var financialTransactionsMemViewmodel: FinancialTransactionsMemViewmodel? = null
    var validate: Validate? = null
    var membersaving: List<MstCOAEntity>? = null
    var memberlistdebited: List<DtmtgDetEntity>? = null
    var memberlistcredited: List<DtmtgDetEntity>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_adjustment)
        validate = Validate(this)
        setLabelText()
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        coaviewmodel = ViewModelProviders.of(this).get(MstCOAViewmodel::class.java)
        financialTransactionsMemViewmodel =
            ViewModelProviders.of(this).get(FinancialTransactionsMemViewmodel::class.java)

        fillmemberspinnerdebited()
        fillspinnersaving()
        replaceFragmenty(
            fragment = MeetingTopBarFragment(95),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )
        btn_cancel.setOnClickListener {
            var intent = Intent(this, MemberMeetingSummaryActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                if (returnmemsavingid() == 6) {
                    SaveSharecapital("OR",6)
                } else if (returnmemsavingid() == 68) {
                    SavecompData()
                    SaveSharecapital("OR",68)

                } else if (returnmemsavingid() == 69) {
                    SavevolData()
                    SaveSharecapital("OR",69)

                }
            }
        }
        spin_member_saving_debited?.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {

                override fun onItemSelected(
                    parent: AdapterView<*>, view: View?,
                    position: Int, id: Long
                ) {

                    if (position > 0) {
                        fillmemberspinnercredited(returnmemdebitedid())
                    }

                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // TODO Auto-generated method stub

                }
            }


    }

    fun SavecompData() {
        var amt = validate!!.returnIntegerValue(et_amount.text.toString())
        var debitamt = amt * -1
        generateMeetingViewmodel!!.updateadjustwithdrawl(
            amt,
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            returnmemcreditedid(),
            validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
            validate!!.Daybetweentime(validate!!.currentdatetime),
            validate!!.returnIntegerValue(et_referance_meeting_no.text.toString())
        )
        generateMeetingViewmodel!!.updateadjustwithdrawl(
            debitamt,
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            returnmemdebitedid(),
            validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
            validate!!.Daybetweentime(validate!!.currentdatetime),validate!!.returnIntegerValue(et_referance_meeting_no.text.toString())
        )
       /* validate!!.CustomAlert(
            LabelSet.getText(
                "data_saved_successfully",
                R.string.data_saved_successfully
            ),
            this, MemberMeetingSummaryActivity::class.java
        )*/
    }

    fun SavevolData() {
        var amt = validate!!.returnIntegerValue(et_amount.text.toString())
        var debitamt = amt * -1
        generateMeetingViewmodel!!.updateadjustVoluntory(
            amt,
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            returnmemcreditedid(),
            validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
            validate!!.Daybetweentime(validate!!.currentdatetime),
            validate!!.returnIntegerValue(et_referance_meeting_no.text.toString())
        )
        generateMeetingViewmodel!!.updateadjustVoluntory(
            debitamt,
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            returnmemdebitedid(),
            validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
            validate!!.Daybetweentime(validate!!.currentdatetime),validate!!.returnIntegerValue(et_referance_meeting_no.text.toString())
        )
        /*validate!!.CustomAlert(
            LabelSet.getText(
                "data_saved_successfully",
                R.string.data_saved_successfully
            ),
            this, MemberMeetingSummaryActivity::class.java
        )*/
    }

    fun SaveSharecapital(type:String,auid:Int) {
        var amt = validate!!.returnIntegerValue(et_amount.text.toString())
        var debitamt = amt * -1
        var financialTransactionsMemEntity = FinancialTransactionsMemEntity(
            0,
            validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            returnmemcreditedid(),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            "",
            auid,/*auid*/
            type,
            amt,
            validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            0,
            1,
            "",
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            null,
            null,
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            0,validate!!.returnIntegerValue(et_referance_meeting_no.text.toString())
        )
        financialTransactionsMemViewmodel!!.insert(financialTransactionsMemEntity)
        var financialTransactionsMemEntitydebit = FinancialTransactionsMemEntity(
            0,
            validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            returnmemdebitedid(),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            "",
            auid,/*auid*/
            type,
            debitamt,
            validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            0,
            1,
            "",
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            null,
            null,
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            0,validate!!.returnIntegerValue(et_referance_meeting_no.text.toString())
        )
        financialTransactionsMemViewmodel!!.insert(financialTransactionsMemEntitydebit)
        validate!!.CustomAlert(
            LabelSet.getText(
                "data_saved_successfully",
                R.string.data_saved_successfully
            ),
            this, MemberMeetingSummaryActivity::class.java
        )
    }


    private fun checkValidation(): Int {
        var value = 1
        if (spin_saving.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_saving,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "saving",
                    R.string.saving
                )
            )
            value = 0
        } else if (validate!!.returnIntegerValue(et_referance_meeting_no.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "referance_meeting_no",
                    R.string.referance_meeting_no
                ),
                this, et_referance_meeting_no
            )
            value = 0
        } else if (spin_member_saving_debited.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_member_saving_debited,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "member_saving_debited",
                    R.string.member_saving_debited
                )
            )
            value = 0
        } else if (spin_member_saving_credited.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_member_saving_credited,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "member_saving_credited",
                    R.string.member_saving_credited
                )
            )
            value = 0
        } else if (validate!!.returnIntegerValue(et_amount.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "amount",
                    R.string.amount
                ),
                this, et_amount
            )
            value = 0
        } else if (returnmemsavingid() == 68 && validate!!.returnIntegerValue(et_amount.text.toString()) > returnmemcompamt()) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "amount",
                    R.string.amount
                ),
                this, et_amount
            )
            value = 0
        }else if (returnmemsavingid() == 69 && validate!!.returnIntegerValue(et_amount.text.toString()) > returnmemvolamt()) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "amount",
                    R.string.amount
                ),
                this, et_amount
            )
            value = 0
        }
        return value
    }


    fun setLabelText() {
       // tv_title.setText(LabelSet.getText("adjustment", R.string.adjustment))
        tv_saving.text = LabelSet.getText(
            "saving",
            R.string.saving
        )
        tv_referance_meeting_no.text = LabelSet.getText(
            "referance_meeting_no",
            R.string.referance_meeting_no
        )


        tv_member_saving_debited.text = LabelSet.getText(
            "member_saving_debited",
            R.string.member_saving_debited
        )
        tv_member_saving_credited.text = LabelSet.getText(
            "member_saving_credited",
            R.string.member_saving_credited
        )
        tv_amount.text = LabelSet.getText(
            "amount",
            R.string.amount
        )

        btn_save.text = LabelSet.getText(
            "save",
            R.string.save
        )
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }


    private fun fillmemberspinnercredited(memid: Long) {
        memberlistcredited = generateMeetingViewmodel!!.getListDatawithoutmember(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), memid
        )
        val adapter: ArrayAdapter<String?>
        if (!memberlistcredited.isNullOrEmpty()) {
            val isize = memberlistcredited!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in memberlistcredited!!.indices) {
                sValue[i + 1] = memberlistcredited!![i].member_name
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_member_saving_credited.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(memberlistcredited!!.size + 1)
            sValue[0] = LabelSet.getText(
                "nomemberavailable",
                R.string.nomemberavailable
            )
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_member_saving_credited.adapter = adapter
        }
        //setMember()

    }

    private fun fillmemberspinnerdebited() {
        memberlistdebited = generateMeetingViewmodel!!.getListDatawithoutmember(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), 0
        )
        val adapter: ArrayAdapter<String?>
        if (!memberlistdebited.isNullOrEmpty()) {
            val isize = memberlistdebited!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in memberlistdebited!!.indices) {
                sValue[i + 1] = memberlistdebited!![i].member_name
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_member_saving_debited.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(memberlistdebited!!.size + 1)
            sValue[0] = LabelSet.getText(
                "nomemberavailable",
                R.string.nomemberavailable
            )
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_member_saving_debited.adapter = adapter
        }
        //setMember()

    }

    private fun fillspinnersaving() {
        membersaving = coaviewmodel!!.getCoaSubHeadlist(
            listOf(6, 68, 69),
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
        )
        val adapter: ArrayAdapter<String?>
        if (!membersaving.isNullOrEmpty()) {
            val isize = membersaving!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in membersaving!!.indices) {
                sValue[i + 1] = membersaving!![i].description
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_saving.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(membersaving!!.size + 1)
            sValue[0] = LabelSet.getText(
                "Select",
                R.string.Select
            )
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_saving.adapter = adapter
        }
        //setMember()

    }

    fun returnmemsavingid(): Int {

        var pos = spin_saving.selectedItemPosition
        var id = 0

        if (!membersaving.isNullOrEmpty()) {
            if (pos > 0) id = membersaving!!.get(pos - 1).uid
        }
        return id
    }

    fun returnmemcreditedid(): Long {

        var pos = spin_member_saving_credited.selectedItemPosition
        var id = 0L

        if (!memberlistcredited.isNullOrEmpty()) {
            if (pos > 0) id = memberlistcredited!!.get(pos - 1).mem_id
        }
        return id
    }

    fun returnmemdebitedid(): Long {

        var pos = spin_member_saving_debited.selectedItemPosition
        var id = 0L

        if (!memberlistdebited.isNullOrEmpty()) {
            if (pos > 0) id = memberlistdebited!!.get(pos - 1).mem_id
        }
        return id
    }

    fun returnmemcompamt(): Int {

        var pos = spin_member_saving_debited.selectedItemPosition
        var id = 0

        if (!memberlistdebited.isNullOrEmpty()) {
            if (pos > 0) id =
                validate!!.returnIntegerValue(memberlistdebited!!.get(pos - 1).sav_comp_cb.toString())
        }
        return id
    }

    fun returnmemvolamt(): Int {

        var pos = spin_member_saving_debited.selectedItemPosition
        var id = 0

        if (!memberlistdebited.isNullOrEmpty()) {
            if (pos > 0) id =
                validate!!.returnIntegerValue(memberlistdebited!!.get(pos - 1).sav_vol_cb.toString())
        }
        return id
    }

    /*fun setMember(): Int {
        var pos = 0
        if (!memberlist.isNullOrEmpty()) {
            for (i in memberlist!!.indices) {
                if (validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid) == memberlist!!.get(i).mem_id)
                    pos = i + 1
            }
        }
        spin_entry_member_name.setSelection(pos)
        spin_entry_member_name.isEnabled = false
        return pos
    }
*/
    override fun onBackPressed() {
        var intent = Intent(this, MemberMeetingSummaryActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

}