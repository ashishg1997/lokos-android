package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.AttendenceZeroDetailAdapter
import com.microware.cdfi.fragment.MeetingTopBarZeroFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import com.microware.cdfi.viewModel.Memberviewmodel
import kotlinx.android.synthetic.main.activity_attendence_zero.*
import kotlinx.android.synthetic.main.buttons.*

class AttendnceZeroDetailActivity : AppCompatActivity() {
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    lateinit var memberviewmodel: Memberviewmodel
    var validate: Validate? = null
    lateinit var attendenceZeroDetailAdapter: AttendenceZeroDetailAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_attendence_zero)
        validate = Validate(this)

        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)

        memberviewmodel =
            ViewModelProviders.of(this).get(Memberviewmodel::class.java)


        btn_save.setOnClickListener {
            getTotalValue()
            setValue()
        }
        btn_cancel.setOnClickListener {
            val intent = Intent(this, CutOffMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        }
        /* ic_Back.setOnClickListener {
             var intent = Intent(this, MeetingMenuActivity::class.java)
             intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
             startActivity(intent)
             overridePendingTransition(0, 0)
         }*/
        replaceFragmenty(
            fragment = MeetingTopBarZeroFragment(1),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        setLabelText()
        fillRecyclerView()
        setValue()
    }

    override fun onBackPressed() {
        val intent = Intent(this, CutOffMeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

    fun setLabelText() {
        tv_srno.text = LabelSet.getText("sr_no", R.string.sr_no)
        tv_memberName.text = LabelSet.getText(
            "member_name",
            R.string.member_name
        )
        tv_attendence.text = LabelSet.getText(
            "attendance",
            R.string.attendance
        )
        // tv_title.setText(LabelSet.getText("attendance", R.string.attendance))
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }

    private fun fillRecyclerView() {
        var list = generateMeetingViewmodel.getListDataMtgByMtgnum(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))

        attendenceZeroDetailAdapter = AttendenceZeroDetailAdapter(this,list)

        rvList.layoutManager = LinearLayoutManager(this)

        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = rvList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvList.layoutParams = params
        rvList.adapter = attendenceZeroDetailAdapter

    }



    fun getTotalValue() {
        var saveValue = 0

        val iCount = rvList.childCount


        for (i in 0 until iCount) {
            val gridChild =
                rvList.getChildAt(i) as? ViewGroup

            val et_Attendance = gridChild!!
                .findViewById<View>(R.id.et_Attendance) as? EditText

            val et_GroupMCode = gridChild
                .findViewById<View>(R.id.et_GroupMCode) as? EditText

            if (et_Attendance!!.length() > 0) {
                var iValue = et_Attendance.text.toString()
                var GroupMCode = validate!!.returnLongValue(et_GroupMCode!!.text.toString())
                saveValue=saveData(iValue,GroupMCode)

            }
        }

        if(saveValue > 0){
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this
            )
        }
    }

    fun saveData(iValue: String, group_m_code:Long):Int {
        var value=0
        generateMeetingViewmodel.updateCutOffAttendance(
            iValue,
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            group_m_code,
            validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
            validate!!.Daybetweentime(validate!!.currentdatetime)
        )
        value=1

        return value
    }
    fun getImage( group_m_code:Long?):String? {

        var value= memberviewmodel.getimage(
            group_m_code
        )

        return value
    }

    fun setValue(){
        var TotalPresent = 0

            TotalPresent = generateMeetingViewmodel.getCutOffTotalPresent(
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
            )
        tv_present_count.text = ""
        tv_absent_count.text = ""
        tv_presentCount.text = ""+TotalPresent



    }

    fun getSecondLastMeetingNum():Int{
        var allowedValue = 0
        allowedValue = validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber)
        return  allowedValue
    }
}