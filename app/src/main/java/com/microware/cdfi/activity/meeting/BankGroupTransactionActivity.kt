package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.replaceFragmenty
import com.microware.cdfi.viewModel.LookupViewmodel
import kotlinx.android.synthetic.main.activity_bank_group_transaction_list.*
import kotlinx.android.synthetic.main.activity_bank_group_transaction_list.tv_Amount
import kotlinx.android.synthetic.main.buttons.*

class BankGroupTransactionActivity : AppCompatActivity() {
    var validate: Validate? = null
    var dataspin_BankTransaction: List<LookupEntity>? = null
    var lookupViewmodel: LookupViewmodel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bank_group_transaction_list)
        validate = Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)


        replaceFragmenty(
            fragment = MeetingTopBarFragment(10),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        setLabelText()
        fillSpinner()

    }

    override fun onBackPressed() {
//        var intent = Intent(this, RepaymentActivity::class.java)
        var intent = Intent(this, MeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0,0)
        finish()
    }


    fun setLabelText() {
        tvbank_transaction.text = LabelSet.getText(
            "bank_transaction",
            R.string.bank_transaction
        )
        tvDeposite.text = LabelSet.getText(
            "deposit",
            R.string.deposit
        )
        tv_formationDate.text = LabelSet.getText(
            "date",
            R.string.date
        )
        tvop_passbook.text = LabelSet.getText(
            "op_passbook",
            R.string.op_passbook
        )
        tv_deposite.text = LabelSet.getText(
            "deposite",
            R.string.deposite
        )
        tvInterest.text = LabelSet.getText(
            "interest",
            R.string.interest
        )
        tv_bank_charges.text = LabelSet.getText(
            "bank_charges",
            R.string.bank_charges
        )
        tvOthers.text = LabelSet.getText(
            "others",
            R.string.others
        )



        tv_withdrawl.text = LabelSet.getText(
            "withdrawal",
            R.string.withdrawal
        )
        tv_formationDateWithdrawal.text = LabelSet.getText(
            "date",
            R.string.date
        )
        tvop_passbookWithdrawal.text = LabelSet.getText(
            "op_passbook",
            R.string.op_passbook
        )
        tv_depositeWithdrawal.text = LabelSet.getText(
            "deposite",
            R.string.deposite
        )
        tvInterestWithdrawal.text = LabelSet.getText(
            "interest",
            R.string.interest
        )
        tv_bank_chargesWithdrawal.text = LabelSet.getText(
            "bank_charges",
            R.string.bank_charges
        )
        tvOthersWithdrawal.text = LabelSet.getText(
            "others",
            R.string.others
        )
        tv_Amount.text = LabelSet.getText(
            "amount",
            R.string.amount
        )
        tv_opening_balance.text = LabelSet.getText(
            "opening_balance",
            R.string.opening_balance
        )
        tv_total_deposite.text = LabelSet.getText(
            "total_deposite",
            R.string.total_deposite
        )
        tv_withdrawal.text = LabelSet.getText(
            "withdrawal",
            R.string.withdrawal
        )
        tv_closing_balance.text = LabelSet.getText(
            "closing_balance",
            R.string.closing_balance
        )
       // tv_title.setText(LabelSet.getText("bank_transaction",R.string.bank_transaction))


        et_formationDate.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )
        et_op_passbook.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_deposite.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_interest.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_bank_charges.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_other.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )

        et_formationDateWithdrawal.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )
        et_op_passbookWithdrawal.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_depositeWithdrawal.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_interestWithdrawal.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_bank_chargesWithdrawal.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_otherWithdrawal.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )

    }

    private fun fillSpinner() {
        dataspin_BankTransaction = lookupViewmodel!!.getlookup(
            99,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        validate!!.fillspinner(this, spin_bankTransaction, dataspin_BankTransaction)

    }



}