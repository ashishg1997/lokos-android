package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.replaceFragmenty
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_bank_box.*

class BankSummeryActivity : AppCompatActivity() {

    var shgBankList: List<Cbo_bankEntity>? = null
    var validate: Validate? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    lateinit var financialTransactionsMemViewmodel: FinancialTransactionsMemViewmodel
    var incomeandExpenditureViewmodel: IncomeandExpenditureViewmodel? = null
    var dtLoanMemberViewmodel: DtLoanMemberViewmodel? = null
    var bankDrAmt = 0
    var bankCrAmt = 0
    var bankCount = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bank_box)

        validate = Validate(this)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        financialTransactionsMemViewmodel =
            ViewModelProviders.of(this).get(FinancialTransactionsMemViewmodel::class.java)
        incomeandExpenditureViewmodel =
            ViewModelProviders.of(this).get(IncomeandExpenditureViewmodel::class.java)
        dtLoanMemberViewmodel = ViewModelProviders.of(this).get(DtLoanMemberViewmodel::class.java)

        replaceFragmenty(
            fragment = MeetingTopBarFragment(10),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )
        if(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber)<validate!!.RetriveSharepreferenceInt(MeetingSP.maxmeetingnumber)) {
            btn_update.isEnabled = false
            btn_Transfer.isEnabled = false
        }else {
            btn_update.isEnabled = true
            btn_Transfer.isEnabled = true
        }
        spin_BankName?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                if (position > 0) {
                    val code = returaccount()
                    fillDebit(code)
                    fillCredit(code)
                    fillBankOP(code)
                    fillCashInTransit(code)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }

        btn_Transfer.setOnClickListener {
            if (bankCount > 1) {
                var intent = Intent(this, BankTransferListActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }else {
                validate!!.CustomAlert(LabelSet.getText("invalid_bank_count",R.string.invalid_bank_count),this)
            }
        }

        btn_update.setOnClickListener {
            if (bankCount > 1) {
                var intent = Intent(this, VoucherActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }else {
                validate!!.CustomAlert(LabelSet.getText("invalid_bank_count",R.string.invalid_bank_count),this)
            }
        }

        fillbank()

    }

    private fun fillCashInTransit(code: String) {

        var cashInTransit = generateMeetingViewmodel.getBank_CashInTransit(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),code
        )
        tv_amout_cash_transit.text = "₹ $cashInTransit"
    }

    private fun fillBankOP(code: String) {
        var openningbal = generateMeetingViewmodel.getopenningbal(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),code
        )
        var BankOP = openningbal + bankCrAmt - bankDrAmt

        tv_amout_total_balance.text = "₹ $BankOP"

    }

    private fun fillCredit(code: String) {

        var loanDisbursedAmt = generateMeetingViewmodel.getgrouptotloanamtinbank(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),code
        )

        var capitailIncome = financialTransactionsMemViewmodel.getBankCodeAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            code,"OR")

        var incomeAmount = incomeandExpenditureViewmodel!!.getBankCodeIncomeAndExpenditureAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            1, code)
        var memtotloanpaid = generateMeetingViewmodel.getmemtotloanpaidinbank(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),code)

        var transferincomeAmount = incomeandExpenditureViewmodel!!.getBankCodeIncomeAndExpendituretransferAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            code,listOf(41),"OI")
        bankCrAmt = (capitailIncome + incomeAmount + loanDisbursedAmt+memtotloanpaid+transferincomeAmount)

        tv_amount_credit.text = "₹ $bankCrAmt"
    }

    private fun fillDebit(code: String) {
        val loanDisbursedAmt = dtLoanMemberViewmodel!!.getMemLoanDisbursedAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),code
        )

        var capitailPayments = financialTransactionsMemViewmodel.getBankCodeAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            code,"OP"
        )

        var grptotloanpaid = generateMeetingViewmodel.getgrptotloanpaidinbank(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),code
        )
        var expenditureAmount =
            incomeandExpenditureViewmodel!!.getBankCodeIncomeAndExpenditureAmount(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                2, code
            )
        var transferincomeAmount = incomeandExpenditureViewmodel!!.getBankCodeIncomeAndExpendituretransferAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            code,listOf(42),"OE"
        )
        bankDrAmt = (loanDisbursedAmt + expenditureAmount+grptotloanpaid + transferincomeAmount + capitailPayments)

        tv_amount_total_debit.text = "₹ $bankDrAmt"
    }

    fun fillbank() {
        shgBankList =
            generateMeetingViewmodel.getBankdata(validate!!.RetriveSharepreferenceString(MeetingSP.SHGGUID))
        bankCount = shgBankList?.size!!
        val adapter: ArrayAdapter<String?>
        if (!shgBankList.isNullOrEmpty()) {
            val isize = shgBankList!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in shgBankList!!.indices) {
                var lastthree =
                    shgBankList!![i].account_no.substring(shgBankList!![i].account_no.length - 3)
                sValue[i + 1] =
                    shgBankList!![i].ifsc_code.toString().dropLast(7) + "XXXXX" + lastthree
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_BankName.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_BankName.adapter = adapter
        }

    }

    fun returaccount(): String {

        var pos = spin_BankName.selectedItemPosition
        var id = ""

        if (!shgBankList.isNullOrEmpty()) {
            if (pos > 0) id =
                shgBankList!!.get(pos - 1).ifsc_code!!.dropLast(7) + shgBankList!!.get(pos - 1).account_no
        }
        return id
    }

    fun setaccount(accountno: String): Int {

        var pos = 0

        if (!shgBankList.isNullOrEmpty()) {
            for (i in shgBankList!!.indices) {
                if (accountno.equals(
                        shgBankList!!.get(i).ifsc_code!!.dropLast(7) + shgBankList!!.get(
                            i
                        ).account_no
                    )
                )
                    pos = i + 1
            }
        }
        spin_BankName.setSelection(pos)
        return pos
    }
}