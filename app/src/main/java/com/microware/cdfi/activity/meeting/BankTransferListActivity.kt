package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.BankTransferAdapter
import com.microware.cdfi.api.meetingmodel.BankTransactionModel
import com.microware.cdfi.entity.ShgFinancialTxnDetailEntity
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.IncomeandExpenditureViewmodel
import kotlinx.android.synthetic.main.activity_bank_transfer_list.*
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.shg_toolbar.*

class BankTransferListActivity : AppCompatActivity() {
    var validate: Validate? = null
    var incomeandExpenditureViewmodel: IncomeandExpenditureViewmodel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bank_transfer_list)
        validate = Validate(this)
        incomeandExpenditureViewmodel = ViewModelProviders.of(this).get(IncomeandExpenditureViewmodel::class.java)

        tv_title.text = LabelSet.getText("bank_transfer",R.string.bank_transfer)

        icBack.setOnClickListener {
            var intent = Intent(this, BankSummeryActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        addBank.setOnClickListener {
            validate!!.SaveSharepreferenceInt(MeetingSP.Auid,0)
            validate!!.SaveSharepreferenceString(MeetingSP.bankFrom,"")
            validate!!.SaveSharepreferenceString(MeetingSP.bankTo,"")
            validate!!.SaveSharepreferenceString(MeetingSP.amountTransferred,"")
            var intent = Intent(this, BankTransferActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }
        setLabelText()
        fillRecyclerview()

    }

    private fun fillRecyclerview() {
        incomeandExpenditureViewmodel!!.getBankTransferList(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),3
        ).observe(this, object : Observer<List<BankTransactionModel>?> {
            override fun onChanged(income_list: List<BankTransactionModel>?) {
                if (!income_list.isNullOrEmpty()) {
                    rvList.layoutManager =
                        LinearLayoutManager(this@BankTransferListActivity)
                    val expenditurePaymentAdapter = BankTransferAdapter(
                        this@BankTransferListActivity,
                        income_list
                    )
                    val isize: Int
                    isize = income_list.size
                    val params: ViewGroup.LayoutParams = rvList.layoutParams
                    val r = resources
                    val px = TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP,
                        resources.getDimension(R.dimen.spraying),
                        r.displayMetrics
                    )
                    val hi = Math.round(px)
                    val gridHeight = hi * isize
                    params.height = gridHeight
                    rvList.layoutParams = params
                    rvList.adapter = expenditurePaymentAdapter
                }
            }
        })
    }

    override fun onBackPressed() {
        var intent = Intent(this, BankSummeryActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

    fun setLabelText(){
        tv_srno.text = LabelSet.getText("",R.string.sr_no)
        tv_bankFrom.text = LabelSet.getText("",R.string.bank_from)
        tv_bankTo.text = LabelSet.getText("",R.string.bank_to)
        tv_amount.text = LabelSet.getText("",R.string.amount)
    }

}