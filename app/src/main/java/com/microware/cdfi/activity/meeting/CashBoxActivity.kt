package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.replaceFragmenty
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_cashbox.*
import kotlinx.android.synthetic.main.buttons.*

class CashBoxActivity : AppCompatActivity() {
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    lateinit var financialTransactionsMemViewmodel: FinancialTransactionsMemViewmodel
    var dtLoanMemberViewmodel: DtLoanMemberViewmodel? = null
    var dtLoanTxnMemViewmodel: DtLoanTxnMemViewmodel? = null
    var dtLoanGPTxnViewmodel: DtLoanGpTxnViewmodel? = null
    var validate: Validate? = null
    var incomeandExpenditureViewmodel: IncomeandExpenditureViewmodel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cashbox)

        generateMeetingViewmodel = ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        financialTransactionsMemViewmodel = ViewModelProviders.of(this).get(FinancialTransactionsMemViewmodel::class.java)
        dtLoanMemberViewmodel = ViewModelProviders.of(this).get(DtLoanMemberViewmodel::class.java)
        dtLoanTxnMemViewmodel = ViewModelProviders.of(this).get(DtLoanTxnMemViewmodel::class.java)
        incomeandExpenditureViewmodel = ViewModelProviders.of(this).get(IncomeandExpenditureViewmodel::class.java)
        dtLoanGPTxnViewmodel = ViewModelProviders.of(this).get(DtLoanGpTxnViewmodel::class.java)
        validate = Validate(this)

        btn_cancel.visibility = View.GONE
        btn_save.visibility = View.GONE

        btn_cancel.setOnClickListener {
            var intent = Intent(this, MeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        replaceFragmenty(
            fragment = MeetingTopBarFragment(12),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        updatecashinhand()
        setOpeningCash()
        setIncomeAmount()
        setOutgoingAmount()
        setCashInTransit()

    }

    private fun setCashInTransit() {

        var cashInTransit = generateMeetingViewmodel.getCashInTransit(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber))

        tv_amout_cash_transit.text = "₹ $cashInTransit"
    }

    private fun setOutgoingAmount() {


        val expenditureAmount = incomeandExpenditureViewmodel!!.getIncomeAndExpenditureAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            1, 2)


        var totloan = generateMeetingViewmodel.gettotloan(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        var totloanpaidgroup = generateMeetingViewmodel.gettotloanpaidgroup(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        val capitalPayments = financialTransactionsMemViewmodel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            1,"OP"
        )
        var totcashwithdrawl = generateMeetingViewmodel.getsumwithdrwal(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        val amt = expenditureAmount  + totloan+totloanpaidgroup + capitalPayments + totcashwithdrawl

        tv_total_outgoing_amount.text = "₹ $amt"
    }

    private fun setOpeningCash() {
        var totcash = generateMeetingViewmodel.getupdatedpeningcash(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        tv_amount_opening_balance.text = "₹ $totcash"
    }

    private fun setIncomeAmount() {

        val capitailIncome = financialTransactionsMemViewmodel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            1,"OR"
        )


        var totcurrentcash = generateMeetingViewmodel.getupdatedcash(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )


        var totloanpaid = generateMeetingViewmodel.gettotloanpaid(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        var totloangroup = generateMeetingViewmodel.gettotloangroup(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )

        val totalPenalty = generateMeetingViewmodel.getTotalPenaltyByMtgNum(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))


        val incomeAmount = incomeandExpenditureViewmodel!!.getIncomeAndExpenditureAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            1, 1
        )


        val total = capitailIncome + totcurrentcash   + totloanpaid+ totloangroup + incomeAmount +totalPenalty
        tv_amount_total_incoming.text = "₹ $total"
    }

    private fun updatecashinhand() {


        val cashhand =validate!!.RetriveSharepreferenceInt(MeetingSP.Cashinhand)

        tv_amount_closing_balance.text = "₹ $cashhand"

    }

    override fun onBackPressed() {
        var intent = Intent(this, MeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

}