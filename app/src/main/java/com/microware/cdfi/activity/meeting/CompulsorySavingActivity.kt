package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.CumpulsorySavingAdapter
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import kotlinx.android.synthetic.main.activity_compulsory_saving.*
import kotlinx.android.synthetic.main.activity_compulsory_saving.tv_memberName
import kotlinx.android.synthetic.main.activity_compulsory_saving.tv_srno
import kotlinx.android.synthetic.main.buttons.*

class CompulsorySavingActivity : AppCompatActivity() {
    var validate:Validate?=null
    lateinit var cumpulsorySavingAdapter: CumpulsorySavingAdapter
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    var Todayvalue=0
    var Cumvalue=0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_compulsory_saving)
        validate= Validate(this)

        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)


        btn_save.isEnabled =
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber) >= validate!!.RetriveSharepreferenceInt(MeetingSP.maxmeetingnumber)

        btn_save.setOnClickListener {
            getCompulsoryValue()
        }
        btn_cancel.setOnClickListener {
            if(validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType)==11 || validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType)==12){
                val intent = Intent(this, CutOffMeetingMenuActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
                finish()
            }else {
                val intent = Intent(this, MeetingMenuActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
                finish()
            }
        }


        replaceFragmenty(
            fragment = MeetingTopBarFragment(2),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )
        setLabelText()
        fillRecyclerView()
    }

    override fun onBackPressed() {
        if(validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType)==11 || validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType)==12){
            val intent = Intent(this, CutOffMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        }else {
            val intent = Intent(this, MeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        }
    }

    fun setLabelText() {
        tv_srno.text = LabelSet.getText("sr_no", R.string.sr_no)
        tv_memberName.text = LabelSet.getText(
            "member_name",
            R.string.member_name
        )
        tv_compulsory_saving.text = LabelSet.getText(
            "compulsory_saving",
            R.string.compulsory_saving
        )
        tv_Today.text = LabelSet.getText(
            "today",
            R.string.today
        ) + " " +LabelSet.getText(
            "rs",
            R.string.rs
        )
        tv_cum.text = LabelSet.getText(
            "cum",
            R.string.cum
        ) + " " +LabelSet.getText(
            "rs",
            R.string.rs
        )

        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }

    private fun fillRecyclerView() {
        Todayvalue=0
        Cumvalue=0
        var list = generateMeetingViewmodel.getListDataMtgByMtgnum(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        val total= generateMeetingViewmodel.getsumcomp(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        cumpulsorySavingAdapter = CumpulsorySavingAdapter(this,list,total)

        rvMemberList.layoutManager = LinearLayoutManager(this)

        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = rvMemberList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvMemberList.layoutParams = params
        rvMemberList.adapter = cumpulsorySavingAdapter
        refreshnotsaved()
    }
    private fun refreshnotsaved() {
        var count=  generateMeetingViewmodel.gettotcountnotsaved(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))

        tv_totalnotsaved.text=count.toString()
    }

    fun getAdjustmentSaving(mtgno:Int,memid:Long,auid:Int):Int?{
        var amount = 0
        amount = validate!!.returnIntegerValue(generateMeetingViewmodel.getMemberAdjustmentAmount(mtgno,memid,auid).toString())
        return amount
    }
    fun getTotalValue() {
        var iValue = 0
        var iValueCum = 0
        val iCount = rvMemberList.childCount


        for (i in 0 until iCount) {
            val gridChild =
                rvMemberList.getChildAt(i) as? ViewGroup

            val tv_count = gridChild!!
                .findViewById<View>(R.id.tv_count) as? EditText

            val tv_cum2 = gridChild
                .findViewById<View>(R.id.tv_cum2) as? TextView

            if (!tv_count!!.text.toString().isNullOrEmpty()) {
                iValue=iValue + validate!!.returnIntegerValue(tv_count.text.toString())
            }

            if (!tv_cum2!!.text.toString().isNullOrEmpty()) {
                iValueCum=iValueCum + validate!!.returnIntegerValue(tv_cum2.text.toString())
            }


        }
        tv_TotalTodayValue.text = iValue.toString()
        tv_TotalCumValue.text = iValueCum.toString()
        refreshnotsaved()

    }

    fun getTotalValue(iValue: Int,flag:Int) {
        if(flag==1) {
            Todayvalue = Todayvalue + iValue
            tv_TotalTodayValue.text = Todayvalue.toString()
        }else if(flag==2){
            Cumvalue = Cumvalue + iValue
            tv_TotalCumValue.text = Cumvalue.toString()
        }
    }



    fun getCompulsoryValue() {
        var saveValue = 0

        val iCount = rvMemberList.childCount


        for (i in 0 until iCount) {
            val gridChild =
                rvMemberList.getChildAt(i) as? ViewGroup

            val tv_count = gridChild!!
                .findViewById<View>(R.id.tv_count) as? EditText

            val et_GroupMCode = gridChild
                .findViewById<View>(R.id.et_GroupMCode) as? EditText

            val et_SavCompOb = gridChild
                .findViewById<View>(R.id.et_SavCompOb) as? EditText


            var iValue = validate!!.returnIntegerValue(tv_count?.text.toString())
            var GroupMCode = validate!!.returnLongValue(et_GroupMCode!!.text.toString())
            var SavCompOb = validate!!.returnIntegerValue(et_SavCompOb!!.text.toString())
            saveValue=saveData(iValue,GroupMCode,SavCompOb)


        }

        if(saveValue > 0){
            CDFIApplication.database?.dtmtgDao()
                ?.updatecompsave(validate!!.returnIntegerValue(tv_TotalCumValue.text.toString()),validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))

            replaceFragmenty(
                fragment = MeetingTopBarFragment(2),
                allowStateLoss = true,
                containerViewId = R.id.mainContent
            )
            fillRecyclerView()
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this
            )
        }
    }

    fun saveData(iValue: Int, group_m_code:Long, SavCompOb:Int):Int {
        var value=0
        var SavCompCb=iValue+SavCompOb
        generateMeetingViewmodel.updateCompulsorySaving(
            iValue,
            SavCompCb,
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            group_m_code,validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
            validate!!.Daybetweentime(validate!!.currentdatetime)
        )
        value=1

        return value
    }

}