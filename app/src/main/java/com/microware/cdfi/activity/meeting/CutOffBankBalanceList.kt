package com.microware.cdfi.activity.meeting

import android.content.Intent
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.CutOffBankBalanceAdapter
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.entity.DtMtgFinTxnEntity
import com.microware.cdfi.fragment.MeetingTopBarZeroFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import com.microware.cdfi.viewModel.LookupViewmodel
import kotlinx.android.synthetic.main.activity_expenditure_payment_summery_list.*
import kotlinx.android.synthetic.main.layout_cutoff_bank_balance_list.*
import kotlinx.android.synthetic.main.layout_cutoff_bank_balance_list.rvList
import kotlinx.android.synthetic.main.layout_cutoff_bank_balance_list.tv_TotalTodayValue
import kotlinx.android.synthetic.main.layout_cutoff_bank_balance_list.tvtotal

class CutOffBankBalanceList : AppCompatActivity() {

    var validate: Validate? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    var lookupViewmodel: LookupViewmodel? = null
    var Todayvalue = 0
    var Cumvalue = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_cutoff_bank_balance_list)
        validate = Validate(this)

        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)

        replaceFragmenty(
            fragment = MeetingTopBarZeroFragment(10),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        setLabelText()
        fillRecyclerView()

    }

    private fun fillRecyclerView() {
        var list =  generateMeetingViewmodel.getBankListDataByMtgnum(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))

        getTotalValue(list)

        if (!list.isNullOrEmpty()){
            rvList.layoutManager = LinearLayoutManager(this)
            val shareCapitalOtherReceiptAdapter = CutOffBankBalanceAdapter(this, list)
            val isize: Int
            isize = list.size
            val params: ViewGroup.LayoutParams = rvList.layoutParams
            val r = resources
            val px = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                resources.getDimension(R.dimen.spraying),
                r.displayMetrics
            )
            val hi = Math.round(px)
            val gridHeight = hi * isize
            params.height = gridHeight
            rvList.layoutParams = params
            rvList.adapter = shareCapitalOtherReceiptAdapter
        }

    }

    private fun setLabelText() {
        tv_srno.text = LabelSet.getText("sr_no", R.string.sr_no)
        tv_bank_name.text = LabelSet.getText(
            "bank_name",
            R.string.bank_name
        )
        tv_total_Cash_in_hand.text = LabelSet.getText(
            "total_balance",
            R.string.total_balance
        )

        //   tv_receiptAmount.setText(LabelSet.getText("receipt_amount", R.string.receipt_amount))

        tvtotal.text = LabelSet.getText("total", R.string.total)
    }

    fun setaccount(accountno:String): String {

        var bankName = ""
        var shgBankList =
            generateMeetingViewmodel.getBankdata(validate!!.RetriveSharepreferenceString(MeetingSP.SHGGUID))

        if (!shgBankList.isNullOrEmpty()) {
            for (i in shgBankList.indices) {
                if (accountno.equals(shgBankList.get(i).ifsc_code!!.dropLast(7) + shgBankList.get(i).account_no))
                    bankName = shgBankList.get(i).ifsc_code!!.dropLast(7) + shgBankList.get(i).account_no
            }
        }
        return bankName
    }

    fun getTotalValue(list:List<DtMtgFinTxnEntity>) {
        var totalAmount = 0
        for(i in 0 until list.size){
            totalAmount = totalAmount + validate!!.returnIntegerValue(list.get(i).closing_balance.toString())
        }
        tv_TotalTodayValue.text = totalAmount.toString()
    }


    override fun onBackPressed() {
        val intent = Intent(this, CutOffMeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()

    }
}