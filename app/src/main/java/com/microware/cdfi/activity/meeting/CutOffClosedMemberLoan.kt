package com.microware.cdfi.activity.meeting

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.*
import com.microware.cdfi.fragment.MeetingTopBarZeroFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.layout_cutoff_closed_member_loan.*

class CutOffClosedMemberLoan : AppCompatActivity() {

    var lookupViewmodel: LookupViewmodel? = null
    var validate: Validate? = null
    var shgBankList: List<Cbo_bankEntity>? = null
    var dataspin_loan_type: List<LookupEntity>? = null
    var dataspin_loan_source: List<LookupEntity>? = null
    var dataspin_modeofpayment: List<LookupEntity>? = null
    var dtLoanMemberEntity: DtLoanMemberEntity? = null
    var dtLoanTxnMemEntity: DtLoanTxnMemEntity? = null
    var dtLoanMemberSheduleEntity: DtLoanMemberSheduleEntity? = null
    var dtLoanTxnMemViewmodel: DtLoanTxnMemViewmodel? = null
    var dtLoanMemberViewmodel: DtLoanMemberViewmodel? = null
    var dtLoanMemberScheduleViewmodel: DtLoanMemberScheduleViewmodel? = null
    var dataspin_frequency: List<LookupEntity>? = null
    var memberlist: List<DtmtgDetEntity>? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    var loanpurpose = 0
    var dtLoanViewmodel: DtLoanViewmodel? = null
    lateinit var mstProductViewmodel: MstProductViewmodel
    var dataspin_product: List<LoanProductEntity>? = null

    var nullStringValue:String?=null
    var nullLongValue:Long?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_cutoff_closed_member_loan)

        validate = Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        dtLoanTxnMemViewmodel = ViewModelProviders.of(this).get(DtLoanTxnMemViewmodel::class.java)
        mstProductViewmodel = ViewModelProviders.of(this).get(MstProductViewmodel::class.java)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        dtLoanViewmodel = ViewModelProviders.of(this).get(DtLoanViewmodel::class.java)
        dtLoanMemberViewmodel = ViewModelProviders.of(this).get(DtLoanMemberViewmodel::class.java)
        dtLoanMemberScheduleViewmodel =
            ViewModelProviders.of(this).get(DtLoanMemberScheduleViewmodel::class.java)
        setLabelText()



        btn_cancel.setOnClickListener {
            var intent = Intent(this, CutOffClosedMemberLoanList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        }
        btn_save.setOnClickListener {
            //var countloanno=dtLoanViewmodel!!.getmaxloanno(validate!!.RetriveSharepreferenceLong(MeetingSP.Shgcode))
            if (checkValidation() == 1) {
                saveData()
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "data_saved_successfully",
                        R.string.data_saved_successfully
                    ),
                    this, CutOffClosedMemberLoanList::class.java
                )
            }
        }
        et_date_loan_taken.setOnClickListener {
            validate!!.datePickerwithmindate(
                validate!!.RetriveSharepreferenceLong(MeetingSP.Formation_dt),et_date_loan_taken)
        }


        // insertMasterdata()
        fillSpinner()
        fillmemberspinner()

        fillCutOffLoandata()

        replaceFragmenty(
            fragment = MeetingTopBarZeroFragment(4),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

    }
    private fun fillmemberspinner() {
        memberlist = generateMeetingViewmodel.getListDataMtgByMtgnum(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        val adapter: ArrayAdapter<String?>
        if (!memberlist.isNullOrEmpty()) {
            val isize = memberlist!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in memberlist!!.indices) {
                sValue[i + 1] = memberlist!![i].member_name
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_member.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(memberlist!!.size + 1)
            sValue[0] = LabelSet.getText(
                "nomemberavailable",
                R.string.nomemberavailable
            )
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_member.adapter = adapter
        }
        setMember()

    }


    fun returnmemid(): Long {

        var pos = spin_member.selectedItemPosition
        var id = 0L

        if (!memberlist.isNullOrEmpty()) {
            if (pos > 0) id = memberlist!!.get(pos - 1).mem_id
        }
        return id
    }

    fun setMember(): Int {
        var pos = 0
        var name = ""
        if (!memberlist.isNullOrEmpty()) {
            for (i in memberlist!!.indices) {
                if (validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid) == memberlist!!.get(i).mem_id) {
                    pos = i + 1
                    name = memberlist!!.get(i).member_name!!
                }

            }
        }
        tv_name.text = name
        tv_number.text = validate!!.RetriveSharepreferenceLong(MeetingSP.MemberCode).toString()
        spin_member.setSelection(pos)
        //   spin_member.isEnabled = false
        return pos
    }

    override fun onBackPressed() {
//        var intent = Intent(this, PeneltyListActivity::class.java)
        var intent = Intent(this, CutOffClosedMemberLoanList::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

    fun setLabelText() {
        tv_loan_no.text = LabelSet.getText(
            "loan_no",
            R.string.loan_no
        )
        tv_original_loan_amount.text = LabelSet.getText(
            "amount_disbursed",
            R.string.amount_disbursed
        )
        tvprincipal_repaid.text = LabelSet.getText(
            "principal_repaid",
            R.string.principal_repaid
        )
        tv_interest_paid.text = LabelSet.getText(
            "interest_paid",
            R.string.interest_paid
        )
        tv_disbursement_date.text = LabelSet.getText(
            "loan_disbursement_date",
            R.string.loan_disbursement_date
        )
        tv_loan_source.text = LabelSet.getText(
            "purpose",
            R.string.purpose
        )

        et_principal_repaid.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_interest_paid.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )

        // tv_loan_type.setText(LabelSet.getText("loan_type",R.string.loan_type))

        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
        btn_savegray.text = LabelSet.getText(
            "save",
            R.string.save
        )
        //tv_title.setText(LabelSet.getText("loan_disbursement",R.string.loan_disbursement))

    }

    fun fillSpinner() {


        dataspin_loan_source = lookupViewmodel!!.getlookup(
            67,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        validate!!.fillspinner(this, spin_purpose, dataspin_loan_source)

    }


    private fun checkValidation(): Int {

        var value = 1
        if (spin_member.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_member,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "member",
                    R.string.member
                )

            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_loan_no.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "pleaseenterloanno",
                    R.string.pleaseenterloanno
                ), this,
                et_loan_no
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_original_loan_amount.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "pleaseenterdisbursedloanamount",
                    R.string.pleaseenterdisbursedloanamount
                ), this,
                et_original_loan_amount
            )
            value = 0
            return value
        }
        if (validate!!.returnIntegerValue(et_principal_repaid.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "enter_principal_repaid",
                    R.string.enter_principal_repaid
                ), this,
                et_principal_repaid
            )
            value = 0
            return value
        }
        if (validate!!.returnIntegerValue(et_original_loan_amount.text.toString()) != validate!!.returnIntegerValue(et_principal_repaid.text.toString())) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "principal_repaid_disbursed",
                    R.string.principal_repaid_disbursed
                ), this,
                et_principal_repaid
            )
            value = 0
            return value
        }

        return value
    }


    private fun fillCutOffLoandata() {

        var loanlist =
            dtLoanMemberViewmodel!!.getLoanDetailData(validate!!.RetriveSharepreferenceInt(MeetingSP.Loanno),validate!!.RetriveSharepreferenceLong(
                MeetingSP.shgid),validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid),validate!!.RetriveSharepreferenceString(
                MeetingSP.mtg_guid)!!)
        if (!loanlist.isNullOrEmpty()) {
            spin_member.isEnabled = false
            et_loan_no.setText(loanlist.get(0).loan_no.toString())
            et_original_loan_amount.setText(validate!!.returnStringValue(loanlist.get(0).original_loan_amount.toString()))
            et_principal_repaid.setText(validate!!.returnStringValue(loanlist.get(0).principal_repaid.toString()))
            et_interest_paid.setText(validate!!.returnStringValue(loanlist.get(0).interest_repaid.toString()))
            et_date_loan_taken.setText(validate!!.convertDatetime(validate!!.returnLongValue(loanlist.get(0).disbursement_date.toString())))



            spin_purpose.setSelection(
                validate!!.returnlookupcodepos(validate!!.returnIntegerValue(
                    loanlist.get(0).loan_purpose.toString()), dataspin_loan_source))

            btn_savegray.visibility = View.VISIBLE
            btn_save.visibility = View.GONE
        } else {
            et_date_loan_taken.setText(validate!!.convertDatetime(
                validate!!.returnLongValue(validate!!.RetriveSharepreferenceLong(
                    MeetingSP.CurrentMtgDate).toString())))
            var loanno = dtLoanMemberViewmodel!!.getCutOffmaxLoanno(validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber))
            et_loan_no.setText((loanno + 1).toString())

            btn_savegray.visibility = View.GONE
            btn_save.visibility = View.VISIBLE
        }

    }

    private fun saveData() {
        var loan_application_id:Long? = null
        var loan_product_id:Int? = null
        dtLoanMemberEntity = DtLoanMemberEntity(
            0,
            loan_application_id,
            validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            returnmemid(),
            validate!!.returnIntegerValue(et_loan_no.text.toString()),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            0,
            validate!!.returnIntegerValue(et_original_loan_amount.text.toString()),
            0,
            validate!!.returnlookupcode(spin_purpose, dataspin_loan_source),
            loan_product_id,
            0.0,
            0,
            0,
            0,
            true,
            0,
            0,
            0,
            "",
            "",
            0,
            0,
            validate!!.returnStringValue(et_loan_no.text.toString()),
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            nullStringValue,
            nullLongValue,
            nullStringValue,
            nullLongValue,
            validate!!.returnIntegerValue(et_principal_repaid.text.toString()),
            validate!!.returnIntegerValue(et_interest_paid.text.toString()),
            validate!!.Daybetweentime(et_date_loan_taken.text.toString()),
            1,0.0,0,0,1


        )
        dtLoanMemberViewmodel!!.insert(dtLoanMemberEntity!!)

        // insert in dtlonmemtxn

        dtLoanTxnMemEntity = DtLoanTxnMemEntity(
            0,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            returnmemid(),
            validate!!.returnIntegerValue(et_loan_no.text.toString()),
            validate!!.returnIntegerValue(et_original_loan_amount.text.toString()),
            0, validate!!.returnIntegerValue(et_principal_repaid.text.toString()),
            validate!!.returnIntegerValue(et_interest_paid.text.toString()),
            0,
            validate!!.returnIntegerValue(et_interest_paid.text.toString()),
            true,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            "",
            "",
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            nullStringValue,
            nullLongValue,
            nullStringValue,
            nullLongValue,
            0.0,0
        )
        dtLoanTxnMemViewmodel!!.insert(dtLoanTxnMemEntity!!)

    }


}