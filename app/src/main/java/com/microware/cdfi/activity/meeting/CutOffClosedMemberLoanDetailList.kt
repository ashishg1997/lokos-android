package com.microware.cdfi.activity.meeting

import android.content.Intent
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.ClosedMemberLoanDetailAdapter
import com.microware.cdfi.api.meetingmodel.LoanListModel
import com.microware.cdfi.fragment.MeetingTopBarZeroFragment
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.replaceFragmenty
import com.microware.cdfi.viewModel.DtLoanMemberScheduleViewmodel
import com.microware.cdfi.viewModel.DtLoanMemberViewmodel
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import kotlinx.android.synthetic.main.layout_cutoff_loan_disbursement_list.*

class CutOffClosedMemberLoanDetailList: AppCompatActivity() {
    var validate: Validate? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    var dtLoanMemberViewmodel: DtLoanMemberViewmodel? = null
    var dtLoanMemberScheduleViewmodel: DtLoanMemberScheduleViewmodel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_cutoff_loan_disbursement_list)

        validate = Validate(this)
        dtLoanMemberScheduleViewmodel =
            ViewModelProviders.of(this).get(DtLoanMemberScheduleViewmodel::class.java)
        dtLoanMemberViewmodel =
            ViewModelProviders.of(this).get(DtLoanMemberViewmodel::class.java)

        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)

        btn_cancel.setOnClickListener {
            var intent = Intent(this, CutOffClosedMemberLoanList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }


        replaceFragmenty(
            fragment = MeetingTopBarZeroFragment(4),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        setLabelText()
        fillRecyclerView()

        img_Add.visibility = View.GONE
    }

    override fun onBackPressed() {

        var intent = Intent(this, CutOffClosedMemberLoanList::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()

    }

    fun setLabelText() {
        tblButton.visibility = View.VISIBLE
        tv_loans.visibility = View.GONE
        tv_laon_outstanding.visibility = View.VISIBLE
        tv_srno.visibility = View.VISIBLE
        tv_srno.text = LabelSet.getText("sr_no", R.string.sr_no)
        tv_member_name.text = LabelSet.getText(
            "member_namenew",
            R.string.member_namenew
        )
        tv_laon_outstanding.text = LabelSet.getText(
            "loan_no",
            R.string.loan_no
        )
        tv_total_demand.text = LabelSet.getText(
            "loan_amount",
            R.string.loan_amount
        )
        tv_total_disbursed.text = LabelSet.getText(
            "total_loan_amount",
            R.string.total_loan_amount
        )
        tv_total.text = LabelSet.getText("total", R.string.total)

    }

    private fun fillRecyclerView() {
        var list = generateMeetingViewmodel.getClosedMemberLoanByMemberId(
            validate!!.RetriveSharepreferenceInt(
                MeetingSP.currentmeetingnumber
            ), validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),true,validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid))

        getTotal(list)

        rvList.layoutManager = LinearLayoutManager(this)
        var closedLoanDetailAdapter =
            ClosedMemberLoanDetailAdapter(this, list)
        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = rvList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvList.layoutParams = params
        rvList.adapter = closedLoanDetailAdapter

    }

    fun getTotal(list:List<LoanListModel>){

        var totalLoans = 0
        var totalLoanAmount = 0
        for (i in 0 until list.size){
            totalLoanAmount = totalLoanAmount + validate!!.returnIntegerValue(list.get(i).original_loan_amount.toString())
            if(validate!!.returnIntegerValue(list.get(i).loan_no.toString()) > 0){
                totalLoans = totalLoans + 1
            }
            tv_today_demand_total.text = totalLoans.toString()
            tv_next_demand_total.text = totalLoanAmount.toString()
        }
    }

    fun getLoanno(appid: Long): Int {
        return dtLoanMemberViewmodel!!.getLoanno(appid)
    }

}