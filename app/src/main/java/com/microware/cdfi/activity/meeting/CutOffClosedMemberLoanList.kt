package com.microware.cdfi.activity.meeting

import android.content.Intent
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.ClosedMemberLoanAdapter
import com.microware.cdfi.fragment.MeetingTopBarZeroFragment
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.replaceFragmenty
import com.microware.cdfi.viewModel.DtLoanMemberScheduleViewmodel
import com.microware.cdfi.viewModel.DtLoanMemberViewmodel
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import kotlinx.android.synthetic.main.layout_cutoff_loan_disbursement_list.*

class CutOffClosedMemberLoanList : AppCompatActivity() {
    var validate: Validate? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    var dtLoanMemberViewmodel: DtLoanMemberViewmodel? = null
    var dtLoanMemberScheduleViewmodel: DtLoanMemberScheduleViewmodel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_cutoff_loan_disbursement_list)

        validate = Validate(this)
        dtLoanMemberScheduleViewmodel =
            ViewModelProviders.of(this).get(DtLoanMemberScheduleViewmodel::class.java)
        dtLoanMemberViewmodel =
            ViewModelProviders.of(this).get(DtLoanMemberViewmodel::class.java)

        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)

        btn_cancel.setOnClickListener {
            var intent = Intent(this, CutOffMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }


        replaceFragmenty(
            fragment = MeetingTopBarZeroFragment(4),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        setLabelText()
        fillRecyclerView()

        img_Add.setOnClickListener {
            validate!!.SaveSharepreferenceLong(MeetingSP.Memberid,0)
            validate!!.SaveSharepreferenceLong(
                MeetingSP.MemberCode,0)
            var inetnt = Intent(this, CutOffClosedMemberLoan::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(inetnt)
            overridePendingTransition(0, 0)
            finish()
        }
    }

    override fun onBackPressed() {

        var intent = Intent(this, CutOffMeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()

    }

    fun setLabelText() {
        tblButton.visibility = View.VISIBLE
        tv_loans.visibility = View.GONE
        tv_laon_outstanding.visibility = View.VISIBLE
        tv_srno.visibility = View.VISIBLE
        tv_srno.text = LabelSet.getText("sr_no", R.string.sr_no)
        tv_member_name.text = LabelSet.getText(
            "member_namenew",
            R.string.member_namenew
        )
        tv_laon_outstanding.text = LabelSet.getText(
            "total_closed_loans",
            R.string.total_closed_loans
        )
        tv_total_demand.text = LabelSet.getText(
            "loan_amount",
            R.string.loan_amount
        )
        tv_total_disbursed.text = LabelSet.getText(
            "total_loan_amount",
            R.string.total_loan_amount
        )
        tv_total.text = LabelSet.getText("total", R.string.total)

    }

    private fun fillRecyclerView() {
        var list = generateMeetingViewmodel.getCutOffClosedMemberLoanDataByMtgNum(
            validate!!.RetriveSharepreferenceInt(
                MeetingSP.currentmeetingnumber
            ), validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),true)

        getTotal()
        rvList.layoutManager = LinearLayoutManager(this)
        var closedLoanAdapter =
            ClosedMemberLoanAdapter(this, list)
        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = rvList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvList.layoutParams = params
        rvList.adapter = closedLoanAdapter

    }

  /*  fun getTotalValue() {
        var iValue1 = 0
        var iValue2 = 0
        var iValue3 = 0
        val iCount = rvList.childCount


        for (i in 0 until iCount) {
            val gridChild =
                rvList.getChildAt(i) as? ViewGroup

            val tv_value1 = gridChild!!
                .findViewById<View>(R.id.tv_value1) as? TextView

            val tv_value2 = gridChild
                .findViewById<View>(R.id.tv_value2) as? TextView
            val tv_value3 = gridChild
                .findViewById<View>(R.id.tv_value3) as? TextView

            if (!tv_value1!!.text.toString().isNullOrEmpty()) {
                iValue1 = iValue1 + validate!!.returnIntegerValue(tv_value1.text.toString())
            }

            if (!tv_value2!!.text.toString().isNullOrEmpty()) {
                iValue2 = iValue2 + validate!!.returnIntegerValue(tv_value2.text.toString())
            }
            if (!tv_value3!!.text.toString().isNullOrEmpty()) {
                iValue3 = iValue3 + validate!!.returnIntegerValue(tv_value3.text.toString())
            }


        }
     //   tv_today_demand_total.text = iValue1.toString()
        tv_paid_total.text = iValue2.toString()
      //  tv_next_demand_total.text = iValue3.toString()

    }*/


    fun getTotalClosedLoanAmount(memid: Long?, mtgNum: Int?): Int {
        return dtLoanMemberViewmodel!!.getTotalClosedLoanAmount(validate!!.returnIntegerValue(mtgNum.toString()), validate!!.returnLongValue(memid.toString()),true)
    }

  fun getTotalClosedLoanCount(memid: Long?, mtgNum: Int?): Int {
        return dtLoanMemberViewmodel!!.getTotalClosedLoanCount(validate!!.returnIntegerValue(mtgNum.toString()), validate!!.returnLongValue(memid.toString()),true)
    }

    fun getLoanno(appid: Long): Int {
        return dtLoanMemberViewmodel!!.getLoanno(appid)
    }

    fun getTotal(){
        var list = generateMeetingViewmodel.getCutOffClosedMemberLoanDataByCboId(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),true)

        var totalLoans = 0
        var totalLoanAmount = 0
        for (i in 0 until list.size){
            totalLoanAmount = totalLoanAmount + validate!!.returnIntegerValue(list.get(i).original_loan_amount.toString())
            if(validate!!.returnIntegerValue(list.get(i).loan_no.toString()) > 0){
                totalLoans = totalLoans + 1
            }
            tv_today_demand_total.text = totalLoans.toString()
            tv_next_demand_total.text = totalLoanAmount.toString()
        }
    }
}