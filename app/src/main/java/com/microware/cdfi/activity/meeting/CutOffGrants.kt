package com.microware.cdfi.activity.meeting

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.*
import com.microware.cdfi.fragment.MeetingTopBarZeroFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_group_receipt_and_income_detail_actiity.*

class CutOffGrants : AppCompatActivity() {

    var validate: Validate? = null
    var lookupViewmodel: LookupViewmodel? = null
    var dataspin_modeofpayment: List<LookupEntity>? = null
    var dataspin_bank: List<BankEntity>? = null
    var bankMasterViewModel: MasterBankViewmodel? = null
    var dataspin_ReceiptType: List<LookupEntity>? = null
    var dataspin_Receipt: List<MstCOAEntity>? = null
    var dataspin_receivedFrom: List<LookupEntity>? = null
    var incomeandExpenditureViewmodel: IncomeandExpenditureViewmodel? = null
    var shgFinancialTxnDetailEntity: ShgFinancialTxnDetailEntity? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    lateinit var mstCoaViewmodel: MstCOAViewmodel
    var shgBankList : List<Cbo_bankEntity>? = null
    var cashInTransit = 0
    var bankCashInTransit = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group_receipt_and_income_detail_actiity)


        validate = Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        bankMasterViewModel = ViewModelProviders.of(this).get(MasterBankViewmodel::class.java)
        incomeandExpenditureViewmodel = ViewModelProviders.of(this).get(
            IncomeandExpenditureViewmodel::class.java)
        generateMeetingViewmodel = ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        mstCoaViewmodel = ViewModelProviders.of(this).get(MstCOAViewmodel::class.java)
        cashInTransit = generateMeetingViewmodel.getCashInTransit(validate!!.RetriveSharepreferenceLong(
            MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber))
        replaceFragmenty(
            fragment = MeetingTopBarZeroFragment(13),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        btn_cancel.setOnClickListener {
            val intent = Intent(this, CutOffGrantsList::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        btn_save.isEnabled =
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber) >= validate!!.RetriveSharepreferenceInt(
                MeetingSP.maxmeetingnumber)

        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                SaveData()
            }
        }

        spin_receipt?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                if (position > 0) {
                    val receipt_id = validate!!.returncoauid(spin_receipt, dataspin_Receipt)
                    if (receipt_id == 41){
                        et_amount.setText(cashInTransit.toString())
                        spin_mode_of_payment.setSelection(2)
                        spin_mode_of_payment.isEnabled = false
                    }else if(receipt_id == 43){
                        spin_mode_of_payment.setSelection(1)
                        spin_mode_of_payment.isEnabled = false
                    }else{
                        spin_mode_of_payment.isEnabled = true
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        spin_mode_of_payment?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                if (position > 0) {
                    val id = validate!!.returnlookupcode(spin_mode_of_payment, dataspin_modeofpayment)
                    if (id > 1){
                        lay_BankName.visibility = View.VISIBLE
                        lay_chequeNO.visibility = View.VISIBLE
                    }else{
                        spin_BankName.setSelection(0)
                        et_cheque_no_transactio_no.setText("")
                        lay_BankName.visibility = View.GONE
                        lay_chequeNO.visibility = View.GONE
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        fillbank(spin_BankName)
        fillSpinner()
        setLabelText()
        showData()

    }

    fun fillbank(spin: Spinner) {
        shgBankList = generateMeetingViewmodel.getBankdata(validate!!.RetriveSharepreferenceString(
            MeetingSP.SHGGUID))

        val adapter: ArrayAdapter<String?>
        if (!shgBankList.isNullOrEmpty()) {
            val isize = shgBankList!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in shgBankList!!.indices) {
                var lastthree=shgBankList!![i].account_no.substring(shgBankList!![i].account_no.length-3)
                sValue[i + 1] =
                    shgBankList!![i].ifsc_code.toString().dropLast(7) +"XXXXX"+ lastthree
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }

    }

    fun returaccount(spin: Spinner): String {

        var pos = spin.selectedItemPosition
        var id = ""

        if (!shgBankList.isNullOrEmpty()) {
            if (pos > 0) id =
                shgBankList!!.get(pos - 1).ifsc_code!!.dropLast(7) + shgBankList!!.get(pos - 1).account_no
        }
        return id
    }

    fun setaccount(accountno:String,spin: Spinner): Int {

        var pos = 0

        if (!shgBankList.isNullOrEmpty()) {
            for (i in shgBankList!!.indices) {
                if (accountno.equals(
                        shgBankList!!.get(i).ifsc_code!!.dropLast(7) + shgBankList!!.get(
                            i
                        ).account_no
                    )
                )
                    pos = i + 1
            }
        }
        spin.setSelection(pos)
        return pos
    }

    private fun showData() {
        val data = incomeandExpenditureViewmodel!!.getIncomeAndExpendituredata(
            validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceInt(MeetingSP.Auid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.savingSource)
        )

        if (!data.isNullOrEmpty()) {
            et_amount.setText(data[0].amount.toString())
            et_cheque_no_transactio_no.setText(data[0].transaction_no)

            spin_received_from.setSelection(
                validate!!.returnLookuppos(
                    validate!!.returnIntegerValue(
                        data[0].amount_to_from.toString()
                    ), dataspin_receivedFrom
                )
            )
            spin_received_from.isEnabled = false
            spin_mode_of_payment.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(
                        data[0].mode_payment.toString()
                    ), dataspin_modeofpayment
                )
            )

            spin_receipt.setSelection(
                validate!!.returncoauidpos(
                    validate!!.returnIntegerValue(
                        data[0].auid.toString()
                    ), dataspin_Receipt
                )
            )

            /*  spin_BankName.setSelection(
                  validate!!.returnMasterBankpos(
                      validate!!.returnIntegerValue(data[0].bank_code.toString()),
                      dataspin_bank
                  )
              )*/

            setaccount(data[0].bank_code.toString(), spin_BankName)
            spin_receipt.isEnabled = false
        }else{
            spin_mode_of_payment.setSelection(
                validate!!.returnlookupcodepos(
                    1, dataspin_modeofpayment
                )
            )
        }


    }

    private fun SaveData() {

        var save = 0

        var bankCode = ""

            bankCode = returaccount(spin_BankName)

        if (validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)
                .isNullOrEmpty() || validate!!.RetriveSharepreferenceInt(MeetingSP.Auid) == 0
        ) {
            shgFinancialTxnDetailEntity = ShgFinancialTxnDetailEntity(
                0,
                validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.returncoauid(spin_receipt, dataspin_Receipt),/*receipt goes here auid mstcoa*/
                1,/*Receipt type goes here fundtype*/
                validate!!.returnLookupcode(spin_received_from,dataspin_receivedFrom),
                "OI",
                validate!!.returnIntegerValue(et_amount.text.toString()),
                validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
                0,
                validate!!.returnlookupcode(spin_mode_of_payment, dataspin_modeofpayment),
                bankCode,
                et_cheque_no_transactio_no.text.toString(),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                null, null, null, null)
            incomeandExpenditureViewmodel!!.insert(shgFinancialTxnDetailEntity!!)

            save = 1
        } else {
            incomeandExpenditureViewmodel!!.updateIncomeAndExpenditure(
                validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceInt(MeetingSP.Auid),
                1,
                validate!!.returnLookupcode(spin_received_from,dataspin_receivedFrom),
                "OI",
                validate!!.returnIntegerValue(et_amount.text.toString()),
                validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
                0,
                validate!!.returnlookupcode(spin_mode_of_payment, dataspin_modeofpayment),
                bankCode,
                et_cheque_no_transactio_no.text.toString(),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime)
            )

            save = 2
        }

        if (save == 1) {
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this, CutOffGrantsList::class.java
            )
        } else {
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_updated_successfully",
                    R.string.data_updated_successfully
                ),
                this, CutOffGrantsList::class.java
            )
        }


    }

    private fun setLabelText() {
        tv_received_from.text = LabelSet.getText(
            "source",R.string.source)
        tv_ReceiptType.text = LabelSet.getText(
            "receipt_type",
            R.string.receipt_type
        )
        tv_receipt.text = LabelSet.getText(
            "fund_type",
            R.string.fund_type
        )
        tv_amount.text = LabelSet.getText(
            "total_amount_received",R.string.total_amount_received)

        tv_balance_amount.text = LabelSet.getText(
            "total_amount_received",
            R.string.total_balance_amount)
        tv_paymentMode.text = LabelSet.getText(
            "mode_of_payment",
            R.string.mode_of_payment
        )
        tv_BankName.text = LabelSet.getText(
            "bank_name",
            R.string.bank_name
        )
        tv_cheque_no_transactio_no.text = LabelSet.getText(
            "cheque_no_transactio_no",
            R.string.cheque_no_transactio_no
        )
    }

    private fun fillSpinner() {
        dataspin_receivedFrom = lookupViewmodel!!.getlookupMasterdata(
            72, validate!!.RetriveSharepreferenceString(
                AppSP.Langaugecode
            ), listOf(3,4,5)
        )
        dataspin_ReceiptType = lookupViewmodel!!.getlookup(
            81, validate!!.RetriveSharepreferenceString(
                AppSP.Langaugecode
            )
        )
        dataspin_Receipt = mstCoaViewmodel.getCoaSubHeadData(
            listOf(8,9),
            "OI", validate!!.RetriveSharepreferenceString(
                AppSP.Langaugecode
            )!!
        )
        dataspin_modeofpayment = lookupViewmodel!!.getlookup(
            65, validate!!.RetriveSharepreferenceString(
                AppSP.Langaugecode
            )
        )
//        dataspin_bank = bankMasterViewModel!!.getBankMaster()
//        validate!!.fillMasterBankspinner(this, spin_BankName, dataspin_bank)
        validate!!.fillLookupspinner(this, spin_received_from, dataspin_receivedFrom)
        validate!!.fillspinner(this, spin_TypeofReceipt, dataspin_ReceiptType)
        validate!!.fillspinner(this, spin_mode_of_payment, dataspin_modeofpayment)
        validate!!.fillcoaspinner(this, spin_receipt, dataspin_Receipt)
        spin_received_from.setSelection(1)
        spin_received_from.isEnabled = false
    }

    private fun checkValidation(): Int {
        var value = 1
        if (spin_received_from.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_received_from,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "paid_to",
                    R.string.received_from
                )
            )
            value = 0
        }else if (spin_receipt.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_receipt,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "payment",
                    R.string.receipt
                )
            )
            value = 0
        }else if (spin_mode_of_payment.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_mode_of_payment,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "mode_of_payment",
                    R.string.mode_of_payment
                )
            )
            value = 0
        } else if (spin_BankName.selectedItemPosition == 0 && lay_BankName.visibility == View.VISIBLE) {
            validate!!.CustomAlertSpinner(
                this, spin_BankName,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "bank_name",
                    R.string.bank_name
                )
            )
            value = 0
        } else if (et_cheque_no_transactio_no.text.toString().length == 0 && lay_chequeNO.visibility == View.VISIBLE) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "cheque_no_transactio_no",
                    R.string.cheque_no_transactio_no
                ),
                this, et_cheque_no_transactio_no
            )
            value = 0
        }else if (et_amount.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "amount",
                    R.string.amount
                ),
                this, et_amount
            )
            value = 0
        }

        return value
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, CutOffGrantsList::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

}