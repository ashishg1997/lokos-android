package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.fragment.MeetingTopBarZeroFragment
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.replaceFragmenty
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import kotlinx.android.synthetic.main.activity_cut_off_group.*

class CutOffGroupActivity : AppCompatActivity() {
    var validate: Validate? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cut_off_group)

        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        validate = Validate(this)

        var TotalPresent = generateMeetingViewmodel.getCutOffTotalPresent(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )

        replaceFragmenty(
            fragment = MeetingTopBarZeroFragment(11),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )
        /*    tblAttendance.setOnClickListener {
                var intent = Intent(this, AttendnceDetailActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }

            tbl_compulsory.setOnClickListener {
                var intent = Intent(this, CompulsorySavingActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }

            tbl_voluntary.setOnClickListener {
                var intent = Intent(this, VoluntorySavingActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }*/
        ivDownLiabilities.setOnClickListener {
            if(TotalPresent>0) {
                lay_Liabilities.visibility = View.VISIBLE
                ivUpLiabilities.visibility = View.VISIBLE
                ivDownLiabilities.visibility = View.GONE
                ivDownAsset.visibility = View.VISIBLE
                ivDownSurplus.visibility = View.VISIBLE
                ivUpAsset.visibility = View.GONE
                ivUpSurplus.visibility = View.GONE
                lay_Asset.visibility = View.GONE
                ivDownLiabilities.visibility = View.GONE
                lay_SurplusORLoss.visibility = View.GONE
            }else {
                validate!!.CustomAlert(LabelSet.getText(
                    "",
                    R.string.markattendance
                ),this)
            }
        }

        ivDownAsset.setOnClickListener {
            if(TotalPresent>0) {
                lay_Asset.visibility = View.VISIBLE
                lay_SurplusORLoss.visibility = View.GONE
                lay_Liabilities.visibility = View.GONE
                ivUpLiabilities.visibility = View.GONE
                ivDownLiabilities.visibility = View.VISIBLE
                ivDownAsset.visibility = View.GONE
                ivDownSurplus.visibility = View.VISIBLE
                ivUpAsset.visibility = View.VISIBLE
                ivUpSurplus.visibility = View.GONE
            }else {
                validate!!.CustomAlert(LabelSet.getText(
                    "",
                    R.string.markattendance
                ),this)
            }
        }

        ivDownSurplus.setOnClickListener {
            if(TotalPresent>0) {

                lay_SurplusORLoss.visibility = View.VISIBLE
                lay_Asset.visibility = View.GONE
                lay_Liabilities.visibility = View.GONE
                ivUpLiabilities.visibility = View.GONE
                ivDownLiabilities.visibility = View.VISIBLE
                ivDownAsset.visibility = View.VISIBLE
                ivDownSurplus.visibility = View.GONE
                ivUpAsset.visibility = View.GONE
                ivUpSurplus.visibility = View.VISIBLE
            }else {
                validate!!.CustomAlert(LabelSet.getText(
                    "",
                    R.string.markattendance
                ),this)
            }
        }

        ivUpLiabilities.setOnClickListener {
            lay_Liabilities.visibility = View.GONE
            ivUpLiabilities.visibility = View.GONE
            ivDownLiabilities.visibility = View.VISIBLE
        }

        ivUpAsset.setOnClickListener {
            lay_Asset.visibility = View.GONE
            ivUpAsset.visibility = View.GONE
            ivDownAsset.visibility = View.VISIBLE
        }

        ivUpSurplus.setOnClickListener {
            lay_SurplusORLoss.visibility = View.GONE
            ivUpSurplus.visibility = View.GONE
            ivDownSurplus.visibility = View.VISIBLE
        }

        setValues()
    }

    fun setValues(){
        // Saving
        val totalCompulsarysaving = generateMeetingViewmodel.getsumcomp(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))

        val totalVoluntaryarysaving = generateMeetingViewmodel.getsumvol(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))

        val rs_string = LabelSet.getText(
            "rs_sign",
            R.string.rs_sign
        )
        tv_compulsory_amount.text = rs_string+ " " +validate!!.returnStringValue(totalCompulsarysaving.toString())
        tv_voluntary_amount.text = rs_string+ " " +validate!!.returnStringValue(totalVoluntaryarysaving.toString())

        /*Total Cash in Hand*/
        var cash_in_hand = validate!!.RetriveSharepreferenceInt(MeetingSP.Cashinhand)
        tv_hand_cash_amount.text = rs_string+ " " + validate!!.returnStringValue(cash_in_hand.toString())
        /*Total Cash in Hand*/

        /*Total Cash at Bank*/
        var cash_at_bank = generateMeetingViewmodel.gettotalinbank(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        tv_bank_amount.text = rs_string+ " " + validate!!.returnStringValue(cash_at_bank.toString())
        /*Total Cash at Bank*/

        /*Total Bank Loan Outstanding*/
        var totalBankLoanOutstanding = generateMeetingViewmodel.getTotalBankLoanOutstanding(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),8)
        tv_bank_loan_amount.text = rs_string+ " " +validate!!.returnStringValue(totalBankLoanOutstanding.toString())
        /*Total Bank Loan Outstanding*/

        /*Total CIF Loan Outstanding*/
        var totalCIFLoanOutstanding = generateMeetingViewmodel.getTotalLoanOutstandingByFundType(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),2)
        tv_cif_outstanding_amount.text = rs_string+ " " +validate!!.returnStringValue(totalCIFLoanOutstanding.toString())
        /*Total CIF Loan Outstanding*/

        /*Total CEF Loan Outstanding*/
        var totalCEFLoanOutstanding = generateMeetingViewmodel.getTotalLoanOutstandingByFundType(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),9)
        tv_cef_loan_outstanding_amount.text = rs_string+ " " +validate!!.returnStringValue(totalCEFLoanOutstanding.toString())
        /*Total CEF Loan Outstanding*/

        /*Total Interest Overdue*/
        var totalInterestOverdue = generateMeetingViewmodel.getTotalInterestOverdue(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        tv_interest_overdue_amount.text = rs_string+ " " +validate!!.returnStringValue(totalInterestOverdue.toString())
        /*Total Interest Overdue*/

        /*Savings with VO CLF*/
        var totalCompulsarySavingwithVO_CLF = generateMeetingViewmodel.getTotalInvestmentByAuid( validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),44,"OE")

        var totalVoluntarySavingwithVO_CLF = generateMeetingViewmodel.getTotalInvestmentByAuid( validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),45,"OE")

        var totalSavingWithVO_CLF = totalCompulsarySavingwithVO_CLF + totalVoluntarySavingwithVO_CLF

        tv_monthlyvo_amount.text = rs_string+ " " +validate!!.returnStringValue(totalSavingWithVO_CLF.toString())
        /*Savings with VO CLF*/

        /*Fixed Deposit with VO CLF*/
        var totalFixedDepositAmount = generateMeetingViewmodel.getTotalFDAmount(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),46,"OE")

        tv_FD_amount.text = rs_string+ " " +validate!!.returnStringValue(totalFixedDepositAmount.toString())
        /*Fixed Deposit with VO CLF*/

        /*Share Capital with VO CLF*/
        var totalShareCapitalWithVO_CLF = generateMeetingViewmodel.getTotalInvestmentByAuid(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),25,"OE")
        tv_share_capital_amount.text = rs_string+ " " +validate!!.returnStringValue(totalShareCapitalWithVO_CLF.toString())
        /*Share Capital with VO CLF*/

        /*Other Loan Outstanding*/
        var otherLoanOutstanding = generateMeetingViewmodel.getOtherLoanOutstanding(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))

        tv_other_loan_amount.text = rs_string+ " " +validate!!.returnStringValue(otherLoanOutstanding.toString())
        /*Other Loan Outstanding*/

        /*Member Loan Outstanding*/
        var TotalOutstandingMemberLoan = generateMeetingViewmodel.getTotalOutstandingMemberLoan(validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),false)

        tv_loan_amount.text = rs_string+ " " +validate!!.returnStringValue(TotalOutstandingMemberLoan.toString())
        /*Member Loan Outstanding*/

        /*Member Overdue Interest*/
        var TotalMemberInterestOverdue = generateMeetingViewmodel.getTotalMemberInterestOverdue(validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber))

        tv_loaninterest_overdue_amount.text = rs_string+ " " +validate!!.returnStringValue(TotalMemberInterestOverdue.toString())
        /*Member Overdue Interest*/

        /*D*/

        var totalSaving = totalCompulsarysaving + totalVoluntaryarysaving
        tv_amt_member_saving.text = rs_string+ " " + validate!!.returnStringValue(totalSaving.toString())

        /*E*/
        var totalLoanAdvances = totalCIFLoanOutstanding + totalCEFLoanOutstanding + otherLoanOutstanding + totalInterestOverdue

        tv_advance_loan_amt.text = rs_string+ " " +validate!!.returnStringValue(totalLoanAdvances.toString())

        /*A*/
        var totalCash = cash_in_hand + cash_at_bank
        tv_amt_cash.text = rs_string+ " " +validate!!.returnStringValue(totalCash.toString())

        /*B*/

        var totalReceivable = TotalOutstandingMemberLoan + TotalMemberInterestOverdue
        /*C*/
        var totalInvestment = totalSavingWithVO_CLF + totalFixedDepositAmount + totalShareCapitalWithVO_CLF
        tv_investments_amt.text = rs_string+ " " +validate!!.returnStringValue(totalInvestment.toString())

        var total_A_B_C = totalCash + totalReceivable + totalInvestment
        tv_total_ABC_amount.text = rs_string+ " " +validate!!.returnStringValue(total_A_B_C.toString())

        var total_D_E = totalSaving + totalLoanAdvances
        tv_total_DE_amount.text = rs_string+ " " +validate!!.returnStringValue(total_D_E.toString())

        var remainingAmount = total_A_B_C - total_D_E
        if(remainingAmount>=0){
            tbl_total_Surplus.visibility = View.VISIBLE
            tbl_total_Loss.visibility = View.GONE
            tv_total_Surplus_amount.text = rs_string+ " " +validate!!.returnStringValue(remainingAmount.toString())
        }else if(remainingAmount<0){
            tbl_total_Surplus.visibility = View.GONE
            tbl_total_Loss.visibility = View.VISIBLE
            tv_total_Loss_amount.text = "-" +" "+rs_string+ " " +validate!!.returnStringValue(remainingAmount.toString())
        }

        tv_advance_loanReceivable_amt.text = rs_string+ " " + validate!!.returnStringValue(totalReceivable.toString())
    }

    override fun onBackPressed() {
        var intent = Intent(this, CutOffMeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()

    }
}