package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.ArrayAdapter
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.api.meetingmodel.BankListModel
import com.microware.cdfi.entity.BankEntity
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.fragment.MeetingTopBarZeroFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.DtMtgFinTxnViewmodel
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import com.microware.cdfi.viewModel.MasterBankViewmodel
import kotlinx.android.synthetic.main.activity_cut_off_group_bank_balance.*
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.buttons.btn_cancel
import kotlinx.android.synthetic.main.buttons.btn_save

class CutOffGroupBankBalanceActivity : AppCompatActivity() {

    var validate: Validate? = null
    var shgBankList: List<Cbo_bankEntity>? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    lateinit var bankmasterViewmodel: MasterBankViewmodel
    var dataspin_bank: List<BankEntity>? = null
    var list : List<BankListModel>? = null
    var dtMtgFinTxnViewmodel: DtMtgFinTxnViewmodel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cut_off_group_bank_balance)
        validate = Validate(this)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        bankmasterViewmodel = ViewModelProviders.of(this).get(MasterBankViewmodel::class.java)
        dtMtgFinTxnViewmodel = ViewModelProviders.of(this).get(DtMtgFinTxnViewmodel::class.java)
        replaceFragmenty(
            fragment = MeetingTopBarZeroFragment(10),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        et_cashatBank.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = validate!!.returnIntegerValue(et_amountNotRealised.text.toString())
                val value1 = validate!!.returnIntegerValue(et_amountNotCredited.text.toString())
                val totalAmt = validate!!.returnIntegerValue(s.toString())+ value1-value
                et_TotalBalance.setText(totalAmt.toString())
            }

        })

        et_amountNotRealised.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = validate!!.returnIntegerValue(et_cashatBank.text.toString())
                val value1 = validate!!.returnIntegerValue(et_amountNotCredited.text.toString())
                val totalAmt = value + value1 - validate!!.returnIntegerValue(s.toString())
                et_TotalBalance.setText(totalAmt.toString())

            }

        })

        et_amountNotCredited.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = validate!!.returnIntegerValue(et_amountNotRealised.text.toString())
                val value1 = validate!!.returnIntegerValue(et_cashatBank.text.toString())
                val totalAmt = value1 + validate!!.returnIntegerValue(s.toString())-value
                et_TotalBalance.setText(totalAmt.toString())
            }

        })

        et_BalanceDate.setOnClickListener {
            validate!!.datePickerwithmindate(
                validate!!.RetriveSharepreferenceLong(MeetingSP.Formation_dt),et_BalanceDate)
        }

        btn_cancel.setOnClickListener {
            var intent = Intent(this, CutOffBankBalanceList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        }

        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                saveData()
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "data_saved_successfully",
                        R.string.data_saved_successfully
                    ),this, CutOffBankBalanceList::class.java)

            }
        }

        setLabelText()
        fillbank()
        showData()
    }


    private fun setLabelText() {
        tv_bankName.text = LabelSet.getText(
            "bank_name",
            R.string.bank_name
        )
        tv_BankAccountNO.text = LabelSet.getText(
            "bank_account_no",
            R.string.bank_account_no
        )
        tv_cashatBank.text = LabelSet.getText(
            "cash_at_bank",
            R.string.cash_at_bank
        )
        tv_amountNotRealised.text = LabelSet.getText(
            "cheque_issued_but_amount_not_realized",
            R.string.cheque_issued_but_amount_not_realized
        )
        tv_amountNotCredited.text = LabelSet.getText(
            "cheque_received_but_amount_not_credited",
            R.string.cheque_received_but_amount_not_credited
        )
        tv_totalBalance.text = LabelSet.getText(
            "total_balance",
            R.string.total_balance
        )
        tv_BalanceDate.text = LabelSet.getText(
            "date_of_balance",
            R.string.date_of_balance
        )
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_savegray.text = LabelSet.getText(
            "save",
            R.string.save
        )
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
        et_cashatBank.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_amountNotRealised.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_amountNotCredited.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )

    }

    private fun checkValidation(): Int {
        var value = 1
        var bankamount = validate!!.returnIntegerValue(et_cashatBank.text.toString()) + validate!!.returnIntegerValue(et_amountNotCredited.text.toString())
        var amountNotRelalized = validate!!.returnIntegerValue(et_amountNotRealised.text.toString())
        if (spin_BankName.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_BankName,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "bank_name",
                    R.string.bank_name
                )
            )
            value = 0
            return value
        }else if (et_cashatBank.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "cash_at_bank",
                    R.string.cash_at_bank
                ),
                this,
                et_cashatBank
            )
            value = 0
            return value
        } else if (et_amountNotRealised.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "cheque_issued_but_amount_not_realized",
                    R.string.cheque_issued_but_amount_not_realized
                ),
                this,
                et_amountNotRealised
            )
            value = 0
            return value
        }else if(bankamount < amountNotRelalized) {
            validate!!.CustomAlert(
                LabelSet.getText(
                    "cash_at_bank_realized",
                    R.string.cash_at_bank_realized
                ),this)
            value = 0
            return value
        } else if (et_amountNotCredited.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "cheque_received_but_amount_not_credited",
                    R.string.cheque_received_but_amount_not_credited
                ),
                this,
                et_amountNotCredited
            )
            value = 0
            return value
        } else if (et_BalanceDate.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "date_of_balance",
                    R.string.date_of_balance
                ),
                this,
                et_BalanceDate
            )
            value = 0
            return value
        }

        return value
    }

    override fun onBackPressed() {
        super.onBackPressed()
        var intent = Intent(this,  CutOffBankBalanceList::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

    fun fillbank(
    ) {
        shgBankList =
            generateMeetingViewmodel.getBankdata(validate!!.RetriveSharepreferenceString(MeetingSP.SHGGUID))

        val adapter: ArrayAdapter<String?>
        if (!shgBankList.isNullOrEmpty()) {
            val isize = shgBankList!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in shgBankList!!.indices) {
                var lastthree=shgBankList!![i].account_no.substring(shgBankList!![i].account_no.length-3)
                sValue[i + 1] =
                    shgBankList!![i].ifsc_code.toString().dropLast(7) +"XXXXX"+ lastthree
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_BankName.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_BankName.adapter = adapter
        }

    }

    fun returaccount(): String {

        var pos = spin_BankName.selectedItemPosition
        var id = ""

        if (!shgBankList.isNullOrEmpty()) {
            if (pos > 0) id =
                shgBankList!!.get(pos - 1).ifsc_code!!.dropLast(7) + shgBankList!!.get(pos - 1).account_no
        }
        return id
    }

    fun setaccount(accountno:String): Int {

        var pos = 0

        if (!shgBankList.isNullOrEmpty()) {
            for (i in shgBankList!!.indices) {
                if (accountno.equals(
                        shgBankList!!.get(i).ifsc_code!!.dropLast(7) + shgBankList!!.get(
                            i
                        ).account_no
                    )
                )
                    pos = i + 1
            }
        }
        spin_BankName.setSelection(pos)
        return pos
    }

    private fun saveData(){
        generateMeetingViewmodel.updateCutOffBankBalance(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            returaccount(),
            validate!!.returnIntegerValue(et_cashatBank.text.toString()),
            validate!!.returnIntegerValue(et_amountNotRealised.text.toString()),
            validate!!.returnIntegerValue(et_amountNotCredited.text.toString()),
            validate!!.returnIntegerValue(et_TotalBalance.text.toString()),
            validate!!.Daybetweentime(et_BalanceDate.text.toString()),
            validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
            validate!!.Daybetweentime(validate!!.currentdatetime),
            validate!!.RetriveSharepreferenceString(AppSP.userid)!!

        )
    }

    private fun showData(){
        var gpfintxnlist = dtMtgFinTxnViewmodel!!.getListDataByMtgnum(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceString(MeetingSP.bankCode)!!)
        if(!gpfintxnlist.isNullOrEmpty()){
            setaccount(validate!!.returnStringValue(gpfintxnlist.get(0).bank_code))
            et_cashatBank.setText(validate!!.returnStringValue(gpfintxnlist.get(0).zero_mtg_cash_bank.toString()))
            et_amountNotRealised.setText(validate!!.returnStringValue(gpfintxnlist.get(0).cheque_issued_not_realized.toString()))
            et_amountNotCredited.setText(validate!!.returnStringValue(gpfintxnlist.get(0).cheque_received_not_credited.toString()))
            if(validate!!.returnLongValue(gpfintxnlist.get(0).balance_date.toString())>0) {
                et_BalanceDate.setText(
                    validate!!.convertDatetime(validate!!.returnLongValue(gpfintxnlist.get(0).balance_date.toString())))
            }else {
                    et_BalanceDate.setText(validate!!.convertDatetime(
                        validate!!.returnLongValue(validate!!.RetriveSharepreferenceLong(
                            MeetingSP.CurrentMtgDate).toString())))
                }
            }
    }
}