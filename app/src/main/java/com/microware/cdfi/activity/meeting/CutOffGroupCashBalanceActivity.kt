package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.ShgFinancialTxnDetailEntity
import com.microware.cdfi.fragment.MeetingTopBarZeroFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import com.microware.cdfi.viewModel.IncomeandExpenditureViewmodel
import kotlinx.android.synthetic.main.activity_cut_off_group_cash_balance.*
import kotlinx.android.synthetic.main.buttons.*

class CutOffGroupCashBalanceActivity : AppCompatActivity() {

    var validate: Validate? = null
    var generateMeetingViewModel: GenerateMeetingViewmodel? = null
    var incomeandExpenditureViewmodel: IncomeandExpenditureViewmodel? = null
    var shgFinancialTxnDetailEntity: ShgFinancialTxnDetailEntity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cut_off_group_cash_balance)

        generateMeetingViewModel = ViewModelProviders.of(this).get(
            GenerateMeetingViewmodel::class.java)
        incomeandExpenditureViewmodel = ViewModelProviders.of(this).get(IncomeandExpenditureViewmodel::class.java)
        validate = Validate(this)

        replaceFragmenty(
            fragment = MeetingTopBarZeroFragment(12),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        et_cashInHand.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = validate!!.returnIntegerValue(et_TransitCash.text.toString())
                val totalAmt = validate!!.returnIntegerValue(s.toString()) - value
                et_netCashHand.setText(totalAmt.toString())
            }

        })

        et_TransitCash.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = validate!!.returnIntegerValue(et_cashInHand.text.toString())
                val totalAmt = value -  validate!!.returnIntegerValue(s.toString())
                et_netCashHand.setText(totalAmt.toString())

            }

        })

        btn_save.setOnClickListener {
            if (checkValidation() == 1){
                SaveData()
            }
        }

        btn_cancel.setOnClickListener {
            var intent = Intent(this, CutOffMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        }

        et_BalanceDate.setOnClickListener {
            validate!!.datePicker(et_BalanceDate)
        }

        setLabelText()
        showData()
    }

    private fun setLabelText() {
        tv_cashInHand.text = LabelSet.getText(
            "cash_in_hand",
            R.string.cash_in_hand
        )
        tv_TransitCash.text = LabelSet.getText(
            "cash_in_transit",
            R.string.cash_in_transit
        )
        tv_netCashHand.text = LabelSet.getText(
            "net_cash_in_hand",
            R.string.net_cash_in_hand
        )
        tv_BalanceDate.text = LabelSet.getText(
            "date_of_balance",
            R.string.date_of_balance
        )
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_savegray.text = LabelSet.getText(
            "save",
            R.string.save
        )
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
        et_cashInHand.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_TransitCash.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
    }

    private fun checkValidation() : Int{
        var value = 1

        if (et_cashInHand.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                )+" " +LabelSet.getText(
                    "cash_in_hand",
                    R.string.cash_in_hand
                ),
                this,
                et_cashInHand
            )
            value = 0
            return value
        }else if (et_TransitCash.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                )+" " +LabelSet.getText(
                    "cash_in_transit",
                    R.string.cash_in_transit
                ),
                this,
                tv_TransitCash
            )
            value = 0
            return value
        }else if(validate!!.returnIntegerValue(et_cashInHand.text.toString()) < validate!!.returnIntegerValue(et_TransitCash.text.toString())){
            validate!!.CustomAlert(
                LabelSet.getText(
                    "cash_in_hand_transit",
                    R.string.cash_in_hand_transit
                ),this)
            value = 0
            return value
        }else if (et_BalanceDate.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                )+" " +LabelSet.getText(
                    "date_of_balance",
                    R.string.date_of_balance
                ),
                this,
                et_BalanceDate
            )
            value = 0
            return value
        }



        return value
    }

    override fun onBackPressed() {
        super.onBackPressed()
        var intent = Intent(this, CutOffMeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

    private fun SaveData() {

        generateMeetingViewModel!!.updateCutOffCash(
            validate!!.returnIntegerValue(et_cashInHand.text.toString()),
            validate!!.returnIntegerValue(et_TransitCash.text.toString()),
            validate!!.Daybetweentime(et_BalanceDate.text.toString()),
            validate!!.returnIntegerValue(et_netCashHand.text.toString()),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            validate!!.RetriveSharepreferenceString(AppSP.userid)!!

        )
        updateFinancialTransactionDetail()

    }

    private fun showData(){
        var list = generateMeetingViewModel!!.getMeetingDetailData(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        if(!list.isNullOrEmpty()){
            et_cashInHand.setText(validate!!.returnStringValue(list.get(0).zero_mtg_cash_in_hand.toString()))
            et_TransitCash.setText(validate!!.returnStringValue(list.get(0).zero_mtg_cash_in_transit.toString()))
            if(validate!!.returnLongValue(list.get(0).balance_date.toString()) > 0){
                et_BalanceDate.setText(validate!!.convertDatetime(validate!!.returnLongValue(list.get(0).balance_date.toString())))
            }else {
                et_BalanceDate.setText(validate!!.convertDatetime(validate!!.returnLongValue(validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate).toString())))
            }
        }
    }

    fun updateFinancialTransactionDetail(){
        var iCount = incomeandExpenditureViewmodel!!.getAdjustmentCashCount(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
        validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        if(iCount == 0){
            if(validate!!.returnIntegerValue(et_netCashHand.text.toString())<validate!!.RetriveSharepreferenceInt(MeetingSP.Cashinhand)){
                var difference = validate!!.RetriveSharepreferenceInt(MeetingSP.Cashinhand) - validate!!.returnIntegerValue(et_netCashHand.text.toString())

                shgFinancialTxnDetailEntity = ShgFinancialTxnDetailEntity(
                    0,
                    validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
                    validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                    validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                    47,/*receipt goes here auid mstcoa*/
                    2,/*Receipt type goes here fundtype*/
                    99,
                    "OE",
                    difference,
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    0,
                    1,
                    "",
                    "",
                    validate!!.RetriveSharepreferenceString(AppSP.userid),
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    "", 0, "", 0
                )
                incomeandExpenditureViewmodel!!.insert(shgFinancialTxnDetailEntity!!)

            }else if(validate!!.returnIntegerValue(et_netCashHand.text.toString())>validate!!.RetriveSharepreferenceInt(MeetingSP.Cashinhand)){
                var difference = validate!!.returnIntegerValue(et_netCashHand.text.toString()) - validate!!.RetriveSharepreferenceInt(MeetingSP.Cashinhand)
                shgFinancialTxnDetailEntity = ShgFinancialTxnDetailEntity(
                    0,
                    validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
                    validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                    validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                    48,/*receipt goes here auid mstcoa*/
                    1,/*Receipt type goes here fundtype*/
                    99,
                    "OI",
                    difference,
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    0,
                    1,
                    "",
                    "",
                    validate!!.RetriveSharepreferenceString(AppSP.userid),
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    "", 0, "", 0
                )
                incomeandExpenditureViewmodel!!.insert(shgFinancialTxnDetailEntity!!)
            }
        }else if(iCount > 0){
            if(validate!!.returnIntegerValue(et_netCashHand.text.toString())<validate!!.RetriveSharepreferenceInt(MeetingSP.Cashinhand)){
                var difference = validate!!.RetriveSharepreferenceInt(MeetingSP.Cashinhand) - validate!!.returnIntegerValue(et_netCashHand.text.toString())

                incomeandExpenditureViewmodel!!.updateCashAdjustment(
                    validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                    validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                    47,
                    2,
                    99,
                    "OE",
                    difference,
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    0,
                    1,
                    "",
                    "",
                    validate!!.RetriveSharepreferenceString(AppSP.userid),
                    validate!!.Daybetweentime(validate!!.currentdatetime)
                )
            }else if(validate!!.returnIntegerValue(et_netCashHand.text.toString())>validate!!.RetriveSharepreferenceInt(MeetingSP.Cashinhand)){
                var difference = validate!!.returnIntegerValue(et_netCashHand.text.toString()) - validate!!.RetriveSharepreferenceInt(MeetingSP.Cashinhand)
                incomeandExpenditureViewmodel!!.updateCashAdjustment(
                    validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                    validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                    48,
                    1,
                    99,
                    "OI",
                    difference,
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    0,
                    1,
                    "",
                    "",
                    validate!!.RetriveSharepreferenceString(AppSP.userid),
                    validate!!.Daybetweentime(validate!!.currentdatetime)
                )
            }
        }

        replaceFragmenty(
            fragment = MeetingTopBarZeroFragment(12),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )
        validate!!.CustomAlert(
            LabelSet.getText(
                "data_saved_successfully",
                R.string.data_saved_successfully
            ),this)
    }
}