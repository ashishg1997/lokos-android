package com.microware.cdfi.activity.meeting

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.api.meetingmodel.FundTypeModel
import com.microware.cdfi.entity.*
import com.microware.cdfi.fragment.MeetingTopBarZeroFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_cut_off_group_loan_received.*
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*

class CutOffGroupLoanReceivedActivity : AppCompatActivity() {

    var dtLoanGpEntity: DtLoanGpEntity? = null
    var dtMtgGrpLoanScheduleEntity: DtMtgGrpLoanScheduleEntity? = null
    var dtLoanGpTxnEntity: DtLoanGpTxnEntity? = null
    var lookupViewmodel: LookupViewmodel? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    lateinit var dtLoanGpViewmodel: DtLoanGpViewmodel
    lateinit var dtLoanGpTxnViewmodel: DtLoanGpTxnViewmodel
    lateinit var dtMtgGrpLoanScheduleViewmodel: DtMtgGrpLoanScheduleViewmodel
    lateinit var loanProductViewmodel: LoanProductViewModel

    var validate: Validate? = null
    var dataspin_loanStatus: List<LookupEntity>? = null
    var dataspin_loan_type: List<LookupEntity>? = null
    var dataspin_loan_source: List<LookupEntity>? = null
    var dataspin_meetingfreq: List<LookupEntity>? = null
    var dataspin_mode_of_payment: List<LookupEntity>? = null
    var shgBankList: List<Cbo_bankEntity>? = null
    var dataspin_fund: List<FundTypeModel>? = null

    var nullStringValue:String?=null
    var nullLongValue:Long?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cut_off_group_loan_received)

        validate = Validate(this)
        dtLoanGpViewmodel = ViewModelProviders.of(this).get(DtLoanGpViewmodel::class.java)
        dtMtgGrpLoanScheduleViewmodel =
            ViewModelProviders.of(this).get(DtMtgGrpLoanScheduleViewmodel::class.java)
        dtLoanGpTxnViewmodel = ViewModelProviders.of(this).get(DtLoanGpTxnViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        generateMeetingViewmodel = ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        loanProductViewmodel = ViewModelProviders.of(this).get(LoanProductViewModel::class.java)
        et_remaining_period_months.filters = arrayOf(InputFilterMinMax (1, 60))

        var loanno = generateMeetingViewmodel.getCutOffmaxLoanno(validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber))
        et_loan_no.setText((loanno + 1).toString())
        et_loanDisbursmentDate.setText(validate!!.convertDatetime(
            validate!!.returnLongValue(validate!!.RetriveSharepreferenceLong(
                MeetingSP.CurrentMtgDate).toString())))
        et_loanDisbursmentDate.setOnClickListener {
            validate!!.datePickerwithmindate(
                validate!!.RetriveSharepreferenceLong(MeetingSP.Formation_dt),et_loanDisbursmentDate)
        }
        //  validate!!.disableEmojiInTitle(et_loan_Refno,50)
        spin_mode_of_payment.isEnabled = false

        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                saveData(0)

                if (validate!!.returnlookupcode(spin_loanStatus, dataspin_loanStatus) == 1){
                    insertscheduler(
                        (validate!!.returnIntegerValue(et_outstanding_principal.text.toString()) - validate!!.returnIntegerValue(et_principal_overdue.text.toString())),
                        validate!!.returnIntegerValue(et_remaining_period_months.text.toString()),
                        0
                    )
                }else {
                    validate!!.CustomAlert(
                        LabelSet.getText(
                            "data_saved_successfully",
                            R.string.data_saved_successfully),
                        this, CutOffGroupLoanlist::class.java
                    )
                }
            }
        }

        btn_cancel.setOnClickListener {
            var intent = Intent(this, CutOffGroupLoanlist::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        spin_mode_of_payment?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (validate!!.returnlookupcode(
                        spin_mode_of_payment,
                        dataspin_mode_of_payment
                    ) == 2 || validate!!.returnlookupcode(
                        spin_mode_of_payment,
                        dataspin_mode_of_payment
                    ) == 3
                ) {
                    lay_bank_name.visibility = View.VISIBLE
                    lay_cheque_no_transactio_no.visibility = View.VISIBLE
                } else {
                    lay_bank_name.visibility = View.GONE
                    lay_cheque_no_transactio_no.visibility = View.GONE
                    spin_bankname.setSelection(0)

                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        et_remaining_period_months.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = validate!!.returnIntegerValue(et_outstanding_principal.text.toString())
                var principleOverDue = validate!!.returnIntegerValue(et_principal_overdue.text.toString())
                val value1 = value - principleOverDue
                if (validate!!.returnIntegerValue(s.toString()) > 0 && value1 > 0) {
                    val installmentamt = value1 / validate!!.returnIntegerValue(s.toString())
                    et_installment_amount.setText(installmentamt.toString())
                }

            }

        })

        et_outstanding_principal.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = validate!!.returnIntegerValue(s.toString())
                var principleOverDue = validate!!.returnIntegerValue(et_principal_overdue.text.toString())
                val value1 = value - principleOverDue
                if (validate!!.returnIntegerValue(et_remaining_period_months.text.toString()) > 0 && value1 > 0) {
                    val installmentamt = value1 / validate!!.returnIntegerValue(et_remaining_period_months.text.toString())
                    et_installment_amount.setText(installmentamt.toString())
                }

            }

        })

        et_principal_overdue.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var principleOverDue = validate!!.returnIntegerValue(s.toString())
                var amount = validate!!.returnIntegerValue(et_outstanding_principal.text.toString())
                val value1 = amount - principleOverDue

                if (validate!!.returnIntegerValue(et_remaining_period_months.text.toString()) > 0 && value1 > 0){
                    val installmentamt = value1 / validate!!.returnIntegerValue(et_remaining_period_months.text.toString())
                    et_installment_amount.setText(installmentamt.toString())
                }

                var value = validate!!.returnIntegerValue(et_interest_overdue.text.toString())
                if (validate!!.returnIntegerValue(s.toString()) > 0 && value > 0) {
                    val totalOverDue = value + validate!!.returnIntegerValue(s.toString())
                    et_total_overdue.setText(totalOverDue.toString())
                }else if (validate!!.returnIntegerValue(s.toString()) > 0 ) {
                    val totalOverDue = validate!!.returnIntegerValue(s.toString())
                    et_total_overdue.setText(totalOverDue.toString())
                }

            }

        })

        et_interest_overdue.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = validate!!.returnIntegerValue(et_principal_overdue.text.toString())
                if (validate!!.returnIntegerValue(s.toString()) > 0 && value > 0) {
                    val totalOverDue = value + validate!!.returnIntegerValue(s.toString())
                    et_total_overdue.setText(totalOverDue.toString())
                }else if (validate!!.returnIntegerValue(s.toString()) > 0 ) {
                    val totalOverDue = validate!!.returnIntegerValue(s.toString())
                    et_total_overdue.setText(totalOverDue.toString())
                }

            }

        })

        et_amount_disbursed.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var outstandingPrincipal = validate!!.returnIntegerValue(et_amount_disbursed.text.toString()) -
                        validate!!.returnIntegerValue(et_principal_repaid.text.toString())
                et_outstanding_principal.setText(validate!!.returnStringValue(outstandingPrincipal.toString()))

            }

        })

        et_principal_repaid.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var outstandingPrincipal = validate!!.returnIntegerValue(et_amount_disbursed.text.toString()) -
                        validate!!.returnIntegerValue(et_principal_repaid.text.toString())
                et_outstanding_principal.setText(validate!!.returnStringValue(outstandingPrincipal.toString()))

            }

        })

        spin_loan_source?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                var source = validate!!.returnFundSourceId(spin_loan_source, dataspin_loan_source)
                dataspin_fund =
                    loanProductViewmodel.getFundTypeBySource_Receipt(source,2)
                validate!!.fillFundType(this@CutOffGroupLoanReceivedActivity,spin_fund_type,dataspin_fund)
                spin_type_loan.isEnabled =
                    validate!!.returnFundSourceId(spin_loan_source, dataspin_loan_source) == 8
                if(validate!!.returnFundSourceId(spin_loan_source, dataspin_loan_source) == 8){
                    spin_mode_of_payment.setSelection(2)
                    lay_fund_type.visibility = View.GONE
                    spin_fund_type.setSelection(0)
                    tv_loan_ref_astrix.visibility = View.VISIBLE
                }else {
                    spin_mode_of_payment.setSelection(1)
                    lay_fund_type.visibility = View.VISIBLE
                    tv_loan_ref_astrix.visibility = View.GONE
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        spin_type_loan?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (validate!!.returnlookupcode(spin_type_loan, dataspin_loan_type) == 1) {
                    lay_drawinglimit.visibility = View.VISIBLE
                    lay_loan_sanctioned.visibility = View.VISIBLE
                    tv_interest_rate.text = LabelSet.getText(
                        "current_roi",
                        R.string.current_roi
                    )
                } else {
                    tv_interest_rate.text = LabelSet.getText(
                        "interest_rate",
                        R.string.interest_rate
                    )
                    lay_drawinglimit.visibility = View.GONE
                    lay_loan_sanctioned.visibility = View.GONE
                    et_drawinglimit.setText("")
                    et_loan_sanctioned.setText("")
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        replaceFragmenty(
            fragment = MeetingTopBarZeroFragment(9),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        setLabelText()
        fillSpinner()
        fillbank()

        spin_repayment_frequency.setSelection(3)

    }

    private fun saveData(morotrum: Int) {
        var completionFlag:Boolean? = null
        if(validate!!.returnlookupcode(spin_loanStatus, dataspin_loanStatus)==1){
            completionFlag = false
        }else if(validate!!.returnlookupcode(spin_loanStatus, dataspin_loanStatus)==2){
            completionFlag = true
        }
        var laonappid =
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid) + 1000 + validate!!.returnIntegerValue(et_loan_no.text.toString()) + 1
        dtLoanGpEntity = DtLoanGpEntity(
            0,
            laonappid,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
            validate!!.returnIntegerValue(et_loan_no.text.toString()),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            validate!!.Daybetweentime(et_loanDisbursmentDate.text.toString()),
            validate!!.addmonth(validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate), morotrum,validate!!.returnlookupcode(spin_repayment_frequency, dataspin_meetingfreq)),
            validate!!.returnIntegerValue(et_outstanding_principal.text.toString()),
            0,
            validate!!.returnFundTypeId(spin_fund_type,dataspin_fund),
            validate!!.returnDoubleValue(et_interest_rate.text.toString()),
            validate!!.returnIntegerValue(et_remaining_period_months.text.toString()),
            validate!!.returnIntegerValue(et_principal_overdue.text.toString()),
            validate!!.returnIntegerValue(et_interest_overdue.text.toString()),
            0.0,
            completionFlag,
            validate!!.returnlookupcode(spin_type_loan, dataspin_loan_type),
            validate!!.returnFundSourceId(spin_loan_source, dataspin_loan_source),
            0,
            validate!!.returnlookupcode(spin_mode_of_payment, dataspin_mode_of_payment),
            returaccount(),
            validate!!.returnStringValue(et_cheque_no_transactio_no.text.toString()),
            validate!!.returnlookupcode(spin_repayment_frequency,dataspin_meetingfreq),
            0,
            "",
            0,
            validate!!.returnFundSourceId(spin_loan_source, dataspin_loan_source),
            validate!!.returnIntegerValue(et_loan_sanctioned.text.toString()),
            validate!!.returnIntegerValue(et_drawinglimit.text.toString()),
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            nullStringValue,
            nullLongValue,
            nullStringValue,
            nullLongValue,
            validate!!.returnStringValue(et_loan_Refno.text.toString()),
            et_Org_name.text.toString(),
            validate!!.returnIntegerValue(et_principal_repaid.text.toString()),
            validate!!.returnIntegerValue(et_amount_disbursed.text.toString()),
            validate!!.returnIntegerValue(et_outstanding_period_months.text.toString()),0,
            validate!!.returnDoubleValue(et_interest_rate.text.toString()),
            validate!!.returnIntegerValue(et_remaining_period_months.text.toString()),0,1
        )
        dtLoanGpViewmodel.insert(dtLoanGpEntity!!)

        // insert in dtloantxn

        dtLoanGpTxnEntity = DtLoanGpTxnEntity(
            0,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            validate!!.returnIntegerValue(et_loan_no.text.toString()),
            validate!!.returnIntegerValue(et_amount_disbursed.text.toString()),
            0, validate!!.returnIntegerValue(et_principal_repaid.text.toString()),
            0,
            validate!!.returnIntegerValue(et_outstanding_principal.text.toString()),
            0,
            completionFlag,
            0,
            validate!!.returnIntegerValue(et_interest_overdue.text.toString()),
            validate!!.returnIntegerValue(et_interest_overdue.text.toString()),

            0,
            validate!!.returnIntegerValue(et_principal_overdue.text.toString()),
            validate!!.returnIntegerValue(et_principal_overdue.text.toString()),

            validate!!.returnlookupcode(spin_mode_of_payment, dataspin_mode_of_payment),
            returaccount(),
            validate!!.returnStringValue(
                et_cheque_no_transactio_no.text.toString()
            ),
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            nullStringValue,
            nullLongValue,
            nullStringValue,
            nullLongValue,0,
            validate!!.returnDoubleValue(et_interest_rate.text.toString()),
            validate!!.returnIntegerValue(et_remaining_period_months.text.toString())
        )
        dtLoanGpTxnViewmodel.insert(dtLoanGpTxnEntity!!)

    }

    private fun insertscheduler(amt: Int, installment: Int, morotrum: Int) {
        var principaldemand = amt / installment
        var loanos = 0
        var saveValue = 0
        for (i in 0 until installment) {
            if (i == installment - 1) {
                loanos = 0
                principaldemand = amt - (principaldemand * i)
            } else {
                loanos = amt - (principaldemand * (i + 1))
            }
            dtMtgGrpLoanScheduleEntity = DtMtgGrpLoanScheduleEntity(
                0,
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
                validate!!.returnIntegerValue(et_loan_no.text.toString()),
                principaldemand,
                principaldemand,
                loanos, 0,
                i + 1,
                1,
                validate!!.addmonth(
                    validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
                    (morotrum + i + 1),validate!!.returnlookupcode(spin_repayment_frequency, dataspin_meetingfreq)),
                //   validate!!.Daybetweentime(validate!!.currentdatetime),
               null,
                0, 0,
                null,
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime), nullStringValue,
                nullLongValue,
                nullStringValue,
                nullLongValue

            )
            dtMtgGrpLoanScheduleViewmodel.insert(dtMtgGrpLoanScheduleEntity!!)
        }
        var principalDemand =
            dtMtgGrpLoanScheduleViewmodel.getPrincipalDemandByInstallmentNum(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
                validate!!.returnIntegerValue(et_loan_no.text.toString()),
                1
            )
        principalDemand += validate!!.returnIntegerValue(et_principal_overdue.text.toString())
        dtMtgGrpLoanScheduleViewmodel.updateCutOffLoanSchedule(
            validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.returnIntegerValue(et_loan_no.text.toString()),
            1,
            principalDemand

        )
        CustomAlertchangeschedule(
            LabelSet.getText(
                "doyouwanttochaneschedule",
                R.string.doyouwanttochaneschedule
            )
        )
    }

    fun setLabelText() {
        tv_loanStatus.text = LabelSet.getText(
            "status_of_loan",
            R.string.status_of_loan
        )
        tv_loan_no.text = LabelSet.getText(
            "loan_no",
            R.string.loan_no
        )
        tv_loan_Refno.text = LabelSet.getText(
            "loan_reference_no",
            R.string.loan_reference_no
        )
        tv_Org_name.text = LabelSet.getText(
            "organization_name",
            R.string.organization_name
        )
        tv_fund_type.text = LabelSet.getText(
            "fund_type",
            R.string.fund_type
        )
        et_loan_no.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_loan_Refno.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_Org_name.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_loan_source.text = LabelSet.getText(
            "loan_source",
            R.string.loan_source
        )
        tv_loan_disbursmentDate.text = LabelSet.getText(
            "loan_disbursement_date",
            R.string.loan_disbursement_date
        )
        tv_installment_amount.text = LabelSet.getText(
            "installment_amount",
            R.string.installment_amount
        )

        tv_loan_sanctioned.text = LabelSet.getText(
            "sanctioned_amount",
            R.string.sanctioned_amount
        )
        tv_drawinglimit.text = LabelSet.getText(
            "current_drawing_power",
            R.string.current_drawing_power
        )
        tv_amount_disbursed.text = LabelSet.getText(
            "disubursed_loan_amt",
            R.string.disubursed_loan_amt
        )
        tv_principal_reapid.text = LabelSet.getText(
            "principal_repaid",
            R.string.principal_repaid
        )
        tv_outstainding_principle.text = LabelSet.getText(
            "outstanding_principal",
            R.string.outstanding_principal
        )
        tv_principal_overdue.text = LabelSet.getText(
            "principal_overdue",
            R.string.principal_overdue
        )
        tv_interest_overdue.text = LabelSet.getText(
            "interest_overdue",
            R.string.interest_overdue
        )

        et_loanDisbursmentDate.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )
        tv_type_loan.text = LabelSet.getText(
            "type_of_loan",
            R.string.type_of_loan
        )
        et_loan_sanctioned.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_amount_disbursed.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_original_period_months.text = LabelSet.getText(
            "original_period_months",
            R.string.original_period_months
        )
        tv_outstanding_period_months.text = LabelSet.getText(
            "total_outstanding_period_months",
            R.string.total_outstanding_period_months
        )
        tv_remaining_period_months.text = LabelSet.getText(
            "remaining_period_months",
            R.string.remaining_period_months
        )

        et_original_period_months.hint = LabelSet.getText(
            "type_here",R.string.type_here)
        et_outstanding_period_months.hint = LabelSet.getText(
            "type_here",R.string.type_here)
        et_remaining_period_months.hint = LabelSet.getText(
            "type_here",R.string.type_here)

        tv_interest_rate.text = LabelSet.getText(
            "interest_rate",
            R.string.interest_rate
        )
        et_interest_rate.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_mode_of_payment.text = LabelSet.getText(
            "mode_of_payment",
            R.string.mode_of_payment
        )
        tv_bank_name.text = LabelSet.getText(
            "name_of_bank",
            R.string.name_of_bank
        )
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )

    }

    fun fillbank() {
        shgBankList =
            generateMeetingViewmodel.getBankdata(validate!!.RetriveSharepreferenceString(MeetingSP.SHGGUID))

        val adapter: ArrayAdapter<String?>
        if (!shgBankList.isNullOrEmpty()) {
            val isize = shgBankList!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in shgBankList!!.indices) {
                sValue[i + 1] =
                    shgBankList!![i].ifsc_code.toString().dropLast(7) + shgBankList!![i].account_no
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_bankname.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_bankname.adapter = adapter
        }

    }

    fun returaccount(): String {

        var pos = spin_bankname.selectedItemPosition
        var id = ""

        if (!shgBankList.isNullOrEmpty()) {
            if (pos > 0) id =
                shgBankList!!.get(pos - 1).ifsc_code!!.dropLast(7) + shgBankList!!.get(pos - 1).account_no
        }
        return id
    }

    fun setaccount(accountno: String): Int {

        var pos = 0

        if (!shgBankList.isNullOrEmpty()) {
            for (i in shgBankList!!.indices) {
                if (accountno.equals(
                        shgBankList!!.get(i).ifsc_code!!.dropLast(7) + shgBankList!!.get(
                            i
                        ).account_no
                    )
                )
                    pos = i + 1
            }
        }
        return pos
    }

    private fun checkValidation(): Int {

        var totalPrincipal = validate!!.returnIntegerValue(et_principal_repaid.text.toString()) +
                validate!!.returnIntegerValue(et_outstanding_principal.text.toString())
        var value = 1
        if (spin_loanStatus.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_loanStatus,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "status_of_loan",
                    R.string.status_of_loan
                )

            )
            value = 0
            return value
        }

        if (spin_loan_source.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_loan_source,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "loan_source",
                    R.string.loan_source
                )

            )
            value = 0
            return value
        }

        if (validate!!.returnFundSourceId(spin_loan_source, dataspin_loan_source) == 8 && validate!!.returnIntegerValue(et_loan_Refno.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter_loan_ref_no",
                    R.string.please_enter_loan_ref_no
                ), this,
                et_loan_Refno
            )
            value = 0
            return value
        }



        if (lay_fund_type.visibility == View.VISIBLE && spin_fund_type.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_fund_type,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "fund_type",
                    R.string.fund_type
                )

            )
            value = 0
            return value
        }

        if (spin_type_loan.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_type_loan,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "type_of_loan",
                    R.string.type_of_loan
                )

            )
            value = 0
            return value
        }

        if (lay_loan_sanctioned.visibility == View.VISIBLE && validate!!.returnIntegerValue(et_loan_sanctioned.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "pleaseentersanctionamount",
                    R.string.pleaseentersanctionamount
                ), this,
                et_loan_sanctioned
            )
            value = 0
            return value
        }

        if (lay_drawinglimit.visibility == View.VISIBLE && validate!!.returnIntegerValue(et_drawinglimit.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                )+LabelSet.getText(
                    "drawinglimit",
                    R.string.drawinglimit
                ), this,
                et_loan_sanctioned
            )
            value = 0
            return value
        }
        if ((validate!!.returnIntegerValue(et_drawinglimit.text.toString()) > validate!!.returnIntegerValue(et_loan_sanctioned.text.toString()) && lay_drawinglimit.visibility == View.VISIBLE )) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "drawing_sanctioned_amount",
                    R.string.drawing_sanctioned_amount
                ), this,
                et_drawinglimit
            )
            value = 0
            return value
        }

        if (validate!!.returnDoubleValue(et_interest_rate.text.toString()) ==0.0  ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                )+LabelSet.getText(
                    "interest_rate",
                    R.string.interest_rate
                ), this,
                et_interest_rate
            )

            value = 0
            return value
        }
        if (validate!!.returnDoubleValue(et_interest_rate.text.toString()) >36 ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "valid_interest",
                    R.string.valid_interest
                ), this,
                et_interest_rate
            )

            value = 0
            return value
        }
        if (validate!!.returnIntegerValue(et_amount_disbursed.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "pleaseenterdisbursedloanamount",
                    R.string.pleaseenterdisbursedloanamount
                ), this,
                et_amount_disbursed
            )
            value = 0
            return value
        }

        if ((validate!!.returnIntegerValue(et_amount_disbursed.text.toString()) > validate!!.returnIntegerValue(et_drawinglimit.text.toString()) && lay_drawinglimit.visibility == View.VISIBLE )) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "disbursed_drawing_limit",
                    R.string.disbursed_drawing_limit
                ), this,
                et_amount_disbursed
            )
            value = 0
            return value
        }

        if(spin_loanStatus.selectedItemPosition==1 && validate!!.returnIntegerValue(et_outstanding_principal.text.toString())==0){
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "enter_outstanding_principal",
                    R.string.enter_outstanding_principal
                ), this,
                et_outstanding_principal
            )
            value = 0
            return value
        }
        if(spin_loanStatus.selectedItemPosition==1 && validate!!.returnIntegerValue(et_amount_disbursed.text.toString())!=totalPrincipal){
            validate!!.CustomAlert(
                LabelSet.getText(
                    "invalid_sum_principal",
                    R.string.invalid_sum_principal
                ), this)
            value = 0
            return value
        }
        if(spin_loanStatus.selectedItemPosition==1 && validate!!.returnIntegerValue(et_amount_disbursed.text.toString())<= validate!!.returnIntegerValue(et_principal_repaid.text.toString())){
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "compare_principal_repaid_amount_disbursed",
                    R.string.compare_principal_repaid_amount_disbursed
                ), this,
                et_principal_repaid
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_remaining_period_months.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "pleaseenterintallment",
                    R.string.pleaseenterintallment
                ), this,
                et_remaining_period_months
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_loan_no.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "pleaseenterloanno",
                    R.string.pleaseenterloanno
                ), this,
                et_loan_no
            )
            value = 0
            return value
        }
        if (lay_bank_name.visibility == View.VISIBLE &&spin_bankname.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_bankname,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "name_of_bank",
                    R.string.name_of_bank
                )

            )
            value = 0
            return value
        }
        if (lay_cheque_no_transactio_no.visibility == View.VISIBLE && validate!!.returnStringValue(
                et_cheque_no_transactio_no.text.toString()).length == 0
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + LabelSet.getText(
                    "cheque_no_transactio_no",
                    R.string.cheque_no_transactio_no
                ), this,
                et_cheque_no_transactio_no
            )
            value = 0
            return value
        }



        return value
    }

    private fun fillSpinner() {
        dataspin_loanStatus = lookupViewmodel!!.getlookup(
            89,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_loan_type = lookupViewmodel!!.getlookup(
            88,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )


        dataspin_mode_of_payment = lookupViewmodel!!.getlookup(
            65,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )


        dataspin_loan_source = lookupViewmodel!!.getlookupMasterdata(
            91,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),listOf(3,4,6,8))

        dataspin_meetingfreq = lookupViewmodel!!.getlookup(
            19,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        validate!!.fillspinner(this, spin_loanStatus, dataspin_loanStatus)
        validate!!.fillspinner(this, spin_type_loan, dataspin_loan_type)
        validate!!.fillFundSource(this, spin_loan_source, dataspin_loan_source)
        validate!!.fillspinner(this, spin_repayment_frequency, dataspin_meetingfreq)
        validate!!.fillspinner(this, spin_mode_of_payment, dataspin_mode_of_payment)
        spin_type_loan.setSelection(2)
        spin_repayment_frequency.isEnabled = false
    }

    override fun onBackPressed() {
        var intent = Intent(this, CutOffGroupLoanlist::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }


    fun CustomAlertchangeschedule(str: String) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str
        mDialogView.btn_yes.setOnClickListener {
            //            PartialSaveData()
            mAlertDialog.dismiss()

            var institutionId = validate!!.returnFundSourceId(spin_loan_source, dataspin_loan_source)
            validate!!.SaveSharepreferenceString(MeetingSP.Institution,validate!!.returnlookupcodevalue(institutionId, dataspin_loan_source))
            validate!!.SaveSharepreferenceInt(
                MeetingSP.Loanno,
                validate!!.returnIntegerValue(et_loan_no.text.toString())
            )

            val intent = Intent(this, GroupPrincipalDemandActivity::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)


        }
        mDialogView.btn_no.setOnClickListener {
            mAlertDialog.dismiss()

            val intent = Intent(this, CutOffGroupLoanlist::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)

        }
    }
}