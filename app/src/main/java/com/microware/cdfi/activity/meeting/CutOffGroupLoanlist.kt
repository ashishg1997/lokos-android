package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.CutOffGroupLoanListAdapter
import com.microware.cdfi.fragment.MeetingTopBarZeroFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_cut_off_group_loanlist.*

class CutOffGroupLoanlist : AppCompatActivity() {

    var validate: Validate? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    lateinit var dtLoanGpTxnViewmodel: DtLoanGpTxnViewmodel
    lateinit var dtLoanGpViewmodel: DtLoanGpViewmodel
    lateinit var lookupViewmodel: LookupViewmodel
    lateinit var loanProductViewmodel: LoanProductViewModel
    var today = 0
    var paid = 0
    var nextpayable = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cut_off_group_loanlist)

        validate = Validate(this)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        dtLoanGpTxnViewmodel =
            ViewModelProviders.of(this).get(DtLoanGpTxnViewmodel::class.java)
        lookupViewmodel =
            ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        dtLoanGpViewmodel =
            ViewModelProviders.of(this).get(DtLoanGpViewmodel::class.java)
        loanProductViewmodel = ViewModelProviders.of(this).get(LoanProductViewModel::class.java)
        img_Add.setOnClickListener {
            var intent = Intent(this, CutOffGroupLoanReceivedActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        replaceFragmenty(
            fragment = MeetingTopBarZeroFragment(9),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        setLabelText()
        fillRecyclerView()

    }
    fun setFundName(source:Int,fundTypeId:Int):String{
        var sFund = ""
        var dataspin_fund =
            loanProductViewmodel.getFundTypeBySource_Receipt(source,2)
        sFund = validate!!.setFundName(fundTypeId,dataspin_fund)
        return sFund
    }
    fun setLabelText() {
        tv_disbursement_date.text = LabelSet.getText(
            "disbursement_date_new",
            R.string.disbursement_date_new)
        tv_loan_source.text = LabelSet.getText(
            "loan_source_new",
            R.string.loan_source_new)
        tv_fund_type.text = LabelSet.getText(
            "fund_type_new",
            R.string.fund_type_new)
        tv_amount_disbursed.text = LabelSet.getText(
            "amount_disbursed_new",
            R.string.amount_disbursed_new)

    }

    private fun fillRecyclerView() {
        today = 0
        paid = 0
        nextpayable = 0
        var list = dtLoanGpViewmodel.getListDataByMtgnum(
            validate!!.RetriveSharepreferenceInt(
                MeetingSP.currentmeetingnumber
            ),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        var repaymentDetailAdapter = CutOffGroupLoanListAdapter(this, list)

        rvList.layoutManager = LinearLayoutManager(this)

        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = rvList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvList.layoutParams = params
        rvList.adapter = repaymentDetailAdapter
    }

    fun returnLookupValue( flag: Int, id:Int): String {


            var listdata = lookupViewmodel.getlookup(
                flag,
                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
            )
            return validate!!.returnlookupcodevalue(id, listdata)


    }

    fun getTotalValue(iValue: Int, flag: Int) {
        if (flag == 1) {
            today = today + iValue
            tv_value1.text = today.toString()
        } else if (flag == 2) {
            paid = paid + iValue
            tv_value2.text = paid.toString()
        } else if (flag == 3) {
            nextpayable = nextpayable + iValue
            tv_value3.text = nextpayable.toString()
        }
    }

    override fun onBackPressed() {
        var intent = Intent(this, CutOffMeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }
}