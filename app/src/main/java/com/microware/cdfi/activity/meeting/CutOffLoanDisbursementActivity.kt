package com.microware.cdfi.activity.meeting

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.api.meetingmodel.FundTypeModel
import com.microware.cdfi.entity.*
import com.microware.cdfi.fragment.MeetingTopBarZeroFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.layout_cutoff_loan_disbursement.*

class CutOffLoanDisbursementActivity : AppCompatActivity() {

    var lookupViewmodel: LookupViewmodel? = null
    var validate: Validate? = null
    var shgBankList: List<Cbo_bankEntity>? = null
    var dataspin_loan_type: List<LookupEntity>? = null
    var dataspin_loan_source: List<LookupEntity>? = null
    var dataspin_modeofpayment: List<LookupEntity>? = null
    var dtLoanMemberEntity: DtLoanMemberEntity? = null
    var dtLoanTxnMemEntity: DtLoanTxnMemEntity? = null
    var dtLoanMemberSheduleEntity: DtLoanMemberSheduleEntity? = null
    var dtLoanTxnMemViewmodel: DtLoanTxnMemViewmodel? = null
    var dtLoanMemberViewmodel: DtLoanMemberViewmodel? = null
    var dtLoanMemberScheduleViewmodel: DtLoanMemberScheduleViewmodel? = null
    var dataspin_frequency: List<LookupEntity>? = null
    var memberlist: List<DtmtgDetEntity>? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    var loanpurpose = 0
    var dtLoanViewmodel: DtLoanViewmodel? = null
    lateinit var loanProductViewmodel: LoanProductViewModel
    var dataspin_fund: List<FundTypeModel>? = null
    var nullStringValue:String?=null
    var nullLongValue:Long?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_cutoff_loan_disbursement)

        validate = Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        dtLoanTxnMemViewmodel = ViewModelProviders.of(this).get(DtLoanTxnMemViewmodel::class.java)
        loanProductViewmodel = ViewModelProviders.of(this).get(LoanProductViewModel::class.java)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        dtLoanViewmodel = ViewModelProviders.of(this).get(DtLoanViewmodel::class.java)
        dtLoanMemberViewmodel = ViewModelProviders.of(this).get(DtLoanMemberViewmodel::class.java)
        dtLoanMemberScheduleViewmodel =
            ViewModelProviders.of(this).get(DtLoanMemberScheduleViewmodel::class.java)
        setLabelText()

        et_no_of_installment.filters = arrayOf(InputFilterMinMax (1, 60))
        btn_cancel.setOnClickListener {
            var intent = Intent(this, CutOffLoanDisbursementListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        }
        btn_save.setOnClickListener {
            //var countloanno=dtLoanViewmodel!!.getmaxloanno(validate!!.RetriveSharepreferenceLong(MeetingSP.Shgcode))
            if (checkValidation() == 1) {
                saveData()
                var loanAmount = validate!!.returnIntegerValue(et_amount.text.toString()) - validate!!.returnIntegerValue(et_principal_overdue.text.toString())
                insertscheduler(
                    loanAmount,
                    validate!!.returnIntegerValue(et_no_of_installment.text.toString()))
            }
        }

        et_date_loan_taken.setOnClickListener {
            validate!!.datePicker(et_date_loan_taken)
        }

        et_no_of_installment.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = validate!!.returnIntegerValue(et_amount.text.toString()) - validate!!.returnIntegerValue(et_principal_overdue.text.toString())
                var installment_num = validate!!.returnIntegerValue(et_no_of_installment.text.toString())
                if (installment_num > 0 && value > 0) {
                    val installmentamt = value / installment_num
                    et_amount_of_installment.setText(installmentamt.toString())
                }
            }

        })

        et_principal_overdue.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = validate!!.returnIntegerValue(et_interest_overdue.text.toString())

                val total_overdue = value + validate!!.returnIntegerValue(et_principal_overdue.text.toString())
                et_overdue.setText(total_overdue.toString())


            }

        })

        et_interest_overdue.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = validate!!.returnIntegerValue(et_principal_overdue.text.toString())

                val total_overdue = value + validate!!.returnIntegerValue(et_interest_overdue.text.toString())
                et_overdue.setText(total_overdue.toString())
            }

        })
        et_amount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = validate!!.returnIntegerValue(et_amount.text.toString()) - validate!!.returnIntegerValue(et_principal_overdue.text.toString())
                var installment_num = validate!!.returnIntegerValue(et_no_of_installment.text.toString())
                if (installment_num > 0 && value > 0) {
                    val installmentamt = value / installment_num
                    et_amount_of_installment.setText(installmentamt.toString())
                }

            }

        })

        et_original_loan_amount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
              var outstandingPrincipal = validate!!.returnIntegerValue(et_original_loan_amount.text.toString()) -
                      validate!!.returnIntegerValue(et_principal_repaid.text.toString())
                et_amount.setText(validate!!.returnStringValue(outstandingPrincipal.toString()))
            }

        })
        et_principal_repaid.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
              var outstandingPrincipal = validate!!.returnIntegerValue(et_original_loan_amount.text.toString()) -
                      validate!!.returnIntegerValue(et_principal_repaid.text.toString())
                et_amount.setText(validate!!.returnStringValue(outstandingPrincipal.toString()))
            }

        })
        spin_mode_of_payment?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (validate!!.returnlookupcode(
                        spin_mode_of_payment,
                        dataspin_modeofpayment
                    ) == 2
                ) {
                    lay_bank.visibility = View.VISIBLE
                    lay_chequeNO.visibility = View.VISIBLE
                } else {
                    lay_bank.visibility = View.GONE
                    lay_chequeNO.visibility = View.GONE
                    spin_source_bank.setSelection(0)

                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }
        // insertMasterdata()
        fillSpinner()
        fillmemberspinner()
        fillbank()
        fillproduct()

        fillCutOffLoandata()

        replaceFragmenty(
            fragment = MeetingTopBarZeroFragment(5),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )
    }

    private fun fillmemberspinner() {
        memberlist = generateMeetingViewmodel.getListDataMtgByMtgnum(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        val adapter: ArrayAdapter<String?>
        if (!memberlist.isNullOrEmpty()) {
            val isize = memberlist!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in memberlist!!.indices) {
                sValue[i + 1] = memberlist!![i].member_name
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_member.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(memberlist!!.size + 1)
            sValue[0] = LabelSet.getText(
                "nomemberavailable",
                R.string.nomemberavailable
            )
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_member.adapter = adapter
        }
        setMember()

    }


    fun returnmemid(): Long {

        var pos = spin_member.selectedItemPosition
        var id = 0L

        if (!memberlist.isNullOrEmpty()) {
            if (pos > 0) id = memberlist!!.get(pos - 1).mem_id
        }
        return id
    }

    fun setMember(): Int {
        var pos = 0
        var name = ""
        if (!memberlist.isNullOrEmpty()) {
            for (i in memberlist!!.indices) {
                if (validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid) == memberlist!!.get(i).mem_id) {
                    pos = i + 1
                    name = memberlist!!.get(i).member_name!!
                }

            }
        }
        tv_name.text = name
        tv_number.text = validate!!.RetriveSharepreferenceLong(MeetingSP.MemberCode).toString()
        spin_member.setSelection(pos)
        spin_member.isEnabled = false
        return pos
    }

    override fun onBackPressed() {
//        var intent = Intent(this, PeneltyListActivity::class.java)
        var intent = Intent(this, CutOffLoanDisbursementListActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

    fun setLabelText() {
        tv_source_bank.text = LabelSet.getText(
            "name_of_bank",
            R.string.name_of_bank
        )

        tv_amount.text = LabelSet.getText(
            "outstanding_principal_loan",
            R.string.outstanding_principal_loan
        )
        tv_original_loan_amount.text = LabelSet.getText(
            "disubursed_loan_amt",
            R.string.disubursed_loan_amt
        )
        tv_principal_overdue.text = LabelSet.getText(
            "principal_overdue",
            R.string.principal_overdue
        )
        tv_interest_overdue.text = LabelSet.getText(
            "interest_overdue_any",
            R.string.interest_overdue_any
        )
        tvprincipal_repaid.text = LabelSet.getText(
            "principal_repaid",
            R.string.principal_repaid
        )
        tv_interest_paid.text = LabelSet.getText(
            "interest_paid",
            R.string.interest_paid
        )
        tv_overdue.text = LabelSet.getText(
            "total_overdue",
            R.string.total_overdue
        )
        tv_disbursement_date.text = LabelSet.getText(
            "loan_disbursement_date",
            R.string.loan_disbursement_date
        )
        tv_frequency.text = LabelSet.getText(
            "loan_repayment_frequency",
            R.string.loan_repayment_frequency
        )
        tv_loan_source.text = LabelSet.getText(
            "purpose",
            R.string.purpose
        )

        et_amount.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_principal_overdue.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_principal_repaid.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_interest_paid.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_interest_overdue.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_overdue.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )

        tv_period_months.text = LabelSet.getText(
            "period_months",
            R.string.period_months
        )
        tv_mode_of_payment.text = LabelSet.getText(
            "mod_of_payment",
            R.string.mod_of_payment
        )
        et_no_of_installment.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_amount_of_installment.text = LabelSet.getText(
            "amount_of_installment",
            R.string.amount_of_installment
        )
        et_amount_of_installment.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_monthly_interest_rate.text = LabelSet.getText(
            "interest_rate",
            R.string.interest_rate
        )
        et_monthly_interest_rate.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        // tv_loan_type.setText(LabelSet.getText("loan_type",R.string.loan_type))

        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
        btn_savegray.text = LabelSet.getText(
            "save",
            R.string.save
        )
        //tv_title.setText(LabelSet.getText("loan_disbursement",R.string.loan_disbursement))

    }

    private fun fillSpinner() {


        dataspin_loan_type = lookupViewmodel!!.getlookup(
            62,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_frequency = lookupViewmodel!!.getlookup(
            19,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_modeofpayment = lookupViewmodel!!.getlookup(
            61,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_loan_source = lookupViewmodel!!.getlookup(
            67,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        // validate!!.fillspinner(this, spin_loan_product, dataspin_loan_product)
        validate!!.fillspinner(this, spin_purpose, dataspin_loan_source)
        validate!!.fillspinner(this, spin_mode_of_payment, dataspin_modeofpayment)
        validate!!.fillspinner(this, spin_frequency, dataspin_frequency)
        spin_frequency.setSelection(3)
        spin_frequency.isEnabled = false
    }

    fun fillbank(
    ) {
        shgBankList =
            generateMeetingViewmodel.getBankdata(validate!!.RetriveSharepreferenceString(MeetingSP.SHGGUID))

        val adapter: ArrayAdapter<String?>
        if (!shgBankList.isNullOrEmpty()) {
            val isize = shgBankList!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in shgBankList!!.indices) {
                var lastthree=shgBankList!![i].account_no.substring(shgBankList!![i].account_no.length-3)
                sValue[i + 1] =
                    shgBankList!![i].ifsc_code.toString().dropLast(7) +"XXXXX"+ lastthree
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_source_bank.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_source_bank.adapter = adapter
        }

    }

    fun returaccount(): String {

        var pos = spin_source_bank.selectedItemPosition
        var id = ""

        if (!shgBankList.isNullOrEmpty()) {
            if (pos > 0) id = shgBankList!!.get(pos - 1).ifsc_code!!.dropLast(7)+ shgBankList!!.get(
                pos - 1
            ).account_no
        }
        return id
    }

    fun setaccount(accountno: String): Int {

        var pos = 0

        if (!shgBankList.isNullOrEmpty()) {
            for (i in shgBankList!!.indices) {
                if (accountno.equals(shgBankList!!.get(i).ifsc_code!!.dropLast(7)+shgBankList!!.get(i).account_no))
                    pos = i + 1
            }
        }
        return pos
    }

    private fun checkValidation(): Int {

        var totalPrincipal = validate!!.returnIntegerValue(et_amount.text.toString()) +
                validate!!.returnIntegerValue(et_principal_repaid.text.toString())
        var value = 1
        if (spin_member.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_member,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "member",
                    R.string.member
                )

            )
            value = 0
            return value
        }
        if(spin_loan_type.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_loan_type,
                LabelSet.getText(
                    "please_select_fund_type",
                    R.string.please_select_fund_type
                ) )
            value = 0
            return value
        }
        if (validate!!.returnIntegerValue(et_original_loan_amount.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "enter_principal_outstanding",
                    R.string.enter_original_loan_amount
                ), this,
                et_original_loan_amount
            )
            value = 0
            return value
        }
        if(validate!!.returnIntegerValue(et_original_loan_amount.text.toString())<= validate!!.returnIntegerValue(et_principal_repaid.text.toString())){
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "compare_principal_repaid_amount_disbursed",
                    R.string.compare_principal_repaid_amount_disbursed
                ), this,
                et_principal_repaid
            )
            value = 0
            return value
        }
        if (validate!!.returnIntegerValue(et_amount.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "enter_principal_outstanding",
                    R.string.enter_principal_outstanding
                ), this,
                et_amount
            )
            value = 0
            return value
        }
        if (validate!!.returnIntegerValue(et_original_loan_amount.text.toString()) != totalPrincipal) {
            validate!!.CustomAlert(
                LabelSet.getText(
                    "total_principal_disbursed",
                    R.string.total_principal_disbursed
                ), this)
            value = 0
            return value
        }
        if (validate!!.returnDoubleValue(et_monthly_interest_rate.text.toString()) == 0.0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "enter_interest_rate",
                    R.string.enter_interest_rate
                ), this,
                et_monthly_interest_rate
            )
            value = 0
            return value
        }

        if (validate!!.returnDoubleValue(et_monthly_interest_rate.text.toString()) > 36) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "valid_interest",
                    R.string.valid_interest
                ), this,
                et_monthly_interest_rate
            )
            value = 0
            return value
        }
        if (validate!!.returnStringValue(et_date_loan_taken.text.toString()).length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "select_disbursement_date",
                    R.string.select_disbursement_date
                ), this,
                et_date_loan_taken
            )
            value = 0
            return value
        }
        if (validate!!.returnlookupcode(spin_mode_of_payment, dataspin_modeofpayment)==1&&validate!!.returnIntegerValue(et_amount.text.toString()) > validate!!.RetriveSharepreferenceInt(
                MeetingSP.Cashinhand
            )
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "inthecashbox",
                    R.string.inthecashbox
                ), this,
                et_amount
            )
            value = 0
            return value
        }
        if (validate!!.returnIntegerValue(et_no_of_installment.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "pleaseenterintallment",
                    R.string.pleaseenterintallment
                ), this,
                et_no_of_installment
            )
            value = 0
            return value
        }
        if (validate!!.returnIntegerValue(et_loan_no.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "pleaseenterloanno",
                    R.string.pleaseenterloanno
                ), this,
                et_loan_no
            )
            value = 0
            return value
        }
        var cashinbank= generateMeetingViewmodel.getclosingbal(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), returaccount())
        if(lay_bank.visibility == View.VISIBLE &&cashinbank<validate!!.returnIntegerValue(et_amount.text.toString())){
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "intheselectedbank",
                    R.string.intheselectedbank
                ), this,
                et_amount
            )
            value = 0
            return value
        }
        if (lay_chequeNO.visibility == View.VISIBLE && validate!!.returnStringValue(
                et_cheque_no_transactio_no.text.toString()
            ).length == 0
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + LabelSet.getText(
                    "cheque_no_transactio_no",
                    R.string.cheque_no_transactio_no
                ), this,
                et_cheque_no_transactio_no
            )
            value = 0
            return value
        }
        return value
    }


    private fun fillCutOffLoandata() {

        var loanlist =
            dtLoanMemberViewmodel!!.getLoanDetailData(validate!!.RetriveSharepreferenceInt(MeetingSP.Loanno),validate!!.RetriveSharepreferenceLong(
                MeetingSP.shgid),validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid),validate!!.RetriveSharepreferenceString(
                MeetingSP.mtg_guid)!!)
        if (!loanlist.isNullOrEmpty()) {
            et_loan_no.setText(loanlist.get(0).loan_no.toString())
            et_no_of_installment.setText(validate!!.returnStringValue(loanlist.get(0).period.toString()))
            et_monthly_interest_rate.setText(validate!!.returnStringValue(loanlist.get(0).interest_rate.toString()))
            et_moratorium_period.setText(validate!!.returnStringValue(loanlist.get(0).moratorium_period.toString()))
            et_original_loan_amount.setText(validate!!.returnStringValue(loanlist.get(0).original_loan_amount.toString()))
            et_amount.setText(validate!!.returnStringValue(loanlist.get(0).amount.toString()))
            et_principal_repaid.setText(validate!!.returnStringValue(loanlist.get(0).principal_repaid.toString()))
            et_principal_overdue.setText(validate!!.returnStringValue(loanlist.get(0).principal_overdue.toString()))
            et_interest_overdue.setText(validate!!.returnStringValue(loanlist.get(0).interest_overdue.toString()))
            et_interest_paid.setText(validate!!.returnStringValue(loanlist.get(0).interest_repaid.toString()))
            et_date_loan_taken.setText(validate!!.convertDatetime(validate!!.returnLongValue(loanlist.get(0).disbursement_date.toString())))

            if(validate!!.returnIntegerValue(loanlist.get(0).period.toString())>0) {
                var installment_Amount =
                    (validate!!.returnIntegerValue(loanlist.get(0).amount.toString())-validate!!.returnIntegerValue(
                        loanlist.get(0).principal_overdue.toString())) / validate!!.returnIntegerValue(
                        loanlist.get(0).period.toString()
                    )
                et_amount_of_installment.setText(validate!!.returnStringValue(installment_Amount.toString()))
            }
            if(validate!!.returnIntegerValue(loanlist.get(0).period.toString())>0) {
                var overdue_Amount =
                    validate!!.returnIntegerValue(loanlist.get(0).principal_overdue.toString()) + validate!!.returnIntegerValue(
                        loanlist.get(0).interest_overdue.toString()
                    )
                et_overdue.setText(validate!!.returnStringValue(overdue_Amount.toString()))
            }
            spin_purpose.setSelection(
                validate!!.returnlookupcodepos(validate!!.returnIntegerValue(
                    loanlist.get(0).loan_purpose.toString()), dataspin_loan_source))
            spin_mode_of_payment.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(
                        loanlist.get(0).modepayment.toString()
                    ), dataspin_modeofpayment
                )
            )
            spin_frequency.setSelection(validate!!.returnlookupcodepos(loanlist.get(0).installment_freq,dataspin_frequency))
            spin_source_bank.setSelection(
                setaccount(
                    loanlist.get(0).bank_code.toString()

                )
            )
            spin_loan_type.setSelection(setproduct(validate!!.returnIntegerValue(loanlist.get(0).loan_type.toString())))
            btn_savegray.visibility = View.VISIBLE
            btn_save.visibility = View.GONE
        } else {
            spin_frequency.setSelection(validate!!.returnlookupcodepos(3,dataspin_frequency))
            spin_mode_of_payment.setSelection(validate!!.returnlookupcodepos(1, dataspin_modeofpayment))
            et_date_loan_taken.setText(validate!!.convertDatetime(
                validate!!.returnLongValue(validate!!.RetriveSharepreferenceLong(
                    MeetingSP.CurrentMtgDate).toString())))
            var loanno = dtLoanMemberViewmodel!!.getCutOffmaxLoanno(validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber))
            et_loan_no.setText((loanno + 1).toString())
          /*  var loanno = dtLoanMemberViewmodel!!.getmaxLoanno(
                validate!!.RetriveSharepreferenceLong(
                    MeetingSP.shgid
                )
            )
            et_loan_no.setText(
                (loanno + 1)
                    .toString()
            )*/
            btn_savegray.visibility = View.GONE
            btn_save.visibility = View.VISIBLE
        }

    }

    private fun saveData() {
        var loan_application_id:Long? = null
        dtLoanMemberEntity = DtLoanMemberEntity(
            0,
            loan_application_id,
            validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            returnmemid(),
            validate!!.returnIntegerValue(et_loan_no.text.toString()),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            validate!!.addmonth(validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),0,validate!!.returnlookupcode(spin_frequency, dataspin_frequency)),
            validate!!.returnIntegerValue(et_original_loan_amount.text.toString()),
            validate!!.returnIntegerValue(et_amount.text.toString()),
            validate!!.returnlookupcode(spin_purpose, dataspin_loan_source),
            returnid(),
            validate!!.returnDoubleValue(et_monthly_interest_rate.text.toString()),
            validate!!.returnIntegerValue(et_no_of_installment.text.toString()),
            validate!!.returnIntegerValue(et_principal_overdue.text.toString()),
            validate!!.returnIntegerValue(et_interest_overdue.text.toString()),
            false,
            returnid(),
            0,
            validate!!.returnlookupcode(spin_mode_of_payment, dataspin_modeofpayment),
            returaccount(),
            validate!!.returnStringValue(et_cheque_no_transactio_no.text.toString()),
            validate!!.returnlookupcode(spin_frequency, dataspin_frequency),
            0,
            validate!!.returnStringValue(et_loan_no.text.toString()),
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            nullStringValue,
            nullLongValue,
            nullStringValue,
            nullLongValue,
            validate!!.returnIntegerValue(et_principal_repaid.text.toString()),
            validate!!.returnIntegerValue(et_interest_paid.text.toString()),
            validate!!.Daybetweentime(et_date_loan_taken.text.toString()),
            1,0.0,0,0,1


        )
        dtLoanMemberViewmodel!!.insert(dtLoanMemberEntity!!)

        // insert in dtlonmemtxn

        dtLoanTxnMemEntity = DtLoanTxnMemEntity(
            0,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            returnmemid(),
            validate!!.returnIntegerValue(et_loan_no.text.toString()),
            validate!!.returnIntegerValue(et_original_loan_amount.text.toString()),
            0, validate!!.returnIntegerValue(et_principal_repaid.text.toString()),
            validate!!.returnIntegerValue(et_interest_paid.text.toString()),
            validate!!.returnIntegerValue(et_amount.text.toString()),
            validate!!.returnIntegerValue(et_interest_paid.text.toString()),
            false,
            0,
            validate!!.returnIntegerValue(et_interest_overdue.text.toString()),
            validate!!.returnIntegerValue(et_interest_overdue.text.toString()),
            0,
            validate!!.returnIntegerValue(et_principal_overdue.text.toString()),
            validate!!.returnIntegerValue(et_principal_overdue.text.toString()),
            validate!!.returnlookupcode(spin_mode_of_payment, dataspin_modeofpayment),
            returaccount(),
            validate!!.returnStringValue(et_cheque_no_transactio_no.text.toString()),
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            nullStringValue,
            nullLongValue,
            nullStringValue,
            nullLongValue,0.0,0
        )
        dtLoanTxnMemViewmodel!!.insert(dtLoanTxnMemEntity!!)

    }

    private fun insertscheduler(amt: Int, installment: Int) {
        var principaldemand = amt / installment
        var loanos = 0
        var saveValue = 0
        var memid = returnmemid()
        for (i in 0 until installment) {
            if (i == installment - 1) {
                loanos = 0
                principaldemand = amt - (principaldemand * i)
            } else {
                loanos = amt - (principaldemand * (i + 1))
            }
            dtLoanMemberSheduleEntity = DtLoanMemberSheduleEntity(
                0,
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                memid,
                validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
                validate!!.returnIntegerValue(et_loan_no.text.toString()),
                principaldemand,
                principaldemand,
                loanos, 0,
                i + 1,
                1,
                validate!!.addmonth(
                    validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
                    (0 + i + 1),validate!!.returnlookupcode(spin_frequency, dataspin_frequency)),
                //   validate!!.Daybetweentime(validate!!.currentdatetime),
                0,
                0,
                null,

                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime),  nullStringValue,
                nullLongValue,
                nullStringValue,
                nullLongValue


            )
            dtLoanMemberScheduleViewmodel!!.insert(dtLoanMemberSheduleEntity!!)

        }

            var principalDemand =
                dtLoanMemberScheduleViewmodel!!.getPrincipalDemandByInstallmentNum(
                    memid,
                    validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
                    validate!!.returnIntegerValue(et_loan_no.text.toString()),
                    1
                )
           var totalprincipalDemand = principalDemand + validate!!.returnIntegerValue(et_principal_overdue.text.toString())

            dtLoanMemberScheduleViewmodel!!.updateCutOffLoanMemberSchedule(
                validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
                memid,
                validate!!.returnIntegerValue(et_loan_no.text.toString()),
                1,
                totalprincipalDemand
            )

        CustomAlertchangeschedule(
            LabelSet.getText(
                "doyouwanttochaneschedule",
                R.string.doyouwanttochaneschedule
            )
        )
    }

    fun CustomAlertchangeschedule(str: String) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str
        mDialogView.btn_yes.setOnClickListener {
            //            PartialSaveData()
            mAlertDialog.dismiss()
            validate!!.SaveSharepreferenceLong(MeetingSP.Memberid, returnmemid())
            validate!!.SaveSharepreferenceInt(
                MeetingSP.Loanno,
                validate!!.returnIntegerValue(et_loan_no.text.toString())
            )
            validate!!.SaveSharepreferenceString(
                MeetingSP.MemberName,
                validate!!.returnStringValue(tv_name.text.toString())
            )

            val intent = Intent(this, PrincipalDemandActivity::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)


        }
        mDialogView.btn_no.setOnClickListener {
            mAlertDialog.dismiss()

            val intent = Intent(this, CutOffLoanDisbursementListActivity::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)

        }
    }

    fun fillproduct(
    ) {
        dataspin_fund =
            loanProductViewmodel.getFundTypeBySource_Receipt(2,1)

        val adapter: ArrayAdapter<String?>
        if (!dataspin_fund.isNullOrEmpty()) {
            val isize = dataspin_fund!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in dataspin_fund!!.indices) {
                sValue[i + 1] =
                    dataspin_fund!![i].fundType
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_loan_type.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_loan_type.adapter = adapter
        }

    }

    fun returnid(): Int {

        var pos = spin_loan_type.selectedItemPosition
        var id = 0

        if (!dataspin_fund.isNullOrEmpty()) {
            if (pos > 0) id = dataspin_fund!!.get(pos - 1).fundTypeId
        }
        return id
    }

    fun setproduct(id: Int): Int {

        var pos = 0

        if (!dataspin_fund.isNullOrEmpty()) {
            for (i in dataspin_fund!!.indices) {
                if (dataspin_fund!!.get(i).fundTypeId == id)
                    pos = i + 1
            }
        }
        return pos
    }
}