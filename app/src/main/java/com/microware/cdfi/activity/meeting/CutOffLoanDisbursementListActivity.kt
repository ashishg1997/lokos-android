package com.microware.cdfi.activity.meeting

import android.content.Intent
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.CutOffLoanDisbursementAdapter
import com.microware.cdfi.fragment.MeetingTopBarZeroFragment
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.replaceFragmenty
import com.microware.cdfi.viewModel.DtLoanMemberScheduleViewmodel
import com.microware.cdfi.viewModel.DtLoanMemberViewmodel
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import kotlinx.android.synthetic.main.layout_cutoff_loan_disbursement_list.*

class CutOffLoanDisbursementListActivity : AppCompatActivity() {
    var validate: Validate? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    var dtLoanMemberViewmodel: DtLoanMemberViewmodel? = null
    var dtLoanMemberScheduleViewmodel: DtLoanMemberScheduleViewmodel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_cutoff_loan_disbursement_list)

        validate = Validate(this)
        dtLoanMemberScheduleViewmodel =
            ViewModelProviders.of(this).get(DtLoanMemberScheduleViewmodel::class.java)
        dtLoanMemberViewmodel =
            ViewModelProviders.of(this).get(DtLoanMemberViewmodel::class.java)

        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)

        btn_cancel.setOnClickListener {
            var intent = Intent(this, CutOffMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        img_Add.setOnClickListener {

            validate!!.SaveSharepreferenceLong(MeetingSP.Memberid, 0)
            validate!!.SaveSharepreferenceLong(MeetingSP.MemberCode, 0)
            validate!!.SaveSharepreferenceInt(MeetingSP.Loanno,0)

            var intent = Intent(this, CutOffLoanDisbursementActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        replaceFragmenty(
            fragment = MeetingTopBarZeroFragment(5),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        setLabelText()
        fillRecyclerView()

    }

    override fun onBackPressed() {

            var intent = Intent(this, CutOffMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()

    }

    fun setLabelText() {

        tv_srno.visibility = View.VISIBLE
        tv_laon_outstanding.visibility = View.VISIBLE
        tv_srno.text = LabelSet.getText("sr_no", R.string.sr_no)
        tv_member_name.text = LabelSet.getText(
            "member_namenew",
            R.string.member_namenew
        )
        tv_laon_outstanding.text = LabelSet.getText(
            "outstanding_principal",
            R.string.outstanding_principal
        )
        tv_total_demand.text = LabelSet.getText(
            "total_demand",
            R.string.total_demand
        )
        tv_total_disbursed.text = LabelSet.getText(
            "total_disbursednew",
            R.string.total_disbursednew
        )
        tv_total.text = LabelSet.getText("total", R.string.total)
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }

    private fun fillRecyclerView() {
        var list = generateMeetingViewmodel.getCutOffloanListDataByMtgnum(
                validate!!.RetriveSharepreferenceInt(
                    MeetingSP.currentmeetingnumber
                ),validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),false)


        rvList.layoutManager = LinearLayoutManager(this)
        var loanDisbursementAdapter =
            CutOffLoanDisbursementAdapter(this, list)
        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = rvList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvList.layoutParams = params
        rvList.adapter = loanDisbursementAdapter

    }

    fun getTotalValue() {
        var iValue1 = 0
        var iValue2 = 0
        var iValue3 = 0
        val iCount = rvList.childCount


        for (i in 0 until iCount) {
            val gridChild =
                rvList.getChildAt(i) as? ViewGroup

            val tv_value1 = gridChild!!
                .findViewById<View>(R.id.tv_value1) as? TextView

            val tv_value2 = gridChild
                .findViewById<View>(R.id.tv_value2) as? TextView
            val tv_value3 = gridChild
                .findViewById<View>(R.id.tv_value3) as? TextView

            if (!tv_value1!!.text.toString().isNullOrEmpty()) {
                iValue1 = iValue1 + validate!!.returnIntegerValue(tv_value1.text.toString())
            }

            if (!tv_value2!!.text.toString().isNullOrEmpty()) {
                iValue2 = iValue2 + validate!!.returnIntegerValue(tv_value2.text.toString())
            }
            if (!tv_value3!!.text.toString().isNullOrEmpty()) {
                iValue3 = iValue3 + validate!!.returnIntegerValue(tv_value3.text.toString())
            }


        }
        tv_today_demand_total.text = iValue1.toString()
        tv_paid_total.text = iValue2.toString()
        tv_next_demand_total.text = iValue3.toString()

    }


    fun gettotalloanamt(loanno: Int,memid: Long, mtgguid: String): Int {
        return dtLoanMemberScheduleViewmodel!!.gettotalloanamt(loanno,memid, mtgguid)
    }

    fun getLoanno(appid: Long): Int {
        return dtLoanMemberViewmodel!!.getLoanno(appid)
    }

}