package com.microware.cdfi.activity.meeting

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.speech.tts.UtteranceProgressListener
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.DtLoanMemberViewmodel
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import kotlinx.android.synthetic.main.activity_cut_off_meeting_detail_ttsmain.*
import kotlinx.android.synthetic.main.repay_toolbar.*
import java.util.*

class CutOffMeetingDetailTTSMainActivity : AppCompatActivity(), TextToSpeech.OnInitListener {

    private val REQ_TTS_STATUS_CHECK = 0
    private val TAG = "TTS Demo"
    private var mTts: TextToSpeech? = null
    private var uttCount = 0
    private var lastUtterance = -1
    private val params = HashMap<String, String>()
    var languageCode: String = ""
    var finalParagraphString: String? = null

    var meetno =  0
    var SHG_name = ""
    var MeetingDate = ""
    var totalMember = ""
    var presentMember = ""
    var investAmount = ""
    var borrowAmount = ""
    var savingAmount = ""
    var cashBalance = ""
    var bankBalance = ""
    var summaryData = ""
    var closedLoan_amount = ""
    var activeLoan_amount = ""
    var share_capital_amount = ""
    var dtLoanMemberViewmodel: DtLoanMemberViewmodel? = null

    var sentence1 = ""
    var sentence2 = ""
    var sentence3 = ""
    var sentence4 = ""
    var sentence5 = ""
    var sentence6 = ""
    var sentence7 = ""
    var sentence8 = ""
    var sentence9 = ""
    var sentence10 = ""
    var sentence11 = ""
    var sentence12 = ""
    var sentence13 = ""
    var sentence14 = ""
    var sentence15 = ""
    var sentence21 = ""

    var savingData = ""
    var closeLoanData = ""
    var activeLoanData = ""
    var sharecapitalData = ""
    var invest_borrowData = ""
    var bankData = ""
    var cashData = ""

    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    var validate: Validate? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cut_off_meeting_detail_ttsmain)

        var intentData = intent
        SHG_name = intentData.getStringExtra("SHG_name").toString()
        MeetingDate = intentData.getStringExtra("MeetingDate").toString()
        totalMember = intentData.getStringExtra("totalMember").toString()
        savingAmount = intentData.getStringExtra("savingAmount").toString()
        closedLoan_amount = intentData.getStringExtra("closedLoan_amount").toString()
        activeLoan_amount = intentData.getStringExtra("activeLoan_amount").toString()
        share_capital_amount = intentData.getStringExtra("share_capital_amount").toString()
        investAmount = intentData.getStringExtra("recepient_and_income_amount").toString()
        borrowAmount = intentData.getStringExtra("borrowAmount").toString()
        cashBalance = intentData.getStringExtra("cashBalance").toString()
        bankBalance = intentData.getStringExtra("bankBalance").toString()
        tv_shg_name.text = SHG_name
        tv_shg_date.text = "Date : " + MeetingDate
        tv_title.text = "Meeting Information"

        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        validate = Validate(this)
        languageCode = validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
        dtLoanMemberViewmodel = ViewModelProviders.of(this).get(DtLoanMemberViewmodel::class.java)


        val checkIntent = Intent()
        checkIntent.action = TextToSpeech.Engine.ACTION_CHECK_TTS_DATA
        startActivityForResult(checkIntent, REQ_TTS_STATUS_CHECK)
        languageCode = CDFIApplication.validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!

        getMeetingSummaryData()

        ic_Back.setOnClickListener {
            onBackPressed()
        }

        tv_summary_play1.setOnClickListener {
            if (mTts != null) {
                mTts!!.stop()

                var testdata = finalParagraphString!!.replace("\n", "")
                if (testdata.isNullOrEmpty()) {
                    doSpeak(sentence21)
                    alertDialog("Overview", "\n\n" + sentence21)
                } else {
                    doSpeak(finalParagraphString!!)
                    finalParagraphString = finalParagraphString!!.replace("=", "")
                    finalParagraphString = finalParagraphString!!.replace("'", "")
                    alertDialog("Overview", finalParagraphString!!)
                }
            }
        }

        iv_savings.setOnClickListener {
            if (mTts != null) {
                mTts!!.stop()

                var testdata = savingData.replace("\n", "")
                if (testdata.isNullOrEmpty()) {
                    doSpeak(sentence21)
                    alertDialog("Total Saving", "\n\n" + sentence21)
                } else {
                    doSpeak(savingData)
                    savingData = savingData.replace("=", "")
                    savingData = savingData.replace("'", "")
                    alertDialog("Total Saving", savingData)
                }
            }
        }

        iv_closeloan.setOnClickListener {
            if (mTts != null) {
                mTts!!.stop()

                var testdata = closeLoanData.replace("\n", "")
                if (testdata.isNullOrEmpty()) {
                    doSpeak(sentence21)
                    alertDialog("Closed Loan", "\n\n" + sentence21)
                } else {
                    doSpeak(closeLoanData)
                    closeLoanData = closeLoanData.replace("=", "")
                    closeLoanData = closeLoanData.replace("'", "")
                    alertDialog("Closed Loan", closeLoanData)
                }
            }
        }

        iv_activeLoan.setOnClickListener {
            if (mTts != null) {
                mTts!!.stop()

                var testdata = activeLoanData.replace("\n", "")
                if (testdata.isNullOrEmpty()) {
                    doSpeak(sentence21)
                    alertDialog("Active Loan", "\n\n" + sentence21)
                } else {
                    doSpeak(activeLoanData)
                    activeLoanData = activeLoanData.replace("=", "")
                    activeLoanData = activeLoanData.replace("'", "")
                    alertDialog("Active Loan", activeLoanData)
                }
            }
        }

        iv_shareCapital.setOnClickListener {
            if (mTts != null) {
                mTts!!.stop()

                var testdata = sharecapitalData.replace("\n", "")
                if (testdata.isNullOrEmpty()) {
                    doSpeak(sentence21)
                    alertDialog("Share Capital", "\n\n" + sentence21)
                } else {
                    doSpeak(sharecapitalData)
                    sharecapitalData = sharecapitalData.replace("=", "")
                    sharecapitalData = sharecapitalData.replace("'", "")
                    alertDialog("Share Capital", sharecapitalData)
                }
            }
        }

        iv_invest.setOnClickListener {
            if (mTts != null) {
                mTts!!.stop()

                var testdata = invest_borrowData.replace("\n", "")
                if (testdata.isNullOrEmpty()) {
                    doSpeak(sentence21)
                    alertDialog("Invest & Borrow", "\n\n" + sentence21)
                } else {
                    doSpeak(invest_borrowData)
                    invest_borrowData = invest_borrowData.replace("=", "")
                    invest_borrowData = invest_borrowData.replace("'", "")
                    alertDialog("Invest & Borrow", invest_borrowData)
                }
            }
        }

        iv_bankCollection.setOnClickListener {
            if (mTts != null) {
                mTts!!.stop()

                var testdata = bankData.replace("\n", "")
                if (testdata.isNullOrEmpty()) {
                    doSpeak(sentence21)
                    alertDialog("Bank Collection", "\n\n" + sentence21)
                } else {
                    doSpeak(bankData)
                    bankData = bankData.replace("=", "")
                    bankData = bankData.replace("'", "")
                    alertDialog("Bank Collection", bankData)
                }
            }
        }


        iv_cashCollection.setOnClickListener {
            if (mTts != null) {
                mTts!!.stop()

                var testdata = cashData.replace("\n", "")
                if (testdata.isNullOrEmpty()) {
                    doSpeak(sentence21)
                    alertDialog("Cash Collection", "\n\n" + sentence21)
                } else {
                    doSpeak(cashData)
                    cashData = cashData.replace("=", "")
                    cashData = cashData.replace("'", "")
                    alertDialog("Cash Collection", cashData)
                }
            }
        }
    }

    private fun getMeetingSummaryData() {

        var Memberlist = generateMeetingViewmodel.getListDataMtgByMtgnum(
            validate!!.RetriveSharepreferenceInt(
                MeetingSP.currentmeetingnumber
            ),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )

        var CloasedLoanlist = generateMeetingViewmodel.getCutOffClosedMemberLoanDataByMtgNum(
            validate!!.RetriveSharepreferenceInt(
                MeetingSP.currentmeetingnumber
            ), validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), true
        )

        var Activelist = generateMeetingViewmodel.getCutOffloanListDataByMtgnum(
            validate!!.RetriveSharepreferenceInt(
                MeetingSP.currentmeetingnumber
            ), validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), false
        )

        var ShareCapitallist = generateMeetingViewmodel.getShareCapitalListByReceiptType(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(
                MeetingSP.ReceiptType
            )
        )

        meetno = validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber)

        finalParagraphString = MeetingTTS.GetSHGCutOffParagraph(
            meetno,
            languageCode,
            this,
            Memberlist,
            CloasedLoanlist,
            Activelist,
            ShareCapitallist,
            SHG_name,
            MeetingDate,
            savingAmount,
            closedLoan_amount,
            activeLoan_amount,
            share_capital_amount,
            investAmount,
            borrowAmount,
            cashBalance,
            bankBalance
        )

        var splitData = finalParagraphString!!.split("@#@")
        sentence1 = splitData.get(0)
        sentence2 = splitData.get(1)
        sentence3 = splitData.get(2)
        sentence4 = splitData.get(3)
        sentence5 = splitData.get(4)
        sentence6 = splitData.get(5)
        sentence7 = splitData.get(6)
        sentence8 = splitData.get(7)
        sentence9 = splitData.get(8)
        sentence10 = splitData.get(9)
        sentence11 = splitData.get(10)
        sentence12 = splitData.get(11)
        sentence13 = splitData.get(12)
        sentence14 = splitData.get(13)
        sentence15 = splitData.get(14)
        sentence21 = splitData.get(15)

        savingData = "\n\n" + sentence3 + sentence4
        closeLoanData = "\n\n" + sentence5 + sentence6
        activeLoanData = "\n\n" + sentence7 + sentence8
        sharecapitalData = "\n\n" + sentence9 + sentence10
        invest_borrowData = "\n\n" + sentence11 + sentence12
        bankData = "\n\n" + sentence14
        cashData = "\n\n" + sentence15

        finalParagraphString =
            "\n" + sentence1 + sentence2 + sentence3 + sentence4 + sentence5 + sentence6 + sentence7 + sentence8 + sentence9 + sentence10 + sentence11 + sentence12 + sentence13 + sentence14 + sentence15

    }

    fun doSpeak(word: String) {
        val st = StringTokenizer(word.toString(), ",.=")
        while (st.hasMoreTokens()) {
            params[TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID] = uttCount++.toString()
            mTts!!.setPitch(1.toFloat())
            mTts!!.setSpeechRate(0.94.toFloat())
            mTts!!.speak(st.nextToken(), TextToSpeech.QUEUE_ADD, params)
//            mTts!!.setOnUtteranceCompletedListener(object :
//                TextToSpeech.OnUtteranceCompletedListener {
//                override fun onUtteranceCompleted(utteranceId: String?) {
//                    Log.v(TAG, "Got completed message for uttId: $utteranceId")
//                }
//            })

            mTts!!.setOnUtteranceProgressListener(object : UtteranceProgressListener() {
                override fun onStart(utteranceId: String) {

                    if (mTts != null) {
//                        tv_summary_play.text = "STOP"
                    }
                }

                override fun onDone(utteranceId: String) {

                    if (mTts != null && utteranceId.equals((uttCount - 1).toString())) {
                        mTts!!.stop()
                        tv_summary_play1.text = "PLAY"

//                        if (mTts == null){
//
//                        }
                    }
                }

                override fun onError(utteranceId: String) {
                    Log.i("TextToSpeech", "On Error")
                }
            })
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQ_TTS_STATUS_CHECK) {
            when (resultCode) {
                TextToSpeech.Engine.CHECK_VOICE_DATA_PASS -> {
                    mTts = TextToSpeech(this, this)
                }
                TextToSpeech.Engine.CHECK_VOICE_DATA_BAD_DATA, TextToSpeech.Engine.CHECK_VOICE_DATA_MISSING_DATA, TextToSpeech.Engine.CHECK_VOICE_DATA_MISSING_VOLUME -> {
                    val installIntent = Intent()
                    installIntent.action = TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA
                    startActivity(installIntent)
                }
                TextToSpeech.Engine.CHECK_VOICE_DATA_FAIL -> Log.e(TAG, "Got a failure.")
                else -> Log.e(TAG, "Got a failure.")

            }
            val available = data!!.getStringArrayListExtra("availableVoices")
            Log.v("languages count", available!!.size.toString())
            val iter: Iterator<String> = available.iterator()
            while (iter.hasNext()) {
                val lang = iter.next()
                val locale = Locale(lang)
            }
            val locales = Locale.getAvailableLocales()
            Log.v(TAG, "available locales:")
            for (i in locales.indices) Log.v(TAG, "locale: " + locales[i].displayName)
            val defloc = Locale.getDefault()
            Log.v(TAG, "current locale: " + defloc.displayName)
        }
    }

    override fun onInit(status: Int) {
        if (status == TextToSpeech.SUCCESS) {
            mTts!!.language = Locale(getDownloadLanguageISO(languageCode), "IND", "variant")
        }

    }


    override fun onPause() {
        super.onPause()
        if (mTts != null) mTts!!.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mTts != null) mTts!!.shutdown()
    }

    override fun onResume() {
        super.onResume()
        if (mTts != null) {
            mTts!!.stop()
            tv_summary_play1.text = "PLAY"
        }
    }


    private fun alertDialog(titleData: String, paragraphData: String) {
        val mDialogView: View = LayoutInflater.from(this@CutOffMeetingDetailTTSMainActivity)
            .inflate(R.layout.paragraph_text_ui, null)
        val mBuilder = android.app.AlertDialog.Builder(this@CutOffMeetingDetailTTSMainActivity)
            .setView(mDialogView)
        var mAlertDialog = mBuilder.show()
//        mAlertDialog!!.setCancelable(false)
        val title = mDialogView.findViewById<TextView>(R.id.title)
        val tv_paragraph = mDialogView.findViewById<TextView>(R.id.tv_paragraph)
        val iv_cancel = mDialogView.findViewById<ImageView>(R.id.iv_cancel)

        iv_cancel.setOnClickListener {
            if (mTts != null) {
                mTts!!.stop()
                doSpeak(summaryData)
            }
            mAlertDialog!!.dismiss()
        }

        title.text = titleData
        tv_paragraph.text = paragraphData

        mAlertDialog!!.setOnDismissListener(object : DialogInterface.OnDismissListener {
            override fun onDismiss(dialog: DialogInterface?) {
                if (mTts != null) {
                    mTts!!.stop()
                }
                mAlertDialog.dismiss()
            }

        })

    }

    fun getTotalClosedLoanAmount(memid: Long?, mtgNum: Int?): Int {
        return dtLoanMemberViewmodel!!.getTotalClosedLoanAmount(
            validate!!.returnIntegerValue(mtgNum.toString()),
            validate!!.returnLongValue(memid.toString()),
            true
        )
    }

}