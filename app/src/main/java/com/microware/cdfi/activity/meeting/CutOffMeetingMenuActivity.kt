package com.microware.cdfi.activity.meeting

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProviders
import com.google.gson.JsonObject
import com.microware.cdfi.R
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.api.response.MeetingResponse
import com.microware.cdfi.entity.DtmtgEntity
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.cash_in_hand_detail_item.view.*
import kotlinx.android.synthetic.main.customealertdialogelogin.*
import kotlinx.android.synthetic.main.customealertdialogelogin.view.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.layout_cutoff_meeting_menu.*
import kotlinx.android.synthetic.main.meeting_detail_item_zero.*
import kotlinx.android.synthetic.main.repay_toolbar_vomeeting.*
import kotlinx.android.synthetic.main.repay_toolbar_zero.*
import kotlinx.android.synthetic.main.repay_toolbar_zero.ic_Back
import kotlinx.android.synthetic.main.repay_toolbar_zero.tv_title
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CutOffMeetingMenuActivity : AppCompatActivity() {

    internal lateinit var progressDialog: ProgressDialog
    var  apiInterface: ApiInterface? = null
    var responseViewModel: ResponseViewModel? = null
    var validate: Validate? = null
    var generateMeetingViewmodel: GenerateMeetingViewmodel? = null
    lateinit var financialTransactionsMemViewmodel: FinancialTransactionsMemViewmodel
    var incomeandExpenditureViewmodel: IncomeandExpenditureViewmodel? = null
    var dtLoanMemberViewmodel: DtLoanMemberViewmodel? = null
    var dtLoanGpViewmodel: DtLoanGpViewmodel? = null
    var mtglist: List<DtmtgEntity>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_cutoff_meeting_menu)
        validate = Validate(this)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        responseViewModel = ViewModelProviders.of(this).get(ResponseViewModel::class.java)
        dtLoanGpViewmodel =ViewModelProviders.of(this).get(DtLoanGpViewmodel::class.java)
        dtLoanMemberViewmodel = ViewModelProviders.of(this).get(DtLoanMemberViewmodel::class.java)
        incomeandExpenditureViewmodel =
            ViewModelProviders.of(this).get(IncomeandExpenditureViewmodel::class.java)
        financialTransactionsMemViewmodel =
            ViewModelProviders.of(this).get(FinancialTransactionsMemViewmodel::class.java)
        setLabelText()
        tv_code.text = validate!!.RetriveSharepreferenceLong(MeetingSP.Shgcode).toString()
        tv_nam.text = validate!!.RetriveSharepreferenceString(MeetingSP.ShgName)
        tv_date.text =
            validate!!.convertDatetime(validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate))
        tv_mtgNum.text =
            validate!!.returnStringValue(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber).toString())
        tv_count.text = "" + validate!!.RetriveSharepreferenceInt(MeetingSP.MemberCount)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)

        var TotalPresent = generateMeetingViewmodel!!.getCutOffTotalPresent(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))



        ic_Back.setOnClickListener {
            var intent = Intent(this, SHGMeetingListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        btn_close.setOnClickListener {
            if(validate!!.RetriveSharepreferenceInt(MeetingSP.Cashinhand)>0) {
                CustomAlertCloseMeeting(LabelSet.getText("", R.string.are_u_sure_u_want_to_close))
            }else {
                validate!!.CustomAlert(LabelSet.getText("",R.string.verify_cash_in_hand),this)
            }
        }

        btn_cutoff_delete.setOnClickListener {
            if (isNetworkConnected()) {
                CustomAlertDeleteMeeting(
                    LabelSet.getText(
                        "want_to_delete_meeting",
                        R.string.want_to_delete_meeting
                    )
                )
            }else {
                    validate!!.CustomAlert(
                        LabelSet.getText(
                            "no_internet_msg",
                            R.string.no_internet_msg
                        ), this
                    )
                }
        }
        setValues()

        tbl_attendence.setOnClickListener {
            var intent = Intent(this, AttendnceZeroDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_member_saving.setOnClickListener {
            var intent = Intent(this, MemberSavingZeroActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_member_closed_loan.setOnClickListener {
            var intent = Intent(this, CutOffShgMemberCloseLoan::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }


        tbl_member_active_loan.setOnClickListener {
            var intent = Intent(this, CutOffLoanDisbursementListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_share_capital.setOnClickListener {
            validate!!.SaveSharepreferenceInt(MeetingSP.ReceiptType,6)
            var intent = Intent(this, CutOffShareCapitalOtherReceiptListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_membership_fee.setOnClickListener {
            validate!!.SaveSharepreferenceInt(MeetingSP.ReceiptType,4)
            var intent = Intent(this, CutOffShareCapitalOtherReceiptListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_group_summarry.setOnClickListener {
            var intent = Intent(this, CutOffGroupLoanlist::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_group_investment.setOnClickListener {
            var intent = Intent(this, GroupInvestmentListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }


        tbl_cashBox.setOnClickListener {
            var intent = Intent(this, CutOffGroupCashBalanceActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_bank.setOnClickListener {
            var intent = Intent(this, CutOffBankBalanceList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_total_Surplus.setOnClickListener {
            var intent = Intent(this, CutOffGroupActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        updateShgType()
        updatecashinhand()

        ll_speaker.visibility = View.VISIBLE
        ll_speaker.setOnClickListener {
            var SHG_name = tv_nam.text.toString()
            var MeetingDate = tv_date.text.toString()
            var totalMember = tv_count.text.toString()
            var savingAmount = tv_compulasory_amount.text.toString()
            var closedLoan_amount = tv_closedLoan_amount.text.toString()
            var activeLoan_amount = tv_activeLoan_amount.text.toString()
            var share_capital_amount = tv_share_capital_amount.text.toString()
            var recepient_and_income_amount = tv_recepient_and_income_amount.text.toString()
            var loan_receipt_amount = tv_loan_receipt_amount.text.toString()
            var cashBalance = tv_cashBox_amount.text.toString()
            var bankBalance = tv_bank_amount.text.toString()

            var intent = Intent(this, CutOffMeetingDetailTTSMainActivity::class.java)
            intent.putExtra("SHG_name", SHG_name)
            intent.putExtra("MeetingDate", MeetingDate)
            intent.putExtra("totalMember", totalMember)
            intent.putExtra("savingAmount",savingAmount)
            intent.putExtra("closedLoan_amount",closedLoan_amount)
            intent.putExtra("activeLoan_amount", activeLoan_amount)
            intent.putExtra("share_capital_amount", share_capital_amount)
            intent.putExtra("recepient_and_income_amount", recepient_and_income_amount)
            intent.putExtra("borrowAmount", loan_receipt_amount)
            intent.putExtra("cashBalance", cashBalance)
            intent.putExtra("bankBalance", bankBalance)
            startActivity(intent)
        }


    }
    override fun onResume() {
        super.onResume()
        setValues()
    }
    fun setLabelText() {
        tv_attendence.text = LabelSet.getText(
            "attendane",
            R.string.attendane
        )
        tv_member_saving.text = LabelSet.getText(
            "member_saving",
            R.string.member_saving
        )
        tv_member_closed_loan.text = LabelSet.getText(
            "member_closed_loan",
            R.string.member_closed_loan
        )
        tv_member_active_loan.text = LabelSet.getText(
            "member_active_loan",
            R.string.member_active_loan
        )
        tv_share_capital.text = LabelSet.getText(
            "member_share_capital",
            R.string.member_share_capital
        )
        tv_membership_fee.text = LabelSet.getText(
            "Membership_fee",
            R.string.Membership_fee
        )
        tv_loan_receipt.text = LabelSet.getText(
            "group_loan_summary",
            R.string.group_loan_summary
        )
        tv_recepient_and_income.text = LabelSet.getText(
            "group_investment",
            R.string.group_investment
        )
        //   tv_expenditure_and_payment.setText(LabelSet.getText("expenditure_and_payment",R.string.expenditure_and_payment))
        tv_cashBox.text = LabelSet.getText(
            "group_cash_balance",
            R.string.group_cash_balance
        )
        tv_bank.text = LabelSet.getText(
            "group_bank_balance",
            R.string.group_bank_balance
        )
        tv_surplus.text = LabelSet.getText(
            "surplus_or_loss",
            R.string.surplus_or_loss
        )
        tv_title.text = LabelSet.getText(
            "cut_off_menu",
            R.string.cut_off_menu
        )
        btn_close.text = LabelSet.getText(
            "close_meeting",
            R.string.close_meeting
        )
        btn_cutoff_delete.text = LabelSet.getText(
            "delete_meeting",
            R.string.delete_meeting
        )

    }

    override fun onBackPressed() {
        var intent = Intent(this, SHGMeetingListActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

    fun setValues(){
        val rs_string = LabelSet.getText("", R.string.rs_sign)
        // ClosedLoan

        setSumValue(tv_closedLoan_amount,ivClosedLoan,generateMeetingViewmodel!!.getCutOffMemberLoanAmount(validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),true),rs_string)

        // Active Loan
        setSumValue(tv_activeLoan_amount,ivActiveLoan,generateMeetingViewmodel!!.getCutOffMemberLoanAmount(validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),false),rs_string)

        // Attendance
        setSumValue(tv_attendence_amount,ivAttendance,generateMeetingViewmodel!!.getCutOffTotalPresent(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)),"")

        // Saving
        val totalCompulsarysaving = generateMeetingViewmodel!!.getsumcomp(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))

        val totalVoluntaryarysaving = generateMeetingViewmodel!!.getsumvol(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))

        var totalSaving = totalCompulsarysaving + totalVoluntaryarysaving
        setSumValue(tv_compulasory_amount,ivSaving,totalSaving,rs_string)

        // ShareCapital
        setSumValue(tv_share_capital_amount,ivShareCapital,generateMeetingViewmodel!!.getTotalAmountByReceiptType(validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),6),rs_string)

        //Membership Fee
        setSumValue(tv_membership_fee_amount,ivMembershipFee,generateMeetingViewmodel!!.getTotalAmountByReceiptType(validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),4),rs_string)


        // GroupLoanSummary
        setSumValue(tv_loan_receipt_amount,ivGrpLoanSummary,dtLoanGpViewmodel!!.gettotloangroupamount(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)),rs_string)

        // Cash In Hand
        setSumValue(tv_cashBox_amount, ivCashBalance, validate!!.RetriveSharepreferenceInt(MeetingSP.Cashinhand),rs_string)

        // Cash At Bank
        setSumValue(tv_bank_amount, ivBankBalance, generateMeetingViewmodel!!.gettotalinbank(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)),rs_string)

        // Group Investment
        setSumValue(tv_recepient_and_income_amount,ivInvestment,
            generateMeetingViewmodel!!.getTotalInvestmentByMtgNum(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                listOf(25, 44, 45, 46),
                "OE"
            ),rs_string)

        //  tv_expenditure_and_payment_amount.setText(rs_string+ " " +validate!!.returnStringValue(totalExpenditure.toString()))

    }
    fun updateShgType() {
        tv_count.setBackgroundResource(R.drawable.item_countactive)
        iv_stauscolor.setBackgroundResource(R.drawable.item_countactive)
        if (validate!!.RetriveSharepreferenceInt(MeetingSP.SHGType) == 1) {
            iv_stauscolor.setImageResource(R.drawable.ic_womangroup)
            iv_stauscolor.visibility = View.INVISIBLE
        } else {
            iv_stauscolor.visibility = View.VISIBLE
            if (validate!!.RetriveSharepreferenceInt(MeetingSP.Tag) == 1) {
                iv_stauscolor.setImageResource(R.drawable.ic_pvtg)
            } else if (validate!!.RetriveSharepreferenceInt(MeetingSP.Tag) == 2) {
                iv_stauscolor.setImageResource(R.drawable.ic_elderly)
            } else if (validate!!.RetriveSharepreferenceInt(MeetingSP.Tag) == 3) {
                iv_stauscolor.setImageResource(R.drawable.ic_disable)
            } else if (validate!!.RetriveSharepreferenceInt(MeetingSP.Tag) == 99) {
                iv_stauscolor.setImageResource(R.drawable.ic_other)
            }
        }
    }

    fun setSumValue(textView: TextView, imageView: ImageView, value: Int,stringPrefix:String) {
        if (value != null && value > 0) {
            if(stringPrefix.trim().length>0){
                textView.text = stringPrefix+ " " +validate!!.returnStringValue(value.toString())
            }else {
                textView.text = validate!!.returnStringValue(value.toString())
            }

            imageView.visibility = View.VISIBLE
        } else {
            textView.text = "0"
            imageView.visibility = View.GONE
        }
    }

    fun CustomAlertDeleteMeeting(str: String) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str
        mDialogView.btn_yes.setOnClickListener {
            //            PartialSaveData()
            mAlertDialog.dismiss()

            DeleteMeeting()

            /*     var intent = Intent(this, SHGMeetingListActivity::class.java)
                 intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                 startActivity(intent)
                 overridePendingTransition(0, 0)*/

        }
        mDialogView.btn_no.setOnClickListener {
            mAlertDialog.dismiss()

        }
    }

    fun CustomAlertCloseMeeting(str: String) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str
        mDialogView.btn_yes.setOnClickListener {
            //            PartialSaveData()
            mAlertDialog.dismiss()

            var intent = Intent(this, CutOffGroupSummary::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        mDialogView.btn_no.setOnClickListener {
            mAlertDialog.dismiss()

        }
    }

    fun deleteMeetingData(){
        /*Table1*/
        generateMeetingViewmodel!!.deleteShg_MtgData(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        /*Table2*/
        generateMeetingViewmodel!!.deleteShgDetailData(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        /*Table3*/
        generateMeetingViewmodel!!.deleteFinancialTransactionMemberDetailData(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        /*Table4*/
        generateMeetingViewmodel!!.deleteGrpFinancialTxnDetailData(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        /*Table5*/
        generateMeetingViewmodel!!.deleteShgFinancialTxnData(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        /*Table6*/
        generateMeetingViewmodel!!.deleteMemberLoanTxnData(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        /*Table7*/
        generateMeetingViewmodel!!.deleteGroupLoanData(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        /*Table8*/
        generateMeetingViewmodel!!.deleteGroupLoanTxnData(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        /*Table9*/
        generateMeetingViewmodel!!.deleteMCPData(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        /*Table10*/
        generateMeetingViewmodel!!.deleteMemberLoan(validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber))
        /*Table11*/
        generateMeetingViewmodel!!.deleteShgLoanApplication(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        /*Table12 Member Schedule */
        generateMeetingViewmodel!!.deleteMemberScheduleByMtgGuid(validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!)

        generateMeetingViewmodel!!.deleteMemberRepaidData(validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))

        generateMeetingViewmodel!!.deleteMemberSubinstallmentData(validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        /*Table13 Group Schedule*/
        generateMeetingViewmodel!!.deleteGroupScheduleByMtgGuid(validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!)

        generateMeetingViewmodel!!.deleteGroupRepaidData(validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))

        generateMeetingViewmodel!!.deleteGroupSubinstallmentData(validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))

        validate!!.CustomAlert(LabelSet.getText("",R.string.meeting_deleted_successfully),this,SHGMeetingListActivity::class.java)

    }

    fun updatecashinhand() {
        var totcurrentcash = generateMeetingViewmodel!!.getupdatedcash(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        var totcash = generateMeetingViewmodel!!.getupdatedpeningcash(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        var totloan = generateMeetingViewmodel!!.gettotalMemberLoan(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        var totloanpaid = generateMeetingViewmodel!!.gettotloanpaid(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        var totloangroup = generateMeetingViewmodel!!.getTotalGrouploan(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))

        var totloanpaidgroup = generateMeetingViewmodel!!.gettotloanpaidgroup(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        val incomeAmount = incomeandExpenditureViewmodel!!.getIncomeAndExpenditureAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            1, 1
        )
        val capitailIncome = financialTransactionsMemViewmodel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            1,"OR"
        )
        val capitailPayments = financialTransactionsMemViewmodel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            1,"OP"
        )


        val expenditureAmount = incomeandExpenditureViewmodel!!.getIncomeAndExpenditureAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            1, 2
        )

        /* var totloanpaidgroup = generateMeetingViewmodel!!.gettotloanpaidgroup(
             validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
             validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
         )*/
        var totalclosingbal =
            totcash + totcurrentcash + totloanpaid + totloangroup + incomeAmount + capitailIncome - capitailPayments - totloanpaidgroup - totloan.toInt() - expenditureAmount
        validate!!.SaveSharepreferenceInt(MeetingSP.Cashinhand, totalclosingbal)

        generateMeetingViewmodel!!.updateclosingcash(
            totalclosingbal,
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
    }

    fun updatecashinBank() {
        var shgBankList =
            generateMeetingViewmodel!!.getBankdata(validate!!.RetriveSharepreferenceString(MeetingSP.SHGGUID))
        for (i in shgBankList.indices) {
            var accountno =
                shgBankList.get(i).ifsc_code!!.dropLast(7) + shgBankList.get(i).account_no

            val openningbal = generateMeetingViewmodel!!.getopenningbal(
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), accountno
            )
            val grploanDisbursedAmt = generateMeetingViewmodel!!.getgrouptotloanamtinbank(
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), accountno
            )

            val capitailIncome = financialTransactionsMemViewmodel.getBankCodeAmount(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                accountno,"OR")

            val capitailPayments = financialTransactionsMemViewmodel.getBankCodeAmount(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                accountno,"OP")

            val incomeAmount =
                incomeandExpenditureViewmodel!!.getBankCodeIncomeAndExpenditureAmount(
                    validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                    validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                    1, accountno
                )
            val memloanDisbursedAmt = dtLoanMemberViewmodel!!.getMemLoanDisbursedAmount(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber), accountno
            )
            var grptotloanpaid = generateMeetingViewmodel!!.getgrptotloanpaidinbank(
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), accountno
            )
            var memtotloanpaid = generateMeetingViewmodel!!.getmemtotloanpaidinbank(
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), accountno
            )
            val expenditureAmount =
                incomeandExpenditureViewmodel!!.getBankCodeIncomeAndExpenditureAmount(
                    validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                    validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                    2, accountno
                )

            var tot =
                openningbal + grploanDisbursedAmt + capitailIncome + incomeAmount + memtotloanpaid - capitailPayments - memloanDisbursedAmt - expenditureAmount - grptotloanpaid
            //  validate!!.SaveSharepreferenceInt(MeetingSP.CashinBank, tot)
            generateMeetingViewmodel!!.updateclosingbalbank(
                tot,
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                accountno
            )
        }
    }

    fun DeleteMeeting() {

        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText("deleting_meeting_data", R.string.deleting_meeting_data)
        )

        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
        val token =  validate!!.returnStringValue(
            AESEncryption.decrypt(
            validate!!.RetriveSharepreferenceString(AppSP.token),
                validate!!.RetriveSharepreferenceString(AppSP.userid)))

        val shgId = validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        val mtgNum = validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber)
        val callCount = apiInterface?.deleteMeeting(
            token,
            user,
            shgId,
            mtgNum
        )

        callCount?.enqueue(object : Callback<MeetingResponse> {
            override fun onFailure(callCount: Call<MeetingResponse>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<MeetingResponse>,
                response: Response<MeetingResponse>
            ) {

                progressDialog.dismiss()
                if (response.isSuccessful) {

                    try {
                        var  msg = response.body()?.result
                        if(msg!!.equals("Meeting delete successfully!!")){
                            deleteMeetingData()
                        }


                        //       CustomAlert(text)
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        //                                    progressDialog.dismiss()
                    }

                } else {
                    progressDialog.dismiss()
                    var resCode = 0
                    var resMsg = ""
                    if (response.code() == 403) {

                        CustomAlertlogin()
                    }else {
                        if (response.errorBody()?.contentLength() == 0L || response.errorBody()
                                ?.contentLength()!! < 0L
                        ) {
                            resCode = response.code()
                            resMsg = response.message()
                        } else {
                            var jsonObject1 =
                                JSONObject(response.errorBody()!!.source().readUtf8().toString())

                            resCode =
                                validate!!.returnIntegerValue(
                                    jsonObject1.optString("responseCode").toString()
                                )
                            resMsg = validate!!.returnStringValue(
                                jsonObject1.optString("responseMsg").toString()
                            )
                            if (resCode == 404 && resMsg.equals("record not found in database")) {

                                deleteMeetingData()

                            }
                        }

                        validate!!.CustomAlertMsg(
                            this@CutOffMeetingMenuActivity,
                            responseViewModel,
                            resCode,
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!, resMsg
                        )

                    }

                }


            }

        })
    }

    fun CustomAlertlogin() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogelogin, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        val password =  validate!!.returnStringValue(AESEncryption.decrypt(
            validate!!.RetriveSharepreferenceString(AppSP.Password),
            validate!!.RetriveSharepreferenceString(AppSP.userid)))

        mDialogView.etUsername.setText(validate!!.RetriveSharepreferenceString(AppSP.userid))
        mDialogView.etPassword.setText(password)
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_dialog_title2.text = LabelSet.getText("app_name",R.string.app_name)
        mDialogView.btn_login.text = LabelSet.getText("log_in",R.string.log_in)
        mDialogView.chk_showHide1.text = LabelSet.getText("show_password",R.string.show_password)
        mDialogView.btn_login.setOnClickListener {
            mAlertDialog.dismiss()
            importtoken(
                mDialogView.etUsername.text.toString(),
                validate!!.returnStringValue(
                    AESEncryption.encrypt(validate!!.returnStringValue(
                        mDialogView.etPassword.text.toString()),mDialogView.etUsername.text.toString()))
            )
        }
        mAlertDialog.chk_showHide1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                mAlertDialog.etPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                mAlertDialog.etPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
        }

    }

    fun importtoken(user: String, pass: String) {
        var grant_type = "password"
        var userId = user
        var password = pass
        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText("authenticate_user", R.string.authenticate_user)
        )
        val callCount = apiInterface?.gettokenforce(
            grant_type,
            userId,
            password, validate!!.RetriveSharepreferenceInt(AppSP.State_Selected), 1,
            validate!!.RetriveSharepreferenceString(AppSP.Roleid)!!)
        callCount?.enqueue(object : Callback<JsonObject> {
            override fun onFailure(callCount: Call<JsonObject>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                //       Log.e("", response!!.body().toString())
                progressDialog.dismiss()


                if (response.isSuccessful) {

                    var jsonObject1: JSONObject? = null
                    try {
                        jsonObject1 = JSONObject(response.body().toString())

                        val auth = jsonObject1.get("accessToken").toString()
                        if (auth.trim().length > 0) {
                            validate!!.SaveSharepreferenceString(
                                AppSP.token,
                                validate!!.returnStringValue(
                                    AESEncryption.encrypt(
                                        auth,validate!!.RetriveSharepreferenceString(AppSP.userid)))
                            )
                        }
                        validate!!.CustomAlert(
                            LabelSet.getText(
                                "userauthenticatedsuccessfully",
                                R.string.userauthenticatedsuccessfully
                            ), this@CutOffMeetingMenuActivity
                        )
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        var jsonObject1 =
                            JSONObject(response.errorBody()!!.source().readUtf8().toString())

                        var resCode =
                            validate!!.returnIntegerValue(
                                jsonObject1.optString("responseCode").toString()
                            )
                        validate!!.CustomAlertMsg(
                            this@CutOffMeetingMenuActivity,
                            responseViewModel,
                            resCode,
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                            validate!!.returnStringValue(
                                jsonObject1.optString("responseMsg").toString()
                            )
                        )
                    }

                } else {
                    var jsonObject1 =
                        JSONObject(response.errorBody()!!.source().readUtf8().toString())

                    var resCode =
                        validate!!.returnIntegerValue(
                            jsonObject1.optString("responseCode").toString()
                        )
                    validate!!.CustomAlertMsg(
                        this@CutOffMeetingMenuActivity,
                        responseViewModel,
                        resCode,
                        validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                        validate!!.returnStringValue(
                            jsonObject1.optString("responseMsg").toString()
                        )
                    )
                }


            }

        })


    }

    private fun isNetworkConnected(): Boolean {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager //1
        val networkInfo = connectivityManager.activeNetworkInfo //2
        return networkInfo != null && networkInfo.isConnected //3
    }
}