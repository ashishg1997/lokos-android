package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.CutoffShareCapitalOtherReceiptAdapter
import com.microware.cdfi.api.meetingmodel.ShareCapitalModel
import com.microware.cdfi.entity.FinancialTransactionsMemEntity
import com.microware.cdfi.fragment.MeetingTopBarZeroFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.FinancialTransactionsMemViewmodel
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import com.microware.cdfi.viewModel.LookupViewmodel
import kotlinx.android.synthetic.main.activity_cut_off_share_capital_other_receipt_list.*
import kotlinx.android.synthetic.main.activity_cut_off_share_capital_other_receipt_list.tv_srno
import kotlinx.android.synthetic.main.buttons.*

class CutOffShareCapitalOtherReceiptListActivity : AppCompatActivity() {

    var validate: Validate? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    var lookupViewmodel: LookupViewmodel? = null
    lateinit var financialTransactionsMemViewmodel: FinancialTransactionsMemViewmodel
    var Todayvalue = 0
    var financialTransactionsMemEntity: FinancialTransactionsMemEntity? = null


    var nullStringValue:String?=null
    var nullLongValue:Long?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cut_off_share_capital_other_receipt_list)

        validate = Validate(this)

        generateMeetingViewmodel = ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        financialTransactionsMemViewmodel = ViewModelProviders.of(this).get(FinancialTransactionsMemViewmodel::class.java)

        if (validate!!.RetriveSharepreferenceInt(MeetingSP.ReceiptType) == 6){
            replaceFragmenty(
                fragment = MeetingTopBarZeroFragment(6),
                allowStateLoss = true,
                containerViewId = R.id.mainContent
            )
        }else{
            replaceFragmenty(
                fragment = MeetingTopBarZeroFragment(7),
                allowStateLoss = true,
                containerViewId = R.id.mainContent
            )
        }

        btn_save.setOnClickListener {
            getShareCapitalValue()
        }

        btn_cancel.setOnClickListener {
            val intent = Intent(this, CutOffMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        }

        setLabelText()
        fillRecyclerView()

    }

    private fun fillRecyclerView() {
        Todayvalue=0
        var list:List<ShareCapitalModel>? = null
        list = generateMeetingViewmodel.getShareCapitalListByReceiptType(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),validate!!.RetriveSharepreferenceInt(
                MeetingSP.ReceiptType))

        if (!list.isNullOrEmpty()){
            rvList.layoutManager = LinearLayoutManager(this)
            val shareCapitalOtherReceiptAdapter = CutoffShareCapitalOtherReceiptAdapter(this, list)
            val isize: Int
            isize = list.size
            val params: ViewGroup.LayoutParams = rvList.layoutParams
            val r = resources
            val px = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                resources.getDimension(R.dimen.spraying),
                r.displayMetrics
            )

            val hi = Math.round(px)
            val gridHeight = hi * isize
            params.height = gridHeight
            rvList.layoutParams = params
            rvList.adapter = shareCapitalOtherReceiptAdapter
        }

    }

    private fun setLabelText() {
        if (validate!!.RetriveSharepreferenceInt(MeetingSP.ReceiptType) == 6){
            tv_share_capital.text = LabelSet.getText(
                "share_capital",
                R.string.share_capital
            )
        }else{
            tv_share_capital.text = LabelSet.getText(
                "Membership_fee",
                R.string.Membership_fee
            )
        }

        tv_srno.text = LabelSet.getText("sr_no", R.string.sr_no)
        tv_member_name.text = LabelSet.getText(
            "member_name",
            R.string.member_name
        )
        tvtotal.text = LabelSet.getText("total", R.string.total)
    }

    fun getTotalValue() {
        var iValue = 0
        val iCount = rvList.childCount

        for (i in 0 until iCount) {
            val gridChild =
                rvList.getChildAt(i) as? ViewGroup

            val tv_count = gridChild!!
                .findViewById<View>(R.id.tv_count) as? EditText

            if (!tv_count!!.text.toString().isNullOrEmpty()) {
                iValue=iValue + validate!!.returnIntegerValue(tv_count.text.toString())
            }

        }
        tvTotalShareCapital.text = iValue.toString()
    }

    fun getTotalValue(iValue: Int) {
        Todayvalue = Todayvalue + iValue
        tvTotalShareCapital.text = Todayvalue.toString()
    }

    fun getShareCapitalValue(){
        var saveValue = 0
        var iValue = 0
        val iCount = rvList.childCount

        for (i in 0 until iCount) {
            val gridChild = rvList.getChildAt(i) as? ViewGroup

            val tv_count = gridChild!!.findViewById<View>(R.id.tv_count) as? EditText
            val tv_memid = gridChild.findViewById<View>(R.id.tv_memid) as? EditText

            if (tv_count!!.length() > 0 && tv_memid!!.length() > 0){
                iValue = validate!!.returnIntegerValue(tv_count.text.toString())
                var memID = validate!!.returnIntegerValue(tv_memid.text.toString()).toLong()

                saveValue=saveData(iValue,memID)
            }
        }

        if(saveValue > 0){
            if (validate!!.RetriveSharepreferenceInt(MeetingSP.ReceiptType) == 6){
                replaceFragmenty(
                    fragment = MeetingTopBarZeroFragment(6),
                    allowStateLoss = true,
                    containerViewId = R.id.mainContent
                )
            }else{
                replaceFragmenty(
                    fragment = MeetingTopBarZeroFragment(7),
                    allowStateLoss = true,
                    containerViewId = R.id.mainContent
                )
            }
    //        fillRecyclerView()
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this
            )
        }
    }


    fun saveData(iValue: Int, memID: Long):Int{

        var value = 0
        financialTransactionsMemEntity = FinancialTransactionsMemEntity(
            0,
            validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            memID,
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            "",
            validate!!.RetriveSharepreferenceInt(MeetingSP.ReceiptType),
            "OR",
            iValue,
            validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            0,
            1,
            "",
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            nullStringValue,
            nullLongValue,
            nullStringValue,
            nullLongValue
        )

        financialTransactionsMemViewmodel.insert(financialTransactionsMemEntity!!)
        value = 1

        return value
    }

    override fun onBackPressed() {
        var intent = Intent(this, CutOffMeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }
}