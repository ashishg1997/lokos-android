package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.CutOffShgMemberCloseLoanAdapter
import com.microware.cdfi.entity.DtLoanMemberEntity
import com.microware.cdfi.entity.DtLoanTxnMemEntity
import com.microware.cdfi.fragment.MeetingTopBarZeroFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.DtLoanMemberViewmodel
import com.microware.cdfi.viewModel.DtLoanTxnMemViewmodel
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import kotlinx.android.synthetic.main.activity_cut_off_shg_member_close_loan.*
import kotlinx.android.synthetic.main.buttons.*

class CutOffShgMemberCloseLoan : AppCompatActivity() {

    var validate: Validate? = null
    lateinit var cutoffshgMemberCloseLoanAdapter: CutOffShgMemberCloseLoanAdapter
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    var dtLoanMemberViewmodel: DtLoanMemberViewmodel? = null
    var dtLoanTxnMemViewmodel: DtLoanTxnMemViewmodel? = null
    var TotalLoan = 0
    var TotalAmount = 0
    var nullStringValue: String? = null
    var nullLongValue: Long? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cut_off_shg_member_close_loan)

        validate = Validate(this)

        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        dtLoanMemberViewmodel = ViewModelProviders.of(this).get(DtLoanMemberViewmodel::class.java)
        dtLoanTxnMemViewmodel = ViewModelProviders.of(this).get(DtLoanTxnMemViewmodel::class.java)

        btn_save.setOnClickListener {
            saveClosedLoan()
        }

        btn_cancel.setOnClickListener {
            var intent = Intent(this, CutOffMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }


        replaceFragmenty(
            fragment = MeetingTopBarZeroFragment(4),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        setLabelText()
        fillRecyclerView()

    }

    private fun saveClosedLoan() {
        var saveValue = 0

        val iCount = rvMemberList.childCount

        for (i in 0 until iCount) {
            var gridChild =
                rvMemberList.getChildAt(i) as? ViewGroup

            var et_totalLoan = gridChild!!
                .findViewById<View>(R.id.et_totalLoan) as? EditText

            var et_LoanNo = gridChild
                .findViewById<View>(R.id.et_LoanNo) as? EditText

            var et_memId = gridChild
                .findViewById<View>(R.id.et_memId) as? EditText

            var et_totalAmount = gridChild
                .findViewById<View>(R.id.et_totalAmount) as? EditText

            if (validate!!.returnIntegerValue(et_totalLoan!!.text.toString()) > 0) {
                var totalLoan = validate!!.returnIntegerValue(et_totalLoan.text.toString())
                var totalAmount = validate!!.returnIntegerValue(et_totalAmount!!.text.toString())
                var memId = validate!!.returnLongValue(et_memId!!.text.toString())
                var loanNo = validate!!.returnIntegerValue(et_LoanNo!!.text.toString())
                saveValue = saveData(memId, totalLoan, totalAmount, loanNo)
            }

        }

        if (saveValue > 0) {

            replaceFragmenty(
                fragment = MeetingTopBarZeroFragment(4),
                allowStateLoss = true,
                containerViewId = R.id.mainContent
            )

            fillRecyclerView()

            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this
            )
        }

    }

    private fun saveData(memId: Long, totalLoan: Int, totalAmount: Int, loanNumber: Int): Int {
        var value = 0
        var loan_application_id: Long? = null
        var loan_product_id: Int? = null
        var loanNo = 0
        if (loanNumber == 0) {
            val maxLoan = dtLoanMemberViewmodel!!.getmaxLoanno(
                validate!!.RetriveSharepreferenceLong(
                    MeetingSP.shgid
                )
            )
            loanNo = maxLoan + 1
        } else {
            loanNo = loanNumber
        }

        val dtLoanMemberEntity = DtLoanMemberEntity(
            0,
            loan_application_id,
            validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            memId,
            loanNo,
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            0,
            totalAmount,
            0,
            0,/*purpose*/
            loan_product_id,
            0.0,
            0,
            0,
            0,
            true,
            0,
            0,
            0,
            "",
            "",
            0,
            0,
            validate!!.returnStringValue(loanNo.toString()),
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            nullStringValue,
            nullLongValue,
            nullStringValue,
            nullLongValue,
            0,/*et_principal_repaid*/
            0,/*et_interest_paid*/
            0,/*et_date_loan_taken*/
            totalLoan, 0.0, 0, 0,1
        )
        dtLoanMemberViewmodel!!.insert(dtLoanMemberEntity)

        // insert in dtlonmemtxn

        var dtLoanTxnMemEntity = DtLoanTxnMemEntity(
            0,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            memId,
            loanNo,
            totalAmount,
            0,
            totalAmount,/*et_principal_repaid*/
            0,/*et_interest_paid*/
            0,
            0,/*et_interest_paid*/
            true,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            "",
            "",
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            nullStringValue,
            nullLongValue,
            nullStringValue,
            nullLongValue,
            0.0,
            0
        )
        dtLoanTxnMemViewmodel!!.insert(dtLoanTxnMemEntity)
        value = 1
        return value
    }

    fun setLabelText() {
        tv_srno.text = LabelSet.getText("sr_no", R.string.sr_no)
        tv_memberName.text = LabelSet.getText(
            "member_name",
            R.string.member_name
        )
        tv_totalLoan.text = LabelSet.getText(
            "number_of_loans",
            R.string.number_of_loans
        )
        tv_LoanAmount.text = LabelSet.getText(
            "amount_of_loans",
            R.string.amount_of_loans
        )

        tvToatalAmount.text = LabelSet.getText(
            "total_amount", R.string.total_amount
        )

        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }

    private fun fillRecyclerView() {
        TotalLoan = 0
        TotalAmount = 0
        var list = generateMeetingViewmodel.getCutOffClosedMemberLoanDataByMtgNum(
            validate!!.RetriveSharepreferenceInt(
                MeetingSP.currentmeetingnumber
            ), validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            true
        )

        cutoffshgMemberCloseLoanAdapter = CutOffShgMemberCloseLoanAdapter(this, list)

        rvMemberList.layoutManager = LinearLayoutManager(this)

        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = rvMemberList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvMemberList.layoutParams = params
        rvMemberList.adapter = cutoffshgMemberCloseLoanAdapter
    }

    fun getTotalLoanValue() {
        var iValue = 0
        val iCount = rvMemberList.childCount


        for (i in 0 until iCount) {
            val gridChild =
                rvMemberList.getChildAt(i) as? ViewGroup

            val tv_ctotal = gridChild!!
                .findViewById<View>(R.id.et_totalLoan) as? EditText

            if (!tv_ctotal!!.text.toString().isNullOrEmpty()) {
                iValue += validate!!.returnIntegerValue(tv_ctotal.text.toString())
            }

        }
        tv_TotalLoan_value.text = iValue.toString()

    }

    fun getTotalAmountValue() {
        var iValue = 0
        val iCount = rvMemberList.childCount

        for (i in 0 until iCount) {
            val gridChild =
                rvMemberList.getChildAt(i) as? ViewGroup

            val tv_vtotal = gridChild!!
                .findViewById<View>(R.id.et_totalAmount) as? EditText

            if (!tv_vtotal!!.text.toString().isNullOrEmpty()) {
                iValue += validate!!.returnIntegerValue(tv_vtotal.text.toString())
            }

        }
        tv_TotalAmountValue.text = iValue.toString()

    }

    fun getTotalClosedLoanAmount(memid: Long?, mtgNum: Int?): Int {
        return dtLoanMemberViewmodel!!.getClosedLoanAmount(
            validate!!.returnIntegerValue(mtgNum.toString()),
            validate!!.returnLongValue(memid.toString()),
            true
        )
    }

    fun getTotalClosedLoanCount(memid: Long?, mtgNum: Int?): Int {
        return dtLoanMemberViewmodel!!.getTotalClosedLoan(
            validate!!.returnIntegerValue(mtgNum.toString()),
            validate!!.returnLongValue(memid.toString()),
            true
        )
    }

    fun getTotalValue(iValue: Int, flag: Int) {
        if (flag == 1) {
            TotalLoan = TotalLoan + iValue
            tv_TotalLoan_value.text = TotalLoan.toString()
        } else if (flag == 2) {
            TotalAmount = TotalAmount + iValue
            tv_TotalAmountValue.text = TotalAmount.toString()
        }
    }

    override fun onBackPressed() {
        var intent = Intent(this, CutOffMeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

}