package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.microware.cdfi.R
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.replaceFragmenty
import kotlinx.android.synthetic.main.activity_expenditure_payment.*
import kotlinx.android.synthetic.main.buttons.*

class ExpenditurePaymentActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_expenditure_payment)


        btn_cancel.setOnClickListener {
            var intent = Intent(this, MeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        replaceFragmenty(
            fragment = MeetingTopBarFragment(14),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )


        setLabelText()
        fillRecyclerView()

    }

    override fun onBackPressed() {
        var intent = Intent(this, MeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0,0)
        finish()
    }
    fun setLabelText() {
        tv_particulars.text = LabelSet.getText(
            "particulars",
            R.string.particulars
        )
        tv_amount.text = LabelSet.getText(
            "amount",
            R.string.amount
        )
      /*  tv_title.setText(LabelSet.getText("expenditure_and_payment", R.string.expenditure_and_payment))
        tv_code.setText(LabelSet.getText("_16649",R.string._16649))
        tv_nam.setText(LabelSet.getText("adarsh_mahila_sangam",R.string.adarsh_mahila_sangam))
        tv_date.setText(LabelSet.getText("_12_oct_2020",R.string._12_oct_2020))*/
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )

    }

    private fun fillRecyclerView() {
       /* rvMemberList.layoutManager = LinearLayoutManager(this)
        rvMemberList.adapter =
            ExpenditurePaymentAdapter(this)*/
    }
}