package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.*
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_expenditure_payment_summery_detail.*

class ExpenditurePaymentSummeryDetailActivity : AppCompatActivity() {

    var validate: Validate? = null
    var lookupViewmodel: LookupViewmodel? = null
    var dataspin_modeofpayment: List<LookupEntity>? = null
    var dataspin_bank: List<BankEntity>? = null
    var bankMasterViewModel: MasterBankViewmodel? = null
    var dataspin_PaymentType: List<LookupEntity>? = null
    var dataspin_Payment: List<MstCOAEntity>? = null
    var dataspin_paidTo: List<LookupEntity>? = null
    var incomeandExpenditureViewmodel: IncomeandExpenditureViewmodel? = null
    var shgFinancialTxnDetailEntity: ShgFinancialTxnDetailEntity? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    lateinit var mstcoaViewmodel: MstCOAViewmodel
    var shgBankList : List<Cbo_bankEntity>? = null

    var cashInTransit = 0
    var bankCashInTransit = 0

    var nullStringValue:String?=null
    var nullLongValue:Long?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_expenditure_payment_summery_detail)

        validate = Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        bankMasterViewModel = ViewModelProviders.of(this).get(MasterBankViewmodel::class.java)
        incomeandExpenditureViewmodel = ViewModelProviders.of(this).get(IncomeandExpenditureViewmodel::class.java)
        generateMeetingViewmodel = ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        mstcoaViewmodel = ViewModelProviders.of(this).get(MstCOAViewmodel::class.java)
        cashInTransit = generateMeetingViewmodel.getCashInTransit(validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber))

        replaceFragmenty(
            fragment = MeetingTopBarFragment(14),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        btn_save.isEnabled =
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber) >= validate!!.RetriveSharepreferenceInt(MeetingSP.maxmeetingnumber)
        btn_cancel.setOnClickListener {
            val intent = Intent(this, ExpenditurePaymentSummeryListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                SaveData()
            }
        }

        spin_payment?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                if (position > 0) {
                    val payment_id = validate!!.returncoauid(spin_payment, dataspin_Payment)
                    if (payment_id == 40){
                        spin_mode_of_payment.setSelection(1)
                        spin_mode_of_payment.isEnabled = false
                    }else if (payment_id == 42){
                        spin_mode_of_payment.setSelection(2)
                        spin_mode_of_payment.isEnabled = false
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }


        spin_mode_of_payment?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                if (position > 0) {
                    val id = validate!!.returnlookupcode(spin_mode_of_payment, dataspin_modeofpayment)
                    if (id > 1){
                        lay_BankName.visibility = View.VISIBLE
                        lay_chequeNO.visibility = View.VISIBLE
                    }else{
                        spin_BankName.setSelection(0)
                        et_cheque_no_transactio_no.setText("")
                        lay_BankName.visibility = View.GONE
                        lay_chequeNO.visibility = View.GONE
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        fillbank()
        fillSpinner()
        setLabelText()
        showData()
    }

    fun fillbank() {
        shgBankList = generateMeetingViewmodel.getBankdata(validate!!.RetriveSharepreferenceString(MeetingSP.SHGGUID))

        val adapter: ArrayAdapter<String?>
        if (!shgBankList.isNullOrEmpty()) {
            val isize = shgBankList!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in shgBankList!!.indices) {
                var lastthree=shgBankList!![i].account_no.substring(shgBankList!![i].account_no.length-3)
                sValue[i + 1] =
                    shgBankList!![i].ifsc_code.toString().dropLast(7) +"XXXXX"+ lastthree
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_BankName.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_BankName.adapter = adapter
        }

    }

    fun returaccount(): String {

        var pos = spin_BankName.selectedItemPosition
        var id = ""

        if (!shgBankList.isNullOrEmpty()) {
            if (pos > 0) id =
                shgBankList!!.get(pos - 1).ifsc_code!!.dropLast(7) + shgBankList!!.get(pos - 1).account_no
        }
        return id
    }

    fun setaccount(accountno:String): Int {

        var pos = 0

        if (!shgBankList.isNullOrEmpty()) {
            for (i in shgBankList!!.indices) {
                if (accountno.equals(
                        shgBankList!!.get(i).ifsc_code!!.dropLast(7) + shgBankList!!.get(
                            i
                        ).account_no
                    )
                )
                    pos = i + 1
            }
        }
        spin_BankName.setSelection(pos)
        return pos
    }

    private fun showData() {
        val data = incomeandExpenditureViewmodel!!.getIncomeAndExpendituredata(
            validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceInt(MeetingSP.Auid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.savingSource)
        )
        if (!data.isNullOrEmpty()) {
            et_amount.setText(data[0].amount.toString())
            et_cheque_no_transactio_no.setText(data[0].transaction_no)
            spin_paidTo.setSelection(
                validate!!.returnLookuppos(
                    validate!!.returnIntegerValue(
                        data[0].amount_to_from.toString()
                    ), dataspin_paidTo
                )
            )
            spin_paidTo.isEnabled = false
            /*    spin_TypeofPayment.setSelection(
                    validate!!.returnlookupcodepos(
                        validate!!.returnIntegerValue(
                            data[0].type.toString()
                        ), dataspin_paidTo
                    )
                )*/

            spin_mode_of_payment.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(
                        data[0].mode_payment.toString()
                    ), dataspin_modeofpayment
                )
            )

            spin_payment.setSelection(
                validate!!.returncoauidpos(
                    validate!!.returnIntegerValue(
                        data[0].auid.toString()
                    ), dataspin_Payment
                )
            )
            if(validate!!.returnIntegerValue(data[0].auid.toString()) == 40 || validate!!.returnIntegerValue(
                data[0].auid.toString())==42){
                et_amount.isEnabled = false
                spin_BankName.isEnabled = false
            }else {
                et_amount.isEnabled = true
            }
            /*spin_BankName.setSelection(
                validate!!.returnMasterBankpos(validate!!.returnIntegerValue(data[0].bank_code.toString())
                    ,
                    dataspin_bank
                )
            )*/

            setaccount(data[0].bank_code.toString())

            spin_payment.isEnabled = false
        }else{
            spin_mode_of_payment.setSelection(
                validate!!.returnlookupcodepos(
                    1, dataspin_modeofpayment
                )
            )
        }


    }

    private fun SaveData() {

        var save = 0

        if (validate!!.RetriveSharepreferenceInt(MeetingSP.Auid) == 0
        ) {
            shgFinancialTxnDetailEntity = ShgFinancialTxnDetailEntity(
                0,
                validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.returncoauid(spin_payment, dataspin_Payment),/*payment goes here auid*/
                2,/*Payment type goes here fundtype*/
                validate!!.returnLookupcode(spin_paidTo, dataspin_paidTo),
                "OE",
                validate!!.returnIntegerValue(et_amount.text.toString()),
                validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
                0,
                validate!!.returnlookupcode(spin_mode_of_payment, dataspin_modeofpayment),
                returaccount(),
                et_cheque_no_transactio_no.text.toString(),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                nullStringValue, nullLongValue, nullStringValue, nullLongValue
            )
            incomeandExpenditureViewmodel!!.insert(shgFinancialTxnDetailEntity!!)

            save = 1
        } else {
            incomeandExpenditureViewmodel!!.updateIncomeAndExpenditure(
                validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceInt(MeetingSP.Auid),
                2,
                validate!!.returnLookupcode(spin_paidTo, dataspin_paidTo),
                "OE",
                validate!!.returnIntegerValue(et_amount.text.toString()),
                validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
                0,
                validate!!.returnlookupcode(spin_mode_of_payment, dataspin_modeofpayment),
                returaccount(),
                et_cheque_no_transactio_no.text.toString(),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime)
            )

            save = 2
        }

        if (save == 1) {
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this, ExpenditurePaymentSummeryListActivity::class.java
            )
        } else {
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_updated_successfully",
                    R.string.data_updated_successfully
                ),
                this, ExpenditurePaymentSummeryListActivity::class.java
            )
        }


    }

    private fun setLabelText() {
        tv_paidTo.text = LabelSet.getText(
            "paid_to",
            R.string.paid_to
        )
        tv_PaymentType.text = LabelSet.getText(
            "payment_type",
            R.string.payment_type
        )
        tv_payments.text = LabelSet.getText(
            "payment",
            R.string.payment
        )
        tv_amount.text = LabelSet.getText(
            "amount",
            R.string.amount
        )
        tv_paymentMode.text = LabelSet.getText(
            "mode_of_payment",
            R.string.mode_of_payment
        )
        tv_BankName.text = LabelSet.getText(
            "bank_name",
            R.string.bank_name
        )
        tv_cheque_no_transactio_no.text = LabelSet.getText(
            "cheque_no_transactio_no",
            R.string.cheque_no_transactio_no
        )
    }

    private fun fillSpinner() {
        dataspin_paidTo = lookupViewmodel!!.getlookupMasterdata(
            75,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),listOf(3,4,5,6,8,99)
        )
        dataspin_PaymentType = lookupViewmodel!!.getlookup(
            84,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_Payment = mstcoaViewmodel.getMstCOAdatanotin(listOf(25, 44, 45,46),
            "OE",
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
        )
        dataspin_modeofpayment = lookupViewmodel!!.getlookup(
            65,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
//        dataspin_bank = bankMasterViewModel!!.getBankMaster()
//        validate!!.fillMasterBankspinner(this, spin_BankName, dataspin_bank)
        validate!!.fillspinner(this, spin_TypeofPayment, dataspin_PaymentType)
        validate!!.fillcoaspinner(this, spin_payment, dataspin_Payment)
        validate!!.fillspinner(this, spin_mode_of_payment, dataspin_modeofpayment)
        validate!!.fillLookupspinner(this, spin_paidTo, dataspin_paidTo)
    }

    private fun checkValidation(): Int {

        var value = 1

        if (spin_paidTo.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_paidTo,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "paid_to",
                    R.string.paid_to
                )
            )
            value = 0
        } else if (spin_payment.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_payment,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "payment",
                    R.string.payment
                )
            )
            value = 0
        } else if (et_amount.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "amount",
                    R.string.amount
                ),
                this, et_amount
            )
            value = 0
        } else if (validate!!.returnlookupcode(spin_mode_of_payment, dataspin_modeofpayment)==1 && et_amount.text.toString().toFloat() > validate!!.RetriveSharepreferenceInt(
                MeetingSP.Cashinhand
            )
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "inthecashbox",
                    R.string.inthecashbox
                ) , this,
                et_amount
            )
            value = 0
            return value
        } else if (spin_mode_of_payment.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_mode_of_payment,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "mode_of_payment",
                    R.string.mode_of_payment
                )
            )
            value = 0
        } else if (spin_BankName.selectedItemPosition == 0 && lay_BankName.visibility == View.VISIBLE) {
            validate!!.CustomAlertSpinner(
                this, spin_BankName,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "bank_name",
                    R.string.bank_name
                )
            )
            value = 0
        } else if (et_cheque_no_transactio_no.text.toString().length == 0 && lay_chequeNO.visibility == View.VISIBLE) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "cheque_no_transactio_no",
                    R.string.cheque_no_transactio_no
                ),
                this, et_cheque_no_transactio_no
            )
            value = 0
        }
        var cashinbank= generateMeetingViewmodel.getclosingbal(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), returaccount())
        if(validate!!.returnlookupcode(spin_mode_of_payment, dataspin_modeofpayment)==2 && cashinbank < validate!!.returnIntegerValue(et_amount.text.toString())){
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "intheselectedbank",
                    R.string.intheselectedbank
                ), this,
                et_amount
            )
            value = 0
            return value
        }
        return value


    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, ExpenditurePaymentSummeryListActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()

    }

}