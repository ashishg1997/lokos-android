package com.microware.cdfi.activity.meeting

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.*
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_generatemeeting.*
import kotlinx.android.synthetic.main.buttons_meeting.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.meeting_detail_item.*
import kotlinx.android.synthetic.main.repay_toolbar.*

class GenerateMeetingActivity : AppCompatActivity() {

    var validate: Validate? = null
    var lookup: LookupEntity? = null
    var generateMeetingViewmodel: GenerateMeetingViewmodel? = null
    var lookupViewmodel: LookupViewmodel? = null
    var memberViewmodel: Memberviewmodel? = null
    var datameetingtype: List<LookupEntity>? = null
    var GroupLastMeetingNumber: Int = 0
    var GroupMaxMeetingDate: Long = 0
    lateinit var dtLoanGpViewmodel: DtLoanGpViewmodel
    lateinit var dtLoanGpTxnViewmodel: DtLoanGpTxnViewmodel
    lateinit var dtMtgGrpLoanScheduleViewmodel: DtMtgGrpLoanScheduleViewmodel
    var dtMtgFinTxnViewmodel: DtMtgFinTxnViewmodel? = null
    var dtLoanTxnMemViewmodel: DtLoanTxnMemViewmodel? = null
    var dtLoanMemberScheduleViewmodel: DtLoanMemberScheduleViewmodel? = null
    var dtLoanMemberViewmodel: DtLoanMemberViewmodel? = null

    var nullStringValue: String? = null
    var nullLongValue: Long? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_generatemeeting)
        validate = Validate(this)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        memberViewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)
        lookupViewmodel =
            ViewModelProviders.of(this).get(LookupViewmodel::class.java)

        dtLoanGpViewmodel = ViewModelProviders.of(this).get(DtLoanGpViewmodel::class.java)
        dtMtgGrpLoanScheduleViewmodel =
            ViewModelProviders.of(this).get(DtMtgGrpLoanScheduleViewmodel::class.java)
        dtLoanGpTxnViewmodel = ViewModelProviders.of(this).get(DtLoanGpTxnViewmodel::class.java)
        dtMtgFinTxnViewmodel = ViewModelProviders.of(this).get(DtMtgFinTxnViewmodel::class.java)
        dtLoanTxnMemViewmodel = ViewModelProviders.of(this).get(DtLoanTxnMemViewmodel::class.java)
        dtLoanMemberScheduleViewmodel =
            ViewModelProviders.of(this).get(DtLoanMemberScheduleViewmodel::class.java)
        dtLoanMemberViewmodel = ViewModelProviders.of(this).get(DtLoanMemberViewmodel::class.java)
        GroupLastMeetingNumber =
            validate!!.RetriveSharepreferenceInt(MeetingSP.GroupLastMeetingNumber)
        GroupMaxMeetingDate = validate!!.RetriveSharepreferenceLong(MeetingSP.GroupLastMeetingDate)
        var totalmtg =
            generateMeetingViewmodel!!.getmeetingCount(
                validate!!.RetriveSharepreferenceLong(
                    MeetingSP.shgid
                ).toString()
            )

        btn_generate_open.setOnClickListener {
            var strMsg = LabelSet.getText(
                "generate_cutoff",
                R.string.generate_cutoff
            )
            if (sCheckValidation() == 1) {
                var meetingCount = generateMeetingViewmodel!!.getMeetingCount(
                    validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
                )
                var maxMeetingNum = generateMeetingViewmodel!!.getMaxMeetingNum(
                    validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
                )
                var meetingGap =
                    validate!!.returnIntegerValue(et_new_meeting_no.text.toString()) - maxMeetingNum
                if (meetingGap >= 12) {
                    if (maxMeetingNum == 0) {
                        CustomAlertGenerateMeeting(strMsg, totalmtg, 11)
                    } else {
                        CustomAlertGenerateMeeting(strMsg, totalmtg, 12)
                    }
                } else {
                    if (sCheckValid_RegularMeeting() == 1) {
                        generatemeeting(totalmtg)
                        validate!!.SaveSharepreferenceInt(MeetingSP.MeetingType, 0)
                        var intent = Intent(this, MeetingMenuActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    }
                }
            }

        }
        btn_cancelmeeting.setOnClickListener {
            var intent = Intent(this, SHGMeetingListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        ic_Back.setOnClickListener {
            var intent = Intent(this, SHGMeetingListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        et_Date.setOnClickListener {
            validate!!.datePickerwithmindate(
                validate!!.RetriveSharepreferenceLong(MeetingSP.Formation_dt),
                et_Date
            )

        }
        /*     rgcut_off_meeting.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->

                 if (checkedId == 1) {
                     lay_old_meeting_no.visibility = View.VISIBLE
                     lay_oldDate.visibility = View.VISIBLE
                     lay_total_meetings_held.visibility = View.GONE
                     lay_toDate.visibility = View.GONE
                     lay_fromDate.visibility = View.GONE
                 } else if (checkedId == 2) {
                     lay_old_meeting_no.visibility = View.GONE
                     lay_oldDate.visibility = View.GONE
                     lay_toDate.visibility = View.GONE
                     lay_fromDate.visibility = View.GONE
                     lay_total_meetings_held.visibility = View.VISIBLE
                 } else if (checkedId == 3) {
                     lay_old_meeting_no.visibility = View.GONE
                     lay_oldDate.visibility = View.GONE
                     lay_toDate.visibility = View.GONE
                     lay_fromDate.visibility = View.GONE
                     lay_total_meetings_held.visibility = View.VISIBLE
                 } else if (checkedId == 4) {
                     lay_old_meeting_no.visibility = View.GONE
                     lay_oldDate.visibility = View.GONE
                     lay_total_meetings_held.visibility = View.GONE
                     lay_toDate.visibility = View.VISIBLE
                     lay_fromDate.visibility = View.VISIBLE
                 } else {
                     lay_old_meeting_no.visibility = View.GONE
                     lay_oldDate.visibility = View.GONE
                     lay_toDate.visibility = View.GONE
                     lay_fromDate.visibility = View.GONE
                     lay_total_meetings_held.visibility = View.GONE

                 }

             })*/
        setLabelText()

        if (generateMeetingViewmodel!!.getlookupCount(999) == 0) {
            //  insertMasterdata()
        }

        fillradio()
        showdata(totalmtg)
        updateShgType()

    }


    fun setLabelText() {
        tv_title.text = LabelSet.getText(
            "generate_meeting",
            R.string.generate_meeting
        )
        txt_cut_off_meeting.text = LabelSet.getText(
            "meeting_type",
            R.string.meeting_type
        )
        txt_new_meeting_no.text = LabelSet.getText(
            "new_meeting_no",
            R.string.new_meeting_no
        )
        txt_old_meeting_no.text = LabelSet.getText(
            "last_meeting_no",
            R.string.last_meeting_no
        )
        txt_Date.text = LabelSet.getText(
            "newmeeting_date",
            R.string.newmeeting_date
        )
        txt_oldDate.text = LabelSet.getText(
            "last_meeting_date",
            R.string.last_meeting_date
        )
        txt_toDate.text = LabelSet.getText(
            "meeting_to",
            R.string.meeting_to
        )
        txt_fromDate.text = LabelSet.getText(
            "meeting_from",
            R.string.meeting_from
        )
        txttotal_meetings_held.text = LabelSet.getText(
            "total_meetings_held",
            R.string.total_meetings_held
        )
        et_Date.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )
        et_total_meetings_held.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        btn_generate_open.text = LabelSet.getText(
            "generate_open",
            R.string.generate_open
        )
        btn_cancelmeeting.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
        /* tv_code.setText(LabelSet.getText("_16649", R.string._16649))
         tv_nam.setText(LabelSet.getText("adarsh_mahila_sangam", R.string.adarsh_mahila_sangam))
         tv_date.setText(LabelSet.getText("_12_oct_2020", R.string._12_oct_2020))
 */
    }

    private fun insertMasterdata() {

        lookup = LookupEntity(1, "MeetingType", 1, "Normal", 999, "Normal", 1, "en")
        generateMeetingViewmodel!!.insertlookup(lookup!!)
        lookup = LookupEntity(2, "MeetingType", 2, "First cut off", 999, "Normal", 2, "en")
        generateMeetingViewmodel!!.insertlookup(lookup!!)
        lookup = LookupEntity(3, "MeetingType", 3, "Next cut off", 999, "Normal", 3, "en")
        generateMeetingViewmodel!!.insertlookup(lookup!!)
        lookup = LookupEntity(4, "MeetingType", 4, "Summary", 999, "Normal", 4, "en")
        generateMeetingViewmodel!!.insertlookup(lookup!!)
    }

    private fun fillradio() {

        datameetingtype = lookupViewmodel!!.getlookup(
            999,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        validate!!.fillradio(
            rgcut_off_meeting,
            0,
            datameetingtype,
            this@GenerateMeetingActivity
        )
    }

    private fun showdata(totalmtg: Int) {
        tv_code.text = validate!!.RetriveSharepreferenceLong(MeetingSP.Shgcode).toString()
        tv_nam.text = validate!!.RetriveSharepreferenceString(MeetingSP.ShgName)
        tv_date.text =
            validate!!.convertDatetime(validate!!.RetriveSharepreferenceLong(MeetingSP.Formation_dt))
        tv_count.text = "" + validate!!.RetriveSharepreferenceInt(MeetingSP.MemberCount)
        et_new_meeting_no.setText("" + (GroupLastMeetingNumber + 1))
        et_total_meetings_held.setText("" + totalmtg)
        var meetingfrequency = validate!!.RetriveSharepreferenceInt(MeetingSP.meetingfrequency)
        if (GroupMaxMeetingDate == 0L) {
            et_Date.setText(
                validate!!.convertDatetime(
                    validate!!.RetriveSharepreferenceLong(
                        MeetingSP.Formation_dt
                    )
                )
            )
        } else {
            var day = validate!!.RetriveSharepreferenceInt(MeetingSP.meeting_on)
            var meetingfrequencyvalue =
                validate!!.RetriveSharepreferenceInt(MeetingSP.meetingfrequencyvalue)
            et_old_meeting_no.setText("" + validate!!.RetriveSharepreferenceInt(MeetingSP.GroupLastMeetingNumber))
            et_oldDate.setText(
                validate!!.convertDatetime(
                    validate!!.RetriveSharepreferenceLong(
                        MeetingSP.GroupLastMeetingDate
                    )
                )
            )
            if (meetingfrequency == 1) {

                et_Date.setText(validate!!.setday(GroupMaxMeetingDate + (7 * 24 * 60 * 60), day))

            } else if (meetingfrequency == 2) {
                et_Date.setText(validate!!.convertDatetime(GroupMaxMeetingDate + (14 * 24 * 60 * 60)))

            } else if (meetingfrequency == 3) {
                if (meetingfrequencyvalue == 6) {
                    et_Date.setText(
                        validate!!.setdate(
                            GroupMaxMeetingDate + (28 * 24 * 60 * 60),
                            day
                        )
                    )
                } else {
                    et_Date.setText(
                        validate!!.setday(
                            GroupMaxMeetingDate + (28 * 24 * 60 * 60),
                            day
                        )
                    )
                }

            }
        }

    }

    private fun generatemeeting(totalmtg: Int) {
        var Mtgdata =
            generateMeetingViewmodel!!.getgroupMeetingsdata(
                validate!!.RetriveSharepreferenceLong(
                    MeetingSP.shgid
                ).toString()
            )
        var meetingfrequency = validate!!.RetriveSharepreferenceInt(MeetingSP.meetingfrequency)
        var meetingfrequencyvalue =
            validate!!.RetriveSharepreferenceInt(MeetingSP.meetingfrequencyvalue)
        var meeting_on = validate!!.RetriveSharepreferenceInt(MeetingSP.meeting_on)
        var currentmeetingnumber = validate!!.returnIntegerValue(et_new_meeting_no.text.toString())

        if (totalmtg == 0) {

            var meeting_dayDiff =
                GroupMaxMeetingDate - validate!!.Daybetweentime(et_Date.text.toString())
            // if (meeting_dayDiff > 26 && totalmtg > 0 || totalmtg == 0) {


            var meetingguid = validate!!.random()
            var dtmtgEntity = DtmtgEntity(
                0,
                validate!!.returnLongValue(Mtgdata?.get(0)?.shg_id.toString()),
                meetingguid,
                0,
                currentmeetingnumber,
                validate!!.Daybetweentime(et_Date.text.toString()),
                "O",
                meetingfrequency,
                "",
                0,
                Mtgdata?.get(0)?.closing_balance,
                Mtgdata?.get(0)?.closing_balance,
                0,
                0,
                1,
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                0,
                0,
                0,
                Mtgdata?.get(0)?.sav_comp_cb,
                0,
                0,
                0,
                Mtgdata?.get(0)?.sav_vol_cb,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0.0,
                0.0,
                Mtgdata?.get(0)?.closing_balance_cash,
                Mtgdata?.get(0)?.closing_balance_cash,

                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                nullStringValue,
                nullLongValue,
                nullStringValue,
                nullLongValue,
                0, 0, 0, 0, "", ""
            )
            generateMeetingViewmodel!!.insert(dtmtgEntity)
            validate!!.SaveSharepreferenceInt(MeetingSP.currentmeetingnumber, currentmeetingnumber)
            validate!!.SaveSharepreferenceInt(MeetingSP.maxmeetingnumber, currentmeetingnumber)
            validate!!.SaveSharepreferenceLong(
                MeetingSP.CurrentMtgDate,
                validate!!.Daybetweentime(et_Date.text.toString())
            )
            validate!!.SaveSharepreferenceString(MeetingSP.mtg_guid, meetingguid)
            insertData_dtMtgDetaildata(
                meetingguid,
                currentmeetingnumber,
                validate!!.Daybetweentime(et_Date.text.toString()),
                totalmtg
            )


        } else {

            var meeting_dayDiff =
                GroupMaxMeetingDate - validate!!.Daybetweentime(et_Date.text.toString())
            // if (meeting_dayDiff > 26 && totalmtg > 0 || totalmtg == 0) {


            var meetingguid = validate!!.random()
            var dtmtgEntity = DtmtgEntity(
                0,
                validate!!.returnLongValue(Mtgdata?.get(0)?.shg_id.toString()),
                meetingguid,
                0,
                currentmeetingnumber,
                validate!!.Daybetweentime(et_Date.text.toString()),
                "O",
                meetingfrequency,
                "",
                0,
                Mtgdata?.get(0)?.closing_balance,
                Mtgdata?.get(0)?.closing_balance,
                0,
                0,
                1,
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                0,
                0,
                0,
                Mtgdata?.get(0)?.sav_comp_cb,
                0,
                0,
                0,
                Mtgdata?.get(0)?.sav_vol_cb,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0.0,
                0.0,
                Mtgdata?.get(0)?.closing_balance_cash,
                Mtgdata?.get(0)?.closing_balance_cash,
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                "",
                0,
                "",
                0,
                0, 0,
                0, 0,
                "", ""
            )
            generateMeetingViewmodel!!.insert(dtmtgEntity)
            validate!!.SaveSharepreferenceInt(MeetingSP.currentmeetingnumber, currentmeetingnumber)
            validate!!.SaveSharepreferenceInt(MeetingSP.maxmeetingnumber, currentmeetingnumber)
            validate!!.SaveSharepreferenceLong(
                MeetingSP.CurrentMtgDate,
                validate!!.Daybetweentime(et_Date.text.toString())
            )
            validate!!.SaveSharepreferenceString(MeetingSP.mtg_guid, meetingguid)
            insertData_dtMtgDetaildata(
                meetingguid,
                currentmeetingnumber,
                validate!!.Daybetweentime(et_Date.text.toString()),
                totalmtg
            )

        }
    }

    private fun insertData_dtMtgDetaildata(
        meetingguid: String,
        mtgNo: Int,
        mtgDate: Long,
        totalMeetings: Int
    ) {
        var memberList =
            memberViewmodel!!.getAllMemberlist(validate!!.RetriveSharepreferenceString(MeetingSP.SHGGUID))
        var dtmtgDetailList: List<DtmtgDetEntity>? = null
        if (totalMeetings > 0) {
            dtmtgDetailList = generateMeetingViewmodel!!.getMtgByMtgnum(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                (mtgNo - 1)
            )
            if (!memberList.isNullOrEmpty()) {
                for (i in 0 until memberList.size) {
                    var mtgDetailList = dtmtgDetailList.filter { dtmtgDetailList ->
                        dtmtgDetailList.mem_id == memberList.get(i).member_id
                    }
                    var dtmtgDetEntity: DtmtgDetEntity? = null

                    if (!mtgDetailList.isNullOrEmpty()) {
                        dtmtgDetEntity = DtmtgDetEntity(
                            0,
                            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                            memberList.get(i).member_id!!,
                            meetingguid,
                            mtgNo,
                            mtgDate,
                            validate!!.returnLongValue(memberList.get(i).member_code.toString()),
                            0,
                            memberList.get(i).member_name,
                            "",
                            mtgDetailList.get(0).sav_comp_cb,
                            0,
                            mtgDetailList.get(0).sav_comp_cb,
                            mtgDetailList.get(0).sav_vol_cb,
                            0,
                            mtgDetailList.get(0).sav_vol_cb,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            mtgDetailList.get(0).loan1_cb,
                            mtgDetailList.get(0).loan2_cb,
                            mtgDetailList.get(0).loan3_cb,
                            mtgDetailList.get(0).loan4_cb,
                            mtgDetailList.get(0).loan5_cb,
                            mtgDetailList.get(0).loan1_int_cb,
                            mtgDetailList.get(0).loan2_int_cb,
                            mtgDetailList.get(0).loan3_int_cb,
                            mtgDetailList.get(0).loan4_int_cb,
                            mtgDetailList.get(0).loan5_int_cb,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            mtgDetailList.get(0).loan1_cb,
                            mtgDetailList.get(0).loan2_cb,
                            mtgDetailList.get(0).loan3_cb,
                            mtgDetailList.get(0).loan4_cb,
                            mtgDetailList.get(0).loan5_cb,
                            mtgDetailList.get(0).loan1_int_cb,
                            mtgDetailList.get(0).loan2_int_cb,
                            mtgDetailList.get(0).loan3_int_cb,
                            mtgDetailList.get(0).loan4_int_cb,
                            mtgDetailList.get(0).loan5_int_cb,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            memberList.get(i).status.toString(),
                            0,
                            validate!!.RetriveSharepreferenceString(AppSP.userid),
                            validate!!.Daybetweentime(validate!!.currentdatetime),
                            nullStringValue,
                            nullLongValue,
                            nullStringValue,
                            nullLongValue,
                            0
                        )
                        generateMeetingViewmodel!!.insertdtmtgDet(dtmtgDetEntity)
                    } else {
                        dtmtgDetEntity = DtmtgDetEntity(
                            0,
                            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                            memberList.get(i).member_id!!,
                            meetingguid,
                            mtgNo,
                            mtgDate,
                            validate!!.returnLongValue(memberList.get(i).member_code.toString()),
                            0,
                            memberList.get(i).member_name,
                            "",
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            memberList.get(i).status.toString(),
                            0,
                            validate!!.RetriveSharepreferenceString(AppSP.userid),
                            validate!!.Daybetweentime(validate!!.currentdatetime),
                            nullStringValue,
                            nullLongValue,
                            nullStringValue,
                            nullLongValue,
                            0
                        )
                        generateMeetingViewmodel!!.insertdtmtgDet(dtmtgDetEntity)
                    }


                    var dtLoanTxnMemlist = dtLoanTxnMemViewmodel!!.getmeetingLoanTxnMemdata(
                        validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                        memberList.get(i).member_id!!,
                        GroupLastMeetingNumber

                    )
                    if (!dtLoanTxnMemlist.isNullOrEmpty()) {
                        for (i in 0 until dtLoanTxnMemlist.size) {
                            val principaldemand =
                                dtLoanMemberScheduleViewmodel!!.getprincipaldemand(
                                    dtLoanTxnMemlist.get(i).loan_no,
                                    dtLoanTxnMemlist.get(i).mem_id,
                                    mtgDate,
                                    validate!!.Daybetweentime(et_oldDate.text.toString())
                                )
                            val loanos = dtLoanMemberScheduleViewmodel!!.gettotaloutstanding(
                                dtLoanTxnMemlist.get(i).loan_no,
                                dtLoanTxnMemlist.get(i).mem_id
                            )
                            val insrate =
                                dtLoanMemberViewmodel!!.getinterestrate(dtLoanTxnMemlist.get(i).loan_no)
                            val loanosint = ((loanos * insrate / (100 * 365)) * validate!!.getday(
                                et_Date.text.toString(),
                                et_oldDate.text.toString()
                            ) + 0.5).toInt()
                            var intrestclosing =
                                loanosint + validate!!.returnIntegerValue(dtLoanTxnMemlist.get(i).int_accrued_cl.toString())
                            var dtLoanTxnMemEntity: DtLoanTxnMemEntity? = null

                            dtLoanTxnMemEntity = DtLoanTxnMemEntity(
                                0,
                                dtLoanTxnMemlist.get(i).cbo_id,
                                dtLoanTxnMemlist.get(i).mtg_guid,
                                mtgNo,
                                mtgDate,
                                dtLoanTxnMemlist.get(i).mem_id,
                                dtLoanTxnMemlist.get(i).loan_no,
                                loanos,
                                dtLoanTxnMemlist.get(i).loan_cl_int,
                                0,
                                0,
                                loanos,
                                dtLoanTxnMemlist.get(i).loan_cl_int,
                                dtLoanTxnMemlist.get(i).completion_flag,
                                dtLoanTxnMemlist.get(i).int_accrued_cl,
                                loanosint,
                                intrestclosing,
                                dtLoanTxnMemlist.get(i).principal_demand_cb,
                                principaldemand,
                                (validate!!.returnIntegerValue(dtLoanTxnMemlist.get(i).principal_demand_cb.toString()) + principaldemand),
                                0,
                                "",
                                "",
                                validate!!.RetriveSharepreferenceString(AppSP.userid),
                                validate!!.Daybetweentime(validate!!.currentdatetime),
                                nullStringValue,
                                nullLongValue,
                                nullStringValue,
                                nullLongValue,
                                dtLoanTxnMemlist.get(i).interest_rate,
                                dtLoanTxnMemlist.get(i).period
                            )
                            dtLoanTxnMemViewmodel!!.insert(dtLoanTxnMemEntity)
                            if (i == 0) {
                                generateMeetingViewmodel!!.updateloandata1(
                                    dtLoanTxnMemlist.get(i).mem_id,
                                    mtgNo,
                                    dtLoanTxnMemlist.get(i).loan_no,
                                    loanos,
                                    intrestclosing
                                )
                            } else if (i == 1) {
                                generateMeetingViewmodel!!.updateloandata2(
                                    dtLoanTxnMemlist.get(i).mem_id,
                                    mtgNo,
                                    dtLoanTxnMemlist.get(i).loan_no,
                                    loanos,
                                    intrestclosing
                                )
                            } else if (i == 2) {
                                generateMeetingViewmodel!!.updateloandata3(
                                    dtLoanTxnMemlist.get(i).mem_id,
                                    mtgNo,
                                    dtLoanTxnMemlist.get(i).loan_no,
                                    loanos,
                                    intrestclosing
                                )
                            } else if (i == 3) {
                                generateMeetingViewmodel!!.updateloandata4(
                                    dtLoanTxnMemlist.get(i).mem_id,
                                    mtgNo,
                                    dtLoanTxnMemlist.get(i).loan_no,
                                    loanos,
                                    intrestclosing
                                )
                            } else if (i == 4) {
                                generateMeetingViewmodel!!.updateloandata5(
                                    dtLoanTxnMemlist.get(i).mem_id,
                                    mtgNo,
                                    dtLoanTxnMemlist.get(i).loan_no,
                                    loanos,
                                    intrestclosing
                                )
                            }
                        }

                    }
                }
            }
            var txnlist = dtLoanGpTxnViewmodel.getListDataByMtgnum(
                GroupLastMeetingNumber,
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
            )
            if (!txnlist.isNullOrEmpty()) {
                for (i in 0 until txnlist.size) {
                    val principaldemand = dtMtgGrpLoanScheduleViewmodel.getprincipaldemand(
                        txnlist.get(i).loan_no,
                        txnlist.get(i).cbo_id,
                        mtgDate,
                        validate!!.Daybetweentime(et_oldDate.text.toString())
                    )
                    val loanos = dtMtgGrpLoanScheduleViewmodel.gettotaloutstanding(
                        txnlist.get(i).loan_no,
                        txnlist.get(i).cbo_id
                    )
                    val insrate = dtLoanGpViewmodel.getinterestrate(txnlist.get(i).loan_no)
                    val loanosint = ((loanos * insrate / (100 * 365)) * validate!!.getday(
                        et_Date.text.toString(),
                        et_oldDate.text.toString()
                    ) + 0.5).toInt()
                    var intrestclosing =
                        loanosint + validate!!.returnIntegerValue(txnlist.get(i).int_accrued_cl.toString())

                    var dtLoanGpTxnEntity: DtLoanGpTxnEntity? = null

                    dtLoanGpTxnEntity = DtLoanGpTxnEntity(
                        0,
                        txnlist.get(i).cbo_id,
                        meetingguid,
                        mtgNo,
                        mtgDate,
                        txnlist.get(i).loan_no,
                        loanos,
                        txnlist.get(i).loan_cl_int,
                        0,
                        0,
                        loanos,
                        txnlist.get(i).loan_cl_int,
                        txnlist.get(i).completion_flag,
                        txnlist.get(i).int_accrued_cl,
                        loanosint,
                        intrestclosing,
                        txnlist.get(i).principal_demand_cb,
                        principaldemand,
                        (validate!!.returnIntegerValue(txnlist.get(i).principal_demand_cb.toString()) + principaldemand),
                        0,
                        "",
                        "",
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        nullStringValue,
                        nullLongValue,
                        nullStringValue,
                        nullLongValue, 0, txnlist.get(i).interest_rate, txnlist.get(i).period
                    )
                    dtLoanGpTxnViewmodel.insert(dtLoanGpTxnEntity)
                }
            }


            var shgBankList = generateMeetingViewmodel!!.getBankdata(
                validate!!.RetriveSharepreferenceString(MeetingSP.SHGGUID)
            )
            for (i in 0 until shgBankList.size) {
                var dtMtgFinTxnEntity: DtMtgFinTxnEntity? = null
                var bankcode =
                    shgBankList.get(i).ifsc_code!!.dropLast(7) + shgBankList.get(i).account_no


                var gpfintxnlist = dtMtgFinTxnViewmodel!!.getListDataByMtgnum(
                    GroupLastMeetingNumber,
                    validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                    bankcode
                )
                var closingbal = 0
                var closingbalcash = 0
                if (!gpfintxnlist.isNullOrEmpty()) {
                    closingbal =
                        validate!!.returnIntegerValue(gpfintxnlist.get(0).closing_balance.toString())
                    closingbalcash =
                        validate!!.returnIntegerValue(gpfintxnlist.get(0).closing_balance_cash.toString())
                }
                dtMtgFinTxnEntity = DtMtgFinTxnEntity(
                    0,
                    validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                    meetingguid,
                    mtgNo,
                    bankcode,
                    closingbal,
                    closingbal,
                    0,
                    0,
                    0, 0,
                    closingbalcash,
                    closingbalcash,
                    validate!!.RetriveSharepreferenceString(AppSP.userid),
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    nullStringValue,
                    nullLongValue,
                    nullStringValue,
                    nullLongValue, 0, 0, 0, 0
                )
                dtMtgFinTxnViewmodel!!.insert(dtMtgFinTxnEntity)
            }


        } else {
            if (!memberList.isNullOrEmpty()) {
                for (i in 0 until memberList.size) {
                    var dtmtgDetEntity: DtmtgDetEntity? = null

                    dtmtgDetEntity = DtmtgDetEntity(
                        0,
                        validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                        memberList.get(i).member_id!!,
                        meetingguid,
                        mtgNo,
                        mtgDate,
                        validate!!.returnLongValue(memberList.get(i).member_code.toString()),
                        0,
                        memberList.get(i).member_name,
                        "",
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        memberList.get(i).status.toString(),
                        0,
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        nullStringValue,
                        nullLongValue,
                        nullStringValue,
                        nullLongValue,
                        0
                    )
                    generateMeetingViewmodel!!.insertdtmtgDet(dtmtgDetEntity)
                    var dtLoanTxnMemlist = dtLoanTxnMemViewmodel!!.getmeetingLoanTxnMemdata(
                        validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                        memberList.get(i).member_id!!,
                        GroupLastMeetingNumber

                    )
                    if (!dtLoanTxnMemlist.isNullOrEmpty()) {
                        for (i in 0 until dtLoanTxnMemlist.size) {
                            val principaldemand =
                                dtLoanMemberScheduleViewmodel!!.getprincipaldemand(
                                    dtLoanTxnMemlist.get(i).loan_no,
                                    dtLoanTxnMemlist.get(i).mem_id,
                                    mtgDate,
                                    validate!!.Daybetweentime(et_oldDate.text.toString())
                                )
                            val loanos = dtLoanMemberScheduleViewmodel!!.gettotaloutstanding(
                                dtLoanTxnMemlist.get(i).loan_no,
                                dtLoanTxnMemlist.get(i).mem_id
                            )
                            val insrate =
                                dtLoanMemberViewmodel!!.getinterestrate(dtLoanTxnMemlist.get(i).loan_no)
                            val loanosint = ((loanos * insrate / (100 * 365)) * validate!!.getday(
                                et_Date.text.toString(),
                                et_oldDate.text.toString()
                            ) + 0.5).toInt()
                            var dtLoanTxnMemEntity: DtLoanTxnMemEntity? = null

                            dtLoanTxnMemEntity = DtLoanTxnMemEntity(
                                0,
                                dtLoanTxnMemlist.get(i).cbo_id,
                                dtLoanTxnMemlist.get(i).mtg_guid,
                                mtgNo,
                                mtgDate,
                                dtLoanTxnMemlist.get(i).mem_id,
                                dtLoanTxnMemlist.get(i).loan_no,
                                loanos,
                                dtLoanTxnMemlist.get(i).loan_cl_int,
                                0,
                                0,
                                loanos,
                                dtLoanTxnMemlist.get(i).loan_cl_int,
                                dtLoanTxnMemlist.get(i).completion_flag,
                                dtLoanTxnMemlist.get(i).int_accrued_cl,
                                loanosint,
                                dtLoanTxnMemlist.get(i).int_accrued_cl,
                                dtLoanTxnMemlist.get(i).principal_demand_cb,
                                principaldemand,
                                (validate!!.returnIntegerValue(dtLoanTxnMemlist.get(i).principal_demand_cb.toString()) + principaldemand),
                                0,
                                "",
                                "",
                                validate!!.RetriveSharepreferenceString(AppSP.userid),
                                validate!!.Daybetweentime(validate!!.currentdatetime),
                                nullStringValue,
                                nullLongValue,
                                nullStringValue,
                                nullLongValue,
                                dtLoanTxnMemlist.get(i).interest_rate,
                                dtLoanTxnMemlist.get(i).period
                            )
                            dtLoanTxnMemViewmodel!!.insert(dtLoanTxnMemEntity)
                            if (i == 0) {
                                generateMeetingViewmodel!!.updateloandata1(
                                    dtLoanTxnMemlist.get(i).mem_id,
                                    mtgNo,
                                    dtLoanTxnMemlist.get(i).loan_no,
                                    loanos,
                                    loanosint
                                )
                            } else if (i == 1) {
                                generateMeetingViewmodel!!.updateloandata2(
                                    dtLoanTxnMemlist.get(i).mem_id,
                                    mtgNo,
                                    dtLoanTxnMemlist.get(i).loan_no,
                                    loanos,
                                    loanosint
                                )
                            } else if (i == 2) {
                                generateMeetingViewmodel!!.updateloandata3(
                                    dtLoanTxnMemlist.get(i).mem_id,
                                    mtgNo,
                                    dtLoanTxnMemlist.get(i).loan_no,
                                    loanos,
                                    loanosint
                                )
                            } else if (i == 3) {
                                generateMeetingViewmodel!!.updateloandata4(
                                    dtLoanTxnMemlist.get(i).mem_id,
                                    mtgNo,
                                    dtLoanTxnMemlist.get(i).loan_no,
                                    loanos,
                                    loanosint
                                )
                            } else if (i == 4) {
                                generateMeetingViewmodel!!.updateloandata5(
                                    dtLoanTxnMemlist.get(i).mem_id,
                                    mtgNo,
                                    dtLoanTxnMemlist.get(i).loan_no,
                                    loanos,
                                    loanosint
                                )
                            }
                        }

                    }
                }
            }

            var txnlist = dtLoanGpTxnViewmodel.getListDataByMtgnum(
                GroupLastMeetingNumber,
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
            )
            if (!txnlist.isNullOrEmpty()) {
                for (i in 0 until txnlist.size) {
                    val principaldemand = dtMtgGrpLoanScheduleViewmodel.getprincipaldemand(
                        txnlist.get(i).loan_no,
                        txnlist.get(i).cbo_id,
                        mtgDate,
                        validate!!.Daybetweentime(et_oldDate.text.toString())
                    )
                    val loanos = dtMtgGrpLoanScheduleViewmodel.gettotaloutstanding(
                        txnlist.get(i).loan_no,
                        txnlist.get(i).cbo_id
                    )
                    val insrate = dtLoanGpViewmodel.getinterestrate(txnlist.get(i).loan_no)
                    val loanosint = ((loanos * insrate / (100 * 365)) * validate!!.getday(
                        et_Date.text.toString(),
                        et_oldDate.text.toString()
                    ) + 0.5).toInt()

                    var dtLoanGpTxnEntity: DtLoanGpTxnEntity? = null

                    dtLoanGpTxnEntity = DtLoanGpTxnEntity(
                        0,
                        txnlist.get(i).cbo_id,
                        meetingguid,
                        mtgNo,
                        mtgDate,
                        txnlist.get(i).loan_no,
                        loanos,
                        txnlist.get(i).loan_cl_int,
                        0,
                        0,
                        loanos,
                        txnlist.get(i).loan_cl_int,
                        txnlist.get(i).completion_flag,
                        txnlist.get(i).int_accrued_cl,
                        loanosint.toInt(),
                        txnlist.get(i).int_accrued_cl,
                        txnlist.get(i).principal_demand_cb,
                        principaldemand,
                        (validate!!.returnIntegerValue(txnlist.get(i).principal_demand_cb.toString()) + principaldemand),
                        0,
                        "",
                        "",
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        nullStringValue,
                        nullLongValue,
                        nullStringValue,
                        nullLongValue, 0, txnlist.get(i).interest_rate, txnlist.get(i).period
                    )
                    dtLoanGpTxnViewmodel.insert(dtLoanGpTxnEntity)
                }
            }
            var shgBankList = generateMeetingViewmodel!!.getBankdata(
                validate!!.RetriveSharepreferenceString(MeetingSP.SHGGUID)
            )
            for (i in shgBankList.indices) {
                var dtMtgFinTxnEntity: DtMtgFinTxnEntity? = null
                var bankcode =
                    shgBankList.get(i).ifsc_code!!.dropLast(7) + shgBankList.get(i).account_no
                dtMtgFinTxnEntity = DtMtgFinTxnEntity(
                    0,
                    validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                    meetingguid,
                    mtgNo,
                    bankcode,
                    0,
                    0,
                    0,
                    0,
                    0, 0,
                    0,
                    0,
                    validate!!.RetriveSharepreferenceString(AppSP.userid),
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    nullStringValue,
                    nullLongValue,
                    nullStringValue,
                    nullLongValue, 0, 0, 0, 0
                )
                dtMtgFinTxnViewmodel!!.insert(dtMtgFinTxnEntity)
            }


        }
    }

    override fun onBackPressed() {
        var intent = Intent(this, SHGMeetingListActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

    fun updateShgType() {
        tv_count.setBackgroundResource(R.drawable.item_countactive)
        iv_stauscolor.setBackgroundResource(R.drawable.item_countactive)
        if (validate!!.RetriveSharepreferenceInt(MeetingSP.SHGType) == 1) {
            iv_stauscolor.setImageResource(R.drawable.ic_womangroup)
            iv_stauscolor.visibility = View.INVISIBLE
        } else {
            iv_stauscolor.visibility = View.VISIBLE
            if (validate!!.RetriveSharepreferenceInt(MeetingSP.Tag) == 1) {
                iv_stauscolor.setImageResource(R.drawable.ic_pvtg)
            } else if (validate!!.RetriveSharepreferenceInt(MeetingSP.Tag) == 2) {
                iv_stauscolor.setImageResource(R.drawable.ic_elderly)
            } else if (validate!!.RetriveSharepreferenceInt(MeetingSP.Tag) == 3) {
                iv_stauscolor.setImageResource(R.drawable.ic_disable)
            } else if (validate!!.RetriveSharepreferenceInt(MeetingSP.Tag) == 99) {
                iv_stauscolor.setImageResource(R.drawable.ic_other)
            }
        }
    }

    fun CustomAlertGenerateMeeting(str: String, totalmtg: Int, meetingType: Int) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str
        mDialogView.btn_yes.setOnClickListener {
            //            PartialSaveData()
            mAlertDialog.dismiss()

            validate!!.SaveSharepreferenceInt(MeetingSP.MeetingType, meetingType)
            generateZeroMeeting(totalmtg, meetingType)
            var intent = Intent(this, CutOffMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)

        }
        mDialogView.btn_no.setOnClickListener {
            mAlertDialog.dismiss()
            if (sCheckValid_RegularMeeting() == 1) {
                generatemeeting(totalmtg)
                validate!!.SaveSharepreferenceInt(MeetingSP.MeetingType, 0)
                var intent = Intent(this, MeetingMenuActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }
    }

    private fun generateZeroMeeting(totalmtg: Int, meetingType: Int) {
        var Mtgdata =
            generateMeetingViewmodel!!.getgroupMeetingsdata(
                validate!!.RetriveSharepreferenceLong(
                    MeetingSP.shgid
                ).toString()
            )
        var meetingfrequency = validate!!.RetriveSharepreferenceInt(MeetingSP.meetingfrequency)
        var meetingfrequencyvalue =
            validate!!.RetriveSharepreferenceInt(MeetingSP.meetingfrequencyvalue)
        var meeting_on = validate!!.RetriveSharepreferenceInt(MeetingSP.meeting_on)
        var currentmeetingnumber =
            validate!!.returnIntegerValue(et_new_meeting_no.text.toString())

        var meetingguid = validate!!.random()
        var dtmtgEntity = DtmtgEntity(
            0,
            validate!!.returnLongValue(Mtgdata?.get(0)?.shg_id.toString()),
            meetingguid,
            meetingType,
            currentmeetingnumber,
            validate!!.Daybetweentime(et_Date.text.toString()),
            "O",
            meetingfrequency,
            "",
            0,
            0,
            0,
            0,
            0,
            1,
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0.0,
            0.0,
            0,
            0,
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            nullStringValue,
            nullLongValue,
            nullStringValue,
            nullLongValue,
            0, 0,
            0, 0,
            "", ""
        )

        generateMeetingViewmodel!!.insert(dtmtgEntity)
        validate!!.SaveSharepreferenceInt(MeetingSP.currentmeetingnumber, currentmeetingnumber)
        validate!!.SaveSharepreferenceInt(MeetingSP.maxmeetingnumber, currentmeetingnumber)
        validate!!.SaveSharepreferenceLong(
            MeetingSP.CurrentMtgDate,
            validate!!.Daybetweentime(et_Date.text.toString())
        )
        validate!!.SaveSharepreferenceString(MeetingSP.mtg_guid, "")
        validate!!.SaveSharepreferenceString(MeetingSP.mtg_guid, meetingguid)

        insertCutoff_dtMtgDetaildata(
            meetingguid,
            currentmeetingnumber,
            validate!!.Daybetweentime(et_Date.text.toString()),
            totalmtg
        )


    }

    private fun insertCutoff_dtMtgDetaildata(
        meetingguid: String,
        mtgNo: Int,
        mtgDate: Long,
        totalMeetings: Int
    ) {
        var memberList =
            memberViewmodel!!.getAllMemberlist(validate!!.RetriveSharepreferenceString(MeetingSP.SHGGUID))

        if (!memberList.isNullOrEmpty()) {
            for (i in 0 until memberList.size) {

                var dtmtgDetEntity: DtmtgDetEntity? = null

                dtmtgDetEntity = DtmtgDetEntity(
                    0,
                    validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                    memberList.get(i).member_id!!,
                    meetingguid,
                    mtgNo,
                    mtgDate,
                    validate!!.returnLongValue(memberList.get(i).member_code.toString()),
                    0,
                    memberList.get(i).member_name,
                    "",
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    memberList.get(i).status.toString(),
                    0,
                    validate!!.RetriveSharepreferenceString(AppSP.userid),
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    nullStringValue,
                    nullLongValue,
                    nullStringValue,
                    nullLongValue,
                    0
                )
                generateMeetingViewmodel!!.insertdtmtgDet(dtmtgDetEntity)
            }
        }

        var shgBankList = generateMeetingViewmodel!!.getBankdata(
            validate!!.RetriveSharepreferenceString(MeetingSP.SHGGUID)
        )
        for (i in 0 until shgBankList.size) {
            var dtMtgFinTxnEntity: DtMtgFinTxnEntity? = null

            var bankcode =
                shgBankList.get(i).ifsc_code!!.dropLast(7) + shgBankList.get(i).account_no
            dtMtgFinTxnEntity = DtMtgFinTxnEntity(
                0,
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
                mtgNo,
                bankcode,
                0,
                0,
                0,
                0,
                0, 0,
                0,
                0,
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                nullStringValue,
                nullLongValue,
                nullStringValue,
                nullLongValue, 0, 0, 0, 0
            )
            dtMtgFinTxnViewmodel!!.insert(dtMtgFinTxnEntity)
        }

    }

    fun sCheckValidation(): Int {
        var iValue = 1
        var maxMeetingNum = generateMeetingViewmodel!!.getMaxMeetingNum(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        var meetingGap =
            validate!!.returnIntegerValue(et_new_meeting_no.text.toString()) - maxMeetingNum
        if (validate!!.returnIntegerValue(et_new_meeting_no.text.toString()) <= maxMeetingNum) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "meeting_num_less_last_meeting_num",
                    R.string.meeting_num_less_last_meeting_num
                ), this, et_new_meeting_no
            )
            iValue = 0
        } else if (meetingGap > 1 && meetingGap < 12) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "valid_meeting_num",
                    R.string.valid_meeting_num
                ), this, et_new_meeting_no
            )
            iValue = 0
        }
        return iValue
    }

    fun sCheckValid_RegularMeeting(): Int {
        var iValue = 1
        var maxMeetingNum = generateMeetingViewmodel!!.getMaxMeetingNum(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        var meetingGap =
            validate!!.returnIntegerValue(et_new_meeting_no.text.toString()) - maxMeetingNum
        if (meetingGap > 1) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "valid_meeting_num",
                    R.string.valid_meeting_num
                ), this, et_new_meeting_no
            )
            iValue = 0
        }
        return iValue
    }

}