package com.microware.cdfi.activity.meeting

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.*
import com.microware.cdfi.fragment.MeetingTopBarZeroFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_group_loanreceived.*
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.layout_group_investment.*

class GroupInvestmentActivity  : AppCompatActivity() {

    var dtLoanGpEntity: DtLoanGpEntity? = null
    var dtLoanGpTxnEntity: DtLoanGpTxnEntity? = null
    var lookupViewmodel: LookupViewmodel? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    lateinit var dtLoanGpViewmodel: DtLoanGpViewmodel
    lateinit var dtLoanGpTxnViewmodel: DtLoanGpTxnViewmodel

    var nullStringValue:String?=null
    var nullLongValue:Long?=null

    var validate: Validate? = null
    var savingType = 0
    var dataspin_loan_source: List<LookupEntity>? = null
    var dataspin_saving_type: List<MstCOAEntity>? = null
    var shgFinancialTxnDetailEntity: ShgFinancialTxnDetailEntity? = null
    var incomeandExpenditureViewmodel: IncomeandExpenditureViewmodel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_group_investment)

        validate = Validate(this)
        dtLoanGpViewmodel = ViewModelProviders.of(this).get(DtLoanGpViewmodel::class.java)
        dtLoanGpTxnViewmodel = ViewModelProviders.of(this).get(DtLoanGpTxnViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        generateMeetingViewmodel = ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        incomeandExpenditureViewmodel = ViewModelProviders.of(this).get(IncomeandExpenditureViewmodel::class.java)


        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                SaveData()

            }
        }

        btn_cancel.setOnClickListener {
            var intent = Intent(this, GroupInvestmentListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }


        replaceFragmenty(
            fragment = MeetingTopBarZeroFragment(8),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        setLabelText()
        fillSpinner()

        spin_source?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                var source = validate!!.returnLookupcode(spin_source, dataspin_loan_source)

                if(source > 0) {
                    if (source == 8) {
                        dataspin_saving_type = incomeandExpenditureViewmodel!!.getCoaSubHeadData(
                            listOf(46),
                            "OE",
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
                        )
                        validate!!.fillCoaSubHeadspinner(
                            this@GroupInvestmentActivity,
                            spin_saving_type,
                            dataspin_saving_type
                        )
                        spin_saving_type.setSelection(validate!!.returnSubHeadpos(savingType,dataspin_saving_type))
                    } else {
                        dataspin_saving_type = incomeandExpenditureViewmodel!!.getCoaSubHeadData(
                            listOf(25, 44, 45),
                            "OE",
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
                        )
                        validate!!.fillCoaSubHeadspinner(
                            this@GroupInvestmentActivity,
                            spin_saving_type,
                            dataspin_saving_type
                        )
                        spin_saving_type.setSelection(validate!!.returnSubHeadpos(savingType,dataspin_saving_type))
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        showData()
    }

    private fun SaveData() {

        var save = 0

        if (validate!!.RetriveSharepreferenceInt(MeetingSP.Auid) == 0) {
            shgFinancialTxnDetailEntity = ShgFinancialTxnDetailEntity(
                0,
                validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.returnSubHeadcode(spin_saving_type,dataspin_saving_type),/*receipt goes here auid mstcoa*/
                2,/*Receipt type goes here fundtype*/
                validate!!.returnLookupcode(spin_source,dataspin_loan_source),
                "OE",
                validate!!.returnIntegerValue(et_amount.text.toString()),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                0,
                1,
                "",
                "",
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                nullStringValue,nullLongValue, nullStringValue,nullLongValue
            )
            incomeandExpenditureViewmodel!!.insert(shgFinancialTxnDetailEntity!!)
            save = 1
        } else {
            incomeandExpenditureViewmodel!!.updateIncomeAndExpenditure(
                validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceInt(MeetingSP.Auid),
                2,
                validate!!.returnLookupcode(spin_source,dataspin_loan_source),
                "OE",
                validate!!.returnIntegerValue(et_amount.text.toString()),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                0,
                1,
                "",
                "",
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime)
            )

            save = 2
        }

        if (save == 1) {
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this, GroupInvestmentListActivity::class.java
            )
        } else {
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_updated_successfully",
                    R.string.data_updated_successfully
                ),
                this, GroupInvestmentListActivity::class.java
            )
        }


    }

    fun setLabelText() {
        tv_source.text = LabelSet.getText(
            "saving_source",
            R.string.saving_source
        )
        tv_saving_type.text = LabelSet.getText(
            "type_of_saving",
            R.string.type_of_saving
        )
        tv_amount.text = LabelSet.getText(
            "amount",
            R.string.amount
        )

        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )

    }



    private fun checkValidation(): Int {

        var value = 1
        if (spin_source.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_source,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "saving_source",
                    R.string.saving_source
                )

            )
            value = 0
            return value
        }
        if (spin_saving_type.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_saving_type,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "type_of_saving",
                    R.string.type_of_saving
                )

            )
            value = 0
            return value
        }
        if (validate!!.returnIntegerValue(et_amount.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "pleaseenteramount",
                    R.string.pleaseenteramount
                ), this,
                et_amount
            )
            value = 0
            return value
        }

        return value
    }

    private fun fillSpinner() {
//  listOf(1, 2)
        //itemType: List<Int>
        dataspin_loan_source = lookupViewmodel!!.getlookupMasterdata(91,validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),
            listOf(3,4,8)
        )

        validate!!.fillLookupspinner(this, spin_source, dataspin_loan_source)



    }

    override fun onBackPressed() {
        var intent = Intent(this, GroupInvestmentListActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

    private fun showData(){

        var list = incomeandExpenditureViewmodel!!.getInvestmentdata(validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceInt(MeetingSP.Auid),"OE",validate!!.RetriveSharepreferenceInt(MeetingSP.savingSource))
        if(!list.isNullOrEmpty()){
            spin_source.setSelection(validate!!.returnLookuppos(list.get(0).amount_to_from,dataspin_loan_source))
            savingType = list.get(0).auid

            et_amount.setText(validate!!.returnStringValue(list.get(0).amount.toString()))
        }
    }
}