package com.microware.cdfi.activity.meeting

import android.content.Intent
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.GroupInvestmentAdapter
import com.microware.cdfi.entity.ShgFinancialTxnDetailEntity
import com.microware.cdfi.fragment.MeetingTopBarZeroFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.IncomeandExpenditureViewmodel
import com.microware.cdfi.viewModel.LookupViewmodel
import kotlinx.android.synthetic.main.layout_group_investment_list.*

class GroupInvestmentListActivity  : AppCompatActivity() {
    var validate: Validate? = null
    var incomeandExpenditureViewmodel: IncomeandExpenditureViewmodel? = null
    var lookupViewmodel: LookupViewmodel? = null
    var Todayvalue = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_group_investment_list)

        validate = Validate(this)
        incomeandExpenditureViewmodel =
            ViewModelProviders.of(this).get(IncomeandExpenditureViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)


        ivAdd.setOnClickListener {
            validate!!.SaveSharepreferenceInt(MeetingSP.Auid, 0)
            val intent = Intent(this, GroupInvestmentActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        replaceFragmenty(
            fragment = MeetingTopBarZeroFragment(8),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        setLabelText()
        fillRecyclerview()
    }

    private fun fillRecyclerview() {
        Todayvalue = 0
        incomeandExpenditureViewmodel!!.getInvestmentListByMtgNum(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            listOf(25,44,45,46),"OE").observe(this, object : Observer<List<ShgFinancialTxnDetailEntity>?> {
            override fun onChanged(income_list: List<ShgFinancialTxnDetailEntity>?) {
                if (!income_list.isNullOrEmpty()) {
                    rvList.layoutManager =
                        LinearLayoutManager(this@GroupInvestmentListActivity)
                    val groupReceiptIncomeAdapter = GroupInvestmentAdapter(
                        this@GroupInvestmentListActivity,
                        income_list
                    )
                    val isize: Int
                    isize = income_list.size
                    val params: ViewGroup.LayoutParams = rvList.layoutParams
                    val r = resources
                    val px = TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP,
                        resources.getDimension(R.dimen.spraying),
                        r.displayMetrics
                    )
                    val hi = Math.round(px)
                    val gridHeight = hi * isize
                    params.height = gridHeight
                    rvList.layoutParams = params
                    rvList.adapter = groupReceiptIncomeAdapter

                }

            }
        })
    }

    fun getValue(keyCode: Int?,flag:Int): String? {
        var name: String? = null
        name = lookupViewmodel!!.getDestination(
            flag,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),
            keyCode
        )
        return name
    }

    fun returnSubHeadDescription(id: Int?): String? {
        var data = incomeandExpenditureViewmodel!!.getCoaSubHeadData(listOf(25,44,45,46),"OE",validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!)

        var value = ""
        if (!data.isNullOrEmpty()) {
            if (id!! > 0) {
                for (i in data.indices) {
                    if (id == data.get(i).uid)
                        value = validate!!.returnStringValue(data.get(i).description)
                }
            }
        }
        return value
    }
    private fun setLabelText() {
        tv_sr_no.text = LabelSet.getText("sr_no", R.string.sr_no)
        tv_type_of_saving.text = LabelSet.getText(
            "type_of_saving",
            R.string.type_of_saving
        )
        tv_saving_source.text = LabelSet.getText(
            "saving_source",
            R.string.saving_source
        )
        tv_amount.text = LabelSet.getText(
            "amount",
            R.string.amount
        )

    }

    fun getTotalValue() {
        var iValue1 = 0
        val iCount = rvList.childCount
        for (i in 0 until iCount) {
            val gridChild = rvList.getChildAt(i) as? ViewGroup
            val tv_value1 = gridChild!!.findViewById<View>(R.id.tv_amount) as? TextView

            if (!tv_value1!!.text.toString().isNullOrEmpty()) {
                iValue1 = iValue1 + validate!!.returnIntegerValue(tv_value1.text.toString())
            }
        }

        tv_TotalTodayValue.text = iValue1.toString()

    }

    fun getTotalValue1(iValue: Int) {
        Todayvalue = Todayvalue + iValue
        tv_TotalTodayValue.text = Todayvalue.toString()
    }

    override fun onBackPressed() {

            val intent = Intent(this, CutOffMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
    }

}