package com.microware.cdfi.activity.meeting

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.api.meetingmodel.FundTypeModel
import com.microware.cdfi.entity.*
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_group_loanreceived.*
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*

class GroupLoanReceivedActivity : AppCompatActivity() {

    var dtLoanGpEntity: DtLoanGpEntity? = null
    var dtMtgGrpLoanScheduleEntity: DtMtgGrpLoanScheduleEntity? = null
    var dtLoanGpTxnEntity: DtLoanGpTxnEntity? = null
    var lookupViewmodel: LookupViewmodel? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    lateinit var dtLoanGpViewmodel: DtLoanGpViewmodel
    lateinit var dtLoanGpTxnViewmodel: DtLoanGpTxnViewmodel
    lateinit var dtMtgGrpLoanScheduleViewmodel: DtMtgGrpLoanScheduleViewmodel
    var nullStringValue:String?=null
    var nullLongValue:Long?=null
    var validate: Validate? = null
    var dataspin_institution: List<LookupEntity>? = null
    var dataspin_loan_type: List<LookupEntity>? = null
    var dataspin_type_of_loan: List<LookupEntity>? = null
    var dataspin_mode_of_payment: List<LookupEntity>? = null
    var dataspin_frequency: List<LookupEntity>? = null
    var dataspin_fund: List<FundTypeModel>? = null
    var shgBankList: List<Cbo_bankEntity>? = null
    lateinit var loanProductViewmodel: LoanProductViewModel
    var dbPeriod  = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group_loanreceived)

        validate = Validate(this)
        dtLoanGpViewmodel = ViewModelProviders.of(this).get(DtLoanGpViewmodel::class.java)
        dtMtgGrpLoanScheduleViewmodel =
            ViewModelProviders.of(this).get(DtMtgGrpLoanScheduleViewmodel::class.java)
        dtLoanGpTxnViewmodel = ViewModelProviders.of(this).get(DtLoanGpTxnViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        loanProductViewmodel = ViewModelProviders.of(this).get(LoanProductViewModel::class.java)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)

        et_period_months.filters = arrayOf(InputFilterMinMax (1, 60))

        spin_frequency.isEnabled = false

        btn_save.isEnabled =
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber) >= validate!!.RetriveSharepreferenceInt(MeetingSP.maxmeetingnumber)
        et_date_loan_taken.setOnClickListener {
            validate!!.datePickerwithmindate(validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),et_date_loan_taken)
        }

        et_effective_date.setOnClickListener {
            validate!!.datePickerwithmindate(validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),et_effective_date)
        }
        et_date_loan_taken.setText(validate!!.convertDatetime(validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate)))
        et_effective_date.setText(validate!!.convertDatetime(validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate)))

        spin_institution?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                var source = validate!!.returnFundSourceId(spin_institution, dataspin_institution)

                    dataspin_fund =
                        loanProductViewmodel.getFundTypeBySource_Receipt(source,2)
                    validate!!.fillFundType(this@GroupLoanReceivedActivity,spin_loan_type,dataspin_fund)

                spin_type_of_loan.isEnabled = validate!!.returnFundSourceId(
                    spin_institution,dataspin_institution) == 8
                if(source == 8){
                    lay_effective_date.visibility=View.GONE
                    lay_loan_type.visibility = View.GONE
                    spin_loan_type.setSelection(0)
                    tv_loan_ref_astrix.visibility = View.VISIBLE
                    et_effective_date.setText("")
                }else {
                    lay_loan_type.visibility = View.VISIBLE
                    lay_effective_date.visibility = View.VISIBLE
                    tv_loan_ref_astrix.visibility = View.GONE
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        spin_type_of_loan?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (validate!!.returnlookupcode(
                        spin_type_of_loan,
                        dataspin_type_of_loan
                    ) == 1
                ) {
                    lay_drawinglimit.visibility = View.VISIBLE
                    lay_loan_sanctioned.visibility = View.VISIBLE
                } else {
                    lay_drawinglimit.visibility = View.GONE
                    lay_loan_sanctioned.visibility = View.GONE
                    et_drawinglimit.setText("")
                    et_loan_sanctioned.setText("")


                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }
        setLabelText()

        if(validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) == 11 || validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) == 12 ){
            lay_principal_overdue.visibility = View.VISIBLE
            lay_interest_overdue.visibility = View.VISIBLE
        }else {
            lay_principal_overdue.visibility = View.GONE
            lay_interest_overdue.visibility = View.GONE
        }
        var loanno = dtLoanGpViewmodel.getmaxLoanno(
            validate!!.RetriveSharepreferenceLong(
                MeetingSP.shgid
            )
        )
        et_loan_no.setText(
            (loanno + 1)
                .toString()
        )
        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                saveData(  validate!!.returnIntegerValue(et_moratorium_period.text.toString()))
                var initial_installment_date:Long? = null
                if(validate!!.returnFundSourceId(
                        spin_institution,dataspin_institution) == 8){
                    initial_installment_date = validate!!.Daybetweentime(et_date_loan_taken.text.toString())
                }else {
                    initial_installment_date = validate!!.Daybetweentime(validate!!.addDays(et_effective_date.text.toString(),5))
                }
                insertscheduler(
                    (validate!!.returnIntegerValue(et_amount_disbursed.text.toString()) - validate!!.returnIntegerValue(et_principal_overdue.text.toString())),
                    validate!!.returnIntegerValue(et_period_months.text.toString()),
                    validate!!.returnIntegerValue(et_moratorium_period.text.toString()),initial_installment_date)
            }
        }
        btn_cancel.setOnClickListener {
            var intent = Intent(this, MeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        spin_mode_of_payment?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (validate!!.returnlookupcode(
                        spin_mode_of_payment,
                        dataspin_mode_of_payment
                    ) == 2 || validate!!.returnlookupcode(
                        spin_mode_of_payment,
                        dataspin_mode_of_payment
                    ) == 3
                ) {
                    lay_bank.visibility = View.VISIBLE
                    lay_cheque_no_transactio_no.visibility = View.VISIBLE
                } else {
                    lay_bank.visibility = View.GONE
                    lay_cheque_no_transactio_no.visibility = View.GONE
                    spin_bank.setSelection(0)

                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }
        replaceFragmenty(
            fragment = MeetingTopBarFragment(15),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )


        fillSpinner()
        fillbank()
    }

    override fun onBackPressed() {
//        var intent = Intent(this, RepaymentActivity::class.java)
        var intent = Intent(this, GroupLoanlist::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

    private fun saveData(morotrum: Int) {
        var laonappid =
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid) + 1000 + validate!!.returnIntegerValue(
                et_loan_no.text.toString()
            ) + 1
        var originalPeriod  = 0
        dtLoanGpEntity = DtLoanGpEntity(
            0,
            laonappid,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
            validate!!.returnIntegerValue(et_loan_no.text.toString()),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            validate!!.Daybetweentime(et_date_loan_taken.text.toString()),
            validate!!.addmonth(
                validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
                morotrum,validate!!.returnlookupcode(spin_frequency, dataspin_frequency)
            ),
            validate!!.returnIntegerValue(et_amount_disbursed.text.toString()),
            0,
            validate!!.returnFundTypeId(spin_loan_type, dataspin_fund),
            validate!!.returnDoubleValue(et_interest_rate.text.toString()),
            validate!!.returnIntegerValue(et_period_months.text.toString()),
            validate!!.returnIntegerValue(et_principal_overdue.text.toString()),
            validate!!.returnIntegerValue(et_interest_overdue.text.toString()),
            0.0,
            false,
            validate!!.returnlookupcode(spin_type_of_loan, dataspin_type_of_loan),
            validate!!.returnFundSourceId(spin_institution, dataspin_institution),
            0,
            validate!!.returnlookupcode(spin_mode_of_payment, dataspin_mode_of_payment),
            returaccount(),
            validate!!.returnStringValue(et_cheque_no_transactio_no.text.toString()),
            validate!!.returnlookupcode(spin_frequency, dataspin_frequency),
            validate!!.returnIntegerValue(et_moratorium_period.text.toString()), "", 0,
            validate!!.returnFundSourceId(spin_institution, dataspin_institution),
            validate!!.returnIntegerValue(et_loan_sanctioned.text.toString()),
            validate!!.returnIntegerValue(et_drawinglimit.text.toString()),
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            nullStringValue,
            nullLongValue,
            nullStringValue,
            nullLongValue,validate!!.returnStringValue(et_loan_Refno.text.toString()),"",0,
            0,0,
            validate!!.Daybetweentime(et_effective_date.text.toString()),validate!!.returnDoubleValue(et_interest_rate.text.toString()),
            validate!!.returnIntegerValue(et_period_months.text.toString()),0,1


        )
        dtLoanGpViewmodel.insert(dtLoanGpEntity!!)

        // insert in dtloantxn

        dtLoanGpTxnEntity = DtLoanGpTxnEntity(
            0,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            validate!!.returnIntegerValue(et_loan_no.text.toString()),
            validate!!.returnIntegerValue(et_amount_disbursed.text.toString()),
            0, 0,
            0,
            0,
            0,

            false,

            0,
            0,
            0,

            0,
            0,
            0,
            validate!!.returnlookupcode(spin_mode_of_payment, dataspin_mode_of_payment),
            returaccount(),
            validate!!.returnStringValue(
                et_cheque_no_transactio_no.text.toString()
            ),
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            nullStringValue,
            nullLongValue,
            nullStringValue,
            nullLongValue,0,validate!!.returnDoubleValue(et_interest_rate.text.toString()), validate!!.returnIntegerValue(et_period_months.text.toString())
        )
        dtLoanGpTxnViewmodel.insert(dtLoanGpTxnEntity!!)


    }

    private fun insertscheduler(amt: Int, installment: Int, morotrum: Int,initial_installment_date:Long) {
        var principaldemand = amt / installment
        var loanos = 0
        var saveValue = 0
        for (i in 0 until installment) {
            if (i == installment - 1) {
                loanos = 0
                principaldemand = amt - (principaldemand * i)
            } else {
                loanos = amt - (principaldemand * (i + 1))
            }
            dtMtgGrpLoanScheduleEntity = DtMtgGrpLoanScheduleEntity(
                0,
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
                validate!!.returnIntegerValue(et_loan_no.text.toString()),
                principaldemand,
                principaldemand,
                loanos, 0,
                i + 1,
                1,
                validate!!.addmonth(
                    initial_installment_date,
                    (morotrum + i + 1),validate!!.returnlookupcode(spin_frequency, dataspin_frequency)
                ),
                //   validate!!.Daybetweentime(validate!!.currentdatetime),
                validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
                0, 0,
                null,

                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime), nullStringValue,
                nullLongValue,
                nullStringValue,
                nullLongValue

            )
            dtMtgGrpLoanScheduleViewmodel.insert(dtMtgGrpLoanScheduleEntity!!)
        }
        var principalDemand =
            dtMtgGrpLoanScheduleViewmodel.getPrincipalDemandByInstallmentNum(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
                validate!!.returnIntegerValue(et_loan_no.text.toString()),
                1
            )
        principalDemand += validate!!.returnIntegerValue(et_principal_overdue.text.toString())
        dtMtgGrpLoanScheduleViewmodel.updateCutOffLoanSchedule(
            validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.returnIntegerValue(et_loan_no.text.toString()),
            1,
            principalDemand

        )
        CustomAlertchangeschedule(
            LabelSet.getText(
                "doyouwanttochaneschedule",
                R.string.doyouwanttochaneschedule
            )
        )
    }

    fun setLabelText() {
        tv_institution.text = LabelSet.getText(
            "institution",
            R.string.institution
        )
        tv_loan_no.text = LabelSet.getText(
            "loan_no",
            R.string.loan_no
        )
        et_loan_no.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_disbursal_date.text = LabelSet.getText(
            "dibursal_date",
            R.string.disbursal_date
        )
        tv_effective_date.text = LabelSet.getText(
            "effective_date",
            R.string.effective_date
        )
        if(validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) == 11 || validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) == 12 ){
            tv_loan_sanctioned.text = LabelSet.getText(
                "loan_sanctioned_amount",
                R.string.loan_sanctioned_amount
            )
            tv_amount_disbursed.text = LabelSet.getText(
                "current_principal_due",
                R.string.current_principal_due
            )
            tv_principal_overdue.text = LabelSet.getText(
                "principal_overdue",
                R.string.principal_overdue
            )
            tv_interest_overdue.text = LabelSet.getText(
                "interest_overdue",
                R.string.interest_overdue
            )
        }else {
            tv_loan_sanctioned.text = LabelSet.getText(
                "loan_sanctioned",
                R.string.loan_sanctioned
            )
            tv_amount_disbursed.text = LabelSet.getText(
                "amount_received",
                R.string.amount_received
            )
        }
        et_date_loan_taken.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )
        tv_loan_type.text = LabelSet.getText(
            "fund_type",
            R.string.fund_type
        )
        //  tv_amount_disbursed.setText(LabelSet.getText("amount_received", R.string.amount_received))
        tv_loan_Refno.text = LabelSet.getText(
            "loan_reference_no",
            R.string.loan_reference_no
        )
        et_loan_sanctioned.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_amount_disbursed.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_period_months.text = LabelSet.getText(
            "period_months",
            R.string.period_months
        )
        et_period_months.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_interest_rate.text = LabelSet.getText(
            "interest_rate",
            R.string.interest_rate
        )
        et_interest_rate.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_mode_of_payment.text = LabelSet.getText(
            "mode_of_payment",
            R.string.mode_of_payment
        )
        tv_bank.text = LabelSet.getText("bank", R.string.bank)
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
        //  tv_title.setText(LabelSet.getText("group_loan_received",R.string.group_loan_received))

    }

    fun fillbank(
    ) {
        shgBankList =
            generateMeetingViewmodel.getBankdata(validate!!.RetriveSharepreferenceString(MeetingSP.SHGGUID))

        val adapter: ArrayAdapter<String?>
        if (!shgBankList.isNullOrEmpty()) {
            val isize = shgBankList!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in shgBankList!!.indices) {
                var lastthree=shgBankList!![i].account_no.substring(shgBankList!![i].account_no.length-3)
                sValue[i + 1] =
                    shgBankList!![i].ifsc_code.toString().dropLast(7) +"XXXXX"+ lastthree
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_bank.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_bank.adapter = adapter
        }

    }

    fun returaccount(): String {

        var pos = spin_bank.selectedItemPosition
        var id = ""

        if (!shgBankList.isNullOrEmpty()) {
            if (pos > 0) id =
                shgBankList!!.get(pos - 1).ifsc_code!!.dropLast(7) + shgBankList!!.get(pos - 1).account_no
        }
        return id
    }

    fun setaccount(accountno: String): Int {

        var pos = 0

        if (!shgBankList.isNullOrEmpty()) {
            for (i in shgBankList!!.indices) {
                if (accountno.equals(
                        shgBankList!!.get(i).ifsc_code!!.dropLast(7) + shgBankList!!.get(
                            i
                        ).account_no
                    )
                )
                    pos = i + 1
            }
        }
        return pos
    }

    private fun checkValidation(): Int {

        var value = 1
        if (spin_institution.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_institution,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "institution",
                    R.string.institution
                )

            )
            value = 0
            return value
        }
        if (validate!!.returnFundSourceId(spin_institution,dataspin_institution)==8 && validate!!.returnIntegerValue(et_loan_Refno.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter_loan_ref_no",
                    R.string.please_enter_loan_ref_no
                ), this,
                et_loan_Refno
            )
            value = 0
            return value
        }
        if (lay_loan_sanctioned.visibility == View.VISIBLE && validate!!.returnIntegerValue(et_loan_sanctioned.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                )+LabelSet.getText(
                    "loan_sanctioned",
                    R.string.loan_sanctioned
                ), this,
                et_loan_sanctioned
            )
            value = 0
            return value
        }
        if (lay_drawinglimit.visibility == View.VISIBLE && validate!!.returnIntegerValue(et_drawinglimit.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                )+LabelSet.getText(
                    "drawinglimit",
                    R.string.drawinglimit
                ), this,
                et_loan_sanctioned
            )
            value = 0
            return value
        }
        if ((validate!!.returnIntegerValue(et_drawinglimit.text.toString()) > validate!!.returnIntegerValue(et_loan_sanctioned.text.toString()) && lay_drawinglimit.visibility == View.VISIBLE )) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "drawing_sanctioned_amount",
                    R.string.drawing_sanctioned_amount
                ), this,
                et_drawinglimit
            )
            value = 0
            return value
        }
        if (validate!!.returnStringValue(et_date_loan_taken.text.toString()).length ==0  ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                )+LabelSet.getText(
                    "disbursement_date",
                    R.string.disbursement_date
                ), this,
                et_date_loan_taken
            )

            value = 0
            return value
        }
        if (validate!!.returnFundSourceId(spin_institution,dataspin_institution)!=8 && spin_loan_type.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_loan_type,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "fund_type",
                    R.string.fund_type
                )

            )
            value = 0
            return value
        }
        if (validate!!.returnIntegerValue(et_amount_disbursed.text.toString()) ==0  ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                )+LabelSet.getText(
                    "pleaseenterdisbursedloanamount",
                    R.string.pleaseenterdisbursedloanamount
                ), this,
                et_amount_disbursed
            )

            value = 0
            return value
        }
        if ((validate!!.returnIntegerValue(et_amount_disbursed.text.toString()) > validate!!.returnIntegerValue(et_drawinglimit.text.toString()) && lay_drawinglimit.visibility == View.VISIBLE )) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "disbursed_drawing_limit",
                    R.string.disbursed_drawing_limit
                ), this,
                et_amount_disbursed
            )
            value = 0
            return value
        }


        if (validate!!.returnDoubleValue(et_interest_rate.text.toString()) ==0.0  ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                )+LabelSet.getText(
                    "interest_rate",
                    R.string.interest_rate
                ), this,
                et_interest_rate
            )

            value = 0
            return value
        }

        if (validate!!.returnDoubleValue(et_interest_rate.text.toString()) >36  ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "valid_interest",
                    R.string.valid_interest
                ), this,
                et_interest_rate
            )

            value = 0
            return value
        }
        if (validate!!.returnIntegerValue(et_period_months.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "pleaseenterintallment",
                    R.string.pleaseenterintallment
                ), this,
                et_period_months
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_loan_no.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "pleaseenterloanno",
                    R.string.pleaseenterloanno
                ), this,
                et_loan_no
            )
            value = 0
            return value
        }
        if (spin_mode_of_payment.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_mode_of_payment,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "mode_of_payment",
                    R.string.mode_of_payment
                )

            )
            value = 0
            return value
        }
        if (lay_bank.visibility == View.VISIBLE &&spin_bank.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_bank,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText("bank", R.string.bank)

            )
            value = 0
            return value
        }
        if (lay_cheque_no_transactio_no.visibility == View.VISIBLE && validate!!.returnStringValue(
                et_cheque_no_transactio_no.text.toString()
            ).length == 0
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + LabelSet.getText(
                    "cheque_no_transactio_no",
                    R.string.cheque_no_transactio_no
                ), this,
                et_cheque_no_transactio_no
            )
            value = 0
            return value
        }
        if (spin_frequency.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_frequency,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "loan_repayment_frequency",
                    R.string.loan_repayment_frequency
                )

            )
            value = 0
            return value
        }
        if (validate!!.returnlookupcode(
                spin_frequency,
                dataspin_frequency
            ) < validate!!.RetriveSharepreferenceInt(MeetingSP.meetingfrequency)
        ) {
            if (validate!!.RetriveSharepreferenceInt(MeetingSP.meetingfrequency) == 2) {
                validate!!.CustomAlertSpinner(
                    this, spin_frequency,
                    LabelSet.getText(
                        "monthlyfortnightly",
                        R.string.monthlyfortnightly
                    )

                )
            } else if (validate!!.RetriveSharepreferenceInt(MeetingSP.meetingfrequency) == 3) {
                validate!!.CustomAlertSpinner(
                    this, spin_frequency,
                    LabelSet.getText(
                        "yuocanselectmonthaly",
                        R.string.yuocanselectmonthaly
                    )

                )
            }else {
                validate!!.CustomAlertSpinner(
                    this, spin_frequency,
                    LabelSet.getText(
                        "please_select",
                        R.string.please_select
                    ) + " " + LabelSet.getText(
                        "loan_repayment_frequency",
                        R.string.loan_repayment_frequency
                    )

                )
            }
            value = 0
            return value
        }
        return value
    }

    private fun fillSpinner() {
        dataspin_institution = lookupViewmodel!!.getlookupMasterdata(
            91,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),listOf(3,4,8,6,99))

        dataspin_type_of_loan = lookupViewmodel!!.getlookup(88,validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))

        dataspin_mode_of_payment = lookupViewmodel!!.getlookupMasterdata(
            65,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),listOf(0,1,2))

        dataspin_frequency = lookupViewmodel!!.getlookup(
            19,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        validate!!.fillFundSource(this, spin_institution, dataspin_institution)

        validate!!.fillspinner(this, spin_type_of_loan, dataspin_type_of_loan)
        validate!!.fillspinner(this, spin_mode_of_payment, dataspin_mode_of_payment)
        validate!!.fillspinner(this, spin_frequency, dataspin_frequency)
        spin_mode_of_payment.setSelection(validate!!.returnlookupcodepos(1, dataspin_mode_of_payment))
        spin_frequency.setSelection(3)
        spin_type_of_loan.setSelection(2)
    }

    fun CustomAlertchangeschedule(str: String) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str
        mDialogView.btn_yes.setOnClickListener {
            //            PartialSaveData()
            mAlertDialog.dismiss()
            /*
            *    validate!!.SaveSharepreferenceString(MeetingSP.Institution, validate!!.returnStringValue(holder.tv_source.text.toString()))
             validate!!.SaveSharepreferenceInt(MeetingSP.Loanno, validate!!.returnIntegerValue(list.get(position).loan_no.toString()))*/
            var institutionId = validate!!.returnFundSourceId(spin_institution, dataspin_institution)
            validate!!.SaveSharepreferenceString(MeetingSP.Institution,validate!!.returnlookupcodevalue(institutionId, dataspin_institution))
            validate!!.SaveSharepreferenceInt(
                MeetingSP.Loanno,
                validate!!.returnIntegerValue(et_loan_no.text.toString())
            )

            val intent = Intent(this, GroupPrincipalDemandActivity::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)


        }
        mDialogView.btn_no.setOnClickListener {
            mAlertDialog.dismiss()

            val intent = Intent(this, GroupLoanlist::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)

        }
    }
}