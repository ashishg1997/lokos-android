package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.utility.replaceFragmenty
import kotlinx.android.synthetic.main.activity_group_loadrepaid.*
import kotlinx.android.synthetic.main.buttons.*

class GroupLoanRepaidActivity : AppCompatActivity() {

    var lookupViewmodel: LookupViewmodel? = null
    var validate: Validate? = null
    var dataspin_institution: List<LookupEntity>? = null
    var dataspin_mode_of_payment: List<LookupEntity>? = null
    var dataspin_bank: List<LookupEntity>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group_loadrepaid)

        validate = Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        setLabelText()



        btn_cancel.setOnClickListener {
            var intent = Intent(this, MeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        replaceFragmenty(
            fragment = MeetingTopBarFragment(16),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )
        fillSpinner()
    }

    override fun onBackPressed() {
//        var intent = Intent(this, RepaymentActivity::class.java)
        var intent = Intent(this, MeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0,0)
        finish()
    }

    fun setLabelText()
    {
        tv_institution.text = LabelSet.getText(
            "institution",
            R.string.institution
        )
        tv_loan_no.text = LabelSet.getText(
            "loan_no",
            R.string.loan_no
        )
        et_loan_no.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_Amount.text = LabelSet.getText(
            "amount",
            R.string.amount
        )
        et_Amount.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_principal_demand.text = LabelSet.getText(
            "principal_demand",
            R.string.principal_demand
        )
        et_principal_demand.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_interest_demand.text = LabelSet.getText(
            "interest_demand",
            R.string.interest_demand
        )
        et_interest_demand.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_penalty_paid.text = LabelSet.getText(
            "penalty_paid",
            R.string.penalty_paid
        )
        et_penalty_paid.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_principal_paid.text = LabelSet.getText(
            "principal_paid",
            R.string.principal_paid
        )
        et_principal_paid.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_interest_paid.text = LabelSet.getText(
            "interest_paid",
            R.string.interest_paid
        )
        et_interest_paid.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_date_loan_taken.text = LabelSet.getText(
            "date_loan_taken",
            R.string.date_loan_taken
        )
        et_date_loan_taken.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )
        tv_mode_of_payment.text = LabelSet.getText(
            "mode_of_payment",
            R.string.mode_of_payment
        )
        tv_bank.text = LabelSet.getText("bank", R.string.bank)
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
        btn_savegray.text = LabelSet.getText(
            "save",
            R.string.save
        )
      //  tv_title.setText(LabelSet.getText("group_loan_repaid",R.string.group_loan_repaid))

    }

    private fun fillSpinner() {
        dataspin_institution = lookupViewmodel!!.getlookup(
            99,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_mode_of_payment = lookupViewmodel!!.getlookup(
            99,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_bank = lookupViewmodel!!.getlookup(
            99,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        validate!!.fillspinner(this, spin_institution, dataspin_institution)
        validate!!.fillspinner(this, spin_mode_of_payment, dataspin_mode_of_payment)
        validate!!.fillspinner(this, spin_bank, dataspin_bank)
    }
}