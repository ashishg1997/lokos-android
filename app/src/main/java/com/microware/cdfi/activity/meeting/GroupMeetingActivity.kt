package com.microware.cdfi.activity.meeting

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.microware.cdfi.R
import com.microware.cdfi.utility.LabelSet
import kotlinx.android.synthetic.main.activity_group_meeting.*
import kotlinx.android.synthetic.main.repay_toolbar.*
import kotlinx.android.synthetic.main.repaytablayout.*

class GroupMeetingActivity  : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group_meeting)

        /*IvAttendance.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSaving.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvCompulsorySaving.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloan_disbursment.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvWithdrawal.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPenalty.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvRepayment.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvgroupLoanRepaid.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvgroupLoanReceived.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBankTransaction.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvCashBox.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvGroupMeeting.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
        IvExpenditurePayment.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvRecipientIncome.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))*/

        ic_Back.setOnClickListener {
            var intent = Intent(this, MeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_attendance.setOnClickListener {
            var intent = Intent(this, AttendnceDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_compulsorySaving.setOnClickListener {
            var intent = Intent(this, CompulsorySavingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        lay_saving.setOnClickListener {
            var intent = Intent(this, VoluntorySavingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        lay_loan_disbursment.setOnClickListener {
            var intent = Intent(this, LoanDisbursement1Activity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_withdrawal.setOnClickListener {
            var intent = Intent(this, WidthdrawalDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_penalty.setOnClickListener {
            var intent = Intent(this, PeneltyDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_repayment.setOnClickListener {
            var intent = Intent(this, RepaymentDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_groupLoanRepaid.setOnClickListener {
            var intent = Intent(this, GroupLoanRepaidActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_groupLoanReceived.setOnClickListener {
            var intent = Intent(this, GroupLoanReceivedActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_BankTransaction.setOnClickListener {
            var intent = Intent(this, BankGroupTransactionActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_CashBox.setOnClickListener {
            var intent = Intent(this, CashBoxActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_ExpenditurePayment.setOnClickListener {
            var intent = Intent(this, ExpenditurePaymentActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_RecipientIncome.setOnClickListener {
            var intent = Intent(this, RecipentIncomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        setLabelText()



    }

    override fun onBackPressed() {
        var intent = Intent(this, MeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0,0)
        finish()
    }

    fun setLabelText()
    {

        tv_total_saving.text = LabelSet.getText(
            "total_saving",
            R.string.total_saving
        )
        tv_total_repayments.text = LabelSet.getText(
            "total_repayments",
            R.string.total_repayments
        )
        tv_total_amount_disbursed.text = LabelSet.getText(
            "total_amount_disbursed",
            R.string.total_amount_disbursed
        )
        tv_cash_in_hand.text = LabelSet.getText(
            "cash_in_hand",
            R.string.cash_in_hand
        )
        tv_cash_at_bank.text = LabelSet.getText(
            "cash_at_bank",
            R.string.cash_at_bank
        )
        tv_insights.text = LabelSet.getText(
            "insights",
            R.string.insights
        )
        tv_member_attendance.text = LabelSet.getText(
            "member_attendance",
            R.string.member_attendance
        )
        tv_repayment.text = LabelSet.getText(
            "repayment",
            R.string.repayment
        )
        btn_save.text = LabelSet.getText(
            "send_for_approval",
            R.string.send_for_approval
        )
        tv_title.text = LabelSet.getText(
            "member_meeting",
            R.string.member_meeting
        )

    }
}