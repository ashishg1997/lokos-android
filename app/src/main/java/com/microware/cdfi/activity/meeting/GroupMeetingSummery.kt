package com.microware.cdfi.activity.meeting

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.layout_group_summary.*

class GroupMeetingSummery  : AppCompatActivity(){

    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    var dtLoanTxnMemViewmodel: DtLoanTxnMemViewmodel? = null
    var dtLoanMemberViewmodel: DtLoanMemberViewmodel? = null
    var dtLoanViewmodel: DtLoanViewmodel? = null
    var dtLoanGpViewmodel: DtLoanGpViewmodel? = null
    var dtLoanGPTxnViewmodel:  DtLoanGpTxnViewmodel? = null

    var validate: Validate? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_group_summary)

        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        dtLoanTxnMemViewmodel =ViewModelProviders.of(this).get(DtLoanTxnMemViewmodel::class.java)
        dtLoanMemberViewmodel =ViewModelProviders.of(this).get(DtLoanMemberViewmodel::class.java)
        dtLoanViewmodel =ViewModelProviders.of(this).get(DtLoanViewmodel::class.java)
        dtLoanGpViewmodel =ViewModelProviders.of(this).get(DtLoanGpViewmodel::class.java)
        dtLoanGPTxnViewmodel =ViewModelProviders.of(this).get(DtLoanGpTxnViewmodel::class.java)

        validate = Validate(this)

        replaceFragmenty(
            fragment = MeetingTopBarFragment(11),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        if(validate!!.RetriveSharepreferenceInt(MeetingSP.buttonHide)==1){
            btn_sendApproval.visibility = View.GONE
        }else {
            btn_sendApproval.visibility = View.VISIBLE
        }
        btn_sendApproval.setOnClickListener {
            generateMeetingViewmodel.closingMeeting("C",validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.Daybetweentime(validate!!.currentdatetime),validate!!.RetriveSharepreferenceString(
                    AppSP.userid)!!)

            var intent = Intent(this, SHGMeetingListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        //   setLabelText()
        setValues()
    }

    override fun onBackPressed() {
//        var intent = Intent(this, RepaymentActivity::class.java)
        var intent = Intent(this, MeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0,0)
        finish()
    }

    private fun setValues(){
        val TotalPresent = generateMeetingViewmodel.getTotalPresentAndAbsent(
            "1",
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )

        val TotalMember = generateMeetingViewmodel.getTotalMember(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )

        val totalCompulsarySaving = LabelSet.getText("",R.string.rs_sign) + generateMeetingViewmodel.getsumcomp(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)).toString()

        val expectedCompulsarySaving = LabelSet.getText("",R.string.rs_sign) + (generateMeetingViewmodel.getMonthlyCompulsarySaving(validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)) * TotalMember).toString()

        var memberRepaymentReceived = dtLoanTxnMemViewmodel!!.getLoanRepaymentAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber))

        var totalMemberRepaymentDemand = generateMeetingViewmodel.getTotalPrincipalDemand(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)) + generateMeetingViewmodel.getTotalPrincipalDemandOb(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)) + generateMeetingViewmodel.getTotalInterestAccured(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)) + generateMeetingViewmodel.getTotalInterestAccuredOb(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))

        var totalLoanReceived = dtLoanGpViewmodel!!.gettotloangroupamount(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        var totalGroupLoans = generateMeetingViewmodel.getTotalLoans(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        var totalGroupRepayment = dtLoanGPTxnViewmodel!!.getLoanRepaymentAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber))
        var TotalGrpRepaymentDemand = generateMeetingViewmodel.getTotalGrpRepaymentDemand(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber))

        var totalGrpLoansRepaid = generateMeetingViewmodel.getTotalGrpLoansRepaid(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber))
        var totalGrpLoans = generateMeetingViewmodel.getTotalGrpLoans(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber))

        var totalLoanDisbursed = dtLoanMemberViewmodel!!.getMemberLoanDisbursedAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber))

        var totalLoanRequest = dtLoanViewmodel!!.gettotaldemand(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))

        var groupPayments = generateMeetingViewmodel.getTotalIncoming_OutgoingAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            "OE")

        var memberPayments = generateMeetingViewmodel.getMemberReceiptAmount( validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            "OP")

        var totalPayments = groupPayments + memberPayments

        var memberReceipt = generateMeetingViewmodel.getMemberReceiptAmount( validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            "OR")
        var groupIncome = generateMeetingViewmodel.getTotalIncoming_OutgoingAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            "OI")
        var totalIncomingAmount = memberReceipt + groupIncome
        var totalBankAccount = generateMeetingViewmodel.getBankCount(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        tv_attendence.text = TotalPresent.toString() + "/" + TotalMember.toString()
        tv_savingAmt.text = totalCompulsarySaving + "/" + expectedCompulsarySaving
        tv_savingcount.text = TotalPresent.toString() + "/" + TotalMember.toString()
        tv_RepaymentAmt.text = LabelSet.getText("",R.string.rs_sign) + memberRepaymentReceived.toString() + "/" +
                LabelSet.getText("",R.string.rs_sign) + totalMemberRepaymentDemand.toString()
        tv_savingcount1.text = TotalPresent.toString() + "/" + TotalMember.toString()
        tv_DisbursedAmt.text = LabelSet.getText("",R.string.rs_sign) + totalLoanDisbursed.toString() + "/" +
                LabelSet.getText("",R.string.rs_sign) + totalLoanRequest.toString()
        tv_amt.text = LabelSet.getText("",R.string.rs_sign) + totalLoanReceived.toString()
        tv_loanreccount.text = totalGroupLoans.toString()
        tv_LoanRepayAmt.text = LabelSet.getText("",R.string.rs_sign) + totalGroupRepayment.toString()+"/" + LabelSet.getText("",R.string.rs_sign) +  TotalGrpRepaymentDemand.toString()
        tv_count3.text = totalGrpLoansRepaid.toString() + "/" + totalGrpLoans.toString()
        tv_receiptIncomeAmt.text = LabelSet.getText("",R.string.rs_sign) + totalIncomingAmount.toString()
        tv_paymentAmt.text = LabelSet.getText("",R.string.rs_sign) + totalPayments.toString()
        tv_cashAmt.text = LabelSet.getText("",R.string.rs_sign) + validate!!.RetriveSharepreferenceInt(MeetingSP.Cashinhand).toString()
        tv_bankBalanceAmt.text = LabelSet.getText("",R.string.rs_sign) + generateMeetingViewmodel.gettotalinbank(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)).toString()
        tv_count4.text = totalBankAccount.toString()
    }

    fun setLabelText(){
        tvAttendanceLabel.text = LabelSet.getText("member_present_total_member",R.string.member_present_total_member)
        tvSavingsLabel.text = LabelSet.getText("total_saving_expected_saving",R.string.total_saving_expected_saving)
        tvMemberRepaymentLabel.text = LabelSet.getText("repayment_received_repayment_demand",R.string.repayment_received_repayment_demand)
        tvLoanDisbursedLabel.text = LabelSet.getText("loan_disbursed_loan_request",R.string.loan_disbursed_loan_request)
        tvshgrepaymentLabel.text = LabelSet.getText("no_of_loans_repaid_total_loans",R.string.no_of_loans_repaid_total_loans)
        tv_bank_count.text = LabelSet.getText("no_of_bank_accounts",R.string.no_of_bank_accounts)
        tv_memattendence.text = LabelSet.getText("member_attendance",R.string.member_attendance)
        tv_savingByMembernce.text = LabelSet.getText("saving_by_members",R.string.saving_by_members)
        tv_memberRepayment.text = LabelSet.getText("loan_repayment_by_members",R.string.loan_repayment_by_members)
        tv_loanDisbursed.text = LabelSet.getText("loan_disbursed_to_members",R.string.loan_disbursed_to_members)
        tv_loanRec.text = LabelSet.getText("loan_received_by_shg",R.string.loan_received_by_shg)
        tv_loanRepayment.text = LabelSet.getText("loan_repayment_by_shg",R.string.loan_repayment_by_shg)
        tv_receiptIncome.text = LabelSet.getText("receipts_and_incomes",R.string.receipts_and_incomes)
        tv_payment.text = LabelSet.getText("expenditure_and_payment",R.string.expenditure_and_payment)
        tv_cashBalance.text = LabelSet.getText("cash_balance",R.string.cash_balance)
        tv_bankBalance.text = LabelSet.getText("bank_balance",R.string.bank_balance)
        btn_sendApproval.text = LabelSet.getText("close_send_for_approval",R.string.close_send_for_approval)

    }

}