package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.GroupPrincipalDemandAdapter
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.DtMtgGrpLoanScheduleViewmodel
import kotlinx.android.synthetic.main.activity_groupprincipal_demand.*
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.meeting_detail_item.*
import kotlinx.android.synthetic.main.meeting_detail_item.iv_stauscolor
import kotlinx.android.synthetic.main.meeting_detail_item.tv_code
import kotlinx.android.synthetic.main.meeting_detail_item.tv_count
import kotlinx.android.synthetic.main.meeting_detail_item.tv_date
import kotlinx.android.synthetic.main.meeting_detail_item.tv_mtgNum
import kotlinx.android.synthetic.main.meeting_detail_item.tv_nam
import kotlinx.android.synthetic.main.meeting_detail_item_zero.*
import kotlinx.android.synthetic.main.repay_toolbar.*

class GroupPrincipalDemandActivity : AppCompatActivity() {
    var dtMtgGrpLoanScheduleViewmodel: DtMtgGrpLoanScheduleViewmodel? = null
    var validate: Validate? = null
    var totalloanamt = 0
    var Todayvalue=0
    var Cumvalue=0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_groupprincipal_demand)
        validate = Validate(this)
        dtMtgGrpLoanScheduleViewmodel =
            ViewModelProviders.of(this).get(DtMtgGrpLoanScheduleViewmodel::class.java)

        tv_title.text = LabelSet.getText(
            "loanschedule",
            R.string.loanschedule
        )
        tv_code.text = validate!!.RetriveSharepreferenceLong(MeetingSP.Shgcode).toString()
        tv_nam.text = validate!!.RetriveSharepreferenceString(MeetingSP.ShgName)
        tv_date.text =
            validate!!.convertDatetime(validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate))
        tv_count.text =  validate!!.RetriveSharepreferenceInt(MeetingSP.MemberCount).toString()
        tv_name.text = validate!!.RetriveSharepreferenceString(MeetingSP.Institution)
        tv_memcode.text = validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid).toString()
        tv_mtgNum.text =
            validate!!.returnStringValue(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber).toString())

        ic_Back.setOnClickListener {
            if(validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) == 11 || validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) == 12){
                var intent = Intent(this, CutOffGroupLoanlist::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }else {
                var intent = Intent(this, GroupLoanlist::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        btn_save.isEnabled =
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber) >= validate!!.RetriveSharepreferenceInt(MeetingSP.maxmeetingnumber)
        btn_cancel.setOnClickListener {
            if(validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) == 11 || validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) == 12){
                var intent = Intent(this, CutOffGroupLoanlist::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }else {
                var intent = Intent(this, GroupLoanlist::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }
        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                updateschedule()
            }
        }



        setLabelText()
        fillRecyclerView()
        updateShgType()
    }

    override fun onBackPressed() {
        if(validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) == 11 || validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) == 12){
            var intent = Intent(this, CutOffGroupLoanlist::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }else {
            var intent = Intent(this, GroupLoanlist::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
    }

    fun setLabelText() {

        tv_installment_no.text = LabelSet.getText(
            "installment_no",
            R.string.installment_no
        )
        tv_installment_date.text = LabelSet.getText(
            "installment_date",
            R.string.installment_date
        )
        tv_principal_demand.text = LabelSet.getText(
            "principal_demand",
            R.string.principal_demand
        )
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )

    }

    private fun checkValidation(): Int {

        var value = 1
        if (!getTotalValue()) {
            validate!!.CustomAlert(
                LabelSet.getText(
                    "pleaseenterloanamtsame",
                    R.string.pleaseenterloanamtsame
                ), this

            )
            value = 0
            return value
        }
        return value
    }

    fun getTotalValue(): Boolean {
        var iValue = 0
        var iValueCum = 0
        val iCount = rvMemberList.childCount


        for (i in 0 until iCount) {
            val gridChild =
                rvMemberList.getChildAt(i) as? ViewGroup

            val et_principal_demand = gridChild!!
                .findViewById<View>(R.id.et_principal_demand) as? EditText
            val tv_principal_demand = gridChild
                .findViewById<View>(R.id.tv_principal_demand) as? TextView

            if (!et_principal_demand!!.text.toString().isNullOrEmpty()) {
                iValue = iValue + validate!!.returnIntegerValue(et_principal_demand.text.toString())
            }
            if (!tv_principal_demand!!.text.toString().isNullOrEmpty()) {
                iValueCum =
                    iValueCum + validate!!.returnIntegerValue(tv_principal_demand.text.toString())
                totalloanamt = iValueCum
            }


        }
        tv_TotalTodayValue.text = iValue.toString()
        tv_loanosvalue.text = iValueCum.toString()
        totalloanamt = iValueCum
        return iValue == iValueCum
        /* tv_TotalTodayValue.setText(iValue.toString())
         tv_TotalCumValue.setText(iValueCum.toString())
         refreshnotsaved()

 */
    }
    fun getTotalValue(iValue: Int,flag:Int) {
        if(flag==1) {
            Todayvalue = Todayvalue + iValue
            tv_TotalTodayValue.text = Todayvalue.toString()
        }else if(flag==2){
            Cumvalue = Cumvalue + iValue
            tv_loanosvalue.text = Cumvalue.toString()
        }
    }

    private fun fillRecyclerView() {
        Todayvalue=0
        Cumvalue=0
        var list = dtMtgGrpLoanScheduleViewmodel!!.getGroupScheduledata(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
            validate!!.RetriveSharepreferenceInt(MeetingSP.Loanno)
        )
        rvMemberList.layoutManager = LinearLayoutManager(this)
        val principalDemandAdapter =
            GroupPrincipalDemandAdapter(this, list)
        rvMemberList.layoutManager = LinearLayoutManager(this)

        val isize: Int
        isize = list!!.size
        val params: ViewGroup.LayoutParams = rvMemberList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvMemberList.layoutParams = params
        rvMemberList.adapter = principalDemandAdapter
    }

    fun updateschedule() {
        var saveValue = 0
        var totalValue = 0

        val iCount = rvMemberList.childCount


        for (i in 0 until iCount) {
            val gridChild =
                rvMemberList.getChildAt(i) as? ViewGroup

            val et_principal_demand = gridChild!!
                .findViewById<View>(R.id.et_principal_demand) as? EditText

            val tv_installment_no_value = gridChild
                .findViewById<View>(R.id.tv_installment_no_value) as? TextView


            var installmentno =
                validate!!.returnIntegerValue(tv_installment_no_value!!.text.toString())
            var prinicpaldemand =
                validate!!.returnIntegerValue(et_principal_demand!!.text.toString())
            var loandemandod = prinicpaldemand
            totalValue = totalValue + prinicpaldemand
            var loanos = totalloanamt - totalValue
            saveValue = saveData(prinicpaldemand, loandemandod, loanos, installmentno)


        }

        if (saveValue > 0) {
            if(validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) == 11 || validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) == 12){
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "data_saved_successfully",
                        R.string.data_saved_successfully
                    ),
                    this, CutOffGroupLoanlist::class.java
                )
            }else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "data_saved_successfully",
                        R.string.data_saved_successfully
                    ),
                    this, GroupLoanlist::class.java
                )
            }

        }
    }

    fun saveData(principaldemand: Int, loandemandos: Int, loanos: Int, installmentno: Int): Int {

        dtMtgGrpLoanScheduleViewmodel!!.updateLoanGroupSchedule(
            validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.Loanno),
            installmentno, principaldemand, loandemandos, loanos
        )

        return 1
    }

    fun updateShgType() {
        tv_count.setBackgroundResource(R.drawable.item_countactive)
        iv_stauscolor.setBackgroundResource(R.drawable.item_countactive)
        if (validate!!.RetriveSharepreferenceInt(MeetingSP.SHGType) == 1) {
            iv_stauscolor.setImageResource(R.drawable.ic_womangroup)
            iv_stauscolor.visibility = View.INVISIBLE
        } else {
            iv_stauscolor.visibility = View.VISIBLE
            if (validate!!.RetriveSharepreferenceInt(MeetingSP.Tag) == 1) {
                iv_stauscolor.setImageResource(R.drawable.ic_pvtg)
            } else if (validate!!.RetriveSharepreferenceInt(MeetingSP.Tag) == 2) {
                iv_stauscolor.setImageResource(R.drawable.ic_elderly)
            } else if (validate!!.RetriveSharepreferenceInt(MeetingSP.Tag) == 3) {
                iv_stauscolor.setImageResource(R.drawable.ic_disable)
            } else if (validate!!.RetriveSharepreferenceInt(MeetingSP.Tag) == 99) {
                iv_stauscolor.setImageResource(R.drawable.ic_other)
            }
        }}

}