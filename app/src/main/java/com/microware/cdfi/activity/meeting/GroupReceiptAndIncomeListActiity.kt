package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.GroupReceiptIncomeAdapter
import com.microware.cdfi.entity.ShgFinancialTxnDetailEntity
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.IncomeandExpenditureViewmodel
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.MstCOAViewmodel
import kotlinx.android.synthetic.main.activity_group_receipt_and_income_list_actiity.*
import kotlinx.android.synthetic.main.buttons.*

class GroupReceiptAndIncomeListActiity : AppCompatActivity() {
    var validate: Validate? = null
    var incomeandExpenditureViewmodel: IncomeandExpenditureViewmodel? = null
    var mstCoaViewmodel: MstCOAViewmodel? = null
    var lookupViewmodel: LookupViewmodel? = null
    var Todayvalue = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group_receipt_and_income_list_actiity)

        validate = Validate(this)
        incomeandExpenditureViewmodel =
            ViewModelProviders.of(this).get(IncomeandExpenditureViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        mstCoaViewmodel = ViewModelProviders.of(this).get(MstCOAViewmodel::class.java)

        ivAdd.isEnabled =
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber) >= validate!!.RetriveSharepreferenceInt(MeetingSP.maxmeetingnumber)

        ivAdd.setOnClickListener {
            validate!!.SaveSharepreferenceInt(MeetingSP.Auid, 0)
            val intent = Intent(this, GroupReceiptAndIncomeDetailActiity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        replaceFragmenty(
            fragment = MeetingTopBarFragment(13),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        setLabelText()
        fillRecyclerview()
    }

    private fun fillRecyclerview() {
        Todayvalue = 0
        incomeandExpenditureViewmodel!!.getIncomeAndExpenditureAlldata(
            validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            1
        ).observe(this, object : Observer<List<ShgFinancialTxnDetailEntity>?> {
            override fun onChanged(income_list: List<ShgFinancialTxnDetailEntity>?) {
                if (!income_list.isNullOrEmpty()) {
                    rvList.layoutManager =
                        LinearLayoutManager(this@GroupReceiptAndIncomeListActiity)
                    val groupReceiptIncomeAdapter = GroupReceiptIncomeAdapter(
                        this@GroupReceiptAndIncomeListActiity,
                        income_list
                    )
                    val isize: Int
                    isize = income_list.size
                    val params: ViewGroup.LayoutParams = rvList.layoutParams
                    val r = resources
                    val px = TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP,
                        resources.getDimension(R.dimen.spraying),
                        r.displayMetrics
                    )
                    val hi = Math.round(px)
                    val gridHeight = hi * isize
                    params.height = gridHeight
                    rvList.layoutParams = params
                    rvList.adapter = groupReceiptIncomeAdapter

                }

            }
        })
    }

    fun getRecepitType(keyCode: Int?): String? {
        var name: String? = null
        name = mstCoaViewmodel!!.getcoaValue(
            "OI",
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),
            keyCode
        )
        return name
    }

    fun getValue(keyCode: Int?,flag:Int): String? {
        var name: String? = null
        name = lookupViewmodel!!.getlookupValue(
            flag,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),
            keyCode
        )
        return name
    }

    private fun setLabelText() {
        tv_particulars.text = LabelSet.getText(
            "particulars",
            R.string.particulars
        )
        tv_received_from.text = LabelSet.getText(
            "received_from",
            R.string.received_from
        )
        tv_amount.text = LabelSet.getText(
            "amount",
            R.string.amount
        )
        tvtotal.text = LabelSet.getText("total", R.string.total)

    }

    fun getTotalValue() {
        var iValue1 = 0
        val iCount = rvList.childCount
        for (i in 0 until iCount) {
            val gridChild = rvList.getChildAt(i) as? ViewGroup
            val tv_value1 = gridChild!!.findViewById<View>(R.id.tv_amount) as? TextView

            if (!tv_value1!!.text.toString().isNullOrEmpty()) {
                iValue1 = iValue1 + validate!!.returnIntegerValue(tv_value1.text.toString())
            }
        }

        tv_TotalTodayValue.text = iValue1.toString()

    }

    fun getTotalValue1(iValue: Int) {
        Todayvalue = Todayvalue + iValue
        tv_TotalTodayValue.text = Todayvalue.toString()
    }

    override fun onBackPressed() {
        if(validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType)==11 || validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType)==12){
            val intent = Intent(this, CutOffMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        }else {
            val intent = Intent(this, MeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        }
    }

}