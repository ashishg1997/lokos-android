package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.GroupRepaymentDetailAdapter
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_group_loanlist.*
import kotlinx.android.synthetic.main.buttons.*

class GroupRepaymentDetailActivity : AppCompatActivity() {
    var validate: Validate? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    lateinit var dtLoanGpTxnViewmodel: DtLoanGpTxnViewmodel
    lateinit var dtLoanGpViewmodel: DtLoanGpViewmodel
    lateinit var dtMtgGrpLoanScheduleViewmodel: DtMtgGrpLoanScheduleViewmodel
    lateinit var lookupViewmodel: LookupViewmodel
    var today = 0
    var paid = 0
    var nextpayable = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group_loanlist)

        validate = Validate(this)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        dtLoanGpTxnViewmodel =
            ViewModelProviders.of(this).get(DtLoanGpTxnViewmodel::class.java)
        lookupViewmodel =
            ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        dtLoanGpViewmodel = ViewModelProviders.of(this).get(DtLoanGpViewmodel::class.java)
        dtMtgGrpLoanScheduleViewmodel = ViewModelProviders.of(this).get(DtMtgGrpLoanScheduleViewmodel::class.java)
        btn_cancel.setOnClickListener {
            var intent = Intent(this, MeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        replaceFragmenty(
            fragment = MeetingTopBarFragment(16),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        setLabelText()
        fillRecyclerView()
    }

    override fun onBackPressed() {
        var intent = Intent(this, MeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

    fun setLabelText() {
        tv_loan_source.text = LabelSet.getText(
            "loan_source",
            R.string.loan_source
        )
        tv_today_s_demand.text = LabelSet.getText(
            "todays_payable",
            R.string.todays_payable
        )
        tv_paid.text = LabelSet.getText("paid", R.string.paid)
        tv_next_demand.text = LabelSet.getText(
            "next_demand",
            R.string.next_demand
        )
        /* tv_code.setText(LabelSet.getText("_16649",R.string._16649))
         tv_nam.setText(LabelSet.getText("adarsh_mahila_sangam",R.string.adarsh_mahila_sangam))
         tv_date.setText(LabelSet.getText("_12_oct_2020",R.string._12_oct_2020))
         tv_title.setText(LabelSet.getText("loan_repayment",R.string.loan_repayment))*/
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }

    private fun fillRecyclerView() {
        today = 0
        paid = 0
        nextpayable = 0
        var list = dtLoanGpTxnViewmodel.getListDataByMtgnum(
            validate!!.RetriveSharepreferenceInt(
                MeetingSP.currentmeetingnumber
            ),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        var repaymentDetailAdapter = GroupRepaymentDetailAdapter(this, list)

        rvList.layoutManager = LinearLayoutManager(this)

        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = rvList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvList.layoutParams = params
        rvList.adapter = repaymentDetailAdapter
    }

    fun returnloanlist(flag: Int, loanno: Int): String {
        var loandata = dtLoanGpViewmodel.getLoandata(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            loanno
        )
        if (!loandata.isNullOrEmpty()) {
            var listdata = lookupViewmodel.getlookup(
                flag,
                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
            )
            return validate!!.returnlookupcodevalue(loandata.get(0).institution, listdata)
        }
        return ""
    }

    fun getTotalValue(iValue: Int, flag: Int) {
        if (flag == 1) {
            today = today + iValue
            tv_value1.text = today.toString()
        } else if (flag == 2) {
            paid = paid + iValue
            tv_value2.text = paid.toString()
        } else if (flag == 3) {
            nextpayable = nextpayable + iValue
            tv_value3.text = nextpayable.toString()
        }
    }

/*fun returnlist(memberid: Long): List<LoanRepaymentListModel>? {
    return dtLoanTxnMemViewmodel!!.getmemberloan(
        memberid,
        validate!!.RetriveSharepreferenceInt(
            MeetingSP.currentmeetingnumber
        )
    )
}*/

    fun getnextdemand(memid: Long, loanno: Int): Int {
        return dtMtgGrpLoanScheduleViewmodel.getnextdemand(
            memid, loanno
        )
    }
}