package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.api.meetingmodel.FundTypeModel
import com.microware.cdfi.api.meetingmodel.mcpDataListModel
import com.microware.cdfi.entity.*
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.replaceFragmenty
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_loan_demand.*
import kotlinx.android.synthetic.main.activity_loan_demand.et_amount
import kotlinx.android.synthetic.main.activity_loan_demand.tv_amount
import kotlinx.android.synthetic.main.buttons.*

class LoanDemandActivity : AppCompatActivity() {
    var lookupViewmodel: LookupViewmodel? = null
    var dtLoanViewmodel: DtLoanViewmodel? = null
    var validate: Validate? = null
    var dataspin_entry_member_name: List<LookupEntity>? = null
    var dataspin_mcp_id: List<mcpDataListModel>? = null
    var dataspin_purpose: List<LookupEntity>? = null
    var dataspin_priority: List<LookupEntity>? = null
    var dataspin_source: List<LookupEntity>? = null

    var memberlist: List<DtmtgDetEntity>? = null
    var dtLoanApplicationEntity: DtLoanApplicationEntity? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    lateinit var loanProductViewmodel: LoanProductViewModel
    var dataspin_fund: List<FundTypeModel>? = null
    lateinit var shgMcpViewmodel: ShgMcpViewmodel

    var linked_mcp_id:Long?=0

    var nullStringValue:String?=null
    var nullLongValue:Long?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loan_demand)
        validate = Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        dtLoanViewmodel = ViewModelProviders.of(this).get(DtLoanViewmodel::class.java)
        loanProductViewmodel = ViewModelProviders.of(this).get(LoanProductViewModel::class.java)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        shgMcpViewmodel = ViewModelProviders.of(this).get(ShgMcpViewmodel::class.java)

        replaceFragmenty(
            fragment = MeetingTopBarFragment(7),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        et_priority_valid_upto.setOnClickListener {
            validate!!.datePickerwithmindate1(validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),et_priority_valid_upto)
        }

        btn_save.isEnabled =
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber) >= validate!!.RetriveSharepreferenceInt(MeetingSP.maxmeetingnumber)
        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                saveData()
            }
        }

        btn_cancel.setOnClickListener {
            var intent = Intent(this, LoanDemandSectionActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        et_sanction_amount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = validate!!.returnIntegerValue(et_amount.text.toString())
                if (validate!!.returnIntegerValue(s.toString()) > value) {
                    validate!!.CustomAlertEditText(
                        LabelSet.getText(
                            "sanctionamountcannotbegfraeterthanpraposedamount",
                            R.string.sanctionamountcannotbegfraeterthanpraposedamount
                        ),
                        this@LoanDemandActivity,
                        et_sanction_amount
                    )
                }

            }

        })

        setLabelText()
        fillSpinner()
        fillmemberspinner()
        fillproduct()
        fillloanapplicationdata()

    }

    override fun onBackPressed() {
        var intent = Intent(this, LoanDemandSectionActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)

    }


    fun setLabelText() {
        //    tv_title.setText(LabelSet.getText("loan_demand", R.string.loan_demand))
        tv_entry_member_name.text = LabelSet.getText(
            "name_of_member",
            R.string.name_of_member
        )
        tv_purpose.text = LabelSet.getText(
            "purpose",
            R.string.purpose
        )
        tv_amount.text = LabelSet.getText(
            "proposed_amount",
            R.string.proposed_amount
        )
        tv_priority_valid_upto.text = LabelSet.getText(
            "request_valid_upto", R.string.request_valid_upto
        )
        tv_priority.text = LabelSet.getText(
            "priority",
            R.string.priority
        )
        et_amount.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_source.text = LabelSet.getText(
            "fund_type",
            R.string.fund_type
        )
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }

    private fun fillSpinner() {
        dataspin_entry_member_name = lookupViewmodel!!.getlookup(
            999,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_purpose = lookupViewmodel!!.getlookup(
            67,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_priority = lookupViewmodel!!.getlookup(
            50,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

    /*    dataspin_source = lookupViewmodel!!.getlookup(
            64,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
*/
        validate!!.fillspinner(this, spin_purpose, dataspin_purpose)
    //    validate!!.fillspinner(this, spin_source, dataspin_source)
    }

    private fun fillmemberspinner() {
        memberlist = generateMeetingViewmodel.getListDataMtgByMtgnum(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        val adapter: ArrayAdapter<String?>
        if (!memberlist.isNullOrEmpty()) {
            val isize = memberlist!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in memberlist!!.indices) {
                sValue[i + 1] = memberlist!![i].member_name
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_entry_member_name.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(memberlist!!.size + 1)
            sValue[0] = LabelSet.getText(
                "nomemberavailable",
                R.string.nomemberavailable
            )
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_entry_member_name.adapter = adapter
        }
        spin_entry_member_name.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                if(position>0){
                    var selected_mem_id = returnmemid()
                    if(validate!!.RetriveSharepreferenceLong(MeetingSP.Loanappid)==0L){
                        dataspin_mcp_id = dtLoanViewmodel!!.getMCP_Linkdata(
                            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), selected_mem_id,0)
                    }else {
                        dataspin_mcp_id = dtLoanViewmodel!!.getMCP_Linkdata(
                            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), selected_mem_id,linked_mcp_id)
                    }
                    var mcp_pos = validate!!.returnMCPpos(linked_mcp_id,dataspin_mcp_id)
                    validate!!.fillMCPspinner(this@LoanDemandActivity,spin_mcp_link,dataspin_mcp_id,mcp_pos)

                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        spin_mcp_link.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                if(position>0){
                    var mcpId = validate!!.returnMCPid(spin_mcp_link,dataspin_mcp_id)
                    fillMCPdata(mcpId)

                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }
    }

    private fun fillMCPdata(mcpId: Long) {
        val listData = shgMcpViewmodel.getShgMcpDataAlllist(
            mcpId,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            returnmemid())

        if(!listData.isNullOrEmpty()){
            spin_purpose.setSelection(validate!!.returnlookupcodepos(validate!!.returnIntegerValue(
                listData.get(0).loan_purpose.toString()), dataspin_purpose))
            et_priority_valid_upto.setText(validate!!.convertDatetime(listData[0].tentative_date))
            et_amount.setText(validate!!.returnStringValue(listData[0].amt_demand.toString()))
            spin_purpose.isEnabled = false
            et_priority_valid_upto.isEnabled = false
            et_amount.isEnabled = false
        }else {
            spin_purpose.isEnabled = true
            et_priority_valid_upto.isEnabled = true
            et_amount.isEnabled = true
        }

    }

    fun returnmemaid(): Long {

        var pos = spin_entry_member_name.selectedItemPosition
        var id = 0L

        if (!memberlist.isNullOrEmpty()) {
            if (pos > 0) id = memberlist!!.get(pos - 1).mem_id
        }
        return id
    }

    fun returnmemid(): Long {

        var pos = spin_entry_member_name.selectedItemPosition
        var id = 0L

        if (!memberlist.isNullOrEmpty()) {
            if (pos > 0) id = memberlist!!.get(pos - 1).mem_id
        }
        return id
    }

    fun setMember(): Int {
        var pos = 0
        if (!memberlist.isNullOrEmpty()) {
            for (i in memberlist!!.indices) {
                if (validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid) == memberlist!!.get(i).mem_id)
                    pos = i + 1
            }
        }
        spin_entry_member_name.setSelection(pos)
        spin_entry_member_name.isEnabled = false
        return pos
    }

    private fun checkValidation(): Int {
        var priorityCount = dtLoanViewmodel!!.getPriorityCount(
            validate!!.returnIntegerValue(et_priority.text.toString()),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        var value = 1
        if (spin_entry_member_name.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_entry_member_name,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "member",
                    R.string.member
                )

            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_amount.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "proposed_amount",
                    R.string.proposed_amount
                ), this,
                et_amount
            )
            value = 0
            return value
        }

        if (spin_purpose.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_purpose,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "purpose",
                    R.string.purpose
                )

            )
            value = 0
            return value
        }
        if (validate!!.returnIntegerValue(et_sanction_amount.text.toString()) > validate!!.returnIntegerValue(
                et_amount.text.toString()
            )
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "sanctionamountcannotbegfraeterthanpraposedamount",
                    R.string.sanctionamountcannotbegfraeterthanpraposedamount
                ),
                this@LoanDemandActivity,
                et_sanction_amount
            )
            return 0
        }
        if (spin_source.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_source,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "fund_type",
                    R.string.fund_type
                )

            )
            value = 0
            return value
        }
        if (validate!!.returnIntegerValue(et_priority.text.toString())==0)
        {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "priority",
                    R.string.priority
                ), this,
                et_priority
            )
            value = 0
            return value
        }
        if (validate!!.RetriveSharepreferenceLong(MeetingSP.Loanappid)==0L && priorityCount>0 ||
            validate!!.RetriveSharepreferenceLong(MeetingSP.Loanappid)>0 && priorityCount>1)
        {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "valid_priority_num",
                    R.string.valid_priority_num
                ), this,
                et_priority
            )
            value = 0
            return value
        }
        if (validate!!.returnStringValue(et_priority_valid_upto.text.toString()).length==0)
        {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "request_valid_upto",
                    R.string.request_valid_upto
                ), this,
                et_priority_valid_upto
            )
            value = 0
            return value
        }
        return value
    }

    fun fillproduct(
    ) {
        dataspin_fund =
            loanProductViewmodel.getFundTypeBySource_Receipt(2,1)

        val adapter: ArrayAdapter<String?>
        if (!dataspin_fund.isNullOrEmpty()) {
            val isize = dataspin_fund!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in dataspin_fund!!.indices) {
                sValue[i + 1] =
                    dataspin_fund!![i].fundType.toString()
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_source.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_source.adapter = adapter
        }

    }

    fun returnid(): Int {

        var pos = spin_source.selectedItemPosition
        var id = 0

        if (!dataspin_fund.isNullOrEmpty()) {
            if (pos > 0) id = dataspin_fund!!.get(pos - 1).fundTypeId
        }
        return id
    }

    fun setproduct(id: Int): Int {

        var pos = 0

        if (!dataspin_fund.isNullOrEmpty()) {
            for (i in dataspin_fund!!.indices) {
                if (dataspin_fund!!.get(i).fundTypeId == id)
                    pos = i + 1
            }
        }
        return pos
    }


    private fun fillloanapplicationdata() {
        var list = dtLoanViewmodel!!.getmemberLoanApplication(
            validate!!.RetriveSharepreferenceLong(MeetingSP.Loanappid)
        )
        if (!list.isNullOrEmpty()) {
            linked_mcp_id = validate!!.returnLongValue(list.get(0).mcp_id.toString())
            setMember()
            et_amount.setText(list.get(0).amt_demand.toString())
            et_sanction_amount.setText(list.get(0).amt_sanction.toString())
            et_priority.setText(list.get(0).loan_request_priority.toString())
            et_priority_valid_upto.setText(validate!!.convertDatetime(list.get(0).tentative_date))
            //  loanappid = list.get(0).loanapplication_id
            spin_source.setSelection(setproduct(
                    validate!!.returnIntegerValue(list.get(0).loan_product_id.toString())))
            spin_purpose.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(
                        list.get(0).loan_purpose.toString()
                    ), dataspin_purpose))

            if (validate!!.returnIntegerValue(list.get(0).amt_disbursed.toString()) > 0) {
                btn_savegray.visibility = View.VISIBLE
                btn_save.visibility = View.GONE
            } else {

                btn_savegray.visibility = View.GONE
                btn_save.visibility = View.VISIBLE
            }
        }else {
            et_priority_valid_upto.setText(validate!!.convertDatetime(validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate)))
        }
    }

    private fun saveData() {
        var defaultMtgNum:Int?=null
        var defaultMtgGuid:String?=null
        var linked_mcp_id:Long?=null
        if(validate!!.returnMCPid(spin_mcp_link,dataspin_mcp_id)==0L){
            linked_mcp_id = null
        }else {
            linked_mcp_id = validate!!.returnMCPid(spin_mcp_link,dataspin_mcp_id)
        }
        if (validate!!.RetriveSharepreferenceLong(MeetingSP.Loanappid) > 0) {
            dtLoanViewmodel!!.updateMemberloanapplicationdetail(
                validate!!.RetriveSharepreferenceLong(MeetingSP.Loanappid),
                validate!!.returnIntegerValue(et_amount.text.toString()),
                validate!!.returnIntegerValue(et_sanction_amount.text.toString()),
                validate!!.returnlookupcode(spin_purpose, dataspin_purpose),
                returnid(),
                validate!!.returnIntegerValue(et_priority.text.toString()),
                validate!!.Daybetweentime(et_priority_valid_upto.text.toString()),
                returnid(),linked_mcp_id, validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime)

            )
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this, LoanDemandSectionActivity::class.java
            )
        } else {
            var countloanno =
                dtLoanViewmodel!!.getmaxloanno(validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
            var laonno =
                (validate!!.RetriveSharepreferenceLong(MeetingSP.Shgcode) * 1000) + countloanno + 1
            dtLoanApplicationEntity = DtLoanApplicationEntity(
                0,
                laonno,
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                returnmemid(),
                validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
                0,
                validate!!.returnIntegerValue(et_amount.text.toString()),
                validate!!.returnIntegerValue(et_sanction_amount.text.toString()),
                0,
                0,
                validate!!.Daybetweentime(et_priority_valid_upto.text.toString()),
                returnid(),
                validate!!.returnIntegerValue(et_priority.text.toString()),
                returnid(),
                validate!!.returnlookupcode(spin_purpose, dataspin_purpose),
                0,
                validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                defaultMtgNum,
                defaultMtgGuid,
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                nullStringValue,
                nullLongValue,
                nullStringValue,
                nullLongValue,linked_mcp_id
            )

            dtLoanViewmodel!!.insert(dtLoanApplicationEntity!!)
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this, LoanDemandSectionActivity::class.java
            )
        }

    }
}