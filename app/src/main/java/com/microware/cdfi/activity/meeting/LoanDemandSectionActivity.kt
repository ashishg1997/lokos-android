package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.LoanDemandSectionAdapter
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.DtLoanViewmodel
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.utility.replaceFragmenty
import kotlinx.android.synthetic.main.demand_sanction_list.*

class LoanDemandSectionActivity : AppCompatActivity() {
    var validate: Validate? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
     var lookupViewmodel: LookupViewmodel?=null
    var dtLoanViewmodel: DtLoanViewmodel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.demand_sanction_list)

        validate = Validate(this)
        dtLoanViewmodel = ViewModelProviders.of(this).get(DtLoanViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)

        replaceFragmenty(
            fragment = MeetingTopBarFragment(17),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )
        img_Add.isEnabled =
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber) >= validate!!.RetriveSharepreferenceInt(MeetingSP.maxmeetingnumber)
        img_Add.setOnClickListener {
            validate!!.SaveSharepreferenceLong(MeetingSP.Loanappid,0)
            validate!!.SaveSharepreferenceLong(MeetingSP.Memberid,0)
            var intent = Intent(this, LoanDemandActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        img_AddMcp.setOnClickListener {

            var intent = Intent(this, MCPPreparationListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }


        setLabelText()
        fillRecyclerView()

    }
    fun getlookupValue(keyCode: Int?,flag:Int): String? {
        var name: String? = null
        name = lookupViewmodel!!.getlookupValue(
            flag,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),
            keyCode
        )
        return name
    }
    fun returnpost(memid:Long):Int{
        return generateMeetingViewmodel.reurnmemberpost(memid)

    }
    override fun onBackPressed() {
        var intent = Intent(this, MeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }
     fun returnvalue(id:Int,flag:Int):String {
       var  listdata = lookupViewmodel!!.getlookup(
           flag,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        return validate!!.returnlookupcodevalue(id,listdata)
    }
    fun setLabelText() {
        tv_member_name.text = LabelSet.getText("name_of_member",R.string.name_of_member)
        tv_loan_purpose.text = LabelSet.getText("loan_purpose",R.string.loan_purpose)
        tv_loan_amount.text = LabelSet.getText("loan_amount",R.string.loan_amount)
        tv_request_date.text = LabelSet.getText("request_date",R.string.request_date)
        tv_request_valid_upto.text = LabelSet.getText("request_valid_upto",R.string.request_valid_upto)
        tv_priority_no.text = LabelSet.getText("priority_no",R.string.priority_no)

    }

    fun setMember(member_id: Long): String {
        var pos = ""
        var memberlist = generateMeetingViewmodel.getListDataMtgByMtgnum(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
           validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        if (!memberlist.isNullOrEmpty()) {
            for (i in memberlist.indices) {
                if (member_id == memberlist.get(i).mem_id)
                    pos = memberlist.get(i).member_name!!
            }
        }

        return pos
    }

    private fun fillRecyclerView() {
        var list = dtLoanViewmodel!!.getLoanApplicationlist(
            validate!!.RetriveSharepreferenceInt(
                MeetingSP.currentmeetingnumber
            ),
           validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        var totdemand = dtLoanViewmodel!!.gettotaldemand(
            validate!!.RetriveSharepreferenceInt(
                MeetingSP.currentmeetingnumber
            ),
           validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        tv_amount.text=totdemand.toString()
        if (!list.isNullOrEmpty()) {
            rvList.layoutManager = LinearLayoutManager(this)
            var loanDemandSectionAdapter =
                LoanDemandSectionAdapter(this, list)
            val isize: Int
            isize = list.size
            val params: ViewGroup.LayoutParams = rvList.layoutParams
            val r = resources
            val px = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, resources.getDimension(R.dimen.spraying),
                r.displayMetrics
            )
            val hi = Math.round(px)
            var gridHeight = hi * isize
            params.height = gridHeight
            rvList.layoutParams = params
            rvList.adapter = loanDemandSectionAdapter
        }
    }

}