package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.LoanDemandSummaryAdapter
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.DtLoanMemberScheduleViewmodel
import com.microware.cdfi.viewModel.DtLoanMemberViewmodel
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import com.microware.cdfi.viewModel.LookupViewmodel
import kotlinx.android.synthetic.main.activity_loan_demand_summary.*
import kotlinx.android.synthetic.main.buttons.btn_cancel

class LoanDemandSummaryActivity : AppCompatActivity() {
    var validate: Validate? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    var dtLoanMemberViewmodel: DtLoanMemberViewmodel? = null
    var dtLoanMemberScheduleViewmodel: DtLoanMemberScheduleViewmodel? = null
    var lookupViewmodel: LookupViewmodel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loan_demand_summary)

        validate = Validate(this)
        dtLoanMemberScheduleViewmodel =
            ViewModelProviders.of(this).get(DtLoanMemberScheduleViewmodel::class.java)
        dtLoanMemberViewmodel =
            ViewModelProviders.of(this).get(DtLoanMemberViewmodel::class.java)

        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        lookupViewmodel =
            ViewModelProviders.of(this).get(LookupViewmodel::class.java)

/*        btn_cancel.setOnClickListener {
            var intent = Intent(this, MeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }*/


        replaceFragmenty(
            fragment = MeetingTopBarFragment(18),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        if(validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) ==11 || validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) ==12){
            tv_laon_outstanding.visibility = View.GONE
            tv_total_demand.visibility = View.GONE
        }else {
            tv_laon_outstanding.visibility = View.VISIBLE
            tv_total_demand.visibility = View.VISIBLE
        }

        setLabelText()
        fillRecyclerView()

        btn_reschedule.visibility = View.GONE
        btn_reschedule.setOnClickListener {
            validate!!.SaveSharepreferenceInt(MeetingSP.loanReschedule,1)
            fillRescheduleRecyclerView()
        }
    }


    override fun onBackPressed() {
        if(validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType)==0) {
            var intent = Intent(this, MeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        }else {
            var intent = Intent(this, CutOffMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        }
    }

    fun setLabelText() {
        //  tv_title.setText(LabelSet.getText("loan_demand_summary",R.string.loan_demand_summary))
        //  tv_cash_in_hand.setText(LabelSet.getText("cash_in_hand",R.string.cash_in_hand))
        tv_loans.text = LabelSet.getText("Loans", R.string.Loans)
        tv_member_name.text = LabelSet.getText(
            "member_name",
            R.string.member_name
        )
        tv_laon_outstanding.text = LabelSet.getText(
            "loan_outstanding",
            R.string.loan_outstanding
        ) + " " +LabelSet.getText(
            "rs",
            R.string.rs
        )
        tv_total_demand.text = LabelSet.getText(
            "total_demandnew",
            R.string.total_demandnew
        ) + " " +LabelSet.getText(
            "rs",
            R.string.rs
        )
        tv_total_disbursed.text = LabelSet.getText(
            "total_disbursednew",
            R.string.total_disbursednew
        ) + " " +LabelSet.getText(
            "rs",
            R.string.rs
        )
        tv_total.text = LabelSet.getText("total", R.string.total)
        btn_reschedule.text = LabelSet.getText("click_to_reschedule",R.string.click_to_reschedule)

    }

    private fun fillRecyclerView() {
        validate!!.SaveSharepreferenceInt(MeetingSP.loanReschedule,0)
        var list = generateMeetingViewmodel.getloanListDataMtgByMtgnum(
            validate!!.RetriveSharepreferenceInt(
                MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )

        rvList.layoutManager = LinearLayoutManager(this)
        var loanDemandSummaryAdapter =
            LoanDemandSummaryAdapter(this, list)
        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = rvList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvList.layoutParams = params
        rvList.adapter = loanDemandSummaryAdapter

    }

    private fun fillRescheduleRecyclerView() {
        var list = generateMeetingViewmodel.getRescheduleloanListMtgByMtgnum(
            validate!!.RetriveSharepreferenceInt(
                MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )

        rvList.layoutManager = LinearLayoutManager(this)
        var loanDemandSummaryAdapter =
            LoanDemandSummaryAdapter(this, list)
        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = rvList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvList.layoutParams = params
        rvList.adapter = loanDemandSummaryAdapter

    }
    fun getlookupValue(keyCode: Int?,flag:Int): String? {
        var name: String? = null
        name = lookupViewmodel!!.getlookupValue(
            flag,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),
            keyCode
        )
        return name
    }
    fun returnpost(memid:Long):Int{
        return generateMeetingViewmodel.reurnmemberpost(memid)

    }
    fun getTotalValue() {
        var iValue1 = 0
        var iValue2 = 0
        var iValue3 = 0
        val iCount = rvList.childCount


        for (i in 0 until iCount) {
            val gridChild =
                rvList.getChildAt(i) as? ViewGroup

            val tv_value1 = gridChild!!
                .findViewById<View>(R.id.tv_value1) as? TextView

            val tv_value2 = gridChild
                .findViewById<View>(R.id.tv_value2) as? TextView
            val tv_value3 = gridChild
                .findViewById<View>(R.id.tv_value3) as? TextView

            if (!tv_value1!!.text.toString().isNullOrEmpty()) {
                iValue1 = iValue1 + validate!!.returnIntegerValue(tv_value1.text.toString())
            }

            if (!tv_value2!!.text.toString().isNullOrEmpty()) {
                iValue2 = iValue2 + validate!!.returnIntegerValue(tv_value2.text.toString())
            }
            if (!tv_value3!!.text.toString().isNullOrEmpty()) {
                iValue3 = iValue3 + validate!!.returnIntegerValue(tv_value3.text.toString())
            }


        }
        tv_today_demand_total.text = iValue1.toString()
        tv_paid_total.text = iValue2.toString()
        tv_next_demand_total.text = iValue3.toString()

    }


    fun gettotalloanamt(loanno: Int,memid: Long, mtgguid: String): Int {
        return dtLoanMemberScheduleViewmodel!!.gettotalloanamt(loanno,memid, mtgguid)
    }

    fun getLoanno(appid: Long): Int {
        return dtLoanMemberViewmodel!!.getLoanno(appid)
    }

}