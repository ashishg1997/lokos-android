package com.microware.cdfi.activity.meeting

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.LoanDisbursementAdapter
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.DtLoanMemberScheduleViewmodel
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import com.microware.cdfi.viewModel.Memberviewmodel
import com.microware.cdfi.utility.replaceFragmenty
import kotlinx.android.synthetic.main.activity_loan_disbursement1.*
import kotlinx.android.synthetic.main.activity_loan_disbursement1.rvList
import kotlinx.android.synthetic.main.buttons.btn_cancel
import kotlinx.android.synthetic.main.buttons.btn_save

class LoanDisbursement1Activity : AppCompatActivity() {
    var validate: Validate? = null
    lateinit var loanDisbursementAdapter: LoanDisbursementAdapter
    var memberviewmodel: Memberviewmodel? = null
    var generateMeetingViewmodel: GenerateMeetingViewmodel? = null
    var dtLoanMemberScheduleViewmodel: DtLoanMemberScheduleViewmodel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loan_disbursement1)

        validate = Validate(this)
        memberviewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        dtLoanMemberScheduleViewmodel =
            ViewModelProviders.of(this).get(DtLoanMemberScheduleViewmodel::class.java)


        setLabelText()
        //fillRecyclerView()
        fillData()
        replaceFragmenty(
            fragment = MeetingTopBarFragment(4),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )
//        getTotalValue()
    }

    override fun onBackPressed() {
        var intent = Intent(this, MeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

    fun setLabelText() {
        // tv_title.setText(LabelSet.getText("loan_disbursement",R.string.loan_disbursement))
        tv_sr_no.text = LabelSet.getText("sr_no", R.string.sr_no)
        tv_member_name.text = LabelSet.getText(
            "member_name",
            R.string.member_name
        )
        tv_amount1.text = LabelSet.getText(
            "amount",
            R.string.amount
        )
        tv_period.text = LabelSet.getText(
            "period",
            R.string.period
        )
        tv_interest_rate.text = LabelSet.getText(
            "interest_rate",
            R.string.interest_rate
        )
        tv_total.text = LabelSet.getText("total", R.string.total)
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }

    /*private fun fillRecyclerView() {
        loanDisbursementAdapter = LoanDisbursementAdapter(this)

        rvList.layoutManager = LinearLayoutManager(this)

        val isize: Int
        isize = 7
        val params: ViewGroup.LayoutParams = rvList.getLayoutParams()
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvList.setLayoutParams(params)
        rvList.setAdapter(loanDisbursementAdapter)
    }*/

    @SuppressLint("WrongViewCast")
    fun getTotalValue() {
        var iValue1 = 0
        var iValue2 = 0
        var iValue3 = 0
        var saveValue = 0
        var etCheckVAlue: EditText? = null

        val iCount = rvList.childCount


        for (i in 0 until iCount) {
            val gridChild =
                rvList.getChildAt(i) as? ViewGroup

            val tv_value1 = gridChild!!
                .findViewById<View>(R.id.tv_value1) as? TextView

            val tv_value2 = gridChild
                .findViewById<View>(R.id.tv_value2) as? TextView

            val tv_value3 = gridChild
                .findViewById<View>(R.id.tv_value3) as? TextView

            if (tv_value1!!.text.toString() != null && tv_value1.text.toString().length > 0) {
                iValue1 = iValue1 + validate!!.returnIntegerValue(tv_value1.text.toString())
            }
            if (tv_value2!!.text.toString() != null && tv_value2.text.toString().length > 0) {
                iValue2 = iValue2 + validate!!.returnIntegerValue(tv_value2.text.toString())
            }
            if (tv_value3!!.text.toString() != null && tv_value3.text.toString().length > 0) {
                iValue3 = iValue3 + validate!!.returnIntegerValue(tv_value3.text.toString())
            }


        }

        tv_amount_total.text = iValue1.toString()
        tv_period_total.text = iValue2.toString()
        tv_interest_rate_total.text = iValue3.toString()


    }

    fun callMethodWithPosition(pos: Int) {
        if (pos == 6) {
            getTotalValue()
        }
    }
 fun gettotalloanamt(memid: Long,mtgguid:String):Int {
        return dtLoanMemberScheduleViewmodel!!.gettotalloanamt(0,memid,mtgguid)
    }

    private fun fillData() {
        var memberlist = generateMeetingViewmodel!!.getListDataMtgByMtgnum(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))

        if (!memberlist.isNullOrEmpty()) {

            rvList.layoutManager = LinearLayoutManager(this@LoanDisbursement1Activity)
            rvList.adapter =
                LoanDisbursementAdapter(this@LoanDisbursement1Activity, memberlist)
        }

    }
}