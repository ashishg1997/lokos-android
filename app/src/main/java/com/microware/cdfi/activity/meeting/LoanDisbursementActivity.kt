package com.microware.cdfi.activity.meeting

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.api.meetingmodel.FundTypeModel
import com.microware.cdfi.entity.*
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_loan_disbursement.*
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*

class LoanDisbursementActivity : AppCompatActivity() {

    var lookupViewmodel: LookupViewmodel? = null
    var validate: Validate? = null
    var shgBankList: List<Cbo_bankEntity>? = null
    var dataspin_loan_type: List<LookupEntity>? = null
    var dataspin_loan_source: List<LookupEntity>? = null
    var dataspin_modeofpayment: List<LookupEntity>? = null
    var dtLoanMemberEntity: DtLoanMemberEntity? = null
    var dtLoanTxnMemEntity: DtLoanTxnMemEntity? = null
    var dtLoanMemberSheduleEntity: DtLoanMemberSheduleEntity? = null
    var dtLoanTxnMemViewmodel: DtLoanTxnMemViewmodel? = null
    var dtLoanMemberViewmodel: DtLoanMemberViewmodel? = null
    var dtLoanMemberScheduleViewmodel: DtLoanMemberScheduleViewmodel? = null
    var memberlist: List<DtmtgDetEntity>? = null
    var dataspin_frequency: List<LookupEntity>? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    var loanpurpose = 0
    var proposedAmount = 0
    var sanctionAmount = 0
    var dtLoanViewmodel: DtLoanViewmodel? = null
    lateinit var loanProductViewmodel: LoanProductViewModel
    var dataspin_fund: List<FundTypeModel>? = null

    var nullStringValue:String?=null
    var nullLongValue:Long?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loan_disbursement)

        validate = Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        dtLoanTxnMemViewmodel = ViewModelProviders.of(this).get(DtLoanTxnMemViewmodel::class.java)
        loanProductViewmodel = ViewModelProviders.of(this).get(LoanProductViewModel::class.java)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        dtLoanViewmodel = ViewModelProviders.of(this).get(DtLoanViewmodel::class.java)
        dtLoanMemberViewmodel = ViewModelProviders.of(this).get(DtLoanMemberViewmodel::class.java)
        dtLoanMemberScheduleViewmodel =
            ViewModelProviders.of(this).get(DtLoanMemberScheduleViewmodel::class.java)
        setLabelText()
        et_no_of_installment.filters = arrayOf(InputFilterMinMax (1, 60))

        if(validate!!.RetriveSharepreferenceInt(MeetingSP.loanReschedule)==1){
            btn_save.visibility = View.VISIBLE
            btn_savegray.visibility = View.GONE
            disableControls()
        }else if(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber) >= validate!!.RetriveSharepreferenceInt(MeetingSP.maxmeetingnumber)
        ){
            btn_save.visibility = View.VISIBLE
            btn_savegray.visibility = View.GONE

        }else{
            btn_savegray.visibility = View.VISIBLE
            btn_save.visibility = View.GONE
        }
        btn_cancel.setOnClickListener {
            var intent = Intent(this, LoanDemandSummaryActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        }
        btn_save.setOnClickListener {
            //var countloanno=dtLoanViewmodel!!.getmaxloanno(validate!!.RetriveSharepreferenceLong(MeetingSP.Shgcode))
            if (checkValidation() == 1) {
                saveData(validate!!.returnIntegerValue(et_moratorium_period.text.toString()))
                var loanAmount = 0
                var principalRepaid = 0
                if(validate!!.RetriveSharepreferenceInt(MeetingSP.loanReschedule)==1){
                    loanAmount = dtLoanMemberScheduleViewmodel!!.gettotaloutstanding(
                        validate!!.returnIntegerValue(et_loan_no.text.toString()),
                        validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid))
                }else {
                    loanAmount = validate!!.returnIntegerValue(et_amount.text.toString())

                }
                insertscheduler(
                    loanAmount,
                    validate!!.returnIntegerValue(et_no_of_installment.text.toString()),
                    validate!!.returnIntegerValue(et_moratorium_period.text.toString())
                )
            }
        }
        spin_member.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {
                    fillloandata()
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }
        et_no_of_installment.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = validate!!.returnIntegerValue(et_amount.text.toString())
                if (validate!!.returnIntegerValue(s.toString()) > 0 && value > 0) {
                    var installmentamt = value / validate!!.returnIntegerValue(s.toString())
                    et_amount_of_installment.setText(installmentamt.toString())
                }

            }

        })
        et_amount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = validate!!.returnIntegerValue(et_no_of_installment.text.toString())
                if (validate!!.returnIntegerValue(s.toString()) > 0 && value > 0) {
                    var installmentamt = value / validate!!.returnIntegerValue(s.toString())
                    et_amount_of_installment.setText(installmentamt.toString())
                }

            }

        })
        spin_mode_of_payment?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (validate!!.returnlookupcode(
                        spin_mode_of_payment,
                        dataspin_modeofpayment
                    ) == 2
                ) {
                    lay_bank.visibility = View.VISIBLE
                    lay_chequeNO.visibility = View.VISIBLE
                } else {
                    lay_bank.visibility = View.GONE
                    lay_chequeNO.visibility = View.GONE
                    spin_source_bank.setSelection(0)

                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }
        // insertMasterdata()
        fillSpinner()
        fillmemberspinner()
        fillbank()
        fillproduct()

        if (validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) == 11 || validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) == 12) {
            spin_loan_type.isEnabled = true
            lay_original_loan_amount.visibility = View.VISIBLE
            lay_principal_overdue.visibility = View.VISIBLE
            lay_interest_overdue.visibility = View.VISIBLE
            fillCutOffLoandata()
        } else {
            fillloandata()
            spin_loan_type.isEnabled = false
            lay_original_loan_amount.visibility = View.GONE
            lay_principal_overdue.visibility = View.GONE
            lay_interest_overdue.visibility = View.GONE
        }
        replaceFragmenty(
            fragment = MeetingTopBarFragment(4),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )
    }

    private fun disableControls() {
        spin_loan_type.isEnabled = false
        et_amount.isEnabled = false
        spin_loan_source.isEnabled = false
        spin_mode_of_payment.isEnabled = false
        spin_source_bank.isEnabled = false
        et_cheque_no_transactio_no.isEnabled = false
        et_amount_of_installment.isEnabled = false
        et_moratorium_period.isEnabled = false
        spin_frequency.isEnabled = false
    }

    private fun fillmemberspinner() {
        memberlist = generateMeetingViewmodel.getListDataMtgByMtgnum(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        val adapter: ArrayAdapter<String?>
        if (!memberlist.isNullOrEmpty()) {
            val isize = memberlist!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in memberlist!!.indices) {
                sValue[i + 1] = memberlist!![i].member_name
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_member.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(memberlist!!.size + 1)
            sValue[0] = LabelSet.getText(
                "nomemberavailable",
                R.string.nomemberavailable
            )
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_member.adapter = adapter
        }
        setMember()

    }


    fun returnmemid(): Long {

        var pos = spin_member.selectedItemPosition
        var id = 0L

        if (!memberlist.isNullOrEmpty()) {
            if (pos > 0) id = memberlist!!.get(pos - 1).mem_id
        }
        return id
    }

    fun setMember(): Int {
        var pos = 0
        var name = ""
        if (!memberlist.isNullOrEmpty()) {
            for (i in memberlist!!.indices) {
                if (validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid) == memberlist!!.get(i).mem_id) {
                    pos = i + 1
                    name = memberlist!!.get(i).member_name!!
                }

            }
        }
        tv_name.text = name
        tv_number.text = validate!!.RetriveSharepreferenceLong(MeetingSP.MemberCode).toString()
        spin_member.setSelection(pos)
        spin_member.isEnabled = false
        return pos
    }

    override fun onBackPressed() {
//        var intent = Intent(this, PeneltyListActivity::class.java)
        var intent = Intent(this, LoanDemandSummaryActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

    fun setLabelText() {
        tv_source_bank.text = LabelSet.getText(
            "source_bank",
            R.string.source_bank
        )
        if (validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) == 11 || validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) == 12) {
            tv_amount.text = LabelSet.getText(
                "current_principal_due",
                R.string.current_principal_due
            )
            tv_original_loan_amount.text = LabelSet.getText(
                "original_loan_amount",
                R.string.original_loan_amount
            )
            tv_principal_overdue.text = LabelSet.getText(
                "principal_overdue",
                R.string.principal_overdue
            )
            tv_interest_overdue.text = LabelSet.getText(
                "interest_overdue",
                R.string.interest_overdue
            )
        } else {
            tv_amount.text = LabelSet.getText(
                "amount",
                R.string.amount
            )
        }

        et_amount.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_period_months.text = LabelSet.getText(
            "period_months",
            R.string.period_months
        )
        tv_mode_of_payment.text = LabelSet.getText(
            "mod_of_payment",
            R.string.mod_of_payment
        )
        tv_loan_source.text = LabelSet.getText(
            "loan_purpose",
            R.string.loan_purpose
        )
        et_no_of_installment.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_amount_of_installment.text = LabelSet.getText(
            "amount_of_installment",
            R.string.amount_of_installment
        )
        et_amount_of_installment.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_monthly_interest_rate.text = LabelSet.getText(
            "interest_rate",
            R.string.interest_rate
        )
        et_monthly_interest_rate.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        // tv_loan_type.setText(LabelSet.getText("loan_type",R.string.loan_type))
        tv_moratorium_period.text = LabelSet.getText(
            "moratorium_period",
            R.string.moratorium_period
        )
        et_moratorium_period.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
        btn_savegray.text = LabelSet.getText(
            "save",
            R.string.save
        )
        //tv_title.setText(LabelSet.getText("loan_disbursement",R.string.loan_disbursement))

    }

    private fun fillSpinner() {



        /* dataspin_loan_product = lookupViewmodel!!.getlookup(
             50,
             validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
         )*/
        dataspin_modeofpayment = lookupViewmodel!!.getlookup(
            61,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_loan_source = lookupViewmodel!!.getlookup(
            67,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_frequency = lookupViewmodel!!.getlookup(
            19,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        validate!!.fillspinner(this, spin_loan_source, dataspin_loan_source)
        validate!!.fillspinner(this, spin_mode_of_payment, dataspin_modeofpayment)
        validate!!.fillspinner(this, spin_frequency, dataspin_frequency)
        spin_frequency.setSelection(3)
        spin_frequency.isEnabled = false
    }

    fun fillbank(
    ) {
        shgBankList =
            generateMeetingViewmodel.getBankdata(validate!!.RetriveSharepreferenceString(MeetingSP.SHGGUID))

        val adapter: ArrayAdapter<String?>
        if (!shgBankList.isNullOrEmpty()) {
            val isize = shgBankList!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in shgBankList!!.indices) {
                var lastthree = shgBankList!![i].account_no.substring(shgBankList!![i].account_no.length - 3)
                sValue[i + 1] =
                    shgBankList!![i].ifsc_code.toString().dropLast(7) + "XXXXX" + lastthree
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_source_bank.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_source_bank.adapter = adapter
        }

    }

    fun returaccount(): String {

        var pos = spin_source_bank.selectedItemPosition
        var id = ""

        if (!shgBankList.isNullOrEmpty()) {
            if (pos > 0) id = shgBankList!!.get(pos - 1).ifsc_code!!.dropLast(7) + shgBankList!!.get(
                pos - 1
            ).account_no
        }
        return id
    }

    fun setaccount(accountno: String): Int {

        var pos = 0

        if (!shgBankList.isNullOrEmpty()) {
            for (i in shgBankList!!.indices) {
                if (accountno.equals(shgBankList!!.get(i).ifsc_code!!.dropLast(7) + shgBankList!!.get(i).account_no))
                    pos = i + 1
            }
        }
        return pos
    }

    fun fillproduct(
    ) {
        dataspin_fund =
            loanProductViewmodel.getFundTypeBySource_Receipt(2,1)

        val adapter: ArrayAdapter<String?>
        if (!dataspin_fund.isNullOrEmpty()) {
            val isize = dataspin_fund!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in dataspin_fund!!.indices) {
                sValue[i + 1] =
                    dataspin_fund!![i].fundType
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_loan_type.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_loan_type.adapter = adapter
        }

    }

    fun returnid(): Int {

        var pos = spin_loan_type.selectedItemPosition
        var id = 0

        if (!dataspin_fund.isNullOrEmpty()) {
            if (pos > 0) id = dataspin_fund!!.get(pos - 1).fundTypeId
        }
        return id
    }

    fun setproduct(id: Int): Int {

        var pos = 0

        if (!dataspin_fund.isNullOrEmpty()) {
            for (i in dataspin_fund!!.indices) {
                if (dataspin_fund!!.get(i).fundTypeId == id)
                    pos = i + 1
            }
        }
        return pos
    }

    private fun checkValidation(): Int {

        var value = 1
        if (spin_member.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_member,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "member",
                    R.string.member
                )

            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_amount.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "pleaseenteramount",
                    R.string.pleaseenteramount
                ), this,
                et_amount
            )
            value = 0
            return value
        }
        if(sanctionAmount>0 && validate!!.returnIntegerValue(et_amount.text.toString())>sanctionAmount){
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "disbursed_proposed_amount",
                    R.string.disbursed_proposed_amount) + ":" +" "+ sanctionAmount.toString()
                , this,
                et_amount
            )
            value = 0
            return value
        }

        if(sanctionAmount==0 && validate!!.returnIntegerValue(et_amount.text.toString())>proposedAmount){
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "disbursed_proposed_amount",
                    R.string.disbursed_proposed_amount) + ":" +" "+ proposedAmount.toString()
                , this,
                et_amount
            )
            value = 0
            return value
        }
        if (validate!!.returnlookupcode(spin_mode_of_payment, dataspin_modeofpayment)==1 && validate!!.returnIntegerValue(et_amount.text.toString()) > validate!!.RetriveSharepreferenceInt(
                MeetingSP.Cashinhand
            )
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "inthecashbox",
                    R.string.inthecashbox
                ), this,
                et_amount
            )
            value = 0
            return value
        }
        if (validate!!.returnIntegerValue(et_no_of_installment.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "pleaseenterintallment",
                    R.string.pleaseenterintallment
                ), this,
                et_no_of_installment
            )
            value = 0
            return value
        }
        if (validate!!.returnDoubleValue(et_monthly_interest_rate.text.toString()) ==0.0  ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                )+LabelSet.getText(
                    "interest_rate",
                    R.string.interest_rate
                ), this,
                et_monthly_interest_rate
            )

            value = 0
            return value
        }

        if (validate!!.returnDoubleValue(et_monthly_interest_rate.text.toString()) > 36) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "valid_interest",
                    R.string.valid_interest
                ), this,
                et_monthly_interest_rate
            )

            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_loan_no.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "pleaseenterloanno",
                    R.string.pleaseenterloanno
                ), this,
                et_loan_no
            )
            value = 0
            return value
        }
        if (spin_mode_of_payment.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_mode_of_payment,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "mode_of_payment",
                    R.string.mode_of_payment
                )

            )
            value = 0
            return value
        }
        if (lay_bank.visibility == View.VISIBLE && spin_source_bank.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_source_bank,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "source_bank",
                    R.string.source_bank
                )

            )
            value = 0
            return value
        }
        var cashinbank= generateMeetingViewmodel.getclosingbal(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), returaccount())
        if(validate!!.returnlookupcode(spin_mode_of_payment, dataspin_modeofpayment)==2 && cashinbank < validate!!.returnIntegerValue(et_amount.text.toString())){
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "insufficient_balance",
                    R.string.insufficient_balance
                ), this,
                et_amount
            )
            value = 0
            return value
        }
        if (lay_chequeNO.visibility == View.VISIBLE && validate!!.returnStringValue(
                et_cheque_no_transactio_no.text.toString()
            ).length == 0
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + LabelSet.getText(
                    "cheque_no_transactio_no",
                    R.string.cheque_no_transactio_no
                ), this,
                et_cheque_no_transactio_no
            )
            value = 0
            return value
        }
        if (spin_frequency.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_frequency,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "loan_repayment_frequency",
                    R.string.loan_repayment_frequency
                )

            )
            value = 0
            return value
        }
        if (validate!!.returnlookupcode(
                spin_frequency,
                dataspin_frequency
            ) < validate!!.RetriveSharepreferenceInt(MeetingSP.meetingfrequency)
        ) {
            if (validate!!.RetriveSharepreferenceInt(MeetingSP.meetingfrequency) == 2) {
                validate!!.CustomAlertSpinner(
                    this, spin_frequency,
                    LabelSet.getText(
                        "monthlyfortnightly",
                        R.string.monthlyfortnightly
                    )

                )
            } else if (validate!!.RetriveSharepreferenceInt(MeetingSP.meetingfrequency) == 3) {
                validate!!.CustomAlertSpinner(
                    this, spin_frequency,
                    LabelSet.getText(
                        "yuocanselectmonthaly",
                        R.string.yuocanselectmonthaly
                    )

                )
            }else {
                validate!!.CustomAlertSpinner(
                    this, spin_frequency,
                    LabelSet.getText(
                        "please_select",
                        R.string.please_select
                    ) + " " + LabelSet.getText(
                        "loan_repayment_frequency",
                        R.string.loan_repayment_frequency
                    )

                )
            }
            value = 0
            return value
        }
        return value
    }

    private fun fillloandata() {
        var list = dtLoanViewmodel!!.getmemberLoanApplication(
            validate!!.RetriveSharepreferenceLong(MeetingSP.Loanappid)
        )
        if (!list.isNullOrEmpty()) {
            sanctionAmount = validate!!.returnIntegerValue(list.get(0).amt_sanction.toString())
            proposedAmount = validate!!.returnIntegerValue(list.get(0).amt_demand.toString())

            if(sanctionAmount>0){
                et_amount.setText(sanctionAmount.toString())
            }else if(proposedAmount > 0){
                et_amount.setText(proposedAmount.toString())
            }

            //  loanappid = list.get(0).loanapplication_id
            loanpurpose = validate!!.returnIntegerValue(list.get(0).loan_purpose.toString())

            spin_loan_type.setSelection(
                setproduct(validate!!.returnIntegerValue(list.get(0).loan_source.toString())))

            spin_loan_source.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(
                        list.get(0).loan_purpose.toString()
                    ), dataspin_loan_source
                )
            )

            var loanlist =
                dtLoanMemberViewmodel!!.getLoancount(validate!!.RetriveSharepreferenceLong(MeetingSP.Loanappid))
            if (!loanlist.isNullOrEmpty()) {
                et_loan_no.setText(loanlist.get(0).loan_no.toString())
                et_amount.setText(loanlist.get(0).amount.toString())
                et_no_of_installment.setText(loanlist.get(0).period.toString())
                var installmentAmount = validate!!.returnIntegerValue(loanlist.get(0).amount.toString())/validate!!.returnIntegerValue(loanlist.get(0).period.toString())
                et_amount_of_installment.setText(installmentAmount.toString())
                et_monthly_interest_rate.setText(loanlist.get(0).interest_rate.toString())
                et_moratorium_period.setText(loanlist.get(0).moratorium_period.toString())
                spin_frequency.setSelection(validate!!.returnlookupcodepos(loanlist.get(0).installment_freq,dataspin_frequency))


                spin_mode_of_payment.setSelection(
                    validate!!.returnlookupcodepos(
                        validate!!.returnIntegerValue(
                            loanlist.get(0).modepayment.toString()
                        ), dataspin_modeofpayment
                    )
                )
                spin_source_bank.setSelection(
                    setaccount(
                        loanlist.get(0).bank_code.toString()

                    )
                )


            } else {
                var loanno = dtLoanMemberViewmodel!!.getmaxLoanno(
                    validate!!.RetriveSharepreferenceLong(
                        MeetingSP.shgid
                    )
                )
                et_loan_no.setText(
                    (loanno + 1)
                        .toString()
                )
                spin_mode_of_payment.setSelection(
                    validate!!.returnlookupcodepos(
                        1, dataspin_modeofpayment
                    )
                )

            }
        }
    }

    private fun fillCutOffLoandata() {

        var loanlist =
            dtLoanMemberViewmodel!!.getLoanDetailData(validate!!.RetriveSharepreferenceInt(MeetingSP.Loanno), validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid), validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!)
        if (!loanlist.isNullOrEmpty()) {
            et_loan_no.setText(loanlist.get(0).loan_no.toString())
            et_no_of_installment.setText(validate!!.returnStringValue(loanlist.get(0).period.toString()))
            et_monthly_interest_rate.setText(validate!!.returnStringValue(loanlist.get(0).interest_rate.toString()))
            et_moratorium_period.setText(validate!!.returnStringValue(loanlist.get(0).moratorium_period.toString()))
            et_original_loan_amount.setText(validate!!.returnStringValue(loanlist.get(0).original_loan_amount.toString()))
            et_amount.setText(validate!!.returnStringValue(loanlist.get(0).amount.toString()))
            et_principal_overdue.setText(validate!!.returnStringValue(loanlist.get(0).principal_overdue.toString()))
            et_interest_overdue.setText(validate!!.returnStringValue(loanlist.get(0).interest_overdue.toString()))

            spin_loan_source.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(
                        loanlist.get(0).loan_purpose.toString()
                    ), dataspin_loan_source
                )
            )
            spin_mode_of_payment.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(
                        loanlist.get(0).modepayment.toString()
                    ), dataspin_modeofpayment
                )
            )
            spin_source_bank.setSelection(
                setaccount(
                    loanlist.get(0).bank_code.toString()

                )
            )
            spin_loan_type.setSelection(
                setproduct(
                    validate!!.returnIntegerValue(loanlist.get(0).loan_type.toString())))

        } else {
            var loanno = dtLoanMemberViewmodel!!.getmaxLoanno(
                validate!!.RetriveSharepreferenceLong(
                    MeetingSP.shgid
                )
            )
            et_loan_no.setText(
                (loanno + 1)
                    .toString()
            )

        }

    }

    private fun saveData(morotrum: Int) {

        dtLoanMemberEntity = DtLoanMemberEntity(
            0,
            validate!!.RetriveSharepreferenceLong(MeetingSP.Loanappid),
            validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            returnmemid(),
            validate!!.returnIntegerValue(et_loan_no.text.toString()),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            validate!!.addmonth(validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate), morotrum,validate!!.returnlookupcode(spin_frequency, dataspin_frequency)),
            validate!!.returnIntegerValue(et_original_loan_amount.text.toString()),
            validate!!.returnIntegerValue(et_amount.text.toString()),
            validate!!.returnlookupcode(spin_loan_source, dataspin_loan_source),
            returnid(),
            validate!!.returnDoubleValue(et_monthly_interest_rate.text.toString()),
            validate!!.returnIntegerValue(et_no_of_installment.text.toString()),
            0,
            0,
            false,
            returnid(),
            validate!!.returnlookupcode(spin_loan_source, dataspin_loan_source),
            validate!!.returnlookupcode(spin_mode_of_payment, dataspin_modeofpayment),
            returaccount(),
            validate!!.returnStringValue(
                et_cheque_no_transactio_no.text.toString()
            ),
            validate!!.returnlookupcode(spin_frequency, dataspin_frequency),
            validate!!.returnIntegerValue(et_moratorium_period.text.toString()),
            validate!!.returnStringValue(et_loan_no.text.toString()),
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            nullStringValue,
            nullLongValue,
            nullStringValue,
            nullLongValue, 0,
            0,
            0,1,validate!!.returnDoubleValue(et_monthly_interest_rate.text.toString()),validate!!.returnIntegerValue(et_no_of_installment.text.toString()),0,1


        )
        dtLoanMemberViewmodel!!.insert(dtLoanMemberEntity!!)

        // insert in dtlonmemtxn

        dtLoanTxnMemEntity = DtLoanTxnMemEntity(
            0,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            returnmemid(),
            validate!!.returnIntegerValue(et_loan_no.text.toString()),
            validate!!.returnIntegerValue(et_amount.text.toString()),
            0, 0,
            0,
            validate!!.returnIntegerValue(et_amount.text.toString()),
            0,
            false,
            0,
            0,
            0,
            0,
            0,
            0,
            validate!!.returnlookupcode(spin_mode_of_payment, dataspin_modeofpayment),
            returaccount(),
            validate!!.returnStringValue(
                et_cheque_no_transactio_no.text.toString()
            ),
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            nullStringValue,
            nullLongValue,
            nullStringValue,
            nullLongValue,validate!!.returnDoubleValue(et_monthly_interest_rate.text.toString()),
            validate!!.returnIntegerValue(et_no_of_installment.text.toString())
        )
        dtLoanTxnMemViewmodel!!.insert(dtLoanTxnMemEntity!!)

        if (validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) == 0) {
            dtLoanViewmodel!!.updateMemberloanapplication(
                validate!!.RetriveSharepreferenceString(
                    MeetingSP.mtg_guid
                )!!,
                returnmemid(),
                validate!!.RetriveSharepreferenceLong(MeetingSP.Loanappid),
                validate!!.returnIntegerValue(et_amount.text.toString()),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber)
            )

        }

    }

    private fun insertscheduler(amt: Int, installment: Int, morotrum: Int) {
        var principaldemand = amt / installment
        var loanos = 0
        var saveValue = 0
        var installmentNum =0
        var memid = returnmemid()
        var maxRepaidInstallment =dtLoanMemberScheduleViewmodel!!.getMaxRepaidInstallmentNum(validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),memid,validate!!.returnIntegerValue(et_loan_no.text.toString()))
        var installment_dt:Long? = 0

        if(validate!!.RetriveSharepreferenceInt(MeetingSP.loanReschedule)==1){
            dtLoanMemberScheduleViewmodel!!.deleteMemberSubinstallment(validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),memid,validate!!.returnIntegerValue(et_loan_no.text.toString()))
            installmentNum = maxRepaidInstallment
            if(installmentNum > 0) {
                installment_dt = dtLoanMemberScheduleViewmodel!!.getInstallmentdateByInstallmentNum(
                    installmentNum,
                    validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                    memid,
                    validate!!.returnIntegerValue(et_loan_no.text.toString())
                )
            }else {
                installment_dt = validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate)
            }
        }else {
            installment_dt = validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate)
        }
        for (i in installmentNum until installment) {
            if(validate!!.RetriveSharepreferenceInt(MeetingSP.loanReschedule)==1){
                installmentNum = installmentNum + 1
            }else {
                installmentNum = i + 1
            }
            if (i == installment - 1) {
                loanos = 0
                principaldemand = amt - (principaldemand * i)
            } else {
                loanos = amt - (principaldemand * (i + 1))
            }
            dtLoanMemberSheduleEntity = DtLoanMemberSheduleEntity(
                0,
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                memid,
                validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
                validate!!.returnIntegerValue(et_loan_no.text.toString()),
                principaldemand,
                principaldemand,
                loanos, 0,
                installmentNum,
                1,
                validate!!.addmonth(
                    installment_dt,
                    (morotrum + installmentNum),validate!!.returnlookupcode(spin_frequency, dataspin_frequency)),
                //   validate!!.Daybetweentime(validate!!.currentdatetime),
                0,
                0,
                null,

                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime), nullStringValue,
                nullLongValue,
                nullStringValue,
                nullLongValue


            )
            dtLoanMemberScheduleViewmodel!!.insert(dtLoanMemberSheduleEntity!!)

        }
        if (validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) == 11 || validate!!.RetriveSharepreferenceInt(
                MeetingSP.MeetingType
            ) == 12
        ) {
            var principalDemand =
                dtLoanMemberScheduleViewmodel!!.getPrincipalDemandByInstallmentNum(
                    memid,
                    validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
                    validate!!.returnIntegerValue(et_loan_no.text.toString()),
                    1
                )
            principalDemand += validate!!.returnIntegerValue(et_principal_overdue.text.toString())

            dtLoanMemberScheduleViewmodel!!.updateCutOffLoanMemberSchedule(
                validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
                memid,
                validate!!.returnIntegerValue(et_loan_no.text.toString()),
                1,
                principalDemand
            )
        }
        CustomAlertchangeschedule(
            LabelSet.getText(
                "doyouwanttochaneschedule",
                R.string.doyouwanttochaneschedule
            )
        )
    }

    fun CustomAlertchangeschedule(str: String) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str
        mDialogView.btn_yes.setOnClickListener {
            //            PartialSaveData()
            mAlertDialog.dismiss()
            validate!!.SaveSharepreferenceLong(MeetingSP.Memberid, returnmemid())
            validate!!.SaveSharepreferenceInt(
                MeetingSP.Loanno,
                validate!!.returnIntegerValue(et_loan_no.text.toString())
            )
            validate!!.SaveSharepreferenceString(
                MeetingSP.MemberName,
                validate!!.returnStringValue(tv_name.text.toString())
            )

            val intent = Intent(this, PrincipalDemandActivity::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)


        }
        mDialogView.btn_no.setOnClickListener {
            mAlertDialog.dismiss()

            val intent = Intent(this, LoanDemandSummaryActivity::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)

        }
    }

}