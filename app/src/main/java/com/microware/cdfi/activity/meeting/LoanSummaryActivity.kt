package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.LookupViewmodel
import kotlinx.android.synthetic.main.activity_loan_summary.*
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.repay_toolbar.*
import kotlinx.android.synthetic.main.repaytablayout.*

class LoanSummaryActivity : AppCompatActivity() {
    var lookupViewmodel: LookupViewmodel? = null
    var validate: Validate? = null
    var dataspin_mode_of_payment: List<LookupEntity>? = null
    var dataspin_bank_name: List<LookupEntity>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loan_summary)
        validate=Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)

        IvAttendance.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSaving.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvWithdrawal.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPenalty.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvRepayment.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
        IvBankTransaction.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvCashBox.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvGroupMeeting.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvExpenditurePayment.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvRecipientIncome.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvAttendance.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tvSaving.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tvWithdrawal.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tvPenalty.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tvRepayment.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        tvBankTransaction.setBackgroundColor(ContextCompat.getColor(this, R.color.white))

        btn_cancel.setOnClickListener {
            var intent = Intent(this, MeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_compulsorySaving.setOnClickListener {
            var intent = Intent(this, CompulsorySavingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        lay_saving.setOnClickListener {
            var intent = Intent(this, VoluntorySavingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        lay_loan_disbursment.setOnClickListener {
            var intent = Intent(this, LoanDisbursementActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_withdrawal.setOnClickListener {
            var intent = Intent(this, WidthdrawalDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_penalty.setOnClickListener {
            var intent = Intent(this, PeneltyDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_groupLoanRepaid.setOnClickListener {
            var intent = Intent(this, GroupLoanRepaidActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_groupLoanReceived.setOnClickListener {
            var intent = Intent(this, GroupLoanReceivedActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_BankTransaction.setOnClickListener {
            var intent = Intent(this, BankGroupTransactionActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }
        lay_CashBox.setOnClickListener {
            var intent = Intent(this, CashBoxActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_GroupMeeting.setOnClickListener {
            var intent = Intent(this, GroupMeetingSummery::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_ExpenditurePayment.setOnClickListener {
            var intent = Intent(this, ExpenditurePaymentActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_RecipientIncome.setOnClickListener {
            var intent = Intent(this, RecipentIncomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        setLabelText()
        fillSpinner()
    }

    override fun onBackPressed() {
        var intent = Intent(this, RepaymentActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0,0)
        finish()
    }

    fun setLabelText() {
        tv_title.text = LabelSet.getText(
            "loan_summary",
            R.string.loan_summary
        )
        tvmember.text = LabelSet.getText(
            "member_code",
            R.string.member_code
        )
        txt_member.text = LabelSet.getText(
            "member_",
            R.string.member_
        )
        tv_mod_of_payment.text = LabelSet.getText(
            "mod_of_payment",
            R.string.mod_of_payment
        )
        tv_bank_name.text = LabelSet.getText(
            "bank_name_",
            R.string.bank_name_
        )

        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }

    private fun fillSpinner() {
        dataspin_mode_of_payment = lookupViewmodel!!.getlookup(
                99,
                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_bank_name = lookupViewmodel!!.getlookup(
                99,
                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        validate!!.fillspinner(this, spin_mode_of_payment, dataspin_mode_of_payment)
        validate!!.fillspinner(this, spin_bank_name, dataspin_bank_name)
    }

}