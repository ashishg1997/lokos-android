package com.microware.cdfi.activity.meeting

import android.content.Intent
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.LoanWiseRepaymentByGroupAdapter
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_loan_wise_repayment_by_group.*

class LoanWiseRepaymentByGroupActivity : AppCompatActivity() {

    var validate: Validate? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    var lookupViewmodel: LookupViewmodel? = null
    lateinit var dtLoanGpViewmodel: DtLoanGpViewmodel
    lateinit var dtLoanGpTxnViewmodel: DtLoanGpTxnViewmodel
    lateinit var dtMtgGrpLoanScheduleViewmodel: DtMtgGrpLoanScheduleViewmodel

    var today = 0
    var dataspin_mode_of_payment: List<LookupEntity>? = null
    var shgBankList: List<Cbo_bankEntity>? = null
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loan_wise_repayment_by_group)
        validate = Validate(this)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        lookupViewmodel =
            ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        dtLoanGpViewmodel = ViewModelProviders.of(this).get(DtLoanGpViewmodel::class.java)
        dtMtgGrpLoanScheduleViewmodel =
            ViewModelProviders.of(this).get(DtMtgGrpLoanScheduleViewmodel::class.java)
        dtLoanGpTxnViewmodel = ViewModelProviders.of(this).get(DtLoanGpTxnViewmodel::class.java)
        tv_institution.text = validate!!.RetriveSharepreferenceString(MeetingSP.loanSource)


        replaceFragmenty(
            fragment = MeetingTopBarFragment(16),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        btn_pay_total.isEnabled =
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber) >= validate!!.RetriveSharepreferenceInt(
                MeetingSP.maxmeetingnumber
            )
        btn_pay_total.setOnClickListener {
            if (checkValidation() == 1) {
                saveloanrepayment()
            }
        }
        btn_pay_partial.setOnClickListener {
            var intent = Intent(this, ScenarioOneLoanRepaymentGroupActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        }
        spin_mode_of_payment?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (validate!!.returnlookupcode(
                        spin_mode_of_payment,
                        dataspin_mode_of_payment
                    ) == 2 || validate!!.returnlookupcode(
                        spin_mode_of_payment,
                        dataspin_mode_of_payment
                    ) == 3
                ) {
                    lay_bank.visibility = View.VISIBLE
                    lay_cheque_no_transactio_no.visibility = View.VISIBLE
                } else {
                    lay_bank.visibility = View.GONE
                    lay_cheque_no_transactio_no.visibility = View.GONE
                    spin_bank.setSelection(0)

                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }
        setLabelText()
        fillRecyclerView()
        fillspinner()
        fillbank()

    }

    private fun checkValidation(): Int {

        var value = 1

        if (validate!!.returnIntegerValue(tv_total.text.toString()) == 0) {
            validate!!.CustomAlert(
                LabelSet.getText(
                    "amountalreadypaid",
                    R.string.amountalreadypaid
                ), this
            )
            value = 0
            return value
        }
        if (spin_mode_of_payment.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_mode_of_payment,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "mode_of_payment",
                    R.string.mode_of_payment
                )

            )
            value = 0
            return value
        }
        if (validate!!.returnlookupcode(
                spin_mode_of_payment,
                dataspin_mode_of_payment
            ) == 1 && validate!!.returnIntegerValue(tv_total.text.toString()) > validate!!.RetriveSharepreferenceInt(
                MeetingSP.Cashinhand
            )
        ) {
            validate!!.CustomAlertSpinner(
                this, spin_mode_of_payment,
                LabelSet.getText(
                    "inthecashbox",
                    R.string.inthecashbox
                )

            )
            value = 0
            return value
        }
        if (lay_bank.visibility == View.VISIBLE && spin_bank.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_bank,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText("bank", R.string.bank)

            )
            value = 0
            return value
        }
        var cashinbank = generateMeetingViewmodel.getclosingbal(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), returaccount()
        )
        if (lay_bank.visibility == View.VISIBLE && cashinbank < validate!!.returnIntegerValue(
                tv_total.text.toString()
            )
        ) {

            validate!!.CustomAlertSpinner(
                this, spin_bank,
                LabelSet.getText(
                    "insufficient_balance",
                    R.string.insufficient_balance
                )

            )
            value = 0
            return value
        }

        if (lay_cheque_no_transactio_no.visibility == View.VISIBLE && validate!!.returnStringValue(
                et_cheque_no_transactio_no.text.toString()
            ).length == 0
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + LabelSet.getText(
                    "cheque_no_transactio_no",
                    R.string.cheque_no_transactio_no
                ), this,
                et_cheque_no_transactio_no
            )
            value = 0
            return value
        }
        return value
    }

    override fun onBackPressed() {
        var intent = Intent(this, GroupRepaymentDetailActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

    fun fillspinner() {
        dataspin_mode_of_payment = lookupViewmodel!!.getlookup(
            65,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        validate!!.fillspinner(this, spin_mode_of_payment, dataspin_mode_of_payment)
        spin_mode_of_payment.setSelection(
            validate!!.returnlookupcodepos(
                1, dataspin_mode_of_payment
            )
        )
    }

    fun fillbank(
    ) {
        shgBankList =
            generateMeetingViewmodel.getBankdata(validate!!.RetriveSharepreferenceString(MeetingSP.SHGGUID))

        val adapter: ArrayAdapter<String?>
        if (!shgBankList.isNullOrEmpty()) {
            val isize = shgBankList!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in shgBankList!!.indices) {
                var lastthree =
                    shgBankList!![i].account_no.substring(shgBankList!![i].account_no.length - 3)
                sValue[i + 1] =
                    shgBankList!![i].ifsc_code.toString().dropLast(7) + "XXXXX" + lastthree
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_bank.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_bank.adapter = adapter
        }

    }

    fun returaccount(): String {

        var pos = spin_bank.selectedItemPosition
        var id = ""

        if (!shgBankList.isNullOrEmpty()) {
            if (pos > 0) id =
                shgBankList!!.get(pos - 1).ifsc_code!!.dropLast(7) + shgBankList!!.get(pos - 1).account_no
        }
        return id
    }

    fun setaccount(accountno: String): Int {

        var pos = 0

        if (!shgBankList.isNullOrEmpty()) {
            for (i in shgBankList!!.indices) {
                if (accountno.equals(
                        shgBankList!!.get(i).ifsc_code!!.dropLast(7) + shgBankList!!.get(
                            i
                        ).account_no
                    )
                )
                    pos = i + 1
            }
        }
        return pos
    }

    fun setLabelText() {
        tv_institution.text = LabelSet.getText(
            "loan_source",
            R.string.loan_source
        )
        tv_loan_no.text = LabelSet.getText(
            "loan_no",
            R.string.loan_no
        )
        tv_outstanding.text = LabelSet.getText(
            "outstanding_installment",
            R.string.outstanding_installment
        )
        tv_original_loan_amount.text = LabelSet.getText(
            "original_loan_amount",
            R.string.original_loan_amount
        )
        tvCurrentDue.text = LabelSet.getText(
            "current_due",
            R.string.current_due
        )
        tv_current_arrear.text = LabelSet.getText(
            "principal",
            R.string.principal
        ) + " + " + LabelSet.getText("interest", R.string.interest)
        tv_current_arrear1.text = LabelSet.getText(
            "principal",
            R.string.principal
        ) + " + " + LabelSet.getText("interest", R.string.interest)
        tv_total_due.text = LabelSet.getText(
            "total_due",
            R.string.total_due
        )
        tv_total_to_repay.text = LabelSet.getText(
            "total_to_repay",
            R.string.total_to_repay
        )

        btn_pay_total.text = LabelSet.getText(
            "pay_total",
            R.string.pay_total
        )
    }

    fun getloanamount(loanno: Int): Int {
        return dtLoanGpViewmodel.gettotamt(loanno)
    }

    private fun fillRecyclerView() {
        today = 0
        var list = dtLoanGpTxnViewmodel.getloandata(
            validate!!.RetriveSharepreferenceInt(MeetingSP.Loanno),
            validate!!.RetriveSharepreferenceInt(
                MeetingSP.currentmeetingnumber
            ), validate!!.RetriveSharepreferenceLong(
                MeetingSP.shgid
            )
        )
        var loanWiseRepaymentByMemberAdapter = LoanWiseRepaymentByGroupAdapter(this, list)

        rvList.layoutManager = LinearLayoutManager(this)

        val isize: Int
        isize = list!!.size
        val params: ViewGroup.LayoutParams = rvList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvList.layoutParams = params
        rvList.adapter = loanWiseRepaymentByMemberAdapter
        getTotalValue()
    }

    fun saveloanrepayment() {
        var saveValue = 0

        val iCount = rvList.childCount


        for (i in 0 until iCount) {
            val gridChild =
                rvList.getChildAt(i) as? ViewGroup

            val tv_loan_no1 = gridChild!!
                .findViewById<View>(R.id.tv_loan_no1) as? TextView

            val total_due1 = gridChild
                .findViewById<View>(R.id.total_due1) as? TextView



            if (validate!!.returnIntegerValue(total_due1!!.text.toString()) > 0) {
                var loanno = validate!!.returnIntegerValue(tv_loan_no1!!.text.toString())
                var shgid = validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
                var paid = validate!!.returnIntegerValue(total_due1.text.toString())
                saveValue = saveData(loanno, shgid, paid)
                // updateschedule(loanno, memid, paid)

            }
        }

        if (saveValue > 0) {
            /* CDFIApplication.database?.dtmtgDao()
                 ?.updatecompsave(validate!!.returnIntegerValue(tv_TotalCumValue.text.toString()),validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),validate!!.RetriveSharepreferenceLong(MeetingSP.Shgcode))

             replaceFragmenty(
                 fragment = MeetingTopBarFragment(2),
                 allowStateLoss = true,
                 containerViewId = R.id.mainContent
             )*/
            fillRecyclerView()
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this
            )
        }
    }

    fun saveData(loanno: Int, shgid: Long, paid: Int): Int {
        var value = 0
        var listdata = dtLoanGpTxnViewmodel.getloandata(
            loanno,
            validate!!.RetriveSharepreferenceInt(
                MeetingSP.currentmeetingnumber
            ),
            shgid
        )
        var loanos = 0
        var amt = paid
        var intrest = 0
        var principaldemandcl = 0
        if (!listdata.isNullOrEmpty()) {
            loanos =
                validate!!.returnIntegerValue(listdata.get(0).loan_op.toString())
            principaldemandcl =
                validate!!.returnIntegerValue(listdata.get(0).principal_demand_ob.toString()) + validate!!.returnIntegerValue(
                    listdata.get(0).principal_demand.toString()
                )
            intrest =
                validate!!.returnIntegerValue(listdata.get(0).int_accrued_op.toString()) + validate!!.returnIntegerValue(
                    listdata.get(0).int_accrued.toString()
                )
            if (amt > intrest) {
                amt = amt - intrest
            } else {
                intrest = intrest - amt
                amt = 0
            }

        }
        if (principaldemandcl > amt) {
            principaldemandcl = principaldemandcl - amt
        } else {
            principaldemandcl = 0
        }

        var completionflag=false
        if (amt>=loanos){
            completionflag=true
        }

        dtLoanGpTxnViewmodel.updateloanpaid(
            loanno,
            validate!!.RetriveSharepreferenceInt(
                MeetingSP.currentmeetingnumber
            ),
            shgid,
            amt,
            intrest,
            validate!!.returnlookupcode(spin_mode_of_payment, dataspin_mode_of_payment),
            returaccount(),
            validate!!.returnStringValue(et_cheque_no_transactio_no.text.toString()),
            principaldemandcl,completionflag
        )

        dtLoanGpViewmodel.updateShgGroupLoan(shgid,loanno,completionflag)
        updateschedule(loanno, shgid, amt)
        value = 1

        return value
    }

    fun updateschedule(loanno: Int, shgid: Long, paid: Int): Int {
        var value = 0
        var amt = paid
        dtMtgGrpLoanScheduleViewmodel.deleterepaid(
            loanno,
            shgid,
            validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber)
        )
        dtMtgGrpLoanScheduleViewmodel.deletsubinstallment(
            loanno,
            shgid,
            validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber)
        )
        var list = dtMtgGrpLoanScheduleViewmodel.getScheduleloanwise(shgid, loanno)

        for (i in list!!.indices) {
            if (amt > 0) {
                if (validate!!.returnIntegerValue(list.get(i).principal_demand.toString()) <= amt) {
                    dtMtgGrpLoanScheduleViewmodel.updaterepaid(
                        loanno,
                        shgid,
                        list.get(i).installment_no,
                        list.get(i).sub_installment_no,
                        validate!!.returnIntegerValue(list.get(i).principal_demand.toString()),
                        validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
                        validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime)
                    )
                    amt =
                        amt - validate!!.returnIntegerValue(list.get(i).principal_demand.toString())
                } else {
                    dtMtgGrpLoanScheduleViewmodel.updaterepaid(
                        loanno,
                        shgid,
                        list.get(i).installment_no,
                        list.get(i).sub_installment_no,
                        amt,
                        validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
                        validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime)
                    )
                    break
                }
            } else {
                break
            }

        }
        value = 1

        return value
    }

    fun getTotalValue() {
        var totalpaid = dtLoanGpTxnViewmodel.gettotalpaid(
            validate!!.RetriveSharepreferenceLong(
                MeetingSP.shgid
            ), validate!!.RetriveSharepreferenceInt(
                MeetingSP.currentmeetingnumber
            ), validate!!.RetriveSharepreferenceInt(
                MeetingSP.Loanno
            )
        )
//            today = today + iValue
        if (totalpaid >= 0) {
            tv_total.text = totalpaid.toString()
        } else {
            tv_total.text = "0"
        }

    }

    fun getremaininginstallment(loanno: Int): Int {
        return dtMtgGrpLoanScheduleViewmodel.getremaininginstallment(
            loanno,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )


    }

}