package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.ArrayAdapter
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.DtmtgDetEntity
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.entity.ShgMcpEntity
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.ShgMcpViewmodel
import kotlinx.android.synthetic.main.activity_loan_demand.*
import kotlinx.android.synthetic.main.activity_mcp_preparation.*
import kotlinx.android.synthetic.main.activity_mcp_preparation.et_priority_valid_upto
import kotlinx.android.synthetic.main.buttons.*

class MCPPreparationActivity : AppCompatActivity() {
    var lookupViewmodel: LookupViewmodel? = null
    var validate: Validate? = null
    var dataspin_demand_purpose: List<LookupEntity>? = null
    var dataspin_demand_urgency: List<LookupEntity>? = null
    var memberlist: List<DtmtgDetEntity>? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    lateinit var shgMcpViewmodel: ShgMcpViewmodel
    var shgMcpEntity: ShgMcpEntity? = null

    var nullStringValue:String?=null
    var nullLongValue:Long?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mcp_preparation)
        validate = Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        generateMeetingViewmodel = ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        shgMcpViewmodel = ViewModelProviders.of(this).get(ShgMcpViewmodel::class.java)

        replaceFragmenty(
            fragment = MeetingTopBarFragment(100),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        et_meeting_no.setText(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber).toString())
        et_meeting_date.setText(validate!!.convertDatetime(validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate)))

        et_priority_valid_upto.setOnClickListener {
            validate!!.datePickerwithmindate1(validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),et_priority_valid_upto)
        }

        btn_cancel.setOnClickListener {
            val intent = Intent(this, MCPPreparationListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        btn_save.isEnabled =
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber) >= validate!!.RetriveSharepreferenceInt(MeetingSP.maxmeetingnumber)
        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                SaveData()
            }
        }

        setLabelText()
        fillSpinner()
        fillmemberspinner()
        showData()

        et_demand_amount.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun afterTextChanged(s: Editable?) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if(validate!!.returnIntegerValue(et_demand_amount.text.toString())>0 && validate!!.returnIntegerValue(et_proposed_emi_amount.text.toString())>0){
                    var tenure = validate!!.returnIntegerValue(et_demand_amount.text.toString())/validate!!.returnIntegerValue(et_proposed_emi_amount.text.toString())
                    et_proposed_tenure_in_months.setText(validate!!.returnStringValue(tenure.toString()))
                }
            }
        })

        et_proposed_emi_amount.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun afterTextChanged(s: Editable?) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if(validate!!.returnIntegerValue(et_demand_amount.text.toString())>0 && validate!!.returnIntegerValue(et_proposed_emi_amount.text.toString())>0){
                    var tenure = validate!!.returnIntegerValue(et_demand_amount.text.toString())/validate!!.returnIntegerValue(et_proposed_emi_amount.text.toString())
                    et_proposed_tenure_in_months.setText(validate!!.returnStringValue(tenure.toString()))
                }
            }
        })
    }

    private fun showData() {
        val listData = shgMcpViewmodel.getShgMcpDataAlllist(
            validate!!.RetriveSharepreferenceLong(MeetingSP.Mcpid),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid)
        )

        if (!listData.isNullOrEmpty()) {
            setMember()
            et_demand_amount.setText(validate!!.returnStringValue(listData[0].amt_demand.toString()))
            et_proposed_tenure_in_months.setText(validate!!.returnStringValue(listData[0].loan_period.toString()))
            if(validate!!.returnIntegerValue(listData[0].amt_demand.toString())>0 && validate!!.returnIntegerValue(listData[0].loan_period.toString())>0){
                var proposedAmount = validate!!.returnIntegerValue(listData[0].amt_demand.toString()) / validate!!.returnIntegerValue(listData[0].loan_period.toString())
            }
            et_proposed_emi_amount.setText(validate!!.returnStringValue(listData[0].proposed_emi_amount.toString()))
            spin_demand_purpose.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(
                        listData[0].loan_purpose.toString()
                    ), dataspin_demand_purpose
                )
            )

            et_priority_valid_upto.setText(validate!!.convertDatetime(listData[0].tentative_date))
        }else {
            et_priority_valid_upto.setText(validate!!.convertDatetime(validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate)))
        }

    }

    private fun setLabelText() {
        tv_meeting_no.text = LabelSet.getText(
            "meeting_no",
            R.string.meeting_no
        )
        tv_meeting_date.text = LabelSet.getText(
            "meeting_date",
            R.string.meeting_date
        )
        tv_name_of_member.text = LabelSet.getText(
            "name_of_member",
            R.string.name_of_member
        )
        tv_demand_amount.text = LabelSet.getText(
            "demand_amount",
            R.string.demand_amount
        )
        tv_demand_purpose.text = LabelSet.getText(
            "demand_purpose",
            R.string.demand_purpose
        )
        tv_demand_urgency.text = LabelSet.getText(
            "demand_urgency",
            R.string.demand_urgency
        )
        tv_proposed_emi_amount.text = LabelSet.getText(
            "proposed_emi_amount",
            R.string.proposed_emi_amount
        )
        tv_proposed_tenure_in_months.text = LabelSet.getText(
            "proposed_tenure_in_months",
            R.string.proposed_tenure_in_months
        )
        et_meeting_no.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_meeting_date.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )
        et_demand_amount.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_proposed_emi_amount.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_proposed_tenure_in_months.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }

    private fun fillSpinner() {

        dataspin_demand_purpose = lookupViewmodel!!.getlookup(
            67,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_demand_urgency = lookupViewmodel!!.getlookup(
            68,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        validate!!.fillspinner(this, spin_demand_purpose, dataspin_demand_purpose)
        validate!!.fillspinner(this, spin_demand_urgency, dataspin_demand_urgency)
    }

    private fun fillmemberspinner() {
        memberlist = generateMeetingViewmodel.getListDataMtgByMtgnum(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        val adapter: ArrayAdapter<String?>
        if (!memberlist.isNullOrEmpty()) {
            val isize = memberlist!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in memberlist!!.indices) {
                sValue[i + 1] = memberlist!![i].member_name
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_membername.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(memberlist!!.size + 1)
            sValue[0] = LabelSet.getText(
                "nomemberavailable",
                R.string.nomemberavailable
            )
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_membername.adapter = adapter
        }
    }

    private fun setMember(): Int {
        var pos = 0
        if (!memberlist.isNullOrEmpty()) {
            for (i in memberlist!!.indices) {
                if (validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid) == memberlist!!.get(i).mem_id)
                    pos = i + 1
            }
        }
        spin_membername.setSelection(pos)
        spin_membername.isEnabled = false
        return pos
    }

    private fun returnmemid(): Long {

        var pos = spin_membername.selectedItemPosition
        var id = 0L

        if (!memberlist.isNullOrEmpty()) {
            if (pos > 0) id = memberlist!!.get(pos - 1).mem_id
        }
        return id
    }

    private fun checkValidation(): Int {
        var value = 1
        if (spin_membername.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_membername,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "name_of_member",
                    R.string.name_of_member
                )
            )
            value = 0
        } else if (et_demand_amount.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "demand_amount",
                    R.string.demand_amount
                ),
                this, et_demand_amount
            )
            value = 0
        } else if (spin_demand_purpose.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_demand_purpose, LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "demand_purpose",
                    R.string.demand_purpose
                )

            )
            value = 0
        } else if (validate!!.returnStringValue(et_priority_valid_upto.text.toString()).length==0){
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "request_valid_upto",
                    R.string.request_valid_upto
                ), this,
                et_priority_valid_upto
            )
            value = 0
            return value
        }else if (et_proposed_emi_amount.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "proposed_emi_amount",
                    R.string.proposed_emi_amount
                ),
                this, et_proposed_emi_amount
            )
            value = 0
        } else if(validate!!.returnIntegerValue(et_proposed_emi_amount.text.toString()) > validate!!.returnIntegerValue(
                et_demand_amount.text.toString())) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "proposed_emi_amount",
                    R.string.proposed_emi_amount
                ),
                this,
                et_proposed_emi_amount
            )
            return 0
        } else if (et_proposed_tenure_in_months.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "proposed_tenure_in_months",
                    R.string.proposed_tenure_in_months
                ),
                this, et_proposed_tenure_in_months
            )
            value = 0
        }else if(validate!!.returnIntegerValue(et_proposed_tenure_in_months.text.toString())>0 && validate!!.returnIntegerValue(et_proposed_tenure_in_months.text.toString())>60){
            validate!!.CustomAlert(LabelSet.getText("valid_tenure_60",R.string.valid_tenure_60),this)
            value = 0
        }


        return value
    }

    private fun SaveData() {
        var loan_product_id:Int? = null
        var save = 0

        var defaultMtgNum:Int?=null
        var defaultMtgGuid:String?=null
        /*For MCP ID (memberID*10000) + recordcount + 1*/
        val mcpCount = shgMcpViewmodel.getCount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )

        val mcpID = (validate!!.RetriveSharepreferenceLong(MeetingSP.Shgcode) * 1000) + mcpCount + 1

        if (validate!!.RetriveSharepreferenceLong(MeetingSP.Mcpid) > 0) {
            shgMcpViewmodel.updateMcpPrepration(
                validate!!.RetriveSharepreferenceLong(MeetingSP.Mcpid),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid),
                validate!!.returnIntegerValue(et_demand_amount.text.toString()),
                validate!!.Daybetweentime(et_priority_valid_upto.text.toString()),
                loan_product_id,
                0,
                validate!!.returnlookupcode(spin_demand_purpose, dataspin_demand_purpose),
                validate!!.returnIntegerValue(et_proposed_tenure_in_months.text.toString()),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid),
                defaultMtgNum,
                defaultMtgGuid,
                0,
                validate!!.returnIntegerValue(et_proposed_emi_amount.text.toString()),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime)
            )
            save = 2

        } else {
            shgMcpEntity = ShgMcpEntity(
                0,
                mcpID,
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                returnmemid(),
                validate!!.returnIntegerValue(et_demand_amount.text.toString()),
                validate!!.Daybetweentime(et_priority_valid_upto.text.toString()),
                loan_product_id,
                0,
                validate!!.returnlookupcode(spin_demand_purpose, dataspin_demand_purpose),
                validate!!.returnIntegerValue(et_proposed_tenure_in_months.text.toString()),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid),
                defaultMtgNum,
                defaultMtgGuid,
                validate!!.returnlookupcode(spin_demand_urgency, dataspin_demand_urgency),
                validate!!.returnIntegerValue(et_proposed_emi_amount.text.toString()),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                nullStringValue,
                nullLongValue,
                nullStringValue,
                nullLongValue,
                validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate)
            )
            shgMcpViewmodel.insert(shgMcpEntity!!)
            save = 1

        }

        if (save == 1) {
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this, MCPPreparationListActivity::class.java
            )
        } else {
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_updated_successfully",
                    R.string.data_updated_successfully
                ),
                this, MCPPreparationListActivity::class.java
            )
        }
    }

    override fun onBackPressed() {
        var intent = Intent(this, MeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

}