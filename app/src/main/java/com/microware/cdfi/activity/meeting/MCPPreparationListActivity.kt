package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.McpPreprationAdapter
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.ShgMcpViewmodel
import kotlinx.android.synthetic.main.activity_mcppreparation_list.*

class MCPPreparationListActivity : AppCompatActivity() {

    var validate: Validate? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    var lookupViewmodel: LookupViewmodel? = null
    var shgMcpViewmodel : ShgMcpViewmodel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mcppreparation_list)

        validate = Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        generateMeetingViewmodel = ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        shgMcpViewmodel = ViewModelProviders.of(this).get(ShgMcpViewmodel::class.java)

        replaceFragmenty(
            fragment = MeetingTopBarFragment(99),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        ivAddMcp.isEnabled =
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber) >= validate!!.RetriveSharepreferenceInt(MeetingSP.maxmeetingnumber)
        tblPriority.visibility = View.GONE
        ivAddMcp.setOnClickListener {
            validate!!.SaveSharepreferenceLong(MeetingSP.Memberid,0)
            validate!!.SaveSharepreferenceLong(MeetingSP.Mcpid,0)
            val intent = Intent(this, MCPPreparationActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        setLabelText()
        fillRecyclerView()

    }

    private fun fillRecyclerView() {
        val listData = shgMcpViewmodel!!.getShgMcpDatalist(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))

        val totdemand = shgMcpViewmodel!!.gettotaldemand(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        tv_amount.text = totdemand.toString()
        if (!listData.isNullOrEmpty()){
            rvList.layoutManager = LinearLayoutManager(this)
            val mcpPreprationAdapter = McpPreprationAdapter(this, listData)
            val isize: Int
            isize = listData.size
            val params: ViewGroup.LayoutParams = rvList.layoutParams
            val r = resources
            val px = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, resources.getDimension(R.dimen.spraying),
                r.displayMetrics
            )
            val hi = Math.round(px)
            var gridHeight = hi * isize
            params.height = gridHeight
            rvList.layoutParams = params
            rvList.adapter = mcpPreprationAdapter
        }

    }

    fun setMember(member_id: Long): String {
        var pos = ""
        var memberlist = generateMeetingViewmodel.getListDataMtgByMtgnum(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        if (!memberlist.isNullOrEmpty()) {
            for (i in memberlist.indices) {
                if (member_id == memberlist.get(i).mem_id)
                    pos = memberlist.get(i).member_name!!
            }
        }

        return pos
    }

    fun returnvalue(id:Int,flag:Int):String {
        val  listdata = lookupViewmodel!!.getlookup(flag, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
        return validate!!.returnlookupcodevalue(id,listdata)
    }

    fun setLabelText() {
        tvNameMember.text = LabelSet.getText(
            "name_of_member",
            R.string.name_of_member
        )
        tvLoanPurpose.text = LabelSet.getText(
            "loan_purpose",
            R.string.loan_purpose
        )
        tvLoanAmount.text = LabelSet.getText(
            "loan_amount",
            R.string.loan_amount
        )
        tvRequestDate.text = LabelSet.getText(
            "request_date",
            R.string.request_date
        )
        tvRequestValidUpto.text = LabelSet.getText(
            "request_valid_upto",
            R.string.request_valid_upto
        )
        tv_priority_no.text = LabelSet.getText(
            "priority_no",
            R.string.priority_no
        )

    }

    override fun onBackPressed() {
        var intent = Intent(this, MeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

}