package com.microware.cdfi.activity.meeting

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.MeetingRemarksAdapter
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import kotlinx.android.synthetic.main.layout_meetings_remarks_list.*
import kotlinx.android.synthetic.main.layout_rjection_remarks_list.*
import kotlinx.android.synthetic.main.layout_rjection_remarks_list.rvList
import kotlinx.android.synthetic.main.meeting_detail_item.*
import kotlinx.android.synthetic.main.repay_toolbar.*

class MeetingCheckerRemarks : AppCompatActivity() {

    var validate: Validate? = null
    var generateMeetingViewmodel: GenerateMeetingViewmodel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_meetings_remarks_list)

        validate = Validate(this)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)

        tv_code.text = validate!!.RetriveSharepreferenceLong(MeetingSP.Shgcode).toString()
        tv_nam.text = validate!!.RetriveSharepreferenceString(MeetingSP.ShgName)
        tv_date.text =
            validate!!.convertDatetime(validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate))
      /*  tv_mtgNum.text =
            validate!!.returnStringValue(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber).toString())*/
        tv_count.text = "" + validate!!.RetriveSharepreferenceInt(MeetingSP.MemberCount)
        tv_srno.text = LabelSet.getText("sr_no",R.string.sr_no)
        tv_meeting_num.text = LabelSet.getText("meeting_number",R.string.meeting_number)

        if(validate!!.RetriveSharepreferenceInt(MeetingSP.isCheckerRemark) == 1) {
            tv_remarks.text = LabelSet.getText("rejection_remarks",R.string.rejection_remarks)

            tv_title.text = LabelSet.getText("meeting_checker_remarks",R.string.meeting_checker_remarks)
        }else {
            tv_remarks.text = LabelSet.getText("remarks",R.string.remarks)

            tv_title.text = LabelSet.getText("queue_status",R.string.queue_status)
        }

        ic_Back.setOnClickListener {
            var intent = Intent(this, SHGMeetingListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }


        fillData()
    }

    override fun onBackPressed() {
        var intent = Intent(this, SHGMeetingListActivity::class.java)
        intent.flags= Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    private fun fillData() {
       var list = generateMeetingViewmodel!!.getMeetingStatusRemarks(
           validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
       )

                    if (!list.isNullOrEmpty() && list.size > 0 ){
                        rvList.layoutManager = LinearLayoutManager(this@MeetingCheckerRemarks)
                        rvList.adapter = MeetingRemarksAdapter(this@MeetingCheckerRemarks, list)
                    }


    }

}