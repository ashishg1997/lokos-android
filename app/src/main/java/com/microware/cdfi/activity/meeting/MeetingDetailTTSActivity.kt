package com.microware.cdfi.activity.meeting

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.speech.tts.UtteranceProgressListener
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.tabs.TabLayout
import com.microware.cdfi.R
import com.microware.cdfi.api.meetingmodel.LoanListModel
import com.microware.cdfi.api.meetingmodel.LoanRepaymentListModel
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.entity.DtmtgDetEntity
import com.microware.cdfi.fragment.ShgMeetingSummaryFragment
import com.microware.cdfi.fragment.ShgMemberSummaryFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.DtLoanMemberViewmodel
import com.microware.cdfi.viewModel.FinancialTransactionsMemViewmodel
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import com.microware.cdfi.viewModel.IncomeandExpenditureViewmodel
import kotlinx.android.synthetic.main.activity_meeting_detail_ttsactivity.*
import kotlinx.android.synthetic.main.activity_meetingmenu.*
import kotlinx.android.synthetic.main.meeting_detail_item.*
import kotlinx.android.synthetic.main.repay_toolbar.*
import java.util.*

class MeetingDetailTTSActivity : AppCompatActivity(), TextToSpeech.OnInitListener {

    var translatedData = ""
    private val REQ_TTS_STATUS_CHECK = 0
    private val TAG = "TTS Demo"
    private var mTts: TextToSpeech? = null
    private var uttCount = 0
    private var lastUtterance = -1
    private val params = HashMap<String, String>()
    var languageCode: String = ""
    var finalParagraphString: String? = null

    var generateMeetingViewmodel: GenerateMeetingViewmodel? = null
    var validate: Validate? = null
    var shgBankList: List<Cbo_bankEntity>? = null
    var incomeandExpenditureViewmodel: IncomeandExpenditureViewmodel? = null
    var financialTransactionsMemViewmodel: FinancialTransactionsMemViewmodel? = null
    var dtLoanMemberViewmodel: DtLoanMemberViewmodel? = null
    var SHG_name = ""
    var MeetingDate = ""
    var totalMember = ""
    var presentMember = ""
    var savingAmount = ""
    var cashBalance = ""
    var bankBalance = ""

    var sentence1 = ""
    var sentence2 = ""
    var sentence3 = ""
    var sentence4 = ""
    var sentence5 = ""
    var sentence6 = ""
    var sentence7 = ""
    var sentence8 = ""
    var sentence9 = ""
    var sentence10 = ""
    var sentence11 = ""
    var sentence12 = ""
    var sentence13 = ""
    var sentence14 = ""
    var sentence15 = ""
    var sentence16 = ""
    var sentence17 = ""
    var sentence18 = ""
    var sentence19 = ""
    var sentence20 = ""
    var sentence21 = ""
    var sentence22 = ""
    var sentence23 = ""

    var grp_investdata = ""
    var grp_borrowdata = ""
    var grp_receiptdata = ""
    var grp_repaymentdata = ""
    var grp_expendituredata = ""
    var grp_transaction = ""
    var finalParagraph = ""

    var summaryData = ""
    var attendanceData = ""
    var savingData = ""
    var distributeData = ""
    var repaymentData = ""
    var bankData = ""
    var cashData = ""
    var groupData = ""

    var grpInvest = ""
    var grpBorrow = ""
    var grpReceipt = ""
    var grpRepayment = ""
    var grpExpenditure = ""
    var meetno =  0

    var Userlist: List<DtmtgDetEntity>? = null
    var LoanDistributelist: List<LoanListModel>? = null
    var LoanRepaymentlist: List<LoanRepaymentListModel>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_meeting_detail_ttsactivity)

        validate = Validate(this)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        incomeandExpenditureViewmodel =
            ViewModelProviders.of(this).get(IncomeandExpenditureViewmodel::class.java)
        financialTransactionsMemViewmodel =
            ViewModelProviders.of(this).get(FinancialTransactionsMemViewmodel::class.java)
        dtLoanMemberViewmodel = ViewModelProviders.of(this).get(DtLoanMemberViewmodel::class.java)

        meetno = validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber)

        var intentData = intent
        SHG_name = intentData.getStringExtra("SHG_name").toString()
        MeetingDate = intentData.getStringExtra("MeetingDate").toString()
        totalMember = intentData.getStringExtra("totalMember").toString()
        presentMember = intentData.getStringExtra("presentMember").toString()
        savingAmount = intentData.getStringExtra("savingAmount").toString()
        cashBalance = intentData.getStringExtra("cashBalance").toString()
        bankBalance = intentData.getStringExtra("bankBalance").toString()

        grpInvest = intentData.getStringExtra("grpInvest").toString()
        grpBorrow = intentData.getStringExtra("grpBorrow").toString()
        grpReceipt = intentData.getStringExtra("grpReceipt").toString()
        grpRepayment = intentData.getStringExtra("grpRepayment").toString()
        grpExpenditure = intentData.getStringExtra("grpExpenditure").toString()

        tv_shg_name.text = SHG_name
        tv_shg_date.text = "Date : " + MeetingDate
        tv_title.text = "Meeting Information"

        val checkIntent = Intent()
        checkIntent.action = TextToSpeech.Engine.ACTION_CHECK_TTS_DATA
        startActivityForResult(checkIntent, REQ_TTS_STATUS_CHECK)
        languageCode = CDFIApplication.validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!

        getMeetingSummaryData()
        setupViewpager()

        ic_Back.setOnClickListener {
            onBackPressed()
        }

        tv_summary_play1.setOnClickListener {
            if (mTts != null) {
                mTts!!.stop()

                var testdata = finalParagraphString!!.replace("\n", "")
                if (testdata.isNullOrEmpty()) {
                    doSpeak(sentence21)
                    alertDialog("Overview", "\n\n" + sentence21)
                } else {
                    doSpeak(finalParagraphString!!)
                    finalParagraphString = finalParagraphString!!.replace("=", "")
                    finalParagraphString = finalParagraphString!!.replace("'", "")
                    alertDialog("Overview", finalParagraphString!!)
                }
            }

        }

    }

    private fun setupViewpager() {
        tabLayout_shgMeeting.addTab(tabLayout_shgMeeting.newTab().setText("Meeting Summary"))
        tabLayout_shgMeeting.addTab(tabLayout_shgMeeting.newTab().setText("Member Summary"))
        tabLayout_shgMeeting.tabGravity = TabLayout.GRAVITY_FILL

        val adapter = SHGMeetingTTSAdapter(
            applicationContext,
            supportFragmentManager,
            tabLayout_shgMeeting.tabCount
        )
        viewPager_shg_meeting.adapter = adapter

        viewPager_shg_meeting.addOnPageChangeListener(
            TabLayout.TabLayoutOnPageChangeListener(tabLayout_shgMeeting)
        )

        tabLayout_shgMeeting.addOnTabSelectedListener(object :
            TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager_shg_meeting.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }


    fun getMeetingSummaryData(): String {

        Userlist = generateMeetingViewmodel!!.getListDataMtgByMtgnum(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )

//        if (!Userlist.get(0).attendance.isNullOrEmpty() && !tv_compulasory_amount.text.toString()
//                .equals("0")
//        ) {
//            ll_summary_play.visibility = View.VISIBLE
//        } else {
//            ll_summary_play.visibility = View.GONE
//        }

        LoanDistributelist = generateMeetingViewmodel!!.getloanListDataMtgByMtgnum(
            validate!!.RetriveSharepreferenceInt(
                MeetingSP.currentmeetingnumber
            ),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )

        LoanRepaymentlist = generateMeetingViewmodel!!.getloanrepaymentList(
            validate!!.RetriveSharepreferenceInt(
                MeetingSP.currentmeetingnumber
            ),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )

        shgBankList = generateMeetingViewmodel!!.getBankdata(
            validate!!.RetriveSharepreferenceString(MeetingSP.SHGGUID)
        )

        var bankCount = shgBankList?.size!!
        bankCount = 0
        var bankPayment = "0"
        var bankReceipt = "0"

        if (bankCount > 0) {
            val code = returaccount()
            bankPayment = fillDebit(code)
            bankReceipt = fillCredit(code)
        } else {
            bankPayment = "0"
            bankReceipt = "0"
        }


        var languageCode = validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!

        finalParagraphString = MeetingTTS.GetSHGRegularParagraph(
            meetno,
            languageCode,
            applicationContext,
            Userlist!!,
            LoanDistributelist!!,
            LoanRepaymentlist!!,
            SHG_name,
            MeetingDate,
            totalMember,
            presentMember,
            savingAmount,
            cashBalance,
            bankBalance,
            setIncomeAmount(),
            setOutgoingAmount(),
            bankReceipt,
            bankPayment,
            grpInvest,
            grpBorrow,
            grpReceipt,
            grpRepayment,
            grpExpenditure
        )

        var splitData = finalParagraphString!!.split("@#@")
        sentence1 = splitData.get(0)
        sentence2 = splitData.get(1)
        sentence3 = splitData.get(2)
        sentence4 = splitData.get(3)
        sentence5 = splitData.get(4)
        sentence6 = splitData.get(5)
        sentence7 = splitData.get(6)
        sentence8 = splitData.get(7)
        sentence9 = splitData.get(8)
        sentence10 = splitData.get(9)
        sentence11 = splitData.get(10)
        sentence12 = splitData.get(11)
        sentence13 = splitData.get(12)
        sentence14 = splitData.get(13)
        sentence15 = splitData.get(14)
        sentence16 = splitData.get(15)
        sentence17 = splitData.get(16)
        sentence18 = splitData.get(17)
        sentence19 = splitData.get(18)
        sentence20 = splitData.get(19)
        sentence21 = splitData.get(20)
        sentence22 = splitData.get(21)
        sentence23 = splitData.get(22)
        grp_transaction = splitData.get(23)
        grp_investdata = splitData.get(24)
        grp_borrowdata = splitData.get(25)
        grp_receiptdata = splitData.get(26)
        grp_repaymentdata = splitData.get(27)
        grp_expendituredata = splitData.get(28)


        summaryData = "\n\n" + sentence1 + sentence2 + sentence3
        attendanceData = "\n\n" + sentence2 + sentence3 + sentence4 + sentence5
        savingData = "\n\n" + sentence6 + sentence7 + sentence8
        distributeData = "\n\n" + sentence9 + sentence10 + sentence11
        repaymentData = "\n\n" + sentence12 + sentence13
        bankData = "\n\n" + sentence14 + sentence17 + sentence18 + sentence19
        cashData = "\n\n" + sentence14 + sentence15 + sentence16 + sentence20
        groupData =
            "\n\n" + grp_transaction + grp_investdata + grp_borrowdata + grp_receiptdata + grp_repaymentdata + grp_expendituredata
        grp_transaction = "\n" + grp_transaction
//        finalParagraphString = "\n" + finalParagraphString!!.replace("@#@", "")
        finalParagraphString =
            "\n" + sentence1 + sentence2 + sentence3 + sentence4 + sentence5 + sentence6 + sentence7 + sentence8 + sentence9 + sentence10 + sentence11 + sentence12 + sentence13 + grp_transaction + grp_investdata + grp_borrowdata + grp_receiptdata + grp_repaymentdata + grp_expendituredata + "\n" + sentence14 + sentence15 + sentence16 + sentence17 + sentence18 + sentence19 + sentence20
        return finalParagraphString!!
    }


    private fun setOutgoingAmount(): String {

        val expenditureAmount = incomeandExpenditureViewmodel!!.getIncomeAndExpenditureAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            1, 2
        )

        var totloan = generateMeetingViewmodel!!.gettotloan(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        var totloanpaidgroup = generateMeetingViewmodel!!.gettotloanpaidgroup(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        val capitalPayments = financialTransactionsMemViewmodel!!.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            1, "OP"
        )
        var totcashwithdrawl = generateMeetingViewmodel!!.getsumwithdrwal(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        val amt =
            expenditureAmount + totloan + totloanpaidgroup + capitalPayments + totcashwithdrawl

        var amtData = ""
        amtData = amt.toString()
        return amtData
    }


    private fun setIncomeAmount(): String {

        val capitailIncome = financialTransactionsMemViewmodel!!.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            1, "OR"
        )

        var totcurrentcash = generateMeetingViewmodel!!.getupdatedcash(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )


        var totloanpaid = generateMeetingViewmodel!!.gettotloanpaid(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        var totloangroup = generateMeetingViewmodel!!.gettotloangroup(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )

        val totalPenalty = generateMeetingViewmodel!!.getTotalPenaltyByMtgNum(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )


        val incomeAmount = incomeandExpenditureViewmodel!!.getIncomeAndExpenditureAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            1, 1
        )

        var totalData = ""
        val total =
            capitailIncome + totcurrentcash + totloanpaid + totloangroup + incomeAmount + totalPenalty
        totalData = total.toString()
        return totalData
    }

    fun returaccount(): String {

        var pos = 1
        var id = ""

        if (!shgBankList.isNullOrEmpty()) {
            if (pos > 0) id =
                shgBankList!!.get(pos - 1).ifsc_code!!.dropLast(7) + shgBankList!!.get(pos - 1).account_no
        }
        return id
    }


    private fun fillDebit(code: String): String {
        val loanDisbursedAmt = dtLoanMemberViewmodel!!.getMemLoanDisbursedAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber), code
        )

        var capitailPayments = financialTransactionsMemViewmodel!!.getBankCodeAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            code, "OP"
        )

        var grptotloanpaid = generateMeetingViewmodel!!.getgrptotloanpaidinbank(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), code
        )
        var expenditureAmount =
            incomeandExpenditureViewmodel!!.getBankCodeIncomeAndExpenditureAmount(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                2, code
            )
        var transferincomeAmount =
            incomeandExpenditureViewmodel!!.getBankCodeIncomeAndExpendituretransferAmount(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                code, listOf(42), "OE"
            )

        var bankDrAmt =
            (loanDisbursedAmt + expenditureAmount + grptotloanpaid + transferincomeAmount + capitailPayments)
        var debitAmount = ""
        debitAmount = bankDrAmt.toString()

        return debitAmount
    }


    private fun fillCredit(code: String): String {

        var loanDisbursedAmt = generateMeetingViewmodel!!.getgrouptotloanamtinbank(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), code
        )

        var capitailIncome = financialTransactionsMemViewmodel!!.getBankCodeAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            code, "OR"
        )

        var incomeAmount = incomeandExpenditureViewmodel!!.getBankCodeIncomeAndExpenditureAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            1, code
        )
        var memtotloanpaid = generateMeetingViewmodel!!.getmemtotloanpaidinbank(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), code
        )

        var transferincomeAmount =
            incomeandExpenditureViewmodel!!.getBankCodeIncomeAndExpendituretransferAmount(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                code, listOf(41), "OI"
            )
        var bankCrAmt =
            (capitailIncome + incomeAmount + loanDisbursedAmt + memtotloanpaid + transferincomeAmount)

        var debitAmount = ""
        debitAmount = bankCrAmt.toString()

        return debitAmount
    }


    fun doSpeak(word: String) {
        val st = StringTokenizer(word.toString(), ",.=")
        while (st.hasMoreTokens()) {
            params[TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID] = uttCount++.toString()
            mTts!!.setPitch(1.toFloat())
            mTts!!.setSpeechRate(0.94.toFloat())
            mTts!!.speak(st.nextToken(), TextToSpeech.QUEUE_ADD, params)
//            mTts!!.setOnUtteranceCompletedListener(object :
//                TextToSpeech.OnUtteranceCompletedListener {
//                override fun onUtteranceCompleted(utteranceId: String?) {
//                    Log.v(TAG, "Got completed message for uttId: $utteranceId")
//                }
//            })

            mTts!!.setOnUtteranceProgressListener(object : UtteranceProgressListener() {
                override fun onStart(utteranceId: String) {

                    if (mTts != null) {
//                        tv_summary_play.text = "STOP"
                    }
                }

                override fun onDone(utteranceId: String) {

                    if (mTts != null && utteranceId.equals((uttCount - 1).toString())) {
                        mTts!!.stop()
                        tv_summary_play1.text = "PLAY"

//                        if (mTts == null){
//
//                        }
                    }
                }

                override fun onError(utteranceId: String) {
                    Log.i("TextToSpeech", "On Error")
                }
            })
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQ_TTS_STATUS_CHECK) {
            when (resultCode) {
                TextToSpeech.Engine.CHECK_VOICE_DATA_PASS -> {
                    mTts = TextToSpeech(this, this)
                }
                TextToSpeech.Engine.CHECK_VOICE_DATA_BAD_DATA, TextToSpeech.Engine.CHECK_VOICE_DATA_MISSING_DATA, TextToSpeech.Engine.CHECK_VOICE_DATA_MISSING_VOLUME -> {
                    val installIntent = Intent()
                    installIntent.action = TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA
                    startActivity(installIntent)
                }
                TextToSpeech.Engine.CHECK_VOICE_DATA_FAIL -> Log.e(TAG, "Got a failure.")
                else -> Log.e(TAG, "Got a failure.")
            }

            val available = data!!.getStringArrayListExtra("availableVoices")
            Log.v("languages count", available!!.size.toString())
            val iter: Iterator<String> = available.iterator()
            while (iter.hasNext()) {
                val lang = iter.next()
                val locale = Locale(lang)
            }
            val locales = Locale.getAvailableLocales()
            Log.v(TAG, "available locales:")
            for (i in locales.indices) Log.v(TAG, "locale: " + locales[i].displayName)
            val defloc = Locale.getDefault()
            Log.v(TAG, "current locale: " + defloc.displayName)
        }
    }

    override fun onInit(status: Int) {
        if (status == TextToSpeech.SUCCESS) {
            mTts!!.language = Locale(getDownloadLanguageISO(languageCode), "IND", "variant")
        }

    }


    override fun onPause() {
        super.onPause()
        if (mTts != null) mTts!!.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mTts != null) mTts!!.shutdown()
    }

    override fun onResume() {
        super.onResume()
        if (mTts != null) {
            mTts!!.stop()
            tv_summary_play1.text = "PLAY"
        }
    }


    private fun alertDialog(titleData: String, paragraphData: String) {
        val mDialogView: View = LayoutInflater.from(this@MeetingDetailTTSActivity)
            .inflate(R.layout.paragraph_text_ui, null)
        val mBuilder = android.app.AlertDialog.Builder(this@MeetingDetailTTSActivity)
            .setView(mDialogView)
        var mAlertDialog = mBuilder.show()
//        mAlertDialog!!.setCancelable(false)
        val title = mDialogView.findViewById<TextView>(R.id.title)
        val tv_paragraph = mDialogView.findViewById<TextView>(R.id.tv_paragraph)
        val iv_cancel = mDialogView.findViewById<ImageView>(R.id.iv_cancel)

        iv_cancel.setOnClickListener {
            if (mTts != null) {
                mTts!!.stop()
//                doSpeak(summaryData!!)
            }
            mAlertDialog!!.dismiss()
        }

        title.text = titleData
        tv_paragraph.text = paragraphData

        mAlertDialog!!.setOnDismissListener(object : DialogInterface.OnDismissListener {
            override fun onDismiss(dialog: DialogInterface?) {
                if (mTts != null) {
                    mTts!!.stop()
                }
                mAlertDialog.dismiss()
            }
        })
    }


    class SHGMeetingTTSAdapter(context: Context, fm: FragmentManager?, totalTabs: Int) :
        FragmentPagerAdapter(fm!!) {
        private val myContext: Context
        var totalTabs: Int

        // this is for fragment tabs
        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> {
                    ShgMeetingSummaryFragment()
                }
                1 -> {
                    ShgMemberSummaryFragment()
                }

                else -> ShgMeetingSummaryFragment()
            }
        }

        // this counts total number of tabs
        override fun getCount(): Int {
            return totalTabs
        }

        init {
            myContext = context
            this.totalTabs = totalTabs
        }
    }


}