package com.microware.cdfi.activity.meeting

import android.app.ProgressDialog
import android.content.Intent
import android.content.LocusId
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProviders
import com.google.gson.JsonObject
import com.microware.cdfi.R
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.api.response.MeetingResponse
import com.microware.cdfi.entity.DtmtgEntity
import com.microware.cdfi.entity.ShgFinancialTxnVouchersEntity
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_meetingmenu.*
import kotlinx.android.synthetic.main.cash_in_hand_detail_item.view.*
import kotlinx.android.synthetic.main.customealertdialogelogin.*
import kotlinx.android.synthetic.main.customealertdialogelogin.view.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.meeting_detail_item.*
import kotlinx.android.synthetic.main.repay_toolbar.*
import kotlinx.android.synthetic.main.repay_toolbar.ic_Back
import kotlinx.android.synthetic.main.repay_toolbar.tv_title
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MeetingMenuActivity : AppCompatActivity() {

    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: ApiInterface? = null
    var responseViewModel: ResponseViewModel? = null
    var validate: Validate? = null
    var generateMeetingViewmodel: GenerateMeetingViewmodel? = null
    var dtLoanViewmodel: DtLoanViewmodel? = null
    var financialTransactionsMemViewmodel: FinancialTransactionsMemViewmodel? = null
    var dtLoanTxnMemViewmodel: DtLoanTxnMemViewmodel? = null
    var dtLoanGpViewmodel: DtLoanGpViewmodel? = null
    var incomeandExpenditureViewmodel: IncomeandExpenditureViewmodel? = null
    var dtLoanGPTxnViewmodel: DtLoanGpTxnViewmodel? = null
    var dtLoanMemberViewmodel: DtLoanMemberViewmodel? = null
    var vouchersViewModel: VouchersViewModel? = null
    var vouchersEntity: ShgFinancialTxnVouchersEntity? = null
    lateinit var memSettlementViewmodel: MemSettlementViewmodel

    var mtglist: List<DtmtgEntity>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_meetingmenu)
        validate = Validate(this)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        responseViewModel = ViewModelProviders.of(this).get(ResponseViewModel::class.java)
        vouchersViewModel = ViewModelProviders.of(this).get(VouchersViewModel::class.java)
        memSettlementViewmodel = ViewModelProviders.of(this).get(MemSettlementViewmodel::class.java)


        setLabelText()
        if (validate!!.RetriveSharepreferenceInt(MeetingSP.isVoluntarySaving) == 1) {
            tbl_voluntary_saving.visibility = View.VISIBLE
        } else {
            tbl_voluntary_saving.visibility = View.GONE
        }
        tv_code.text = validate!!.RetriveSharepreferenceLong(MeetingSP.Shgcode).toString()
        tv_nam.text = validate!!.RetriveSharepreferenceString(MeetingSP.ShgName)
        tv_date.text =
            validate!!.convertDatetime(validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate))
        tv_count.text = "" + validate!!.RetriveSharepreferenceInt(MeetingSP.MemberCount)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        financialTransactionsMemViewmodel =
            ViewModelProviders.of(this).get(FinancialTransactionsMemViewmodel::class.java)
        dtLoanViewmodel = ViewModelProviders.of(this).get(DtLoanViewmodel::class.java)
        dtLoanTxnMemViewmodel = ViewModelProviders.of(this).get(DtLoanTxnMemViewmodel::class.java)
        dtLoanGpViewmodel = ViewModelProviders.of(this).get(DtLoanGpViewmodel::class.java)
        incomeandExpenditureViewmodel =
            ViewModelProviders.of(this).get(IncomeandExpenditureViewmodel::class.java)
        dtLoanGPTxnViewmodel = ViewModelProviders.of(this).get(DtLoanGpTxnViewmodel::class.java)
        dtLoanMemberViewmodel = ViewModelProviders.of(this).get(DtLoanMemberViewmodel::class.java)



        ic_Back.setOnClickListener {
            var intent = Intent(this, SHGMeetingListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        showSettlementStatus()
        btn_close.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(MeetingSP.Cashinhand) > 0) {
                createVouchers(
                    validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                    validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber)
                )
                CustomAlertCloseMeeting(
                    LabelSet.getText(
                        "are_u_sure_u_want_to_close",
                        R.string.are_u_sure_u_want_to_close
                    )
                )
            } else {
                validate!!.CustomAlert(LabelSet.getText("", R.string.verify_cash_in_hand), this)
            }

        }

        btn_delete.setOnClickListener {
            CustomAlertDeleteMeeting(
                LabelSet.getText(
                    "want_to_delete_meeting",
                    R.string.want_to_delete_meeting
                )
            )
        }

        tbl_attendence.setOnClickListener {
            var intent = Intent(this, AttendnceDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_compulasory_saving.setOnClickListener {
            var intent = Intent(this, CompulsorySavingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_voluntary_saving.setOnClickListener {
            var intent = Intent(this, VoluntorySavingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_withdrawl.setOnClickListener {
            var intent = Intent(this, WidthdrawalDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_settlement.setOnClickListener {
            var intent = Intent(this, SettlementListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_loandisbursement.setOnClickListener {
            var intent = Intent(this, LoanDisbursementActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_loanrepayment.setOnClickListener {
            var intent = Intent(this, RepaymentDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_Penality.setOnClickListener {
            var intent = Intent(this, PeneltyDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_loanRequest.setOnClickListener {
            var intent = Intent(this, LoanDemandSectionActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        tbl_loandisbursement.setOnClickListener {
            var intent = Intent(this, LoanDemandSummaryActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_group_investment.setOnClickListener {
            var intent = Intent(this, RegularMeetingInvestmentListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        /*tbl_group_loan_repaid.setOnClickListener {
            var intent = Intent(this, GroupLoanRepaidActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        tbl_group_loan_received.setOnClickListener {
            var intent = Intent(this, GroupLoanReceivedActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }*/

        tbl_bank.setOnClickListener {
            var intent = Intent(this, BankSummeryActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_cashBox.setOnClickListener {
            var intent = Intent(this, CashBoxActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        tbl_new_borrowing.setOnClickListener {
            var intent = Intent(this, GroupLoanlist::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        tbl_group_loan_repayment.setOnClickListener {
            var intent = Intent(this, GroupRepaymentDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_expenditure_and_payment.setOnClickListener {
            var intent = Intent(this, ExpenditurePaymentSummeryListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_recepient_and_income.setOnClickListener {
            var intent = Intent(this, GroupReceiptAndIncomeListActiity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        tbl_share_capital.setOnClickListener {
            var intent = Intent(this, ShareCapitalOtherReceiptListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_other_payments.setOnClickListener {
            var intent = Intent(this, ShareCapitalOtherPaymentListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_group_summarry.setOnClickListener {
            validate!!.SaveSharepreferenceInt(MeetingSP.buttonHide, 1)
            var intent = Intent(this, GroupMeetingSummery::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        tbl_member_summarry.setOnClickListener {
            var intent = Intent(this, MemberMeetingSummaryActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        ll_speaker.visibility = View.VISIBLE
        ll_speaker.setOnClickListener {
            var SHG_name = tv_nam.text.toString()
            var MeetingDate = tv_date.text.toString()
            var totalMember = tv_count.text.toString()
            var presentMember = tv_attendence_amount.text.toString()
            var savingAmount = tv_compulasory_amount.text.toString()
            var cashBalance = tv_cashBox_amount.text.toString()
            var bankBalance = tv_bank_amount.text.toString()

            var grpInvest = tv_group_investment_amount.text.toString()
            var grpBorrow = tv_new_borrowings_amount.text.toString()
            var grpReceipt = tv_recepient_and_income_amount.text.toString()
            var grpRepayment = tv_group_loan_repayment_amount.text.toString()
            var grpExpenditure = tv_expenditure_and_payment_amount.text.toString()

            var intent = Intent(this, MeetingDetailTTSActivity::class.java)
            intent.putExtra("SHG_name", SHG_name)
            intent.putExtra("MeetingDate", MeetingDate)
            intent.putExtra("totalMember", totalMember)
            intent.putExtra("presentMember", presentMember)
            intent.putExtra("savingAmount", savingAmount)
            intent.putExtra("cashBalance", cashBalance)
            intent.putExtra("bankBalance", bankBalance)
            intent.putExtra("grpInvest", grpInvest)
            intent.putExtra("grpBorrow", grpBorrow)
            intent.putExtra("grpReceipt", grpReceipt)
            intent.putExtra("grpRepayment", grpRepayment)
            intent.putExtra("grpExpenditure", grpExpenditure)
            startActivity(intent)
        }


        mtglist =
            generateMeetingViewmodel!!.getMtglist(validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        spin_meeting_no.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {

                    et_meeting_date.setText(
                        validate!!.convertDatetime(
                            validate!!.returnMeetingdate(
                                spin_meeting_no,
                                mtglist
                            )
                        )
                    )
                    validate!!.SaveSharepreferenceLong(
                        MeetingSP.CurrentMtgDate, validate!!.returnMeetingdate(
                            spin_meeting_no,
                            mtglist
                        )
                    )
                    validate!!.SaveSharepreferenceInt(
                        MeetingSP.currentmeetingnumber, validate!!.returnMeetingNum(
                            spin_meeting_no,
                            mtglist
                        )
                    )
                    validate!!.SaveSharepreferenceString(
                        MeetingSP.mtg_guid, validate!!.returnMeetingGuid(
                            spin_meeting_no,
                            mtglist
                        )
                    )
                    tv_date.text =
                        validate!!.convertDatetime(validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate))

                    btn_delete.isEnabled =
                        validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber) >= validate!!.RetriveSharepreferenceInt(
                            MeetingSP.maxmeetingnumber
                        )

                    updatecashinhand()
                    updatecashinBank()
                    setDataValues()
                    showSettlementStatus()
                } else {
                    et_meeting_date.setText("")

                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }
        validate!!.fillmeetingspinner(this, spin_meeting_no, mtglist)
        spin_meeting_no.setSelection(
            validate!!.returnmeetingpos(
                validate!!.RetriveSharepreferenceInt(
                    MeetingSP.currentmeetingnumber
                ), mtglist
            )
        )
        updateShgType()
        updatecashinhand()
        updatecashinBank()
        setDataValues()

    }

    fun setDataValues() {
        setSumValue(
            tv_attendence_amount, imgtick_attendence, generateMeetingViewmodel!!.getsumattendance(
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
            )
        )
        setSumValue(
            tv_compulasory_amount, imgtick_compulasory, generateMeetingViewmodel!!.getsumcomp(
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
            )
        )
        setSumValue(
            tv_voluntary_amount, imgtick_voluntary, generateMeetingViewmodel!!.getsumvol(
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
            )
        )
        setSumValue(tv_Penality_amount, imgtick_Penalty, 0)
        setSumValue(
            tv_loanrepayment_amount,
            imgtick_loanrepayment,
            dtLoanTxnMemViewmodel!!.getLoanRepaymentAmount(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber)
            )
        )
        setSumValue(
            tv_share_capital_amount,
            imgtick_share_caital,
            financialTransactionsMemViewmodel!!.getMemberIncomeAmount(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber), "OR"
            )
        )

        setSumValue(
            tv_other_payments_amount,
            imgtick_other_payments,
            financialTransactionsMemViewmodel!!.getMemberIncomeAmount(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber), "OP"
            )
        )
        setSumValue(
            tv_loandisbursement_amount,
            imgtick_loandisbursement,
            dtLoanMemberViewmodel!!.getMemberLoanDisbursedAmount(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber)
            )
        )
        setSumValue(
            tv_withdrawl_amount, imgtick_withdrawl, generateMeetingViewmodel!!.getsumwithdrwal(
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
            )
        )
//        setSumValue(tv_member_summary_amount, 0)
        setSumValue(
            tv_group_investment_amount, imgtick_group_investment,
            incomeandExpenditureViewmodel!!.getInvestmentAmount(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                "OE",
                listOf(25, 44, 45, 46)
            )
        )

        setSumValue(
            tv_recepient_and_income_amount,
            imgtick_receipt_income,
            incomeandExpenditureViewmodel!!.getMemberIncomeAndExpenditureAmount(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                1
            )
        )
        setSumValue(
            tv_group_loan_repayment_amount,
            imgtick_grouploanrepayment,
            dtLoanGPTxnViewmodel!!.getLoanRepaymentAmount(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber)
            )
        )
        setSumValue(
            tv_expenditure_and_payment_amount,
            imgtick_expenditure,
            incomeandExpenditureViewmodel!!.getMemberIncomeAndExpenditureAmountnotinauid(
                listOf(25, 44, 45, 46),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                2
            )
        )
//        setSumValue(tv_group_summary_amount, 0)
        setSumValue(
            tv_bank_amount, imgtick_bank, generateMeetingViewmodel!!.gettotalinbank(
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
            )
        )

        setSumValue(
            tv_Penality_amount, imgtick_Penalty, generateMeetingViewmodel!!.getTotalPenaltyByMtgNum(
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
            )
        )

        setSumValue(
            tv_cashBox_amount,
            imgtick_cashbox,
            validate!!.RetriveSharepreferenceInt(MeetingSP.Cashinhand)
        )
        setSumValue(
            tv_loanRequest_amount, imgtick_loanRequest, dtLoanViewmodel!!.gettotaldemand(
                validate!!.RetriveSharepreferenceInt(
                    MeetingSP.currentmeetingnumber
                ),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
            )
        )
        setSumValue(
            tv_settlement_amount,
            imgtick_settlement,
            memSettlementViewmodel.getTotalSettelMentAmt(
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
            )
        )
        setSumValue(
            tv_new_borrowings_amount,
            imgtick_new_borrowings,
            dtLoanGpViewmodel!!.gettotloangroupamount(

                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
            )
        )
    }

    fun setLabelText() {
        tv_attendence.text = LabelSet.getText(
            "attendane",
            R.string.attendane
        )
        tv_compulasory_saving.text = LabelSet.getText(
            "compulsory_saving",
            R.string.compulsory_saving
        )
        tv_voluntary_saving.text = LabelSet.getText(
            "voluntary_saving",
            R.string.voluntary_saving
        )
        tv_withdrawl.text = LabelSet.getText(
            "withdrawal",
            R.string.withdrawal
        )
        tv_loanRequest.text = LabelSet.getText(
            "loan_request",
            R.string.loan_request
        )
        tv_loandisbursement.text = LabelSet.getText(
            "loan_disbursement",
            R.string.loan_disbursement
        )
        tv_group_loan_repayment.text = LabelSet.getText(
            "loan_repayment",
            R.string.loan_repayment
        )
        tv_group_investment.text = LabelSet.getText(
            "group_investment",
            R.string.group_investment
        )
        tv_loanrepayment.text = LabelSet.getText(
            "loan_repayment",
            R.string.loan_repayment
        )
        tv_Penality.text = LabelSet.getText(
            "penalty",
            R.string.penalty
        )
        // tv_group_loan_repaid.setText(LabelSet.getText("group_loan_repaid",R.string.group_loan_repaid))
        // tv_group_loan_received.setText(LabelSet.getText("group_loan_received",R.string.group_loan_received))
        tv_share_capital.text = LabelSet.getText(
            "share_capital_other_receipt_label",
            R.string.share_capital_other_receipt_label
        )
        tv_bank.text = LabelSet.getText("bank", R.string.bank)
        tv_cashBox.text = LabelSet.getText(
            "cashBox",
            R.string.cashBox
        )
        //tv_summary.setText(LabelSet.getText("summary",R.string.summary))
        tv_expenditure_and_payment.text = LabelSet.getText(
            "expenditure_and_payment",
            R.string.expenditure_and_payment
        )
        tv_recepient_and_income.text = LabelSet.getText(
            "recepient_and_income1",
            R.string.recepient_and_income1
        )
        tv_member_transaction.text = LabelSet.getText(
            "member_transaction",
            R.string.member_transaction
        )
        tv_group_transaction.text = LabelSet.getText(
            "group_transaction",
            R.string.group_transaction
        )
        tv_other_payments.text = LabelSet.getText(
            "other_payment",
            R.string.other_payment
        )
        tv_title.text = LabelSet.getText(
            "meeting_menu",
            R.string.meeting_menu
        )
        btn_delete.text = LabelSet.getText(
            "delete_meeting",
            R.string.delete_meeting
        )
        btn_close.text = LabelSet.getText(
            "close_meeting",
            R.string.close_meeting
        )

    }

    fun setSumValue(textView: TextView, imageView: ImageView, value: Int) {
        if (value != null && value > 0) {
            textView.text = value.toString()
            imageView.visibility = View.VISIBLE
        } else {
            textView.text = "0"
            imageView.visibility = View.GONE
        }
    }


    override fun onBackPressed() {
        var intent = Intent(this, SHGMeetingListActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

    fun updateShgType() {
        tv_count.setBackgroundResource(R.drawable.item_countactive)
        iv_stauscolor.setBackgroundResource(R.drawable.item_countactive)
        if (validate!!.RetriveSharepreferenceInt(MeetingSP.SHGType) == 1) {
            iv_stauscolor.setImageResource(R.drawable.ic_womangroup)
            iv_stauscolor.visibility = View.INVISIBLE
        } else {
            iv_stauscolor.visibility = View.VISIBLE
            if (validate!!.RetriveSharepreferenceInt(MeetingSP.Tag) == 1) {
                iv_stauscolor.setImageResource(R.drawable.ic_pvtg)
            } else if (validate!!.RetriveSharepreferenceInt(MeetingSP.Tag) == 2) {
                iv_stauscolor.setImageResource(R.drawable.ic_elderly)
            } else if (validate!!.RetriveSharepreferenceInt(MeetingSP.Tag) == 3) {
                iv_stauscolor.setImageResource(R.drawable.ic_disable)
            } else if (validate!!.RetriveSharepreferenceInt(MeetingSP.Tag) == 99) {
                iv_stauscolor.setImageResource(R.drawable.ic_other)
            }
        }
    }

    fun CustomAlertDeleteMeeting(str: String) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str
        mDialogView.btn_yes.setOnClickListener {
            //            PartialSaveData()
            mAlertDialog.dismiss()

            DeleteMeeting()


        }
        mDialogView.btn_no.setOnClickListener {
            mAlertDialog.dismiss()

        }
    }

    fun CustomAlertCloseMeeting(str: String) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str
        mDialogView.btn_yes.setOnClickListener {
            //            PartialSaveData()
            mAlertDialog.dismiss()
            validate!!.SaveSharepreferenceInt(MeetingSP.buttonHide, 0)
            var intent = Intent(this, GroupMeetingSummery::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)


        }
        mDialogView.btn_no.setOnClickListener {
            mAlertDialog.dismiss()

        }
    }

    fun deleteMeetingData() {
        /*Table1*/
        generateMeetingViewmodel!!.deleteShg_MtgData(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        /*Table2*/
        generateMeetingViewmodel!!.deleteShgDetailData(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        /*Table3*/
        generateMeetingViewmodel!!.deleteFinancialTransactionMemberDetailData(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        /*Table4*/
        generateMeetingViewmodel!!.deleteGrpFinancialTxnDetailData(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        /*Table5*/
        generateMeetingViewmodel!!.deleteShgFinancialTxnData(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        /*Table6*/
        generateMeetingViewmodel!!.deleteMemberLoanTxnData(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        /*Table7*/
        generateMeetingViewmodel!!.deleteGroupLoanData(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        /*Table8*/
        generateMeetingViewmodel!!.deleteGroupLoanTxnData(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        /*Table9*/
        generateMeetingViewmodel!!.deleteMCPData(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        /*Table10*/
        generateMeetingViewmodel!!.deleteMemberLoan(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber)
        )
        /*Table11*/
        generateMeetingViewmodel!!.deleteShgLoanApplication(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        /*Table12*/
        generateMeetingViewmodel!!.deleteMemberScheduleByMtgGuid(
            validate!!.RetriveSharepreferenceString(
                MeetingSP.mtg_guid
            )!!
        )

        generateMeetingViewmodel!!.deleteMemberRepaidData(
            validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )

        generateMeetingViewmodel!!.deleteMemberSubinstallmentData(
            validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )

        /*Table13*/

        generateMeetingViewmodel!!.deleteGroupScheduleByMtgGuid(
            validate!!.RetriveSharepreferenceString(
                MeetingSP.mtg_guid
            )!!
        )

        generateMeetingViewmodel!!.deleteGroupRepaidData(
            validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )

        generateMeetingViewmodel!!.deleteGroupSubinstallmentData(
            validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )

        validate!!.CustomAlert(
            LabelSet.getText("", R.string.meeting_deleted_successfully),
            this,
            SHGMeetingListActivity::class.java
        )

    }

    fun updatecashinhand() {
        var totcurrentcash = generateMeetingViewmodel!!.getupdatedcash(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        var totcash = generateMeetingViewmodel!!.getupdatedpeningcash(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        var totloan = generateMeetingViewmodel!!.gettotloan(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        var totloanpaid = generateMeetingViewmodel!!.gettotloanpaid(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        var totloangroup = generateMeetingViewmodel!!.gettotloangroup(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        var totloanpaidgroup = generateMeetingViewmodel!!.gettotloanpaidgroup(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        val incomeAmount = incomeandExpenditureViewmodel!!.getIncomeAndExpenditureAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            1, 1
        )
        val capitailIncome = financialTransactionsMemViewmodel!!.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            1, "OR"
        )

        val capitailPayments = financialTransactionsMemViewmodel!!.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            1, "OP"
        )


        val expenditureAmount = incomeandExpenditureViewmodel!!.getIncomeAndExpenditureAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            1, 2
        )

        val totalPenalty = generateMeetingViewmodel!!.getTotalPenaltyByMtgNum(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )

        var totcashwithdrawl = generateMeetingViewmodel!!.getsumwithdrwal(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )

        var totalclosingbal =
            totcash + totcurrentcash + totloanpaid + totloangroup + incomeAmount + totalPenalty + capitailIncome - totcashwithdrawl - capitailPayments - totloanpaidgroup - totloan.toInt() - expenditureAmount
        validate!!.SaveSharepreferenceInt(MeetingSP.Cashinhand, totalclosingbal)
        if (validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber) == validate!!.RetriveSharepreferenceInt(
                MeetingSP.maxmeetingnumber
            )
        ) {
            generateMeetingViewmodel!!.updateclosingcash(
                totalclosingbal,
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
            )
        }
    }

    fun updatecashinBank() {
        var shgBankList =
            generateMeetingViewmodel!!.getBankdata(validate!!.RetriveSharepreferenceString(MeetingSP.SHGGUID))
        for (i in shgBankList.indices) {
            var accountno =
                shgBankList.get(i).ifsc_code!!.dropLast(7) + shgBankList.get(i).account_no

            val openningbal = generateMeetingViewmodel!!.getopenningbal(
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), accountno
            )
            val grploanDisbursedAmt = generateMeetingViewmodel!!.getgrouptotloanamtinbank(
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), accountno
            )

            val capitailIncome = financialTransactionsMemViewmodel!!.getBankCodeAmount(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                accountno, "OR"
            )

            val capitailPayments = financialTransactionsMemViewmodel!!.getBankCodeAmount(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                accountno, "OP"
            )

            val incomeAmount =
                incomeandExpenditureViewmodel!!.getBankCodeIncomeAndExpenditureAmount(
                    validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                    validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                    1, accountno
                )
            val memloanDisbursedAmt = dtLoanMemberViewmodel!!.getMemLoanDisbursedAmount(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber), accountno
            )
            var grptotloanpaid = generateMeetingViewmodel!!.getgrptotloanpaidinbank(
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), accountno
            )
            var memtotloanpaid = generateMeetingViewmodel!!.getmemtotloanpaidinbank(
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), accountno
            )
            val expenditureAmount =
                incomeandExpenditureViewmodel!!.getBankCodeIncomeAndExpenditureAmount(
                    validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                    validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                    2, accountno
                )

            var tot =
                openningbal + grploanDisbursedAmt + capitailIncome + incomeAmount + memtotloanpaid - capitailPayments - memloanDisbursedAmt - expenditureAmount - grptotloanpaid
            //  validate!!.SaveSharepreferenceInt(MeetingSP.CashinBank, tot)
            if (validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber) == validate!!.RetriveSharepreferenceInt(
                    MeetingSP.maxmeetingnumber
                )
            ) {
                generateMeetingViewmodel!!.updateclosingbalbank(
                    tot,
                    validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                    validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                    accountno
                )
            }
        }
    }

    fun DeleteMeeting() {

        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText("deleting_meeting_data", R.string.deleting_meeting_data)
        )

        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
        val token = validate!!.returnStringValue(
            AESEncryption.decrypt(
                validate!!.RetriveSharepreferenceString(AppSP.token),
                validate!!.RetriveSharepreferenceString(AppSP.userid)
            )
        )
        val shgId = validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        val mtgNum = validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber)
        val callCount = apiInterface?.deleteMeeting(
            token,
            user,
            shgId,
            mtgNum
        )

        callCount?.enqueue(object : Callback<MeetingResponse> {
            override fun onFailure(callCount: Call<MeetingResponse>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<MeetingResponse>,
                response: Response<MeetingResponse>
            ) {

                progressDialog.dismiss()
                if (response.isSuccessful) {

                    try {
                        var msg = response.body()?.result
                        if (msg!!.equals("Meeting delete successfully!!")) {
                            deleteMeetingData()
                        }


                        //       CustomAlert(text)
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        //                                    progressDialog.dismiss()
                    }

                } else {
                    progressDialog.dismiss()
                    var resCode = 0
                    var resMsg = ""
                    if (response.code() == 403) {

                        CustomAlertlogin()
                    } else {
                        if (response.code() == 500 || response.code() == 503) {
                            resCode = response.code()
                            resMsg = response.message()
                        } else {
                            var jsonObject1 =
                                JSONObject(response.errorBody()!!.source().readUtf8().toString())

                            resCode =
                                validate!!.returnIntegerValue(
                                    jsonObject1.optString("responseCode").toString()
                                )
                            resMsg = validate!!.returnStringValue(
                                jsonObject1.optString("responseMsg").toString()
                            )

                        }
                        if (resCode == 404 && resMsg.equals("record not found in database")) {

                            deleteMeetingData()

                        } else {
                            validate!!.CustomAlertMsg(
                                this@MeetingMenuActivity,
                                responseViewModel,
                                resCode,
                                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                                resMsg
                            )
                        }
                    }

                }


            }

        })
    }

    fun CustomAlertlogin() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogelogin, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        val password = validate!!.returnStringValue(
            AESEncryption.decrypt(
                validate!!.RetriveSharepreferenceString(AppSP.Password),
                validate!!.RetriveSharepreferenceString(AppSP.userid)
            )
        )
        mDialogView.etUsername.setText(validate!!.RetriveSharepreferenceString(AppSP.userid))
        mDialogView.etPassword.setText(password)
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_dialog_title2.text = LabelSet.getText("app_name", R.string.app_name)
        mDialogView.btn_login.text = LabelSet.getText("log_in", R.string.log_in)
        mDialogView.chk_showHide1.text = LabelSet.getText("show_password", R.string.show_password)
        mDialogView.btn_login.setOnClickListener {
            mAlertDialog.dismiss()
            importtoken(
                mDialogView.etUsername.text.toString(),
                validate!!.returnStringValue(
                    AESEncryption.encrypt(
                        validate!!.returnStringValue(
                            mDialogView.etPassword.text.toString()
                        ), mDialogView.etUsername.text.toString()
                    )
                )
            )
        }
        mAlertDialog.chk_showHide1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                mAlertDialog.etPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                mAlertDialog.etPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
        }

    }

    fun importtoken(user: String, pass: String) {
        var grant_type = "password"
        var userId = user
        var password = pass
        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText("authenticate_user", R.string.authenticate_user)
        )
        val callCount = apiInterface?.gettokenforce(
            grant_type,
            userId,
            password, validate!!.RetriveSharepreferenceInt(AppSP.State_Selected), 1,
            validate!!.RetriveSharepreferenceString(AppSP.Roleid)!!
        )
        callCount?.enqueue(object : Callback<JsonObject> {
            override fun onFailure(callCount: Call<JsonObject>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                //       Log.e("", response!!.body().toString())
                progressDialog.dismiss()


                if (response.isSuccessful) {

                    var jsonObject1: JSONObject? = null
                    try {
                        jsonObject1 = JSONObject(response.body().toString())

                        val auth = jsonObject1.get("accessToken").toString()
                        if (auth.trim().length > 0) {
                            validate!!.SaveSharepreferenceString(
                                AppSP.token,
                                validate!!.returnStringValue(
                                    AESEncryption.encrypt(
                                        auth, validate!!.RetriveSharepreferenceString(AppSP.userid)
                                    )
                                )
                            )
                        }
                        validate!!.CustomAlert(
                            LabelSet.getText(
                                "userauthenticatedsuccessfully",
                                R.string.userauthenticatedsuccessfully
                            ), this@MeetingMenuActivity
                        )
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        var jsonObject1 =
                            JSONObject(response.errorBody()!!.source().readUtf8().toString())

                        var resCode =
                            validate!!.returnIntegerValue(
                                jsonObject1.optString("responseCode").toString()
                            )
                        validate!!.CustomAlertMsg(
                            this@MeetingMenuActivity,
                            responseViewModel,
                            resCode,
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                            validate!!.returnStringValue(
                                jsonObject1.optString("responseMsg").toString()
                            )
                        )
                    }

                } else {
                    var jsonObject1 =
                        JSONObject(response.errorBody()!!.source().readUtf8().toString())

                    var resCode =
                        validate!!.returnIntegerValue(
                            jsonObject1.optString("responseCode").toString()
                        )
                    validate!!.CustomAlertMsg(
                        this@MeetingMenuActivity,
                        responseViewModel,
                        resCode,
                        validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                        validate!!.returnStringValue(
                            jsonObject1.optString("responseMsg").toString()
                        )
                    )
                }

            }

        })
    }

    private fun createVouchers(cboId: Long, mtgNum: Int) {
        var shgMtgDetailList = generateMeetingViewmodel!!.getMtgByMtgnum(cboId, mtgNum)
        var shgMemberloanList = dtLoanMemberViewmodel!!.getUploadListData(mtgNum, cboId)
        var grpLoanlist = dtLoanGpViewmodel!!.getListDataByMtgnum(mtgNum, cboId)
        var grpLoanTxnList = dtLoanGPTxnViewmodel!!.getListDataByMtgnum(mtgNum, cboId)
        var memberLoanTxnList = dtLoanTxnMemViewmodel!!.getMemberLoanTxnData(mtgNum, cboId)
        val shgFinanceTxnDetailGrpList =
            incomeandExpenditureViewmodel!!.getUploadListData(mtgNum, cboId)
        var memberFinancialTxnList =
            generateMeetingViewmodel!!.getMemberFinancialTxnData(mtgNum, cboId)
        if (!shgMtgDetailList.isNullOrEmpty()) {
            for (i in shgMtgDetailList.indices) {
                if (validate!!.returnIntegerValue(shgMtgDetailList[i].sav_comp.toString()) > 0) {
                    var voucherNum = generateVoucherNum("OR")
                    vouchersEntity = ShgFinancialTxnVouchersEntity(
                        cboId,
                        shgMtgDetailList.get(i).mem_id,
                        mtgNum,
                        1,
                        "",
                        mtgNum,
                        "",
                        "",
                        shgMtgDetailList.get(i).sav_comp,
                        validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
                        voucherNum,
                        1,
                        68,
                        0,
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        null
                    )
                    vouchersViewModel!!.insert(vouchersEntity!!)
                }
                if (validate!!.returnIntegerValue(shgMtgDetailList[i].sav_vol.toString()) > 0) {
                    var voucherNum = generateVoucherNum("OR")
                    vouchersEntity = ShgFinancialTxnVouchersEntity(
                        cboId,
                        shgMtgDetailList.get(i).mem_id,
                        mtgNum,
                        1,
                        "",
                        mtgNum,
                        "",
                        "",
                        shgMtgDetailList.get(i).sav_vol,
                        validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
                        voucherNum,
                        1,
                        69,
                        0,
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        null
                    )
                    vouchersViewModel!!.insert(vouchersEntity!!)
                }
            }
        }
        if (!shgMemberloanList.isNullOrEmpty()) {
            for (i in shgMemberloanList.indices) {
                /*validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType)== 0 && */
                if (validate!!.returnIntegerValue(shgMemberloanList.get(i).amount.toString()) > 0) {
                    var voucherNum = generateVoucherNum("OP")
                    vouchersEntity = ShgFinancialTxnVouchersEntity(
                        cboId,
                        shgMemberloanList.get(i).mem_id,
                        mtgNum,
                        shgMemberloanList.get(i).modepayment,
                        shgMemberloanList.get(i).bank_code,
                        mtgNum,
                        shgMemberloanList.get(i).transaction_no,
                        shgMemberloanList.get(i).transaction_no,
                        shgMemberloanList.get(i).amount,
                        validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
                        voucherNum,
                        2,
                        75,
                        0,
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        null
                    )
                    vouchersViewModel!!.insert(vouchersEntity!!)
                }
            }
        }

        if (!memberLoanTxnList.isNullOrEmpty()) {
            for (i in memberLoanTxnList.indices) {
                if (validate!!.returnIntegerValue(memberLoanTxnList.get(i).loan_paid.toString()) > 0 || validate!!.returnIntegerValue(
                        memberLoanTxnList.get(i).loan_paid_int.toString()
                    ) > 0
                ) {
                    var total_paid =
                        validate!!.returnIntegerValue(memberLoanTxnList.get(i).loan_paid.toString()) + validate!!.returnIntegerValue(
                            memberLoanTxnList.get(i).loan_paid_int.toString()
                        )
                    var voucherNum = generateVoucherNum("OR")
                    vouchersEntity = ShgFinancialTxnVouchersEntity(
                        cboId,
                        memberLoanTxnList.get(i).mem_id,
                        mtgNum,
                        memberLoanTxnList.get(i).mode_payment,
                        memberLoanTxnList.get(i).bank_code,
                        mtgNum,
                        memberLoanTxnList.get(i).transaction_no,
                        memberLoanTxnList.get(i).transaction_no,
                        total_paid,
                        validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
                        voucherNum,
                        1,
                        70,
                        0,
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        null
                    )
                    vouchersViewModel!!.insert(vouchersEntity!!)
                }
            }
        }

        if (!grpLoanlist.isNullOrEmpty()) {
            for (i in grpLoanlist.indices) {
                /*validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType)== 0 && */
                if (validate!!.returnIntegerValue(grpLoanlist.get(i).amount.toString()) > 0) {
                    var voucherNum = generateVoucherNum("OP")
                    vouchersEntity = ShgFinancialTxnVouchersEntity(
                        cboId,
                        0,
                        mtgNum,
                        grpLoanlist.get(i).mode_payment,
                        grpLoanlist.get(i).bank_code,
                        mtgNum,
                        grpLoanlist.get(i).transaction_no,
                        grpLoanlist.get(i).transaction_no,
                        grpLoanlist.get(i).amount,
                        validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
                        voucherNum,
                        1,
                        72,
                        0,
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        null
                    )
                    vouchersViewModel!!.insert(vouchersEntity!!)
                }
            }
        }
        if (!grpLoanTxnList.isNullOrEmpty()) {
            for (i in grpLoanTxnList.indices) {
                if (validate!!.returnIntegerValue(grpLoanTxnList.get(i).loan_paid.toString()) > 0 || validate!!.returnIntegerValue(
                        grpLoanTxnList.get(i).loan_paid_int.toString()
                    ) > 0
                ) {
                    var total_paid =
                        validate!!.returnIntegerValue(grpLoanTxnList.get(i).loan_paid.toString()) + validate!!.returnIntegerValue(
                            grpLoanTxnList.get(i).loan_paid_int.toString()
                        )
                    var voucherNum = generateVoucherNum("OE")
                    vouchersEntity = ShgFinancialTxnVouchersEntity(
                        cboId,
                        0,
                        mtgNum,
                        grpLoanTxnList.get(i).mode_payment,
                        grpLoanTxnList.get(i).bank_code,
                        mtgNum,
                        grpLoanTxnList.get(i).transaction_no,
                        grpLoanTxnList.get(i).transaction_no,
                        total_paid,
                        validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
                        voucherNum,
                        1,
                        76,
                        0,
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        null
                    )
                    vouchersViewModel!!.insert(vouchersEntity!!)
                }
            }
        }
        if (!shgFinanceTxnDetailGrpList.isNullOrEmpty()) {
            for (i in shgFinanceTxnDetailGrpList.indices) {
                if (validate!!.returnIntegerValue(shgFinanceTxnDetailGrpList.get(i).amount.toString()) > 0) {
                    var voucherNum =
                        generateVoucherNum(shgFinanceTxnDetailGrpList.get(i).type.toString())
                    vouchersEntity = ShgFinancialTxnVouchersEntity(
                        cboId,
                        0,
                        mtgNum,
                        shgFinanceTxnDetailGrpList.get(i).mode_payment,
                        shgFinanceTxnDetailGrpList.get(i).bank_code,
                        mtgNum,
                        shgFinanceTxnDetailGrpList.get(i).transaction_no,
                        shgFinanceTxnDetailGrpList.get(i).transaction_no,
                        shgFinanceTxnDetailGrpList.get(i).amount,
                        validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
                        voucherNum,
                        1,
                        shgFinanceTxnDetailGrpList.get(i).auid,
                        0,
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        null
                    )
                    vouchersViewModel!!.insert(vouchersEntity!!)
                }
            }
        }

        if (!memberFinancialTxnList.isNullOrEmpty()) {
            for (i in memberFinancialTxnList.indices) {
                if (validate!!.returnIntegerValue(memberFinancialTxnList.get(i).amount.toString()) > 0) {
                    var voucherNum =
                        generateVoucherNum(memberFinancialTxnList.get(i).type.toString())
                    vouchersEntity = ShgFinancialTxnVouchersEntity(
                        cboId,
                        memberFinancialTxnList.get(i).mem_id,
                        mtgNum,
                        memberFinancialTxnList.get(i).mode_payment,
                        memberFinancialTxnList.get(i).bank_code,
                        mtgNum,
                        memberFinancialTxnList.get(i).transaction_no,
                        memberFinancialTxnList.get(i).transaction_no,
                        memberFinancialTxnList.get(i).amount,
                        validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
                        voucherNum,
                        1,
                        memberFinancialTxnList.get(i).auid,
                        0,
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        null
                    )
                    vouchersViewModel!!.insert(vouchersEntity!!)
                }
            }
        }
    }

    fun generateVoucherNum(type: String): String {
        //validate!!.convertDatetime(validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate))
        var voucherNum: String? = null
        var num = validate!!.returnIntegerValue(
            vouchersViewModel!!.getMaxVoucherNum(
                validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate)
            ).toString()
        )
        var maxVoucherNum = (num + 1).toString()
        if (maxVoucherNum.trim().length == 1) {
            maxVoucherNum = "000" + maxVoucherNum
        } else if (maxVoucherNum.trim().length == 2) {
            maxVoucherNum = "00" + maxVoucherNum
        } else if (maxVoucherNum.trim().length == 3) {
            maxVoucherNum = "0" + maxVoucherNum
        }
        voucherNum =
            validate!!.convertDatetime(validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate)) + "-" + type + "-" + maxVoucherNum
        return voucherNum
    }

    fun showSettlementStatus() {
        var list = generateMeetingViewmodel!!.getListinactivemember(
            validate!!.RetriveSharepreferenceInt(
                MeetingSP.currentmeetingnumber
            ), validate!!.RetriveSharepreferenceLong(AppSP.shgid)
        )
        if (!list.isNullOrEmpty()) {
            tbl_settlement.visibility = View.VISIBLE
        } else {
            tbl_settlement.visibility = View.GONE
        }
    }
}