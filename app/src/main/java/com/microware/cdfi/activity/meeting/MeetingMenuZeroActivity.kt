package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.DtmtgEntity
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import kotlinx.android.synthetic.main.activity_meetingmenu_zero.*
import kotlinx.android.synthetic.main.meeting_detail_item_zero.*
import kotlinx.android.synthetic.main.repay_toolbar_zero.*

class MeetingMenuZeroActivity : AppCompatActivity() {
    var validate: Validate? = null
    var generateMeetingViewmodel: GenerateMeetingViewmodel? = null
    var mtglist: List<DtmtgEntity>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_meetingmenu_zero)
        validate = Validate(this)
        setLabelText()
        tv_code.text = validate!!.RetriveSharepreferenceLong(MeetingSP.shgid).toString()
        tv_nam.text = validate!!.RetriveSharepreferenceString(MeetingSP.ShgName)
        tv_date.text =
            validate!!.convertDatetime(validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate))
        tv_count.text = "" + validate!!.RetriveSharepreferenceInt(MeetingSP.MemberCount)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        ic_Back.setOnClickListener {
            var intent = Intent(this, SHGMeetingListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_attendence.setOnClickListener {
           // var intent = Intent(this, AttendnceDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_member_saving.setOnClickListener {
          //  var intent = Intent(this, CompulsorySavingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_member_closed_loan.setOnClickListener {
            //var intent = Intent(this, VoluntorySavingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_member_active_loan.setOnClickListener {
           // var intent = Intent(this, WidthdrawalDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_member_share_capital.setOnClickListener {
          //  var intent = Intent(this, LoanDisbursementActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_membership_fee.setOnClickListener {
          //  var intent = Intent(this, RepaymentActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_group_investment.setOnClickListener {
          //  var intent = Intent(this, PeneltyDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_group_loan_summary.setOnClickListener {
           // var intent = Intent(this, GroupLoanRepaidActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        tbl_group_cash_balance.setOnClickListener {
          //  var intent = Intent(this, GroupLoanReceivedActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        tbl_group_bank_balance.setOnClickListener {
          //  var intent = Intent(this, BankGroupTransactionActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_group_income.setOnClickListener {
          //  var intent = Intent(this, CashBoxActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_group_expenditure.setOnClickListener {
           // var intent = Intent(this, ExpenditurePaymentActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

    }

    fun setLabelText() {
        tv_attendence.text = LabelSet.getText(
            "attendane",
            R.string.attendane
        )
        tv_member_saving.text = LabelSet.getText(
            "member_saving",
            R.string.member_saving
        )
        tv_member_closed_loan.text = LabelSet.getText(
            "member_closed_loan",
            R.string.member_closed_loan
        )
        tv_member_active_loan.text = LabelSet.getText(
            "member_active_loan",
            R.string.member_active_loan
        )
        tv_member_share_capital.text = LabelSet.getText(
            "member_share_capital",
            R.string.member_share_capital
        )
        tv_membership_fee.text = LabelSet.getText(
            "Membership_fee",
            R.string.Membership_fee
        )
        tv_group_investment.text = LabelSet.getText(
            "group_investment",
            R.string.group_investment
        )
        tv_group_loan_summary.text = LabelSet.getText(
            "group_loan_summary",
            R.string.group_loan_summary
        )
        tv_group_cash_balance.text = LabelSet.getText(
            "group_cash_balance",
            R.string.group_cash_balance
        )
        tv_group_bank_balance.text = LabelSet.getText(
            "group_bank_balance",
            R.string.group_bank_balance
        )
        tv_group_income.text = LabelSet.getText(
            "group_income",
            R.string.group_income
        )
        tv_group_expenditure.text = LabelSet.getText(
            "group_expenditure",
            R.string.group_expenditure
        )
        tv_title.text = LabelSet.getText(
            "cut_off_menu",
            R.string.cut_off_menu
        )

    }

    override fun onBackPressed() {
        var intent = Intent(this, SHGMeetingListActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }
}