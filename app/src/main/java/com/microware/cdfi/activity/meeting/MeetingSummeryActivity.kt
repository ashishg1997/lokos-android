package com.microware.cdfi.activity.meeting

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.replaceFragmenty
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_meeting_summery.*

class MeetingSummeryActivity : AppCompatActivity() {

    var shgViewModel: SHGViewmodel? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    lateinit var financialTransactionsMemViewmodel: FinancialTransactionsMemViewmodel
    var dtLoanMemberViewmodel: DtLoanMemberViewmodel? = null
    var dtLoanGpViewmodel: DtLoanGpViewmodel? = null
    var dtLoanTxnMemViewmodel: DtLoanTxnMemViewmodel? = null
    var dtLoanGPTxnViewmodel: DtLoanGpTxnViewmodel? = null
    var incomeandExpenditureViewmodel: IncomeandExpenditureViewmodel? = null
    var validate: Validate? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_meeting_summery)

        shgViewModel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        incomeandExpenditureViewmodel =
            ViewModelProviders.of(this).get(IncomeandExpenditureViewmodel::class.java)
        financialTransactionsMemViewmodel =
            ViewModelProviders.of(this).get(FinancialTransactionsMemViewmodel::class.java)
        dtLoanMemberViewmodel = ViewModelProviders.of(this).get(DtLoanMemberViewmodel::class.java)
        dtLoanGpViewmodel = ViewModelProviders.of(this).get(DtLoanGpViewmodel::class.java)
        dtLoanTxnMemViewmodel = ViewModelProviders.of(this).get(DtLoanTxnMemViewmodel::class.java)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        dtLoanGPTxnViewmodel = ViewModelProviders.of(this).get(DtLoanGpTxnViewmodel::class.java)
        validate = Validate(this)

        replaceFragmenty(
            fragment = MeetingTopBarFragment(11),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        btn_sendApproval.isEnabled =
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber) >= validate!!.RetriveSharepreferenceInt(MeetingSP.maxmeetingnumber)

        updatecashinhand()
        updateTotalSaving()
        updateatBank()
        setIncomeAmount()
        setOutgoingAmount()
        setLoanDisbursedAmount()
        setLabelText()
        calculateAttendencePercentage()
        calculateRepaymentPercentage()
        setRepaymentAmount()

    }

    private fun setLabelText() {
        tv_total_saving.text = LabelSet.getText(
            "total_saving",
            R.string.total_saving
        )
        tv_total_repayment.text = LabelSet.getText(
            "total_repayments",
            R.string.total_repayments
        )
        total_amount_disbursed.text = LabelSet.getText(
            "total_amount_disbursed",
            R.string.total_amount_disbursed
        )
        tv_total_incoming.text = LabelSet.getText(
            "total_incoming_amount",
            R.string.total_incoming_amount
        )
        tv_total_outcome.text = LabelSet.getText(
            "total_outcome_amount",
            R.string.total_outcome_amount
        )
        tv_cash_in_hand.text = LabelSet.getText(
            "cash_in_hand",
            R.string.cash_in_hand
        )
        tv_cash_at_bank.text = LabelSet.getText(
            "cash_at_bank",
            R.string.cash_at_bank
        )
        tvInsight.text = LabelSet.getText(
            "insights",
            R.string.insights
        )
        tvMemberAttendence.text = LabelSet.getText(
            "member_attendance",
            R.string.member_attendance
        )
    }

    private fun updateatBank() {

        val bankAmt = generateMeetingViewmodel.gettotalinbank(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        tv_amount_cash_at_bank.text = "₹ $bankAmt"

    }

    private fun calculateRepaymentPercentage() {

    }

    private fun setRepaymentAmount() {
        val memberTxnAmt = dtLoanTxnMemViewmodel!!.getLoanRepaymentAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber)
        )
        val txnGPLoanRepaymentAmt = dtLoanGPTxnViewmodel!!.getLoanRepaymentAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber)
        )

        val totalTxnAmt = memberTxnAmt+txnGPLoanRepaymentAmt

        tv_amount_total_repayments.text = "₹ $totalTxnAmt"
    }

    private fun calculateAttendencePercentage() {
        val TotalPresent = generateMeetingViewmodel.getTotalPresentAndAbsent(
            "1",
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )

        val TotalMember = generateMeetingViewmodel.getTotalMember(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )

        val calpercent: Float = (((TotalPresent.toFloat() / TotalMember.toFloat()) * 100).toFloat())
        tvAttendencePercntage.text = "${calpercent.toInt()} %"
        pbAttendence.progress = calpercent.toInt()
    }

    private fun setLoanDisbursedAmount() {
        val loanDisbursedAmt = dtLoanMemberViewmodel!!.getMemberLoanDisbursedAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber)
        )
        /*val GrouploanDisbursedAmt = dtLoanGpViewmodel!!.gettotloangroupamount(

            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )*/
        var total = loanDisbursedAmt
        tv_amount_total_disbursed.text = "₹ $total"
    }

    private fun setOutgoingAmount() {

        val incomeAmount = incomeandExpenditureViewmodel!!.getMemberIncomeAndExpenditureAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            2)

        val capitailPayments = financialTransactionsMemViewmodel.getMemberIncomeAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),"OP")

        val txnGPLoanRepaymentAmt = dtLoanGPTxnViewmodel!!.getLoanRepaymentAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber)
        )
        val loanDisbursedAmt = dtLoanMemberViewmodel!!.getMemberLoanDisbursedAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber)
        )
        val amount = incomeAmount  + txnGPLoanRepaymentAmt+loanDisbursedAmt + capitailPayments

        tv_amout_total_outcome_amount.text = "₹ $amount"
    }

    private fun setIncomeAmount() {
        val capitailIncome = financialTransactionsMemViewmodel.getMemberIncomeAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),"OR")

        val volamt = generateMeetingViewmodel.getsumvol(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )

        val cumamt = generateMeetingViewmodel.getsumcomp(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )


        val incomeAmount = incomeandExpenditureViewmodel!!.getMemberIncomeAndExpenditureAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            1
        )
        val GrouploanDisbursedAmt = dtLoanGpViewmodel!!.gettotloangroupamount(

            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        val txnMemLoanRepaymentAmt = dtLoanTxnMemViewmodel!!.getLoanRepaymentAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber)
        )

        val totalPenalty = generateMeetingViewmodel.getTotalPenaltyByMtgNum(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))

        val total =
            (capitailIncome + volamt + cumamt + GrouploanDisbursedAmt + incomeAmount + txnMemLoanRepaymentAmt + totalPenalty )
        tv_amount_total_incoming.text = "₹ $total"

    }

    private fun updateTotalSaving() {
        val volamt = generateMeetingViewmodel.getsumvol(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )

        val cumamt = generateMeetingViewmodel.getsumcomp(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )

        val totalPenalty = generateMeetingViewmodel.getTotalPenaltyByMtgNum(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))

        val totcash = volamt + cumamt + totalPenalty

        tv_amount_total_saving.text = "₹ $totcash"
    }

    private fun updatecashinhand() {

        val totcash = validate!!.RetriveSharepreferenceInt(MeetingSP.Cashinhand)

        tv_amount_cash_in_hand.text ="₹ $totcash"

    }

}