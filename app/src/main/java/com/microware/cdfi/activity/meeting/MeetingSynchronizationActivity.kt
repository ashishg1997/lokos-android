package com.microware.cdfi.activity.meeting

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.microware.cdfi.R
import com.microware.cdfi.activity.MainActivityDrawer
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.api.meetingdatamodel.*
import com.microware.cdfi.api.meetingdatamodel.UploadMeetingData
import com.microware.cdfi.api.meetinguploadmodel.*
import com.microware.cdfi.api.response.SHGResponse
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.*
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_meeting_sync.*
import kotlinx.android.synthetic.main.customealertdialogelogin.*
import kotlinx.android.synthetic.main.customealertdialogelogin.etPassword
import kotlinx.android.synthetic.main.dialoge.view.*
import kotlinx.android.synthetic.main.customealertdialogelogin.view.*
import kotlinx.android.synthetic.main.sync_toolbar.*
import okhttp3.MediaType
import okhttp3.RequestBody
import org.apache.http.conn.ConnectTimeoutException
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException

class MeetingSynchronizationActivity : AppCompatActivity() {
    lateinit var vouchersViewModel: VouchersViewModel
    internal lateinit var progressDialog: ProgressDialog
    var  apiInterface: ApiInterface? = null
    var validate: Validate? = null
    var generateMeetingViewmodel: GenerateMeetingViewmodel? = null
    var financialTransactionsMemViewmodel: FinancialTransactionsMemViewmodel? = null
    var dtLoanGpViewmodel: DtLoanGpViewmodel? = null
    var dtMtgGrpLoanScheduleViewmodel: DtMtgGrpLoanScheduleViewmodel? = null
    var dtLoanMemberScheduleViewmodel: DtLoanMemberScheduleViewmodel? = null
    var dtLoanGpTxnViewmodel: DtLoanGpTxnViewmodel? = null
    var incomeandExpenditureViewmodel: IncomeandExpenditureViewmodel? = null
    var dtLoanViewmodel: DtLoanViewmodel? = null
    var dtLoanMemberViewmodel: DtLoanMemberViewmodel? = null
    var dtLoanTxnMemViewmodel: DtLoanTxnMemViewmodel? = null

    var transactionViewmodel: TransactionViewmodel? = null
    var responseViewModel: ResponseViewModel? = null
    var mcpViewmodel: ShgMcpViewmodel? = null
    lateinit var shgMemberLoanSettelmentViewmodel : MemSettlementViewmodel
    var iDownload = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_meeting_sync)

        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()

        validate = Validate(this)
        //setLabelText()
        tv_title.text = LabelSet.getText("",R.string.meeting_sync)
        vouchersViewModel = ViewModelProviders.of(this).get(VouchersViewModel::class.java)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        financialTransactionsMemViewmodel =
            ViewModelProviders.of(this).get(FinancialTransactionsMemViewmodel::class.java)
        dtLoanGpViewmodel = ViewModelProviders.of(this).get(DtLoanGpViewmodel::class.java)
        dtMtgGrpLoanScheduleViewmodel =
            ViewModelProviders.of(this).get(DtMtgGrpLoanScheduleViewmodel::class.java)
        dtLoanGpTxnViewmodel = ViewModelProviders.of(this).get(DtLoanGpTxnViewmodel::class.java)
        dtLoanMemberScheduleViewmodel =
            ViewModelProviders.of(this).get(DtLoanMemberScheduleViewmodel::class.java)
        incomeandExpenditureViewmodel =
            ViewModelProviders.of(this).get(IncomeandExpenditureViewmodel::class.java)
        dtLoanViewmodel = ViewModelProviders.of(this).get(DtLoanViewmodel::class.java)
        dtLoanMemberViewmodel = ViewModelProviders.of(this).get(DtLoanMemberViewmodel::class.java)
        dtLoanTxnMemViewmodel = ViewModelProviders.of(this).get(DtLoanTxnMemViewmodel::class.java)
        transactionViewmodel = ViewModelProviders.of(this).get(TransactionViewmodel::class.java)
        mcpViewmodel = ViewModelProviders.of(this).get(ShgMcpViewmodel::class.java)
        responseViewModel = ViewModelProviders.of(this).get(ResponseViewModel::class.java)
        shgMemberLoanSettelmentViewmodel = ViewModelProviders.of(this).get(MemSettlementViewmodel::class.java)

        IvHome.setOnClickListener {
            val i = Intent(this, MainActivityDrawer::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)
            finish()
        }

        val shgMeetinglist = generateMeetingViewmodel!!.getShgMeetingList()

        tv_upload_meeting.text = LabelSet.getText(
            "upload_meeting",
            R.string.upload_meeting
        ) + " (" + shgMeetinglist.size + ")"

        layout_upload_meeting.setOnClickListener {
            if (isNetworkConnected()) {
                val shgMeetinglist = generateMeetingViewmodel!!.getShgMeetingList()
                if (!shgMeetinglist.isNullOrEmpty()) {
                    exportDatawithimage(shgMeetinglist)
                    //  exportData(shgMeetinglist)
                } else {
                    val text = LabelSet.getText("nothing_upload", R.string.nothing_upload)
                    validate!!.CustomAlert(text, this@MeetingSynchronizationActivity)
                }
            } else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "no_internet_msg",
                        R.string.no_internet_msg
                    ), this
                )
            }
        }

        layout_download_meeting.setOnClickListener {
            if (isNetworkConnected()) {

                importuploadstatus()

            } else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "no_internet_msg",
                        R.string.no_internet_msg
                    ), this
                )
            }
        }

        layout_download_meetingstatus.setOnClickListener {
            if (isNetworkConnected()) {

                importMeeting_Approval_Rejection_status()
            } else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "no_internet_msg",
                        R.string.no_internet_msg
                    ), this
                )
            }
        }


    }

    private fun isNetworkConnected(): Boolean {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager //1
        val networkInfo = connectivityManager.activeNetworkInfo //2
        return networkInfo != null && networkInfo.isConnected //3
    }

    fun exportDatawithimage(shgMeetinglist: List<DtmtgEntity>) {
        progressDialog = ProgressDialog.show(
            this, LabelSet.getText("app_name", R.string.app_name),
            LabelSet.getText("DataUploading", R.string.DataUploading)
        )
        progressDialog.setIcon(this.getDrawable(R.drawable.appicon))
        var idownload = 0
        var msg = ""
        var code = 0
        object : Thread() {

            //@SuppressLint("NewApi")
            override fun run() {
                try {
                    for (j in shgMeetinglist.indices) {
                        var transactionid = validate!!.randomtransactionno()
                        var shgMeetinglistentity = shgMeetinglist.get(j)
                        val shgFinanceTxnDetailGrpList =
                            incomeandExpenditureViewmodel!!.getUploadListData(
                                shgMeetinglist[j].mtg_no,
                                shgMeetinglist[j].cbo_id
                            )
                        val shgFinanceTxnList = generateMeetingViewmodel!!.getUploadListData1(
                            shgMeetinglist[j].mtg_no,
                            shgMeetinglist[j].cbo_id
                        )
                        val dtLoanGpList = dtLoanGpViewmodel!!.getUploadGroupLoanList(
                            shgMeetinglist[j].cbo_id)
                        val dtLoanGpTxnList = dtLoanGpTxnViewmodel!!.getUploadListData(
                            shgMeetinglist[j].mtg_no,
                            shgMeetinglist[j].cbo_id
                        )

                        val shgMcpList = mcpViewmodel!!.getUploadListData(
                            shgMeetinglist[j].mtg_no,
                            shgMeetinglist[j].cbo_id
                        )
                        val shgvoucherList = vouchersViewModel.getVoucherUploadList(
                            shgMeetinglist[j].mtg_no,
                            shgMeetinglist[j].cbo_id
                        )
                       /*NEw Added */
                        val shgMeetingdetailList = generateMeetingViewmodel!!.getUploadListData(
                            shgMeetinglist[j].mtg_no,
                            shgMeetinglist[j].cbo_id
                        )
                        val shgMemberloanList = dtLoanMemberViewmodel!!.getUploadMemberLoanList(
                            shgMeetinglist[j].cbo_id)

                        val shgLoanApplicationList1 = dtLoanViewmodel!!.getUploadListData(
                            shgMeetinglist[j].mtg_no, shgMeetinglist[j].cbo_id
                        )

                        var shgGrpLoan: List<ShgGroupLoanUploadData>? = null
                        shgGrpLoan = MappingData.returnGrpLoanObj(dtLoanGpList)

                        var shgLoanApplicationdata: List<ShgLoanApplicationListUploadData>? = null
                        shgLoanApplicationdata = MappingData.returnloanApplicationObj(shgLoanApplicationList1)

                        var memberLoanList: List<ShgMemberLoanListUploadData>? = null
                        memberLoanList = MappingData.returnmemLoanListObj(shgMemberloanList)

                        /*Mcp */
                        /*  var mcpList : List<ShgMcpData>? = null
                          mcpList = MappingData.returnmcpObj(shgMcpList)*/

                        var mcpListmodel: List<ShgMcpData>? = null
                        mcpListmodel = MappingData.returnmcpObj(shgMcpList)

                        var shgUploadModel: UploadMeetingData = UploadMeetingData()
                        shgUploadModel = MappingData.returnmeetingObj(shgMeetinglistentity)

                        var uploaddatalist = UploadData(
                            transactionid,
                            shgUploadModel, shgGrpLoan, memberLoanList, shgLoanApplicationdata,
                            mcpListmodel)

                        var uploaddataModel: UploadData = UploadData()
                        uploaddataModel = MappingData.returnUploadDataObj(uploaddatalist)

                        var shgmeetingdetailListmodel: List<ShgMeetingDetailsUploadListData>? = null
                        shgmeetingdetailListmodel = MappingData.returnmeetingDetailObj(shgMeetingdetailList)

                        var shgfinancetxnList: List<ShgFinanceTransactionUploadListData>? = null
                        shgfinancetxnList = MappingData.returnShgFinanceTransactionListObj(shgFinanceTxnList)

                        var shgfinancetxnGrpList: List<ShgFinanceTransactionDetailGroupUploadListData>? = null
                        shgfinancetxnGrpList = MappingData.returnShgFinanceTransactionGrpListObj(shgFinanceTxnDetailGrpList)

                        var shgGrpLoanTxn: List<ShgGroupLoanTransactionUploadData>? = null
                        shgGrpLoanTxn = MappingData.returnGrpLoanTxnObj(dtLoanGpTxnList)
                        var shgFinanceTransactionVouchers : List<ShgFinancialTxnVouchersDataModel>? = null
                        shgFinanceTransactionVouchers = MappingData.returnShgFinTxnVoucherTxnObj(shgvoucherList)

                        shgUploadModel.shgMeetingDetailsList = shgmeetingdetailListmodel
                        shgUploadModel.shgFinanceTransactionList = shgfinancetxnList
                        shgUploadModel.shgFinanceTransactionDetailGroupList = shgfinancetxnGrpList
                        shgUploadModel.shgGroupLoanTransactionList = shgGrpLoanTxn
                        shgUploadModel.shgFinanceTransactionVouchersList = shgFinanceTransactionVouchers


                        for (i in 0 until  shgMeetingdetailList.size) {
                            val meetingDetailModel: ShgMeetingDetailsUploadListData =
                                shgmeetingdetailListmodel.get(i)

                            var shgFinanceTxnDetailMemberList =
                                financialTransactionsMemViewmodel!!.getUploadData(
                                    shgMeetingdetailList[i].mtg_no,
                                    shgMeetingdetailList[i].cbo_id!!,
                                    shgMeetingdetailList[i].mem_id
                                )

                            val shgMemberLoanTxnList1 = dtLoanTxnMemViewmodel!!.getUploadListData(
                                shgMeetingdetailList[i].mtg_no,
                                shgMeetingdetailList[i].cbo_id!!,
                                shgMeetingdetailList[i].mem_id
                            )
                            val shgMemSettelMentList = shgMemberLoanSettelmentViewmodel.getSettlementdata(
                                shgMeetingdetailList[i].mem_id,
                                shgMeetingdetailList[i].mtg_no
                            )
                            var settelmentmodelList : List<ShgMemberSettlementListDataModel>? = null
                            settelmentmodelList = MappingData.returnmemSettelmemtObj(shgMemSettelMentList)

                            meetingDetailModel.shgMemberSettlementList = settelmentmodelList
                            var financeTxnmemDetail: List<ShgFinanceTransactionDetailMemberUploadData>?
                            financeTxnmemDetail =
                                MappingData.returnmemfintxndetailObj(shgFinanceTxnDetailMemberList)

                            var memberLoanTxn: List<ShgMemberLoanTransactionUploadListData>? = null
                            memberLoanTxn = MappingData.returnmemloanTxnObj(shgMemberLoanTxnList1)

                            meetingDetailModel.shgFinanceTransactionDetailMemberList = financeTxnmemDetail
                            meetingDetailModel.shgMemberLoanTransactionList = memberLoanTxn

                        }

                        for (i in 0 until dtLoanGpList.size) {
                            val gpLoanList: ShgGroupLoanUploadData = shgGrpLoan.get(i)
                            val dtMtgGrpLoanScheduleList =
                                dtMtgGrpLoanScheduleViewmodel!!.getUploadListData(
                                    dtLoanGpList[i].cbo_id,dtLoanGpList[i].loan_no)
                            var shgGrpLoanSchedule: List<ShgGroupLoanScheduleUploadData>? = null
                            shgGrpLoanSchedule = MappingData.returnGrpLoanScheduleObj(dtMtgGrpLoanScheduleList)
                            gpLoanList.shgGroupLoanScheduleList = shgGrpLoanSchedule
                        }

                        for (i in 0 until shgMemberloanList.size) {
                            val memberloan: ShgMemberLoanListUploadData = memberLoanList.get(i)
                            val dtLoanMemberScheduleList =
                                dtLoanMemberScheduleViewmodel!!.getUploadListData(
                                    shgMemberloanList[i].mem_id,
                                    shgMemberloanList[i].cbo_id,
                                    shgMemberloanList[i].loan_no
                                )
                            var shgMemLoanSchedule: List<ShgMeberLoanScheduleUploadData>? = null
                            shgMemLoanSchedule =
                                MappingData.returnmemberLoanScheduleObj(dtLoanMemberScheduleList)
                            memberloan.shgMemberLoanScheduleList = shgMemLoanSchedule
                        }

                        var gsonbuilder = GsonBuilder()
                        gsonbuilder.serializeNulls()
                        val gson: Gson = gsonbuilder.setPrettyPrinting().create()
                        val json = gson.toJson(uploaddataModel)

                        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                        val token =  validate!!.returnStringValue(AESEncryption.decrypt(
                            validate!!.RetriveSharepreferenceString(AppSP.token),
                            validate!!.RetriveSharepreferenceString(AppSP.userid)))

                        val file = RequestBody.create(
                            MediaType.parse("application/json; charset=utf-8"),
                            json
                        )

                        val call1 = apiInterface!!.uploadMeetingData(
                            "application/json",
                            token,
                            user,
                            file
                        )

                        val res = call1.execute()

                        if (res!!.isSuccessful) {
                            iDownload = 0
                            msg = res.body()?.response!!

                            generateMeetingViewmodel!!.updateMeetinguploadStatus(shgMeetinglist[j].cbo_id,
                                shgMeetinglist[j].mtg_no,transactionid,
                                validate!!.Daybetweentime(validate!!.currentdatetime),"")
                            /*if (res.body()?.msg.equals("Record Added To Queue")!!) {
                                try {
                                    CDFIApplication.database?.shgDao()
                                        ?.updateuploadStatus(
                                            shglist.get(j).guid, transactionid,
                                            validate!!.Daybetweentime(validate!!.currentdatetime)
                                        )
                                    var transaction = TransactionEntity(
                                        0, transactionid
                                        ,
                                        shglist.get(j).guid,
                                        "",
                                        1, 0, 0
                                    )
                                    if (shglist.get(j).is_edited == 1 && shglist.get(j).approve_status == 1) {
                                        transactionViewmodel!!.insert(transaction)
                                    }
                                    CDFIApplication.database?.cboBankDao()
                                        ?.updateBankDetailisedit(shglist.get(j).guid,validate!!.Daybetweentime(validate!!.currentdatetime))
                                    CDFIApplication.database?.cboaddressDao()
                                        ?.updateisedit(shglist.get(j).guid,validate!!.Daybetweentime(validate!!.currentdatetime))
                                    CDFIApplication.database?.cbophoneDao()
                                        ?.updateisedit(shglist.get(j).guid,validate!!.Daybetweentime(validate!!.currentdatetime))
                                    CDFIApplication.database?.systemtagDao()
                                        ?.updateisedit(shglist.get(j).guid,validate!!.Daybetweentime(validate!!.currentdatetime))
                                    CDFIApplication.database?.memberDesignationDao()
                                        ?.updateisedit(shglist.get(j).guid,validate!!.Daybetweentime(validate!!.currentdatetime))
                                    msg = res.body()?.msg!!
                                    memberviewmodel!!.updateMemberuploadStatus(shglist.get(j).guid, transactionid, validate!!.Daybetweentime(validate!!.currentdatetime))
                                    for (k in memberlist!!.indices) {
                                        CDFIApplication.database?.memberaddressDao()
                                            ?.updateisedit(memberlist!!.get(k).member_guid,validate!!.Daybetweentime(validate!!.currentdatetime))
                                        CDFIApplication.database?.memberphoneDao()
                                            ?.updateisedit(memberlist!!.get(k).member_guid,validate!!.Daybetweentime(validate!!.currentdatetime))
                                        CDFIApplication.database?.memberkycDao()
                                            ?.updateisedit(memberlist!!.get(k).member_guid,validate!!.Daybetweentime(validate!!.currentdatetime))
                                        CDFIApplication.database?.memberbanckDao()
                                            ?.updateisedit(memberlist!!.get(k).member_guid,validate!!.Daybetweentime(validate!!.currentdatetime))
                                        CDFIApplication.database?.memberSystemtagDao()
                                            ?.updateisedit(memberlist!!.get(k).member_guid,validate!!.Daybetweentime(validate!!.currentdatetime))
                                        var kycimage1 = TransactionEntity(
                                            0, transactionid
                                            ,
                                            shglist.get(j).guid,
                                            memberlist!!.get(k).member_guid,
                                            2, 0, 0
                                        )
                                        transactionViewmodel!!.insert(kycimage1)

                                    }
                                    progressDialog.setMessage(
                                        LabelSet.getText("DataUploading",
                                            R.string.DataUploading) + " " + (j + 1) + "/" + shglist.size)
//                                    progressDialog.dismiss()
                                } catch (ex: Exception) {
                                    ex.printStackTrace()
//                                    progressDialog.dismiss()
                                }
                            }*/
                        } else {
                            msg = "" + res.code() + " " + res.message()
                            code = res.code()
                            idownload = 1
                        }

                    }

                    runOnUiThread {
                        if (idownload == 0) {
                            progressDialog.dismiss()
                            val text = msg
                            validate!!.CustomAlert(text, this@MeetingSynchronizationActivity)
//                            var shglist = shgViewModel!!.getPendingSHGlistdata()
                            var shgPendingMeetinglist = generateMeetingViewmodel!!.getShgMeetingList()
                            tv_upload_meeting.text = LabelSet.getText(
                                "upload_meeting",
                                R.string.upload_meeting
                            ) + " (" + shgPendingMeetinglist.size + ")"
                            /*tv_upload_meeting.setText(
                                    Html.fromHtml(
                                        LabelSet.getText(
                                            "upload_data",
                                            R.string.upload_data
                                        ) + "\n" + LabelSet.getText(
                                            "upload_meeting",
                                            R.string.upload_meeting
                                        ) + " (" + shglist!!.size + ")"
                                    )
                                )*/
                        } else if (idownload == -1) {
                            progressDialog.dismiss()
                            val text = LabelSet.getText("nothing_upload", R.string.nothing_upload)
                            validate!!.CustomAlert(text, this@MeetingSynchronizationActivity)
                            Toast.makeText(
                                this@MeetingSynchronizationActivity,
                                msg,
                                Toast.LENGTH_LONG
                            ).show()
                        } else {
                            progressDialog.dismiss()
                            if (code == 403) {
                                Toast.makeText(
                                    this@MeetingSynchronizationActivity,
                                    msg,
                                    Toast.LENGTH_LONG
                                ).show()
                                CustomAlertlogin()
                            } else {
                                validate!!.CustomAlert(msg, this@MeetingSynchronizationActivity)
                                Toast.makeText(
                                    this@MeetingSynchronizationActivity,
                                    msg,
                                    Toast.LENGTH_LONG
                                )
                                    .show()
                            }
                        }

                    }

                } catch (bug: SocketTimeoutException) {
                    progressDialog.dismiss()
//                                Toast.makeText(activity!!, "Socket Timeout", Toast.LENGTH_LONG).show();
                } catch (bug: ConnectTimeoutException) {
                    progressDialog.dismiss()
                    // Toast.makeText(getApplicationContext(), "Connection Timeout", Toast.LENGTH_LONG).show();
                } catch (e: Exception) {
                    e.printStackTrace()
                    progressDialog.dismiss()
                }

            }

        }.start()
    }

    fun importSHGMeetinglist() {
        progressDialog = ProgressDialog.show(
            this, LabelSet.getText("app_name", R.string.app_name),
            LabelSet.getText("DataLoading", R.string.DataLoading)
        )
        progressDialog.setIcon(this.getDrawable(R.drawable.appicon))
        var idownload = 0
        var msg = ""
        var code = 0
        object : Thread() {

            //@SuppressLint("NewApi")
            override fun run() {
                try {

                    val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                    val token =  validate!!.returnStringValue(AESEncryption.decrypt(
                        validate!!.RetriveSharepreferenceString(AppSP.token),
                        validate!!.RetriveSharepreferenceString(AppSP.userid)))
                    val shgID = validate!!.RetriveSharepreferenceLong(AppSP.shgid)

                    val call1 = apiInterface?.getShgMeeinglist(
                        token,
                        user,
                        shgID,
                        4
                    )

                    val res1 = call1?.execute()

                    if (res1!!.isSuccessful) {
                        try {

                            msg = "" + res1.code() + " " + res1.message()

                            if (res1.body()?.downLoadMtgList != null) {
                                try {
                                    var dowLoadMtgList = res1.body()?.downLoadMtgList

                                    for (i in 0 until dowLoadMtgList!!.size){
                                        var shgMeetingList = dowLoadMtgList[i].shgMeeting
                                        var shgGpLoanList = dowLoadMtgList[i].shgGroupLoanList
                                        var shgmemLoanList = dowLoadMtgList[i].shgMemberLoanList
                                        var loanApplicationLIst = dowLoadMtgList[i].shgLoanApplicationList
                                        var mcpList = dowLoadMtgList[i].shgMcpList

                                        var meetingList = MappingData.returnmtgObjentity(shgMeetingList)
                                        var gploanList = MappingData.returnGpmtgObjentity(shgGpLoanList)
                                        var memloanList = MappingData.returnmemmtgObjentity(shgmemLoanList)
                                        var loanappList = MappingData.returnloanAppliObjentity(loanApplicationLIst)
                                        var shgmcpData = MappingData.returnmcpObjentity(mcpList)

//                                        generateMeetingViewmodel!!.insert(meetingList!!)
                                        CDFIApplication.database?.dtmtgDao()?.insertDtmtg1(meetingList)

                                        var shgMeetingDetailsListData = shgMeetingList!!.shgMeetingDetailsList
                                        var shgFinanceTransactionListData = shgMeetingList.shgFinanceTransactionList
                                        var shgFinanceTransactionDetailGroupListData = shgMeetingList.shgFinanceTransactionDetailGroupList
                                        var shgGroupLoanTransactionData = shgMeetingList.shgGroupLoanTransaction

                                        for(j in 0 until shgMeetingDetailsListData?.size!!){
                                            shgMeetingDetailsListData[j].cbo_id = shgMeetingList.cbo_id
                                            shgMeetingDetailsListData[j].mtg_no = shgMeetingList.mtg_no
                                            shgMeetingDetailsListData[j].mtg_guid = shgMeetingList.mtg_guid
                                        }

                                        for(j in 0 until shgFinanceTransactionListData?.size!!){
                                            shgFinanceTransactionListData[j].cbo_id = shgMeetingList.cbo_id
                                            shgFinanceTransactionListData[j].mtg_no =  shgMeetingList.mtg_no
                                            shgFinanceTransactionListData[j].mtg_guid = shgMeetingList.mtg_guid
                                        }

                                        for(j in 0 until shgFinanceTransactionDetailGroupListData?.size!!){
                                            shgFinanceTransactionDetailGroupListData[j].cbo_id = shgMeetingList.cbo_id
                                            shgFinanceTransactionDetailGroupListData[j].mtg_no = shgMeetingList.mtg_no
                                            shgFinanceTransactionDetailGroupListData[j].mtg_guid = shgMeetingList.mtg_guid
                                        }

                                        for(j in 0 until shgGroupLoanTransactionData?.size!!){
                                            shgGroupLoanTransactionData[j].cbo_id = shgMeetingList.cbo_id
                                            shgGroupLoanTransactionData[j].mtg_no = shgMeetingList.mtg_no
                                            shgGroupLoanTransactionData[j].mtg_guid = shgMeetingList.mtg_guid
                                        }

                                        var shgMeetingDetailsList = MappingData.returnshgMeetingDetailObjentity(shgMeetingDetailsListData)
                                        var shgFinanceTransactionList = MappingData.returnshgshgFinanceTxnObjentity(shgFinanceTransactionListData)
                                        var shgFinanceTransactionDetailGroupList = MappingData.returnshgFinanceTxnDtGroupObjentity(shgFinanceTransactionDetailGroupListData)
                                        var shgGroupLoanTransaction = MappingData.returnshgGrpLoanTxnObjentity(shgGroupLoanTransactionData)

                                        if (!shgMeetingDetailsList.isNullOrEmpty()) {
                                            CDFIApplication.database?.dtdetmtgDao()?.insertAlldtmtgDet(shgMeetingDetailsList)
                                        }

                                        if (!shgFinanceTransactionList.isNullOrEmpty()) {
                                            CDFIApplication.database?.dtMtgFinTxnDao()?.insertMtgFinTxnAllData(shgFinanceTransactionList)
                                        }
                                        if (!shgFinanceTransactionDetailGroupList.isNullOrEmpty()) {
                                            CDFIApplication.database?.incomeandExpendatureDao()?.insertIncomeAndExpenditureAllData(shgFinanceTransactionDetailGroupList)
                                        }

                                        if (!shgGroupLoanTransaction.isNullOrEmpty()) {
                                            CDFIApplication.database?.dtLoanGpTxnDao()?.insertLoanGpTxnAllData(shgGroupLoanTransaction)
                                        }

                                        for(j in 0 until shgMeetingDetailsListData.size){
                                            var shgMemberLoanTxnList = shgMeetingDetailsListData[j].shgMemberLoanTransactionList
                                            var shgFinanceTxnDetailMemList = shgMeetingDetailsListData[j].shgFinanceTransactionDetailMemberList

                                            for(k in 0 until shgMemberLoanTxnList!!.size){
                                                shgMemberLoanTxnList[k].cbo_id = shgMeetingDetailsListData[j].cbo_id
                                                shgMemberLoanTxnList[k].mtg_no = shgMeetingDetailsListData[j].mtg_no
                                                shgMemberLoanTxnList[k].mtg_guid = shgMeetingDetailsListData[j].mtg_guid
                                            }

                                            for(k in 0 until shgFinanceTxnDetailMemList!!.size){
                                                shgFinanceTxnDetailMemList[k].cbo_id = shgMeetingDetailsListData[j].cbo_id
                                                shgFinanceTxnDetailMemList[k].mtg_no = shgMeetingDetailsListData[j].mtg_no
                                                shgFinanceTxnDetailMemList[k].mtg_guid = shgMeetingDetailsListData[j].mtg_guid
                                            }

                                            var shgMemberLoanTxnData = MappingData.returnshgmemLoanTxnObjentity(shgMemberLoanTxnList)
                                            var shgFinanceTxnDetailMemData = MappingData.returnFinanceTxnDtMemberObjentity(shgFinanceTxnDetailMemList)

                                            if (!shgMemberLoanTxnData.isNullOrEmpty()) {
                                                CDFIApplication.database?.dtLoanTxnMemDao()?.insertLoanTxnMemAllData(shgMemberLoanTxnData)
                                            }

                                            if (!shgFinanceTxnDetailMemData.isNullOrEmpty()) {
                                                CDFIApplication.database?.dtFinTxnDetDao()?.insertMtgFinTxnAllData(shgFinanceTxnDetailMemData)
                                            }

                                        }

                                        if (!gploanList.isNullOrEmpty()){
                                            CDFIApplication.database?.dtLoanGpDao()?.insertLoanGpAllData(gploanList)
                                        }

                                        if (!memloanList.isNullOrEmpty()){
                                            CDFIApplication.database?.dtLoanMemberDao()?.insertLoanMemberAllData(memloanList)
                                        }

                                        if (!loanappList.isNullOrEmpty()){
                                            CDFIApplication.database?.dtLoanApplicationDao()?.insertLoanApplicationAllData(loanappList)
                                        }

                                        if(!shgmcpData.isNullOrEmpty()){
                                            CDFIApplication.database?.shgmcpDao()?.insertShgMcpAllData(shgmcpData)
                                        }

                                        for (l in 0 until shgGpLoanList!!.size){
                                            var shgGroupLoanSchedule = shgGpLoanList[l].shgGroupLoanSchedule
                                            for(j in 0 until shgGroupLoanSchedule!!.size){
                                                shgGroupLoanSchedule[j].cbo_id = shgGpLoanList[l].cbo_id
                                                shgGroupLoanSchedule[j].mtg_no = shgGpLoanList[l].mtg_no
                                                shgGroupLoanSchedule[j].mtg_guid = shgGpLoanList[l].mtg_guid
                                            }
                                            var shgGpScheduleList = MappingData.returngploanScheduleObjentity(shgGroupLoanSchedule)
                                            if (!shgGpScheduleList.isNullOrEmpty()) {
                                                CDFIApplication.database?.dtMtgGrpLoanScheduleDao()?.insertMtgGrpLoanScheduleAllData(shgGpScheduleList)
                                            }

                                        }

                                        for (k in 0 until shgmemLoanList!!.size){
                                            var shgmemLoanSchedule = shgmemLoanList[k].shgMeberLoanSchedule
                                            for(m in 0 until shgmemLoanSchedule!!.size){
                                                shgmemLoanSchedule[m].cbo_id = shgmemLoanList[k].cbo_id
                                                shgmemLoanSchedule[m].mtg_no = shgmemLoanList[k].mtg_no
                                                shgmemLoanSchedule[m].mtg_guid = shgmemLoanList[k].mtg_guid
                                            }
                                            var shgmebScheduleList = MappingData.returnmembloanScheduleObjentity(shgmemLoanSchedule)
                                            if (!shgmebScheduleList.isNullOrEmpty()) {
                                                CDFIApplication.database?.dtLoanMemberScheduleDao()?.insertLoanMemberScheduleAllData(shgmebScheduleList)
                                            }

                                        }

                                    }

                                } catch (ex: Exception) {
                                    ex.printStackTrace()
                                }
                            }

                            idownload = 1

                            progressDialog.dismiss()

                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }

                    } else {
                        var resMsg = ""
                        if (res1.code() == 403) {
                            code = res1.code()
                            msg = res1.message()
                        } else {
                            if (res1.errorBody()?.contentLength() == 0L || res1.errorBody()
                                    ?.contentLength()!! < 0L
                            ) {
                                code = res1.code()
                                resMsg = res1.message()
                            } else {
                                var jsonObject1 =
                                    JSONObject(res1.errorBody()!!.source().readUtf8().toString())

                                code =
                                    validate!!.returnIntegerValue(
                                        jsonObject1.optString("responseCode").toString()
                                    )
                                resMsg = validate!!.returnStringValue(
                                    jsonObject1.optString("responseMsg").toString()
                                )
                            }
                            msg = "" + validate!!.alertMsg(
                                this@MeetingSynchronizationActivity,
                                responseViewModel,
                                code,
                                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                                resMsg
                            )

                        }

                    }

                    runOnUiThread {
                        if (idownload == 1) {
                            progressDialog.dismiss()
                            val text = LabelSet.getText(
                                "Datadownloadedsuccessfully",
                                R.string.Datadownloadedsuccessfully
                            )
                            validate!!.CustomAlert(text, this@MeetingSynchronizationActivity)
                        } else {
                            progressDialog.dismiss()
                            if (code == 403) {
                                Toast.makeText(
                                    this@MeetingSynchronizationActivity,
                                    msg,
                                    Toast.LENGTH_LONG
                                )
                                    .show()
                                CustomAlertlogin()
                            } else {
                                val text = msg
                                validate!!.CustomAlert(text, this@MeetingSynchronizationActivity)
                                Toast.makeText(
                                    this@MeetingSynchronizationActivity,
                                    msg,
                                    Toast.LENGTH_LONG
                                )
                                    .show()
                            }
                        }

                    }

                } catch (bug: SocketTimeoutException) {
                    progressDialog.dismiss()
                } catch (bug: ConnectTimeoutException) {
                    progressDialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                    progressDialog.dismiss()
                }
            }

        }.start()
    }



    /*   fun importSHGStatus() {
           progressDialog = ProgressDialog.show(
               this, LabelSet.getText("app_name", R.string.app_name),
               LabelSet.getText("DataLoading", R.string.DataLoading)
           )
           progressDialog!!.setIcon(this.getDrawable(R.drawable.appicon))
           var idownload = 0
           var code = 0
           var msg = ""
           object : Thread() {

               //@SuppressLint("NewApi")
               override fun run() {
                   try {

                       val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                       val token = validate!!.RetriveSharepreferenceString(AppSP.Token)!!

                       val call = apiInterface?.getnonSyncShgsByActivationStatus(
                           token,
                           user
                       )

                       val res = call?.execute()

                       if (res!!.isSuccessful) {
                           if (!res.body()?.groupResponseStatus.isNullOrEmpty()) {
                               try {
                                   for (i in 0 until res.body()?.groupResponseStatus!!.size) {
                                       shgViewModel!!.updateShgDedupStatus(
                                           res.body()?.groupResponseStatus?.get(i)?.guid!!,
                                           res.body()?.groupResponseStatus?.get(i)?.code!!,
                                           res.body()?.groupResponseStatus?.get(i)?.activationStatus!!,
                                           res.body()?.groupResponseStatus?.get(i)?.approve_status!!,
                                           res.body()?.groupResponseStatus?.get(i)?.checker_remarks!!
                                       )
                                       var memberList = res.body()?.groupResponseStatus!!.get(i)
                                           .memberDownloadStatus
                                       for (j in 0 until memberList.size) {
                                           memberviewmodel!!.updateMemberDedupStatus(
                                               memberList.get(j).guid,
                                               memberList.get(j).code!!,
                                               memberList.get(j).activationStatus,
                                               memberList.get(j).approve_status,
                                               validate!!.returnStringValue(memberList.get(j).checker_remarks)
                                           )
                                       }
                                   }
//
                               } catch (ex: Exception) {
                                   ex.printStackTrace()
//                                    progressDialog.dismiss()
                               }
                           } else {
                               idownload = 0
                           }
                       } else {
                           var resMsg=""
                           idownload = 1
                           if (res.code() == 403) {

                               code = res.code()
                               msg = res.message()
                           } else {
                               if(res.errorBody()?.contentLength()==0L || res.errorBody()?.contentLength()!! <0L){
                                   code = res.code()
                                   resMsg = res.message()
                               }else {
                                   var jsonObject1 =
                                       JSONObject(res.errorBody()!!.source().readUtf8().toString())

                                   code =
                                       validate!!.returnIntegerValue(
                                           jsonObject1.optString("responseCode").toString()
                                       )
                                   resMsg = validate!!.returnStringValue(jsonObject1.optString("responseMsg").toString())
                               }
                               msg = "" + validate!!.alertMsg(
                                   this@MeetingSynchronizationActivity,
                                   responseViewModel,
                                   code,
                                   validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                                   resMsg)

                           }


                       }




                       runOnUiThread {
                           if (idownload == 0) {
                               progressDialog!!.dismiss()
                               val text = LabelSet.getText(
                                   "Datadownloadedsuccessfully",
                                   R.string.Datadownloadedsuccessfully
                               )
                               CustomAlert(text)
                           } else {
                               progressDialog!!.dismiss()
                               if (code == 403) {
                                   Toast.makeText(this@MeetingSynchronizationActivity, msg, Toast.LENGTH_LONG)
                                       .show()
                                   CustomAlertlogin()
                               } else {

                                   validate!!.CustomAlert(msg, this@MeetingSynchronizationActivity)

                               }

                           }

                       }

                   } catch (bug: SocketTimeoutException) {
                       progressDialog!!.dismiss()
//                                Toast.makeText(activity!!, "Socket Timeout", Toast.LENGTH_LONG).show();
                   } catch (bug: ConnectTimeoutException) {
                       progressDialog!!.dismiss()
                       // Toast.makeText(getApplicationContext(), "Connection Timeout", Toast.LENGTH_LONG).show();
                   } catch (e: Exception) {
                       e.printStackTrace()
                       progressDialog!!.dismiss()
                   }
               }

           }.start()
       }
*/

    fun importMeeting_Approval_Rejection_status() {

        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText("DataLoading", R.string.DataLoading)
        )

        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
        val token =  validate!!.returnStringValue(AESEncryption.decrypt(
            validate!!.RetriveSharepreferenceString(AppSP.token),
            validate!!.RetriveSharepreferenceString(AppSP.userid)))
      //  val shgId = validate!!.RetriveSharepreferenceString(AppSP.Token)!!
        val callCount = apiInterface?.getMeetingApprove_Reject_Status(
            token,
            user,
            939
        )

        callCount?.enqueue(object : Callback<List<DownloadMeetingStatus>> {
            override fun onFailure(callCount: Call<List<DownloadMeetingStatus>>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<List<DownloadMeetingStatus>>,
                response: Response<List<DownloadMeetingStatus>>
            ) {

                progressDialog.dismiss()
                if (response.isSuccessful) {
                    if (!response.body()!!.isNullOrEmpty()) {
                        try {
                            var list = response.body()!!

                            for (i in 0 until list.size) {

                                generateMeetingViewmodel!!.updateMtgApprova_Rejection_Status(
                                    list.get(i).mtgNo,
                                    list.get(i).cboId,
                                    validate!!.Daybetweentime(validate!!.currentdatetime),
                                    validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                                    list.get(i).approvalStatus,
                                    list.get(i).checkerRemarks)
                            }
                            val text = LabelSet.getText(
                                "Datadownloadedsuccessfully",
                                R.string.Datadownloadedsuccessfully
                            )
                            CustomAlert(text)

                        } catch (ex: Exception) {
                            ex.printStackTrace()
                            //                                    progressDialog.dismiss()
                        }
                    }
                } else {
                    progressDialog.dismiss()
                    var resCode = 0
                    var resMsg = ""
                    if (response.code() == 403) {

                        CustomAlertlogin()
                    } else {
                        if (response.errorBody()?.contentLength() == 0L || response.errorBody()
                                ?.contentLength()!! < 0L
                        ) {
                            resCode = response.code()
                            resMsg = response.message()
                        } else {
                            var jsonObject1 =
                                JSONObject(response.errorBody()!!.source().readUtf8().toString())

                            resCode =
                                validate!!.returnIntegerValue(
                                    jsonObject1.optString("responseCode").toString()
                                )
                            resMsg = validate!!.returnStringValue(
                                jsonObject1.optString("responseMsg").toString()
                            )
                        }

                        validate!!.CustomAlertMsg(
                            this@MeetingSynchronizationActivity,
                            responseViewModel,
                            resCode,
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!, resMsg
                        )

                    }

                }


            }

        })
    }

    fun importtoken(user: String, pass: String) {
        var grant_type = "password"
        var userId = user
        var password = pass
        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText("authenticate_user", R.string.authenticate_user)
        )
        val callCount = apiInterface?.gettokenforce(
            grant_type,
            userId,
            password, validate!!.RetriveSharepreferenceInt(AppSP.State_Selected), 1,
            validate!!.RetriveSharepreferenceString(AppSP.Roleid)!!)
        callCount?.enqueue(object : Callback<JsonObject> {
            override fun onFailure(callCount: Call<JsonObject>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                //       Log.e("", response!!.body().toString())
                progressDialog.dismiss()


                if (response.isSuccessful) {

                    var jsonObject1: JSONObject? = null
                    try {
                        jsonObject1 = JSONObject(response.body().toString())

                        val auth = jsonObject1.get("accessToken").toString()
                        if (auth.trim().length > 0) {
                            validate!!.SaveSharepreferenceString(
                                AppSP.token,
                                validate!!.returnStringValue(
                                    AESEncryption.encrypt(
                                        auth,validate!!.RetriveSharepreferenceString(AppSP.userid)))
                            )
                        }
                        validate!!.CustomAlert(
                            LabelSet.getText(
                                "userauthenticatedsuccessfully",
                                R.string.userauthenticatedsuccessfully
                            ), this@MeetingSynchronizationActivity
                        )
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        var jsonObject1 =
                            JSONObject(response.errorBody()!!.source().readUtf8().toString())

                        var resCode =
                            validate!!.returnIntegerValue(
                                jsonObject1.optString("responseCode").toString()
                            )
                        validate!!.CustomAlertMsg(
                            this@MeetingSynchronizationActivity,
                            responseViewModel,
                            resCode,
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                            validate!!.returnStringValue(
                                jsonObject1.optString("responseMsg").toString()
                            )
                        )
                    }

                } else {
                    var jsonObject1 =
                        JSONObject(response.errorBody()!!.source().readUtf8().toString())

                    var resCode =
                        validate!!.returnIntegerValue(
                            jsonObject1.optString("responseCode").toString()
                        )
                    validate!!.CustomAlertMsg(
                        this@MeetingSynchronizationActivity,
                        responseViewModel,
                        resCode,
                        validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                        validate!!.returnStringValue(
                            jsonObject1.optString("responseMsg").toString()
                        )
                    )
                }


            }

        })


    }

    fun CustomAlertlogin() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogelogin, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        val password =  validate!!.returnStringValue(AESEncryption.decrypt(
            validate!!.RetriveSharepreferenceString(AppSP.Password),
            validate!!.RetriveSharepreferenceString(AppSP.userid)))
        mDialogView.etUsername.setText(validate!!.RetriveSharepreferenceString(AppSP.userid))
        mDialogView.etPassword.setText(password)
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_dialog_title2.text = LabelSet.getText("app_name",R.string.app_name)
        mDialogView.btn_login.text = LabelSet.getText("log_in",R.string.log_in)
        mDialogView.chk_showHide1.text = LabelSet.getText("show_password",R.string.show_password)
        mDialogView.btn_login.setOnClickListener {
            mAlertDialog.dismiss()
            importtoken(
                mDialogView.etUsername.text.toString(),
                validate!!.returnStringValue(
                    AESEncryption.encrypt(validate!!.returnStringValue(
                        mDialogView.etPassword.text.toString()),mDialogView.etUsername.text.toString()))
            )
        }
        mAlertDialog.chk_showHide1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                mAlertDialog.etPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                mAlertDialog.etPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
        }

    }

    fun CustomAlert(str: String) {
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.customealertdialoge, null)
        val mBuilder = android.app.AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str
        mDialogView.btn_ok.setOnClickListener {


            mAlertDialog.dismiss()


        }
    }

    /* fun setLabelText() {
         tvUploadData.setText(LabelSet.getText("upload_data", R.string.upload_data))
         tvStatusData.setText(LabelSet.getText("download_status", R.string.download_status))
         tbl_downloadData.setText(LabelSet.getText("download_data", R.string.download_data))
         tvMasterData.setText(
             LabelSet.getText(
                 "master_data_download",
                 R.string.master_data_download
             )
         )
     }
*/

    fun importuploadstatus() {

        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText("DataLoading", R.string.DataLoading)
        )

        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
        val token =  validate!!.returnStringValue(AESEncryption.decrypt(
            validate!!.RetriveSharepreferenceString(AppSP.token),
            validate!!.RetriveSharepreferenceString(AppSP.userid)))
        val callCount = apiInterface?.getdownloadTransactionStatus(
            token,
            user
        )

        callCount?.enqueue(object : Callback<SHGResponse> {
            override fun onFailure(callCount: Call<SHGResponse>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<SHGResponse>,
                response: Response<SHGResponse>
            ) {

                progressDialog.dismiss()
                if (response.isSuccessful) {
                    if (!response.body()?.grouptransactionResponse.isNullOrEmpty()) {
                        try {
                            var list = response.body()?.grouptransactionResponse

                            for (i in list!!.indices) {

                                generateMeetingViewmodel!!.updatedTransactionstatus(list.get(i).remarks,
                                    list.get(i).transaction_id)

                            }
                            val text = LabelSet.getText(
                                "Datadownloadedsuccessfully",
                                R.string.Datadownloadedsuccessfully
                            )
                            CustomAlert(text)

                        } catch (ex: Exception) {
                            ex.printStackTrace()
                            //                                    progressDialog.dismiss()
                        }
                    }
                } else {
                    progressDialog.dismiss()
                    var resCode = 0
                    var resMsg = ""
                    if (response.code() == 403) {

                        CustomAlertlogin()
                    } else {
                        if(response.errorBody()?.contentLength()==0L || response.errorBody()?.contentLength()!! <0L){
                            resCode = response.code()
                            resMsg = response.message()
                        }else {
                            var jsonObject1 =
                                JSONObject(response.errorBody()!!.source().readUtf8().toString())

                            resCode =
                                validate!!.returnIntegerValue(
                                    jsonObject1.optString("responseCode").toString()
                                )
                            resMsg = validate!!.returnStringValue(jsonObject1.optString("responseMsg").toString())
                        }

                        validate!!.CustomAlertMsg(
                            this@MeetingSynchronizationActivity,
                            responseViewModel,
                            resCode,
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,resMsg)

                    }

                }


            }

        })
    }

    override fun onBackPressed() {
        var intent = Intent(this, MainActivityDrawer::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }


}
