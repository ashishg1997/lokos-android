package com.microware.cdfi.activity.meeting

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.MemberMeetingSummaryAdapter
import com.microware.cdfi.entity.DtmtgDetEntity
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_member_meeting_summary.*

class MemberMeetingSummaryActivity : AppCompatActivity() {

    var memberlist: List<DtmtgDetEntity>? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    var dtLoanTxnMemViewmodel: DtLoanTxnMemViewmodel? = null
    var dtLoanMemViewmodel: DtLoanMemberViewmodel? = null
    lateinit var lookupViewmodel: LookupViewmodel
    var dtLoanViewmodel: DtLoanViewmodel? = null
    var validate: Validate? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_member_meeting_summary)

        validate = Validate(this)
        generateMeetingViewmodel = ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        dtLoanTxnMemViewmodel = ViewModelProviders.of(this).get(DtLoanTxnMemViewmodel::class.java)
        dtLoanMemViewmodel = ViewModelProviders.of(this).get(DtLoanMemberViewmodel::class.java)
        dtLoanViewmodel = ViewModelProviders.of(this).get(DtLoanViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)

        replaceFragmenty(
            fragment = MeetingTopBarFragment(96),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        spin_membername?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                if (position > 0) {
                    val id = returnmemid()
                    setCompuslorySaving(id)
                    loanRepayment(id)
                    fillRecylcer(id)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }

        tv_adjustment.setOnClickListener {
            var intent = Intent(this, AdjustmentActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        fillmemberspinner()
        setLabelText()

    }

    private fun loanRepayment(id: Long) {
        val loanPaid = dtLoanTxnMemViewmodel!!.getMemberLoanRepaymentAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber), id
        )

        val loanInterest = dtLoanTxnMemViewmodel!!.getMemberLoanRepaymentIntAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber), id
        )

        val loanOP = dtLoanTxnMemViewmodel!!.getMemberLoanRepaymentOPAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber), id
        )

        val com = (loanPaid + loanOP)
        tv_loan_repay_amt.text = validate!!.returnStringValue(loanPaid.toString())
        tv_int_repay_amt.text = validate!!.returnStringValue(loanInterest.toString())
        tv_cum_repay_amt.text = validate!!.returnStringValue(com.toString())

    }

    private fun fillRecylcer(id: Long) {
        var list = dtLoanMemViewmodel!!.getloanListSummeryData(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            id
        )

        rvList.layoutManager = LinearLayoutManager(this)
        val membersummaryAdapter = MemberMeetingSummaryAdapter(this, list)
        rvList.adapter = membersummaryAdapter
    }

    private fun setCompuslorySaving(id: Long) {
        val list = generateMeetingViewmodel.getCompulsoryData(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            id
        )

        if (!list.isNullOrEmpty()) {
            /*Compulsory Saving*/
            tv_today_com_amount.text = list[0].sav_comp.toString()
            val value = list[0].sav_comp_cb
            val sav_comp_ob = list[0].sav_comp_ob
            tv_cum_com_amount.text = (value).toString()
            /*Voluntry saving*/
            tv_vol_amt.text = list[0].sav_vol.toString()
            val vol_value = list[0].sav_vol_cb
            val vol_sav_comp_ob = list[0].sav_vol_ob
            tv_cum_amt.text = (vol_value!! ).toString()
            tv_today_penalty_amt.text = list[0].penalty.toString()
            tv_vl_withdrawl_amount.text = list[0].sav_vol_withdrawal.toString()
        } else {
            tv_today_com_amount.text = "0"
            tv_cum_com_amount.text = "0"
            tv_vol_amt.text = "0"
            tv_cum_amt.text = "0"
            tv_today_penalty_amt.text = "0"
            tv_vl_withdrawl_amount.text = "0"
        }
    }
    fun getValue(keyCode: Int?,flag:Int): String? {
        var name: String? = null
        name = lookupViewmodel.getlookupValue(
            flag,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),
            keyCode
        )
        return name
    }
    private fun fillmemberspinner() {
        memberlist = generateMeetingViewmodel.getListDataMtgByMtgnum(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        val adapter: ArrayAdapter<String?>
        if (!memberlist.isNullOrEmpty()) {
            val isize = memberlist!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in memberlist!!.indices) {
                var post= generateMeetingViewmodel.reurnmemberpost(memberlist!![i].mem_id)
                var postname=getValue(post,1)

                sValue[i + 1] = memberlist!![i].member_name+" ("+postname+")"
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_membername.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(memberlist!!.size + 1)
            sValue[0] = LabelSet.getText(
                "nomemberavailable",
                R.string.nomemberavailable
            )
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_membername.adapter = adapter
        }
    }

    private fun setMember(): Int {
        var pos = 0
        if (!memberlist.isNullOrEmpty()) {
            for (i in memberlist!!.indices) {
                if (validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid) == memberlist!!.get(i).mem_id)
                    pos = i + 1
            }
        }
        spin_membername.setSelection(pos)
        spin_membername.isEnabled = false
        return pos
    }

    private fun returnmemid(): Long {

        val pos = spin_membername.selectedItemPosition
        var id = 0L

        if (!memberlist.isNullOrEmpty()) {
            if (pos > 0) id = memberlist!!.get(pos - 1).mem_id
        }
        return id
    }

    fun setLabelText() {
        tv_receipt_from_member.text = LabelSet.getText(
            "receipt_from_member",
            R.string.receipt_from_member
        )
        tv_adjustment.text = LabelSet.getText(
            "adjustment",
            R.string.adjustment
        )
        tv_compulasory_saving.text = LabelSet.getText(
            "compulsory_saving",
            R.string.compulsory_saving
        )
        tv_today_com.text = LabelSet.getText(
            "today",
            R.string.today
        )
        tv_cum_com.text = LabelSet.getText("cum", R.string.cum)
        tv_voluntary_saving.text = LabelSet.getText(
            "voluntary_saving",
            R.string.voluntary_saving
        )
        tv_total_vol.text = LabelSet.getText(
            "today",
            R.string.today
        )
        tv_cum_vol.text = LabelSet.getText("cum", R.string.cum)
        tv_total_loan_repayment.text = LabelSet.getText(
            "total_loan_repayment",
            R.string.total_loan_repayment
        )
        tv_total_tot.text = LabelSet.getText(
            "today",
            R.string.today
        )
        tv_cum_tot.text = LabelSet.getText("cum", R.string.cum)
        tv_interest_tot.text = LabelSet.getText(
            "interest",
            R.string.interest
        )
        tv_penalty.text = LabelSet.getText(
            "penalty",
            R.string.penalty
        )
        tv_paid_to_member.text = LabelSet.getText(
            "paid_to_member",
            R.string.paid_to_member
        )
        tv_withdrawl.text = LabelSet.getText(
            "withdrawal_vol_saving",
            R.string.withdrawal_vol_saving
        )

    }

    fun returnvalue(id: Int, flag: Int): String {
        var listdata = lookupViewmodel.getlookup(
            flag,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        return validate!!.returnlookupcodevalue(id, listdata)
    }

    override fun onBackPressed() {
        var intent = Intent(this, MeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

}