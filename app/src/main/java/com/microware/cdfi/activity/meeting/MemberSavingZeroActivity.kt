package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.MemberSavingZeroAdapter
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.fragment.MeetingTopBarZeroFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import kotlinx.android.synthetic.main.activity_member_saving_zero.*
import kotlinx.android.synthetic.main.buttons.*

class MemberSavingZeroActivity : AppCompatActivity() {
    var validate:Validate?=null
    lateinit var memberSavingZeroAdapter: MemberSavingZeroAdapter
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    var TodayCompulsaryvalue=0
    var TodayVoluntaryvalue=0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_member_saving_zero)
        validate= Validate(this)

        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)


        btn_save.setOnClickListener {
            getCompulsoryValue()
        }
        btn_cancel.setOnClickListener {
            var intent = Intent(this, CutOffMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }


        replaceFragmenty(
            fragment = MeetingTopBarZeroFragment(2),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )
        setLabelText()
        fillRecyclerView()
    }

    override fun onBackPressed() {
        var intent = Intent(this, CutOffMeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

    fun setLabelText() {
        tv_srno.text = LabelSet.getText("sr_no", R.string.sr_no)
        tv_memberName.text = LabelSet.getText(
            "member_name",
            R.string.member_name
        )
        tv_compulsory_saving.text = LabelSet.getText(
            "compulsory_saving",
            R.string.compulsory_saving
        )
        tv_voluntary_saving.text = LabelSet.getText(
            "voluntary_saving",
            R.string.voluntary_saving
        )

        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }

    private fun fillRecyclerView() {
        TodayCompulsaryvalue=0
        TodayVoluntaryvalue=0
        var list = generateMeetingViewmodel.getListDataMtgByMtgnum(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        val total= generateMeetingViewmodel.getsumcomp(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        memberSavingZeroAdapter = MemberSavingZeroAdapter(this,list,total)

        rvMemberList.layoutManager = LinearLayoutManager(this)

        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = rvMemberList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvMemberList.layoutParams = params
        rvMemberList.adapter = memberSavingZeroAdapter
    }

    fun getTotalValue() {
        var iValue = 0
        var iValueCum = 0
        val iCount = rvMemberList.childCount


        for (i in 0 until iCount) {
            val gridChild =
                rvMemberList.getChildAt(i) as? ViewGroup

            val tv_count = gridChild!!
                .findViewById<View>(R.id.et_compulasory_amount) as? EditText

            if (!tv_count!!.text.toString().isNullOrEmpty()) {
                iValue=iValue + validate!!.returnIntegerValue(tv_count.text.toString())
            }

        }
        tv_TotalCompulsaryValue.text = iValue.toString()

    }

    fun getTotalVoluntaryValue() {
        var iValue = 0
        val iCount = rvMemberList.childCount


        for (i in 0 until iCount) {
            val gridChild =
                rvMemberList.getChildAt(i) as? ViewGroup

            val tv_count = gridChild!!
                .findViewById<View>(R.id.et_voluntary_saving_amount) as? EditText

            if (!tv_count!!.text.toString().isNullOrEmpty()) {
                iValue=iValue + validate!!.returnIntegerValue(tv_count.text.toString())
            }

        }
        tv_TotalVoluntaryValue.text = iValue.toString()

    }

    fun getTotalValue(iValue: Int,flag:Int) {
        if(flag==1) {
            TodayCompulsaryvalue = TodayCompulsaryvalue + iValue
            tv_TotalCompulsaryValue.text = TodayCompulsaryvalue.toString()
        }else if(flag==2){
            TodayVoluntaryvalue = TodayVoluntaryvalue + iValue
            tv_TotalVoluntaryValue.text = TodayVoluntaryvalue.toString()
        }
    }



    fun getCompulsoryValue() {
        var saveValue = 0

        val iCount = rvMemberList.childCount


        for (i in 0 until iCount) {
            val gridChild =
                rvMemberList.getChildAt(i) as? ViewGroup

            val tv_count = gridChild!!
                .findViewById<View>(R.id.et_compulasory_amount) as? EditText

            val et_GroupMCode = gridChild
                .findViewById<View>(R.id.et_GroupMCode) as? EditText

            val et_Voluntary = gridChild
                .findViewById<View>(R.id.et_voluntary_saving_amount) as? EditText

            if (tv_count!!.length() > 0) {
                var iCompulsary = validate!!.returnIntegerValue(tv_count.text.toString())
                var GroupMCode = validate!!.returnLongValue(et_GroupMCode!!.text.toString())
                var iVoluntary = validate!!.returnIntegerValue(et_Voluntary!!.text.toString())
                saveValue=saveData(iCompulsary,GroupMCode,iVoluntary)

            }
        }

        if(saveValue > 0){
            CDFIApplication.database?.dtmtgDao()
                ?.updatevolsave(validate!!.returnIntegerValue(tv_TotalVoluntaryValue.text.toString()),validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))

            CDFIApplication.database?.dtmtgDao()
                ?.updatecompsave(validate!!.returnIntegerValue(tv_TotalCompulsaryValue.text.toString()),validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))

            replaceFragmenty(
                fragment = MeetingTopBarZeroFragment(2),
                allowStateLoss = true,
                containerViewId = R.id.mainContent
            )
            fillRecyclerView()
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this
            )
        }
    }

    fun saveData(iValue: Int, group_m_code:Long, voluntaryAmount:Int):Int {
        var value=0
        generateMeetingViewmodel.updateCutOffSaving(
            iValue,
            voluntaryAmount,
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            group_m_code,validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
            validate!!.Daybetweentime(validate!!.currentdatetime)
        )
        value=1

        return value
    }

}