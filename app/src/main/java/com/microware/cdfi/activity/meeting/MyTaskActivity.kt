package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.microware.cdfi.R
import com.microware.cdfi.activity.MainActivityDrawer
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.activity_my_tasks.*
import kotlinx.android.synthetic.main.repay_toolbar.*

class MyTaskActivity : AppCompatActivity() {
    var validate: Validate? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_tasks)
        validate = Validate(this)
        setLabelText()


        tv_date.text=validate!!.convertDatetime(validate!!.Daybetweentime(validate!!.currentdatetime))
        ic_Back.setOnClickListener {
            val i = Intent(this, MainActivityDrawer::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)
            finish()

        }



        tbl_de_duplication.setOnClickListener {
            //var intent = Intent(this, AttendnceDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_bank_deposit.setOnClickListener {
            //var intent = Intent(this, CompulsorySavingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        tbl_upload_meetings.setOnClickListener {
            //var intent = Intent(this, VoluntorySavingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        tbl_sync_master.setOnClickListener {
            // var intent = Intent(this, WidthdrawalDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        tbl_mcp_preparation.setOnClickListener {
//            var intent = Intent(this, MCPPreparationActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        tbl_mcp_status.setOnClickListener {
            // var intent = Intent(this, RepaymentActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        tbl_loan_demand.setOnClickListener {
            // var intent = Intent(this, PeneltyDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        tbl_regular_demand_status.setOnClickListener {
            //var intent = Intent(this, GroupLoanRepaidActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        tbl_loan_repayment_schedule.setOnClickListener {
            // var intent = Intent(this, GroupLoanReceivedActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        tbl_dividend.setOnClickListener {
            //var intent = Intent(this, BankGroupTransactionActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        tbl_meeting_summary_approval.setOnClickListener {
            //var intent = Intent(this, CashBoxActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        tbl_vouchers.setOnClickListener {
            // var intent = Intent(this, GroupMeetingSummery::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }
    }

    fun setLabelText()
    {
        tv_de_duplication.text = LabelSet.getText(
            "de_duplication",
            R.string.de_duplication
        )
        tv_bank_deposit.text = LabelSet.getText(
            "bank_deposit",
            R.string.bank_deposit
        )
        tv_upload_meetings.text = LabelSet.getText(
            "upload_meetings",
            R.string.upload_meetings
        )
        tv_sync_master.text = LabelSet.getText(
            "sync_master",
            R.string.sync_master
        )
        tv_mcp_preparation.text = LabelSet.getText(
            "mcp_preparation",
            R.string.mcp_preparation
        )
        tv_mcp_status.text = LabelSet.getText(
            "mcp_status",
            R.string.mcp_status
        )
        tv_loan_demand.text = LabelSet.getText(
            "loan_demand",
            R.string.loan_demand
        )
        tv_regular_demand_status.text = LabelSet.getText(
            "regular_demand_status",
            R.string.regular_demand_status
        )
        tv_loan_repayment_schedule.text = LabelSet.getText(
            "loan_repayment_schedule",
            R.string.loan_repayment_schedule
        )
        tv_dividend.text = LabelSet.getText(
            "dividend",
            R.string.dividend
        )
        tv_meeting_summary_approval.text = LabelSet.getText(
            "meeting_summary_approval",
            R.string.meeting_summary_approval
        )
        tv_vouchers.text = LabelSet.getText(
            "vouchers",
            R.string.vouchers
        )
        tv_my_tasks.text = LabelSet.getText(
            "my_task",
            R.string.my_task
        )
        // tv_date.setText(LabelSet.getText("today_12_dec_2020", R.string.today_12_dec_2020))
        tv_title.text = LabelSet.getText(
            "my_task",
            R.string.my_task
        )
    }

    override fun onBackPressed() {
        val i = Intent(this, MainActivityDrawer::class.java)
        i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(i)
        finish()
    }
}