package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.PeneltyDetailAdapter
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import kotlinx.android.synthetic.main.activity_penelty_detail.*
import kotlinx.android.synthetic.main.activity_penelty_detail.rvList
import kotlinx.android.synthetic.main.activity_penelty_detail.tv_TotalTodayValue
import kotlinx.android.synthetic.main.activity_penelty_detail.tv_srno
import kotlinx.android.synthetic.main.buttons.*

class PeneltyDetailActivity : AppCompatActivity() {
    var validate:Validate?=null
    lateinit var peneltyDetailAdapter:PeneltyDetailAdapter
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    var Todayvalue = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_penelty_detail)
        validate= Validate(this)

        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)

        btn_save.isEnabled =
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber) >= validate!!.RetriveSharepreferenceInt(MeetingSP.maxmeetingnumber)
        btn_save.setOnClickListener {
            getPenaltyValue()
        }

        replaceFragmenty(
            fragment = MeetingTopBarFragment(6),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        btn_cancel.setOnClickListener {
            var intent = Intent(this, MeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        setLabelText()
        fillRecyclerView()
    }

    override fun onBackPressed() {
//        var intent = Intent(this, PeneltyListActivity::class.java)
        var intent = Intent(this, MeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0,0)
        finish()
    }

    fun setLabelText() {
        tv_srno.text = LabelSet.getText("sr_no", R.string.sr_no)
        tv_member_name.text = LabelSet.getText(
            "member_name",
            R.string.member_name
        )
        tv_penalty.text = LabelSet.getText(
            "penalty",
            R.string.penalty
        )
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }

    private fun fillRecyclerView() {
        Todayvalue =0
        var list = generateMeetingViewmodel.getListDataMtgByMtgnum(validate!!.RetriveSharepreferenceInt(
            MeetingSP.currentmeetingnumber),
           validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))

        peneltyDetailAdapter = PeneltyDetailAdapter(this,list)

        rvList.layoutManager = LinearLayoutManager(this)

        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = rvList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvList.layoutParams = params
        rvList.adapter = peneltyDetailAdapter
    }

    fun getTotalValue(iValue: Int,flag:Int) {
        if(flag==1) {
            Todayvalue = Todayvalue + iValue
            tv_TotalTodayValue.text = Todayvalue.toString()
        }
    }

    fun getTotalValue() {
        var iValue = 0

        val iCount = rvList.childCount


        for (i in 0 until iCount) {
            val gridChild =
                rvList.getChildAt(i) as? ViewGroup

            val tv_count = gridChild!!
                .findViewById<View>(R.id.tv_count) as? EditText

            if (tv_count!!.length() > 0) {
                iValue=iValue + validate!!.returnIntegerValue(tv_count.text.toString())
            }


        }
        tv_TotalTodayValue.text = iValue.toString()

    }

    fun getPenaltyValue() {
        var saveValue = 0

        val iCount = rvList.childCount


        for (i in 0 until iCount) {
            val gridChild =
                rvList.getChildAt(i) as? ViewGroup

            val tv_count = gridChild!!
                .findViewById<View>(R.id.tv_count) as? EditText

            val et_GroupMCode = gridChild
                .findViewById<View>(R.id.et_GroupMCode) as? EditText


            if (tv_count!!.length() > 0) {
                var iValue = validate!!.returnIntegerValue(tv_count.text.toString())
                var GroupMCode = validate!!.returnLongValue(et_GroupMCode!!.text.toString())
                saveValue=saveData(GroupMCode,iValue)

            }
        }

        if(saveValue > 0){
            replaceFragmenty(
                fragment = MeetingTopBarFragment(6),
                allowStateLoss = true,
                containerViewId = R.id.mainContent
            )
            fillRecyclerView()
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this
            )
        }
    }

    fun saveData(group_m_code:Long, penalty:Int):Int {
        var value=0
        generateMeetingViewmodel.updatePenaltySaving(
            penalty,
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
           validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            group_m_code,validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
            validate!!.Daybetweentime(validate!!.currentdatetime)
        )
        value=1

        return value
    }

}