package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.microware.cdfi.R
import kotlinx.android.synthetic.main.activity_penelty_list.*
import kotlinx.android.synthetic.main.repaytablayout.*

class PeneltyListActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_penelty_list)


        IvAttendance.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSaving.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvWithdrawal.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPenalty.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
        IvRepayment.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBankTransaction.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvAttendance.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tvSaving.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tvWithdrawal.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tvPenalty.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        tvRepayment.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tvBankTransaction.setBackgroundColor(ContextCompat.getColor(this, R.color.white))



     /*   lay_saving.setOnClickListener {
         //   var intent = Intent(this, SavingListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }*/

        lay_withdrawal.setOnClickListener {
            var intent = Intent(this, WithdrawalActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_attendance.setOnClickListener {
            var intent = Intent(this, AttendnceDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_repayment.setOnClickListener {
            var intent = Intent(this, RepaymentActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_BankTransaction.setOnClickListener {
            var intent = Intent(this, BankGroupTransactionActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        addPenality.setOnClickListener {
            var intent = Intent(this, PeneltyDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            finish()
        }
    }
}