package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.LookupViewmodel
import kotlinx.android.synthetic.main.activity_regular_loan_demand.*
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.cash_in_hand_detail_item.*
import kotlinx.android.synthetic.main.repay_toolbar.*
import kotlinx.android.synthetic.main.repaytablayout.*

class RegularLoanDemandActivity : AppCompatActivity() {
    var lookupViewmodel: LookupViewmodel? = null
    var validate: Validate? = null
    var dataspin_name_of_shg_member: List<LookupEntity>? = null
    var dataspin_purpose: List<LookupEntity>? = null
    var dataspin_source_of_fund: List<LookupEntity>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_regular_loan_demand)
        validate=Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)


        /*IvAttendance.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSaving.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvWithdrawal.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPenalty.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvRepayment.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBankTransaction.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvCashBox.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvGroupMeeting.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvExpenditurePayment.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvRecipientIncome.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))*/

        btn_cancel.setOnClickListener {
            var intent = Intent(this, MeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        ic_Back.setOnClickListener {
            var intent = Intent(this, MeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_compulsorySaving.setOnClickListener {
            var intent = Intent(this, CompulsorySavingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        lay_saving.setOnClickListener {
            var intent = Intent(this, VoluntorySavingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        lay_loan_disbursment.setOnClickListener {
            var intent = Intent(this, LoanDisbursementActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_withdrawal.setOnClickListener {
            var intent = Intent(this, WidthdrawalDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_penalty.setOnClickListener {
            var intent = Intent(this, PeneltyDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_groupLoanRepaid.setOnClickListener {
            var intent = Intent(this, GroupLoanRepaidActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_groupLoanReceived.setOnClickListener {
            var intent = Intent(this, GroupLoanReceivedActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_BankTransaction.setOnClickListener {
            var intent = Intent(this, BankGroupTransactionActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        lay_CashBox.setOnClickListener {
            var intent = Intent(this, CashBoxActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_GroupMeeting.setOnClickListener {
            var intent = Intent(this, GroupMeetingSummery::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_ExpenditurePayment.setOnClickListener {
            var intent = Intent(this, ExpenditurePaymentActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_RecipientIncome.setOnClickListener {
            var intent = Intent(this, RecipentIncomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        setLabelText()
        fillSpinner()
    }

    override fun onBackPressed() {
        var intent = Intent(this, MeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

    fun setLabelText() {
        tv_title.text = LabelSet.getText(
            "regular_loan_demand",
            R.string.regular_loan_demand
        )
        tv_meeting_no.text = LabelSet.getText(
            "meeting_no",
            R.string.meeting_no
        )
        tv_meeting_date.text = LabelSet.getText(
            "meeting_date",
            R.string.meeting_date
        )
        tv_name_of_shg_member.text = LabelSet.getText(
            "name_of_shg_member",
            R.string.name_of_shg_member
        )
        tv_proposed_amount.text = LabelSet.getText(
            "proposed_amount",
            R.string.proposed_amount
        )
        tv_purpose.text = LabelSet.getText(
            "purpose",
            R.string.purpose
        )
        tv_sanction_amount.text = LabelSet.getText(
            "sanction_date",
            R.string.sanction_date
        )
        tv_source_of_fund.text = LabelSet.getText(
            "source_of_fund",
            R.string.source_of_fund
        )
        tv_priority_no.text = LabelSet.getText(
            "priority_no",
            R.string.priority_no
        )
        tv_priority_valid_upto.text = LabelSet.getText(
            "request_valid_upto",
            R.string.request_valid_upto
        )
        tv_cash_in_hand.text = LabelSet.getText(
            "cash_in_hand",
            R.string.cash_in_hand
        )

        et_meeting_no.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_meeting_date.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )
        et_proposed_amount.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_sanction_amount.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_priority_no.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_priority_valid_upto.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )

        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }

    private fun fillSpinner() {
        dataspin_name_of_shg_member = lookupViewmodel!!.getlookup(
                99,
                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_purpose = lookupViewmodel!!.getlookup(
                99,
                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_source_of_fund = lookupViewmodel!!.getlookup(
                99,
                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        validate!!.fillspinner(this, spin_name_of_shg_member, dataspin_name_of_shg_member)
        validate!!.fillspinner(this, spin_purpose, dataspin_purpose)
        validate!!.fillspinner(this, spin_source_of_fund, dataspin_source_of_fund)
    }

}