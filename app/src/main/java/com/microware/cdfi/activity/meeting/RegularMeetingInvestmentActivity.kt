package com.microware.cdfi.activity.meeting

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.*
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.buttons.btn_cancel
import kotlinx.android.synthetic.main.buttons.btn_save
import kotlinx.android.synthetic.main.layout_regular_meeting_investment.*

class RegularMeetingInvestmentActivity  : AppCompatActivity() {

    var dtLoanGpEntity: DtLoanGpEntity? = null
    var dtLoanGpTxnEntity: DtLoanGpTxnEntity? = null
    var lookupViewmodel: LookupViewmodel? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    lateinit var dtLoanGpViewmodel: DtLoanGpViewmodel
    lateinit var dtLoanGpTxnViewmodel: DtLoanGpTxnViewmodel
    var shgBankList: List<Cbo_bankEntity>? = null

    var savingType = 0
    var validate: Validate? = null

    var dataspin_loan_source: List<LookupEntity>? = null
    var dataspin_saving_type: List<MstCOAEntity>? = null
    var shgFinancialTxnDetailEntity: ShgFinancialTxnDetailEntity? = null
    var incomeandExpenditureViewmodel: IncomeandExpenditureViewmodel? = null
    var dataspin_mode_of_payment: List<LookupEntity>? = null

    var nullStringValue:String?=null
    var nullLongValue:Long?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_regular_meeting_investment)

        validate = Validate(this)
        dtLoanGpViewmodel = ViewModelProviders.of(this).get(DtLoanGpViewmodel::class.java)
        dtLoanGpTxnViewmodel = ViewModelProviders.of(this).get(DtLoanGpTxnViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        generateMeetingViewmodel = ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        incomeandExpenditureViewmodel = ViewModelProviders.of(this).get(IncomeandExpenditureViewmodel::class.java)


        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                SaveData()

            }
        }

        btn_cancel.setOnClickListener {
            var intent = Intent(this, RegularMeetingInvestmentListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        spin_mode_of_payment?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (validate!!.returnlookupcode(
                        spin_mode_of_payment,
                        dataspin_mode_of_payment
                    ) == 2 || validate!!.returnlookupcode(
                        spin_mode_of_payment,
                        dataspin_mode_of_payment
                    ) == 3
                ) {
                    lay_bank.visibility = View.VISIBLE
                    lay_cheque_no_transactio_no.visibility = View.VISIBLE
                } else {
                    lay_bank.visibility = View.GONE
                    lay_cheque_no_transactio_no.visibility = View.GONE
                    spin_bank.setSelection(0)

                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        replaceFragmenty(
            fragment = MeetingTopBarFragment(98),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        setLabelText()
        fillSpinner()
        spin_source?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                var source = validate!!.returnLookupcode(spin_source, dataspin_loan_source)


                    if (source == 8) {
                        dataspin_saving_type = incomeandExpenditureViewmodel!!.getCoaSubHeadData(
                            listOf(46),
                            "OE",
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
                        )
                        validate!!.fillCoaSubHeadspinner(
                            this@RegularMeetingInvestmentActivity,
                            spin_saving_type,
                            dataspin_saving_type
                        )
                        spin_saving_type.setSelection(validate!!.returnSubHeadpos(savingType,dataspin_saving_type))
                    } else {
                        dataspin_saving_type = incomeandExpenditureViewmodel!!.getCoaSubHeadData(
                            listOf(25, 44, 45),
                            "OE",
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
                        )
                        validate!!.fillCoaSubHeadspinner(
                            this@RegularMeetingInvestmentActivity,
                            spin_saving_type,
                            dataspin_saving_type
                        )
                        spin_saving_type.setSelection(validate!!.returnSubHeadpos(savingType,dataspin_saving_type))
                    }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }
        fillbank()
        showData()
    }

    private fun SaveData() {

        var save = 0

        if (validate!!.RetriveSharepreferenceInt(MeetingSP.Auid) == 0) {
            shgFinancialTxnDetailEntity = ShgFinancialTxnDetailEntity(
                0,
                validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.returnSubHeadcode(spin_saving_type,dataspin_saving_type),/*receipt goes here auid mstcoa*/
                2,/*Receipt type goes here fundtype*/
                validate!!.returnLookupcode(spin_source,dataspin_loan_source),
                "OE",
                validate!!.returnIntegerValue(et_amount.text.toString()),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                0,
                validate!!.returnlookupcode(spin_mode_of_payment, dataspin_mode_of_payment),
                returaccount(),
                validate!!.returnStringValue(et_cheque_no_transactio_no.text.toString()),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                nullStringValue,
                nullLongValue, nullStringValue,
                nullLongValue
            )
            incomeandExpenditureViewmodel!!.insert(shgFinancialTxnDetailEntity!!)
            save = 1
        } else {
            incomeandExpenditureViewmodel!!.updateIncomeAndExpenditure(
                validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceInt(MeetingSP.Auid),
                2,
                validate!!.returnLookupcode(spin_source,dataspin_loan_source),
                "OE",
                validate!!.returnIntegerValue(et_amount.text.toString()),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                0,
                1,
                "",
                "",
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime)
            )

            save = 2
        }

        if (save == 1) {
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this, RegularMeetingInvestmentListActivity::class.java
            )
        } else {
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_updated_successfully",
                    R.string.data_updated_successfully
                ),
                this, RegularMeetingInvestmentListActivity::class.java
            )
        }

    }

    fun setLabelText() {
        tv_source.text = LabelSet.getText(
            "saving_source",
            R.string.saving_source
        )
        tv_saving_type.text = LabelSet.getText(
            "type_of_saving",
            R.string.type_of_saving
        )
        tv_amount.text = LabelSet.getText(
            "amount",
            R.string.amount
        )

        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )

    }

    private fun checkValidation(): Int {

        var cashinbank= generateMeetingViewmodel.getclosingbal(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), returaccount())

        var value = 1
        if (spin_source.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_source,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "saving_source",
                    R.string.saving_source
                )

            )
            value = 0
            return value
        }
        if (spin_saving_type.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_saving_type,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "type_of_saving",
                    R.string.type_of_saving
                )

            )
            value = 0
            return value
        }
        if (validate!!.returnIntegerValue(et_amount.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "pleaseenteramount",
                    R.string.pleaseenteramount
                ), this,
                et_amount
            )
            value = 0
            return value
        } else if (validate!!.returnlookupcode(spin_mode_of_payment, dataspin_mode_of_payment)==1 && et_amount.text.toString().toFloat() > validate!!.RetriveSharepreferenceInt(
                MeetingSP.Cashinhand
            )
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "inthecashbox",
                    R.string.inthecashbox
                ) , this,
                et_amount
            )
            value = 0
            return value
        }else if ((validate!!.returnlookupcode(spin_mode_of_payment, dataspin_mode_of_payment) ==2 || validate!!.returnlookupcode(spin_mode_of_payment, dataspin_mode_of_payment) ==3) && cashinbank<validate!!.returnIntegerValue(et_amount.text.toString())){
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "intheselectedbank",
                    R.string.intheselectedbank
                ), this,
                et_amount
            )
            value = 0
        }

        return value
    }

    private fun fillSpinner() {
//  listOf(1, 2)
        //itemType: List<Int>
        dataspin_loan_source = lookupViewmodel!!.getlookupMasterdata(91,validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),
            listOf(3,4,8)
        )
        dataspin_mode_of_payment = lookupViewmodel!!.getlookup(
            65,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        validate!!.fillLookupspinner(this, spin_source, dataspin_loan_source)
   //     validate!!.fillCoaSubHeadspinner(this, spin_saving_type, dataspin_saving_type)
        validate!!.fillspinner(this, spin_mode_of_payment, dataspin_mode_of_payment)
        spin_mode_of_payment.setSelection(
            validate!!.returnlookupcodepos(
                1, dataspin_mode_of_payment
            )
        )
    }

    override fun onBackPressed() {
        var intent = Intent(this, RegularMeetingInvestmentListActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

    private fun showData(){

        var list = incomeandExpenditureViewmodel!!.getInvestmentdata(validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceInt(MeetingSP.Auid),"OE",validate!!.RetriveSharepreferenceInt(MeetingSP.savingSource))
        if(!list.isNullOrEmpty()){
            spin_source.setSelection(validate!!.returnLookuppos(list.get(0).amount_to_from,dataspin_loan_source))

            savingType = list.get(0).auid

            et_amount.setText(validate!!.returnStringValue(list.get(0).amount.toString()))
            spin_mode_of_payment.setSelection(validate!!.returnlookupcodepos(
                validate!!.returnIntegerValue(list.get(0).mode_payment.toString()), dataspin_mode_of_payment))
            spin_bank.setSelection(setaccount(list.get(0).bank_code.toString()))
            et_cheque_no_transactio_no.setText(list.get(0).transaction_no)
        }else {
            spin_mode_of_payment.setSelection(validate!!.returnlookupcodepos(1, dataspin_mode_of_payment))
        }
    }

    fun fillbank() {
        shgBankList = generateMeetingViewmodel.getBankdata(validate!!.RetriveSharepreferenceString(MeetingSP.SHGGUID))

        val adapter: ArrayAdapter<String?>
        if (!shgBankList.isNullOrEmpty()) {
            val isize = shgBankList!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in shgBankList!!.indices) {
                var lastthree=shgBankList!![i].account_no.substring(shgBankList!![i].account_no.length-3)
                sValue[i + 1] =
                    shgBankList!![i].ifsc_code.toString().dropLast(7) +"XXXXX"+ lastthree
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_bank.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_bank.adapter = adapter
        }

    }

    fun returaccount(): String {

        var pos = spin_bank.selectedItemPosition
        var id = ""

        if (!shgBankList.isNullOrEmpty()) {
            if (pos > 0) id =
                shgBankList!!.get(pos - 1).ifsc_code!!.dropLast(7) + shgBankList!!.get(pos - 1).account_no
        }
        return id
    }

    fun setaccount(accountno: String): Int {

        var pos = 0

        if (!shgBankList.isNullOrEmpty()) {
            for (i in shgBankList!!.indices) {
                if (accountno.equals(
                        shgBankList!!.get(i).ifsc_code!!.dropLast(7) + shgBankList!!.get(
                            i
                        ).account_no
                    )
                )
                    pos = i + 1
            }
        }
        return pos
    }
}