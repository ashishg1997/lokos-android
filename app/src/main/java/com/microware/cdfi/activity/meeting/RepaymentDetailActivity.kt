package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.RepaymentDetailAdapter
import com.microware.cdfi.api.meetingmodel.LoanRepaymentListModel
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.replaceFragmenty
import com.microware.cdfi.viewModel.DtLoanMemberScheduleViewmodel
import com.microware.cdfi.viewModel.DtLoanTxnMemViewmodel
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import kotlinx.android.synthetic.main.activity_repayment_detail.*
import kotlinx.android.synthetic.main.buttons.*

class RepaymentDetailActivity : AppCompatActivity() {
    var validate: Validate? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    lateinit var dtLoanTxnMemViewmodel: DtLoanTxnMemViewmodel
    lateinit var dtLoanMemberScheduleViewmodel: DtLoanMemberScheduleViewmodel
    var today = 0
    var paid = 0
    var nextpayable = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repayment_detail)

        validate = Validate(this)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        dtLoanTxnMemViewmodel =
            ViewModelProviders.of(this).get(DtLoanTxnMemViewmodel::class.java)
        dtLoanMemberScheduleViewmodel =
            ViewModelProviders.of(this).get(DtLoanMemberScheduleViewmodel::class.java)
        btn_cancel.setOnClickListener {
            var intent = Intent(this, MeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        replaceFragmenty(
            fragment = MeetingTopBarFragment(8),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        setLabelText()
        fillRecyclerView()
    }

    override fun onBackPressed() {
        var intent = Intent(this, MeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

    fun setLabelText() {
        tv_Loans.text = LabelSet.getText("Loans", R.string.Loans)
        tv_member_name.text = LabelSet.getText(
            "member",
            R.string.member
        )
        tv_today_s_demand.text = LabelSet.getText(
            "todays_payable",
            R.string.todays_payable
        )
        tv_paid.text = LabelSet.getText("paid", R.string.paid)
        tv_next_demand.text = LabelSet.getText(
            "next_payable_amount",
            R.string.next_payable_amount
        )
        /* tv_code.setText(LabelSet.getText("_16649",R.string._16649))
         tv_nam.setText(LabelSet.getText("adarsh_mahila_sangam",R.string.adarsh_mahila_sangam))
         tv_date.setText(LabelSet.getText("_12_oct_2020",R.string._12_oct_2020))
         tv_title.setText(LabelSet.getText("loan_repayment",R.string.loan_repayment))*/
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }

    private fun fillRecyclerView() {
        today = 0
        paid = 0
        nextpayable = 0
        var list = generateMeetingViewmodel.getloanrepaymentList(
            validate!!.RetriveSharepreferenceInt(
                MeetingSP.currentmeetingnumber
            ),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        var repaymentDetailAdapter = RepaymentDetailAdapter(this, list)

        rvList.layoutManager = LinearLayoutManager(this)

        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = rvList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvList.layoutParams = params
        rvList.adapter = repaymentDetailAdapter
    }

    fun getTotalValue(iValue: Int, flag: Int) {
        if (flag == 1) {
            today = today + iValue
            if(today<0){
                tv_value1.text = "0"
            }else {
                tv_value1.text = today.toString()
            }
        } else if (flag == 2) {
            paid = paid + iValue
            tv_value2.text = paid.toString()
        } else if (flag == 3) {
            nextpayable = nextpayable + iValue
            tv_value3.text = nextpayable.toString()
        }
    }

    fun returnlist(memberid: Long): List<LoanRepaymentListModel>? {
        return dtLoanTxnMemViewmodel.getmemberloan(
            memberid,
            validate!!.RetriveSharepreferenceInt(
                MeetingSP.currentmeetingnumber
            )
        )
    }
    fun getnextdemand(memid:Long,loanno: Int): Int {
        return dtLoanMemberScheduleViewmodel.getnextdemand(memid,loanno
        )
    }
}