package com.microware.cdfi.activity.meeting

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.JsonObject
import com.microware.cdfi.R
import com.microware.cdfi.activity.MainActivityDrawer
import com.microware.cdfi.adapter.SHGMeetingListAdapter
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.api.meetingdatamodel.DownloadMeetingStatus
import com.microware.cdfi.api.model.SHGMeetingModel
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.DtmtgEntity
import com.microware.cdfi.entity.VillageEntity
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_shgmeeting_list.*
import kotlinx.android.synthetic.main.activity_shgmeeting_list.tv_activegroup
import kotlinx.android.synthetic.main.activity_shgmeeting_list.tv_date
import kotlinx.android.synthetic.main.activity_shgmeeting_list.tv_inactivegroup
import kotlinx.android.synthetic.main.activity_shgmeeting_list.tv_pendingapproval
import kotlinx.android.synthetic.main.customealertdialogelogin.*
import kotlinx.android.synthetic.main.customealertdialogelogin.view.*
import kotlinx.android.synthetic.main.meeting_detail_item.*
import kotlinx.android.synthetic.main.repay_toolbar.*
import org.apache.http.conn.ConnectTimeoutException
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException


class SHGMeetingListActivity : AppCompatActivity() {
    internal lateinit var progressDialog: ProgressDialog
    var  apiInterface: ApiInterface? = null
    var shgviewmodel: SHGViewmodel? = null
    var memberviewmodel: Memberviewmodel? = null
    var generateMeetingViewmodel: GenerateMeetingViewmodel? = null
    var responseViewModel: ResponseViewModel? = null
    var locationViewModel: LocationViewModel? = null
    var villageData: List<VillageEntity>? = null
    var validate: Validate? = null
    var sStateCode = 0
    var sDistrictCode = 0
    var sBlockCode = 0
    var sPanchayatCode = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shgmeeting_list)

        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()

        shgviewmodel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        memberviewmodel = ViewModelProviders.of(this).get(Memberviewmodel::class.java)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        locationViewModel = ViewModelProviders.of(this).get(LocationViewModel::class.java)
        responseViewModel = ViewModelProviders.of(this).get(ResponseViewModel::class.java)
        validate = Validate(this)
        sStateCode = validate!!.RetriveSharepreferenceInt(AppSP.statecode)
        sDistrictCode = validate!!.RetriveSharepreferenceInt(AppSP.districtcode)
        sBlockCode = validate!!.RetriveSharepreferenceInt(AppSP.blockcode)
        sPanchayatCode = validate!!.RetriveSharepreferenceInt(AppSP.panchayatcode)
        var totalmtg =
            generateMeetingViewmodel!!.gettotmeetingCount()
        //  tv_meeting2.text = totalmtg.toString()
        tv_date.text =
            validate!!.convertDatetime(validate!!.Daybetweentime(validate!!.currentdatetime))

        ic_Back.setOnClickListener {
            var intent = Intent(this, MainActivityDrawer::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_Missed.setOnClickListener {
            bindMeetingList(0, 1)
        }
        lay_Current.setOnClickListener {
            bindMeetingList(0, 2)
        }

        lay_Upcoming.setOnClickListener {
            bindMeetingList(0, 3)
        }
        spin_village?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                var villageid = validate!!.returnVillageID(spin_village, villageData)
                if (position > 0) {
                    bindMeetingList(villageid, 0)
                } else {
                    bindMeetingList(0, 0)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }
        //    setMeetingCounts()
        getCounts()
        bindMeetingList(0, 0)
        bindVillageSpinner(sStateCode, sDistrictCode, sBlockCode, sPanchayatCode)

        setLabelText()
//        updateShgType()
        //  fillRecyclerView()
    }

    fun fillDataall() {
        var SHGList = generateMeetingViewmodel!!.getgroupMeetingsdatalist()
        if (SHGList != null) {
            rvList.visibility = View.VISIBLE
            rvList.layoutManager = LinearLayoutManager(this@SHGMeetingListActivity)
            rvList.adapter = SHGMeetingListAdapter(this@SHGMeetingListActivity, SHGList)
        } else {
            rvList.visibility = View.INVISIBLE
        }


    }


    fun returnmeetingdata(shgid: Long): List<DtmtgEntity>? {
        return generateMeetingViewmodel!!.getMtglistdata(shgid)
    }

    fun returnmeetingstatus(shgid: Long, Mtgno: Int): String {
        return generateMeetingViewmodel!!.returnmeetingstatus(shgid, Mtgno)
    }
    fun openmeeting(shgid: Long, Mtgno: Int) {

        return generateMeetingViewmodel!!.openMeeting("O",Mtgno,shgid)
    }
    fun bindVillageSpinner(
        stateCode: Int,
        districtCode: Int,
        blockCode: Int,
        panchayatCode: Int
    ) {
        villageData = locationViewModel?.getVillagedata_by_panchayatCode(
            stateCode,
            districtCode,
            blockCode,
            panchayatCode
        )
        if (villageData != null) {
            val iGen = villageData!!.size
            val name = arrayOfNulls<String>(iGen + 1)
            name[0] = LabelSet.getText("all", R.string.all)

            for (i in 0 until villageData!!.size) {
                name[i + 1] = villageData!!.get(i).village_name_en
            }
            val adapter_category = ArrayAdapter<String>(
                this, R.layout.my_spinner_space, name
            )
            adapter_category.setDropDownViewResource(R.layout.my_spinner)
            spin_village?.adapter = adapter_category

        }


    }


    fun getmembercount(shgguid: String): Int {
        return memberviewmodel!!.getcount(shgguid)
    }

    fun fillData(villageid: Int) {

        var SHGList = generateMeetingViewmodel!!.getgroupMeetingsdatalistbyvillage(villageid)

        if (SHGList != null) {
            rvList.visibility = View.VISIBLE
            rvList.layoutManager = LinearLayoutManager(this@SHGMeetingListActivity)
            rvList.adapter = SHGMeetingListAdapter(this@SHGMeetingListActivity, SHGList)
        } else {
            rvList.visibility = View.INVISIBLE
        }
        var pendingcount = shgviewmodel!!.getShgpendingCount(villageid)

    }

    override fun onBackPressed() {
        var intent = Intent(this, MainActivityDrawer::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

    fun setLabelText() {
        tv_title.text = LabelSet.getText(
            "shg",
            R.string.my_meeting
        )
        // tv_village.setText(LabelSet.getText("village", R.string.village))
        tv_pendingapproval.text = LabelSet.getText(
            "pending_approval_1",
            R.string.pending_approval_1
        )
        tv_activegroup.text = LabelSet.getText(
            "active_group_1",
            R.string.active_group_1
        )
        tv_inactivegroup.text = LabelSet.getText(
            "inactive_group_1",
            R.string.inactive_group_1
        )
    }

    fun bindMeetingList(villageid: Int, iFlag: Int) {
        var SHGList: ArrayList<SHGMeetingModel>? = null
        if (iFlag == 0) {
            SHGList =
                generateMeetingViewmodel!!.getgroupMeetingsdatalist() as ArrayList<SHGMeetingModel>
        } else if (iFlag == 1) {
            SHGList =
                generateMeetingViewmodel!!.getMissedMeetingslist(validate!!.Daybetweentime(validate!!.currentdate)) as ArrayList<SHGMeetingModel>
        } else if (iFlag == 2) {
            SHGList =
                generateMeetingViewmodel!!.getCurrentMeetingslist(validate!!.Daybetweentime(validate!!.currentdate)) as ArrayList<SHGMeetingModel>
        } else if (iFlag == 3) {
            SHGList = generateMeetingViewmodel!!.getUpcomingMeetingslist(
                validate!!.Daybetweentime(validate!!.currentdate)
            ) as ArrayList<SHGMeetingModel>
        }

        if (SHGList != null) {
            rvList.visibility = View.VISIBLE
            rvList.layoutManager = LinearLayoutManager(this@SHGMeetingListActivity)
            rvList.adapter = SHGMeetingListAdapter(this@SHGMeetingListActivity, SHGList)
        } else {
            rvList.visibility = View.INVISIBLE
        }
    }

    fun getCounts() {

        var currentMeetingList =
            generateMeetingViewmodel!!.getCurrentMeetingslist(validate!!.Daybetweentime(validate!!.currentdate))
        var missedMeetingList =
            generateMeetingViewmodel!!.getMissedMeetingslist(validate!!.Daybetweentime(validate!!.currentdate))
        var upcomingMeetingList =
            generateMeetingViewmodel!!.getUpcomingMeetingslist(validate!!.Daybetweentime(validate!!.currentdate))
        if (!currentMeetingList.isNullOrEmpty()) {
            tv_meeting2.text = currentMeetingList.size.toString()
        } else {
            tv_meeting2.text = "0"
        }
        if (!missedMeetingList.isNullOrEmpty()) {
            tv_meeting1.text = missedMeetingList.size.toString()
        } else {
            tv_meeting1.text = "0"
        }

        if (!upcomingMeetingList.isNullOrEmpty()) {
            tv_meeting3.text = upcomingMeetingList.size.toString()
        } else {
            tv_meeting3.text = "0"
        }

    }

    fun getMeetingCountByCboId(shgid: Long): Int {

        return validate!!.returnIntegerValue(generateMeetingViewmodel!!.getMeetingCount(shgid).toString())
    }

    fun importSHGMeetinglist() {
        progressDialog = ProgressDialog.show(
            this, LabelSet.getText("app_name", R.string.app_name),
            LabelSet.getText("DataLoading", R.string.DataLoading)
        )
        progressDialog.setIcon(this.getDrawable(R.drawable.appicon))
        var idownload = 0
        var msg = ""
        var code = 0
        object : Thread() {

            //@SuppressLint("NewApi")
            override fun run() {
                try {

                    val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                    val token =  validate!!.returnStringValue(AESEncryption.decrypt(
                        validate!!.RetriveSharepreferenceString(AppSP.token),
                        validate!!.RetriveSharepreferenceString(AppSP.userid)))
                    val shgID = validate!!.RetriveSharepreferenceLong(AppSP.shgid)

                    val call1 = apiInterface?.getShgMeeinglist(
                        token,
                        user,
                        shgID,
                        4
                    )

                    val res1 = call1?.execute()

                    if (res1!!.isSuccessful) {
                        try {
                            deleteMeetingData()
                            msg = "" + res1.code() + " " + res1.message()

                            if (res1.body()?.downLoadMtgList != null) {
                                try {
                                    var dowLoadMtgList = res1.body()?.downLoadMtgList

                                    for (i in 0 until dowLoadMtgList!!.size){
                                        var shgMeetingList = dowLoadMtgList[i].shgMeeting
                                        var shgGpLoanList = dowLoadMtgList[i].shgGroupLoanList
                                        var shgmemLoanList = dowLoadMtgList[i].shgMemberLoanList
                                        var loanApplicationLIst = dowLoadMtgList[i].shgLoanApplicationList
                                        var mcpList = dowLoadMtgList[i].shgMcpList

                                        var meetingList = MappingData.returnmtgObjentity(shgMeetingList)
                                        var gploanList = MappingData.returnGpmtgObjentity(shgGpLoanList)
                                        var memloanList = MappingData.returnmemmtgObjentity(shgmemLoanList)
                                        var loanappList = MappingData.returnloanAppliObjentity(loanApplicationLIst)
                                        var shgmcpData = MappingData.returnmcpObjentity(mcpList)

//                                        generateMeetingViewmodel!!.insert(meetingList!!)
                                        CDFIApplication.database?.dtmtgDao()?.insertDtmtg1(meetingList)

                                        var shgMeetingDetailsListData = shgMeetingList!!.shgMeetingDetailsList
                                        var shgFinanceTransactionListData = shgMeetingList.shgFinanceTransactionList
                                        var shgFinanceTransactionDetailGroupListData = shgMeetingList.shgFinanceTransactionDetailGroupList
                                        var shgGroupLoanTransactionData = shgMeetingList.shgGroupLoanTransaction
                                        var shgFinTxnVoucherData = shgMeetingList.shgFinanceTransactionVouchersList

                                        for(j in 0 until shgMeetingDetailsListData?.size!!){
                                            shgMeetingDetailsListData[j].cbo_id = dowLoadMtgList[i].shgMeeting!!.cbo_id
                                            shgMeetingDetailsListData[j].mtg_no = dowLoadMtgList[i].shgMeeting!!.mtg_no
                                            shgMeetingDetailsListData[j].mtg_guid = dowLoadMtgList[i].shgMeeting!!.mtg_guid
                                            shgMeetingDetailsListData[j].mtg_date = dowLoadMtgList[i].shgMeeting!!.mtg_date
                                        }

                                        for(j in 0 until shgFinanceTransactionListData?.size!!){
                                            shgFinanceTransactionListData[j].cbo_id = dowLoadMtgList[i].shgMeeting!!.cbo_id
                                            shgFinanceTransactionListData[j].mtg_no =  dowLoadMtgList[i].shgMeeting!!.mtg_no
                                            shgFinanceTransactionListData[j].mtg_guid = dowLoadMtgList[i].shgMeeting!!.mtg_guid
                                        }

                                        for(j in 0 until shgFinanceTransactionDetailGroupListData?.size!!){
                                            shgFinanceTransactionDetailGroupListData[j].cbo_id = dowLoadMtgList[i].shgMeeting!!.cbo_id
                                            shgFinanceTransactionDetailGroupListData[j].mtg_no = dowLoadMtgList[i].shgMeeting!!.mtg_no
                                            shgFinanceTransactionDetailGroupListData[j].mtg_guid = dowLoadMtgList[i].shgMeeting!!.mtg_guid
                                        }

                                        for(j in 0 until shgGroupLoanTransactionData?.size!!){
                                            shgGroupLoanTransactionData[j].cbo_id = dowLoadMtgList[i].shgMeeting!!.cbo_id
                                            shgGroupLoanTransactionData[j].mtg_no = dowLoadMtgList[i].shgMeeting!!.mtg_no
                                            shgGroupLoanTransactionData[j].mtg_guid = dowLoadMtgList[i].shgMeeting!!.mtg_guid
                                            shgGroupLoanTransactionData[j].mtg_date = dowLoadMtgList[i].shgMeeting!!.mtg_date
                                        }
                                        for(j in 0 until shgFinTxnVoucherData?.size!!){
                                            shgFinTxnVoucherData[j].cbo_id = dowLoadMtgList[i].shgMeeting!!.cbo_id
                                            shgFinTxnVoucherData[j].mtg_no = dowLoadMtgList[i].shgMeeting!!.mtg_no
                                        }

                                        var shgMeetingDetailsList = MappingData.returnshgMeetingDetailObjentity(shgMeetingDetailsListData)
                                        var shgFinanceTransactionList = MappingData.returnshgshgFinanceTxnObjentity(shgFinanceTransactionListData)
                                        var shgFinanceTransactionDetailGroupList = MappingData.returnshgFinanceTxnDtGroupObjentity(shgFinanceTransactionDetailGroupListData)
                                        var shgGroupLoanTransaction = MappingData.returnshgGrpLoanTxnObjentity(shgGroupLoanTransactionData)
                                        var shgFinTxnVoucherListData = MappingData.returnshgfINTxnvoucherObjentity(shgFinTxnVoucherData)
                                        if (!shgFinTxnVoucherListData.isNullOrEmpty()){
                                            CDFIApplication.database?.vouchersDao()?.insertVoucherList(shgFinTxnVoucherListData)
                                        }
                                        if (!shgMeetingDetailsList.isNullOrEmpty()) {
                                            CDFIApplication.database?.dtdetmtgDao()?.insertAlldtmtgDet(shgMeetingDetailsList)
                                        }

                                        if (!shgFinanceTransactionList.isNullOrEmpty()) {
                                            CDFIApplication.database?.dtMtgFinTxnDao()?.insertMtgFinTxnAllData(shgFinanceTransactionList)
                                        }
                                        if (!shgFinanceTransactionDetailGroupList.isNullOrEmpty()) {
                                            CDFIApplication.database?.incomeandExpendatureDao()?.insertIncomeAndExpenditureAllData(shgFinanceTransactionDetailGroupList)
                                        }

                                        if (!shgGroupLoanTransaction.isNullOrEmpty()) {
                                            CDFIApplication.database?.dtLoanGpTxnDao()?.insertLoanGpTxnAllData(shgGroupLoanTransaction)
                                        }

                                        for(j in 0 until shgMeetingDetailsListData.size){
                                            var shgMemberLoanTxnList = shgMeetingDetailsListData[j].shgMemberLoanTransactionList
                                            var shgFinanceTxnDetailMemList = shgMeetingDetailsListData[j].shgFinanceTransactionDetailMemberList

                                            for(k in 0 until shgMemberLoanTxnList!!.size){
                                                shgMemberLoanTxnList[k].cbo_id = shgMeetingDetailsListData[j].cbo_id
                                                shgMemberLoanTxnList[k].mtg_no = shgMeetingDetailsListData[j].mtg_no
                                                shgMemberLoanTxnList[k].mtg_guid = shgMeetingDetailsListData[j].mtg_guid
                                                shgMemberLoanTxnList[k].mtg_date = shgMeetingDetailsListData[j].mtg_date
                                                shgMemberLoanTxnList[k].mem_id = shgMeetingDetailsListData[j].mem_id
                                            }

                                            for(k in 0 until shgFinanceTxnDetailMemList!!.size){
                                                shgFinanceTxnDetailMemList[k].cbo_id = shgMeetingDetailsListData[j].cbo_id
                                                shgFinanceTxnDetailMemList[k].mtg_no = shgMeetingDetailsListData[j].mtg_no
                                                shgFinanceTxnDetailMemList[k].mtg_guid = shgMeetingDetailsListData[j].mtg_guid
                                                shgFinanceTxnDetailMemList[k].mem_id = shgMeetingDetailsListData[j].mem_id
                                            }

                                            var shgMemberLoanTxnData = MappingData.returnshgmemLoanTxnObjentity(shgMemberLoanTxnList)
                                            var shgFinanceTxnDetailMemData = MappingData.returnFinanceTxnDtMemberObjentity(shgFinanceTxnDetailMemList)

                                            if (!shgMemberLoanTxnData.isNullOrEmpty()) {
                                                CDFIApplication.database?.dtLoanTxnMemDao()?.insertLoanTxnMemAllData(shgMemberLoanTxnData)
                                            }

                                            if (!shgFinanceTxnDetailMemData.isNullOrEmpty()) {
                                                CDFIApplication.database?.dtFinTxnDetDao()?.insertMtgFinTxnAllData(shgFinanceTxnDetailMemData)
                                            }

                                            var shgSettelmentDetailList = shgMeetingDetailsListData[j].shgMemberSettlementList
                                          if(!shgSettelmentDetailList.isNullOrEmpty()){
                                            for(k in 0 until shgSettelmentDetailList.size){
                                                shgSettelmentDetailList[k].cbo_id = shgMeetingDetailsListData[j].cbo_id
                                                shgSettelmentDetailList[k].mtg_no = shgMeetingDetailsListData[j].mtg_no
                                                shgSettelmentDetailList[k].mtg_guid = shgMeetingDetailsListData[j].mtg_guid
                                                shgSettelmentDetailList[k].mem_id = shgMeetingDetailsListData[j].mem_id
                                            }
                                            var shgSettelMentListData = MappingData.returnShgMemSettelmentObjentity(shgSettelmentDetailList)
                                            if (shgSettelMentListData.isNullOrEmpty()){
                                                CDFIApplication.database?.memSettlementDao()?.insertSettlement(shgSettelMentListData)
                                            }}

                                        }

                                        if (!gploanList.isNullOrEmpty()){
                                            CDFIApplication.database?.dtLoanGpDao()?.insertLoanGpAllData(gploanList)
                                        }

                                        if (!memloanList.isNullOrEmpty()){
                                            CDFIApplication.database?.dtLoanMemberDao()?.insertLoanMemberAllData(memloanList)
                                        }

                                        if (!loanappList.isNullOrEmpty()){
                                            CDFIApplication.database?.dtLoanApplicationDao()?.insertLoanApplicationAllData(loanappList)
                                        }

                                        if(!shgmcpData.isNullOrEmpty()){
                                            CDFIApplication.database?.shgmcpDao()?.insertShgMcpAllData(shgmcpData)
                                        }

                                        for (l in 0 until shgGpLoanList!!.size){
                                            var shgGroupLoanSchedule = shgGpLoanList[l].shgGroupLoanSchedule
                                            if(!shgGroupLoanSchedule.isNullOrEmpty()) {
                                                for (j in 0 until shgGroupLoanSchedule.size) {
                                                    shgGroupLoanSchedule[j].cbo_id =
                                                        shgGpLoanList[l].cbo_id
                                                /*    shgGroupLoanSchedule[j].mtg_no =
                                                        shgGpLoanList[l].mtg_no
                                                    shgGroupLoanSchedule[j].mtg_guid =
                                                        shgGpLoanList[l].mtg_guid*/
                                                }
                                                var shgGpScheduleList = MappingData.returngploanScheduleObjentity(shgGroupLoanSchedule)
                                                if (!shgGpScheduleList.isNullOrEmpty()) {
                                                    CDFIApplication.database?.dtMtgGrpLoanScheduleDao()?.insertMtgGrpLoanScheduleAllData(shgGpScheduleList)
                                                }
                                            }


                                        }

                                        for (k in 0 until shgmemLoanList!!.size) {
                                            var shgmemLoanSchedule =
                                                shgmemLoanList[k].shgMeberLoanSchedule
                                            if(!shgmemLoanSchedule.isNullOrEmpty()){
                                                for (m in 0 until shgmemLoanSchedule.size) {
                                                    shgmemLoanSchedule[m].cbo_id =
                                                        shgmemLoanList[k].cbo_id
                                                /*    shgmemLoanSchedule[m].mtg_guid =
                                                        shgmemLoanList[k].mtg_guid*/
                                                }
                                                var shgmebScheduleList = MappingData.returnmembloanScheduleObjentity(shgmemLoanSchedule)
                                                if (!shgmebScheduleList.isNullOrEmpty()) {
                                                    CDFIApplication.database?.dtLoanMemberScheduleDao()?.insertLoanMemberScheduleAllData(shgmebScheduleList)
                                                }
                                            }


                                        }

                                    }

                                } catch (ex: Exception) {
                                    ex.printStackTrace()
                                }
                            }

                            idownload = 1

                            progressDialog.dismiss()

                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }

                    } else {
                        var resMsg = ""
                        if (res1.code() == 403) {
                            code = res1.code()
                            msg = res1.message()
                        } else {
                            if (res1.errorBody()?.contentLength() == 0L || res1.errorBody()
                                    ?.contentLength()!! < 0L
                            ) {
                                code = res1.code()
                                resMsg = res1.message()
                            } else {
                                var jsonObject1 =
                                    JSONObject(res1.errorBody()!!.source().readUtf8().toString())

                                code =
                                    validate!!.returnIntegerValue(
                                        jsonObject1.optString("responseCode").toString()
                                    )
                                resMsg = validate!!.returnStringValue(
                                    jsonObject1.optString("responseMsg").toString()
                                )
                            }
                            msg = "" + validate!!.alertMsg(
                                this@SHGMeetingListActivity,
                                responseViewModel,
                                code,
                                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                                resMsg
                            )

                        }

                    }

                    runOnUiThread {
                        if (idownload == 1) {
                            progressDialog.dismiss()
                            val text = LabelSet.getText(
                                "Datadownloadedsuccessfully",
                                R.string.Datadownloadedsuccessfully
                            )
                            validate!!.CustomAlert(text, this@SHGMeetingListActivity)
                            bindMeetingList(0, 0)
                        } else {
                            progressDialog.dismiss()
                            if (code == 403) {
                                Toast.makeText(
                                    this@SHGMeetingListActivity,
                                    msg,
                                    Toast.LENGTH_LONG
                                )
                                    .show()
                                CustomAlertlogin()
                            } else {
                                val text = msg
                                validate!!.CustomAlert(text, this@SHGMeetingListActivity)
                                Toast.makeText(
                                    this@SHGMeetingListActivity,
                                    msg,
                                    Toast.LENGTH_LONG
                                )
                                    .show()
                            }
                        }

                    }

                } catch (bug: SocketTimeoutException) {
                    progressDialog.dismiss()
                } catch (bug: ConnectTimeoutException) {
                    progressDialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                    progressDialog.dismiss()
                }
            }

        }.start()
    }

    fun importMeeting_Approval_Rejection_status() {

        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText("DataLoading", R.string.DataLoading)
        )

        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
        val token =  validate!!.returnStringValue(AESEncryption.decrypt(
            validate!!.RetriveSharepreferenceString(AppSP.token),
            validate!!.RetriveSharepreferenceString(AppSP.userid)))
        val shgId = validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        val callCount = apiInterface?.getMeetingApprove_Reject_Status(
            token,
            user,
            shgId
        )

        callCount?.enqueue(object : Callback<List<DownloadMeetingStatus>> {
            override fun onFailure(callCount: Call<List<DownloadMeetingStatus>>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<List<DownloadMeetingStatus>>,
                response: Response<List<DownloadMeetingStatus>>
            ) {

                progressDialog.dismiss()
                if (response.isSuccessful) {
                    if (!response.body()!!.isNullOrEmpty()) {
                        try {
                            var list = response.body()!!

                            for (i in 0 until list.size) {

                                generateMeetingViewmodel!!.updateMtgApprova_Rejection_Status(
                                    list.get(i).mtgNo,
                                    list.get(i).cboId,
                                    validate!!.Daybetweentime(validate!!.currentdatetime),
                                    validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                                    list.get(i).approvalStatus,
                                    list.get(i).checkerRemarks)
                            }
                            val text = LabelSet.getText(
                                "Datadownloadedsuccessfully",
                                R.string.Datadownloadedsuccessfully
                            )
                            validate!!.CustomAlert(text,this@SHGMeetingListActivity)
                            bindMeetingList(0, 0)

                        } catch (ex: Exception) {
                            ex.printStackTrace()
                            //                                    progressDialog.dismiss()
                        }
                    }
                } else {
                    progressDialog.dismiss()
                    var resCode = 0
                    var resMsg = ""
                    if (response.code() == 403) {

                        CustomAlertlogin()
                    } else {
                        if (response.errorBody()?.contentLength() == 0L || response.errorBody()
                                ?.contentLength()!! < 0L
                        ) {
                            resCode = response.code()
                            resMsg = response.message()
                        } else {
                            var jsonObject1 =
                                JSONObject(response.errorBody()!!.source().readUtf8().toString())

                            resCode =
                                validate!!.returnIntegerValue(
                                    jsonObject1.optString("responseCode").toString()
                                )
                            resMsg = validate!!.returnStringValue(
                                jsonObject1.optString("responseMsg").toString()
                            )
                        }

                        validate!!.CustomAlertMsg(
                            this@SHGMeetingListActivity,
                            responseViewModel,
                            resCode,
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!, resMsg
                        )

                    }

                }


            }

        })
    }

    fun CustomAlertlogin() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogelogin, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        val password =  validate!!.returnStringValue(AESEncryption.decrypt(
            validate!!.RetriveSharepreferenceString(AppSP.Password),
            validate!!.RetriveSharepreferenceString(AppSP.userid)))
        mDialogView.etUsername.setText(validate!!.RetriveSharepreferenceString(AppSP.userid))
        mDialogView.etPassword.setText(password)
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_dialog_title2.text = LabelSet.getText("app_name",R.string.app_name)
        mDialogView.btn_login.text = LabelSet.getText("log_in",R.string.log_in)
        mDialogView.chk_showHide1.text = LabelSet.getText("show_password",R.string.show_password)
        mDialogView.btn_login.setOnClickListener {
            mAlertDialog.dismiss()
            importtoken(
                mDialogView.etUsername.text.toString(),
                validate!!.returnStringValue(
                    AESEncryption.encrypt(validate!!.returnStringValue(
                        mDialogView.etPassword.text.toString()),mDialogView.etUsername.text.toString()))
            )
        }
        mAlertDialog.chk_showHide1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                mAlertDialog.etPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                mAlertDialog.etPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
        }

    }

    fun importtoken(user: String, pass: String) {
        var grant_type = "password"
        var userId = user
        var password = pass
        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText("authenticate_user", R.string.authenticate_user)
        )
        val callCount = apiInterface?.gettokenforce(
            grant_type,
            userId,
            password, validate!!.RetriveSharepreferenceInt(AppSP.State_Selected), 1,
            validate!!.RetriveSharepreferenceString(AppSP.Roleid)!!
        )
        callCount?.enqueue(object : Callback<JsonObject> {
            override fun onFailure(callCount: Call<JsonObject>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                //       Log.e("", response!!.body().toString())
                progressDialog.dismiss()


                if (response.isSuccessful) {

                    var jsonObject1: JSONObject? = null
                    try {
                        jsonObject1 = JSONObject(response.body().toString())

                        val auth = jsonObject1.get("accessToken").toString()
                        if (auth.trim().length > 0) {
                            validate!!.SaveSharepreferenceString(
                                AppSP.token,
                                validate!!.returnStringValue(
                                    AESEncryption.encrypt(
                                        auth,validate!!.RetriveSharepreferenceString(AppSP.userid)))
                            )
                        }
                        validate!!.CustomAlert(
                            LabelSet.getText(
                                "userauthenticatedsuccessfully",
                                R.string.userauthenticatedsuccessfully
                            ), this@SHGMeetingListActivity
                        )
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        var jsonObject1 =
                            JSONObject(response.errorBody()!!.source().readUtf8().toString())

                        var resCode =
                            validate!!.returnIntegerValue(
                                jsonObject1.optString("responseCode").toString()
                            )
                        validate!!.CustomAlertMsg(
                            this@SHGMeetingListActivity,
                            responseViewModel,
                            resCode,
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                            validate!!.returnStringValue(
                                jsonObject1.optString("responseMsg").toString()
                            )
                        )
                    }

                } else {
                    var jsonObject1 =
                        JSONObject(response.errorBody()!!.source().readUtf8().toString())

                    var resCode =
                        validate!!.returnIntegerValue(
                            jsonObject1.optString("responseCode").toString()
                        )
                    validate!!.CustomAlertMsg(
                        this@SHGMeetingListActivity,
                        responseViewModel,
                        resCode,
                        validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                        validate!!.returnStringValue(
                            jsonObject1.optString("responseMsg").toString()
                        )
                    )
                }


            }

        })


    }

    fun deleteMeetingData(){
        /*Table1*/
        generateMeetingViewmodel!!.deleteShg_MtgByCboId(validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        /*Table2*/
        generateMeetingViewmodel!!.deleteShgDetailDataByCboId(validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        /*Table3*/
        generateMeetingViewmodel!!.deleteFinancialTransactionMemberDetailDataByCboId(validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        /*Table4*/
        generateMeetingViewmodel!!.deleteGrpFinancialTxnDetailDataByCboId(validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        /*Table5*/
        generateMeetingViewmodel!!.deleteShgFinancialTxnDataByCboId(validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        /*Table6*/
        generateMeetingViewmodel!!.deleteMemberLoanTxnDataByCboId(validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        /*Table7*/
        generateMeetingViewmodel!!.deleteGroupLoanDataByCboId(validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        /*Table8*/
        generateMeetingViewmodel!!.deleteGroupLoanTxnDataByCboId(validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        /*Table9*/
        generateMeetingViewmodel!!.deleteMCPDataByCboId(validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        /*Table10*/
        generateMeetingViewmodel!!.deleteMemberLoanByCboId(validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        /*Table11*/
        generateMeetingViewmodel!!.deleteShgLoanApplicationByCboId(validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        /*Table12*/

        generateMeetingViewmodel!!.deleteMemberSubinstallmentDataByCboId(validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
        /*Table13*/

        generateMeetingViewmodel!!.deleteGroupSubinstallmentDataByCboId(validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))


    }

    fun sCheckValidation(frequency:Int,shgId:Long):Int{
        var value = 1
        var nonApprovedMeetings = generateMeetingViewmodel!!.getNonApprovedShgsCount(shgId)
        var nonSynchedMeetings = generateMeetingViewmodel!!.getNonSynchedShgsCount(shgId)
        val lastMeetingStatus = generateMeetingViewmodel!!.getMeetingStatus(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),validate!!.RetriveSharepreferenceInt(MeetingSP.GroupLastMeetingNumber)
        )

        if (lastMeetingStatus == 3){
            validate!!.CustomAlert(LabelSet.getText("not_meeting_cannot_be_generated",R.string.new_meeting_cannot_be_generated),this)
            value = 0
        }else if(frequency == 1 && (nonApprovedMeetings>7 || nonSynchedMeetings >3)){
            //Weekly
                validate!!.CustomAlert(LabelSet.getText("new_mtg_not_allowed",R.string.new_mtg_not_allowed),this)
            value = 0
            return value
        }else if(frequency == 2 && (nonApprovedMeetings>5 || nonSynchedMeetings >2)) {
            //Fortnightly
            value = 0
            return value
        }else if(frequency == 3 && (nonApprovedMeetings>1 || nonSynchedMeetings >2))    {
            //Monthly
            value = 0
            return value
        }


        return value
    }
}


/*private fun insertLookupData(){
    INSERT INTO `LookupMaster` VALUES(1,'designation',0,'Select',1,'Select',0,'en');
    INSERT INTO `LookupMaster` VALUES(2,'designation',1,'President',1,'President',1,'en');
}*/
/* tv_meeting1.setText(missedMeetingCount.toString())
                tv_meeting2.setText(currentMeetingCount.toString())
                tv_meeting3.setText(upcomingMeetingCount.toString())*/

/*,( CASE WHEN tblgroup.meeting_frequency = 1 and mtgdate>0 then dtMtg.MtgDate+(7 * 24 * 60 * 60)  WHEN tblgroup.meeting_frequency = 2 and mtgdate>0 then dtMtg.MtgDate+(14 * 24 * 60 * 60)  WHEN tblgroup.meeting_frequency = 3 and  mtgdate>0 then dtMtg.MtgDate+(28 * 24 * 60 * 60) WHEN mtgdate==0 then :c_date END)as MtgDate*/