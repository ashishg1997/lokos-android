package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.LookupViewmodel
import kotlinx.android.synthetic.main.activity_shg_withdrawal.*
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.cash_in_hand_detail_item.*
import kotlinx.android.synthetic.main.repay_toolbar.*
import kotlinx.android.synthetic.main.repaytablayout.*

class SHGWithdrawalActivity : AppCompatActivity() {
    var lookupViewmodel: LookupViewmodel? = null
    var validate: Validate? = null
    var dataspin_name_of_member: List<LookupEntity>? = null
    var dataspin_type_of_saving_deposit: List<LookupEntity>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shg_withdrawal)
        validate=Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)


        /*IvAttendance.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSaving.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvWithdrawal.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPenalty.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvRepayment.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBankTransaction.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvCashBox.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvGroupMeeting.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvExpenditurePayment.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvRecipientIncome.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))*/

        btn_cancel.setOnClickListener {
            var intent = Intent(this, MeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        ic_Back.setOnClickListener {
            var intent = Intent(this, MeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_compulsorySaving.setOnClickListener {
            var intent = Intent(this, CompulsorySavingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        lay_saving.setOnClickListener {
            var intent = Intent(this, VoluntorySavingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        lay_loan_disbursment.setOnClickListener {
            var intent = Intent(this, LoanDisbursementActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_withdrawal.setOnClickListener {
            var intent = Intent(this, WidthdrawalDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_penalty.setOnClickListener {
            var intent = Intent(this, PeneltyDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_groupLoanRepaid.setOnClickListener {
            var intent = Intent(this, GroupLoanRepaidActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_groupLoanReceived.setOnClickListener {
            var intent = Intent(this, GroupLoanReceivedActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_BankTransaction.setOnClickListener {
            var intent = Intent(this, BankGroupTransactionActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        lay_CashBox.setOnClickListener {
            var intent = Intent(this, CashBoxActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_GroupMeeting.setOnClickListener {
            var intent = Intent(this, GroupMeetingSummery::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_ExpenditurePayment.setOnClickListener {
            var intent = Intent(this, ExpenditurePaymentActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_RecipientIncome.setOnClickListener {
            var intent = Intent(this, RecipentIncomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        setLabelText()
        fillSpinner()
    }

    override fun onBackPressed() {
        var intent = Intent(this, MeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

    fun setLabelText() {
        tv_title.text = LabelSet.getText(
            "withdrawal",
            R.string.withdrawal
        )
        tv_name_of_member.text = LabelSet.getText(
            "name_of_member",
            R.string.name_of_member
        )
        tv_type_of_saving_deposit.text = LabelSet.getText(
            "type_of_saving_deposit",
            R.string.type_of_saving_deposit
        )
        tv_total_saved_amount.text = LabelSet.getText(
            "total_saved_amount",
            R.string.total_saved_amount
        )
        tv_total_share_in_profit.text = LabelSet.getText(
            "total_share_in_profit",
            R.string.total_share_in_profit
        )
        tv_total_outstanding_loan_payable.text = LabelSet.getText(
            "total_outstanding_loan_payable",
            R.string.total_outstanding_loan_payable
        )
        tv_total_amount_available_for_withdrawal.text = LabelSet.getText(
            "total_amount_available_for_withdrawal",
            R.string.total_amount_available_for_withdrawal
        )
        tv_amount_paid.text = LabelSet.getText(
            "amount_paid",
            R.string.amount_paid
        )
        tv_name_of_receiver_nominee.text = LabelSet.getText(
            "name_of_receiver_nominee",
            R.string.name_of_receiver_nominee
        )
        tv_Relation_of_receiver.text = LabelSet.getText(
            "relation_of_receiver",
            R.string.relation_of_receiver
        )
        tv_date_of_payment.text = LabelSet.getText(
            "date_of_payment",
            R.string.date_of_payment
        )
        tv_cash_in_hand.text = LabelSet.getText(
            "cash_in_hand",
            R.string.cash_in_hand
        )

        et_total_saved_amount.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_total_share_in_profit.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_total_outstanding_loan_payable.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_total_amount_available_for_withdrawal.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_amount_paid.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_name_of_receiver_nominee.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_Relation_of_receiver.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_date_of_payment.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )

        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }

    private fun fillSpinner() {
        dataspin_name_of_member = lookupViewmodel!!.getlookup(
                99,
                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_type_of_saving_deposit = lookupViewmodel!!.getlookup(
                99,
                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )


        validate!!.fillspinner(this, spin_name_of_member, dataspin_name_of_member)
        validate!!.fillspinner(this, spin_type_of_saving_deposit, dataspin_type_of_saving_deposit)
    }

}