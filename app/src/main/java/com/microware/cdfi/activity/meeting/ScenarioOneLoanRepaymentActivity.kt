package com.microware.cdfi.activity.meeting

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.ScenarioOneLoanRepaymentAdapter
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.entity.DtLoanMemberSheduleEntity
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.DtLoanMemberScheduleViewmodel
import com.microware.cdfi.viewModel.DtLoanMemberViewmodel
import com.microware.cdfi.viewModel.DtLoanTxnMemViewmodel
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import kotlinx.android.synthetic.main.activity_scenario_one_loan_repayment.*
import kotlinx.android.synthetic.main.bankdialoge.view.*
import kotlinx.android.synthetic.main.buttons.view.*

class ScenarioOneLoanRepaymentActivity : AppCompatActivity() {

    var validate: Validate? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    lateinit var dtLoanTxnMemViewmodel: DtLoanTxnMemViewmodel
    lateinit var dtLoanMemberViewmodel: DtLoanMemberViewmodel
    lateinit var dtLoanMemberScheduleViewmodel: DtLoanMemberScheduleViewmodel
    var demand = 0
    var intdemand = 0
    var totdemand = 0
    var totpaid = 0
    var shgBankList: List<Cbo_bankEntity>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scenario_one_loan_repayment)

        validate = Validate(this)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        dtLoanTxnMemViewmodel =
            ViewModelProviders.of(this).get(DtLoanTxnMemViewmodel::class.java)
        dtLoanMemberViewmodel =
            ViewModelProviders.of(this).get(DtLoanMemberViewmodel::class.java)
        dtLoanMemberScheduleViewmodel =
            ViewModelProviders.of(this).get(DtLoanMemberScheduleViewmodel::class.java)

        tvmember_name.text = validate!!.RetriveSharepreferenceString(MeetingSP.MemberName)
        tvmember_code.text = validate!!.RetriveSharepreferenceLong(MeetingSP.MemberCode).toString()

        btn_cancel.setOnClickListener {
            val intent = Intent(this,RepaymentDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        }

        btn_pay.setOnClickListener {
            if (validate!!.returnIntegerValue(tv_total_pay.text.toString()) > 0) {
                saveloanrepayment()
            } else {
                validate!!.CustomAlert(getString(R.string.amountalreadypaid),this)
            }
        }
        replaceFragmenty(
            fragment = MeetingTopBarFragment(8),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        fillRecyclerView()

    }

    private fun fillRecyclerView() {
        demand = 0
        intdemand = 0
        totdemand = 0
        totpaid = 0
        var list = dtLoanTxnMemViewmodel.getmemberloantxnlist(
            validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid),
            validate!!.RetriveSharepreferenceInt(
                MeetingSP.currentmeetingnumber
            )
        )
        var scenarioOneLoneRepaymentAdapter = ScenarioOneLoanRepaymentAdapter(this, list)

        rvList.layoutManager = LinearLayoutManager(this)

        val isize: Int
        isize = list!!.size
        val params: ViewGroup.LayoutParams = rvList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvList.layoutParams = params
        rvList.adapter = scenarioOneLoneRepaymentAdapter
        tv_total_loan.text = isize.toString()
    }

    fun getTotalValue() {
        var iValue = 0
        var iValueCum = 0
        val iCount = rvList.childCount


        for (i in 0 until iCount) {
            val gridChild =
                rvList.getChildAt(i) as? ViewGroup

            val et_principal_demand = gridChild!!
                .findViewById<View>(R.id.et_principal_demand) as? EditText



            if (!et_principal_demand!!.text.toString().isNullOrEmpty()) {
                iValue = iValue + validate!!.returnIntegerValue(et_principal_demand.text.toString())
            }


        }
        tv_total_pay.text = iValue.toString()
        var bal=totdemand-iValue
        if(bal>0) {
            tv_total_balance.text = bal.toString()
        }else {
            tv_total_balance.text = "0"
        }
    }

    fun saveloanrepayment() {
        var saveValue = 0

        val iCount = rvList.childCount


        for (i in 0 until iCount) {
            val gridChild =
                rvList.getChildAt(i) as? ViewGroup

            val tv_loan_no1 = gridChild!!
                .findViewById<View>(R.id.tv_loan_no) as? TextView

            val tvCash = gridChild
                .findViewById<View>(R.id.tvCash) as? TextView

            val tvBank = gridChild
                .findViewById<View>(R.id.tvBank) as? TextView
            val tvaccountno = gridChild
                .findViewById<View>(R.id.tvaccountno) as? TextView
            val tvtransactionno = gridChild
                .findViewById<View>(R.id.tvtransactionno) as? TextView

            val total_due1 = gridChild
                .findViewById<View>(R.id.et_principal_demand) as? EditText



            if (validate!!.returnIntegerValue(total_due1!!.text.toString()) > 0) {
                var loanno = validate!!.returnIntegerValue(tv_loan_no1!!.text.toString())
                var memid = validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid)
                var paid = validate!!.returnIntegerValue(total_due1.text.toString())
                var mode=0
                var accountno=validate!!.returnStringValue(tvaccountno!!.text.toString())
                var transaction=validate!!.returnStringValue(tvtransactionno!!.text.toString())
                if(tvCash!!.visibility==View.VISIBLE){
                    mode=1
                }else if(tvBank!!.visibility==View.VISIBLE){
                    mode=2
                }
                saveValue = saveData(loanno, memid, paid,mode,accountno,transaction)
                // updateschedule(loanno, memid, paid)

            }
        }

        if (saveValue > 0) {
            /* CDFIApplication.database?.dtmtgDao()
                 ?.updatecompsave(validate!!.returnIntegerValue(tv_TotalCumValue.text.toString()),validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),validate!!.RetriveSharepreferenceLong(MeetingSP.Shgcode))

             replaceFragmenty(
                 fragment = MeetingTopBarFragment(2),
                 allowStateLoss = true,
                 containerViewId = R.id.mainContent
             )*/
            fillRecyclerView()
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this
            )
        }
    }

    fun saveData(
        loanno: Int,
        memid: Long,
        paid: Int,
        mode: Int,
        accountno: String,
        transaction: String
    ): Int {
        var value = 0
        var listdata = dtLoanTxnMemViewmodel.getmemberloandata(
            loanno,
            validate!!.RetriveSharepreferenceInt(
                MeetingSP.currentmeetingnumber
            ),
            memid
        )
        var amt = paid
        var intrest = 0
        var loanos = 0
        var principaldemandcl = 0
        if (!listdata.isNullOrEmpty()) {
            loanos =
                validate!!.returnIntegerValue(listdata.get(0).loan_op.toString())
           principaldemandcl =
                validate!!.returnIntegerValue(listdata.get(0).principal_demand_ob.toString()) + validate!!.returnIntegerValue(
                    listdata.get(0).principal_demand.toString()
                )
            intrest =
                validate!!.returnIntegerValue(listdata.get(0).int_accrued_op.toString()) + validate!!.returnIntegerValue(
                    listdata.get(0).int_accrued.toString()
                )
            if (amt > intrest) {
                amt = amt - intrest
            } else {
                intrest = intrest - amt
                amt = 0
            }

        }
        if (principaldemandcl > amt) {
            principaldemandcl=principaldemandcl-amt
        } else {
            principaldemandcl=0
        }

        var completionflag=false
        if (amt>=loanos){
            completionflag=true
        }

        dtLoanTxnMemViewmodel.updateloanpaid(
            loanno,
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            memid,
            amt, intrest,
            mode, accountno, transaction, principaldemandcl,completionflag)

        dtLoanMemberViewmodel.updateMemberLoanEditFlag(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            memid,
            loanno,
            completionflag
        )
        updateschedule(loanno, memid, amt)
        value = 1

        return value
    }

    fun updateschedule(loanno: Int, memid: Long, paid: Int): Int {
        var value = 0
        var amt = paid
        dtLoanMemberScheduleViewmodel.deleterepaid(
            loanno,
            memid,
            validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber)
        )
        dtLoanMemberScheduleViewmodel.deletesubinstallment(
            loanno,
            memid,
            validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber)
        )
        var list = dtLoanMemberScheduleViewmodel.getMemberScheduleloanwise(memid, loanno,
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber))

        for (i in list!!.indices) {
            if (amt > 0) {
                if (validate!!.returnIntegerValue(list.get(i).principal_demand.toString()) <= amt) {
                    dtLoanMemberScheduleViewmodel.updaterepaid(
                        loanno,
                        memid,
                        list.get(i).installment_no,
                        list.get(i).sub_installment_no,
                        validate!!.returnIntegerValue(list.get(i).principal_demand.toString()),
                        validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
                        validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime)
                    )
                    amt =
                        amt - validate!!.returnIntegerValue(list.get(i).principal_demand.toString())
                } else {
                    var remainingamt=validate!!.returnIntegerValue(list.get(i).principal_demand.toString()) - amt
                    dtLoanMemberScheduleViewmodel.updaterepaid(
                        loanno,
                        memid,
                        list.get(i).installment_no,
                        list.get(i).sub_installment_no,
                        amt,
                        validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
                        validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber) ,validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime)
                    )
                    var dtLoanMemberSheduleEntity = DtLoanMemberSheduleEntity(
                        0,
                        validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                        memid,
                        validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
                        validate!!.returnIntegerValue(list.get(i).loan_no.toString()),
                        remainingamt,
                        remainingamt,
                        (validate!!.returnIntegerValue(list.get(i).loan_os.toString())), 0,
                        validate!!.returnIntegerValue(list.get(i).installment_no.toString()),
                        validate!!.returnIntegerValue(list.get(i).sub_installment_no.toString())+1,
                        validate!!.returnLongValue(list.get(i).installment_date.toString()),
                        //   validate!!.Daybetweentime(validate!!.currentdatetime),
                        0,
                        0,
                        null,

                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime), null, null,
                        "", 0


                    )
                    dtLoanMemberScheduleViewmodel.insert(dtLoanMemberSheduleEntity)
                    amt = 0
                    break
                }
            }else{
                break
            }
        }
        value = 1

        return value
    }


    override fun onBackPressed() {
        val intent = Intent(this,RepaymentDetailActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

    fun getTotalValue(iValue: Int, iValue1: Int, iValue2: Int, iValue3: Int) {

        demand = demand + iValue
        intdemand = intdemand + iValue1
        totdemand = totdemand + iValue2
        totpaid = totpaid + iValue3
        tv_total_principal_demand.text = LabelSet.getText(
            "rs",
            R.string.rs
        ) + demand.toString()
        tv_total_interest_demand.text = LabelSet.getText(
            "rs",
            R.string.rs
        ) + intdemand.toString()
        tv_total_amount.text = LabelSet.getText("rs", R.string.rs) + totdemand.toString()
        tv_total_pay.text = totpaid.toString()
        tv_repaid.text = totpaid.toString()
        var remainingBalance = totdemand-totpaid
        if(remainingBalance>0) {
            tv_total_balance.text = remainingBalance.toString()
        }else {
            tv_total_balance.text = "0"
        }
    }

    fun getremaininginstallment(loanno: Int): Int {
        return dtLoanMemberScheduleViewmodel.getremaininginstallment(
            loanno,
            validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid)
        )


    }

    fun CustomAlertBankDialog(
        tvaccountno: TextView,
        tvtransactionno: TextView,
        tvCash: TextView,
        tvBank: TextView
    ) {
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.bankdialoge, null)
        val mBuilder = AlertDialog.Builder(this).setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.setCancelable(false)
        mAlertDialog.setCanceledOnTouchOutside(false)
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        mDialogView.btn_save.setOnClickListener {
            if (checkValidation(mDialogView) == 1){
                tvaccountno.text = returaccount(mDialogView)
                tvtransactionno.text = validate!!.returnStringValue(mDialogView.et_cheque_no_transactio_no.text.toString())
                tvCash.visibility=View.GONE
                tvBank.visibility=View.VISIBLE
                mAlertDialog.dismiss()
            }
        }
       // mDialogView.btn_cancel.visibility=View.GONE
        mDialogView.btn_cancel.setOnClickListener {
            mAlertDialog.dismiss()
          tvCash.visibility=View.VISIBLE
           tvBank.visibility=View.GONE
        }

        fillbank(mDialogView)

    }

    fun fillbank(mDialogView: View) {
        shgBankList = generateMeetingViewmodel.getBankdata(validate!!.RetriveSharepreferenceString(MeetingSP.SHGGUID))

        val adapter: ArrayAdapter<String?>
        if (!shgBankList.isNullOrEmpty()) {
            val isize = shgBankList!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in shgBankList!!.indices) {
                var lastthree=shgBankList!![i].account_no.substring(shgBankList!![i].account_no.length-3)
                sValue[i + 1] =
                    shgBankList!![i].ifsc_code.toString().dropLast(7) +"XXXXX"+ lastthree
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            mDialogView.spin_name_of_bank.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            mDialogView.spin_name_of_bank.adapter = adapter
        }

    }

    fun SaveData(mDialogView: View){

    }

    fun checkValidation(mDialogView: View):Int{
        var value = 1

        if (mDialogView.spin_name_of_bank.selectedItemPosition == 0){
            validate!!.CustomAlertSpinner(
                this, mDialogView.spin_name_of_bank,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "name_of_bank",
                    R.string.name_of_bank
                )
            )
            value = 0
        }else if (mDialogView.et_cheque_no_transactio_no.text.toString().length == 0){
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "cheque_no_transactio_no",
                    R.string.cheque_no_transactio_no
                ),
                this, mDialogView.et_cheque_no_transactio_no
            )
            value = 0
        }


        return value
    }
    fun returaccount(mDialogView: View): String {

        var pos = mDialogView.spin_name_of_bank.selectedItemPosition
        var id = ""

        if (!shgBankList.isNullOrEmpty()) {
            if (pos > 0) id =
                shgBankList!!.get(pos - 1).ifsc_code!!.dropLast(7) + shgBankList!!.get(pos - 1).account_no
        }
        return id
    }
/*fun returaccount(mDialogView: View): String {

    var pos = mDialogView.spin_name_of_bank.getSelectedItemPosition()
    var id = ""

    if (!shgBankList.isNullOrEmpty()) {
        if (pos > 0) id =
            shgBankList!!.get(pos - 1).ifsc_code!!.dropLast(7) + shgBankList!!.get(pos - 1).account_no!!
    }
    return id
}

fun setaccount(accountno:String,mDialogView:View): Int {

    var pos = 0

    if (!shgBankList.isNullOrEmpty()) {
        for (i in shgBankList!!.indices!!) {
            if (accountno.equals(
                    shgBankList!!.get(i).ifsc_code!!.dropLast(7) + shgBankList!!.get(
                        i
                    ).account_no
                )
            )
                pos = i + 1
        }
    }
    mDialogView.spin_name_of_bank.setSelection(pos)
    return pos
}*/
}