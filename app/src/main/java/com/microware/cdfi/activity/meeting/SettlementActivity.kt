package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.*
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_settlement.*
import kotlinx.android.synthetic.main.buttons.btn_cancel
import kotlinx.android.synthetic.main.buttons.btn_save
import kotlinx.android.synthetic.main.repay_toolbar.*

class SettlementActivity : AppCompatActivity() {
    var lookupViewmodel: LookupViewmodel? = null
    var dtLoanTxnMemViewmodel: DtLoanTxnMemViewmodel? = null
    var memSettlementViewmodel: MemSettlementViewmodel? = null
    var dtLoanMemberScheduleViewmodel: DtLoanMemberScheduleViewmodel? = null
    var generateMeetingViewmodel: GenerateMeetingViewmodel? = null
    lateinit var dtLoanMemberViewmodel: DtLoanMemberViewmodel
    lateinit var financialTransactionsMemViewmodel: FinancialTransactionsMemViewmodel
    var validate: Validate? = null
    var dataspin_name_of_member: List<LookupEntity>? = null
    var dataspin_withdrawl: List<LookupEntity>? = null
    var dataspin_reason_for_withdrawal: List<LookupEntity>? = null
    var dataspin_relation: List<LookupEntity>? = null
    var memberlist: List<DtmtgDetEntity>? = null
    var dtMemSettlementEntity: DtMemSettlementEntity? = null
    var dataspin_mode_of_payment: List<LookupEntity>? = null
    var shgBankList: List<Cbo_bankEntity>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settlement)
        validate = Validate(this)
        setLabelText()
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        dtLoanMemberViewmodel = ViewModelProviders.of(this).get(DtLoanMemberViewmodel::class.java)
        memSettlementViewmodel = ViewModelProviders.of(this).get(MemSettlementViewmodel::class.java)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        dtLoanTxnMemViewmodel =
            ViewModelProviders.of(this).get(DtLoanTxnMemViewmodel::class.java)
        dtLoanMemberScheduleViewmodel =
            ViewModelProviders.of(this).get(DtLoanMemberScheduleViewmodel::class.java)
        financialTransactionsMemViewmodel =ViewModelProviders.of(this).get(FinancialTransactionsMemViewmodel::class.java)

        fillmemberspinner()
        btn_cancel.setOnClickListener {
            var intent = Intent(this, SettlementListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                SaveData()
            }
        }
        et_date_of_payment.setOnClickListener {
            validate!!.datePickerwithmindate(
                validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
                et_date_of_payment
            )
        }

        et_membersurplus.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var totalamt =
                    validate!!.returnIntegerValue(et_compulsory_saving.text.toString()) + validate!!.returnIntegerValue(
                        et_voluntary_saving.text.toString()
                    ) + validate!!.returnIntegerValue(
                        et_membersurplus.text.toString()
                    ) - validate!!.returnIntegerValue(
                        et_deficit.text.toString()
                    ) - validate!!.returnIntegerValue(et_total_outstanding_loan_on_member.text.toString())
                et_amount_paid_to_member.setText(totalamt.toString())
            }

        })

        et_deficit.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var totalamt =
                    validate!!.returnIntegerValue(et_compulsory_saving.text.toString()) + validate!!.returnIntegerValue(
                        et_voluntary_saving.text.toString()
                    ) + validate!!.returnIntegerValue(
                        et_membersurplus.text.toString()
                    ) - validate!!.returnIntegerValue(
                        et_deficit.text.toString()
                    ) - validate!!.returnIntegerValue(et_total_outstanding_loan_on_member.text.toString())
                et_amount_paid_to_member.setText(totalamt.toString())
            }

        })

        fillSpinner()
        showdata()
        fillbank()

        spin_mode_of_payment?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (validate!!.returnlookupcode(
                        spin_mode_of_payment,
                        dataspin_mode_of_payment
                    ) == 2 || validate!!.returnlookupcode(
                        spin_mode_of_payment,
                        dataspin_mode_of_payment
                    ) == 3
                ) {
                    lay_bank.visibility = View.VISIBLE
                    lay_cheque_no_transactio_no.visibility = View.VISIBLE
                } else {
                    lay_bank.visibility = View.GONE
                    lay_cheque_no_transactio_no.visibility = View.GONE
                    spin_bank.setSelection(0)

                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }
    }
    fun fillbank(
    ) {
        shgBankList =
            generateMeetingViewmodel!!.getBankdata(validate!!.RetriveSharepreferenceString(MeetingSP.SHGGUID))

        val adapter: ArrayAdapter<String?>
        if (!shgBankList.isNullOrEmpty()) {
            val isize = shgBankList!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in shgBankList!!.indices) {
                var lastthree =
                    shgBankList!![i].account_no.substring(shgBankList!![i].account_no.length - 3)
                sValue[i + 1] =
                    shgBankList!![i].ifsc_code.toString().dropLast(7) + "XXXXX" + lastthree
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_bank.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_bank.adapter = adapter
        }

    }
    fun showdata() {
        val list = generateMeetingViewmodel!!.getCompulsoryData(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid)
        )
        if (!list.isNullOrEmpty()) {
            et_compulsory_saving.setText(list[0].sav_comp_cb.toString())
            et_voluntary_saving.setText(list[0].sav_vol_cb.toString())
            et_share_capital.setText("0")
            et_membersurplus.setText("0")
        }
        var totalpaid = dtLoanTxnMemViewmodel!!.gettotalpaid(
            validate!!.RetriveSharepreferenceLong(
                MeetingSP.Memberid
            ), validate!!.RetriveSharepreferenceInt(
                MeetingSP.currentmeetingnumber
            )
        )
        val loanos = dtLoanMemberScheduleViewmodel!!.gettotalloanoutstanding(
            validate!!.RetriveSharepreferenceLong(
                MeetingSP.Memberid
            )
        )
        et_total_outstanding_loan_on_member.setText((totalpaid + loanos).toString())
        var totalamt =
            validate!!.returnIntegerValue(et_compulsory_saving.text.toString()) + validate!!.returnIntegerValue(
                et_voluntary_saving.text.toString()
            ) + validate!!.returnIntegerValue(
                et_membersurplus.text.toString()
            ) - validate!!.returnIntegerValue(
                et_deficit.text.toString()
            ) - validate!!.returnIntegerValue(et_total_outstanding_loan_on_member.text.toString())
        et_total_amount_available.setText(totalamt.toString())
        et_amount_paid_to_member.setText(totalamt.toString())


    }

    private fun checkValidation(): Int {
        var value = 1
        if (spin_entry_member_name.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_entry_member_name,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "name_of_member",
                    R.string.name_of_member
                )
            )
            value = 0
        } else if (validate!!.returnIntegerValue(et_amount_paid_to_member.text.toString()) < 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "amount_paid_to_member_amount_received_from_member",
                    R.string.amount_paid_to_member_amount_received_from_member
                ),
                this, et_amount_paid_to_member
            )
            value = 0
        } else if (et_name_of_receiver_nominee.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "name_of_receiver_nominee",
                    R.string.name_of_receiver_nominee
                ),
                this, et_name_of_receiver_nominee
            )
            value = 0
        } else if (spin_relation.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_relation,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "relation_of_receiver",
                    R.string.relation_of_receiver
                )
            )
            value = 0
        } else if (spin_reason_for_withdrawal.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_reason_for_withdrawal,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "reason_for_withdrawal_settlement",
                    R.string.reason_for_withdrawal_settlement
                )
            )
            value = 0
        } else if (spin_mode_of_payment.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_mode_of_payment,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "mode_of_payment",
                    R.string.mode_of_payment
                )

            )
            value = 0
            return value
        } else if (lay_bank.visibility == View.VISIBLE && spin_bank.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_bank,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText("bank", R.string.bank)

            )
            value = 0
            return value
        } else if (lay_cheque_no_transactio_no.visibility == View.VISIBLE && validate!!.returnStringValue(
                et_cheque_no_transactio_no.text.toString()
            ).length == 0
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + LabelSet.getText(
                    "cheque_no_transactio_no",
                    R.string.cheque_no_transactio_no
                ), this,
                et_cheque_no_transactio_no
            )
            value = 0
            return value
        } else if (et_date_of_payment.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "date_of_payment",
                    R.string.date_of_payment
                ),
                this, et_date_of_payment
            )
            value = 0
        }


        return value
    }

    private fun SaveData() {
        dtMemSettlementEntity = DtMemSettlementEntity(
            0,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid),
            validate!!.returnIntegerValue(et_compulsory_saving.text.toString()),
            validate!!.returnIntegerValue(et_voluntary_saving.text.toString()),
            validate!!.returnIntegerValue(et_share_capital.text.toString()),
            validate!!.returnIntegerValue(et_membersurplus.text.toString()),
            validate!!.returnIntegerValue(et_total_outstanding_loan_on_member.text.toString()), 0,
            validate!!.returnIntegerValue(et_total_amount_available.text.toString()),
            validate!!.returnIntegerValue(et_amount_paid_to_member.text.toString()),
            validate!!.returnStringValue(et_name_of_receiver_nominee.text.toString()),
            validate!!.returnlookupcode(spin_relation, dataspin_relation),
            validate!!.returnlookupcode(spin_reason_for_withdrawal, dataspin_reason_for_withdrawal),
            validate!!.Daybetweentime(et_date_of_payment.text.toString()),
            validate!!.returnlookupcode(spin_mode_of_payment, dataspin_mode_of_payment),
            returaccount(),
            validate!!.returnStringValue(
                et_cheque_no_transactio_no.text.toString()
            ), "",
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            "",
            0,
            "",
            0, 0, "", 1
        )

        memSettlementViewmodel!!.insert(dtMemSettlementEntity!!)
        if (validate!!.returnIntegerValue(et_membersurplus.text.toString()) > 0) {
            insertsurplus()

        }
        if (validate!!.returnIntegerValue(et_deficit.text.toString()) > 0) {
            insertdeficit()

        }
        if (validate!!.returnIntegerValue(et_amount_paid_to_member.text.toString()) > 0) {
            updatewithdrwal()

        }
        if (validate!!.returnIntegerValue(et_total_outstanding_loan_on_member.text.toString()) > 0) {
            saveloanrepayment()
        }
        validate!!.CustomAlert(
            LabelSet.getText(
                "data_saved_successfully",
                R.string.data_saved_successfully
            ),
            this, SettlementListActivity::class.java
        )
        generateMeetingViewmodel!!.updateSettlementstatus(
            validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid),
            1
        )
    }
    fun returaccount(): String {

        var pos = spin_bank.selectedItemPosition
        var id = ""

        if (!shgBankList.isNullOrEmpty()) {
            if (pos > 0) id =
                shgBankList!!.get(pos - 1).ifsc_code!!.dropLast(7) + shgBankList!!.get(pos - 1).account_no
        }
        return id
    }
    fun setLabelText() {
        tv_title.text = LabelSet.getText("settelment", R.string.settelment)
        tv_name_of_member.text = LabelSet.getText(
            "name_of_member",
            R.string.name_of_member
        )

        tv_compulsory_saving.text = LabelSet.getText(
            "compulsory_saving",
            R.string.compulsory_saving
        )
        tv_name_of_receiver_nominee.text = LabelSet.getText(
            "name_of_receiver_nominee",
            R.string.name_of_receiver_nominee
        )
        tv_Relation_of_receiver.text = LabelSet.getText(
            "relation_of_receiver",
            R.string.relation_of_receiver
        )
        tv_date_of_payment.text = LabelSet.getText(
            "date_of_payment",
            R.string.date_of_payment
        )
//
        et_name_of_receiver_nominee.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_date_of_payment.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )

        btn_save.text = LabelSet.getText(
            "save",
            R.string.save
        )
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }

    private fun fillSpinner() {


        dataspin_relation = lookupViewmodel!!.getlookup(
            23,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_reason_for_withdrawal = lookupViewmodel!!.getlookup(
            41,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_mode_of_payment = lookupViewmodel!!.getlookup(
            65,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        validate!!.fillspinner(this, spin_mode_of_payment, dataspin_mode_of_payment)
        spin_mode_of_payment.setSelection(
            validate!!.returnlookupcodepos(
                1, dataspin_mode_of_payment
            )
        )
        validate!!.fillspinner(this, spin_relation, dataspin_relation)
        validate!!.fillspinner(this, spin_reason_for_withdrawal, dataspin_reason_for_withdrawal)
    }

    private fun fillmemberspinner() {
        memberlist = generateMeetingViewmodel!!.getListinactivemember(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        val adapter: ArrayAdapter<String?>
        if (!memberlist.isNullOrEmpty()) {
            val isize = memberlist!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in memberlist!!.indices) {
                sValue[i + 1] = memberlist!![i].member_name
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_entry_member_name.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(memberlist!!.size + 1)
            sValue[0] = LabelSet.getText(
                "nomemberavailable",
                R.string.nomemberavailable
            )
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_entry_member_name.adapter = adapter
        }
        setMember()

    }

    fun returnmemaid(): Long {

        var pos = spin_entry_member_name.selectedItemPosition
        var id = 0L

        if (!memberlist.isNullOrEmpty()) {
            if (pos > 0) id = memberlist!!.get(pos - 1).mem_id
        }
        return id
    }

    fun returnmemid(): Long {

        var pos = spin_entry_member_name.selectedItemPosition
        var id = 0L

        if (!memberlist.isNullOrEmpty()) {
            if (pos > 0) id = memberlist!!.get(pos - 1).mem_id
        }
        return id
    }

    fun setMember(): Int {
        var pos = 0
        if (!memberlist.isNullOrEmpty()) {
            for (i in memberlist!!.indices) {
                if (validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid) == memberlist!!.get(
                        i
                    ).mem_id
                )
                    pos = i + 1
            }
        }
        spin_entry_member_name.setSelection(pos)
        spin_entry_member_name.isEnabled = false
        return pos
    }

    fun saveloanrepayment() {
        var saveValue = 0

        var list = dtLoanTxnMemViewmodel!!.getmemberloantxnlist(
            validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid),
            validate!!.RetriveSharepreferenceInt(
                MeetingSP.currentmeetingnumber
            )
        )

        for (i in list!!.indices) {
            var loanno = validate!!.returnIntegerValue(list.get(i).loan_no.toString())
            var memid = validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid)
            var paid =
                validate!!.returnIntegerValue(list.get(i).loan_op.toString()) + validate!!.returnIntegerValue(
                    list.get(i).int_accrued_op.toString()
                ) + validate!!.returnIntegerValue(
                    list.get(i).int_accrued.toString()
                )
            saveValue = updaterepayment(loanno, memid, paid)
            // updateschedule(loanno, memid, paid)


        }

        if (saveValue > 0) {
            /* CDFIApplication.database?.dtmtgDao()
                 ?.updatecompsave(validate!!.returnIntegerValue(tv_TotalCumValue.text.toString()),validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),validate!!.RetriveSharepreferenceLong(MeetingSP.Shgcode))

             replaceFragmenty(
                 fragment = MeetingTopBarFragment(2),
                 allowStateLoss = true,
                 containerViewId = R.id.mainContent
             )*/

        }
    }

    fun insertsurplus() {
        var financialTransactionsMemEntity = FinancialTransactionsMemEntity(
            0,
            validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            returnmemid(),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            returaccount(),
            6,/*auid*/
            "OR",
            validate!!.returnIntegerValue(et_membersurplus.text.toString()),
            validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            0,
            validate!!.returnlookupcode(spin_mode_of_payment, dataspin_mode_of_payment),
            et_cheque_no_transactio_no.text.toString(),
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            null,
            null,
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            0
        )
        financialTransactionsMemViewmodel.insert(financialTransactionsMemEntity)


    }
    fun insertdeficit() {
        var financialTransactionsMemEntity = FinancialTransactionsMemEntity(
            0,
            validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            returnmemid(),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            returaccount(),
            6,/*auid*/
            "OP",
            validate!!.returnIntegerValue(et_deficit.text.toString()),
            validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            0,
            validate!!.returnlookupcode(spin_mode_of_payment, dataspin_mode_of_payment),
            et_cheque_no_transactio_no.text.toString(),
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            null,
            null,
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            0
        )
        financialTransactionsMemViewmodel.insert(financialTransactionsMemEntity)

    }
    fun updatewithdrwal() {
        generateMeetingViewmodel!!.updatesavingwithdrawl(
            validate!!.returnIntegerValue(et_amount_paid_to_member.text.toString()),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid),
            validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
            validate!!.Daybetweentime(validate!!.currentdatetime)
        )
        generateMeetingViewmodel!!.updateSettlementstatus(
            validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid),
            1
        )
        /*validate!!.CustomAlert(
            LabelSet.getText(
                "data_saved_successfully",
                R.string.data_saved_successfully
            ),
            this, WidthdrawalDetailActivity::class.java
        )*/
    }

    fun updaterepayment(loanno: Int, memid: Long, paid: Int): Int {
        var value = 0
        var listdata = dtLoanTxnMemViewmodel!!.getmemberloandata(
            loanno,
            validate!!.RetriveSharepreferenceInt(
                MeetingSP.currentmeetingnumber
            ),
            memid
        )
        var amt = paid
        var intrest = 0
        var loanos = 0
        var principaldemandcl = 0
        if (!listdata.isNullOrEmpty()) {
            loanos =
                validate!!.returnIntegerValue(listdata.get(0).loan_op.toString())
            principaldemandcl =
                validate!!.returnIntegerValue(listdata.get(0).principal_demand_ob.toString()) + validate!!.returnIntegerValue(
                    listdata.get(0).principal_demand.toString()
                )
            intrest =
                validate!!.returnIntegerValue(listdata.get(0).int_accrued_op.toString()) + validate!!.returnIntegerValue(
                    listdata.get(0).int_accrued.toString()
                )
            if (amt > intrest) {
                amt = amt - intrest
            } else {
                intrest = intrest - amt
                amt = 0
            }

        }
        if (principaldemandcl > amt) {
            principaldemandcl = principaldemandcl - amt
        } else {
            principaldemandcl = 0
        }

        var completionflag=false
        if (amt>=loanos){
            completionflag=true
        }
        dtLoanTxnMemViewmodel!!.updateloanpaid(
            loanno,
            validate!!.RetriveSharepreferenceInt(
                MeetingSP.currentmeetingnumber
            ),
            memid,
            amt,
            intrest,
            1,
            "",
            "", principaldemandcl,
            completionflag
        )

        dtLoanMemberViewmodel.updateMemberLoanEditFlag(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            memid,
            loanno,
            completionflag
        )

        updateschedule(loanno, memid, amt)
        value = 1

        return value
    }

    fun updateschedule(loanno: Int, memid: Long, paid: Int): Int {
        var value = 0
        var amt = paid
        dtLoanMemberScheduleViewmodel!!.deleterepaid(
            loanno,
            memid,
            validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber)
        )
        dtLoanMemberScheduleViewmodel!!.deletesubinstallment(
            loanno,
            memid,
            validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber)
        )
        var list = dtLoanMemberScheduleViewmodel!!.getMemberScheduleloanwise(
            memid, loanno,
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber)
        )

        for (i in list!!.indices) {
            if (amt > 0) {
                if (validate!!.returnIntegerValue(list.get(i).principal_demand.toString()) <= amt) {
                    dtLoanMemberScheduleViewmodel!!.updaterepaid(
                        loanno,
                        memid,
                        list.get(i).installment_no,
                        list.get(i).sub_installment_no,
                        validate!!.returnIntegerValue(list.get(i).principal_demand.toString()),
                        validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
                        validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime)
                    )
                    amt =
                        amt - validate!!.returnIntegerValue(list.get(i).principal_demand.toString())
                } else {
                    var remainingamt =
                        validate!!.returnIntegerValue(list.get(i).principal_demand.toString()) - amt
                    dtLoanMemberScheduleViewmodel!!.updaterepaid(
                        loanno,
                        memid,
                        list.get(i).installment_no,
                        list.get(i).sub_installment_no,
                        amt,
                        validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
                        validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime)
                    )
                    var dtLoanMemberSheduleEntity = DtLoanMemberSheduleEntity(
                        0,
                        validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                        memid,
                        validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
                        validate!!.returnIntegerValue(list.get(i).loan_no.toString()),
                        remainingamt,
                        remainingamt,
                        (validate!!.returnIntegerValue(list.get(i).loan_os.toString())), 0,
                        validate!!.returnIntegerValue(list.get(i).installment_no.toString()),
                        validate!!.returnIntegerValue(list.get(i).sub_installment_no.toString()) + 1,
                        validate!!.returnLongValue(list.get(i).installment_date.toString()),
                        //   validate!!.Daybetweentime(validate!!.currentdatetime),
                        0,
                        0,
                        null,

                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime), "", 0,
                        "", 0


                    )
                    dtLoanMemberScheduleViewmodel!!.insert(dtLoanMemberSheduleEntity)
                    amt = 0
                    break
                }
            } else {
                break
            }
        }
        value = 1

        return value
    }

    override fun onBackPressed() {
        var intent = Intent(this, SettlementListActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

}