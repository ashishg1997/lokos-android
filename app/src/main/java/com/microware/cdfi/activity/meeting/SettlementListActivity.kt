package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.SettlementDetailAdapter
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.utility.replaceFragmenty
import kotlinx.android.synthetic.main.activity_settlementlist.*
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.repay_toolbar.*

class SettlementListActivity : AppCompatActivity() {
    var lookupViewmodel: LookupViewmodel? = null
    var validate: Validate? = null
    var dataspin_name_of_member: List<LookupEntity>? = null
    var dataspin_type_of_saving_deposit: List<LookupEntity>? = null
    lateinit var settlementDetailAdapter:SettlementDetailAdapter
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    var iValue = 0
    var iValue1 = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settlementlist)
        validate=Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)


        btn_cancel.setOnClickListener {
            var intent = Intent(this, MeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tv_title.text = LabelSet.getText("settelment", R.string.settelment)

        ic_Back.setOnClickListener {
            var intent = Intent(this, MeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }



        setLabelText()
        fillRecyclerView()

    }

    override fun onBackPressed() {
//        var intent = Intent(this, WidthdrawalActivity::class.java)
        var intent = Intent(this, MeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0,0)
        finish()
    }

    fun setLabelText() {
        tv_srno.text = LabelSet.getText("sr_no", R.string.sr_no)
        tv_memberName.text = LabelSet.getText(
            "member_name",
            R.string.member_name
        )
        tv_total_saved.text = LabelSet.getText(
            "total_saved",
            R.string.total_saved
        )
        tv_withdrawl_n_for_saving.text = LabelSet.getText(
            "withdrawl_n_for_saving",
            R.string.withdrawl_n_for_saving
        )
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }

    private fun fillRecyclerView() {
        var list = generateMeetingViewmodel.getListinactivemember(validate!!.RetriveSharepreferenceInt(
            MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(AppSP.shgid))
        settlementDetailAdapter = SettlementDetailAdapter(this,list)

        rvList.layoutManager = LinearLayoutManager(this)

        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = rvList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvList.layoutParams = params
        rvList.adapter = settlementDetailAdapter
    }

    fun getTotalValue(iAmount: Int, flag: Int) {
        if (flag == 1) {
            iValue = iValue + iAmount
            if(iValue>0){
                tv_TotalTodayValue.text = iValue.toString()
            }else {
                tv_TotalTodayValue.text = "0"
            }
        } else if (flag == 2) {
            iValue1 = iValue1 + iAmount
            if (iValue1 > 0){
                tvTotalWithDrwn.text = iValue1.toString()
            }else{
                tvTotalWithDrwn.text = "0"
            }

        }
    }




}