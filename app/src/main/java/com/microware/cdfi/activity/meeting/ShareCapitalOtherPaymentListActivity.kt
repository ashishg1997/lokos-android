package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.ShareCapitalOtherPaymentAdapter
import com.microware.cdfi.api.meetingmodel.ShareCapitalModel
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.MstCOAViewmodel
import kotlinx.android.synthetic.main.activity_share_capital_other_payment_list.*
import kotlinx.android.synthetic.main.activity_share_capital_other_payment_list.rvList
import kotlinx.android.synthetic.main.activity_share_capital_other_payment_list.tv_TotalTodayValue
import kotlinx.android.synthetic.main.activity_share_capital_other_payment_list.tv_member_name
import kotlinx.android.synthetic.main.activity_share_capital_other_payment_list.tv_srno
import kotlinx.android.synthetic.main.activity_share_capital_other_payment_list.tvtotal
import kotlinx.android.synthetic.main.activity_share_capital_other_receipt_list.*

class ShareCapitalOtherPaymentListActivity : AppCompatActivity() {

    var validate: Validate? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    var lookupViewmodel: LookupViewmodel? = null
    lateinit var mstcoaviewmodel: MstCOAViewmodel

    var Todayvalue = 0
    var Cumvalue = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share_capital_other_payment_list)
        validate = Validate(this)

        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        mstcoaviewmodel = ViewModelProviders.of(this).get(MstCOAViewmodel::class.java)

        replaceFragmenty(
            fragment = MeetingTopBarFragment(97),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        setLabelText()
        fillRecyclerView()

    }

    private fun fillRecyclerView() {
        val list = generateMeetingViewmodel.getCapitalListDataMtgByMtgnum(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),"OP"
        )

        getTotal(list)

        if (!list.isNullOrEmpty()){
            rvList.layoutManager = LinearLayoutManager(this)
            val shareCapitalOtherpaymentAdapter = ShareCapitalOtherPaymentAdapter(this, list)
            val isize: Int
            isize = list.size
            val params: ViewGroup.LayoutParams = rvList.layoutParams
            val r = resources
            val px = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                resources.getDimension(R.dimen.spraying),
                r.displayMetrics
            )
            val hi = Math.round(px)
            val gridHeight = hi * isize
            params.height = gridHeight
            rvList.layoutParams = params
            rvList.adapter = shareCapitalOtherpaymentAdapter
        }

    }

    private fun setLabelText() {
        tv_srno.text = LabelSet.getText("sr_no", R.string.sr_no)
        tv_member_name.text = LabelSet.getText(
            "member_name",
            R.string.member_name
        )
        tv_PaymentType.text = LabelSet.getText(
            "payment",
            R.string.payment
        )
        tv_paymentAmount.text = LabelSet.getText(
            "amount",
            R.string.amount
        )
        tvtotal.text = LabelSet.getText("total", R.string.total)
    }


    fun getPaymentType(keyCode: Int): String? {
        var name: String? = null
        name = mstcoaviewmodel.getcoaValue(
            "OP",
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),
            keyCode
        )
        return name
    }
    fun getTotal(list:List<ShareCapitalModel>) {
        var totalAmount = 0
        if (!list.isNullOrEmpty()) {
            for (i in 0 until list.size){
                totalAmount = totalAmount + validate!!.returnIntegerValue(list.get(i).amount.toString())
            }
            tv_TotalTodayValue.text = totalAmount.toString()
        }
    }

    override fun onBackPressed() {
        val intent = Intent(this, MeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

}