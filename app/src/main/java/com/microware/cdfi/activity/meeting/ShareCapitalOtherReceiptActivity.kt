package com.microware.cdfi.activity.meeting

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.ShareCapitalAdapter
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.replaceFragmenty
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import com.microware.cdfi.viewModel.LookupViewmodel
import kotlinx.android.synthetic.main.activity_share_capital_other_receipts.*
import kotlinx.android.synthetic.main.activity_share_capital_other_receipts.view.*
import kotlinx.android.synthetic.main.activity_widthdrawal_detail.*
import kotlinx.android.synthetic.main.activity_widthdrawal_detail.rvList
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.buttons.view.*
import kotlinx.android.synthetic.main.repay_toolbar.view.*

class ShareCapitalOtherReceiptActivity : AppCompatActivity() {
    var lookupViewmodel: LookupViewmodel? = null
    var validate: Validate? = null
    lateinit var shareCapitalAdapter: ShareCapitalAdapter
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    var dataspin_receipt_type: List<LookupEntity>? = null
    var dataspin_mode_of_payment: List<LookupEntity>? = null
    var dataspin_name_of_bank: List<LookupEntity>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share_capitalreceipt)
        validate=Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)



        btn_cancel.setOnClickListener {
            var intent = Intent(this, MeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        replaceFragmenty(
            fragment = MeetingTopBarFragment(9),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )


        fillRecyclerView()

    }

    override fun onBackPressed() {
//        var intent = Intent(this, WidthdrawalActivity::class.java)
        var intent = Intent(this, MeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0,0)
        finish()
    }


    private fun fillRecyclerView() {
        var list = generateMeetingViewmodel.getListDataMtgByMtgnum(validate!!.RetriveSharepreferenceInt(
            MeetingSP.currentmeetingnumber),
            validate!!.returnLongValue(validate!!.RetriveSharepreferenceString(AppSP.shgid)))
        shareCapitalAdapter = ShareCapitalAdapter(this,list)

        rvList.layoutManager = LinearLayoutManager(this)

        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = rvList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvList.layoutParams = params
        rvList.adapter = shareCapitalAdapter
    }

    fun getTotalValue() {
        var iValue = 0
        var saveValue = 0
        var etCheckVAlue: EditText? = null

        val iCount = rvList.childCount


        for (i in 0 until iCount) {
            val gridChild =
                rvList.getChildAt(i) as? ViewGroup

            val tv_count = gridChild!!
                .findViewById<View>(R.id.tv_count) as? TextView

            if (tv_count!!.length() > 0) {
                iValue=iValue + validate!!.returnIntegerValue(tv_count.text.toString())
            }


        }
        tv_TotalTodayValue.text = iValue.toString()

    }

    fun CustomAlert() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.activity_share_capital_other_receipts, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.WHITE))
        mDialogView.tv_title.text = LabelSet.getText(
            "share_capital_other_receipts",
            R.string.share_capital_other_receipts
        )
//        mDialogView.img_scan.visibility = View.INVISIBLE
//        mDialogView.img_refresh.visibility = View.INVISIBLE
        fillSpinner(mDialogView)
        setLabelText(mDialogView)
        /* mDialogView.btn_yes.setOnClickListener {
            mAlertDialog.dismiss()


        }

        mDialogView.btn_no.setOnClickListener {

            mAlertDialog.dismiss()*/


    }

    private fun fillSpinner(view: View) {
        dataspin_receipt_type = lookupViewmodel!!.getlookup(
            99,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_mode_of_payment = lookupViewmodel!!.getlookup(
            99,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_name_of_bank = lookupViewmodel!!.getlookup(
            99,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )


        validate!!.fillspinner(this, view.spin_receipt_type, dataspin_receipt_type)
        validate!!.fillspinner(this, view.spin_mode_of_payment, dataspin_mode_of_payment)
        validate!!.fillspinner(this, view.spin_name_of_bank, dataspin_name_of_bank)
        spin_mode_of_payment.setSelection(
            validate!!.returnlookupcodepos(
                1, dataspin_mode_of_payment
            )
        )
    }

    fun setLabelText(view: View) {
        view.et_name_of_member.isEnabled=false

        view.tv_name_of_member.text = LabelSet.getText(
            "name_of_member",
            R.string.name_of_member
        )
        view.tv_receipt_type.text = LabelSet.getText(
            "receipt_type",
            R.string.receipt_type
        )
        view.tv_amount.text = LabelSet.getText(
            "amount",
            R.string.amount
        )
        view.tv_narration.text = LabelSet.getText(
            "narration",
            R.string.narration
        )
        view.tv_mode_of_payment.text = LabelSet.getText(
            "mode_of_payment",
            R.string.mode_of_payment
        )
        view.tv_name_of_bank.text = LabelSet.getText(
            "name_of_bank",
            R.string.name_of_bank
        )
        view.tv_cheque_no_transactio_no.text = LabelSet.getText(
            "cheque_no_transactio_no",
            R.string.cheque_no_transactio_no
        )
//        view.tv_cash_in_hand.setText(LabelSet.getText("cash_in_hand", R.string.cash_in_hand))

        view.et_name_of_member.hint = validate!!.RetriveSharepreferenceString(MeetingSP.MemberName)
        view.et_amount.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        view.et_narration.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        view.et_cheque_no_transactio_no.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )

        view.btn_save.text = LabelSet.getText(
            "save",
            R.string.save
        )
        view.btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }
}