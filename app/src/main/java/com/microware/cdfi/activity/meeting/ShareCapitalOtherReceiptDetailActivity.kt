package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.*
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_share_capital_other_receipt_detail.*

class ShareCapitalOtherReceiptDetailActivity : AppCompatActivity() {

    var validate: Validate? = null
    var lookupViewmodel: LookupViewmodel? = null
    var dataspin_Receipt: List<MstCOAEntity>? = null
    var dataspin_modeofpayment: List<LookupEntity>? = null
    var dataspin_bank: List<BankEntity>? = null
    var bankMasterViewModel: MasterBankViewmodel? = null
    var memberlist: List<DtmtgDetEntity>? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    lateinit var financialTransactionsMemViewmodel: FinancialTransactionsMemViewmodel
    var financialTransactionsMemEntity: FinancialTransactionsMemEntity? = null
    var shgBankList : List<Cbo_bankEntity>? = null
    var coaviewmodel : MstCOAViewmodel? = null

    var nullStringValue:String?=null
    var nullLongValue:Long?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share_capital_other_receipt_detail)

        validate = Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        bankMasterViewModel = ViewModelProviders.of(this).get(MasterBankViewmodel::class.java)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        financialTransactionsMemViewmodel = ViewModelProviders.of(this).get(FinancialTransactionsMemViewmodel::class.java)
        coaviewmodel = ViewModelProviders.of(this).get(MstCOAViewmodel::class.java)

        replaceFragmenty(
            fragment = MeetingTopBarFragment(9),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )
        btn_save.isEnabled =
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber) >= validate!!.RetriveSharepreferenceInt(MeetingSP.maxmeetingnumber)
        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                SaveData()
            }
        }

        btn_cancel.setOnClickListener {
            val intent = Intent(this, ShareCapitalOtherReceiptListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        spin_mode_of_payment?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                if (position > 0) {
                    val id = validate!!.returnlookupcode(spin_mode_of_payment, dataspin_modeofpayment)
                    if (id > 1){
                        lay_BankName.visibility = View.VISIBLE
                        lay_chequeNO.visibility = View.VISIBLE
                    }else{
                        lay_BankName.visibility = View.GONE
                        lay_chequeNO.visibility = View.GONE
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        fillbank()
        fillmemberspinner()
        fillSpinner()
        setLabelText()
        showData()

    }

    private fun showData() {
        val data = financialTransactionsMemViewmodel.getMtgFinTxnDetdata(
            validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceInt(MeetingSP.Auid)
        )
        if (!data.isNullOrEmpty()) {

            et_cheque_no_transactio_no.setText(data[0].transaction_no)
            et_amount.setText(data[0].amount.toString())
            spin_mode_of_payment.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(
                        data[0].mode_payment.toString()
                    ), dataspin_modeofpayment
                )
            )

            spin_TypeofReceipt.setSelection(
                validate!!.returncoauidpos(
                    validate!!.returnIntegerValue(
                        data[0].auid.toString()
                    ), dataspin_Receipt
                )
            )

           /* spin_BankName.setSelection(
                validate!!.returnMasterBankpos(
                    validate!!.returnIntegerValue(data[0].bank_code.toString()),
                    dataspin_bank
                )
            )*/

            setaccount(data[0].bank_code.toString())
            spin_TypeofReceipt.isEnabled = false
        }else{
            spin_mode_of_payment.setSelection(
                validate!!.returnlookupcodepos(
                    1, dataspin_modeofpayment
                )
            )
        }
    }

    fun fillbank() {
        shgBankList =
            generateMeetingViewmodel.getBankdata(validate!!.RetriveSharepreferenceString(MeetingSP.SHGGUID))

        val adapter: ArrayAdapter<String?>
        if (!shgBankList.isNullOrEmpty()) {
            val isize = shgBankList!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in shgBankList!!.indices) {
                var lastthree=shgBankList!![i].account_no.substring(shgBankList!![i].account_no.length-3)
                sValue[i + 1] =
                    shgBankList!![i].ifsc_code.toString().dropLast(7) +"XXXXX"+ lastthree
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_BankName.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_BankName.adapter = adapter
        }

    }

    fun returaccount(): String {

        var pos = spin_BankName.selectedItemPosition
        var id = ""

        if (!shgBankList.isNullOrEmpty()) {
            if (pos > 0) id =
                shgBankList!!.get(pos - 1).ifsc_code!!.dropLast(7) + shgBankList!!.get(pos - 1).account_no
        }
        return id
    }

    fun setaccount(accountno:String): Int {

        var pos = 0

        if (!shgBankList.isNullOrEmpty()) {
            for (i in shgBankList!!.indices) {
                if (accountno.equals(
                        shgBankList!!.get(i).ifsc_code!!.dropLast(7) + shgBankList!!.get(
                            i
                        ).account_no
                    )
                )
                    pos = i + 1
            }
        }
        spin_BankName.setSelection(pos)
        return pos
    }

    fun setMember(): Int {
        var pos = 0
        if (!memberlist.isNullOrEmpty()) {
            for (i in memberlist!!.indices) {
                if (validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid) == memberlist!!.get(i).mem_id)
                    pos = i + 1
            }
        }
        spin_membername.setSelection(pos)
        spin_membername.isEnabled = false
        return pos
    }

    private fun SaveData() {
        var save = 0
        if (validate!!.RetriveSharepreferenceInt(MeetingSP.Auid) == 0) {
            financialTransactionsMemEntity = FinancialTransactionsMemEntity(
                0,
                validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                returnmemid(),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                returaccount(),
                validate!!.returncoauid(spin_TypeofReceipt, dataspin_Receipt),/*auid*/
                "OR",
                validate!!.returnIntegerValue(et_amount.text.toString()),
                validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
                0,
                validate!!.returnlookupcode(spin_mode_of_payment, dataspin_modeofpayment),
                et_cheque_no_transactio_no.text.toString(),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                nullStringValue,
                nullLongValue,
                nullStringValue,
                nullLongValue,0
            )

            financialTransactionsMemViewmodel.insert(financialTransactionsMemEntity!!)

            save = 1
        } else {
            financialTransactionsMemViewmodel.updateShareCapitalDetail(
                validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                returnmemid(),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceInt(MeetingSP.Auid),
                returaccount(),
                "OR",
                validate!!.returnIntegerValue(et_amount.text.toString()),
                validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
                0,
                validate!!.returnlookupcode(spin_mode_of_payment, dataspin_modeofpayment),
                et_cheque_no_transactio_no.text.toString(),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime)
            )

            save = 2
        }

        if (save == 1) {
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this, ShareCapitalOtherReceiptListActivity::class.java
            )
        } else {
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_updated_successfully",
                    R.string.data_updated_successfully
                ),
                this, ShareCapitalOtherReceiptListActivity::class.java
            )
        }
    }

    private fun checkValidation(): Int {
        var value = 1
        if (spin_membername.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_membername,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "name_of_member",
                    R.string.name_of_member
                )
            )
            value = 0
        } else if (spin_TypeofReceipt.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_TypeofReceipt,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "receipt_type",
                    R.string.receipt_type
                )
            )
            value = 0
        } else if (et_amount.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "amount",
                    R.string.amount
                ),
                this, et_amount
            )
            value = 0
        } else if (spin_mode_of_payment.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_mode_of_payment,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "mode_of_payment",
                    R.string.mode_of_payment
                )
            )
            value = 0
        } else if (spin_BankName.selectedItemPosition == 0 && lay_BankName.visibility == View.VISIBLE) {
            validate!!.CustomAlertSpinner(
                this, spin_BankName,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "bank_name",
                    R.string.bank_name
                )
            )
            value = 0
        } else if (et_cheque_no_transactio_no.text.toString().length == 0 && lay_chequeNO.visibility == View.VISIBLE) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "cheque_no_transactio_no",
                    R.string.cheque_no_transactio_no
                ),
                this, et_cheque_no_transactio_no
            )
            value = 0
        }


        return value
    }

    private fun setLabelText() {
        tv_memberName.text = LabelSet.getText(
            "name_of_member",
            R.string.name_of_member
        )
        tv_receiptType.text = LabelSet.getText(
            "receipt_type",
            R.string.receipt_type
        )
        tv_amount.text = LabelSet.getText(
            "amount",
            R.string.amount
        )
        tv_paymentMode.text = LabelSet.getText(
            "mode_of_payment",
            R.string.mode_of_payment
        )
        tv_BankName.text = LabelSet.getText(
            "bank_name",
            R.string.bank_name
        )
        tv_cheque_no_transactio_no.text = LabelSet.getText(
            "cheque_no_transactio_no",
            R.string.cheque_no_transactio_no
        )
        tv_narration.text = LabelSet.getText(
            "narration",
            R.string.narration
        )
    }

    private fun fillmemberspinner() {
        memberlist = generateMeetingViewmodel.getListDataMtgByMtgnum(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        val adapter: ArrayAdapter<String?>
        if (!memberlist.isNullOrEmpty()) {
            val isize = memberlist!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in memberlist!!.indices) {
                sValue[i + 1] = memberlist!![i].member_name
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_membername.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(memberlist!!.size + 1)
            sValue[0] = LabelSet.getText(
                "nomemberavailable",
                R.string.nomemberavailable
            )
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_membername.adapter = adapter
        }
        setMember()
    }

    private fun fillSpinner() {
        dataspin_Receipt = coaviewmodel!!.getMstCOAdata(
            "OR",
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
        )
        dataspin_modeofpayment = lookupViewmodel!!.getlookup(
            65,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
//        dataspin_bank = bankMasterViewModel!!.getBankMaster()
//        validate!!.fillMasterBankspinner(this, spin_BankName, dataspin_bank)
        validate!!.fillcoaspinner(this, spin_TypeofReceipt, dataspin_Receipt)
        validate!!.fillspinner(this, spin_mode_of_payment, dataspin_modeofpayment)


    }

    private fun returnmemid(): Long {

        var pos = spin_membername.selectedItemPosition
        var id = 0L

        if (!memberlist.isNullOrEmpty()) {
            if (pos > 0) id = memberlist!!.get(pos - 1).mem_id
        }
        return id
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, ShareCapitalOtherReceiptListActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()

    }

}

