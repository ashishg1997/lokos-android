package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.ShareCapitalOtherReceiptAdapter
import com.microware.cdfi.api.meetingmodel.ShareCapitalModel
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.MstCOAViewmodel
import kotlinx.android.synthetic.main.activity_share_capital_other_receipt_list.*

class ShareCapitalOtherReceiptListActivity : AppCompatActivity() {

    var validate: Validate? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    lateinit var mstcoaviewmodel: MstCOAViewmodel
    var lookupViewmodel: LookupViewmodel? = null
    var Todayvalue = 0
    var Cumvalue = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share_capital_other_receipt_list)
        validate = Validate(this)

        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        mstcoaviewmodel = ViewModelProviders.of(this).get(MstCOAViewmodel::class.java)

        replaceFragmenty(
            fragment = MeetingTopBarFragment(9),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        setLabelText()
        fillRecyclerView()

    }

    private fun fillRecyclerView() {
        var list:List<ShareCapitalModel>? = null

            list = generateMeetingViewmodel.getCapitalListDataMtgByMtgnum(
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),"OR"
            )

        getTotal(list)

        if (!list.isNullOrEmpty()){
            rvList.layoutManager = LinearLayoutManager(this)
            val shareCapitalOtherReceiptAdapter = ShareCapitalOtherReceiptAdapter(this, list)
            val isize: Int
            isize = list.size
            val params: ViewGroup.LayoutParams = rvList.layoutParams
            val r = resources
            val px = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                resources.getDimension(R.dimen.spraying),
                r.displayMetrics
            )
            val hi = Math.round(px)
            val gridHeight = hi * isize
            params.height = gridHeight
            rvList.layoutParams = params
            rvList.adapter = shareCapitalOtherReceiptAdapter
        }

    }

    private fun setLabelText() {
        tv_srno.text = LabelSet.getText("sr_no", R.string.sr_no)
        tv_member_name.text = LabelSet.getText(
            "member_name",
            R.string.member_name
        )
        tv_receiptType.text = LabelSet.getText(
            "receipt_type",
            R.string.receipt_type
        )

        tv_receiptAmount.text = LabelSet.getText(
            "receipt_amount",
            R.string.receipt_amount
        )

        tvtotal.text = LabelSet.getText("total", R.string.total)
    }

    fun getRecepitType(keyCode: Int): String? {
        var name: String? = null
        name = mstcoaviewmodel.getcoaValue(
            "OR",
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),
            keyCode
        )
        return name
    }

    fun getTotalValue() {
        var iValue1 = 0
        val iCount = rvList.childCount
        for (i in 0 until iCount){
            val gridChild = rvList.getChildAt(i) as? ViewGroup
            val tv_value1 = gridChild!!.findViewById<View>(R.id.tv_amount) as? TextView

            if (!tv_value1!!.text.toString().isNullOrEmpty()) {
                iValue1 = iValue1 + validate!!.returnIntegerValue(tv_value1.text.toString())
            }

        }

        tv_TotalTodayValue.text = iValue1.toString()


    }

    override fun onBackPressed() {

            val intent = Intent(this, MeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        }


    fun getTotal(list:List<ShareCapitalModel>) {
        var totalAmount = 0
        if (!list.isNullOrEmpty()) {
            for (i in 0 until list.size){
                totalAmount = totalAmount + validate!!.returnIntegerValue(list.get(i).amount.toString())
            }
            tv_TotalTodayValue.text = totalAmount.toString()
        }
    }
}