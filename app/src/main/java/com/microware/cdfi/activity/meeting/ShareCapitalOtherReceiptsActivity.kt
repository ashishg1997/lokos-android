package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.LookupViewmodel
import kotlinx.android.synthetic.main.activity_share_capital_other_receipts.*
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.cash_in_hand_detail_item.*
import kotlinx.android.synthetic.main.repay_toolbar.*
import kotlinx.android.synthetic.main.repaytablayout.*

class ShareCapitalOtherReceiptsActivity : AppCompatActivity() {
    var lookupViewmodel: LookupViewmodel? = null
    var validate: Validate? = null
    var dataspin_receipt_type: List<LookupEntity>? = null
    var dataspin_mode_of_payment: List<LookupEntity>? = null
    var dataspin_name_of_bank: List<LookupEntity>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share_capital_other_receipts)
        validate=Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)


        /*IvAttendance.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSaving.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvWithdrawal.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPenalty.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvRepayment.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBankTransaction.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvCashBox.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvGroupMeeting.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvExpenditurePayment.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvRecipientIncome.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))*/

        btn_cancel.setOnClickListener {
            var intent = Intent(this, MeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        ic_Back.setOnClickListener {
            var intent = Intent(this, MeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_compulsorySaving.setOnClickListener {
            var intent = Intent(this, CompulsorySavingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        lay_saving.setOnClickListener {
            var intent = Intent(this, VoluntorySavingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        lay_loan_disbursment.setOnClickListener {
            var intent = Intent(this, LoanDisbursementActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_withdrawal.setOnClickListener {
            var intent = Intent(this, WidthdrawalDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_penalty.setOnClickListener {
            var intent = Intent(this, PeneltyDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_groupLoanRepaid.setOnClickListener {
            var intent = Intent(this, GroupLoanRepaidActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_groupLoanReceived.setOnClickListener {
            var intent = Intent(this, GroupLoanReceivedActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_BankTransaction.setOnClickListener {
            var intent = Intent(this, BankGroupTransactionActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        lay_CashBox.setOnClickListener {
            var intent = Intent(this, CashBoxActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_GroupMeeting.setOnClickListener {
            var intent = Intent(this, GroupMeetingSummery::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_ExpenditurePayment.setOnClickListener {
            var intent = Intent(this, ExpenditurePaymentActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_RecipientIncome.setOnClickListener {
            var intent = Intent(this, RecipentIncomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        setLabelText()
        fillSpinner()
    }

    override fun onBackPressed() {
        var intent = Intent(this, MeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

    fun setLabelText() {
        tv_title.text = LabelSet.getText(
            "share_capital_other_receipts",
            R.string.share_capital_other_receipts
        )
        tv_name_of_member.text = LabelSet.getText(
            "name_of_member",
            R.string.name_of_member
        )
        tv_receipt_type.text = LabelSet.getText(
            "receipt_type",
            R.string.receipt_type
        )
        tv_amount.text = LabelSet.getText(
            "amount",
            R.string.amount
        )
        tv_narration.text = LabelSet.getText(
            "narration",
            R.string.narration
        )
        tv_mode_of_payment.text = LabelSet.getText(
            "mode_of_payment",
            R.string.mode_of_payment
        )
        tv_name_of_bank.text = LabelSet.getText(
            "name_of_bank",
            R.string.name_of_bank
        )
        tv_cheque_no_transactio_no.text = LabelSet.getText(
            "cheque_no_transactio_no",
            R.string.cheque_no_transactio_no
        )
        tv_cash_in_hand.text = LabelSet.getText(
            "cash_in_hand",
            R.string.cash_in_hand
        )

        et_name_of_member.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_amount.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_narration.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_cheque_no_transactio_no.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )

        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }

    private fun fillSpinner() {
        dataspin_receipt_type = lookupViewmodel!!.getlookup(
                99,
                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_mode_of_payment = lookupViewmodel!!.getlookup(
            99,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_name_of_bank = lookupViewmodel!!.getlookup(
            99,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )


        validate!!.fillspinner(this, spin_receipt_type, dataspin_receipt_type)
        validate!!.fillspinner(this, spin_mode_of_payment, dataspin_mode_of_payment)
        validate!!.fillspinner(this, spin_name_of_bank, dataspin_name_of_bank)
    }

}