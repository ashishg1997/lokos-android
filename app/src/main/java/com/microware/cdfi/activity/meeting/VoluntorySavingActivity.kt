package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.VoluntorySavingAdapter
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import kotlinx.android.synthetic.main.activity_voluntory_saving.*
import kotlinx.android.synthetic.main.activity_voluntory_saving.tv_TotalTodayValue
import kotlinx.android.synthetic.main.activity_voluntory_saving.tv_memberName
import kotlinx.android.synthetic.main.activity_voluntory_saving.tv_srno
import kotlinx.android.synthetic.main.buttons.*

class VoluntorySavingActivity : AppCompatActivity() {
    var validate:Validate?=null
    lateinit var voluntorySavingAdapter:VoluntorySavingAdapter
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    var Todayvalue=0
    var Cumvalue=0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_voluntory_saving)

        validate=Validate(this)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)


        replaceFragmenty(
            fragment = MeetingTopBarFragment(3),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        /*IvAttendance.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSaving.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary))
        IvCompulsorySaving.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloan_disbursment.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvWithdrawal.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPenalty.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvRepayment.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvgroupLoanRepaid.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvgroupLoanReceived.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBankTransaction.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvCashBox.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvGroupMeeting.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvExpenditurePayment.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvRecipientIncome.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvAttendance.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tvSaving.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        tvWithdrawal.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tvPenalty.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tvRepayment.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tvBankTransaction.setBackgroundColor(ContextCompat.getColor(this, R.color.white))*/

        btn_save.isEnabled =
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber) >= validate!!.RetriveSharepreferenceInt(MeetingSP.maxmeetingnumber)

        btn_save.setOnClickListener {
            getVoluntoryValue()
        }
        btn_cancel.setOnClickListener {
            if(validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType)==11 || validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType)==12){
                val intent = Intent(this, CutOffMeetingMenuActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
                finish()
            }else {
                val intent = Intent(this, MeetingMenuActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
                finish()
            }
        }

        setLabelText()
        fillRecyclerView()

    }

    override fun onBackPressed() {
        if(validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType)==11 || validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType)==12){
            val intent = Intent(this, CutOffMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        }else {
            val intent = Intent(this, MeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        }
    }

    fun setLabelText() {
        tv_srno.text = LabelSet.getText("sr_no", R.string.sr_no)
        tv_memberName.text = LabelSet.getText(
            "member_name",
            R.string.member_name
        )
        tv_voluntary_saving.text = LabelSet.getText(
            "voluntary_saving",
            R.string.voluntary_saving
        )
        tv_Today.text = LabelSet.getText(
            "today",
            R.string.today
        ) + " " +LabelSet.getText(
            "rs",
            R.string.rs
        )
        tv_cum.text = LabelSet.getText(
            "cum",
            R.string.cum
        ) + " " +LabelSet.getText(
            "rs",
            R.string.rs
        )

        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }

    private fun fillRecyclerView() {
        var list = generateMeetingViewmodel.getListDataMtgByMtgnum(validate!!.RetriveSharepreferenceInt(
            MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))

        voluntorySavingAdapter = VoluntorySavingAdapter(this,list)

        rvList.layoutManager = LinearLayoutManager(this)

        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = rvList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvList.layoutParams = params
        rvList.adapter = voluntorySavingAdapter


    }

    fun getAdjustmentSaving(mtgno:Int,memid:Long,auid:Int):Int?{
        var amount = 0
        amount = validate!!.returnIntegerValue(generateMeetingViewmodel.getMemberAdjustmentAmount(mtgno,memid,auid).toString())
        return amount
    }

    fun getTotalValue() {
        var iValue = 0
        var iValueCum = 0
        val iCount = rvList.childCount


        for (i in 0 until iCount) {
            val gridChild =
                rvList.getChildAt(i) as? ViewGroup

            val tv_count = gridChild!!
                .findViewById<View>(R.id.tv_count) as? EditText

            val tv_cum2 = gridChild
                .findViewById<View>(R.id.tv_cum2) as? TextView

            if (!tv_count!!.text.toString().isNullOrEmpty()) {
                iValue=iValue + validate!!.returnIntegerValue(tv_count.text.toString())
            }

            if (!tv_cum2!!.text.toString().isNullOrEmpty()) {
                iValueCum=iValueCum + validate!!.returnIntegerValue(tv_cum2.text.toString())
            }


        }
        tv_TotalTodayValue.text = iValue.toString()
        tv_TotalCumValue.text = iValueCum.toString()

    }

    fun getTotalValue(iValue: Int,flag:Int) {
        if(flag==1) {
            Todayvalue = Todayvalue + iValue
            tv_TotalTodayValue.text = Todayvalue.toString()
        }else if(flag==2){
            Cumvalue = Cumvalue + iValue
            tv_TotalCumValue.text = Cumvalue.toString()
        }
    }

    fun getVoluntoryValue() {
        var saveValue = 0

        val iCount = rvList.childCount


        for (i in 0 until iCount) {
            val gridChild =
                rvList.getChildAt(i) as? ViewGroup

            val tv_count = gridChild!!
                .findViewById<View>(R.id.tv_count) as? EditText

            val et_GroupMCode = gridChild
                .findViewById<View>(R.id.et_GroupMCode) as? EditText

            val et_SavVolOb = gridChild
                .findViewById<View>(R.id.et_SavVolOb) as? EditText

            if (tv_count!!.length() > 0) {
                var iValue = validate!!.returnIntegerValue(tv_count.text.toString())
                var GroupMCode = validate!!.returnLongValue(et_GroupMCode!!.text.toString())
                var SavVolOb = validate!!.returnIntegerValue(et_SavVolOb!!.text.toString())
                saveValue=saveData(iValue,GroupMCode,SavVolOb)

            }
        }

        if(saveValue > 0){
            CDFIApplication.database?.dtmtgDao()
                ?.updatevolsave(validate!!.returnIntegerValue(tv_TotalCumValue.text.toString()),validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))


            replaceFragmenty(
                fragment = MeetingTopBarFragment(3),
                allowStateLoss = true,
                containerViewId = R.id.mainContent
            )
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this
            )
        }
    }

    fun saveData(iValue: Int, group_m_code:Long, SavVolOb:Int):Int {
        var value=0
        var SavVolCb=iValue+SavVolOb
        generateMeetingViewmodel.updateVoluntorySaving(
            iValue,
            SavVolCb,
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            group_m_code,validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
            validate!!.Daybetweentime(validate!!.currentdatetime)
        )
        value=1

        return value
    }
}