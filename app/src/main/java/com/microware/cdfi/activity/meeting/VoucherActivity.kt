package com.microware.cdfi.activity.meeting

import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.VoucherAdapter
import com.microware.cdfi.entity.*
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.MstCOAViewmodel
import com.microware.cdfi.viewModel.VouchersViewModel
import kotlinx.android.synthetic.main.activity_member_designation.*
import kotlinx.android.synthetic.main.activity_voucher.*
import kotlinx.android.synthetic.main.fragment_basicdetail.*
import kotlinx.android.synthetic.main.row_dialog_voucher_list_layout.*
import kotlinx.android.synthetic.main.white_toolbar.*

class VoucherActivity : AppCompatActivity() {
    var validate: Validate? = null
    var vouchersViewModel: VouchersViewModel? = null
    var mstcoaViewModel: MstCOAViewmodel? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_voucher)
        tv_title.text = LabelSet.getText(
            "voucher_list",
            R.string.voucher_list
        )
        validate = Validate(this)
        vouchersViewModel = ViewModelProviders.of(this).get(VouchersViewModel::class.java)
        mstcoaViewModel = ViewModelProviders.of(this).get(MstCOAViewmodel::class.java)


        fillData()

    }


    private fun fillData() {
        vouchersViewModel!!.getDataList(
           // 1538352022,
            validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )!!
            .observe(this, object : Observer<List<ShgFinancialTxnVouchersEntity>> {
                override fun onChanged(voucher_list: List<ShgFinancialTxnVouchersEntity>?) {
                    if (!voucher_list.isNullOrEmpty() && voucher_list.size > 0) {
                        rvVocherList.layoutManager = LinearLayoutManager(this@VoucherActivity)
                        rvVocherList.adapter = VoucherAdapter(this@VoucherActivity, voucher_list,mstcoaViewModel)
                    }
                }
            })
    }


    fun OpenVoucherDialog(
        voucher_date: Long,
        name: String,
        amount: Int?,
        cbo_id: Long,
        voucherNo: String
    ) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.row_dialog_voucher_list_layout, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.et_bank.isEnabled = false
        mAlertDialog.et_amt.isEnabled = false
        mAlertDialog.et_bank.isClickable = false
        mAlertDialog.et_amt.isClickable = false

        mAlertDialog.et_bank.text =name

        if(amount != null){
            mAlertDialog.et_amt.text = amount.toString()
}


    mAlertDialog.btn_save.setOnClickListener {
        if(validate!!.returnStringValue(
                mAlertDialog.et_reliDate.text.toString()
            ).trim().length == 0){

            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "realisation_date",
                    R.string.realisation_date
                ),
                this,
                mAlertDialog.et_reliDate
            )
        }else{
            vouchersViewModel!!.updateDateRelisation(validate!!.Daybetweentime(mAlertDialog.et_reliDate.text.toString()),voucherNo,cbo_id)
            mAlertDialog.dismiss()


        }
}

        mAlertDialog.et_reliDate.setOnClickListener {
            validate!!.datePickerwithmindate(
                voucher_date,
                mAlertDialog.et_reliDate
            )
        }
    }




//    validate!!.CustomAlert(
//    LabelSet.getText(
//    "data_updated_successfully",
//    R.string.data_updated_successfully
//    ),
//    this, VoucherActivity::class.java
//    )
}