package com.microware.cdfi.activity.meeting

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.WithdrawalDetailAdapter
import com.microware.cdfi.entity.DtmtgDetEntity
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.fragment.MeetingTopBarFragment
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.utility.replaceFragmenty
import kotlinx.android.synthetic.main.activity_shg_withdrawal.view.*
import kotlinx.android.synthetic.main.activity_widthdrawal_detail.*
import kotlinx.android.synthetic.main.activity_widthdrawal_detail.rvList
import kotlinx.android.synthetic.main.activity_widthdrawal_detail.tv_TotalTodayValue
import kotlinx.android.synthetic.main.activity_widthdrawal_detail.tv_memberName
import kotlinx.android.synthetic.main.activity_widthdrawal_detail.tv_srno
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.buttons.view.*
import kotlinx.android.synthetic.main.repay_toolbar.view.*

class WidthdrawalDetailActivity : AppCompatActivity() {
    var lookupViewmodel: LookupViewmodel? = null
    var validate: Validate? = null
    var dataspin_name_of_member: List<LookupEntity>? = null
    var dataspin_type_of_saving_deposit: List<LookupEntity>? = null
    lateinit var withdrawalDetailAdapter:WithdrawalDetailAdapter
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_widthdrawal_detail)
        validate=Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)


        btn_cancel.setOnClickListener {
            var intent = Intent(this, MeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        replaceFragmenty(
            fragment = MeetingTopBarFragment(5),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )
        
        setLabelText()
        fillRecyclerView()

    }

    override fun onBackPressed() {
//        var intent = Intent(this, WidthdrawalActivity::class.java)
        var intent = Intent(this, MeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0,0)
        finish()
    }

    fun setLabelText() {
        tv_srno.text = LabelSet.getText("sr_no", R.string.sr_no)
        tv_memberName.text = LabelSet.getText(
            "member_name",
            R.string.member_name
        )
        tv_total_saved.text = LabelSet.getText(
            "total_saved",
            R.string.total_saved
        )
        tv_withdrawl_n_for_saving.text = LabelSet.getText(
            "withdrawl_n_for_saving",
            R.string.withdrawl_n_for_saving
        )
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }

    private fun fillRecyclerView() {
        var list = generateMeetingViewmodel.getListDataMtgByMtgnum(validate!!.RetriveSharepreferenceInt(
            MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(AppSP.shgid))
        withdrawalDetailAdapter = WithdrawalDetailAdapter(this,list)
        getTotal(list)
        rvList.layoutManager = LinearLayoutManager(this)

        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = rvList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvList.layoutParams = params
        rvList.adapter = withdrawalDetailAdapter
    }

    fun getTotal(list: List<DtmtgDetEntity>) {
        var totalsavingAmount = 0
        var totalwithAmount = 0
        if (!list.isNullOrEmpty()) {
            for (i in 0 until list.size){
                totalsavingAmount = totalsavingAmount + validate!!.returnIntegerValue(list.get(i).sav_vol_cb.toString())
                totalwithAmount = totalwithAmount + validate!!.returnIntegerValue(list.get(i).sav_vol_withdrawal.toString())
            }
            tv_TotalTodayValue.text = totalsavingAmount.toString()
            tv_totwithdrawl.text = totalwithAmount.toString()
        }
    }
    fun CustomAlert() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.activity_shg_withdrawal, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.WHITE))
        mDialogView.tv_title.text = LabelSet.getText(
            "withdrawal",
            R.string.withdrawal
        )
//        mDialogView.img_scan.visibility = View.INVISIBLE
//        mDialogView.img_refresh.visibility = View.INVISIBLE
        fillSpinner(mDialogView)
        setLabelText(mDialogView)
        /* mDialogView.btn_yes.setOnClickListener {
            mAlertDialog.dismiss()


        }

        mDialogView.btn_no.setOnClickListener {

            mAlertDialog.dismiss()*/


    }

    fun setLabelText(view: View) {
        view.spin_name_of_member.isEnabled=false
//        view.tv_title.setText(LabelSet.getText("withdrawal", R.string.withdrawal))
        view.tv_name_of_member.text = LabelSet.getText(
            "name_of_member",
            R.string.name_of_member
        )
        view.tv_type_of_saving_deposit.text = LabelSet.getText(
            "type_of_saving_deposit",
            R.string.type_of_saving_deposit
        )
        view.tv_total_saved_amount.text = LabelSet.getText(
            "total_saved_amount",
            R.string.total_saved_amount
        )
        view.tv_total_share_in_profit.text = LabelSet.getText(
            "total_share_in_profit",
            R.string.total_share_in_profit
        )
        view.tv_total_outstanding_loan_payable.text = LabelSet.getText(
            "total_outstanding_loan_payable",
            R.string.total_outstanding_loan_payable
        )
        view.tv_total_amount_available_for_withdrawal.text = LabelSet.getText(
            "total_amount_available_for_withdrawal",
            R.string.total_amount_available_for_withdrawal
        )
        view.tv_amount_paid.text = LabelSet.getText(
            "amount_paid",
            R.string.amount_paid
        )
        view.tv_name_of_receiver_nominee.text = LabelSet.getText(
            "name_of_receiver_nominee",
            R.string.name_of_receiver_nominee
        )
        view.tv_Relation_of_receiver.text = LabelSet.getText(
            "relation_of_receiver",
            R.string.relation_of_receiver
        )
        view.tv_date_of_payment.text = LabelSet.getText(
            "date_of_payment",
            R.string.date_of_payment
        )
//        view.tv_cash_in_hand.setText(LabelSet.getText("cash_in_hand", R.string.cash_in_hand))
//        view.tv_code.setText(LabelSet.getText("_16649", R.string._16649))
//        view.tv_nam.setText(LabelSet.getText("adarsh_mahila_sangam", R.string.adarsh_mahila_sangam))
//        view.tv_date.setText(LabelSet.getText("_19_nov_2014", R.string._19_nov_2014))

        view.et_total_saved_amount.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        view.et_total_share_in_profit.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        view.et_total_outstanding_loan_payable.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        view.et_total_amount_available_for_withdrawal.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        view.et_amount_paid.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        view.et_name_of_receiver_nominee.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        view.et_Relation_of_receiver.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        view.et_date_of_payment.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )

        view.btn_save.text = LabelSet.getText(
            "save",
            R.string.save
        )
        view.btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }

    private fun fillSpinner(view: View) {
        dataspin_name_of_member = lookupViewmodel!!.getlookup(
            99,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_type_of_saving_deposit = lookupViewmodel!!.getlookup(
            99,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )


        fillspinner(this, view.spin_name_of_member, dataspin_name_of_member)
        validate!!.fillspinner(this, view.spin_type_of_saving_deposit, dataspin_type_of_saving_deposit)
    }

    fun fillspinner(activity: Activity, spin: Spinner, data: List<LookupEntity>?) {

        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize)

            for (i in data.indices) {
                sValue[i] = data[i].key_value
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = validate!!.RetriveSharepreferenceString(MeetingSP.MemberName)
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }

    }

}