package com.microware.cdfi.activity.meeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.DtMemSettlementEntity
import com.microware.cdfi.entity.DtmtgDetEntity
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_widthdrawal.*
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.repay_toolbar.*

class WithdrawalActivity : AppCompatActivity() {
    var lookupViewmodel: LookupViewmodel? = null
    var dtLoanTxnMemViewmodel: DtLoanTxnMemViewmodel? = null
    var memSettlementViewmodel: MemSettlementViewmodel? = null
    var dtLoanMemberScheduleViewmodel: DtLoanMemberScheduleViewmodel? = null
    var generateMeetingViewmodel: GenerateMeetingViewmodel? = null
    var validate: Validate? = null
    var dataspin_name_of_member: List<LookupEntity>? = null
    var dataspin_withdrawl: List<LookupEntity>? = null
    var dataspin_reason_for_withdrawal: List<LookupEntity>? = null
    var dataspin_relation: List<LookupEntity>? = null
    var memberlist: List<DtmtgDetEntity>? = null
    var dtMemSettlementEntity: DtMemSettlementEntity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_widthdrawal)
        validate = Validate(this)
        setLabelText()
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        memSettlementViewmodel = ViewModelProviders.of(this).get(MemSettlementViewmodel::class.java)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        dtLoanTxnMemViewmodel =
            ViewModelProviders.of(this).get(DtLoanTxnMemViewmodel::class.java)
        dtLoanMemberScheduleViewmodel =
            ViewModelProviders.of(this).get(DtLoanMemberScheduleViewmodel::class.java)

        fillmemberspinner()
        btn_cancel.setOnClickListener {
            var intent = Intent(this, WidthdrawalDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                SaveData()
            }
        }

        showdata()

    }

    fun SaveData() {
        generateMeetingViewmodel!!.updatewithdrawl(
            validate!!.returnIntegerValue(et_amount_paid_to_member.text.toString()),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid),
            validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
            validate!!.Daybetweentime(validate!!.currentdatetime)
        )
        validate!!.CustomAlert(
            LabelSet.getText(
                "data_saved_successfully",
                R.string.data_saved_successfully
            ),
            this,WidthdrawalDetailActivity::class.java
        )
    }

    fun showdata() {
        val list = generateMeetingViewmodel!!.getCompulsoryData(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid)
        )
        if (!list.isNullOrEmpty()) {
            et_voluntary_saving.setText(list[0].sav_vol_cb.toString())
            et_amount_paid_to_member.setText(list[0].sav_vol_cb.toString())
        }


    }

    private fun checkValidation(): Int {
        var value = 1
        if (spin_entry_member_name.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_entry_member_name,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "name_of_member",
                    R.string.name_of_member
                )
            )
            value = 0
        } else if (et_amount_paid_to_member.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "amount_paid_to_member_amount_received_from_member",
                    R.string.amount_paid_to_member_amount_received_from_member
                ),
                this, et_amount_paid_to_member
            )
            value = 0
        } else if (validate!!.returnIntegerValue(et_amount_paid_to_member.text.toString()) > validate!!.returnIntegerValue(
                et_voluntary_saving.text.toString()
            )
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "amount_paid_to_member_amount_received_from_member",
                    R.string.amount_paid_to_member_amount_received_from_member
                ),
                this, et_amount_paid_to_member
            )
            value = 0
        }

        return value
    }

    /* private fun SaveData() {
         dtMemSettlementEntity = DtMemSettlementEntity(
             0,
             validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
             validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!,
             validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
             validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate),
             validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid),
             validate!!.returnIntegerValue(et_compulsory_saving.text.toString()),
             validate!!.returnIntegerValue(et_voluntary_saving.text.toString()),
             validate!!.returnIntegerValue(et_share_capital.text.toString()),
             validate!!.returnIntegerValue(et_membersurplus.text.toString()),
             validate!!.returnIntegerValue(et_total_outstanding_loan_on_member.text.toString()),
             validate!!.returnIntegerValue(et_total_amount_available.text.toString()),
             validate!!.returnIntegerValue(et_amount_paid_to_member.text.toString()),
             validate!!.returnStringValue(et_name_of_receiver_nominee.text.toString()),
             validate!!.returnlookupcode(spin_relation, dataspin_relation),
             validate!!.returnlookupcode(spin_reason_for_withdrawal, dataspin_reason_for_withdrawal),
             validate!!.Daybetweentime(et_date_of_payment.text.toString()),
             validate!!.RetriveSharepreferenceString(AppSP.userid),
             validate!!.Daybetweentime(validate!!.currentdatetime),
             "",
             0,
             validate!!.RetriveSharepreferenceString(AppSP.userid),
             0
         )

         memSettlementViewmodel!!.insert(dtMemSettlementEntity!!)
         validate!!.CustomAlert(
             LabelSet.getText(
                 "data_saved_successfully",
                 R.string.data_saved_successfully
             ),
             this, WidthdrawalDetailActivity::class.java
         )

     }*/

    fun setLabelText() {
        tv_title.text = LabelSet.getText("withdrawal", R.string.withdrawal)
        tv_name_of_member.text = LabelSet.getText(
            "name_of_member",
            R.string.name_of_member
        )




        btn_save.text = LabelSet.getText(
            "save",
            R.string.save
        )
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }


    private fun fillmemberspinner() {
        memberlist = generateMeetingViewmodel!!.getListDataMtgByMtgnum(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        val adapter: ArrayAdapter<String?>
        if (!memberlist.isNullOrEmpty()) {
            val isize = memberlist!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in memberlist!!.indices) {
                sValue[i + 1] = memberlist!![i].member_name
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_entry_member_name.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(memberlist!!.size + 1)
            sValue[0] = LabelSet.getText(
                "nomemberavailable",
                R.string.nomemberavailable
            )
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_entry_member_name.adapter = adapter
        }
        setMember()

    }

    fun returnmemaid(): Long {

        var pos = spin_entry_member_name.selectedItemPosition
        var id = 0L

        if (!memberlist.isNullOrEmpty()) {
            if (pos > 0) id = memberlist!!.get(pos - 1).mem_id
        }
        return id
    }

    fun returnmemid(): Long {

        var pos = spin_entry_member_name.selectedItemPosition
        var id = 0L

        if (!memberlist.isNullOrEmpty()) {
            if (pos > 0) id = memberlist!!.get(pos - 1).mem_id
        }
        return id
    }

    fun setMember(): Int {
        var pos = 0
        if (!memberlist.isNullOrEmpty()) {
            for (i in memberlist!!.indices) {
                if (validate!!.RetriveSharepreferenceLong(MeetingSP.Memberid) == memberlist!!.get(i).mem_id)
                    pos = i + 1
            }
        }
        spin_entry_member_name.setSelection(pos)
        spin_entry_member_name.isEnabled = false
        return pos
    }

    override fun onBackPressed() {
        var intent = Intent(this, WidthdrawalDetailActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

}