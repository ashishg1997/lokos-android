package com.microware.cdfi.activity.vo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.activity.vo.VOListActivity
import com.microware.cdfi.api.model.EcScModelJoindata
import com.microware.cdfi.entity.*
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_clf.*
import kotlinx.android.synthetic.main.summary_toolbar.*
import java.io.File

class ClfSummaryctivity : AppCompatActivity() {

    var validate: Validate? = null
    var federationViewmodel: FedrationViewModel? = null
    var lookupViewmodel: LookupViewmodel? = null
    var locationViewModel: LocationViewModel? = null
    var dataPanchayat : List<PanchayatEntity>? = null
    var dataVillage : List<VillageEntity>? = null
    var dataspinMember: List<EcScModelJoindata>? = null
    var cboAddressViewModel: CboAddressViewmodel? = null
    var cboBankViewmodel: CboBankViewmodel? = null
    var bankMasterViewModel : MasterBankViewmodel? = null
    var masterBankBranchViewModel : MasterBankBranchViewModel? = null
    var cboKycViewmodel: CboKycViewmodel? = null
    var executiveviewmodel: ExecutiveMemberViewmodel? = null
    var cboPhoneViewmodel: CboPhoneViewmodel? = null
    var scMemberViewmodel: Subcommitee_memberViewModel? = null
    var committeeViewModel: SubcommitteeViewModel? = null
    var committeeMasterViewmodel: subcommitteeMasterViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_clf)

        validate = Validate(this)
        federationViewmodel = ViewModelProviders.of(this).get(FedrationViewModel::class.java)
        locationViewModel = ViewModelProviders.of(this).get(LocationViewModel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        cboAddressViewModel = ViewModelProviders.of(this).get(CboAddressViewmodel::class.java)
        cboBankViewmodel = ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        bankMasterViewModel = ViewModelProviders.of(this).get(MasterBankViewmodel::class.java)
        masterBankBranchViewModel = ViewModelProviders.of(this).get(MasterBankBranchViewModel::class.java)
        cboKycViewmodel = ViewModelProviders.of(this).get(CboKycViewmodel::class.java)
        executiveviewmodel = ViewModelProviders.of(this).get(ExecutiveMemberViewmodel::class.java)
        cboPhoneViewmodel = ViewModelProviders.of(this).get(CboPhoneViewmodel::class.java)
        scMemberViewmodel = ViewModelProviders.of(this).get(Subcommitee_memberViewModel::class.java)
        committeeViewModel = ViewModelProviders.of(this).get(SubcommitteeViewModel::class.java)
        committeeMasterViewmodel = ViewModelProviders.of(this).get(subcommitteeMasterViewModel::class.java)

        icBack.setOnClickListener {
            var intent = Intent(this, VOListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

        tv_title.text =
            validate!!.RetriveSharepreferenceString(AppSP.FedrationName)!! + "(" + validate!!.RetriveSharepreferenceString(
                AppSP.FedrationCode
            ) + ")"


        liBasicDetail.visibility = View.VISIBLE
        ivUpBasicDetail.visibility = View.VISIBLE
        ivDownBasicDetail.visibility = View.GONE

        ivDownBasicDetail.setOnClickListener {
            liBasicDetail.visibility = View.VISIBLE
            ivUpBasicDetail.visibility = View.VISIBLE
            ivDownBasicDetail.visibility = View.GONE
        }
        ivUpBasicDetail.setOnClickListener {
            liBasicDetail.visibility = View.GONE
            ivUpBasicDetail.visibility = View.GONE
            ivDownBasicDetail.visibility = View.VISIBLE
        }

        ivDownMobileDetail.setOnClickListener {
            liMobileDetail.visibility = View.VISIBLE
            ivUpMobileDetail.visibility = View.VISIBLE
            ivDownMobileDetail.visibility = View.GONE
        }
        ivUpMobileDetail.setOnClickListener {
            liMobileDetail.visibility = View.GONE
            ivUpMobileDetail.visibility = View.GONE
            ivDownMobileDetail.visibility = View.VISIBLE
        }
        ivUpPhoneDetail.setOnClickListener {
            liPhoneDetail.visibility = View.GONE
            ivUpPhoneDetail.visibility = View.GONE
            ivDownPhoneDetail.visibility = View.VISIBLE
        }

        ivDownPhoneDetail.setOnClickListener {
            liPhoneDetail.visibility = View.VISIBLE
            ivUpPhoneDetail.visibility = View.VISIBLE
            ivDownPhoneDetail.visibility = View.GONE
        }

        ivDownAddressDetails.setOnClickListener {
            liAddressDetails.visibility = View.VISIBLE
            ivUpAddressDetails.visibility = View.VISIBLE
            ivDownAddressDetails.visibility = View.GONE
        }
        ivUpAddressDetails.setOnClickListener {
            liAddressDetails.visibility = View.GONE
            ivUpAddressDetails.visibility = View.GONE
            ivDownAddressDetails.visibility = View.VISIBLE
        }
        ivDownBankDetails.setOnClickListener {
            liBankDetails.visibility = View.VISIBLE
            ivUpBankDetails.visibility = View.VISIBLE
            ivDownBankDetails.visibility = View.GONE
        }
        ivUpBankDetails.setOnClickListener {
            liBankDetails.visibility = View.GONE
            ivUpBankDetails.visibility = View.GONE
            ivDownBankDetails.visibility = View.VISIBLE
        }

        ivUpKycDetailsID.setOnClickListener {
            liKycDetailsID.visibility = View.GONE
            ivUpKycDetailsID.visibility = View.GONE
            ivDownKycDetailsID.visibility = View.VISIBLE
        }

        ivDownKycDetailsID.setOnClickListener {
            liKycDetailsID.visibility = View.VISIBLE
            ivUpKycDetailsID.visibility = View.VISIBLE
            ivDownKycDetailsID.visibility = View.GONE
        }

        ivUpSubCommitteeID.setOnClickListener {
            liSubCommitteeID.visibility = View.GONE
            ivUpSubCommitteeID.visibility = View.GONE
            ivDownSubCommitteeID.visibility = View.VISIBLE
        }

        ivDownSubCommitteeID.setOnClickListener {
            liSubCommitteeID.visibility = View.VISIBLE
            ivUpSubCommitteeID.visibility = View.VISIBLE
            ivDownSubCommitteeID.visibility = View.GONE
        }

        /*ivUpScMemberID.setOnClickListener {
            liScMemberID.visibility = View.GONE
            ivUpScMemberID.visibility = View.GONE
            ivDownScMemberID.visibility = View.VISIBLE
        }

        ivDownScMemberID.setOnClickListener {
            liScMemberID.visibility = View.VISIBLE
            ivUpScMemberID.visibility = View.VISIBLE
            ivDownScMemberID.visibility = View.GONE
        }*/

        showData()

    }

    private fun showData() {
        try {
            var listData = federationViewmodel!!.getFedrationdata(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))

            if(!listData.isNullOrEmpty()){
                var dataspin_pramotedby: List<LookupEntity>? = null
                var dataspin_meetingfreq: List<LookupEntity>? = null
                var dataspin_yesno: List<LookupEntity>? = null
                var databookkeeper: List<LookupEntity>? = null
                var dataspin_status: List<LookupEntity>? = null
                dataspin_status = lookupViewmodel!!.getlookup(
                    5,
                    validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
                )
                dataspin_pramotedby = lookupViewmodel!!.getlookupfromkeycode("PROMOTEDBY",
                    validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
                dataspin_meetingfreq = lookupViewmodel!!.getlookup(
                    19,
                    validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
                )
                databookkeeper = lookupViewmodel!!.getlookup(
                    44,
                    validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
                )

                dataspin_yesno =  lookupViewmodel!!.getlookup(9,validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))



                var panchayatname = locationViewModel!!.getPanchayatName(listData.get(0).panchayat_id!!)
                if (!panchayatname.isNullOrEmpty()){
                    tv_set_grampanchayat.text = panchayatname
                }else{
                    tv_set_grampanchayat.text = ""
                }

                var villageName = locationViewModel!!.getVillageName(listData.get(0).village_id!!)
                if (!villageName.isNullOrEmpty()) {
                    tv_set_village.text = villageName
                } else {
                    tv_set_village.text = ""
                }

                tv_set_name.text = listData.get(0).federation_name
                tv_set_formationDate.text = validate!!.convertDatetime(validate!!.returnLongValue(listData.get(0).federation_formation_date.toString()))
                tv_Set_promotedby.text = validate!!.returnlookupcodevalue(
                    listData.get(0).promoted_by,
                    dataspin_pramotedby
                )

                tv_Set_promoter_name.text = validate!!.returnStringValue(listData.get(0).promoter_name)

                tv_Set_frequency.text = validate!!.returnlookupcodevalue(
                    listData.get(0).meeting_frequency,
                    dataspin_meetingfreq
                )

                tv_set_Intermediation.text = validate!!.returnlookupcodevalue(
                    listData.get(0).is_financial_intermediation,
                    dataspin_yesno
                )

                tv_set_savingfrequency.text = validate!!.returnlookupcodevalue(
                    listData.get(0).saving_frequency,
                    dataspin_meetingfreq
                )

                tv_set_saving.text = validate!!.returnStringValue(listData.get(0).month_comp_saving.toString())
                tv_set_ComsavingRoi.text = validate!!.returnStringValue(listData.get(0).savings_interest.toString())
                tv_set_voluntarysavingROI.text = validate!!.returnStringValue(listData.get(0).voluntary_savings_interest.toString())

                tv_Set_volunteer.text = validate!!.returnlookupcodevalue(
                    listData.get(0).is_voluntary_saving,
                    dataspin_yesno
                )

                tv_set_Bookeeper_identify.text = validate!!.returnlookupcodevalue(
                    listData.get(0).bookkeeper_identified,
                    databookkeeper
                )

                tv_set_bookkeeper_name.text = validate!!.returnStringValue(listData.get(0).bookkeeper_name)
                tv_set_bookkeeper_s_mobile_no.text = validate!!.returnStringValue(listData.get(0).bookkeeper_mobile)
                tv_set_electiontenure.text = validate!!.returnStringValue(listData.get(0).election_tenure.toString())

                tv_set_status.text = validate!!.returnlookupcodevalue(
                    listData.get(0).status,
                    dataspin_status
                )

                val mediaStorageDirectory = File(
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                    AppSP.IMAGE_DIRECTORY_NAME
                )
                var mediaFile1 = File(mediaStorageDirectory.path + File.separator + validate!!.returnStringValue(
                    listData.get(0).federation_resolution))
                Picasso.with(this).load(mediaFile1).into(iv_set_resolution_copy)



            }

            cboAddressViewModel!!.getAddressdata(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))!!
                .observe(this,object : Observer<List<AddressEntity>> {
                    override fun onChanged(t: List<AddressEntity>?) {
                        if (!t.isNullOrEmpty()){
                            tv_set_address_1.text = t.get(0).address_line1
                            tv_set_address_2.text = t.get(0).address_line2
                            tv_set_pincode.text = validate!!.returnStringValue(t.get(0).postal_code.toString())
                            var villageName =
                                locationViewModel!!.getVillageName(t.get(0).village_id!!)
                            if (!villageName.isNullOrEmpty()) {
                                tv_set_village_town.text = villageName
                            } else {
                                tv_set_village_town.text = ""
                            }

                            var panchayatname =
                                locationViewModel!!.getPanchayatName(t.get(0).panchayat_id!!)
                            if (!panchayatname.isNullOrEmpty()) {
                                tv_set_grampanchayat1.text = panchayatname
                            } else {
                                tv_set_grampanchayat1.text = ""
                            }

                        }
                    }
                })

            cboBankViewmodel!!.getBankdata(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))!!
                .observe(this,object : Observer<List<Cbo_bankEntity>> {
                    override fun onChanged(bank_list: List<Cbo_bankEntity>?) {
                        var dataspin_yesno: List<LookupEntity>? = null
                        if (!bank_list.isNullOrEmpty() && bank_list.size > 0 ){
                            dataspin_yesno = lookupViewmodel!!.getlookup(
                                9,
                                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
                            )
                            tv_set_is_default_bank.text = validate!!.returnlookupcodevalue(
                                bank_list.get(0).is_default!!,
                                dataspin_yesno
                            )
                            tv_set_nameinbankpassbook.text = bank_list.get(0).bankpassbook_name
                            tv_set_ifsc_code.text = bank_list.get(0).ifsc_code
                            tv_set_account_no.text = bank_list.get(0).account_no
                            tv_set_account_opening_date.text = validate!!.returnStringValue(bank_list.get(0).account_opening_date.toString())
                            val mediaStorageDirectory = File(
                                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                                AppSP.IMAGE_DIRECTORY_NAME
                            )
                            var mediaFile1 = File(mediaStorageDirectory.path + File.separator + validate!!.returnStringValue(
                                bank_list.get(0).passbook_firstpage))
                            Picasso.with(this@ClfSummaryctivity).load(mediaFile1).into(iv_set_upload_passbook_first_page)
                            var bankname = bankMasterViewModel!!.getBankName(bank_list.get(0).bank_id!!)
                            if (!bankname.isNullOrEmpty()){
                                tv_set_bank_name.text = bankname
                            }else{
                                tv_set_bank_name.text = ""
                            }

                            var bankbranchname = masterBankBranchViewModel!!.getBranchname(bank_list.get(0).bank_branch_id!!)
                            if (!bankbranchname.isNullOrEmpty()){
                                tv_set_bank_branch.text = bankbranchname
                            }else{
                                tv_set_bank_branch.text = ""
                            }

                        }
                    }
                })

            cboKycViewmodel!!.getKycdetaildata(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))!!
                .observe(this,object : Observer<List<Cbo_kycEntity>> {
                    override fun onChanged(kyc_list: List<Cbo_kycEntity>?) {
                        if (!kyc_list.isNullOrEmpty()){
                            var dataspin_doctype: List<LookupEntity>? = null
                            dataspin_doctype = lookupViewmodel!!.getlookup(35, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
                            tv_set_kyc_id_type.text = validate!!.returnlookupcodevalue(kyc_list.get(0).kyc_type, dataspin_doctype)
                            tv_set_kyc_id.text = validate!!.returnStringValue(kyc_list.get(0).kyc_number)
                            val mediaStorageDirectory = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), AppSP.IMAGE_DIRECTORY_NAME)
                            var mediaFile = File(mediaStorageDirectory.path + File.separator + validate!!.returnStringValue(
                                kyc_list.get(0).kyc_front_doc_orig_name))
                            Picasso.with(this@ClfSummaryctivity).load(mediaFile).into(iv_set_frontimage)
                        }
                    }
                })

            cboPhoneViewmodel!!.getphonedata(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))!!
                .observe(this,object : Observer<List<Cbo_phoneEntity>> {
                    override fun onChanged(cbo_list: List<Cbo_phoneEntity>?) {
                        if (!cbo_list.isNullOrEmpty()){
                            var member_Name = returnMemberName(cbo_list.get(0).member_guid)
                            if (!member_Name.isNullOrEmpty()) {
                                tv_set_mobile_belongs_to.text = member_Name
                            }else{
                                tv_set_mobile_belongs_to.text = ""
                            }

                            tv_set_mobile_no.text = validate!!.returnStringValue(cbo_list.get(0).mobile_no)
                        }
                    }
                })

            executiveviewmodel!!.getExecutiveList(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))!!
                .observe(this,object : Observer<List<Executive_memberEntity>> {
                    override fun onChanged(ecMemberlist: List<Executive_memberEntity>?) {
                        if (!ecMemberlist.isNullOrEmpty()){
                            var dataspin_status: List<LookupEntity>? = null
                            var dataspindesignation:List<LookupEntity>? = null
                            dataspindesignation = lookupViewmodel!!.getlookup(49,validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
                            dataspin_status = lookupViewmodel!!.getlookup(5, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
                            tv_set_formDate.text = validate!!.convertDatetime(ecMemberlist.get(0).joining_date!!)
                            tv_set_toDate.text = validate!!.convertDatetime(ecMemberlist.get(0).leaving_date!!)

                            tv_set_status.text = validate!!.returnlookupcodevalue(
                                ecMemberlist.get(0).status,
                                dataspin_status
                            )

                            tv_set_ecdesignation.text = validate!!.returnlookupcodevalue(
                                ecMemberlist.get(0).designation,
                                dataspin_status
                            )
                        }
                    }
                })

            committeeViewModel!!.getCommitteedata(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))!!
                .observe(this,object : Observer<List<subcommitteeEntity>> {
                    override fun onChanged(subCommitteelist: List<subcommitteeEntity>?) {
                        if (!subCommitteelist.isNullOrEmpty()){
                            var dataspin_status: List<LookupEntity>? = null
                            dataspin_status = lookupViewmodel!!.getlookup(
                                5,
                                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
                            )
                            var dataspincommittee:List<subcommitee_masterEntity>? = null
                            dataspincommittee = committeeMasterViewmodel!!.getScMaster(validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
                            var commiteename = validate!!.returnSubcommitteeName(subCommitteelist.get(0).subcommitee_type_id,dataspincommittee)

                            if (!commiteename.isNullOrEmpty()){
                                tv_set_commitee.text = commiteename
                            }else{
                                tv_set_commitee.text = ""
                            }

                            tv_Set_sub_formationdate.text = validate!!.convertDatetime(subCommitteelist.get(0).fromdate!!)
                            tv_Set_sub_status.text = validate!!.returnlookupcodevalue(
                                subCommitteelist.get(0).status,
                                dataspin_status
                            )
                        }
                    }
                })


        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    fun returnMemberName(memberGuid:String): String {
        var   dataspinMember = scMemberViewmodel!!.getExecutiveMemberList(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var sValue = ""

        if (!dataspinMember.isNullOrEmpty()) {
            for (i in dataspinMember.indices) {
                if (memberGuid.equals(dataspinMember.get(i).member_guid))
                    sValue = dataspinMember.get(i).member_name!!
            }
        }
        return sValue
    }

    override fun onBackPressed() {
        super.onBackPressed()
        var intent = Intent(this, VOListActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }
}