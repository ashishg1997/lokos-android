package com.microware.cdfi.activity.vo

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.ArrayAdapter
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.VOFundsBankListAdapter
import com.microware.cdfi.entity.BankEntity
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.entity.VoCoaMappingEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import com.microware.cdfi.viewModel.*
import com.microware.cdfi.viewModel.MstVOCOAViewmodel
import kotlinx.android.synthetic.main.activity_funds_with_bank.*
import kotlinx.android.synthetic.main.amountdialog.view.*
import kotlinx.android.synthetic.main.buttons_vo.*
import kotlinx.android.synthetic.main.buttons_vo.view.*
import kotlinx.android.synthetic.main.fund_bank_dialog.view.*
import kotlinx.android.synthetic.main.votablayout.*
import kotlinx.android.synthetic.main.white_toolbar.*

class FundsWithBankActivity : AppCompatActivity() {

    var shgBankList: List<Cbo_bankEntity>? = null
    var cboBankViewmodel: CboBankViewmodel? = null
    var executiveviewmodel: ExecutiveMemberViewmodel? = null
    var dataspin_bank: List<BankEntity>? = null
    var bankMasterViewModel: MasterBankViewmodel? = null
    var masterbankBranchViewmodel: MasterBankBranchViewModel? = null
    var lookupViewmodel: LookupViewmodel? = null
    var fedrationViewModel: FedrationViewModel? = null
    var federationViewmodel: FedrationViewModel? = null
    var mstVOCOAViewmodel: MstVOCOAViewmodel? = null
    var masterBankBranchViewModel: MasterBankBranchViewModel? = null
    var validate: Validate? = null
    var cboType = 0
    var imageUploadViewmodel: ImageUploadViewmodel? = null
    var shgViewmodel: SHGViewmodel? = null
    var addressViewmodel: CboAddressViewmodel? = null
    var phoneViewmodel: CboPhoneViewmodel? = null
    var systemtagViewmodel: SystemtagViewmodel? = null
    var vocoaMappingViewModel: VoCoaMappingViewmodel? = null
    var vocoaMappingEntity: VoCoaMappingEntity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_funds_with_bank)

        validate = Validate(this)


        if (validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType) == 2) {
            cboType = 2
        } else {
            cboType = 1
        }

        mstVOCOAViewmodel = ViewModelProviders.of(this).get(MstVOCOAViewmodel::class.java)

        cboBankViewmodel = ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        bankMasterViewModel = ViewModelProviders.of(this).get(MasterBankViewmodel::class.java)
        masterBankBranchViewModel =
            ViewModelProviders.of(this).get(MasterBankBranchViewModel::class.java)
        federationViewmodel = ViewModelProviders.of(this).get(FedrationViewModel::class.java)
        executiveviewmodel = ViewModelProviders.of(this).get(ExecutiveMemberViewmodel::class.java)
        vocoaMappingViewModel = ViewModelProviders.of(this).get(VoCoaMappingViewmodel::class.java)

        ivBack.setOnClickListener {
            var intent = Intent(this, VoBankListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
            //    overridePendingTransition(0, 0)
        }

        ivHome.visibility = View.GONE
        tv_title.text = LabelSet.getText("all_fund_linkage", R.string.all_fund_linkage)

        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary1))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvEc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvVector.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary1))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_Ec.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this, R.color.white))

        var mapped_shg_count =
            federationViewmodel!!.getMappedShgCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var mapped_vo_count =
            federationViewmodel!!.getMappedVoCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var basicComplete =
            federationViewmodel!!.getIsCompleteValue(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var addressIsComplete =
            federationViewmodel!!.getAddressCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var phoneIsComplete =
            federationViewmodel!!.getPhoneCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var bankIsComplete =
            federationViewmodel!!.getBankCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var ecIsComplete =
            federationViewmodel!!.getEcMemberCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var scIsComplete =
            federationViewmodel!!.getScCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var kycIsComplete =
            federationViewmodel!!.getKycCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))




        if (basicComplete > 0) {
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }

        if (ecIsComplete > 0) {
            IvEc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            IvEc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if (phoneIsComplete > 0) {
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if (addressIsComplete > 0) {
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if (kycIsComplete > 0) {
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }

        if (scIsComplete > 0) {
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }

        ivBack.setOnClickListener {
            var intent = Intent(this, VOListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }
        lay_systemTag.isEnabled = ecIsComplete > 0
        lay_systemTag.setOnClickListener {
            var intent = Intent(this, VoSubCommiteeList::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_mapcbo.setOnClickListener {
            if (cboType == 1) {
                var intent = Intent(this, VOMapCBOActivity::class.java)
                intent.flags =
                    Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            } else if (cboType == 2) {
                var intent = Intent(this, CLFMapCBOActivity::class.java)
                intent.flags =
                    Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }
        lay_vector.setOnClickListener {
            var intent = Intent(this, VoBasicDetailActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        lay_phone.isEnabled = ecIsComplete > 0
        lay_phone.setOnClickListener {
            var intent = Intent(this, VoPhoneDetailListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_location.setOnClickListener {
            var intent = Intent(this, VoAddressList::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_kyc.setOnClickListener {
            var intent = Intent(this, VoKycDetailList::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_Ec.setOnClickListener {
            var intent = Intent(this, VoEcListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        btn_cancel.setOnClickListener {
            var intent = Intent(this, VoBankListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        btn_save.setOnClickListener {

        }

        fillRecyclerView()

    }


    private fun fillRecyclerView() {


        var list=vocoaMappingViewModel!!.getcboData( validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id))
        if(list.isNullOrEmpty()){
            insertcoadata()
        }

        vocoaMappingViewModel!!.getcboDatalist(validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id)
        ).observe(this, object : Observer<List<VoCoaMappingEntity>> {
            override fun onChanged(coaData: List<VoCoaMappingEntity>) {
                if (!coaData.isNullOrEmpty()) {
                    rvVoFundList.apply {
                        layoutManager = LinearLayoutManager(this@FundsWithBankActivity)
                        addItemDecoration(DividerItemDecoration(this@FundsWithBankActivity, LinearLayoutManager.VERTICAL))
                        adapter = VOFundsBankListAdapter(this@FundsWithBankActivity, coaData)

                    }
                }
            }

        })

    }
    fun insertcoadata() {
      var coaData=  mstVOCOAViewmodel!!.getAlllist(listOf("RG", "RL", "RO","PG", "PL", "PO"), validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!)

            for (i in coaData!!.indices){
            vocoaMappingEntity = VoCoaMappingEntity(
                validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id),
                coaData.get(i).uid,
                returndefaaultaccount()
            )
            vocoaMappingViewModel!!.insert(vocoaMappingEntity!!)
        }
    }


    fun returndefaaultaccount(): String {
        var accountNo = ""
        val bankList = cboBankViewmodel!!.getcboBankdataModel(
            validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID),
            cboType
        )
        if (!bankList.isNullOrEmpty()) {
            for (i in bankList.indices) {
                if (bankList.get(i).is_default == 1) {
                    accountNo = bankList.get(i).ifsc_code!!.dropLast(7)+ bankList.get(i).account_no!!
                    break
                }
            }
        }
        return accountNo
    }

    fun CustomAlertBankDialog(uid: Int) {

        val mDialogView = LayoutInflater.from(this).inflate(R.layout.fund_bank_dialog, null)
        val mBuilder = androidx.appcompat.app.AlertDialog.Builder(this).setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.setCancelable(false)
        mAlertDialog.setCanceledOnTouchOutside(false)
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        mDialogView.btn_save.setOnClickListener {
            if (mDialogView.spinBank.selectedItemPosition > 0) {
                SaveBankFund(mDialogView, uid)
                mAlertDialog.dismiss()
            } else {
                validate!!.CustomAlertSpinner(
                    this, mDialogView.spinBank,
                    LabelSet.getText(
                        "please_select",
                        R.string.please_select
                    ) + " " + LabelSet.getText(
                        "bank",
                        R.string.bank
                    )
                )
            }

        }

        mDialogView.btn_cancel.setOnClickListener {
            mAlertDialog.dismiss()
        }

        fillbank(mDialogView)
        showDialogData(mDialogView, uid)
    }

    private fun SaveBankFund(mDialogView: View, uid: Int) {
        vocoaMappingEntity = VoCoaMappingEntity(
            validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id),
            uid,
            returaccount(mDialogView)
        )
        vocoaMappingViewModel!!.insert(vocoaMappingEntity!!)
        validate!!.CustomAlertVO(
            LabelSet.getText(
                "data_saved_successfully",
                R.string.data_saved_successfully
            ),
            this
        )
    }

    private fun showDialogData(mDialogView: View, uid: Int) {
        val listData = vocoaMappingViewModel!!.getCoaMappinData(
            uid,
            validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id)
        )

        if (!listData.isNullOrEmpty()) {
            setaccount(listData[0].bankcode!!, mDialogView)
        }

    }

    fun fillbank(mDialogView: View) {
        shgBankList =
            cboBankViewmodel!!.getBankdata1(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        val adapter: ArrayAdapter<String?>
        if (!shgBankList.isNullOrEmpty()) {
            val isize = shgBankList!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in shgBankList!!.indices) {
                var lastthree =
                    shgBankList!![i].account_no.substring(shgBankList!![i].account_no.length - 3)
                sValue[i + 1] =
                    shgBankList!![i].ifsc_code.toString().dropLast(7) + "XXXXX" + lastthree
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            mDialogView.spinBank.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            mDialogView.spinBank.adapter = adapter
        }

    }

    fun returaccount(mDialogView: View): String {

        var pos = mDialogView.spinBank.selectedItemPosition
        var id = ""

        if (!shgBankList.isNullOrEmpty()) {
            if (pos > 0) id =
                shgBankList!!.get(pos - 1).ifsc_code!!.dropLast(7) + shgBankList!!.get(pos - 1).account_no
        }
        return id
    }

    fun setaccount(accountno: String, mDialogView: View): Int {
        var pos = 0
        if (!shgBankList.isNullOrEmpty()) {
            for (i in shgBankList!!.indices) {
                if (accountno.equals(
                        shgBankList!!.get(i).ifsc_code!!.dropLast(7) + shgBankList!!.get(
                            i
                        ).account_no
                    )
                )
                    pos = i + 1
            }
        }
        mDialogView.spinBank.setSelection(pos)
        return pos
    }

    fun getBankCode(uid: Int): String? {
        var value: String? = null
        value = vocoaMappingViewModel!!.getBankCode(uid,validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id))
        return value
    }

    fun getbankpos(bankcode: String): Int {
        var pos = 99
        val bankList = cboBankViewmodel!!.getBankdata1(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        if (!bankList.isNullOrEmpty()) {
            for (i in bankList.indices) {
                if (bankcode.equals(bankList.get(i).ifsc_code!!.dropLast(7)+ bankList.get(i).account_no))
                    pos = i
            }
        }
        return pos
    }

    override fun onBackPressed() {
        var intent = Intent(this, VoBankListActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    fun getname(uid: Int): String {

        return mstVOCOAViewmodel!!.getCoaSubHeadname(
            uid,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
        )
    }

}