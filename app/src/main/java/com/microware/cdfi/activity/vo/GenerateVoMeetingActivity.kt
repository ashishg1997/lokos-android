package com.microware.cdfi.activity.vo
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.microware.cdfi.R
import com.microware.cdfi.utility.LabelSet
import kotlinx.android.synthetic.main.activity_generatevomeeting.*
import kotlinx.android.synthetic.main.buttons_vomeeting.btn_cancelmeeting
import kotlinx.android.synthetic.main.buttons_vomeeting.btn_generate_open
import kotlinx.android.synthetic.main.repay_toolbar_vomeeting.*

class GenerateVoMeetingActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_generatevomeeting)

        btn_generate_open.setOnClickListener {
            var intent = Intent(this, VoMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        btn_cancelmeeting.setOnClickListener {
            var intent = Intent(this, VoMeetingListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        ic_Back.setOnClickListener {
            var intent = Intent(this, VoMeetingListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        setLabelText()
    }

    override fun onBackPressed() {
        var intent = Intent(this, VoMeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0,0)
        finish()
    }

    fun setLabelText() {
        tv_title.text = LabelSet.getText(
            "generate_meeting",
            R.string.generate_meeting
        )
        tv_cut_off_meeting.text = LabelSet.getText(
            "cut_off_meeting",
            R.string.cut_off_meeting
        )
        tv_new_meeting_no.text = LabelSet.getText(
            "new_meeting_no",
            R.string.new_meeting_no
        )
        tv_Date.text = LabelSet.getText("date", R.string.date)
//        tv_total_meeting_held.setText(LabelSet.getText("total_meetings_held",R.string.total_meetings_held))
        et_new_meeting_no.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_Date.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )
//        et_total_meetings_held.setHint(LabelSet.getText("type_here",R.string.type_here))
        btn_generate_open.text = LabelSet.getText(
            "generate_open",
            R.string.generate_open
        )
        btn_cancelmeeting.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
         }

}