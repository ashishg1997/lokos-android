package com.microware.cdfi.activity.vo

import android.Manifest
import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModelProviders
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.google.gson.JsonObject
import com.microware.cdfi.BuildConfig
import com.microware.cdfi.R
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.*
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_member_bank_detail.*
import kotlinx.android.synthetic.main.activity_vobank_detail.*
import kotlinx.android.synthetic.main.activity_vobank_detail.ImgFrntpage
import kotlinx.android.synthetic.main.activity_vobank_detail.Imgsearch
import kotlinx.android.synthetic.main.activity_vobank_detail.Imgshow
import kotlinx.android.synthetic.main.activity_vobank_detail.btn_add
import kotlinx.android.synthetic.main.activity_vobank_detail.btn_addgray
import kotlinx.android.synthetic.main.activity_vobank_detail.et_Accountno
import kotlinx.android.synthetic.main.activity_vobank_detail.et_ifsc
import kotlinx.android.synthetic.main.activity_vobank_detail.et_nameinbankpassbook
import kotlinx.android.synthetic.main.activity_vobank_detail.et_opdate
import kotlinx.android.synthetic.main.activity_vobank_detail.rgisDefault
import kotlinx.android.synthetic.main.activity_vobank_detail.spin_bankname
import kotlinx.android.synthetic.main.activity_vobank_detail.spin_branch
import kotlinx.android.synthetic.main.activity_vobank_detail.tblFront
import kotlinx.android.synthetic.main.activity_vobank_detail.tvBankName
import kotlinx.android.synthetic.main.activity_vobank_detail.tvUploadFiles
import kotlinx.android.synthetic.main.customealertdialogelogin.*
import kotlinx.android.synthetic.main.customealertdialogelogin.view.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.votablayout.*
import kotlinx.android.synthetic.main.white_toolbar.*
import okhttp3.ResponseBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*
import java.text.SimpleDateFormat
import java.util.*

class VOBankDetailActivity : AppCompatActivity() {

    var validate: Validate? = null
    var cboBankEntity: Cbo_bankEntity? = null
    var cboBankViewmodel: CboBankViewmodel? = null
    var executiveviewmodel: ExecutiveMemberViewmodel? = null
    var dataspin_bank: List<BankEntity>? = null
    var dataspin_bankBranch: List<Bank_branchEntity>? = null
    var bankMasterViewModel: MasterBankViewmodel? = null
    var masterbankBranchViewmodel: MasterBankBranchViewModel? = null
    var lookupViewmodel: LookupViewmodel? = null
    var fedrationViewModel: FedrationViewModel? = null
    var dataspin_name: List<LookupEntity>? = null
    var dataspin_branch: List<Bank_branchEntity>? = null
    var dataspin_yesno: List<LookupEntity>? = null

    var sBankCode = 0
    var branch_id = 0
    var cboType = 0
    var bankGUID = " "
    var imageName = ""
    var bitmap: Bitmap? = null

    private var fileUri: Uri? = null
    var imgPathUpload = ""
    var mediaFile: File? = null

    var shgcode = ""
    var shgName = ""
    var newimageName = false
    var imageUploadViewmodel: ImageUploadViewmodel? = null
    var shgViewmodel: SHGViewmodel? = null
    var addressViewmodel: CboAddressViewmodel? = null
    var phoneViewmodel: CboPhoneViewmodel? = null
    var bank: CboBankViewmodel? = null
    var systemtagViewmodel: SystemtagViewmodel? = null
    var apiInterface: ApiInterface? = null
    internal lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vobank_detail)

        validate = Validate(this)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()

        cboBankViewmodel = ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        bankMasterViewModel = ViewModelProviders.of(this).get(MasterBankViewmodel::class.java)
        masterbankBranchViewmodel =
            ViewModelProviders.of(this).get(MasterBankBranchViewModel::class.java)
        fedrationViewModel = ViewModelProviders.of(this).get(FedrationViewModel::class.java)
        executiveviewmodel = ViewModelProviders.of(this).get(ExecutiveMemberViewmodel::class.java)

        imageUploadViewmodel = ViewModelProviders.of(this).get(ImageUploadViewmodel::class.java)
        shgViewmodel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        addressViewmodel = ViewModelProviders.of(this).get(CboAddressViewmodel::class.java)
        phoneViewmodel = ViewModelProviders.of(this).get(CboPhoneViewmodel::class.java)
        bank = ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        systemtagViewmodel = ViewModelProviders.of(this).get(SystemtagViewmodel::class.java)

        ivBack.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) == 1) {
                if (checkValidation() == 1) {
                    SaveBankDetail(1, 1)

                    var intent = Intent(this, VoBankListActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    finish()
                    //   overridePendingTransition(0, 0)
                }
            } else {
                var intent = Intent(this, VoBankListActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                finish()
                //    overridePendingTransition(0, 0)
            }
        }

        if (validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType) == 2) {
            cboType = 2

        } else {
            cboType = 1

        }

        ivHome.visibility = View.GONE
        tv_title.text = LabelSet.getText(
            "bankdetails",
            R.string.bankdetails
        )
        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary1))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvEc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvVector.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary1))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_Ec.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this, R.color.white))

        var mapped_shg_count =
            fedrationViewModel!!.getMappedShgCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var basicComplete =
            fedrationViewModel!!.getIsCompleteValue(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var addressIsComplete =
            fedrationViewModel!!.getAddressCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var phoneIsComplete =
            fedrationViewModel!!.getPhoneCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var bankIsComplete =
            fedrationViewModel!!.getBankCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var ecIsComplete =
            fedrationViewModel!!.getEcMemberCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var scIsComplete =
            fedrationViewModel!!.getScCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var kycIsComplete =
            fedrationViewModel!!.getKycCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))

        if (basicComplete > 0) {
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }

        if (ecIsComplete > 0) {
            IvEc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            IvEc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if (phoneIsComplete > 0) {
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if (addressIsComplete > 0) {
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }

        if (kycIsComplete > 0) {
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }

        if (scIsComplete > 0) {
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        lay_systemTag.isEnabled = ecIsComplete > 0
        lay_systemTag.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation() == 1) {
                    var isdefault = bank!!.getBankdatalistdefualtcount(
                        validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)
                    )
                    if (isdefault == 0) {
                        SaveBankDetail(1, 1)
                    } else {
                        CustomAlertisdefault(1)
                    }

                    var intent = Intent(this, VoSubCommiteeList::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            } else {
                var intent = Intent(this, VoSubCommiteeList::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_mapcbo.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation() == 1) {
                    var isdefault = bank!!.getBankdatalistdefualtcount(
                        validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)
                    )
                    if (isdefault == 0) {
                        SaveBankDetail(1, 1)
                    } else {
                        CustomAlertisdefault(1)
                    }

                    if (cboType == 1) {
                        var intent = Intent(this, VOMapCBOActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    } else if (cboType == 2) {
                        var intent = Intent(this, CLFMapCBOActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    }
                }
            } else {
                if (cboType == 1) {
                    var intent = Intent(this, VOMapCBOActivity::class.java)
                    intent.flags =
                        Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                } else if (cboType == 2) {
                    var intent = Intent(this, CLFMapCBOActivity::class.java)
                    intent.flags =
                        Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }
        }
        lay_vector.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation() == 1) {
                    var isdefault = bank!!.getBankdatalistdefualtcount(
                        validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)
                    )
                    if (isdefault == 0) {
                        SaveBankDetail(1, 1)
                    } else {
                        CustomAlertisdefault(1)
                    }

                    var intent = Intent(this, VoBasicDetailActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            } else {
                var intent = Intent(this, VoBasicDetailActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }


        lay_Ec.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation() == 1) {
                    var isdefault = bank!!.getBankdatalistdefualtcount(
                        validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)
                    )
                    if (isdefault == 0) {
                        SaveBankDetail(1, 1)
                    } else {
                        CustomAlertisdefault(1)
                    }

                    var intent = Intent(this, VoEcListActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            } else {
                var intent = Intent(this, VoEcListActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_phone.isEnabled = ecIsComplete > 0


        lay_phone.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation() == 1) {
                    var isdefault = bank!!.getBankdatalistdefualtcount(
                        validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)
                    )
                    if (isdefault == 0) {
                        SaveBankDetail(1, 1)
                    } else {
                        CustomAlertisdefault(1)
                    }

                    var intent = Intent(this, VoPhoneDetailListActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            } else {
                var intent = Intent(this, VoPhoneDetailListActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_location.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation() == 1) {
                    var isdefault = bank!!.getBankdatalistdefualtcount(
                        validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)
                    )
                    if (isdefault == 0) {
                        SaveBankDetail(1, 1)
                    } else {
                        CustomAlertisdefault(1)
                    }

                    var intent = Intent(this, VoAddressList::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            } else {
                var intent = Intent(this, VoAddressList::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_kyc.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation() == 1) {
                    var isdefault = bank!!.getBankdatalistdefualtcount(
                        validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)
                    )
                    if (isdefault == 0) {
                        SaveBankDetail(1, 1)
                    } else {
                        CustomAlertisdefault(1)
                    }

                    var intent = Intent(this, VoKycDetailList::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            } else {
                var intent = Intent(this, VoKycDetailList::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        et_opdate.setOnClickListener {
            validate!!.datePickerwithmindate(
                validate!!.RetriveSharepreferenceLong(AppSP.Formation_dt),
                et_opdate
            )
        }

        spin_bankname?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                if (position > 0) {
                    sBankCode = validate!!.returnMasterBankID(spin_bankname, dataspin_bank)
                    bindBankBranchSpinner(sBankCode)
                } else {
                    bindBankBranchSpinner(0)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }
        spin_branch?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                if (position > 0) {
                    var branchifsc =
                        validate!!.returnMasterBankBranchifsc(spin_branch, dataspin_bankBranch)
                    et_ifsc.setText(branchifsc)

                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        Imgsearch.setOnClickListener {

            spin_bankname.setSelection(0)
            branch_id = 0
            // spin_branch.setSelection(0)
            if (et_ifsc.text.toString().toUpperCase().trim().length == 11) {
                dataspin_branch =
                    masterbankBranchViewmodel!!.BankBranchlistifsc(
                        et_ifsc.text.toString().toUpperCase()
                    )

                if (!dataspin_branch.isNullOrEmpty()) {
                    branch_id = dataspin_branch!!.get(0).bank_branch_id
                    spin_bankname.setSelection(
                        validate!!.returnMasterBankpos(
                            validate!!.returnIntegerValue(dataspin_branch!!.get(0).bank_id.toString()),
                            dataspin_bank
                        )
                    )
                    bindBankBranchSpinner(validate!!.returnIntegerValue(dataspin_branch!!.get(0).bank_id.toString()))

                } else if (et_ifsc.text.toString().toUpperCase()
                        .trim().length == 11 && isNetworkConnected()
                ) {
//                spin_branch.setSelection(0)
//                spin_bankname.setSelection(0)
                    importBankList(et_ifsc.text.toString().toUpperCase())

                } else {
                    validate!!.CustomAlertEditText(
                        LabelSet.getText(
                            "no_internet_msg",
                            R.string.no_internet_msg
                        ),
                        this,
                        et_ifsc
                    )
                }

            } else {
                validate!!.CustomAlertEditText(
                    LabelSet.getText(
                        "pleaseenetervalidifsc",
                        R.string.pleaseenetervalidifsc
                    ),
                    this,
                    et_ifsc
                )
            }
        }
        ImgFrntpage.setOnClickListener {
            if (!checkPermission()) {
                requestPermission(101)
            } else {
                captureimage(101)
            }
        }
        tblFront.setOnClickListener {
            if (imageName.isNotEmpty()) {
                ShowImage(imageName)
            } else {
                if (!checkPermission()) {
                    requestPermission(101)
                } else {
                    captureimage(101)
                }

            }

        }

        if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) == 1) {
            ivLock.visibility = View.VISIBLE
            btn_addgray.visibility = View.VISIBLE
            btn_add.visibility = View.GONE

        } else {
            ivLock.visibility = View.GONE
            btn_addgray.visibility = View.GONE
            btn_add.visibility = View.VISIBLE
        }

        btn_add.setOnClickListener {
            if (!validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID).isNullOrEmpty()) {
                if (checkValidation() == 1) {
                    var isdefault = bank!!.getBankdatalistdefualtcount(
                        validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)
                    )
                    if (isdefault == 0) {
                        SaveBankDetail(0, 1)
                    } else {
                        CustomAlertisdefault(0)
                    }
                }
            } else {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "insert_federation_data_first",
                        R.string.insert_federation_data_first
                    ), this,
                    VoBasicDetailActivity::class.java
                )
            }
        }

        spin_bankname.setTitle(
            LabelSet.getText(
                "selectbankname",
                R.string.selectbankname
            )
        )
        spin_branch.setTitle(
            LabelSet.getText(
                "selectbranchname",
                R.string.selectbranchname
            )
        )
        fillSpinner()
        fillRadio()
        showData()
        setLabelText()

    }

    private fun fillRadio() {
        dataspin_yesno = lookupViewmodel!!.getlookup(
            9,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        validate!!.fillradio(rgisDefault, 0, dataspin_yesno, this)
    }

    private fun bindBankBranchSpinner(sBankCode: Int) {
        dataspin_bankBranch = masterbankBranchViewmodel!!.getBankMaster(sBankCode)
        validate!!.fillMasterBankBranchspinner(this, spin_branch, dataspin_bankBranch, branch_id)
        if (branch_id > 0) {
            spin_branch.setSelection(
                validate!!.returnMasterBankBranchpos(
                    branch_id,
                    dataspin_bankBranch
                )
            )
        }
    }

    private fun fillSpinner() {
        dataspin_bank = bankMasterViewModel!!.getBankMaster()
        validate!!.fillMasterBankspinner(this, spin_bankname, dataspin_bank)
    }

    private fun SaveBankDetail(iAutoSave: Int, isdefault: Int) {
        if (checkData() == 0) {
            var isEdited = setFederationEditFlag()
            bankGUID = validate!!.random()
            var bankID = validate!!.returnMasterBankID(spin_bankname, dataspin_bank)
            var branchID = validate!!.returnMasterBankBranchID(spin_branch, dataspin_bankBranch)

            val mediaStorageDirectory = File(
                getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
                AppSP.IMAGE_DIRECTORY_NAME
            )
            if (validate!!.RetriveSharepreferenceString(AppSP.VOBankGUID).isNullOrEmpty()) {
                if (imageName.length > 0) {
                    var file = File(mediaStorageDirectory.path + File.separator + imageName)
                    var newFile =
                        File(mediaStorageDirectory.path + File.separator + bankGUID + ",301.jpg")
                    if (file.renameTo(newFile)) {

                        println("File move success")
                    } else {
                        println("File move failed")
                    }
                    imageName = bankGUID + ",301.jpg"
                }
                cboBankEntity = Cbo_bankEntity(
                    0,
                    0,
                    et_Accountno.text.toString(),
                    validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!,
                    bankGUID,
                    bankID,
                    validate!!.Daybetweentime(et_opdate.text.toString()),
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    "",
                    branchID.toString(),
                    et_ifsc.text.toString(),
                    isdefault,
                    0,
                    0,
                    0,
                    cboType, 1,
                    1,
                    1,
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    validate!!.RetriveSharepreferenceString(AppSP.userid),
                    0,
                    "",
                    0,
                    "",
                    et_nameinbankpassbook.text.toString(),
                    imageName, branchID, 0, 1, 1, 1, 1
                )
                cboBankViewmodel!!.insert(cboBankEntity!!)

                var kycimage1 = ImageuploadEntity(
                    imageName,
                    validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID),
                    "",
                    1,
                    0
                )
                if (imageName.isNotEmpty() && newimageName) {
                    imageUploadViewmodel!!.insert(kycimage1)
                }
                validate!!.SaveSharepreferenceString(AppSP.VOBankGUID, bankGUID)
                fedrationViewModel!!.updatededup(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!)
                validate!!.updateFederationEditFlag(
                    cboType,
                    validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!,
                    validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id),
                    fedrationViewModel,
                    executiveviewmodel
                )

                if (iAutoSave == 0) {
                    validate!!.CustomAlertVO(
                        LabelSet.getText(
                            "data_saved_successfully",
                            R.string.data_saved_successfully
                        ), this,
                        VoBankListActivity::class.java
                    )
                }
            } else {
                if (imageName.length > 0) {
                    var file = File(mediaStorageDirectory.path + File.separator + imageName)
                    var newFile = File(
                        mediaStorageDirectory.path + File.separator + validate!!.RetriveSharepreferenceString(
                            AppSP.VOBankGUID
                        )!! + ",301.jpg"
                    )
                    if (file.renameTo(newFile)) {

                        println("File move success")
                    } else {
                        println("File move failed")
                    }
                    imageName =
                        validate!!.RetriveSharepreferenceString(AppSP.VOBankGUID)!! + ",301.jpg"
                }
                cboBankViewmodel!!.updateBankDetail(
                    validate!!.RetriveSharepreferenceString(AppSP.VOBankGUID)!!,
                    bankID,
                    validate!!.Daybetweentime(et_opdate.text.toString()),
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    et_Accountno.text.toString(),
                    branchID,
                    et_ifsc.text.toString(),
                    isdefault,
                    0,
                    0,
                    0,
                    1,
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                    imageName,
                    1,
                    validate!!.returnStringValue(et_nameinbankpassbook.text.toString().trim())
                )

                var kycimage1 = ImageuploadEntity(
                    imageName,
                    validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID),
                    "",
                    1,
                    0
                )
                if (imageName.isNotEmpty() && newimageName) {
                    imageUploadViewmodel!!.insert(kycimage1)
                }
                fedrationViewModel!!.updatededup(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!)
                validate!!.updateFederationEditFlag(
                    cboType,
                    validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!,
                    validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id),
                    fedrationViewModel,
                    executiveviewmodel
                )

                if (iAutoSave == 0) {
                    validate!!.CustomAlertVO(
                        LabelSet.getText(
                            "updated_successfully",
                            R.string.updated_successfully
                        ),
                        this,
                        VoBankListActivity::class.java
                    )
                }
            }
        } else {
            if (iAutoSave == 0) {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "updated_successfully",
                        R.string.updated_successfully
                    ),
                    this,
                    VoBankListActivity::class.java
                )
            }
        }
    }

    fun checkValidation(): Int {
        var value = 1
        var isValidAccount = 0
        var bankID = validate!!.returnMasterBankID(spin_bankname, dataspin_bank)
        var branchID = validate!!.returnMasterBankBranchID(spin_branch, dataspin_bankBranch)
        var acc_count = cboBankViewmodel!!.getbank_acc_count(
            validate!!.returnStringValue(et_Accountno.text.toString()), cboType
        )
        var accountLengthAllowed = bankMasterViewModel!!.getAccountLength(bankID)
        var arr = validate!!.returnStringValue(accountLengthAllowed.toString()).split(",")
        var account_length = et_Accountno.text.toString().trim().length
        if (!arr.isNullOrEmpty()) {
            for (i in 0 until arr.size) {
                if (account_length == validate!!.returnIntegerValue(arr[i])) {
                    isValidAccount = 1
                    break
                } else {
                    isValidAccount = 0
                }
            }
        } else {
            isValidAccount = 0
        }


        if (et_nameinbankpassbook.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "nameinbankpassbook",
                    R.string.nameinbankpassbook
                ),
                this,
                et_nameinbankpassbook
            )
            value = 0
            return value
        } else if (!validate!!.checkname(et_nameinbankpassbook.text.toString())) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "nameinbankpassbook",
                    R.string.nameinbankpassbook
                ),
                this,
                et_nameinbankpassbook
            )
            value = 0
            return value
        } else if (et_ifsc.text.toString().length == 0 || et_ifsc.text.toString().length != 11) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "pleaseenetervalidifsc",
                    R.string.pleaseenetervalidifsc
                ),
                this,
                et_ifsc
            )
            value = 0
            return value
        } else if (spin_bankname.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_bankname,
                LabelSet.getText("selectbank", R.string.selectbank)
            )
            value = 0
            return value
        } else if (spin_branch.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_branch,
                LabelSet.getText(
                    "selectbranch",
                    R.string.selectbranch
                )
            )
            value = 0
            return value
        } else if ((validate!!.returnStringValue(accountLengthAllowed)
                .trim().length > 0 && isValidAccount == 0) || (validate!!.returnStringValue(
                accountLengthAllowed
            ).trim().length == 0 && et_Accountno.text.toString().length < 4)
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "valid_account_no",
                    R.string.valid_account_no
                ),
                this,
                et_Accountno
            )
            value = 0
            return value
        } else if (et_opdate.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "account_opening_date",
                    R.string.account_opening_date
                ),
                this,
                et_opdate
            )
            value = 0
            return value
        } else if (tvUploadFiles.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "capture_image",
                    R.string.capture_image
                ),
                this,
                tvUploadFiles
            )
            value = 0
            return value
        } else if ((validate!!.RetriveSharepreferenceString(AppSP.VOBankGUID)!!
                .trim().length == 0 && acc_count > 0) ||
            (validate!!.RetriveSharepreferenceString(AppSP.VOBankGUID)!!
                .trim().length > 0 && acc_count > 1)
        ) {
            validate!!.CustomAlertVO(
                LabelSet.getText(
                    "acc_no_exists",
                    R.string.acc_no_exists
                ), this
            )
            value = 0
            return value
        }

        return value

    }

    private fun showData() {
        var list =
            cboBankViewmodel!!.getBankdetaildata(validate!!.RetriveSharepreferenceString(AppSP.VOBankGUID))
        if (!list.isNullOrEmpty() && list.size > 0) {
            et_nameinbankpassbook.setText(validate!!.returnStringValue(list.get(0).bankpassbook_name))
            et_ifsc.setText(validate!!.returnStringValue(list.get(0).ifsc_code))
            et_Accountno.setText(validate!!.returnStringValue(list.get(0).account_no))
            et_opdate.setText(validate!!.convertDatetime(validate!!.returnLongValue(list.get(0).account_opening_date.toString())))
            spin_bankname.setSelection(
                validate!!.returnMasterBankpos(
                    validate!!.returnIntegerValue(list.get(0).bank_id.toString()),
                    dataspin_bank
                )
            )
            tvUploadFiles.text = validate!!.returnStringValue(list.get(0).passbook_firstpage)
            branch_id = validate!!.returnIntegerValue(list.get(0).bank_branch_id.toString())

            validate!!.fillradio(
                rgisDefault,
                validate!!.returnIntegerValue(list.get(0).is_default.toString()),
                dataspin_yesno,
                this
            )

            val mediaStorageDirectory = File(
                getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
                AppSP.IMAGE_DIRECTORY_NAME
            )
            var mediaFile1 =
                File(
                    mediaStorageDirectory.path + File.separator + validate!!.returnStringValue(
                        list.get(0).passbook_firstpage
                    )
                )
            Picasso.with(this).load(mediaFile1).into(Imgshow)
            imageName = validate!!.returnStringValue(list.get(0).passbook_firstpage)

        } else {
            et_nameinbankpassbook.setText(validate!!.RetriveSharepreferenceString(AppSP.FedrationName))
        }
    }

    override fun onBackPressed() {
        var intent = Intent(this, VoBankListActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }

    private fun checkPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        val result1 =
            ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val result2 =
            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission(requestCode: Int) {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ),
            requestCode
        )
    }

    fun captureimage(code: Int) {
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
                m.invoke(null)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//        fileUri = getOutputMediaFileUri(MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE, code)
        fileUri = FileProvider.getUriForFile(
            this, BuildConfig.APPLICATION_ID + ".provider", getOutputMediaFile(
                MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE, code
            )!!
        )
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
        startActivityForResult(intent, code)

    }

    fun getOutputMediaFileUri(type: Int, flag: Int): Uri {
        return Uri.fromFile(getOutputMediaFile(type, flag))
    }

    private fun getOutputMediaFile(type: Int, flag: Int): File? {

        // External sdcard location
        val mediaStorageDir = File(
            getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
            AppSP.IMAGE_DIRECTORY_NAME
        )
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null
            }
        }

        // Create a media file name
        val timeStamp = SimpleDateFormat(
            "yyyyMMdd_HHmmss",
            Locale.ENGLISH
        ).format(Date())
        imageName = "IMG_$timeStamp.jpg"
        if (type == MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE) {
            mediaFile = File(
                mediaStorageDir.path + File.separator
                        + imageName
            )
            imgPathUpload = mediaStorageDir.path + File.separator + "IMG_" + timeStamp + ".jpg"


        } else {
            return null
        }

        return mediaFile
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            compreesedimage()
            newimageName = true
            bitmap = BitmapFactory.decodeFile(fileUri!!.path)
            if (requestCode == 101 && resultCode == Activity.RESULT_OK) {
                //  ImgFrntpage.setImageBitmap(bitmap)
                Imgshow.setImageBitmap(bitmap)
                tvUploadFiles.text = imageName
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun compreesedimage() {
        val options = BitmapFactory.Options()
        var bitmap = BitmapFactory.decodeFile(fileUri!!.path, options)

        if (bitmap != null) {
            var actualWidth = options.outWidth
            var actualHeight = options.outHeight
            var mutableBitmap =
                Bitmap.createScaledBitmap(bitmap, actualWidth / 2, actualHeight / 2, false)

            var fOut: OutputStream? = null
            var file: File = File(fileUri!!.path)
            try {
                fOut = FileOutputStream(file)
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }
            mutableBitmap.compress(Bitmap.CompressFormat.JPEG, 50, fOut)
            try {
                fOut!!.flush()
                fOut.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
    }

    private fun ShowImage(name: String?) {
        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val layout: View = inflater.inflate(R.layout.popupwindow, null, false)
        val window = PopupWindow(
            layout,
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT,
            true
        )
        window.showAtLocation(layout, Gravity.CENTER, 0, 0)
        val cancel: ImageView = layout.findViewById(R.id.cancel)
        val image_preview: ImageView = layout.findViewById(R.id.image_preview)
        val mediaStorageDirectory = File(
            getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
            AppSP.IMAGE_DIRECTORY_NAME
        )
        var mediaFile1 = File(mediaStorageDirectory.path + File.separator + name)
        cancel.setOnClickListener {
            window.dismiss()
        }

        if (mediaFile1.exists()) {
            Picasso.with(this).load(mediaFile1).into(image_preview)
        } else {
            window.dismiss()
        }

    }

    private fun setLabelText() {
        tv_BankPassbook.text = LabelSet.getText(
            "nameinbankpassbook",
            R.string.nameinbankpassbook
        )
        et_nameinbankpassbook.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tvIFSC.text = LabelSet.getText(
            "ifsc_code",
            R.string.ifsc_code
        )
        et_ifsc.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tvBankName.text = LabelSet.getText(
            "bank_name",
            R.string.bank_name
        )
        tv_bankBranch.text = LabelSet.getText(
            "bank_branch",
            R.string.bank_branch
        )
        tv_accountNo.text = LabelSet.getText(
            "account_no",
            R.string.account_no
        )
        et_Accountno.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_opDate.text = LabelSet.getText(
            "account_opening_date",
            R.string.account_opening_date
        )
        et_opdate.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )
        tvPaasbookPage.text = LabelSet.getText(
            "upload_passbook_first_page",
            R.string.upload_passbook_first_page
        )
        tv_isDefault.text = LabelSet.getText(
            "is_default",
            R.string.is_default
        )
        btn_add.text = LabelSet.getText(
            "add_bank",
            R.string.add_bank
        )
        btn_addgray.text = LabelSet.getText(
            "add_bank",
            R.string.add_bank
        )
    }

    fun importBankList(ifscCode: String) {
        var progressDialog: ProgressDialog = ProgressDialog.show(
            this, LabelSet.getText("app_name", R.string.app_name),
            LabelSet.getText(
                "fetchingbankdata",
                R.string.fetchingbankdata
            )
        )
        progressDialog.setIcon(this.getDrawable(R.drawable.appicon))


        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
        val token = validate!!.returnStringValue(
            AESEncryption.decrypt(
                validate!!.RetriveSharepreferenceString(AppSP.token),
                validate!!.RetriveSharepreferenceString(AppSP.userid)
            )
        )

        val call1 = apiInterface?.getBankList(
            token,
            user, ifscCode
        )

        call1?.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(callCount: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
                Toast.makeText(this@VOBankDetailActivity, t.toString(), Toast.LENGTH_LONG).show()
            }

            override fun onResponse(
                call: Call<ResponseBody>,
                response: Response<ResponseBody>
            ) {
                progressDialog.dismiss()
                if (response.isSuccessful) {
                    var alldata = response.body()!!.source().readUtf8().toString()
                    val objectMapper = ObjectMapper()

                    val langList: List<Bank_branchEntity> =
                        objectMapper.readValue(
                            alldata,
                            object : TypeReference<List<Bank_branchEntity>>() {})
                    if (langList != null) {
                        if (langList.size == 0) {
                            validate!!.CustomAlertVO(
                                LabelSet.getText(
                                    "branchnotavialable",
                                    R.string.branchnotavialable
                                ), this@VOBankDetailActivity
                            )
                        } else {
                            CDFIApplication.database?.masterbankbranchDao()
                                ?.insertbranch(
                                    langList
                                )
                            validate!!.CustomAlertVO(
                                LabelSet.getText(
                                    "branchdownlodedsuccessfully",
                                    R.string.branchdownlodedsuccessfully
                                ), this@VOBankDetailActivity
                            )

                        }
                    }



                    dataspin_branch =
                        masterbankBranchViewmodel!!.BankBranchlistifsc(
                            et_ifsc.text.toString().toUpperCase()
                        )
                    if (!dataspin_branch.isNullOrEmpty()) {
                        branch_id = dataspin_branch!!.get(0).bank_branch_id
                        spin_bankname.setSelection(
                            validate!!.returnMasterBankpos(
                                validate!!.returnIntegerValue(dataspin_branch!!.get(0).bank_id.toString()),
                                dataspin_bank
                            )
                        )
                    }


                } else {

                    when (response.code()) {

                        403 ->
                            CustomAlertlogin()
                        else -> {
                            validate!!.CustomAlertVO(
                                LabelSet.getText(
                                    "branchnotavialable",
                                    R.string.branchnotavialable
                                ), this@VOBankDetailActivity
                            )
                            Toast.makeText(
                                this@VOBankDetailActivity,
                                response.message(),
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }

                }
            }
        })


    }


    fun importtoken(user: String, pass: String) {
        var grant_type = "password"
        var userId = user
        var password = pass
        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText(
                "authenticate_user",
                R.string.authenticate_user
            )
        )


        val callCount = apiInterface?.gettokenforce(
            grant_type,
            userId,
            password, validate!!.RetriveSharepreferenceInt(AppSP.State_Selected), 1,
            validate!!.RetriveSharepreferenceString(AppSP.Roleid)!!
        )
        callCount?.enqueue(object : Callback<JsonObject> {
            override fun onFailure(callCount: Call<JsonObject>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                progressDialog.dismiss()


                if (response.isSuccessful) {

                    var jsonObject1: JSONObject? = null
                    try {
                        jsonObject1 = JSONObject(response.body().toString())

                        val auth = jsonObject1.get("accessToken").toString()
                        if (auth.trim().length > 0) {
                            validate!!.SaveSharepreferenceString(
                                AppSP.token,
                                validate!!.returnStringValue(
                                    AESEncryption.encrypt(
                                        auth, validate!!.RetriveSharepreferenceString(AppSP.userid)
                                    )
                                )
                            )
                        }
                        validate!!.CustomAlertVO(
                            LabelSet.getText(
                                "userauthenticatedsuccessfully",
                                R.string.userauthenticatedsuccessfully
                            ), this@VOBankDetailActivity
                        )
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Toast.makeText(
                            this@VOBankDetailActivity,
                            response.message(),
                            Toast.LENGTH_LONG
                        ).show()


                    }

                } else {
                    Toast.makeText(this@VOBankDetailActivity, response.message(), Toast.LENGTH_LONG)
                        .show()
                }


            }

        })


    }

    fun CustomAlertlogin() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogelogin, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mDialogView.etUsername.setText(validate!!.RetriveSharepreferenceString(AppSP.userid))
        mDialogView.etPassword.setText(
            validate!!.returnStringValue(
                AESEncryption.decrypt(
                    validate!!.RetriveSharepreferenceString(AppSP.Password),
                    validate!!.RetriveSharepreferenceString(AppSP.userid)
                )
            )
        )
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.btn_login.setBackgroundColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.txt_dialog_title2.text = LabelSet.getText("app_name", R.string.app_name)
        mDialogView.btn_login.text = LabelSet.getText("log_in", R.string.log_in)
        mDialogView.chk_showHide1.text = LabelSet.getText("show_password", R.string.show_password)
        mDialogView.btn_login.setOnClickListener {
            mAlertDialog.dismiss()
            importtoken(
                mDialogView.etUsername.text.toString(),
                validate!!.returnStringValue(
                    AESEncryption.encrypt(
                        validate!!.returnStringValue(
                            mDialogView.etPassword.text.toString()
                        ), mDialogView.etUsername.text.toString()
                    )
                )
            )
        }
        mAlertDialog.chk_showHide1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                mAlertDialog.etPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                mAlertDialog.etPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
        }


    }

    private fun isNetworkConnected(): Boolean {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager //1
        val networkInfo = connectivityManager.activeNetworkInfo //2
        return networkInfo != null && networkInfo.isConnected //3
    }

    fun CustomAlertisdefault(iAutoSave: Int) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_dialog_title.setTextColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.btn_yes.setBackgroundColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.btn_no.setBackgroundColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.txt_msg.text = LabelSet.getText(
            "doyouwanttomakedeualt",
            R.string.doyouwanttomakedeualt
        )
        mDialogView.btn_yes.setOnClickListener {
            CDFIApplication.database?.cboBankDao()
                ?.updateisdefault(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!)
            SaveBankDetail(iAutoSave, 1)
            mAlertDialog.dismiss()


        }
        mDialogView.btn_no.setOnClickListener {
            SaveBankDetail(iAutoSave, 0)
            mAlertDialog.dismiss()

        }
    }

    fun setFederationEditFlag(): Int {
        var is_Edited = 0
        var bank_Fi = 0
        var financialIntermidiation =
            fedrationViewModel!!.getFI(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var bankCount =
            fedrationViewModel!!.getBankCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))

        if ((financialIntermidiation == 1 && bankCount > 0) || financialIntermidiation == 0) {
            bank_Fi = 1
        } else {
            bank_Fi = 0
        }
        var officeBearerCount = executiveviewmodel!!.getOBCount(
            listOf(1, 3, 5),
            validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!
        )
        var cboCount =
            executiveviewmodel!!.getDistinctCboCount(validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id))
        var ecCount =
            fedrationViewModel!!.getEcMemberCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var mapped_shg_count =
            fedrationViewModel!!.getMappedShgCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var mapped_vo_count =
            fedrationViewModel!!.getMappedVoCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        if (cboType == 1 && ecCount > 4 && mapped_shg_count > 4 && mapped_shg_count == cboCount && officeBearerCount == 3 && bank_Fi == 1) {
            is_Edited = 1
        } else if (cboType == 2 && ecCount > 4 && mapped_vo_count > 4 && mapped_vo_count == cboCount && officeBearerCount == 3 && bank_Fi == 1) {
            is_Edited = 1
        } else {
            is_Edited = -1
        }
        return is_Edited
    }

    private fun checkData(): Int {
        var value = 1
        var bankID = validate!!.returnMasterBankID(spin_bankname, dataspin_bank)
        var branchID = validate!!.returnMasterBankBranchID(spin_branch, dataspin_bankBranch)
        if(!validate!!.RetriveSharepreferenceString(AppSP.VOBankGUID).isNullOrEmpty()){
            var list =
                cboBankViewmodel!!.getBankdetaildata(validate!!.RetriveSharepreferenceString(AppSP.VOBankGUID))


            if (et_nameinbankpassbook.text.toString() != validate!!.returnStringValue(list?.get(0)?.bankpassbook_name)) {
                value = 0
                return value
            } else if (et_ifsc.text.toString() != validate!!.returnStringValue(list?.get(0)?.ifsc_code)) {
                value = 0
                return value
            } else if (et_Accountno.text.toString() != validate!!.returnStringValue(list?.get(0)?.account_no)) {
                value = 0
                return value
            } else if (bankID != validate!!.returnIntegerValue(list?.get(0)?.bank_id.toString())) {
                value = 0
                return value
            }  else if (branchID != validate!!.returnIntegerValue(list?.get(0)?.bank_branch_id.toString())) {
                value = 0
                return value
            } else if (validate!!.Daybetweentime(et_opdate.text.toString()) != validate!!.returnLongValue(
                    list?.get(0)?.account_opening_date.toString()
                )
            ) {
                value = 0
                return value
            } else if (rgisDefault.checkedRadioButtonId != validate!!.returnIntegerValue(list?.get(0)?.is_default.toString())) {
                value = 0
                return value
            }else if(imageName != validate!!.returnStringValue(list?.get(0)?.passbook_firstpage)){
                value = 0
                return value
            }
        }else {
            value = 0
            return value
        }
        return value
    }
}
