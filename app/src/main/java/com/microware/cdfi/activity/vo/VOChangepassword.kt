package com.microware.cdfi.activity.vo

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.microware.cdfi.R
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.api.model.ChangePasswordModel
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.ResponseViewModel
import kotlinx.android.synthetic.main.activity_vochangepassword.*
import kotlinx.android.synthetic.main.vosummary_toolbar.*
import okhttp3.MediaType
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class VOChangepassword : AppCompatActivity() {
    var apiInterface: ApiInterface? = null
    var validate: Validate? = null
    var responseViewModel: ResponseViewModel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vochangepassword)

        validate = Validate(this)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        responseViewModel = ViewModelProviders.of(this).get(ResponseViewModel::class.java)

        icBack.setOnClickListener {
            var intent = Intent(this, VoDrawerActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }
        btn_login.setOnClickListener {
            if (validate!!.isNetworkConnected(this)) {
                if (checkValidation() == 1) {
                    changepassword()
                }
            } else {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "no_internet_msg",
                        R.string.no_internet_msg
                    ), this
                )
            }
        }

//        etPassword.setText("Microware@123")
//        etnewPassword.setText("Micro@123")
//        etconfirmPassword.setText("Micro@123")
    }

    fun checkValidation(): Int {
        var value = 1

        if (etPassword.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "password",
                    R.string.password
                ),
                this,
                etPassword
            )
            value = 0
            return value
        } else if (etnewPassword.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "new_password",
                    R.string.new_password
                ),
                this,
                etnewPassword
            )
            value = 0
            return value
        } else if (etconfirmPassword.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "confirm_password",
                    R.string.confirm_password
                ),
                this,
                etconfirmPassword
            )
            value = 0
            return value
        } else if (!etnewPassword.text.toString().equals(etconfirmPassword.text.toString())) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "pleaseenetersamepassword",
                    R.string.pleaseenetersamepassword
                ),
                this,
                etconfirmPassword
            )
            value = 0
            return value
        }

        return value

    }

    fun setLabelText() {

        tvchangepassword.text = LabelSet.getText(
            "change_password",
            R.string.change_password
        )
        btn_login.text = LabelSet.getText(
            "change_password",
            R.string.change_password
        )
        etPassword.hint = LabelSet.getText(
            "password",
            R.string.password
        )
        etnewPassword.hint = LabelSet.getText(
            "new_password",
            R.string.new_password
        )
        etconfirmPassword.hint = LabelSet.getText(
            "confirm_password",
            R.string.confirm_password
        )
    }

    override fun onBackPressed() {
        var intent = Intent(this, VoDrawerActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        finish()
    }

    fun changepassword() {
        var progressDialog: ProgressDialog = ProgressDialog.show(
            this, LabelSet.getText("app_name", R.string.app_name),
            LabelSet.getText(
                "changingpassword",
                R.string.changingpassword
            )
        )
        progressDialog.setIcon(this.getDrawable(R.drawable.appicon))
        var changepasswordmodel = ChangePasswordModel()

        changepasswordmodel.userId = validate!!.returnStringValue(validate!!.RetriveSharepreferenceString(AppSP.userid))
        changepasswordmodel.oldPassword=validate!!.returnStringValue(AESEncryption.encrypt(
            validate!!.returnStringValue(etPassword.text.toString()),validate!!.RetriveSharepreferenceString(AppSP.userid)))
        changepasswordmodel.newPassword=validate!!.returnStringValue(AESEncryption.encrypt(
                validate!!.returnStringValue(etnewPassword.text.toString()),validate!!.RetriveSharepreferenceString(AppSP.userid)))
        changepasswordmodel.confirmPassword=validate!!.returnStringValue(AESEncryption.encrypt(
                    validate!!.returnStringValue(etconfirmPassword.text.toString()),validate!!.RetriveSharepreferenceString(AppSP.userid)))
        val gson = Gson()
        val json = gson.toJson(changepasswordmodel)
        val sData = RequestBody.create(
            MediaType.parse("application/json; charset=utf-8"),json)
//        val sData1=RequestBody.create(
//            "application/json; charset=utf-8".toMediaTypeOrNull(),
//            data)
        val call1 = apiInterface?.changepassword("application/json",
            validate!!.returnStringValue(
                AESEncryption.decrypt(
                validate!!.RetriveSharepreferenceString(AppSP.token),
                validate!!.RetriveSharepreferenceString(AppSP.userid)
            )),
            validate!!.RetriveSharepreferenceString(AppSP.userid),sData)

        call1?.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(callCount: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
                Toast.makeText(this@VOChangepassword, t.toString(), Toast.LENGTH_LONG).show()
            }

            override fun onResponse(
                call: Call<ResponseBody>,
                response: Response<ResponseBody>
            ) {
                progressDialog.dismiss()
                if (response.isSuccessful) {
                    validate!!.CustomAlertVO(
                        LabelSet.getText(
                            "passwordchanged",
                            R.string.passwordchanged
                        ), this@VOChangepassword, VoDrawerActivity::class.java
                    )

                } else {
                    var jsonObject1 = JSONObject(response.errorBody()!!.source().readUtf8().toString())

                    val auth = validate!!.returnIntegerValue(jsonObject1.get("responseCode").toString())
                    validate!!.CustomAlertMsgVO(
                        this@VOChangepassword,responseViewModel ,auth,
                        validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,validate!!.returnStringValue(jsonObject1.get("responseMsg").toString())
                    )
                }
            }
        })


    }
}