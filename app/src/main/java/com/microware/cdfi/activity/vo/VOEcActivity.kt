package com.microware.cdfi.activity.vo

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.RadioGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.microware.cdfi.R
import com.microware.cdfi.activity.BasicDetailActivity
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.*
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_v_o_ec.*
import kotlinx.android.synthetic.main.customealertdialogelogin.*
import kotlinx.android.synthetic.main.customealertdialogelogin.view.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.votablayout.*
import kotlinx.android.synthetic.main.votablayout.lay_bank
import kotlinx.android.synthetic.main.votablayout.lay_phone
import kotlinx.android.synthetic.main.white_toolbar.*
import okhttp3.MediaType
import okhttp3.RequestBody
import org.apache.http.conn.ConnectTimeoutException
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException

class VOEcActivity : AppCompatActivity() {
    var validate: Validate? = null
    var executiveEntity: Executive_memberEntity? = null
    var lookupviewmodel:LookupViewmodel? = null
    var executiveviewmodel:ExecutiveMemberViewmodel? = null
    var mappedShgViewmodel:MappedShgViewmodel? = null
    var mappedVoViewmodel:MappedVoViewmodel? = null
    var voShgMemberViewmodel:VOShgMemberViewmodel? = null
    var clfVoMemberViewmodel:ClfVoMemberViewmodel? = null
    var federationViewmodel:FedrationViewModel? = null
    var datastatus:List<LookupEntity>? = null
    var dataspindesignation:List<LookupEntity>? = null
    var datayes:List<LookupEntity>? = null
    var dataspin_member: List<VoShgMemberEntity>? = null
    var dataspin_Vomember: List<ClfVoMemberEntity>? = null
    var dataspinShg: List<MappedShgEntity>? = null
    var dataspinVo: List<MappedVoEntity>? = null
    var apiInterface: ApiInterface? = null
    var responseViewModel: ResponseViewModel? = null
    var iButtonPressed = 0
    var isUpdate = 0
    var selectedSignatoryId = 0
    var inactiveDate:Long? = 0
    var ecGuid = ""

    internal lateinit var progressDialog: ProgressDialog

    var cboType=0
    var ecMemberCode:Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_v_o_ec)
        animationView.visibility = View.VISIBLE
        ivHome.visibility = View.GONE
        tv_title.text = getString(R.string.commite_memebr)
        validate = Validate(this)
        lookupviewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        mappedShgViewmodel = ViewModelProviders.of(this).get(MappedShgViewmodel::class.java)
        mappedVoViewmodel = ViewModelProviders.of(this).get(MappedVoViewmodel::class.java)
        voShgMemberViewmodel = ViewModelProviders.of(this).get(VOShgMemberViewmodel::class.java)
        clfVoMemberViewmodel = ViewModelProviders.of(this).get(ClfVoMemberViewmodel::class.java)
        executiveviewmodel = ViewModelProviders.of(this).get(ExecutiveMemberViewmodel::class.java)
        federationViewmodel = ViewModelProviders.of(this).get(FedrationViewModel::class.java)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        responseViewModel = ViewModelProviders.of(this).get(ResponseViewModel::class.java)

        if(validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType)==2){
            cboType = 2
        }else {
            cboType = 1
        }

        btn_refresh.setOnClickListener {
            validate!!.SaveSharepreferenceInt(AppSP.mappingRefreshed, 1)
            if(cboType == 1){
                importVO_SHG_Mapping()
            }else if(cboType == 2){
                importCLF_VO_Mapping()
            }
        }

        ivBack.setOnClickListener {

                var intent = Intent(this, VoEcListActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                finish()

        }

        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvEc.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary1))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvVector.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_Ec.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary1))

        var basicComplete = federationViewmodel!!.getIsCompleteValue(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var addressIsComplete = federationViewmodel!!.getAddressCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var phoneIsComplete = federationViewmodel!!.getPhoneCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var bankIsComplete = federationViewmodel!!.getBankCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var ecIsComplete = federationViewmodel!!.getEcMemberCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var scIsComplete = federationViewmodel!!.getScCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var kycIsComplete = federationViewmodel!!.getKycCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))

        if(basicComplete > 0){
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }

        if(phoneIsComplete > 0){
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(addressIsComplete > 0){
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(bankIsComplete > 0){
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(kycIsComplete > 0){
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(scIsComplete > 0){
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }

        lay_mapcbo.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if(validate!!.RetriveSharepreferenceInt(AppSP.mappingRefreshed) == 1) {
                    if (sCheckValidation() == 1) {
                        saveEcMember(1)

                        if (cboType == 1) {
                            var intent = Intent(this, VOMapCBOActivity::class.java)
                            intent.flags =
                                Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                            overridePendingTransition(0, 0)
                        } else if (cboType == 2) {
                            var intent = Intent(this, CLFMapCBOActivity::class.java)
                            intent.flags =
                                Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                            overridePendingTransition(0, 0)
                        }
                    }
                }else {
                    if (cboType == 1) {
                        var intent = Intent(this, VOMapCBOActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    } else if (cboType == 2) {
                        var intent = Intent(this, CLFMapCBOActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    }
                }
            }else {
                if (cboType == 1) {
                    var intent = Intent(this, VOMapCBOActivity::class.java)
                    intent.flags =
                        Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }else if(cboType == 2){
                    var intent = Intent(this, CLFMapCBOActivity::class.java)
                    intent.flags =
                        Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }
        }

        lay_systemTag.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if(validate!!.RetriveSharepreferenceInt(AppSP.mappingRefreshed) == 1) {
                    if (sCheckValidation() == 1) {
                        saveEcMember(1)

                        var intent = Intent(this, VoSubCommiteeList::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    }
                }else {
                    var intent = Intent(this, VoSubCommiteeList::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoSubCommiteeList::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_phone.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if(validate!!.RetriveSharepreferenceInt(AppSP.mappingRefreshed) == 1) {
                    if (sCheckValidation() == 1) {
                        saveEcMember(1)

                        var intent = Intent(this, VoPhoneDetailListActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    }
                }else {
                    var intent = Intent(this, VoPhoneDetailListActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoPhoneDetailListActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_location.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if(validate!!.RetriveSharepreferenceInt(AppSP.mappingRefreshed) == 1){
                    if (sCheckValidation() == 1) {
                        saveEcMember(1)

                        var intent = Intent(this, VoAddressList::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    }
                }else {
                    var intent = Intent(this, VoAddressList::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoAddressList::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_bank.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if(validate!!.RetriveSharepreferenceInt(AppSP.mappingRefreshed) == 1){
                    if (sCheckValidation() == 1) {
                        saveEcMember(1)

                        var intent = Intent(this, VoBankListActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    }
                }else {
                    var intent = Intent(this, VoBankListActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoBankListActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_vector.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if(validate!!.RetriveSharepreferenceInt(AppSP.mappingRefreshed) == 1) {
                    if (sCheckValidation() == 1) {
                        saveEcMember(1)

                        var intent = Intent(this, VoBasicDetailActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    }
                }else {
                    var intent = Intent(this, VoBasicDetailActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoBasicDetailActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_kyc.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if(validate!!.RetriveSharepreferenceInt(AppSP.mappingRefreshed) == 1){
                    if (sCheckValidation() == 1) {
                        saveEcMember(1)

                        var intent = Intent(this, VoKycDetailList::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    }
                }else {
                    var intent = Intent(this, VoKycDetailList::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoKycDetailList::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        if(validate!!.RetriveSharepreferenceString(AppSP.ECMemGuid)!!.trim().length==0 && validate!!.RetriveSharepreferenceLong(AppSP.EcCboCode)==0L) {
            if (cboType == 1) {
                validate!!.SaveSharepreferenceInt(AppSP.mappingRefreshed, 1)
                importVO_SHG_Mapping()
            } else if (cboType == 2) {
                validate!!.SaveSharepreferenceInt(AppSP.mappingRefreshed, 1)
                importCLF_VO_Mapping()
            }
        }

        if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) == 1) {
            ivLock.visibility = View.VISIBLE
            btn_savegray.visibility = View.VISIBLE
            tbl_save_refresh.visibility = View.GONE

        } else {
            ivLock.visibility = View.GONE
            btn_savegray.visibility = View.GONE
            tbl_save_refresh.visibility = View.VISIBLE
        }

        btn_save.setOnClickListener {
            if (!validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID).isNullOrEmpty()) {
                if(validate!!.RetriveSharepreferenceInt(AppSP.mappingRefreshed) == 1) {
                    if (sCheckValidation() == 1) {
                        saveEcMember(0)
                    }
                }else {
                    validate!!.CustomAlertVO(LabelSet.getText(
                        "refresh_mapping_data",
                        R.string.refresh_mapping_data
                    ),this)
                }
            } else {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "insert_federation_data_first",
                        R.string.insert_federation_data_first
                    ),
                    this,
                    BasicDetailActivity::class.java
                )
            }
        }

        et_valid_From.setOnClickListener {
            validate!!.datePickerwithmindate(
                validate!!.Daybetweentime(et_formDate.text.toString()),
                et_valid_From
            )
        }

        et_valid_till.setOnClickListener {
            validate!!.datePickerwithmindate(
                validate!!.Daybetweentime(et_valid_From.text.toString()),
                et_valid_From
            )
        }
        et_formDate.setOnClickListener {
            var member_code = 0L
            if(cboType == 1){
                member_code = returnmember_code()
            }else if(cboType == 2){
                member_code = returnClfmember_code()
            }
            var joiningdate = executiveviewmodel!!.getMemberJoiningDate(member_code)
            if(joiningdate < validate!!.RetriveSharepreferenceLong(AppSP.Formation_dt)){
                validate!!.datePickerwithmindate(
                    validate!!.RetriveSharepreferenceLong(joiningdate.toString()),
                    et_formDate
                )
            }else {
                validate!!.datePickerwithmindate(
                    validate!!.RetriveSharepreferenceLong(AppSP.Formation_dt),
                    et_formDate
                )
            }

        }
        et_toDate.setOnClickListener {
            validate!!.datePickerwithmindate(
                validate!!.Daybetweentime(et_formDate.text.toString()),
                et_toDate
            )
        }

        fillSpinner()
        showdata()

        setLabelText()
    }

    private fun fillSpinner() {
        if(cboType == 1){
            dataspinShg = mappedShgViewmodel!!.getShgNameList(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!)
            validate!!.fillShgspinner(this,spin_cboName,dataspinShg)
        }else if(cboType == 2){
            dataspinVo = mappedVoViewmodel!!.getVoNameList(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!)
            validate!!.fillVospinner(this,spin_cboName,dataspinVo)
        }
        datayes = lookupviewmodel!!.getlookup(9,validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
        dataspindesignation = lookupviewmodel!!.getlookup(49,validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
        datastatus = lookupviewmodel!!.getlookup(5,validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))


        validate!!.fillspinner(this,spin_status,datastatus)
        validate!!.fillspinner(this,spin_ecdesignation,dataspindesignation)
        validate!!.fillradio(rgsignatory, 0, datayes, this)

        spin_cboName?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (cboType == 1) {
                    if (position > 0) {
                        var cboMemberCount = executiveviewmodel!!.getCboMemberCount(returnshg_code())
                        if ((validate!!.RetriveSharepreferenceString(AppSP.ECMemGuid).isNullOrEmpty() && cboMemberCount <5) ||
                            (validate!!.RetriveSharepreferenceString(AppSP.ECMemGuid)!!.trim().length>0 && cboMemberCount <6)) {
                            dataspin_member =
                                voShgMemberViewmodel!!.getMemberList(returnshg_guid())
                            validate!!.fill_Ec_memberspinner(
                                this@VOEcActivity,
                                spin_ecmembername,
                                dataspin_member
                            )
                            var mempos = setMember(ecMemberCode, dataspin_member)
                            spin_ecmembername.setSelection(mempos)
                        } else {
                            validate!!.CustomAlertSpinner(
                                this@VOEcActivity,
                                spin_cboName,
                                LabelSet.getText(
                                    "cbo_max5_member",
                                    R.string.cbo_max5_member
                                )
                            )
                        }
                    } else {
                        dataspin_member = voShgMemberViewmodel!!.getMemberList("")
                        validate!!.fill_Ec_memberspinner(
                            this@VOEcActivity,
                            spin_ecmembername,
                            dataspin_member
                        )

                    }
                }else if (cboType == 2) {
                    if (position > 0) {
                        var cboMemberCount = executiveviewmodel!!.getCboMemberCount(returnVo_code())
                        if ((validate!!.RetriveSharepreferenceString(AppSP.ECMemGuid).isNullOrEmpty() && cboMemberCount <5) ||
                            (validate!!.RetriveSharepreferenceString(AppSP.ECMemGuid)!!.trim().length>0 && cboMemberCount <6)) {
                            dataspin_Vomember =
                                clfVoMemberViewmodel!!.getMemberList(returnVo_guid())
                            validate!!.fill_ClfEc_memberspinner(
                                this@VOEcActivity,
                                spin_ecmembername,
                                dataspin_Vomember
                            )
                            var mempos = setClfMember(ecMemberCode, dataspin_Vomember)
                            spin_ecmembername.setSelection(mempos)
                        } else {
                            validate!!.CustomAlertSpinner(
                                this@VOEcActivity,
                                spin_cboName,
                                LabelSet.getText(
                                    "cbo_max5_member",
                                    R.string.cbo_max5_member
                                )
                            )
                        }
                    } else {
                        dataspin_member = voShgMemberViewmodel!!.getMemberList("")
                        validate!!.fill_Ec_memberspinner(
                            this@VOEcActivity,
                            spin_ecmembername,
                            dataspin_member
                        )

                    }
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        rgsignatory.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->

            var isSignatory = rgsignatory.checkedRadioButtonId
            if (isSignatory == 1) {
                lay_valid_till.visibility = View.GONE
                lay_validform.visibility=View.VISIBLE
                et_valid_From.isEnabled = true
                et_valid_till.setText("")

            } else if (isSignatory == 0 && et_valid_From.text.trim().toString().length>0) {
                lay_valid_till.visibility = View.VISIBLE
                lay_validform.visibility = View.VISIBLE
                et_valid_till.isEnabled = true
                et_valid_From.isEnabled = false

            }else {
                lay_valid_till.visibility = View.GONE
                lay_validform.visibility = View.GONE
            }

        })
        spin_status?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if(position>0){
                    var status = validate!!.returnlookupcode(spin_status,datastatus)
                    if(status == 2){
                        var ecScCount = executiveviewmodel!!.getScMember_count(validate!!.RetriveSharepreferenceString(AppSP.ECMemGuid)!!,ecMemberCode)
                        if(ecScCount==0) {
                            if(linear_signatoryControls.visibility == View.VISIBLE && rgsignatory.checkedRadioButtonId==1){
                                validate!!.fillradio(rgsignatory, 0, datayes, this@VOEcActivity)
                            }
                            et_toDate.isEnabled = true
                            if(inactiveDate==0L){
                                et_toDate.setText(
                                    validate!!.convertDatetime(
                                        validate!!.Daybetweentime(
                                            validate!!.currentdatetime
                                        )
                                    )
                                )
                            }else {
                                et_toDate.setText(
                                    validate!!.convertDatetime(inactiveDate)
                                )
                            }
                        }else {
                            validate!!.CustomAlertVO(LabelSet.getText(
                                "ec_not_deleted",
                                R.string.ec_not_deleted
                            ),this@VOEcActivity)
                        }
                    }else {
                        et_toDate.isEnabled = false
                        et_toDate.setText("")
                    }
                }else {
                    et_toDate.setText("")
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        spin_ecdesignation?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                var selectedDesignation = validate!!.returnlookupcode(spin_ecdesignation,dataspindesignation)
                if(selectedDesignation == 1 || selectedDesignation == 3 || selectedDesignation == 5) {
                    linear_signatoryControls.visibility = View.VISIBLE
                }else {
                    linear_signatoryControls.visibility = View.GONE
                    validate!!.fillradio(rgsignatory,0,datayes,this@VOEcActivity)
                }


            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }
    }

    override fun onBackPressed() {
        var intent = Intent(this, VoEcListActivity::class.java)
        intent.flags= Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    private fun saveEcMember(iAutoSave:Int) {
        if (isNetworkConnected()) {
            var signatory_joining_date:Long? = null
            var signatory_leaving_date:Long? = null
            var isSignatory:Int? = null
            if(linear_signatoryControls.visibility == View.VISIBLE){
                isSignatory = rgsignatory.checkedRadioButtonId
            }
            if(lay_validform.visibility == View.VISIBLE){
                signatory_joining_date = validate!!.Daybetweentime(et_valid_From.text.toString())
            }

            if(lay_valid_till.visibility == View.VISIBLE){
                signatory_leaving_date = validate!!.Daybetweentime(et_valid_till.text.toString())
            }
            var status = 1
            if (spin_status.selectedItemPosition > 0) {
                status = validate!!.returnlookupcode(spin_status, datastatus)
            }
            var cbo_code = 0L
            var ec_cbo_level = 0
            var ec_member_code = 0L
            if (cboType == 1) {
                cbo_code = returnshg_code()
            } else if (cboType == 2) {
                cbo_code = returnVo_code()
            }

            if (cboType == 1) {
                ec_member_code = returnmember_code()
                ec_cbo_level = 0
            } else if (cboType == 2) {
                ec_member_code = returnClfmember_code()
                ec_cbo_level = 1
            }

            if (validate!!.RetriveSharepreferenceString(AppSP.ECMemGuid).isNullOrEmpty()) {
                isUpdate = 0
                var guid = validate!!.random()
                executiveEntity = Executive_memberEntity(
                    validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id),
                    validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID),
                    guid,
                    cboType,
                    ec_cbo_level,
                    cbo_code,
                    cbo_code,
                    ec_member_code,
                    validate!!.returnlookupcode(spin_ecdesignation, dataspindesignation),
                    isSignatory,
                    validate!!.Daybetweentime(et_formDate.text.toString()),
                    0,signatory_joining_date,
                    signatory_leaving_date, 1, 1, 1, 1,
                    0, "",
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    validate!!.RetriveSharepreferenceString(AppSP.userid),
                    0,
                    "", 1
                )
                executiveviewmodel!!.insert(executiveEntity!!)
                validate!!.SaveSharepreferenceString(AppSP.ECMemGuid, guid)
                uploadECdata(iAutoSave, isUpdate)

            } else {
                isUpdate = 1
                executiveviewmodel!!.updateExecutiveMember(
                    validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id),
                    validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID),
                    validate!!.RetriveSharepreferenceString(AppSP.ECMemGuid)!!,
                    cbo_code,
                    cbo_code,
                    ec_member_code,
                    validate!!.returnlookupcode(spin_ecdesignation, dataspindesignation),
                    validate!!.Daybetweentime(et_formDate.text.toString()),
                    validate!!.Daybetweentime(et_toDate.text.toString()),
                    status, 1, 1, 1,
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    validate!!.RetriveSharepreferenceString(AppSP.userid), 1,
                    isSignatory,signatory_joining_date,signatory_leaving_date
                )

                uploadECdata(iAutoSave, isUpdate)
            }
        } else {
            validate!!.CustomAlertVO(
                LabelSet.getText(
                    "no_internet_msg",
                    R.string.no_internet_msg
                ), this
            )
        }
    }

    private fun showdata(){
        try{
            if(validate!!.returnStringValue(validate!!.RetriveSharepreferenceString(AppSP.ECMemGuid)).trim().length>0){
                val list = executiveviewmodel!!.getExecutivedetaildata(validate!!.RetriveSharepreferenceString(AppSP.ECMemGuid))
                if(!list.isNullOrEmpty()) {
                    if(cboType == 1) {
                        spin_cboName.setSelection(
                            setShgName(
                                validate!!.returnLongValue(list.get(0).ec_cbo_code.toString()),
                                dataspinShg
                            )
                        )
                    }else if(cboType == 2){
                        spin_cboName.setSelection(
                            setVoName(
                                validate!!.returnLongValue(list.get(0).ec_cbo_code.toString()),
                                dataspinVo
                            )
                        )
                    }
                    ecMemberCode = validate!!.returnLongValue(list.get(0).ec_member_code.toString())
                    spin_ecmembername.isEnabled = ecMemberCode <= 0
                    spin_ecdesignation.isEnabled =
                        validate!!.returnIntegerValue(list.get(0).designation.toString()) <= 0
                    spin_ecdesignation.setSelection(
                        validate!!.returnlookupcodepos(
                            validate!!.returnIntegerValue(list.get(0).designation.toString()),
                            dataspindesignation
                        )
                    )
                    selectedSignatoryId = validate!!.returnIntegerValue(list.get(0).is_signatory.toString())
                    validate!!.fillradio(
                        rgsignatory,
                        validate!!.returnIntegerValue(list.get(0).is_signatory.toString()),
                        datayes,
                        this
                    )
                    spin_status.setSelection(
                        validate!!.returnlookupcodepos(
                            validate!!.returnIntegerValue(list.get(0).status.toString()),
                            datastatus
                        )
                    )
                    if (validate!!.returnLongValue(list.get(0).joining_date.toString()) > 0) {
                        et_formDate.setText(validate!!.convertDatetime(list.get(0).joining_date!!))
                    }
                    if (validate!!.returnLongValue(list.get(0).leaving_date.toString()) > 0) {
                        inactiveDate = list.get(0).leaving_date!!
                        et_toDate.setText(validate!!.convertDatetime(list.get(0).leaving_date!!))
                    }

                    if (validate!!.returnLongValue(list.get(0).signatory_joining_date.toString()) > 0) {
                        et_valid_From.setText(validate!!.convertDatetime(list.get(0).signatory_joining_date!!))
                    }
                    if (validate!!.returnLongValue(list.get(0).signatory_leaving_date.toString()) > 0) {
                        et_valid_till.setText(validate!!.convertDatetime(list.get(0).signatory_leaving_date!!))
                    }
                }
            }else {
                if(validate!!.RetriveSharepreferenceLong(AppSP.EcCboCode)>0 && cboType==1){
                    spin_cboName.setSelection(setShgName(validate!!.RetriveSharepreferenceLong(AppSP.EcCboCode),dataspinShg))
                }else if(validate!!.RetriveSharepreferenceLong(AppSP.EcCboCode)>0 && cboType==2){
                    spin_cboName.setSelection(setVoName(validate!!.RetriveSharepreferenceLong(AppSP.EcCboCode),dataspinVo))
                }else {
                    spin_cboName.setSelection(0)
                }
                lay_validform.visibility=View.GONE
                spin_ecmembername.isEnabled = true
                spin_ecdesignation.isEnabled = true
                spin_ecdesignation.setSelection(validate!!.returnlookupcodepos(6,dataspindesignation))
                et_formDate.setText(validate!!.convertDatetime(validate!!.Daybetweentime(validate!!.currentdatetime)))
                lay_status.visibility = View.GONE
                lay_toDate.visibility = View.GONE
            }

        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    fun returnshg_guid(): String {

        var pos = spin_cboName.selectedItemPosition
        var id = ""

        if (!dataspinShg.isNullOrEmpty()) {
            if (pos > 0) id = dataspinShg!!.get(pos - 1).guid
        }
        return id
    }

    fun returnshg_code(): Long {

        var pos = spin_cboName.selectedItemPosition
        var id:Long = 0

        if (!dataspinShg.isNullOrEmpty()) {
            if (pos > 0) id = dataspinShg!!.get(pos-1).shg_id!!
        }
        return id
    }

    fun returnVo_guid(): String {

        var pos = spin_cboName.selectedItemPosition
        var id = ""

        if (!dataspinVo.isNullOrEmpty()) {
            if (pos > 0) id = dataspinVo!!.get(pos - 1).cbo_child_guid
        }
        return id
    }

    fun returnVo_code(): Long {

        var pos = spin_cboName.selectedItemPosition
        var id:Long = 0

        if (!dataspinVo.isNullOrEmpty()) {
            if (pos > 0) id = dataspinVo!!.get(pos-1).cbo_child_id!!
        }
        return id
    }

    fun setShgName(shgid:Long,data: List<MappedShgEntity>?): Int {


        var pos = 0

        if (!data.isNullOrEmpty()) {
            for (i in data.indices) {
                if (shgid.equals(data.get(i).shg_id))
                    pos = i + 1
            }
        }
        return pos
    }

    fun setVoName(vo_id:Long,data: List<MappedVoEntity>?): Int {


        var pos = 0

        if (!data.isNullOrEmpty()) {
            for (i in data.indices) {
                if (vo_id.equals(data.get(i).cbo_child_id))
                    pos = i + 1
            }
        }
        return pos
    }

    fun returnmember_code(): Long {

        var pos = spin_ecmembername.selectedItemPosition
        var id:Long = 0

        if (!dataspin_member.isNullOrEmpty()) {
            if (pos > 0) id = dataspin_member!!.get(pos-1).member_id!!
        }
        return id
    }

    fun returnClfmember_code(): Long {

        var pos = spin_ecmembername.selectedItemPosition
        var id:Long = 0

        if (!dataspin_Vomember.isNullOrEmpty()) {
            if (pos > 0) id = dataspin_Vomember!!.get(pos-1).member_id!!
        }
        return id
    }

    fun setMember(memberCode:Long,data: List<VoShgMemberEntity>?): Int {

        var pos = 0

        if (!data.isNullOrEmpty()) {
            for (i in data.indices) {
                if (memberCode == data.get(i).member_id)
                    pos = i + 1
            }
        }
        return pos
    }

    fun setClfMember(memberCode:Long,data: List<ClfVoMemberEntity>?): Int {

        var pos = 0

        if (!data.isNullOrEmpty()) {
            for (i in data.indices) {
                if (memberCode == data.get(i).member_id)
                    pos = i + 1
            }
        }
        return pos
    }

    private fun sCheckValidation():Int{
        var ec_member_code = 0L
        if(cboType == 1){
            ec_member_code = returnmember_code()
        }else if(cboType == 2){
            ec_member_code = returnClfmember_code()
        }
        var memberCount = executiveviewmodel!!.getECMember_count(validate!!.returnStringValue(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)),ec_member_code)
        var designation = validate!!.returnlookupcode(spin_ecdesignation,dataspindesignation)
        var designationCount = executiveviewmodel!!.getDesignationCount(designation,validate!!.returnStringValue(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)))

        var mem_count = executiveviewmodel!!.getECMember_count(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!,returnmember_code())
        var value = 1
        var designationMsg = ""
        if(cboType == 1){
            designationMsg = LabelSet.getText(
                "designation_vo",
                R.string.designation_vo
            )
        }else if(cboType == 2){
            designationMsg = LabelSet.getText(
                "designation_clf",
                R.string.designation_clf
            )
        }
        if (spin_cboName.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(this,spin_cboName,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                )+" " +LabelSet.getText(
                    "cbo_name",
                    R.string.cbo_name
                ))
            value = 0
            return value
        }else if (spin_ecmembername.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(this,spin_ecmembername,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                )+" " +LabelSet.getText(
                    "ec_member_name",
                    R.string.ec_member_name
                ))
            value = 0
            return value
        }else if((validate!!.RetriveSharepreferenceString(AppSP.ECMemGuid).isNullOrEmpty() && memberCount>0) || (validate!!.RetriveSharepreferenceString(AppSP.ECMemGuid)!!.trim().length>0 && memberCount>1)){
            validate!!.CustomAlertSpinner(this,spin_ecmembername,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                )+" " +LabelSet.getText(
                    "differ_ec",
                    R.string.differ_ec
                ))
            value = 0
            return value
        }else if (spin_ecdesignation.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(this,spin_ecdesignation,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                )+" " + designationMsg)
            value = 0
            return value
        }else if (et_formDate.text.toString().trim().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                )+" " +LabelSet.getText(
                    "from_date",
                    R.string.from_date
                ),
                this,et_formDate)
            value = 0
            return value
        }else if (validate!!.returnlookupcode(spin_status,datastatus)==2 && et_toDate.text.toString().trim().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                )+" " +LabelSet.getText(
                    "to_date",
                    R.string.to_date
                ),
                this,et_toDate)
            value = 0
            return value
        }else if((validate!!.returnStringValue(validate!!.RetriveSharepreferenceString(AppSP.ECMemGuid)).isNullOrEmpty() && mem_count>0) ||
            (validate!!.returnStringValue(validate!!.RetriveSharepreferenceString(AppSP.ECMemGuid)).trim().length>0 && mem_count>1)){
            validate!!.CustomAlertSpinner(this,spin_ecmembername
                ,LabelSet.getText(
                    "please_select",
                    R.string.please_select
                )+" " +LabelSet.getText(
                    "valid_ec_member",
                    R.string.valid_ec_member
                ))
            value = 0
            return value
        }else if(designation !=6 && ((validate!!.returnStringValue(validate!!.RetriveSharepreferenceString(AppSP.ECMemGuid)).isNullOrEmpty() && designationCount>0) ||
                    (validate!!.returnStringValue(validate!!.RetriveSharepreferenceString(AppSP.ECMemGuid)).trim().length>0 && designationCount>1))){
            validate!!.CustomAlertSpinner(this,spin_ecdesignation
                ,LabelSet.getText(
                    "please_select",
                    R.string.please_select
                )+" " +LabelSet.getText(
                    "differ_designation",
                    R.string.differ_designation
                ))
            value = 0
            return value
        }else if(validate!!.returnStringValue(validate!!.RetriveSharepreferenceString(AppSP.ECMemGuid)).trim().length>0 && ecMemberCode!=returnmember_code() && mem_count>0){
            validate!!.CustomAlertSpinner(this,spin_ecmembername
                ,LabelSet.getText(
                    "please_select",
                    R.string.please_select
                )+" " +LabelSet.getText(
                    "valid_ec_member",
                    R.string.valid_ec_member
                ))
            value = 0
            return value
        }else if(linear_signatoryControls.visibility==View.VISIBLE && lay_validform.visibility==View.VISIBLE && validate!!.returnStringValue(et_valid_From.text.toString()).trim().length==0){
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "signatory_join_date",
                    R.string.signatory_join_date
                ),
                this,et_valid_From)
            value = 0
            return value
        }else if(linear_signatoryControls.visibility==View.VISIBLE && lay_valid_till.visibility==View.VISIBLE && validate!!.returnStringValue(et_valid_till.text.toString()).trim().length==0){
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "signatory_leave_date",
                    R.string.signatory_leave_date
                ),
                this,et_valid_From)
            value = 0
            return value
        }
        return value
    }

    private fun setLabelText() {
        tvCBOName.text = LabelSet.getText(
            "cbo_name",
            R.string.cbo_name
        )
        tvEcMemberName.text = LabelSet.getText(
            "ec_member_name",
            R.string.ec_member_name
        )
        tv_signatory.text = LabelSet.getText(
            "signatory",
            R.string.signatory
        )
        if(cboType==1){
            tvDesignationFed.text = LabelSet.getText(
                "designation_vo",
                R.string.designation_vo
            )
        }else if(cboType==2){
            tvDesignationFed.text = LabelSet.getText(
                "designation_clf",
                R.string.designation_clf
            )
        }

        tvStatus.text = LabelSet.getText(
            "status",
            R.string.status)
        tvFormDate.text = LabelSet.getText(
            "from_date",
            R.string.from_date
        )
        et_formDate.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )
        tvToDate.text = LabelSet.getText(
            "to_date",
            R.string.to_date
        )
        et_toDate.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )
        btn_save.text = LabelSet.getText(
            "add_ec",
            R.string.add_ec
        )
        btn_savegray.text = LabelSet.getText(
            "add_ec",
            R.string.add_ec
        )
    }

    private fun isNetworkConnected(): Boolean {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager //1
        val networkInfo = connectivityManager.activeNetworkInfo //2
        return networkInfo != null && networkInfo.isConnected //3
    }


    fun importVO_SHG_Mapping() {
        var iEcDownload = 0
        if (isNetworkConnected()) {
            clearMappingData(1,validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!)
            progressDialog = ProgressDialog.show(
                this, LabelSet.getText(
                    "app_name",
                    R.string.app_name
                ),
                LabelSet.getText(
                    "DataLoading",
                    R.string.DataLoading
                )
            )
            progressDialog.setIcon(this.getDrawable(R.drawable.appicon))
            var idownload = 0
            var code = 0
            var msg = ""
            object : Thread() {

                //@SuppressLint("NewApi")
                override fun run() {
                    try {

                        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                        val token = validate!!.returnStringValue(
                            AESEncryption.decrypt(
                                validate!!.RetriveSharepreferenceString(AppSP.token),
                                validate!!.RetriveSharepreferenceString(AppSP.userid)
                            ))
                        val call = apiInterface?.getVO_Shg_Mappingdata(
                            token,
                            user,
                            validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!,
                            cboType
                        )

                        val res = call?.execute()

                        if (res!!.isSuccessful) {
                            if (!res.body()?.VO_Shg_mapped_response.isNullOrEmpty()) {
                                try {
                                    var mapped_Shg = res.body()?.VO_Shg_mapped_response
                                    var shglist = MappingData.returnVO_Shg_listObj(mapped_Shg)
                                    if (!shglist.isNullOrEmpty()) {
                                        CDFIApplication.database?.mappedShgDao()
                                            ?.insertMapped_data(shglist)
                                    }
                                    for (i in mapped_Shg!!.indices) {
                                        var member_details_list = mapped_Shg.get(i).memberDetailsList
                                        var member_list =
                                            MappingData.returnVO_Shgmemberentity(member_details_list)
                                        CDFIApplication.database?.voShgMemberDao()?.insertMapped_Memberdata(member_list)
                                        for(j in member_details_list!!.indices){
                                            var memberPhoneList = member_details_list.get(j).memberPhoneDetailsList!!
                                            CDFIApplication.database?.vOShgMemberPhoneDao()?.insertMapped_MemberPhonedata((memberPhoneList))
                                            CDFIApplication.database?.voShgMemberDao()?.updateVO_shgMember(mapped_Shg.get(i).guid,mapped_Shg.get(i).shg_name,member_details_list.get(j).member_guid,mapped_Shg.get(i).vo_guid)
                                            federationViewmodel!!.updateFedrationCode(mapped_Shg.get(i).vo_code,mapped_Shg.get(i).vo_guid)
                                        }


                                    }
                                    idownload = 0
                                    iEcDownload = getFederationdata()
                                    if(idownload == 0 && iEcDownload == 1){
                                        idownload = 0
                                    }else {
                                        idownload = 1
                                    }
                                    progressDialog.dismiss()
                                } catch (ex: Exception) {
                                    ex.printStackTrace()
//                                    progressDialog.dismiss()
                                }

                            }else {
                                idownload = 0
                            }
                        } else {
                            var resMsg = ""
                            idownload = 1
                            if (res.code() == 403) {

                                code = res.code()
                                msg = res.message()
                            } else {
                                if(res.errorBody()?.contentLength()==0L || res.errorBody()?.contentLength()!!< 0L){
                                    code = res.code()
                                    resMsg = res.message()
                                }else {
                                    var jsonObject1 =
                                        JSONObject(res.errorBody()!!.source().readUtf8().toString())

                                    code =
                                        validate!!.returnIntegerValue(
                                            jsonObject1.optString("responseCode").toString()
                                        )
                                    resMsg = validate!!.returnStringValue(
                                        jsonObject1.optString("responseMsg").toString()
                                    )
                                }
                                msg = "" + validate!!.alertMsg(
                                    this@VOEcActivity,
                                    responseViewModel,
                                    code,
                                    validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                                    resMsg)

                            }
                        }

                        runOnUiThread {
                            if (idownload == 0) {
                                progressDialog.dismiss()
                                iButtonPressed = 1
                                fillSpinner()
                                if(validate!!.RetriveSharepreferenceLong(AppSP.EcCboCode)>0 && cboType==1){
                                    spin_cboName.setSelection(setShgName(validate!!.RetriveSharepreferenceLong(AppSP.EcCboCode),dataspinShg))
                                }
                                /*   val text = LabelSet.getText("Datadownloadedsuccessfully",R.string.Datadownloadedsuccessfully)
                                   validate!!.CustomAlert(text!!,this@VOEcActivity)*/
                            }else {
                                progressDialog.dismiss()
                                if (code == 403) {
                                    Toast.makeText(this@VOEcActivity, msg, Toast.LENGTH_LONG)
                                        .show()
                                    CustomAlertlogin()
                                } else {
                                    val text = LabelSet.getText(
                                        "DatadownloadingFailed",
                                        R.string.DatadownloadingFailed
                                    )
                                    validate!!.CustomAlertVO(msg, this@VOEcActivity)

                                }

                            }

                        }

                    } catch (bug: SocketTimeoutException) {
                        progressDialog.dismiss()
//                                Toast.makeText(activity!!, "Socket Timeout", Toast.LENGTH_LONG).show();
                    } catch (bug: ConnectTimeoutException) {
                        progressDialog.dismiss()
                        // Toast.makeText(getApplicationContext(), "Connection Timeout", Toast.LENGTH_LONG).show();
                    } catch (e: Exception) {
                        e.printStackTrace()
                        progressDialog.dismiss()
                    }
                }

            }.start()

        } else {
            AlertDialog.Builder(this).setTitle("No Internet Connection")
                .setMessage("Please check your internet connection and try again")
                .setPositiveButton(android.R.string.ok) { _, _ -> }
                .setIcon(android.R.drawable.ic_dialog_alert).show()
        }
    }

    fun importCLF_VO_Mapping() {
        var iEcDownload = 0
        if (isNetworkConnected()) {
            clearMappingData(2,validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!)
            progressDialog = ProgressDialog.show(
                this, LabelSet.getText(
                    "app_name",
                    R.string.app_name
                ),
                LabelSet.getText(
                    "DataLoading",
                    R.string.DataLoading
                )
            )
            progressDialog.setIcon(this.getDrawable(R.drawable.appicon))
            var idownload = 0
            var code = 0
            var msg=""
            object : Thread() {

                //@SuppressLint("NewApi")
                override fun run() {
                    try {


                        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                        val token = validate!!.returnStringValue(AESEncryption.decrypt(
                            validate!!.RetriveSharepreferenceString(AppSP.token),
                            validate!!.RetriveSharepreferenceString(AppSP.userid)
                        ))

                        val call = apiInterface?.getCLF_VO_Shg_Mappingdata(
                            token,
                            user,
                            validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!,
                            cboType
                        )

                        val res = call?.execute()

                        if (res!!.isSuccessful) {
                            if (!res.body()?.CLF_VO_Shg_mapped_response.isNullOrEmpty()) {
                                try {
                                    var mapped_Vo = res.body()?.CLF_VO_Shg_mapped_response
                                    var volist = MappingData.returnCLF_VO_listObj(mapped_Vo)
                                    if (!volist.isNullOrEmpty()) {
                                        CDFIApplication.database?.mappedVoDao()
                                            ?.insertMapped_data(volist)
                                    }
                                    for (i in mapped_Vo!!.indices) {
                                        var member_details_list = mapped_Vo.get(i).ecMemberDetailsForMappingList
                                        var member_list =
                                            MappingData.returnClf_VO_memberentity(member_details_list)
                                        CDFIApplication.database?.clfVoMemberDao()?.insertMapped_Memberdata(member_list)
                                        for(j in member_details_list!!.indices){
                                            var memberPhoneList = member_details_list.get(j).memberPhoneDetailsList!!
                                            CDFIApplication.database?.vOShgMemberPhoneDao()?.insertMapped_MemberPhonedata((memberPhoneList))
                                            CDFIApplication.database?.clfVoMemberDao()?.updateVO_shgMember(validate!!.returnStringValue(mapped_Vo.get(i).cbo_child_guid),validate!!.returnStringValue(mapped_Vo.get(i).cbo_guid),validate!!.returnStringValue(member_details_list.get(j).member_guid))
                                            federationViewmodel!!.updateFedrationCode(validate!!.returnLongValue(mapped_Vo.get(i).cbo_code.toString()),mapped_Vo.get(i).cbo_guid)
                                        }


                                    }
                                    idownload = 0
                                    iEcDownload = getFederationdata()
                                    if(idownload == 0 && iEcDownload == 1){
                                        idownload = 0
                                    }else {
                                        idownload = 1
                                    }
                                    progressDialog.dismiss()
                                } catch (ex: Exception) {
                                    ex.printStackTrace()
//                                    progressDialog.dismiss()
                                }

                            }else {
                                idownload = 0
                            }
                        } else {
                            idownload = 1
                            var resMsg = ""
                            if (res.code() == 403) {

                                code = res.code()
                                msg = res.message()
                            } else {
                                if(res.errorBody()?.contentLength()==0L || res.errorBody()?.contentLength()!!< 0L){
                                    code = res.code()
                                    resMsg = res.message()
                                }else {
                                    var jsonObject1 =
                                        JSONObject(res.errorBody()!!.source().readUtf8().toString())

                                    code =
                                        validate!!.returnIntegerValue(
                                            jsonObject1.optString("responseCode").toString()
                                        )
                                    resMsg = validate!!.returnStringValue(
                                        jsonObject1.optString("responseMsg").toString()
                                    )
                                }
                                msg = "" + validate!!.alertMsg(
                                    this@VOEcActivity,
                                    responseViewModel,
                                    code,
                                    validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                                    resMsg)

                            }
                        }

                        runOnUiThread {
                            if (idownload == 0) {
                                progressDialog.dismiss()
                                iButtonPressed = 1
                                fillSpinner()
                                if(validate!!.RetriveSharepreferenceLong(AppSP.EcCboCode)>0 && cboType==2){
                                    spin_cboName.setSelection(setVoName(validate!!.RetriveSharepreferenceLong(AppSP.EcCboCode),dataspinVo))
                                }
                                /*val text = LabelSet.getText("Datadownloadedsuccessfully",R.string.Datadownloadedsuccessfully)
                                validate!!.CustomAlert(text!!,this@VOEcActivity)*/
                            }else {
                                progressDialog.dismiss()
                                if (code == 403) {
                                    Toast.makeText(this@VOEcActivity, msg, Toast.LENGTH_LONG)
                                        .show()
                                    CustomAlertlogin()
                                } else {
                                    val text = LabelSet.getText(
                                        "DatadownloadingFailed",
                                        R.string.DatadownloadingFailed
                                    )
                                    validate!!.CustomAlertVO(msg, this@VOEcActivity)

                                }

                            }

                        }

                    } catch (bug: SocketTimeoutException) {
                        progressDialog.dismiss()
//                                Toast.makeText(activity!!, "Socket Timeout", Toast.LENGTH_LONG).show();
                    } catch (bug: ConnectTimeoutException) {
                        progressDialog.dismiss()
                        // Toast.makeText(getApplicationContext(), "Connection Timeout", Toast.LENGTH_LONG).show();
                    } catch (e: Exception) {
                        e.printStackTrace()
                        progressDialog.dismiss()
                    }
                }

            }.start()

        } else {
            AlertDialog.Builder(this).setTitle("No Internet Connection")
                .setMessage("Please check your internet connection and try again")
                .setPositiveButton(android.R.string.ok) { _, _ -> }
                .setIcon(android.R.drawable.ic_dialog_alert).show()
        }
    }

    fun getFederationdata():Int {
        var iDownValue = 0
        val userId = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
        val token = validate!!.returnStringValue(AESEncryption.decrypt(
            validate!!.RetriveSharepreferenceString(AppSP.token),
            validate!!.RetriveSharepreferenceString(AppSP.userid)
        ))

        val call = apiInterface?.getFederationData(
            "application/json", token, userId,
            validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id)
        )
        val response = call!!.execute()
        if (response.isSuccessful) {
            when (response.code()) {
                200 -> {

                    if (response.body()?.voDownloadModel != null) {
                        try {
                            var federation = response.body()?.voDownloadModel
                            var ecMemberList = federation!!.ecMembersList

                            if (!ecMemberList.isNullOrEmpty()) {
                                CDFIApplication.database?.executiveDao()
                                    ?.insertExectiveDetail(ecMemberList)
                            }

                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }
                    }
                    iDownValue = 1
                }

            }

        } else {
            iDownValue = 0

        }
        return iDownValue
    }

    fun CustomAlertlogin() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogelogin, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mDialogView.etUsername.setText(validate!!.RetriveSharepreferenceString(AppSP.userid))
        mDialogView.etPassword.setText(validate!!.returnStringValue(AESEncryption.decrypt(
            validate!!.RetriveSharepreferenceString(AppSP.Password),
            validate!!.RetriveSharepreferenceString(AppSP.userid))))
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.btn_login.setBackgroundColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.txt_dialog_title2.text = LabelSet.getText("app_name",R.string.app_name)
        mDialogView.btn_login.text = LabelSet.getText("log_in",R.string.log_in)
        mDialogView.chk_showHide1.text = LabelSet.getText("show_password",R.string.show_password)
        mDialogView.btn_login.setOnClickListener {
            mAlertDialog.dismiss()
            importtoken(
                mDialogView.etUsername.text.toString(),
                validate!!.returnStringValue(
                    AESEncryption.encrypt(validate!!.returnStringValue(
                        mDialogView.etPassword.text.toString()),mDialogView.etUsername.text.toString()))
            )
        }
        mAlertDialog.chk_showHide1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                mAlertDialog.etPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                mAlertDialog.etPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
        }

    }

    fun importtoken(user: String, pass: String) {
        var grant_type = "password"
        var userId = user
        var password = pass
        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText(
                "authenticate_user",
                R.string.authenticate_user
            )
        )


        val callCount = apiInterface?.gettokenforce(
            grant_type,
            userId,
            password, validate!!.RetriveSharepreferenceInt(AppSP.State_Selected), 1,
            validate!!.RetriveSharepreferenceString(AppSP.Roleid)!!
        )
        callCount?.enqueue(object : Callback<JsonObject> {
            override fun onFailure(callCount: Call<JsonObject>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                progressDialog.dismiss()


                if (response.isSuccessful) {

                    var jsonObject1: JSONObject? = null
                    try {
                        jsonObject1 = JSONObject(response.body().toString())

                        val auth = jsonObject1.get("accessToken").toString()
                        if (auth.trim().length > 0) {
                            validate!!.SaveSharepreferenceString(
                                AppSP.token,
                                validate!!.returnStringValue(
                                    AESEncryption.encrypt(
                                        auth,validate!!.RetriveSharepreferenceString(AppSP.userid)))
                            )
                        }
                        validate!!.CustomAlertVO(
                            LabelSet.getText(
                                "userauthenticatedsuccessfully",
                                R.string.userauthenticatedsuccessfully
                            ), this@VOEcActivity
                        )

                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Toast.makeText(this@VOEcActivity, response.message(), Toast.LENGTH_LONG)
                            .show()


                    }

                } else {
                    Toast.makeText(this@VOEcActivity, response.message(), Toast.LENGTH_LONG)
                        .show()
                }


            }

        })

    }

    fun uploadECdata(iAutoSave:Int,isUpdate:Int){
        var bank_Fi = 0
        var financialIntermidiation = federationViewmodel!!.getFI(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var bankCount = federationViewmodel!!.getBankCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var signatoryCount = federationViewmodel!!.getSignatoryCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!)

        if((financialIntermidiation == 1 && bankCount>0 && signatoryCount>=2) || financialIntermidiation == 0){
            bank_Fi = 1
        }else{
            bank_Fi = 0
        }
        var scMemberCount = federationViewmodel!!.getSCMemberCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!)
        var officeBearerCount = executiveviewmodel!!.getOBCount(listOf(1,3,5),validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!)
        var cboCount = executiveviewmodel!!.getDistinctCboCount(validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id))
        var ecCount = federationViewmodel!!.getEcMemberCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var mapped_shg_count =
            federationViewmodel!!.getMappedShgCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var mapped_vo_count =
            federationViewmodel!!.getMappedVoCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        if(cboType == 1 && ecCount > 4 && mapped_shg_count > 4 && mapped_shg_count == cboCount && officeBearerCount==3 && bank_Fi ==1 && scMemberCount==0) {
            federationViewmodel!!.updateFedrationEditFlag(1,validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!)
        }else if(cboType == 2 && ecCount > 4 && mapped_vo_count > 4 && mapped_vo_count == cboCount && officeBearerCount==3 && bank_Fi ==1 && scMemberCount==0) {
            federationViewmodel!!.updateFedrationEditFlag(1, validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!)
        } else {
            federationViewmodel!!.updateFedrationEditFlag(-1,validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!)
        }

        if (isNetworkConnected()) {
            var ecMemberlist = executiveviewmodel!!.getExecutivedetaildata(validate!!.RetriveSharepreferenceString(AppSP.ECMemGuid))
            exportEcData(ecMemberlist,iAutoSave,isUpdate)
        } else {
            validate!!.CustomAlertVO(
                LabelSet.getText(
                    "no_internet_msg",
                    R.string.no_internet_msg
                ), this
            )
        }
    }

    fun exportEcData(ecMemberlist: List<Executive_memberEntity>?,iAutoSave:Int,isUpdate:Int) {
        progressDialog = ProgressDialog.show(
            this, LabelSet.getText("app_name", R.string.app_name),
            LabelSet.getText(
                "DataUploading",
                R.string.DataUploading
            )
        )
        progressDialog.setIcon(this.getDrawable(R.drawable.appicon))
        var idownload = 0
        var msg = ""
        var code = 0
        object : Thread() {

            //@SuppressLint("NewApi")
            override fun run() {
                try {

                    if (ecMemberlist!!.size > 0) {

                        val gson = Gson()
                        val json = gson.toJson(ecMemberlist)
                        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                        val token = validate!!.returnStringValue(AESEncryption.decrypt(
                            validate!!.RetriveSharepreferenceString(AppSP.token),
                            validate!!.RetriveSharepreferenceString(AppSP.userid)
                        ))

                        val sData = RequestBody.create(
                            MediaType.parse("application/json; charset=utf-8"),
                            json
                        )
                        val call = apiInterface?.uploadEcMemberdata(
                            "application/json",
                            user,
                            token,
                            sData
                        )

                        val res = call?.execute()

                        if (res!!.isSuccessful) {
                            try {
                                CDFIApplication.database?.executiveDao()
                                    ?.updateuploadStatus(
                                        validate!!.Daybetweentime(validate!!.currentdatetime)
                                    )
                                //       msg = res.body()?.msg!!

                            } catch (ex: Exception) {
                                ex.printStackTrace()
//                                    progressDialog.dismiss()
                            }
                        } else {
                            msg = "" + res.code() + " " + res.message()
                            code = res.code()
                            idownload = 1
//
                        }


                    } else {
                        idownload = -1
                    }

                    runOnUiThread {
                        if (idownload == 0) {
                            progressDialog.dismiss()
                            var text = ""
                            if(iAutoSave==0 && isUpdate ==0) {
                                text = LabelSet.getText(
                                    "data_saved_successfully",
                                    R.string.data_saved_successfully
                                )
                                validate!!.CustomAlertVO(
                                    text,
                                    this@VOEcActivity,
                                    VoEcListActivity::class.java
                                )
                            }else if(iAutoSave==0 && isUpdate == 1) {
                                text = LabelSet.getText(
                                    "updated_successfully",
                                    R.string.updated_successfully
                                )
                                validate!!.CustomAlertVO(
                                    text,
                                    this@VOEcActivity,
                                    VoEcListActivity::class.java
                                )
                            }

                        }else if (idownload == -1) {
                            progressDialog.dismiss()
                            val text = LabelSet.getText(
                                "nothing_upload",
                                R.string.nothing_upload
                            )
                            validate!!.CustomAlertVO(text, this@VOEcActivity)
                        } else {
                            progressDialog.dismiss()
                            if (code == 403) {
                                Toast.makeText(this@VOEcActivity, msg, Toast.LENGTH_LONG)
                                    .show()
                                CustomAlertlogin()
                            } else {

                                validate!!.CustomAlertVO(msg, this@VOEcActivity)
                                Toast.makeText(this@VOEcActivity, msg, Toast.LENGTH_LONG)
                                    .show()
                            }
                        }


                    }
                } catch (bug: SocketTimeoutException) {
                    progressDialog.dismiss()
//                                Toast.makeText(activity!!, "Socket Timeout", Toast.LENGTH_LONG).show();
                } catch (bug: ConnectTimeoutException) {
                    progressDialog.dismiss()
                    // Toast.makeText(getApplicationContext(), "Connection Timeout", Toast.LENGTH_LONG).show();
                } catch (e: Exception) {
                    e.printStackTrace()
                    progressDialog.dismiss()
                }
            }

        }.start()
    }

    fun clearMappingData(cboType:Int,federationGuid:String){
        if(cboType == 1){
            executiveviewmodel!!.deleteVOMappingdata(federationGuid)
            executiveviewmodel!!.deleteVOShgMember(federationGuid)
            executiveviewmodel!!.deleteVOSHGMemberPhone(federationGuid)
        }else if(cboType == 2){
            executiveviewmodel!!.deleteClfMappingdata(federationGuid)
            executiveviewmodel!!.deleteClfVOMember(federationGuid)
            executiveviewmodel!!.deleteClfVOMemberPhone(federationGuid)
        }

    }
}