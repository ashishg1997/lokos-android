package com.microware.cdfi.activity.vo

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.PopupWindow
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.BuildConfig
import com.microware.cdfi.R
import com.microware.cdfi.entity.Cbo_kycEntity
import com.microware.cdfi.entity.ImageuploadEntity
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.Config
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_vokyc_detail.*
import kotlinx.android.synthetic.main.activity_vokyc_detail.btn_savegray
import kotlinx.android.synthetic.main.activity_vokyc_detail.tvUploadFiles
import kotlinx.android.synthetic.main.votablayout.*
import kotlinx.android.synthetic.main.white_toolbar.*
import java.io.*
import java.text.SimpleDateFormat
import java.util.*

class VOKycDetailActivity : AppCompatActivity() {

    var validate: Validate? = null
    var cboKycViewmodel: CboKycViewmodel? = null
    var executiveviewmodel: ExecutiveMemberViewmodel? = null
    var cboKycentity: Cbo_kycEntity? = null
    var frontName = ""
    var rearName = ""
    var bitmap: Bitmap? = null
    var IMAGE_DIRECTORY_NAME = "CDFI"
    private var fileUri: Uri? = null
    var imgPathUpload = ""
    var imgNameUpload = ""
    var guid = ""
    var lookupViewmodel: LookupViewmodel? = null
    var dataspin_doctype: List<LookupEntity>? = null
    var imageUploadViewmodel: ImageUploadViewmodel? = null
    var fedrationViewModel: FedrationViewModel? = null
    var sValidFrom = 0L
    var cboType = 0
    var newimagename = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vokyc_detail)

        validate = Validate(this)
        cboKycViewmodel = ViewModelProviders.of(this).get(CboKycViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        imageUploadViewmodel = ViewModelProviders.of(this).get(ImageUploadViewmodel::class.java)
        fedrationViewModel = ViewModelProviders.of(this).get(FedrationViewModel::class.java)
        executiveviewmodel = ViewModelProviders.of(this).get(ExecutiveMemberViewmodel::class.java)

        ivBack.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if(CheckValidation() == 1){
                    SaveKycDetail(1)

                    var intent = Intent(this, VoKycDetailList::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoKycDetailList::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        if(validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType)==2){
            cboType = 2
        }else {
            cboType = 1
        }

        ivHome.visibility = View.GONE
        tv_title.text = getString(R.string.kycdetails)
        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvEc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary1))
        tvVector.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimary1))
        tv_Ec.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this,R.color.white))

        var mapped_shg_count = fedrationViewModel!!.getMappedShgCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var mapped_vo_count = fedrationViewModel!!.getMappedVoCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var basicComplete = fedrationViewModel!!.getIsCompleteValue(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var addressIsComplete = fedrationViewModel!!.getAddressCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var phoneIsComplete = fedrationViewModel!!.getPhoneCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var bankIsComplete = fedrationViewModel!!.getBankCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var ecIsComplete = fedrationViewModel!!.getEcMemberCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var scIsComplete = fedrationViewModel!!.getScCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))

        if(basicComplete > 0){
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(ecIsComplete > 0){
            IvEc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvEc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(phoneIsComplete > 0){
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(addressIsComplete > 0){
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(bankIsComplete > 0){
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }

        if(scIsComplete > 0){
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }

        lay_mapcbo.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if(CheckValidation() == 1){
                    SaveKycDetail(1)

                    if (cboType == 1) {
                        var intent = Intent(this, VOMapCBOActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    }else if(cboType == 2){
                        var intent = Intent(this, CLFMapCBOActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    }
                }
            }else {
                if (cboType == 1) {
                    var intent = Intent(this, VOMapCBOActivity::class.java)
                    intent.flags =
                        Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }else if(cboType == 2){
                    var intent = Intent(this, CLFMapCBOActivity::class.java)
                    intent.flags =
                        Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }
        }

        lay_vector.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if(CheckValidation() == 1){
                    SaveKycDetail(1)

                    var intent = Intent(this, VoBasicDetailActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoBasicDetailActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_Ec.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if(CheckValidation() == 1){
                    SaveKycDetail(1)

                    var intent = Intent(this, VoEcListActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoEcListActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }
        lay_phone.isEnabled = ecIsComplete > 0
        lay_phone.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if(CheckValidation() == 1){
                    SaveKycDetail(1)

                    var intent = Intent(this, VoPhoneDetailListActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoPhoneDetailListActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_location.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if(CheckValidation() == 1){
                    SaveKycDetail(1)

                    var intent = Intent(this, VoAddressList::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoAddressList::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_bank.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if(CheckValidation() == 1){
                    SaveKycDetail(1)

                    var intent = Intent(this, VoBankListActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoBankListActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }
        lay_systemTag.isEnabled = ecIsComplete>0
        lay_systemTag.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if(CheckValidation() == 1){
                    SaveKycDetail(1)

                    var intent = Intent(this, VoSubCommiteeList::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoSubCommiteeList::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }


        et_validform.setOnClickListener {
            validate!!.datePicker(et_validform)
        }

        et_validtill.setOnClickListener {
            validate!!.futureDatePicker(et_validtill)
        }

        lay_frontattach.setOnClickListener {
            if (frontName.isNotEmpty()) {
                ShowImage(frontName)
            }
        }

        lay_rearattach.setOnClickListener {
            if (rearName.isNotEmpty()) {
                ShowImage(rearName)
            }
        }

        IvFrntUpload.setOnClickListener {
            if (!checkPermission()) {
                requestPermission(101)
            } else {
                if (validate!!.returnlookupcode(spin_kyctype, dataspin_doctype) == 1) {
                    captureimage(7000)
                } else if (validate!!.returnlookupcode(spin_kyctype, dataspin_doctype) == 2) {
                    captureimage(8000)
                } else {
                    validate!!.CustomAlertVO(LabelSet.getText(
                        "pleaseselectid",
                        R.string.pleaseselectid
                    ), this)
                }

            }

        }

        IvrearUpload.setOnClickListener {
            if (!checkPermission()) {
                requestPermission(102)
            } else {
                if (validate!!.returnlookupcode(spin_kyctype, dataspin_doctype) == 1) {
                    captureimage(6000)
                } else if (validate!!.returnlookupcode(spin_kyctype, dataspin_doctype) == 2) {
                    captureimage(4000)
                } else {
                    validate!!.CustomAlertVO(LabelSet.getText(
                        "pleaseselectid",
                        R.string.pleaseselectid
                    ), this)
                }
            }

        }
        if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) == 1) {
            ivLock.visibility = View.VISIBLE
            btn_savegray.visibility = View.VISIBLE
            btn_kyc.visibility = View.GONE

        } else {
            ivLock.visibility = View.GONE
            btn_savegray.visibility = View.GONE
            btn_kyc.visibility = View.VISIBLE
        }
        btn_kyc.setOnClickListener {
            if (!validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID).isNullOrEmpty()){
                if(CheckValidation() == 1){
                    SaveKycDetail(0)
                }
            }else{
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "insert_federation_data_first",
                        R.string.insert_federation_data_first
                    ),this,
                    VoBasicDetailActivity::class.java)
            }
        }

        fillSpinner()

        spin_kyctype?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {
                    var kyc_type = validate!!.returnlookupcode(spin_kyctype,dataspin_doctype)
                    if(kyc_type == 1){
                        lay_validtill.visibility = View.VISIBLE
                    }else if(kyc_type == 2){
                        lay_validtill.visibility = View.GONE
                        et_validtill.setText("")
                    }


                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }
        showData()
        setLabelText()

    }

    private fun fillSpinner() {
        dataspin_doctype  = lookupViewmodel!!.getlookup(21,validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
        validate!!.fillspinner(this,spin_kyctype,dataspin_doctype)
    }
    override
    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            compreesedimage()
            newimagename = true
            bitmap = BitmapFactory.decodeFile(fileUri!!.path)
            if ((requestCode == 7000 || requestCode == 8000) && resultCode == Activity.RESULT_OK) {
                tvUploadFiles.text = frontName
                 ImgFrntUpload.setImageBitmap(bitmap)
            } else if ((requestCode == 4000 || requestCode == 6000) && resultCode == Activity.RESULT_OK) {
                tvUploadReadFiles.text = rearName
                //      IvrearUpload.setImageBitmap(bitmap)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    fun compreesedimage() {
        val options = BitmapFactory.Options()
        var bitmap = BitmapFactory.decodeFile(fileUri!!.path, options)

        if (bitmap != null) {
            var actualWidth = options.outWidth
            var actualHeight = options.outHeight
            var mutableBitmap =
                Bitmap.createScaledBitmap(bitmap, actualWidth / 2, actualHeight / 2, false)

            var fOut: OutputStream? = null
            var file: File = File(fileUri!!.path)
            try {
                fOut = FileOutputStream(file)
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }
            mutableBitmap.compress(Bitmap.CompressFormat.JPEG, 50, fOut)
            try {
                fOut!!.flush()
                fOut.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
    }

    private fun SaveKycDetail(iAutoSave:Int) {

        if(sValidFrom==0L){
            sValidFrom = validate!!.Daybetweentime(validate!!.currentdatetime)
        }

        var kyc_type = validate!!.returnlookupcode(spin_kyctype,dataspin_doctype)

        if (validate!!.RetriveSharepreferenceString(AppSP.VokycGUID).isNullOrEmpty()){
            guid = validate!!.random()
            cboKycentity = Cbo_kycEntity(
                validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!,
                guid,
                validate!!.returnLongValue(validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id).toString()),
                kyc_type,
                et_kycno.text.toString(),
                frontName,
                "",
                rearName,
                "",
                sValidFrom,
                validate!!.Daybetweentime(et_validtill.text.toString()),
                1,
                1,
                "",
                1,
                1,
                1,
                1,
                validate!!.Daybetweentime(validate!!.currentdatetime),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                0,
                "",
                0,
                "",1,cboType)
            cboKycViewmodel!!.insert(cboKycentity!!)

            var kycimage1 = ImageuploadEntity(
                frontName,
                validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID),
                "",
                4,
                0
            )
            var kycimage2 = ImageuploadEntity(
                rearName,
                validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID),
                "",
                4,
                0
            )
            if (frontName.isNotEmpty() && newimagename) {
                imageUploadViewmodel!!.insert(kycimage1)
            }
            if (rearName.isNotEmpty() && newimagename) {
                imageUploadViewmodel!!.insert(kycimage2)
            }

            validate!!.SaveSharepreferenceString(AppSP.VokycGUID,guid)
            cboKycViewmodel!!.updatededup(guid)
            validate!!.updateFederationEditFlag(cboType,validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!,
                validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id),fedrationViewModel,executiveviewmodel)

            if(iAutoSave==0) {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "data_saved_successfully",
                        R.string.data_saved_successfully
                    ),
                    this,
                    VoKycDetailList::class.java
                )
            }
        }else {
            if (checkData() == 0) {
                cboKycViewmodel!!.updateKycDetail(
                    validate!!.RetriveSharepreferenceString(AppSP.VokycGUID)!!,
                    kyc_type,
                    et_kycno.text.toString(),
                    frontName,
                    "",
                    rearName,
                    "",
                    sValidFrom,
                    validate!!.Daybetweentime(et_validtill.text.toString()),
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    validate!!.RetriveSharepreferenceString(AppSP.userid)!!, 1
                )

                var kycimage1 = ImageuploadEntity(
                    frontName,
                    validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID),
                    validate!!.RetriveSharepreferenceString(AppSP.MEMBERGUID),
                    4,
                    0
                )
                var kycimage2 = ImageuploadEntity(
                    rearName,
                    validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID),
                    "",
                    4,
                    0
                )
                if (frontName.isNotEmpty() && newimagename) {
                    imageUploadViewmodel!!.insert(kycimage1)
                }
                if (rearName.isNotEmpty() && newimagename) {
                    imageUploadViewmodel!!.insert(kycimage2)
                }

                cboKycViewmodel!!.updatededup(validate!!.RetriveSharepreferenceString(AppSP.VokycGUID)!!)
                fedrationViewModel!!.updatededup(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!)
                validate!!.updateFederationEditFlag(
                    cboType,
                    validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!,
                    validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id),
                    fedrationViewModel,
                    executiveviewmodel
                )
                if (iAutoSave == 0) {
                    validate!!.CustomAlertVO(
                        LabelSet.getText(
                            "updated_successfully",
                            R.string.updated_successfully
                        ), this, VoKycDetailList::class.java
                    )
                }
            }else  if (iAutoSave == 0) {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "updated_successfully",
                        R.string.updated_successfully
                    ), this, VoKycDetailList::class.java
                )
            }
        }
    }

    private fun showData() {
        var list = cboKycViewmodel!!.getKycdetail(validate!!.RetriveSharepreferenceString(AppSP.VokycGUID))
        if (!list.isNullOrEmpty() && list.size > 0){
            spin_kyctype.setSelection(validate!!.returnlookupcodepos(validate!!.returnIntegerValue(list.get(0).kyc_type.toString()),dataspin_doctype))
            sValidFrom = validate!!.returnLongValue(list.get(0).kyc_valid_from.toString())
            et_validtill.setText(
                validate!!.convertDatetime(validate!!.returnLongValue(list.get(0).kyc_valid_to.toString())))
            et_kycno.setText(validate!!.returnStringValue(list.get(0).kyc_number))
            frontName = validate!!.returnStringValue(list.get(0).kyc_front_doc_orig_name)
            //     rearName = validate!!.returnStringValue(list.get(0).kyc_rear_doc_name)
            if (!list.get(0).kyc_front_doc_orig_name.isNullOrEmpty()){
                tvUploadFiles.text = frontName
            }

            val mediaStorageDirectory = File(
                getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
                AppSP.IMAGE_DIRECTORY_NAME
            )

            if (!list.get(0).kyc_front_doc_orig_name.isNullOrEmpty()) {
                tvUploadFiles.text = frontName
                var mediaFile1 =
                    File(mediaStorageDirectory.path + File.separator + frontName)
                Picasso.with(this).load(mediaFile1).into(ImgFrntUpload)
                //showimage(list.get(0).kyc_front_doc_orig_name!!, IvFrntUpload)
            }
            /*    if (!list.get(0).kyc_rear_doc_name.isNullOrEmpty()){
                    tvUploadReadFiles.text = rearName
                }*/
        }
    }

    fun showimage(name: String, image: ImageView) {
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
                m.invoke(null)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
        val mediaStorageDir = File(
            getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
            AppSP.IMAGE_DIRECTORY_NAME
        )
        val mediaFile: File
        mediaFile = File(
            mediaStorageDir.path + File.separator
                    + name
        )
        imgPathUpload = mediaStorageDir.path + File.separator + name
        imgNameUpload = name

        Picasso.with(this).load(mediaFile).into(image)

    }

    fun captureimage(code: Int) {
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
                m.invoke(null)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//        fileUri = getOutputMediaFileUri(MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE, code)
        fileUri = FileProvider.getUriForFile(
            this,
            BuildConfig.APPLICATION_ID + ".provider",
            getOutputMediaFile(MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE, code)!!
        )
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
        startActivityForResult(intent, code)

    }

    fun getOutputMediaFileUri(type: Int, flag: Int): Uri {
        return Uri.fromFile(getOutputMediaFile(type, flag))
    }

    private fun getOutputMediaFile(type: Int, flag: Int): File? {

        // External sdcard location
        val mediaStorageDir = File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
            AppSP.IMAGE_DIRECTORY_NAME)
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null
            }
        }

        // Create a media file name
        val timeStamp = SimpleDateFormat(
            "yyyyMMdd_HHmmss",
            Locale.ENGLISH
        ).format(Date())
        var seqno = validate!!.RetriveSharepreferenceInt(AppSP.Seqno)
        var imagename = "IMG_$timeStamp,$flag.jpg"
        val mediaFile: File
        if (type == MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE) {
            mediaFile = File(
                mediaStorageDir.path + File.separator
                        + imagename
            )
            imgPathUpload = mediaStorageDir.path + File.separator + "IMG_" + timeStamp + ".jpg"
            if (flag == 7000 || flag == 8000) {
                frontName = imagename
            } else if (flag == 4000 || flag == 6000) {
                rearName = imagename
            }

        } else {
            return null
        }

        return mediaFile
    }

    private fun CheckValidation(): Int {

        var kyc_type = validate!!.returnlookupcode(spin_kyctype,dataspin_doctype)

        var kycCount = cboKycViewmodel!!.getKyc_count(validate!!.returnStringValue(et_kycno.text.toString()),cboType)
        var value = 1
        if (spin_kyctype.selectedItemPosition == 0){
            validate!!.CustomAlertSpinner(
                this,spin_kyctype,LabelSet.getText(
                    "please_select",
                    R.string.please_select
                )+" "+ LabelSet.getText(
                    "kyc_id_type",
                    R.string.kyc_id_type
                ))
            value = 0
            return value
        }else if (et_kycno.text.toString().trim().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                )+" "+LabelSet.getText("kyc_id", R.string.kyc_id), this, et_kycno
            )
            value = 0
            return value
        }else if (kyc_type == 2 && !validate!!.validateTAN(et_kycno.text.toString())) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                )+" "+LabelSet.getText(
                    "valid_kyc_id",
                    R.string.valid_kyc_id
                ), this, et_kycno
            )
            value = 0
            return value
        }else if (lay_validtill.visibility == View.VISIBLE && validate!!.returnStringValue(et_validtill.text.toString()).trim().length==0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "valid_valid_till",
                    R.string.valid_valid_till
                ), this, et_validtill
            )
            value = 0
            return value
        }else if (tvUploadFiles.text.toString().trim().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "capture_kyc_image",
                    R.string.capture_kyc_image
                ), this, tvUploadFiles
            )
            value = 0
            return value
        }else if ((validate!!.RetriveSharepreferenceString(AppSP.VokycGUID)!!.trim().length==0 && kycCount > 0) ||
            (validate!!.RetriveSharepreferenceString(AppSP.VokycGUID)!!.trim().length>0 && kycCount > 1)){
            validate!!.CustomAlertEditText(LabelSet.getText("duplicate_id_number", R.string.duplicate_id_number), this, et_kycno)
            value = 0
            return value
        }

        return value
    }

    private fun checkPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        val result1 =
            ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val result2 =
            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission(requestCode: Int) {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ),
            requestCode
        )
    }

    override fun onBackPressed() {
        var intent = Intent(this, VoKycDetailList::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }

    private fun ShowImage(name: String?) {
        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val layout: View = inflater.inflate(R.layout.popupwindow, null, false)
        val window = PopupWindow(
            layout,
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT,
            true
        )
        window.showAtLocation(layout, Gravity.CENTER, 0, 0)
        val cancel: ImageView = layout.findViewById(R.id.cancel)
        val image_preview: ImageView = layout.findViewById(R.id.image_preview)
        val mediaStorageDirectory = File(
            getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
            AppSP.IMAGE_DIRECTORY_NAME
        )
        var mediaFile1 = File(mediaStorageDirectory.path + File.separator + name)
        cancel.setOnClickListener {
            window.dismiss()
        }

        if (mediaFile1.exists()) {
            Picasso.with(this).load(mediaFile1).into(image_preview)
        } else {
            window.dismiss()
        }

    }

    private fun setLabelText(){
        tvCode.text = LabelSet.getText(
            "member_code",
            R.string.member_code
        )
        et_membercode.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tvKYCType.text = LabelSet.getText(
            "kyc_id_type",
            R.string.kyc_id_type
        )
        tvKycID.text = LabelSet.getText("id", R.string.id)
        et_kycno.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tvKycValid.text = LabelSet.getText(
            "kyc_valid",
            R.string.kyc_valid
        )
        tvValidForm.text = LabelSet.getText(
            "valid_form",
            R.string.valid_form
        )
        et_validform.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )
        tvValidTill.text = LabelSet.getText(
            "valid_till",
            R.string.valid_till
        )
        et_validtill.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )
        tvFront.text = LabelSet.getText(
            "frnt_attcahment",
            R.string.frnt_attcahment
        )
        tvRear.text = LabelSet.getText(
            "rear_attachment",
            R.string.rear_attachment
        )
        tvUploadReadFiles.text = LabelSet.getText(
            "upload_files",
            R.string.upload_files
        )
        btn_kyc.text = LabelSet.getText(
            "add_kyc",
            R.string.add_kyc
        )
        btn_savegray.text = LabelSet.getText(
            "add_kyc",
            R.string.add_kyc
        )
    }

    private fun checkData(): Int {
        var value = 1
        if (!validate!!.RetriveSharepreferenceString(AppSP.VokycGUID).isNullOrEmpty()) {

            var list = cboKycViewmodel!!.getKycdetail(validate!!.RetriveSharepreferenceString(AppSP.VokycGUID))
                if (validate!!.returnStringValue(et_kycno.text.toString()) != validate!!.returnStringValue(
                        list?.get(0)?.kyc_number
                    )
                ) {

                    value = 0

                } else if (validate!!.returnlookupcode(spin_kyctype,dataspin_doctype) != validate!!.returnIntegerValue(
                        list?.get(
                            0
                        )?.kyc_type.toString()
                    )
                ) {

                    value = 0

                }else if(frontName!=validate!!.returnStringValue(list?.get(0)?.kyc_front_doc_orig_name)){
                    value = 0
                }


        } else {
            value = 0

        }
        return value
    }

}
