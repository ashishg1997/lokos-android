package com.microware.cdfi.activity.vo

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.JsonObject
import com.microware.cdfi.R
import com.microware.cdfi.adapter.VOListAdapter
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.api.meetinguploadmodel.MeetingApprovalListModel
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.*
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.FedrationViewModel
import com.microware.cdfi.viewModel.LocationViewModel
import com.microware.cdfi.viewModel.ResponseViewModel
import kotlinx.android.synthetic.main.activity_volist.*
import kotlinx.android.synthetic.main.customealertdialogelogin.*
import kotlinx.android.synthetic.main.customealertdialogelogin.view.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.votoolbar.*
import org.apache.http.conn.ConnectTimeoutException
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException

class VOListActivity : AppCompatActivity() {

    internal lateinit var progressDialog: ProgressDialog
    var locationViewModel: LocationViewModel? = null
    var validate: Validate? = null
    var fedrationViewModel: FedrationViewModel? = null
    var responseViewModel: ResponseViewModel? = null

    var apiInterface: ApiInterface? = null
    var count = 0
    var activecount = 0
    var sStateCode = 0
    var sDistrictCode = 0
    var sBlockCode = 0
    var sPanchayatCode = 0
    var dataPanchayat: List<PanchayatEntity>? = null
    var datablock : List<BlockEntity>? = null
    var cboType = 0
    var pendingcount = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_volist)

        validate = Validate(this)
        fedrationViewModel = ViewModelProviders.of(this).get(FedrationViewModel::class.java)
        locationViewModel = ViewModelProviders.of(this).get(LocationViewModel::class.java)
        responseViewModel = ViewModelProviders.of(this).get(ResponseViewModel::class.java)

        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        IvScan.visibility = View.GONE
        IVSync.visibility = View.GONE
        sStateCode = validate!!.RetriveSharepreferenceInt(AppSP.statecode)
        sDistrictCode = validate!!.RetriveSharepreferenceInt(AppSP.districtcode)
        sBlockCode = validate!!.RetriveSharepreferenceInt(AppSP.blockcode)
        sPanchayatCode = validate!!.RetriveSharepreferenceInt(AppSP.panchayatcode)


        if(validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType)==2){
            cboType = 2
        }else {
            cboType = 1
        }
        count = fedrationViewModel!!.getVoCount(cboType)

        if (count < 10){
            tv_vo_count.text = " 0"+count.toString()
        }else{
            tv_vo_count.text = count.toString()
        }
        icBack.setOnClickListener {
            var intent = Intent(this,VoDrawerActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            finish()
        }

        IvAdd.setOnClickListener {
            validate!!.SaveSharepreferenceString(AppSP.FedrationGUID, "")
            validate!!.SaveSharepreferenceString(AppSP.FedrationName, "")
            validate!!.SaveSharepreferenceLong(AppSP.FedrationCode, 0)
            validate!!.SaveSharepreferenceLong(AppSP.Fedration_id, 0)
            validate!!.SaveSharepreferenceLong(AppSP.Formation_dt, 0)
            validate!!.SaveSharepreferenceInt(AppSP.LockRecord, 0)
            validate!!.SaveSharepreferenceInt(AppSP.isEdited, 0)
            validate!!.SaveSharepreferenceInt(AppSP.mappingRefreshed, 0)
            val i = Intent(this, VoBasicDetailActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)
        }

        fillSpinner()
        setLabelText()

    }

    private fun fillSpinner(){
        if(cboType == 1){
            tbl_panchayat.visibility = View.VISIBLE
            tbl_block.visibility = View.GONE
        }else if(cboType == 2){
            tbl_panchayat.visibility = View.GONE
            tbl_block.visibility = View.VISIBLE
        }
        datablock = locationViewModel!!.getBlock_data(sStateCode,sDistrictCode)
        dataPanchayat = locationViewModel!!.getPanchayatByPanchayatCode(sStateCode, sDistrictCode, sBlockCode)
        validate!!.fillVOPanchayatSpinner(this,spin_panchayat,dataPanchayat)
        validate!!.fillCLFBlockSpinner(this,spin_block,datablock)
        spin_block?.onItemSelectedListener = object:AdapterView.OnItemSelectedListener{
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                sBlockCode = validate!!.returnBlockID(spin_block,datablock)
                if (position > 0 && cboType == 2){
                    validate!!.SaveSharepreferenceInt(AppSP.blockcode,sBlockCode)
                    fillCLFData(sBlockCode)
                }else if (position == 0 && cboType == 2){
                    fillDataall()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

        }

        spin_panchayat?.onItemSelectedListener = object:AdapterView.OnItemSelectedListener{
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                sPanchayatCode = validate!!.returnPanchayatID(spin_panchayat,dataPanchayat)
                if (position > 0 && cboType==1){
                    validate!!.SaveSharepreferenceInt(AppSP.panchayatcode,sPanchayatCode)
                    fillData(sPanchayatCode)
                }else if(position == 0 && cboType ==1){
                    fillDataall()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

        }
    }
    private fun setLabelText() {
        if(validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType)==2){
            tvTotalVO.text = LabelSet.getText(
                "total_clf",
                R.string.total_clf
            )
            tv_title.text = LabelSet.getText(
                "clf_list",
                R.string.clf_list
            )
        }else {
            tvTotalVO.text = LabelSet.getText(
                "total_vo",
                R.string.total_vo
            )
            tv_title.text = LabelSet.getText(
                "vo_list",
                R.string.vo_list
            )
        }
    }


    fun fillData(panchayatCode: Int) {

        fedrationViewModel!!.getFederation_by_PanchayatCode(panchayatCode,cboType)?.observe(this@VOListActivity, object : Observer<List<FederationEntity>> {
            override fun onChanged(voList: List<FederationEntity>?) {

                if (voList!=null) {
                    tblLegend.visibility = View.VISIBLE
                    rvVoList.visibility = View.VISIBLE
                    rvVoList.layoutManager = LinearLayoutManager(this@VOListActivity)
                    rvVoList.adapter = VOListAdapter(this@VOListActivity, voList,cboType)
                } else {
                    rvVoList.visibility = View.INVISIBLE
                    tblLegend.visibility = View.GONE
                }

                pendingcount = fedrationViewModel!!.getVoPendingCount(sPanchayatCode, cboType)
                count = fedrationViewModel!!.getVoCount(sPanchayatCode, cboType)
                activecount = fedrationViewModel!!.getVoActiveCount(sPanchayatCode, cboType)

                tv_vo_count.text = "" + activecount + "/" + count.toString()
                tv_activegroup.text = LabelSet.getText(
                    "active_group_1",
                    R.string.active_group_1
                )+" " +"("+" "+ activecount + " "+")"
                tv_inactivegroup.text = LabelSet.getText(
                    "inactive_group_1",
                    R.string.inactive_group_1
                )+" " +"("+" "+ 0 + " "+")"
                tv_pendingapproval.text = LabelSet.getText(
                    "pending_approval_1",
                    R.string.pending_approval_1
                )+" " +"("+" "+ pendingcount + " "+")"
            }
        })
    }

    fun fillCLFData(blockCode: Int) {

        fedrationViewModel!!.getClfDataByBlock(blockCode,cboType)?.observe(this@VOListActivity, object : Observer<List<FederationEntity>> {
            override fun onChanged(voList: List<FederationEntity>?) {

                if (voList!=null) {
                    tblLegend.visibility = View.VISIBLE
                    rvVoList.visibility = View.VISIBLE
                    rvVoList.layoutManager = LinearLayoutManager(this@VOListActivity)
                    rvVoList.adapter = VOListAdapter(this@VOListActivity, voList,cboType)
                } else {
                    rvVoList.visibility = View.INVISIBLE
                    tblLegend.visibility = View.GONE
                }

                pendingcount = fedrationViewModel!!.getCLFPendingCount(blockCode, cboType)
                count = fedrationViewModel!!.getCLFCount(blockCode, cboType)
                activecount = fedrationViewModel!!.getCLFActiveCount(blockCode, cboType)

                tv_vo_count.text = "" + activecount + "/" + count.toString()
                tv_activegroup.text = LabelSet.getText(
                    "active_group_1",
                    R.string.active_group_1
                )+" " +"("+" "+ activecount + " "+")"
                tv_inactivegroup.text = LabelSet.getText(
                    "inactive_group_1",
                    R.string.inactive_group_1
                )+" " +"("+" "+ 0 + " "+")"
                tv_pendingapproval.text = LabelSet.getText(
                    "pending_approval_1",
                    R.string.pending_approval_1
                )+" " +"("+" "+ pendingcount + " "+")"
            }
        })
    }
    fun fillDataall() {

        fedrationViewModel!!.getFedrationAlldata(cboType)?.observe(this@VOListActivity, object : Observer<List<FederationEntity>> {
            override fun onChanged(voList: List<FederationEntity>?) {

                if (voList!=null) {
                    tblLegend.visibility = View.VISIBLE
                    rvVoList.visibility = View.VISIBLE
                    rvVoList.layoutManager = LinearLayoutManager(this@VOListActivity)
                    rvVoList.adapter = VOListAdapter(this@VOListActivity, voList,cboType)
                } else {
                    rvVoList.visibility = View.INVISIBLE
                    tblLegend.visibility = View.GONE
                }
                pendingcount = fedrationViewModel!!.getVoPendingCount(cboType)
                count = fedrationViewModel!!.getVoCount(cboType)
                activecount = fedrationViewModel!!.getVoActiveCount(cboType)

                tv_vo_count.text = "" + activecount + "/" + count.toString()
                tv_activegroup.text = LabelSet.getText(
                    "active_group_1",
                    R.string.active_group_1
                )+" " +"("+" "+ activecount + " "+")"
                tv_inactivegroup.text = LabelSet.getText(
                    "inactive_group_1",
                    R.string.inactive_group_1
                )+" " +"("+" "+ 0 + " "+")"
                tv_pendingapproval.text = LabelSet.getText(
                    "pending_approval_1",
                    R.string.pending_approval_1
                )+" " +"("+" "+ pendingcount + " "+")"
            }
        })
    }
    override fun onBackPressed() {
        val intent = Intent(this,VoDrawerActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()

    }

    fun getVillage(villageId: Int): String {
        var value:String? = null
        value = locationViewModel!!.getVillageName(villageId)

        return value!!

    }


    fun importVO_SHG_Mapping(cbo_guid:String) {
        if (isNetworkConnected()) {
            progressDialog = ProgressDialog.show(
                this, LabelSet.getText(
                    "app_name",
                    R.string.app_name
                ),
                LabelSet.getText(
                    "DataLoading",
                    R.string.DataLoading
                )
            )
            progressDialog.setIcon(this.getDrawable(R.drawable.appicon))
            var idownload = 0
            var code = 0
            var msg = ""
            object : Thread() {

                //@SuppressLint("NewApi")
                override fun run() {
                    try {

                        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                        val token = validate!!.returnStringValue(
                            AESEncryption.decrypt(
                            validate!!.RetriveSharepreferenceString(AppSP.token),
                            validate!!.RetriveSharepreferenceString(AppSP.userid)
                        ))

                        val call = apiInterface?.getVO_Shg_Mappingdata(
                            token,
                            user,
                            cbo_guid,
                            cboType
                        )

                        val res = call?.execute()

                        if (res!!.isSuccessful) {
                            if (!res.body()?.VO_Shg_mapped_response.isNullOrEmpty()) {
                                try {
                                    var mapped_Shg = res.body()?.VO_Shg_mapped_response
                                    var shglist = MappingData.returnVO_Shg_listObj(mapped_Shg)
                                    if (!shglist.isNullOrEmpty()) {
                                        CDFIApplication.database?.mappedShgDao()
                                            ?.insertMapped_data(shglist)
                                    }
                                    for (i in mapped_Shg!!.indices) {
                                        var member_details_list = mapped_Shg.get(i).memberDetailsList
                                        var member_list =
                                            MappingData.returnVO_Shgmemberentity(member_details_list)
                                        CDFIApplication.database?.voShgMemberDao()?.insertMapped_Memberdata(member_list)
                                        for(j in member_details_list!!.indices){
                                            var memberPhoneList = member_details_list.get(j).memberPhoneDetailsList!!
                                            CDFIApplication.database?.vOShgMemberPhoneDao()?.insertMapped_MemberPhonedata((memberPhoneList))
                                            CDFIApplication.database?.voShgMemberDao()?.updateVO_shgMember(mapped_Shg.get(i).guid,mapped_Shg.get(i).shg_name,member_details_list.get(j).member_guid,mapped_Shg.get(i).vo_guid)
                                            fedrationViewModel!!.updateFedrationCode(mapped_Shg.get(i).vo_code,mapped_Shg.get(i).vo_guid)
                                        }


                                    }
                                    idownload = 0
                                    progressDialog.dismiss()
                                } catch (ex: Exception) {
                                    ex.printStackTrace()
//                                    progressDialog.dismiss()
                                }

                            }else {
                                idownload = 0
                            }
                        } else {
                            var resMsg = ""
                            idownload = 1
                            if (res.code() == 403) {

                                code = res.code()
                                msg = res.message()
                            } else {
                                if(res.errorBody()?.contentLength()==0L || res.errorBody()?.contentLength()!!< 0L){
                                    code = res.code()
                                    resMsg = res.message()
                                }else {
                                    var jsonObject1 =
                                        JSONObject(res.errorBody()!!.source().readUtf8().toString())

                                    code =
                                        validate!!.returnIntegerValue(
                                            jsonObject1.optString("responseCode").toString()
                                        )
                                    resMsg = validate!!.returnStringValue(
                                        jsonObject1.optString("responseMsg").toString()
                                    )
                                }
                                msg = "" + validate!!.alertMsg(
                                    this@VOListActivity,
                                    responseViewModel,
                                    code,
                                    validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                                    resMsg)

                            }
                        }

                        runOnUiThread {
                            if (idownload == 0) {
                                progressDialog.dismiss()
                                val text = LabelSet.getText(
                                    "Datadownloadedsuccessfully",
                                    R.string.Datadownloadedsuccessfully
                                )
                                validate!!.CustomAlertVO(text,this@VOListActivity)
                            }else {
                                progressDialog.dismiss()
                                if (code == 403) {
                                    Toast.makeText(this@VOListActivity, msg, Toast.LENGTH_LONG)
                                        .show()
                                    CustomAlertlogin()
                                } else {
                                    val text = LabelSet.getText(
                                        "DatadownloadingFailed",
                                        R.string.DatadownloadingFailed
                                    )
                                    validate!!.CustomAlertVO(msg, this@VOListActivity)

                                }

                            }

                        }

                    } catch (bug: SocketTimeoutException) {
                        progressDialog.dismiss()
//                                Toast.makeText(activity!!, "Socket Timeout", Toast.LENGTH_LONG).show();
                    } catch (bug: ConnectTimeoutException) {
                        progressDialog.dismiss()
                        // Toast.makeText(getApplicationContext(), "Connection Timeout", Toast.LENGTH_LONG).show();
                    } catch (e: Exception) {
                        e.printStackTrace()
                        progressDialog.dismiss()
                    }
                }

            }.start()

        } else {
            AlertDialog.Builder(this).setTitle("No Internet Connection")
                .setMessage("Please check your internet connection and try again")
                .setPositiveButton(android.R.string.ok) { _, _ -> }
                .setIcon(android.R.drawable.ic_dialog_alert).show()
        }
    }

    private fun isNetworkConnected(): Boolean {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager //1
        val networkInfo = connectivityManager.activeNetworkInfo //2
        return networkInfo != null && networkInfo.isConnected //3
    }

    fun getEcMembercount(shgguid: String): Int {
        return fedrationViewModel!!.getcount(shgguid)
    }

    fun getMappedShgCount(voguid: String): Int {
        return fedrationViewModel!!.getMappedShgCount(voguid)
    }

    fun getMappedVoCount(voguid: String): Int {
        return fedrationViewModel!!.getMappedVoCount(voguid)
    }

    fun importCLF_VO_Mapping(cbo_guid:String) {
        if (isNetworkConnected()) {
            progressDialog = ProgressDialog.show(
                this, LabelSet.getText(
                    "app_name",
                    R.string.app_name
                ),
                LabelSet.getText(
                    "DataLoading",
                    R.string.DataLoading
                )
            )
            progressDialog.setIcon(this.getDrawable(R.drawable.appicon))
            var idownload = 0
            var code = 0
            var msg=""
            object : Thread() {

                //@SuppressLint("NewApi")
                override fun run() {
                    try {


                        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                        val token = validate!!.returnStringValue(AESEncryption.decrypt(
                            validate!!.RetriveSharepreferenceString(AppSP.token),
                            validate!!.RetriveSharepreferenceString(AppSP.userid)
                        ))

                        val call = apiInterface?.getCLF_VO_Shg_Mappingdata(
                            token,
                            user,
                            cbo_guid,
                            cboType
                        )

                        val res = call?.execute()

                        if (res!!.isSuccessful) {
                            if (!res.body()?.CLF_VO_Shg_mapped_response.isNullOrEmpty()) {
                                try {
                                    var mapped_Vo = res.body()?.CLF_VO_Shg_mapped_response
                                    var volist = MappingData.returnCLF_VO_listObj(mapped_Vo)
                                    if (!volist.isNullOrEmpty()) {
                                        CDFIApplication.database?.mappedVoDao()
                                            ?.insertMapped_data(volist)
                                    }
                                    for (i in mapped_Vo!!.indices) {
                                        var member_details_list = mapped_Vo.get(i).ecMemberDetailsForMappingList
                                        var member_list =
                                            MappingData.returnClf_VO_memberentity(member_details_list)
                                        CDFIApplication.database?.clfVoMemberDao()?.insertMapped_Memberdata(member_list)
                                        for(j in member_details_list!!.indices){
                                            var memberPhoneList = member_details_list.get(j).memberPhoneDetailsList!!
                                            CDFIApplication.database?.vOShgMemberPhoneDao()?.insertMapped_MemberPhonedata((memberPhoneList))
                                            CDFIApplication.database?.clfVoMemberDao()?.updateVO_shgMember(validate!!.returnStringValue(mapped_Vo.get(i).cbo_child_guid),validate!!.returnStringValue(mapped_Vo.get(i).cbo_guid),validate!!.returnStringValue(member_details_list.get(j).member_guid))
                                            fedrationViewModel!!.updateFedrationCode(validate!!.returnLongValue(mapped_Vo.get(i).cbo_code.toString()),mapped_Vo.get(i).cbo_guid)
                                        }


                                    }
                                    idownload = 0
                                    progressDialog.dismiss()
                                } catch (ex: Exception) {
                                    ex.printStackTrace()
//                                    progressDialog.dismiss()
                                }

                            }else {
                                idownload = 0
                            }
                        } else {
                            idownload = 1
                            var resMsg = ""
                            if (res.code() == 403) {

                                code = res.code()
                                msg = res.message()
                                CustomAlertlogin()
                            } else {
                                if(res.errorBody()?.contentLength()==0L || res.errorBody()?.contentLength()!!< 0L){
                                    code = res.code()
                                    resMsg = res.message()
                                }else {
                                    var jsonObject1 =
                                        JSONObject(res.errorBody()!!.source().readUtf8().toString())

                                    code =
                                        validate!!.returnIntegerValue(
                                            jsonObject1.optString("responseCode").toString()
                                        )
                                    resMsg = validate!!.returnStringValue(
                                        jsonObject1.optString("responseMsg").toString()
                                    )
                                }
                                msg = "" + validate!!.alertMsg(
                                    this@VOListActivity,
                                    responseViewModel,
                                    code,
                                    validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                                    resMsg)

                            }
                        }

                        runOnUiThread {
                            if (idownload == 0) {
                                progressDialog.dismiss()
                                val text = LabelSet.getText(
                                    "Datadownloadedsuccessfully",
                                    R.string.Datadownloadedsuccessfully
                                )
                                validate!!.CustomAlertVO(text,this@VOListActivity)
                            }else {
                                progressDialog.dismiss()
                                if (code == 403) {
                                    Toast.makeText(this@VOListActivity, msg, Toast.LENGTH_LONG)
                                        .show()
                                    CustomAlertlogin()
                                } else {
                                    val text = LabelSet.getText(
                                        "DatadownloadingFailed",
                                        R.string.DatadownloadingFailed
                                    )
                                    validate!!.CustomAlertVO(msg, this@VOListActivity)

                                }

                            }

                        }

                    } catch (bug: SocketTimeoutException) {
                        progressDialog.dismiss()
//                                Toast.makeText(activity!!, "Socket Timeout", Toast.LENGTH_LONG).show();
                    } catch (bug: ConnectTimeoutException) {
                        progressDialog.dismiss()
                        // Toast.makeText(getApplicationContext(), "Connection Timeout", Toast.LENGTH_LONG).show();
                    } catch (e: Exception) {
                        e.printStackTrace()
                        progressDialog.dismiss()
                    }
                }

            }.start()

        } else {
            AlertDialog.Builder(this).setTitle("No Internet Connection")
                .setMessage("Please check your internet connection and try again")
                .setPositiveButton(android.R.string.ok) { _, _ -> }
                .setIcon(android.R.drawable.ic_dialog_alert).show()
        }
    }

    fun CustomAlert(guid:String,iFlag:Int) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = android.app.AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.btn_yes.setBackgroundColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.btn_no.setBackgroundColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.txt_msg.text = LabelSet.getText(
            "do_u_want_to_delete",
            R.string.do_u_want_to_delete
        )
        mDialogView.btn_yes.setOnClickListener {
            if(iFlag==0){
                fedrationViewModel!!.deleteFederationByGuid(guid)
                fedrationViewModel!!.deleteBankByfederationGUID(guid)
                fedrationViewModel!!.deletePhoneByfederationGUID(guid)
                fedrationViewModel!!.deleteAddressByfederationGUID(guid)
                fedrationViewModel!!.deleteEcByfederationGUID(guid)
                fedrationViewModel!!.deleteScMemberByfederationGUID(guid)
                fedrationViewModel!!.deleteScByfederationGUID(guid)
            }else {
                fedrationViewModel!!.deleteFederationDataByGuid(guid)
                fedrationViewModel!!.deleteBankDataByfederationGUID(guid)
                fedrationViewModel!!.deletePhoneDataByfederationGUID(guid)
                fedrationViewModel!!.deleteAddressDataByfederationGUID(guid)
                fedrationViewModel!!.deleteEcDataByfederationGUID(guid)
                fedrationViewModel!!.deleteScMemberDataByfederationGUID(guid)
                fedrationViewModel!!.deleteScDataByfederationGUID(guid)
            }
            if (spin_panchayat.selectedItemPosition > 0 && cboType==1) {
                sPanchayatCode = validate!!.returnPanchayatID(spin_panchayat,dataPanchayat)
                fillData(sPanchayatCode)
            }else if(spin_block.selectedItemPosition > 0 && cboType==2){
                sBlockCode = validate!!.returnBlockID(spin_block,datablock)
                fillCLFData(sBlockCode)
            } else {
                fillDataall()
            }
            mAlertDialog.dismiss()


        }

        mDialogView.btn_no.setOnClickListener {

            mAlertDialog.dismiss()


        }
    }

    fun CustomAlertlogin() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogelogin, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mDialogView.etUsername.setText(validate!!.RetriveSharepreferenceString(AppSP.userid))
        mDialogView.etPassword.setText(validate!!.returnStringValue(AESEncryption.decrypt(
            validate!!.RetriveSharepreferenceString(AppSP.Password),
            validate!!.RetriveSharepreferenceString(AppSP.userid)
        )))
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.btn_login.setBackgroundColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.txt_dialog_title2.text = LabelSet.getText("app_name",R.string.app_name)
        mDialogView.btn_login.text = LabelSet.getText("log_in",R.string.log_in)
        mDialogView.chk_showHide1.text = LabelSet.getText("show_password",R.string.show_password)
        mDialogView.btn_login.setOnClickListener {
            mAlertDialog.dismiss()
            importtoken(
                mDialogView.etUsername.text.toString(),
                validate!!.returnStringValue(
                    AESEncryption.encrypt(validate!!.returnStringValue(
                        mDialogView.etPassword.text.toString()),mDialogView.etUsername.text.toString()))
            )
        }
        mAlertDialog.chk_showHide1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                mAlertDialog.etPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                mAlertDialog.etPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
        }


    }

    fun importtoken(user: String, pass: String) {
        var grant_type = "password"
        var userId = user
        var password = pass
        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText(
                "authenticate_user",
                R.string.authenticate_user
            )
        )


        val callCount = apiInterface?.gettokenforce(
            grant_type,
            userId,
            password, validate!!.RetriveSharepreferenceInt(AppSP.State_Selected), 1,
            validate!!.RetriveSharepreferenceString(AppSP.Roleid)!!
        )
        callCount?.enqueue(object : Callback<JsonObject> {
            override fun onFailure(callCount: Call<JsonObject>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                progressDialog.dismiss()


                if (response.isSuccessful) {

                    var jsonObject1: JSONObject? = null
                    try {
                        jsonObject1 = JSONObject(response.body().toString())

                        val auth = jsonObject1.get("accessToken").toString()
                        if (auth.trim().length > 0) {
                            validate!!.SaveSharepreferenceString(
                                AppSP.token,
                                validate!!.returnStringValue(
                                    AESEncryption.encrypt(
                                        auth,validate!!.RetriveSharepreferenceString(AppSP.userid)))
                            )
                        }
                        validate!!.CustomAlertVO(
                            LabelSet.getText(
                                "userauthenticatedsuccessfully",
                                R.string.userauthenticatedsuccessfully
                            ), this@VOListActivity
                        )
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        var jsonObject1 =
                            JSONObject(response.errorBody()!!.source().readUtf8().toString())

                        var resCode =
                            validate!!.returnIntegerValue(jsonObject1.optString("responseCode").toString())
                        validate!!.CustomAlertMsgVO(
                            this@VOListActivity,
                            responseViewModel,
                            resCode,
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                            validate!!.returnStringValue(jsonObject1.optString("responseMsg").toString())
                        )
                    }

                } else {
                    var jsonObject1 =
                        JSONObject(response.errorBody()!!.source().readUtf8().toString())

                    var resCode =
                        validate!!.returnIntegerValue(jsonObject1.optString("responseCode").toString())
                    validate!!.CustomAlertMsgVO(
                        this@VOListActivity,
                        responseViewModel,
                        resCode,
                        validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                        validate!!.returnStringValue(jsonObject1.optString("responseMsg").toString())
                    )
                }


            }

        })

    }


    fun importMeetingApprovalList() {

        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText("DataLoading", R.string.DataLoading)
        )

        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
        val token = validate!!.returnStringValue(AESEncryption.decrypt(
            validate!!.RetriveSharepreferenceString(AppSP.token),
            validate!!.RetriveSharepreferenceString(AppSP.userid)
        ))
        val callCount = apiInterface?.getMeetingApprovalList(
            token,
            user
        )

        callCount?.enqueue(object : Callback<List<MeetingApprovalListModel>> {
            override fun onFailure(callCount: Call<List<MeetingApprovalListModel>>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<List<MeetingApprovalListModel>>,
                response: Response<List<MeetingApprovalListModel>>
            ) {

                progressDialog.dismiss()
                if (response.isSuccessful) {
                    if (!response.body()!!.isNullOrEmpty()) {
                        try {
                            var list = response.body()

                            for (i in list!!.indices) {
                                fillApprovalRecycleView(list)
                            }
                            val text = LabelSet.getText(
                                "Datadownloadedsuccessfully",
                                R.string.Datadownloadedsuccessfully
                            )
                            validate!!.CustomAlertVO(text,this@VOListActivity)

                        } catch (ex: Exception) {
                            ex.printStackTrace()
                            //                                    progressDialog.dismiss()
                        }
                    }
                } else {
                    progressDialog.dismiss()
                    var resCode = 0
                    var resMsg = ""
                    if (response.code() == 403) {

                        CustomAlertlogin()
                    } else {
                        if(response.errorBody()?.contentLength()==0L || response.errorBody()?.contentLength()!! <0L){
                            resCode = response.code()
                            resMsg = response.message()
                        }else {
                            var jsonObject1 =
                                JSONObject(response.errorBody()!!.source().readUtf8().toString())

                            resCode =
                                validate!!.returnIntegerValue(
                                    jsonObject1.optString("responseCode").toString()
                                )
                            resMsg = validate!!.returnStringValue(jsonObject1.optString("responseMsg").toString())
                        }

                        validate!!.CustomAlertMsgVO(
                            this@VOListActivity,
                            responseViewModel,
                            resCode,
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,resMsg)

                    }

                }


            }

        })
    }

    fun fillApprovalRecycleView(approvalList:List<MeetingApprovalListModel>?){
        var shgName = ""
        var list = approvalList
        for(i in 0 until approvalList?.size!!) {
            shgName = list?.get(i)?.shgName!!
            var shg = shgName
        }
    }

}
