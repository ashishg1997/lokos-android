package com.microware.cdfi.activity.vo

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.microware.cdfi.R
import com.microware.cdfi.adapter.MapUnmapAdapter
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.api.model.MappedShgUploadModel
import com.microware.cdfi.api.model.MappingDataModel
import com.microware.cdfi.api.model.ParameterUploadModel
import com.microware.cdfi.entity.PanchayatEntity
import com.microware.cdfi.entity.VillageEntity
import com.microware.cdfi.interfaces.ResultCallBacks
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.FedrationViewModel
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.ResponseViewModel
import kotlinx.android.synthetic.main.activity_v_o_map_c_b_o.*
import kotlinx.android.synthetic.main.activity_v_o_map_c_b_o.btn_add
import kotlinx.android.synthetic.main.activity_v_o_map_c_b_o.spin_panchayat
import kotlinx.android.synthetic.main.activity_v_o_map_c_b_o.spin_village
import kotlinx.android.synthetic.main.customealertdialogelogin.*
import kotlinx.android.synthetic.main.customealertdialogelogin.view.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.fragment_address_detail.*
import kotlinx.android.synthetic.main.votablayout.*
import kotlinx.android.synthetic.main.white_toolbar.*
import okhttp3.MediaType
import okhttp3.RequestBody
import org.apache.http.conn.ConnectTimeoutException
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException

@RequiresApi(Build.VERSION_CODES.M)
class VOMapCBOActivity : AppCompatActivity(), ResultCallBacks {

    var uploadModel: ParameterUploadModel? = null
    var validate: Validate? = null
    internal lateinit var progressDialog: ProgressDialog
    var federationViewmodel: FedrationViewModel? = null
    var apiInterface: ApiInterface? = null
    var list: ArrayList<MappingDataModel>? = null
    var responseViewModel: ResponseViewModel? = null
    var cboType = 0
    var iButtonPressed = 0
    var childLevel = 0
    var panchayatId = ""
    var villageId = ""
    var column_name = ""
    var flag = ""
    var mappedShgUploadModel: MappedShgUploadModel? = null
    var lookupViewmodel: LookupViewmodel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_v_o_map_c_b_o)

        validate = Validate(this)
        animationView.visibility = View.VISIBLE
        federationViewmodel = ViewModelProviders.of(this).get(FedrationViewModel::class.java)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        responseViewModel = ViewModelProviders.of(this).get(ResponseViewModel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)

        if (validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType) == 2) {
            cboType = 2
            childLevel = 1
            column_name = "federationId"
            flag = "MAPPING_DATA"
        } else {
            cboType = 1
            childLevel = 0
            column_name = "shgName"
            flag = "VO_MAPPING"
        }

        ivBack.setOnClickListener {
            var intent = Intent(this, VOListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

        tv_title.text = LabelSet.getText(
            "mapping_vo",
            R.string.mapping_vo
        )
        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvEc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivmapcbo.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary1))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvVector.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_Ec.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_mapcbo.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary1))

        var mapped_shg_count =
            federationViewmodel!!.getMappedShgCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var mapped_vo_count =
            federationViewmodel!!.getMappedVoCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var basicComplete =
            federationViewmodel!!.getIsCompleteValue(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var addressIsComplete =
            federationViewmodel!!.getAddressCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var phoneIsComplete =
            federationViewmodel!!.getPhoneCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var bankIsComplete =
            federationViewmodel!!.getBankCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var ecIsComplete =
            federationViewmodel!!.getEcMemberCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var scIsComplete =
            federationViewmodel!!.getScCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var kycIsComplete =
            federationViewmodel!!.getKycCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))

        if (basicComplete > 0) {
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }

        if (ecIsComplete > 0) {
            IvEc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            IvEc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if (phoneIsComplete > 0) {
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if (addressIsComplete > 0) {
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if (bankIsComplete > 0) {
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }

        if (kycIsComplete > 0) {
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }

        if (scIsComplete > 0) {
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }

        lay_systemTag.setOnClickListener {
            var intent = Intent(this, VoSubCommiteeList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_Ec.setOnClickListener {
            var intent = Intent(this, VoEcListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_phone.setOnClickListener {
            var intent = Intent(this, VoPhoneDetailListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_location.setOnClickListener {
            var intent = Intent(this, VoAddressList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_bank.setOnClickListener {
            var intent = Intent(this, VoBankListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_vector.setOnClickListener {
            var intent = Intent(this, VoBasicDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_kyc.setOnClickListener {
            var intent = Intent(this, VoKycDetailList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        setLabelText()

        if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
            bindPanchayatSpinner()
        }


        /*  if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
              importMappingdata()
          }*/

        btnUnmapShg.setOnClickListener {
            if (checkValidation() == 1) {
                btnUnmapShg.setBackgroundColor(Color.parseColor("#18A0FB"))
                btnMapShg.setBackgroundColor(Color.parseColor("#a6a6a6"))
                iButtonPressed = 0
                btn_add.text = LabelSet.getText(
                    "upload_mapped_shg",
                    R.string.upload_mapped_shg
                )
                uploadPendingData()
            }
        }

        btnMapShg.setOnClickListener {
            if (checkValidation() == 1) {
                btnMapShg.setBackgroundColor(Color.parseColor("#18A0FB"))
                btnUnmapShg.setBackgroundColor(Color.parseColor("#a6a6a6"))
                iButtonPressed = 1
                btn_add.text = LabelSet.getText(
                    "upload_unmapped_shg",
                    R.string.upload_unmapped_shg
                )
                uploadPendingData()
            }
        }

        btn_add.setOnClickListener {
            if (checkValidation() == 1) {
                iButtonPressed = 2
                uploadPendingData()
            }
        }
    }

    private fun setLabelText() {
        btnUnmapShg.text = LabelSet.getText(
            "unmapped_shg",
            R.string.unmapped_shg
        )
        btnMapShg.text = LabelSet.getText(
            "linked_shg",
            R.string.linked_shg
        )
        btn_add.text = LabelSet.getText(
            "upload_mapped_shg",
            R.string.upload_mapped_shg
        )
    }

    override fun onBackPressed() {
        var intent = Intent(this, VOListActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    fun getJson(panchayatId: Int, villageId: Int): ParameterUploadModel {
        try {

            //     uploadModel = ParameterUploadModel(panchayat_id,village_id,validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id),"shgName","ASC","VO_MAPPING")
            uploadModel = ParameterUploadModel(
                validate!!.RetriveSharepreferenceInt(AppSP.statecode),
                validate!!.RetriveSharepreferenceInt(AppSP.districtcode),
                validate!!.RetriveSharepreferenceInt(AppSP.blockcode),
                panchayatId,
                villageId,
                validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id),
                column_name,
                "ASC",
                flag,
                100
            )


        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return uploadModel!!
    }

    override fun onError(message: String) {
        progressDialog.dismiss()
        toast(message)
    }

    override fun onSuccess(loginResponse: LiveData<String>) {
        TODO("Not yet implemented")
    }

    override fun onError(message: Int) {
        progressDialog.dismiss()
        toast(getString(message))
    }

    override fun onStarted() {
        progressDialog = ProgressDialog.show(
            this, LabelSet.getText("app_name", R.string.app_name),
            LabelSet.getText("DataLoading", R.string.DataLoading)
        )
        progressDialog.setIcon(this.getDrawable(R.drawable.appicon))
    }

    override fun onnetworkfailed() {
        progressDialog.dismiss()
        validate!!.CustomAlertVO(
            LabelSet.getText(
                "no_internet_msg",
                R.string.no_internet_msg
            ), this
        )
    }

    //    exportMappedData(uploadList)

    fun importMappingdata(panchayatId: Int, villageId: Int) {
        if (isNetworkConnected()) {
            progressDialog = ProgressDialog.show(
                this, LabelSet.getText(
                    "app_name",
                    R.string.app_name
                ),
                LabelSet.getText(
                    "Dataloading",
                    R.string.Dataloading
                )
            )
            progressDialog.setIcon(this.getDrawable(R.drawable.appicon))
            var idownload = 0
            var msg = ""
            var code = 0
            object : Thread() {

                //@SuppressLint("NewApi")
                override fun run() {
                    try {
                        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                        val token = validate!!.returnStringValue(
                            AESEncryption.decrypt(
                                validate!!.RetriveSharepreferenceString(AppSP.token),
                                validate!!.RetriveSharepreferenceString(AppSP.userid)
                            )
                        )

                        val gson = Gson()
                        val json = gson.toJson(getJson(panchayatId, villageId))
                        /*      val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                              val token = validate!!.RetriveSharepreferenceString(AppSP.Token)!!
    */
                        val sData = RequestBody.create(
                            MediaType.parse("application/json; charset=utf-8"),
                            json
                        )
                        val call = apiInterface?.getMappingData(
                            "application/json",
                            user,
                            token,
                            sData
                        )

                        val res = call?.execute()

                        if (res!!.isSuccessful) {
                            if (!list.isNullOrEmpty()) {
                                list?.clear()
                            }
                            if (!res.body()!!.mappingList.isNullOrEmpty()) {

                                list = res.body()!!.mappingList as ArrayList<MappingDataModel>
                                list =
                                    list?.filter { list -> validate!!.returnLongValue(list.shg_code.toString()) > 0L } as ArrayList<MappingDataModel>
                            }
                        } else {
                            idownload = 1
                            if (res.errorBody()?.contentLength() == 0L || res.errorBody()
                                    ?.contentLength()!! < 0L
                            ) {
                                code = res.code()
                                msg = res.message()
                            } else {
                                var jsonObject1 =
                                    JSONObject(res.errorBody()!!.source().readUtf8().toString())

                                code =
                                    validate!!.returnIntegerValue(
                                        jsonObject1.optString("responseCode").toString()
                                    )
                                msg = validate!!.returnStringValue(
                                    jsonObject1.optString("responseMsg").toString()
                                )
                            }
                            msg = "" + validate!!.alertMsg(
                                this@VOMapCBOActivity,
                                responseViewModel,
                                code,
                                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                                msg
                            )

                        }

                        runOnUiThread {
                            if (idownload == 0) {
                                progressDialog.dismiss()
                                val text = "abcd"
                                //          validate!!.CustomAlertVO(text, this@VOMapCBOActivity)
                                if (list != null) {
                                    fillRecycler(
                                        list!!,
                                        validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id),
                                        0
                                    )
                                }
                            } else if (idownload == -1) {
                                progressDialog.dismiss()
                                val text = LabelSet.getText(
                                    "nothing_upload",
                                    R.string.nothing_upload
                                )
                                validate!!.CustomAlertVO(text, this@VOMapCBOActivity)
                            } else {
                                progressDialog.dismiss()
                                if (code == 403) {
                                    Toast.makeText(this@VOMapCBOActivity, msg, Toast.LENGTH_LONG)
                                        .show()
                                    CustomAlertlogin()
                                } else {

                                    validate!!.CustomAlertVO(msg, this@VOMapCBOActivity)
                                    Toast.makeText(this@VOMapCBOActivity, msg, Toast.LENGTH_LONG)
                                        .show()
                                }
                            }


                        }
                    } catch (bug: SocketTimeoutException) {
                        progressDialog.dismiss()
//                                Toast.makeText(activity!!, "Socket Timeout", Toast.LENGTH_LONG).show();
                    } catch (bug: ConnectTimeoutException) {
                        progressDialog.dismiss()
                        // Toast.makeText(getApplicationContext(), "Connection Timeout", Toast.LENGTH_LONG).show();
                    } catch (e: Exception) {
                        e.printStackTrace()
                        progressDialog.dismiss()
                    }
                }

            }.start()
        } else {
            validate!!.CustomAlertVO(
                LabelSet.getText(
                    "no_internet_msg",
                    R.string.no_internet_msg
                ), this
            )
        }
    }

    private fun fillRecycler(listUnmap: List<MappingDataModel>?, cbo_id: Long, iFlag: Int) {
        var listUnmapData: List<MappingDataModel>? = null
        var guid =
            validate!!.returnStringValue(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        if (iFlag == 0) {
            listUnmapData = listUnmap?.filter { listUnmap -> listUnmap.parent_cbo_code != cbo_id }
        } else if (iFlag == 1) {
            listUnmapData = listUnmap?.filter { listUnmap -> listUnmap.parent_cbo_code == cbo_id }
        } else {
            if (btn_add.text.toString()
                    .equals(LabelSet.getText("upload_mapped_shg", R.string.upload_mapped_shg))
            ) {
                listUnmapData =
                    listUnmap?.filter { listUnmap -> listUnmap.parent_cbo_code != cbo_id }
            } else if (btn_add.text.toString()
                    .equals(LabelSet.getText("upload_unmapped_shg", R.string.upload_unmapped_shg))
            ) {
                listUnmapData =
                    listUnmap?.filter { listUnmap -> listUnmap.parent_cbo_code == cbo_id }
            }
        }

        if (listUnmapData != null) {
            rvList.layoutManager = LinearLayoutManager(this@VOMapCBOActivity)
            rvList.adapter = MapUnmapAdapter(
                this@VOMapCBOActivity,
                listUnmapData,
                listUnmap,
                cbo_id,
                iFlag,
                federationViewmodel!!,
                guid,
                this,
                lookupViewmodel
            )
        }
    }

    fun CustomAlertlogin() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogelogin, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mDialogView.etUsername.setText(validate!!.RetriveSharepreferenceString(AppSP.userid))
        mDialogView.etPassword.setText(
            validate!!.returnStringValue(
                AESEncryption.decrypt(
                    validate!!.RetriveSharepreferenceString(AppSP.Password),
                    validate!!.RetriveSharepreferenceString(AppSP.userid)
                )
            )
        )
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.btn_login.setBackgroundColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.txt_dialog_title2.text = LabelSet.getText("app_name", R.string.app_name)
        mDialogView.btn_login.text = LabelSet.getText("log_in", R.string.log_in)
        mDialogView.chk_showHide1.text = LabelSet.getText("show_password", R.string.show_password)
        mDialogView.btn_login.setOnClickListener {
            mAlertDialog.dismiss()
            importtoken(
                mDialogView.etUsername.text.toString(),
                validate!!.returnStringValue(
                    AESEncryption.encrypt(
                        validate!!.returnStringValue(
                            mDialogView.etPassword.text.toString()
                        ), mDialogView.etUsername.text.toString()
                    )
                )
            )
        }
        mAlertDialog.chk_showHide1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                mAlertDialog.etPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                mAlertDialog.etPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
        }

    }

    fun importtoken(user: String, pass: String) {
        var grant_type = "password"
        var userId = user
        var password = pass
        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText(
                "authenticate_user",
                R.string.authenticate_user
            )
        )


        val callCount = apiInterface?.gettokenforce(
            grant_type,
            userId,
            password, validate!!.RetriveSharepreferenceInt(AppSP.State_Selected), 1,
            validate!!.RetriveSharepreferenceString(AppSP.Roleid)!!
        )
        callCount?.enqueue(object : Callback<JsonObject> {
            override fun onFailure(callCount: Call<JsonObject>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                progressDialog.dismiss()


                if (response.isSuccessful) {

                    var jsonObject1: JSONObject? = null
                    try {
                        jsonObject1 = JSONObject(response.body().toString())

                        val auth = jsonObject1.get("accessToken").toString()
                        if (auth.trim().length > 0) {
                            validate!!.SaveSharepreferenceString(
                                AppSP.token,
                                validate!!.returnStringValue(
                                    AESEncryption.encrypt(
                                        auth, validate!!.RetriveSharepreferenceString(AppSP.userid)
                                    )
                                )
                            )
                        }
                        validate!!.CustomAlertVO(
                            LabelSet.getText(
                                "userauthenticatedsuccessfully",
                                R.string.userauthenticatedsuccessfully
                            ), this@VOMapCBOActivity
                        )

                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Toast.makeText(this@VOMapCBOActivity, response.message(), Toast.LENGTH_LONG)
                            .show()


                    }

                } else {
                    Toast.makeText(this@VOMapCBOActivity, response.message(), Toast.LENGTH_LONG)
                        .show()
                }


            }

        })

    }

    fun exportMappedData(mappinglist: List<MappedShgUploadModel>?) {
        if (isNetworkConnected()) {
            progressDialog = ProgressDialog.show(
                this, LabelSet.getText(
                    "app_name",
                    R.string.app_name
                ),
                LabelSet.getText(
                    "DataUploading",
                    R.string.DataUploading
                )
            )
            progressDialog.setIcon(this.getDrawable(R.drawable.appicon))
            var idownload = 0
            var msg = ""
            var code = 0
            object : Thread() {

                //@SuppressLint("NewApi")
                override fun run() {
                    try {

                        if (mappinglist!!.size > 0) {

                            val gson = Gson()
                            val json = gson.toJson(mappinglist)
                            val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                            val token = validate!!.returnStringValue(
                                AESEncryption.decrypt(
                                    validate!!.RetriveSharepreferenceString(AppSP.token),
                                    validate!!.RetriveSharepreferenceString(AppSP.userid)
                                )
                            )

                            val sData = RequestBody.create(
                                MediaType.parse("application/json; charset=utf-8"),
                                json
                            )
                            val call = apiInterface?.uploadMappingdata(
                                "application/json",
                                user,
                                token,
                                sData
                            )

                            val res = call?.execute()

                            if (res!!.isSuccessful) {
                                idownload == 0
                                for (i in 0 until mappinglist.size) {
                                    for (j in 0 until list?.size!!) {
                                        if (mappinglist.get(i).cbo_guid == list?.get(j)?.guid) {
                                            list?.get(j)?.is_edited = 0
                                        }
                                    }
                                }
                            } else {
                                msg = "" + res.code() + " " + res.message()
                                code = res.code()
                                idownload = 1
//
                            }


                        } else {
                            idownload = -1
                        }

                        runOnUiThread {
                            if (idownload == 0) {
                                progressDialog.dismiss()
                                val text = "Record added to queue"
                                fillRecycler(
                                    list,
                                    validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id),
                                    iButtonPressed
                                )
                                if (iButtonPressed == 2) {
                                    validate!!.CustomAlertVO(text, this@VOMapCBOActivity)
                                }
                            } else if (idownload == -1) {
                                progressDialog.dismiss()
                                val text =
                                    LabelSet.getText(
                                        "nothing_upload",
                                        R.string.nothing_upload
                                    )
                                fillRecycler(
                                    list,
                                    validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id),
                                    iButtonPressed
                                )
                                if (iButtonPressed == 2) {
                                    validate!!.CustomAlertVO(text, this@VOMapCBOActivity)
                                }
                            } else {
                                progressDialog.dismiss()
                                if (code == 403) {
                                    Toast.makeText(this@VOMapCBOActivity, msg, Toast.LENGTH_LONG)
                                        .show()
                                    CustomAlertlogin()
                                    fillRecycler(
                                        list,
                                        validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id),
                                        iButtonPressed
                                    )
                                } else {
                                    fillRecycler(
                                        list,
                                        validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id),
                                        iButtonPressed
                                    )
                                    validate!!.CustomAlertVO(msg, this@VOMapCBOActivity)
                                    Toast.makeText(this@VOMapCBOActivity, msg, Toast.LENGTH_LONG)
                                        .show()
                                }
                            }


                        }
                    } catch (bug: SocketTimeoutException) {
                        progressDialog.dismiss()
//                                Toast.makeText(activity!!, "Socket Timeout", Toast.LENGTH_LONG).show();
                    } catch (bug: ConnectTimeoutException) {
                        progressDialog.dismiss()
                        // Toast.makeText(getApplicationContext(), "Connection Timeout", Toast.LENGTH_LONG).show();
                    } catch (e: Exception) {
                        e.printStackTrace()
                        progressDialog.dismiss()
                    }
                }

            }.start()
        } else {
            validate!!.CustomAlertVO(
                LabelSet.getText(
                    "no_internet_msg",
                    R.string.no_internet_msg
                ), this
            )
        }
    }

    private fun isNetworkConnected(): Boolean {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager //1
        val networkInfo = connectivityManager.activeNetworkInfo //2
        return networkInfo != null && networkInfo.isConnected //3
    }

    private fun uploadPendingData() {
        var uploadList1 = list
        var mappedlist: ArrayList<MappedShgUploadModel> = ArrayList<MappedShgUploadModel>()

        if (!uploadList1.isNullOrEmpty()) {
            for (i in 0 until uploadList1.size) {
                if (list?.get(i)?.is_edited == 1) {
                    mappedShgUploadModel = MappedShgUploadModel(
                        validate!!.returnStringValue(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)),
                        list?.get(i)?.guid,
                        validate!!.returnLongValue(
                            validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id).toString()
                        ),
                        list?.get(i)?.shg_id,
                        list?.get(i)?.shg_code,
                        "",
                        "",
                        cboType,
                        childLevel,
                        list?.get(i)?.federation_joining_date,
                        list?.get(i)?.shg_revival_date,
                        list?.get(i)?.settlement_status,
                        list?.get(i)?.leaving_reason,
                        list?.get(i)?.status,
                        list?.get(i)?.is_active,
                        1,
                        list?.get(i)?.is_edited,
                        validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                        validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                        validate!!.Daybetweentime(validate!!.currentdatetime)
                    )
                    mappedlist.add(mappedShgUploadModel!!)

                }
            }
            var uploadList = mappedlist
            if (uploadList != null && uploadList.size > 0) {
                exportMappedData(uploadList)
            } else {
                fillRecycler(
                    list,
                    validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id),
                    iButtonPressed
                )
            }
        }
    }

    fun bindPanchayatSpinner() {

        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText("DataLoading", R.string.DataLoading)
        )

        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
        val token = validate!!.returnStringValue(
            AESEncryption.decrypt(
                validate!!.RetriveSharepreferenceString(AppSP.token),
                validate!!.RetriveSharepreferenceString(AppSP.userid)
            )
        )
        val callCount = apiInterface?.getAllPanchayatByBlock(
            "application/json",
            token,
            user,
            validate!!.RetriveSharepreferenceInt(AppSP.blockcode)
        )

        callCount?.enqueue(object : Callback<List<PanchayatEntity>> {
            override fun onFailure(callCount: Call<List<PanchayatEntity>>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<List<PanchayatEntity>>,
                response: Response<List<PanchayatEntity>>
            ) {

                progressDialog.dismiss()
                if (response.isSuccessful) {
                    if (!response.body()!!.isNullOrEmpty()) {
                        try {
                            var panchayatlist = response.body()

                            fillPanchayatSpinner(panchayatlist)

                        } catch (ex: Exception) {
                            ex.printStackTrace()
                            //                                    progressDialog.dismiss()
                        }
                    }
                } else {
                    progressDialog.dismiss()
                    var resCode = 0
                    var resMsg = ""
                    if (response.code() == 403) {

                        CustomAlertlogin()
                    } else {
                        if (response.errorBody()?.contentLength() == 0L || response.errorBody()
                                ?.contentLength()!! < 0L
                        ) {
                            resCode = response.code()
                            resMsg = response.message()
                        } else {
                            var jsonObject1 =
                                JSONObject(response.errorBody()!!.source().readUtf8().toString())

                            resCode =
                                validate!!.returnIntegerValue(
                                    jsonObject1.optString("responseCode").toString()
                                )
                            resMsg = validate!!.returnStringValue(
                                jsonObject1.optString("responseMsg").toString()
                            )
                        }

                        validate!!.CustomAlertMsg(
                            this@VOMapCBOActivity,
                            responseViewModel,
                            resCode,
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!, resMsg
                        )

                    }

                }


            }

        })
    }

    fun bindVillageSpinner(panchayatId: Int) {

        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText("DataLoading", R.string.DataLoading)
        )

        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
        val token = validate!!.returnStringValue(
            AESEncryption.decrypt(
                validate!!.RetriveSharepreferenceString(AppSP.token),
                validate!!.RetriveSharepreferenceString(AppSP.userid)
            )
        )
        val callCount = apiInterface?.getAllVillagesByPanchayat(
            "application/json",
            token,
            user,
            panchayatId
        )

        callCount?.enqueue(object : Callback<List<VillageEntity>> {
            override fun onFailure(callCount: Call<List<VillageEntity>>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<List<VillageEntity>>,
                response: Response<List<VillageEntity>>
            ) {

                progressDialog.dismiss()
                if (response.isSuccessful) {
                    if (!response.body()!!.isNullOrEmpty()) {
                        try {
                            var villageList = response.body()
                            var listVillage =
                                villageList?.sortedBy { villageList -> villageList.village_name_en }
                            fillVillageSpinner(listVillage, panchayatId)

                        } catch (ex: Exception) {
                            ex.printStackTrace()
                            //                                    progressDialog.dismiss()
                        }
                    }
                } else {
                    progressDialog.dismiss()
                    var resCode = 0
                    var resMsg = ""
                    if (response.code() == 403) {

                        CustomAlertlogin()
                    } else {
                        if (response.errorBody()?.contentLength() == 0L || response.errorBody()
                                ?.contentLength()!! < 0L
                        ) {
                            resCode = response.code()
                            resMsg = response.message()
                        } else {
                            var jsonObject1 =
                                JSONObject(response.errorBody()!!.source().readUtf8().toString())

                            resCode =
                                validate!!.returnIntegerValue(
                                    jsonObject1.optString("responseCode").toString()
                                )
                            resMsg = validate!!.returnStringValue(
                                jsonObject1.optString("responseMsg").toString()
                            )
                        }

                        validate!!.CustomAlertMsg(
                            this@VOMapCBOActivity,
                            responseViewModel,
                            resCode,
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!, resMsg
                        )

                    }

                }


            }

        })
    }

    fun fillPanchayatSpinner(panchayatList: List<PanchayatEntity>?) {
        /* var list1 = list.sortedWith(compareBy(Person::age, Person::name))*/

        var listpanchayat =
            panchayatList?.sortedBy { panchayatList -> panchayatList.panchayat_name_en }
        validate!!.fillPanchayatSpinner(this, spin_panchayat, listpanchayat)
        spin_panchayat?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                var sPanchayatCode = validate!!.returnPanchayatID(spin_panchayat, listpanchayat)
                if (position > 0) {

                    bindVillageSpinner(sPanchayatCode)

                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }
    }

    fun fillVillageSpinner(villageList: List<VillageEntity>?, panchayatId: Int) {
        validate!!.fillVillageSpinner(this, spin_village, villageList)
        spin_village?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                var iVillageId = 0
                if (position > 0) {
                    iVillageId = validate!!.returnVillageID(spin_village, villageList)

                    importMappingdata(panchayatId, iVillageId)
                    btnUnmapShg.setBackgroundColor(Color.parseColor("#18A0FB"))
                    btnMapShg.setBackgroundColor(Color.parseColor("#a6a6a6"))
                    iButtonPressed = 0
                    btn_add.text = LabelSet.getText(
                        "upload_mapped_shg",
                        R.string.upload_mapped_shg
                    )


                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }
    }

    fun CustomAlert(iPanchayatId: Int, iVillageId: Int) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = android.app.AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.btn_yes.setBackgroundColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.btn_no.setBackgroundColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.txt_msg.text = LabelSet.getText(
            "do_u_want_to_upload",
            R.string.do_u_want_to_upload
        )
        mDialogView.btn_yes.setOnClickListener {

            uploadPendingData()
            mAlertDialog.dismiss()

        }

        mDialogView.btn_no.setOnClickListener {

            mAlertDialog.dismiss()
            importMappingdata(iPanchayatId, iVillageId)

        }
    }

    fun checkValidation(): Int {
        var value = 1

        if (spin_panchayat.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_panchayat, LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "grampanchayat",
                    R.string.grampanchayat
                )
            )
            value = 0
            return value
        } else if (spin_village.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_village, LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "city_town_village",
                    R.string.city_town_village
                )
            )
            value = 0
            return value
        }
        return value
    }
}