package com.microware.cdfi.activity.vo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.microware.cdfi.R
import com.microware.cdfi.utility.LabelSet
import kotlinx.android.synthetic.main.activity_vomember_kyc_detail.*
import kotlinx.android.synthetic.main.tablayout.*
import kotlinx.android.synthetic.main.white_toolbar.*

class VOMemberKycDetail : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vomember_kyc_detail)

        ivHome.visibility = View.GONE
        tv_title.text = LabelSet.getText(
            "kycdetails",
            R.string.kycdetails
        )
        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary1))
        tvVector.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimary1))

        lay_vector.setOnClickListener {
            var intent = Intent(this, VoMemberDetailActviity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_phone.setOnClickListener {
            var intent = Intent(this, VoMemebrPhoneDetail::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_location.setOnClickListener {
            var intent = Intent(this, VoMemberAddressDetail::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_bank.setOnClickListener {
            var intent = Intent(this, VoMemberBankDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        setLabelText()
    }

    private fun setLabelText() {
        tvCode.text = LabelSet.getText(
            "member_code",
            R.string.member_code
        )
        tvKYCIDType.text = LabelSet.getText(
            "kyc_id_type",
            R.string.kyc_id_type
        )
        tv_kycNo.text = LabelSet.getText(
            "kyc_no",
            R.string.kyc_no
        )
        tvValidForm.text = LabelSet.getText(
            "valid_form",
            R.string.valid_form
        )
        tvValidTill.text = LabelSet.getText(
            "valid_till",
            R.string.valid_till
        )
        tvFront.text = LabelSet.getText(
            "frnt_attcahment",
            R.string.frnt_attcahment
        )
        tvUploadfront.text = LabelSet.getText(
            "upload_files",
            R.string.upload_files
        )
        et_kyctype.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_kycno.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tvRear.text = LabelSet.getText(
            "rear_attachment",
            R.string.rear_attachment
        )
        tvRearFile.text = LabelSet.getText(
            "upload_files",
            R.string.upload_files
        )
        btn_kyc.text = LabelSet.getText(
            "system_tag",
            R.string.system_tag
        )
    }

    override fun onBackPressed() {
        var intent = Intent(this, VoMemberSbsoListActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }
}
