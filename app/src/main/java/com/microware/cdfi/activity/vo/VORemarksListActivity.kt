package com.microware.cdfi.activity.vo

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.VORejectionRemarksAdapter
import com.microware.cdfi.entity.FederationEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.layout_rjection_remarks_list.*
import kotlinx.android.synthetic.main.white_toolbar.*

class VORemarksListActivity : AppCompatActivity() {

    var fedrationViewModel: FedrationViewModel? = null

    var validate: Validate? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_rjection_remarks_list)

        fedrationViewModel = ViewModelProviders.of(this).get(FedrationViewModel::class.java)

        validate = Validate(this)

        ivHome.visibility = View.GONE
        tv_title.text =  LabelSet.getText(
            "vo_rejection_remarks",
            R.string.vo_rejection_remarks
        )


        ivBack.setOnClickListener {
            var intent = Intent(this, VOListActivity::class.java)
            intent.flags= Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }


        fillData()
    }

    override fun onBackPressed() {
        var intent = Intent(this, VOListActivity::class.java)
        intent.flags= Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    private fun fillData() {
        fedrationViewModel!!.getFedrationDetaildata(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))!!
            .observe(this,object : Observer<List<FederationEntity>> {
                override fun onChanged(volist: List<FederationEntity>?) {
                    if (!volist.isNullOrEmpty() && volist.size > 0 ){
                        rvList.layoutManager = LinearLayoutManager(this@VORemarksListActivity)
                        rvList.adapter = VORejectionRemarksAdapter(this@VORemarksListActivity, volist)
                    }
                }
            })
    }

}