package com.microware.cdfi.activity.vo

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.*
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_vo_address_detail.*
import kotlinx.android.synthetic.main.votablayout.*
import kotlinx.android.synthetic.main.white_toolbar.*
import kotlinx.android.synthetic.main.white_toolbar.tv_title
import java.util.*

class VoAddressDetail : AppCompatActivity() {

    var validate: Validate? = null
    var addressEntity: AddressEntity? = null
    var cboAddressViewModel: CboAddressViewmodel? = null
    var executiveviewmodel: ExecutiveMemberViewmodel? = null
    var sStateCode = 0
    var sDistrictCode = 0
    var sBlockCode = 0
    var sPanchayatCode = 0
    var sVillageCode = 0
    var locationViewModel: LocationViewModel? = null
    var lookupViewmodel: LookupViewmodel? = null
    var federationViewmodel: FedrationViewModel? = null
    var dataspin_addresstype: List<LookupEntity>? = null
    var dataPanchayat: List<PanchayatEntity>? = null
    var dataVillage: List<VillageEntity>? = null
    var datastate: List<StateEntity>? = null
    var datadistrcit: List<DistrictEntity>? = null
    var datablock: List<BlockEntity>? = null
    var vocode = ""
    var voname = ""
    var cboType = 0


    private var CURRENT_SELECTED_EDITTEXT = 0
    var languageCode: String = ""
    var mSpeechRecognizer: SpeechRecognizer? = null
    var is_listening = false
    var is_lang_change = false
    var mSpeechRecognizerIntent: Intent? = null
    private val REQ_CODE_SPEECH_INPUT = 11



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vo_address_detail)

        validate = Validate(this)
        cboAddressViewModel = ViewModelProviders.of(this).get(CboAddressViewmodel::class.java)
        locationViewModel = ViewModelProviders.of(this).get(LocationViewModel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        federationViewmodel = ViewModelProviders.of(this).get(FedrationViewModel::class.java)
        executiveviewmodel = ViewModelProviders.of(this).get(ExecutiveMemberViewmodel::class.java)

        vocode = validate!!.RetriveSharepreferenceLong(AppSP.FedrationCode).toString()
        voname = validate!!.RetriveSharepreferenceString(AppSP.FedrationName)!!
        sStateCode = validate!!.RetriveSharepreferenceInt(AppSP.statecode)
        sDistrictCode = validate!!.RetriveSharepreferenceInt(AppSP.districtcode)
        sBlockCode = validate!!.RetriveSharepreferenceInt(AppSP.blockcode)

        tvCode.text = vocode.toString()
        et_shgcode.setText(voname)
        spin_state.isEnabled = false
        spin_district.isEnabled = false
        spin_block.isEnabled = false

        if (validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType) == 2) {
            cboType = 2
            lay_city_town.visibility = View.GONE
            spin_village.setSelection(0)

        } else {
            cboType = 1
            lay_city_town.visibility = View.GONE
        }
        ivHome.visibility = View.GONE
        tv_title.text = getString(R.string.addressdetails)
        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary1))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvEc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvVector.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary1))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_Ec.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this, R.color.white))

        var mapped_shg_count =
            federationViewmodel!!.getMappedShgCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var mapped_vo_count =
            federationViewmodel!!.getMappedVoCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var basicComplete =
            federationViewmodel!!.getIsCompleteValue(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var addressIsComplete =
            federationViewmodel!!.getAddressCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var phoneIsComplete =
            federationViewmodel!!.getPhoneCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var bankIsComplete =
            federationViewmodel!!.getBankCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var ecIsComplete =
            federationViewmodel!!.getEcMemberCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var scIsComplete =
            federationViewmodel!!.getScCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var kycIsComplete =
            federationViewmodel!!.getKycCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))

        if (ecIsComplete > 0) {
            IvEc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            IvEc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if (phoneIsComplete > 0) {
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if (basicComplete > 0) {
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if (bankIsComplete > 0) {
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }

        if (kycIsComplete > 0) {
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if (scIsComplete > 0) {
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }

        ivBack.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation() == 1) {
                    SaveAddressDetail(1)
                    var intent = Intent(this, VoAddressList::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    finish()
                    //       overridePendingTransition(0, 0)
                }
            } else {
                var intent = Intent(this, VoAddressList::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                finish()
                //   overridePendingTransition(0, 0)
            }
        }
        lay_systemTag.isEnabled = ecIsComplete > 0
        lay_systemTag.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation() == 1) {
                    SaveAddressDetail(1)

                    var intent = Intent(this, VoSubCommiteeList::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            } else {
                var intent = Intent(this, VoSubCommiteeList::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_mapcbo.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation() == 1) {
                    SaveAddressDetail(1)

                    if (cboType == 1) {
                        var intent = Intent(this, VOMapCBOActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    } else if (cboType == 2) {
                        var intent = Intent(this, CLFMapCBOActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    }
                }
            } else {
                if (cboType == 1) {
                    var intent = Intent(this, VOMapCBOActivity::class.java)
                    intent.flags =
                        Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                } else if (cboType == 2) {
                    var intent = Intent(this, CLFMapCBOActivity::class.java)
                    intent.flags =
                        Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }
        }


        lay_Ec.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation() == 1) {
                    SaveAddressDetail(1)

                    var intent = Intent(this, VoEcListActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            } else {
                var intent = Intent(this, VoEcListActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }
        lay_vector.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation() == 1) {
                    SaveAddressDetail(1)
                    var intent = Intent(this, VoBasicDetailActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            } else {
                var intent = Intent(this, VoBasicDetailActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }
        lay_phone.isEnabled = ecIsComplete > 0

        lay_phone.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation() == 1) {
                    SaveAddressDetail(1)
                    var intent = Intent(this, VoPhoneDetailListActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            } else {
                var intent = Intent(this, VoPhoneDetailListActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_bank.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation() == 1) {
                    SaveAddressDetail(1)
                    var intent = Intent(this, VoBankListActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            } else {
                var intent = Intent(this, VoBankListActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_kyc.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation() == 1) {
                    SaveAddressDetail(1)
                    var intent = Intent(this, VoKycDetailList::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            } else {
                var intent = Intent(this, VoKycDetailList::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        spin_panchayat?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                sPanchayatCode = validate!!.returnPanchayatID(spin_panchayat, dataPanchayat)
                if (position > 0) {
                    dataVillage = locationViewModel!!.getVillagedata_by_panchayatCode(
                        sStateCode,
                        sDistrictCode,
                        sBlockCode,
                        sPanchayatCode
                    )
                    validate!!.fillVillageSpinner(
                        this@VoAddressDetail,
                        spin_village,
                        dataVillage
                    )
                    spin_village.setSelection(
                        validate!!.returnVillagepos(
                            sVillageCode,
                            dataVillage
                        )
                    )
                } else {
                    dataVillage = locationViewModel!!.getVillagedata_by_panchayatCode(
                        sStateCode,
                        sDistrictCode,
                        sBlockCode,
                        sPanchayatCode
                    )
                    validate!!.fillVillageSpinner(
                        this@VoAddressDetail,
                        spin_village,
                        dataVillage
                    )
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }
        if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) == 1) {
            ivLock.visibility = View.VISIBLE
            btn_addgray.visibility = View.VISIBLE
            btn_add.visibility = View.GONE

        } else {
            ivLock.visibility = View.GONE
            btn_addgray.visibility = View.GONE
            btn_add.visibility = View.VISIBLE
        }

        btn_add.setOnClickListener {
            if (!validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID).isNullOrEmpty()) {
                if (checkValidation() == 1) {
                    SaveAddressDetail(0)
                }
            } else {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "insert_federation_data_first",
                        R.string.insert_federation_data_first
                    ),
                    this,
                    VoProfileListActivity::class.java
                )
            }
        }

        fillSpinner()
        showData()
        setLabelText()

        languageCode = CDFIApplication.validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!

        iv_vo_address1.setOnClickListener {
            CURRENT_SELECTED_EDITTEXT = 1
            promptSpeechInput(languageCode)
        }

        iv_vo_address2.setOnClickListener {
            CURRENT_SELECTED_EDITTEXT = 2
            promptSpeechInput(languageCode)
        }

    }

    private fun fillSpinner() {
        datastate = locationViewModel!!.getStateByStateCode()
        datadistrcit = locationViewModel!!.getDistrictData(sStateCode)
        datablock = locationViewModel!!.getBlock_data(sStateCode, sDistrictCode)
        validate!!.fillStateSpinner(this, spin_state, datastate)
        validate!!.fillDistrictSpinner(this, spin_district, datadistrcit)
        validate!!.fillBlockSpinner(this, spin_block, datablock)

        dataPanchayat =
            locationViewModel!!.getPanchayatByPanchayatCode(sStateCode, sDistrictCode, sBlockCode)
        validate!!.fillPanchayatSpinner(this, spin_panchayat, dataPanchayat)

        dataVillage = locationViewModel!!.getVillagedata_by_panchayatCode(
            sStateCode,
            sDistrictCode,
            sBlockCode,
            sPanchayatCode
        )
        validate!!.fillVillageSpinner(this, spin_village, dataVillage)

        dataspin_addresstype = lookupViewmodel!!.getlookupfromkeycode(
            "ADDRESSTYPE",
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        validate!!.fillspinner(this, spin_addresstype, dataspin_addresstype)

    }

    private fun showData() {
        var list =
            cboAddressViewModel!!.getAddressdetaildata(validate!!.RetriveSharepreferenceString(AppSP.VoAddressGUID))
        spin_state.setSelection(validate!!.returnStatepos(sStateCode, datastate))
        spin_district.setSelection(
            validate!!.returnDistrictpos(
                sDistrictCode,
                datadistrcit
            )
        )
        spin_block.setSelection(validate!!.returnBlockpos(sBlockCode, datablock))

        if (!list.isNullOrEmpty() && list.size > 0) {
            spin_panchayat.setSelection(
                validate!!.returnPanchayatpos(
                    validate!!.returnIntegerValue(list.get(0).panchayat_id.toString()),
                    dataPanchayat
                )
            )
            sVillageCode = validate!!.returnIntegerValue(list.get(0).village_id.toString())
            et_address1.setText(validate!!.returnStringValue(list.get(0).address_line1))
            et_address2.setText(validate!!.returnStringValue(list.get(0).address_line2))
            et_pincode.setText(validate!!.returnStringValue(list.get(0).postal_code.toString()))
            et_landmark.setText(validate!!.returnStringValue(list.get(0).landmark))
        } else {
            spin_panchayat.setSelection(
                validate!!.returnPanchayatpos(
                    validate!!.RetriveSharepreferenceInt(
                        AppSP.panchayatcode
                    ), dataPanchayat
                )
            )
            sVillageCode = validate!!.RetriveSharepreferenceInt(AppSP.villagecode)
        }
    }

    fun checkValidation(): Int {
        var value = 1
        if (et_address1.text.toString().trim().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "address_1",
                    R.string.address_1
                ), this, et_address1
            )
            value = 0
            return value
        } else if (spin_panchayat.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_panchayat, LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "grampanchayat",
                    R.string.grampanchayat
                )
            )
            value = 0
            return value
        } else if (lay_city_town.visibility == View.VISIBLE && spin_village.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_village, LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "city_town_village",
                    R.string.city_town_village
                )
            )
            value = 0
            return value
        } else if (et_pincode.text.toString().length == 0 || (et_pincode.text.toString().length > 0 && validate!!.returnIntegerValue(
                et_pincode.text.toString()
            ) == 0) || (et_pincode.text.toString().length > 0 && et_pincode.text.toString().length < 6)
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "pincode",
                    R.string.pincode
                ),
                this,
                et_pincode
            )
            value = 0
            return value
        }

        return value

    }

    private fun SaveAddressDetail(iAutoSave: Int) {
        if (checkData() == 0) {
            var isEdited = setFederationEditFlag()
            var villagecode = validate!!.returnVillageID(spin_village, dataVillage)
            var panchayatcode = validate!!.returnPanchayatID(spin_panchayat, dataPanchayat)
            if (validate!!.RetriveSharepreferenceString(AppSP.VoAddressGUID).isNullOrEmpty()) {
                var addressGUID = validate!!.random()
                addressEntity = AddressEntity(
                    0,
                    validate!!.returnLongValue(vocode),
                    0,
                    validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!,
                    addressGUID,
                    cboType,
                    validate!!.returnStringValue(et_address1.text.toString()),
                    validate!!.returnStringValue(et_address2.text.toString()),
                    villagecode,
                    0,
                    panchayatcode,
                    validate!!.RetriveSharepreferenceInt(AppSP.blockcode),
                    validate!!.returnStringValue(et_landmark.text.toString()),
                    validate!!.RetriveSharepreferenceInt(AppSP.statecode),
                    validate!!.RetriveSharepreferenceInt(AppSP.districtcode),
                    validate!!.returnIntegerValue(et_pincode.text.toString()),
                    1,
                    1,
                    1,
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                    0,
                    "",
                    0,
                    0,
                    1, 1
                )
                cboAddressViewModel!!.insert(addressEntity!!)

                validate!!.updateFederationEditFlag(
                    cboType,
                    validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!,
                    validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id),
                    federationViewmodel,
                    executiveviewmodel
                )

                validate!!.SaveSharepreferenceString(AppSP.VoAddressGUID, addressGUID)
                if (iAutoSave == 0) {
                    validate!!.CustomAlertVO(
                        LabelSet.getText(
                            "data_saved_successfully",
                            R.string.data_saved_successfully
                        ), this, VoAddressList::class.java
                    )
                }
            } else {
                cboAddressViewModel!!.updateAddressDetail(
                    validate!!.RetriveSharepreferenceString(AppSP.VoAddressGUID)!!,
                    validate!!.returnStringValue(et_address1.text.toString()),
                    validate!!.returnStringValue(et_address2.text.toString()),
                    villagecode,
                    panchayatcode,
                    0,
                    et_landmark.text.toString(),
                    validate!!.returnIntegerValue(et_pincode.text.toString()),
                    1,
                    1,
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                )

                validate!!.updateFederationEditFlag(
                    cboType,
                    validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!,
                    validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id),
                    federationViewmodel,
                    executiveviewmodel
                )

                if (iAutoSave == 0) {
                    validate!!.CustomAlertVO(
                        LabelSet.getText(
                            "updated_successfully",
                            R.string.updated_successfully
                        ), this, VoAddressList::class.java
                    )
                }
            }
        } else {
            if (iAutoSave == 0) {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "updated_successfully",
                        R.string.updated_successfully
                    ), this, VoAddressList::class.java
                )
            }
        }
    }

    fun setLabelText() {
        if (cboType == 2) {
            tvCode.text = LabelSet.getText(
                "clf_name",
                R.string.clf_name
            )
        } else {
            tvCode.text = LabelSet.getText(
                "fedName",
                R.string.fedName
            )
        }

        tvAddressType.text = LabelSet.getText(
            "address_type",
            R.string.address_type
        )
        et_shgcode.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tvAddressLine1.text = LabelSet.getText(
            "addresslin1",
            R.string.addresslin1
        )
        et_address1.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tvAddressLine2.text = LabelSet.getText(
            "addresslin2",
            R.string.addresslin2
        )
        et_address2.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tvState.text = LabelSet.getText("state", R.string.state)
        tvDistrict.text = LabelSet.getText(
            "district",
            R.string.district
        )
        tvBlock.text = LabelSet.getText("block", R.string.block)
        tvPanchayat.text = LabelSet.getText(
            "panchayat",
            R.string.panchayat
        )
        tvVillageTown.text = LabelSet.getText(
            "city_town_village",
            R.string.city_town_village
        )
        tvPincode.text = LabelSet.getText(
            "pincode",
            R.string.pincode
        )
        et_pincode.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tvLandmark.text = LabelSet.getText(
            "landmark",
            R.string.landmark
        )
        et_landmark.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        btn_add.text = LabelSet.getText(
            "add_address",
            R.string.add_address
        )
        btn_addgray.text = LabelSet.getText(
            "add_address",
            R.string.add_address
        )

    }

    fun setFederationEditFlag(): Int {
        var is_Edited = 0
        var bank_Fi = 0
        var financialIntermidiation =
            federationViewmodel!!.getFI(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var bankCount =
            federationViewmodel!!.getBankCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))

        if ((financialIntermidiation == 1 && bankCount > 0) || financialIntermidiation == 0) {
            bank_Fi = 1
        } else {
            bank_Fi = 0
        }
        var officeBearerCount = executiveviewmodel!!.getOBCount(
            listOf(1, 3, 5),
            validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!
        )
        var cboCount =
            executiveviewmodel!!.getDistinctCboCount(validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id))
        var ecCount =
            federationViewmodel!!.getEcMemberCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var mapped_shg_count =
            federationViewmodel!!.getMappedShgCount(
                validate!!.RetriveSharepreferenceString(
                    AppSP.FedrationGUID
                )
            )
        var mapped_vo_count =
            federationViewmodel!!.getMappedVoCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        if (cboType == 1 && ecCount > 4 && mapped_shg_count > 4 && mapped_shg_count == cboCount && officeBearerCount == 3 && bank_Fi == 1) {
            is_Edited = 1
        } else if (cboType == 2 && ecCount > 4 && mapped_vo_count > 4 && mapped_vo_count == cboCount && officeBearerCount == 3 && bank_Fi == 1) {
            is_Edited = 1
        } else {
            is_Edited = -1
        }
        return is_Edited
    }

    override fun onBackPressed() {
        var intent = Intent(this, VoAddressList::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }

    private fun checkData(): Int {
        var value = 1
        if(!validate!!.RetriveSharepreferenceString(AppSP.VoAddressGUID).isNullOrEmpty()) {
            var list = cboAddressViewModel!!.getAddressdetaildata(
                validate!!.RetriveSharepreferenceString(AppSP.VoAddressGUID)
            )

            if (et_address1.text.toString() != validate!!.returnStringValue(list?.get(0)?.address_line1)) {
                value = 0
                return value
            } else if (et_address2.text.toString() != validate!!.returnStringValue(list?.get(0)?.address_line2)) {
                value = 0
                return value
            } else if (et_pincode.text.toString() != validate!!.returnStringValue(list?.get(0)?.postal_code.toString())) {
                value = 0
                return value
            } else if (et_landmark.text.toString() != validate!!.returnStringValue(list?.get(0)?.landmark)) {
                value = 0
                return value
            }else if (validate!!.returnPanchayatID(spin_panchayat, dataPanchayat) != validate!!.returnIntegerValue(
                    list?.get(
                        0
                    )?.panchayat_id.toString()
                )
            ) {
                value = 0
                return value
            }
        }else {
            value = 0
            return value
        }
        return value
    }


    fun promptSpeechInput(language: String) {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )
        if (isNetworkConnected()){
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, language)
        }
        else{
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.ENGLISH)
        }
        intent.putExtra(
            RecognizerIntent.EXTRA_PROMPT,
            getString(R.string.speech_prompt)
        )
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT)
        } catch (a: ActivityNotFoundException) {
            Toast.makeText(
                applicationContext,
                getString(R.string.speech_not_supported),
                Toast.LENGTH_SHORT
            ).show()
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (resultCode == RESULT_OK) {
                if (requestCode == 11) {
                    if (resultCode == RESULT_OK && null != data) {
                        val result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                        var textdata = result!![0]
                        if (textdata.isNullOrEmpty()) {
                        } else {
                            if (CURRENT_SELECTED_EDITTEXT == 1) {
                                if (et_address1.hasFocus()) {
                                    var cursorPosition: Int = et_address1.selectionStart
                                    et_address1.text.insert(cursorPosition, textdata)
                                } else {
                                    var getData = et_address1.text.toString()
                                    if (getData.length > 0) {
                                        getData = getData + " " + textdata
                                    } else {
                                        getData = getData + textdata
                                    }
                                    et_address1.setText(getData)
                                }
                            }
                            if (CURRENT_SELECTED_EDITTEXT == 2) {
                                if (et_address2.hasFocus()) {
                                    var cursorPosition: Int = et_address2.selectionStart
                                    et_address2.text.insert(cursorPosition, textdata)
                                } else {
                                    var getData = et_address2.text.toString()
                                    if (getData.length > 0) {
                                        getData = getData + " " + textdata
                                    } else {
                                        getData = getData + textdata
                                    }
                                    et_address2.setText(getData)
                                }
                            }
                        }
                    }
                }

            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
//                Toast.makeText(applicationContext, R.string.cancel, Toast.LENGTH_SHORT)
//                    .show()

            }
        } catch (e: Exception) {
            e.printStackTrace()
//            Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show()
        }
    }

    private fun isNetworkConnected(): Boolean {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager //1
        val networkInfo = connectivityManager.activeNetworkInfo //2
        return networkInfo != null && networkInfo.isConnected //3
    }

}
