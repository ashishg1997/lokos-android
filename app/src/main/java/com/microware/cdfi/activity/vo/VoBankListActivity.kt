package com.microware.cdfi.activity.vo

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.VoBankAdapter
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_vo_bank_list.*
import kotlinx.android.synthetic.main.activity_vo_bank_list.btnaddgrey
import kotlinx.android.synthetic.main.activity_vo_bank_list.rvList
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.votablayout.*
import kotlinx.android.synthetic.main.white_toolbar.*

class VoBankListActivity : AppCompatActivity() {

    var validate: Validate? = null
    var cboBankEntity: Cbo_bankEntity? = null
    var cboBankViewmodel: CboBankViewmodel? = null
    var bankMasterViewModel : MasterBankViewmodel? = null
    var masterBankBranchViewModel : MasterBankBranchViewModel? = null
    var federationViewmodel: FedrationViewModel? = null
    var executiveviewmodel: ExecutiveMemberViewmodel? = null
    var cboType = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vo_bank_list)

        validate = Validate(this)
        cboBankViewmodel = ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        bankMasterViewModel = ViewModelProviders.of(this).get(MasterBankViewmodel::class.java)
        masterBankBranchViewModel = ViewModelProviders.of(this).get(MasterBankBranchViewModel::class.java)
        federationViewmodel = ViewModelProviders.of(this).get(FedrationViewModel::class.java)
        executiveviewmodel = ViewModelProviders.of(this).get(ExecutiveMemberViewmodel::class.java)

        if(validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType)==2){
            cboType = 2

        }else {
            cboType = 1
        }

        ivHome.visibility = View.GONE
        tv_title.text = LabelSet.getText(
            "bankdetails",
            R.string.bankdetails
        )
        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary1))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvEc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvVector.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimary1))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_Ec.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this,R.color.white))

        var mapped_shg_count = federationViewmodel!!.getMappedShgCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var mapped_vo_count = federationViewmodel!!.getMappedVoCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var basicComplete = federationViewmodel!!.getIsCompleteValue(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var addressIsComplete = federationViewmodel!!.getAddressCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var phoneIsComplete = federationViewmodel!!.getPhoneCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var bankIsComplete = federationViewmodel!!.getBankCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var ecIsComplete = federationViewmodel!!.getEcMemberCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var scIsComplete = federationViewmodel!!.getScCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var kycIsComplete = federationViewmodel!!.getKycCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))

        if(basicComplete > 0){
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }

        if(ecIsComplete > 0){
            IvEc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvEc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(phoneIsComplete > 0){
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(addressIsComplete > 0){
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(kycIsComplete > 0){
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }

        if(scIsComplete > 0){
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }

        btnFundLinkage.setOnClickListener {
            var intent = Intent(this, FundsWithBankActivity::class.java)
            startActivity(intent)
            finish()
        }

        ivBack.setOnClickListener {
            var intent = Intent(this, VOListActivity::class.java)
            intent.flags=Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }
        lay_systemTag.isEnabled = ecIsComplete>0
        lay_systemTag.setOnClickListener {
            var intent = Intent(this, VoSubCommiteeList::class.java)
             intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_mapcbo.setOnClickListener {
            if (cboType == 1) {
                var intent = Intent(this, VOMapCBOActivity::class.java)
                intent.flags =
                    Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }else if(cboType == 2){
                var intent = Intent(this, CLFMapCBOActivity::class.java)
                intent.flags =
                    Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }
        lay_vector.setOnClickListener {
            var intent = Intent(this, VoBasicDetailActivity::class.java)
             intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(0,0)
        }
        lay_phone.isEnabled = ecIsComplete > 0
        lay_phone.setOnClickListener {
            var intent = Intent(this, VoPhoneDetailListActivity::class.java)
             intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_location.setOnClickListener {
            var intent = Intent(this, VoAddressList::class.java)
             intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_kyc.setOnClickListener {
            var intent = Intent(this, VoKycDetailList::class.java)
             intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_Ec.setOnClickListener {
            var intent = Intent(this, VoEcListActivity::class.java)
             intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) == 1) {
            ivLock.visibility = View.VISIBLE
            btnaddgrey.visibility = View.VISIBLE
            addBank.visibility = View.GONE
        } else {
            ivLock.visibility = View.GONE
            addBank.visibility = View.VISIBLE
            btnaddgrey.visibility = View.GONE

        }

        addBank!!.setOnClickListener {
            if (!validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID).isNullOrEmpty()) {
                validate!!.SaveSharepreferenceString(AppSP.VOBankGUID, "")
                var intent = Intent(this, VOBankDetailActivity::class.java)
                startActivity(intent)
                finish()
            }else {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "insert_federation_data_first",
                        R.string.insert_federation_data_first
                    ),
                    this,
                    VoBasicDetailActivity::class.java
                )
            }
        }

        fillData()

    }

    private fun fillData() {
        cboBankViewmodel!!.getBankdata(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))!!
            .observe(this,object : Observer<List<Cbo_bankEntity>> {
                override fun onChanged(bank_list: List<Cbo_bankEntity>?) {
                    if (bank_list!=null){
                        rvList.layoutManager = LinearLayoutManager(this@VoBankListActivity)
                        rvList.adapter = VoBankAdapter(this@VoBankListActivity, bank_list)
                        if(bank_list.size>0){
                            lay_nobankavialable.visibility=View.GONE
                        }else {
                            lay_nobankavialable.visibility = View.VISIBLE
                            tv_nobank_avialable.text = LabelSet.getText(
                                "no_bank_s_avialable",
                                R.string.no_bank_s_avialable
                            )
                        }
                    }
                }
            })
    }
    fun getBankName(bankID : Int?): String?{
        var value: String? = null
        value = bankMasterViewModel!!.getBankName(bankID!!)
        return value
    }
    fun getBranchname(branchid : Int): String?{
        var value: String? = null
        value = masterBankBranchViewModel!!.getBranchname(branchid)
        return value
    }
    override fun onBackPressed() {
        var intent = Intent(this, VOListActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    fun CustomAlert(guid:String,iFlag:Int) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.btn_yes.setBackgroundColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.btn_no.setBackgroundColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.txt_msg.text = LabelSet.getText(
            "do_u_want_to_delete",
            R.string.do_u_want_to_delete
        )
        mDialogView.btn_yes.setOnClickListener {

            if(iFlag==0){
                cboBankViewmodel!!.deleteRecord(guid)
            }else {
                cboBankViewmodel!!.deleteData(guid)

            }
            validate!!.updateFederationEditFlag(cboType,validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!,
                validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id),federationViewmodel,executiveviewmodel)

            mAlertDialog.dismiss()

        }

        mDialogView.btn_no.setOnClickListener {

            mAlertDialog.dismiss()

        }
    }
}