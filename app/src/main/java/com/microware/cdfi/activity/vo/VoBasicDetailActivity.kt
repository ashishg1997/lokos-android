package com.microware.cdfi.activity.vo

import android.Manifest
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.app.Activity
import android.app.ProgressDialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.provider.Settings
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.microware.cdfi.BuildConfig
import com.microware.cdfi.R
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.api.model.EcScModelJoindata
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.*
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_vo_basic_detail.*
import kotlinx.android.synthetic.main.activity_vo_basic_detail.Imgmember
import kotlinx.android.synthetic.main.activity_vo_basic_detail.Imgshow
import kotlinx.android.synthetic.main.activity_vo_basic_detail.et_ComsavingRoi
import kotlinx.android.synthetic.main.activity_vo_basic_detail.et_bookkeeper_name
import kotlinx.android.synthetic.main.activity_vo_basic_detail.et_bookkeeper_s_mobile_no
import kotlinx.android.synthetic.main.activity_vo_basic_detail.et_coopt_Date
import kotlinx.android.synthetic.main.activity_vo_basic_detail.et_electiontenure
import kotlinx.android.synthetic.main.activity_vo_basic_detail.et_formationDate
import kotlinx.android.synthetic.main.activity_vo_basic_detail.et_promoter_name
import kotlinx.android.synthetic.main.activity_vo_basic_detail.et_revivalDate
import kotlinx.android.synthetic.main.activity_vo_basic_detail.et_saving
import kotlinx.android.synthetic.main.activity_vo_basic_detail.et_shgcode
import kotlinx.android.synthetic.main.activity_vo_basic_detail.et_voluntarysavingROI
import kotlinx.android.synthetic.main.activity_vo_basic_detail.lay_SavingFrequency
import kotlinx.android.synthetic.main.activity_vo_basic_detail.lay_bookkeepermobile
import kotlinx.android.synthetic.main.activity_vo_basic_detail.lay_bookkeepername
import kotlinx.android.synthetic.main.activity_vo_basic_detail.lay_compulsoryROI
import kotlinx.android.synthetic.main.activity_vo_basic_detail.lay_compulsorySaving
import kotlinx.android.synthetic.main.activity_vo_basic_detail.lay_coptdate
import kotlinx.android.synthetic.main.activity_vo_basic_detail.lay_fortnightdate
import kotlinx.android.synthetic.main.activity_vo_basic_detail.lay_fundingAgency
import kotlinx.android.synthetic.main.activity_vo_basic_detail.lay_monthly
import kotlinx.android.synthetic.main.activity_vo_basic_detail.lay_monthlydate
import kotlinx.android.synthetic.main.activity_vo_basic_detail.lay_promoter_name
import kotlinx.android.synthetic.main.activity_vo_basic_detail.lay_revivalDate
import kotlinx.android.synthetic.main.activity_vo_basic_detail.lay_rgrevival
import kotlinx.android.synthetic.main.activity_vo_basic_detail.lay_spinmember
import kotlinx.android.synthetic.main.activity_vo_basic_detail.lay_status
import kotlinx.android.synthetic.main.activity_vo_basic_detail.lay_voluntaryROI
import kotlinx.android.synthetic.main.activity_vo_basic_detail.lay_weekday
import kotlinx.android.synthetic.main.activity_vo_basic_detail.rgBookeeper
import kotlinx.android.synthetic.main.activity_vo_basic_detail.rgrevival
import kotlinx.android.synthetic.main.activity_vo_basic_detail.rgvolunteer
import kotlinx.android.synthetic.main.activity_vo_basic_detail.spin_FundingAgency
import kotlinx.android.synthetic.main.activity_vo_basic_detail.spin_fortnightdate
import kotlinx.android.synthetic.main.activity_vo_basic_detail.spin_frequency
import kotlinx.android.synthetic.main.activity_vo_basic_detail.spin_member
import kotlinx.android.synthetic.main.activity_vo_basic_detail.spin_monthly
import kotlinx.android.synthetic.main.activity_vo_basic_detail.spin_monthlydate
import kotlinx.android.synthetic.main.activity_vo_basic_detail.spin_primaryActivity
import kotlinx.android.synthetic.main.activity_vo_basic_detail.spin_promotedby
import kotlinx.android.synthetic.main.activity_vo_basic_detail.spin_savingfrequency
import kotlinx.android.synthetic.main.activity_vo_basic_detail.spin_secondaryActivity
import kotlinx.android.synthetic.main.activity_vo_basic_detail.spin_status
import kotlinx.android.synthetic.main.activity_vo_basic_detail.spin_tertiaryActivity
import kotlinx.android.synthetic.main.activity_vo_basic_detail.spin_village
import kotlinx.android.synthetic.main.activity_vo_basic_detail.spin_weekday
import kotlinx.android.synthetic.main.activity_vo_basic_detail.tblimage
import kotlinx.android.synthetic.main.activity_vo_basic_detail.tvDate
import kotlinx.android.synthetic.main.activity_vo_basic_detail.tvDate1
import kotlinx.android.synthetic.main.activity_vo_basic_detail.tvPaasbookPage
import kotlinx.android.synthetic.main.activity_vo_basic_detail.tv_bookkeeper
import kotlinx.android.synthetic.main.activity_vo_basic_detail.tv_bookkeeperName
import kotlinx.android.synthetic.main.activity_vo_basic_detail.tv_copt_date
import kotlinx.android.synthetic.main.activity_vo_basic_detail.tv_formationdate
import kotlinx.android.synthetic.main.activity_vo_basic_detail.tv_meetingFrequency
import kotlinx.android.synthetic.main.activity_vo_basic_detail.tv_monthlyday
import kotlinx.android.synthetic.main.activity_vo_basic_detail.tv_promotedName
import kotlinx.android.synthetic.main.activity_vo_basic_detail.tv_revivaldate
import kotlinx.android.synthetic.main.customealertdialogelogin.*
import kotlinx.android.synthetic.main.customealertdialogelogin.view.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.customselectdialoge.view.*
import kotlinx.android.synthetic.main.fragment_basicdetail.*
import kotlinx.android.synthetic.main.votablayout.*
import kotlinx.android.synthetic.main.white_toolbar.*
import okhttp3.MediaType
import okhttp3.RequestBody
import org.apache.commons.io.IOUtils
import org.apache.http.conn.ConnectTimeoutException
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*
import java.net.SocketTimeoutException
import java.nio.channels.FileChannel
import java.text.SimpleDateFormat
import java.util.*

class VoBasicDetailActivity : AppCompatActivity() {

    var validate: Validate? = null
    var fedrationEntity: FederationEntity? = null
    var federationViewmodel: FedrationViewModel? = null
    var executiveviewmodel: ExecutiveMemberViewmodel? = null
    var scMemberViewmodel: Subcommitee_memberViewModel? = null
    var fundingViewModel: FundingAgencyViewModel? = null
    var phoneViewmodel: VOShgMemberPhoneViewmodel? = null
    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: ApiInterface? = null
    var guid = ""
    var stringMsg = ""
    var dataspin_funding: List<FundingEntity>? = null
    var lookupViewmodel: LookupViewmodel? = null
    var dataspin_name: List<LookupEntity>? = null
    var dataspin_monthly: List<LookupEntity>? = null
    var dataspin_monthlydate: List<LookupEntity>? = null
    var dataspin_fortnightdate: List<LookupEntity>? = null
    var dataspin_meetingfreq: List<LookupEntity>? = null
    var dataspin_activity: List<LookupEntity>? = null
    var dataspin_weekday: List<LookupEntity>? = null
    var dataspin_promotedby: List<LookupEntity>? = null
    var dataspin_status: List<LookupEntity>? = null
    var dataspin_yesno: List<LookupEntity>? = null
    var databookkeeper: List<LookupEntity>? = null
    var datarevival: List<LookupEntity>? = null
    var locationViewModel: LocationViewModel? = null
    var dataPanchayat: List<PanchayatEntity>? = null
    var dataVillage: List<VillageEntity>? = null
    var dataspinMember: List<EcScModelJoindata>? = null
    var sStateCode = 0
    var sDistrictCode = 0
    var sBlockCode = 0
    var sPanchayatCode = 0
    var REQUEST_TAKE_GALLERY_VIDEO = 1
    var sVillageCode = 0
    var vocode = ""
    var voName = " "
    var iMeetingOn = 0
    var ifrequencyValue = 0
    var initial_dt: String = "11-06-2011"
    var rgrevivalid = 0
    var cboType = 0
    var childLevel = 0
    var imageName = ""
    var newimageName = false
    var bitmap: Bitmap? = null
    var imageUploadViewmodel: ImageUploadViewmodel? = null
    private var fileUri: Uri? = null
    var mediaFile: File? = null
    var isCompleteDb = 0
    var activationStatus = 0
    var datablock: List<BlockEntity>? = null

    var dataspin_belongmemberphone: List<VoShgMemberPhoneEntity>? = null


    private var CURRENT_SELECTED_EDITTEXT = 0
    var languageCode: String = ""
    var mSpeechRecognizer: SpeechRecognizer? = null
    var is_listening = false
    var is_lang_change = false
    var mSpeechRecognizerIntent: Intent? = null
    private val REQ_CODE_SPEECH_INPUT = 11


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vo_basic_detail)

        ivHome.visibility = View.GONE
        lay_memberCBO.visibility = View.GONE
        tv_title.text = LabelSet.getText(
            "basic_detail",
            R.string.basic_detail
        )
        spin_ParentCBO.isEnabled = false

        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()

        btn_cancel.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) == 1) {
                var intent = Intent(this, VOListActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                finish()
            } else {
                CustomAlertPartialsave(
                    LabelSet.getText(
                        "doyouwanttoexit",
                        R.string.doyouwanttoexit
                    ), VOListActivity::class.java
                )
            }
        }


        ivBack.setOnClickListener {

            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation(1) == 1) {
                    SaveBasicDetail(1)
                    var intent = Intent(this, VOListActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    finish()
                }
            } else {
                var intent = Intent(this, VOListActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                finish()
            }
        }

        validate = Validate(this)
        federationViewmodel = ViewModelProviders.of(this).get(FedrationViewModel::class.java)
        locationViewModel = ViewModelProviders.of(this).get(LocationViewModel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        scMemberViewmodel = ViewModelProviders.of(this).get(Subcommitee_memberViewModel::class.java)
        imageUploadViewmodel = ViewModelProviders.of(this).get(ImageUploadViewmodel::class.java)
        phoneViewmodel = ViewModelProviders.of(this).get(VOShgMemberPhoneViewmodel::class.java)
        fundingViewModel = ViewModelProviders.of(this).get(FundingAgencyViewModel::class.java)
        executiveviewmodel = ViewModelProviders.of(this).get(ExecutiveMemberViewmodel::class.java)

        sStateCode = validate!!.RetriveSharepreferenceInt(AppSP.statecode)
        sDistrictCode = validate!!.RetriveSharepreferenceInt(AppSP.districtcode)
        sBlockCode = validate!!.RetriveSharepreferenceInt(AppSP.blockcode)

        if (validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType) == 2) {
            cboType = 2
            childLevel = 1
            lay_block.visibility = View.VISIBLE
            lay_grampanchayat.visibility = View.GONE
            lay_village.visibility = View.GONE
            spin_block.isEnabled = false
        } else {
            cboType = 1
            childLevel = 0
            lay_block.visibility = View.GONE
            lay_grampanchayat.visibility = View.VISIBLE
            lay_village.visibility = View.GONE
        }
        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary1))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvEc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvVector.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary1))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_Ec.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this, R.color.white))

        var mapped_shg_count =
            federationViewmodel!!.getMappedShgCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var mapped_vo_count =
            federationViewmodel!!.getMappedVoCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var basicComplete =
            federationViewmodel!!.getIsCompleteValue(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var addressIsComplete =
            federationViewmodel!!.getAddressCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var phoneIsComplete =
            federationViewmodel!!.getPhoneCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var bankIsComplete =
            federationViewmodel!!.getBankCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var ecIsComplete =
            federationViewmodel!!.getEcMemberCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var scIsComplete =
            federationViewmodel!!.getScCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var kycIsComplete =
            federationViewmodel!!.getKycCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))

        if (ecIsComplete > 0) {
            IvEc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            IvEc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if (phoneIsComplete > 0) {
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if (addressIsComplete > 0) {
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if (bankIsComplete > 0) {
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }

        if (kycIsComplete > 0) {
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }

        if (scIsComplete > 0) {
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        } else {
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        lay_phone.isEnabled = ecIsComplete > 0
        lay_phone.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation(1) == 1) {
                    SaveBasicDetail(1)

                    var intent = Intent(this, VoPhoneDetailListActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            } else {
                var intent = Intent(this, VoPhoneDetailListActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }
        lay_systemTag.isEnabled = ecIsComplete > 0
        lay_systemTag.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation(1) == 1) {
                    SaveBasicDetail(1)

                    var intent = Intent(this, VoSubCommiteeList::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            } else {
                var intent = Intent(this, VoSubCommiteeList::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_location.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation(1) == 1) {
                    SaveBasicDetail(1)

                    var intent = Intent(this, VoAddressList::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            } else {
                var intent = Intent(this, VoAddressList::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_bank.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation(1) == 1) {
                    SaveBasicDetail(1)

                    var intent = Intent(this, VoBankListActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            } else {
                var intent = Intent(this, VoBankListActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_Ec.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation(1) == 1) {
                    SaveBasicDetail(1)

                    var intent = Intent(this, VoEcListActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            } else {
                var intent = Intent(this, VoEcListActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }


        lay_mapcbo.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation(1) == 1) {
                    SaveBasicDetail(1)
                    if (cboType == 1) {
                        var intent = Intent(this, VOMapCBOActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    } else if (cboType == 2) {
                        var intent = Intent(this, CLFMapCBOActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    }
                }
            } else {
                if (cboType == 1) {
                    var intent = Intent(this, VOMapCBOActivity::class.java)
                    intent.flags =
                        Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                } else if (cboType == 2) {
                    var intent = Intent(this, CLFMapCBOActivity::class.java)
                    intent.flags =
                        Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }
        }

        lay_kyc.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation(1) == 1) {
                    SaveBasicDetail(1)

                    var intent = Intent(this, VoKycDetailList::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            } else {
                var intent = Intent(this, VoKycDetailList::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        et_formationDate.setOnClickListener {
            validate!!.datePickerwithmindate(
                validate!!.Daybetweentime("01-01-1990"),
                et_formationDate
            )

        }

        et_revivalDate.setOnClickListener {
            var date = validate!!.Daybetweentime(et_formationDate.text.toString())
            if (date > 0) {
                validate!!.datePickerwithmindate(date, et_revivalDate)
            } else {
                validate!!.datePicker(et_revivalDate)
            }

        }
        et_coopt_Date.setOnClickListener {
            var date = validate!!.Daybetweentime(et_formationDate.text.toString())
            if (date > 0) {
                validate!!.datePickerwithmindate(date, et_coopt_Date)
            } else {
                validate!!.datePicker(et_coopt_Date)
            }

        }
        if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) == 1) {
            ivLock.visibility = View.VISIBLE
            btn_savegray.visibility = View.VISIBLE
            btn_save.visibility = View.GONE

        } else {
            ivLock.visibility = View.GONE
            btn_savegray.visibility = View.GONE
            btn_save.visibility = View.VISIBLE
        }
        btn_save.setOnClickListener {
            if (checkValidation(0) == 1) {
                SaveBasicDetail(0)
            }

        }

        spin_grampanchayat?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                sPanchayatCode = validate!!.returnPanchayatID(spin_grampanchayat, dataPanchayat)
                if (position > 0) {
                    dataVillage = locationViewModel!!.getVillagedata_by_panchayatCode(
                        sStateCode,
                        sDistrictCode,
                        sBlockCode,
                        sPanchayatCode
                    )
                    validate!!.fillVillageSpinner(
                        this@VoBasicDetailActivity,
                        spin_village,
                        dataVillage
                    )
                    spin_village.setSelection(
                        validate!!.returnVillagepos(
                            sVillageCode,
                            dataVillage
                        )
                    )
                } else {
                    dataVillage = locationViewModel!!.getVillagedata_by_panchayatCode(
                        sStateCode,
                        sDistrictCode,
                        sBlockCode,
                        sPanchayatCode
                    )
                    validate!!.fillVillageSpinner(
                        this@VoBasicDetailActivity,
                        spin_village,
                        dataVillage
                    )
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }
        Imgmember.setOnClickListener {
            CustomSelectAlert()
        }
        tblimage.setOnClickListener {
            if (imageName.isNotEmpty()) {
                if (imageName.contains("pdf")) {
                    if (Build.VERSION.SDK_INT >= 24) {
                        try {
                            val m =
                                StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
                            m.invoke(null)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }
                    val pdfFile = File(
                        getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
                        AppSP.IMAGE_DIRECTORY_NAME + File.separator + imageName
                    )
                    val path = FileProvider.getUriForFile(
                        this,
                        BuildConfig.APPLICATION_ID + ".provider",
                        pdfFile
                    )
                    Log.e("create pdf uri path==>", "" + path)

                    try {
                        val intent = Intent(Intent.ACTION_VIEW)
                        intent.setDataAndType(path, "application/pdf")
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                        startActivity(intent)
                    } catch (e: ActivityNotFoundException) {
                        Toast.makeText(
                            this
                                .applicationContext,
                            "There is no any PDF Viewer",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } else {


                    ShowImage(imageName)
                }
            }

        }


        languageCode = CDFIApplication.validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
        et_voluntarysavingROI.filters = arrayOf<InputFilter>(DecimalDigitsInputFilter(3, 2))
        et_ComsavingRoi.filters = arrayOf<InputFilter>(DecimalDigitsInputFilter(3, 2))
        validate!!.disableEmojiInTitle(et_vonamelocal, 100)
        fillSpinner()
        fillRadio()
        showData()
        setLabelText()

        iv_voname.setOnClickListener {
            CURRENT_SELECTED_EDITTEXT = 1
            promptSpeechInput("en")
        }

        iv_vonamelocal.setOnClickListener {
            CURRENT_SELECTED_EDITTEXT = 2
            promptSpeechInput(languageCode)
        }

        iv_promoter_name.setOnClickListener {
            CURRENT_SELECTED_EDITTEXT = 3
            promptSpeechInput("en")
        }

        iv_bookkeeper_name.setOnClickListener {
            CURRENT_SELECTED_EDITTEXT = 4
            promptSpeechInput("en")
        }
    }

    private fun fillRadio() {
        dataspin_yesno = lookupViewmodel!!.getlookup(
            9,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        databookkeeper = lookupViewmodel!!.getlookup(
            44,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        datarevival = lookupViewmodel!!.getlookupnotin(
            51, 3,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        validate!!.fillradio(rgvolunteer, 0, dataspin_yesno, this)
        if (cboType == 1) {
            validate!!.fillradioFinancial(rgIntermediation, 0, dataspin_yesno, this, cboType)
        } else if (cboType == 2) {
            validate!!.fillradioFinancial(rgIntermediation, 1, dataspin_yesno, this, cboType)
        }
        validate!!.fillradio(rgBookeeper, 0, databookkeeper, this)
        validate!!.fillradio(rgrevival, 1, datarevival, this)

        rgrevival.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->

            if (checkedId == 1) {
                lay_coptdate.visibility = View.GONE
                lay_revivalDate.visibility = View.GONE
                et_coopt_Date.setText("")
                et_revivalDate.setText("")
            } else if (checkedId == 2) {
                lay_coptdate.visibility = View.VISIBLE
                lay_revivalDate.visibility = View.GONE
                et_revivalDate.setText("")
            } else if (checkedId == 3) {
                lay_coptdate.visibility = View.GONE
                et_coopt_Date.setText("")
                lay_revivalDate.visibility = View.VISIBLE
            } else {
                lay_coptdate.visibility = View.GONE
                lay_revivalDate.visibility = View.GONE
            }
        })


        rgIntermediation.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->

            if (checkedId == 1) {
                lay_SavingFrequency.visibility = View.VISIBLE
                lay_compulsorySaving.visibility = View.VISIBLE
                lay_compulsoryROI.visibility = View.VISIBLE
                lay_Volunatry.visibility = View.VISIBLE
                lay_voluntaryROI.visibility = View.GONE
            } else if (checkedId == 0) {
                lay_SavingFrequency.visibility = View.GONE
                lay_compulsorySaving.visibility = View.GONE
                lay_compulsoryROI.visibility = View.GONE
                lay_Volunatry.visibility = View.GONE
                spin_savingfrequency.setSelection(0)
                et_saving.setText("")
                et_ComsavingRoi.setText("")
                validate!!.fillradio(rgvolunteer, 0, dataspin_yesno, this)
                lay_voluntaryROI.visibility = View.GONE
                et_voluntarysavingROI.setText("")
            }

        })
    }

    private fun fillSpinner() {
        dataspin_funding =
            fundingViewModel!!.getfundingAgency(validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!)
        if (cboType == 1) {
            dataspinMember =
                scMemberViewmodel!!.getExecutiveMemberList(
                    validate!!.RetriveSharepreferenceString(
                        AppSP.FedrationGUID
                    )
                )
        } else if (cboType == 2) {
            dataspinMember =
                scMemberViewmodel!!.getCLFExecutiveMemberList(
                    validate!!.RetriveSharepreferenceString(
                        AppSP.FedrationGUID
                    )
                )
        }

        if (cboType == 1) {
            stringMsg = LabelSet.getText(
                "noec_memberavailable",
                R.string.noec_memberavailable
            )
        } else if (cboType == 2) {
            stringMsg = LabelSet.getText(
                "noec_memberavail",
                R.string.noec_memberavail
            )
        }
        validate!!.fillScMemberSpinner(
            this,
            spin_member,
            dataspinMember,
            stringMsg
        )
        dataspin_activity = lookupViewmodel!!.getlookup(
            47,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_status = lookupViewmodel!!.getlookup(
            5,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        datablock = locationViewModel!!.getBlock_data(sStateCode, sDistrictCode)
        validate!!.fillBlockSpinner(this, spin_block, datablock)
        dataPanchayat =
            locationViewModel!!.getPanchayatByPanchayatCode(sStateCode, sDistrictCode, sBlockCode)
        validate!!.fillPanchayatSpinner(this, spin_grampanchayat, dataPanchayat)

        dataVillage = locationViewModel!!.getVillagedata_by_panchayatCode(
            sStateCode,
            sDistrictCode,
            sBlockCode,
            sPanchayatCode
        )
        validate!!.fillVillageSpinner(this, spin_village, dataVillage)

        dataspin_meetingfreq = lookupViewmodel!!.getlookupMasterdata(
            19,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),
            listOf(0, 2, 3)
        )

        dataspin_weekday = lookupViewmodel!!.getlookup(
            34,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )


        dataspin_monthly = lookupViewmodel!!.getlookup(
            15,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_monthlydate = lookupViewmodel!!.getlookup(
            14,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_fortnightdate = lookupViewmodel!!.getlookup(
            14,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_name = lookupViewmodel!!.getlookup(
            1,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_promotedby = lookupViewmodel!!.getlookupfromkeycode(
            "PROMOTEDBY",
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        validate!!.fillFundingspinner(this, spin_FundingAgency, dataspin_funding)
        validate!!.fillspinner(this, spin_savingfrequency, dataspin_meetingfreq)
        validate!!.fillspinner(this, spin_promotedby, dataspin_promotedby)
        validate!!.fillspinner(this, spin_frequency, dataspin_meetingfreq)
        validate!!.fillspinner(this, spin_weekday, dataspin_weekday)
        validate!!.fillspinner(this, spin_monthly, dataspin_monthly)
        validate!!.fillspinner(this, spin_fortnightdate, dataspin_fortnightdate)
        validate!!.fillspinner(this, spin_monthlydate, dataspin_monthlydate)
        validate!!.fillspinner(this, spin_status, dataspin_status)
        validate!!.fillspinner(this, spin_ParentCBO, dataspin_name)
        validate!!.fillspinner(this, spin_primaryActivity, dataspin_activity)
        validate!!.fillspinner(this, spin_secondaryActivity, dataspin_activity)
        validate!!.fillspinner(this, spin_tertiaryActivity, dataspin_activity)

        spin_member.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {
                    dataspin_belongmemberphone =
                        phoneViewmodel!!.getphonedetaillistdata(returnmemguid())
                    fillmemberphoneno(dataspin_belongmemberphone)
                } else {
                    dataspin_belongmemberphone = phoneViewmodel!!.getphonedetaillistdata("")
                    fillmemberphoneno(dataspin_belongmemberphone)

                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }
        et_formationDate.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var promoter = validate!!.returnlookupcode(spin_promotedby, dataspin_promotedby)
                if (validate!!.returnString_DateValue(et_formationDate.text.toString()).length > 0) {

                    if (validate!!.returnString_DateValue(et_revivalDate.text.toString()).length > 0) {
                        if (validate!!.getDate(
                                et_revivalDate.text.toString(),
                                et_formationDate.text.toString()
                            ) == 1
                        ) {
                            validate!!.CustomAlertEditText(
                                LabelSet.getText(
                                    "formation_revival_date",
                                    R.string.formation_revival_date
                                ),
                                this@VoBasicDetailActivity,
                                et_formationDate
                            )
                        }
                    }

                    if (validate!!.returnString_DateValue(et_coopt_Date.text.toString()).length > 0) {
                        if (validate!!.getDate(
                                et_coopt_Date.text.toString(),
                                et_formationDate.text.toString()
                            ) == 1
                        ) {
                            validate!!.CustomAlertEditText(
                                LabelSet.getText(
                                    "formation_copt_date",
                                    R.string.formation_copt_date
                                ),
                                this@VoBasicDetailActivity,
                                et_formationDate
                            )
                        }
                    }
                    if (validate!!.getDate(
                            et_formationDate.text.toString(),
                            initial_dt
                        ) == 1 && promoter == 1
                    ) {
                        validate!!.CustomAlertSpinner(
                            this@VoBasicDetailActivity,
                            spin_promotedby,
                            LabelSet.getText(
                                "nrlm_allowed",
                                R.string.nrlm_allowed
                            ) + initial_dt
                        )
                    }
                    if (validate!!.getDate(
                            et_formationDate.text.toString(),
                            initial_dt
                        ) == 1 || promoter != 1
                    ) {
                        lay_rgrevival.visibility = View.VISIBLE

                    } else {
                        lay_rgrevival.visibility = View.GONE
                        lay_coptdate.visibility = View.GONE
                        lay_revivalDate.visibility = View.GONE
                        et_coopt_Date.setText("")
                        et_revivalDate.setText("")
                    }
                }
            }
        })

        et_revivalDate.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (validate!!.returnString_DateValue(et_formationDate.text.toString()).length > 0 && validate!!.returnString_DateValue(
                        et_revivalDate.text.toString()
                    ).length > 0
                ) {
                    if (validate!!.getDate(
                            et_revivalDate.text.toString(),
                            et_formationDate.text.toString()
                        ) == 1
                    ) {
                        validate!!.CustomAlertEditText(
                            LabelSet.getText(
                                "revival_formation_date",
                                R.string.revival_formation_date
                            ),
                            this@VoBasicDetailActivity,
                            et_revivalDate
                        )
                    }
                }
            }
        })

        et_ComsavingRoi.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                var value = validate!!.returnDoubleValue(et_ComsavingRoi.text.toString())
                if (validate!!.returnDoubleValue(s.toString()) > 0 && value > 24) {
                    var installmentamt = "0"
                    et_ComsavingRoi.setText(installmentamt.toString())
                    validate!!.CustomAlertEditText(
                        LabelSet.getText(
                            "please_enter",
                            R.string.please_enter
                        ) + " " + LabelSet.getText(
                            "interest_less_complusory",
                            R.string.interest_less_complusory
                        ), this@VoBasicDetailActivity,
                        et_ComsavingRoi
                    )
                }
            }
        })

        et_coopt_Date.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (validate!!.returnString_DateValue(et_formationDate.text.toString()).length > 0 && validate!!.returnString_DateValue(
                        et_coopt_Date.text.toString()
                    ).length > 0
                ) {
                    if (validate!!.getDate(
                            et_coopt_Date.text.toString(),
                            et_formationDate.text.toString()
                        ) == 1
                    ) {
                        validate!!.CustomAlertEditText(
                            LabelSet.getText(
                                "coopt_formation_date",
                                R.string.coopt_formation_date
                            ),
                            this@VoBasicDetailActivity,
                            et_revivalDate
                        )
                    }
                }
            }
        })

        spin_promotedby.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {
                    var promoter = validate!!.returnlookupcode(spin_promotedby, dataspin_promotedby)
                    if (validate!!.getDate(
                            et_formationDate.text.toString(),
                            initial_dt
                        ) == 1 && promoter == 1
                    ) {
                        validate!!.CustomAlertSpinner(
                            this@VoBasicDetailActivity,
                            spin_promotedby,
                            LabelSet.getText(
                                "nrlm_allowed",
                                R.string.nrlm_allowed
                            ) + initial_dt
                        )
                    }
                    if (promoter == 2 || promoter == 99) {
                        lay_promoter_name.visibility = View.VISIBLE
                        lay_fundingAgency.visibility = View.GONE
                        spin_FundingAgency.setSelection(0)
                    } else if (promoter == 3) {
                        lay_promoter_name.visibility = View.GONE
                        lay_fundingAgency.visibility = View.VISIBLE
                        et_promoter_name.setText("")
                    } else {
                        lay_promoter_name.visibility = View.GONE
                        lay_fundingAgency.visibility = View.GONE
                        spin_FundingAgency.setSelection(0)
                        et_promoter_name.setText("")
                    }
                    if (promoter != 1) {
                        if (validate!!.getDate(
                                et_formationDate.text.toString(),
                                initial_dt
                            ) == 1 || promoter != 1
                        ) {
                            lay_rgrevival.visibility = View.VISIBLE
                            lay_coptdate.visibility = View.VISIBLE
                            datarevival = lookupViewmodel!!.getlookupnotin(
                                51, 1,
                                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
                            )
                            var checkValue = 0
                            if (rgrevivalid == 0) {
                                checkValue = 2
                            } else {
                                checkValue = rgrevivalid
                            }

                            validate!!.fillradio(
                                rgrevival,
                                checkValue,
                                datarevival,
                                this@VoBasicDetailActivity
                            )
                            //  rgrevivalid=0
                            /*lay_coptdate.visibility = View.VISIBLE
                            lay_revivalDate.visibility = View.VISIBLE*/
                        } else {

                            lay_rgrevival.visibility = View.GONE
                            lay_coptdate.visibility = View.GONE
                            lay_revivalDate.visibility = View.GONE
                            et_coopt_Date.setText("")
                            et_revivalDate.setText("")
                        }
                    } else if (promoter == 1) {
                        lay_rgrevival.visibility = View.VISIBLE
                        datarevival = lookupViewmodel!!.getlookupnotin(
                            51, 2,
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
                        )
                        var checkValue = 0
                        if (rgrevivalid == 0) {
                            checkValue = 1
                        } else {
                            checkValue = rgrevivalid
                        }
                        validate!!.fillradio(
                            rgrevival,
                            checkValue,
                            datarevival,
                            this@VoBasicDetailActivity
                        )
                        if (rgrevivalid == 3) {
                            lay_revivalDate.visibility = View.VISIBLE
                        } else {
                            lay_revivalDate.visibility = View.GONE
                            et_revivalDate.setText("")
                        }
                        lay_coptdate.visibility = View.GONE
                        et_coopt_Date.setText("")

                    }
                } else {
                    lay_rgrevival.visibility = View.GONE
                    lay_coptdate.visibility = View.GONE
                    lay_revivalDate.visibility = View.GONE
                    lay_promoter_name.visibility = View.GONE
                    lay_fundingAgency.visibility = View.GONE
                    spin_FundingAgency.setSelection(0)
                    et_coopt_Date.setText("")
                    et_revivalDate.setText("")
                    et_promoter_name.setText("")
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        spin_FundingAgency.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {
                    var promotorCode =
                        validate!!.returnFundingcode(spin_FundingAgency, dataspin_funding)
                    if (promotorCode == 99) {
                        lay_promoter_name.visibility = View.VISIBLE
                    } else {
                        lay_promoter_name.visibility = View.GONE
                    }

                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        et_electiontenure.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (validate!!.returnStringValue(et_electiontenure.text.toString()).length >= 2 && (validate!!.returnIntegerValue(
                        et_electiontenure.text.toString()
                    ) > 60 || validate!!.returnIntegerValue(et_electiontenure.text.toString()) < 12)
                ) {
                    validate!!.CustomAlertEditText(
                        LabelSet.getText(
                            "valid_election_tenure_12_60",
                            R.string.valid_election_tenure_12_60
                        ),
                        this@VoBasicDetailActivity,
                        et_electiontenure
                    )
                }
            }
        })

        rgBookeeper.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->
            var book_keeper_identified = validate!!.GetAnswerTypeRadioButtonID(rgBookeeper)
            if (book_keeper_identified == 1) {
                lay_spinmember.visibility = View.VISIBLE
                lay_bookkeepername.visibility = View.GONE
                lay_bookkeepermobile.visibility = View.VISIBLE
                tv_aestrix.visibility = View.GONE
                validate!!.fillScMemberSpinner(
                    this,
                    spin_member,
                    dataspinMember,
                    LabelSet.getText(
                        "noec_memberavailable",
                        R.string.noec_memberavailable
                    )
                )
                et_bookkeeper_s_mobile_no.isEnabled = false
            } else if (book_keeper_identified == 2) {
                lay_spinmember.visibility = View.GONE
                lay_bookkeepername.visibility = View.VISIBLE
                lay_bookkeepermobile.visibility = View.VISIBLE
                tv_aestrix.visibility = View.VISIBLE
                et_bookkeeper_s_mobile_no.isEnabled = true
            } else {
                lay_spinmember.visibility = View.GONE
                lay_bookkeepername.visibility = View.GONE
                lay_bookkeepermobile.visibility = View.GONE
                et_bookkeeper_name.setText("")
                et_bookkeeper_s_mobile_no.setText("")
                spin_member.setSelection(0)
                et_bookkeeper_s_mobile_no.setText("")
            }

        })

        spin_frequency?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {
                    var frequency =
                        validate!!.returnlookupcode(spin_frequency, dataspin_meetingfreq)
                    if (frequency == 1) {
                        lay_weekday.visibility = View.VISIBLE
                        lay_monthly.visibility = View.GONE
                        lay_fortnightdate.visibility = View.GONE
                        lay_monthlydate.visibility = View.GONE
                        spin_fortnightdate.setSelection(0)
                        spin_monthly.setSelection(0)
                        spin_monthlydate.setSelection(0)
                        spin_weekday.setSelection(
                            validate!!.returnlookupcodepos(
                                iMeetingOn,
                                dataspin_weekday
                            )
                        )
                    } else if (frequency == 2) {
                        lay_fortnightdate.visibility = View.VISIBLE
                        lay_monthlydate.visibility = View.VISIBLE
                        spin_fortnightdate.setSelection(
                            validate!!.returnlookupcodepos(
                                ifrequencyValue,
                                dataspin_fortnightdate
                            )
                        )
                        spin_monthlydate.setSelection(
                            validate!!.returnlookupcodepos(
                                iMeetingOn,
                                dataspin_monthlydate
                            )
                        )
                        lay_monthly.visibility = View.GONE
                        lay_weekday.visibility = View.GONE
                        spin_monthly.setSelection(0)
                        spin_weekday.setSelection(0)

                    } else if (frequency == 3) {
                        lay_monthly.visibility = View.VISIBLE
                        lay_fortnightdate.visibility = View.GONE
                        lay_monthlydate.visibility = View.GONE
                        lay_weekday.visibility = View.GONE
                        spin_fortnightdate.setSelection(0)
                        spin_monthly.setSelection(0)
                        spin_monthlydate.setSelection(0)
                        spin_weekday.setSelection(0)
                        spin_monthly.setSelection(
                            validate!!.returnlookupcodepos(
                                ifrequencyValue,
                                dataspin_monthly
                            )
                        )
                    }
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        spin_savingfrequency?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {
                    // savingfrequencyid--
                    var frequency =
                        validate!!.returnlookupcode(spin_frequency, dataspin_meetingfreq)
                    if (frequency == 2) {
                        var savingfrequency =
                            validate!!.returnlookupcode(spin_savingfrequency, dataspin_meetingfreq)
                        if (savingfrequency == 1) {
                            validate!!.CustomAlertVO(
                                LabelSet.getText(
                                    "monthlyfortnightly",
                                    R.string.monthlyfortnightly
                                ),
                                this@VoBasicDetailActivity
                            )
                            spin_savingfrequency.setSelection(0)
                            // savingfrequencyid=0
                        }
                    } else if (frequency == 3) {
                        var savingfrequency =
                            validate!!.returnlookupcode(spin_savingfrequency, dataspin_meetingfreq)
                        if (savingfrequency == 1 || savingfrequency == 2) {
                            validate!!.CustomAlertVO(
                                LabelSet.getText(
                                    "yuocanselectmonthaly",
                                    R.string.yuocanselectmonthaly
                                ),
                                this@VoBasicDetailActivity
                            )
                            spin_savingfrequency.setSelection(0)
                            //  savingfrequencyid=0
                        }
                    }
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }


        spin_monthly?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {
                    var monthlyId =
                        validate!!.returnlookupcode(spin_monthly, dataspin_monthly)
                    if (monthlyId == 1 || monthlyId == 2 || monthlyId == 3 || monthlyId == 4 || monthlyId == 5) {
                        lay_weekday.visibility = View.VISIBLE
                        lay_fortnightdate.visibility = View.GONE
                        lay_monthlydate.visibility = View.GONE
                        spin_fortnightdate.setSelection(0)
                        spin_monthlydate.setSelection(0)
                        spin_weekday.setSelection(
                            validate!!.returnlookupcodepos(
                                iMeetingOn,
                                dataspin_weekday
                            )
                        )
                    } else if (monthlyId == 6) {
                        lay_monthlydate.visibility = View.VISIBLE
                        lay_weekday.visibility = View.GONE
                        lay_fortnightdate.visibility = View.GONE
                        spin_fortnightdate.setSelection(0)
                        spin_weekday.setSelection(0)
                        spin_monthlydate.setSelection(
                            validate!!.returnlookupcodepos(
                                iMeetingOn,
                                dataspin_monthlydate
                            )
                        )
                    }
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        rgvolunteer.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->

            if (checkedId == 1) {
                lay_voluntaryROI.visibility = View.VISIBLE
            } else {
                lay_voluntaryROI.visibility = View.GONE
                et_voluntarysavingROI.setText("")
            }

        })

        spin_primaryActivity?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {
                    spin_primaryActivity.isEnabled = true
                    var p_activity =
                        validate!!.returnlookupcode(spin_primaryActivity, dataspin_activity)
                    if (p_activity == validate!!.returnlookupcode(
                            spin_secondaryActivity,
                            dataspin_activity
                        )
                    ) {
                        validate!!.CustomAlertSpinner(
                            this@VoBasicDetailActivity,
                            spin_primaryActivity,
                            LabelSet.getText(
                                "primary_secondary_activity",
                                R.string.primary_secondary_activity
                            )
                        )
                    } else if (p_activity == validate!!.returnlookupcode(
                            spin_tertiaryActivity,
                            dataspin_activity
                        )
                    ) {
                        validate!!.CustomAlertSpinner(
                            this@VoBasicDetailActivity,
                            spin_primaryActivity,
                            LabelSet.getText(
                                "primary_tertiary_activity",
                                R.string.primary_tertiary_activity
                            )
                        )
                    }
                } else {
                    spin_secondaryActivity.isEnabled = false
                    spin_tertiaryActivity.isEnabled = false
                    spin_secondaryActivity.setSelection(0)
                    spin_tertiaryActivity.setSelection(0)
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        spin_secondaryActivity?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {
                    spin_tertiaryActivity.isEnabled = true
                    var s_activity =
                        validate!!.returnlookupcode(spin_secondaryActivity, dataspin_activity)
                    if (s_activity == validate!!.returnlookupcode(
                            spin_primaryActivity,
                            dataspin_activity
                        )
                    ) {
                        validate!!.CustomAlertSpinner(
                            this@VoBasicDetailActivity,
                            spin_secondaryActivity,
                            LabelSet.getText(
                                "secondary_primary_activity",
                                R.string.secondary_primary_activity
                            )
                        )
                    } else if (s_activity == validate!!.returnlookupcode(
                            spin_tertiaryActivity,
                            dataspin_activity
                        )
                    ) {
                        validate!!.CustomAlertSpinner(
                            this@VoBasicDetailActivity,
                            spin_secondaryActivity,
                            LabelSet.getText(
                                "secondary_tertiary_activity",
                                R.string.secondary_tertiary_activity
                            )
                        )
                    }
                } else {
                    spin_tertiaryActivity.isEnabled = false
                    spin_tertiaryActivity.setSelection(0)
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        spin_tertiaryActivity?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {
                    var t_activity =
                        validate!!.returnlookupcode(spin_tertiaryActivity, dataspin_activity)
                    if (t_activity == validate!!.returnlookupcode(
                            spin_primaryActivity,
                            dataspin_activity
                        )
                    ) {
                        validate!!.CustomAlertSpinner(
                            this@VoBasicDetailActivity,
                            spin_tertiaryActivity,
                            LabelSet.getText(
                                "tertiary_primary_activity",
                                R.string.tertiary_primary_activity
                            )
                        )
                    } else if (t_activity == validate!!.returnlookupcode(
                            spin_secondaryActivity,
                            dataspin_activity
                        )
                    ) {
                        validate!!.CustomAlertSpinner(
                            this@VoBasicDetailActivity,
                            spin_tertiaryActivity,
                            LabelSet.getText(
                                "tertiary_secondary_activity",
                                R.string.tertiary_secondary_activity
                            )
                        )
                    }
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }
    }

    private fun showData() {
        var listData =
            federationViewmodel!!.getFedrationdata(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))

        spin_block.setSelection(validate!!.returnBlockpos(sBlockCode, datablock))
        if (!listData.isNullOrEmpty()) {

            spin_grampanchayat.setSelection(
                validate!!.returnPanchayatpos(
                    validate!!.returnIntegerValue(
                        listData.get(0).panchayat_id.toString()
                    ), dataPanchayat
                )
            )
            if (validate!!.returnIntegerValue(listData.get(0).activation_status.toString()) == 0) {
                activationStatus = 1
            } else {
                activationStatus =
                    validate!!.returnIntegerValue(listData.get(0).activation_status.toString())
            }

            spin_grampanchayat.isEnabled = false
            spin_village.isEnabled = false
            spin_block.isEnabled = false
            sVillageCode = validate!!.returnIntegerValue(listData.get(0).village_id.toString())
            et_shgcode.setText(validate!!.returnStringValue(listData.get(0).federation_code.toString()))
            et_voname.setText(validate!!.returnStringValue(listData.get(0).federation_name))
            et_vonamelocal.setText(validate!!.returnStringValue(listData.get(0).federation_name_local))
            et_formationDate.setText(
                validate!!.convertDatetime(
                    validate!!.returnLongValue(
                        listData.get(
                            0
                        ).federation_formation_date.toString()
                    )
                )
            )
            spin_promotedby.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(listData.get(0).promoted_by.toString()),
                    dataspin_promotedby
                )
            )
            isCompleteDb = validate!!.returnIntegerValue(listData.get(0).is_complete.toString())
            spin_FundingAgency.setSelection(
                validate!!.returnFundingpos(
                    validate!!.returnIntegerValue(
                        listData.get(0).promoter_code.toString()
                    ), dataspin_funding
                )
            )
            et_promoter_name.setText(validate!!.returnStringValue(listData.get(0).promoter_name))
            et_revivalDate.setText(
                validate!!.convertDatetime(
                    validate!!.returnLongValue(
                        listData.get(
                            0
                        ).federation_revival_date.toString()
                    )
                )
            )
            et_coopt_Date.setText(
                validate!!.convertDatetime(
                    validate!!.returnLongValue(
                        listData.get(
                            0
                        ).cooption_date.toString()
                    )
                )
            )
            if (validate!!.returnLongValue(
                    listData.get(0).cooption_date.toString()
                ) > 0L
            ) {
                validate!!.fillradio(rgrevival, 2, datarevival, this)
                rgrevivalid = 2
                lay_coptdate.visibility = View.VISIBLE
                lay_revivalDate.visibility = View.GONE
            } else if (validate!!.returnLongValue(
                    listData.get(0).federation_revival_date.toString()
                ) > 0L
            ) {
                validate!!.fillradio(rgrevival, 3, datarevival, this)
                rgrevivalid = 3
                lay_coptdate.visibility = View.GONE
                lay_revivalDate.visibility = View.VISIBLE
            } else {
                validate!!.fillradio(rgrevival, 1, datarevival, this)
                rgrevivalid = 1
                lay_coptdate.visibility = View.GONE
                lay_revivalDate.visibility = View.GONE
            }

            spin_frequency.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(listData.get(0).meeting_frequency.toString()),
                    dataspin_meetingfreq
                )
            )

            spin_status.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(listData.get(0).status.toString()),
                    dataspin_status
                )
            )

            iMeetingOn = validate!!.returnIntegerValue(listData.get(0).meeting_on.toString())
            ifrequencyValue =
                validate!!.returnIntegerValue(listData.get(0).meeting_frequency_value.toString())
            spin_savingfrequency.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(listData.get(0).saving_frequency.toString()),
                    dataspin_meetingfreq
                )
            )
            et_saving.setText(validate!!.returnStringValue(listData.get(0).month_comp_saving.toString()))
            /*   spin_primaryActivity.setSelection(
                   validate!!.returnlookupcodepos(
                       listData.get(0).primary_activity, dataspin_activity
                   )
               )

               spin_secondaryActivity.setSelection(
                   validate!!.returnlookupcodepos(
                       listData.get(0).secondary_activity, dataspin_activity
                   )
               )

               spin_tertiaryActivity.setSelection(
                   validate!!.returnlookupcodepos(
                       listData.get(0).tertiary_activity, dataspin_activity
                   )
               )*/
            validate!!.fillradio(
                rgvolunteer,
                validate!!.returnIntegerValue(listData.get(0).is_voluntary_saving.toString()),
                dataspin_yesno,
                this
            )

            validate!!.fillradio(
                rgBookeeper,
                validate!!.returnIntegerValue(listData.get(0).bookkeeper_identified.toString()),
                databookkeeper,
                this
            )
            if (validate!!.returnIntegerValue(listData.get(0).bookkeeper_identified.toString()) == 1) {
                spin_member.setSelection(
                    setMemberName(
                        validate!!.returnStringValue(listData.get(0).bookkeeper_name),
                        dataspinMember
                    )
                )
            } else {
                et_bookkeeper_name.setText(validate!!.returnStringValue(listData.get(0).bookkeeper_name))
            }
            et_ComsavingRoi.setText(validate!!.returnStringValue(listData.get(0).savings_interest.toString()))

            et_bookkeeper_s_mobile_no.setText(validate!!.returnStringValue(listData.get(0).bookkeeper_mobile))
            et_electiontenure.setText(validate!!.returnStringValue(listData.get(0).election_tenure.toString()))

            spin_ParentCBO.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(
                        listData.get(0).parent_cbo_code.toString()
                    ), dataspin_name
                )
            )
            validate!!.fillradioFinancial(
                rgIntermediation,
                validate!!.returnIntegerValue(listData.get(0).is_financial_intermediation.toString()),
                dataspin_yesno, this, cboType
            )
            if (listData.get(0).is_financial_intermediation == 1) {
                lay_SavingFrequency.visibility = View.VISIBLE
                lay_compulsorySaving.visibility = View.VISIBLE
                lay_compulsoryROI.visibility = View.VISIBLE
                lay_Volunatry.visibility = View.VISIBLE
                lay_voluntaryROI.visibility = View.GONE
                et_voluntarysavingROI.setText("")
            } else if (listData.get(0).is_financial_intermediation == 0) {
                lay_SavingFrequency.visibility = View.GONE
                lay_compulsorySaving.visibility = View.GONE
                lay_compulsoryROI.visibility = View.GONE
                lay_Volunatry.visibility = View.GONE
                lay_voluntaryROI.visibility = View.GONE
                spin_savingfrequency.setSelection(0)
                et_saving.setText("")
                et_ComsavingRoi.setText("")
                validate!!.fillradio(rgvolunteer, 0, dataspin_yesno, this)
                et_voluntarysavingROI.setText("")
            }
            if (listData.get(0).is_voluntary_saving == 1) {
                lay_voluntaryROI.visibility = View.VISIBLE
            } else {
                lay_voluntaryROI.visibility = View.GONE
                et_voluntarysavingROI.setText("")
            }
            et_voluntarysavingROI.setText(validate!!.returnStringValue(listData.get(0).voluntary_savings_interest.toString()))
            et_memberCBO.setText(validate!!.returnStringValue(listData.get(0).meber_cbo_count.toString()))
            val mediaStorageDirectory =  File(
                getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
                AppSP.IMAGE_DIRECTORY_NAME
            )
            imageName = validate!!.returnStringValue(listData.get(0).federation_resolution)
            if (imageName.contains("pdf")) {
                Imgshow.setImageResource(R.drawable.pdf)
            } else {
                var mediaFile1 =
                    File(mediaStorageDirectory.path + File.separator + imageName)
                Picasso.with(this).load(mediaFile1).into(Imgshow)
            }
            lay_status.visibility = View.VISIBLE
        } else {
            lay_status.visibility = View.GONE
            spin_grampanchayat.setSelection(
                validate!!.returnPanchayatpos(
                    validate!!.RetriveSharepreferenceInt(
                        AppSP.panchayatcode
                    ).toInt(), dataPanchayat
                )
            )
            sVillageCode = validate!!.RetriveSharepreferenceInt(AppSP.villagecode)
            lay_SavingFrequency.visibility = View.GONE
            lay_compulsorySaving.visibility = View.GONE
            lay_compulsoryROI.visibility = View.GONE
            lay_Volunatry.visibility = View.GONE
            lay_voluntaryROI.visibility = View.GONE
            spin_savingfrequency.setSelection(0)
            et_saving.setText("")
            et_ComsavingRoi.setText("")
            validate!!.fillradio(rgvolunteer, 0, dataspin_yesno, this)
            if (cboType == 1) {
                validate!!.fillradioFinancial(rgIntermediation, 0, dataspin_yesno, this, cboType)

            } else if (cboType == 2) {
                validate!!.fillradioFinancial(rgIntermediation, 1, dataspin_yesno, this, cboType)

            }

            et_voluntarysavingROI.setText("")
        }

    }

    private fun SaveBasicDetail(iAutoSave: Int) {
        if (checkData() == 0) {
            var promotor_name = ""
            var promotor_code = 0
            if (lay_promoter_name.visibility == View.VISIBLE) {
                promotor_name = validate!!.returnStringValue(et_promoter_name.text.toString())
            } else {
                promotor_name = ""
            }
            if (lay_fundingAgency.visibility == View.VISIBLE) {
                promotor_code = validate!!.returnFundingcode(spin_FundingAgency, dataspin_funding)
            } else {
                promotor_code = 0
            }
            guid = validate!!.random()
            var isEdited = 0

            validate!!.SaveSharepreferenceInt(AppSP.isEdited, isEdited)
            var iVolunteerId: Int? = null
            if (lay_Volunatry.visibility == View.VISIBLE) {
                iVolunteerId = rgvolunteer.checkedRadioButtonId
            } else {
                iVolunteerId = null
            }
            var panchayatId: Int? = null
            //   guid = "4jxx9fNxyoZQjzNDf4FAEqKL7vd2vA2021013016095172"
            //    var villageId = validate!!.returnVillageID(spin_village, dataVillage)
            if (cboType == 1) {
                panchayatId = validate!!.returnPanchayatID(spin_grampanchayat, dataPanchayat)
            } else if (cboType == 2) {
                panchayatId = null
            }
            var meetingfrequency = validate!!.returnlookupcode(spin_frequency, dataspin_meetingfreq)
            var meetingfrequencyvalue = 0
            var meetingon = 0
            var book_keeper_name = ""
            if (rgBookeeper.checkedRadioButtonId == 1) {
                book_keeper_name = returnMemberName()
            } else {
                book_keeper_name = et_bookkeeper_name.text.toString()
            }
            if (lay_fortnightdate.visibility == View.VISIBLE) {
                meetingfrequencyvalue =
                    validate!!.returnlookupcode(spin_fortnightdate, dataspin_fortnightdate)
            } else if (lay_monthly.visibility == View.VISIBLE) {
                meetingfrequencyvalue = validate!!.returnlookupcode(spin_monthly, dataspin_monthly)
            }
            if (lay_weekday.visibility == View.VISIBLE) {
                meetingon = validate!!.returnlookupcode(spin_weekday, dataspin_weekday)
            } else if (lay_monthlydate.visibility == View.VISIBLE) {
                meetingon = validate!!.returnlookupcode(spin_monthlydate, dataspin_monthlydate)
            }
            var federation = validate!!.returnlookupcode(spin_ParentCBO, dataspin_name)
            voName = et_voname.text.toString()
            var pramotedby = validate!!.returnlookupcode(spin_promotedby, dataspin_promotedby)
            var priactivity = validate!!.returnlookupcode(spin_primaryActivity, dataspin_activity)
            var secactivity = validate!!.returnlookupcode(spin_secondaryActivity, dataspin_activity)
            var teractivity = validate!!.returnlookupcode(spin_tertiaryActivity, dataspin_activity)

            if (validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID).isNullOrEmpty()) {
                fedrationEntity = FederationEntity(
                    0,
                    0,
                    0,
                    guid,
                    sStateCode,
                    sDistrictCode,
                    sBlockCode,
                    null,
                    panchayatId,
                    voName,
                    cboType,
                    cboType,
                    childLevel,
                    0,
                    "",
                    validate!!.returnStringValue(et_vonamelocal.text.toString()),
                    "",
                    validate!!.Daybetweentime(et_formationDate.text.toString()),
                    validate!!.Daybetweentime(et_revivalDate.text.toString()),
                    pramotedby,
                    promotor_code,
                    promotor_name,
                    meetingfrequency,
                    meetingfrequencyvalue,
                    meetingon,
                    0,
                    validate!!.returnIntegerValue(et_saving.text.toString()),
                    0,
                    0,
                    0,
                    validate!!.returnlookupcode(spin_savingfrequency, dataspin_meetingfreq),
                    iVolunteerId,
                    validate!!.returnDoubleValue(et_ComsavingRoi.text.toString()),
                    validate!!.returnDoubleValue(et_voluntarysavingROI.text.toString()),
                    priactivity,
                    secactivity,
                    teractivity,
                    rgBookeeper.checkedRadioButtonId,
                    book_keeper_name,
                    et_bookkeeper_s_mobile_no.text.toString(),
                    validate!!.returnIntegerValue(et_electiontenure.text.toString()),
                    1,
                    1,
                    "",
                    0,
                    validate!!.RetriveSharepreferenceString(AppSP.userid),
                    1,
                    1,
                    1,
                    -1,
                    0,
                    "",
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    validate!!.RetriveSharepreferenceString(AppSP.userid),
                    0,
                    "",
                    rgIntermediation.checkedRadioButtonId,
                    validate!!.returnIntegerValue(et_memberCBO.text.toString()),
                    0,
                    validate!!.Daybetweentime(et_coopt_Date.text.toString()),
                    0,
                    1,
                    1,
                    imageName
                )


                generateFederationId(guid, iAutoSave, fedrationEntity!!, voName)
            } else {
                isEdited = setFederationEditFlag()
                federationViewmodel!!.updateBasicDetail(
                    validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!,
                    panchayatId,
                    null,
                    et_voname.text.toString(),
                    "",
                    validate!!.returnStringValue(et_vonamelocal.text.toString()),
                    "",
                    validate!!.Daybetweentime(et_formationDate.text.toString()),
                    validate!!.Daybetweentime(et_revivalDate.text.toString()),
                    meetingfrequency,
                    meetingfrequencyvalue,
                    meetingon,
                    validate!!.returnIntegerValue(et_saving.text.toString()),
                    0,
                    federation,
                    0,
                    1,
                    1,
                    1,
                    activationStatus,
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                    validate!!.returnlookupcode(spin_savingfrequency, dataspin_meetingfreq),
                    iVolunteerId,
                    validate!!.returnDoubleValue(et_ComsavingRoi.text.toString()),
                    validate!!.returnDoubleValue(et_voluntarysavingROI.text.toString()),
                    priactivity,
                    secactivity,
                    teractivity,
                    rgBookeeper.checkedRadioButtonId,
                    book_keeper_name,
                    et_bookkeeper_s_mobile_no.text.toString(),
                    validate!!.returnIntegerValue(et_electiontenure.text.toString()),
                    validate!!.returnlookupcode(spin_status, dataspin_status),
                    rgIntermediation.checkedRadioButtonId,
                    validate!!.returnIntegerValue(et_memberCBO.text.toString()),
                    validate!!.Daybetweentime(et_coopt_Date.text.toString()),
                    pramotedby,
                    promotor_code,
                    promotor_name,
                    validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                    1,
                    isEdited,
                    1,
                    imageName
                )
                var image1 = ImageuploadEntity(
                    imageName,
                    validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID),
                    "",
                    1,
                    0
                )
                if (imageName.isNotEmpty() && newimageName) {
                    imageUploadViewmodel!!.insert(image1)
                }
                if (iAutoSave == 0) {
                    if (cboType == 1) {
                        validate!!.CustomAlertVO(
                            LabelSet.getText(
                                "updated_successfully",
                                R.string.updated_successfully
                            ),
                            this,
                            VOMapCBOActivity::class.java
                        )

                    } else {
                        validate!!.CustomAlertVO(
                            LabelSet.getText(
                                "updated_successfully",
                                R.string.updated_successfully
                            ),
                            this,
                            CLFMapCBOActivity::class.java
                        )
                    }
                }

            }
        } else {
            if (iAutoSave == 0) {
                if (cboType == 1) {
                    validate!!.CustomAlertVO(
                        LabelSet.getText(
                            "updated_successfully",
                            R.string.updated_successfully
                        ),
                        this,
                        VOMapCBOActivity::class.java
                    )

                } else {
                    validate!!.CustomAlertVO(
                        LabelSet.getText(
                            "updated_successfully",
                            R.string.updated_successfully
                        ),
                        this,
                        CLFMapCBOActivity::class.java
                    )
                }
            }
        }
    }

    fun checkValidation(partial: Int): Int? {
        var value = 1
        val monthlyId = validate!!.returnlookupcode(spin_monthly, dataspin_monthly)
        var pramotedby = validate!!.returnlookupcode(spin_promotedby, dataspin_promotedby)

        if (lay_block.visibility == View.VISIBLE && spin_block.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_block,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText("block", R.string.block)
            )
            value = 0
            return value
        } else if (lay_grampanchayat.visibility == View.VISIBLE && spin_grampanchayat.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_grampanchayat,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "grampanchayat",
                    R.string.grampanchayat
                )
            )
            value = 0
            return value
        } else if (lay_village.visibility == View.VISIBLE && spin_village.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_village,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "village",
                    R.string.village
                )
            )
            value = 0
            return value
        } else if (et_voname.text.toString().trim().length == 0) {
            if (cboType == 1) {
                validate!!.CustomAlertEditText(
                    LabelSet.getText(
                        "please_enter",
                        R.string.please_enter
                    ) + " " + LabelSet.getText(
                        "vo_name",
                        R.string.vo_name
                    ), this, et_voname
                )
            } else {
                validate!!.CustomAlertEditText(
                    LabelSet.getText(
                        "please_enter",
                        R.string.please_enter
                    ) + " " + LabelSet.getText(
                        "clf_name",
                        R.string.clf_name
                    ), this, et_voname
                )
            }
            value = 0
            return value
        } else if (!validate!!.checkname(et_voname.text.toString())) {
            if (cboType == 1) {
                validate!!.CustomAlertEditText(
                    LabelSet.getText(
                        "please_enter",
                        R.string.please_enter
                    ) + " " + LabelSet.getText(
                        "vo_name",
                        R.string.vo_name
                    ), this, et_voname
                )
            } else {
                validate!!.CustomAlertEditText(
                    LabelSet.getText(
                        "please_enter",
                        R.string.please_enter
                    ) + " " + LabelSet.getText(
                        "clf_name",
                        R.string.clf_name
                    ), this, et_voname
                )
            }
            value = 0
            return value
        } else if (et_formationDate.text.toString().trim().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "formation_date",
                    R.string.formation_date
                ),
                this,
                et_formationDate
            )
            value = 0
            return value
        } else if (spin_promotedby.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_promotedby,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "promoted_by",
                    R.string.promoted_by
                )
            )
            value = 0
            return value
        } else if (spin_promotedby.selectedItemPosition == 3 && spin_FundingAgency.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_FundingAgency,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "promoters_name",
                    R.string.promoters_name
                )
            )
            value = 0
            return value
        } else if (lay_promoter_name.visibility == View.VISIBLE && validate!!.returnStringValue(
                et_promoter_name.text.toString()
            ).trim().length == 0
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "promoters_name",
                    R.string.promoters_name
                ),
                this,
                et_promoter_name
            )
            value = 0
            return value
        } else if (pramotedby == 1 && rgrevival.checkedRadioButtonId == 2) {
            validate!!.CustomAlertRadio(
                this,
                LabelSet.getText(
                    "new_can_not_be_selected",
                    R.string.new_can_not_be_selected
                ), rgrevival

            )
            value = 0
            return value
        } else if (lay_revivalDate.visibility == View.VISIBLE && validate!!.returnStringValue(
                et_revivalDate.text.toString()
            ).trim().length == 0
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "revival_date",
                    R.string.revival_date
                ),
                this,
                et_revivalDate
            )
            value = 0
            return value
        } else if (lay_coptdate.visibility == View.VISIBLE && validate!!.returnStringValue(
                et_coopt_Date.text.toString()
            ).trim().length == 0
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "co_optdate",
                    R.string.co_optdate
                ),
                this,
                et_coopt_Date
            )
            value = 0
            return value
        } else if (validate!!.returnlookupcode(
                spin_frequency,
                dataspin_meetingfreq
            ) == 1 && spin_weekday.selectedItemPosition == 0
        ) {
            validate!!.CustomAlertSpinner(
                this,
                spin_weekday,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText("day", R.string.day)
            )
            value = 0
            return value
        } else if (validate!!.returnlookupcode(spin_frequency, dataspin_meetingfreq) == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_frequency,
                LabelSet.getText(
                    "please_select_meeting_frequency",
                    R.string.please_select_meeting_frequency
                )
            )
            value = 0
            return value
        } else if (validate!!.returnlookupcode(
                spin_frequency,
                dataspin_meetingfreq
            ) == 2 && spin_fortnightdate.selectedItemPosition == 0
        ) {
            validate!!.CustomAlertSpinner(
                this,
                spin_fortnightdate,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText("date", R.string.date)
            )
            value = 0
            return value
        } else if (validate!!.returnlookupcode(
                spin_frequency,
                dataspin_meetingfreq
            ) == 3 && spin_monthly.selectedItemPosition == 0
        ) {
            validate!!.CustomAlertSpinner(
                this,
                spin_monthly,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "monthly_day",
                    R.string.monthly_day
                )
            )
            value = 0
            return value
        } else if (validate!!.returnlookupcode(
                spin_frequency,
                dataspin_meetingfreq
            ) == 3 && (monthlyId == 1 || monthlyId == 2 || monthlyId == 3 || monthlyId == 4) && spin_weekday.selectedItemPosition == 0
        ) {
            validate!!.CustomAlertSpinner(
                this,
                spin_weekday,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText("day", R.string.day)
            )
            value = 0
            return value
        } else if (validate!!.returnlookupcode(
                spin_frequency,
                dataspin_meetingfreq
            ) == 3 && monthlyId == 6 && spin_monthlydate.selectedItemPosition == 0
        ) {
            validate!!.CustomAlertSpinner(
                this,
                spin_monthlydate,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText("date", R.string.date)
            )
            value = 0
            return value
        } else if (rgIntermediation.checkedRadioButtonId == 1 && spin_savingfrequency.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_savingfrequency, LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText("saving_frequency", R.string.saving_frequency)
            )
            value = 0
            return value
        } else if (rgIntermediation.checkedRadioButtonId == 1 && validate!!.returnIntegerValue(
                et_saving.text.toString()
            ) == 0
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "cumpulsory_saving",
                    R.string.cumpulsory_saving
                ),
                this,
                et_saving
            )
            value = 0
            return value
        } else if (validate!!.returnlookupcode(
                spin_frequency,
                dataspin_meetingfreq
            ) == 2 && validate!!.returnIntegerValue(et_saving.text.toString()) > 1000
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "weekly_less_complusory_saving",
                    R.string.weekly_less_complusory_saving
                ),
                this,
                et_saving
            )
            value = 0
            return value
        } else if (validate!!.returnlookupcode(
                spin_frequency,
                dataspin_meetingfreq
            ) == 3 && validate!!.returnIntegerValue(et_saving.text.toString()) > 2000
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "less_complusory_saving",
                    R.string.less_complusory_saving
                ),
                this,
                et_saving
            )
            value = 0
            return value
        } else if (validate!!.returnDoubleValue(et_ComsavingRoi.text.toString()) > 0 && validate!!.returnDoubleValue(
                et_ComsavingRoi.text.toString()
            ) > 24
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "CompulsorySavingroi",
                    R.string.CompulsorySavingroi
                ),
                this,
                et_ComsavingRoi
            )
            value = 0
            return value
        } else if (lay_voluntaryROI.visibility == View.VISIBLE && validate!!.returnDoubleValue(
                et_voluntarysavingROI.text.toString()
            ) > 0 && validate!!.returnDoubleValue(
                et_voluntarysavingROI.text.toString()
            ) > 24
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "VoluntarySavingroi",
                    R.string.VoluntarySavingroi
                ),
                this,
                et_voluntarysavingROI
            )
            value = 0
            return value
        } else if (lay_bookkeepername.visibility == View.VISIBLE && et_bookkeeper_name.text.toString()
                .trim().length == 0
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "bookkeeper_name",
                    R.string.bookkeeper_name
                ),
                this,
                et_bookkeeper_name
            )
            value = 0
            return value
        } else if (rgBookeeper.checkedRadioButtonId == 2 && et_bookkeeper_s_mobile_no.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "bookkeeper_s_mobile_no",
                    R.string.bookkeeper_s_mobile_no
                ),
                this,
                et_bookkeeper_s_mobile_no
            )
            value = 0
            return value
        } else if (et_bookkeeper_s_mobile_no.text.toString().length > 0 && validate!!.checkmobileno(
                et_bookkeeper_s_mobile_no.text.toString()
            ) == 0 && lay_bookkeepermobile.visibility == View.VISIBLE
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "bookkeeper_s_mobile_no",
                    R.string.bookkeeper_s_mobile_no
                ),
                this,
                et_bookkeeper_s_mobile_no
            )
            value = 0
            return value
        } else if (lay_status.visibility == View.VISIBLE && spin_status.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_status,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "status",
                    R.string.status
                )
            )
            value = 0
            return value
        } else if (validate!!.returnStringValue(et_electiontenure.text.toString())
                .trim().length == 0
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "valid_election_tenure_12_60",
                    R.string.valid_election_tenure_12_60
                ),
                this,
                et_electiontenure
            )
            value = 0
            return value
        } else if (validate!!.returnIntegerValue(et_electiontenure.text.toString()) > 0 && validate!!.returnIntegerValue(
                et_electiontenure.text.toString()
            ) < 12 || validate!!.returnIntegerValue(et_electiontenure.text.toString()) > 60
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "valid_election_tenure_12_60",
                    R.string.valid_election_tenure_12_60
                ),
                this,
                et_electiontenure
            )
            value = 0
            return value
        }

        return value
    }

    private fun setLabelText() {
        tv_grampanchayat.text = LabelSet.getText(
            "grampanchayat",
            R.string.grampanchayat
        )
        tvVillage.text = LabelSet.getText(
            "village1",
            R.string.village1
        )
        if (cboType == 2) {
            tvVoCode.text = LabelSet.getText(
                "clf_code",
                R.string.clf_code
            )
            tvVOName.text = LabelSet.getText(
                "clf_name",
                R.string.clf_name
            )
            tvVONamelocal.text = LabelSet.getText(
                "clf_namelocal",
                R.string.clf_namelocal
            )
            tvPaasbookPage.text = LabelSet.getText(
                "clf_resolution_copy",
                R.string.clf_resolution_copy
            )

        } else {
            tvVoCode.text = LabelSet.getText(
                "voor",
                R.string.voor
            )
            tvVOName.text = LabelSet.getText(
                "vo_name",
                R.string.vo_name
            )
            tvVONamelocal.text = LabelSet.getText(
                "vo_namelocal",
                R.string.vo_namelocal
            )
            tvPaasbookPage.text = LabelSet.getText(
                "vo_resolution_copy",
                R.string.vo_resolution_copy
            )
        }

        et_shgcode.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )

        et_voname.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_formationdate.text = LabelSet.getText(
            "formation_date",
            R.string.formation_date
        )
        et_formationDate.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )
        tvPromotedBy.text = LabelSet.getText(
            "promoted_by",
            R.string.promoted_by
        )
        tv_promotedName.text = LabelSet.getText(
            "promoters_name",
            R.string.promoters_name
        )
        et_promoter_name.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_copt_date.text = LabelSet.getText(
            "co_optdate",
            R.string.co_optdate
        )
        et_coopt_Date.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )
        tv_revivaldate.text = LabelSet.getText(
            "revival_date",
            R.string.revival_date
        )
        et_revivalDate.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )
        tv_meetingFrequency.text = LabelSet.getText(
            "meeting_frequency",
            R.string.meeting_frequency
        )
        tv_monthlyday.text = LabelSet.getText(
            "monthly_day",
            R.string.monthly_day
        )
        tvDay.text = LabelSet.getText("day", R.string.day)
        tvDate.text = LabelSet.getText("date", R.string.date)
        tvDate1.text = LabelSet.getText("date", R.string.date)
        tvFinancial.text = LabelSet.getText(
            "financial_intermediation",
            R.string.financial_intermediation
        )
        tvSavingFrequency.text = LabelSet.getText(
            "saving_frequency",
            R.string.saving_frequency
        )
        tvCompulsory.text = LabelSet.getText(
            "cumpulsory_saving",
            R.string.cumpulsory_saving
        )
        et_saving.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tvcompulsoryRoi.text = LabelSet.getText(
            "compulsory_roi",
            R.string.compulsory_roi
        )
        et_ComsavingRoi.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tvVoluntarySaving.text = LabelSet.getText(
            "voluntary_saving",
            R.string.voluntary_saving
        )
        tvVoluntarySavingRoi.text = LabelSet.getText(
            "voluntary_saving_roi",
            R.string.voluntary_saving_roi
        )
        et_voluntarysavingROI.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tvParentCBO.text = LabelSet.getText(
            "parent_cbo",
            R.string.parent_cbo
        )
        tvMemberCBOCount.text = LabelSet.getText(
            "member_cbo_count",
            R.string.member_cbo_count
        )
        et_memberCBO.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_bookkeeper.text = LabelSet.getText(
            "bookkeeper_identified",
            R.string.bookkeeper_identified
        )
        tv_bookkeeperName.text = LabelSet.getText(
            "bookkeeper_identified",
            R.string.bookkeeper_name
        )
        tvBook.text = LabelSet.getText(
            "bookkeeper_name",
            R.string.bookkeeper_name
        )
        et_bookkeeper_name.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tvBookKeeperMob.text = LabelSet.getText(
            "bookkeeper_s_mobile_no",
            R.string.bookkeeper_s_mobile_no
        )
        et_bookkeeper_s_mobile_no.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tvSecondaryActivity.text = LabelSet.getText(
            "secondary_activity",
            R.string.secondary_activity
        )
        tvtertiaryactivity.text = LabelSet.getText(
            "tertiaryactivity",
            R.string.tertiaryactivity
        )
        tvElection.text = LabelSet.getText(
            "election_tenure",
            R.string.election_tenure
        )
        et_electiontenure.hint = LabelSet.getText(
            "valid_tenure_12_60",
            R.string.valid_tenure_12_60
        )
        tvStatus.hint = LabelSet.getText(
            "status",
            R.string.status
        )
        tvMapVillage.hint = LabelSet.getText(
            "map_villages",
            R.string.map_villages
        )
        btn_save.hint = LabelSet.getText("save", R.string.save)
        btn_savegray.text = LabelSet.getText(
            "save",
            R.string.save
        )
        btn_savegray.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
        tvStatus.text = LabelSet.getText(
            "status",
            R.string.status
        )
    }

    override fun onBackPressed() {
        var intent = Intent(this, VOListActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()

    }

    fun returnMemberName(): String {

        var pos = spin_member.selectedItemPosition
        var id = ""

        if (!dataspinMember.isNullOrEmpty()) {
            if (pos > 0) id = dataspinMember!!.get(pos - 1).member_name!!
        }
        return id
    }

    fun returnmemguid(): String {

        var pos = spin_member.selectedItemPosition
        var id = ""

        if (!dataspinMember.isNullOrEmpty()) {
            if (pos > 0) id = dataspinMember!!.get(pos - 1).member_guid!!
        }
        return id
    }

    fun setMemberName(memberName: String, data: List<EcScModelJoindata>?): Int {


        var pos = 0

        if (!data.isNullOrEmpty()) {
            for (i in data.indices) {
                if (memberName.equals(data.get(i).member_name))
                    pos = i + 1
            }
        }
        return pos
    }

    private fun checkPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        val result1 =
            ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val result2 =
            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission(requestCode: Int) {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ),
            requestCode
        )
    }

    fun captureimage(code: Int) {
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
                m.invoke(null)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//        fileUri = getOutputMediaFileUri(MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE, code)
        fileUri = FileProvider.getUriForFile(
            this, BuildConfig.APPLICATION_ID + ".provider", getOutputMediaFile(
                MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE, code
            )!!
        )
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
        startActivityForResult(intent, code)

    }

    fun getOutputMediaFileUri(type: Int, flag: Int): Uri {
        return Uri.fromFile(getOutputMediaFile(type, flag))
    }

    private fun getOutputMediaFile(type: Int, flag: Int): File? {

        // External sdcard location
        val mediaStorageDir =  File(
            getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
            AppSP.IMAGE_DIRECTORY_NAME
        )
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null
            }
        }

        // Create a media file name
        val timeStamp = SimpleDateFormat(
            "yyyyMMdd_HHmmss",
            Locale.ENGLISH
        ).format(Date())
        imageName = "IMG_$timeStamp,9000.jpg"
        if (type == MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE) {
            mediaFile = File(
                mediaStorageDir.path + File.separator
                        + imageName
            )


        } else {
            return null
        }

        return mediaFile
    }

    private fun isPermissionGranted(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            return Environment.isExternalStorageManager()
        } else {
            var readExternalStorage =
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
            return readExternalStorage == PackageManager.PERMISSION_GRANTED
        }
    }

    private fun takePermissions() {

            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                101
            )

    }

    private fun pickPdfFromGallery() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "application/pdf"
        intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        getResult.launch(intent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (resultCode == RESULT_OK) {
                if (requestCode == 100) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                        if (Environment.isExternalStorageManager()) {
                            pickPdfFromGallery()
                        } else {
                            takePermissions()
                        }
                    }
                }

                else if (requestCode == 11) {
                    if (resultCode == RESULT_OK && null != data) {
                        val result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                        var textdata = result!![0]
                        if (textdata.isNullOrEmpty()) {
                        } else {
                            if (CURRENT_SELECTED_EDITTEXT == 1) {
                                if (et_voname.hasFocus()) {
                                    var cursorPosition: Int = et_voname.selectionStart
                                    et_voname.text.insert(cursorPosition, textdata)
                                } else {
                                    var getData = et_voname.text.toString()
                                    if (getData.length > 0) {
                                        getData =  getData +" " +  textdata
                                    } else {
                                        getData = getData + textdata
                                    }
                                    et_voname.setText(getData)
                                }
                            }
                            if (CURRENT_SELECTED_EDITTEXT == 2) {
                                if (et_vonamelocal.hasFocus()) {
                                    var cursorPosition: Int = et_vonamelocal.selectionStart
                                    et_vonamelocal.text.insert(cursorPosition, textdata)
                                } else {
                                    var getData = et_vonamelocal.text.toString()
                                    if (getData.length > 0) {
                                        getData =  getData +" " +  textdata
                                    } else {
                                        getData = getData + textdata
                                    }
                                    et_vonamelocal.setText(getData)
                                }
                            }
                            if (CURRENT_SELECTED_EDITTEXT == 3) {
                                if (et_promoter_name.hasFocus()) {
                                    var cursorPosition: Int = et_promoter_name.selectionStart
                                    et_promoter_name.text.insert(cursorPosition, textdata)
                                } else {
                                    var getData = et_promoter_name.text.toString()
                                    if (getData.length > 0) {
                                        getData =  getData +" " +  textdata
                                    } else {
                                        getData = getData + textdata
                                    }
                                    et_promoter_name.setText(getData)
                                }
                            }
                            if (CURRENT_SELECTED_EDITTEXT == 4) {
                                if (et_bookkeeper_name.hasFocus()) {
                                    var cursorPosition: Int = et_bookkeeper_name.selectionStart
                                    et_bookkeeper_name.text.insert(cursorPosition, textdata)
                                } else {
                                    var getData = et_bookkeeper_name.text.toString()
                                    if (getData.length > 0) {
                                        getData = getData + " " +  textdata
                                    } else {
                                        getData = getData + textdata
                                    }
                                    et_bookkeeper_name.setText(getData)
                                }
                            }

                        }
                    }
                }

                else if (requestCode == 102) {
                    if (data != null) {
                        fileUri = data.data
                        //  toast("$fileUri")
                        if (fileUri != null) {
                            //  var realPath = Backup(this).getpath(this, fileUri!!)
                            var mediaStorageDir =  File(
                                getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
                                AppSP.IMAGE_DIRECTORY_NAME
                            )

                            // Create the storage directory if it does not exist
                            if (!mediaStorageDir.exists()) {
                                if (!mediaStorageDir.mkdirs()) {
                                    // Create a media file name
                                } else {
                                    //   Toast.makeText(this,"error",Toast.LENGTH_LONG).show()
                                }
                            }
                            if (mediaStorageDir.canWrite()) {
                                val timeStamp = SimpleDateFormat(
                                    "yyyyMMdd_HHmmss",
                                    Locale.ENGLISH
                                ).format(Date())
                                imageName = "FEDRATION_$timeStamp,9000.pdf"
                                //   var realPath = FileUtils.getPath(this, fileUri!!)
                                var realPath = ImageFilePath1.getPath(
                                    this,
                                    fileUri!!
                                )
                                //  Toast.makeText(this,realPath,Toast.LENGTH_LONG).show()
                                val source = File(realPath)
                                var destination = File(mediaStorageDir, imageName)

                                if (source.exists()) {
                                    newimageName = true
                                    Imgshow.setImageResource(R.drawable.pdf)
                                    val src: FileChannel = FileInputStream(source).channel
                                    val dst: FileChannel = FileOutputStream(destination).channel
                                    dst.transferFrom(
                                        src,
                                        0,
                                        src.size()
                                    ) // copy the first file to second.....
                                    src.close()
                                    dst.close()
//                            Com(sd.path + File.separator + destinationImagePath)
                                    //                            save(3,destinationImagePath);
                                }
                            } else {
                                // Toast.makeText(this,"write error",Toast.LENGTH_LONG).show()
                            }


                        } else {
                            // Toast.makeText(this,"write main error",Toast.LENGTH_LONG).show()
                        }
                    }
                } else {
                    compreesedimage()
                    newimageName = true
                    bitmap = BitmapFactory.decodeFile(fileUri!!.path)
                    if (requestCode == 101 && resultCode == Activity.RESULT_OK) {
                        //  ImgFrntpage.setImageBitmap(bitmap)
                        Imgshow.setImageBitmap(bitmap)
                        //tvUploadFiles.setText(imageName)
                    }
                }
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
//                Toast.makeText(applicationContext, R.string.cancel, Toast.LENGTH_SHORT)
//                    .show()

            }
        } catch (e: Exception) {
            e.printStackTrace()
//            Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (grantResults.size > 0) {
            if (requestCode == 101) {
                var readExternalStorage: Boolean =
                    grantResults[0] == PackageManager.PERMISSION_GRANTED
                if (readExternalStorage) {
                    pickPdfFromGallery()
                } else {
                    takePermissions()
                }
            }
        }

    }

    fun compreesedimage() {
        val options = BitmapFactory.Options()
        var bitmap = BitmapFactory.decodeFile(fileUri!!.path, options)

        if (bitmap != null) {
            var actualWidth = options.outWidth
            var actualHeight = options.outHeight
            var mutableBitmap =
                Bitmap.createScaledBitmap(bitmap, actualWidth / 2, actualHeight / 2, false)

            var fOut: OutputStream? = null
            var file: File = File(fileUri!!.path)
            try {
                fOut = FileOutputStream(file)
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }
            mutableBitmap.compress(Bitmap.CompressFormat.JPEG, 50, fOut)
            try {
                fOut!!.flush()
                fOut.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
    }

    private fun ShowImage(name: String?) {
        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val layout: View = inflater.inflate(R.layout.popupwindow, null, false)
        val window = PopupWindow(
            layout,
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT,
            true
        )
        window.showAtLocation(layout, Gravity.CENTER, 0, 0)
        val cancel: ImageView = layout.findViewById(R.id.cancel)
        val image_preview: ImageView = layout.findViewById(R.id.image_preview)
        val mediaStorageDirectory =  File(
            getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
            AppSP.IMAGE_DIRECTORY_NAME
        )
        var mediaFile1 = File(mediaStorageDirectory.path + File.separator + name)
        cancel.setOnClickListener {
            window.dismiss()
        }

        if (mediaFile1.exists()) {
            Picasso.with(this).load(mediaFile1).into(image_preview)
        } else {
            window.dismiss()
        }

    }

    fun CustomAlertPartialsave(str: String, java: Class<*>) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_dialog_title.setTextColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.btn_yes.setBackgroundColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.btn_no.setBackgroundColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.txt_msg.text = str
        mDialogView.btn_yes.setOnClickListener {
            //            PartialSaveData()
            mAlertDialog.dismiss()
            val intent = Intent(this, java)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)


        }
        mDialogView.btn_no.setOnClickListener {
            mAlertDialog.dismiss()

            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation(1) == 1) {
                    SaveBasicDetail(1)
                    var intent = Intent(this, java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    finish()
                }
            }

        }
    }

    fun CustomSelectAlert() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customselectdialoge, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_title.setTextColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.txt_title.text =
            LabelSet.getText(
                "vo_resolution_copy",
                R.string.vo_resolution_copy
            )
        mDialogView.tvMasterData.text =
            LabelSet.getText("captureimage", R.string.captureimage)
        mDialogView.tvUploadData.text =
            LabelSet.getText("upolad_pdf", R.string.upolad_pdf)

        mDialogView.layout_gallery.setOnClickListener {

            mAlertDialog.dismiss()
            if (isPermissionGranted()) {
                pickPdfFromGallery()
            } else {
                takePermissions()
            }


        }
        mDialogView.layout_camera.setOnClickListener {
            if (!checkPermission()) {
                requestPermission(101)
            } else {
                captureimage(101)
            }
            mAlertDialog.dismiss()


        }
    }

    fun fillmemberphoneno(data: List<VoShgMemberPhoneEntity>?) {

        if (!data.isNullOrEmpty()) {
            et_bookkeeper_s_mobile_no.setText(data.get(0).phone_no)
        } else {
            et_bookkeeper_s_mobile_no.setText("")

        }

    }

    private fun generateFederationId(
        guid: String?,
        iAutoSave: Int?,
        federationEntity: FederationEntity,
        voName: String
    ) {
        if (isNetworkConnected()) {
            var listData =
                federationViewmodel!!.getFedrationdata(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
            progressDialog = ProgressDialog.show(
                this, LabelSet.getText(
                    "app_name",
                    R.string.app_name
                ),
                LabelSet.getText(
                    "generateFedId",
                    R.string.generateFedId
                )
            )
            progressDialog.setIcon(this.getDrawable(R.drawable.appicon))
            var idownload = 0
            var msg = ""
            var code = 0
            object : Thread() {

                //@SuppressLint("NewApi")
                override fun run() {
                    try {

                        val gson = Gson()
                        val json = gson.toJson(federationEntity)
                        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                        val token = validate!!.returnStringValue(
                            AESEncryption.decrypt(
                                validate!!.RetriveSharepreferenceString(AppSP.token),
                                validate!!.RetriveSharepreferenceString(AppSP.userid)
                            )
                        )

                        val sData = RequestBody.create(
                            MediaType.parse("application/json; charset=utf-8"),
                            json
                        )
                        val call = apiInterface?.getFederationId(
                            "application/json",
                            user,
                            token,
                            sData
                        )

                        val res = call?.execute()

                        if (res!!.isSuccessful) {
                            try {
                                var federation_id =
                                    validate!!.returnLongValue(res.body()?.federationId)
                                federationEntity.federation_id = federation_id
                                federationViewmodel!!.insert(fedrationEntity!!)
                                validate!!.SaveSharepreferenceLong(
                                    AppSP.Fedration_id,
                                    federation_id
                                )
                                validate!!.SaveSharepreferenceString(AppSP.FedrationGUID, guid!!)
                                validate!!.SaveSharepreferenceString(AppSP.FedrationName, voName)
                                validate!!.SaveSharepreferenceLong(
                                    AppSP.Formation_dt,
                                    validate!!.Daybetweentime(et_formationDate.text.toString())
                                )

                                var image1 = ImageuploadEntity(
                                    imageName,
                                    guid,
                                    "",
                                    1,
                                    0
                                )
                                if (imageName.isNotEmpty() && newimageName) {
                                    imageUploadViewmodel!!.insert(image1)
                                }

                            } catch (ex: Exception) {
                                ex.printStackTrace()
//                                    progressDialog.dismiss()
                            }
                        } else {
                            msg = "" + res.code() + " " + res.message()
                            code = res.code()
                            idownload = 1
//
                        }


                        runOnUiThread {
                            if (idownload == 0) {
                                progressDialog.dismiss()
                                showAlertMsg(guid, iAutoSave)
                            } else if (idownload == -1) {
                                progressDialog.dismiss()
                                val text = LabelSet.getText(
                                    "nothing_upload",
                                    R.string.nothing_upload
                                )
                                validate!!.CustomAlertVO(text, this@VoBasicDetailActivity)
                            } else {
                                progressDialog.dismiss()
                                if (code == 403) {
                                    Toast.makeText(
                                        this@VoBasicDetailActivity,
                                        msg,
                                        Toast.LENGTH_LONG
                                    )
                                        .show()
                                    CustomAlertlogin()
                                } else {

                                    validate!!.CustomAlertVO(msg, this@VoBasicDetailActivity)
                                    Toast.makeText(
                                        this@VoBasicDetailActivity,
                                        msg,
                                        Toast.LENGTH_LONG
                                    )
                                        .show()
                                }
                            }


                        }
                    } catch (bug: SocketTimeoutException) {
                        progressDialog.dismiss()
//                                Toast.makeText(activity!!, "Socket Timeout", Toast.LENGTH_LONG).show();
                    } catch (bug: ConnectTimeoutException) {
                        progressDialog.dismiss()
                        // Toast.makeText(getApplicationContext(), "Connection Timeout", Toast.LENGTH_LONG).show();
                    } catch (e: Exception) {
                        e.printStackTrace()
                        progressDialog.dismiss()
                    }
                }

            }.start()

        } else {
            var msg = LabelSet.getText(
                "no_internet_msg",
                R.string.no_internet_msg
            )
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
            //   showAlertMsg(guid,iAutoSave)
        }
    }



    fun CustomAlertlogin() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogelogin, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mDialogView.etUsername.setText(validate!!.RetriveSharepreferenceString(AppSP.userid))
        mDialogView.etPassword.setText(
            validate!!.returnStringValue(
                AESEncryption.decrypt(
                    validate!!.RetriveSharepreferenceString(AppSP.Password),
                    validate!!.RetriveSharepreferenceString(AppSP.userid)
                )
            )
        )
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.btn_login.setBackgroundColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.txt_dialog_title2.text = LabelSet.getText("app_name", R.string.app_name)
        mDialogView.btn_login.text = LabelSet.getText("log_in", R.string.log_in)
        mDialogView.chk_showHide1.text = LabelSet.getText("show_password", R.string.show_password)
        mDialogView.btn_login.setOnClickListener {
            mAlertDialog.dismiss()
            importtoken(
                mDialogView.etUsername.text.toString(),
                validate!!.returnStringValue(
                    AESEncryption.encrypt(
                        validate!!.returnStringValue(
                            mDialogView.etPassword.text.toString()
                        ), mDialogView.etUsername.text.toString()
                    )
                )
            )
        }
        mAlertDialog.chk_showHide1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                mAlertDialog.etPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                mAlertDialog.etPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
        }

    }

    fun importtoken(user: String, pass: String) {
        var grant_type = "password"
        var userId = user
        var password = pass
        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText(
                "authenticate_user",
                R.string.authenticate_user
            )
        )


        val callCount = apiInterface?.gettokenforce(
            grant_type,
            userId,
            password, validate!!.RetriveSharepreferenceInt(AppSP.State_Selected), 1,
            validate!!.RetriveSharepreferenceString(AppSP.Roleid)!!
        )
        callCount?.enqueue(object : Callback<JsonObject> {
            override fun onFailure(callCount: Call<JsonObject>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                //       Log.e("", response!!.body().toString())
                progressDialog.dismiss()


                if (response.isSuccessful) {

                    var jsonObject1: JSONObject? = null
                    try {
                        jsonObject1 = JSONObject(response.body().toString())

                        val auth = jsonObject1.get("accessToken").toString()
                        if (auth.trim().length > 0) {
                            validate!!.SaveSharepreferenceString(
                                AppSP.token,
                                validate!!.returnStringValue(
                                    AESEncryption.encrypt(
                                        auth, validate!!.RetriveSharepreferenceString(AppSP.userid)
                                    )
                                )
                            )
                        }
                        validate!!.CustomAlertVO(
                            LabelSet.getText(
                                "userauthenticatedsuccessfully",
                                R.string.userauthenticatedsuccessfully
                            ), this@VoBasicDetailActivity
                        )

                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Toast.makeText(
                            this@VoBasicDetailActivity,
                            response.message(),
                            Toast.LENGTH_LONG
                        )
                            .show()


                    }

                } else {
                    Toast.makeText(
                        this@VoBasicDetailActivity,
                        response.message(),
                        Toast.LENGTH_LONG
                    )
                        .show()
                }


            }

        })

    }

    fun showAlertMsg(guid: String?, iAutoSave: Int?) {
        var mapped_shg_count = federationViewmodel!!.getMappedShgCount(guid)
        var mapped_vo_count = federationViewmodel!!.getMappedVoCount(guid)
        if (iAutoSave == 0) {
            if (cboType == 1) {
                if (mapped_shg_count > 0 || mapped_vo_count > 0) {
                    validate!!.CustomAlertVO(
                        LabelSet.getText(
                            "data_saved_successfully",
                            R.string.data_saved_successfully
                        ),
                        this,
                        VoEcListActivity::class.java
                    )
                } else {
                    validate!!.CustomAlertVO(
                        LabelSet.getText(
                            "data_saved_successfully",
                            R.string.data_saved_successfully
                        ),
                        this,
                        VOMapCBOActivity::class.java
                    )
                }
            } else {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "data_saved_successfully",
                        R.string.data_saved_successfully
                    ),
                    this,
                    CLFMapCBOActivity::class.java
                )
            }
        }
    }

    fun setFederationEditFlag(): Int {
        var is_Edited = 0
        var bank_Fi = 0
        var financialIntermidiation = rgIntermediation.checkedRadioButtonId
        var bankCount =
            federationViewmodel!!.getBankCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var signatoryCount =
            federationViewmodel!!.getSignatoryCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!)

        if ((financialIntermidiation == 1 && bankCount > 0 && signatoryCount >= 2) || financialIntermidiation == 0) {
            bank_Fi = 1
        } else {
            bank_Fi = 0
        }
        var scMemberCount =
            federationViewmodel!!.getSCMemberCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!)
        var officeBearerCount = executiveviewmodel!!.getOBCount(
            listOf(1, 3, 5),
            validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!
        )
        var cboCount =
            executiveviewmodel!!.getDistinctCboCount(validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id))
        var ecCount =
            federationViewmodel!!.getEcMemberCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var mapped_shg_count =
            federationViewmodel!!.getMappedShgCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var mapped_vo_count =
            federationViewmodel!!.getMappedVoCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        if (cboType == 1 && ecCount > 4 && mapped_shg_count > 4 && mapped_shg_count == cboCount && officeBearerCount == 3 && bank_Fi == 1 && scMemberCount == 0) {
            is_Edited = 1
        } else if (cboType == 2 && ecCount > 4 && mapped_vo_count > 4 && mapped_vo_count == cboCount && officeBearerCount == 3 && bank_Fi == 1 && scMemberCount == 0) {
            is_Edited = 1
        } else {
            is_Edited = -1
        }
        return is_Edited
    }

    private fun checkData(): Int {
        var value = 1
        var meetingfrequencyvalue = 0
        var meetingon = 0
        var pramotedby = validate!!.returnlookupcode(spin_promotedby, dataspin_promotedby)
        var promotor_code = validate!!.returnFundingcode(spin_FundingAgency, dataspin_funding)
        var meetingfrequency = validate!!.returnlookupcode(spin_frequency, dataspin_meetingfreq)
        val monthlyId = validate!!.returnlookupcode(spin_monthly, dataspin_monthly)

        if (lay_fortnightdate.visibility == View.VISIBLE) {
            meetingfrequencyvalue =
                validate!!.returnlookupcode(spin_fortnightdate, dataspin_fortnightdate)
        } else if (lay_monthly.visibility == View.VISIBLE) {
            meetingfrequencyvalue = validate!!.returnlookupcode(spin_monthly, dataspin_monthly)
        }
        if (lay_weekday.visibility == View.VISIBLE) {
            meetingon = validate!!.returnlookupcode(spin_weekday, dataspin_weekday)
        } else if (lay_monthlydate.visibility == View.VISIBLE) {
            meetingon = validate!!.returnlookupcode(spin_monthlydate, dataspin_monthlydate)
        }

        var book_keeper_name = ""
        if (rgBookeeper.checkedRadioButtonId == 1) {
            book_keeper_name = returnMemberName()
        } else {
            book_keeper_name = et_bookkeeper_name.text.toString()
        }
        if(!validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID).isNullOrEmpty()){
            var listData =
                federationViewmodel!!.getFedrationdata(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))


           if (et_voname.text.toString() != validate!!.returnStringValue(listData?.get(0)?.federation_name)) {
                value = 0
            } else if (et_vonamelocal.text.toString() != validate!!.returnStringValue(listData?.get(0)?.federation_name_local)) {
                value = 0
            } else if (validate!!.Daybetweentime(et_formationDate.text.toString()) != validate!!.returnLongValue(
                    listData?.get(0)?.federation_formation_date.toString()
                )
            ) {
                value = 0
            } else if (pramotedby != validate!!.returnIntegerValue(
                    listData?.get(
                        0
                    )?.promoted_by.toString()
                )
            ) {
                value = 0
            } else if (promotor_code != validate!!.returnIntegerValue(
                    listData?.get(
                        0
                    )?.promoter_code.toString()
                )
            ) {
                value = 0
                return value
            } else if (et_promoter_name.text.toString() != validate!!.returnStringValue(listData?.get(0)?.promoter_name)) {
                value = 0
            } else if (validate!!.Daybetweentime(et_coopt_Date.text.toString()) != validate!!.returnLongValue(
                    listData?.get(0)?.cooption_date.toString()
                )
            ) {
                value = 0
            } else if (validate!!.Daybetweentime(et_revivalDate.text.toString()) != validate!!.returnLongValue(
                    listData?.get(0)?.federation_revival_date.toString()
                )
            ) {
                value = 0
            } else if (meetingfrequency != validate!!.returnIntegerValue(
                    listData?.get(0)?.meeting_frequency.toString())
            ) {
                value = 0
            }else if(  meetingon != validate!!.returnIntegerValue(listData?.get(0)?.meeting_on.toString())){
                value = 0
            }else if(  meetingfrequencyvalue != validate!!.returnIntegerValue(listData?.get(0)?.meeting_frequency_value.toString())){
                value = 0
            }else if (rgIntermediation.checkedRadioButtonId != validate!!.returnIntegerValue(
                    listData?.get(
                        0
                    )?.is_financial_intermediation.toString()
                )
            ) {
                value = 0
                return value
            } else if (validate!!.returnlookupcode(spin_savingfrequency, dataspin_meetingfreq) != validate!!.returnIntegerValue(
                    listData?.get(0)?.saving_frequency.toString()
                )
            ) {
                value = 0
            } else if (et_saving.text.toString() != validate!!.returnStringValue(listData?.get(0)?.month_comp_saving.toString())) {

                value = 0
            } else if (et_ComsavingRoi.text.toString() != validate!!.returnStringValue(listData?.get(0)?.savings_interest.toString())) {

                value = 0
            } else if (rgvolunteer.checkedRadioButtonId != validate!!.returnIntegerValue(listData?.get(0)?.is_voluntary_saving.toString())) {
                value = 0
            } else if (et_voluntarysavingROI.text.toString() != validate!!.returnStringValue(
                    listData?.get(
                        0
                    )?.voluntary_savings_interest.toString()
                )
            ) {
                value = 0
            } else if (rgBookeeper.checkedRadioButtonId != validate!!.returnIntegerValue(listData?.get(0)?.bookkeeper_identified.toString())) {
                value = 0
            } else if (book_keeper_name != validate!!.returnStringValue(listData?.get(0)?.bookkeeper_name.toString())) {
                value = 0
            } else if (validate!!.returnlookupcode(spin_status, dataspin_status) != validate!!.returnIntegerValue(listData?.get(0)?.status.toString())) {
                value = 0
            } else if (et_bookkeeper_s_mobile_no.text.toString() != validate!!.returnStringValue(
                    listData?.get(0)?.bookkeeper_mobile
                )
            ) {
                value = 0
            } else if (validate!!.returnIntegerValue(et_electiontenure.text.toString()) != validate!!.returnIntegerValue(listData?.get(0)?.election_tenure.toString())) {
                value = 0
            } else if (    imageName != validate!!.returnStringValue(listData?.get(0)?.federation_resolution)) {
                value = 0
            }
        }else {
            value = 0

        }
        return value
    }

    private val getResult =
        registerForActivityResult(
            ActivityResultContracts.StartActivityForResult()
        ) {
            if (it.resultCode == Activity.RESULT_OK) {
                val value = it.data!!

                if (value != null) {
                    fileUri = value.data
                    //toast("$fileUri")
                    if (fileUri != null) {
                        val timeStamp = SimpleDateFormat(
                            "yyyyMMdd_HHmmss",
                            Locale.ENGLISH
                        ).format(Date())
                        imageName = "SHG_$timeStamp,2000.pdf"
                        //   var realPath = FileUtils.getPath(this, fileUri!!)

                        val path2 = File(
                            getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
                            AppSP.IMAGE_DIRECTORY_NAME
                        )
                        if (!path2.exists()) {
                            path2.mkdirs()
                        }
                        var destination = File(path2.path + File.separator + imageName)
                        getFileName(fileUri!!,destination)
                    }
                }
            }
        }

    //
    @Throws(IllegalArgumentException::class)
    private fun getFileName(uri: Uri, destination: File) {

        try {
            val parcelFileDescriptor = this.contentResolver.openFileDescriptor(uri, "r", null)

            val inputStream = FileInputStream(parcelFileDescriptor!!.fileDescriptor)

            // Obtain a cursor with information regarding this uri
            val cursor: Cursor? = contentResolver.query(uri, null, null, null, null)
            if (cursor!!.count <= 0) {
                cursor.close()
                throw IllegalArgumentException("Can't obtain file name, cursor is empty")
            }
            cursor.moveToFirst()
            val fileName: String =
                cursor.getString(cursor.getColumnIndexOrThrow(OpenableColumns.DISPLAY_NAME))
            cursor.close()

//            val file = File(this.cacheDir, fileName)
//            var str=file.absolutePath
            val outputStream = FileOutputStream(destination)
            IOUtils.copy(inputStream, outputStream)
            newimageName = true
            Imgshow.setImageResource(R.drawable.pdf)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    fun promptSpeechInput(language: String) {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )
        if (isNetworkConnected()){
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, language)
        }
        else{
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.ENGLISH)
        }
        intent.putExtra(
            RecognizerIntent.EXTRA_PROMPT,
            getString(R.string.speech_prompt)
        )
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT)
        } catch (a: ActivityNotFoundException) {
            Toast.makeText(
                applicationContext,
                getString(R.string.speech_not_supported),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun isNetworkConnected(): Boolean {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager //1
        val networkInfo = connectivityManager.activeNetworkInfo //2
        return networkInfo != null && networkInfo.isConnected //3
    }

}
