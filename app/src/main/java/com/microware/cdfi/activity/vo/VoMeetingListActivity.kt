package com.microware.cdfi.activity.vo
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.VoMeetingListAdapter
import com.microware.cdfi.utility.LabelSet
import kotlinx.android.synthetic.main.activity_vomeeting_list.*
import kotlinx.android.synthetic.main.repay_toolbar.*


class VoMeetingListActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vomeeting_list)

        ic_Back.visibility = View.GONE

        setLabelText()
        fillRecyclerView()
    }

    override fun onBackPressed() {
        var intent = Intent(this, VoMeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0,0)
        finish()
    }

    fun setLabelText() {
        tv_title.text = LabelSet.getText(
            "vo_list",
            R.string.vo_list
        )
        tv_village.text = LabelSet.getText(
            "village",
            R.string.village
        )
        tv_pendingapproval.text = LabelSet.getText(
            "pending_approval_1",
            R.string.pending_approval_1
        )
        tv_activegroup.text = LabelSet.getText(
            "active_group_1",
            R.string.active_group_1
        )
        tv_inactivegroup.text = LabelSet.getText(
            "inactive_group_1",
            R.string.inactive_group_1
        )
    }

    private fun fillRecyclerView() {
        rvList.layoutManager = LinearLayoutManager(this)
        rvList.adapter =
            VoMeetingListAdapter(this)
    }
}