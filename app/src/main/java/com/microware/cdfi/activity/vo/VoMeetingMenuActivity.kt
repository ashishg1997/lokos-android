package com.microware.cdfi.activity.vo
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.LookupViewmodel
import kotlinx.android.synthetic.main.activity_vomeetingmenu.*
import kotlinx.android.synthetic.main.buttons_vomeeting.btn_cancelmeeting
import kotlinx.android.synthetic.main.buttons_vomeeting.btn_generate_open
import kotlinx.android.synthetic.main.buttons_vomeeting_deleteandclose.btn_cancel
import kotlinx.android.synthetic.main.buttons_vomeeting_deleteandclose.btn_save
import kotlinx.android.synthetic.main.repay_toolbar_vomeeting.ic_Back
import kotlinx.android.synthetic.main.repay_toolbar_vomeeting.tv_title

class VoMeetingMenuActivity : AppCompatActivity() {
    var lookupViewmodel: LookupViewmodel? = null
    var validate: Validate? = null
    var dataspin_meeting_no: List<LookupEntity>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vomeetingmenu)

        validate=Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)

        ic_Back.setOnClickListener {
            var intent = Intent(this, GenerateVoMeetingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        btn_cancelmeeting.setOnClickListener {
            var intent = Intent(this, GenerateVoMeetingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        btn_cancel.setOnClickListener {
            var intent = Intent(this, GenerateVoMeetingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_shg_attendence.setOnClickListener {
            var intent = Intent(this, VoAttendnceDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_visitor_attendance.setOnClickListener {
            var intent = Intent(this, GenerateVoMeetingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        tbl_receipt.setOnClickListener {
            var intent = Intent(this, ReceiptActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        tbl_payment.setOnClickListener {
            var intent = Intent(this, CutOffMeetingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        tbl_contra.setOnClickListener {
           // var intent = Intent(this, LoanDisbursementActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        tbl_journal.setOnClickListener {
            //var intent = Intent(this, RepaymentActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        tbl_summary.setOnClickListener {
            //var intent = Intent(this, PeneltyDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        tbl_shg_loans_others.setOnClickListener {
            //var intent = Intent(this, PeneltyDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        tbl_cashBox.setOnClickListener {
            //var intent = Intent(this, GroupLoanRepaidActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        setLabelText()
        fillSpinner()

    }

    fun setLabelText()
    {

        tv_meeting_no.text = LabelSet.getText(
            "meeting_no",
            R.string.meeting_no
        )
        tv_meeting_date.text = LabelSet.getText(
            "meeting_date",
            R.string.meeting_date
        )
        tv_shg_attendence.text = LabelSet.getText(
            "shg_attendance",
            R.string.shg_attendance
        )
        tv_visitor_attendance.text = LabelSet.getText(
            "visitor_attendance",
            R.string.visitor_attendance
        )
        tv_receipt.text = LabelSet.getText(
            "receipt",
            R.string.receipt
        )
        tv_payment.text = LabelSet.getText(
            "payment",
            R.string.payment
        )
        tv_contra.text = LabelSet.getText(
            "contra",
            R.string.contra
        )
        tv_journal.text = LabelSet.getText(
            "journal",
            R.string.journal
        )
        tv_summary.text = LabelSet.getText(
            "summary",
            R.string.summary
        )
        tv_shg_loans_others.text = LabelSet.getText(
            "shg_loan_others",
            R.string.shg_loan_others
        )
        tv_cashBox.text = LabelSet.getText(
            "cashBox",
            R.string.cashBox
        )
        tv_title.text = LabelSet.getText(
            "meeting_menu",
            R.string.meeting_menu
        )
        btn_save.text = LabelSet.getText(
            "delete_meeting",
            R.string.delete_meeting
        )
        btn_cancel.text = LabelSet.getText(
            "close_meeting",
            R.string.close_meeting
        )
        btn_generate_open.text = LabelSet.getText(
            "cash_openbalance",
            R.string.cash_openbalance
        )
        btn_cancelmeeting.text = LabelSet.getText(
            "closing_balance",
            R.string.closing_balance
        )

        et_meeting_date.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )

    }

    private fun fillSpinner() {
        dataspin_meeting_no = lookupViewmodel!!.getlookup(
            99,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        validate!!.fillspinner(this, spin_meeting_no, dataspin_meeting_no)
    }
}