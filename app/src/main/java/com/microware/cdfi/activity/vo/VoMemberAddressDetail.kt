package com.microware.cdfi.activity.vo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.microware.cdfi.R
import com.microware.cdfi.utility.LabelSet
import kotlinx.android.synthetic.main.activity_vo_member_address_detail.*
import kotlinx.android.synthetic.main.tablayout.*
import kotlinx.android.synthetic.main.white_toolbar.*

class VoMemberAddressDetail : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vo_member_address_detail)

        ivHome.visibility = View.GONE
        tv_title.text = LabelSet.getText(
            "addressdetails",
            R.string.addressdetails
        )
        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary1))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvVector.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimary1))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this,R.color.white))

        lay_vector.setOnClickListener {
            var intent = Intent(this, VoMemberDetailActviity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_phone.setOnClickListener {
            var intent = Intent(this, VoMemebrPhoneDetail::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_bank.setOnClickListener {
            var intent = Intent(this, VoMemberBankDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_kyc.setOnClickListener {
            var intent = Intent(this, VOMemberKycDetail::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        setLabelText()

    }

    private fun setLabelText(){

        tvMemberCode.text = LabelSet.getText(
            "member_code",
            R.string.member_code
        )
        tvAddressType.text = LabelSet.getText(
            "address_type",
            R.string.address_type
        )
        tvAddress.text = LabelSet.getText(
            "address",
            R.string.address
        )
        et_address1.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tvState.text = LabelSet.getText("state", R.string.state)
        tvDistrict.text = LabelSet.getText(
            "district",
            R.string.district
        )
        tvBlock.text = LabelSet.getText("block", R.string.block)
        tvCity.text = LabelSet.getText("city", R.string.city)
        tvpincode.text = LabelSet.getText(
            "pincode",
            R.string.pincode
        )
        et_pincode.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_landmark.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tvLandmark.text = LabelSet.getText(
            "landmark",
            R.string.landmark
        )
        tvStatus.text = LabelSet.getText(
            "status",
            R.string.status
        )
        btn_add.text = LabelSet.getText(
            "add_address",
            R.string.add_address
        )
    }

    override fun onBackPressed() {
        var intent = Intent(this, VoMemberSbsoListActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }
}
