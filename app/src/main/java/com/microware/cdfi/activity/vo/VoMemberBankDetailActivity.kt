package com.microware.cdfi.activity.vo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.microware.cdfi.R
import com.microware.cdfi.utility.LabelSet
import kotlinx.android.synthetic.main.activity_vo_member_bank_detail.*
import kotlinx.android.synthetic.main.tablayout.*
import kotlinx.android.synthetic.main.white_toolbar.*

class VoMemberBankDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vo_member_bank_detail)

        ivHome.visibility = View.GONE
        tv_title.text = LabelSet.getText(
            "bankdetails",
            R.string.bankdetails
        )
        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary1))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvVector.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimary1))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this,R.color.white))

        lay_vector.setOnClickListener {
            var intent = Intent(this, VoMemberDetailActviity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_phone.setOnClickListener {
            var intent = Intent(this, VoMemebrPhoneDetail::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_location.setOnClickListener {
            var intent = Intent(this, VoMemberAddressDetail::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_kyc.setOnClickListener {
            var intent = Intent(this, VOMemberKycDetail::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        setLabelText()

    }

    private fun setLabelText() {
        tvCode.text = LabelSet.getText(
            "member_code",
            R.string.member_code
        )
        tvBankID.text = LabelSet.getText(
            "bank_id",
            R.string.bank_id
        )
        et_bankid.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_ifsc.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_Accountno.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tvIFSCCode.text = LabelSet.getText(
            "ifsc_code",
            R.string.ifsc_code
        )
        tvAccountType.text = LabelSet.getText(
            "account_type",
            R.string.account_type
        )
        tvAccountno.text = LabelSet.getText(
            "account_no",
            R.string.account_no
        )
        tvValidForm.text = LabelSet.getText(
            "valid_form",
            R.string.valid_form
        )
        tvISDefault.text = LabelSet.getText(
            "is_default",
            R.string.is_default
        )
        tvStatus.text = LabelSet.getText(
            "status",
            R.string.status
        )
        tvClosingDate.text = LabelSet.getText(
            "closingdate",
            R.string.closingdate
        )
        tvGlCode.text = LabelSet.getText(
            "gl_code",
            R.string.gl_code
        )
        et_glcode.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tvSameAsGroup.text = LabelSet.getText(
            "same_as_group",
            R.string.same_as_group
        )
        btn_add.text = LabelSet.getText(
            "add_bank",
            R.string.add_bank
        )
    }

    override fun onBackPressed() {
        var intent = Intent(this, VoMemberSbsoListActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }
}
