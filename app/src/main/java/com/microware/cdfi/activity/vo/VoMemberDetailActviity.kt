package com.microware.cdfi.activity.vo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.microware.cdfi.R
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.activity_vo_member_detail_actviity.*
import kotlinx.android.synthetic.main.tablayout.*
import kotlinx.android.synthetic.main.white_toolbar.*

class VoMemberDetailActviity : AppCompatActivity() {
    var validate: Validate? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vo_member_detail_actviity)

        ivHome.visibility = View.GONE

        tv_title.text = LabelSet.getText(
            "shgbasicdetails",
            R.string.shgbasicdetails
        )

        validate = Validate(this)

        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary1))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvVector.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary1))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this, R.color.white))

        lay_phone.setOnClickListener {
            var intent = Intent(this, VoMemebrPhoneDetail::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_location.setOnClickListener {
            var intent = Intent(this, VoMemberAddressDetail::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_bank.setOnClickListener {
            var intent = Intent(this, VoMemberBankDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_kyc.setOnClickListener {
            var intent = Intent(this, VOMemberKycDetail::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        setLabelText()
    }

    private fun setLabelText() {
        tvPanchayat.text = LabelSet.getText(
            "grampanchayat",
            R.string.grampanchayat
        )
        tvVillage.text = LabelSet.getText(
            "village",
            R.string.village
        )
        tvVoName.text = LabelSet.getText(
            "vo_name",
            R.string.vo_name
        )
        et_voname.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tvVOCode.text = LabelSet.getText("voor", R.string.voor)
        et_shgcode.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tvFormationDate.text = LabelSet.getText(
            "formation_date",
            R.string.formation_date
        )
        tvRegistration.text = LabelSet.getText(
            "registration",
            R.string.registration
        )
        tvRegTypeAct.text = LabelSet.getText(
            "registration_type_act",
            R.string.registration_type_act
        )
        tvRegNumber.text = LabelSet.getText(
            "registration_number",
            R.string.registration_number
        )
        et_regnum.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tvFedRegDate.text = LabelSet.getText(
            "fed_Reg_dte",
            R.string.fed_Reg_dte
        )
        tvMtgFreq.text = LabelSet.getText(
            "mtg_freq",
            R.string.mtg_freq
        )
        tvPromotedBy.text = LabelSet.getText(
            "promoted_by",
            R.string.promoted_by
        )
        tvComSaving.text = LabelSet.getText(
            "com_saving",
            R.string.com_saving
        )
        et_comSavingAcc.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tvConPrimary.text = LabelSet.getText(
            "con_primary",
            R.string.con_primary
        )
        et_conPrimary.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_ConSecondary.text = LabelSet.getText(
            "con_secondary",
            R.string.con_secondary
        )
        et_conSecondary.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tvTotalCBs.text = LabelSet.getText(
            "totl_cbs",
            R.string.totl_cbs
        )
        et_totalcbs.setText(LabelSet.getText(
            "type_here",
            R.string.type_here
        ))
        btn_add.text = LabelSet.getText("save", R.string.save)
    }
}