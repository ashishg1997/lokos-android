package com.microware.cdfi.activity.vo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.microware.cdfi.R
import com.microware.cdfi.utility.LabelSet
import kotlinx.android.synthetic.main.tablayout.*
import kotlinx.android.synthetic.main.white_toolbar.*

class VoMemberPhoneList : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vo_member_phone_list)

        ivHome.visibility = View.GONE
        tv_title.text = LabelSet.getText(
            "phonedetails",
            R.string.phonedetails
        )
        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary1))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvVector.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimary1))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this,R.color.white))

        lay_vector.setOnClickListener {

        }

        lay_location.setOnClickListener {
            var intent = Intent(this, VoMemberAddressDetail::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_bank.setOnClickListener {
            var intent = Intent(this, VoMemberBankDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_kyc.setOnClickListener {
            var intent = Intent(this, VOMemberKycDetail::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }


        setLabelText()

    }

    private fun setLabelText() {

    }
}