package com.microware.cdfi.activity.vo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.microware.cdfi.R
import com.microware.cdfi.utility.LabelSet
import kotlinx.android.synthetic.main.activity_vo_member_sbso_list.*

class VoMemberSbsoListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vo_member_sbso_list)

        IvAdd.setOnClickListener {
            var inetnt = Intent(this, VoMemberDetailActviity::class.java)
            inetnt.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(inetnt)
        }

        setLabelText()

    }

    private fun setLabelText() {
        tvGroupName.text = LabelSet.getText(
            "group_name",
            R.string.group_name
        )
        tv_pendingapproval.text = LabelSet.getText(
            "pending_approval_1",
            R.string.pending_approval_1
        )
        tv_activegroup.text = LabelSet.getText(
            "active_group_1",
            R.string.active_group_1
        )
        tv_inactivegroup.text = LabelSet.getText(
            "inactive_group_1",
            R.string.inactive_group_1
        )

    }
}
