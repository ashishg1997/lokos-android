package com.microware.cdfi.activity.vo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.microware.cdfi.R
import com.microware.cdfi.utility.LabelSet
import kotlinx.android.synthetic.main.activity_vo_memebr_phone_detail.*
import kotlinx.android.synthetic.main.tablayout.*
import kotlinx.android.synthetic.main.white_toolbar.*

class VoMemebrPhoneDetail : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vo_memebr_phone_detail)

        ivHome.visibility = View.GONE
        tv_title.text = LabelSet.getText(
            "phonedetails",
            R.string.phonedetails
        )
        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary1))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvVector.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimary1))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this,R.color.white))

        lay_vector.setOnClickListener {
            var intent = Intent(this, VoMemberDetailActviity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_location.setOnClickListener {
            var intent = Intent(this, VoMemberAddressDetail::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_bank.setOnClickListener {
            var intent = Intent(this, VoMemberBankDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_kyc.setOnClickListener {
            var intent = Intent(this, VOMemberKycDetail::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        setLabelText()
    }

    private fun setLabelText() {
        tvCode.text = LabelSet.getText(
            "member_code",
            R.string.member_code
        )
        tvOwnerShop.text = LabelSet.getText(
            "ownership",
            R.string.ownership
        )
        tvPhoneNO.text = LabelSet.getText(
            "phone_no",
            R.string.phone_no
        )
        et_phoneno.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tvStatus.text = LabelSet.getText(
            "status",
            R.string.status
        )
        tvValidForm.text = LabelSet.getText(
            "valid_form",
            R.string.valid_form
        )
        tvValidTill.text = LabelSet.getText(
            "valid_till",
            R.string.valid_till
        )
        tvISDefault.text = LabelSet.getText(
            "is_default",
            R.string.is_default
        )
        btn_add.text = LabelSet.getText(
            "add_phone",
            R.string.add_phone
        )
    }

    override fun onBackPressed() {
        var intent = Intent(this, VoMemberSbsoListActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }
}
