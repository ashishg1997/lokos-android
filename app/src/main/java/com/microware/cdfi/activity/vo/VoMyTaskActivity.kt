package com.microware.cdfi.activity.vo

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProviders
import com.google.gson.JsonObject
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.PendingMeetingActivity
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.api.meetinguploadmodel.MeetingApprovalCountModel
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.ResponseViewModel
import kotlinx.android.synthetic.main.activity_vomy_tasks.*
import kotlinx.android.synthetic.main.customealertdialogelogin.*
import kotlinx.android.synthetic.main.customealertdialogelogin.view.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.votoolbar.*
import org.apache.http.conn.ConnectTimeoutException
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException

class VoMyTaskActivity : AppCompatActivity() {
    var validate: Validate? = null
    internal lateinit var progressDialog: ProgressDialog
    var responseViewModel: ResponseViewModel? = null

    var apiInterface: ApiInterface? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vomy_tasks)
        validate = Validate(this)
        responseViewModel = ViewModelProviders.of(this).get(ResponseViewModel::class.java)

        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        IvScan.visibility = View.GONE
        IVSync.visibility = View.GONE
        setLabelText()


        tv_date.text = validate!!.convertDatetime(validate!!.Daybetweentime(validate!!.currentdatetime))

        icBack.setOnClickListener {
            val i = Intent(this, VoDrawerActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)
            finish()

        }

        updatePendingMeetingCount()

        /*tbl_de_duplication.setOnClickListener {
            //var intent = Intent(this, AttendnceDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_bank_deposit.setOnClickListener {
            //var intent = Intent(this, CompulsorySavingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_upload_meetings.setOnClickListener {
            //var intent = Intent(this, VoluntorySavingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_sync_master.setOnClickListener {
            // var intent = Intent(this, WidthdrawalDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_mcp_preparation.setOnClickListener {
            var intent = Intent(this, MCPPreparationActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_mcp_status.setOnClickListener {
            // var intent = Intent(this, RepaymentActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_loan_demand.setOnClickListener {
            // var intent = Intent(this, PeneltyDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_regular_demand_status.setOnClickListener {
            //var intent = Intent(this, GroupLoanRepaidActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_loan_repayment_schedule.setOnClickListener {
            // var intent = Intent(this, GroupLoanReceivedActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_dividend.setOnClickListener {
            //var intent = Intent(this, BankGroupTransactionActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_vouchers.setOnClickListener {
            // var intent = Intent(this, GroupMeetingSummery::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        */

        tbl_meeting_summary_approval.setOnClickListener {
            var intent = Intent(this, PendingMeetingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }


    }

    fun setLabelText() {
        tv_de_duplication.text = LabelSet.getText(
            "de_duplication",
            R.string.de_duplication
        )
        tv_bank_deposit.text = LabelSet.getText(
            "bank_deposit",
            R.string.bank_deposit
        )
        tv_upload_meetings.text = LabelSet.getText(
            "upload_meetings",
            R.string.upload_meetings
        )
        tv_sync_master.text = LabelSet.getText(
            "sync_master",
            R.string.sync_master
        )
        tv_mcp_preparation.text = LabelSet.getText(
            "mcp_preparation",
            R.string.mcp_preparation
        )
        tv_mcp_status.text = LabelSet.getText(
            "mcp_status",
            R.string.mcp_status
        )
        tv_loan_demand.text = LabelSet.getText(
            "loan_demand",
            R.string.loan_demand
        )
        tv_regular_demand_status.text = LabelSet.getText(
            "regular_demand_status",
            R.string.regular_demand_status
        )
        tv_loan_repayment_schedule.text = LabelSet.getText(
            "loan_repayment_schedule",
            R.string.loan_repayment_schedule
        )
        tv_dividend.text = LabelSet.getText(
            "dividend",
            R.string.dividend
        )
        tv_meeting_summary_approval.text = LabelSet.getText(
            "meeting_summary_approval",
            R.string.meeting_summary_approval
        )
        tv_vouchers.text = LabelSet.getText(
            "vouchers",
            R.string.vouchers
        )
        tv_my_tasks.text = LabelSet.getText(
            "my_task",
            R.string.my_task
        )
        // tv_date.setText(LabelSet.getText("today_12_dec_2020", R.string.today_12_dec_2020))
        tv_title.text = LabelSet.getText(
            "my_task",
            R.string.my_task
        )
    }

    override fun onBackPressed() {
        val i = Intent(this, VoDrawerActivity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(i)
        finish()
    }

    fun importPendingMeetingCount() {

        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText("DataLoading", R.string.DataLoading)
        )

        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
        val token = validate!!.returnStringValue(AESEncryption.decrypt(
            validate!!.RetriveSharepreferenceString(AppSP.token),
            validate!!.RetriveSharepreferenceString(AppSP.userid)
        ))
        val callCount = apiInterface?.getMeetingApprovalCount(
            token,
            user
        )

        callCount?.enqueue(object : Callback<MeetingApprovalCountModel> {
            override fun onFailure(callCount: Call<MeetingApprovalCountModel>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<MeetingApprovalCountModel>,
                response: Response<MeetingApprovalCountModel>
            ) {

                progressDialog.dismiss()
                if (response.isSuccessful) {
                    try {
                        validate!!.SaveSharepreferenceString(MeetingSP.pendingMeetings,"0")
                        var pendingCount = validate!!.returnStringValue(response.body()?.pending.toString())
                        validate!!.SaveSharepreferenceString(MeetingSP.pendingMeetings,pendingCount)


                        val text = LabelSet.getText(
                            "Datadownloadedsuccessfully",
                            R.string.Datadownloadedsuccessfully
                        )
                        Toast.makeText(this@VoMyTaskActivity,text, Toast.LENGTH_LONG).show()

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        //                                    progressDialog.dismiss()
                    }

                } else {
                    progressDialog.dismiss()
                    var resCode = 0
                    var resMsg = ""
                    if (response.code() == 403) {

                        CustomAlertlogin()
                    } else {
                        if(response.errorBody()?.contentLength()==0L || response.errorBody()?.contentLength()!! <0L){
                            resCode = response.code()
                            resMsg = response.message()
                        }else {
                            var jsonObject1 =
                                JSONObject(response.errorBody()!!.source().readUtf8().toString())

                            resCode =
                                validate!!.returnIntegerValue(
                                    jsonObject1.optString("responseCode").toString()
                                )
                            resMsg = validate!!.returnStringValue(jsonObject1.optString("responseMsg").toString())
                        }

                        validate!!.CustomAlertMsg(
                            this@VoMyTaskActivity,
                            responseViewModel,
                            resCode,
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,resMsg)

                    }

                }


            }

        })
    }

    fun updatePendingMeetingCount() {
        progressDialog = ProgressDialog.show(
            this, LabelSet.getText("app_name", R.string.app_name),
            LabelSet.getText(
                "DataLoading",
                R.string.DataLoading
            )
        )
        progressDialog.setIcon(this.getDrawable(R.drawable.appicon))
        var idownload = 0
        var msg = ""
        var code = 0
        var pendingCount = "0"
        object : Thread() {

            //@SuppressLint("NewApi")
            override fun run() {
                try {

                    val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                    val token = validate!!.returnStringValue(AESEncryption.decrypt(
                        validate!!.RetriveSharepreferenceString(AppSP.token),
                        validate!!.RetriveSharepreferenceString(AppSP.userid)
                    ))
                    val call1 = apiInterface?.getMeetingApprovalCount(
                        token,
                        user
                    )

                    val res = call1?.execute()

                    if (res!!.isSuccessful) {
                        try {
                            validate!!.SaveSharepreferenceString(MeetingSP.pendingMeetings,"0")
                            pendingCount = validate!!.returnStringValue(res.body()?.pending.toString())
                            validate!!.SaveSharepreferenceString(MeetingSP.pendingMeetings,pendingCount)


                            msg = LabelSet.getText(
                                "Datadownloadedsuccessfully",
                                R.string.Datadownloadedsuccessfully
                            )

                        } catch (ex: Exception) {
                            ex.printStackTrace()
                            //                                    progressDialog.dismiss()
                        }
                    } else {
                        msg = "" + res.code() + " " + res.message()
                        code = res.code()
                        idownload = 1
//                            val text = LabelSet.getText("lokos_core",R.string.lokos_core)R.string.data_saved_successfully)
//                            validate!!.CustomAlert( text,this@MasterSyncActivity)
                    }


                    runOnUiThread {
                        if (idownload == 0) {
                            progressDialog.dismiss()
                            var textMsg = LabelSet.getText("",R.string.meeting_summary_approval_pending) + "(" + pendingCount + ")"
                            tv_meeting_summary_approval.text = textMsg
                            val text = msg
                            Toast.makeText(this@VoMyTaskActivity,text, Toast.LENGTH_LONG).show()

                        } else {
                            progressDialog.dismiss()
                            if (code == 403) {
                                Toast.makeText(this@VoMyTaskActivity, msg, Toast.LENGTH_LONG)
                                    .show()
                                CustomAlertlogin()
                            } else {
                                validate!!.CustomAlertVO(msg, this@VoMyTaskActivity)
                                Toast.makeText(this@VoMyTaskActivity, msg, Toast.LENGTH_LONG)
                                    .show()
                            }
                        }

                    }

                } catch (bug: SocketTimeoutException) {
                    progressDialog.dismiss()
//                                Toast.makeText(activity!!, "Socket Timeout", Toast.LENGTH_LONG).show();
                } catch (bug: ConnectTimeoutException) {
                    progressDialog.dismiss()
                    // Toast.makeText(getApplicationContext(), "Connection Timeout", Toast.LENGTH_LONG).show();
                } catch (e: Exception) {
                    e.printStackTrace()
                    progressDialog.dismiss()
                }
            }

        }.start()
    }
    fun CustomAlertlogin() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogelogin, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mDialogView.etUsername.setText(validate!!.RetriveSharepreferenceString(AppSP.userid))
        mDialogView.etPassword.setText(validate!!.returnStringValue(AESEncryption.decrypt(
            validate!!.RetriveSharepreferenceString(AppSP.Password),
            validate!!.RetriveSharepreferenceString(AppSP.userid))))
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.btn_login.setBackgroundColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.txt_dialog_title2.text = LabelSet.getText("app_name",R.string.app_name)
        mDialogView.btn_login.text = LabelSet.getText("log_in",R.string.log_in)
        mDialogView.chk_showHide1.text = LabelSet.getText("show_password",R.string.show_password)
        mDialogView.btn_login.setOnClickListener {
            mAlertDialog.dismiss()
            importtoken(
                mDialogView.etUsername.text.toString(),
                validate!!.returnStringValue(
                    AESEncryption.encrypt(validate!!.returnStringValue(
                        mDialogView.etPassword.text.toString()),mDialogView.etUsername.text.toString()))
            )
        }
        mAlertDialog.chk_showHide1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                mAlertDialog.etPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                mAlertDialog.etPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
        }


    }

    fun importtoken(user: String, pass: String) {
        var grant_type = "password"
        var userId = user
        var password = pass
        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText(
                "authenticate_user",
                R.string.authenticate_user
            )
        )


        val callCount = apiInterface?.gettokenforce(
            grant_type,
            userId,
            password, validate!!.RetriveSharepreferenceInt(AppSP.State_Selected), 1,
            validate!!.RetriveSharepreferenceString(AppSP.Roleid)!!
        )
        callCount?.enqueue(object : Callback<JsonObject> {
            override fun onFailure(callCount: Call<JsonObject>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                progressDialog.dismiss()


                if (response.isSuccessful) {

                    var jsonObject1: JSONObject? = null
                    try {
                        jsonObject1 = JSONObject(response.body().toString())

                        val auth = jsonObject1.get("accessToken").toString()
                        if (auth.trim().length > 0) {
                            validate!!.SaveSharepreferenceString(
                                AppSP.token,
                                validate!!.returnStringValue(
                                    AESEncryption.encrypt(
                                        auth, validate!!.RetriveSharepreferenceString(AppSP.userid)))
                            )
                        }
                        validate!!.CustomAlertVO(
                            LabelSet.getText(
                                "userauthenticatedsuccessfully",
                                R.string.userauthenticatedsuccessfully
                            ), this@VoMyTaskActivity
                        )
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        var jsonObject1 =
                            JSONObject(response.errorBody()!!.source().readUtf8().toString())

                        var resCode =
                            validate!!.returnIntegerValue(jsonObject1.optString("responseCode").toString())
                        validate!!.CustomAlertMsgVO(
                            this@VoMyTaskActivity,
                            responseViewModel,
                            resCode,
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                            validate!!.returnStringValue(jsonObject1.optString("responseMsg").toString())
                        )
                    }

                } else {
                    var jsonObject1 =
                        JSONObject(response.errorBody()!!.source().readUtf8().toString())

                    var resCode =
                        validate!!.returnIntegerValue(jsonObject1.optString("responseCode").toString())
                    validate!!.CustomAlertMsgVO(this@VoMyTaskActivity,
                        responseViewModel,
                        resCode,
                        validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                        validate!!.returnStringValue(jsonObject1.optString("responseMsg").toString())
                    )
                }


            }

        })

    }


}