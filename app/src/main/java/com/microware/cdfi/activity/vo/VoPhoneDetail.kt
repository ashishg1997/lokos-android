package com.microware.cdfi.activity.vo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.api.model.EcScModelJoindata
import com.microware.cdfi.entity.*
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_vo_phone_detail.*
import kotlinx.android.synthetic.main.votablayout.*
import kotlinx.android.synthetic.main.white_toolbar.*

class VoPhoneDetail : AppCompatActivity() {

    var validate: Validate? = null
    var cbophoneDetailEntity: Cbo_phoneEntity? = null
    var cboPhoneViewmodel: CboPhoneViewmodel? = null
    var executiveviewmodel: ExecutiveMemberViewmodel? = null
    var vocode = ""
    var voName = ""
    var lookupViewmodel: LookupViewmodel? = null
    var federationViewmodel: FedrationViewModel? = null
    var scMemberViewmodel: Subcommitee_memberViewModel? = null
    var memberPhoneViewmodel: MemberPhoneViewmodel? = null
    var dataspin_name: List<LookupEntity>? = null
    var dataspinMember: List<EcScModelJoindata>? = null
    var dataspin_belongmemberphone: List<VoShgMemberPhoneEntity>? = null
    var phone_num = ""
    var sValidFrom = 0L

    var cboType = 0
    var childLevel = 0
    var dataspin_status: List<LookupEntity>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vo_phone_detail)

        validate = Validate(this)
        memberPhoneViewmodel = ViewModelProviders.of(this).get(MemberPhoneViewmodel::class.java)
        cboPhoneViewmodel = ViewModelProviders.of(this).get(CboPhoneViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        scMemberViewmodel = ViewModelProviders.of(this).get(Subcommitee_memberViewModel::class.java)
        federationViewmodel = ViewModelProviders.of(this).get(FedrationViewModel::class.java)
        executiveviewmodel = ViewModelProviders.of(this).get(ExecutiveMemberViewmodel::class.java)

        vocode = validate!!.returnStringValue(
            validate!!.RetriveSharepreferenceLong(AppSP.FedrationCode).toString())
        voName = validate!!.RetriveSharepreferenceString(AppSP.FedrationName)!!
        tvCode.text = vocode
        et_membercode.setText(voName)


        if(validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType)==2){
            cboType = 2
            childLevel = 1
        }else {
            cboType = 1
            childLevel = 0
        }

        ivHome.visibility = View.GONE
        tv_title.text = LabelSet.getText(
            "phonedetails",
            R.string.phonedetails
        )
        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary1))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvEc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvVector.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimary1))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_Ec.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this,R.color.white))

        var basicComplete = federationViewmodel!!.getIsCompleteValue(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var addressIsComplete = federationViewmodel!!.getAddressCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var phoneIsComplete = federationViewmodel!!.getPhoneCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var bankIsComplete = federationViewmodel!!.getBankCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var ecIsComplete = federationViewmodel!!.getEcMemberCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var scIsComplete = federationViewmodel!!.getScCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var kycIsComplete = federationViewmodel!!.getKycCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))

        if(basicComplete > 0){
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(ecIsComplete > 0){
            IvEc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvEc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }

        if(addressIsComplete > 0){
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(bankIsComplete > 0){
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(kycIsComplete > 0){
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(scIsComplete > 0){
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }


        ivBack.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation() == 1) {
                    SavePhoneDetail(1)

                    var intent = Intent(this, VoPhoneDetailListActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoPhoneDetailListActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_systemTag.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation() == 1) {
                    SavePhoneDetail(1)

                    var intent = Intent(this, VoSubCommiteeList::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoSubCommiteeList::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }


        lay_mapcbo.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation() == 1) {
                    SavePhoneDetail(1)

                    if (cboType == 1) {
                        var intent = Intent(this, VOMapCBOActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    }else if(cboType == 2){
                        var intent = Intent(this, CLFMapCBOActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    }
                }
            }else {
                if (cboType == 1) {
                    var intent = Intent(this, VOMapCBOActivity::class.java)
                    intent.flags =
                        Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }else if(cboType == 2){
                    var intent = Intent(this, CLFMapCBOActivity::class.java)
                    intent.flags =
                        Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }
        }


        lay_vector.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation() == 1) {
                    SavePhoneDetail(1)

                    var intent = Intent(this, VoBasicDetailActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoBasicDetailActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_Ec.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation() == 1) {
                    SavePhoneDetail(1)

                    var intent = Intent(this, VoEcListActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoEcListActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_location.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation() == 1) {
                    SavePhoneDetail(1)

                    var intent = Intent(this, VoAddressList::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoAddressList::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_bank.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation() == 1) {
                    SavePhoneDetail(1)

                    var intent = Intent(this, VoBankListActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoBankListActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_kyc.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (checkValidation() == 1) {
                    SavePhoneDetail(1)

                    var intent = Intent(this, VoKycDetailList::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoKycDetailList::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        et_validform.setOnClickListener {
            validate!!.datePicker(et_validform)
        }

        et_validtill.setOnClickListener {
            validate!!.datePicker(et_validtill)
        }

        if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) == 1) {
            ivLock.visibility = View.VISIBLE
            btn_savegray.visibility = View.VISIBLE
            btn_add.visibility = View.GONE

        } else {
            ivLock.visibility = View.GONE
            btn_savegray.visibility = View.GONE
            btn_add.visibility = View.VISIBLE
        }
        btn_add.setOnClickListener {
            if (!validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID).isNullOrEmpty()){
                if(checkValidation() == 1){
                    SavePhoneDetail(0)
                }
            }else{
                validate!!.CustomAlertVO(LabelSet.getText(
                    "insert_federation_data_first",
                    R.string.insert_federation_data_first
                ),this,
                    VOProfileActivity::class.java)
            }

        }

        spin_belongMember?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {
                    dataspin_belongmemberphone = scMemberViewmodel!!.getMemberPhone(returnmember_code())
                    fillmemberphoneno(spin_Memberphone, dataspin_belongmemberphone)
                    spin_Memberphone.setSelection(setPhone(phone_num,dataspin_belongmemberphone))
                }else{
                    dataspin_belongmemberphone = scMemberViewmodel!!.getMemberPhone(0)
                    fillmemberphoneno(spin_Memberphone, dataspin_belongmemberphone)

                }

            }override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        fillSpinner()
        showData()

        setLabelText()

    }

    private fun setLabelText() {
        tvCode.text = LabelSet.getText(
            "member_code",
            R.string.member_code
        )
        tvBelongs.text = LabelSet.getText(
            "belongs_to_member",
            R.string.belongs_to_member
        )
        tvPhoneNo.text = LabelSet.getText(
            "phone_no",
            R.string.phone_no
        )
        tvStatus.text = LabelSet.getText(
            "status",
            R.string.status
        )
        tvValidTill.text = LabelSet.getText(
            "valid_till",
            R.string.valid_till
        )
        tvValidForm.text = LabelSet.getText(
            "valid_form",
            R.string.valid_form
        )
        tvIsDefault.text = LabelSet.getText(
            "is_default",
            R.string.is_default
        )
        btn_add.text = LabelSet.getText(
            "add_phone",
            R.string.add_phone
        )
        btn_savegray.text = LabelSet.getText(
            "add_phone",
            R.string.add_phone
        )
        et_membercode.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        et_validform.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )
        et_validtill.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )
    }

    fun returnmemguid(): String {

        var pos = spin_belongMember.selectedItemPosition
        var id = ""

        if (!dataspinMember.isNullOrEmpty()) {
            if (pos > 0) id = dataspinMember!!.get(pos-1).member_guid!!
        }
        return id
    }

    fun returnPhone(): String {

        var pos = spin_Memberphone.selectedItemPosition
        var id = ""

        if (!dataspin_belongmemberphone.isNullOrEmpty()) {
            if (pos > 0) id = dataspin_belongmemberphone!!.get(pos-1).phone_no!!
        }
        return id
    }

    fun setMember(member_guid:String,data: List<EcScModelJoindata>?): Int {

        var pos = 0

        if (!data.isNullOrEmpty()) {
            for (i in data.indices) {
                if (member_guid == data.get(i).member_guid)
                    pos = i + 1
            }
        }
        return pos
    }

    fun setPhone(phone:String,data: List<VoShgMemberPhoneEntity>?): Int {

        var pos = 0

        if (!data.isNullOrEmpty()) {
            for (i in data.indices) {
                if (phone.equals(data.get(i).phone_no))
                    pos = i + 1
            }
        }
        return pos
    }

    private fun fillSpinner() {
        var stringValue = ""
        if(cboType == 1) {
            dataspinMember = scMemberViewmodel!!.getExecutiveMemberList(
                validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)
            )
            stringValue  = LabelSet.getText(
                "noec_memberavailable",
                R.string.noec_memberavailable
            )
        }else if(cboType == 2){
            dataspinMember = scMemberViewmodel!!.getECMemberList(
                validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)
            )
            stringValue  = LabelSet.getText(
                "noec_memberavail",
                R.string.noec_memberavail
            )
        }
        validate!!.fillScMemberSpinner(this,spin_belongMember,dataspinMember,stringValue)
        dataspin_name = lookupViewmodel!!.getlookup(1,validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
        validate!!.fillspinner(this,spin_status,dataspin_name)
    }

    fun fillmemberphoneno(spin: Spinner, data: List<VoShgMemberPhoneEntity>?) {

        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize+1 )
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in data.indices) {
                sValue[i+1] = data[i].phone_no
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue)
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }

    }


    private fun showData() {
        var list = cboPhoneViewmodel!!.getphonedetaildata(validate!!.RetriveSharepreferenceString(AppSP.VoPhoneGUID))
        if (!list.isNullOrEmpty() && list.size > 0){
            spin_belongMember.setSelection(setMember(validate!!.returnStringValue(list.get(0).member_guid),dataspinMember))
            phone_num = validate!!.returnStringValue(list.get(0).mobile_no)
            sValidFrom = validate!!.returnLongValue(list.get(0).valid_from.toString())
        }

    }

    private fun SavePhoneDetail(iAutoSave:Int) {
        if(sValidFrom==0L){
            sValidFrom = validate!!.Daybetweentime(validate!!.currentdatetime)
        }

        var guid = validate!!.random()

        if (validate!!.RetriveSharepreferenceString(AppSP.VoPhoneGUID).isNullOrEmpty()) {
            cbophoneDetailEntity = Cbo_phoneEntity(0,
                validate!!.returnLongValue(vocode),
                validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!,
                guid,
                returnmemguid(),
                returnPhone(),
                cboType,
                0,
                "",
                sValidFrom,
                0,
                1,
                1,
                1,
                1,
                validate!!.Daybetweentime(validate!!.currentdatetime),
                validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                0,
                "",
                0,
                "",1,1,1
            )
            cboPhoneViewmodel!!.insert(cbophoneDetailEntity!!)

            validate!!.updateFederationEditFlag(cboType,validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!,
                validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id),federationViewmodel,executiveviewmodel)

            validate!!.SaveSharepreferenceString(AppSP.VoPhoneGUID,guid)
            if(iAutoSave==0) {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "data_saved_successfully",
                        R.string.data_saved_successfully
                    ),
                    this,
                    VoPhoneDetailListActivity::class.java
                )
            }
        }else {
            if (checkData() == 0) {
                cboPhoneViewmodel!!.updatePhoneDetail(
                    validate!!.RetriveSharepreferenceString(AppSP.VoPhoneGUID)!!,
                    returnmemguid(),
                    returnPhone(),
                    1, "", "",
                    sValidFrom,
                    validate!!.Daybetweentime(et_validtill.text.toString()),
                    1,
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    validate!!.RetriveSharepreferenceString(AppSP.userid)!!, 1
                )

                validate!!.updateFederationEditFlag(
                    cboType,
                    validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!,
                    validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id),
                    federationViewmodel,
                    executiveviewmodel
                )

                if (iAutoSave == 0) {
                    validate!!.CustomAlertVO(
                        LabelSet.getText(
                            "updated_successfully",
                            R.string.updated_successfully
                        ),
                        this,
                        VoPhoneDetailListActivity::class.java
                    )
                }
            }else if (iAutoSave == 0) {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "updated_successfully",
                        R.string.updated_successfully
                    ),
                    this,
                    VoPhoneDetailListActivity::class.java
                )
            }
        }
    }

    fun checkValidation():Int{
        var value = 1
        val phonecount = cboPhoneViewmodel!!.getphone_count(returnPhone(),cboType)
        if (spin_belongMember.selectedItemPosition == 0){
            validate!!.CustomAlertSpinner(this,spin_belongMember,LabelSet.getText(
                "please_select",
                R.string.please_select
            )+" "+ LabelSet.getText(
                "belongs_to_member",
                R.string.belongs_to_member
            ))
            value = 0
            return value
        }else if (spin_Memberphone.selectedItemPosition == 0){
            validate!!.CustomAlertSpinner(this,spin_Memberphone,LabelSet.getText(
                "please_select",
                R.string.please_select
            )+" "+ LabelSet.getText("phone_no", R.string.phone_no))
            value = 0
            return value
        }else if ((validate!!.RetriveSharepreferenceString(AppSP.VoPhoneGUID)!!.trim().length==0 && phonecount > 0) ||
            (validate!!.RetriveSharepreferenceString(AppSP.VoPhoneGUID)!!.trim().length>0 && phonecount > 1)){
            validate!!.CustomAlertSpinner(this,spin_Memberphone,LabelSet.getText(
                "phone_no_exists",
                R.string.phone_no_exists
            ))
            value = 0
            return value
        }

        return value

    }

    override fun onBackPressed() {
        var intent = Intent(this, VoPhoneDetailListActivity::class.java)
        intent.flags=Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }

    fun returnmember_code(): Long {

        var pos = spin_belongMember.selectedItemPosition
        var id:Long = 0L

        if (!dataspinMember.isNullOrEmpty()) {
            if (pos > 0) id = dataspinMember!!.get(pos-1).member_cd!!
        }
        return id
    }
    fun returnmember_guid(): String {

        var pos = spin_Memberphone.selectedItemPosition
        var guid:String = ""

        if (!dataspinMember.isNullOrEmpty()) {
            if (pos > 0) guid = validate!!.returnStringValue(dataspinMember!!.get(pos-1).member_guid)
        }
        return guid
    }

    private fun checkData(): Int {
        var value = 1

        var list = cboPhoneViewmodel!!.getphonedetaildata(validate!!.RetriveSharepreferenceString(AppSP.VoPhoneGUID))

        if (spin_belongMember.selectedItemPosition != validate!!.returnIntegerValue(
                list?.get(
                    0
                )?.member_guid
            )
        ) {

            value = 0

        }


        return value
    }
}
