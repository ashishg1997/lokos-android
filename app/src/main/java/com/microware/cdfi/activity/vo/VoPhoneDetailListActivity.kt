package com.microware.cdfi.activity.vo

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.VoPhoneAdapter
import com.microware.cdfi.api.model.EcScModelJoindata
import com.microware.cdfi.entity.Cbo_phoneEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.CboPhoneViewmodel
import com.microware.cdfi.viewModel.ExecutiveMemberViewmodel
import com.microware.cdfi.viewModel.FedrationViewModel
import com.microware.cdfi.viewModel.Subcommitee_memberViewModel
import kotlinx.android.synthetic.main.activity_vo_phone_detail_list.*
import kotlinx.android.synthetic.main.activity_vo_phone_detail_list.btnaddgrey
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.votablayout.*
import kotlinx.android.synthetic.main.white_toolbar.*

class VoPhoneDetailListActivity : AppCompatActivity() {

    var validate: Validate? = null
    var cboPhoneViewmodel: CboPhoneViewmodel? = null
    var scMemberViewmodel: Subcommitee_memberViewModel? = null
    var federationViewmodel: FedrationViewModel? = null
    var executiveviewmodel: ExecutiveMemberViewmodel? = null
    var dataspinMember:List<EcScModelJoindata>? = null

    var cboType = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vo_phone_detail_list)

        ivBack.setOnClickListener {
            var intent = Intent(this, VOListActivity::class.java)
            intent.flags=Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

        validate = Validate(this)
        cboPhoneViewmodel = ViewModelProviders.of(this).get(CboPhoneViewmodel::class.java)
        scMemberViewmodel = ViewModelProviders.of(this).get(Subcommitee_memberViewModel::class.java)
        federationViewmodel = ViewModelProviders.of(this).get(FedrationViewModel::class.java)
        executiveviewmodel = ViewModelProviders.of(this).get(ExecutiveMemberViewmodel::class.java)

        if(validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType)==2){
            cboType = 2
        }else {
            cboType = 1
        }
        ivHome.visibility = View.GONE
        tv_title.text = LabelSet.getText(
            "phonedetails",
            R.string.phonedetails
        )
        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary1))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvEc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        tvVector.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimary1))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_Ec.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this,R.color.white))

        var basicComplete = federationViewmodel!!.getIsCompleteValue(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var addressIsComplete = federationViewmodel!!.getAddressCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var phoneIsComplete = federationViewmodel!!.getPhoneCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var bankIsComplete = federationViewmodel!!.getBankCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var ecIsComplete = federationViewmodel!!.getEcMemberCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var scIsComplete = federationViewmodel!!.getScCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var kycIsComplete = federationViewmodel!!.getKycCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))

        if(basicComplete > 0){
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(ecIsComplete > 0){
            IvEc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvEc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }

        if(addressIsComplete > 0){
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(bankIsComplete > 0){
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(kycIsComplete > 0){
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(scIsComplete > 0){
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }

        lay_systemTag.setOnClickListener {
            var intent = Intent(this, VoSubCommiteeList::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_mapcbo.setOnClickListener {
            if (cboType == 1) {
                var intent = Intent(this, VOMapCBOActivity::class.java)
                intent.flags =
                    Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }else if(cboType == 2){
                var intent = Intent(this, CLFMapCBOActivity::class.java)
                intent.flags =
                    Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_location.setOnClickListener {
            var intent = Intent(this, VoAddressList::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_bank.setOnClickListener {
            var intent = Intent(this, VoBankListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_kyc.setOnClickListener {
            var intent = Intent(this, VOKycDetailActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_Ec.setOnClickListener {
            var intent = Intent(this, VoEcListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) == 1) {
            ivLock.visibility = View.VISIBLE
            btnaddgrey.visibility = View.VISIBLE
            addPhone.visibility = View.GONE
        } else {
            ivLock.visibility = View.GONE
            addPhone.visibility = View.VISIBLE
            btnaddgrey.visibility = View.GONE

        }
        addPhone!!.setOnClickListener {
            if (!validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID).isNullOrEmpty()) {
                validate!!.SaveSharepreferenceString(AppSP.VoPhoneGUID, "")
                var inetnt = Intent(this, VoPhoneDetail::class.java)
                startActivity(inetnt)
                finish()
            }else{
                validate!!.CustomAlertVO(getString(R.string.insert_federation_data_first),this,
                    VoBasicDetailActivity::class.java)
            }
        }

        fillData()

    }

    private fun fillData() {
        cboPhoneViewmodel!!.getphonedata(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))!!
            .observe(this,object : Observer<List<Cbo_phoneEntity>> {
                override fun onChanged(cbo_list: List<Cbo_phoneEntity>?) {
                    if (cbo_list!=null){
                        rvPhoneList.layoutManager = LinearLayoutManager(this@VoPhoneDetailListActivity)
                        rvPhoneList.adapter = VoPhoneAdapter(this@VoPhoneDetailListActivity, cbo_list)
                        if(cbo_list.size>0){
                            lay_nophoneavialable.visibility = View.GONE
                        }else {
                            lay_nophoneavialable.visibility = View.VISIBLE
                            tv_nophone_avialable.text = LabelSet.getText(
                                "noec_memberavailable",
                                R.string.noec_memberavailable
                            )
                        }
                    }
                }
            })
    }

    override fun onBackPressed() {
        var intent = Intent(this, VOListActivity::class.java)
        startActivity(intent)
        finish()
    }

    fun returnMemberName(memberGuid:String): String {
        var sValue = ""
        if(validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType)==1){
            dataspinMember = scMemberViewmodel!!.getExecutiveMemberList(
                validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)
            )
        }else  if(validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType)==2){
            dataspinMember = scMemberViewmodel!!.getECMemberList(
                validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)
            )
        }
        if (!dataspinMember.isNullOrEmpty()) {
            for (i in dataspinMember!!.indices) {
                if (memberGuid.equals(dataspinMember!!.get(i).member_guid))
                    sValue = dataspinMember!!.get(i).member_name!!
            }
        }
        return sValue
    }

    fun CustomAlert(guid:String,iFlag:Int) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_dialog_title.setTextColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.btn_yes.setBackgroundColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.btn_no.setBackgroundColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.txt_msg.text = LabelSet.getText(
            "do_u_want_to_delete",
            R.string.do_u_want_to_delete
        )
        mDialogView.btn_yes.setOnClickListener {

            if(iFlag==0){
                cboPhoneViewmodel!!.deleteRecord(guid)
            }else {
                cboPhoneViewmodel!!.deleteData(guid)
                validate!!.updateFederationEditFlag(cboType,validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!,
                    validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id),federationViewmodel,executiveviewmodel)

            }
            mAlertDialog.dismiss()

        }

        mDialogView.btn_no.setOnClickListener {

            mAlertDialog.dismiss()

        }
    }
}
