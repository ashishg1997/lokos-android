package com.microware.cdfi.activity.vo

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.microware.cdfi.R
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.api.model.EcScModelJoindata
import com.microware.cdfi.api.model.SubcommitteeModel
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.entity.subcommitee_masterEntity
import com.microware.cdfi.entity.subcommitee_memberEntity
import com.microware.cdfi.entity.subcommitteeEntity
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_vo_sub_commitee_detail.*
import kotlinx.android.synthetic.main.customealertdialogelogin.*
import kotlinx.android.synthetic.main.customealertdialogelogin.view.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.layout_sc_member.*
import kotlinx.android.synthetic.main.layout_sc_member.btn_save
import kotlinx.android.synthetic.main.layout_sc_member.btn_savegray
import kotlinx.android.synthetic.main.layout_sc_member.et_shgcode
import kotlinx.android.synthetic.main.layout_sc_member.et_toDate
import kotlinx.android.synthetic.main.layout_sc_member.lay_status
import kotlinx.android.synthetic.main.layout_sc_member.lay_toDate
import kotlinx.android.synthetic.main.layout_sc_member.spin_committee
import kotlinx.android.synthetic.main.layout_sc_member.spin_status
import kotlinx.android.synthetic.main.layout_sc_member.tvToDate
import kotlinx.android.synthetic.main.layout_sc_member.tvsubcommitee
import kotlinx.android.synthetic.main.votablayout.*
import kotlinx.android.synthetic.main.white_toolbar.*
import okhttp3.MediaType
import okhttp3.RequestBody
import org.apache.http.conn.ConnectTimeoutException
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException

class VoSCMemberActivity : AppCompatActivity() {

    var scMemberEntity: subcommitee_memberEntity? = null
    var committeeViewModel: SubcommitteeViewModel? = null
    var federationViewmodel: FedrationViewModel? = null
    var validate: Validate?=null
    var dataspincommittee:List<subcommitee_masterEntity>? = null
    var dataspinMember:List<EcScModelJoindata>? = null
    var committeeMasterViewmodel: subcommitteeMasterViewModel? = null
    var scMemberViewmodel: Subcommitee_memberViewModel? = null
    var lookupviewmodel: LookupViewmodel? = null
    var executiveviewmodel: ExecutiveMemberViewmodel? = null
    var datastatus:List<LookupEntity>? = null
    var ec_joining_date = 0L
    var cboType = 0
    var childLevel = 0
    var apiInterface: ApiInterface? = null
    var responseViewModel: ResponseViewModel? = null
    var iButtonPressed = 0
    var isUpdate = 0

    internal lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_sc_member)

        committeeMasterViewmodel = ViewModelProviders.of(this).get(subcommitteeMasterViewModel::class.java)
        committeeViewModel = ViewModelProviders.of(this).get(SubcommitteeViewModel::class.java)
        scMemberViewmodel = ViewModelProviders.of(this).get(Subcommitee_memberViewModel::class.java)
        federationViewmodel = ViewModelProviders.of(this).get(FedrationViewModel::class.java)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        responseViewModel = ViewModelProviders.of(this).get(ResponseViewModel::class.java)
        lookupviewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        executiveviewmodel = ViewModelProviders.of(this).get(ExecutiveMemberViewmodel::class.java)
        animationView.visibility = View.VISIBLE
        validate = Validate(this)

        if(validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType)==2){
            cboType = 2
            childLevel = 1
        }else {
            cboType = 1
            childLevel = 0
        }

        spin_committee.isEnabled = false
        ivBack.setOnClickListener {

                var intent = Intent(this, VoSubCommiteeList::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)

        }

        ivHome.visibility = View.GONE
        tv_title.text = LabelSet.getText(
            "sub_committee_member",
            R.string.sub_committee_member
        )
        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvEc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary1))
        tvVector.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_Ec.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimary1))

        var basicComplete = federationViewmodel!!.getIsCompleteValue(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var addressIsComplete = federationViewmodel!!.getAddressCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var phoneIsComplete = federationViewmodel!!.getPhoneCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var bankIsComplete = federationViewmodel!!.getBankCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var ecIsComplete = federationViewmodel!!.getEcMemberCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var scIsComplete = federationViewmodel!!.getScCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var kycIsComplete = federationViewmodel!!.getKycCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))

        if(basicComplete > 0){
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(ecIsComplete > 0){
            IvEc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvEc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(phoneIsComplete > 0){
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(addressIsComplete > 0){
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(bankIsComplete > 0){
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }

        if(kycIsComplete > 0){
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }

        lay_mapcbo.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if(validate!!.RetriveSharepreferenceInt(AppSP.mappingRefreshed) == 1) {
                    if (sCheckValidation() == 1) {
                        saveData(1)

                        if (cboType == 1) {
                            var intent = Intent(this, VOMapCBOActivity::class.java)
                            intent.flags =
                                Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                            overridePendingTransition(0, 0)
                        } else if (cboType == 2) {
                            var intent = Intent(this, CLFMapCBOActivity::class.java)
                            intent.flags =
                                Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                            overridePendingTransition(0, 0)
                        }
                    }
                }else {
                    if (cboType == 1) {
                        var intent = Intent(this, VOMapCBOActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    } else if (cboType == 2) {
                        var intent = Intent(this, CLFMapCBOActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    }
                }
            }else {
                if (cboType == 1) {
                    var intent = Intent(this, VOMapCBOActivity::class.java)
                    intent.flags =
                        Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }else if(cboType == 2){
                    var intent = Intent(this, CLFMapCBOActivity::class.java)
                    intent.flags =
                        Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }
        }

        lay_vector.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if(validate!!.RetriveSharepreferenceInt(AppSP.mappingRefreshed) == 1) {
                    if (sCheckValidation() == 1) {
                        saveData(1)

                        var intent = Intent(this, VoBasicDetailActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    }
                }else {
                    var intent = Intent(this, VoBasicDetailActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoBasicDetailActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_kyc.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if(validate!!.RetriveSharepreferenceInt(AppSP.mappingRefreshed) == 1) {
                    if (sCheckValidation() == 1) {
                        saveData(1)

                        var intent = Intent(this, VoKycDetailList::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    }
                }else {
                    var intent = Intent(this, VoKycDetailList::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoKycDetailList::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_Ec.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if(validate!!.RetriveSharepreferenceInt(AppSP.mappingRefreshed) == 1) {
                    if (sCheckValidation() == 1) {
                        saveData(1)

                        var intent = Intent(this, VoEcListActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    }
                }else {
                    var intent = Intent(this, VoEcListActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoEcListActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }


        lay_phone.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if(validate!!.RetriveSharepreferenceInt(AppSP.mappingRefreshed) == 1) {
                    if (sCheckValidation() == 1) {
                        saveData(1)

                        var intent = Intent(this, VoPhoneDetailListActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    }
                }else {
                    var intent = Intent(this, VoPhoneDetailListActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoPhoneDetailListActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_location.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if(validate!!.RetriveSharepreferenceInt(AppSP.mappingRefreshed) == 1) {
                    if (sCheckValidation() == 1) {
                        saveData(1)

                        var intent = Intent(this, VoAddressList::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    }
                }else {
                    var intent = Intent(this, VoAddressList::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoAddressList::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_bank.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if(validate!!.RetriveSharepreferenceInt(AppSP.mappingRefreshed) == 1) {
                    if (sCheckValidation() == 1) {
                        saveData(1)

                        var intent = Intent(this, VoBankListActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    }
                }else {
                    var intent = Intent(this, VoBankListActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoBankListActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        et_joining_dt.setOnClickListener {
            if(spin_ecmembername.selectedItemPosition>0) {
                if(ec_joining_date<validate!!.RetriveSharepreferenceLong(AppSP.scFromDate)) {
                    validate!!.datePickerwithmindate(
                        validate!!.RetriveSharepreferenceLong(AppSP.scFromDate),
                        et_joining_dt
                    )
                }else {
                    validate!!.datePickerwithmindate(
                        ec_joining_date,
                        et_joining_dt
                    )
                }
            }else {
                validate!!.CustomAlertSpinner(this,spin_ecmembername,
                    LabelSet.getText(
                        "please_select",
                        R.string.please_select
                    )+" " +LabelSet.getText(
                        "ec_member_name",
                        R.string.ec_member_name
                    ))

            }
        }

        et_toDate.setOnClickListener {
            validate!!.datePickerwithmindate(
                validate!!.Daybetweentime(et_joining_dt.text.toString()),
                et_toDate
            )
        }
        if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) == 1) {
            ivLock.visibility = View.VISIBLE
            btn_savegray.visibility = View.VISIBLE
            tbl_refresh_save.visibility = View.GONE

        } else {
            ivLock.visibility = View.GONE
            btn_savegray.visibility = View.GONE
            tbl_refresh_save.visibility = View.VISIBLE
        }

        btn_refresh.setOnClickListener {
            validate!!.SaveSharepreferenceInt(AppSP.mappingRefreshed, 1)
            if(cboType == 1){
                importVO_SHG_Mapping()
            }else if(cboType == 2){
                importCLF_VO_Mapping()
            }
        }
        btn_save.setOnClickListener {
            if (sCheckValidation() == 1) {
                saveData(0)
            }
        }
        fillSpinner()
        showdata()
        setLabelText()
    }

    private fun setLabelText() {
        tvFEdCode.text = LabelSet.getText(
            "fedName",
            R.string.fedName
        )
        tvsubcommitee.text = LabelSet.getText(
            "sub_committee",
            R.string.sub_committee
        )
        tv_EcMemberName.text = LabelSet.getText(
            "ec_member_name",
            R.string.ec_member_name
        )
        tvstatus.text = LabelSet.getText(
            "status",
            R.string.status
        )
        tv_DOJ.text = LabelSet.getText(
            "from_date",
            R.string.from_date
        )
        tvToDate.text = LabelSet.getText(
            "to_Date",
            R.string.to_Date
        )
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_savegray.text = LabelSet.getText(
            "save",
            R.string.save
        )
        tv_DOJ.text = LabelSet.getText(
            "date_of_joining",
            R.string.date_of_joining
        )
        et_joining_dt.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )
    }

    private fun fillSpinner(){
        var stringValue = ""
        datastatus = lookupviewmodel!!.getlookup(5,validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
        validate!!.fillspinner(this,spin_status,datastatus)
        dataspincommittee = committeeMasterViewmodel!!.getScMaster(validate!!.RetriveSharepreferenceString(
            AppSP.Langaugecode))
        validate!!.fillSubcommitteeSpinner(this,spin_committee,dataspincommittee)
        if(cboType == 1) {
            dataspinMember = scMemberViewmodel!!.getExecutiveMemberList(
                validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)
            )
            stringValue  = LabelSet.getText(
                "ec_not_available",
                R.string.ec_not_available
            )
        }else if(cboType == 2){
            dataspinMember = scMemberViewmodel!!.getECMemberList(
                validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)
            )
            stringValue  = LabelSet.getText(
                "ec_not_avail",
                R.string.ec_not_avail
            )
        }
        validate!!.fillScMemberSpinner(this,spin_ecmembername,dataspinMember,stringValue)

        spin_ecmembername.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (position > 0) {

                    ec_joining_date =returnmember_joining()

                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        spin_status?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if(position>0){
                    var status = validate!!.returnlookupcode(spin_status,datastatus)
                    if(status == 2){
                        et_toDate.isEnabled = true
                        et_toDate.setText(validate!!.convertDatetime(validate!!.Daybetweentime(validate!!.currentdatetime)))
                    }else {
                        et_toDate.isEnabled = false
                        et_toDate.setText("")
                    }
                }else {
                    et_toDate.setText("")
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }
    }

    private fun saveData(iAutoSave:Int) {
        if (isNetworkConnected()) {
            var statusValue=0
            if(lay_status.visibility==View.GONE){
                statusValue = 1
            }else if(lay_status.visibility==View.VISIBLE){
                statusValue = validate!!.returnlookupcode(spin_status, datastatus)
            }

            if (validate!!.RetriveSharepreferenceString(AppSP.SubCommGuid)
                    .isNullOrEmpty() || validate!!.RetriveSharepreferenceLong(AppSP.SCMemebrCode) == 0L
            ) {
                isUpdate = 0
                scMemberEntity = subcommitee_memberEntity(
                    validate!!.returnStringValue(validate!!.RetriveSharepreferenceString(AppSP.SubCommGuid)),
                    0,
                    returnmember_code(),
                    returnmember_guid(),
                    returnmember_code(),
                    validate!!.Daybetweentime(et_joining_dt.text.toString()),
                    0, 1, 1, 1, 1,
                    0, "",
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    validate!!.RetriveSharepreferenceString(AppSP.userid),
                    0,
                    "", 0, 1
                )
                scMemberViewmodel!!.insert(scMemberEntity!!)
                validate!!.SaveSharepreferenceLong(AppSP.SCMemebrCode, returnmember_code())
                var totalScMember = scMemberViewmodel!!.getCommitteeMemberCount(validate!!.RetriveSharepreferenceString(AppSP.SubCommGuid))
                if(totalScMember>=2) {
                    committeeViewModel!!.updateScCompleteStatus(
                        validate!!.RetriveSharepreferenceString(
                            AppSP.SubCommGuid
                        )!!, validate!!.Daybetweentime(validate!!.currentdatetime)
                    )
                }
                setFederationEditFlag()

                uploadSCdata(iAutoSave, isUpdate)

            } else {
                isUpdate = 1
                scMemberViewmodel!!.updateScMemberdata(
                    validate!!.returnStringValue(validate!!.RetriveSharepreferenceString(AppSP.SubCommGuid)),
                    validate!!.RetriveSharepreferenceLong(AppSP.SCMemebrCode),
                    validate!!.Daybetweentime(et_joining_dt.text.toString()),
                    validate!!.Daybetweentime(et_toDate.text.toString()),
                    statusValue,
                    1,
                    1,
                    1,
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                    1
                )
                var totalScMember = scMemberViewmodel!!.getCommitteeMemberCount(validate!!.RetriveSharepreferenceString(AppSP.SubCommGuid))

                if(totalScMember>=2) {
                    committeeViewModel!!.updateScCompleteStatus(
                        validate!!.RetriveSharepreferenceString(
                            AppSP.SubCommGuid
                        )!!, validate!!.Daybetweentime(validate!!.currentdatetime)
                    )
                    uploadSCdata(iAutoSave, isUpdate)
                }
                setFederationEditFlag()


            }
        } else {
            validate!!.CustomAlertVO(
                LabelSet.getText(
                    "no_internet_msg",
                    R.string.no_internet_msg
                ), this
            )
        }
    }

    private fun showdata(){
        et_shgcode.setText(validate!!.RetriveSharepreferenceString(AppSP.FedrationName))
        var scId = validate!!.returnIntegerValue(validate!!.RetriveSharepreferenceInt(AppSP.SCTypeId).toString())
        spin_committee.setSelection(validate!!.returnSubcommitteePos(scId,dataspincommittee))
        if(validate!!.RetriveSharepreferenceLong(AppSP.SCMemebrCode)>0 &&
            !validate!!.RetriveSharepreferenceString(AppSP.SubCommGuid).isNullOrEmpty()){
            var list = scMemberViewmodel!!.getCommitteeMemberdata(validate!!.RetriveSharepreferenceString(
                AppSP.SubCommGuid),validate!!.RetriveSharepreferenceLong(AppSP.SCMemebrCode))
            if(!list.isNullOrEmpty()){
                spin_ecmembername.setSelection(setMember(validate!!.returnLongValue(list.get(0).ec_member_code.toString()),dataspinMember))
                if(validate!!.returnLongValue(list.get(0).fromdate.toString())>0) {
                    et_joining_dt.setText(validate!!.convertDatetime(list.get(0).fromdate!!))
                }
                if(validate!!.returnIntegerValue(list.get(0).status.toString())>0){
                    spin_status.setSelection(validate!!.returnlookupcodepos(validate!!.returnIntegerValue(list.get(0).status.toString()),datastatus))
                }
            }
        }else {
            lay_status.visibility=View.GONE
            lay_toDate.visibility=View.GONE
        }
    }

    private fun sCheckValidation():Int{

        var scCode = validate!!.returnIntegerValue(validate!!.RetriveSharepreferenceInt(AppSP.SCTypeId).toString())
        var memberCountwithinSC = scMemberViewmodel!!.getScMembercountbySCGUID(returnmember_code(),validate!!.RetriveSharepreferenceString(AppSP.SubCommGuid))
        var scMemberCount = scMemberViewmodel!!.getScMembercount(returnmember_code())
        var totalScMember = scMemberViewmodel!!.getCommitteeMemberCount(validate!!.RetriveSharepreferenceString(AppSP.SubCommGuid))
        var value = 1
        if (spin_committee.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(this,spin_committee,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                )+" " +LabelSet.getText(
                    "sub_committee",
                    R.string.sub_committee
                ))
            value = 0
            return value
        }else if (spin_ecmembername.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(this,spin_ecmembername,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                )+" " +LabelSet.getText(
                    "ec_member_name",
                    R.string.ec_member_name
                ))
            value = 0
            return value
        }else if (et_joining_dt.text.toString().trim().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                )+" " +LabelSet.getText(
                    "date_of_joining",
                    R.string.date_of_joining
                ),
                this,et_joining_dt)
            value = 0
            return value
        }else if(scCode==6 &&((validate!!.RetriveSharepreferenceLong(AppSP.SCMemebrCode)==0L && memberCountwithinSC>0) ||
            (validate!!.RetriveSharepreferenceLong(AppSP.SCMemebrCode)>0L && memberCountwithinSC>1) ||
            (validate!!.RetriveSharepreferenceLong(AppSP.SCMemebrCode)>0L &&
                    validate!!.RetriveSharepreferenceLong(AppSP.SCMemebrCode) != returnmember_code() && memberCountwithinSC>0))){
            validate!!.CustomAlertSpinner(this,spin_ecmembername,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                )+" " +LabelSet.getText(
                    "valid_sc_member",
                    R.string.valid_sc_member
                ))
            value = 0
            return value
        }else if(scCode!=6 &&((validate!!.RetriveSharepreferenceLong(AppSP.SCMemebrCode)==0L && scMemberCount>0) ||
            (validate!!.RetriveSharepreferenceLong(AppSP.SCMemebrCode)>0L && scMemberCount>1) ||
            (validate!!.RetriveSharepreferenceLong(AppSP.SCMemebrCode)>0L &&
                    validate!!.RetriveSharepreferenceLong(AppSP.SCMemebrCode) != returnmember_code() && scMemberCount>0))){
            validate!!.CustomAlertSpinner(this,spin_ecmembername,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                )+" " +LabelSet.getText(
                    "valid_sc_member",
                    R.string.valid_sc_member
                ))
            value = 0
            return value
        }else if(validate!!.RetriveSharepreferenceLong(AppSP.SCMemebrCode)==0L && totalScMember==5){
            validate!!.CustomAlertVO(LabelSet.getText(
                "valid_member_count",
                R.string.valid_member_count
            ),this)
            value = 0
            return value
        }
        return value
    }

    fun returnmember_code(): Long {

        var pos = spin_ecmembername.selectedItemPosition
        var id:Long = 0L

        if (!dataspinMember.isNullOrEmpty()) {
            if (pos > 0) id = dataspinMember!!.get(pos-1).member_cd!!
        }
        return id
    }

    fun returnmember_joining(): Long {

        var pos = spin_ecmembername.selectedItemPosition
        var joining:Long = 0L

        if (!dataspinMember.isNullOrEmpty()) {
            if (pos > 0) joining = dataspinMember!!.get(pos-1).join_date!!
        }
        return joining
    }
    fun returnmember_guid(): String {

        var pos = spin_ecmembername.selectedItemPosition
        var guid:String = ""

        if (!dataspinMember.isNullOrEmpty()) {
            if (pos > 0) guid = validate!!.returnStringValue(dataspinMember!!.get(pos-1).member_guid)
        }
        return guid
    }

    fun setMember(memberCode:Long,data: List<EcScModelJoindata>?): Int {

        var pos = 0

        if (!data.isNullOrEmpty()) {
            for (i in data.indices) {
                if (memberCode == data.get(i).member_cd)
                    pos = i + 1
            }
        }
        return pos
    }

    private fun isNetworkConnected(): Boolean {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager //1
        val networkInfo = connectivityManager.activeNetworkInfo //2
        return networkInfo != null && networkInfo.isConnected //3
    }


    fun importVO_SHG_Mapping() {
        var iEcDownload = 0
        if (isNetworkConnected()) {
            progressDialog = ProgressDialog.show(
                this, LabelSet.getText(
                    "app_name",
                    R.string.app_name
                ),
                LabelSet.getText(
                    "DataLoading",
                    R.string.DataLoading
                )
            )
            progressDialog.setIcon(this.getDrawable(R.drawable.appicon))
            var idownload = 0
            var code = 0
            var msg = ""
            object : Thread() {

                //@SuppressLint("NewApi")
                override fun run() {
                    try {

                        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                        val token = validate!!.returnStringValue(
                            AESEncryption.decrypt(
                            validate!!.RetriveSharepreferenceString(AppSP.token),
                            validate!!.RetriveSharepreferenceString(AppSP.userid)
                        ))

                        val call = apiInterface?.getVO_Shg_Mappingdata(
                            token,
                            user,
                            validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!,
                            cboType
                        )

                        val res = call?.execute()

                        if (res!!.isSuccessful) {
                            if (!res.body()?.VO_Shg_mapped_response.isNullOrEmpty()) {
                                try {
                                    var mapped_Shg = res.body()?.VO_Shg_mapped_response
                                    var shglist = MappingData.returnVO_Shg_listObj(mapped_Shg)
                                    if (!shglist.isNullOrEmpty()) {
                                        CDFIApplication.database?.mappedShgDao()
                                            ?.insertMapped_data(shglist)
                                    }
                                    for (i in mapped_Shg!!.indices) {
                                        var member_details_list = mapped_Shg.get(i).memberDetailsList
                                        var member_list =
                                            MappingData.returnVO_Shgmemberentity(member_details_list)
                                        CDFIApplication.database?.voShgMemberDao()?.insertMapped_Memberdata(member_list)
                                        for(j in member_details_list!!.indices){
                                            var memberPhoneList = member_details_list.get(j).memberPhoneDetailsList!!
                                            CDFIApplication.database?.vOShgMemberPhoneDao()?.insertMapped_MemberPhonedata((memberPhoneList))
                                            CDFIApplication.database?.voShgMemberDao()?.updateVO_shgMember(mapped_Shg.get(i).guid,mapped_Shg.get(i).shg_name,member_details_list.get(j).member_guid,mapped_Shg.get(i).vo_guid)
                                            federationViewmodel!!.updateFedrationCode(mapped_Shg.get(i).vo_code,mapped_Shg.get(i).vo_guid)
                                        }


                                    }
                                    idownload = 0
                                    iEcDownload = getFederationdata()
                                    if(idownload == 0 && iEcDownload == 1){
                                        idownload = 0
                                    }else {
                                        idownload = 1
                                    }
                                    progressDialog.dismiss()
                                } catch (ex: Exception) {
                                    ex.printStackTrace()
//                                    progressDialog.dismiss()
                                }

                            }else {
                                idownload = 0
                            }
                        } else {
                            var resMsg = ""
                            idownload = 1
                            if (res.code() == 403) {

                                code = res.code()
                                msg = res.message()
                            } else {
                                if(res.errorBody()?.contentLength()==0L || res.errorBody()?.contentLength()!!<0L){
                                    code = res.code()
                                    resMsg = res.message()
                                }else {
                                    var jsonObject1 =
                                        JSONObject(res.errorBody()!!.source().readUtf8().toString())

                                    code =
                                        validate!!.returnIntegerValue(
                                            jsonObject1.optString("responseCode").toString()
                                        )
                                    resMsg = validate!!.returnStringValue(
                                        jsonObject1.optString("responseMsg").toString()
                                    )
                                }
                                msg = "" + validate!!.alertMsg(
                                    this@VoSCMemberActivity,
                                    responseViewModel,
                                    code,
                                    validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                                    resMsg)

                            }
                        }

                        runOnUiThread {
                            if (idownload == 0) {
                                progressDialog.dismiss()
                                iButtonPressed = 1
                                val text = LabelSet.getText(
                                    "Datadownloadedsuccessfully",
                                    R.string.Datadownloadedsuccessfully
                                )
                                validate!!.CustomAlertVO(text,this@VoSCMemberActivity)
                            }else {
                                progressDialog.dismiss()
                                if (code == 403) {
                                    Toast.makeText(this@VoSCMemberActivity, msg, Toast.LENGTH_LONG)
                                        .show()
                                    CustomAlertlogin()
                                } else {
                                    val text = LabelSet.getText(
                                        "DatadownloadingFailed",
                                        R.string.DatadownloadingFailed
                                    )
                                    validate!!.CustomAlertVO(msg, this@VoSCMemberActivity)

                                }

                            }

                        }

                    } catch (bug: SocketTimeoutException) {
                        progressDialog.dismiss()
//                                Toast.makeText(activity!!, "Socket Timeout", Toast.LENGTH_LONG).show();
                    } catch (bug: ConnectTimeoutException) {
                        progressDialog.dismiss()
                        // Toast.makeText(getApplicationContext(), "Connection Timeout", Toast.LENGTH_LONG).show();
                    } catch (e: Exception) {
                        e.printStackTrace()
                        progressDialog.dismiss()
                    }
                }

            }.start()

        } else {
            AlertDialog.Builder(this).setTitle("No Internet Connection")
                .setMessage("Please check your internet connection and try again")
                .setPositiveButton(android.R.string.ok) { _, _ -> }
                .setIcon(android.R.drawable.ic_dialog_alert).show()
        }
    }

    fun importCLF_VO_Mapping() {
        var iEcDownload = 0
        if (isNetworkConnected()) {
            progressDialog = ProgressDialog.show(
                this, LabelSet.getText(
                    "app_name",
                    R.string.app_name
                ),
                LabelSet.getText(
                    "DataLoading",
                    R.string.DataLoading
                )
            )
            progressDialog.setIcon(this.getDrawable(R.drawable.appicon))
            var idownload = 0
            var code = 0
            var msg=""
            object : Thread() {

                //@SuppressLint("NewApi")
                override fun run() {
                    try {


                        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                        val token = validate!!.returnStringValue(AESEncryption.decrypt(
                            validate!!.RetriveSharepreferenceString(AppSP.token),
                            validate!!.RetriveSharepreferenceString(AppSP.userid)
                        ))

                        val call = apiInterface?.getCLF_VO_Shg_Mappingdata(
                            token,
                            user,
                            validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!,
                            cboType
                        )

                        val res = call?.execute()

                        if (res!!.isSuccessful) {
                            if (!res.body()?.CLF_VO_Shg_mapped_response.isNullOrEmpty()) {
                                try {
                                    var mapped_Vo = res.body()?.CLF_VO_Shg_mapped_response
                                    var volist = MappingData.returnCLF_VO_listObj(mapped_Vo)
                                    if (!volist.isNullOrEmpty()) {
                                        CDFIApplication.database?.mappedVoDao()
                                            ?.insertMapped_data(volist)
                                    }
                                    for (i in mapped_Vo!!.indices) {
                                        var member_details_list = mapped_Vo.get(i).ecMemberDetailsForMappingList
                                        var member_list =
                                            MappingData.returnClf_VO_memberentity(member_details_list)
                                        CDFIApplication.database?.clfVoMemberDao()?.insertMapped_Memberdata(member_list)
                                        for(j in member_details_list!!.indices){
                                            var memberPhoneList = member_details_list.get(j).memberPhoneDetailsList!!
                                            CDFIApplication.database?.vOShgMemberPhoneDao()?.insertMapped_MemberPhonedata((memberPhoneList))
                                            CDFIApplication.database?.clfVoMemberDao()?.updateVO_shgMember(validate!!.returnStringValue(mapped_Vo.get(i).cbo_child_guid),validate!!.returnStringValue(mapped_Vo.get(i).cbo_guid),validate!!.returnStringValue(member_details_list.get(j).member_guid))
                                            federationViewmodel!!.updateFedrationCode(validate!!.returnLongValue(mapped_Vo.get(i).cbo_code.toString()),mapped_Vo.get(i).cbo_guid)
                                        }


                                    }
                                    idownload = 0
                                    iEcDownload = getFederationdata()
                                    if(idownload == 0 && iEcDownload == 1){
                                        idownload = 0
                                    }else {
                                        idownload = 1
                                    }
                                    progressDialog.dismiss()
                                } catch (ex: Exception) {
                                    ex.printStackTrace()
//                                    progressDialog.dismiss()
                                }

                            }else {
                                idownload = 0
                            }
                        } else {
                            idownload = 1
                            var resMsg = ""
                            if (res.code() == 403) {

                                code = res.code()
                                msg = res.message()
                            } else {
                                if(res.errorBody()?.contentLength()==0L || res.errorBody()?.contentLength()!!<0L){
                                    code = res.code()
                                    resMsg = res.message()
                                }else {
                                    var jsonObject1 =
                                        JSONObject(res.errorBody()!!.source().readUtf8().toString())

                                    code =
                                        validate!!.returnIntegerValue(
                                            jsonObject1.optString("responseCode").toString()
                                        )
                                    resMsg = validate!!.returnStringValue(
                                        jsonObject1.optString("responseMsg").toString()
                                    )
                                }
                                msg = "" + validate!!.alertMsg(
                                    this@VoSCMemberActivity,
                                    responseViewModel,
                                    code,
                                    validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                                    resMsg)

                            }
                        }

                        runOnUiThread {
                            if (idownload == 0) {
                                progressDialog.dismiss()
                                iButtonPressed = 1
                                val text = LabelSet.getText(
                                    "Datadownloadedsuccessfully",
                                    R.string.Datadownloadedsuccessfully
                                )
                                validate!!.CustomAlertVO(text,this@VoSCMemberActivity)
                            }else {
                                progressDialog.dismiss()
                                if (code == 403) {
                                    Toast.makeText(this@VoSCMemberActivity, msg, Toast.LENGTH_LONG)
                                        .show()
                                    CustomAlertlogin()
                                } else {
                                    val text = LabelSet.getText(
                                        "DatadownloadingFailed",
                                        R.string.DatadownloadingFailed
                                    )
                                    validate!!.CustomAlertVO(msg, this@VoSCMemberActivity)

                                }

                            }

                        }

                    } catch (bug: SocketTimeoutException) {
                        progressDialog.dismiss()
//                                Toast.makeText(activity!!, "Socket Timeout", Toast.LENGTH_LONG).show();
                    } catch (bug: ConnectTimeoutException) {
                        progressDialog.dismiss()
                        // Toast.makeText(getApplicationContext(), "Connection Timeout", Toast.LENGTH_LONG).show();
                    } catch (e: Exception) {
                        e.printStackTrace()
                        progressDialog.dismiss()
                    }
                }

            }.start()

        } else {
            AlertDialog.Builder(this).setTitle("No Internet Connection")
                .setMessage("Please check your internet connection and try again")
                .setPositiveButton(android.R.string.ok) { _, _ -> }
                .setIcon(android.R.drawable.ic_dialog_alert).show()
        }
    }

    fun getFederationdata():Int {
        var iDownValue = 0
        val userId = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
        val token = validate!!.returnStringValue(AESEncryption.decrypt(
            validate!!.RetriveSharepreferenceString(AppSP.token),
            validate!!.RetriveSharepreferenceString(AppSP.userid)
        ))

        val call = apiInterface?.getFederationData(
            "application/json", token, userId,
            validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id)
        )
        val response = call!!.execute()
        if (response.isSuccessful) {
            when (response.code()) {
                200 -> {

                    if (response.body()?.voDownloadModel != null) {
                        try {
                            var federation = response.body()?.voDownloadModel
                            var ecMemberList = federation!!.ecMembersList

                            if (!ecMemberList.isNullOrEmpty()) {
                                CDFIApplication.database?.executiveDao()
                                    ?.insertExectiveDetail(ecMemberList)
                            }

                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }
                    }
                    iDownValue = 1
                }

            }

        } else {
            iDownValue = 0

        }
        return iDownValue
    }

    fun CustomAlertlogin() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogelogin, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mDialogView.etUsername.setText(validate!!.RetriveSharepreferenceString(AppSP.userid))
        mDialogView.etPassword.setText(validate!!.returnStringValue(AESEncryption.decrypt(
            validate!!.RetriveSharepreferenceString(AppSP.Password),
            validate!!.RetriveSharepreferenceString(AppSP.userid)
        )))
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.btn_login.setBackgroundColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.txt_dialog_title2.text = LabelSet.getText("app_name",R.string.app_name)
        mDialogView.btn_login.text = LabelSet.getText("log_in",R.string.log_in)
        mDialogView.chk_showHide1.text = LabelSet.getText("show_password",R.string.show_password)
        mDialogView.btn_login.setOnClickListener {
            mAlertDialog.dismiss()
            importtoken(
                mDialogView.etUsername.text.toString(),
                validate!!.returnStringValue(
                    AESEncryption.encrypt(validate!!.returnStringValue(
                        mDialogView.etPassword.text.toString()),mDialogView.etUsername.text.toString()))
            )
        }
        mAlertDialog.chk_showHide1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                mAlertDialog.etPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                mAlertDialog.etPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
        }

    }

    fun importtoken(user: String, pass: String) {
        var grant_type = "password"
        var userId = user
        var password = pass
        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText(
                "authenticate_user",
                R.string.authenticate_user
            )
        )


        val callCount = apiInterface?.gettokenforce(
            grant_type,
            userId,
            password, validate!!.RetriveSharepreferenceInt(AppSP.State_Selected), 1,
            validate!!.RetriveSharepreferenceString(AppSP.Roleid)!!
        )
        callCount?.enqueue(object : Callback<JsonObject> {
            override fun onFailure(callCount: Call<JsonObject>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                progressDialog.dismiss()


                if (response.isSuccessful) {

                    var jsonObject1: JSONObject? = null
                    try {
                        jsonObject1 = JSONObject(response.body().toString())

                        val auth = jsonObject1.get("accessToken").toString()
                        if (auth.trim().length > 0) {
                            validate!!.SaveSharepreferenceString(
                                AppSP.token,
                                validate!!.returnStringValue(
                                    AESEncryption.encrypt(
                                        auth,validate!!.RetriveSharepreferenceString(AppSP.userid)))
                            )
                        }
                        validate!!.CustomAlertVO(
                            LabelSet.getText(
                                "userauthenticatedsuccessfully",
                                R.string.userauthenticatedsuccessfully
                            ), this@VoSCMemberActivity
                        )

                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Toast.makeText(this@VoSCMemberActivity, response.message(), Toast.LENGTH_LONG)
                            .show()


                    }

                } else {
                    Toast.makeText(this@VoSCMemberActivity, response.message(), Toast.LENGTH_LONG)
                        .show()
                }


            }

        })

    }

    fun  uploadSCdata(iAutoSave:Int,isUpdate:Int){
        if (isNetworkConnected()) {
            var scList = committeeViewModel!!.getCommitteedetaildata(validate!!.RetriveSharepreferenceString(AppSP.SubCommGuid))
            if (!scList.isNullOrEmpty()) {
                exportSubcommittee(scList,iAutoSave,isUpdate)
            } else {
                val text = LabelSet.getText(
                    "nothing_upload",
                    R.string.nothing_upload
                )
                validate!!.CustomAlertVO(text, this@VoSCMemberActivity)
            }
        } else {
            validate!!.CustomAlertVO(
                LabelSet.getText(
                    "no_internet_msg",
                    R.string.no_internet_msg
                ), this
            )
        }
    }

    fun exportSubcommittee(scList: List<subcommitteeEntity>?,iAutoSave:Int,isUpdate:Int) {
        progressDialog = ProgressDialog.show(
            this, LabelSet.getText("app_name", R.string.app_name),
            LabelSet.getText(
                "DataUploading",
                R.string.DataUploading
            )
        )
        progressDialog.setIcon(this.getDrawable(R.drawable.appicon))
        var idownload = 0
        var msg = ""
        var code = 0
        object : Thread() {

            //@SuppressLint("NewApi")
            override fun run() {
                try {

                    if (scList!!.size > 0) {
                        for (j in 0..scList.size - 1) {

                            var scListmodel = scList.get(j)
                            var scMemberList =
                                committeeViewModel!!.getCommitteeMemberPendingdata(scList.get(j).subcommitee_guid)

                            var scUploadModel: SubcommitteeModel = SubcommitteeModel()


                            scUploadModel = MappingData.returnScObject(scListmodel)
                            scUploadModel.subCommitteeMemberList = scMemberList

                            val gson = Gson()
                            val json = gson.toJson(scUploadModel)
                            val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                            val token = validate!!.returnStringValue(AESEncryption.decrypt(
                                validate!!.RetriveSharepreferenceString(AppSP.token),
                                validate!!.RetriveSharepreferenceString(AppSP.userid)
                            ))

                            val sData = RequestBody.create(
                                MediaType.parse("application/json; charset=utf-8"),
                                json
                            )
                            val call = apiInterface?.uploadSCdata(
                                "application/json",
                                user,
                                token,
                                sData
                            )

                            val res = call?.execute()

                            if (res!!.isSuccessful) {
                                if (res.body()?.msg.equals("Record Added To Queue")) {
                                    try {
                                        committeeViewModel!!.updateuploadStatus(
                                            scList.get(j).subcommitee_guid,
                                            validate!!.Daybetweentime(validate!!.currentdatetime)
                                        )
                                        committeeViewModel!!.updateScuploadStatus(
                                            scList.get(j).subcommitee_guid,
                                            validate!!.Daybetweentime(validate!!.currentdatetime)
                                        )
                                        msg = res.body()?.msg!!

                                    } catch (ex: Exception) {
                                        ex.printStackTrace()
//                                    progressDialog.dismiss()
                                    }
                                }
                            } else {
                                msg = "" + res.code() + " " + res.message()
                                code = res.code()
                                idownload = 1
//
                            }
                        }


                    } else {
                        idownload = -1
                    }

                    runOnUiThread {
                        if (idownload == 0) {
                            progressDialog.dismiss()
                            var text = msg
                            if(iAutoSave==0 && isUpdate ==1) {
                                text = LabelSet.getText(
                                    "updated_successfully",
                                    R.string.updated_successfully
                                )
                                validate!!.CustomAlertVO(
                                    text,
                                    this@VoSCMemberActivity,
                                    VoSubCommiteeList::class.java
                                )
                            }else if(iAutoSave==0 && isUpdate ==0){
                                text = LabelSet.getText(
                                    "data_saved_successfully",
                                    R.string.data_saved_successfully
                                )
                                validate!!.CustomAlertVO(
                                    text,
                                    this@VoSCMemberActivity,
                                    VoSubCommiteeList::class.java
                                )
                            }
                        } else if (idownload == -1) {
                            progressDialog.dismiss()
                            val text = LabelSet.getText(
                                "nothing_upload",
                                R.string.nothing_upload
                            )
                            validate!!.CustomAlertVO(text, this@VoSCMemberActivity)
                        }  else {
                            progressDialog.dismiss()
                            if (code == 403) {
                                Toast.makeText(this@VoSCMemberActivity, msg, Toast.LENGTH_LONG)
                                    .show()
                                CustomAlertlogin()
                            } else {

                                validate!!.CustomAlertVO(msg, this@VoSCMemberActivity)
                                Toast.makeText(this@VoSCMemberActivity, msg, Toast.LENGTH_LONG)
                                    .show()
                            }
                        }


                    }
                } catch (bug: SocketTimeoutException) {
                    progressDialog.dismiss()
//                                Toast.makeText(activity!!, "Socket Timeout", Toast.LENGTH_LONG).show();
                } catch (bug: ConnectTimeoutException) {
                    progressDialog.dismiss()
                    // Toast.makeText(getApplicationContext(), "Connection Timeout", Toast.LENGTH_LONG).show();
                } catch (e: Exception) {
                    e.printStackTrace()
                    progressDialog.dismiss()
                }
            }

        }.start()
    }

    fun setFederationEditFlag(){
        var bank_Fi = 0
        var financialIntermidiation = federationViewmodel!!.getFI(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var bankCount = federationViewmodel!!.getBankCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var signatoryCount = federationViewmodel!!.getSignatoryCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!)

        if((financialIntermidiation == 1 && bankCount>0 && signatoryCount>=2) || financialIntermidiation == 0){
            bank_Fi = 1
        }else{
            bank_Fi = 0
        }
        var scMemberCount = federationViewmodel!!.getSCMemberCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!)
        var officeBearerCount = executiveviewmodel!!.getOBCount(listOf(1,3,5),validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!)
        var cboCount = executiveviewmodel!!.getDistinctCboCount(validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id))
        var ecCount = federationViewmodel!!.getEcMemberCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var mapped_shg_count =
            federationViewmodel!!.getMappedShgCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var mapped_vo_count =
            federationViewmodel!!.getMappedVoCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        if(cboType == 1 && ecCount > 4 && mapped_shg_count > 4 && mapped_shg_count == cboCount && officeBearerCount==3 && bank_Fi ==1 && scMemberCount==0) {
            federationViewmodel!!.updateFedrationEditFlag(1,validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!)
        }else if(cboType == 2 && ecCount > 4 && mapped_vo_count > 4 && mapped_vo_count == cboCount && officeBearerCount==3 && bank_Fi ==1  && scMemberCount==0) {
            federationViewmodel!!.updateFedrationEditFlag(1, validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!)
        } else {
            federationViewmodel!!.updateFedrationEditFlag(-1,validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!)
        }
    }
}