package com.microware.cdfi.activity.vo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.entity.subcommitee_masterEntity
import com.microware.cdfi.entity.subcommitteeEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_vo_sub_commitee_detail.*
import kotlinx.android.synthetic.main.activity_vo_sub_commitee_detail.btn_save
import kotlinx.android.synthetic.main.activity_vo_sub_commitee_detail.btn_savegray
import kotlinx.android.synthetic.main.activity_vo_sub_commitee_detail.et_formDate
import kotlinx.android.synthetic.main.activity_vo_sub_commitee_detail.et_toDate
import kotlinx.android.synthetic.main.activity_vo_sub_commitee_detail.lay_status
import kotlinx.android.synthetic.main.activity_vo_sub_commitee_detail.lay_toDate
import kotlinx.android.synthetic.main.activity_vo_sub_commitee_detail.spin_status
import kotlinx.android.synthetic.main.activity_vo_sub_commitee_detail.tvStatus
import kotlinx.android.synthetic.main.activity_vo_sub_commitee_detail.tvToDate
import kotlinx.android.synthetic.main.votablayout.*
import kotlinx.android.synthetic.main.white_toolbar.*

class VoSubCommiteeDetail : AppCompatActivity() {

    var subcommitteeEntity: subcommitteeEntity? = null
    var committeeViewModel: SubcommitteeViewModel? = null
    var federationViewmodel: FedrationViewModel? = null
    var validate:Validate?=null
    var dataspincommittee:List<subcommitee_masterEntity>? = null
    var committeeMasterViewmodel: subcommitteeMasterViewModel? = null
    var lookupviewmodel: LookupViewmodel? = null
    var executiveviewmodel: ExecutiveMemberViewmodel? = null
    var datastatus:List<LookupEntity>? = null

    var cboType = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vo_sub_commitee_detail)

        committeeMasterViewmodel = ViewModelProviders.of(this).get(subcommitteeMasterViewModel::class.java)
        committeeViewModel = ViewModelProviders.of(this).get(SubcommitteeViewModel::class.java)
        federationViewmodel = ViewModelProviders.of(this).get(FedrationViewModel::class.java)
        lookupviewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        executiveviewmodel = ViewModelProviders.of(this).get(ExecutiveMemberViewmodel::class.java)

        validate = Validate(this)

        ivBack.setOnClickListener {

                var intent = Intent(this, VoSubCommiteeList::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)

        }

        if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) == 1) {
            ivLock.visibility = View.VISIBLE
            btn_savegray.visibility = View.VISIBLE
            btn_save.visibility = View.GONE

        } else {
            ivLock.visibility = View.GONE
            btn_savegray.visibility = View.GONE
            btn_save.visibility = View.VISIBLE
        }

        if(validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType)==2){
            cboType = 2
        }else {
            cboType = 1
        }


        ivHome.visibility = View.GONE
        tv_title.text = LabelSet.getText(
            "sub_committee",
            R.string.sub_committee
        )
        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvEc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary1))
        tvVector.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_Ec.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimary1))

        var basicComplete = federationViewmodel!!.getIsCompleteValue(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var addressIsComplete = federationViewmodel!!.getAddressCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var phoneIsComplete = federationViewmodel!!.getPhoneCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var bankIsComplete = federationViewmodel!!.getBankCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var ecIsComplete = federationViewmodel!!.getEcMemberCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var scIsComplete = federationViewmodel!!.getScCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var kycIsComplete = federationViewmodel!!.getKycCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))

        if(basicComplete > 0){
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(ecIsComplete > 0){
            IvEc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvEc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(phoneIsComplete > 0){
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(addressIsComplete > 0){
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(bankIsComplete > 0){
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }

        if(kycIsComplete > 0){
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }

        lay_vector.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (sCheckValidation() == 1) {
                    saveData(1)

                    var intent = Intent(this, VoBasicDetailActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoBasicDetailActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_mapcbo.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (sCheckValidation() == 1) {
                    saveData(1)

                    if (cboType == 1) {
                        var intent = Intent(this, VOMapCBOActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    }else if(cboType == 2){
                        var intent = Intent(this, CLFMapCBOActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    }
                }
            }else {
                if (cboType == 1) {
                    var intent = Intent(this, VOMapCBOActivity::class.java)
                    intent.flags =
                        Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }else if(cboType == 2){
                    var intent = Intent(this, CLFMapCBOActivity::class.java)
                    intent.flags =
                        Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }
        }

        lay_kyc.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (sCheckValidation() == 1) {
                    saveData(1)

                    var intent = Intent(this, VoKycDetailList::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoKycDetailList::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_Ec.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (sCheckValidation() == 1) {
                    saveData(1)

                    var intent = Intent(this, VoEcListActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoEcListActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }


        lay_phone.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (sCheckValidation() == 1) {
                    saveData(1)

                    var intent = Intent(this, VoPhoneDetailListActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoPhoneDetailListActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_location.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (sCheckValidation() == 1) {
                    saveData(1)

                    var intent = Intent(this, VoAddressList::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoAddressList::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_bank.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) != 1) {
                if (sCheckValidation() == 1) {
                    saveData(1)

                    var intent = Intent(this, VoBankListActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                }
            }else {
                var intent = Intent(this, VoBankListActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        et_formDate.setOnClickListener {
            validate!!.datePickerwithmindate(
                validate!!.RetriveSharepreferenceLong(AppSP.Formation_dt),
                et_formDate
            )
        }

           et_toDate.setOnClickListener {
                validate!!.datePickerwithmindate(
                    validate!!.Daybetweentime(et_formDate.text.toString()),
                    et_toDate
                )
            }
        btn_save.setOnClickListener {
            if (sCheckValidation() == 1) {
                saveData(0)
            }
        }
        fillSpinner()
        showdata()

        setLabelText()
    }

    private fun setLabelText() {
        tvFedCode.text = LabelSet.getText(
            "fedName",
            R.string.fedName
        )
        tvsubcommitee.text = LabelSet.getText(
            "sub_committee",
            R.string.sub_committee
        )
        tvFormationDate.text = LabelSet.getText(
            "from_date",
            R.string.from_date
        )
        tvToDate.text = LabelSet.getText(
            "to_Date",
            R.string.to_Date
        )
        tvStatus.text = LabelSet.getText(
            "status",
            R.string.status
        )
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_savegray.text = LabelSet.getText(
            "save",
            R.string.save
        )
        et_formDate.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )
    }

    private fun fillSpinner(){
        datastatus = lookupviewmodel!!.getlookup(5,validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
        dataspincommittee = committeeMasterViewmodel!!.getScMaster(validate!!.RetriveSharepreferenceString(AppSP.Langaugecode))
        validate!!.fillSubcommitteeSpinner(this,spin_committee,dataspincommittee)
        validate!!.fillspinner(this,spin_status,datastatus)

        spin_status?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if(position>0){
                    var status = validate!!.returnlookupcode(spin_status,datastatus)
                    if(status == 2){
                        et_toDate.isEnabled = true
                        et_toDate.setText(validate!!.convertDatetime(validate!!.Daybetweentime(validate!!.currentdatetime)))
                    }else {
                        et_toDate.isEnabled = false
                        et_toDate.setText("")
                    }
                }else {
                    et_toDate.setText("")
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }
    }

    private fun saveData(iAutoSave:Int){

        if(validate!!.RetriveSharepreferenceString(AppSP.SubCommGuid).isNullOrEmpty()){
            var guid = validate!!.random()
            subcommitteeEntity = subcommitteeEntity(guid,
                0,
                validate!!.returnSubcommitteeID(spin_committee,dataspincommittee),
                validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id),
                validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID),
                0,
                validate!!.Daybetweentime(et_formDate.text.toString()),
                0,1,1,1,1,
                0,"",
                validate!!.Daybetweentime(validate!!.currentdatetime),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                0,
                "",0,0)
            committeeViewModel!!.insert(subcommitteeEntity!!)
            setFederationEditFlag()
            validate!!.SaveSharepreferenceString(AppSP.SubCommGuid, guid)
            if(iAutoSave==0) {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "data_saved_successfully",
                        R.string.data_saved_successfully
                    ),
                    this,
                    VoSubCommiteeList::class.java
                )
            }
        }else {
            committeeViewModel!!.updateCommitteedata(validate!!.RetriveSharepreferenceString(AppSP.SubCommGuid)!!,
                validate!!.returnSubcommitteeID(spin_committee,dataspincommittee),
                validate!!.Daybetweentime(et_formDate.text.toString()),
                validate!!.Daybetweentime(et_toDate.text.toString()),1,1,1,
                validate!!.Daybetweentime(validate!!.currentdatetime),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.returnlookupcode(spin_status,datastatus)
            )
            setFederationEditFlag()
            if(validate!!.returnlookupcode(spin_status,datastatus)==2){
                committeeViewModel!!.updateScMemberStatus(validate!!.RetriveSharepreferenceString(AppSP.SubCommGuid)!!,
                    validate!!.Daybetweentime(et_toDate.text.toString()),
                    1,
                    1,
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    validate!!.RetriveSharepreferenceString(AppSP.userid)!!)
            }
            if(iAutoSave==0) {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "updated_successfully",
                        R.string.updated_successfully
                    ),
                    this,
                    VoSubCommiteeList::class.java
                )
            }

        }
    }

    private fun showdata(){
        et_shgcode.setText(validate!!.RetriveSharepreferenceString(AppSP.FedrationName))
        //     et_shgcode.setText(validate!!.RetriveSharepreferenceString(AppSP.SubCommGuid))
        if(!validate!!.RetriveSharepreferenceString(AppSP.SubCommGuid).isNullOrEmpty()){
            var list = committeeViewModel!!.getCommitteedetaildata(validate!!.RetriveSharepreferenceString(AppSP.SubCommGuid))
            if(!list.isNullOrEmpty()){
                spin_committee.setSelection(validate!!.returnSubcommitteePos(validate!!.returnIntegerValue(list.get(0).subcommitee_type_id.toString()),dataspincommittee))
                if(validate!!.returnLongValue(list.get(0).fromdate.toString())>0) {
                    et_formDate.setText(validate!!.convertDatetime(list.get(0).fromdate!!))
                }
                if(validate!!.returnIntegerValue(list.get(0).status.toString())>0){
                    spin_status.setSelection(validate!!.returnlookupcodepos(validate!!.returnIntegerValue(list.get(0).status.toString()),datastatus))
                }
            }
        }else {
            lay_status.visibility=View.GONE
            lay_toDate.visibility=View.GONE
        }
    }

    private fun sCheckValidation():Int{
        var value = 1
        var sc_id = validate!!.returnSubcommitteeID(spin_committee,dataspincommittee)
        var scCount = committeeViewModel!!.getScCount(sc_id,validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        if (spin_committee.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(this,spin_committee,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                )+" " +LabelSet.getText(
                    "sub_committee",
                    R.string.sub_committee
                ))
            value = 0
            return value
        }else if((validate!!.RetriveSharepreferenceString(AppSP.SubCommGuid).isNullOrEmpty() && scCount>0) ||
            (!validate!!.RetriveSharepreferenceString(AppSP.SubCommGuid).isNullOrEmpty() && scCount>1)){
            validate!!.CustomAlertSpinner(this,spin_committee,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                )+" " +LabelSet.getText(
                    "differ_committe",
                    R.string.differ_committe
                ))
            value = 0
            return value
        }else if (et_formDate.text.toString().trim().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                )+" " +LabelSet.getText(
                    "formation_date",
                    R.string.formation_date
                ),
                this,et_formDate)
            value = 0
            return value
        }
        return value
    }

    fun setFederationEditFlag(){
        var bank_Fi = 0
        var financialIntermidiation = federationViewmodel!!.getFI(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var bankCount = federationViewmodel!!.getBankCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var signatoryCount = federationViewmodel!!.getSignatoryCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!)

        if((financialIntermidiation == 1 && bankCount>0 && signatoryCount>=2) || financialIntermidiation == 0){
            bank_Fi = 1
        }else{
            bank_Fi = 0
        }
        var scMemberCount = federationViewmodel!!.getSCMemberCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!)
        var officeBearerCount = executiveviewmodel!!.getOBCount(listOf(1,3,5),validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!)
        var cboCount = executiveviewmodel!!.getDistinctCboCount(validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id))
        var ecCount = federationViewmodel!!.getEcMemberCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var mapped_shg_count =
            federationViewmodel!!.getMappedShgCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var mapped_vo_count =
            federationViewmodel!!.getMappedVoCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        if(cboType == 1 && ecCount > 4 && mapped_shg_count > 4 && mapped_shg_count == cboCount && officeBearerCount==3 && bank_Fi ==1  && scMemberCount==0) {
            federationViewmodel!!.updateFedrationEditFlag(1,validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!)
        }else if(cboType == 2 && ecCount > 4 && mapped_vo_count > 4 && mapped_vo_count == cboCount && officeBearerCount==3 && bank_Fi ==1  && scMemberCount==0) {
            federationViewmodel!!.updateFedrationEditFlag(1, validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!)
        } else {
            federationViewmodel!!.updateFedrationEditFlag(-1,validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!)
        }
    }
}