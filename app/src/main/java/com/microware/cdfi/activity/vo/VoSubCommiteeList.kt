package com.microware.cdfi.activity.vo

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.microware.cdfi.R
import com.microware.cdfi.adapter.SubCommitteeAdapter
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.api.model.EcScModelJoindata
import com.microware.cdfi.api.model.SubcommitteeModel
import com.microware.cdfi.entity.subcommitee_masterEntity
import com.microware.cdfi.entity.subcommitteeEntity
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_vo_sub_commitee_list.*
import kotlinx.android.synthetic.main.activity_vo_sub_commitee_list.rvList
import kotlinx.android.synthetic.main.customealertdialogelogin.*
import kotlinx.android.synthetic.main.customealertdialogelogin.view.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.votablayout.*
import kotlinx.android.synthetic.main.white_toolbar.*
import okhttp3.MediaType
import okhttp3.RequestBody
import org.apache.http.conn.ConnectTimeoutException
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException

class VoSubCommiteeList : AppCompatActivity() {

    var executiveviewmodel: ExecutiveMemberViewmodel? = null
    var committeeViewModel: SubcommitteeViewModel? = null
    var scMemberViewmodel: Subcommitee_memberViewModel? = null
    var federationViewmodel: FedrationViewModel? = null
    var validate:Validate?=null
    var dataspincommittee:List<subcommitee_masterEntity>? = null
    var committeeMasterViewmodel: subcommitteeMasterViewModel? = null
    var subcommiteeMasterentity: subcommitee_masterEntity? = null
    var dataspinMember:List<EcScModelJoindata>? = null
    var apiInterface: ApiInterface? = null
    internal lateinit var progressDialog: ProgressDialog
    var responseViewModel: ResponseViewModel? = null

    var cboType = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vo_sub_commitee_list)
        committeeMasterViewmodel = ViewModelProviders.of(this).get(subcommitteeMasterViewModel::class.java)
        committeeViewModel = ViewModelProviders.of(this).get(SubcommitteeViewModel::class.java)
        scMemberViewmodel = ViewModelProviders.of(this).get(Subcommitee_memberViewModel::class.java)
        executiveviewmodel = ViewModelProviders.of(this).get(ExecutiveMemberViewmodel::class.java)
        federationViewmodel = ViewModelProviders.of(this).get(FedrationViewModel::class.java)
        responseViewModel = ViewModelProviders.of(this).get(ResponseViewModel::class.java)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()

        validate = Validate(this)

        ivBack.setOnClickListener {
            var intent = Intent(this, VOListActivity::class.java)
            intent.flags= Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }

        if(validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType)==2){
            cboType = 2
        }else {
            cboType = 1
        }

        ivHome.visibility = View.GONE
        tv_title.text = LabelSet.getText(
            "sub_committee",
            R.string.sub_committee
        )
        IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvEc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        IvSystemTag.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary1))
        tvVector.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_phone.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_loc.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_bank.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_kyc.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_Ec.setBackgroundColor(ContextCompat.getColor(this,R.color.white))
        tv_systemTag.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimary1))

        var basicComplete = federationViewmodel!!.getIsCompleteValue(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var addressIsComplete = federationViewmodel!!.getAddressCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var phoneIsComplete = federationViewmodel!!.getPhoneCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var bankIsComplete = federationViewmodel!!.getBankCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var ecIsComplete = federationViewmodel!!.getEcMemberCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var scIsComplete = federationViewmodel!!.getScCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))
        var kycIsComplete = federationViewmodel!!.getKycCount(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))

        if(basicComplete > 0){
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvVector.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(ecIsComplete > 0){
            IvEc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvEc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(phoneIsComplete > 0){
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvPhone.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(addressIsComplete > 0){
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            Ivloc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }
        if(bankIsComplete > 0){
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvBank.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }

        if(kycIsComplete > 0){
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.khakigreen1))
        }else {
            IvKyc.setColorFilter(ContextCompat.getColor(this, R.color.unselectedclr))
        }

        lay_kyc.setOnClickListener {
            var intent = Intent(this, VoKycDetailList::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_mapcbo.setOnClickListener {
            if (cboType == 1) {
                var intent = Intent(this, VOMapCBOActivity::class.java)
                intent.flags =
                    Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }else if(cboType == 2){
                var intent = Intent(this, CLFMapCBOActivity::class.java)
                intent.flags =
                    Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }

        lay_vector.setOnClickListener {
            var intent = Intent(this, VoBasicDetailActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_Ec.setOnClickListener {
            var intent = Intent(this, VoEcListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(0, 0)
        }


        lay_phone.setOnClickListener {
            var intent = Intent(this, VoPhoneDetailListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_location.setOnClickListener {
            var intent = Intent(this, VoAddressList::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        lay_bank.setOnClickListener {
            var intent = Intent(this, VoBankListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            overridePendingTransition(0,0)
        }

        addcommitee.setOnClickListener {
            validate!!.SaveSharepreferenceString(AppSP.SubCommGuid,"")
            var intent = Intent(this, VoSubCommiteeDetail::class.java)
            intent.flags= Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            finish()
        }
        /*   if(committeeMasterViewmodel!!.getScMasterCount()==0) {
               insertMasterdata()
           }*/

        tv_sc_comments.text = LabelSet.getText("Subcommitte_screen_with_plus_button",R.string.Subcommitte_screen_with_plus_button)


        fillData()
    }

    fun returnCommitteeName(id:Int):String{
        dataspincommittee = committeeMasterViewmodel!!.getScMaster(validate!!.RetriveSharepreferenceString(
            AppSP.Langaugecode))
        return validate!!.returnSubcommitteeName(id,dataspincommittee)

    }
    override fun onBackPressed() {
        val intent = Intent(this, VOListActivity::class.java)
        intent.flags= Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    private fun fillData() {
        committeeViewModel!!.getCommitteedata(validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID))!!
            .observe(this,object : Observer<List<subcommitteeEntity>> {
                override fun onChanged(subCommitteelist: List<subcommitteeEntity>?) {
                    if (subCommitteelist!=null){
                        rvList.layoutManager = LinearLayoutManager(this@VoSubCommiteeList)
                        rvList.adapter = SubCommitteeAdapter(this@VoSubCommiteeList, subCommitteelist,committeeViewModel!!,scMemberViewmodel!!)
                        if(subCommitteelist.size>0){
                            lay_noscavialable.visibility = View.GONE
                        }else {
                            lay_noscavialable.visibility = View.VISIBLE
                            tv_nosc_avialable.text = LabelSet.getText(
                                "no_sc_s_avialable",
                                R.string.no_sc_s_avialable
                            )
                        }
                    }
                }
            })
    }

    private fun insertMasterdata(){
        subcommiteeMasterentity = subcommitee_masterEntity(1,"Education","en",1)
        committeeMasterViewmodel!!.insert(subcommiteeMasterentity!!)
        subcommiteeMasterentity = subcommitee_masterEntity(2,"Literacy","en",1)
        committeeMasterViewmodel!!.insert(subcommiteeMasterentity!!)
        subcommiteeMasterentity = subcommitee_masterEntity(3,"Health","en",1)
        committeeMasterViewmodel!!.insert(subcommiteeMasterentity!!)
        subcommiteeMasterentity = subcommitee_masterEntity(3,"Hygiene","en",1)
        committeeMasterViewmodel!!.insert(subcommiteeMasterentity!!)
    }

    fun setMember(memberCode:Long): String {

        var sValue = ""
        if(validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType)==1){
            dataspinMember = scMemberViewmodel!!.getExecutiveMemberList(
                validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)
            )
        }else  if(validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType)==2){
            dataspinMember = scMemberViewmodel!!.getECMemberList(
                validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)
            )
        }
        if (!dataspinMember.isNullOrEmpty()) {
            for (i in dataspinMember!!.indices) {
                if (memberCode == dataspinMember!!.get(i).member_cd)
                    sValue = validate!!.returnStringValue(dataspinMember!!.get(i).member_name)
            }
        }
        return sValue
    }

    fun CustomAlert(guid:String,iFlag:Int) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_dialog_title.setTextColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.btn_yes.setBackgroundColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.btn_no.setBackgroundColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.txt_msg.text = LabelSet.getText(
            "do_u_want_to_delete",
            R.string.do_u_want_to_delete
        )
        mDialogView.btn_yes.setOnClickListener {

            if(iFlag==0){
                executiveviewmodel!!.deleteSCRecord(guid)
                validate!!.updateFederationEditFlag(cboType,validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!,
                    validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id),federationViewmodel,executiveviewmodel)
            }else {
                if (isNetworkConnected()) {
                    executiveviewmodel!!.deleteScMemeber(guid,validate!!.Daybetweentime(validate!!.currentdatetime))
                    validate!!.updateFederationEditFlag(cboType,validate!!.RetriveSharepreferenceString(AppSP.FedrationGUID)!!,
                        validate!!.RetriveSharepreferenceLong(AppSP.Fedration_id),federationViewmodel,executiveviewmodel)
                    committeeViewModel!!.updateScCompleteStatus(
                        guid,validate!!.Daybetweentime(validate!!.currentdatetime))
                    uploadScdata()

                }else {
                    validate!!.CustomAlertVO(
                        LabelSet.getText(
                            "no_internet_msg",
                            R.string.no_internet_msg
                        ), this
                    )
                }
            }
            mAlertDialog.dismiss()

        }

        mDialogView.btn_no.setOnClickListener {

            mAlertDialog.dismiss()

        }
    }

    private fun isNetworkConnected(): Boolean {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager //1
        val networkInfo = connectivityManager.activeNetworkInfo //2
        return networkInfo != null && networkInfo.isConnected //3
    }

   fun uploadScdata(){
       if (isNetworkConnected()) {
           var scList = committeeViewModel!!.getCommitteePendingdata()
           if (!scList.isNullOrEmpty()) {
               //      CustomAlertlogin()
               exportSubcommittee(scList)
           } else {
               val text = LabelSet.getText(
                   "nothing_upload",
                   R.string.nothing_upload
               )
               validate!!.CustomAlertVO(text, this@VoSubCommiteeList)
           }
       } else {
           validate!!.CustomAlertVO(
               LabelSet.getText(
                   "no_internet_msg",
                   R.string.no_internet_msg
               ), this
           )
       }
   }

    fun exportSubcommittee(scList: List<subcommitteeEntity>?) {
        progressDialog = ProgressDialog.show(
            this, LabelSet.getText("app_name", R.string.app_name),
            LabelSet.getText(
                "DataUploading",
                R.string.DataUploading
            )
        )
        progressDialog.setIcon(this.getDrawable(R.drawable.appicon))
        var idownload = 0
        var msg = ""
        var code = 0
        object : Thread() {

            //@SuppressLint("NewApi")
            override fun run() {
                try {

                    if (scList!!.size > 0) {
                        for (j in 0..scList.size - 1) {

                            var scListmodel = scList.get(j)
                            var scMemberList =
                                committeeViewModel!!.getCommitteeMemberPendingdata(scList.get(j).subcommitee_guid)

                            var scUploadModel: SubcommitteeModel = SubcommitteeModel()


                            scUploadModel = MappingData.returnScObject(scListmodel)
                            scUploadModel.subCommitteeMemberList = scMemberList

                            val gson = Gson()
                            val json = gson.toJson(scUploadModel)
                            val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                            val token = validate!!.returnStringValue(
                                AESEncryption.decrypt(
                                    validate!!.RetriveSharepreferenceString(AppSP.token),
                                    validate!!.RetriveSharepreferenceString(AppSP.userid)
                                ))

                            val sData = RequestBody.create(
                                MediaType.parse("application/json; charset=utf-8"),
                                json
                            )
                            val call = apiInterface?.uploadSCdata(
                                "application/json",
                                user,
                                token,
                                sData
                            )

                            val res = call?.execute()

                            if (res!!.isSuccessful) {
                                if (res.body()?.msg.equals("Record Added To Queue")) {
                                    try {

                                        committeeViewModel!!.updateuploadStatus(
                                            scList.get(j).subcommitee_guid,
                                            validate!!.Daybetweentime(validate!!.currentdatetime)
                                        )
                                        committeeViewModel!!.updateScuploadStatus(
                                            scList.get(j).subcommitee_guid,
                                            validate!!.Daybetweentime(validate!!.currentdatetime)
                                        )
                                        msg = res.body()?.msg!!

                                    } catch (ex: Exception) {
                                        ex.printStackTrace()
//                                    progressDialog.dismiss()
                                    }
                                }
                            } else {
                                var resMsg = ""
                                if (res.code() == 403) {

                                    code = res.code()
                                    msg = res.message()
                                } else {
                                    if(res.errorBody()?.contentLength()==0L || res.errorBody()?.contentLength()!!< 0L){
                                        code = res.code()
                                        resMsg = res.message()
                                    }else {
                                        var jsonObject1 =
                                            JSONObject(res.errorBody()!!.source().readUtf8().toString())

                                        code =
                                            validate!!.returnIntegerValue(
                                                jsonObject1.optString("responseCode").toString()
                                            )
                                        resMsg = validate!!.returnStringValue(
                                            jsonObject1.optString("responseMsg").toString()
                                        )
                                    }
                                    msg = "" + validate!!.alertMsg(
                                        this@VoSubCommiteeList,
                                        responseViewModel,
                                        code,
                                        validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                                        resMsg)

                                }
                            }
                        }


                    } else {
                        idownload = -1
                    }

                    runOnUiThread {
                        if (idownload == 0) {
                            progressDialog.dismiss()
                            val text = msg
                            validate!!.CustomAlertVO(text, this@VoSubCommiteeList)
                        } else if (idownload == -1) {
                            progressDialog.dismiss()
                            val text = LabelSet.getText(
                                "nothing_upload",
                                R.string.nothing_upload
                            )
                            validate!!.CustomAlertVO(text, this@VoSubCommiteeList)
                        }  else {
                            progressDialog.dismiss()
                            if (code == 403) {
                                Toast.makeText(this@VoSubCommiteeList, msg, Toast.LENGTH_LONG)
                                    .show()
                                CustomAlertlogin()
                            } else {

                                validate!!.CustomAlertVO(msg, this@VoSubCommiteeList)
                                Toast.makeText(this@VoSubCommiteeList, msg, Toast.LENGTH_LONG)
                                    .show()
                            }
                        }


                    }
                } catch (bug: SocketTimeoutException) {
                    progressDialog.dismiss()
//                                Toast.makeText(activity!!, "Socket Timeout", Toast.LENGTH_LONG).show();
                } catch (bug: ConnectTimeoutException) {
                    progressDialog.dismiss()
                    // Toast.makeText(getApplicationContext(), "Connection Timeout", Toast.LENGTH_LONG).show();
                } catch (e: Exception) {
                    e.printStackTrace()
                    progressDialog.dismiss()
                }
            }

        }.start()
    }

    fun CustomAlertlogin() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogelogin, null)
        val mBuilder = androidx.appcompat.app.AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mDialogView.etUsername.setText(validate!!.RetriveSharepreferenceString(AppSP.userid))
        mDialogView.etPassword.setText(validate!!.returnStringValue(
            AESEncryption.decrypt(
                validate!!.RetriveSharepreferenceString(AppSP.Password),
                validate!!.RetriveSharepreferenceString(AppSP.userid)
            )))
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.btn_login.setBackgroundColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.txt_dialog_title2.text = LabelSet.getText("app_name",R.string.app_name)
        mDialogView.btn_login.text = LabelSet.getText("log_in",R.string.log_in)
        mDialogView.chk_showHide1.text = LabelSet.getText("show_password",R.string.show_password)
        mDialogView.btn_login.setOnClickListener {
            mAlertDialog.dismiss()
            importtoken(
                mDialogView.etUsername.text.toString(),
                validate!!.returnStringValue(
                    AESEncryption.encrypt(validate!!.returnStringValue(
                        mDialogView.etPassword.text.toString()),mDialogView.etUsername.text.toString()))
            )
        }
        mAlertDialog.chk_showHide1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                mAlertDialog.etPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                mAlertDialog.etPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
        }

    }

    fun importtoken(user: String, pass: String) {
        var grant_type = "password"
        var userId = user
        var password = pass
        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText(
                "authenticate_user",
                R.string.authenticate_user
            )
        )


        val callCount = apiInterface?.gettokenforce(
            grant_type,
            userId,
            password, validate!!.RetriveSharepreferenceInt(AppSP.State_Selected), 1,
            validate!!.RetriveSharepreferenceString(AppSP.Roleid)!!
        )
        callCount?.enqueue(object : Callback<JsonObject> {
            override fun onFailure(callCount: Call<JsonObject>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                progressDialog.dismiss()


                if (response.isSuccessful) {

                    var jsonObject1: JSONObject? = null
                    try {
                        jsonObject1 = JSONObject(response.body().toString())

                        val auth = jsonObject1.get("accessToken").toString()
                        if (auth.trim().length > 0) {
                            validate!!.SaveSharepreferenceString(
                                AppSP.token,
                                validate!!.returnStringValue(
                                    AESEncryption.encrypt(
                                        auth,validate!!.RetriveSharepreferenceString(AppSP.userid)))
                            )
                        }
                        validate!!.CustomAlertVO(
                            LabelSet.getText(
                                "userauthenticatedsuccessfully",
                                R.string.userauthenticatedsuccessfully
                            ), this@VoSubCommiteeList
                        )

                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Toast.makeText(this@VoSubCommiteeList, response.message(), Toast.LENGTH_LONG)
                            .show()


                    }

                } else {
                    Toast.makeText(this@VoSubCommiteeList, response.message(), Toast.LENGTH_LONG)
                        .show()
                }


            }

        })

    }
}