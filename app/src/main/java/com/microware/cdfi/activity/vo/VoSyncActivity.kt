package com.microware.cdfi.activity.vo

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.text.Html
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProviders
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.microware.cdfi.R
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.api.model.SubcommitteeModel
import com.microware.cdfi.api.model.VOUploadModel
import com.microware.cdfi.api.response.SHGResponse
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.*
import com.microware.cdfi.utility.*
import com.microware.cdfi.utility.MappingData.returnObjentity
import com.microware.cdfi.utility.MappingData.returnVO_Shgmemberentity
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.activity_vo_sync_new.*
import kotlinx.android.synthetic.main.customealertdialogelogin.*
import kotlinx.android.synthetic.main.customealertdialogelogin.view.*
import kotlinx.android.synthetic.main.dialoge.view.*
import kotlinx.android.synthetic.main.so_syncsearchtoolbar.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.apache.http.conn.ConnectTimeoutException
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.net.SocketTimeoutException

class VoSyncActivity : AppCompatActivity() {

    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: ApiInterface? = null
    var validate: Validate? = null

    var federationViewmodel: FedrationViewModel? = null
    var ecMemberViewModel: ExecutiveMemberViewmodel? = null
    var scViewModel: SubcommitteeViewModel? = null
    var cboPhoneViewmodel: CboPhoneViewmodel? = null
    var cboAddressViewmodel: CboAddressViewmodel? = null
    var cboBankViewmodel: CboBankViewmodel? = null
    var cboKycViewmodel: CboKycViewmodel? = null
    var imageUploadViewmodel: ImageUploadViewmodel? = null
    var responseViewModel: ResponseViewModel? = null
    var iDownload = 0
    var cboType = 0
    var transactionViewmodel: TransactionViewmodel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vo_sync_new)

        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        validate = Validate(this)
        IvScan.visibility = View.GONE
        federationViewmodel = ViewModelProviders.of(this).get(FedrationViewModel::class.java)
        ecMemberViewModel = ViewModelProviders.of(this).get(ExecutiveMemberViewmodel::class.java)
        cboPhoneViewmodel = ViewModelProviders.of(this).get(CboPhoneViewmodel::class.java)
        cboAddressViewmodel = ViewModelProviders.of(this).get(CboAddressViewmodel::class.java)
        cboBankViewmodel = ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        cboKycViewmodel = ViewModelProviders.of(this).get(CboKycViewmodel::class.java)
        scViewModel = ViewModelProviders.of(this).get(SubcommitteeViewModel::class.java)
        transactionViewmodel = ViewModelProviders.of(this).get(TransactionViewmodel::class.java)
        responseViewModel = ViewModelProviders.of(this).get(ResponseViewModel::class.java)
        imageUploadViewmodel =
            ViewModelProviders.of(this).get(ImageUploadViewmodel::class.java)

        if (validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType) == 2) {
            cboType = 2
        } else {
            cboType = 1
        }

        setLabelText()

        tbl_download.setOnClickListener {
            layout_download.visibility=View.VISIBLE
            layout_upload.visibility=View.GONE
            tbl_UploadMsg.visibility=View.GONE
            tbl_download.background=getDrawable(R.drawable.color_primary_bg)
            tbl_upload.background=getDrawable(R.drawable.light_gray_bg)
            img_download.setImageResource(R.drawable.ic_white_download)
            img_upload.setImageResource(R.drawable.ic_gray)
            tv_download.setTextColor(getColor(R.color.white))
            tv_upload.setTextColor(getColor(R.color.black))
            layout_upload.visibility=View.GONE

        }
        tbl_upload.setOnClickListener {
            layout_download.visibility=View.GONE
            layout_upload.visibility=View.VISIBLE
            tbl_UploadMsg.visibility=View.VISIBLE
            tbl_download.background=getDrawable(R.drawable.light_gray_bg)
            tbl_upload.background=getDrawable(R.drawable.color_primary_bg)
            img_download.setImageResource(R.drawable.ic_downloadgray)
            img_upload.setImageResource(R.drawable.ic_white)
            tv_download.setTextColor(getColor(R.color.black))
            tv_upload.setTextColor(getColor(R.color.white))

        }
        tbl_download_status.setOnClickListener {
            if (isNetworkConnected()) {

                //       importSHGlist()
                importFederationStatus()

            } else {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "no_internet_msg",
                        R.string.no_internet_msg
                    ), this
                )
            }
        }
        tbl_downloaduploadstatus.setOnClickListener {
            if (isNetworkConnected()) {
                importuploadstatus()

            } else {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "no_internet_msg",
                        R.string.no_internet_msg
                    ), this
                )
            }
        }
        tbl_downloaddata.setOnClickListener {
            if (isNetworkConnected()) {
                importFederationlist()

            } else {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "no_internet_msg",
                        R.string.no_internet_msg
                    ), this
                )
            }
        }

        tbl_uploaddata.setOnClickListener {
            if (isNetworkConnected()) {
                var federationlist = federationViewmodel!!.getAllFederationdata(cboType)

                exportData(federationlist)

            } else {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "no_internet_msg",
                        R.string.no_internet_msg
                    ), this
                )
            }
        }

        tbl_ec_upload.setOnClickListener {
            if (isNetworkConnected()) {
                var ecMemberlist = ecMemberViewModel!!.getPendingExecutiveMember()
                exportEcData(ecMemberlist)
            } else {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "no_internet_msg",
                        R.string.no_internet_msg
                    ), this
                )
            }
        }

        tbl_upload_sc.setOnClickListener {
            if (isNetworkConnected()) {
                var scList = scViewModel!!.getCommitteePendingdata()
                //    exportDatawithimage(shglist)
                if (!scList.isNullOrEmpty()) {
                    //      CustomAlertlogin()
                    exportSubcommittee(scList)
                } else {
                    val text = LabelSet.getText(
                        "nothing_upload",
                        R.string.nothing_upload
                    )
                    validate!!.CustomAlertVO(text, this@VoSyncActivity)
                }
            } else {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "no_internet_msg",
                        R.string.no_internet_msg
                    ), this
                )
            }
        }
    }

    fun setLabelText() {
        var federationlist = federationViewmodel!!.getAllFederationdata(cboType)
        tvUploadData.text = LabelSet.getText(
            "upload_data",
            R.string.upload_data
        )

        if (cboType == 1) {
            tv_title.text = LabelSet.getText(
                "vo_profile_sync",
                R.string.vo_profile_sync
            )
            tvUploadData.text = Html.fromHtml(
                LabelSet.getText(
                    "upload_data",
                    R.string.upload_data
                ) + "\n" + LabelSet.getText(
                    "VOrecordtobeuploaded",
                    R.string.VOrecordtobeuploaded
                ) + " (" + federationlist!!.size + ")"
            )
        } else {
            tv_title.text = LabelSet.getText(
                "clf_profile_sync",
                R.string.clf_profile_sync
            )
            tvUploadData.text = Html.fromHtml(
                LabelSet.getText(
                    "upload_data",
                    R.string.upload_data
                ) + "\n" + LabelSet.getText(
                    "CLFrecordtobeuploaded",
                    R.string.CLFrecordtobeuploaded
                ) + " (" + federationlist!!.size + ")"
            )
        }
        tvUploadDataMsg.text = LabelSet.getText(
            "EC_screen_with_plus_button",
            R.string.EC_screen_with_plus_button
        )
        tvStatusData.text = LabelSet.getText(
            "upload_sc",
            R.string.upload_sc
        )
        tbl_downloadData.text = LabelSet.getText(
            "complete_data",
            R.string.complete_data
        )
        tvMasterData.text = LabelSet.getText(
            "upload_ec_member",
            R.string.upload_ec_member
        )
        tvStatus_download.text = LabelSet.getText(
            "approval_status",
            R.string.approval_status
        )
        tv_downloaduploadstatus.text = LabelSet.getText(
            "upload_stauts",
            R.string.upload_stauts
        )
    }

    fun exportData(federationlist: List<FederationEntity>?) {
        progressDialog = ProgressDialog.show(
            this, LabelSet.getText("app_name", R.string.app_name),
            LabelSet.getText(
                "DataUploading",
                R.string.DataUploading
            )
        )
        progressDialog.setIcon(this.getDrawable(R.drawable.appicon))
        var idownload = 0
        var msg = ""
        var code = 0
        object : Thread() {

            //@SuppressLint("NewApi")
            override fun run() {
                try {

                    if (federationlist!!.size > 0) {
                        for (j in 0..federationlist.size - 1) {

                            var federationlistmodel = federationlist.get(j)
                            var cbophone =
                                cboPhoneViewmodel!!.getuploadlist(federationlist.get(j).guid)
                            var cboaddress =
                                cboAddressViewmodel!!.getAddressDatalist(federationlist.get(j).guid)
                            var cbobank =
                                cboBankViewmodel!!.getBankdatalist(federationlist.get(j).guid)
                            var kyclist =
                                cboKycViewmodel!!.getkycdatalist(federationlist.get(j).guid)

                            var voUploadModel: VOUploadModel = VOUploadModel()


                            voUploadModel = MappingData.returnObject(federationlistmodel)
                            voUploadModel.cboPhoneNoDetailsList = cbophone
                            voUploadModel.cboAddressesList = cboaddress
                            voUploadModel.cboBankDetailsList = cbobank
                            voUploadModel.cboKYCDetailsList = kyclist
                            var transactionid = validate!!.randomtransactionno()
                            voUploadModel.transaction_id = transactionid

                            var gsonbuilder = GsonBuilder()
                            gsonbuilder.serializeNulls()
                            val gson: Gson = gsonbuilder.setPrettyPrinting().create()
                            val json = gson.toJson(voUploadModel)
                            val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                            val token = validate!!.returnStringValue(AESEncryption.decrypt(
                                validate!!.RetriveSharepreferenceString(AppSP.token),
                                validate!!.RetriveSharepreferenceString(AppSP.userid)
                            ))
                            val file =
                                RequestBody.create(MediaType.parse("application/json"), json)


                            var arr: MultipartBody
                            var sFullImagePath = ""
                            val mediaStorageDir = File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
                                AppSP.IMAGE_DIRECTORY_NAME)

                            var imagefile1 = MultipartBody.Builder()
                                .addFormDataPart("federationProfile", "federationProfile", file)
                            var imageList =
                                imageUploadViewmodel!!.getimage_by_guid(federationlist.get(j).guid)
                            for (k in 0..imageList!!.size - 1) {
                                if (imageList.get(k).image_name.length > 0) {
                                    sFullImagePath =
                                        mediaStorageDir.path + File.separator + imageList.get(
                                            k
                                        ).image_name
                                    val file1 = File(sFullImagePath)
                                    if (file1.exists()) {
                                        val file3 =
                                            RequestBody.create(
                                                MediaType.parse("multipart/form-data"),
                                                file1
                                            )
                                        imagefile1.addFormDataPart("uploadFiles", file1.name, file3)
                                    }
                                }
                            }
                            arr = imagefile1.build()
                            val call = apiInterface!!.uploadfederationdata(
                                "multipart/form-data; boundary=" + arr.boundary(),
                                user,
                                token, arr
                            )

                            val res = call.execute()

                            if (res!!.isSuccessful) {
                                if (res.body()?.msg.equals("Record Added To Queue")) {
                                    try {
                                        CDFIApplication.database?.fedrationDao()
                                            ?.updateuploadStatus(
                                                federationlist.get(j).guid,
                                                validate!!.Daybetweentime(validate!!.currentdatetime)
                                            )
                                        CDFIApplication.database?.cboBankDao()
                                            ?.updateBankDetailisedit(federationlist.get(j).guid,validate!!.Daybetweentime(validate!!.currentdatetime))
                                        CDFIApplication.database?.cboaddressDao()
                                            ?.updateisedit(federationlist.get(j).guid,validate!!.Daybetweentime(validate!!.currentdatetime))
                                        CDFIApplication.database?.cbophoneDao()
                                            ?.updateisedit(federationlist.get(j).guid,validate!!.Daybetweentime(validate!!.currentdatetime))
                                        CDFIApplication.database?.cbokycDao()?.updateisedit(federationlist.get(j).guid,validate!!.Daybetweentime(validate!!.currentdatetime))
                                        var transaction = TransactionEntity(
                                            0, transactionid
                                            ,
                                            federationlist.get(j).guid,
                                            "",
                                            3, 0, 0,""
                                        )
                                        if (federationlist.get(j).is_edited == 1 && federationlist.get(
                                                j
                                            ).approve_status == 1
                                        ) {
                                            transactionViewmodel!!.insert(transaction)
                                        }
                                        msg = res.body()?.msg!!

                                    } catch (ex: Exception) {
                                        ex.printStackTrace()
//                                    progressDialog.dismiss()
                                    }
                                }
                            } else {
                                msg = "" + res.code() + " " + res.message()
                                code = res.code()
                                idownload = 1
//
                            }
                        }


                    } else {
                        idownload = -1
                    }

                    runOnUiThread {
                        if (idownload == 0) {
                            progressDialog.dismiss()
                            val text = msg
                            var federationlist1 =
                                federationViewmodel!!.getAllFederationdata(cboType)


                            if (cboType == 1) {
                                tvUploadData.text = Html.fromHtml(
                                    LabelSet.getText(
                                        "upload_data",
                                        R.string.upload_data
                                    ) + "\n" + LabelSet.getText(
                                        "VOrecordtobeuploaded",
                                        R.string.VOrecordtobeuploaded
                                    ) + " (" + federationlist1!!.size + ")"
                                )
                            } else {
                                tvUploadData.text = Html.fromHtml(
                                    LabelSet.getText(
                                        "upload_data",
                                        R.string.upload_data
                                    ) + "\n" + LabelSet.getText(
                                        "CLFrecordtobeuploaded",
                                        R.string.CLFrecordtobeuploaded
                                    ) + " (" + federationlist1!!.size + ")"
                                )
                            }
                            validate!!.CustomAlertVO(text, this@VoSyncActivity)
                        } else if (idownload == -1) {
                            progressDialog.dismiss()
                            val text = LabelSet.getText(
                                "nothing_upload",
                                R.string.nothing_upload
                            )
                            validate!!.CustomAlertVO(text, this@VoSyncActivity)
                        } else {
                            progressDialog.dismiss()
                            if (code == 403) {
                                Toast.makeText(this@VoSyncActivity, msg, Toast.LENGTH_LONG)
                                    .show()
                                CustomAlertlogin()
                            } else {
                                val text = LabelSet.getText(
                                    "DataUploadingFailed",
                                    R.string.DataUploadingFailed
                                )
                                validate!!.CustomAlertVO(text, this@VoSyncActivity)
                                Toast.makeText(this@VoSyncActivity, msg, Toast.LENGTH_LONG)
                                    .show()
                            }
                        }


                    }
                } catch (bug: SocketTimeoutException) {
                    progressDialog.dismiss()
//                                Toast.makeText(activity!!, "Socket Timeout", Toast.LENGTH_LONG).show();
                } catch (bug: ConnectTimeoutException) {
                    progressDialog.dismiss()
                    // Toast.makeText(getApplicationContext(), "Connection Timeout", Toast.LENGTH_LONG).show();
                } catch (e: Exception) {
                    e.printStackTrace()
                    progressDialog.dismiss()
                }
            }

        }.start()
    }

    private fun isNetworkConnected(): Boolean {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager //1
        val networkInfo = connectivityManager.activeNetworkInfo //2
        return networkInfo != null && networkInfo.isConnected //3
    }

    fun importVO_SHG_Mapping() {
        progressDialog = ProgressDialog.show(
            this, LabelSet.getText("app_name", R.string.app_name),
            LabelSet.getText("DataLoading", R.string.DataLoading)
        )
        progressDialog.setIcon(this.getDrawable(R.drawable.appicon))
        var idownload = 0
        object : Thread() {

            //@SuppressLint("NewApi")
            override fun run() {
                try {

                    val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                    val token = validate!!.returnStringValue(AESEncryption.decrypt(
                        validate!!.RetriveSharepreferenceString(AppSP.token),
                        validate!!.RetriveSharepreferenceString(AppSP.userid)
                    ))
                    val cbo_guid = "4jxx9fNxyoZQjzNDf4FAEqKL7vd2vA2021013016095172"

                    val call = apiInterface?.getVO_Shg_Mappingdata(
                        token,
                        user,
                        cbo_guid,
                        cboType
                    )

                    val res = call?.execute()

                    if (res!!.isSuccessful) {
                        if (!res.body()?.VO_Shg_mapped_response.isNullOrEmpty()) {
                            try {
                                var mapped_Shg = res.body()?.VO_Shg_mapped_response
                                var shglist = MappingData.returnVO_Shg_listObj(mapped_Shg)
                                if (!shglist.isNullOrEmpty()) {
                                    CDFIApplication.database?.mappedShgDao()
                                        ?.insertMapped_data(shglist)
                                }
                                for (i in mapped_Shg!!.indices) {
                                    var member_details_list = mapped_Shg.get(i).memberDetailsList
                                    var member_list = returnVO_Shgmemberentity(member_details_list)
                                    CDFIApplication.database?.voShgMemberDao()
                                        ?.insertMapped_Memberdata(member_list)
                                    for (j in member_details_list!!.indices) {
                                        var memberPhoneList =
                                            member_details_list.get(j).memberPhoneDetailsList!!
                                        CDFIApplication.database?.vOShgMemberPhoneDao()
                                            ?.insertMapped_MemberPhonedata((memberPhoneList))
                                        CDFIApplication.database?.voShgMemberDao()
                                            ?.updateVO_shgMember(
                                                mapped_Shg.get(i).guid,
                                                mapped_Shg.get(i).shg_name,
                                                member_details_list.get(j).member_guid,
                                                mapped_Shg.get(i).vo_guid
                                            )
                                    }

                                }
                                idownload = 0
                                progressDialog.dismiss()
                            } catch (ex: Exception) {
                                ex.printStackTrace()
                            }

                        } else {
                            idownload = 0
                        }
                    } else {
                        idownload = 1
                    }




                    runOnUiThread {
                        if (idownload == 0) {
                            progressDialog.dismiss()
                            val text = LabelSet.getText(
                                "Datadownloadedsuccessfully",
                                R.string.Datadownloadedsuccessfully
                            )
                            validate!!.CustomAlertVO(text, this@VoSyncActivity)
                        } else {
                            progressDialog.dismiss()
                            val text = LabelSet.getText(
                                "DatadownloadingFailed",
                                R.string.DatadownloadingFailed
                            )
                            validate!!.CustomAlertVO(text, this@VoSyncActivity)
                        }

                    }

                } catch (bug: SocketTimeoutException) {
                    progressDialog.dismiss()
//                                Toast.makeText(activity!!, "Socket Timeout", Toast.LENGTH_LONG).show();
                } catch (bug: ConnectTimeoutException) {
                    progressDialog.dismiss()
                    // Toast.makeText(getApplicationContext(), "Connection Timeout", Toast.LENGTH_LONG).show();
                } catch (e: Exception) {
                    e.printStackTrace()
                    progressDialog.dismiss()
                }
            }

        }.start()
    }

    fun importFederationlist() {
        progressDialog = ProgressDialog.show(
            this, LabelSet.getText("app_name", R.string.app_name),
            LabelSet.getText("DataLoading", R.string.DataLoading)
        )
        progressDialog.setIcon(this.getDrawable(R.drawable.appicon))
        var idownload = 0
        var msg = ""
        var code = 0
        object : Thread() {

            //@SuppressLint("NewApi")
            override fun run() {
                try {

                    val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                    val token = validate!!.returnStringValue(AESEncryption.decrypt(
                        validate!!.RetriveSharepreferenceString(AppSP.token),
                        validate!!.RetriveSharepreferenceString(AppSP.userid)
                    ))

                    val call1 = apiInterface?.getFederationlist(
                        token,
                        user,
                        cboType
                    )

                    val res1 = call1?.execute()

                    if (res1!!.isSuccessful) {
                        try {
                            msg = "" + res1.code() + " " + res1.message()

                            if (!res1.body()?.federationList!!.isNullOrEmpty()) {
                                CDFIApplication.database?.fedrationDao()
                                    ?.insertFedrationDetail(res1.body()?.federationList)
                                var voList = res1.body()?.federationList!!
                                for (i in voList.indices) {
                                    getFederationdata(
                                        validate!!.returnLongValue(voList.get(i).federation_id.toString()),
                                        progressDialog,
                                        LabelSet.getText(
                                            "DataLoading",
                                            R.string.DataLoading
                                        ) + " " + (i + 1) + "/" + voList.size
                                    )
                                    // progressDialog.setTitle(LabelSet.getText("lokos_core",R.string.lokos_core)R.string.DataLoading) + " "+ (i + 1) + "/" + shglist.size)
//
                                }
                            }

                            idownload = 1
                            if(idownload == 1){
                                importIFSC()
                            }
                            progressDialog.dismiss()

                        } catch (ex: Exception) {
                            ex.printStackTrace()
//                                    progressDialog.dismiss()
                        }

                    } else {
                        if(res1.errorBody()?.contentLength()==0L || res1.errorBody()?.contentLength()!!<0L){
                            code = res1.code()
                            msg = res1.message()
                        }else {
                            var jsonObject1 =
                                JSONObject(res1.errorBody()!!.source().readUtf8().toString())

                            code =
                                validate!!.returnIntegerValue(
                                    jsonObject1.optString("responseCode").toString()
                                )
                            msg = validate!!.returnStringValue(
                                jsonObject1.optString("responseMsg").toString()
                            )
                        }

                    }

                    runOnUiThread {
                        if (idownload == 1) {
                            progressDialog.dismiss()
                            val text = LabelSet.getText(
                                "Datadownloadedsuccessfully",
                                R.string.Datadownloadedsuccessfully
                            )
                            validate!!.CustomAlertVO(text, this@VoSyncActivity)
                        } else {
                            progressDialog.dismiss()
                            if (code == 403) {
                                Toast.makeText(this@VoSyncActivity, msg, Toast.LENGTH_LONG).show()
                                CustomAlertlogin()
                            } else {
                                val text = msg
                                validate!!.CustomAlertVO(text, this@VoSyncActivity)
                                Toast.makeText(this@VoSyncActivity, msg, Toast.LENGTH_LONG).show()
                            }
                        }

                    }

                } catch (bug: SocketTimeoutException) {
                    progressDialog.dismiss()
//                                Toast.makeText(activity!!, "Socket Timeout", Toast.LENGTH_LONG).show();
                } catch (bug: ConnectTimeoutException) {
                    progressDialog.dismiss()
                    // Toast.makeText(getApplicationContext(), "Connection Timeout", Toast.LENGTH_LONG).show();
                } catch (e: Exception) {
                    e.printStackTrace()
                    progressDialog.dismiss()
                }
            }

        }.start()

    }

    fun getFederationdata(federation_id: Long, progressDialog: ProgressDialog, str: String) {

        val userId = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
        val token = validate!!.returnStringValue(AESEncryption.decrypt(
            validate!!.RetriveSharepreferenceString(AppSP.token),
            validate!!.RetriveSharepreferenceString(AppSP.userid)
        ))

        val call = apiInterface?.getFederationData(
            "application/json", token, userId,
            federation_id
        )
        val response = call!!.execute()
        if (response.isSuccessful) {
            when (response.code()) {
                200 -> {

                    if (response.body()?.voDownloadModel != null) {
                        try {
                            var federation = response.body()?.voDownloadModel
                            var federationaddress = federation!!.cboAddressesList
                            var federationbank = federation.cboBankDetailsList
                            var federationphone = federation.cboPhoneNoDetailsList
                            var federationkyc = federation.cboKYCDetailsList
                            var ecMemberList = federation.ecMembersList
                            var scModelList = federation.subCommitteeList
                            var scList = MappingData.returnScEntity(scModelList)
                            if (!scList.isNullOrEmpty()) {
                                CDFIApplication.database?.subcommitteeDao()
                                    ?.insertCommittee(scList)
                            }
                            for (i in scModelList!!.indices) {
                                var scMemberList = scModelList.get(i).subCommitteeMemberList
                                if (!scMemberList.isNullOrEmpty()) {
                                    CDFIApplication.database?.subcommitee_memberDao()
                                        ?.insertCommitteeMember(scMemberList)
                                }
                            }
                            var federationlist = returnObjentity(federation)
                            if (federationlist != null) {
                                CDFIApplication.database?.fedrationDao()
                                    ?.insertFedrationDetailData(federationlist)
                            }
                            if (!federationaddress.isNullOrEmpty()) {
                                CDFIApplication.database?.cboaddressDao()
                                    ?.insertAddressDetail(
                                        federationaddress
                                    )
                            }
                            if (!federationkyc.isNullOrEmpty()) {
                                CDFIApplication.database?.cbokycDao()
                                    ?.insertKycDetail(federationkyc)
                            }
                            if (!ecMemberList.isNullOrEmpty()) {
                                CDFIApplication.database?.executiveDao()
                                    ?.insertExectiveDetail(ecMemberList)
                            }

                            if (!federationphone.isNullOrEmpty()) {
                                CDFIApplication.database?.cbophoneDao()
                                    ?.insertPhoneDetail(
                                        federationphone
                                    )
                            }
                            if (!federationbank.isNullOrEmpty()) {
                                CDFIApplication.database?.cboBankDao()
                                    ?.insertBankDetail(
                                        federationbank
                                    )
                            }

                            progressDialog.setMessage(str)
//
                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }
                    }

                }

            }

        } else {
            // iDownload = 0

        } // if
    }

    fun CustomAlertlogin() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogelogin, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mDialogView.etUsername.setText(validate!!.RetriveSharepreferenceString(AppSP.userid))
        mDialogView.etPassword.setText(validate!!.returnStringValue(AESEncryption.decrypt(
            validate!!.RetriveSharepreferenceString(AppSP.Password),
            validate!!.RetriveSharepreferenceString(AppSP.userid)
        )))
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.btn_login.setBackgroundColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.txt_dialog_title2.text = LabelSet.getText("app_name",R.string.app_name)
        mDialogView.btn_login.text = LabelSet.getText("log_in",R.string.log_in)
        mDialogView.chk_showHide1.text = LabelSet.getText("show_password",R.string.show_password)
        mDialogView.btn_login.setOnClickListener {
            mAlertDialog.dismiss()
            importtoken(
                mDialogView.etUsername.text.toString(),
                validate!!.returnStringValue(
                    AESEncryption.encrypt(validate!!.returnStringValue(
                        mDialogView.etPassword.text.toString()),mDialogView.etUsername.text.toString()))
            )
        }
        mAlertDialog.chk_showHide1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                mAlertDialog.etPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                mAlertDialog.etPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
        }

    }

    fun importtoken(user: String, pass: String) {
        var grant_type = "password"
        var userId = user
        var password = pass
        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText(
                "authenticate_user",
                R.string.authenticate_user
            )
        )


        val callCount = apiInterface?.gettokenforce(
            grant_type,
            userId,
            password, validate!!.RetriveSharepreferenceInt(AppSP.State_Selected), 1,
            validate!!.RetriveSharepreferenceString(AppSP.Roleid)!!
        )
        callCount?.enqueue(object : Callback<JsonObject> {
            override fun onFailure(callCount: Call<JsonObject>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                progressDialog.dismiss()


                if (response.isSuccessful) {

                    var jsonObject1: JSONObject? = null
                    try {
                        jsonObject1 = JSONObject(response.body().toString())

                        val auth = jsonObject1.get("accessToken").toString()
                        if (auth.trim().length > 0) {
                            validate!!.SaveSharepreferenceString(
                                AppSP.token,
                                validate!!.returnStringValue(
                                    AESEncryption.encrypt(
                                        auth,validate!!.RetriveSharepreferenceString(AppSP.userid)))
                            )
                        }
                        validate!!.CustomAlertVO(
                            LabelSet.getText(
                                "userauthenticatedsuccessfully",
                                R.string.userauthenticatedsuccessfully
                            ), this@VoSyncActivity
                        )

                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Toast.makeText(this@VoSyncActivity, response.message(), Toast.LENGTH_LONG)
                            .show()


                    }

                } else {
                    Toast.makeText(this@VoSyncActivity, response.message(), Toast.LENGTH_LONG)
                        .show()
                }


            }

        })


    }

    fun exportEcData(ecMemberlist: List<Executive_memberEntity>?) {
        progressDialog = ProgressDialog.show(
            this, LabelSet.getText("app_name", R.string.app_name),
            LabelSet.getText(
                "DataUploading",
                R.string.DataUploading
            )
        )
        progressDialog.setIcon(this.getDrawable(R.drawable.appicon))
        var idownload = 0
        var msg = ""
        var code = 0
        object : Thread() {

            //@SuppressLint("NewApi")
            override fun run() {
                try {

                    if (ecMemberlist!!.size > 0) {

                        val gson = Gson()
                        val json = gson.toJson(ecMemberlist)
                        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                        val token = validate!!.returnStringValue(AESEncryption.decrypt(
                            validate!!.RetriveSharepreferenceString(AppSP.token),
                            validate!!.RetriveSharepreferenceString(AppSP.userid)
                        ))

                        val sData = RequestBody.create(
                            MediaType.parse("application/json; charset=utf-8"),
                            json
                        )
                        val call = apiInterface?.uploadEcMemberdata(
                            "application/json",
                            user,
                            token,
                            sData
                        )

                        val res = call?.execute()

                        if (res!!.isSuccessful) {
                            try {
                                CDFIApplication.database?.executiveDao()
                                    ?.updateuploadStatus(
                                        validate!!.Daybetweentime(validate!!.currentdatetime)
                                    )
                                //       msg = res.body()?.msg!!

                            } catch (ex: Exception) {
                                ex.printStackTrace()
//                                    progressDialog.dismiss()
                            }
                        } else {
                            msg = "" + res.code() + " " + res.message()
                            code = res.code()
                            idownload = 1
//
                        }


                    } else {
                        idownload = -1
                    }

                    runOnUiThread {
                        if (idownload == 0) {
                            progressDialog.dismiss()
                            val text = "Data uploaded successfully"
                            validate!!.CustomAlertVO(text, this@VoSyncActivity)
                        }else if (idownload == -1) {
                            progressDialog.dismiss()
                            val text = LabelSet.getText(
                                "nothing_upload",
                                R.string.nothing_upload
                            )
                            validate!!.CustomAlertVO(text, this@VoSyncActivity)
                        } else {
                            progressDialog.dismiss()
                            if (code == 403) {
                                Toast.makeText(this@VoSyncActivity, msg, Toast.LENGTH_LONG)
                                    .show()
                                CustomAlertlogin()
                            } else {

                                validate!!.CustomAlertVO(msg, this@VoSyncActivity)
                                Toast.makeText(this@VoSyncActivity, msg, Toast.LENGTH_LONG)
                                    .show()
                            }
                        }


                    }
                } catch (bug: SocketTimeoutException) {
                    progressDialog.dismiss()
//                                Toast.makeText(activity!!, "Socket Timeout", Toast.LENGTH_LONG).show();
                } catch (bug: ConnectTimeoutException) {
                    progressDialog.dismiss()
                    // Toast.makeText(getApplicationContext(), "Connection Timeout", Toast.LENGTH_LONG).show();
                } catch (e: Exception) {
                    e.printStackTrace()
                    progressDialog.dismiss()
                }
            }

        }.start()
    }

    fun exportSubcommittee(scList: List<subcommitteeEntity>?) {
        progressDialog = ProgressDialog.show(
            this, LabelSet.getText("app_name", R.string.app_name),
            LabelSet.getText(
                "DataUploading",
                R.string.DataUploading
            )
        )
        progressDialog.setIcon(this.getDrawable(R.drawable.appicon))
        var idownload = 0
        var msg = ""
        var code = 0
        object : Thread() {

            //@SuppressLint("NewApi")
            override fun run() {
                try {

                    if (scList!!.size > 0) {
                        for (j in 0..scList.size - 1) {

                            var scListmodel = scList.get(j)
                            var scMemberList =
                                scViewModel!!.getCommitteeMemberPendingdata(scList.get(j).subcommitee_guid)

                            var scUploadModel: SubcommitteeModel = SubcommitteeModel()


                            scUploadModel = MappingData.returnScObject(scListmodel)
                            scUploadModel.subCommitteeMemberList = scMemberList

                            val gson = Gson()
                            val json = gson.toJson(scUploadModel)
                            val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                            val token = validate!!.returnStringValue(AESEncryption.decrypt(
                                validate!!.RetriveSharepreferenceString(AppSP.token),
                                validate!!.RetriveSharepreferenceString(AppSP.userid)
                            ))

                            val sData = RequestBody.create(
                                MediaType.parse("application/json; charset=utf-8"),
                                json
                            )
                            val call = apiInterface?.uploadSCdata(
                                "application/json",
                                user,
                                token,
                                sData
                            )

                            val res = call?.execute()

                            if (res!!.isSuccessful) {
                                if (res.body()?.msg.equals("Record Added To Queue")) {
                                    try {
                                        scViewModel!!.updateuploadStatus(
                                            scList.get(j).subcommitee_guid,
                                            validate!!.Daybetweentime(validate!!.currentdatetime)
                                        )
                                        scViewModel!!.updateScuploadStatus(
                                            scList.get(j).subcommitee_guid,
                                            validate!!.Daybetweentime(validate!!.currentdatetime)
                                        )
                                        msg = res.body()?.msg!!

                                    } catch (ex: Exception) {
                                        ex.printStackTrace()
//                                    progressDialog.dismiss()
                                    }
                                }
                            } else {
                                msg = "" + res.code() + " " + res.message()
                                code = res.code()
                                idownload = 1
//
                            }
                        }


                    } else {
                        idownload = -1
                    }

                    runOnUiThread {
                        if (idownload == 0) {
                            progressDialog.dismiss()
                            val text = msg
                            validate!!.CustomAlertVO(text, this@VoSyncActivity)
                        } else if (idownload == -1) {
                            progressDialog.dismiss()
                            val text = LabelSet.getText(
                                "nothing_upload",
                                R.string.nothing_upload
                            )
                            validate!!.CustomAlertVO(text, this@VoSyncActivity)
                        }  else {
                            progressDialog.dismiss()
                            if (code == 403) {
                                Toast.makeText(this@VoSyncActivity, msg, Toast.LENGTH_LONG)
                                    .show()
                                CustomAlertlogin()
                            } else {

                                validate!!.CustomAlertVO(msg, this@VoSyncActivity)
                                Toast.makeText(this@VoSyncActivity, msg, Toast.LENGTH_LONG)
                                    .show()
                            }
                        }


                    }
                } catch (bug: SocketTimeoutException) {
                    progressDialog.dismiss()
//                                Toast.makeText(activity!!, "Socket Timeout", Toast.LENGTH_LONG).show();
                } catch (bug: ConnectTimeoutException) {
                    progressDialog.dismiss()
                    // Toast.makeText(getApplicationContext(), "Connection Timeout", Toast.LENGTH_LONG).show();
                } catch (e: Exception) {
                    e.printStackTrace()
                    progressDialog.dismiss()
                }
            }

        }.start()
    }

    fun importFederationStatus() {
        progressDialog = ProgressDialog.show(
            this, LabelSet.getText("app_name", R.string.app_name),
            LabelSet.getText("DataLoading", R.string.DataLoading)
        )
        progressDialog.setIcon(this.getDrawable(R.drawable.appicon))
        var idownload = 0
        var code = 0
        var msg = ""
        object : Thread() {

            //@SuppressLint("NewApi")
            override fun run() {
                try {

                    val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                    val token = validate!!.returnStringValue(AESEncryption.decrypt(
                        validate!!.RetriveSharepreferenceString(AppSP.token),
                        validate!!.RetriveSharepreferenceString(AppSP.userid)
                    ))

                    val call = apiInterface?.getnonSyncFederationByAS(
                        token,
                        user,
                        cboType
                    )

                    val res = call?.execute()

                    if (res!!.isSuccessful) {
                        if (!res.body()?.federationResponseStatus.isNullOrEmpty()) {
                            try {
                                for (i in 0 until res.body()?.federationResponseStatus!!.size) {
                                    federationViewmodel!!.updateFedrationDedupStatus(
                                        res.body()?.federationResponseStatus?.get(i)?.guid!!,
                                        res.body()?.federationResponseStatus?.get(i)?.code!!,
                                        res.body()?.federationResponseStatus?.get(i)?.activationStatus!!,
                                        res.body()?.federationResponseStatus?.get(i)?.approve_status!!,
                                        res.body()?.federationResponseStatus?.get(i)?.checker_remarks!!
                                    )

                                }
//
                            } catch (ex: Exception) {
                                ex.printStackTrace()
//                                    progressDialog.dismiss()
                            }
                        } else {
                            idownload = 0
                        }
                    } else {
                        idownload = 1
                        var resMsg = ""
                        if (res.code() == 403) {

                            code = res.code()
                            msg = res.message()
                        } else {
                            if(res.errorBody()?.contentLength()==0L || res.errorBody()?.contentLength()!!<0L){
                                code = res.code()
                                resMsg = res.message()
                            }else {
                                var jsonObject1 =
                                    JSONObject(res.errorBody()!!.source().readUtf8().toString())

                                code =
                                    validate!!.returnIntegerValue(
                                        jsonObject1.optString("responseCode").toString()
                                    )
                                resMsg = validate!!.returnStringValue(
                                    jsonObject1.optString("responseMsg").toString()
                                )
                            }
                            msg = "" + validate!!.alertMsg(
                                this@VoSyncActivity,
                                responseViewModel,
                                code,
                                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                                resMsg)

                        }
                    }


                    runOnUiThread {
                        if (idownload == 0) {
                            progressDialog.dismiss()
                            val text = LabelSet.getText(
                                "Datadownloadedsuccessfully",
                                R.string.Datadownloadedsuccessfully
                            )
                            CustomAlert(text)
                        } else {
                            progressDialog.dismiss()
                            if (code == 403) {
                                Toast.makeText(this@VoSyncActivity, msg, Toast.LENGTH_LONG).show()
                                CustomAlertlogin()
                            } else {
                                val text = msg
                                validate!!.CustomAlertVO(text, this@VoSyncActivity)
                                Toast.makeText(this@VoSyncActivity, msg, Toast.LENGTH_LONG).show()
                            }

                        }

                    }

                } catch (bug: SocketTimeoutException) {
                    progressDialog.dismiss()
//                                Toast.makeText(activity!!, "Socket Timeout", Toast.LENGTH_LONG).show();
                } catch (bug: ConnectTimeoutException) {
                    progressDialog.dismiss()
                    // Toast.makeText(getApplicationContext(), "Connection Timeout", Toast.LENGTH_LONG).show();
                } catch (e: Exception) {
                    e.printStackTrace()
                    progressDialog.dismiss()
                }
            }

        }.start()
    }

    fun importuploadstatus() {

        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText("DataLoading", R.string.DataLoading)
        )

        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
        val token = validate!!.returnStringValue(AESEncryption.decrypt(
            validate!!.RetriveSharepreferenceString(AppSP.token),
            validate!!.RetriveSharepreferenceString(AppSP.userid)
        ))
        val callCount = apiInterface?.getdownloadTransactionStatus(
            token,
            user
        )

        callCount?.enqueue(object : Callback<SHGResponse> {
            override fun onFailure(callCount: Call<SHGResponse>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<SHGResponse>,
                response: Response<SHGResponse>
            ) {

                progressDialog.dismiss()
                if (response.isSuccessful) {
                    if (!response.body()?.grouptransactionResponse.isNullOrEmpty()) {
                        try {
                            var list = response.body()?.grouptransactionResponse

                            for (i in list!!.indices) {
                                if (list.get(i).status == 3) {
                                    CDFIApplication.database?.fedrationDao()
                                        ?.updatetransactionstatus(
                                            list.get(i).transaction_id
                                        )

                                }
                                CDFIApplication.database?.transactionDao()
                                    ?.updatetransactionstatus(
                                        list.get(i).transaction_id, list.get(i).status, list.get(i).remarks
                                    )
                            }
                            val text = LabelSet.getText(
                                "Datadownloadedsuccessfully",
                                R.string.Datadownloadedsuccessfully
                            )
                            CustomAlert(text)

                        } catch (ex: Exception) {
                            ex.printStackTrace()
                            //                                    progressDialog.dismiss()
                        }
                    }
                } else {
                    var resCode = 0
                    var resMsg = ""
                    progressDialog.dismiss()
                    if (response.code() == 403) {

                        CustomAlertlogin()
                    } else {
                        if(response.errorBody()?.contentLength()==0L || response.errorBody()?.contentLength()!!<0L){
                            resCode = response.code()
                            resMsg = response.message()
                        }else {
                            var jsonObject1 =
                                JSONObject(response.errorBody()!!.source().readUtf8().toString())

                            resCode =
                                validate!!.returnIntegerValue(
                                    jsonObject1.optString("responseCode").toString()
                                )
                            resMsg = validate!!.returnStringValue(
                                jsonObject1.optString("responseMsg").toString()
                            )
                        }
                        validate!!.CustomAlertMsgVO(
                            this@VoSyncActivity,
                            responseViewModel,
                            resCode,
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                            resMsg)

                    }

                }


            }

        })
    }

    fun CustomAlert(str: String) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialoge, null)
        val mBuilder = android.app.AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.btn_ok.setBackgroundColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.txt_msg.text = str
        mDialogView.btn_ok.setOnClickListener {


            mAlertDialog.dismiss()


        }
    }

    override fun onBackPressed() {
        var intent = Intent(this, VoDrawerActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    fun importIFSC(){
        var ifscList = federationViewmodel!!.getIfscCode()
        for (i in 0 until ifscList.size){
            importBankList(ifscList.get(i))
        }
    }

    fun importBankList(ifscCode: String) {
        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
        val token = validate!!.returnStringValue(AESEncryption.decrypt(
            validate!!.RetriveSharepreferenceString(AppSP.token),
            validate!!.RetriveSharepreferenceString(AppSP.userid)
        ))

        val call1 = apiInterface?.getBankList(
            token,
            user, ifscCode
        )

        var bankResponse = call1?.execute()
        if(bankResponse!!.isSuccessful){


            var alldata = bankResponse.body()!!.source().readUtf8().toString()
            val objectMapper = ObjectMapper()

            val langList: List<Bank_branchEntity> =
                objectMapper.readValue(
                    alldata,
                    object : TypeReference<List<Bank_branchEntity>>() {})
            if (langList != null && langList.size >0) {

                CDFIApplication.database?.masterbankbranchDao()
                    ?.insertbranch(
                        langList
                    )

            }

        }


    }
}
