package com.microware.cdfi.activity.vo

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.*
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.LocationViewModel
import kotlinx.android.synthetic.main.vo_layout_location.*

class VoVillageSelection  : AppCompatActivity() {

    var locationViewModel:LocationViewModel? = null
    var sStateCode = 0
    var sDistrictCode = 0
    var sBlockCode = 0
    var sPanchayatCode = 0
    var sVillageCode = 0
    var validate:Validate? = null
    var dataPanchayat : List<PanchayatEntity>? = null
    var dataVillage : List<VillageEntity>? = null
    var datastate : List<StateEntity>? = null
    var datadistrcit : List<DistrictEntity>? = null
    var datablock : List<BlockEntity>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.vo_layout_location)
        locationViewModel = ViewModelProviders.of(this).get(LocationViewModel::class.java)
        validate = Validate(this)
        setLabelText()
        sStateCode = validate!!.RetriveSharepreferenceInt(AppSP.statecode)
        sDistrictCode = validate!!.RetriveSharepreferenceInt(AppSP.districtcode)
        sBlockCode = validate!!.RetriveSharepreferenceInt(AppSP.blockcode)
        sPanchayatCode = validate!!.RetriveSharepreferenceInt(AppSP.panchayatcode)
        sVillageCode = validate!!.RetriveSharepreferenceInt(AppSP.villagecode)

        spin_state.isEnabled = false
        spin_district.isEnabled = false
        spin_block.isEnabled = false
        tbl_state.visibility = View.GONE
        if (validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType) == 1) {
            tbl_district.visibility = View.GONE
            tbl_village.visibility = View.GONE
            tbl_block.visibility = View.VISIBLE
            tbl_panchayat.visibility = View.VISIBLE
            spin_block.isEnabled = false
            spin_panchayat.isEnabled = true
        }else if (validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType) == 2) {
            tbl_district.visibility = View.VISIBLE
            tbl_block.visibility = View.VISIBLE
            tbl_panchayat.visibility = View.GONE
            tbl_village.visibility = View.GONE
            spin_block.isEnabled = true
            spin_district.isEnabled = false
        }


        spin_panchayat?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                sPanchayatCode = validate!!.returnPanchayatID(spin_panchayat,dataPanchayat)
                if (position > 0){
                    dataVillage =  locationViewModel!!.getVillagedata_by_panchayatCode(sStateCode, sDistrictCode, sBlockCode,sPanchayatCode)
                    validate!!.fillVillageSpinner(this@VoVillageSelection,spin_village,dataVillage)
                    spin_village.setSelection(validate!!.returnVillagepos(sVillageCode.toInt(),dataVillage))
                }else{
                    dataVillage =  locationViewModel!!.getVillagedata_by_panchayatCode(sStateCode, sDistrictCode, sBlockCode,sPanchayatCode)
                    validate!!.fillVillageSpinner(this@VoVillageSelection,spin_village,dataVillage)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        btn_location.setOnClickListener {
            if (checkValidation() == 1){
//              validate!!.SaveSharepreferenceString(AppSP.statecode,sStateCode)
//              validate!!.SaveSharepreferenceString(AppSP.districtcode,sDistrictCode)
//              validate!!.SaveSharepreferenceString(AppSP.panchayatcode,sPanchayatCode)
                sPanchayatCode = validate!!.returnPanchayatID(spin_panchayat,dataPanchayat)
                sVillageCode = validate!!.returnVillageID(spin_village,dataVillage)
                validate!!.SaveSharepreferenceInt(AppSP.panchayatcode,sPanchayatCode)
                validate!!.SaveSharepreferenceInt(AppSP.villagecode,sVillageCode)

                var intent = Intent(this,VoDrawerActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                finish()
            }
        }

        fillSpinner()
        ShowData()
//        bindStateSpinner()

    }

    private fun ShowData() {
        spin_state.setSelection(validate!!.returnStatepos(sStateCode.toInt(),datastate))
        spin_district.setSelection(validate!!.returnDistrictpos(sDistrictCode.toInt(),datadistrcit))
        spin_block.setSelection(validate!!.returnBlockpos(sBlockCode.toInt(),datablock))
        spin_panchayat.setSelection(validate!!.returnPanchayatpos(sPanchayatCode.toInt(),dataPanchayat))
        spin_village.setSelection(validate!!.returnVillagepos(sVillageCode.toInt(),dataVillage))
    }

    private fun fillSpinner() {
        datastate = locationViewModel!!.getStateByStateCode()
        datadistrcit = locationViewModel!!.getDistrictData(sStateCode)
        datablock = locationViewModel!!.getBlock_data(sStateCode,sDistrictCode)
        validate!!.fillStateSpinner(this,spin_state,datastate)
        validate!!.fillDistrictSpinner(this,spin_district,datadistrcit)
        validate!!.fillBlockSpinner(this,spin_block,datablock)

        dataPanchayat = locationViewModel!!.getPanchayatByPanchayatCode(sStateCode, sDistrictCode, sBlockCode)
        validate!!.fillPanchayatSpinner(this,spin_panchayat,dataPanchayat)

        dataVillage = locationViewModel!!.getVillagedata_by_panchayatCode(sStateCode, sDistrictCode, sBlockCode,sPanchayatCode)
        validate!!.fillVillageSpinner(this,spin_village,dataVillage)


    }

    fun bindStateSpinner() {

        locationViewModel?.getState()?.observe(this, Observer { stateData ->
            if (stateData != null) {
                val iGen = stateData.size
                val name = arrayOfNulls<String>(iGen + 1)
                name[0] = resources.getString(R.string.Select)

                for (i in 0 until stateData.size) {
                    name[i + 1] = stateData.get(i).state_name_en
                }
                val adapter_category = ArrayAdapter<String>(
                    this,
                    R.layout.my_spinner, name
                )
                adapter_category.setDropDownViewResource(R.layout.my_spinner)
                spin_state?.adapter = adapter_category

            }
        })

        spin_state?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                // TODO Auto-generated method stub

                if (position > 0) {
                    sStateCode = returnStateID(position)
                    bindDistrictSpinner(sStateCode)
                    img_state.setImageResource(R.drawable.state_s)
                }else{
                    img_state.setImageResource(R.drawable.state_un)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

    }

    fun bindDistrictSpinner(stateCode:Int) {
        locationViewModel?.getDistrictByStateCode(stateCode)?.observe(this, Observer { districtData ->
            if (districtData != null) {
                val iGen = districtData.size
                val name = arrayOfNulls<String>(iGen + 1)
                name[0] = resources.getString(R.string.Select)

                for (i in 0 until districtData.size) {
                    name[i + 1] = districtData.get(i).district_name_en
                }
                val adapter_category = ArrayAdapter<String>(
                    this,
                    R.layout.my_spinner_space, name
                )
                adapter_category.setDropDownViewResource(R.layout.my_spinner)
                spin_district?.adapter = adapter_category

            }
        })

        spin_district?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                // TODO Auto-generated method stub

                if (position > 0) {
                    sDistrictCode = returnDistrictID(position)
                    bindBlockSpinner(sStateCode,sDistrictCode)
                    img_district.setImageResource(R.drawable.district_s)
                }else{
                    img_district.setImageResource(R.drawable.district_un)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

    }

    fun bindBlockSpinner(stateCode:Int,districtCode:Int) {
        locationViewModel?.getBlock_district_code(stateCode,districtCode)?.observe(this, Observer { locationData ->
            if (locationData != null) {
                val iGen = locationData.size
                val name = arrayOfNulls<String>(iGen + 1)
                name[0] = resources.getString(R.string.Select)

                for (i in 0 until locationData.size) {
                    name[i + 1] = locationData.get(i).block_name_en
                }
                val adapter_category = ArrayAdapter<String>(
                    this, R.layout.my_spinner_space, name
                )
                adapter_category.setDropDownViewResource(R.layout.my_spinner)
                spin_block?.adapter = adapter_category

            }
        })


        spin_block?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                // TODO Auto-generated method stub

                if (position > 0) {
                    sBlockCode = returnBlockID(position)
                    bindPanchayatSpinner(sStateCode,sDistrictCode,sBlockCode)
                    img_block.setImageResource(R.drawable.block_s)
                }else{
                    img_block.setImageResource(R.drawable.block_un)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

    }

    fun bindPanchayatSpinner(stateCode: Int,districtCode: Int,blockCode: Int) {
        locationViewModel?.getPanchayatdataByPanchayatCode(stateCode,districtCode,blockCode)?.observe(this, Observer { panchayatData ->
            if (panchayatData != null) {
                val iGen = panchayatData.size
                val name = arrayOfNulls<String>(iGen + 1)
                name[0] = resources.getString(R.string.Select)

                for (i in 0 until panchayatData.size) {
                    name[i + 1] = panchayatData.get(i).panchayat_name_en
                }
                val adapter_category = ArrayAdapter<String>(
                    this, R.layout.my_spinner_space, name
                )
                adapter_category.setDropDownViewResource(R.layout.my_spinner)
                spin_panchayat?.adapter = adapter_category

            }
        })


        spin_panchayat?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                // TODO Auto-generated method stub

                if (position > 0) {
                    sPanchayatCode = returnPanchayatID(position)
                    bindVillageSpinner(sStateCode,sDistrictCode,sBlockCode,sPanchayatCode)
                    img_gp.setImageResource(R.drawable.grampanchayat_s)
                }else{
                    img_gp.setImageResource(R.drawable.grampanchayat_un)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }
       // setLabelText()
    }

    fun bindVillageSpinner(stateCode: Int,districtCode: Int,blockCode: Int,panchayatCode: Int) {
        locationViewModel?.getVillage_by_panchayatcode(stateCode,districtCode,blockCode,panchayatCode)?.observe(this, Observer { villageData ->
            if (villageData != null) {
                val iGen = villageData.size
                val name = arrayOfNulls<String>(iGen + 1)
                name[0] = resources.getString(R.string.Select)

                for (i in 0 until villageData.size) {
                    name[i + 1] = villageData.get(i).village_name_en
                }
                val adapter_category = ArrayAdapter<String>(
                    this, R.layout.my_spinner_space, name
                )
                adapter_category.setDropDownViewResource(R.layout.my_spinner)
                spin_village?.adapter = adapter_category

            }
        })


        spin_village?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                // TODO Auto-generated method stub

                if (position > 0) {
                    sVillageCode = returnVillageID(position)

                    img_vill.setImageResource(R.drawable.village_s)
                }else{
                    img_vill.setImageResource(R.drawable.village_un)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

    }

    fun returnBlockID(pos: Int): Int {
        var sPos = 0
        var block = ArrayList<BlockEntity>()

        block = locationViewModel?.getBlock_data(sStateCode,sDistrictCode) as ArrayList<BlockEntity>

        if (block != null && block.size > 0) {
            if (pos > 0) {

                sPos = block[pos - 1].block_id
            }
        }
        return sPos
    }

    fun returnPanchayatID(pos: Int): Int {
        var sPos = 0
        var panchayat = ArrayList<PanchayatEntity>()

        panchayat = locationViewModel?.getPanchayatByPanchayatCode(sStateCode,sDistrictCode,sBlockCode) as ArrayList<PanchayatEntity>

        if (panchayat != null && panchayat.size > 0) {
            if (pos > 0) {

                sPos = panchayat[pos - 1].panchayat_id
            }
        }
        return sPos
    }

    fun returnVillageID(pos: Int): Int {
        var sPos = 0
        var village = ArrayList<VillageEntity>()

        village = locationViewModel?.getVillagedata_by_panchayatCode(sStateCode,sDistrictCode,sBlockCode,sPanchayatCode) as ArrayList<VillageEntity>

        if (village != null && village.size > 0) {
            if (pos > 0) {

                sPos = village[pos - 1].village_id
            }
        }
        return sPos
    }

    fun returnStateID(pos: Int): Int {
        var sPos = 0
        var state = ArrayList<StateEntity>()

        state = locationViewModel?.getStateByStateCode() as ArrayList<StateEntity>

        if (state != null && state.size > 0) {
            if (pos > 0) {

                sPos = state[pos - 1].state_id
            }
        }
        return sPos
    }

    fun returnStatePosition(stateCode: Int): Int {
        var value: Int = 0
        var state = ArrayList<StateEntity>()

        state = locationViewModel?.getStateByStateCode(sStateCode) as ArrayList<StateEntity>
        if (state != null && state.size > 0) {
            for (i in 0 until state.size) {
                if (stateCode == state.get(i).state_id) {
                    value = i + 1
                    break
                }
            }
        }
        return value
    }

    fun returnDistrictID(pos: Int): Int {
        var sPos = 0
        var district = ArrayList<DistrictEntity>()

        district = locationViewModel?.getDistrictData(sStateCode) as ArrayList<DistrictEntity>

        if (district != null && district.size > 0) {
            if (pos > 0) {

                sPos = district[pos - 1].district_id
            }
        }
        return sPos
    }

    fun returnDistrictPosition(districtCode: Int): Int {
        var value: Int = 0
        var district = ArrayList<DistrictEntity>()

        district = locationViewModel?.getDistrictData(sStateCode) as ArrayList<DistrictEntity>
        if (district != null && district.size > 0) {
            for (i in 0 until district.size) {
                if (districtCode == district.get(i).district_id) {
                    value = i + 1
                    break
                }
            }
        }
        return value
    }

    fun returnBlockPosition(blockCode:Int): Int {
        var value: Int = 0
        var block = ArrayList<BlockEntity>()

        block = locationViewModel?.getBlock_data(sStateCode,sDistrictCode) as ArrayList<BlockEntity>

        if (block != null && block.size > 0) {
            for (i in 0 until block.size) {
                if (blockCode.equals(block.get(i).block_id)) {
                    value = i + 1
                    break
                }
            }
        }
        return value
    }

    private fun checkValidation():Int{
        var value= 1

        val sp = arrayOf<Spinner>(
            spin_state, spin_district, spin_block,spin_panchayat
        )

        for (i in sp.indices) {
            if (sp[i].selectedItemPosition == 0) {
                validate!!.CustomAlertSpinner(
                    this,
                    sp[i],
                    getString(R.string.Pleaseenetrmandatorydetails)
                )
                value = 0
                break
            }
        }


        return value
    }

    override fun onBackPressed() {
        val i = Intent(this, VoDrawerActivity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(i)
        finish()
    }
    fun setLabelText()
    {
        tvPanchayat.text = LabelSet.getText(
            "panchayat",
            R.string.panchayat
        )
        tvVillage.text = LabelSet.getText(
            "village1",
            R.string.village1
        )
    }
}