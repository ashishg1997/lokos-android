package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.api.meetingmodel.FundTypeModel
import com.microware.cdfi.entity.*
import com.microware.cdfi.entity.voentity.VoFinTxnDetMemEntity
import com.microware.cdfi.entity.voentity.VoMtgDetEntity
import com.microware.cdfi.fragment.OthersScreenTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.MstVOCOAViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoFinTxnDetMemViewModel
import com.microware.cdfi.viewModel.voviewmodel.VoFinTxnViewModel
import com.microware.cdfi.viewModel.voviewmodel.VoGenerateMeetingViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoMtgDetViewModel
import kotlinx.android.synthetic.main.adjustment.*
import kotlinx.android.synthetic.main.buttons_vo.*

class Adjustment : AppCompatActivity() {
    var validate: Validate? = null
    var voMtgDetViewModel: VoMtgDetViewModel? = null
    lateinit var lookupViewmodel: LookupViewmodel
    lateinit var voGenerateMeetingViewmodel: VoGenerateMeetingViewmodel
    lateinit var mstVOCOAViewmodel: MstVOCOAViewmodel
    lateinit var voFinTxnDetMemViewModel: VoFinTxnDetMemViewModel
    var shgmemberlistdebited: List<VoMtgDetEntity>? = null
    var shgmemberlistcredited: List<VoMtgDetEntity>? = null
    var dataspin_reason: List<LookupEntity>? = null
    var bankListDebited: List<Cbo_bankEntity>? = null
    var bankListCredited: List<Cbo_bankEntity>? = null
    var dataspin_fund: List<MstVOCOAEntity>? = null
    var dataspin_modeofpayment: List<LookupEntity>? = null
    var voFinTxnViewModel: VoFinTxnViewModel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.adjustment)

        validate = Validate(this)


        voMtgDetViewModel = ViewModelProviders.of(this).get(VoMtgDetViewModel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        voGenerateMeetingViewmodel =
            ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        mstVOCOAViewmodel = ViewModelProviders.of(this).get(MstVOCOAViewmodel::class.java)
        voFinTxnDetMemViewModel =
            ViewModelProviders.of(this).get(VoFinTxnDetMemViewModel::class.java)
        voFinTxnViewModel = ViewModelProviders.of(this).get(VoFinTxnViewModel::class.java)

        replaceFragmenty(
            fragment = OthersScreenTopBarFragment(27),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        et_referance_meeting_date.setOnClickListener {
            validate!!.datePicker(et_referance_meeting_date)
        }

        et_date_of_adjustment.setOnClickListener {
            validate!!.datePicker(et_date_of_adjustment)
        }

        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                SaveData()
            }
        }

        btn_cancel.setOnClickListener {
            val intent = Intent(this, SHGVoSHGTransactionSummary::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        spin_ModePayment?.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {

                override fun onItemSelected(
                    parent: AdapterView<*>, view: View?,
                    position: Int, id: Long
                ) {
                    if (position > 0) {
                        if (validate!!.returnlookupcode(
                                spin_ModePayment,
                                dataspin_modeofpayment
                            ) == 2
                        ) {
                            lay_account_no_amount_debited.visibility = View.VISIBLE
                            lay_DebitchequeNO.visibility = View.VISIBLE
                            lay_DebitchequeView.visibility = View.VISIBLE
                            viewDebit.visibility = View.VISIBLE
                        } else {
                            lay_account_no_amount_debited.visibility = View.GONE
                            viewDebit.visibility = View.GONE
                            lay_DebitchequeNO.visibility = View.GONE
                            lay_DebitchequeView.visibility = View.GONE
                            spin_DebitAccountNO.setSelection(0)
                            et_cheque_no_transactio_no.setText("")
                        }
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // TODO Auto-generated method stub

                }
            }

        spin_ModePayment1?.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {

                override fun onItemSelected(
                    parent: AdapterView<*>, view: View?,
                    position: Int, id: Long
                ) {
                    if (position > 0) {
                        if (validate!!.returnlookupcode(
                                spin_ModePayment1,
                                dataspin_modeofpayment
                            ) == 2
                        ) {
                            lay_account_no_amount_credited.visibility = View.VISIBLE
                            lay_CreditchequeNO.visibility = View.VISIBLE
                            lay_CreditchequeView.visibility = View.VISIBLE
                            viewCredit.visibility = View.VISIBLE
                        } else {
                            lay_account_no_amount_credited.visibility = View.GONE
                            viewCredit.visibility = View.GONE
                            lay_CreditchequeNO.visibility = View.GONE
                            lay_CreditchequeView.visibility = View.GONE
                            et_creditcheque_no_transactio_no.setText("")
                            spin_AccountCredit.setSelection(0)
                        }
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // TODO Auto-generated method stub

                }
            }

        spin_DebitAccountNO?.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {

                override fun onItemSelected(
                    parent: AdapterView<*>, view: View?,
                    position: Int, id: Long
                ) {
                    if (position > 0) {
                        fillbankCredited(returnbankguidDebited())
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // TODO Auto-generated method stub

                }
            }

        spin_CLF?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                if (position > 0) {
                    fillshgmemberspinnerCredited(returnshgmemdebitedid())
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        setLabelText()
        fillshgmemberspinnerDebited()
        fillshgmemberspinnerCredited(0)
        fillbankDebited()
        fillbankCredited("")
        fillspinner()
    }

    fun SaveData() {
        var amt = validate!!.returnIntegerValue(et_amount_of_adjustment.text.toString())
        var debitamt = amt * -1
        var voFinTxnDetMemEntity = VoFinTxnDetMemEntity(
            0,
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            returnshgmemcreditedid(),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.returnVOSubHeadcode(spinFundtype, dataspin_fund),/*auid*/
            "RO",
            amt,
            validate!!.returnlookupcode(spin_ModePayment1, dataspin_modeofpayment),
            validate!!.Daybetweentime(et_date_of_adjustment.text.toString()),/*date of adjustment*/
            returaccountCredited(),
            et_creditcheque_no_transactio_no.text.toString(),
            "",
            validate!!.Daybetweentime(et_referance_meeting_date.text.toString()),/*reference date*/
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            "",
            0,
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            0,
            validate!!.returnIntegerValue(et_referance_meeting_no.text.toString()),
            0,
            1
        )
        voFinTxnDetMemViewModel.insertVoGroupLoanSchedule(voFinTxnDetMemEntity)

        var voFinTxnDetMemEntitydebit = VoFinTxnDetMemEntity(
            0,
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            returnshgmemdebitedid(),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.returnVOSubHeadcode(spinFundtype, dataspin_fund),/*auid*/
            "RO",
            debitamt,
            validate!!.returnlookupcode(spin_ModePayment, dataspin_modeofpayment),
            validate!!.Daybetweentime(et_date_of_adjustment.text.toString()),/*date of adjustment*/
            returaccountDebited(),
            et_cheque_no_transactio_no.text.toString(),
            "",
            validate!!.Daybetweentime(et_referance_meeting_date.text.toString()),/*reference date*/
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            "",
            0,
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            0,
            validate!!.returnIntegerValue(et_referance_meeting_no.text.toString()), 0, 1
        )
        voFinTxnDetMemViewModel.insertVoGroupLoanSchedule(voFinTxnDetMemEntitydebit)
        if (validate!!.returnVOSubHeadcode(spinFundtype, dataspin_fund) == 82) {
            voMtgDetViewModel!!.adjustCompclosing(
                returnshgmemdebitedid(),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                debitamt
            )
            voMtgDetViewModel!!.adjustCompclosing(
                returnshgmemcreditedid(),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                amt
            )
        } else if (validate!!.returnVOSubHeadcode(spinFundtype, dataspin_fund) == 3) {
            voMtgDetViewModel!!.adjustvolclosing(
                returnshgmemdebitedid(),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                debitamt
            )
            voMtgDetViewModel!!.adjustvolclosing(
                returnshgmemcreditedid(),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                amt
            )
        }
        validate!!.CustomAlertVO(
            LabelSet.getText(
                "data_saved_successfully",
                R.string.data_saved_successfully
            ),
            this, SHGVoSHGTransactionSummary::class.java
        )
    }

    //setlabelText
    fun setLabelText() {
        btn_save.text = LabelSet.getText(
            "save",
            R.string.save
        )
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )

        tv_fund_type.text = LabelSet.getText(
            "fund_type",
            R.string.fund_type
        )
        tv_referance_meeting_no.text = LabelSet.getText(
            "referance_meeting_no",
            R.string.referance_meeting_no
        )
        tv_referance_meeting_date.text = LabelSet.getText(
            "referance_meeting_date",
            R.string.referance_meeting_date
        )
        tv_date_of_adjustment.text = LabelSet.getText(
            "date_of_adjustment",
            R.string.date_of_adjustment
        )
        tv_amount_of_adjustment.text = LabelSet.getText(
            "amount_of_adjustment",
            R.string.amount_of_adjustment
        )
        tv_name_of_clf_shg_other.text = LabelSet.getText(
            "name_of_clf_shg_other",
            R.string.name_of_clf_shg_other
        )
        tv_name_of_bank_amount_debited.text = LabelSet.getText(
            "name_of_bank_amount_debited",
            R.string.name_of_bank_amount_debited
        )
        tv_account_no_amount_debited.text = LabelSet.getText(
            "account_no_amount_debited",
            R.string.account_no_amount_debited
        )
        tv_name_of_chg_shg_others_amount_credited.text = LabelSet.getText(
            "name_of_chg_shg_others_amount_credited",
            R.string.name_of_chg_shg_others_amount_credited
        )
        tv_name_of_bank_amount_credited.text = LabelSet.getText(
            "name_of_bank_amount_credited",
            R.string.name_of_bank_amount_credited
        )
        tv_account_no_amount_credited.text = LabelSet.getText(
            "account_no_amount_credited",
            R.string.account_no_amount_credited
        )
        tv_reason_for_adjustment.text = LabelSet.getText(
            "reason_for_adjustment",
            R.string.reason_for_adjustment
        )

        tv_cheque_no_transactio_no.text =
            LabelSet.getText("cheque_no_transactio_no", R.string.cheque_no_transactio_no)
        tv_creditcheque_no_transactio_no.text =
            LabelSet.getText("cheque_no_transactio_no", R.string.cheque_no_transactio_no)

        et_referance_meeting_no.hint = LabelSet.getText("type_here", R.string.type_here)
        et_referance_meeting_date.hint = LabelSet.getText("date_format", R.string.date_format)
        et_date_of_adjustment.hint = LabelSet.getText("date_format", R.string.date_format)
        et_amount_of_adjustment.hint = LabelSet.getText("type_here", R.string.type_here)
        et_cheque_no_transactio_no.hint = LabelSet.getText("type_here", R.string.type_here)
        et_creditcheque_no_transactio_no.hint = LabelSet.getText("type_here", R.string.type_here)
    }

    private fun fillspinner() {

        dataspin_fund = mstVOCOAViewmodel.getCoaSubHeadlist(
            listOf(2, 3, 82), validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
        )

        dataspin_reason = lookupViewmodel.getlookup(
            96, validate!!.RetriveSharepreferenceString(
                AppSP.Langaugecode
            )
        )

        dataspin_modeofpayment = lookupViewmodel.getlookup(
            61,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        validate!!.fillspinner(this, spin_ReasonAdjust, dataspin_reason)
        validate!!.fillVOCoaSubHeadspinner(this, spinFundtype, dataspin_fund)
        validate!!.fillspinner(this, spin_ModePayment, dataspin_modeofpayment)
        validate!!.fillspinner(this, spin_ModePayment1, dataspin_modeofpayment)

    }


    private fun checkValidation(): Int {
        var value = 1
        if (spinFundtype.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spinFundtype,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "fund_type",
                    R.string.fund_type
                )
            )
            value = 0
        } else if (validate!!.returnIntegerValue(et_referance_meeting_no.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "referance_meeting_no",
                    R.string.referance_meeting_no
                ),
                this, et_referance_meeting_no
            )
            value = 0
        } else if (validate!!.returnStringValue(et_referance_meeting_date.text.toString()).length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "referance_meeting_date",
                    R.string.referance_meeting_date
                ),
                this, et_referance_meeting_date
            )
            value = 0
        } else if (validate!!.returnStringValue(et_date_of_adjustment.text.toString()).length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "date_of_adjustment",
                    R.string.date_of_adjustment
                ),
                this, et_date_of_adjustment
            )
            value = 0
        } else if (validate!!.returnIntegerValue(et_amount_of_adjustment.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "amount_of_adjustment",
                    R.string.amount_of_adjustment
                ),
                this, et_amount_of_adjustment
            )
            value = 0
        } else if (spin_CLF.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_CLF,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "name_of_clf_shg_other",
                    R.string.name_of_clf_shg_other
                )
            )
            value = 0
        } else if (spin_BankName.selectedItemPosition == 0 && layDebitBanName.visibility == View.VISIBLE) {
            validate!!.CustomAlertSpinner(
                this, spin_BankName,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "name_of_bank_amount_debited",
                    R.string.name_of_bank_amount_debited
                )
            )
            value = 0
        } else if (spin_DebitAccountNO.selectedItemPosition == 0 && lay_account_no_amount_debited.visibility == View.VISIBLE) {
            validate!!.CustomAlertSpinner(
                this, spin_DebitAccountNO,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "account_no_amount_debited",
                    R.string.account_no_amount_debited
                )
            )
            value = 0
        } else if (spin_CLFShg.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_CLFShg,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "name_of_chg_shg_others_amount_credited",
                    R.string.name_of_chg_shg_others_amount_credited
                )
            )
            value = 0
        } else if (spin_BankCredit.selectedItemPosition == 0 && lay_name_of_bank_amount_credited.visibility == View.VISIBLE) {
            validate!!.CustomAlertSpinner(
                this, spin_BankCredit,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "name_of_bank_amount_credited",
                    R.string.name_of_bank_amount_credited
                )
            )
            value = 0
        } else if (spin_AccountCredit.selectedItemPosition == 0 && lay_account_no_amount_credited.visibility == View.VISIBLE) {
            validate!!.CustomAlertSpinner(
                this, spin_AccountCredit,
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "account_no_amount_credited",
                    R.string.account_no_amount_credited
                )
            )
            value = 0
        } else if (validate!!.returnlookupcode(
                spin_ModePayment,
                dataspin_modeofpayment
            ) == 1 && validate!!.returnIntegerValue(et_amount_of_adjustment.text.toString()) > validate!!.RetriveSharepreferenceInt(
                VoSpData.voCashinhand
            )
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "inthecashbox",
                    R.string.inthecashbox
                ), this,
                et_amount_of_adjustment
            )
            value = 0
            return value
        }
        var bankamt = voFinTxnViewModel!!.gettotalclosinginbank(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            returaccountDebited()
        )
        if ((validate!!.returnlookupcode(
                spin_ModePayment,
                dataspin_modeofpayment
            ) == 2 || validate!!.returnlookupcode(
                spin_ModePayment,
                dataspin_modeofpayment
            ) == 3) && validate!!.returnIntegerValue(et_amount_of_adjustment.text.toString()) > bankamt
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "intheselectedbank",
                    R.string.intheselectedbank
                ), this,
                et_amount_of_adjustment
            )
            value = 0
            return value
        }
        var compamt = voMtgDetViewModel!!.getCompclosing(
            returnshgmemdebitedid(),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
        )
       var Volamt = voMtgDetViewModel!!.getVolclosing(
            returnshgmemdebitedid(),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
        )
        if (validate!!.returnVOSubHeadcode(spinFundtype, dataspin_fund) == 82 && validate!!.returnIntegerValue(et_amount_of_adjustment.text.toString()) > compamt
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "intheselectedmem",
                    R.string.intheselectedmem
                ), this,
                et_amount_of_adjustment
            )
            value = 0
            return value
        }
        if (validate!!.returnVOSubHeadcode(spinFundtype, dataspin_fund) == 3 && validate!!.returnIntegerValue(et_amount_of_adjustment.text.toString()) > Volamt
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "intheselectedmem",
                    R.string.intheselectedmem
                ), this,
                et_amount_of_adjustment
            )
            value = 0
            return value
        }
        return value
    }

    //2 field missing
    //reason for adjustment
    //fundtype spinner


    private fun fillshgmemberspinnerDebited() {

        shgmemberlistdebited = voGenerateMeetingViewmodel.getListDatawithoutmember(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid), 0
        )
        val adapter: ArrayAdapter<String?>
        if (!shgmemberlistdebited.isNullOrEmpty()) {
            val isize = shgmemberlistdebited!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in shgmemberlistdebited!!.indices) {
                sValue[i + 1] = shgmemberlistdebited!![i].childCboName
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_CLF.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(shgmemberlistdebited!!.size + 1)
            sValue[0] = LabelSet.getText(
                "nomemberavailable",
                R.string.nomemberavailable
            )
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_CLF.adapter = adapter
        }
    }

    fun returnshgmemdebitedid(): Long {

        var pos = spin_CLF.selectedItemPosition
        var id = 0L

        if (!shgmemberlistdebited.isNullOrEmpty()) {
            if (pos > 0) id = shgmemberlistdebited!!.get(pos - 1).memId
        }
        return id
    }

    private fun fillshgmemberspinnerCredited(memid: Long) {
        shgmemberlistcredited = voGenerateMeetingViewmodel.getListDatawithoutmember(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid), memid
        )
        val adapter: ArrayAdapter<String?>
        if (!shgmemberlistcredited.isNullOrEmpty()) {
            val isize = shgmemberlistcredited!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in shgmemberlistcredited!!.indices) {
                sValue[i + 1] = shgmemberlistcredited!![i].childCboName
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_CLFShg.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(shgmemberlistcredited!!.size + 1)
            sValue[0] = LabelSet.getText(
                "nomemberavailable",
                R.string.nomemberavailable
            )
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_CLFShg.adapter = adapter
        }
    }

    fun returnshgmemcreditedid(): Long {

        var pos = spin_CLFShg.selectedItemPosition
        var id = 0L

        if (!shgmemberlistcredited.isNullOrEmpty()) {
            if (pos > 0) id = shgmemberlistcredited!!.get(pos - 1).memId
        }
        return id
    }

    fun fillbankDebited() {
        bankListDebited = voGenerateMeetingViewmodel.getBankdataWithAccountNo(
            validate!!.RetriveSharepreferenceString(
                VoSpData.voSHGGUID
            ), ""
        )

        val adapter: ArrayAdapter<String?>
        if (!bankListDebited.isNullOrEmpty()) {
            val isize = bankListDebited!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in bankListDebited!!.indices) {
                var lastthree =
                    bankListDebited!![i].account_no.substring(bankListDebited!![i].account_no.length - 3)
                sValue[i + 1] =
                    bankListDebited!![i].ifsc_code.toString().dropLast(7) + "XXXXX" + lastthree
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_DebitAccountNO.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_DebitAccountNO.adapter = adapter
        }

    }

    fun returaccountDebited(): String {

        var pos = spin_DebitAccountNO.selectedItemPosition
        var id = ""

        if (!bankListDebited.isNullOrEmpty()) {
            if (pos > 0) id =
                bankListDebited!!.get(pos - 1).ifsc_code!!.dropLast(7) + bankListDebited!!.get(pos - 1).account_no
        }
        return id
    }

    fun returnbankguidDebited(): String {

        var pos = spin_DebitAccountNO.selectedItemPosition
        var id = ""

        if (!bankListDebited.isNullOrEmpty()) {
            if (pos > 0)
                id = bankListDebited!!.get(pos - 1).bank_guid
        }
        return id
    }

    fun setaccountDebited(accountno: String): Int {

        var pos = 0

        if (!bankListDebited.isNullOrEmpty()) {
            for (i in bankListDebited!!.indices) {
                if (accountno.equals(
                        bankListDebited!!.get(i).ifsc_code!!.dropLast(7) + bankListDebited!!.get(
                            i
                        ).account_no
                    )
                )
                    pos = i + 1
            }
        }
        spin_DebitAccountNO.setSelection(pos)
        return pos
    }

    fun fillbankCredited(bank_guid: String) {
        bankListCredited = voGenerateMeetingViewmodel.getBankdataWithAccountNo(
            validate!!.RetriveSharepreferenceString(
                VoSpData.voSHGGUID
            ), bank_guid
        )

        val adapter: ArrayAdapter<String?>
        if (!bankListCredited.isNullOrEmpty()) {
            val isize = bankListCredited!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in bankListCredited!!.indices) {
                var lastthree =
                    bankListCredited!![i].account_no.substring(bankListCredited!![i].account_no.length - 3)
                sValue[i + 1] =
                    bankListCredited!![i].ifsc_code.toString().dropLast(7) + "XXXXX" + lastthree
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_AccountCredit.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_AccountCredit.adapter = adapter
        }

    }

    fun returaccountCredited(): String {

        var pos = spin_AccountCredit.selectedItemPosition
        var id = ""

        if (!bankListCredited.isNullOrEmpty()) {
            if (pos > 0) id =
                bankListCredited!!.get(pos - 1).ifsc_code!!.dropLast(7) + bankListCredited!!.get(pos - 1).account_no
        }
        return id
    }

    fun setaccountCredited(accountno: String): Int {

        var pos = 0

        if (!bankListCredited.isNullOrEmpty()) {
            for (i in bankListCredited!!.indices) {
                if (accountno.equals(
                        bankListCredited!!.get(i).ifsc_code!!.dropLast(7) + bankListCredited!!.get(
                            i
                        ).account_no
                    )
                )
                    pos = i + 1
            }
        }
        spin_AccountCredit.setSelection(pos)
        return pos
    }

    override fun onBackPressed() {
        val intent = Intent(this, SHGVoSHGTransactionSummary::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

}
