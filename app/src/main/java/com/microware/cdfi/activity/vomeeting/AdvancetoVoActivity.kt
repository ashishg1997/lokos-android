package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.LookupViewmodel
import kotlinx.android.synthetic.main.activity_advance_to_vo.*
import kotlinx.android.synthetic.main.buttons_vo.*
import kotlinx.android.synthetic.main.repay_toolbar_vomeeting.*

class AdvancetoVoActivity : AppCompatActivity() {

    var lookupViewmodel: LookupViewmodel? = null
    var validate: Validate? = null
    var dataspin_source_of_advance: List<LookupEntity>? = null
    var dataspin_advance_type: List<LookupEntity>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_advance_to_vo)

        validate = Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        setLabelText()

        btn_save.setOnClickListener {
            var intent = Intent(this, ReceiptEntryScreen1Activity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        btn_cancel.setOnClickListener {
            var intent = Intent(this, VoMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        ic_Back.setOnClickListener {
            var intent = Intent(this, VoMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        fillSpinner()
    }

    override fun onBackPressed() {
        var intent = Intent(this, VoMeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0,0)
        finish()
    }

    fun setLabelText()
    {
        tv_source_of_advance.text = LabelSet.getText(
            "source_of_advance",
            R.string.source_of_advance
        )
        tv_name.text = LabelSet.getText("name", R.string.name)
        et_name.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_advance_type.text = LabelSet.getText(
            "advance_type",
            R.string.advance_type
        )
        tv_amount_received.text = LabelSet.getText(
            "amount_received",
            R.string.amount_received
        )
        et_amount_received.setText(LabelSet.getText(
            "type_here",
            R.string.type_here
        ))
        tv_date_of_advance_received.text = LabelSet.getText(
            "date_of_advance_received",
            R.string.date_of_advance_received
        )
        et_date_of_advance_received.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )
        tv_last_date_of_advance_settlement.text = LabelSet.getText(
            "last_date_of_advance_settlement",
            R.string.last_date_of_advance_settlement
        )
        et_last_date_of_advance_settlement.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )
        tv_narration.text = LabelSet.getText(
            "narration",
            R.string.narration
        )
        et_narration.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
        tv_title.text = LabelSet.getText(
            "advance_to_vo",
            R.string.advance_to_vo
        )

    }

    private fun fillSpinner() {
        dataspin_source_of_advance = lookupViewmodel!!.getlookup(
            99,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_advance_type = lookupViewmodel!!.getlookup(
            99,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        validate!!.fillspinner(this, spin_source_of_advance, dataspin_source_of_advance)
        validate!!.fillspinner(this, spin_advance_type, dataspin_advance_type)
    }
}