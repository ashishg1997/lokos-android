package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.VoucherActivity
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.fragment.OthersScreenTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.CboBankViewmodel
import com.microware.cdfi.viewModel.voviewmodel.*
import kotlinx.android.synthetic.main.bank_summary.*

class BankSummary : AppCompatActivity() {

    var validate: Validate? = null
    lateinit var cboBankViewmodel: CboBankViewmodel
    lateinit var voFinTxnDetMemViewModel: VoFinTxnDetMemViewModel
    lateinit var voFinTxnDetGrpViewModel: VoFinTxnDetGrpViewModel
    lateinit var voFinTxnViewModel: VoFinTxnViewModel
    lateinit var voMemLoanTxnViewModel: VoMemLoanTxnViewModel
    lateinit var voMemLoanViewModel: VoMemLoanViewModel
    lateinit var voGroupLoanTxnViewModel: VoGroupLoanTxnViewModel
    lateinit var voGroupLoanViewModel: VoGroupLoanViewModel
    var voBankList: List<Cbo_bankEntity>? = null
    var bankCount = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.bank_summary)

        validate = Validate(this)
        cboBankViewmodel =
            ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        voFinTxnDetMemViewModel =
            ViewModelProviders.of(this).get(VoFinTxnDetMemViewModel::class.java)
        voFinTxnDetGrpViewModel =
            ViewModelProviders.of(this).get(VoFinTxnDetGrpViewModel::class.java)
        voFinTxnViewModel =
            ViewModelProviders.of(this).get(VoFinTxnViewModel::class.java)
        voMemLoanTxnViewModel =
            ViewModelProviders.of(this).get(VoMemLoanTxnViewModel::class.java)
        voMemLoanViewModel =
            ViewModelProviders.of(this).get(VoMemLoanViewModel::class.java)
        voGroupLoanTxnViewModel =
            ViewModelProviders.of(this).get(VoGroupLoanTxnViewModel::class.java)
        voGroupLoanViewModel =
            ViewModelProviders.of(this).get(VoGroupLoanViewModel::class.java)

        replaceFragmenty(
            fragment = OthersScreenTopBarFragment(21),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        if(validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)<validate!!.RetriveSharepreferenceInt(VoSpData.vomaxmeetingnumber)) {
            btn_update.isEnabled = false
            btn_Transfer.isEnabled = false
        }else {
            btn_update.isEnabled = true
            btn_Transfer.isEnabled = true
        }

        spin_BankName?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                val bankCode = returnAccount()
                getOpeningBalance(bankCode)
                getTotalBalance(bankCode)
                getTotalDebitBalance(bankCode)
                getTotalCreditBalance(bankCode)
                getAmountNotDebit(bankCode)
                getAmountNotCredit(bankCode)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }

        btn_Transfer.setOnClickListener {
            if (bankCount > 1) {
                var intent = Intent(this, VOBankTransferListActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }else {
                validate!!.CustomAlertVO(LabelSet.getText("invalid_bank_count",R.string.invalid_bank_count),this)
            }
        }

        btn_update.setOnClickListener {
            if (bankCount > 1) {
                /*var intent = Intent(this, VOVoucherActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)*/
            }else {
                validate!!.CustomAlertVO(LabelSet.getText("invalid_bank_count",R.string.invalid_bank_count),this)
            }
        }

        setLabel()
        fillBankSpinner()
    }

    override fun onBackPressed() {
        val intent = Intent(this, VoMeetingMenuActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

    fun setLabel() {
        tv_BankName.text = LabelSet.getText("bank_name", R.string.bank_name)
        tv_opening_balance.text = LabelSet.getText("opening_balance", R.string.opening_balance)
        tv_total_debit.text = LabelSet.getText(
            "total_debit",
            R.string.total_debit
        )
        tv_total_credit.text = LabelSet.getText("total_credit", R.string.total_credit)
        tv_total_balance.text = LabelSet.getText("total_balance", R.string.total_balance)
        tv_amtnotCredit.text = LabelSet.getText(
            "cheque_received_but_amount_not_credited",
            R.string.cheque_received_but_amount_not_credited
        )
        tv_AmtDebited.text = LabelSet.getText(
            "cheque_issued_but_amount_not_debited",
            R.string.cheque_issued_but_amount_not_debited
        )
        btn_update.text = LabelSet.getText("passbook_update", R.string.passbook_update)
        btn_Transfer.text = LabelSet.getText(
            "inter_bank_account_transfer",
            R.string.inter_bank_account_transfer
        )
    }

    fun fillBankSpinner() {
        voBankList = cboBankViewmodel.getBankdata1(validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID))
        bankCount = voBankList!!.size
        val adapter: ArrayAdapter<String?>
        if (!voBankList.isNullOrEmpty()) {
            val isize = voBankList!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in voBankList!!.indices) {
                var lastfour =
                    voBankList!![i].account_no.substring(voBankList!![i].account_no.length - 4)
                sValue[i + 1] =
                    voBankList!![i].ifsc_code.toString().dropLast(7) + "XXXXX" + lastfour
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_BankName.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_BankName.adapter = adapter
        }

    }

    fun returnAccount(): String {

        var pos = spin_BankName.selectedItemPosition
        var id = ""

        if (!voBankList.isNullOrEmpty()) {
            if (pos > 0) id =
                voBankList!!.get(pos - 1).ifsc_code!!.dropLast(7) + voBankList!!.get(pos - 1).account_no
        }
        return id
    }

    fun setAccount(accountno: String): Int {
        var pos = 0
        if (!voBankList.isNullOrEmpty()) {
            for (i in voBankList!!.indices) {
                if (accountno.equals(
                        voBankList!!.get(i).ifsc_code!!.dropLast(7) + voBankList!!.get(
                            i
                        ).account_no
                    )
                )
                    pos = i + 1
            }
        }
        spin_BankName.setSelection(pos)
        return pos
    }

    fun getOpeningBalance(bankCode: String) {
        var balance = 0
        balance = voFinTxnViewModel.gettotalopeninginbank(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            bankCode
        )

        tv_amount_opebalance.text = "₹ $balance"
    }

    fun getTotalBalance(bankCode: String) {
        var balance = 0
        balance = voFinTxnViewModel.gettotalclosinginbank(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            bankCode
        )
        tv_amount_total_balance.text = "₹ $balance"
    }

    fun getTotalDebitBalance(bankCode: String) {
        var balance = 0
        var totshgcashdebit = voFinTxnDetMemViewModel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(2, 3),
            listOf("PG", "PL", "PO"),
            bankCode
        )
        var totvocashdebit = voFinTxnDetGrpViewModel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(2, 3),
            listOf("PG", "PL", "PO"),
            bankCode
        )

        val grptotloanpaid = voGroupLoanTxnViewModel.getgrptotloanpaidinbank(

            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            bankCode, listOf<Int>(2, 3)
        )

        val loanDisbursedAmt = voMemLoanViewModel.getMemLoanDisbursedAmount(

            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            bankCode, listOf<Int>(2, 3)
        )
        balance = totshgcashdebit + totvocashdebit + grptotloanpaid + loanDisbursedAmt
        tv_amount_total_debit.text = "₹ $balance"
    }

    fun getTotalCreditBalance(bankCode: String) {
        var balance = 0
        var totshgcash = voFinTxnDetMemViewModel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(2, 3),
            listOf("RG", "RL", "RO"),
            bankCode
        )

        var totvocash = voFinTxnDetGrpViewModel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(2, 3),
            listOf("RG", "RL", "RO"),
            bankCode
        )
        val memtotloanpaid = voMemLoanTxnViewModel.getmemtotloanpaidinbank(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            bankCode,listOf<Int>(2,3)
        )

        val loanDisbursedAmt = voGroupLoanViewModel.getgrouptotloanamtinbank(

            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            bankCode, listOf<Int>(2, 3)
        )
        balance = totshgcash + totvocash + memtotloanpaid + loanDisbursedAmt
        tv_amount_total_credit.text = "₹ $balance"
    }

    fun getAmountNotDebit(bankCode: String) {
        var balance = 0

        tv_amount_Debited.text = "₹ $balance"
    }

    fun getAmountNotCredit(bankCode: String) {
        var balance = 0

        total_amount_amtnotCredit.text = "₹ $balance"
    }

}