package com.microware.cdfi.activity.vomeeting

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.CLFtoVOReceiptAdapter
import com.microware.cdfi.adapter.vomeetingadapter.CLFtoVOReceiptBankAdapter
import com.microware.cdfi.adapter.vomeetingadapter.GrpTxnListAdapter
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.entity.MstVOCOAEntity
import com.microware.cdfi.entity.voentity.VoFinTxnDetGrpEntity
import com.microware.cdfi.fragment.VoReceiptsTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import com.microware.cdfi.viewModel.voviewmodel.VoFinTxnDetGrpViewModel
import com.microware.cdfi.viewModel.voviewmodel.VoFinTxnViewModel
import com.microware.cdfi.viewModel.voviewmodel.VoGenerateMeetingViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoGroupLoanViewModel
import kotlinx.android.synthetic.main.activity_crlfto_voreceipts.*
import kotlinx.android.synthetic.main.amountdialog.view.*
import kotlinx.android.synthetic.main.buttons_vo.btn_cancel
import kotlinx.android.synthetic.main.buttons_vo.btn_save
import kotlinx.android.synthetic.main.buttons_vo.view.*
import kotlinx.android.synthetic.main.receive_dialog.view.*

class CLFtoVOReceipts : AppCompatActivity() {
    var validate: Validate? = null
    var mstVOCOAViewmodel: MstVOCOAViewmodel? = null
    var cboBankViewModel: CboBankViewmodel? = null
    var bankMasterViewModel: MasterBankViewmodel? = null
    var voFinTxnDetGrpViewModel: VoFinTxnDetGrpViewModel? = null
    var lookupViewmodel: LookupViewmodel? = null
    var voGroupLoanViewModel: VoGroupLoanViewModel? = null
    var dataspin_mode_of_payment: List<LookupEntity>? = null
    var voFinTxnDetGrpEntity: VoFinTxnDetGrpEntity? = null
    var generateMeetingViewmodel: VoGenerateMeetingViewmodel? = null
    var cboType = 0
    var voFinTxnViewModel: VoFinTxnViewModel? = null
    var vocoaMappingViewModel: VoCoaMappingViewmodel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_crlfto_voreceipts)
        validate = Validate(this)
        if (validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType) == 2) {
            cboType = 2
        } else {
            cboType = 1
        }

        checkFragment()
        setLabelText()
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        mstVOCOAViewmodel = ViewModelProviders.of(this).get(MstVOCOAViewmodel::class.java)
        cboBankViewModel = ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        bankMasterViewModel = ViewModelProviders.of(this).get(MasterBankViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        voGroupLoanViewModel = ViewModelProviders.of(this).get(VoGroupLoanViewModel::class.java)
        voFinTxnDetGrpViewModel =
            ViewModelProviders.of(this).get(VoFinTxnDetGrpViewModel::class.java)
        voFinTxnViewModel = ViewModelProviders.of(this).get(VoFinTxnViewModel::class.java)
        vocoaMappingViewModel = ViewModelProviders.of(this).get(VoCoaMappingViewmodel::class.java)

        img_next.setOnClickListener {
            checkActivityFillRecycler1()
            lay_bank.visibility = View.VISIBLE
            tbl_back.visibility = View.VISIBLE
            tbl_next.visibility = View.GONE
            lay_fund.visibility = View.GONE
        }

        img_back.setOnClickListener {
            checkActivityFillRecycler()
            lay_bank.visibility = View.GONE
            tbl_back.visibility = View.GONE
            tbl_next.visibility = View.VISIBLE
            lay_fund.visibility = View.VISIBLE
        }

        btn_save.setOnClickListener {

        }

        btn_cancel.setOnClickListener {
            var intent = Intent(this, VoMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_bank.visibility = View.GONE
        tbl_back.visibility = View.GONE
        lay_fund.visibility = View.VISIBLE
        checkActivityFillRecycler()
        checkActivityFillRecycler1()

    }

    private fun setLabelText() {
        tv_default_bank.text = LabelSet.getText("default_bank", R.string.default_bank)
        tv_funds.text = LabelSet.getText("funds", R.string.funds)
        tv_amount.text = LabelSet.getText("amount", R.string.amount)
        tv_actual_payment_location.text = LabelSet.getText(
            "actual_payment_nlocation",
            R.string.actual_payment_nlocation
        )
        tv_total.text = LabelSet.getText("total", R.string.total)
        tv_total_amount_to_be_received.text = LabelSet.getText(
            "total_amount_to_be_received",
            R.string.total_amount_to_be_received
        )
        tv_total_amount_received_by_vo.text = LabelSet.getText(
            "total_amount_received_by_vo",
            R.string.total_amount_received_by_vo
        )
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText("cancel", R.string.cancel)
    }

    private fun fillRecycler(receipt: Int, type: List<String>) {
        var coaData = mstVOCOAViewmodel!!.getCoaSubHeadData(
            receipt,
            type,
            validate!!.RetriveSharepreferenceString(
                AppSP.Langaugecode
            )!!
        )
        if (!coaData.isNullOrEmpty()) {
            getTotalValue(coaData)
            rvList.layoutManager = LinearLayoutManager(this)
            rvList.adapter =
                CLFtoVOReceiptAdapter(this, coaData, cboBankViewModel, bankMasterViewModel)
        }
    }

    private fun fillRecycler1() {
        val bankList = cboBankViewModel!!.getcboBankdataModel(
            validate!!.RetriveSharepreferenceString(
                VoSpData.voSHGGUID
            ), cboType
        )
        if (!bankList.isNullOrEmpty()) {
            rvBankList.layoutManager = LinearLayoutManager(this)
            rvBankList.adapter =
                CLFtoVOReceiptBankAdapter(this, bankList, voFinTxnDetGrpViewModel)
        }

    }

    fun returndefaaultaccount(): String {
        var accountNo = ""
        val bankList = cboBankViewModel!!.getcboBankdataModel(
            validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID),
            cboType
        )
        if (!bankList.isNullOrEmpty()) {
            for (i in bankList.indices) {
                if (bankList.get(i).is_default == 1) {
                    accountNo =
                        bankList.get(i).ifsc_code!!.dropLast(7) + bankList.get(i).account_no!!
                    break
                }
            }
        }
        return accountNo
    }

    fun getBankName(bankID: Int?): String? {
        var value: String? = null
        value = bankMasterViewModel!!.getBankName(bankID!!)
        return value
    }

    fun updatefindetailmemcash() {
        var amt = voFinTxnDetGrpViewModel!!.getpendingIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            1,
            listOf("PG", "PL", "PO"),
            "",
            validate!!.RetriveSharepreferenceInt(VoSpData.voOrgType)
        )
        if ((validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 6 || validate!!.RetriveSharepreferenceInt(
                VoSpData.voFormType
            ) == 7) && amt> validate!!.RetriveSharepreferenceInt(
                VoSpData.voCashinhand
            )
        ) {
            validate!!.CustomAlertVO(
                LabelSet.getText(
                    "inthecashbox",
                    R.string.inthecashbox
                ), this
            )
        } else {
            voFinTxnDetGrpViewModel!!.updatefindetailgrp(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                "",
                1,
                "",
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime)
            )
            checkFragment()
            validate!!.CustomAlertVO(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ), this
            )
        }
    }

    fun customReceiveDialog(accountNo: String) {
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.receive_dialog, null)
        val mBuilder = AlertDialog.Builder(this).setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.WHITE))
        fillSpinner(mDialogView)
        var amt = 0
        when {
            validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 2 -> {
                amt = voFinTxnDetGrpViewModel!!.getpendingIncomeAmount(
                    validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                    validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                    2,
                    listOf("RG", "RL", "RO"),
                    accountNo, validate!!.RetriveSharepreferenceInt(VoSpData.voOrgType)
                )
            }
            validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 3 -> {
                amt = voFinTxnDetGrpViewModel!!.getpendingIncomeAmount(
                    validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                    validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                    2,
                    listOf("RG", "RL", "RO"),
                    accountNo,
                    validate!!.RetriveSharepreferenceInt(VoSpData.voOrgType)
                )
            }
            validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 4 -> {
                amt = voFinTxnDetGrpViewModel!!.getpendingIncomeAmount(
                    validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                    validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                    2,
                    listOf("RG", "RL", "RO"),
                    accountNo,
                    validate!!.RetriveSharepreferenceInt(VoSpData.voOrgType)
                )
            }
            validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 6 -> {
                amt = voFinTxnDetGrpViewModel!!.getpendingIncomeAmount(
                    validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                    validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                    2,
                    listOf("PG", "PL", "PO"),
                    accountNo,
                    validate!!.RetriveSharepreferenceInt(VoSpData.voOrgType)
                )
            }
            validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 7 -> {
                amt = voFinTxnDetGrpViewModel!!.getpendingIncomeAmount(
                    validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                    validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                    2,
                    listOf("PG", "PL", "PO"),
                    accountNo,
                    validate!!.RetriveSharepreferenceInt(VoSpData.voOrgType)
                )
            }
        }
        var bankamt = voFinTxnViewModel!!.gettotalclosinginbank(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            accountNo
        )
        mDialogView.tvTotalAmt.text = amt.toString()

        mDialogView.et_cheque_no.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                s.toString()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = s.toString()
                filltxnlist(mDialogView.rvTxnList, accountNo, value)
            }

        })
        mDialogView.btn_save.setOnClickListener {
            if (mDialogView.spincheque.selectedItemPosition == 0) {
                validate!!.CustomAlertSpinner(
                    this, mDialogView.spincheque,
                    LabelSet.getText(
                        "please_select",
                        R.string.please_select
                    ) + " " + LabelSet.getText(
                        "cheque_payslip_rtgs_neft_imps_upi",
                        R.string.cheque_payslip_rtgs_neft_imps_upi
                    )

                )
            } else if (validate!!.returnStringValue(
                    mDialogView.et_cheque_no.text.toString()
                ).length == 0
            ) {
                validate!!.CustomAlertEditText(
                    LabelSet.getText(
                        "please_enter",
                        R.string.please_enter
                    ) + LabelSet.getText(
                        "cheque_no_transactio_no",
                        R.string.cheque_no_transactio_no
                    ), this,
                    mDialogView.et_cheque_no
                )
            } else if (validate!!.returnStringValue(
                    mDialogView.tvTotalAmt.text.toString()
                ).length == 0
            ) {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "please_enter",
                        R.string.please_enter
                    ) + LabelSet.getText(
                        "amount_to_be_received",
                        R.string.amount_to_be_received
                    ), this
                )
            } else if ((validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 6 || validate!!.RetriveSharepreferenceInt(
                    VoSpData.voFormType
                ) == 7) && validate!!.returnIntegerValue(
                    mDialogView.tvTotalAmt.text.toString()
                ) > bankamt
            ) {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "intheselectedbank",
                        R.string.intheselectedbank
                    ), this
                )
            } else {
                voFinTxnDetGrpViewModel!!.updatefindetailgrp(
                    validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                    validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                    accountNo,
                    validate!!.returnlookupcode(mDialogView.spincheque, dataspin_mode_of_payment),
                    validate!!.returnStringValue(mDialogView.et_cheque_no.text.toString()),
                    validate!!.RetriveSharepreferenceString(AppSP.userid),
                    validate!!.Daybetweentime(validate!!.currentdatetime)
                )
                checkFragment()
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "data_saved_successfully",
                        R.string.data_saved_successfully
                    ), this
                )
                mAlertDialog.dismiss()
            }

        }

        mDialogView.btn_cancel.setOnClickListener {
            mAlertDialog.dismiss()
        }

    }

    private fun filltxnlist(rvTxnList: RecyclerView, bankcode: String, txnno: String) {
        val txnlist = voFinTxnDetGrpViewModel!!.gettxnkist(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber), bankcode, txnno
        )
        if (!txnlist.isNullOrEmpty()) {
            rvTxnList.layoutManager = LinearLayoutManager(this)
            rvTxnList.adapter =
                GrpTxnListAdapter(this, txnlist)
            rvTxnList.visibility = View.VISIBLE
        } else {
            rvTxnList.visibility = View.GONE
        }

    }

    private fun fillSpinner(mDialogView: View) {

        dataspin_mode_of_payment = lookupViewmodel!!.getlookupnotin(
            65, 1,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        validate!!.fillspinner(this, mDialogView.spincheque, dataspin_mode_of_payment)
    }

    fun CustomAlertAmountDialog(
        auid: Int,
        type: String,
        bankcode: String
    ) {
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.amountdialog, null)
        val mBuilder = androidx.appcompat.app.AlertDialog.Builder(this).setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.setCancelable(false)
        mAlertDialog.setCanceledOnTouchOutside(false)
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        mDialogView.btn_save.setOnClickListener {
            if (checkValidation(mDialogView) == 1) {
                SaveAmount(mDialogView, auid, type, bankcode)
                mAlertDialog.dismiss()
            }
        }

        mDialogView.btn_cancel.setOnClickListener {
            mAlertDialog.dismiss()
        }

        showDialogData(mDialogView, auid)
    }

    private fun showDialogData(mDialogView: View?, auid: Int) {

        var listdata = voFinTxnDetGrpViewModel!!.getSavingamount(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            auid
        )

        if (!listdata.isNullOrEmpty()) {

            mDialogView!!.et_amount.setText(validate!!.returnStringValue(listdata[0].amount.toString()))
        }
    }

    private fun SaveAmount(
        mDialogView: View?,
        auid: Int,
        type: String,
        bankcode: String
    ) {
        var mode = 0
        if (bankcode.isNotEmpty()) {
            mode = 2
        } else {
            mode = 1
        }
        voFinTxnDetGrpEntity = VoFinTxnDetGrpEntity(
            0,
            0,
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            auid,
            0,
            0,
            0,
            type,
            validate!!.returnIntegerValue(mDialogView!!.et_amount.text.toString()),
            "",
            0,
            mode,
            bankcode,

            "",
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            "",
            0,
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            0,
            1,
            validate!!.RetriveSharepreferenceInt(VoSpData.voOrgType)
        )

        voFinTxnDetGrpViewModel!!.insertVoGroupLoanSchedule(voFinTxnDetGrpEntity)
        validate!!.CustomAlertVO(
            LabelSet.getText(
                "data_saved_successfully",
                R.string.data_saved_successfully
            ), this
        )
        checkFragment()
        checkActivityFillRecycler()
        checkActivityFillRecycler1()
    }

    fun checkValidation(mDialogView: View): Int {
        var value = 1

        if (mDialogView.et_amount.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "amount",
                    R.string.amount
                ),
                this, mDialogView.et_amount
            )
            value = 0
        }


        return value
    }


    override fun onBackPressed() {
        var intent = Intent(this, VoMeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

    fun getTotalValue(list: List<MstVOCOAEntity>) {
        var totalAmount = 0
        for (i in 0 until list.size) {
            val amount = voFinTxnDetGrpViewModel!!.getAmount(
                list[i].uid,
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
            )
            totalAmount = totalAmount + validate!!.returnIntegerValue(amount.toString())
        }
        tvTotalAmt.text = totalAmount.toString()
        tv_Amount.text = totalAmount.toString()
    }


    fun getAmount(uid: Int): List<VoFinTxnDetGrpEntity>? {

        return voFinTxnDetGrpViewModel!!.getSavingamount(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            uid
        )
    }
    fun returnbankcode(uid: Int): String? {
        var bancode:String? = null
        bancode = vocoaMappingViewModel!!.getBankCode(
            uid, validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        return bancode
    }

    fun getname(uid: Int): String {

        return mstVOCOAViewmodel!!.getCoaSubHeadname(
            uid,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
        )
    }

    fun getbankpos(bankcode: String): Int {
        var pos = 99
        val bankList = cboBankViewModel!!.getcboBankdataModel(
            validate!!.RetriveSharepreferenceString(
                VoSpData.voSHGGUID
            ), cboType
        )
        if (!bankList.isNullOrEmpty()) {
            for (i in bankList.indices) {
                if (bankcode.equals(bankList.get(i).ifsc_code!!.dropLast(7) + bankList.get(i).account_no))
                    pos = i
            }
        }
        return pos
    }

    private fun checkActivityFillRecycler() {
        when {
            validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 2 -> {
                fillRecycler(2, listOf("RG", "RL", "RO"))
            }
            validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 3 -> {
                fillRecycler(3, listOf("RG", "RL", "RO"))
            }
            validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 4 -> {
                fillRecycler(4, listOf("RG", "RL", "RO"))
            }
            validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 6 -> {
                fillRecycler(2, listOf("PG", "PL", "PO"))
            }
            validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 7 -> {
                fillRecycler(3, listOf("PG", "PL", "PO"))
            }
        }
    }

    private fun checkActivityFillRecycler1() {
        /*if (validate!!.RetriveSharepreferenceInt(VoSpData.voOrgType) == 2) {
            fillRecycler1()
        } else if (validate!!.RetriveSharepreferenceInt(VoSpData.voOrgType) == 3) {
            fillRecycler1()
        } else if (validate!!.RetriveSharepreferenceInt(VoSpData.voOrgType) == 4) {
            fillRecycler1()
        }*/
        fillRecycler1()
    }

    private fun checkFragment() {
        when {
            validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 2 -> {
                replaceFragmenty(
                    fragment = VoReceiptsTopBarFragment(4),
                    allowStateLoss = true,
                    containerViewId = R.id.mainContent
                )
            }
            validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 3 -> {
                replaceFragmenty(
                    fragment = VoReceiptsTopBarFragment(9),
                    allowStateLoss = true,
                    containerViewId = R.id.mainContent
                )
            }
            validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 4 -> {
                replaceFragmenty(
                    fragment = VoReceiptsTopBarFragment(13),
                    allowStateLoss = true,
                    containerViewId = R.id.mainContent
                )
            }
            validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 6 -> {
                replaceFragmenty(
                    fragment = VoReceiptsTopBarFragment(25),
                    allowStateLoss = true,
                    containerViewId = R.id.mainContent
                )
            }
            validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 7 -> {
                replaceFragmenty(
                    fragment = VoReceiptsTopBarFragment(29),
                    allowStateLoss = true,
                    containerViewId = R.id.mainContent
                )
            }
        }
        /*if (validate!!.RetriveSharepreferenceInt(VoSpData.voOrgType) == 2) {

        } else if (validate!!.RetriveSharepreferenceInt(VoSpData.voOrgType) == 3) {

        } else if (validate!!.RetriveSharepreferenceInt(VoSpData.voOrgType) == 4) {

        }*/
    }

    fun getFundTypeId(receiptPayment: Int, type: String, uid: Int): String {
        return mstVOCOAViewmodel!!.getFundTypeId(receiptPayment, type, uid)
    }

    fun getFundamount(fundType: Int): Int {
        return voGroupLoanViewModel!!.getfundamt(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),

            fundType, validate!!.RetriveSharepreferenceInt(VoSpData.LoanSource)
        )
    }

    fun getFundlaonrepaymentamount(fundType: Int): Int {
        return generateMeetingViewmodel!!.getgrouploanpaidbyauid(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            fundType,
            validate!!.RetriveSharepreferenceInt(VoSpData.LoanSource)
        )
    }

}