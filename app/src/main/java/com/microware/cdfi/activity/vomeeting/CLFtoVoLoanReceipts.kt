package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.api.meetingmodel.FundTypeModel
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.entity.voentity.VoGroupLoanEntity
import com.microware.cdfi.entity.voentity.VoGroupLoanScheduleEntity
import com.microware.cdfi.entity.voentity.VoGroupLoanTxnEntity
import com.microware.cdfi.fragment.VoReceiptsTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.CboBankViewmodel
import com.microware.cdfi.viewModel.LoanProductViewModel
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.MstProductViewmodel
import com.microware.cdfi.viewModel.voviewmodel.*
import kotlinx.android.synthetic.main.activity_clfto_vo_loan_receipts.*
import kotlinx.android.synthetic.main.activity_clfto_vo_loan_receipts.et_cheque_no_transactio_no
import kotlinx.android.synthetic.main.activity_clfto_vo_loan_receipts.et_installment_amount
import kotlinx.android.synthetic.main.activity_clfto_vo_loan_receipts.et_interest_rate
import kotlinx.android.synthetic.main.activity_clfto_vo_loan_receipts.et_loan_no
import kotlinx.android.synthetic.main.activity_clfto_vo_loan_receipts.et_loan_tenure
import kotlinx.android.synthetic.main.activity_clfto_vo_loan_receipts.et_sanction_amount
import kotlinx.android.synthetic.main.activity_clfto_vo_loan_receipts.et_sanction_date
import kotlinx.android.synthetic.main.activity_clfto_vo_loan_receipts.spin_loan_repayment_frequency
import kotlinx.android.synthetic.main.activity_clfto_vo_loan_receipts.tv_cheque_no_transactio_no
import kotlinx.android.synthetic.main.activity_clfto_vo_loan_receipts.tv_fund_type
import kotlinx.android.synthetic.main.activity_clfto_vo_loan_receipts.tv_installment_amount
import kotlinx.android.synthetic.main.activity_clfto_vo_loan_receipts.tv_interest_rate
import kotlinx.android.synthetic.main.activity_clfto_vo_loan_receipts.tv_loan_no
import kotlinx.android.synthetic.main.activity_clfto_vo_loan_receipts.tv_loan_tenure
import kotlinx.android.synthetic.main.activity_clfto_vo_loan_receipts.tv_sanction_amount
import kotlinx.android.synthetic.main.activity_clfto_vo_loan_receipts.tv_sanction_date
import kotlinx.android.synthetic.main.buttons_vo.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*

class CLFtoVoLoanReceipts : AppCompatActivity() {

    var validate: Validate? = null
    var voGroupLoanEntity: VoGroupLoanEntity? = null
    var voGroupLoanTxnEntity: VoGroupLoanTxnEntity? = null
    var voGroupLoanScheduleEntity: VoGroupLoanScheduleEntity? = null
    lateinit var voGroupLoanViewModel: VoGroupLoanViewModel
    lateinit var voGroupLoanTxnViewModel: VoGroupLoanTxnViewModel
    lateinit var voGroupLoanScheduleViewModel: VoGroupLoanScheduleViewModel
    lateinit var cboBankViewmodel: CboBankViewmodel
    lateinit var mstProductViewmodel: MstProductViewmodel
    lateinit var voMemLoanViewModel: VoMemLoanViewModel
    var lookupViewmodel: LookupViewmodel? = null
    var loanProductViewmodel: LoanProductViewModel? = null
    var voBankList: List<Cbo_bankEntity>? = null
    var dataspin_fund_type: List<LookupEntity>? = null
    var dataspin_loan_product: List<LookupEntity>? = null
    var dataspin_loan_repayment_frequency: List<LookupEntity>? = null
    var dataspin_modeofpayment: List<LookupEntity>? = null
    var dataspin_product: List<FundTypeModel>? = null
    var loanpurpose = 0
    var proposedAmount = 0
    var sanctionAmount = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_clfto_vo_loan_receipts)
        validate = Validate(this)
        voGroupLoanViewModel = ViewModelProviders.of(this).get(VoGroupLoanViewModel::class.java)
        voGroupLoanTxnViewModel =
            ViewModelProviders.of(this).get(VoGroupLoanTxnViewModel::class.java)
        voGroupLoanScheduleViewModel =
            ViewModelProviders.of(this).get(VoGroupLoanScheduleViewModel::class.java)
        cboBankViewmodel =
            ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        mstProductViewmodel = ViewModelProviders.of(this).get(MstProductViewmodel::class.java)
        voMemLoanViewModel = ViewModelProviders.of(this).get(VoMemLoanViewModel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        loanProductViewmodel = ViewModelProviders.of(this).get(LoanProductViewModel::class.java)


        var loanno = voGroupLoanViewModel.getmaxLoanno(
            validate!!.RetriveSharepreferenceLong(
                VoSpData.voshgid
            )
        )
        et_loan_no.setText(
            (loanno + 1)
                .toString()
        )
        /* spin_fundType?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

             override fun onItemSelected(
                 parent: AdapterView<*>, view: View?,
                 position: Int, id: Long
             ) {

                 fillfunddata(position)

             }

             override fun onNothingSelected(parent: AdapterView<*>) {

             }
         }*/
        et_loan_tenure.filters = arrayOf(InputFilterMinMax(1, 60))

        et_sanction_date.setOnClickListener {
            validate!!.datePickerwithmindate(
                validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                et_sanction_date
            )
        }

        et_date_of_amount_received.setOnClickListener {
            validate!!.datePickerwithmindate(
                validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                et_date_of_amount_received
            )
        }

        et_loan_tenure.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = validate!!.returnIntegerValue(et_amount_received.text.toString())
                if (validate!!.returnIntegerValue(s.toString()) > 0 && value > 0) {
                    var installmentamt = value / validate!!.returnIntegerValue(s.toString())
                    et_installment_amount.setText(installmentamt.toString())
                }

            }

        })

        et_amount_received.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = validate!!.returnIntegerValue(et_loan_tenure.text.toString())
                if (validate!!.returnIntegerValue(s.toString()) > 0 && value > 0) {
                    var installmentamt = value / validate!!.returnIntegerValue(s.toString())
                    et_installment_amount.setText(installmentamt.toString())
                }
            }
        })

        spin_mode_of_payment?.onItemSelectedListener =
            object :
                AdapterView.OnItemSelectedListener {

                override fun onItemSelected(
                    parent: AdapterView<*>, view: View?,
                    position: Int, id: Long
                ) {

                    if (validate!!.returnlookupcode(
                            spin_mode_of_payment,
                            dataspin_modeofpayment
                        ) == 2
                    ) {
                        lay_source_bank.visibility = View.VISIBLE
                        lay_cheque_no_transactio_no.visibility = View.VISIBLE
                    } else {
                        lay_source_bank.visibility = View.GONE
                        lay_cheque_no_transactio_no.visibility = View.GONE
                        spin_source_bank.setSelection(0)

                    }

                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // TODO Auto-generated method stub

                }
            }

        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                saveData(validate!!.returnIntegerValue(et_moratorium.text.toString()))
                insertscheduler(
                    validate!!.returnIntegerValue(et_amount_received.text.toString()),
                    validate!!.returnIntegerValue(et_loan_tenure.text.toString()),
                    validate!!.returnIntegerValue(et_moratorium.text.toString())
                )
            }
        }

        btn_cancel.setOnClickListener {
            var intent = Intent(this, CLFtoVoLoanReceiptsSummaryActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        setLabel()
        fillSpinner()
        fillBankSpinner()
        fillproduct()
        showData()
        checkFragment()

    }

    private fun checkFragment() {
        when {
            validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 2 -> {
                replaceFragmenty(
                    fragment = VoReceiptsTopBarFragment(5),
                    allowStateLoss = true,
                    containerViewId = R.id.mainContent
                )
            }
            validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 3 -> {
                replaceFragmenty(
                    fragment = VoReceiptsTopBarFragment(10),
                    allowStateLoss = true,
                    containerViewId = R.id.mainContent
                )
            }
            validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 4 -> {
                replaceFragmenty(
                    fragment = VoReceiptsTopBarFragment(14),
                    allowStateLoss = true,
                    containerViewId = R.id.mainContent
                )
            }

        }
    }

    fun fillfunddata() {
        if (!dataspin_product.isNullOrEmpty()) {
            et_interest_rate.setText(validate!!.returnStringValue(dataspin_product!![0].interestDefault.toString()))
            et_loan_tenure.setText(validate!!.returnStringValue(dataspin_product!![0].defaultPeriod.toString()))

        } else {
            et_interest_rate.setText("0")
            et_loan_tenure.setText("0")

        }


    }

    fun setminmaxlimit() {
        if (!dataspin_product.isNullOrEmpty()) {
            var maxamt = validate!!.returnIntegerValue(dataspin_product!![0].maxAmount.toString())
            var maxperiod =
                validate!!.returnIntegerValue(dataspin_product!![0].maxPeriod.toString())
            var maxint = validate!!.returnDoubleValue(dataspin_product!![0].maxInterest.toString())
            /*if(maxint>0.0) {
                et_interest_rate.filters = arrayOf<InputFilter>(DecimalDigitsInputFilter(3, 2))
            }*/
            if (maxamt > 0.0) {
                et_amount_received.filters = arrayOf(InputFilterMinMax(0, maxamt))
            }
            if (maxperiod > 0.0) {
                et_loan_tenure.filters = arrayOf(InputFilterMinMax(0, maxperiod))
            }
        }


    }

    override fun onBackPressed() {
        var intent = Intent(this, CLFtoVoLoanReceiptsSummaryActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

    private fun setLabel() {
        btn_save.text = LabelSet.getText("confirm", R.string.confirm)
        btn_cancel.text = LabelSet.getText("cancel", R.string.cancel)
        tv_fund_type.text = LabelSet.getText("fund_type", R.string.fund_type)
        tv_fund_product.text = LabelSet.getText("loan_product", R.string.loan_product)
        tv_loan_no.text = LabelSet.getText("loan_no", R.string.loan_no)
        tv_sanction_amount.text = LabelSet.getText(
            "sanction_amount",
            R.string.sanction_amount
        )
        tv_sanction_date.text = LabelSet.getText("sanction_date", R.string.sanction_date)
        tv_amount_received.text = LabelSet.getText(
            "amount_received",
            R.string.amount_received
        )
        tv_date_amount_received.text = LabelSet.getText(
            "date_of_amount_received",
            R.string.date_of_amount_received
        )
        tv_mode_of_payment.text = LabelSet.getText("mode_of_payment", R.string.mode_of_payment)
        tv_source_bank.text = LabelSet.getText("source_bank", R.string.source_bank)
        tv_cheque_no_transactio_no.text = LabelSet.getText(
            "cheque_no_transactio_no",
            R.string.cheque_no_transactio_no
        )
        tv_interest_rate.text = LabelSet.getText("interest_rate", R.string.interest_rate)
        tv_loan_tenure.text = LabelSet.getText("loan_tenure", R.string.loan_tenure)
        tv_loan_repyament_frequency.text = LabelSet.getText(
            "loan_repyament_frequency",
            R.string.loan_repyament_frequency
        )
        tv_installment_amount.text = LabelSet.getText(
            "installment_amount_principal",
            R.string.installment_amount_principal
        )
        tv_moratorium.text = LabelSet.getText(
            "moratorium_in_months",
            R.string.moratorium_in_months
        )
        tv_total.text = LabelSet.getText("total", R.string.total)
        et_loan_no.hint = LabelSet.getText("auto", R.string.auto)
        et_sanction_amount.hint = LabelSet.getText("type_here", R.string.type_here)
        et_sanction_date.hint = LabelSet.getText("date_format", R.string.date_format)
        et_amount_received.hint = LabelSet.getText("enter_amount", R.string.enter_amount)
        et_date_of_amount_received.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )
        et_cheque_no_transactio_no.hint = LabelSet.getText("type_here", R.string.type_here)
        et_interest_rate.hint = LabelSet.getText("type_here", R.string.type_here)
        et_loan_tenure.hint = LabelSet.getText("type_here", R.string.type_here)
        et_installment_amount.hint = LabelSet.getText("type", R.string.type)
        et_moratorium.hint = LabelSet.getText(
            "enter_not_more_than_permissible_limit",
            R.string.enter_not_more_than_permissible_limit
        )

    }

    private fun checkValidation(): Int {

        var value = 1
        if (spin_fundType.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_fundType, LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "fund_type",
                    R.string.fund_type
                )
            )
            value = 0
            return value
        }
        if (spin_loan_product.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_loan_product, LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "loan_product",
                    R.string.loan_product
                )
            )
            value = 0
            return value
        }
        if (validate!!.returnStringValue(et_sanction_date.text.toString()).length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "sanction_date",
                    R.string.sanction_date
                ), this, et_sanction_date
            )
            value = 0
            return value
        }
        if (validate!!.returnIntegerValue(et_amount_received.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "amount_received",
                    R.string.amount_received
                ), this, et_amount_received
            )
            value = 0
            return value
        }
        if (validate!!.returnStringValue(et_date_of_amount_received.text.toString()).length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "date_of_amount_received",
                    R.string.date_of_amount_received
                ), this, et_date_of_amount_received
            )
            value = 0
            return value
        }
        if (spin_mode_of_payment.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_mode_of_payment, LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "mode_of_payment",
                    R.string.mode_of_payment
                )
            )
            value = 0
            return value
        }

        if (spin_source_bank.selectedItemPosition == 0 && lay_source_bank.visibility == View.VISIBLE) {
            validate!!.CustomAlertSpinner(
                this, spin_source_bank, LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "source_bank",
                    R.string.source_bank
                )
            )
            value = 0
            return value
        }
        if (validate!!.returnStringValue(et_cheque_no_transactio_no.text.toString())
                .isEmpty() && lay_cheque_no_transactio_no.visibility == View.VISIBLE
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "cheque_no_transactio_no",
                    R.string.cheque_no_transactio_no
                ), this, et_cheque_no_transactio_no
            )
            value = 0
            return value
        }
        if (validate!!.returnDoubleValue(et_interest_rate.text.toString()) == 0.0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "interest_rate",
                    R.string.interest_rate
                ), this, et_interest_rate
            )
            value = 0
            return value
        }
        if (validate!!.returnIntegerValue(et_loan_tenure.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "loan_tenure",
                    R.string.loan_tenure
                ), this, et_loan_tenure
            )
            value = 0
            return value
        }
        if (spin_loan_repayment_frequency.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_loan_repayment_frequency, LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "loan_repyament_frequency",
                    R.string.loan_repyament_frequency
                )
            )
            value = 0
            return value
        }
        if (validate!!.returnIntegerValue(et_installment_amount.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "installment_amount_principal",
                    R.string.installment_amount_principal
                ), this, et_installment_amount
            )
            value = 0
            return value
        }


        return value
    }

// et_sanction_date no column
// et_sanction_amount issue

    private fun saveData(morotrum: Int) {

        voGroupLoanEntity = VoGroupLoanEntity(
            0,
            0,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid),
            validate!!.returnIntegerValue(et_loan_no.text.toString()),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.Daybetweentime(et_date_of_amount_received.text.toString()), // check this field
            validate!!.addmonth(
                validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                morotrum,
                validate!!.returnlookupcode(
                    spin_loan_repayment_frequency,
                    dataspin_loan_repayment_frequency
                )
            ),
            0,
            validate!!.returnIntegerValue(et_amount_received.text.toString()),
            validate!!.RetriveSharepreferenceInt(VoSpData.LoanSource),
            validate!!.returnFundTypeId(spin_fundType, dataspin_product),
            validate!!.returnDoubleValue(et_interest_rate.text.toString()),
            validate!!.returnIntegerValue(et_loan_tenure.text.toString()),
            0,
            0,
            0,
            0,
            0,
            false,
            validate!!.returnFundTypeId(spin_fundType, dataspin_product),
            validate!!.RetriveSharepreferenceInt(VoSpData.LoanSource),
            0,
            validate!!.returnlookupcode(spin_mode_of_payment, dataspin_modeofpayment),
            returnAccount(),
            validate!!.returnStringValue(et_cheque_no_transactio_no.text.toString()),
            validate!!.returnlookupcode(
                spin_loan_repayment_frequency,
                dataspin_loan_repayment_frequency
            ),
            validate!!.returnIntegerValue(et_moratorium.text.toString()),
            validate!!.returnStringValue(et_loan_no.text.toString()),
            0,
            0,
            "",
            validate!!.returnIntegerValue(et_sanction_amount.text.toString()),
            0,
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            "",
            0,
            "",
            0,
            validate!!.Daybetweentime(et_sanction_date.text.toString()),0,0,0,0.0,0,1
        )
        voGroupLoanViewModel.insertVoGroupLoan(voGroupLoanEntity!!)

        // insert into vogrploantxn

        voGroupLoanTxnEntity = VoGroupLoanTxnEntity(
            0,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.returnIntegerValue(et_loan_no.text.toString()),
            validate!!.returnIntegerValue(et_amount_received.text.toString()),
            0,
            0,
            0,
            validate!!.returnIntegerValue(et_amount_received.text.toString()),
            0,
            false,
            0,
            0,
            0,
            0,
            0,
            0,
            validate!!.returnlookupcode(spin_mode_of_payment, dataspin_modeofpayment),
            returnAccount(),
            validate!!.returnStringValue(et_cheque_no_transactio_no.text.toString()),
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            "",
            0,
            "",
            0,0.0,0,0
        )

        voGroupLoanTxnViewModel.insertVoGroupLoanTxn(voGroupLoanTxnEntity!!)

        /*if (validate!!.RetriveSharepreferenceInt(VoSpData.voMeetingType) == 0) {
            voLoanApplicationViewmodel!!.updateloanapplication(
                validate!!.RetriveSharepreferenceString(
                    VoSpData.vomtg_guid
                )!!,
                validate!!.RetriveSharepreferenceLong(VoSpData.voShgMemID),
                validate!!.RetriveSharepreferenceLong(VoSpData.voLoanappid),
                validate!!.returnIntegerValue(et_amount_received.text.toString()),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
            )

        }*/
    }

    private fun insertscheduler(amt: Int, installment: Int, morotrum: Int) {
        var principaldemand = amt / installment
        var loanos = 0
        var saveValue = 0
        for (i in 0 until installment) {
            if (i == installment - 1) {
                loanos = 0
                principaldemand = amt - (principaldemand * i)
            } else {
                loanos = amt - (principaldemand * (i + 1))
            }
            var installmentDate = validate!!.addmonth(
                validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                (morotrum + i + 1),
                validate!!.returnlookupcode(
                    spin_loan_repayment_frequency,
                    dataspin_loan_repayment_frequency
                )
            )
            voGroupLoanScheduleEntity = VoGroupLoanScheduleEntity(
                0,
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
                validate!!.returnIntegerValue(et_loan_no.text.toString()),
                principaldemand,
                principaldemand,
                loanos, 0,
                i + 1,
                1,
                installmentDate,
                validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                false, 0,
                null,
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime), "", 0,
                "", 0

            )
            voGroupLoanScheduleViewModel.insertVoGroupLoanSchedule(voGroupLoanScheduleEntity!!)
        }

        CustomAlertchangeschedule(
            LabelSet.getText(
                "doyouwanttochaneschedule",
                R.string.doyouwanttochaneschedule
            )
        )
    }

    fun fillBankSpinner() {
        voBankList =
            cboBankViewmodel.getBankdata1(validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID))
        val adapter: ArrayAdapter<String?>
        if (!voBankList.isNullOrEmpty()) {
            val isize = voBankList!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in voBankList!!.indices) {
                var lastfour =
                    voBankList!![i].account_no.substring(voBankList!![i].account_no.length - 4)
                sValue[i + 1] =
                    voBankList!![i].ifsc_code.toString().dropLast(7) + "XXXXX" + lastfour
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_source_bank.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_source_bank.adapter = adapter
        }

    }

    fun returnAccount(): String {

        var pos = spin_source_bank.selectedItemPosition
        var id = ""

        if (!voBankList.isNullOrEmpty()) {
            if (pos > 0) id =
                voBankList!!.get(pos - 1).ifsc_code!!.dropLast(7) + voBankList!!.get(pos - 1).account_no
        }
        return id
    }

    fun setAccount(accountno: String): Int {
        var pos = 0
        if (!voBankList.isNullOrEmpty()) {
            for (i in voBankList!!.indices) {
                if (accountno.equals(
                        voBankList!!.get(i).ifsc_code!!.dropLast(7) + voBankList!!.get(
                            i
                        ).account_no
                    )
                )
                    pos = i + 1
            }
        }
        spin_source_bank.setSelection(pos)
        return pos
    }

    fun fillproduct() {
        when {
            validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 2 -> {
                dataspin_product = loanProductViewmodel!!.getFundTypeBySource_Receiptwithtypeid(
                    4, 3, validate!!.RetriveSharepreferenceInt(VoSpData.ShortDescription)
                )
                validate!!.fillFundType(this, spin_fundType, dataspin_product)

            }
            validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 3 -> {
                dataspin_product = loanProductViewmodel!!.getFundTypeBySource_Receiptwithtypeid(
                    5, 3, validate!!.RetriveSharepreferenceInt(VoSpData.ShortDescription)
                )
                validate!!.fillFundType(this, spin_fundType, dataspin_product)

            }
            validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 4 -> {
                dataspin_product = loanProductViewmodel!!.getFundTypeBySource_Receiptwithtypeid(
                    99, 3, validate!!.RetriveSharepreferenceInt(VoSpData.ShortDescription)
                )
                validate!!.fillFundType(this, spin_fundType, dataspin_product)

            }

        }


    }


    private fun fillSpinner() {
        /*dataspin_fund_type = lookupViewmodel!!.getlookup(
            62,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )*/
        dataspin_loan_repayment_frequency = lookupViewmodel!!.getlookup(
            19,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_modeofpayment = lookupViewmodel!!.getlookup(
            61,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        //  validate!!.fillspinner(this, spin_fundType, dataspin_fund_type)
        validate!!.fillspinner(
            this,
            spin_loan_repayment_frequency,
            dataspin_loan_repayment_frequency
        )
        validate!!.fillspinner(this, spin_mode_of_payment, dataspin_modeofpayment)
    }

    fun CustomAlertchangeschedule(str: String) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str
        mDialogView.btn_yes.background =
            resources.getDrawable(R.drawable.button_bg_vomeeting)
        mDialogView.btn_no.background =
            resources.getDrawable(R.drawable.button_bg_vomeeting)
        mDialogView.btn_yes.setOnClickListener {
            //            PartialSaveData()
            mAlertDialog.dismiss()
            validate!!.SaveSharepreferenceInt(
                VoSpData.voLoanno,
                validate!!.returnIntegerValue(et_loan_no.text.toString())
            )

            val intent = Intent(this, VoGroupPrincipalDemandActivity::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)

        }
        mDialogView.btn_no.setOnClickListener {
            val intent = Intent(this, CLFtoVoLoanReceiptsSummaryActivity::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)
            mAlertDialog.dismiss()
        }
    }

    fun showData() {
        var list = voGroupLoanViewModel.getLoandata(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.voLoanno),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
        )

        if (!list.isNullOrEmpty() && list.size > 0) {

            /*  spin_fundType.setSelection(
                  validate!!.returnlookupcodepos(
                      validate!!.returnIntegerValue(list.get(0).loanType.toString()),
                      dataspin_fund_type
                  )
              )*/

            /*spin_loan_product.setSelection(
                setproduct(
                    validate!!.returnLongValue(list.get(0).loanProductId.toString())
                )
            )*/
            spin_fundType.setSelection(
                validate!!.setFundType(
                    validate!!.returnIntegerValue(
                        list.get(0).loanProductId.toString()
                    ), dataspin_product
                )
            )
            et_loan_no.setText(validate!!.returnStringValue(list.get(0).loanNo.toString()))
            et_sanction_amount.setText(validate!!.returnStringValue(list.get(0).sanctionedAmount.toString()))

            /*var date = validate!!.returnLongValue(list.get(0).installmentDate.toString())
            et_sanction_amount.setText(validate!!.convertDatetime(date))*/

            et_amount_received.setText(validate!!.returnStringValue(list.get(0).amount.toString()))
            var date = validate!!.returnLongValue(list.get(0).installmentDate.toString())
            et_sanction_date.setText(
                validate!!.convertDatetime(
                    validate!!.returnLongValue(
                        list.get(
                            0
                        ).sanctionDate.toString()
                    )
                )
            )
            et_date_of_amount_received.setText(validate!!.convertDatetime(date))

            spin_mode_of_payment.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(list.get(0).modePayment.toString()),
                    dataspin_modeofpayment
                )
            )

            spin_source_bank.setSelection(
                setAccount(
                    validate!!.returnStringValue(list.get(0).bankCode.toString())
                )
            )

            et_cheque_no_transactio_no.setText(validate!!.returnStringValue(list.get(0).transactionNo.toString()))
            et_interest_rate.setText(validate!!.returnStringValue(list.get(0).interestRate.toString()))
            et_loan_tenure.setText(validate!!.returnStringValue(list.get(0).period.toString()))

            spin_loan_repayment_frequency.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(list.get(0).installmentFreq.toString()),
                    dataspin_loan_repayment_frequency
                )
            )

            et_moratorium.setText(validate!!.returnStringValue(list.get(0).moratoriumPeriod.toString()))
            setminmaxlimit()
        } else {
            spin_fundType.setSelection(
                validate!!.setFundType(
                    validate!!.RetriveSharepreferenceInt(
                        VoSpData.ShortDescription
                    ), dataspin_product
                )
            )
            fillfunddata()
            setminmaxlimit()
            et_sanction_date.setText(
                validate!!.convertDatetime(
                    validate!!.RetriveSharepreferenceLong(
                        VoSpData.voCurrentMtgDate
                    )
                )
            )
            et_date_of_amount_received.setText(
                validate!!.convertDatetime(
                    validate!!.RetriveSharepreferenceLong(
                        VoSpData.voCurrentMtgDate
                    )
                )
            )
        }
    }

}
