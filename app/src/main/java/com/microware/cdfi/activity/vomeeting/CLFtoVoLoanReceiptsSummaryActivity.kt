package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.LoanReceiptSummaryAdapter
import com.microware.cdfi.entity.voentity.VoGroupLoanEntity
import com.microware.cdfi.fragment.VoReceiptsTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.MstVOCOAViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoGroupLoanViewModel
import kotlinx.android.synthetic.main.buttons_vo.btn_cancel
import kotlinx.android.synthetic.main.buttons_vo.btn_save
import kotlinx.android.synthetic.main.loan_summary_clf_to_vo.*
import kotlinx.android.synthetic.main.loan_summary_clf_to_vo.tv_total

class CLFtoVoLoanReceiptsSummaryActivity : AppCompatActivity() {
    var validate: Validate? = null
    var voGroupLoanViewModel: VoGroupLoanViewModel? = null
    var mstVOCOAViewmodel: MstVOCOAViewmodel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.loan_summary_clf_to_vo)

        validate = Validate(this)
        voGroupLoanViewModel =
            ViewModelProviders.of(this).get(VoGroupLoanViewModel::class.java)
        mstVOCOAViewmodel =
            ViewModelProviders.of(this).get(MstVOCOAViewmodel::class.java)




        btn_cancel.setOnClickListener {
            var intent = Intent(this, CLFtoVOReceipts::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        ivADD.setOnClickListener {
            validate!!.SaveSharepreferenceInt(VoSpData.voLoanno, 0)
            var intent = Intent(this, CLFtoVoLoanReceipts::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        setLabelText()
        fillRecyclerView()
        checkFragment()

    }
    private fun checkFragment(){
        when {
            validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 2 -> {
                replaceFragmenty(
                    fragment = VoReceiptsTopBarFragment(5),
                    allowStateLoss = true,
                    containerViewId = R.id.mainContent
                )
            }
            validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 3 -> {
                replaceFragmenty(
                    fragment = VoReceiptsTopBarFragment(10),
                    allowStateLoss = true,
                    containerViewId = R.id.mainContent
                )
            }
            validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 4 -> {
                replaceFragmenty(
                    fragment = VoReceiptsTopBarFragment(14),
                    allowStateLoss = true,
                    containerViewId = R.id.mainContent
                )
            }

        }
        /*if (validate!!.RetriveSharepreferenceInt(VoSpData.voOrgType) == 2) {

        } else if (validate!!.RetriveSharepreferenceInt(VoSpData.voOrgType) == 3) {

        } else if (validate!!.RetriveSharepreferenceInt(VoSpData.voOrgType) == 4) {

        }*/
    }

    override fun onBackPressed() {
        var intent = Intent(this, CLFtoVOReceipts::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

    fun setLabelText() {
        tv_Loans.text = LabelSet.getText("Loans", R.string.Loans)
        tv_funds.text = LabelSet.getText(
            "funds",
            R.string.funds
        )
        tv_total_disbursed.text = LabelSet.getText(
            "total_amount_received",
            R.string.total_amount_received
        )
        tv_total.text = LabelSet.getText("total", R.string.total)
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }

    private fun fillRecyclerView() {
        var list = voGroupLoanViewModel!!.getListDataByMtgnum(
            validate!!.RetriveSharepreferenceInt(
                VoSpData.vocurrentmeetingnumber
            ),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(
                VoSpData.LoanSource
            ), validate!!.RetriveSharepreferenceInt(VoSpData.ShortDescription)

        )
        getTotal(list)
        loan_summary.layoutManager = LinearLayoutManager(this)
        var loanReceiptSummaryAdapter =
            LoanReceiptSummaryAdapter(this, list)
        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = loan_summary.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        loan_summary.layoutParams = params
        loan_summary.adapter = loanReceiptSummaryAdapter

    }

    fun getTotal(list: List<VoGroupLoanEntity>) {

        var total = 0
        for (i in 0 until list.size) {
            total += validate!!.returnIntegerValue(list.get(i).amount.toString())
            tv_total_loan_payment.text = total.toString()
        }
    }

    fun getRecepitType(keyCode: Int?): String? {
        var name: String? = null
        name = mstVOCOAViewmodel!!.getcoaValue(
            "RL",
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),
            keyCode
        )
        return name
    }

}