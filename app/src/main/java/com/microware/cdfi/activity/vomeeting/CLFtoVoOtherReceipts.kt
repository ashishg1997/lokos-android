package com.microware.cdfi.activity.vomeeting

import android.app.DatePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.DatePicker
import android.widget.EditText
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.entity.voentity.VoFinTxnDetGrpEntity
import com.microware.cdfi.fragment.VoReceiptsTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoFinTxnDetGrpViewModel
import kotlinx.android.synthetic.main.activity_clfto_vo_other_receipts.et_amount_received
import kotlinx.android.synthetic.main.activity_clfto_vo_other_receipts.et_date_of_amount_received
import kotlinx.android.synthetic.main.activity_clfto_vo_other_receipts.spin_fund_type
import kotlinx.android.synthetic.main.activity_clfto_vo_other_receipts.tv_amount_received
import kotlinx.android.synthetic.main.activity_clfto_vo_other_receipts.tv_date_of_amount_received
import kotlinx.android.synthetic.main.activity_clfto_vo_other_receipts.tv_fund_type
import kotlinx.android.synthetic.main.activity_clfto_vo_other_receipts.tv_total
import kotlinx.android.synthetic.main.buttons_vo.*
import java.text.SimpleDateFormat
import java.util.*

class CLFtoVoOtherReceipts : AppCompatActivity() {
    var cal = Calendar.getInstance()
    var dataspin_fund_type: List<LookupEntity>? = null
    var validate: Validate? = null
    var lookupViewmodel: LookupViewmodel? = null
    var voFinTxnDetGrpEntity: VoFinTxnDetGrpEntity? = null
    lateinit var voFinTxnDetGrpViewModel: VoFinTxnDetGrpViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_clfto_vo_other_receipts)
        btn_save.text = LabelSet.getText("confirm",R.string.confirm)
        validate = Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        voFinTxnDetGrpViewModel = ViewModelProviders.of(this).get(VoFinTxnDetGrpViewModel::class.java)
        setLabelText()
        fillSpinner()

        replaceFragmenty(
            fragment = VoReceiptsTopBarFragment(7),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        val dateSetListener = object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(
                view: DatePicker, year: Int, monthOfYear: Int,
                dayOfMonth: Int
            ) {
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateDateInView(et_date_of_amount_received)
            }
        }
        et_date_of_amount_received.setOnClickListener {
            val calendar = Calendar.getInstance()

            val mYear = calendar.get(Calendar.YEAR)
            val mMonth = calendar.get(Calendar.MONTH)
            val mDay = calendar.get(Calendar.DAY_OF_MONTH)

            val dpDialog = DatePickerDialog(this, dateSetListener, mYear, mMonth, mDay)
            dpDialog.show()
            dpDialog.datePicker.maxDate = calendar.timeInMillis
        }

        btn_save.setOnClickListener {
          //  if (checkValidation() == 1) {
                saveData()
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),this, CLFtoVoWithdrawal::class.java)

            //   }
        }

        btn_cancel.setOnClickListener {
            var intent = Intent(this, CLFtoVOReceipts::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        showData()
    }


    fun setLabelText()
    {
        tv_fund_type.text = LabelSet.getText(
            "fund_type",
            R.string.fund_type
        )

        tv_amount_received.text = LabelSet.getText(
            "amount_received",
            R.string.amount_received
        )

        tv_date_of_amount_received.text = LabelSet.getText("date_of_amount_received",
            R.string.date_of_amount_received
        )

        et_amount_received.hint = LabelSet.getText(
            "enter_amount",
            R.string.enter_amount
        )

        et_date_of_amount_received.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )

        tv_total.text = LabelSet.getText(
            "total",
            R.string.total
        )

        btn_save.text = LabelSet.getText(
            "confirm",
            R.string.confirm
        )
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }

    private fun saveData(){
        voFinTxnDetGrpEntity = VoFinTxnDetGrpEntity(
            0,
            0,
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            0,
            validate!!.returnlookupcode(spin_fund_type, dataspin_fund_type),
            0,
            0,
            "",
            validate!!.returnIntegerValue(et_amount_received.text.toString()),
            "",
            validate!!.Daybetweentime(et_date_of_amount_received.text.toString()),
            0,
            "",

            "",
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            "",
            0,
            "",
            0
        )
        voFinTxnDetGrpViewModel.insertVoGroupLoanSchedule(voFinTxnDetGrpEntity!!)
    }

    private fun showData(){
        var list =
            voFinTxnDetGrpViewModel.getVoFinTxnDetGrpList((validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!)
                ,validate!!.RetriveSharepreferenceLong(VoSpData.voshgid))

        if (!list.isNullOrEmpty() && list.size > 0) {

            spin_fund_type.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(list.get(0).fundType.toString()),
                    dataspin_fund_type)
            )

            var amout = validate!!.returnIntegerValue(list.get(0).amount.toString())
            et_amount_received.setText(amout.toString())

            var date = validate!!.returnLongValue(list.get(0).dateRealisation.toString())
            et_date_of_amount_received.setText(validate!!.convertDatetime(date))
        }
    }

    private fun checkValidation(): Int {
        var value = 1
        if (spin_fund_type.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_fund_type, LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "fund_type",
                    R.string.fund_type
                )
            )
            value = 0
            return value
        }
        if (validate!!.returnStringValue(et_amount_received.text.toString()).length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "amount_received",
                    R.string.amount_received
                ), this, et_amount_received
            )
            value = 0
            return value
        }
        if (validate!!.returnStringValue(et_date_of_amount_received.text.toString()).length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "date_of_amount_received",
                    R.string.date_of_amount_received
                ), this, et_date_of_amount_received
            )
            value = 0
            return value
        }

        return value
    }

    private fun fillSpinner() {
        //spinner value not available
        dataspin_fund_type = lookupViewmodel!!.getlookupMasterdata(
            82, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode), listOf(3)
        )
        validate!!.fillspinner(this, spin_fund_type, dataspin_fund_type)
    }

    private fun updateDateInView(editText: EditText) {
        val myFormat = "dd-MM-yyyy" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        editText.setText((sdf.format(cal.time)))
    }

    override fun onBackPressed() {
        var intent = Intent(this, CLFtoVORfVrfGrantReceipts::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
    }
}