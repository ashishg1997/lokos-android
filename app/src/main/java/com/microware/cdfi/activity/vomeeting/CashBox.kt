package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.fragment.OthersScreenTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.voviewmodel.*
import kotlinx.android.synthetic.main.buttons_vo.*
import kotlinx.android.synthetic.main.cash_box.*

class CashBox : AppCompatActivity() {
    var validate: Validate? = null
    lateinit var voGenerateMeetingViewmodel: VoGenerateMeetingViewmodel
    lateinit var voFinTxnDetMemViewModel: VoFinTxnDetMemViewModel
    lateinit var voFinTxnDetGrpViewModel: VoFinTxnDetGrpViewModel
    lateinit var voMemLoanTxnViewModel: VoMemLoanTxnViewModel
    lateinit var voGroupLoanViewModel: VoGroupLoanViewModel
    lateinit var voGroupLoanTxnViewModel: VoGroupLoanTxnViewModel
    lateinit var voMemLoanViewModel: VoMemLoanViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.cash_box)

        validate = Validate(this)
        voGenerateMeetingViewmodel =
            ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        voFinTxnDetMemViewModel =
            ViewModelProviders.of(this).get(VoFinTxnDetMemViewModel::class.java)
        voFinTxnDetGrpViewModel =
            ViewModelProviders.of(this).get(VoFinTxnDetGrpViewModel::class.java)
        voMemLoanTxnViewModel =
            ViewModelProviders.of(this).get(VoMemLoanTxnViewModel::class.java)
        voGroupLoanViewModel =
            ViewModelProviders.of(this).get(VoGroupLoanViewModel::class.java)
        voGroupLoanTxnViewModel =
            ViewModelProviders.of(this).get(VoGroupLoanTxnViewModel::class.java)
        voMemLoanViewModel =
            ViewModelProviders.of(this).get(VoMemLoanViewModel::class.java)

        replaceFragmenty(
            fragment = OthersScreenTopBarFragment(22),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        setLabel()
        getOpeningBalance()
        getClosingBalance()
        getTotalCreditBalance()
        getTotalDebitBalance()
        updateCashInTransit()
    }

    fun setLabel() {
        btn_save.text = LabelSet.getText("confirm", R.string.confirm)
        btn_cancel.text = LabelSet.getText("cancel", R.string.cancel)
        tv_opening_balance.text = LabelSet.getText("opening_balance", R.string.opening_balance)
        tv_total_incoming.text = LabelSet.getText(
            "total_incoming_amount",
            R.string.total_incoming_amount
        )
        tv_total_outgoing.text = LabelSet.getText(
            "total_outcome_amount",
            R.string.total_outcome_amount
        )
        tv_closing_balance.text = LabelSet.getText("closing_balance", R.string.closing_balance)
        tv_total_cash_in_transit.text = LabelSet.getText(
            "cash_in_transit",
            R.string.cash_in_transit
        )
    }

    fun getOpeningBalance() {
        var balance = 0
        balance = voGenerateMeetingViewmodel.getupdatedpeningcash(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        tv_amount_opening_balance.text = "₹ $balance"
    }

    fun getClosingBalance() {
        var balance = 0
        balance = voGenerateMeetingViewmodel.getClosingBalanceCash(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        tv_amount_closing_balance.text = "₹ $balance"
    }

    fun updateCashInTransit() {
        var cashInTransit = 0
        cashInTransit = voGenerateMeetingViewmodel.getCashInTransit(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        tv_amout_cash_transit.text = "₹ $cashInTransit"

    }

    fun getTotalDebitBalance() {
        var balance = 0
        var totshgcashdebit = voFinTxnDetMemViewModel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(1),
            listOf("PG", "PL", "PO"),
            ""
        )
        var totvocashdebit = voFinTxnDetGrpViewModel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(1),
            listOf("PG", "PL", "PO"),
            ""
        )

        val grptotloanpaid = voGroupLoanTxnViewModel.getgrptotloanpaidinbank(

            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            "",
            listOf<Int>(1)
        )

        val loanDisbursedAmt = voMemLoanViewModel.getMemLoanDisbursedAmount(

            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            "",
            listOf<Int>(1)
        )

        balance = totshgcashdebit + totvocashdebit + grptotloanpaid + loanDisbursedAmt
        tv_amount_total_incoming.text = "₹ $balance"
    }

    fun getTotalCreditBalance() {
        var balance = 0
        var totshgcash = voFinTxnDetMemViewModel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(1),
            listOf("RG", "RL", "RO"),
            ""
        )
        var totvocash = voFinTxnDetGrpViewModel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(1),
            listOf("RG", "RL", "RO"),
            ""
        )
        val memtotloanpaid = voMemLoanTxnViewModel.getmemtotloanpaidinbank(

            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            "",
            listOf<Int>(1)
        )
        val loanDisbursedAmt = voGroupLoanViewModel.getgrouptotloanamtinbank(

            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            "",
            listOf<Int>(1)
        )
        balance = totshgcash + totvocash + memtotloanpaid + loanDisbursedAmt
        tv_total_outgoing_amount.text = "₹ $balance"
    }

    override fun onBackPressed() {
        val intent = Intent(this, VoMeetingMenuActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }
}