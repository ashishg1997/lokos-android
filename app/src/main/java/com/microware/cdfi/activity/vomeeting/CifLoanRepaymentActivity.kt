package com.microware.cdfi.activity.vomeeting

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.view.View
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.R
import kotlinx.android.synthetic.main.activity_cif_loan_repayment_shg_to_vo.*
import kotlinx.android.synthetic.main.buttons_vo.*
import kotlinx.android.synthetic.main.repay_toolbar_vomeeting.*

class CifLoanRepaymentActivity : AppCompatActivity(), View.OnClickListener {
    var lookupViewmodel: LookupViewmodel? = null
    var validate: Validate? = null
    var dataspin_total_loan_due: List<LookupEntity>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cif_loan_repayment_shg_to_vo)

        validate = Validate(this)
        setLabelText()
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)

        btn_save.setOnClickListener {
            var intent = Intent(this, ShareCapital53Activity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        btn_cancel.setOnClickListener {
            var intent = Intent(this, CifLoanRepaymentActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            finish()
        }
        ic_Back.setOnClickListener {
            var intent = Intent(this, VoMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        fillSpinner()
    }

    private fun setLabelText() {
        tv_typeofpayee.text = LabelSet.getText(
            "type_of_payee",
            R.string.type_of_payee
        )
        et_typeofpayee.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_nameofpayee.text = LabelSet.getText(
            "name_of_payee",
            R.string.name_of_payee
        )
        et_nameofpayee.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_loan_no.text = LabelSet.getText(
            "loan_no",
            R.string.loan_no
        )
        et_loan_no.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_loanproduct.text = LabelSet.getText(
            "loan_product",
            R.string.loan_product
        )
        et_loanproduct.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_total_loan_due.text = LabelSet.getText(
            "total_loan_due",
            R.string.total_loan_due
        )
        tv_principaldemand_cm.text = LabelSet.getText(
            "principal_demand_of_current_month",
            R.string.principal_demand_of_current_month
        )
        et_principaldemand_cm.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_interestdemand_cm.text = LabelSet.getText(
            "interest_demand_of_current_month",
            R.string.interest_demand_of_current_month
        )
        et_interestdemand_cm.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_principalrepaid.text = LabelSet.getText(
            "principal_repaid",
            R.string.principal_repaid
        )
        et_principalrepaid.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_interestrepaid.text = LabelSet.getText(
            "interest_repaid",
            R.string.interest_repaid
        )
        et_interestrepaid.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_balance_amount.text = LabelSet.getText(
            "balance_amount",
            R.string.balance_amount
        )
        et_balance_amount.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
        tv_title.text = LabelSet.getText(
            "cif_5_1_loan_repayment_shg_to_vo",
            R.string.cif_5_1_loan_repayment_shg_to_vo
        )
    }

    private fun fillSpinner() {
        dataspin_total_loan_due = lookupViewmodel!!.getlookup(
            99,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        validate!!.fillspinner(this, spin_total_loan_due, dataspin_total_loan_due)
    }

    override fun onClick(v: View?) {
        TODO("Not yet implemented")
    }
}