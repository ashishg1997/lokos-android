package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.ClftoVoReceiptAdapter
import com.microware.cdfi.utility.LabelSet
import kotlinx.android.synthetic.main.activity_clf_to_vo_receipt.*
import kotlinx.android.synthetic.main.buttons_vo.btn_cancel
import kotlinx.android.synthetic.main.buttons_vo.btn_save
import kotlinx.android.synthetic.main.repay_toolbar_vomeeting.*

class ClftoVoReceiptActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_clf_to_vo_receipt)

        btn_cancel.setOnClickListener {
            var intent = Intent(this, VoMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        btn_save.setOnClickListener {
            var intent = Intent(this, LoantoVoActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        ic_Back.setOnClickListener {
            var intent = Intent(this, VoMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        setLabelText()
        fillRecyclerView()
    }

    override fun onBackPressed() {
        var intent = Intent(this, VoMeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0,0)
        finish()
    }

    fun setLabelText() {
        tv_title.text = LabelSet.getText(
            "clf_to_vo_receipt",
            R.string.clf_to_vo_receipt
        )
        tv_srno.text = LabelSet.getText("sr_no", R.string.sr_no)
        tv_receipt_head.text = LabelSet.getText(
            "receipt_head",
            R.string.receipt_head
        )
        tv_amount.text = LabelSet.getText(
            "amount",
            R.string.amount
        )
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }

    private fun fillRecyclerView() {
        rvList.layoutManager = LinearLayoutManager(this)
        rvList.adapter =
            ClftoVoReceiptAdapter(this)
    }
}