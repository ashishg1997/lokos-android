package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.LookupViewmodel
import kotlinx.android.synthetic.main.activity_compulsory_from_shg.*
import kotlinx.android.synthetic.main.buttons_vo.*
import kotlinx.android.synthetic.main.repay_toolbar_vomeeting.*

class CompulsoryfromShgActivity : AppCompatActivity() {

    var lookupViewmodel: LookupViewmodel? = null
    var validate: Validate? = null
    var dataspin_type_of_payee: List<LookupEntity>? = null
    var dataspin_amount_of_saving: List<LookupEntity>? = null
    var dataspin_comulative_saving: List<LookupEntity>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_compulsory_from_shg)

        validate = Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        setLabelText()

        btn_cancel.setOnClickListener {
            var intent = Intent(this, VoMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        btn_save.setOnClickListener {
            var intent = Intent(this, AdvancetoVoActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        ic_Back.setOnClickListener {
            var intent = Intent(this, VoMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        fillSpinner()
    }

    override fun onBackPressed() {
        var intent = Intent(this, VoMeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0,0)
        finish()
    }

    fun setLabelText()
    {
        tv_type_of_payee.text = LabelSet.getText(
            "type_of_payee",
            R.string.type_of_payee
        )
        tv_name_of_payee.text = LabelSet.getText(
            "name_of_payee",
            R.string.name_of_payee
        )
        et_name_of_payee.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_amount_of_saving.text = LabelSet.getText(
            "amount_of_saving",
            R.string.amount_of_saving
        )
        tv_frequency_of_saving.text = LabelSet.getText(
            "frequency_of_saving",
            R.string.frequency_of_saving
        )
        et_frequency_of_saving.setText(LabelSet.getText(
            "type_here",
            R.string.type_here
        ))
        tv_comulative_saving.text = LabelSet.getText(
            "comulative_saving",
            R.string.comulative_saving
        )
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
        tv_title.text = LabelSet.getText(
            "compulsory_saving_from_shg",
            R.string.compulsory_saving_from_shg
        )

    }

    private fun fillSpinner() {
        dataspin_type_of_payee = lookupViewmodel!!.getlookup(
            99,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_amount_of_saving = lookupViewmodel!!.getlookup(
            99,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_comulative_saving = lookupViewmodel!!.getlookup(
            99,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        validate!!.fillspinner(this, spin_type_of_payee, dataspin_type_of_payee)
        validate!!.fillspinner(this, spin_amount_of_saving, dataspin_amount_of_saving)
        validate!!.fillspinner(this, spin_comulative_saving, dataspin_comulative_saving)
    }
}