package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.ECAttendanceAdapter
import com.microware.cdfi.fragment.VoMeetingTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.ExecutiveMemberViewmodel
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoMtgDetViewModel
import kotlinx.android.synthetic.main.buttons_vo.*
import kotlinx.android.synthetic.main.ec_member_attendance.*

class Ec_MemberAttendanceActivity : AppCompatActivity() {
    var validate: Validate? = null
//    lateinit var mappedShgViewmodel: MappedShgViewmodel
//    lateinit var voMtgViewmodel: VoMtgDetViewModel

    lateinit var vomtgDetViewmodel: VoMtgDetViewModel
    lateinit var executiveMemberViewmodel: ExecutiveMemberViewmodel
    lateinit var lookupViewmodel: LookupViewmodel
    lateinit var attendenceDetailAdapter: ECAttendanceAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.ec_member_attendance)

        validate = Validate(this)
//        mappedShgViewmodel = ViewModelProviders.of(this).get(MappedShgViewmodel::class.java)
//        voMtgViewmodel = ViewModelProviders.of(this).get(VoMtgDetViewModel::class.java)
        vomtgDetViewmodel = ViewModelProviders.of(this).get(VoMtgDetViewModel::class.java)
        executiveMemberViewmodel =
            ViewModelProviders.of(this).get(ExecutiveMemberViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)

        replaceFragmenty(
            fragment = VoMeetingTopBarFragment(4),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        btn_cancel.setOnClickListener {
            val intent = Intent(this, VoMeetingMenuActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)
        }

        btn_save.setOnClickListener {
            getTotalValue()
        }

        fillRecyclerView()
        setLabelText()

    }
    fun getTotalValue() {
        var saveValue = 0
        val iCount = rvList!!.childCount

        for (i in 0 until iCount) {

            val gridChild =
                rvList!!.getChildAt(i) as? ViewGroup

            val rvChild = gridChild!!.findViewById<View>(R.id.rvListMember) as? RecyclerView

            val iCountChild = rvChild!!.childCount

            for (j in 0 until iCountChild) {

                val gridChild2 =
                    rvChild.getChildAt(j) as? ViewGroup

                val et_attendance = gridChild2!!
                    .findViewById<View>(R.id.et_attendance) as? EditText

                val et_attendance_other = gridChild2
                    .findViewById<View>(R.id.et_attendance_other) as? EditText

                val tv_member_Designation = gridChild2
                    .findViewById<View>(R.id.tv_member_Designation) as? TextView

                val designation = tv_member_Designation!!.text.toString()

                val et_mem_id = gridChild2
                    .findViewById<View>(R.id.et_mem_id) as? EditText

                var ec1 = ""
                var ec2 = ""
                var ec3 = ""
                var ec4 = ""
                var ec5 = ""
                var attendanceOther = 0
                var memID = validate!!.returnLongValue(et_mem_id!!.text.toString())

                if (et_attendance!!.length() > 0) {

                    if (designation.isNotEmpty()) {
                        if (designation.equals("(President)", true)) {
                            ec1 = et_attendance.text.toString()
                            saveValue = saveData(ec1, ec2, ec3, ec4, ec5, memID, attendanceOther, 1)
                        } else if (designation.equals("(Vice-President)", true)) {
                            ec2 = et_attendance.text.toString()
                            saveValue = saveData(ec1, ec2, ec3, ec4, ec5, memID, attendanceOther, 2)
                        } else if (designation.equals("(Secretary)", true)) {
                            ec3 = et_attendance.text.toString()
                            saveValue = saveData(ec1, ec2, ec3, ec4, ec5, memID, attendanceOther, 3)
                        } else if (designation.equals("(Joint-Secretary)", true)) {
                            ec4 = et_attendance.text.toString()
                            saveValue = saveData(ec1, ec2, ec3, ec4, ec5, memID, attendanceOther, 4)
                        } else if (designation.equals("(Treasurer)", true)) {
                            ec5 = et_attendance.text.toString()
                            saveValue = saveData(ec1, ec2, ec3, ec4, ec5, memID, attendanceOther, 5)
                        }
                    }
                } else if (et_attendance_other!!.length() > 0) {

                    attendanceOther =
                        validate!!.returnIntegerValue(et_attendance_other.text.toString())
                    saveValue = saveData(ec1, ec2, ec3, ec4, ec5, memID, attendanceOther, 9)
                }
            }

        }

        if (saveValue > 0) {
            validate!!.CustomAlertVO(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this
            )
        }
    }
    private fun fillRecyclerView() {
        val list = vomtgDetViewmodel.getListDataMtgByMtgnum(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        attendenceDetailAdapter =
            ECAttendanceAdapter(
                this,
                list,
                executiveMemberViewmodel,
                lookupViewmodel
            )
        rvList.layoutManager = LinearLayoutManager(this)
        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = rvList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvList.layoutParams = params
        rvList.adapter = attendenceDetailAdapter
    }





    override fun onBackPressed() {
        val intent = Intent(this, VoMeetingMenuActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
        overridePendingTransition(0, 0)
    }

    fun setLabelText() {
        tv_ec_member_name.text = LabelSet.getText("ec_member_name", R.string.ec_member_name)
        tv_attendance.text = LabelSet.getText("attendance", R.string.attendance)

        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }



    fun saveData(
        EC1: String,
        EC2: String,
        EC3: String,
        EC4: String,
        EC5: String,
        memID: Long,
        attendanceOther: Int,
        flag: Int
    ): Int {
        var value = 0
        if (flag == 1) {
            vomtgDetViewmodel.updateAttendanceEC1(
                EC1,
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                memID,
                validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                validate!!.Daybetweentime(validate!!.currentdatetime)
            )
            value = 1
        } else if (flag == 2) {
            vomtgDetViewmodel.updateAttendanceEC2(
                EC2,
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                memID,
                validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                validate!!.Daybetweentime(validate!!.currentdatetime)
            )
            value = 1
        } else if (flag == 3) {
            vomtgDetViewmodel.updateAttendanceEC3(
                EC3,
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                memID,
                validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                validate!!.Daybetweentime(validate!!.currentdatetime)
            )
            value = 1
        } else if (flag == 4) {
            vomtgDetViewmodel.updateAttendanceEC4(
                EC4,
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                memID,
                validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                validate!!.Daybetweentime(validate!!.currentdatetime)
            )
            value = 1
        } else if (flag == 5) {
            vomtgDetViewmodel.updateAttendanceEC5(
                EC5,
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                memID,
                validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                validate!!.Daybetweentime(validate!!.currentdatetime)
            )
            value = 1
        } else if (flag == 9) {
            vomtgDetViewmodel.updateAttendanceOther(
                attendanceOther,
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                memID,
                validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
                validate!!.Daybetweentime(validate!!.currentdatetime)
            )
            value = 1
        }
        return value
    }
}
