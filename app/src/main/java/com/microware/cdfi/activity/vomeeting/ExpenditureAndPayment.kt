package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.entity.MstCOAEntity
import com.microware.cdfi.entity.voentity.VoFinTxnDetMemEntity
import com.microware.cdfi.fragment.OthersScreenTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoFinTxnDetMemViewModel
import com.microware.cdfi.viewModel.voviewmodel.VoGenerateMeetingViewmodel
import kotlinx.android.synthetic.main.buttons_vo.btn_cancel
import kotlinx.android.synthetic.main.buttons_vo.btn_save
import kotlinx.android.synthetic.main.expenditure_and_payment.*


class ExpenditureAndPayment : AppCompatActivity() {
    var validate: Validate? = null
    var lookupViewmodel: LookupViewmodel? = null
    lateinit var voGenerateMeetingViewmodel: VoGenerateMeetingViewmodel
    var voFinTxnDetMemEntity: VoFinTxnDetMemEntity? = null
    lateinit var voFinTxnDetMemViewmodel: VoFinTxnDetMemViewModel
    var dataspin_payment_type: List<MstCOAEntity>? = null
    var dataspin_paidTo: List<LookupEntity>? = null
    var dataspin_modeofpayment: List<LookupEntity>? = null
    var voBankList: List<Cbo_bankEntity>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.expenditure_and_payment)
        replaceFragmenty(
            fragment = OthersScreenTopBarFragment(23),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )
        validate = Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        voFinTxnDetMemViewmodel = ViewModelProviders.of(this).get(VoFinTxnDetMemViewModel::class.java)
        voGenerateMeetingViewmodel =
            ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)

        btn_cancel.setOnClickListener {
            val intent = Intent(this,ExpenditureAndPaymentSummary::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                SaveData()
            }
        }
        fillSpinner()
        fillbank()
        setLabelText()
        showData()
    }

    override fun onBackPressed() {
        val intent = Intent(this,ExpenditureAndPaymentSummary::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

    private fun setLabelText() {
        tv_paid_to.text = LabelSet.getText(
            "paid_to",
            R.string.paid_to
        )
        tv_payment_type.text = LabelSet.getText(
            "payment_type",
            R.string.payment_type
        )
        tv_name_of_shg_bank_others.text = LabelSet.getText(
            "name_of_shg_bank_others",
            R.string.name_of_shg_bank_others
        )
        tv_amount_paid.text = LabelSet.getText(
            "amount_paid",
            R.string.amount_paid
        )
        tv_cheque_payslip_rtgs_neft_imps.text = LabelSet.getText(
            "cheque_payslip_rtgs_neft_imps",
            R.string.cheque_payslip_rtgs_neft_imps
        )
        tv_cheque_no_transaction_no.text = LabelSet.getText(
            "cheque_no_transactio_no",
            R.string.cheque_no_transactio_no
        )
        tv_name_of_bank_receiver.text = LabelSet.getText(
            "name_of_bank_receiver",
            R.string.name_of_bank_receiver
        )
    }

    //fields not known for
    //spinner Paid to
    //name of shg/bank/others
    private fun SaveData() {

        var save = 0

        if (validate!!.RetriveSharepreferenceInt(VoSpData.voAuid) == 0) {

            voFinTxnDetMemEntity = VoFinTxnDetMemEntity(
                0,
                validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                0,
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                validate!!.returncoauid(spin_paymentType, dataspin_payment_type), /*payment type goes here auid*/
                "OE", /*type goes here fundtype*/
                validate!!.returnIntegerValue(et_amount_paid.text.toString()),
                validate!!.returnlookupcode(spin_ChequePayslip, dataspin_modeofpayment),
                0,
                returaccount(),
                et_cheque_no_transaction_no.text.toString(),

                "",
                validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                "",
                0,
                "",
                0,0
            )
            voFinTxnDetMemViewmodel.insertVoGroupLoanSchedule(voFinTxnDetMemEntity!!)
            save = 1
        } else {
            voFinTxnDetMemViewmodel.updateIncomeAndExpenditure(
                validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceInt(VoSpData.voAuid),
                "OE",
                validate!!.returnIntegerValue(et_amount_paid.text.toString()),
                0,
                validate!!.returnlookupcode(spin_ChequePayslip, dataspin_modeofpayment),
                returaccount(),
                et_cheque_no_transaction_no.text.toString(),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime)
            )

            save = 2
        }

        if (save == 1) {
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this, ExpenditureAndPaymentSummary::class.java
            )
        } else {
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_updated_successfully",
                    R.string.data_updated_successfully
                ),
                this, ExpenditureAndPaymentSummary::class.java
            )
        }
    }

    private fun showData() {
        val data = voFinTxnDetMemViewmodel.getIncomeAndExpendituredata(
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceInt(VoSpData.voAuid)
        )
        if (!data.isNullOrEmpty()) {
            et_amount_paid.setText(data[0].amount.toString())
            et_cheque_no_transaction_no.setText(data[0].transactionNo)

           /* spin_paid_to.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(
                        data[0].amount_to_from.toString()
                    ), dataspin_paidTo
                )
            )
            spin_paid_to.isEnabled = false*/

            spin_ChequePayslip.setSelection(
                    validate!!.returnlookupcodepos(
                        validate!!.returnIntegerValue(
                            data[0].modePayment.toString()
                        ), dataspin_modeofpayment
                    )
                )

            spin_paymentType.setSelection(
                validate!!.returncoauidpos(
                    validate!!.returnIntegerValue(
                        data[0].auid.toString()
                    ), dataspin_payment_type
                )
            )


            /*spin_BankName.setSelection(
                validate!!.returnMasterBankpos(validate!!.returnIntegerValue(data[0].bank_code.toString())
                    ,
                    dataspin_bank
                )
            )*/

            setaccount(data[0].bankCode.toString())

            spin_paymentType.isEnabled = false
        }else{
            spin_ChequePayslip.setSelection(
                validate!!.returnlookupcodepos(
                    1, dataspin_modeofpayment
                )
            )
        }
    }

    fun fillbank() {
        voBankList = voGenerateMeetingViewmodel.getBankdata(
            validate!!.RetriveSharepreferenceString(
                VoSpData.voSHGGUID
            )
        )

        val adapter: ArrayAdapter<String?>
        if (!voBankList.isNullOrEmpty()) {
            val isize = voBankList!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in voBankList!!.indices) {
                var lastthree=voBankList!![i].account_no.substring(voBankList!![i].account_no.length-3)
                sValue[i + 1] =
                    voBankList!![i].ifsc_code.toString().dropLast(7) +"XXXXX"+ lastthree
            }
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_bankReceiver.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_bankReceiver.adapter = adapter
        }

    }

    fun returaccount(): String {

        var pos = spin_bankReceiver.selectedItemPosition
        var id = ""

        if (!voBankList.isNullOrEmpty()) {
            if (pos > 0) id =
                voBankList!!.get(pos - 1).ifsc_code!!.dropLast(7) + voBankList!!.get(pos - 1).account_no
        }
        return id
    }

    fun setaccount(accountno:String): Int {

        var pos = 0

        if (!voBankList.isNullOrEmpty()) {
            for (i in voBankList!!.indices) {
                if (accountno.equals(
                        voBankList!!.get(i).ifsc_code!!.dropLast(7) + voBankList!!.get(
                            i
                        ).account_no
                    )
                )
                    pos = i + 1
            }
        }
        spin_bankReceiver.setSelection(pos)
        return pos
    }

    private fun checkValidation(): Int {

        var value = 1

        if (spin_paid_to.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_paid_to,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "paid_to",
                    R.string.paid_to
                )
            )
            value = 0
        }
        else if (spin_paymentType.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_paymentType,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "payment_type",
                    R.string.payment_type
                )
            )
            value = 0
        }
       /* else if (spin_SHGName.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_SHGName,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "name_of_shg_bank_others",
                    R.string.name_of_shg_bank_others
                )
            )
            value = 0
        }*/
        else if (et_amount_paid.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "amount_paid",
                    R.string.amount_paid
                ),
                this, et_amount_paid
            )
            value = 0
        }
        else if (spin_ChequePayslip.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_ChequePayslip,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "cheque_payslip_rtgs_neft_imps",
                    R.string.cheque_payslip_rtgs_neft_imps
                )
            )
            value = 0
        }
        else if (et_cheque_no_transaction_no.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "cheque_no_transactio_no",
                    R.string.cheque_no_transactio_no
                ),
                this, et_cheque_no_transaction_no
            )
            value = 0
        }
        else if (spin_bankReceiver.selectedItemPosition == 0 ) {
            validate!!.CustomAlertSpinner(
                this, spin_bankReceiver,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "name_of_bank_receiver",
                    R.string.name_of_bank_receiver
                )
            )
            value = 0
        }

        return value


    }

    private fun fillSpinner() {
        dataspin_paidTo = lookupViewmodel!!.getlookup(
            75,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_modeofpayment = lookupViewmodel!!.getlookup(
            65,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_payment_type = voFinTxnDetMemViewmodel.getCoaSubHeadData(listOf(20,21,47),"OE",validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!)

        validate!!.fillspinner(this, spin_paid_to, dataspin_paidTo)
        validate!!.fillCoaSubHeadspinner(this,spin_paymentType,dataspin_payment_type)
        validate!!.fillspinner(this, spin_ChequePayslip, dataspin_modeofpayment)

    }


}