package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.ExpenditurePaymentAdapter
import com.microware.cdfi.adapter.vomeetingadapter.ExpenditureandPaymentSummaryAdapter
import com.microware.cdfi.entity.ShgFinancialTxnDetailEntity
import com.microware.cdfi.entity.voentity.VoFinTxnDetMemEntity
import com.microware.cdfi.entity.voentity.VomtgEntity
import com.microware.cdfi.fragment.OthersScreenTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.IncomeandExpenditureViewmodel
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.MstCOAViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoFinTxnDetMemViewModel
import com.microware.cdfi.viewModel.voviewmodel.VoGenerateMeetingViewmodel
import kotlinx.android.synthetic.main.expenditure_and_payment_summary.*

class ExpenditureAndPaymentSummary : AppCompatActivity() {
    var Todayvalue = 0
    var validate: Validate? = null
    lateinit var voFinTxnDetMemViewmodel: VoFinTxnDetMemViewModel
    var mstcoaViewmodel: MstCOAViewmodel? = null
    var lookupViewmodel: LookupViewmodel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.expenditure_and_payment_summary)
        validate = Validate(this)

        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        mstcoaViewmodel = ViewModelProviders.of(this).get(MstCOAViewmodel::class.java)
        voFinTxnDetMemViewmodel =
            ViewModelProviders.of(this).get(VoFinTxnDetMemViewModel::class.java)


        replaceFragmenty(
            fragment = OthersScreenTopBarFragment(24),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        ivAdd.isEnabled =
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber) >= validate!!.RetriveSharepreferenceInt(
                VoSpData.vomaxmeetingnumber
            )

        ivAdd.setOnClickListener {
            validate!!.SaveSharepreferenceInt(VoSpData.voAuid, 0)
            var intent = Intent(this, ExpenditureAndPayment::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        expenditureAndPaymentSummaryRecycler()
        setLabelText()
    }
   /* private fun expenditureAndPaymentSummaryRecycler() {
        rvList.layoutManager = LinearLayoutManager(this)
        rvList.adapter = ExpenditureandPaymentSummaryAdapter(this)
    }*/

    private fun expenditureAndPaymentSummaryRecycler() {
        Todayvalue = 0
        voFinTxnDetMemViewmodel.getIncomeAndExpenditureAlldata(
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)

        ).observe(this, object : Observer<List<VoFinTxnDetMemEntity>?> {
            override fun onChanged(list: List<VoFinTxnDetMemEntity>?) {
                if (!list.isNullOrEmpty()) {
                    rvList.layoutManager = LinearLayoutManager(this@ExpenditureAndPaymentSummary)
                    val expenditurePaymentAdapter = ExpenditureandPaymentSummaryAdapter(
                        this@ExpenditureAndPaymentSummary,
                        list
                    )
                    val isize: Int
                    isize = list.size
                    val params: ViewGroup.LayoutParams = rvList.layoutParams
                    val r = resources
                    val px = TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_PX,
                        resources.getDimension(R.dimen.spraying),
                        r.displayMetrics
                    )
                    val hi = Math.round(px)
                    val gridHeight = hi * isize
                    params.height = gridHeight
                    rvList.layoutParams = params
                    rvList.adapter = expenditurePaymentAdapter
                }
            }
        })
    }

    private fun setLabelText() {
        tv_particulars.text = LabelSet.getText(
            "particulars",
            R.string.particulars
        )
        tv_amount.text = LabelSet.getText(
            "amount",
            R.string.amount
        )
        tv_total.text = LabelSet.getText("total", R.string.total)
    }

    override fun onBackPressed() {
        var intent = Intent(this,VoMeetingMenuActivity::class.java)
        intent.flags  = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

    fun getRecepitType(keyCode: Int?): String? {
        var name: String? = null
        name = mstcoaViewmodel!!.getcoaValue(
            "OE",
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),
            keyCode
        )
        return name
    }

    fun getTotalValue() {
        var iValue1 = 0
        val iCount = rvList.childCount
        for (i in 0 until iCount) {
            val gridChild = rvList.getChildAt(i) as? ViewGroup
            val tv_value1 = gridChild!!.findViewById<View>(R.id.tv_amount) as? TextView

            if (!tv_value1!!.text.toString().isNullOrEmpty()) {
                iValue1 = iValue1 + validate!!.returnIntegerValue(tv_value1.text.toString())
            }
        }
        tv_total_Amount.text = iValue1.toString()
    }

    fun getTotalValue(iValue: Int) {
        Todayvalue = Todayvalue + iValue
        tv_total_Amount.text = Todayvalue.toString()
    }
}