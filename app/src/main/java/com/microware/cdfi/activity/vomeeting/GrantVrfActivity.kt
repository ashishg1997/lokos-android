package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.LookupViewmodel
import kotlinx.android.synthetic.main.activity_grant_vrf.*
import kotlinx.android.synthetic.main.buttons_vo.*
import kotlinx.android.synthetic.main.repay_toolbar_vomeeting.*

class GrantVrfActivity : AppCompatActivity(), View.OnClickListener {
    var lookupViewmodel: LookupViewmodel? = null
    var validate: Validate? = null
    var dataspin_type_of_payee_source: List<LookupEntity>? = null
    var dataspin_receipt_type: List<LookupEntity>? = null
    var dataspin_narration1: List<LookupEntity>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grant_vrf)
        validate = Validate(this)
        setLabelText()
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)

        btn_save.setOnClickListener {
            // var intent = Intent(this, GrantVrfActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        btn_cancel.setOnClickListener {
            var intent = Intent(this, VoMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            finish()
        }
        ic_Back.setOnClickListener {
            var intent = Intent(this, VoMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        fillSpinner()
    }

    private fun setLabelText() {
        tv_type_of_payee_source.text = LabelSet.getText(
            "type_of_payee_source",
            R.string.type_of_payee_source
        )
        tv_name.text = LabelSet.getText("name", R.string.name)
        et_name.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_receipt_type.text = LabelSet.getText(
            "receipt_type",
            R.string.receipt_type
        )
        tv_amount.text = LabelSet.getText(
            "amount",
            R.string.amount
        )
        et_amount.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_narration.text = LabelSet.getText(
            "narration",
            R.string.narration
        )
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
        tv_title.text = LabelSet.getText(
            "grant_vrf",
            R.string.grant_vrf
        )
    }

    private fun fillSpinner() {
        dataspin_type_of_payee_source = lookupViewmodel!!.getlookup(
            99,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_receipt_type = lookupViewmodel!!.getlookup(
            99,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_narration1 = lookupViewmodel!!.getlookup(
            99,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        validate!!.fillspinner(this, spin_type_of_payee_source, dataspin_type_of_payee_source)
        validate!!.fillspinner(this, spin_receipt_type, dataspin_receipt_type)
        validate!!.fillspinner(this, spin_narration1, dataspin_narration1)
    }


    override fun onClick(v: View?) {
        TODO("Not yet implemented")
    }
}