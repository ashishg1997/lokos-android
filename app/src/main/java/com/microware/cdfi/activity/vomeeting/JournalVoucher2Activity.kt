package com.microware.cdfi.activity.vomeeting

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.microware.cdfi.R
import com.microware.cdfi.fragment.JournalVoucherFragment
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.replaceFragmenty
import kotlinx.android.synthetic.main.activity_journal_voucher2.*

class JournalVoucher2Activity : AppCompatActivity() {
    var validate: Validate?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_journal_voucher2)

        replaceFragmenty(
            fragment = JournalVoucherFragment(),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        validate= Validate(this)
        et_date_of_transaction.setOnClickListener {
            validate!!.datePicker(et_date_of_transaction)
        }
    }
}