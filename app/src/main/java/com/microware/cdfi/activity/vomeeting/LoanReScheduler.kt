package com.microware.cdfi.activity.vomeeting

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.LoanReSchedulerAdapter
import com.microware.cdfi.fragment.OthersScreenTopBarFragment
import com.microware.cdfi.utility.replaceFragmenty
import kotlinx.android.synthetic.main.activity_loan_re_scheduler.*

class LoanReScheduler : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loan_re_scheduler)
        replaceFragmenty(
            fragment = OthersScreenTopBarFragment(29),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )
        loanReSchedulerRecycler()
    }
    private fun loanReSchedulerRecycler() {
        loan_rescheduler.layoutManager = LinearLayoutManager(this)
        loan_rescheduler.adapter = LoanReSchedulerAdapter(this)
    }
}