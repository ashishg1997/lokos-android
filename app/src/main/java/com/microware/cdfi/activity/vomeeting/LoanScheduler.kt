package com.microware.cdfi.activity.vomeeting

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.LoanSchedulerAdapter
import com.microware.cdfi.fragment.OthersScreenTopBarFragment
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.replaceFragmenty
import kotlinx.android.synthetic.main.loan_scheduler.*

class LoanScheduler : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.loan_scheduler)
        replaceFragmenty(
            fragment = OthersScreenTopBarFragment(28),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )
        setLabel()
        loanSchedulerRecycler()
    }

    private fun loanSchedulerRecycler() {
        loan_scheduler.layoutManager = LinearLayoutManager(this)
        loan_scheduler.adapter = LoanSchedulerAdapter(this)
    }

    fun setLabel(){
        tv_loan_no.text = LabelSet.getText("loan_no_", R.string.loan_no_)
        tv_loan_amount_rs.text = LabelSet.getText("loan_amount_rs", R.string.loan_amount_rs)
        tv_monthly_interest_rate_per.text = LabelSet.getText("monthly_interest_rate_per", R.string.monthly_interest_rate_per)
        tv_loan_duration_years.text = LabelSet.getText("loan_duration_years", R.string.loan_duration_years)
        tv_repayment_frequency.text = LabelSet.getText("repayment_frequency", R.string.repayment_frequency)
        tv_no_of_installment.text = LabelSet.getText("number_of_installments", R.string.number_of_installments)
        tv_principal_repayment.text = LabelSet.getText("principal_repayment", R.string.principal_repayment)
        tv_date_of_disbursement.text = LabelSet.getText("date_of_disbursement", R.string.date_of_disbursement)
        tv_loan_schedule.text = LabelSet.getText("loan_schedule", R.string.loan_schedule)
        tv_installment_date.text = LabelSet.getText("installment_date", R.string.installment_date)
        tv_principal_repayment1.text = LabelSet.getText("principal_repayment", R.string.principal_repayment)
        tv_interest.text = LabelSet.getText("interest", R.string.interest)
        tv_total.text = LabelSet.getText("total", R.string.total)
        tv_balance.text = LabelSet.getText("balance", R.string.balance)

    }

}