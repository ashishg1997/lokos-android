package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.LookupViewmodel
import kotlinx.android.synthetic.main.activity_loan_to_vo.*
import kotlinx.android.synthetic.main.buttons_vo.*
import kotlinx.android.synthetic.main.repay_toolbar_vomeeting.*

class LoantoVoActivity : AppCompatActivity() {

    var lookupViewmodel: LookupViewmodel? = null
    var validate: Validate? = null
    var dataspin_loan_product: List<LookupEntity>? = null
    var dataspin_loan_purpose: List<LookupEntity>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loan_to_vo)

        validate = Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        setLabelText()

        btn_cancel.setOnClickListener {
            var intent = Intent(this, VoMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        btn_save.setOnClickListener {
            var intent = Intent(this, CompulsoryfromShgActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        ic_Back.setOnClickListener {
            var intent = Intent(this, VoMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        fillSpinner()
    }

    override fun onBackPressed() {
        var intent = Intent(this, VoMeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0,0)
        finish()
    }

    fun setLabelText()
    {
        tv_source_of_loan.text = LabelSet.getText(
            "source_of_loan",
            R.string.source_of_loan
        )
        et_source_of_loan.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_name.text = LabelSet.getText("name", R.string.name)
        et_name.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_loan_type.text = LabelSet.getText(
            "fund_type",
            R.string.fund_type
        )
        et_loan_type.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_loan_product.text = LabelSet.getText(
            "loan_product_drop_down_list",
            R.string.loan_product_drop_down_list
        )
        tv_loan_purpose.text = LabelSet.getText(
            "loan_purpose",
            R.string.loan_purpose
        )
        tv_loan_no.text = LabelSet.getText(
            "loan_no",
            R.string.loan_no
        )
        et_loan_no.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_sanction_amount.text = LabelSet.getText(
            "sanction_amount",
            R.string.sanction_amount
        )
        et_sanction_amount.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_sanction_date.text = LabelSet.getText(
            "sanction_date",
            R.string.sanction_date
        )
        et_sanction_date.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )
        tv_total_amount_received.text = LabelSet.getText(
            "total_amount_received",
            R.string.total_amount_received
        )
        et_total_amount_received.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_interest_rate.text = LabelSet.getText(
            "interest_rate",
            R.string.interest_rate
        )
        et_interest_rate.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_loan_tenure.text = LabelSet.getText(
            "loan_tenure",
            R.string.loan_tenure
        )
        et_loan_tenure.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_no_of_installment_enter.text = LabelSet.getText(
            "no_of_installment_enter",
            R.string.no_of_installment_enter
        )
        et_no_of_installment_enter.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_repayment_frequency.text = LabelSet.getText(
            "repayment_frequency",
            R.string.repayment_frequency
        )
        et_repayment_frequency.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_emi_amount.text = LabelSet.getText(
            "emi_amount",
            R.string.emi_amount
        )
        et_emi_amount.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
       // tv_title.setText(LabelSet.getText("_5_2_loan_to_vo",R.string._5_2_loan_to_vo))

    }

    private fun fillSpinner() {
        dataspin_loan_product = lookupViewmodel!!.getlookup(
            99,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_loan_purpose = lookupViewmodel!!.getlookup(
            99,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        validate!!.fillspinner(this, spin_loan_product, dataspin_loan_product)
        validate!!.fillspinner(this, spin_loan_purpose, dataspin_loan_purpose)
    }
}