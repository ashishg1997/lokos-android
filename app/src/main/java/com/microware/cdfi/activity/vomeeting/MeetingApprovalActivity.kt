package com.microware.cdfi.activity.vomeeting

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProviders
import com.google.gson.JsonObject
import com.microware.cdfi.R
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.api.meetinguploadmodel.MeetingSummaryModel
import com.microware.cdfi.api.response.MeetingResponse
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.ResponseViewModel
import kotlinx.android.synthetic.main.activity_master_sync.*
import kotlinx.android.synthetic.main.activity_meeting_approval.*
import kotlinx.android.synthetic.main.customealertdialogelogin.*
import kotlinx.android.synthetic.main.customealertdialogelogin.view.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.btn_no
import kotlinx.android.synthetic.main.customealertdialogepartial.view.btn_yes
import kotlinx.android.synthetic.main.dialoge.view.*
import kotlinx.android.synthetic.main.dialoge.view.txt_msg
import kotlinx.android.synthetic.main.layout_meeting_rejection_remarks.view.*
import kotlinx.android.synthetic.main.meeting_detail_item.*
import org.apache.http.conn.ConnectTimeoutException
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException

class MeetingApprovalActivity : AppCompatActivity() {

    internal lateinit var progressDialog: ProgressDialog
    var  apiInterface: ApiInterface? = null
    var validate: Validate? = null
    var responseViewModel: ResponseViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_meeting_approval)

        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        responseViewModel = ViewModelProviders.of(this).get(ResponseViewModel::class.java)

        validate = Validate(this)
        relative.visibility = View.GONE
        tv_nam.text = validate!!.RetriveSharepreferenceString(AppSP.ShgName)
        tv_date.text = validate!!.convertDatetime(validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate))
        tv_mtgNum.text = validate!!.returnStringValue(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber).toString())
        tv_code.text =
            validate!!.returnStringValue(validate!!.RetriveSharepreferenceLong(MeetingSP.shgid).toString())
        importMeetingSummary()

        btn_reject.setOnClickListener {
            if (validate!!.isNetworkConnected(this)) {
                CustomAlertRejectionRemarks()
            } else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "no_internet_msg",
                        R.string.no_internet_msg
                    ), this
                )
            }

        }

        btn_approve.setOnClickListener {
            if (validate!!.isNetworkConnected(this)) {
                updateMeetingStatus(2,"")
            } else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "no_internet_msg",
                        R.string.no_internet_msg
                    ), this
                )
            }
        }
    }

    fun importMeetingSummary() {

        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText("DataLoading", R.string.DataLoading)
        )

        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
        val token = validate!!.returnStringValue(AESEncryption.decrypt(
            validate!!.RetriveSharepreferenceString(AppSP.token),
            validate!!.RetriveSharepreferenceString(AppSP.userid)
        ))
        val callCount = apiInterface?.getMeetingSummary(
            "application/json",
            token,
            user,
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber)
        )

        callCount?.enqueue(object : Callback<MeetingSummaryModel> {
            override fun onFailure(callCount: Call<MeetingSummaryModel>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<MeetingSummaryModel>,
                response: Response<MeetingSummaryModel>
            ) {
                progressDialog.dismiss()
                if (response.isSuccessful) {
                    try {
                        var list = response.body()

                        showSummaryData(response)

                        val text = LabelSet.getText(
                            "Datadownloadedsuccessfully",
                            R.string.Datadownloadedsuccessfully
                        )

                        Toast.makeText(this@MeetingApprovalActivity,text,Toast.LENGTH_LONG).show()

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        //                                    progressDialog.dismiss()
                    }

                } else {
                    progressDialog.dismiss()
                    var resCode = 0
                    var resMsg = ""
                    if (response.code() == 403) {

                        CustomAlertlogin()
                    } else {
                        if(response.errorBody()?.contentLength()==0L || response.errorBody()?.contentLength()!! <0L){
                            resCode = response.code()
                            resMsg = response.message()
                        }else {
                            var jsonObject1 =
                                JSONObject(response.errorBody()!!.source().readUtf8().toString())

                            resCode =
                                validate!!.returnIntegerValue(
                                    jsonObject1.optString("responseCode").toString()
                                )
                            resMsg = validate!!.returnStringValue(jsonObject1.optString("responseMsg").toString())
                        }

                        validate!!.CustomAlertMsg(
                            this@MeetingApprovalActivity,
                            responseViewModel,
                            resCode,
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,resMsg)

                    }

                }


            }

        })
    }

    fun showSummaryData( response: Response<MeetingSummaryModel>?){

        validate!!.SaveSharepreferenceInt(MeetingSP.uid,validate!!.returnIntegerValue(response?.body()?.uid.toString()))
        // Member Attendance
        var totalMember = ""
        if(validate!!.returnIntegerValue(response?.body()?.totalAttendance.toString())>0 && validate!!.returnIntegerValue(response?.body()?.memberCount.toString())>0){
            var member_Present = response?.body()?.totalAttendance.toString()
            if(response?.body()?.mtgType==11 || response?.body()?.mtgType==12){
                totalMember = (response.body()?.memberCount!! * response.body()?.mtgNo!!).toString()
            }else {
                totalMember = response?.body()?.memberCount.toString()
            }
            tv_attendence.text = member_Present + "/" + totalMember
        }

        // Member Savings
        var expectedMemberSaving = 0
        if(validate!!.returnIntegerValue(response?.body()?.savCompCb.toString())>0){
            var totalSAving = LabelSet.getText("",R.string.rs_sign) + validate!!.returnStringValue(response?.body()?.savCompCb.toString())
            if(response?.body()?.mtgType==11 || response?.body()?.mtgType==12) {
                expectedMemberSaving =
                    validate!!.returnIntegerValue(response.body()?.mnthCompSav.toString()) * validate!!.returnIntegerValue(
                        response.body()?.memberCount.toString()) * validate!!.returnIntegerValue(
                        response.body()?.mtgNo.toString())
            }else {
                expectedMemberSaving =
                    validate!!.returnIntegerValue(response?.body()?.mnthCompSav.toString()) * validate!!.returnIntegerValue(
                        response?.body()?.memberCount.toString()
                    )
            }
            var expectedSaving = LabelSet.getText("",R.string.rs_sign) + validate!!.returnStringValue(expectedMemberSaving.toString())
            tv_savingAmt.text = totalSAving + "/" + expectedSaving
            tv_savingcount.text = response?.body()?.totalAttendance.toString() + "/" + response?.body()?.memberCount.toString()
        }

        // Loan Repayment By Members
        if(validate!!.returnIntegerValue(response?.body()?.amtLoansRepaid.toString())>0 && validate!!.returnIntegerValue(response?.body()?.amtLoansDemand.toString())>0){
            var totalRepayment = LabelSet.getText("",R.string.rs_sign) + validate!!.returnStringValue(response?.body()?.amtLoansRepaid.toString())
            var totalDemand = LabelSet.getText("",R.string.rs_sign) + validate!!.returnStringValue(response?.body()?.amtLoansDemand.toString())
            tv_RepaymentAmt.text = totalRepayment + "/" + totalDemand
            tv_savingcount1.text = validate!!.returnStringValue(response?.body()?.noLoansRepaid.toString()) + "/" + validate!!.returnStringValue(response?.body()?.noLoansDisbursed.toString())
        }

        // Loan Disbursed To Members
        if(response?.body()?.mtgType==11 || response?.body()?.mtgType==12) {
            tvLoanDisbursedLabel.text = LabelSet.getText("total_loan_disbursed",R.string.total_loan_disbursed)
            if (validate!!.returnIntegerValue(response.body()?.amtLoansDisbursed.toString()) > 0) {
                var totalDisbursement = LabelSet.getText(
                    "",
                    R.string.rs_sign
                ) + validate!!.returnStringValue(response.body()?.amtLoansDisbursed.toString())

                tv_DisbursedAmt.text = totalDisbursement
                tv_savingcount2.text = validate!!.returnStringValue(response.body()?.noLoansDisbursed.toString())
                // + "/" + validate!!.returnStringValue(response?.body()?.noLoanR.toString()))
            }
        }else {
            tvLoanDisbursedLabel.text = LabelSet.getText("loan_disbursed_loan_request",R.string.loan_disbursed_loan_request)
            if (validate!!.returnIntegerValue(response?.body()?.amtLoansDisbursed.toString()) > 0 && validate!!.returnIntegerValue(
                    response?.body()?.amtLoansRequest.toString()
                ) > 0
            ) {
                var totalDisbursement = LabelSet.getText(
                    "",
                    R.string.rs_sign
                ) + validate!!.returnStringValue(response?.body()?.amtLoansDisbursed.toString())
                var totalLoanRequest = LabelSet.getText(
                    "",
                    R.string.rs_sign
                ) + validate!!.returnStringValue(response?.body()?.amtLoansRequest.toString())
                tv_DisbursedAmt.text = totalDisbursement + "/" + totalLoanRequest
                tv_savingcount2.text = validate!!.returnStringValue(response?.body()?.noLoansDisbursed.toString())
                // + "/" + validate!!.returnStringValue(response?.body()?.noLoanR.toString()))
            }
        }

        // Loan Received By SHG
        if(validate!!.returnIntegerValue(response?.body()?.amtLoansReceivedExt.toString())>0){
            var totalLoanReceived = LabelSet.getText("",R.string.rs_sign) + validate!!.returnStringValue(response?.body()?.amtLoansReceivedExt.toString())
            tv_amt.text = totalLoanReceived
            tv_loanreccount.text = validate!!.returnStringValue(response?.body()?.noLoansReceivedExt.toString())
        }

        // SHG Loan Repayment
        if(validate!!.returnIntegerValue(response?.body()?.amtLoansRepaidExt.toString())>0 && validate!!.returnIntegerValue(response?.body()?.amtLoansDemandExt.toString())>0){
            var totalExternalRepayment = LabelSet.getText("",R.string.rs_sign) + validate!!.returnStringValue(response?.body()?.amtLoansRepaidExt.toString())
            var totalExternalDemand = LabelSet.getText("",R.string.rs_sign) + validate!!.returnStringValue(response?.body()?.amtLoansDemandExt.toString())
            tv_LoanRepayAmt.text = totalExternalRepayment + "/" + totalExternalDemand
            var num_Loans_total_loans = validate!!.returnStringValue(response?.body()?.noLoansRepaidExt.toString()) + "/" + validate!!.returnStringValue(response?.body()?.noLoansReceivedExt.toString())
            tv_count3.text = num_Loans_total_loans
        }

        // Receipts & Income
        if(validate!!.returnIntegerValue(response?.body()?.otherReceipts.toString())>0 || validate!!.returnIntegerValue(response?.body()?.memOtherPayments.toString())>0){
            var total_Receipt_Income = validate!!.returnIntegerValue(response?.body()?.otherReceipts.toString()) + validate!!.returnIntegerValue(response?.body()?.memOtherPayments.toString())
            var totalReceipt = LabelSet.getText("",R.string.rs_sign) + validate!!.returnStringValue(total_Receipt_Income.toString())
            tv_receiptIncomeAmt.text = totalReceipt
        }

        // Payments & Expenditure
        if(validate!!.returnIntegerValue(response?.body()?.otherPayments.toString())>0 || validate!!.returnIntegerValue(response?.body()?.memOtherReceipts.toString())>0){
            var total_Payment_Expenditure = validate!!.returnIntegerValue(response?.body()?.otherPayments.toString()) + validate!!.returnIntegerValue(response?.body()?.memOtherReceipts.toString())
            var totalPayments = LabelSet.getText("",R.string.rs_sign) + validate!!.returnStringValue(total_Payment_Expenditure.toString())
            tv_paymentAmt.text = totalPayments
        }

        // Cash Balance
        if(validate!!.returnIntegerValue(response?.body()?.closingBalance.toString())>0){
            var totalCash = LabelSet.getText("",R.string.rs_sign) + validate!!.returnStringValue(response?.body()?.closingBalance.toString())
            tv_cashAmt.text = totalCash
        }

        // Bank Balance
        var totalBankBalance = 0
        if(!response?.body()?.shgFinanceTransactionList!!.isNullOrEmpty()) {
            for (i in 0 until response.body()?.shgFinanceTransactionList?.size!!){
                totalBankBalance = totalBankBalance + response.body()?.shgFinanceTransactionList?.get(i)?.closingBalance!!
            }
            tv_bankBalanceAmt.text = LabelSet.getText("",R.string.rs_sign) + totalBankBalance.toString()
            tv_count4.text = validate!!.returnStringValue(response.body()?.shgFinanceTransactionList?.size!!.toString())
        }
        if(validate!!.returnIntegerValue(response.body()?.amtLoansReceivedExt.toString())>0){
            var totalLoanReceived = LabelSet.getText("",R.string.rs_sign) + validate!!.returnStringValue(
                response.body()?.amtLoansReceivedExt.toString())
            tv_bankBalanceAmt.text = totalLoanReceived
            if(!response.body()?.shgFinanceTransactionList.isNullOrEmpty() && response.body()?.shgFinanceTransactionList?.size!! > 0) {

            }
        }


    }
    fun CustomAlertlogin() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogelogin, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mDialogView.etUsername.setText(validate!!.RetriveSharepreferenceString(AppSP.userid))
        mDialogView.etPassword.setText(validate!!.returnStringValue(AESEncryption.decrypt(
            validate!!.RetriveSharepreferenceString(AppSP.Password),
            validate!!.RetriveSharepreferenceString(AppSP.userid)
        )))
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_dialog_title2.text = LabelSet.getText("app_name",R.string.app_name)
        mDialogView.btn_login.text = LabelSet.getText("log_in",R.string.log_in)
        mDialogView.chk_showHide1.text = LabelSet.getText("show_password",R.string.show_password)
        mDialogView.btn_login.setOnClickListener {
            mAlertDialog.dismiss()
            importtoken(
                mDialogView.etUsername.text.toString(),
                validate!!.returnStringValue(
                    AESEncryption.encrypt(validate!!.returnStringValue(
                        mDialogView.etPassword.text.toString()),mDialogView.etUsername.text.toString()))
            )
        }
        mAlertDialog.chk_showHide1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                mAlertDialog.etPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                mAlertDialog.etPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
        }

    }

    fun importtoken(user: String, pass: String) {
        var grant_type = "password"
        var userId = user
        var password = pass
        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText("authenticate_user", R.string.authenticate_user)
        )
        val callCount = apiInterface?.gettokenforce(
            grant_type,
            userId,
            password, validate!!.RetriveSharepreferenceInt(AppSP.State_Selected), 1,
            validate!!.RetriveSharepreferenceString(AppSP.Roleid)!!
        )
        callCount?.enqueue(object : Callback<JsonObject> {
            override fun onFailure(callCount: Call<JsonObject>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                //       Log.e("", response!!.body().toString())
                progressDialog.dismiss()


                if (response.isSuccessful) {

                    var jsonObject1: JSONObject? = null
                    try {
                        jsonObject1 = JSONObject(response.body().toString())

                        val auth = jsonObject1.get("accessToken").toString()
                        if (auth.trim().length > 0) {
                            validate!!.SaveSharepreferenceString(
                                AppSP.token,
                                validate!!.returnStringValue(
                                    AESEncryption.encrypt(
                                        auth,validate!!.RetriveSharepreferenceString(AppSP.userid)))
                            )
                        }
                        validate!!.CustomAlert(
                            LabelSet.getText(
                                "userauthenticatedsuccessfully",
                                R.string.userauthenticatedsuccessfully
                            ), this@MeetingApprovalActivity
                        )
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        var jsonObject1 =
                            JSONObject(response.errorBody()!!.source().readUtf8().toString())

                        var resCode =
                            validate!!.returnIntegerValue(
                                jsonObject1.optString("responseCode").toString()
                            )
                        validate!!.CustomAlertMsg(
                            this@MeetingApprovalActivity,
                            responseViewModel,
                            resCode,
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                            validate!!.returnStringValue(
                                jsonObject1.optString("responseMsg").toString()
                            )
                        )
                    }

                } else {
                    var jsonObject1 =
                        JSONObject(response.errorBody()!!.source().readUtf8().toString())

                    var resCode =
                        validate!!.returnIntegerValue(
                            jsonObject1.optString("responseCode").toString()
                        )
                    validate!!.CustomAlertMsg(
                        this@MeetingApprovalActivity,
                        responseViewModel,
                        resCode,
                        validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                        validate!!.returnStringValue(
                            jsonObject1.optString("responseMsg").toString()
                        )
                    )
                }


            }

        })


    }

    fun CustomAlert(str: String) {
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.customealertdialoge, null)
        val mBuilder = android.app.AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str
        mDialogView.btn_ok.setOnClickListener {

            mAlertDialog.dismiss()
            var intent = Intent(this, PendingMeetingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)

        }
    }



    fun updateMeetingStatus(approvalStatus:Int,remarks:String) {

        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText("DataLoading", R.string.DataLoading)
        )

        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
        val token = validate!!.returnStringValue(AESEncryption.decrypt(
            validate!!.RetriveSharepreferenceString(AppSP.token),
            validate!!.RetriveSharepreferenceString(AppSP.userid)
        ))
        val callCount = apiInterface?.updateMeetingStatus(
            "application/json",
            user,
            token,
            validate!!.RetriveSharepreferenceInt(MeetingSP.uid),
            approvalStatus,
            remarks
        )

        callCount?.enqueue(object : Callback<MeetingResponse> {
            override fun onFailure(callCount: Call<MeetingResponse>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<MeetingResponse>,
                response: Response<MeetingResponse>
            ) {
                progressDialog.dismiss()
                if (response.isSuccessful) {
                    try {
                        var  msg = response.body()?.result

                        val text = LabelSet.getText(
                            "update_approval_status",
                            R.string.update_approval_status
                        )

                        CustomAlert(text)

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        //                                    progressDialog.dismiss()
                    }

                } else {
                    progressDialog.dismiss()
                    var resCode = 0
                    var resMsg = ""
                    if (response.code() == 403) {

                        CustomAlertlogin()
                    } else {
                        if(response.errorBody()?.contentLength()==0L || response.errorBody()?.contentLength()!! <0L){
                            resCode = response.code()
                            resMsg = response.message()
                        }else {
                            var jsonObject1 =
                                JSONObject(response.errorBody()!!.source().readUtf8().toString())

                            resCode =
                                validate!!.returnIntegerValue(
                                    jsonObject1.optString("responseCode").toString()
                                )
                            resMsg = validate!!.returnStringValue(jsonObject1.optString("responseMsg").toString())
                        }

                        validate!!.CustomAlertMsg(
                            this@MeetingApprovalActivity,
                            responseViewModel,
                            resCode,
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,resMsg)

                    }

                }


            }

        })
    }

    fun CustomAlertRejectionRemarks() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.layout_meeting_rejection_remarks, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        mDialogView.btn_yes.setOnClickListener {
            //            PartialSaveData()
            mAlertDialog.dismiss()
            if(validate!!.returnStringValue(mDialogView.et_Remarks.text.toString()).trim().length>0) {
                updateMeetingStatus(3,validate!!.returnStringValue(mDialogView.et_Remarks.text.toString()))

            }else {
                validate!!.CustomAlert(LabelSet.getText("valid_rejection_remarks",R.string.valid_rejection_remarks),this)
            }
        }
        mDialogView.btn_no.setOnClickListener {
            mAlertDialog.dismiss()

        }
    }

    override fun onBackPressed() {
        val i = Intent(this, PendingMeetingActivity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(i)
        finish()
    }

    fun updateMeetingApprovalStatus(approvalStatus:Int,remarks:String) {
        progressDialog = ProgressDialog.show(
            this, LabelSet.getText("app_name", R.string.app_name),
            LabelSet.getText(
                "DataLoading",
                R.string.DataLoading
            )
        )
        progressDialog.setIcon(this.getDrawable(R.drawable.appicon))
        var idownload = 0
        var msg = ""
        var code = 0
        object : Thread() {

            //@SuppressLint("NewApi")
            override fun run() {
                try {


                    val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                    val token = validate!!.returnStringValue(AESEncryption.decrypt(
                        validate!!.RetriveSharepreferenceString(AppSP.token),
                        validate!!.RetriveSharepreferenceString(AppSP.userid)
                    ))
                    val call1 = apiInterface?.updateMeetingStatus(
                        "application/json",
                        user,
                        token,
                        validate!!.RetriveSharepreferenceInt(MeetingSP.uid),
                        approvalStatus,
                        remarks
                    )
                        val res = call1?.execute()

                        if (res!!.isSuccessful) {
                            if (res.body()?.equals("Update Approval Status Successfully!")!!) {
                                try {

//                                    progressDialog.dismiss()
                                } catch (ex: Exception) {
                                    ex.printStackTrace()
//                                    progressDialog.dismiss()
                                }
                            }
                        } else {
                            msg = "" + res.code() + " " + res.message()
                            code = res.code()
                            idownload = 1
//                            val text = LabelSet.getText("lokos_core",R.string.lokos_core)R.string.data_saved_successfully)
//                            validate!!.CustomAlert( text,this@MasterSyncActivity)
                        }





                    runOnUiThread {
                        if (idownload == 0) {
                            progressDialog.dismiss()
                            val text = msg
                            validate!!.CustomAlert(text, this@MeetingApprovalActivity)



                        } else {
                            progressDialog.dismiss()
                            if (code == 403) {
                                Toast.makeText(this@MeetingApprovalActivity, msg, Toast.LENGTH_LONG)
                                    .show()
                                CustomAlertlogin()
                            } else {
                                validate!!.CustomAlert(msg, this@MeetingApprovalActivity)
                                Toast.makeText(this@MeetingApprovalActivity, msg, Toast.LENGTH_LONG)
                                    .show()
                            }
                        }

                    }

                } catch (bug: SocketTimeoutException) {
                    progressDialog.dismiss()
//                                Toast.makeText(activity!!, "Socket Timeout", Toast.LENGTH_LONG).show();
                } catch (bug: ConnectTimeoutException) {
                    progressDialog.dismiss()
                    // Toast.makeText(getApplicationContext(), "Connection Timeout", Toast.LENGTH_LONG).show();
                } catch (e: Exception) {
                    e.printStackTrace()
                    progressDialog.dismiss()
                }
            }

        }.start()
    }
}