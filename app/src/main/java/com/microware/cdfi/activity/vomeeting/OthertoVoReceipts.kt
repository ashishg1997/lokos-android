package com.microware.cdfi.activity.vomeeting

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.OthertoVOReceiptsAdapter
import com.microware.cdfi.adapter.vomeetingadapter.OthertoVOReceiptBankAdapter
import com.microware.cdfi.fragment.VoReceiptsTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.CboBankViewmodel
import com.microware.cdfi.viewModel.MasterBankViewmodel
import com.microware.cdfi.viewModel.MstCOAViewmodel
import kotlinx.android.synthetic.main.activity_otherto_vo_receipts.*
import kotlinx.android.synthetic.main.activity_otherto_vo_receipts.rvBankList
import kotlinx.android.synthetic.main.activity_otherto_vo_receipts.rvList
import kotlinx.android.synthetic.main.buttons_vo.*
import kotlinx.android.synthetic.main.buttons_vo.view.*

class OthertoVoReceipts : AppCompatActivity() {
    var mstCOAViewmodel: MstCOAViewmodel? = null
    var cboBankViewModel: CboBankViewmodel? = null
    var bankMasterViewModel: MasterBankViewmodel? = null
    var validate: Validate? = null
    var cboType = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otherto_vo_receipts)
        validate = Validate(this)
        setLabelText()
        if(validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType)==2){
            cboType = 2
        }else {
            cboType = 1
        }

        replaceFragmenty(
            fragment = VoReceiptsTopBarFragment(13),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        mstCOAViewmodel = ViewModelProviders.of(this).get(MstCOAViewmodel::class.java)
        cboBankViewModel = ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        bankMasterViewModel = ViewModelProviders.of(this).get(MasterBankViewmodel::class.java)


        btn_save.setOnClickListener {
            var intent = Intent(this, OthertoVoLoanReceipt::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        btn_cancel.setOnClickListener {
            var intent = Intent(this, VoMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        fillRecyclerView()
        fillRecyclerView1()

    }

    private fun fillRecyclerView() {
        var coaData = mstCOAViewmodel!!.getCoaSubHeadData(
            listOf<Int>(10), "OI", validate!!.RetriveSharepreferenceString(
                AppSP.Langaugecode
            )!!
        )
       /* val coaData = mstCOAViewmodel!!.getReceiptCoaSubHeadData(
            "OR",
            "OI",
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
        )*/
       if (!coaData.isNullOrEmpty()){
           rvList.layoutManager = LinearLayoutManager(this)
           rvList.adapter = OthertoVOReceiptsAdapter(this, coaData, cboBankViewModel, mstCOAViewmodel)
       }
    }

    private fun fillRecyclerView1() {
        val bankList = cboBankViewModel!!.getcboBankdata(validate!!.RetriveSharepreferenceString(
            VoSpData.voSHGGUID),cboType)
      if (!bankList.isNullOrEmpty()){
          rvBankList.layoutManager = LinearLayoutManager(this)
          rvBankList.adapter = OthertoVOReceiptBankAdapter(this,bankList,mstCOAViewmodel)
      }
    }

    fun getBankName(bankID : Int?): String?{
        var value: String? = null
        value = bankMasterViewModel!!.getBankName(bankID!!)
        return value
    }

    fun customReceiveDialog(){
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.receive_dialog, null)
        val mBuilder = AlertDialog.Builder(this).setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.WHITE))


        mDialogView.btn_save.setOnClickListener {
            mAlertDialog.dismiss()
        }

        mDialogView.btn_cancel.setOnClickListener {
            mAlertDialog.dismiss()
        }

    }

    private fun setLabelText() {
        tv_default_bank.text = LabelSet.getText("default_bank", R.string.default_bank)
        tv_funds.text = LabelSet.getText("funds", R.string.funds)
        tv_amount.text = LabelSet.getText("amount", R.string.amount)
        tv_other_payment.text = LabelSet.getText("other_payments", R.string.other_payments)
        tv_total.text = LabelSet.getText("total", R.string.total)
        tv_total_amount_to_be_received.text = LabelSet.getText("total_amount_to_be_received", R.string.total_amount_to_be_received)
        tv_total_amount_received_by_vo.text = LabelSet.getText("total_amount_received_by_vo", R.string.total_amount_received_by_vo)
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText("cancel", R.string.cancel)
    }

    override fun onBackPressed() {
        var intent = Intent(this, VoMeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
    }
}