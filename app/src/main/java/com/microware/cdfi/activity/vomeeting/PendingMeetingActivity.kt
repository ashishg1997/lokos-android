package com.microware.cdfi.activity.vomeeting

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.JsonObject
import com.microware.cdfi.R
import com.microware.cdfi.activity.vo.VoMyTaskActivity
import com.microware.cdfi.adapter.PendingMeetingAdapter
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.api.meetinguploadmodel.MeetingApprovalListModel
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.ResponseViewModel
import kotlinx.android.synthetic.main.activity_pending_meeting.*
import kotlinx.android.synthetic.main.customealertdialogelogin.*
import kotlinx.android.synthetic.main.customealertdialogelogin.view.*
import kotlinx.android.synthetic.main.dialoge.view.*
import kotlinx.android.synthetic.main.votoolbar.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PendingMeetingActivity : AppCompatActivity() {
    internal lateinit var progressDialog: ProgressDialog
    var  apiInterface: ApiInterface? = null
    var validate: Validate? = null
    var responseViewModel: ResponseViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pending_meeting)

        tv_title.text = LabelSet.getText("pending_n_meetings", R.string.pending_n_meetings)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        responseViewModel = ViewModelProviders.of(this).get(ResponseViewModel::class.java)

        validate = Validate(this)

        importMeetingApprovalList()
    }


    fun importMeetingApprovalList() {

        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText("DataLoading", R.string.DataLoading)
        )

        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
        val token = validate!!.returnStringValue(AESEncryption.decrypt(
            validate!!.RetriveSharepreferenceString(AppSP.token),
            validate!!.RetriveSharepreferenceString(AppSP.userid)
        ))
        val callCount = apiInterface?.getMeetingApprovalList(
            token,
            user
        )

        callCount?.enqueue(object : Callback<List<MeetingApprovalListModel>> {
            override fun onFailure(callCount: Call<List<MeetingApprovalListModel>>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<List<MeetingApprovalListModel>>,
                response: Response<List<MeetingApprovalListModel>>
            ) {
                progressDialog.dismiss()
                if (response.isSuccessful) {
                    if (!response.body()!!.isNullOrEmpty()) {
                        try {
                            var list = response.body()

                            fillApprovalRecycleView(list)
                            val text = LabelSet.getText(
                                "Datadownloadedsuccessfully",
                                R.string.Datadownloadedsuccessfully
                            )

                            Toast.makeText(this@PendingMeetingActivity,text,Toast.LENGTH_LONG).show()

                        } catch (ex: Exception) {
                            ex.printStackTrace()
                            //                                    progressDialog.dismiss()
                        }
                    }
                } else {
                    progressDialog.dismiss()
                    var resCode = 0
                    var resMsg = ""
                    if (response.code() == 403) {

                        CustomAlertlogin()
                    } else {
                        if(response.errorBody()?.contentLength()==0L || response.errorBody()?.contentLength()!! <0L){
                            resCode = response.code()
                            resMsg = response.message()
                        }else {
                            var jsonObject1 =
                                JSONObject(response.errorBody()!!.source().readUtf8().toString())

                            resCode =
                                validate!!.returnIntegerValue(
                                    jsonObject1.optString("responseCode").toString()
                                )
                            resMsg = validate!!.returnStringValue(jsonObject1.optString("responseMsg").toString())
                        }

                        validate!!.CustomAlertMsg(
                            this@PendingMeetingActivity,
                            responseViewModel,
                            resCode,
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,resMsg)

                    }

                }


            }

        })
    }

    fun fillApprovalRecycleView(approvalList:List<MeetingApprovalListModel>?){
        if (!approvalList.isNullOrEmpty()){
            rvList.layoutManager = LinearLayoutManager(this)
            rvList.adapter = PendingMeetingAdapter(this,approvalList)
        }
    }

    fun CustomAlertlogin() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogelogin, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mDialogView.etUsername.setText(validate!!.RetriveSharepreferenceString(AppSP.userid))
        mDialogView.etPassword.setText(validate!!.returnStringValue(AESEncryption.decrypt(
            validate!!.RetriveSharepreferenceString(AppSP.Password),
            validate!!.RetriveSharepreferenceString(AppSP.userid)
        )))
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_dialog_title2.text = LabelSet.getText("app_name",R.string.app_name)
        mDialogView.btn_login.text = LabelSet.getText("log_in",R.string.log_in)
        mDialogView.chk_showHide1.text = LabelSet.getText("show_password",R.string.show_password)
        mDialogView.btn_login.setOnClickListener {
            mAlertDialog.dismiss()
            importtoken(
                mDialogView.etUsername.text.toString(),
                validate!!.returnStringValue(
                    AESEncryption.encrypt(validate!!.returnStringValue(
                        mDialogView.etPassword.text.toString()),mDialogView.etUsername.text.toString()))
            )
        }
        mAlertDialog.chk_showHide1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                mAlertDialog.etPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                mAlertDialog.etPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
        }

    }

    fun importtoken(user: String, pass: String) {
        var grant_type = "password"
        var userId = user
        var password = pass
        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText("authenticate_user", R.string.authenticate_user)
        )
        val callCount = apiInterface?.gettokenforce(
            grant_type,
            userId,
            password, validate!!.RetriveSharepreferenceInt(AppSP.State_Selected), 1,
            validate!!.RetriveSharepreferenceString(AppSP.Roleid)!!
        )
        callCount?.enqueue(object : Callback<JsonObject> {
            override fun onFailure(callCount: Call<JsonObject>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                //       Log.e("", response!!.body().toString())
                progressDialog.dismiss()


                if (response.isSuccessful) {

                    var jsonObject1: JSONObject? = null
                    try {
                        jsonObject1 = JSONObject(response.body().toString())

                        val auth = jsonObject1.get("accessToken").toString()
                        if (auth.trim().length > 0) {
                            validate!!.SaveSharepreferenceString(
                                AppSP.token,
                                validate!!.returnStringValue(
                                    AESEncryption.encrypt(
                                        auth, validate!!.RetriveSharepreferenceString(AppSP.userid)))
                            )
                        }
                        validate!!.CustomAlert(
                            LabelSet.getText(
                                "userauthenticatedsuccessfully",
                                R.string.userauthenticatedsuccessfully
                            ), this@PendingMeetingActivity
                        )


                    } catch (e: JSONException) {
                        e.printStackTrace()
                        var jsonObject1 =
                            JSONObject(response.errorBody()!!.source().readUtf8().toString())

                        var resCode =
                            validate!!.returnIntegerValue(
                                jsonObject1.optString("responseCode").toString()
                            )
                        validate!!.CustomAlertMsg(
                            this@PendingMeetingActivity,
                            responseViewModel,
                            resCode,
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                            validate!!.returnStringValue(
                                jsonObject1.optString("responseMsg").toString()
                            )
                        )
                    }

                } else {
                    var jsonObject1 =
                        JSONObject(response.errorBody()!!.source().readUtf8().toString())

                    var resCode =
                        validate!!.returnIntegerValue(
                            jsonObject1.optString("responseCode").toString()
                        )
                    validate!!.CustomAlertMsg(
                        this@PendingMeetingActivity,
                        responseViewModel,
                        resCode,
                        validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                        validate!!.returnStringValue(
                            jsonObject1.optString("responseMsg").toString()
                        )
                    )
                }


            }

        })


    }

    fun CustomAlert(str: String) {
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.customealertdialoge, null)
        val mBuilder = android.app.AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str
        mDialogView.btn_ok.setOnClickListener {


            mAlertDialog.dismiss()


        }
    }

    override fun onBackPressed() {
        val i = Intent(this, VoMyTaskActivity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(i)
        finish()
    }
}