package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.LookupViewmodel
import kotlinx.android.synthetic.main.activity_receipt_entry_screen_1.*
import kotlinx.android.synthetic.main.buttons_vo.*
import kotlinx.android.synthetic.main.repay_toolbar_vomeeting.*

class ReceiptEntryScreen1Activity : AppCompatActivity(), View.OnClickListener {
    var lookupViewmodel: LookupViewmodel? = null
    var validate: Validate? = null
    var dataspin_ReceiptSource: List<LookupEntity>? = null
    var dataspin_MemberName: List<LookupEntity>? = null
    var dataspin_TypeofReceipt: List<LookupEntity>? = null
    var dataspin_BankName: List<LookupEntity>? = null
    var dataspin_Account_head: List<LookupEntity>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_receipt_entry_screen_1)

        validate = Validate(this)
        setLabelText()
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)

        btn_save.setOnClickListener {
            var intent = Intent(this, CifLoanRepaymentActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        btn_cancel.setOnClickListener {
            var intent = Intent(this, VoMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            finish()
        }
        ic_Back.setOnClickListener {
            var intent = Intent(this, VoMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        fillSpinner()
    }

    private fun setLabelText() {
        tv_ReceiptSource.text = LabelSet.getText(
            "source_of_receipt",
            R.string.source_of_receipt
        )
        tv_MemberName.text = LabelSet.getText(
            "name_of_shg_clf_member",
            R.string.name_of_shg_clf_member
        )
        tv_TypeofReceipt.text = LabelSet.getText(
            "type_of_receipt",
            R.string.type_of_receipt
        )
        tv_ModeofPayment.text = LabelSet.getText(
            "mode_of_payment",
            R.string.mode_of_payment
        )
        et_ModeofPayment.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_BankName.text = LabelSet.getText(
            "bank_branch_name",
            R.string.bank_branch_name
        )
        tv_Cheque_Account_no.text = LabelSet.getText(
            "cheque_challan_no_account_no",
            R.string.cheque_challan_no_account_no
        )
        et_Cheque_Account_no.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_Amount.text = LabelSet.getText(
            "amount",
            R.string.amount
        )
        et_Amount.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_Voucher_no.text = LabelSet.getText(
            "voucher_no",
            R.string.voucher_no
        )
        et_Voucher_no.hint = LabelSet.getText(
            "type_here",
            R.string.type_here
        )
        tv_Account_head.text = LabelSet.getText(
            "account_head_sub_head",
            R.string.account_head_sub_head
        )
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
        tv_title.text = LabelSet.getText(
            "receipt_entry_screen_1.xml",
            R.string.receipt_entry_screen_1
        )

    }

    private fun fillSpinner() {
        dataspin_ReceiptSource = lookupViewmodel!!.getlookup(
            99,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_MemberName = lookupViewmodel!!.getlookup(
            99,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_TypeofReceipt = lookupViewmodel!!.getlookup(
            99,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_BankName = lookupViewmodel!!.getlookup(
            99,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_Account_head = lookupViewmodel!!.getlookup(
            99,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        validate!!.fillspinner(this, spin_ReceiptSource, dataspin_ReceiptSource)
        validate!!.fillspinner(this, spin_MemberName, dataspin_MemberName)
        validate!!.fillspinner(this, spin_TypeofReceipt, dataspin_TypeofReceipt)
        validate!!.fillspinner(this, spin_BankName, dataspin_MemberName)
        validate!!.fillspinner(this, spin_Account_head, dataspin_Account_head)
    }

    override fun onClick(view: View?) {
        TODO("Not yet implemented")
    }
}

