package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.entity.MstCOAEntity
import com.microware.cdfi.entity.voentity.VoFinTxnDetGrpEntity
import com.microware.cdfi.fragment.OthersScreenTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.MstCOAViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoFinTxnDetGrpViewModel
import com.microware.cdfi.viewModel.voviewmodel.VoGenerateMeetingViewmodel
import kotlinx.android.synthetic.main.buttons_vo.*
import kotlinx.android.synthetic.main.receipt_and_income.*

class ReceiptsAndIncome : AppCompatActivity() {

    var validate: Validate? = null
    lateinit var lookupViewmodel: LookupViewmodel
    lateinit var mstCoaViewmodel: MstCOAViewmodel
    lateinit var voGenerateMeetingViewmodel: VoGenerateMeetingViewmodel
    lateinit var voFinTxnDetGrpViewModel: VoFinTxnDetGrpViewModel
    var dataspin_ReceiptType: List<MstCOAEntity>? = null
    var dataspin_receivedFrom: List<LookupEntity>? = null
    var dataspin_mode: List<LookupEntity>? = null
    var voBankList: List<Cbo_bankEntity>? = null
    var voFinTxnDetGrpEntity: VoFinTxnDetGrpEntity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.receipt_and_income)

        validate = Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        mstCoaViewmodel = ViewModelProviders.of(this).get(MstCOAViewmodel::class.java)
        voGenerateMeetingViewmodel =
            ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        voFinTxnDetGrpViewModel =
            ViewModelProviders.of(this).get(VoFinTxnDetGrpViewModel::class.java)

        replaceFragmenty(
            fragment = OthersScreenTopBarFragment(25),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                SaveData()
            }
        }

        btn_cancel.setOnClickListener {
            val intent = Intent(this, ReceiptsAndIncomeSummary::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        setLabelText()
        fillbank()
        fillSpinner()
        showData()
    }

    private fun setLabelText() {
        tv_received_from.text = LabelSet.getText(
            "received_from",
            R.string.received_from
        )
        tv_receipt_type.text = LabelSet.getText(
            "receipt_type",
            R.string.receipt_type
        )
        tv_name_of_shg_bank_others.text = LabelSet.getText(
            "name_of_shg_bank_others",
            R.string.name_of_shg_bank_others
        )
        tv_amount.text = LabelSet.getText(
            "amount",
            R.string.amount
        )
        tv_cheque_payslip_rtgs_neft_imps.text = LabelSet.getText(
            "cheque_payslip_rtgs_neft_imps",
            R.string.cheque_payslip_rtgs_neft_imps
        )
        tv_cheque_no_transactio_no.text = LabelSet.getText(
            "cheque_no_transactio_no",
            R.string.cheque_no_transactio_no
        )
        tv_name_of_bank_receiver.text = LabelSet.getText(
            "name_of_bank_receiver",
            R.string.name_of_bank_receiver
        )

        et_amount.hint = LabelSet.getText("type_here", R.string.type_here)
        et_cheque_no_transactio_no.hint = LabelSet.getText("type_here", R.string.type_here)

        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText("cancel", R.string.cancel)
    }

    private fun fillSpinner() {
        dataspin_receivedFrom = lookupViewmodel.getlookup(
            72, validate!!.RetriveSharepreferenceString(
                AppSP.Langaugecode
            )
        )
        dataspin_ReceiptType = mstCoaViewmodel.getMstCOAdata(
            "OR", validate!!.RetriveSharepreferenceString(
                AppSP.Langaugecode
            )!!
        )
        dataspin_mode = lookupViewmodel.getlookup(
            72, validate!!.RetriveSharepreferenceString(
                AppSP.Langaugecode
            )
        )
        validate!!.fillspinner(this, spin_received_from, dataspin_receivedFrom)
        validate!!.fillcoaspinner(this, spin_receipt_type, dataspin_ReceiptType)
        validate!!.fillspinner(this, spin_cheque_payslip_rtgs_neft_imps, dataspin_mode)
    }

    fun fillbank() {
        voBankList = voGenerateMeetingViewmodel.getBankdata(
            validate!!.RetriveSharepreferenceString(
                VoSpData.voSHGGUID
            )
        )

        val adapter: ArrayAdapter<String?>
        if (!voBankList.isNullOrEmpty()) {
            val isize = voBankList!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in voBankList!!.indices) {
                var lastthree =
                    voBankList!![i].account_no.substring(voBankList!![i].account_no.length - 3)
                sValue[i + 1] =
                    voBankList!![i].ifsc_code.toString().dropLast(7) + "XXXXX" + lastthree
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_name_of_bank_receiver.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_name_of_bank_receiver.adapter = adapter
        }

    }

    fun returaccount(): String {

        var pos = spin_name_of_bank_receiver.selectedItemPosition
        var id = ""

        if (!voBankList.isNullOrEmpty()) {
            if (pos > 0) id =
                voBankList!!.get(pos - 1).ifsc_code!!.dropLast(7) + voBankList!!.get(pos - 1).account_no
        }
        return id
    }

    fun setaccount(accountno: String): Int {

        var pos = 0

        if (!voBankList.isNullOrEmpty()) {
            for (i in voBankList!!.indices) {
                if (accountno.equals(
                        voBankList!!.get(i).ifsc_code!!.dropLast(7) + voBankList!!.get(
                            i
                        ).account_no
                    )
                )
                    pos = i + 1
            }
        }
        spin_name_of_bank_receiver.setSelection(pos)
        return pos
    }

    private fun checkValidation(): Int {
        var value = 1
        if (spin_received_from.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_received_from,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "received_from",
                    R.string.received_from
                )
            )
            value = 0
        } else if (spin_receipt_type.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_receipt_type,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "receipt_type",
                    R.string.receipt_type
                )
            )
            value = 0
        } /*else if (spin_name_of_shg_bank_others.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_name_of_shg_bank_others,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "name_of_shg_bank_others",
                    R.string.name_of_shg_bank_others
                )
            )
            value = 0
        }*/ else if (et_amount.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "amount",
                    R.string.amount
                ),
                this, et_amount
            )
            value = 0
        } else if (spin_cheque_payslip_rtgs_neft_imps.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_cheque_payslip_rtgs_neft_imps,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "cheque_payslip_rtgs_neft_imps",
                    R.string.cheque_payslip_rtgs_neft_imps
                )
            )
            value = 0
        } else if (et_cheque_no_transactio_no.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "cheque_no_transactio_no",
                    R.string.cheque_no_transactio_no
                ),
                this, et_cheque_no_transactio_no
            )
            value = 0
        } else if (spin_name_of_bank_receiver.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_name_of_bank_receiver,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "name_of_bank_receiver",
                    R.string.name_of_bank_receiver
                )
            )
            value = 0
        }

        return value
    }

    //1 field
    //name of shg/bank/other
    private fun SaveData() {

        var save = 0

        if (validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)
                .isNullOrEmpty() || validate!!.RetriveSharepreferenceInt(VoSpData.voAuid) == 0
        ) {
            voFinTxnDetGrpEntity = VoFinTxnDetGrpEntity(
                0,
                0,
                validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                validate!!.returncoauid(
                    spin_receipt_type,
                    dataspin_ReceiptType
                ),/*receipt goes here auid mstcoa*/
                1,/*Receipt type goes here fundtype*/
                0,
                validate!!.returnlookupcode(spin_received_from, dataspin_receivedFrom),
                "OI",
                validate!!.returnIntegerValue(et_amount.text.toString()),
                et_cheque_no_transactio_no.text.toString(),
                0,
                validate!!.returnlookupcode(
                    spin_cheque_payslip_rtgs_neft_imps,
                    dataspin_mode
                ),/*not sure about this field*/
                returaccount(),

                "",
                validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                "",
                0,
                "",
                0
            )
            voFinTxnDetGrpViewModel.insertVoGroupLoanSchedule(voFinTxnDetGrpEntity!!)
            save = 1
        } else {
            voFinTxnDetGrpViewModel.updateIncomeAndExpenditure(
                validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.returncoauid(
                    spin_receipt_type,
                    dataspin_ReceiptType
                ),/*receipt goes here auid mstcoa*/
                1,
                validate!!.returnlookupcode(spin_received_from, dataspin_receivedFrom),
                "OI",
                validate!!.returnIntegerValue(et_amount.text.toString()),
                0,
                validate!!.returnlookupcode(spin_cheque_payslip_rtgs_neft_imps, dataspin_mode),
                returaccount(),
                et_cheque_no_transactio_no.text.toString(),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime)
            )

            save = 2
        }

        if (save == 1) {
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this, ReceiptsAndIncomeSummary::class.java
            )
        } else {
            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_updated_successfully",
                    R.string.data_updated_successfully
                ),
                this, ReceiptsAndIncomeSummary::class.java
            )
        }


    }

    private fun showData() {
        val data = voFinTxnDetGrpViewModel.getIncomeAndExpendituredata(
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceInt(VoSpData.voAuid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vosavingSource)
        )
        if (!data.isNullOrEmpty()) {
            et_amount.setText(data[0].amount.toString())
            et_cheque_no_transactio_no.setText(data[0].transactionNo)

            spin_received_from.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(
                        data[0].amountToFrom.toString()
                    ), dataspin_receivedFrom
                )
            )
            spin_received_from.isEnabled = false
            spin_cheque_payslip_rtgs_neft_imps.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(
                        data[0].modePayment.toString()
                    ), dataspin_mode
                )
            )

            spin_receipt_type.setSelection(
                validate!!.returncoauidpos(
                    validate!!.returnIntegerValue(
                        data[0].auid.toString()
                    ), dataspin_ReceiptType
                )
            )
            setaccount(data[0].bankCode.toString())

            spin_receipt_type.isEnabled = false
        } else {
            spin_cheque_payslip_rtgs_neft_imps.setSelection(
                validate!!.returnlookupcodepos(
                    1, dataspin_mode
                )
            )
        }


    }

    override fun onBackPressed() {
        val intent = Intent(this, ReceiptsAndIncomeSummary::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }
}