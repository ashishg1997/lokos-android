package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.ReceiptAndIncomeSummaryAdapter
import com.microware.cdfi.entity.voentity.VoFinTxnDetGrpEntity
import com.microware.cdfi.fragment.OthersScreenTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.MstCOAViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoFinTxnDetGrpViewModel
import kotlinx.android.synthetic.main.receipts_and_income_summary.*

class ReceiptsAndIncomeSummary : AppCompatActivity() {

    var validate: Validate? = null
    var mstCoaViewmodel: MstCOAViewmodel? = null
    var lookupViewmodel: LookupViewmodel? = null
    lateinit var voFinTxnDetGrpViewModel: VoFinTxnDetGrpViewModel
    var Todayvalue = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.receipts_and_income_summary)

        validate = Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        mstCoaViewmodel = ViewModelProviders.of(this).get(MstCOAViewmodel::class.java)
        voFinTxnDetGrpViewModel =
            ViewModelProviders.of(this).get(VoFinTxnDetGrpViewModel::class.java)

        replaceFragmenty(
            fragment = OthersScreenTopBarFragment(26),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )
        ivAdd.setOnClickListener {

            validate!!.SaveSharepreferenceInt(VoSpData.voAuid, 0)

            validate!!.SaveSharepreferenceInt(VoSpData.vosavingSource, 0)

            val intent = Intent(this, ReceiptsAndIncome::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        setLabelText()
        receiptAndIncomeSummaryRecycler()
    }

    private fun setLabelText() {
        tv_particulars.text = LabelSet.getText(
            "particulars",
            R.string.particulars
        )
        tv_amount.text = LabelSet.getText(
            "amount",
            R.string.amount
        )
        tvtotal.text = LabelSet.getText("total", R.string.total)

    }

    fun getTotalValue() {
        var iValue1 = 0
        val iCount = recipt_income_summary.childCount
        for (i in 0 until iCount) {
            val gridChild = recipt_income_summary.getChildAt(i) as? ViewGroup
            val tv_value1 = gridChild!!.findViewById<View>(R.id.tv_amount) as? TextView

            if (!tv_value1!!.text.toString().isNullOrEmpty()) {
                iValue1 += validate!!.returnIntegerValue(tv_value1.text.toString())
            }
        }

        tv_Amount1.text = iValue1.toString()

    }

    fun getTotalValue1(iValue: Int) {
        Todayvalue += iValue
        tv_Amount1.text = Todayvalue.toString()
    }

    fun getRecepitType(keyCode: Int?): String? {
        var name: String? = null
        name = mstCoaViewmodel!!.getcoaValue(
            "OR",
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),
            keyCode
        )
        return name
    }

    private fun receiptAndIncomeSummaryRecycler() {
        Todayvalue = 0

        voFinTxnDetGrpViewModel.getVoFinTxnDetGrpData(
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            1
        )!!.observe(this, object : Observer<List<VoFinTxnDetGrpEntity>?> {
            override fun onChanged(income_list: List<VoFinTxnDetGrpEntity>?) {
                if (!income_list.isNullOrEmpty()) {
                    recipt_income_summary.layoutManager =
                        LinearLayoutManager(this@ReceiptsAndIncomeSummary)
                    val receiptAndIncomeSummaryAdapter = ReceiptAndIncomeSummaryAdapter(
                        this@ReceiptsAndIncomeSummary,
                        income_list
                    )
                    val isize: Int
                    isize = income_list.size
                    val params: ViewGroup.LayoutParams = recipt_income_summary.layoutParams
                    val r = resources
                    val px = TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_PX,
                        resources.getDimension(R.dimen.spraying),
                        r.displayMetrics
                    )
                    val hi = Math.round(px)
                    val gridHeight = hi * isize
                    params.height = gridHeight
                    recipt_income_summary.layoutParams = params
                    recipt_income_summary.adapter = receiptAndIncomeSummaryAdapter

                }

            }
        })
    }

    override fun onBackPressed() {
        val intent = Intent(this, VoMeetingMenuActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

}