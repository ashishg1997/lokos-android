package com.microware.cdfi.activity.vomeeting

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.microware.cdfi.R
import com.microware.cdfi.fragment.SHGmeetingApprovalTopBarFragment
import com.microware.cdfi.utility.replaceFragmenty

class SHGMeetingApproval : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shgmeeting_approval)
        replaceFragmenty(
            fragment = SHGmeetingApprovalTopBarFragment(2),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )
    }
}