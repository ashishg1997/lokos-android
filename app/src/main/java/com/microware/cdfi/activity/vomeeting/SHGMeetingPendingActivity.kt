package com.microware.cdfi.activity.vomeeting

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.SHGPendingMeetingAdapter
import com.microware.cdfi.fragment.SHGmeetingApprovalTopBarFragment
import com.microware.cdfi.utility.replaceFragmenty
import kotlinx.android.synthetic.main.activity_shgmeeting_pending.*

class SHGMeetingPendingActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shgmeeting_pending)

        replaceFragmenty(
            fragment = SHGmeetingApprovalTopBarFragment(1),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )
          fillRecyclerDataList()
    }

    private fun fillRecyclerDataList() {
        shg_pending_list.layoutManager=LinearLayoutManager(this)
        shg_pending_list.adapter= SHGPendingMeetingAdapter(this)
    }
}