package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.SHGVoShgTransactionSummaryAdapter
import com.microware.cdfi.entity.voentity.VoMtgDetEntity
import com.microware.cdfi.fragment.OthersScreenTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.FundTypeViewmodel
import com.microware.cdfi.viewModel.voviewmodel.*
import kotlinx.android.synthetic.main.transaction_summary.*

class SHGVoSHGTransactionSummary : AppCompatActivity() {

    var validate: Validate? = null
    var memberlist: List<VoMtgDetEntity>? = null
    lateinit var voMtgDetViewModel: VoMtgDetViewModel
    lateinit var fundTypeViewmodel: FundTypeViewmodel
    lateinit var voMemLoanViewModel: VoMemLoanViewModel
    lateinit var voMemLoanTxnViewModel: VoMemLoanTxnViewModel
    lateinit var voFinTxnDetMemViewModel: VoFinTxnDetMemViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.transaction_summary)

        validate = Validate(this)
        voMtgDetViewModel = ViewModelProviders.of(this).get(VoMtgDetViewModel::class.java)
        fundTypeViewmodel = ViewModelProviders.of(this).get(FundTypeViewmodel::class.java)
        voMemLoanViewModel = ViewModelProviders.of(this).get(VoMemLoanViewModel::class.java)
        voMemLoanTxnViewModel = ViewModelProviders.of(this).get(VoMemLoanTxnViewModel::class.java)
        voFinTxnDetMemViewModel =
            ViewModelProviders.of(this).get(VoFinTxnDetMemViewModel::class.java)

        replaceFragmenty(
            fragment = OthersScreenTopBarFragment(19),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        spin_shg_name?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                if (position > 0) {
                    val id = returnmemid()
                    setCompuslorySaving(id)
                    loanRepayment(id)
                    fillRecylcer(id)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }

        tv_Adjustment.setOnClickListener {
            val intent = Intent(this, Adjustment::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        fillmemberspinner()
        setLabelText()

    }

    fun setLabelText() {
        tv_receipt_from_shg.text = LabelSet.getText(
            "receipt_from_shg",
            R.string.receipt_from_shg
        )
        tv_Adjustment.text = LabelSet.getText(
            "adjustment",
            R.string.adjustment
        )
        tv_compulasory_saving.text = LabelSet.getText(
            "compulsory_saving",
            R.string.compulsory_saving
        )
        tv_today_com.text = LabelSet.getText(
            "today",
            R.string.today
        )
        tv_cum_com.text = LabelSet.getText("cum", R.string.cum)
        tv_voluntary_saving.text = LabelSet.getText(
            "voluntary_saving",
            R.string.voluntary_saving
        )
        tv_total_vol.text = LabelSet.getText(
            "today",
            R.string.today
        )
        tv_cum_vol.text = LabelSet.getText("cum", R.string.cum)
        tv_total_loan_repayment.text = LabelSet.getText(
            "loan_repayment",
            R.string.loan_repayment
        )
        tv_total_tot.text = LabelSet.getText(
            "principal",
            R.string.principal
        )
        tv_cum_tot.text = LabelSet.getText("cum", R.string.cum)
        tv_interest_tot.text = LabelSet.getText(
            "interest",
            R.string.interest
        )
        tv_penalty.text = LabelSet.getText(
            "other_penalty",
            R.string.other_penalty
        )
        tv_today_penalty.text = LabelSet.getText(
            "penalty",
            R.string.penalty
        )
        tv_paid_to_member.text = LabelSet.getText(
            "paid_to_shg",
            R.string.paid_to_shg
        )
        tv_withdrawl.text = LabelSet.getText(
            "withdrawal_vol_saving",
            R.string.withdrawal_vol_saving
        )

    }

    private fun fillmemberspinner() {
        memberlist = voMtgDetViewModel.getListDataMtgByMtgnum(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        val adapter: ArrayAdapter<String?>
        if (!memberlist.isNullOrEmpty()) {
            val isize = memberlist!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in memberlist!!.indices) {
                sValue[i + 1] = memberlist!![i].childCboName
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_shg_name.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(memberlist!!.size + 1)
            sValue[0] = LabelSet.getText(
                "nomemberavailable",
                R.string.nomemberavailable
            )
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_shg_name.adapter = adapter
        }
    }

    private fun setMember(): Int {
        var pos = 0
        if (!memberlist.isNullOrEmpty()) {
            for (i in memberlist!!.indices) {
                if (validate!!.RetriveSharepreferenceLong(VoSpData.voShgMemID) == memberlist!!.get(i).memId)
                    pos = i + 1
            }
        }
        spin_shg_name.setSelection(pos)
//        spin_shg_name.isEnabled = false
        return pos
    }

    private fun returnmemid(): Long {

        val pos = spin_shg_name.selectedItemPosition
        var id = 0L

        if (!memberlist.isNullOrEmpty()) {
            if (pos > 0) id = memberlist!!.get(pos - 1).memId
        }
        return id
    }

    // Today value of compulsory and voluntary left
    private fun setCompuslorySaving(id: Long) {
        val memdata = voMtgDetViewModel.getmemdata(
            id,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
        )
        if (!memdata.isNullOrEmpty()) {
            var compulsorySaving =
                validate!!.returnIntegerValue(memdata.get(0).savComp.toString())
            var totcompulsorySaving =
                validate!!.returnIntegerValue(memdata.get(0).savCompCb.toString())
            tv_today_com_amount.text = compulsorySaving.toString()
            tv_cum_com_amount.text = totcompulsorySaving.toString()
            val voluntorySaving = validate!!.returnIntegerValue(memdata.get(0).savVol.toString())
            val totvoluntorySaving = validate!!.returnIntegerValue(memdata.get(0).savVolCb.toString())
            tv_vol_amt.text = voluntorySaving.toString()
            tv_cum_amt.text = totvoluntorySaving.toString()


            val penalty = voFinTxnDetMemViewModel.getAmountByAuid(
                35,//need to change
                id,
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
            )
            if (penalty > 0) {
                tv_today_penalty_amt.text = penalty.toString()
            } else {
                tv_today_penalty_amt.text = "0"
            }

            val withdrawal =
                validate!!.returnIntegerValue(memdata.get(0).savVolWithdrawal.toString())

            tv_vl_withdrawl_amount.text = withdrawal.toString()
        } else {
            tv_today_com_amount.text = "0"
            tv_vol_amt.text = "0"
            tv_today_penalty_amt.text = "0"
            tv_vl_withdrawl_amount.text = "0"
        }
    }

    private fun loanRepayment(id: Long) {
        val loanPaid = voMemLoanTxnViewModel.getMemberLoanRepaymentAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            id
        )

        val loanInterest = voMemLoanTxnViewModel.getMemberLoanRepaymentIntAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            id
        )

        val com = (loanPaid + loanInterest)
        tv_loan_repay_amt.text = validate!!.returnStringValue(loanPaid.toString())
        tv_int_repay_amt.text = validate!!.returnStringValue(loanInterest.toString())
        tv_cum_repay_amt.text = validate!!.returnStringValue(com.toString())

    }

    private fun fillRecylcer(id: Long) {
        var list = voMemLoanViewModel.getloanListSummeryData(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            id
        )

        rvList.layoutManager = LinearLayoutManager(this)
        val transactionSummaryAdapter = SHGVoShgTransactionSummaryAdapter(this, list)
        rvList.adapter = transactionSummaryAdapter
    }

    fun returnFundName(uid: Int): String {
        var fundName = fundTypeViewmodel.getFundName(
            uid,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
        )!!
        return fundName
    }

    override fun onBackPressed() {
        val intent = Intent(this, VoMeetingMenuActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }
}