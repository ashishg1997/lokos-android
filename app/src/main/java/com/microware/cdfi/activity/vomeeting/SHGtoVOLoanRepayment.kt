package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.SHGtoVOLoanRepaymentAdapter
import com.microware.cdfi.adapter.vomeetingadapter.VoRepaymentDetailAdapter
import com.microware.cdfi.api.vomodel.VoLoanRepaymentListModel
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.entity.voentity.VoMemLoanScheduleEntity
import com.microware.cdfi.fragment.VoReceiptsTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import com.microware.cdfi.viewModel.voviewmodel.VoGenerateMeetingViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoMemLoanScheduleViewModel
import com.microware.cdfi.viewModel.voviewmodel.VoMemLoanTxnViewModel
import com.microware.cdfi.viewModel.voviewmodel.VoMemLoanViewModel
import kotlinx.android.synthetic.main.activity_shg_to_vo_loan_repayment.*
import kotlinx.android.synthetic.main.bankdialoge.view.*
import kotlinx.android.synthetic.main.buttons.view.*

class SHGtoVOLoanRepayment : AppCompatActivity() {

    var validate: Validate? = null
    var mstVOCoaViewmodel: MstVOCOAViewmodel? = null
    var mappedshgViewmodel: MappedShgViewmodel? = null
    var mappedVoViewmodel: MappedVoViewmodel? = null
    lateinit var generateMeetingViewmodel: VoGenerateMeetingViewmodel
    lateinit var voMemLoanTxnViewModel: VoMemLoanTxnViewModel
    lateinit var voMemLoanScheduleViewModel: VoMemLoanScheduleViewModel
    lateinit var voMemLoanViewModel:VoMemLoanViewModel
    var cboBankViewModel: CboBankViewmodel? = null
    var shgBankList: List<Cbo_bankEntity>? = null
    var demand = 0
    var intdemand = 0
    var totdemand = 0
    var totpaid = 0
    var cboType = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shg_to_vo_loan_repayment)

        validate = Validate(this)

        mstVOCoaViewmodel = ViewModelProviders.of(this).get(MstVOCOAViewmodel::class.java)
        mappedshgViewmodel = ViewModelProviders.of(this).get(MappedShgViewmodel::class.java)
        mappedVoViewmodel = ViewModelProviders.of(this).get(MappedVoViewmodel::class.java)
        generateMeetingViewmodel = ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        voMemLoanTxnViewModel = ViewModelProviders.of(this).get(VoMemLoanTxnViewModel::class.java)
        voMemLoanScheduleViewModel = ViewModelProviders.of(this).get(VoMemLoanScheduleViewModel::class.java)
        voMemLoanViewModel = ViewModelProviders.of(this).get(VoMemLoanViewModel::class.java)
        cboBankViewModel = ViewModelProviders.of(this).get(CboBankViewmodel::class.java)

        if (validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType) == 2) {
            cboType = 2
        } else {
            cboType = 1
        }


        val name  = getRecepitType(validate!!.RetriveSharepreferenceInt(VoSpData.voAuid))
        tv_fund.text = name

        replaceFragmenty(
            fragment = VoReceiptsTopBarFragment(2),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        btn_cancel.setOnClickListener {
            val intent = Intent(this, LoanDueStatusSHGtoVOActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        btn_pay.setOnClickListener {
            if (validate!!.returnIntegerValue(tv_total_pay.text.toString()) > 0) {
                if (chksaveloanrepayment() == 0) {
                    saveloanrepayment()
                } else {
                    validate!!.CustomAlertVO(LabelSet.getText("please_enter",R.string.please_enter)+LabelSet.getText("cheque_no_transactio_no",R.string.cheque_no_transactio_no), this)

                }
            } else {
                validate!!.CustomAlertVO(LabelSet.getText("amountalreadypaid",R.string.amountalreadypaid), this)

            }
        }

        setLabelText()
        fillLoanRecyclerView()
        fillRecyclerView()

    }
    fun chksaveloanrepayment(): Int {
        var saveValue = 0
        if (validate!!.RetriveSharepreferenceInt(VoSpData.voModePayment) > 1) {
            val iCount = rvList.childCount

            for (i in 0 until iCount) {
                val gridChild =
                    rvList.getChildAt(i) as? ViewGroup

                val tv_loan_no1 = gridChild!!
                    .findViewById<View>(R.id.tv_loan_no) as? TextView

                val tvModePayment = gridChild
                    .findViewById<View>(R.id.tvModePayment) as? TextView
                val tvaccountno = gridChild
                    .findViewById<View>(R.id.tvaccountno) as? TextView
                val tvtransactionno = gridChild
                    .findViewById<View>(R.id.tvtransactionno) as? TextView

                val total_due1 = gridChild
                    .findViewById<View>(R.id.et_principal_demand) as? EditText

                if (validate!!.returnIntegerValue(total_due1!!.text.toString()) > 0) {

                    var accountno = validate!!.returnStringValue(tvaccountno!!.text.toString())
                    var transaction =
                        validate!!.returnStringValue(tvtransactionno!!.text.toString())


                    if (accountno.equals("") || transaction.equals("")) {
                        saveValue = 1
                        break
                    }


                }
            }
        }
        return saveValue

    }
    fun saveloanrepayment() {
        var saveValue = 0

        val iCount = rvList.childCount

        for (i in 0 until iCount) {
            val gridChild =
                rvList.getChildAt(i) as? ViewGroup

            val tv_loan_no1 = gridChild!!
                .findViewById<View>(R.id.tv_loan_no) as? TextView

            val tvModePayment = gridChild
                .findViewById<View>(R.id.tvModePayment) as? TextView
            val tvaccountno = gridChild
                .findViewById<View>(R.id.tvaccountno) as? TextView
            val tvtransactionno = gridChild
                .findViewById<View>(R.id.tvtransactionno) as? TextView

            val total_due1 = gridChild
                .findViewById<View>(R.id.et_principal_demand) as? EditText

            if (validate!!.returnIntegerValue(total_due1!!.text.toString()) > 0) {
                var loanno = validate!!.returnIntegerValue(tv_loan_no1!!.text.toString())
                var memid = validate!!.RetriveSharepreferenceLong(VoSpData.voShgMemID)
                var paid = validate!!.returnIntegerValue(total_due1.text.toString())
                var mode= validate!!.RetriveSharepreferenceInt(VoSpData.voModePayment)
                var accountno=validate!!.returnStringValue(tvaccountno!!.text.toString())
                var transaction=validate!!.returnStringValue(tvtransactionno!!.text.toString())

                saveValue = saveData(loanno, memid, paid,mode,accountno,transaction)

            }
        }

        if (saveValue > 0) {
            fillRecyclerView()
            validate!!.CustomAlertVO(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this
            )
        }
    }

    fun saveData(
        loanno: Int,
        memid: Long,
        paid: Int,
        mode: Int,
        accountno: String,
        transaction: String): Int {
        var value = 0
        var listdata = voMemLoanTxnViewModel.getmemberloandata(
            loanno,
            validate!!.RetriveSharepreferenceInt(
                VoSpData.vocurrentmeetingnumber
            ),
            memid
        )
        var amt = paid
        var intrest = 0
        var loanos = 0
        var principaldemandcl = 0
        if (!listdata.isNullOrEmpty()) {
            loanos =
                validate!!.returnIntegerValue(listdata.get(0).loanOp.toString())

            principaldemandcl =
                validate!!.returnIntegerValue(listdata.get(0).principalDemandOb.toString()) + validate!!.returnIntegerValue(
                    listdata.get(0).principalDemand.toString()
                )
            intrest =
                validate!!.returnIntegerValue(listdata.get(0).intAccruedOp.toString()) + validate!!.returnIntegerValue(
                    listdata.get(0).intAccrued.toString()
                )
            if (amt > intrest) {
                amt = amt - intrest
            } else {
                intrest = intrest - amt
                amt = 0
            }

        }
        if (principaldemandcl > amt) {
            principaldemandcl=principaldemandcl-amt
        } else {
            principaldemandcl=0
        }
        var completionflag=false
        if (amt>=loanos){
            completionflag=true
        }
        voMemLoanTxnViewModel.updateloanpaid(
            loanno,
            validate!!.RetriveSharepreferenceInt(
                VoSpData.vocurrentmeetingnumber
            ),
            memid,
            amt, intrest,
            mode, accountno, transaction, principaldemandcl, completionflag
        )
        voMemLoanViewModel.updateMemberLoanEditFlag( validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),memid,loanno,completionflag)

        updateschedule(loanno, memid, amt)
        value = 1

        return value
    }

    fun updateschedule(loanno: Int, memid: Long, paid: Int): Int {
        var value = 0
        var amt = paid
        voMemLoanScheduleViewModel.deleterepaid(
            loanno,
            memid,
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
        )
        voMemLoanScheduleViewModel.deletesubinstallment(
            loanno,
            memid,
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
        )
        var list = voMemLoanScheduleViewModel.getMemberScheduleloanwise(memid, loanno,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber))

        for (i in list!!.indices) {
            if (amt > 0) {
                if (validate!!.returnIntegerValue(list.get(i).principalDemand.toString()) <= amt) {
                    voMemLoanScheduleViewModel.updaterepaid(
                        loanno,
                        memid,
                        list.get(i).installmentNo,
                        list.get(i).subInstallmentNo,
                        validate!!.returnIntegerValue(list.get(i).principalDemand.toString()),
                        validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                        validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime)
                    )
                    amt =
                        amt - validate!!.returnIntegerValue(list.get(i).principalDemand.toString())
                } else {
                    var remainingamt=validate!!.returnIntegerValue(list.get(i).principalDemand.toString()) - amt
                    voMemLoanScheduleViewModel.updaterepaid(
                        loanno,
                        memid,
                        list.get(i).installmentNo,
                        list.get(i).subInstallmentNo,
                        amt,
                        validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                        validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber) ,validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime)
                    )
                    var voMemLoanScheduleEntity = VoMemLoanScheduleEntity(
                        0,
                        validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                        memid,
                        validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
                        validate!!.returnIntegerValue(list.get(i).loanNo.toString()),
                        remainingamt,
                        remainingamt,
                        (validate!!.returnIntegerValue(list.get(i).loanOs.toString())), 0,
                        validate!!.returnIntegerValue(list.get(i).installmentNo.toString()),
                        validate!!.returnIntegerValue(list.get(i).subInstallmentNo.toString())+1,
                        validate!!.returnLongValue(list.get(i).installmentDate.toString()),
                        //   validate!!.Daybetweentime(validate!!.currentdatetime),
                        false,
                        0,
                        validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),

                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime), null, null,
                        "", 0


                    )
                    voMemLoanScheduleViewModel.insertVoMemLoanSchedule(voMemLoanScheduleEntity)
                    amt = 0
                    break
                }
            }else{
                break
            }
        }
        value = 1

        return value
    }

    fun fillLoanRecyclerView(){
        var list = generateMeetingViewmodel.getloanrepaymentList(
            validate!!.RetriveSharepreferenceInt(
                VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voShgMemID)
        )

        var repaymentDetailAdapter = VoRepaymentDetailAdapter(this, list)

        rvLoanList.layoutManager = LinearLayoutManager(this)

        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = rvLoanList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvLoanList.layoutParams = params
        rvLoanList.adapter = repaymentDetailAdapter

    }

    private fun fillRecyclerView() {
        demand = 0
        intdemand = 0
        totdemand = 0
        totpaid = 0
        var list = voMemLoanTxnViewModel.getmemberloantxnlist(
            validate!!.RetriveSharepreferenceLong(VoSpData.voShgMemID),
            validate!!.RetriveSharepreferenceInt(
                VoSpData.vocurrentmeetingnumber
            ),validate!!.RetriveSharepreferenceInt(
                VoSpData.ShortDescription
            )
        )
        var shgtoVOLoanRepaymentAdapter = SHGtoVOLoanRepaymentAdapter(this, list)

        rvList.layoutManager = LinearLayoutManager(this)

        val isize: Int
        isize = list!!.size
        val params: ViewGroup.LayoutParams = rvList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvList.layoutParams = params
        rvList.adapter = shgtoVOLoanRepaymentAdapter
        tv_total_loan.text = isize.toString()

    }

    private fun setLabelText() {
        var shgName = getShgName()
        tvshg_name.text = shgName

        tv_loans.text = LabelSet.getText("Loans", R.string.Loans)
        tv_total_payable.text = LabelSet.getText("today_s_payable", R.string.today_s_payable)
        tv_paid.text = LabelSet.getText("paid_r", R.string.paid_r)
        tv_next_payable.text = LabelSet.getText("next_payable_amount", R.string.next_payable_amount)
        tv_total_repaid.text = LabelSet.getText("total_repaid", R.string.total_repaid)
        tv_loan_no.text = LabelSet.getText("loan_no", R.string.loan_no)
        tv_principal_demand.text = LabelSet.getText("principal_demand", R.string.principal_demand)
        tv_interest_demand.text = LabelSet.getText("interest_demand", R.string.interest_demand)
        tv_total.text = LabelSet.getText("total", R.string.total)
        tv_pay.text = LabelSet.getText("pay", R.string.pay)
        tv_balance.text = LabelSet.getText("balance", R.string.balance)

        btn_pay.text = LabelSet.getText("pay", R.string.pay)
        btn_cancel.text = LabelSet.getText("cancel", R.string.cancel)

    }

    private fun getShgName(): String? {
        var shgName =
            mappedshgViewmodel!!.getShgName(
                validate!!.RetriveSharepreferenceLong(VoSpData.voShgMemID),
                validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID)!!
            )
        return shgName
    }

    fun getRecepitType(keyCode: Int?): String? {
        var name: String? = null
        name = mstVOCoaViewmodel!!.getcoaValue(
            "RL",
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),
            keyCode
        )
        return name
    }

    fun returnlist(memberid: Long): List<VoLoanRepaymentListModel>? {
        return voMemLoanTxnViewModel.getmemberloan(
            memberid,
            validate!!.RetriveSharepreferenceInt(
                VoSpData.vocurrentmeetingnumber
            )
        )
    }

    fun getnextdemand(memid:Long,loanno: Int): Int {
        return voMemLoanScheduleViewModel.getnextdemand(memid,loanno)
    }
    fun getTotalValue() {
        var iValue = 0
        var iValueCum = 0
        val iCount = rvList.childCount


        for (i in 0 until iCount) {
            val gridChild =
                rvList.getChildAt(i) as? ViewGroup

            val et_principal_demand = gridChild!!
                .findViewById<View>(R.id.et_principal_demand) as? EditText



            if (!et_principal_demand!!.text.toString().isNullOrEmpty()) {
                iValue = iValue + validate!!.returnIntegerValue(et_principal_demand.text.toString())
            }


        }
        tv_total_pay.text = iValue.toString()
        var bal=totdemand-iValue
        if(bal>0) {
            tv_total_balance.text = bal.toString()
        }else {
            tv_total_balance.text = "0"
        }
    }

    fun getTotalValue(iValue: Int, iValue1: Int, iValue2: Int, iValue3: Int) {

        demand = demand + iValue
        intdemand = intdemand + iValue1
        totdemand = totdemand + iValue2
        totpaid = totpaid + iValue3
        tv_total_principal_demand.text = LabelSet.getText(
            "rs",
            R.string.rs
        ) + demand.toString()
        tv_total_interest_demand.text = LabelSet.getText(
            "rs",
            R.string.rs
        ) + intdemand.toString()
        tv_total_amount.text = LabelSet.getText("rs", R.string.rs) + totdemand.toString()
        tv_total_pay.text = totpaid.toString()
        tv_repaid.text = totpaid.toString()
        var remainingBalance = totdemand-totpaid
        if(remainingBalance>0) {
            tv_total_balance.text = remainingBalance.toString()
        }else {
            tv_total_balance.text = "0"
        }
    }

    fun getremaininginstallment(loanno: Int): Int {
        return voMemLoanScheduleViewModel.getremaininginstallment(
            loanno,
            validate!!.RetriveSharepreferenceLong(VoSpData.voShgMemID)
        )
    }

    fun getbankpos(bankcode: String): Int {
        var pos = 99
        val bankList = cboBankViewModel!!.getcboBankdataModel(
            validate!!.RetriveSharepreferenceString(
                VoSpData.voSHGGUID
            ), cboType
        )
        if (!bankList.isNullOrEmpty()) {
            for (i in bankList.indices) {
                if (bankcode.equals(bankList.get(i).ifsc_code!!.dropLast(7) + bankList.get(i).account_no))
                    pos = i
            }
        }
        return pos
    }

    fun CustomAlertBankDialog(
        tvaccountno: TextView,
        tvtransactionno: TextView,
        ivSelectBank: ImageView
    ) {
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.bankdialoge, null)
        val mBuilder = AlertDialog.Builder(this).setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.setCancelable(false)
        mAlertDialog.setCanceledOnTouchOutside(false)
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.btn_save.background = resources.getDrawable(R.drawable.button_bg_vomeeting)

        mDialogView.btn_save.setOnClickListener {
            if (checkValidation(mDialogView) == 1){
                var colorRes = 0
                tvaccountno.text = returaccount(mDialogView)
                tvtransactionno.text = validate!!.returnStringValue(mDialogView.et_cheque_no_transactio_no.text.toString())
                when (getbankpos(
                    validate!!.returnStringValue(tvaccountno.text.toString()))) {
                    0 -> colorRes = R.color.color1
                    1 -> colorRes = R.color.color2
                    2 -> colorRes = R.color.color3
                    3 -> colorRes = R.color.color4
                    4 -> colorRes = R.color.color5
                    5 -> colorRes = R.color.color6
                    6 -> colorRes = R.color.color7
                    else -> colorRes = R.color.colordefault
                }
                ivSelectBank.setColorFilter(
                    ContextCompat.getColor(this, colorRes),
                    android.graphics.PorterDuff.Mode.SRC_IN
                )
                mAlertDialog.dismiss()
            }
        }

        mDialogView.btn_cancel.setOnClickListener {
            mAlertDialog.dismiss()
        }

        fillbank(mDialogView)

    }

    fun fillbank(mDialogView: View) {
        shgBankList = generateMeetingViewmodel.getBankdata(validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID))

        val adapter: ArrayAdapter<String?>
        if (!shgBankList.isNullOrEmpty()) {
            val isize = shgBankList!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in shgBankList!!.indices) {
                var lastthree=shgBankList!![i].account_no.substring(shgBankList!![i].account_no.length-3)
                sValue[i + 1] =
                    shgBankList!![i].ifsc_code.toString().dropLast(7) +"XXXXX"+ lastthree
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            mDialogView.spin_name_of_bank.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            mDialogView.spin_name_of_bank.adapter = adapter
        }

    }

    fun returaccount(mDialogView: View): String {

        var pos = mDialogView.spin_name_of_bank.selectedItemPosition
        var id = ""

        if (!shgBankList.isNullOrEmpty()) {
            if (pos > 0) id =
                shgBankList!!.get(pos - 1).ifsc_code!!.dropLast(7) + shgBankList!!.get(pos - 1).account_no
        }
        return id
    }

    fun checkValidation(mDialogView: View):Int{
        var value = 1

        if (mDialogView.spin_name_of_bank.selectedItemPosition == 0){
            validate!!.CustomAlertSpinner(
                this, mDialogView.spin_name_of_bank,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "name_of_bank",
                    R.string.name_of_bank
                )
            )
            value = 0
        }else if (mDialogView.et_cheque_no_transactio_no.text.toString().length == 0){
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "cheque_no_transactio_no",
                    R.string.cheque_no_transactio_no
                ),
                this, mDialogView.et_cheque_no_transactio_no
            )
            value = 0
        }


        return value
    }

    override fun onBackPressed() {
        val intent = Intent(this, LoanDueStatusSHGtoVOActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

}