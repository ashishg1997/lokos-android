package com.microware.cdfi.activity.vomeeting

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.SHGtoVOReceiptAdapter
import com.microware.cdfi.adapter.vomeetingadapter.SHGtoVOReceiptBankAdapter
import com.microware.cdfi.adapter.vomeetingadapter.TxnListAdapter
import com.microware.cdfi.entity.*
import com.microware.cdfi.entity.voentity.VoFinTxnDetMemEntity
import com.microware.cdfi.fragment.VoReceiptsTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import com.microware.cdfi.viewModel.voviewmodel.*
import kotlinx.android.synthetic.main.activity_s_h_gto_v_o_receipts.*
import kotlinx.android.synthetic.main.activity_s_h_gto_v_o_receipts.tvTotalAmt
import kotlinx.android.synthetic.main.buttons_vo.btn_save
import kotlinx.android.synthetic.main.buttons_vo.view.btn_cancel
import kotlinx.android.synthetic.main.buttons_vo.view.btn_save
import kotlinx.android.synthetic.main.receive_dialog.view.*
import kotlinx.android.synthetic.main.amountdialog.view.*
import kotlinx.android.synthetic.main.buttons_vo.btn_cancel
import kotlinx.android.synthetic.main.receive_dialog.*

class SHGtoVOReceipts : AppCompatActivity() {
    var voMtgDetViewModel: VoMtgDetViewModel? = null
    var voMemLoanViewModel: VoMemLoanViewModel? = null
    var voMemLoanTxnViewModel: VoMemLoanTxnViewModel? = null
    var mstvoCOAViewmodel: MstVOCOAViewmodel? = null
    var cboBankViewModel: CboBankViewmodel? = null
    var bankMasterViewModel: MasterBankViewmodel? = null
    var mappedshgViewmodel: MappedShgViewmodel? = null
    var mappedVoViewmodel: MappedVoViewmodel? = null
    var generateMeetingViewmodel: VoGenerateMeetingViewmodel? = null
    var dataspinShg: List<MappedShgEntity>? = null
    var dataspinVo: List<MappedVoEntity>? = null
    var validate: Validate? = null
    var dataspin_mode_of_payment: List<LookupEntity>? = null
    var lookupViewmodel: LookupViewmodel? = null
    var cboType = 0
    var bankList: List<Cbo_bankEntity>? = null
    var voFinTxnDetMemViewModel: VoFinTxnDetMemViewModel? = null
    var voFinTxnDetMemEntity: VoFinTxnDetMemEntity? = null
    var vocoaMappingViewModel: VoCoaMappingViewmodel? = null
    var voFinTxnViewModel: VoFinTxnViewModel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_s_h_gto_v_o_receipts)
        validate = Validate(this)

        if (validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType) == 2) {
            cboType = 2
        } else {
            cboType = 1
        }


        checkFragment()
        voMtgDetViewModel = ViewModelProviders.of(this).get(VoMtgDetViewModel::class.java)
        voMemLoanViewModel = ViewModelProviders.of(this).get(VoMemLoanViewModel::class.java)
        voMemLoanTxnViewModel = ViewModelProviders.of(this).get(VoMemLoanTxnViewModel::class.java)
        mstvoCOAViewmodel = ViewModelProviders.of(this).get(MstVOCOAViewmodel::class.java)
        cboBankViewModel = ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        bankMasterViewModel = ViewModelProviders.of(this).get(MasterBankViewmodel::class.java)
        mappedshgViewmodel = ViewModelProviders.of(this).get(MappedShgViewmodel::class.java)
        mappedVoViewmodel = ViewModelProviders.of(this).get(MappedVoViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        voFinTxnDetMemViewModel =
            ViewModelProviders.of(this).get(VoFinTxnDetMemViewModel::class.java)
        vocoaMappingViewModel = ViewModelProviders.of(this).get(VoCoaMappingViewmodel::class.java)
        voFinTxnViewModel = ViewModelProviders.of(this).get(VoFinTxnViewModel::class.java)

        spin_shgName?.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {

                override fun onItemSelected(
                    parent: AdapterView<*>, view: View?,
                    position: Int, id: Long
                ) {
                    var shgID: Long = 0
                    var shgname = ""
                    if (position > 0) {
                        if (cboType == 1) {
                            shgID = validate!!.returnShgspinnerID(spin_shgName, dataspinShg)
                            shgname = validate!!.returnShgspinnerName(spin_shgName, dataspinShg)

                        } else if (cboType == 2) {
                            shgID = validate!!.returnVospinnerID(spin_shgName, dataspinVo)
                        }
                    }
                    getamount(shgID)
                    fillRecyclerView(shgID, shgname)
                    fillRecyclerView1(shgID, shgname)
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // TODO Auto-generated method stub

                }
            }
        img_next.setOnClickListener {
            var shgID: Long = 0
            var shgname = ""
            if (cboType == 1) {
                shgID = validate!!.returnShgspinnerID(spin_shgName, dataspinShg)
                shgname = validate!!.returnShgspinnerName(spin_shgName, dataspinShg)

            } else if (cboType == 2) {
                shgID = validate!!.returnVospinnerID(spin_shgName, dataspinVo)

            }
            fillRecyclerView1(shgID, shgname)
            lay_bank.visibility = View.VISIBLE
            tbl_back.visibility = View.VISIBLE
            tbl_next.visibility = View.GONE
            lay_fund.visibility = View.GONE

        }
        img_back.setOnClickListener {
            var shgID: Long = 0
            if (cboType == 1) {
                shgID = validate!!.returnShgspinnerID(spin_shgName, dataspinShg)
            } else if (cboType == 2) {
                shgID = validate!!.returnVospinnerID(spin_shgName, dataspinVo)

            }
            fillRecyclerView(shgID, "")
            lay_bank.visibility = View.GONE
            lay_fund.visibility = View.VISIBLE
            tbl_back.visibility = View.GONE
            tbl_next.visibility = View.VISIBLE
        }
        btn_save.setOnClickListener {

        }

        btn_cancel.setOnClickListener {
            val intent = Intent(this, VoMeetingMenuActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }
        lay_bank.visibility = View.GONE
        tbl_back.visibility = View.GONE
        lay_fund.visibility = View.VISIBLE
        fillSpinner1()
        fillRecyclerView(0, "")
        fillRecyclerView1(0, "")

    }

    private fun fillSpinner1() {
        if (cboType == 1) {
            dataspinShg =
                mappedshgViewmodel!!.getShgNameList(validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID)!!)
            validate!!.fillShgspinner(this, spin_shgName, dataspinShg)

        } else if (cboType == 2) {
            dataspinVo =
                mappedVoViewmodel!!.getVoNameList(validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID)!!)
            validate!!.fillVospinner(this, spin_shgName, dataspinVo)
        }
        if (!dataspinShg.isNullOrEmpty()) {
            spin_shgName.setSelection(1)
        }
    }

    private fun fillRecyclerView(shgID: Long, shgname: String) {
        var listData: List<MstVOCOAEntity>? = null
        if (validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 1) {
            listData = mstvoCOAViewmodel!!.getCoaSubHeadData(
                validate!!.RetriveSharepreferenceInt(VoSpData.voOrgType),
                listOf("RG", "RL", "RO"),
                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
            )
        } else if (validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 5) {
            listData = mstvoCOAViewmodel!!.getCoaSubHeadData(
                validate!!.RetriveSharepreferenceInt(VoSpData.voOrgType),
                listOf("PG", "PL", "PO"),
                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
            )!!
        }


        if (!listData.isNullOrEmpty()) {
            getTotalValue(listData, shgID)
            rvList.apply {
                layoutManager = LinearLayoutManager(this@SHGtoVOReceipts)
                adapter = SHGtoVOReceiptAdapter(
                    this@SHGtoVOReceipts,
                    listData,
                    cboBankViewModel,
                    bankMasterViewModel,
                    shgID, shgname
                )
            }
        }

    }

    private fun fillRecyclerView1(shgID: Long, shgname: String) {
        val bankList = cboBankViewModel!!.getcboBankdataModel(
            validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID),
            cboType
        )
        if (!bankList.isNullOrEmpty()) {
            rvBankList.layoutManager = LinearLayoutManager(this)
            rvBankList.adapter =
                SHGtoVOReceiptBankAdapter(this, bankList, voFinTxnDetMemViewModel, shgID)
        }

    }

    private fun filltxnlist(rvTxnList: RecyclerView, shgID: Long, bankcode: String, txnno: String) {
        val txnlist = voFinTxnDetMemViewModel!!.gettxnkist(
            shgID,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber), bankcode, txnno
        )
        if (!txnlist.isNullOrEmpty()) {
            rvTxnList.layoutManager = LinearLayoutManager(this)
            rvTxnList.adapter =
                TxnListAdapter(this, txnlist)
            rvTxnList.visibility = View.VISIBLE
        } else {
            rvTxnList.visibility = View.GONE
        }

    }

    fun getRecepitType(keyCode: Int?, type: String): String? {
        var name: String? = null
        name = mstvoCOAViewmodel!!.getcoaValue(
            type,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),
            keyCode
        )
        return name
    }

    fun returndefaaultaccount(): String {
        var accountNo = ""
        val bankList = cboBankViewModel!!.getcboBankdataModel(
            validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID),
            cboType
        )
        if (!bankList.isNullOrEmpty()) {
            for (i in bankList.indices) {
                if (bankList.get(i).is_default == 1) {
                    accountNo =
                        bankList.get(i).ifsc_code!!.dropLast(7) + bankList.get(i).account_no!!
                    break
                }
            }
        }
        return accountNo
    }

    fun getBankName(bankID: Int?): String {
        var value: String? = null
        value = bankMasterViewModel!!.getBankName(bankID!!)
        return value!!
    }

    fun updatefindetailmemcash(shgID: Long, accountNo: String, amt1: Int) {
        var amt = voFinTxnDetMemViewModel!!.getpendingIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            shgID,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            1,
            listOf("PG", "PL", "PO"),
            ""
        )
        if (validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 5 && amt > validate!!.RetriveSharepreferenceInt(
                VoSpData.voCashinhand
            )
        ) {
            validate!!.CustomAlertVO(
                LabelSet.getText(
                    "inthecashbox",
                    R.string.inthecashbox
                ), this
            )
        } else {
            voFinTxnDetMemViewModel!!.updatefindetailmem(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                shgID,
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                "",
                1,
                "",
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime)
            )
            replaceFragmenty(
                fragment = VoReceiptsTopBarFragment(1),
                allowStateLoss = true,
                containerViewId = R.id.mainContent
            )
            getamount(shgID)
            validate!!.CustomAlertVO(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ), this
            )
        }
    }

    fun customReceiveDialog(shgID: Long, accountNo: String) {
        var mDialogView = LayoutInflater.from(this).inflate(R.layout.receive_dialog, null)
        val mBuilder = AlertDialog.Builder(this).setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.WHITE))
        fillSpinner(mDialogView)

        var amt = voFinTxnDetMemViewModel!!.getpendingIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            shgID,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            2,
            listOf("RG", "RL", "RO"),
            accountNo
        )
        var bankamt = voFinTxnViewModel!!.gettotalclosinginbank(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            accountNo
        )
        mDialogView.tvTotalAmt.text = amt.toString()

        mDialogView.et_cheque_no.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                s.toString()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = s.toString()
                filltxnlist(mDialogView.rvTxnList, shgID, accountNo, value)
            }

        })
        mDialogView.btn_save.setOnClickListener {
            if (mDialogView.spincheque.selectedItemPosition == 0) {
                validate!!.CustomAlertSpinner(
                    this, mDialogView.spincheque,
                    LabelSet.getText(
                        "please_select",
                        R.string.please_select
                    ) + " " + LabelSet.getText(
                        "cheque_payslip_rtgs_neft_imps_upi",
                        R.string.cheque_payslip_rtgs_neft_imps_upi
                    )

                )
            } else if (validate!!.returnStringValue(
                    mDialogView.et_cheque_no.text.toString()
                ).length == 0
            ) {
                validate!!.CustomAlertEditText(
                    LabelSet.getText(
                        "please_enter",
                        R.string.please_enter
                    ) + LabelSet.getText(
                        "cheque_no_transactio_no",
                        R.string.cheque_no_transactio_no
                    ), this,
                    mDialogView.et_cheque_no
                )
            } else if (validate!!.returnStringValue(
                    mDialogView.tvTotalAmt.text.toString()
                ).length == 0
            ) {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "please_enter",
                        R.string.please_enter
                    ) + LabelSet.getText(
                        "amount_to_be_received",
                        R.string.amount_to_be_received
                    ), this
                )
            } else if (validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 5 && amt > bankamt) {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "intheselectedbank",
                        R.string.intheselectedbank
                    ), this
                )
            } else {
                voFinTxnDetMemViewModel!!.updatefindetailmem(
                    validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                    shgID,
                    validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                    accountNo,
                    validate!!.returnlookupcode(mDialogView.spincheque, dataspin_mode_of_payment),
                    validate!!.returnStringValue(mDialogView.et_cheque_no.text.toString()),
                    validate!!.RetriveSharepreferenceString(AppSP.userid),
                    validate!!.Daybetweentime(validate!!.currentdatetime)
                )
                replaceFragmenty(
                    fragment = VoReceiptsTopBarFragment(1),
                    allowStateLoss = true,
                    containerViewId = R.id.mainContent
                )
                getamount(shgID)
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "data_saved_successfully",
                        R.string.data_saved_successfully
                    ), this
                )
                mAlertDialog.dismiss()
            }

        }

        mDialogView.btn_cancel.setOnClickListener {
            mAlertDialog.dismiss()
        }

    }

    private fun fillSpinner(mDialogView: View) {

        dataspin_mode_of_payment = lookupViewmodel!!.getlookupnotin(
            65, 1,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        validate!!.fillspinner(this, mDialogView.spincheque, dataspin_mode_of_payment)
    }

    fun CustomAlertAmountDialog(
        auid: Int,
        type: String,
        shgID: Long,
        bankcode: String,
        shgname: String
    ) {
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.amountdialog, null)
        val mBuilder = androidx.appcompat.app.AlertDialog.Builder(this).setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.setCancelable(false)
        mAlertDialog.setCanceledOnTouchOutside(false)
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        mDialogView.btn_save.setOnClickListener {
            if (checkValidation(mDialogView) == 1) {
                SaveAmount(mDialogView, auid, type, shgID, bankcode, shgname)
                mAlertDialog.dismiss()
            }
        }

        mDialogView.btn_cancel.setOnClickListener {
            mAlertDialog.dismiss()
        }

        showDialogData(mDialogView, auid, shgID)
    }

    private fun showDialogData(mDialogView: View?, auid: Int, shgID: Long) {

        var listdata = voFinTxnDetMemViewModel!!.getSavingamount(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            shgID, auid
        )

        if (!listdata.isNullOrEmpty()) {

            mDialogView!!.et_amount.setText(validate!!.returnStringValue(listdata[0].amount.toString()))
        }
    }

    private fun SaveAmount(
        mDialogView: View?,
        auid: Int,
        type: String,
        shgID: Long,
        bankcode: String,
        shgname: String
    ) {


        var mode = 0
        if (bankcode.isNotEmpty()) {
            mode = 2
        } else {
            mode = 1
        }
        if (auid == 35) {
            var totvolamt = voMtgDetViewModel!!.getVolsaving(
                shgID,
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
            )
            var bankamt = voFinTxnViewModel!!.gettotalclosinginbank(
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                bankcode
            )
            if (validate!!.returnIntegerValue(mDialogView!!.et_amount.text.toString()) > totvolamt) {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "pleaseenteramount",
                        R.string.pleaseenteramount
                    ), this
                )
            } else if (mode == 1 && validate!!.returnIntegerValue(mDialogView.et_amount.text.toString()) > validate!!.RetriveSharepreferenceInt(
                    VoSpData.voCashinhand
                )
            ) {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "inthecashbox",
                        R.string.inthecashbox
                    ), this
                )
            } else if (mode == 2 && validate!!.returnIntegerValue(mDialogView.et_amount.text.toString()) > bankamt) {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "intheselectedbank",
                        R.string.intheselectedbank
                    ), this
                )
            } else {
                voFinTxnDetMemEntity = VoFinTxnDetMemEntity(
                    0,
                    validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
                    validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                    shgID,
                    validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                    validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                    auid,
                    type,
                    validate!!.returnIntegerValue(mDialogView.et_amount.text.toString()),
                    mode,
                    0,
                    bankcode,
                    "",

                    "",
                    validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                    validate!!.RetriveSharepreferenceString(AppSP.userid),
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    "", 0,
                    validate!!.RetriveSharepreferenceString(AppSP.userid),
                    0,
                    0,
                    1,
                    validate!!.RetriveSharepreferenceInt(VoSpData.voOrgType)
                )

                voFinTxnDetMemViewModel!!.insertVoGroupLoanSchedule(voFinTxnDetMemEntity)
                voMtgDetViewModel!!.updateWithdrawal(
                    shgID,
                    validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                    validate!!.returnIntegerValue(mDialogView.et_amount.text.toString())
                )
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "data_saved_successfully",
                        R.string.data_saved_successfully
                    ), this
                )
            }
        } else if (auid == 82) {

            voFinTxnDetMemEntity = VoFinTxnDetMemEntity(
                0,
                validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                shgID,
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                auid,
                type,
                validate!!.returnIntegerValue(mDialogView!!.et_amount.text.toString()),
                mode,
                0,
                bankcode,
                "",

                "",
                validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                "", 0,
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                0,
                0,
                1,
                validate!!.RetriveSharepreferenceInt(VoSpData.voOrgType)
            )

            voFinTxnDetMemViewModel!!.insertVoGroupLoanSchedule(voFinTxnDetMemEntity)
            voMtgDetViewModel!!.updateCompclosing(
                shgID,
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.returnIntegerValue(mDialogView.et_amount.text.toString())
            )
            validate!!.CustomAlertVO(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ), this
            )

        }else if (auid == 3) {

            voFinTxnDetMemEntity = VoFinTxnDetMemEntity(
                0,
                validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                shgID,
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                auid,
                type,
                validate!!.returnIntegerValue(mDialogView!!.et_amount.text.toString()),
                mode,
                0,
                bankcode,
                "",

                "",
                validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                "", 0,
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                0,
                0,
                1,
                validate!!.RetriveSharepreferenceInt(VoSpData.voOrgType)
            )

            voFinTxnDetMemViewModel!!.insertVoGroupLoanSchedule(voFinTxnDetMemEntity)
            voMtgDetViewModel!!.updatevolclosing(
                shgID,
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.returnIntegerValue(mDialogView.et_amount.text.toString())
            )
            validate!!.CustomAlertVO(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ), this
            )

        } else {
            voFinTxnDetMemEntity = VoFinTxnDetMemEntity(
                0,
                validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                shgID,
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                auid,
                type,
                validate!!.returnIntegerValue(mDialogView!!.et_amount.text.toString()),
                mode,
                0,
                bankcode,
                "",

                "",
                validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                "", 0,
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                0,
                0,
                1,
                validate!!.RetriveSharepreferenceInt(VoSpData.voOrgType)
            )

            voFinTxnDetMemViewModel!!.insertVoGroupLoanSchedule(voFinTxnDetMemEntity)
            validate!!.CustomAlertVO(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ), this
            )
        }
        getamount(shgID)
        fillRecyclerView(shgID, shgname)
        fillRecyclerView1(shgID, shgname)
    }

    fun returnshg_code(): Long {

        var pos = spin_shgName.selectedItemPosition
        var id: Long = 0

        if (!dataspinShg.isNullOrEmpty()) {
            if (pos > 0) id = dataspinShg!!.get(pos - 1).shg_id!!
        }
        return id
    }

    fun returnVo_code(): Long {

        var pos = spin_shgName.selectedItemPosition
        var id: Long = 0

        if (!dataspinVo.isNullOrEmpty()) {
            if (pos > 0) id = dataspinVo!!.get(pos - 1).cbo_child_id!!
        }
        return id
    }

    fun checkValidation(mDialogView: View): Int {
        var value = 1

        if (mDialogView.et_amount.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "amount",
                    R.string.amount
                ),
                this, mDialogView.et_amount
            )
            value = 0
        }

        return value
    }

    fun getTotalValue(list: List<MstVOCOAEntity>, shgID: Long) {
        var totalAmount = 0
        for (i in 0 until list.size) {
            val amount = voFinTxnDetMemViewModel!!.getAmount(
                list[i].uid,
                shgID,
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
            )
            totalAmount = totalAmount + validate!!.returnIntegerValue(amount.toString())
        }
        tvTotalAmt.text = totalAmount.toString()
        //tv_Amount.text = totalAmount.toString()
    }

    fun getamount(shgID: Long) {
        var recievedAmount = 0
        var pendingAmount = 0
        if (validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 1) {
            recievedAmount = voFinTxnDetMemViewModel!!.getrecivedAmount(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                shgID,
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                listOf("RG", "RL", "RO")
            )
            pendingAmount = voFinTxnDetMemViewModel!!.getpendingAmount(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                shgID,
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                listOf("RG", "RL", "RO")
            )
        } else if (validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 5) {
            recievedAmount = voFinTxnDetMemViewModel!!.getrecivedAmount(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                shgID,
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                listOf("PG", "PL", "PO")
            )
            pendingAmount = voFinTxnDetMemViewModel!!.getpendingAmount(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                shgID,
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                listOf("PG", "PL", "PO")
            )
        }



        tv_Amount.text = pendingAmount.toString()
        tv_Amount1.text = recievedAmount.toString()
    }

    override fun onBackPressed() {
        val intent = Intent(this, VoMeetingMenuActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

    fun getAmount(uid: Int, shgID: Long): List<VoFinTxnDetMemEntity>? {
        return voFinTxnDetMemViewModel!!.getauidlist(
            uid,
            shgID,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
        )
    }

    fun getname(uid: Int): String {

        return mstvoCOAViewmodel!!.getCoaSubHeadname(
            uid,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
        )
    }

    fun getbankpos(bankcode: String): Int {
        var pos = 99
        val bankList = cboBankViewModel!!.getcboBankdataModel(
            validate!!.RetriveSharepreferenceString(
                VoSpData.voSHGGUID
            ), cboType
        )
        if (!bankList.isNullOrEmpty()) {
            for (i in bankList.indices) {
                if (bankcode.equals(bankList.get(i).ifsc_code!!.dropLast(7) + bankList.get(i).account_no))
                    pos = i
            }
        }
        return pos
    }

    fun returnbankcode(uid: Int): String? {
        var bancode:String? = null
        bancode = vocoaMappingViewModel!!.getBankCode(
            uid, validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        return bancode
    }

    private fun checkFragment() {
        if (validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 1) {
            replaceFragmenty(
                fragment = VoReceiptsTopBarFragment(1),
                allowStateLoss = true,
                containerViewId = R.id.mainContent
            )
        } else if (validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 5) {
            replaceFragmenty(
                fragment = VoReceiptsTopBarFragment(17),
                allowStateLoss = true,
                containerViewId = R.id.mainContent
            )

        }
    }

    fun getFundTypeId(receiptPayment: Int, type: String, uid: Int): String {
        return mstvoCOAViewmodel!!.getFundTypeId(receiptPayment, type, uid)
    }

    fun getFundlaonamount(shgID: Long, fundType: Int): Int {
        return voMemLoanViewModel!!.getfundamt(
            shgID,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            fundType
        )
    }

    fun getFundlaonrepaymentamount(shgID: Long, fundType: Int): Int {
        return generateMeetingViewmodel!!.getloanpaidbyauid(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber), shgID,
            fundType
        )
    }

}