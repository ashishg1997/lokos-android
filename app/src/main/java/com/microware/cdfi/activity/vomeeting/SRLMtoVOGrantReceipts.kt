package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.entity.voentity.VoFinTxnDetGrpEntity
import com.microware.cdfi.fragment.VoReceiptsTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoFinTxnDetGrpViewModel
import kotlinx.android.synthetic.main.activity_srlmto_vo_grant_receipts.*
import kotlinx.android.synthetic.main.buttons_vo.*
import java.util.*

class SRLMtoVOGrantReceipts : AppCompatActivity() {
    var cal = Calendar.getInstance()
    var dataspin_fund_type: List<LookupEntity>? = null
    var validate: Validate? = null
    var lookupViewmodel: LookupViewmodel? = null
    var voFinTxnDetGrpEntity: VoFinTxnDetGrpEntity? = null
    lateinit var voFinTxnDetGrpViewModel: VoFinTxnDetGrpViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_srlmto_vo_grant_receipts)
        btn_save.text = LabelSet.getText("confirm", R.string.confirm)
        validate = Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        voFinTxnDetGrpViewModel =
            ViewModelProviders.of(this).get(VoFinTxnDetGrpViewModel::class.java)

        fillSpinner()

        replaceFragmenty(
            fragment = VoReceiptsTopBarFragment(11),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        et_sanction_date.setOnClickListener {
            validate!!.datePickerwithmindate(
                validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                et_sanction_date
            )
        }


        et_date_of_amount_received.setOnClickListener {
            validate!!.datePickerwithmindate(
                validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                et_date_of_amount_received
            )
        }

        /* btn_save.setOnClickListener {
             var intent = Intent(this, SRLMtoVOOtherReceipts::class.java)
             intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
             startActivity(intent)
             overridePendingTransition(0, 0)
         }*/

        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                saveData()
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "data_saved_successfully",
                        R.string.data_saved_successfully
                    ), this, SRLMtoVOOtherReceipts::class.java
                )
            }
        }

        btn_cancel.setOnClickListener {
            var intent = Intent(this, SRLMtoVOLoanReceipts::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        showData()
        setLabelText()

    }

    private fun saveData() {
        //missing fields
        //Sanction Amount
        //Sanction Date
        voFinTxnDetGrpEntity = VoFinTxnDetGrpEntity(
            0,
            0,
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            0,
            validate!!.returnlookupcode(spin_fund_type, dataspin_fund_type),
            0,
            0,
            "",
            validate!!.returnIntegerValue(et_amount_received.text.toString()),
            "",
            validate!!.Daybetweentime(et_date_of_amount_received.text.toString()),
            0,
            "",

            "",
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            "",
            0,
            "",
            0
        )
        voFinTxnDetGrpViewModel.insertVoGroupLoanSchedule(voFinTxnDetGrpEntity!!)
    }

    private fun showData() {
        var list =
            voFinTxnDetGrpViewModel.getVoFinTxnDetGrpList(
                (validate!!.RetriveSharepreferenceString(
                    VoSpData.vomtg_guid
                )!!), validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
            )

        if (!list.isNullOrEmpty() && list.size > 0) {

            spin_fund_type.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(list.get(0).fundType.toString()),
                    dataspin_fund_type
                )
            )

            var amout = validate!!.returnIntegerValue(list.get(0).amount.toString())
            et_amount_received.setText(amout.toString())

            var date = validate!!.returnLongValue(list.get(0).dateRealisation.toString())
            et_date_of_amount_received.setText(validate!!.convertDatetime(date))
        }
    }

    private fun fillSpinner() {
        dataspin_fund_type = lookupViewmodel!!.getlookupMasterdata(
            73, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode), listOf(0, 2, 3, 5)
        )
        validate!!.fillspinner(this, spin_fund_type, dataspin_fund_type)
    }

    fun setLabelText() {
        tv_fund_type.text = LabelSet.getText(
            "fund_type",
            R.string.fund_type
        )

        tv_sanction_amount.text = LabelSet.getText(
            "sanction_amount",
            R.string.sanction_amount
        )

        tv_sanction_date.text = LabelSet.getText(
            "sanction_date",
            R.string.sanction_date
        )

        tv_amount_received.text = LabelSet.getText(
            "amount_received",
            R.string.amount_received
        )

        tv_amount_date.text = LabelSet.getText(
            "date_of_amount_received",
            R.string.date_of_amount_received
        )

        et_sanction_amount.hint = LabelSet.getText(
            "auto",
            R.string.auto
        )

        et_sanction_date.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )

        et_amount_received.hint = LabelSet.getText(
            "enter_amount",
            R.string.enter_amount
        )

        et_date_of_amount_received.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )

        tv_total.text = LabelSet.getText(
            "total",
            R.string.total
        )

        btn_save.text = LabelSet.getText(
            "confirm",
            R.string.confirm
        )
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }

    private fun checkValidation(): Int {
        var value = 1
        if (spin_fund_type.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_fund_type, LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "fund_type",
                    R.string.fund_type
                )
            )
            value = 0
            return value
        }
        if (validate!!.returnStringValue(et_sanction_amount.text.toString()).length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "sanction_amount",
                    R.string.sanction_amount
                ), this, et_sanction_amount
            )
            value = 0
            return value
        }
        if (validate!!.returnStringValue(et_sanction_date.text.toString()).length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "sanction_date",
                    R.string.sanction_date
                ), this, et_sanction_date
            )
            value = 0
            return value
        }
        if (validate!!.returnStringValue(et_amount_received.text.toString()).length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "amount_received",
                    R.string.amount_received
                ), this, et_amount_received
            )
            value = 0
            return value
        }
        if (validate!!.returnStringValue(et_date_of_amount_received.text.toString()).length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "date_of_amount_received",
                    R.string.date_of_amount_received
                ), this, et_date_of_amount_received
            )
            value = 0
            return value
        }

        return value
    }

    override fun onBackPressed() {
        var intent = Intent(this, SRLMtoVOLoanReceipts::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
    }
}