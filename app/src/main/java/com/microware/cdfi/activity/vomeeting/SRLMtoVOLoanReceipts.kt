package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.entity.voentity.VoGroupLoanEntity
import com.microware.cdfi.entity.voentity.VoGroupLoanScheduleEntity
import com.microware.cdfi.entity.voentity.VoGroupLoanTxnEntity
import com.microware.cdfi.fragment.VoReceiptsTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoGroupLoanScheduleViewModel
import com.microware.cdfi.viewModel.voviewmodel.VoGroupLoanTxnViewModel
import com.microware.cdfi.viewModel.voviewmodel.VoGroupLoanViewModel
import kotlinx.android.synthetic.main.activity_srlmto_vo_loan_receipts.*
import kotlinx.android.synthetic.main.activity_srlmto_vo_loan_receipts.et_interest_rate
import kotlinx.android.synthetic.main.activity_srlmto_vo_loan_receipts.et_loan_no
import kotlinx.android.synthetic.main.activity_srlmto_vo_loan_receipts.tv_interest_rate
import kotlinx.android.synthetic.main.activity_srlmto_vo_loan_receipts.tv_loan_no
import kotlinx.android.synthetic.main.buttons_vo.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import java.util.*

class SRLMtoVOLoanReceipts : AppCompatActivity() {
    var cal = Calendar.getInstance()
    var validate: Validate? = null
    var voGroupLoanEntity: VoGroupLoanEntity? = null
    var voGroupLoanTxnEntity: VoGroupLoanTxnEntity? = null
    var voGroupLoanScheduleEntity: VoGroupLoanScheduleEntity? = null
    lateinit var voGroupLoanViewModel: VoGroupLoanViewModel
    lateinit var voGroupLoanTxnViewModel: VoGroupLoanTxnViewModel
    lateinit var voGroupLoanScheduleViewModel: VoGroupLoanScheduleViewModel
    var lookupViewmodel: LookupViewmodel? = null

    var dataspin_fund_type: List<LookupEntity>? = null
    var dataspin_loan_product: List<LookupEntity>? = null
    var dataspin_loan_repayment_frequency: List<LookupEntity>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_srlmto_vo_loan_receipts)
        validate = Validate(this)
        voGroupLoanViewModel = ViewModelProviders.of(this).get(VoGroupLoanViewModel::class.java)
        voGroupLoanTxnViewModel =
            ViewModelProviders.of(this).get(VoGroupLoanTxnViewModel::class.java)
        voGroupLoanScheduleViewModel =
            ViewModelProviders.of(this).get(VoGroupLoanScheduleViewModel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)

        setLabel()
        fillSpinner()

        replaceFragmenty(
            fragment = VoReceiptsTopBarFragment(10),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        et_sanction_date.setOnClickListener {
            validate!!.datePickerwithmindate(
                validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                et_sanction_date
            )
        }

        et_date_of_amount_received.setOnClickListener {
            validate!!.datePickerwithmindate(
                validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                et_date_of_amount_received
            )
        }

        var loanno = voGroupLoanViewModel.getmaxLoanno(
            validate!!.RetriveSharepreferenceLong(
                VoSpData.voshgid
            )
        )

        et_loan_no.setText(
            (loanno + 1)
                .toString()
        )

        btn_save.setOnClickListener {

            if (checkValidation() == 1) {
                saveData(validate!!.returnIntegerValue(et_moratorium.text.toString()))
                insertscheduler(
                    (validate!!.returnIntegerValue(et_amount_received.text.toString())),
                    validate!!.returnIntegerValue(et_loan_tenure.text.toString()),
                    validate!!.returnIntegerValue(et_moratorium.text.toString())

                )
            }

        }

        btn_cancel.setOnClickListener {
            val intent = Intent(this, SRLMtoVOReceipts::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

    }

    override fun onBackPressed() {
        val intent = Intent(this, SRLMtoVOReceipts::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

    private fun setLabel() {
        btn_save.text = LabelSet.getText("confirm", R.string.confirm)
        btn_cancel.text = LabelSet.getText("cancel", R.string.cancel)
        tv_fund_type.text = LabelSet.getText("fund_type", R.string.fund_type)
        tv_fund_product.text = LabelSet.getText("loan_product", R.string.loan_product)
        tv_loan_no.text = LabelSet.getText("loan_no", R.string.loan_no)
        tv_sanction_amount.text = LabelSet.getText("sanction_amount", R.string.sanction_amount)
        tv_sanction_date.text = LabelSet.getText("sanction_date", R.string.sanction_date)
        tv_amount_received.text = LabelSet.getText("amount_received", R.string.amount_received)
        tv_date_amount_received.text = LabelSet.getText(
            "date_of_amount_received",
            R.string.date_of_amount_received
        )
        tv_interest_rate.text = LabelSet.getText("interest_rate", R.string.interest_rate)
        tv_loan_tenure.text = LabelSet.getText("loan_tenure", R.string.loan_tenure)
        tv_loan_repyament_frequency.text = LabelSet.getText(
            "loan_repyament_frequency",
            R.string.loan_repyament_frequency
        )
        tv_installment_amount.text = LabelSet.getText(
            "installment_amount_principal",
            R.string.installment_amount_principal
        )
        tv_moratorium.text = LabelSet.getText(
            "moratorium_in_months",
            R.string.moratorium_in_months
        )
        tv_total.text = LabelSet.getText("total", R.string.total)
        et_loan_no.hint = LabelSet.getText("auto", R.string.auto)
        et_sanction_amount.hint = LabelSet.getText("auto", R.string.auto)
        et_sanction_date.hint = LabelSet.getText("date_format", R.string.date_format)
        et_amount_received.hint = LabelSet.getText("enter_amount", R.string.enter_amount)
        et_date_of_amount_received.hint = LabelSet.getText("date_format", R.string.date_format)
        et_interest_rate.hint = LabelSet.getText("auto", R.string.auto)
        et_loan_tenure.hint = LabelSet.getText("type_here", R.string.type_here)
        et_installment_amount.hint = LabelSet.getText("type", R.string.type)
        et_moratorium.hint = LabelSet.getText(
            "enter_not_more_than_permissible_limit",
            R.string.enter_not_more_than_permissible_limit
        )

    }

    private fun checkValidation(): Int {

        var value = 1
        if (spin_fundType.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_fundType, LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "fund_type",
                    R.string.fund_type
                )
            )
            value = 0
            return value
        }
        if (spin_loan_product.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_loan_product, LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "loan_product",
                    R.string.loan_product
                )
            )
            value = 0
            return value
        }
        if (validate!!.returnStringValue(et_sanction_date.text.toString()).length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "sanction_date",
                    R.string.sanction_date
                ), this, et_sanction_date
            )
            value = 0
            return value
        }
        if (validate!!.returnIntegerValue(et_amount_received.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "amount_received",
                    R.string.amount_received
                ), this, et_amount_received
            )
            value = 0
            return value
        }
        if (validate!!.returnStringValue(et_date_of_amount_received.text.toString()).length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "date_of_amount_received",
                    R.string.date_of_amount_received
                ), this, et_date_of_amount_received
            )
            value = 0
            return value
        }
        if (validate!!.returnIntegerValue(et_interest_rate.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "interest_rate",
                    R.string.interest_rate
                ), this, et_interest_rate
            )
            value = 0
            return value
        }
        if (validate!!.returnIntegerValue(et_loan_tenure.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "loan_tenure",
                    R.string.loan_tenure
                ), this, et_loan_tenure
            )
            value = 0
            return value
        }
        if (spin_loan_repayment_frequency.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_loan_repayment_frequency, LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "loan_repyament_frequency",
                    R.string.loan_repyament_frequency
                )
            )
            value = 0
            return value
        }
        if (validate!!.returnIntegerValue(et_installment_amount.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "installment_amount_principal",
                    R.string.installment_amount_principal
                ), this, et_installment_amount
            )
            value = 0
            return value
        }
        if (validate!!.returnIntegerValue(et_moratorium.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "moratorium_in_months",
                    R.string.moratorium_in_months
                ), this, et_moratorium
            )
            value = 0
            return value
        }

        return value
    }

    private fun saveData(morotrum: Int) {

        var loanappid =
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid) + 1000 + validate!!.returnIntegerValue(
                et_loan_no.text.toString()
            ) + 1

        // et_sanction_date , et_installment_amount needs column
        // et_sanction date to be auto calculated

        voGroupLoanEntity = VoGroupLoanEntity(
            0,
            loanappid,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid),
            validate!!.returnIntegerValue(et_loan_no.text.toString()),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.Daybetweentime(et_date_of_amount_received.text.toString()),
            validate!!.addmonth(
                validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate), morotrum,0
            ),
            0,
            validate!!.returnIntegerValue(et_amount_received.text.toString()),
            0,
            validate!!.returnlookupcode(spin_loan_product, dataspin_loan_product),
            validate!!.returnDoubleValue(et_interest_rate.text.toString()),
            validate!!.returnIntegerValue(et_loan_tenure.text.toString()),
            0,
            0,
            0,
            0,
            0,
            true,
            validate!!.returnlookupcode(spin_fundType, dataspin_fund_type),
            0,
            0,
            0,
            "",
            "",
            validate!!.returnlookupcode(
                spin_loan_repayment_frequency,
                dataspin_loan_repayment_frequency
            ),
            validate!!.returnIntegerValue(et_moratorium.text.toString()),
            "",
            0,
            0,
            "",
            validate!!.returnIntegerValue(et_sanction_amount.text.toString()),
            0,
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            "",
            0,
            "",
            0
        )
        voGroupLoanViewModel.insertVoGroupLoan(voGroupLoanEntity!!)

        // insert into vogrploantxn

        voGroupLoanTxnEntity = VoGroupLoanTxnEntity(
            0,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.returnIntegerValue(et_loan_no.text.toString()),
            validate!!.returnIntegerValue(et_amount_received.text.toString()),
            0,
            0,
            0,
            0,
            0,
            true,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            "",
            "",
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            "",
            0,
            "",
            0,0.0,0,0
        )

        voGroupLoanTxnViewModel.insertVoGroupLoanTxn(voGroupLoanTxnEntity!!)
    }

    private fun insertscheduler(amt: Int, installment: Int, morotrum: Int) {
        var principaldemand = amt / installment
        var loanos = 0
        var saveValue = 0
        for (i in 0 until installment) {
            if (i == installment - 1) {
                loanos = 0
                principaldemand = amt - (principaldemand * i)
            } else {
                loanos = amt - (principaldemand * (i + 1))
            }
            voGroupLoanScheduleEntity = VoGroupLoanScheduleEntity(
                0,
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
                validate!!.returnIntegerValue(et_loan_no.text.toString()),
                principaldemand,
                principaldemand,
                loanos, 0,
                i + 1,
                1,
                validate!!.addmonth(
                    validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                    (morotrum + i + 1),0
                ),
                //   validate!!.Daybetweentime(validate!!.currentdatetime),
                validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                true, 0,
                0,
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime), "", 0,
                "", 0

            )
            voGroupLoanScheduleViewModel.insertVoGroupLoanSchedule(voGroupLoanScheduleEntity!!)
        }
        var principalDemand =
            voGroupLoanScheduleViewModel.getPrincipalDemandByInstallmentNum(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
                validate!!.returnIntegerValue(et_loan_no.text.toString()),
                1
            )

        //No principal_overdue in this activity
//        principalDemand += validate!!.returnIntegerValue(et_principal_overdue.text.toString())
        principalDemand += 1
        voGroupLoanScheduleViewModel.updateCutOffLoanSchedule(
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.returnIntegerValue(et_loan_no.text.toString()),
            1,
            principalDemand

        )
        CustomAlertchangeschedule(
            LabelSet.getText(
                "doyouwanttochaneschedule",
                R.string.doyouwanttochaneschedule
            )
        )
    }

    private fun fillSpinner() {
        dataspin_fund_type = lookupViewmodel!!.getlookupMasterdata(
            64,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode), listOf(0, 1, 2, 3, 4)
        )
        dataspin_loan_product = lookupViewmodel!!.getlookup(
            88,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_loan_repayment_frequency = lookupViewmodel!!.getlookup(
            88,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        validate!!.fillspinner(this, spin_fundType, dataspin_fund_type)
        validate!!.fillspinner(this, spin_loan_product, dataspin_loan_product)
        validate!!.fillspinner(
            this,
            spin_loan_repayment_frequency,
            dataspin_loan_repayment_frequency
        )
    }

    fun CustomAlertchangeschedule(str: String) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str
        mDialogView.btn_yes.background = resources.getDrawable(R.drawable.button_bg_vomeeting)
        mDialogView.btn_no.background = resources.getDrawable(R.drawable.button_bg_vomeeting)
        mDialogView.btn_yes.setOnClickListener {
            //            PartialSaveData()
            mAlertDialog.dismiss()
            validate!!.SaveSharepreferenceInt(
                VoSpData.voLoanno,
                validate!!.returnIntegerValue(et_loan_no.text.toString())
            )

            val intent = Intent(this, VoGroupPrincipalDemandActivity::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)

        }
        mDialogView.btn_no.setOnClickListener {
            val intent = Intent(this, VoGroupLoanlist::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)
            mAlertDialog.dismiss()
        }
    }

}