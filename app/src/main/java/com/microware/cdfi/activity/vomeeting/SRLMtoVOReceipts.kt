package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.SRLMtoVOReceiptsAdapter
import com.microware.cdfi.adapter.vomeetingadapter.SRLMtoVOReceiptBankAdapter
import com.microware.cdfi.fragment.VoReceiptsTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.CboBankViewmodel
import com.microware.cdfi.viewModel.MasterBankViewmodel
import com.microware.cdfi.viewModel.MstCOAViewmodel
import kotlinx.android.synthetic.main.activity_srlmto_voreceipts.*
import kotlinx.android.synthetic.main.buttons_vo.*

class SRLMtoVOReceipts : AppCompatActivity() {
    var mstCOAViewmodel: MstCOAViewmodel? = null
    var cboBankViewModel: CboBankViewmodel? = null
    var bankMasterViewModel: MasterBankViewmodel? = null
    var validate: Validate? = null
    var cboType = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_srlmto_voreceipts)

        setLabel()

        validate = Validate(this)
        if (validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType) == 2) {
            cboType = 2
        } else {
            cboType = 1
        }

        replaceFragmenty(
            fragment = VoReceiptsTopBarFragment(9),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        mstCOAViewmodel = ViewModelProviders.of(this).get(MstCOAViewmodel::class.java)
        cboBankViewModel = ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        bankMasterViewModel = ViewModelProviders.of(this).get(MasterBankViewmodel::class.java)

        btn_save.setOnClickListener {
            var intent = Intent(this, SRLMtoVOLoanReceipts::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        btn_cancel.setOnClickListener {
            var intent = Intent(this, VoMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        fillRecyclerView()
        fillRecyclerView1()
    }

    private fun fillRecyclerView() {
        var coaData = mstCOAViewmodel!!.getCoaSubHeadData(
            listOf<Int>(10), "OI", validate!!.RetriveSharepreferenceString(
                AppSP.Langaugecode
            )!!
        )
        /* var coaData = mstCOAViewmodel!!.getReceiptCoaSubHeadData(
             "OR",
             "OI",
             validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
         )*/
        if (!coaData.isNullOrEmpty()) {
            rvList.layoutManager = LinearLayoutManager(this)
            rvList.adapter =
                SRLMtoVOReceiptsAdapter(this, coaData, cboBankViewModel, mstCOAViewmodel,bankMasterViewModel)
        }
    }

    private fun fillRecyclerView1() {
        val bankList = cboBankViewModel!!.getcboBankdata(
            validate!!.RetriveSharepreferenceString(
                VoSpData.voSHGGUID
            ), cboType
        )
        if (!bankList.isNullOrEmpty()) {
            rvBankList.layoutManager = LinearLayoutManager(this)
            rvBankList.adapter = SRLMtoVOReceiptBankAdapter(this, bankList, mstCOAViewmodel)
        }
    }

    fun getBankName(bankID: Int?): String? {
        var value: String? = null
        value = bankMasterViewModel!!.getBankName(bankID!!)
        return value
    }

    private fun setLabel(){
        btn_save.text = LabelSet.getText("save",R.string.save)
        btn_cancel.text = LabelSet.getText("cancel",R.string.cancel)

        tv_dif_bank.text = LabelSet.getText("default_nbank",R.string.default_nbank)
        tv_funds.text = LabelSet.getText("funds",R.string.funds)
        tv_amount.text = LabelSet.getText("amount",R.string.amount)
        tv_payment_option.text = LabelSet.getText("payment_option",R.string.payment_option)
        tv_total.text = LabelSet.getText("total",R.string.total)
        tv_amount_received.text = LabelSet.getText("total_amount_received",R.string.total_amount_received)
        tv_amount_receiced_vo.text = LabelSet.getText("total_amount_received_by_vo",R.string.total_amount_received_by_vo)
    }

    override fun onBackPressed() {
        var intent = Intent(this, VoMeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

}