package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.fragment.OthersScreenTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.voviewmodel.*
import kotlinx.android.synthetic.main.transaction_payment_summary_all.*

class TransactionSummaryAll : AppCompatActivity() {
    var validate: Validate? = null
    lateinit var voGroupLoanTxnViewModel: VoGroupLoanTxnViewModel
    lateinit var voFinTxnDetGrpViewModel: VoFinTxnDetGrpViewModel
    lateinit var voGenerateMeetingViewmodel: VoGenerateMeetingViewmodel
    lateinit var voMtgDetViewModel: VoMtgDetViewModel
    lateinit var voFinTxnDetMemViewModel: VoFinTxnDetMemViewModel
    lateinit var voMemLoanViewModel: VoMemLoanViewModel
    lateinit var voGroupLoanViewModel: VoGroupLoanViewModel
    lateinit var voMemLoanTxnViewModel: VoMemLoanTxnViewModel
    lateinit var voLoanApplicationViewmodel: VoLoanApplicationViewmodel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.transaction_payment_summary_all)

        validate = Validate(this)
        voGroupLoanTxnViewModel =
            ViewModelProviders.of(this).get(VoGroupLoanTxnViewModel::class.java)
        voFinTxnDetGrpViewModel =
            ViewModelProviders.of(this).get(VoFinTxnDetGrpViewModel::class.java)
        voGenerateMeetingViewmodel =
            ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        voMtgDetViewModel =
            ViewModelProviders.of(this).get(VoMtgDetViewModel::class.java)
        voFinTxnDetMemViewModel =
            ViewModelProviders.of(this).get(VoFinTxnDetMemViewModel::class.java)
        voMemLoanViewModel =
            ViewModelProviders.of(this).get(VoMemLoanViewModel::class.java)
        voGroupLoanViewModel =
            ViewModelProviders.of(this).get(VoGroupLoanViewModel::class.java)
        voMemLoanTxnViewModel = ViewModelProviders.of(this).get(VoMemLoanTxnViewModel::class.java)
        voLoanApplicationViewmodel =
            ViewModelProviders.of(this).get(VoLoanApplicationViewmodel::class.java)

        replaceFragmenty(
            fragment = OthersScreenTopBarFragment(20),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        btn_savings.setOnClickListener {
            payments_layout.visibility = View.GONE
            saving_received_layout.visibility = View.VISIBLE
            btn_savings.setBackgroundColor(resources.getColor(R.color.green))
            tv_receipt.setTextColor(resources.getColor(R.color.white))
            btn_payments.setBackgroundColor(resources.getColor(R.color.white))
            tv_payments.setTextColor(resources.getColor(R.color.custom_red))
        }

        btn_payments.setOnClickListener {
            payments_layout.visibility = View.VISIBLE
            saving_received_layout.visibility = View.GONE
            btn_payments.setBackgroundColor(resources.getColor(R.color.custom_red))
            tv_payments.setTextColor(resources.getColor(R.color.white))
            btn_savings.setBackgroundColor(resources.getColor(R.color.white))
            tv_receipt.setTextColor(resources.getColor(R.color.green))
        }

        btn_sendApproval.isEnabled =
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber) >= validate!!.RetriveSharepreferenceInt(
                VoSpData.vomaxmeetingnumber
            )

        setLabel()
        setSavingReceivedFromShg()
        setLoanRepaymentReceivedFromShg()
        setLoanReceivedFromClf()
        setOtherReceipts()
        setIncomeAmount()
        setLoanGivenToShg()
        setSavingDepositedAtClf()
        setRepaymentToClf()
        setOtherPayments()
        setOutgoingAmount()
        updateBank()
        updateCashInHand()
        calculateAttendencePercentage()
        calculateRepaymentPercentage()

    }

    private fun setLabel() {
        btn_sendApproval.text = LabelSet.getText("send_for_approval", R.string.send_for_approval)
        tv_receipt.text = LabelSet.getText("receipt", R.string.receipt)
        tv_payments.text = LabelSet.getText("payment", R.string.payment)
        tv_savings_deposited_at_clf.text = LabelSet.getText(
            "savings_deposited_at_clf",
            R.string.savings_deposited_at_clf
        )
        tv_repayments_to_clf_other_bank.text = LabelSet.getText(
            "repayments_to_clf_other_bank",
            R.string.repayments_to_clf_other_bank
        )
        tv_other_payments.text = LabelSet.getText("other_payments", R.string.other_payments)
        tv_total_outgoing_amout.text = LabelSet.getText(
            "total_outgoing_amout",
            R.string.total_outgoing_amout
        )
        tv_saving_received_from_shg.text = LabelSet.getText(
            "saving_received_from_shg",
            R.string.saving_received_from_shg
        )
        tv_loan_repayment_received_from_shg.text = LabelSet.getText(
            "loan_repayment_received_from_shg",
            R.string.loan_repayment_received_from_shg
        )
        tv_loan_given_to_shg.text = LabelSet.getText(
            "loan_given_to_shg",
            R.string.loan_given_to_shg
        )
        tv_loan_recieved_clf.text = LabelSet.getText(
            "loan_received_fro_clf",
            R.string.loan_received_fro_clf
        )
        tv_other_receipts.text = LabelSet.getText("other_receipts1", R.string.other_receipts1)
        tv_total_incoming_amount.text = LabelSet.getText(
            "total_incoming_amount",
            R.string.total_incoming_amount
        )
        tv_cash_in_hand.text = LabelSet.getText("cash_in_hand", R.string.cash_in_hand)
        tv_cash_at_bank.text = LabelSet.getText("cash_at_bank", R.string.cash_at_bank)
        tvInsight.text = LabelSet.getText("insights", R.string.insights)
        tvMemberAttendence.text = LabelSet.getText("ec_attendance", R.string.ec_attendance)
        tv_repayment.text = LabelSet.getText("repayment", R.string.repayment)
    }

    private fun setSavingReceivedFromShg() {
        val totalPresent = voMtgDetViewModel.getECPresent(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        val totalshg = voMtgDetViewModel.getTotalPresentAbsent(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        val total =
            validate!!.returnStringValue(totalPresent.toString()) + "/" + validate!!.returnStringValue(
                totalshg.toString()
            )

        val amount = voFinTxnDetMemViewModel.getFinTxnDetailMemAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf("RG", "RL", "RO")
        )

        saving_received_from_shg.text = "₹ $amount"
        total_saving_received_from_shg.text = "($amount)"
        tvSHGAttendance.text = "$total"
        tvSHGAttendance1.text = "$total"

    }

    private fun setLoanRepaymentReceivedFromShg() {

        val amount = voMemLoanTxnViewModel.getLoanRepayReceived(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
        )

        val total = voMemLoanTxnViewModel.getTotalLoanRepayReceived(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
        )

        loan_repayment_received.text = "₹ $amount"
        total_loan_repayment_received.text = "($total)"

    }

    private fun setLoanReceivedFromClf() {
        val amount = voGroupLoanViewModel.getLoanRecAmount(
            2,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        val total = voGroupLoanViewModel.getLoanRecAmount(
            2,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        loan_recieved_clf.text = "₹ $amount"
        total_loan_recieved_clf.text = "($total)"

    }

    private fun setOtherReceipts() {
        val voFinTxnDetMemAmt = voFinTxnDetMemViewModel.getFinTxnDetailMemAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf("RO", "RL", "RG")
        )

        val voFinTxnDetGrpAmt = voFinTxnDetGrpViewModel.getFinTxnDetailGrpAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf("RO", "RL", "RG")
        )

        val total = voFinTxnDetMemAmt + voFinTxnDetGrpAmt
        other_receipts.text = "₹ $total"

    }

    private fun setIncomeAmount() {

        var balance = 0
        var totshgcashdebit = voFinTxnDetMemViewModel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf(1, 2, 3),
            listOf("PG", "PL", "PO")
        )
        var totvocashdebit = voFinTxnDetGrpViewModel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf(1, 2, 3),
            listOf("PG", "PL", "PO")
        )

        val grptotloanpaid = voGroupLoanTxnViewModel.getgrptotloanpaidinbank(
            listOf(1, 2, 3),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        val loanDisbursedAmt = voMemLoanViewModel.getMemLoanDisbursedAmount(
            listOf(1, 2, 3),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
        )
        balance = totshgcashdebit + totvocashdebit + grptotloanpaid + loanDisbursedAmt

        tv_ttl_incoming_amount.text = "₹ $balance"

    }

    private fun setLoanGivenToShg() {
        val totalPresent = voMtgDetViewModel.getECPresent(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        val totalshg = voMtgDetViewModel.getTotalPresentAbsent(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        val total =
            validate!!.returnStringValue(totalPresent.toString()) + "/" + validate!!.returnStringValue(
                totalshg.toString()
            )

        val amount = voMemLoanViewModel.gettotcboamt(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
        )

        val loanApplicationAmount = voLoanApplicationViewmodel.getdemand(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        loan_given_to_shg.text = "₹ $amount"
        total_loan_given_to_shg.text = "($loanApplicationAmount)"
        tvSHGAttendance2.text = "($total)"

    }

    private fun setSavingDepositedAtClf() {
        val amount = voFinTxnDetGrpViewModel.getSavingDepositedAmount(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            listOf("PO", "PG", "PL")
        )

        val total = voFinTxnDetGrpViewModel.getSavingDepositedAmount(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            listOf("PO", "PG", "PL")
        )

        savings_deposited_at_clf.text = "₹ $amount"
        total_savings_deposited_at_clf.text = "($total)"

    }

    private fun setRepaymentToClf() {
        val amount = voGroupLoanTxnViewModel.getRepaymentClfAmount(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        val total = voGroupLoanTxnViewModel.getTotalRepaymentClfAmount(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        repayments_to_clf_other_bank.text = "₹ $amount"
        total_repayments_to_clf_other_bank.text = "($total)"

    }

    private fun setOtherPayments() {
        val voFinTxnDetMemAmt = voFinTxnDetMemViewModel.getFinTxnDetailMemAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf("PO", "PL", "PG")
        )

        val voFinTxnDetGrpAmt = voFinTxnDetGrpViewModel.getFinTxnDetailGrpAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf("PO", "PL", "PG")
        )

        val total = voFinTxnDetMemAmt + voFinTxnDetGrpAmt

        other_payments.text = "₹ $total"

    }

    private fun setOutgoingAmount() {

        var amount = 0
        val totshgcash = voFinTxnDetMemViewModel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf(1, 2, 3),
            listOf("RG", "RL", "RO")
        )

        val totvocash = voFinTxnDetGrpViewModel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf(1, 2, 3),
            listOf("RG", "RL", "RO")
        )
        val memtotloanpaid = voMemLoanTxnViewModel.getmemtotloanpaidinbank(
            listOf(1, 2, 3),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        val loanDisbursedAmt = voGroupLoanViewModel.getgrouptotloanamtinbank(
            listOf(1, 2, 3),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        amount = totshgcash + totvocash + memtotloanpaid + loanDisbursedAmt

        tv_ttl_outgoing_amount.text = "₹ $amount"
    }

    private fun updateBank() {
        val bankAmt = voGenerateMeetingViewmodel.gettotalinbank(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        tv_total_Cash_in_bank.text = "₹ $bankAmt"

    }

    private fun updateCashInHand() {

        var openingcash = voGenerateMeetingViewmodel.getupdatedpeningcash(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        var totshgcash = voFinTxnDetMemViewModel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(1),
            listOf("RG", "RL", "RO"),
            ""
        )

        var totvocash = voFinTxnDetGrpViewModel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(1),
            listOf("RG", "RL", "RO"),
            ""
        )

        var totshgcashdebit = voFinTxnDetMemViewModel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(1),
            listOf("PG", "PL", "PO"),
            ""
        )

        var totvocashdebit = voFinTxnDetGrpViewModel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(1),
            listOf("PG", "PL", "PO"),
            ""
        )

        var totmemloan = voMemLoanViewModel.getMemLoanDisbursedAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            "", listOf<Int>(1)
        )

        var totvoloan = voGroupLoanViewModel.getgrouptotloanamtinbank(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            "", listOf<Int>(1)
        )

        var totmemloantxn = voMemLoanTxnViewModel.getmemtotloanpaidinbank(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            "", listOf<Int>(1)
        )

        var totvoloantxn = voGroupLoanTxnViewModel.getgrptotloanpaidinbank(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            "", listOf<Int>(1)
        )

        var totalclosingbal =
            openingcash + totshgcash + totvocash + totmemloantxn + totvoloan - totshgcashdebit - totvocashdebit - totmemloan - totvoloantxn

        tv_total_Cash_in_hand.text = "₹ $totalclosingbal"

    }

    private fun calculateAttendencePercentage() {
        val totalPresent = voMtgDetViewModel.getECPresent(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        val totalshg = voMtgDetViewModel.getTotalPresentAbsent(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        val calpercent: Float = (((totalPresent.toFloat() / totalshg.toFloat()) * 100).toFloat())
        tvAttendencePercntage.text = "${calpercent.toInt()} %"
        pbAttendence.progress = calpercent.toInt()
    }

    private fun calculateRepaymentPercentage() {

        val amount = voGroupLoanTxnViewModel.getRepaymentClfAmount(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        val total = voGroupLoanTxnViewModel.getTotalRepaymentClfAmount(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        if (amount > 0 && total > 0) {
            val calpercent: Float = (((amount.toFloat() / total.toFloat()) * 100).toFloat())
            tvRepaymentPercntage.text = "${calpercent.toInt()} %"
            pbRepayment.progress = calpercent.toInt()
        }


    }

    override fun onBackPressed() {
        val intent = Intent(this, VoMeetingMenuActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

}