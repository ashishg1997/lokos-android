package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.VoBankTransferAdapter
import com.microware.cdfi.api.vomodel.VOBankTransactionModel
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.VoSpData
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.voviewmodel.VoFinTxnDetGrpViewModel
import kotlinx.android.synthetic.main.activity_vobank_transfer_list.*
import kotlinx.android.synthetic.main.votoolbar.*

class VOBankTransferListActivity : AppCompatActivity() {
    var validate: Validate? = null
    var voFinTxnDetGrpViewModel: VoFinTxnDetGrpViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vobank_transfer_list)
        validate = Validate(this)
        voFinTxnDetGrpViewModel = ViewModelProviders.of(this).get(VoFinTxnDetGrpViewModel::class.java)

        tv_title.text = LabelSet.getText("bank_transfer",R.string.bank_transfer)
        IvScan.visibility = View.INVISIBLE
        IVSync.visibility = View.INVISIBLE

        icBack.setOnClickListener {
            var intent = Intent(this, BankSummary::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        addBank.setOnClickListener {
            validate!!.SaveSharepreferenceInt(VoSpData.voAuid,0)
            validate!!.SaveSharepreferenceString(VoSpData.bankFrom,"")
            validate!!.SaveSharepreferenceString(VoSpData.bankTo,"")
            validate!!.SaveSharepreferenceString(VoSpData.amountTransferred,"")
            var intent = Intent(this, VoBankTransferActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        setLabelText()
        fillRecyclerview()

    }

    private fun fillRecyclerview() {
        voFinTxnDetGrpViewModel!!.getBankTransferList(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),0
        ).observe(this, object : Observer<List<VOBankTransactionModel>?> {
            override fun onChanged(income_list: List<VOBankTransactionModel>?) {
                if (!income_list.isNullOrEmpty()) {
                    rvList.layoutManager =
                        LinearLayoutManager(this@VOBankTransferListActivity)
                    val expenditurePaymentAdapter = VoBankTransferAdapter(
                        this@VOBankTransferListActivity,
                        income_list
                    )
                    val isize: Int
                    isize = income_list.size
                    val params: ViewGroup.LayoutParams = rvList.layoutParams
                    val r = resources
                    val px = TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP,
                        resources.getDimension(R.dimen.spraying),
                        r.displayMetrics
                    )
                    val hi = Math.round(px)
                    val gridHeight = hi * isize
                    params.height = gridHeight
                    rvList.layoutParams = params
                    rvList.adapter = expenditurePaymentAdapter
                }
            }
        })
    }

    override fun onBackPressed() {
        var intent = Intent(this, BankSummary::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

    fun setLabelText(){
        tv_srno.text = LabelSet.getText("",R.string.sr_no)
        tv_bankFrom.text = LabelSet.getText("",R.string.bank_from)
        tv_bankTo.text = LabelSet.getText("",R.string.bank_to)
        tv_amount.text = LabelSet.getText("",R.string.amount)
    }

}