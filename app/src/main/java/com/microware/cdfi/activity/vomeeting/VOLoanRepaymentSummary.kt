package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.VotoClfLoanRepaymentSummaryAdapter
import com.microware.cdfi.fragment.VoReceiptsTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.FundTypeViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoGenerateMeetingViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoGroupLoanTxnViewModel
import com.microware.cdfi.viewModel.voviewmodel.VoGroupLoanViewModel
import kotlinx.android.synthetic.main.loan_repayment_summary.*
import kotlinx.android.synthetic.main.buttons_vo.*

class VOLoanRepaymentSummary : AppCompatActivity() {
    var validate: Validate? = null
    lateinit var generateMeetingViewmodel: VoGenerateMeetingViewmodel
    lateinit var voGroupLoanTxnViewModel: VoGroupLoanTxnViewModel
    lateinit var voGroupLoanViewModel: VoGroupLoanViewModel
    lateinit var fundTypeViewmodel: FundTypeViewmodel
    var today = 0
    var paid = 0
    var nextpayable = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.loan_repayment_summary)



        validate = Validate(this)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        voGroupLoanTxnViewModel =
            ViewModelProviders.of(this).get(VoGroupLoanTxnViewModel::class.java)
        fundTypeViewmodel =
            ViewModelProviders.of(this).get(FundTypeViewmodel::class.java)
        voGroupLoanViewModel =
            ViewModelProviders.of(this).get(VoGroupLoanViewModel::class.java)

        setLabelText()
        loanRepaymentRecycler()
        checkFragment()
    }

    private fun checkFragment() {
        when {

            validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 6 -> {
                replaceFragmenty(
                    fragment = VoReceiptsTopBarFragment(26),
                    allowStateLoss = true,
                    containerViewId = R.id.mainContent
                )
            }
            validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 7 -> {
                replaceFragmenty(
                    fragment = VoReceiptsTopBarFragment(30),
                    allowStateLoss = true,
                    containerViewId = R.id.mainContent
                )
            }
        }

    }

    private fun setLabelText() {
        tv_loan_source.text = LabelSet.getText("loan_source", R.string.loan_source)
        tv_todays_payable.text = LabelSet.getText("today_s_payable", R.string.today_s_payable)
        tv_paid.text = LabelSet.getText("paid_r", R.string.paid_r)
        tv_net_payable.text = LabelSet.getText("next_payble_amount", R.string.next_payble_amount)
        tv_total.text = LabelSet.getText("total", R.string.total)
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText("cancel", R.string.cancel)
    }

    private fun loanRepaymentRecycler() {
        today = 0
        paid = 0
        nextpayable = 0
        var list = voGroupLoanTxnViewModel.getListDataByLoansorce(
            validate!!.RetriveSharepreferenceInt(
                VoSpData.vocurrentmeetingnumber
            ),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(
                VoSpData.LoanSource
            )
        )
        var repaymentDetailAdapter = VotoClfLoanRepaymentSummaryAdapter(this, list)

        loan_repayment_summ.layoutManager = LinearLayoutManager(this)

        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = loan_repayment_summ.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        loan_repayment_summ.layoutParams = params
        loan_repayment_summ.adapter = repaymentDetailAdapter
    }

    fun getTotalValue(iValue: Int, flag: Int) {
        if (flag == 1) {
            today = today + iValue
            tv_value1.text = today.toString()
        } else if (flag == 2) {
            paid = paid + iValue
            tv_value2.text = paid.toString()
        } else if (flag == 3) {
            nextpayable = nextpayable + iValue
            tv_value3.text = nextpayable.toString()
        }
    }

    fun returnloanlist(loanno: Int): String {
        var loandata = voGroupLoanViewModel.getLoandatabyloanno(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            loanno
        )
        if (!loandata.isNullOrEmpty()) {
            var fundName = fundTypeViewmodel.getFundName(
                loandata.get(0).loanProductId!!,
                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
            )

            return fundName!!
        }
        return ""
    }

    override fun onBackPressed() {
        var intent = Intent(this, CLFtoVOReceipts::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

}