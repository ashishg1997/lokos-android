package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.VoCLfLoanWiseListAdapter
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.entity.voentity.VoGroupLoanScheduleEntity
import com.microware.cdfi.fragment.VoReceiptsTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import com.microware.cdfi.viewModel.voviewmodel.*
import kotlinx.android.synthetic.main.loan_wise_due_status_vo_to_clf.*

class VOLoanwisedustatus : AppCompatActivity() {
    var validate: Validate? = null
    lateinit var generateMeetingViewmodel: VoGenerateMeetingViewmodel
    var lookupViewmodel: LookupViewmodel? = null
    lateinit var voGroupLoanViewModel: VoGroupLoanViewModel
    lateinit var voGroupLoanTxnViewModel: VoGroupLoanTxnViewModel
    lateinit var voGroupLoanScheduleViewModel: VoGroupLoanScheduleViewModel
    var mstVOCoaViewmodel: MstVOCOAViewmodel? = null
    var mappedshgViewmodel: MappedShgViewmodel? = null
    var mappedVoViewmodel: MappedVoViewmodel? = null
    var today = 0
    var dataspin_mode_of_payment: List<LookupEntity>? = null
    var shgBankList: List<Cbo_bankEntity>? = null
    var voFinTxnViewModel: VoFinTxnViewModel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.loan_wise_due_status_vo_to_clf)



        validate = Validate(this)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        voGroupLoanViewModel = ViewModelProviders.of(this).get(VoGroupLoanViewModel::class.java)
        voGroupLoanScheduleViewModel =
            ViewModelProviders.of(this).get(VoGroupLoanScheduleViewModel::class.java)
        voGroupLoanTxnViewModel =
            ViewModelProviders.of(this).get(VoGroupLoanTxnViewModel::class.java)
        mstVOCoaViewmodel = ViewModelProviders.of(this).get(MstVOCOAViewmodel::class.java)
        mappedshgViewmodel = ViewModelProviders.of(this).get(MappedShgViewmodel::class.java)
        mappedVoViewmodel = ViewModelProviders.of(this).get(MappedVoViewmodel::class.java)
        voFinTxnViewModel = ViewModelProviders.of(this).get(VoFinTxnViewModel::class.java)

        val name = getRecepitType(validate!!.RetriveSharepreferenceInt(VoSpData.voAuid))
        tv_fund.text = name

        if (validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber) < validate!!.RetriveSharepreferenceInt(
                VoSpData.vomaxmeetingnumber
            )
        ) {
            btn_pay_total.isEnabled = false
            btn_pay_partial.isEnabled = false
        } else {
            btn_pay_total.isEnabled = true
            btn_pay_partial.isEnabled = true
        }

        if (validate!!.RetriveSharepreferenceInt(VoSpData.voModePayment) > 1) {
            lay_mode_of_payment.visibility = View.VISIBLE
            lay_cheque_no_transactio_no.visibility = View.VISIBLE
        } else {
            lay_mode_of_payment.visibility = View.GONE
            lay_cheque_no_transactio_no.visibility = View.GONE
        }

        btn_pay_total.setOnClickListener {
            if (checkValidation() == 1) {
                saveloanrepayment()
            }
        }

        btn_pay_partial.setOnClickListener {
            var intent = Intent(this, VOLoanRepayment::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        }

        setLabel()
        fillRecyclerView()
        fillspinner()
        checkFragment()

    }

    private fun checkFragment() {
        when {

            validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 6 -> {
                replaceFragmenty(
                    fragment = VoReceiptsTopBarFragment(27),
                    allowStateLoss = true,
                    containerViewId = R.id.mainContent
                )
            }
            validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 7 -> {
                replaceFragmenty(
                    fragment = VoReceiptsTopBarFragment(31),
                    allowStateLoss = true,
                    containerViewId = R.id.mainContent
                )
            }
        }

    }


    private fun checkValidation(): Int {

        var value = 1

        if (validate!!.returnIntegerValue(tv_total.text.toString()) == 0) {
            validate!!.CustomAlertVO(
                LabelSet.getText(
                    "amountalreadypaid",
                    R.string.amountalreadypaid
                ), this
            )
            value = 0
            return value
        }
        if (spin_mode_of_payment.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_mode_of_payment,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "mode_of_payment",
                    R.string.mode_of_payment
                )

            )
            value = 0
            return value
        }
        if (validate!!.RetriveSharepreferenceInt(VoSpData.voModePayment) == 1 && validate!!.returnIntegerValue(
                tv_total.text.toString()
            ) > validate!!.RetriveSharepreferenceInt(
                VoSpData.voCashinhand
            )
        ) {
            validate!!.CustomAlertVO(
                LabelSet.getText(
                    "inthecashbox",
                    R.string.inthecashbox
                ), this
            )
            value = 0
            return value
        }
        var bankamt = voFinTxnViewModel!!.gettotalclosinginbank(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceString(VoSpData.voBankCode)
        )
        if (validate!!.RetriveSharepreferenceInt(VoSpData.voModePayment) > 1 && validate!!.returnIntegerValue(
                tv_total.text.toString()
            ) > bankamt
        ) {
            validate!!.CustomAlertVO(
                LabelSet.getText(
                    "intheselectedbank",
                    R.string.intheselectedbank
                ), this
            )
            value = 0
            return value
        }
        if (lay_cheque_no_transactio_no.visibility == View.VISIBLE && validate!!.returnStringValue(
                et_cheque_no_transactio_no.text.toString()
            ).length == 0
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + LabelSet.getText(
                    "cheque_no_transactio_no",
                    R.string.cheque_no_transactio_no
                ), this,
                et_cheque_no_transactio_no
            )
            value = 0
            return value
        }
        return value
    }

    fun fillspinner() {
        dataspin_mode_of_payment = lookupViewmodel!!.getlookupnotin(
            65,
            1,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        validate!!.fillspinner(this, spin_mode_of_payment, dataspin_mode_of_payment)
    }

    private fun setLabel() {
        /*  var shgName = getShgName()
          tvshg_name.text = shgName*/
        tv_loan_no.text = LabelSet.getText("loan_no", R.string.loan_no)
        tv_outstanding.text = LabelSet.getText(
            "outstanding_installment",
            R.string.outstanding_installment
        )
        tv_original_loan_amount.text = LabelSet.getText(
            "original_loan_amount",
            R.string.original_loan_amount
        )
        tvCurrentDue.text = LabelSet.getText("current_due", R.string.current_due)
        tv_current_arrear.text = LabelSet.getText(
            "principal_interest",
            R.string.principal_interest
        )
        tv_total_due.text = LabelSet.getText("total_due", R.string.total_due)
        tv_current_arrear1.text = LabelSet.getText(
            "principal_interest",
            R.string.principal_interest
        )
        tv_total_to_repay.text = LabelSet.getText("total_to_repay", R.string.total_to_repay)
        tv_mode_of_payment.text = LabelSet.getText("mode_of_payment", R.string.mode_of_payment)
        tv_cheque_no_transactio_no.text = LabelSet.getText(
            "cheque_no_transactio_no",
            R.string.cheque_no_transactio_no
        )
        btn_pay_total.text = LabelSet.getText("pay_total", R.string.pay_total)
        btn_pay_partial.text = LabelSet.getText("pay_partial", R.string.pay_partial)
    }

    private fun getShgName(): String? {
        var shgName =
            mappedshgViewmodel!!.getShgName(
                validate!!.RetriveSharepreferenceLong(VoSpData.voShgMemID),
                validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID)!!
            )
        return shgName
    }

    fun getRecepitType(keyCode: Int?): String? {
        var name: String? = null
        name = mstVOCoaViewmodel!!.getcoaValue(
            "PL",
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),
            keyCode
        )
        return name
    }

    private fun fillRecyclerView() {
        today = 0
        var list = voGroupLoanTxnViewModel.getgrouploantxnlist(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(
                VoSpData.vocurrentmeetingnumber
            )
        )

        var loanDueStatusSHGtoVOAdapter = VoCLfLoanWiseListAdapter(this, list)

        rvList.layoutManager = LinearLayoutManager(this)

        val isize: Int
        isize = list!!.size
        val params: ViewGroup.LayoutParams = rvList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvList.layoutParams = params
        rvList.adapter = loanDueStatusSHGtoVOAdapter
        getTotalValue()

    }

    fun getTotalValue() {
        var totalpaid = voGroupLoanTxnViewModel.gettotalpaid(
            validate!!.RetriveSharepreferenceLong(
                VoSpData.voshgid
            ),
            validate!!.RetriveSharepreferenceInt(
                VoSpData.vocurrentmeetingnumber
            )
        )
        if (totalpaid > 0) {
            tv_total.text = totalpaid.toString()
        } else {
            tv_total.text = "0"
        }
    }

    fun getloanamount(loanno: Int): Double {
        return voGroupLoanViewModel.gettotamt(
            loanno,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
    }

    fun getremaininginstallment(loanno: Int): Int {
        return voGroupLoanScheduleViewModel.getremaininginstallment(
            loanno,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )


    }

    fun saveloanrepayment() {
        var saveValue = 0

        val iCount = rvList.childCount


        for (i in 0 until iCount) {
            val gridChild =
                rvList.getChildAt(i) as? ViewGroup

            val tv_loan_no1 = gridChild!!
                .findViewById<View>(R.id.tv_loan_no1) as? TextView

            val total_due1 = gridChild
                .findViewById<View>(R.id.total_due1) as? TextView

            if (validate!!.returnIntegerValue(total_due1!!.text.toString()) > 0) {
                var loanno = validate!!.returnIntegerValue(tv_loan_no1!!.text.toString())
                var shgId = validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
                var paid = validate!!.returnIntegerValue(total_due1.text.toString())
                saveValue = saveData(loanno, shgId, paid)
            }
        }

        if (saveValue > 0) {
            fillRecyclerView()
            validate!!.CustomAlertVO(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this
            )
        }
    }

    fun saveData(loanno: Int, shgId: Long, paid: Int): Int {
        var value = 0
        var listdata = voGroupLoanTxnViewModel.getgrouploandata(
            loanno,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            shgId
        )

        var amt = paid
        var intrest = 0
        var loanos = 0
        var principaldemandcl = 0
        if (!listdata.isNullOrEmpty()) {
            loanos =
                validate!!.returnIntegerValue(listdata.get(0).loanOp.toString())

            principaldemandcl =
                validate!!.returnIntegerValue(listdata.get(0).principalDemandOb.toString()) + validate!!.returnIntegerValue(
                    listdata.get(0).principalDemand.toString()
                )
            intrest =
                validate!!.returnIntegerValue(listdata.get(0).intAccruedOp.toString()) + validate!!.returnIntegerValue(
                    listdata.get(0).intAccrued.toString()
                )
            if (amt > intrest) {
                amt = amt - intrest
            } else {
                intrest = intrest - amt
                amt = 0
            }

        }
        if (principaldemandcl > amt) {
            principaldemandcl = principaldemandcl - amt
        } else {
            principaldemandcl = 0
        }
        var bankode = ""
        var paymentmode = 0
        if (validate!!.RetriveSharepreferenceInt(VoSpData.voModePayment) > 1) {
            paymentmode =
                validate!!.returnlookupcode(spin_mode_of_payment, dataspin_mode_of_payment)
            bankode = validate!!.RetriveSharepreferenceString(VoSpData.voBankCode)!!
        } else {
            bankode = ""
            paymentmode = 1
        }
        var completionflag=false
        if (amt>=loanos){
            completionflag=true
        }
        voGroupLoanTxnViewModel.updateloanpaid(
            loanno,
            validate!!.RetriveSharepreferenceInt(
                VoSpData.vocurrentmeetingnumber
            ),
            shgId,
            amt,
            intrest,
            paymentmode,
            bankode,
            validate!!.returnStringValue(
                et_cheque_no_transactio_no.text.toString()
            ), principaldemandcl, completionflag
        )
        voGroupLoanViewModel.updateShgGroupLoan(shgId,loanno,completionflag)
        updateschedule(loanno, shgId, amt)
        value = 1

        return value
    }

    fun updateschedule(loanno: Int, shgId: Long, paid: Int): Int {
        var value = 0
        var amt = paid
        voGroupLoanScheduleViewModel.deleterepaid(
            loanno,
            shgId,
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
        )
        voGroupLoanScheduleViewModel.deletesubinstallment(
            loanno,
            shgId,
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
        )
        var list = voGroupLoanScheduleViewModel.getMemberScheduleloanwise(
            shgId, loanno,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
        )

        for (i in list!!.indices) {
            if (amt > 0) {
                if (validate!!.returnIntegerValue(list.get(i).principalDemand.toString()) <= amt) {
                    voGroupLoanScheduleViewModel.updaterepaid(
                        loanno,
                        shgId,
                        list.get(i).installmentNo,
                        list.get(i).subInstallmentNo,
                        validate!!.returnIntegerValue(list.get(i).principalDemand.toString()),
                        validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                        validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime)
                    )
                    amt =
                        amt - validate!!.returnIntegerValue(list.get(i).principalDemand.toString())
                } else {
                    var remainingamt =
                        validate!!.returnIntegerValue(list.get(i).principalDemand.toString()) - amt
                    voGroupLoanScheduleViewModel.updaterepaid(
                        loanno,
                        shgId,
                        list.get(i).installmentNo,
                        list.get(i).subInstallmentNo,
                        amt,
                        validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                        validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime)
                    )

                    var voGpLoanScheduleEntity = VoGroupLoanScheduleEntity(
                        0,
                        shgId,
                        validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid),
                        validate!!.returnIntegerValue(list.get(i).loanNo.toString()),
                        remainingamt,
                        remainingamt,
                        (validate!!.returnIntegerValue(list.get(i).loanOs.toString())), 0,
                        validate!!.returnIntegerValue(list.get(i).installmentNo.toString()),
                        validate!!.returnIntegerValue(list.get(i).subInstallmentNo.toString()) + 1,
                        validate!!.returnLongValue(list.get(i).installmentDate.toString()),
                        0,
                        false,
                        0,
                        validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime), "", 0,
                        "", 0
                    )
                    voGroupLoanScheduleViewModel.insertVoGroupLoanSchedule(voGpLoanScheduleEntity)
                    amt = 0
                    break
                }
            } else {
                break
            }
        }
        value = 1

        return value
    }

    override fun onBackPressed() {
        var intent = Intent(this, VOLoanRepaymentSummary::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

}