package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.VotoOthersLoanRepaymentAdapter
import com.microware.cdfi.fragment.VoReceiptsTopBarFragment
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.replaceFragmenty
import kotlinx.android.synthetic.main.buttons_vo.*
import kotlinx.android.synthetic.main.vo_loan_repayments_vo_to_others.*

class VOtoOthersLoanRepayment : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.vo_loan_repayments_vo_to_others)
        btn_save.text = LabelSet.getText("confirm",R.string.confirm)
        replaceFragmenty(
            fragment = VoReceiptsTopBarFragment(32),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        btn_cancel.setOnClickListener {
            var intent = Intent(this, VOtoOthersLoanWiseDueStatus::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        btn_save.setOnClickListener {
            var intent = Intent(this, VOtoOthersHrCaderPayments::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        loanRepaymentRecycler()
    }

    private fun loanRepaymentRecycler() {
        loan_repayment.layoutManager = LinearLayoutManager(this)
        loan_repayment.adapter = VotoOthersLoanRepaymentAdapter(this)
    }

    override fun onBackPressed() {
        var intent = Intent(this, VOtoOthersLoanWiseDueStatus::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

}