package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.VotoOthersLoanRepaymentSummaryAdapter
import com.microware.cdfi.fragment.VoReceiptsTopBarFragment

import com.microware.cdfi.utility.replaceFragmenty
import kotlinx.android.synthetic.main.buttons_vo.*
import kotlinx.android.synthetic.main.loan_repayment_summary_vo_to_others.*

class VOtoOthersLoanRepaymentSumamry : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.loan_repayment_summary_vo_to_others)
        replaceFragmenty(
            fragment = VoReceiptsTopBarFragment(30),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        btn_cancel.setOnClickListener {
            var intent = Intent(this, VOtoOtherPayments::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }
        btn_save.setOnClickListener {
            var intent = Intent(this, VOtoOthersLoanWiseDueStatus::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        loanRepaymentSummaryRecycler()
    }

    private fun loanRepaymentSummaryRecycler() {
        loan_repayment.layoutManager = LinearLayoutManager(this)
        loan_repayment.adapter = VotoOthersLoanRepaymentSummaryAdapter(this)
    }

    override fun onBackPressed() {
        var intent = Intent(this, VOtoOtherPayments::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

}