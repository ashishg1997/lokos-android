package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.VoOtherLoanWiseListAdapter
import com.microware.cdfi.fragment.VoReceiptsTopBarFragment

import com.microware.cdfi.utility.replaceFragmenty
import kotlinx.android.synthetic.main.loan_wise_due_status_vo_to_others.*

class VOtoOthersLoanWiseDueStatus : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.loan_wise_due_status_vo_to_others)

        replaceFragmenty(
            fragment = VoReceiptsTopBarFragment(31),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        btnPay.setOnClickListener {
            var intent = Intent(this, VOtoOthersLoanRepayment::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        loanWiseDueRecycler()
    }

    private fun loanWiseDueRecycler() {
        rvList.layoutManager = LinearLayoutManager(this)
        rvList.adapter = VoOtherLoanWiseListAdapter(this)
    }

    override fun onBackPressed() {
        var intent = Intent(this, VOtoOthersLoanRepaymentSumamry::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

}