package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.entity.voentity.VoFinTxnDetMemEntity
import com.microware.cdfi.fragment.VoReceiptsTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoFinTxnDetMemViewModel
import kotlinx.android.synthetic.main.buttons_vo.*
import kotlinx.android.synthetic.main.rf_vrf_grantpayments_vo_to_shg.*

class VOtoSHGGrantPayments : AppCompatActivity() {
    var validate: Validate? = null
    var dataspin_fund_type: List<LookupEntity>? = null
    var voFinTxnDetMemEntity: VoFinTxnDetMemEntity? = null

    lateinit var lookupViewmodel: LookupViewmodel
    lateinit var voFinTxnDetMemViewModel: VoFinTxnDetMemViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.rf_vrf_grantpayments_vo_to_shg)
        replaceFragmenty(
            fragment = VoReceiptsTopBarFragment(22),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )
        validate = Validate(this)

        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        voFinTxnDetMemViewModel =
            ViewModelProviders.of(this).get(VoFinTxnDetMemViewModel::class.java)

        et_sanction_date.setOnClickListener {
            validate!!.datePicker(et_sanction_date)
        }

        et_date_of_amount_paid.setOnClickListener {
            validate!!.datePicker(et_date_of_amount_paid)
        }

        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                saveData()
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "data_saved_successfully",
                        R.string.data_saved_successfully
                    ), this, VOtoSHGOtherPayments::class.java
                )
            }

            /*var intent = Intent(this, VOtoSHGOtherPayments::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            overridePendingTransition(0, 0)*/
        }

        btn_cancel.setOnClickListener {
            var intent = Intent(this, VOtoSHGLoanPayment::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        ivADD.setOnClickListener {
            clearAll()
        }

        setLabelText()
        fillSpinner()
//        showData()

    }

    fun setLabelText() {
        btn_save.text = LabelSet.getText("confirm", R.string.confirm)
        btn_cancel.text = LabelSet.getText("cancel", R.string.cancel)

        tv_fund_type.text = LabelSet.getText("fund_type", R.string.fund_type)
        tv_sanction_amount.text = LabelSet.getText("sanction_amount", R.string.sanction_amount)
        tv_sanction_date.text = LabelSet.getText("sanction_date", R.string.sanction_date)
        tv_amount_paid.text = LabelSet.getText("amount_paid", R.string.amount_paid)
        tv_date_of_amount_paid.text = LabelSet.getText(
            "date_of_amount_paid",
            R.string.date_of_amount_paid
        )
        tv_total.text = LabelSet.getText("total", R.string.total)

        et_sanction_amount.hint = LabelSet.getText("type_here", R.string.type_here)
        et_sanction_date.hint = LabelSet.getText("date_format", R.string.date_format)
        et_amount_paid.hint = LabelSet.getText("enter_amount", R.string.enter_amount)
        et_date_of_amount_paid.hint = LabelSet.getText("date_format", R.string.date_format)
    }

    private fun fillSpinner() {
        dataspin_fund_type = lookupViewmodel.getlookupMasterdata(
            73, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode), listOf(0, 2, 3, 5)
        )
        validate!!.fillspinner(this, spin_fund_type, dataspin_fund_type)
    }

    private fun checkValidation(): Int {
        var value = 1
        if (spin_fund_type.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_fund_type, LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "fund_type",
                    R.string.fund_type
                )
            )
            value = 0
            return value
        }
        if (validate!!.returnStringValue(et_sanction_amount.text.toString()).length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "sanction_amount",
                    R.string.sanction_amount
                ), this, et_sanction_amount
            )
            value = 0
            return value
        }
        if (validate!!.returnStringValue(et_sanction_date.text.toString()).length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "sanction_date",
                    R.string.sanction_date
                ), this, et_sanction_date
            )
            value = 0
            return value
        }
        if (validate!!.returnStringValue(et_amount_paid.text.toString()).length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "amount_paid",
                    R.string.amount_paid
                ), this, et_amount_paid
            )
            value = 0
            return value
        }
        if (validate!!.returnStringValue(et_date_of_amount_paid.text.toString()).length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "date_of_amount_paid",
                    R.string.date_of_amount_paid
                ), this, et_date_of_amount_paid
            )
            value = 0
            return value
        }

        return value
    }

    private fun saveData() {
        //missing fields : 3
        //Sanction Amount
        //Sanction Date
        //fund type
        voFinTxnDetMemEntity = VoFinTxnDetMemEntity(
            0,
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            0,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            0,
            "",
            validate!!.returnIntegerValue(et_amount_paid.text.toString()),
            0,
            validate!!.Daybetweentime(et_date_of_amount_paid.text.toString()),
            "",
            "",

            "",
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            "",
            0,
            "",
            0,0
        )
        voFinTxnDetMemViewModel.insertVoGroupLoanSchedule(voFinTxnDetMemEntity!!)
    }

    /*private fun showData() {
        var list =
            voFinTxnDetMemViewModel!!.getVoFinTxnDetMemList(
                (validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
            )

        if (!list.isNullOrEmpty() && list.size > 0) {

            *//*spin_fund_type.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(list.get(0).fundType.toString()),
                    dataspin_fund_type
                )
            )

            et_sanction_amount.setText(validate!!.returnIntegerValue(list.get(0).amount.toString()))
            et_sanction_date.setText(
                validate!!.convertDatetime(
                    validate!!.returnLongValue(
                        list.get(0).dateRealisation.toString()
                    )
                )
            )*//*


            et_amount_paid.setText(validate!!.returnIntegerValue(list.get(0).amount.toString()))

            et_date_of_amount_paid.setText(
                validate!!.convertDatetime(
                    validate!!.returnLongValue(
                        list.get(0).dateRealisation.toString()
                    )
                )
            )
        }
    }*/

    private fun clearAll() {
        spin_fund_type.setSelection(0)
        et_sanction_amount.setText("")
        et_sanction_date.setText("")
        et_amount_paid.setText("")
        et_date_of_amount_paid.setText("")
    }

    override fun onBackPressed() {
        var intent = Intent(this, VOtoSHGLoanPayment::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        overridePendingTransition(0, 0)
    }
}