package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.voentity.VoLoanApplicationEntity
import com.microware.cdfi.fragment.VoReceiptsTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.MstVOCOAViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoLoanApplicationViewmodel
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.buttons_vo.*
import kotlinx.android.synthetic.main.buttons_vo.btn_cancel
import kotlinx.android.synthetic.main.buttons_vo.btn_save
import kotlinx.android.synthetic.main.buttons_vo.btn_savegray
import kotlinx.android.synthetic.main.loan_request_vo_to_shg.*


class VOtoSHGLoanRequest : AppCompatActivity() {
    var validate: Validate? = null
    var lookupViewmodel: LookupViewmodel? = null
    var voLoanApplicationViewmodel: VoLoanApplicationViewmodel? = null
    var voLoanApplicationEntity: VoLoanApplicationEntity? = null
    lateinit var mstVOCoaViewmodel: MstVOCOAViewmodel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.loan_request_vo_to_shg)

        validate = Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        voLoanApplicationViewmodel =
            ViewModelProviders.of(this).get(VoLoanApplicationViewmodel::class.java)
        mstVOCoaViewmodel = ViewModelProviders.of(this).get(MstVOCOAViewmodel::class.java)

        replaceFragmenty(
            fragment = VoReceiptsTopBarFragment(19),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        btn_save.isEnabled =
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber) >= validate!!.RetriveSharepreferenceInt(
                VoSpData.vomaxmeetingnumber
            )

        et_tentativeDate.setOnClickListener {
            validate!!.datePickerwithmindate1(
                validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                et_valid_date
            )
        }

        et_valid_date.setOnClickListener {
            validate!!.datePickerwithmindate1(
                validate!!.addmonth(
                    validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                    3, 0
                )!!,
                et_valid_date
            )
        }

        tvShgName.text = validate!!.RetriveSharepreferenceString(VoSpData.voMemberName)


        var name = getRecepitType(validate!!.RetriveSharepreferenceInt(VoSpData.voAuid))
        tv_fund.text = name

        et_sanction_amount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = validate!!.returnIntegerValue(et_proposed_amount.text.toString())
                if (validate!!.returnIntegerValue(s.toString()) > value) {
                    validate!!.CustomAlertEditText(
                        LabelSet.getText(
                            "sanctionamountcannotbegfraeterthanpraposedamount",
                            R.string.sanctionamountcannotbegfraeterthanpraposedamount
                        ),
                        this@VOtoSHGLoanRequest,
                        et_sanction_amount
                    )
                }

            }

        })

        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                saveData()
            }
        }

        btn_cancel.setOnClickListener {
            var intent = Intent(this, VOtoSHGLoanRequestSummary::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        LabelSetText()
        fillloanapplicationdata()

    }

    fun getRecepitType(keyCode: Int?): String? {
        var name: String? = null
        name = mstVOCoaViewmodel.getcoaValue(
            "PL",
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),
            keyCode
        )
        return name
    }

    private fun fillloanapplicationdata() {
        var list = voLoanApplicationViewmodel!!.getmemberLoanApplication(
            validate!!.RetriveSharepreferenceLong(VoSpData.voLoanappid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voShgMemID)
        )
        if (!list.isNullOrEmpty()) {
            et_proposed_amount.setText(validate!!.returnStringValue(list[0].amtDemand.toString()))
            et_sanction_amount.setText(validate!!.returnStringValue(list[0].amtSanction.toString()))
            et_priority.setText(validate!!.returnStringValue(list[0].loanRequestPriority.toString()))
            et_valid_date.setText(validate!!.convertDatetime(list[0].approvalDate))
            et_tentativeDate.setText(validate!!.convertDatetime(list[0].tentativeDate))

            if (validate!!.returnIntegerValue(list[0].amtDisbursed.toString()) > 0) {
                btn_savegray.visibility = View.VISIBLE
                btn_save.visibility = View.GONE
            } else {
                btn_savegray.visibility = View.GONE
                btn_save.visibility = View.VISIBLE
            }
        } else {
            et_valid_date.setText(
                validate!!.convertDatetime(
                    validate!!.addmonth(
                        validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                        3, 0
                    )
                )
            )
            et_tentativeDate.setText(
                validate!!.convertDatetime(
                    validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate)

                )
            )
        }
    }

    private fun saveData() {
        var save = 0
        if (validate!!.RetriveSharepreferenceLong(VoSpData.voLoanappid) > 0) {
            voLoanApplicationViewmodel!!.updateMemberloanapplicationdetail(
                validate!!.RetriveSharepreferenceLong(VoSpData.voLoanappid),
                validate!!.returnIntegerValue(et_proposed_amount.text.toString()),
                validate!!.returnIntegerValue(et_sanction_amount.text.toString()),
                validate!!.returnIntegerValue(et_priority.text.toString()),
                validate!!.Daybetweentime(et_valid_date.text.toString()),
                validate!!.Daybetweentime(et_tentativeDate.text.toString())
            )
            save = 2
        } else {
            var countloanno = voLoanApplicationViewmodel!!.getmaxloanno(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
            )
            var laonno =
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid) + 1000 + countloanno + 1
            voLoanApplicationEntity = VoLoanApplicationEntity(
                0,
                0,
                laonno,
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceLong(VoSpData.voShgMemID),
                validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                0,
                validate!!.returnIntegerValue(et_proposed_amount.text.toString()),
                validate!!.returnIntegerValue(et_sanction_amount.text.toString()),
                0,
                validate!!.Daybetweentime(et_valid_date.text.toString()),
                validate!!.Daybetweentime(et_tentativeDate.text.toString()),
                validate!!.RetriveSharepreferenceInt(VoSpData.ShortDescription),
                validate!!.RetriveSharepreferenceInt(VoSpData.LoanSource),
                0,
                0,
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
                0,
                "",
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                "", 0, "", 0,
                validate!!.returnIntegerValue(et_priority.text.toString())
            )

            voLoanApplicationViewmodel!!.insertFedLoanApplicationData(voLoanApplicationEntity!!)
            save = 1

        }

        if (save == 1) {
            validate!!.CustomAlertVO(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this, VOtoSHGLoanRequestSummary::class.java
            )
        } else {
            validate!!.CustomAlertVO(
                LabelSet.getText(
                    "data_updated_successfully",
                    R.string.data_updated_successfully
                ),
                this, VOtoSHGLoanRequestSummary::class.java
            )
        }
    }

    private fun LabelSetText() {
        tv_proposed_amount.text = LabelSet.getText("proposed_amount", R.string.proposed_amount)
        et_proposed_amount.hint = LabelSet.getText("type_here", R.string.type_here)
        tv_sanction_amount.text = LabelSet.getText("sanction_amount", R.string.sanction_amount)
        tv_priority_no.text = LabelSet.getText("priority_no", R.string.priority_no)
        tv_tentiveDate.text = LabelSet.getText(
            "tentative_date_of_fund_requirement",
            R.string.tentative_date_of_fund_requirement
        )
        tvvalid_date.text = LabelSet.getText("request_valid_up_to", R.string.request_valid_up_to)
        et_sanction_amount.hint = LabelSet.getText("type_here", R.string.type_here)
        et_priority.hint = LabelSet.getText("type_here", R.string.type_here)
        et_tentativeDate.hint = LabelSet.getText("date_format", R.string.date_format)
        et_valid_date.hint = LabelSet.getText("date_format", R.string.date_format)

        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_savegray.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText("cancel", R.string.cancel)
    }

    private fun checkValidation(): Int {

        var priorityCount = voLoanApplicationViewmodel!!.getPriorityCount(
            validate!!.returnIntegerValue(et_priority.text.toString()),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        var value = 1

        if (validate!!.returnIntegerValue(et_proposed_amount.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "proposed_amount",
                    R.string.proposed_amount
                ), this,
                et_proposed_amount
            )
            value = 0
            return value
        } else if (validate!!.returnIntegerValue(et_sanction_amount.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "sanction_amount",
                    R.string.sanction_amount
                ), this,
                et_sanction_amount
            )
            value = 0
            return value
        } else if (validate!!.returnIntegerValue(et_sanction_amount.text.toString()) > validate!!.returnIntegerValue(
                et_proposed_amount.text.toString()
            )
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "sanctionamountcannotbegfraeterthanpraposedamount",
                    R.string.sanctionamountcannotbegfraeterthanpraposedamount
                ), this, et_sanction_amount
            )
            value = 0
            return value

        } else if (validate!!.returnIntegerValue(et_priority.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "priority",
                    R.string.priority
                ), this,
                et_priority
            )
            value = 0
            return value
        } else if (validate!!.RetriveSharepreferenceLong(VoSpData.voLoanappid) == 0L && priorityCount > 0 ||
            validate!!.RetriveSharepreferenceLong(VoSpData.voLoanappid) > 0 && priorityCount > 1
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "valid_priority_num",
                    R.string.valid_priority_num
                ), this,
                et_priority
            )
            value = 0
            return value
        } else if (et_tentativeDate.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "tentative_date_of_fund_requirement",
                    R.string.tentative_date_of_fund_requirement
                ), this,
                et_tentativeDate
            )

            value = 0
            return value
        } else if (et_valid_date.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "request_valid_up_to",
                    R.string.request_valid_up_to
                ), this,
                et_valid_date
            )
            value = 0
            return value
        }

        return value

    }

    override fun onBackPressed() {
        var intent = Intent(this, VOtoSHGLoanRequestSummary::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

}