package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.VoShgLoanRequestSummaryAdapter
import com.microware.cdfi.fragment.VoReceiptsTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.MstVOCOAViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoLoanApplicationViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoMtgDetViewModel
import kotlinx.android.synthetic.main.loan_request_summary_vo_to_shg.*


class VOtoSHGLoanRequestSummary : AppCompatActivity() {

    var validate: Validate? = null
    var voLoanApplicationViewmodel: VoLoanApplicationViewmodel? = null
    lateinit var mstVOCoaViewmodel: MstVOCOAViewmodel
    lateinit var vomtgDetViewmodel: VoMtgDetViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.loan_request_summary_vo_to_shg)

        validate = Validate(this)
        voLoanApplicationViewmodel = ViewModelProviders.of(this).get(VoLoanApplicationViewmodel::class.java)
        mstVOCoaViewmodel =
            ViewModelProviders.of(this).get(MstVOCOAViewmodel::class.java)
        vomtgDetViewmodel = ViewModelProviders.of(this).get(VoMtgDetViewModel::class.java)

        ivADD.isEnabled = validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber) >= validate!!.RetriveSharepreferenceInt(VoSpData.vomaxmeetingnumber)

        ivADD.setOnClickListener {
            validate!!.SaveSharepreferenceLong(VoSpData.voLoanappid, 0)
            var intent = Intent(this, VOtoSHGLoanRequest::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        var name  = getRecepitType(validate!!.RetriveSharepreferenceInt(VoSpData.voAuid))
        tv_fund.text = name

        replaceFragmenty(
            fragment = VoReceiptsTopBarFragment(20),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        fillRecyclerView()
    }

    fun getRecepitType(keyCode: Int?): String? {
        var name: String? = null
        name = mstVOCoaViewmodel.getcoaValue(
            "PL",
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),
            keyCode
        )
        return name
    }

    fun setMember(memId: Long): String {
        var pos = ""
        val list = vomtgDetViewmodel.getListDataMtgByMtgnum(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        if (!list.isNullOrEmpty()) {
            for (i in list.indices) {
                if (memId == list.get(i).memId)
                    pos = list.get(i).childCboName!!
            }
        }

        return pos
    }

    private fun fillRecyclerView() {
        var list = voLoanApplicationViewmodel!!.getLoanApplicationlist(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceLong(VoSpData.voShgMemID),validate!!.RetriveSharepreferenceInt(VoSpData.ShortDescription)
        )
        var totdemand = voLoanApplicationViewmodel!!.gettotaldemand(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceLong(VoSpData.voShgMemID),validate!!.RetriveSharepreferenceInt(VoSpData.ShortDescription)
        )
        tv_amount.text = totdemand.toString()
        if (!list.isNullOrEmpty()) {
            loan_req_summary.layoutManager = LinearLayoutManager(this)
            var loanDemandSectionAdapter =
                VoShgLoanRequestSummaryAdapter(this, list)
            val isize: Int
            isize = list.size
            val params: ViewGroup.LayoutParams = loan_req_summary.layoutParams
            val r = resources
            val px = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.spraying),
                r.displayMetrics
            )
            val hi = Math.round(px)
            var gridHeight = hi * isize
            params.height = gridHeight
            loan_req_summary.layoutParams = params
            loan_req_summary.adapter = loanDemandSectionAdapter
        }
    }

    override fun onBackPressed() {
        var intent = Intent(this,VOtoSHGLoanSummary::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

}