package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.VoShgLoanSummaryAdapter
import com.microware.cdfi.fragment.VoReceiptsTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.MstVOCOAViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoGenerateMeetingViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoLoanApplicationViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoMemLoanScheduleViewModel
import com.microware.cdfi.viewModel.voviewmodel.VoMemLoanViewModel
import kotlinx.android.synthetic.main.buttons_vo.*
import kotlinx.android.synthetic.main.loan_summary_vo_to_shg.*


class VOtoSHGLoanSummary : AppCompatActivity() {
    var validate: Validate? = null

    lateinit var voGenerateMeetingViewmodel: VoGenerateMeetingViewmodel
    lateinit var voMemLoanViewModel: VoMemLoanViewModel
    lateinit var voMemLoanScheduleViewModel: VoMemLoanScheduleViewModel
    lateinit var mstVOCoaViewmodel: MstVOCOAViewmodel
    var voLoanApplicationViewmodel: VoLoanApplicationViewmodel? = null
    var iValue1 = 0
    var iValue2 = 0
    var iValue3 = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.loan_summary_vo_to_shg)

        validate = Validate(this)

        voGenerateMeetingViewmodel =
            ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        voMemLoanViewModel =
            ViewModelProviders.of(this).get(VoMemLoanViewModel::class.java)
        voMemLoanScheduleViewModel =
            ViewModelProviders.of(this).get(VoMemLoanScheduleViewModel::class.java)
        mstVOCoaViewmodel =
            ViewModelProviders.of(this).get(MstVOCOAViewmodel::class.java)
        voLoanApplicationViewmodel = ViewModelProviders.of(this).get(VoLoanApplicationViewmodel::class.java)

        replaceFragmenty(
            fragment = VoReceiptsTopBarFragment(18),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        var name  = getRecepitType(validate!!.RetriveSharepreferenceInt(VoSpData.voAuid))
        tv_fund.text = name
        tv_name.text = validate!!.RetriveSharepreferenceString(VoSpData.voMemberName)

        /*ivADD.setOnClickListener {
            var intent = Intent(this, VOtoSHGLoanRequestSummary::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }*/

        btn_cancel.setOnClickListener {
            var intent = Intent(this, SHGtoVOReceipts::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        setLabelText()
        fillSpinner()
        loanSummaryRecyceler()

        ivADD.setOnClickListener {

            validate!!.SaveSharepreferenceInt(VoSpData.voLoanno,0)

            var intent = Intent(this, VOtoSHGLoanRequestSummary::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
    }

    private fun loanSummaryRecyceler() {
        var list = voGenerateMeetingViewmodel.getloanListDataMtgByMtgnum(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceLong(VoSpData.voShgMemID), validate!!.RetriveSharepreferenceInt(VoSpData.ShortDescription)
        )

        loan_summary.layoutManager = LinearLayoutManager(this)
        var voShgLoanSummaryAdapter = VoShgLoanSummaryAdapter(this, list)


        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = loan_summary.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        loan_summary.layoutParams = params

        loan_summary.adapter = voShgLoanSummaryAdapter
    }

    private fun setLabelText() {
        tv_Loans.text = LabelSet.getText("Loans", R.string.Loans)
//        tv_members_name.setText(LabelSet.getText("shg_name", R.string.shg_name))
        tv_loan_outstanding.text = LabelSet.getText("loan_os", R.string.loan_os)
        tv_loan_request_rs.text = LabelSet.getText("loan_request_rs", R.string.loan_request_rs)
        tv_loan_payment_rs.text = LabelSet.getText("amount_disbursed", R.string.amount_disbursed)
        tv_total.text = LabelSet.getText("total", R.string.total)

        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText("cancel", R.string.cancel)
    }

    private fun fillSpinner(){

    }


    fun getRecepitType(keyCode: Int?): String? {
        var name: String? = null
        name = mstVOCoaViewmodel.getcoaValue(
            "PL",
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),
            keyCode
        )
        return name
    }

    fun getTotalValue(Value1: Int, Value2: Int, Value3: Int) {
        iValue1 += Value1
        iValue2 += Value2
        iValue3 += Value3
        tv_toal_loan_outstanding.text = iValue1.toString()
        tv_total_loan_request.text = iValue2.toString()
        tv_total_loan_payment.text = iValue3.toString()

    }


   /* fun gettotalloanamt(loanno: Int, memid: Long, mtgguid: String): Int {
        return voMemLoanScheduleViewModel!!.gettotalloanamt(loanno, memid, mtgguid)
    }

    fun getLoanno(appid: Long): Int {
        return voMemLoanViewModel!!.getLoanno(appid)
    }*/

    fun getTotalLoanCount(mtgNum:Int,mem_id: Long):Int{
        return voLoanApplicationViewmodel!!.getTotalLoanCount(mtgNum,mem_id)
    }

    fun getAmountPaid(mtgNum:Int,mem_id: Long):Int{
        return voMemLoanViewModel.getAmountPaid(mtgNum, mem_id)
    }

   /* fun gettotaldemand(mtgNum:Int,shgid:Long,mem_id: Long):Int{
        return voLoanApplicationViewmodel!!.gettotaldemand(mtgNum, shgid, mem_id,validate!!.RetriveSharepreferenceInt(VoSpData.ShortDescription)
        )
    }*/

    fun gettotaloutstanding(mtgNum:Int,shgid:Long,mem_id: Long):Int{
        return voLoanApplicationViewmodel!!.getLoanOutstanding(mtgNum, shgid, mem_id)
    }

    fun getLoanno(appid: Long): Int {
        return voMemLoanViewModel.getLoanno(appid)
    }
    override fun onBackPressed() {
        var intent = Intent(this, SHGtoVOReceipts::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

}