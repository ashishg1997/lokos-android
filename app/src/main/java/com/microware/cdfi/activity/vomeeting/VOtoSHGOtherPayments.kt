package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.entity.MstCOAEntity
import com.microware.cdfi.entity.voentity.VoFinTxnDetMemEntity
import com.microware.cdfi.fragment.VoReceiptsTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoFinTxnDetMemViewModel
import kotlinx.android.synthetic.main.activity_cut_off_vo_investment.*
import kotlinx.android.synthetic.main.buttons_vo.*
import kotlinx.android.synthetic.main.loan_repayment_vo_to_shg.*
import kotlinx.android.synthetic.main.other_payments_vo_to_shg.*
import kotlinx.android.synthetic.main.other_payments_vo_to_shg.et_amount_paid
import kotlinx.android.synthetic.main.other_payments_vo_to_shg.et_date_of_amount_paid
import kotlinx.android.synthetic.main.other_payments_vo_to_shg.tv_amount_paid
import kotlinx.android.synthetic.main.other_payments_vo_to_shg.tv_date_of_amount_paid

class VOtoSHGOtherPayments : AppCompatActivity() {
    var validate:Validate?=null
    lateinit var voFinTxnDetMemViewModel: VoFinTxnDetMemViewModel
    var voFinTxnDetMemEntity:VoFinTxnDetMemEntity?=null
    var dataspin_payment_type:List<MstCOAEntity>? = null
    lateinit var lookupViewmodel: LookupViewmodel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.other_payments_vo_to_shg)
        validate= Validate(this)
        voFinTxnDetMemViewModel= ViewModelProviders.of(this).get(VoFinTxnDetMemViewModel::class.java)
        lookupViewmodel= ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        setLabel()
        replaceFragmenty(
            fragment = VoReceiptsTopBarFragment(23),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )
        validate = Validate(this)
        et_date_of_amount_paid.setOnClickListener {
            validate!!.datePicker(et_date_of_amount_paid)
        }

        btn_save.setOnClickListener {
            if (checkValidation() == 1){
                saveData()
            }

            validate!!.CustomAlert(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ), this, VOtoSHGWithdrawal::class.java
            )

        }

        btn_cancel.setOnClickListener {
            var intent = Intent(this, VOtoOthersHrCaderPayments::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }
        fillSpinner()
        showData()
    }

    override fun onBackPressed() {
        var intent = Intent(this, VOtoOthersHrCaderPayments::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

    private fun saveData(){
        voFinTxnDetMemEntity = VoFinTxnDetMemEntity(
            0,
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            0,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.returnSubHeadcode(spin_fund_payment_type,dataspin_payment_type),//auid
            "OP",
            validate!!.returnIntegerValue(et_amount_paid.text.toString()),
            0,
            validate!!.Daybetweentime(et_date_of_amount_paid.text.toString()),
            "",
            "",

            "",
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            "",
            0,
            "",
            0,0
        )
        voFinTxnDetMemViewModel.insertVoGroupLoanSchedule(voFinTxnDetMemEntity!!)
    }

    private fun showData(){
        val list =
            voFinTxnDetMemViewModel.getVoFinTxnDetMemList((validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!)
                ,validate!!.RetriveSharepreferenceLong(VoSpData.voshgid))

        if (!list.isNullOrEmpty() && list.size > 0) {

            spin_fund_payment_type.setSelection(validate!!.returnSubHeadpos(list.get(0).auid,dataspin_payment_type))

            var amout = validate!!.returnIntegerValue(list.get(0).amount.toString())
            et_amount_paid.setText(amout.toString())

            var date = validate!!.returnLongValue(list.get(0).dateRealisation.toString())
            et_date_of_amount_paid.setText(validate!!.convertDatetime(date))
        }
    }

    private fun checkValidation(): Int {
        var value = 1
        if (spin_fund_payment_type.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_fund_payment_type, LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "fund_payment_type",
                    R.string.fund_payment_type
                )
            )
            value = 0
            return value
        }
        if (validate!!.returnStringValue(et_amount_paid.text.toString()).length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "amount_paid",
                    R.string.amount_paid
                ), this, et_amount_paid
            )
            value = 0
            return value
        }
        if (validate!!.returnStringValue(et_date_of_amount_paid.text.toString()).length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "date_of_amount_paid",
                    R.string.date_of_amount_paid
                ), this, et_date_of_amount_paid
            )
            value = 0
            return value
        }

        return value
    }

    private fun setLabel() {
        btn_save.text = LabelSet.getText("confirm", R.string.confirm)
        btn_cancel.text = LabelSet.getText("cancel", R.string.cancel)

        tv_fund_payment_type.text = LabelSet.getText("fund_payment_type",R.string.fund_payment_type)
        tv_date_of_amount_paid.text = LabelSet.getText("date_of_amount_paid",R.string.date_of_amount_paid)
        tv_amount_paid.text = LabelSet.getText("date_of_amount_received",R.string.amount_paid)

        et_amount_paid.hint = LabelSet.getText("enter_amount",R.string.enter_amount)
        et_date_of_amount_paid.hint = LabelSet.getText("date_format",R.string.date_format)
    }

    private fun fillSpinner() {
        dataspin_payment_type = voFinTxnDetMemViewModel.getCoaSubHeadData(listOf(18,39,29,31),"OP",validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!)

        validate!!.fillCoaSubHeadspinner(this,spin_fund_payment_type,dataspin_payment_type)

    }


}