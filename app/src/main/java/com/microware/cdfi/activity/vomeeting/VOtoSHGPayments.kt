package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.VOtoSHGPaymentsBankAdapter
import com.microware.cdfi.adapter.vomeetingadapter.VoShgPaymentAdapter
import com.microware.cdfi.fragment.VoReceiptsTopBarFragment
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import com.microware.cdfi.utility.replaceFragmenty
import com.microware.cdfi.viewModel.CboBankViewmodel
import com.microware.cdfi.viewModel.MasterBankViewmodel
import com.microware.cdfi.viewModel.MstCOAViewmodel
import kotlinx.android.synthetic.main.buttons_vo.*
import kotlinx.android.synthetic.main.vo_to_shg_payments.*

class VOtoSHGPayments : AppCompatActivity() {

    var mstCOAViewmodel: MstCOAViewmodel? = null
    var cboBankViewModel: CboBankViewmodel? = null
    var bankMasterViewModel: MasterBankViewmodel? = null
    var validate: Validate? = null
    var cboType = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.vo_to_shg_payments)

        validate = Validate(this)
        if (validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType) == 2) {
            cboType = 2
        } else {
            cboType = 1
        }


        replaceFragmenty(
            fragment = VoReceiptsTopBarFragment(17),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        mstCOAViewmodel = ViewModelProviders.of(this).get(MstCOAViewmodel::class.java)
        cboBankViewModel = ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        bankMasterViewModel = ViewModelProviders.of(this).get(MasterBankViewmodel::class.java)


        btn_save.setOnClickListener {
            var intent = Intent(this, VOtoSHGLoanSummary::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        btn_cancel.setOnClickListener {
            var intent = Intent(this, VoMeetingMenuActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        fillRecyclerView()
        fillRecyclerView1()

    }


    private fun fillRecyclerView() {
        var coaData = mstCOAViewmodel!!.getCoaSubHeadData(
            listOf<Int>(49,31), "OP", validate!!.RetriveSharepreferenceString(
                AppSP.Langaugecode
            )!!
        )
        /* var coaData = mstCOAViewmodel!!.getReceiptCoaSubHeadData(
             "OP",
             "OE",
             validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
         )*/
        if (!coaData.isNullOrEmpty()) {
            rvList.layoutManager = LinearLayoutManager(this)
            rvList.adapter = VoShgPaymentAdapter(this, coaData)
        }
    }
    private fun fillRecyclerView1() {
        val bankList = cboBankViewModel!!.getcboBankdata(validate!!.RetriveSharepreferenceString(
            VoSpData.voSHGGUID),cboType)
       if (!bankList.isNullOrEmpty()){
           rvBankList.layoutManager = LinearLayoutManager(this)
           rvBankList.adapter = VOtoSHGPaymentsBankAdapter(this,bankList,mstCOAViewmodel)
       }
    }

    fun getBankName(bankID : Int?): String?{
        var value: String? = null
        value = bankMasterViewModel!!.getBankName(bankID!!)
        return value
    }

    override fun onBackPressed() {
        var intent = Intent(this, VoMeetingMenuActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }
}