package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.entity.voentity.VoFinTxnDetMemEntity
import com.microware.cdfi.fragment.VoReceiptsTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoFinTxnDetMemViewModel
import kotlinx.android.synthetic.main.buttons_vo.*
import kotlinx.android.synthetic.main.withdrawal_vo_to_shg.*

class VOtoSHGWithdrawal : AppCompatActivity() {
    var validate: Validate? = null
    var dataspin_fund_type: List<LookupEntity>? = null
    var voFinTxnDetMemEntity: VoFinTxnDetMemEntity? = null

    lateinit var lookupViewmodel: LookupViewmodel
    lateinit var voFinTxnDetMemViewModel: VoFinTxnDetMemViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.withdrawal_vo_to_shg)
        replaceFragmenty(
            fragment = VoReceiptsTopBarFragment(24),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )
        validate = Validate(this)

        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        voFinTxnDetMemViewModel =
            ViewModelProviders.of(this).get(VoFinTxnDetMemViewModel::class.java)

        et_date_of_amount_paid.setOnClickListener {
            validate!!.datePicker(et_date_of_amount_paid)
        }

        btn_cancel.setOnClickListener {
            var intent = Intent(this, VOtoSHGOtherPayments::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                saveData()
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "data_saved_successfully",
                        R.string.data_saved_successfully
                    ), this, VOtoSHGPayments::class.java
                )
            }
        }

        ivADD.setOnClickListener {
            clearAll()
        }

        setLabelText()
        fillSpinner()
//        showData()
    }

    fun setLabelText() {
        tv_fund_type.text = LabelSet.getText(
            "fund_type",
            R.string.fund_type
        )

        tv_total_saving.text = LabelSet.getText(
            "total_voluntary_saving_compulsory_saving_share_capital",
            R.string.total_voluntary_saving_compulsory_saving_share_capital
        )

        tv_total_amount_withdrawn.text = LabelSet.getText(
            "total_amount_withdrawn",
            R.string.total_amount_withdrawn
        )

        tv_date_of_amount_paid.text = LabelSet.getText(
            "date_of_amount_paid",
            R.string.date_of_amount_paid
        )

        et_total_saving.hint = LabelSet.getText(
            "auto",
            R.string.auto
        )

        et_total_amount_withdrawn.hint = LabelSet.getText(
            "enter_amount",
            R.string.enter_amount
        )

        et_date_of_amount_paid.hint = LabelSet.getText(
            "date_format",
            R.string.date_format
        )

        tv_total.text = LabelSet.getText(
            "total",
            R.string.total
        )

        btn_save.text = LabelSet.getText(
            "confirm",
            R.string.confirm
        )
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }

    private fun fillSpinner() {
        dataspin_fund_type = lookupViewmodel.getlookupMasterdata(
            71, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode), listOf(0, 1, 2, 3)
        )
        validate!!.fillspinner(this, spin_fund_type, dataspin_fund_type)
    }

    private fun checkValidation(): Int {
        var value = 1
        if (spin_fund_type.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_fund_type, LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "fund_type",
                    R.string.fund_type
                )
            )
            value = 0
            return value
        }
        if (validate!!.returnStringValue(et_total_saving.text.toString()).length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "total_voluntary_saving_compulsory_saving_share_capital",
                    R.string.total_voluntary_saving_compulsory_saving_share_capital
                ), this, et_total_saving
            )
            value = 0
            return value
        }
        if (validate!!.returnStringValue(et_total_amount_withdrawn.text.toString()).length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "total_amount_withdrawn",
                    R.string.total_amount_withdrawn
                ), this, et_total_amount_withdrawn
            )
            value = 0
            return value
        }
        if (validate!!.returnStringValue(et_date_of_amount_paid.text.toString()).length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "date_of_amount_paid",
                    R.string.date_of_amount_paid
                ), this, et_date_of_amount_paid
            )
            value = 0
            return value
        }

        return value
    }

    private fun saveData() {
        //missing fields : 3
        //Total Saving
        //Total Amount Withdrawn
        //fund type
        voFinTxnDetMemEntity = VoFinTxnDetMemEntity(
            0,
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            0,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            0,
            "",
            0,
            0,
            validate!!.Daybetweentime(et_date_of_amount_paid.text.toString()),
            "",
            "",

            "",
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            "",
            0,
            "",
            0,0
        )
        voFinTxnDetMemViewModel.insertVoGroupLoanSchedule(voFinTxnDetMemEntity!!)
    }

    /*private fun showData() {
        var list =
            voFinTxnDetMemViewModel!!.getVoFinTxnDetMemList(
                (validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
            )

        if (!list.isNullOrEmpty() && list.size > 0) {

            *//*spin_fund_type.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(list.get(0).fundType.toString()),
                    dataspin_fund_type
                )
            )

            et_total_saving.setText(validate!!.returnIntegerValue(list.get(0).amount.toString()))
            et_total_amount_withdrawn.setText(validate!!.returnIntegerValue(list.get(0).amount.toString()))*//*

            et_date_of_amount_paid.setText(
                validate!!.convertDatetime(
                    validate!!.returnLongValue(
                        list.get(0).dateRealisation.toString()
                    )
                )
            )
        }
    }*/

    private fun clearAll(){
        spin_fund_type.setSelection(0)
        et_total_saving.setText("")
        et_total_amount_withdrawn.setText("")
        et_date_of_amount_paid.setText("")
    }

    override fun onBackPressed() {
        var intent = Intent(this, VOtoSHGOtherPayments::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        overridePendingTransition(0, 0)
    }
}