package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.entity.voentity.VoFinTxnDetGrpEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.VoSpData
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.voviewmodel.VoFinTxnDetGrpViewModel
import com.microware.cdfi.viewModel.voviewmodel.VoFinTxnViewModel
import com.microware.cdfi.viewModel.voviewmodel.VoGenerateMeetingViewmodel
import kotlinx.android.synthetic.main.activity_vobank_transfer.*
import kotlinx.android.synthetic.main.buttons_vo.*
import kotlinx.android.synthetic.main.votoolbar.*

class VoBankTransferActivity : AppCompatActivity() {

    var validate: Validate? = null
    var generateMeetingViewmodel: VoGenerateMeetingViewmodel? = null
    var voFinTxnViewmodel: VoFinTxnViewModel? = null
    var voFinTxnDetGrpEntity: VoFinTxnDetGrpEntity? = null
    var voFinTxnDetGrpViewModel: VoFinTxnDetGrpViewModel? = null
    var shgBankList: List<Cbo_bankEntity>? = null

    var nullStringValue:String?=null
    var nullLongValue:Long?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vobank_transfer)
        validate = Validate(this)
        generateMeetingViewmodel = ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        voFinTxnViewmodel = ViewModelProviders.of(this).get(VoFinTxnViewModel::class.java)
        voFinTxnDetGrpViewModel = ViewModelProviders.of(this).get(VoFinTxnDetGrpViewModel::class.java)

        tv_title.text = LabelSet.getText("bank_transfer",R.string.bank_transfer)
        IvScan.visibility = View.INVISIBLE
        IVSync.visibility = View.INVISIBLE

        icBack.setOnClickListener {
            var intent = Intent(this, VOBankTransferListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                SaveData()
            }
        }

        btn_cancel.setOnClickListener {
            var intent = Intent(this, VOBankTransferListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        fillBankFrom()
        fillBankTo()
        showData()

    }

    private fun showData() {
       if(validate!!.RetriveSharepreferenceString(VoSpData.bankFrom).toString().trim().length>0 && validate!!.RetriveSharepreferenceString(
               VoSpData.bankTo
           ).toString().trim().length>0){
           setBankFrom(validate!!.RetriveSharepreferenceString(VoSpData.bankFrom)!!)
           setBankTo(validate!!.RetriveSharepreferenceString(VoSpData.bankTo)!!)
           et_Amount.setText(validate!!.RetriveSharepreferenceString(VoSpData.amountTransferred))
           btn_save.visibility = View.GONE
           btn_savegray.visibility = View.VISIBLE
       }

    }

    private fun SaveData() {
        voFinTxnDetGrpEntity = VoFinTxnDetGrpEntity(
            0,
            0,
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            42,
            0,
            0,/*according to flag 75 lookup master sent bank value*/
            0,
            "RO",
            validate!!.returnIntegerValue(et_Amount.text.toString()),
            "",
            validate!!.Daybetweentime(validate!!.currentdatetime),
            0,
            returnBankFromID(),
            "",
            0,
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            nullStringValue, nullLongValue, nullStringValue, nullLongValue,
            1,
            validate!!.RetriveSharepreferenceInt(VoSpData.voOrgType)
        )
        voFinTxnDetGrpViewModel!!.insertVoGroupLoanSchedule(voFinTxnDetGrpEntity!!)

        voFinTxnDetGrpEntity = VoFinTxnDetGrpEntity(
            0,
            0,
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            41,
            0,
            0,/*according to flag 75 lookup master sent bank value*/
            0,
            "PO",
            validate!!.returnIntegerValue(et_Amount.text.toString()),
            "",
            validate!!.Daybetweentime(validate!!.currentdatetime),
            0,
            returnBankToID(),
            "",0,
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            nullStringValue, nullLongValue, nullStringValue, nullLongValue,1,
            validate!!.RetriveSharepreferenceInt(VoSpData.voOrgType)
        )
        voFinTxnDetGrpViewModel!!.insertVoGroupLoanSchedule(voFinTxnDetGrpEntity!!)

        validate!!.CustomAlertVO(
            LabelSet.getText(
                "data_saved_successfully",
                R.string.data_saved_successfully
            ),
            this, VOBankTransferListActivity::class.java
        )
    }

    private fun fillBankTo() {
        shgBankList =
            generateMeetingViewmodel!!.getBankdata(validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID))

        val adapter: ArrayAdapter<String?>
        if (!shgBankList.isNullOrEmpty()) {
            val isize = shgBankList!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in shgBankList!!.indices) {
                var lastthree =
                    shgBankList!![i].account_no.substring(shgBankList!![i].account_no.length - 3)
                sValue[i + 1] =
                    shgBankList!![i].ifsc_code.toString().dropLast(7) + "XXXXX" + lastthree
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_bankTo.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_bankTo.adapter = adapter
        }
    }

    private fun returnBankToID(): String {
        var pos = spin_bankTo.selectedItemPosition
        var id = ""

        if (!shgBankList.isNullOrEmpty()) {
            if (pos > 0) id =
                shgBankList!!.get(pos - 1).ifsc_code!!.dropLast(7) + shgBankList!!.get(pos - 1).account_no
        }
        return id
    }

    private fun returnBankFromID(): String {
        var pos = spin_bankFrom.selectedItemPosition
        var id = ""

        if (!shgBankList.isNullOrEmpty()) {
            if (pos > 0) id =
                shgBankList!!.get(pos - 1).ifsc_code!!.dropLast(7) + shgBankList!!.get(pos - 1).account_no
        }
        return id
    }

    private fun fillBankFrom() {

        shgBankList =
            generateMeetingViewmodel!!.getBankdata(validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID))

        val adapter: ArrayAdapter<String?>
        if (!shgBankList.isNullOrEmpty()) {
            val isize = shgBankList!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in shgBankList!!.indices) {
                var lastthree =
                    shgBankList!![i].account_no.substring(shgBankList!![i].account_no.length - 3)
                sValue[i + 1] =
                    shgBankList!![i].ifsc_code.toString().dropLast(7) + "XXXXX" + lastthree
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_bankFrom.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_bankFrom.adapter = adapter
        }
    }

    private fun checkValidation(): Int {
        var value = 1

        var cashinbank=generateMeetingViewmodel!!.getclosingbal(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            returnBankFromID())

        if (spin_bankFrom.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_bankFrom,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "bank",
                    R.string.bank
                )
            )
            value = 0
        } else if (spin_bankTo.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_bankTo,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "bank",
                    R.string.bank
                )
            )
            value = 0
        } else if (returnBankFromID() == returnBankToID()) {
            validate!!.CustomAlertSpinner(
                this,spin_bankTo,
                LabelSet.getText(
                    "bank_from_and_bank_to",
                    R.string.bank_from_and_bank_to
                )
            )
            value = 0
        }  else if (et_Amount.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "amount",
                    R.string.amount
                ),
                this, et_Amount
            )
            value = 0
        }else if (cashinbank == 0){
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "intheselectedbank",
                    R.string.intheselectedbank
                ), this,
                et_Amount
            )
            value = 0
        }else if (cashinbank < validate!!.returnIntegerValue(et_Amount.text.toString())){
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "intheselectedbank",
                    R.string.intheselectedbank
                ), this,
                et_Amount
            )
            value = 0
        }

        return value
    }

    fun setBankFrom(bankCode:String): Int {

        var pos = 0

        if (!shgBankList.isNullOrEmpty()) {
            for (i in shgBankList!!.indices) {
                if (bankCode.equals(
                        shgBankList!!.get(i).ifsc_code!!.dropLast(7) + shgBankList!!.get(
                            i
                        ).account_no
                    )
                )
                    pos = i + 1
            }
        }
        spin_bankFrom.setSelection(pos)
        return pos
    }

    fun setBankTo(bankCode:String): Int {
        var pos = 0

        if (!shgBankList.isNullOrEmpty()) {
            for (i in shgBankList!!.indices) {
                if (bankCode.equals(
                        shgBankList!!.get(i).ifsc_code!!.dropLast(7) + shgBankList!!.get(
                            i
                        ).account_no
                    )
                )
                    pos = i + 1
            }
        }
        spin_bankTo.setSelection(pos)
        return pos
    }

    override fun onBackPressed() {
        var intent = Intent(this, VOBankTransferListActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }


}