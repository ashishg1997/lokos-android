package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.api.meetingmodel.FundTypeModel
import com.microware.cdfi.entity.voentity.VoMemLoanEntity
import com.microware.cdfi.entity.voentity.VoMemLoanTxnEntity
import com.microware.cdfi.entity.voentity.VoMtgDetEntity
import com.microware.cdfi.fragment.VoCutOffTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.LoanProductViewModel
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoMemLoanTxnViewModel
import com.microware.cdfi.viewModel.voviewmodel.VoMemLoanViewModel
import com.microware.cdfi.viewModel.voviewmodel.VoMtgDetViewModel
import kotlinx.android.synthetic.main.activity_vo_cut_off_close_loan_voto_shg.*
import kotlinx.android.synthetic.main.buttons_vo.*

class VoCutOffCloseLoanVOtoSHG : AppCompatActivity() {
    var validate: Validate? = null
    var memberlist: List<VoMtgDetEntity>? = null
    var voMemLoanEntity: VoMemLoanEntity? = null
    var voMemLoanTxnEntity: VoMemLoanTxnEntity? = null
    var dataspin_fund: List<FundTypeModel>? = null

    lateinit var voMemLoanViewModel: VoMemLoanViewModel
    lateinit var voMemLoanTxnViewModel: VoMemLoanTxnViewModel
    lateinit var vomtgDetViewmodel: VoMtgDetViewModel
    lateinit var lookupViewmodel: LookupViewmodel
    var loanProductViewmodel: LoanProductViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vo_cut_off_close_loan_voto_shg)

        validate = Validate(this)

        voMemLoanViewModel = ViewModelProviders.of(this).get(VoMemLoanViewModel::class.java)
        voMemLoanTxnViewModel = ViewModelProviders.of(this).get(VoMemLoanTxnViewModel::class.java)
        vomtgDetViewmodel = ViewModelProviders.of(this).get(VoMtgDetViewModel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        loanProductViewmodel = ViewModelProviders.of(this).get(LoanProductViewModel::class.java)

        replaceFragmenty(
            fragment = VoCutOffTopBarFragment(7),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        et_disbursmentDate.setOnClickListener {
            validate!!.datePicker(et_disbursmentDate)
        }

        btn_cancel.setOnClickListener {
            val intent = Intent(this, VoCutOffClosedLoanVotoShgSummary::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        }

        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                saveData()
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "data_saved_successfully",
                        R.string.data_saved_successfully
                    ), this, VoCutOffClosedLoanVotoShgSummary::class.java
                )
            }
        }

        tv_summary.setOnClickListener {
            val intent = Intent(this, VoCutOffClosedLoanVotoShgSummary::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        }

        setLabelText()
        fillmemberspinner()
        fillSpinner()
        showData()
    }

    private fun setLabelText() {
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
        tv_shg_name.text = LabelSet.getText("shg_name", R.string.shg_name)
        tv_fund_type.text = LabelSet.getText("fund_type", R.string.fund_type)
        tv_loan_no.text = LabelSet.getText("loan_no", R.string.loan_no)
        tv_disbursement_date.text = LabelSet.getText(
            "loan_disbursement_date",
            R.string.loan_disbursement_date
        )
        tv_total_loan_given.text = LabelSet.getText(
            "total_loan_given_amount",
            R.string.total_loan_given_amount
        )
        tv_total_loan_paid.text = LabelSet.getText("total_loan_paid", R.string.total_loan_paid)
        tv_summary.text = LabelSet.getText("see_summary", R.string.see_summary)

        et_LoanNum.hint = LabelSet.getText("type_here", R.string.type_here)
        et_disbursmentDate.hint = LabelSet.getText("date_format", R.string.date_format)
        et_LoanGivenAmt.hint = LabelSet.getText("type_here", R.string.type_here)
        et_loanPaidAmt.hint = LabelSet.getText("type_here", R.string.type_here)
    }

    private fun fillmemberspinner() {
        memberlist = vomtgDetViewmodel.getListDataMtgByMtgnum(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        val adapter: ArrayAdapter<String?>
        if (!memberlist.isNullOrEmpty()) {
            val isize = memberlist!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in memberlist!!.indices) {
                sValue[i + 1] = memberlist!![i].childCboName
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_shgName.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(memberlist!!.size + 1)
            sValue[0] = LabelSet.getText(
                "nomemberavailable",
                R.string.nomemberavailable
            )
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_shgName.adapter = adapter
        }
        setMember()

    }

    fun returnmemid(): Long {

        var pos = spin_shgName.selectedItemPosition
        var id = 0L

        if (!memberlist.isNullOrEmpty()) {
            if (pos > 0)
                id = memberlist!!.get(pos - 1).memId
        }
        return id
    }

    fun setMember(): Int {
        var pos = 0
        var name = ""
        if (!memberlist.isNullOrEmpty()) {
            for (i in memberlist!!.indices) {
                if (validate!!.RetriveSharepreferenceLong(VoSpData.voShgMemID) == memberlist!!.get(i).memId) {
                    pos = i + 1
                    name = memberlist!!.get(i).childCboName!!
                }

            }
        }
        spin_shgName.setSelection(pos)

        var shgName = spin_shgName.setSelection(pos)

        validate!!.SaveSharepreferenceString(VoSpData.voMemberName, shgName.toString())
        // spin_shgName.isEnabled = false
        return pos
    }

    fun fillSpinner() {
        dataspin_fund = loanProductViewmodel!!.getFundTypeBySource_Receipt(
            3, 2
        )

        validate!!.fillFundType(this, spin_Fund_type, dataspin_fund)

    }

    private fun checkValidation(): Int {

        var value = 1
        if (spin_shgName.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_shgName,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "shg_name",
                    R.string.shg_name
                )

            )
            value = 0
            return value
        }

        if (spin_Fund_type.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_Fund_type,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "fund_type",
                    R.string.fund_type
                )

            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_LoanNum.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "pleaseenterloanno",
                    R.string.pleaseenterloanno
                ), this,
                et_LoanNum
            )
            value = 0
            return value
        }

        if (validate!!.returnStringValue(et_disbursmentDate.text.toString()).length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "loan_disbursement_date",
                    R.string.loan_disbursement_date
                ), this,
                et_disbursmentDate
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_LoanGivenAmt.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "total_loan_given_amount",
                    R.string.total_loan_given_amount
                ), this,
                et_LoanGivenAmt
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_loanPaidAmt.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "total_loan_paid",
                    R.string.total_loan_paid
                ), this,
                et_loanPaidAmt
            )
            value = 0
            return value
        }
        if (validate!!.returnIntegerValue(et_LoanGivenAmt.text.toString()) != validate!!.returnIntegerValue(et_loanPaidAmt.text.toString())) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "loan_repaid_disbursed",
                    R.string.loan_repaid_disbursed
                ), this,
                et_loanPaidAmt
            )
            value = 0
            return value
        }
        return value
    }

    //Loan paid amount to be auto calculated and not saved
    private fun saveData() {
        var loan_application_id: Long? = null
        var loan_product_id: Int? = null
        voMemLoanEntity = VoMemLoanEntity(
            0,
            0,
            loan_application_id,
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            returnmemid(),
            validate!!.returnIntegerValue(et_LoanNum.text.toString()),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.addmonth(
                validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                0, 0
            ),
            validate!!.returnIntegerValue(et_LoanGivenAmt.text.toString()),
            validate!!.returnIntegerValue(et_loanPaidAmt.text.toString()),
            0,
            validate!!.returnFundTypeId(spin_Fund_type, dataspin_fund),
            0.0,
            0,
            0,
            0,
            true,//for closed laon vo to shg
            0,
            0,
            0,
            "",
            "",
            0,
            0,
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            "",
            0,
            "",
            0,
            0,
            0,
            validate!!.Daybetweentime(et_disbursmentDate.text.toString())
            ,0,0,0.0,0,1,1
        )
        voMemLoanViewModel.insertVoMemLoan(voMemLoanEntity!!)

        // insert in voloanmemtxn

        voMemLoanTxnEntity = VoMemLoanTxnEntity(
            0,
            0,
            0,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            returnmemid(),
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.returnIntegerValue(et_LoanNum.text.toString()),
            validate!!.returnIntegerValue(et_LoanGivenAmt.text.toString()),
            validate!!.returnIntegerValue(et_loanPaidAmt.text.toString()),
            0,
            0,
            0,
            true,// for closed loan vo to shg
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            "",
            "",
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            "",
            0,
            "",
            0,
            validate!!.returnIntegerValue(et_LoanNum.text.toString())
            ,0.0,0
        )
        voMemLoanTxnViewModel.insert(voMemLoanTxnEntity!!)

    }

    private fun showData() {
        val loanlist =
            voMemLoanViewModel.getLoanDetailData(
                validate!!.RetriveSharepreferenceInt(VoSpData.voLoanno),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceLong(VoSpData.voShgMemID),
                validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!
            )
        if (!loanlist.isNullOrEmpty()) {
            spin_shgName.isEnabled = false
            et_LoanNum.setText(loanlist.get(0).loanNo.toString())
            et_LoanGivenAmt.setText(validate!!.returnStringValue(loanlist.get(0).originalLoanAmount.toString()))
            et_loanPaidAmt.setText(validate!!.returnStringValue(loanlist.get(0).amount.toString()))
            et_disbursmentDate.setText(
                validate!!.convertDatetime(
                    validate!!.returnLongValue(
                        loanlist.get(0).disbursementDate.toString()
                    )
                )
            )

            spin_Fund_type.setSelection(
                validate!!.setFundType(
                    validate!!.returnIntegerValue(
                        loanlist.get(0).fundType.toString()
                    ), dataspin_fund
                )
            )

            btn_save.visibility = View.GONE
            btn_savegray.visibility = View.VISIBLE

        } else {
            et_disbursmentDate.setText(
                validate!!.convertDatetime(
                    validate!!.returnLongValue(
                        validate!!.RetriveSharepreferenceLong(
                            VoSpData.voCurrentMtgDate
                        ).toString()
                    )
                )
            )
            var loanno = voMemLoanViewModel.getCutOffmaxLoanno(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
            )
            et_LoanNum.setText((loanno + 1).toString())

        }

    }

    override fun onBackPressed() {
        val intent = Intent(this, VoCutOffClosedLoanVotoShgSummary::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }
}