package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.VoCutOffClosedLoanDetailAdapter
import com.microware.cdfi.api.vomodel.VoLoanListModel
import com.microware.cdfi.fragment.VoCutOffTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.FundTypeViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoGenerateMeetingViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoMemLoanViewModel
import kotlinx.android.synthetic.main.activity_vo_cut_off_closed_loan_voto_shg_summary.*

class VoCutOffClosedLoanDetailList : AppCompatActivity() {

    var validate: Validate? = null
    lateinit var voGenerateMeetingViewmodel: VoGenerateMeetingViewmodel
    lateinit var voMemLoanViewModel: VoMemLoanViewModel
    lateinit var funTypeViewModel: FundTypeViewmodel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vo_cut_off_closed_loan_voto_shg_summary)

        validate = Validate(this)

        voGenerateMeetingViewmodel =
            ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        voMemLoanViewModel = ViewModelProviders.of(this).get(VoMemLoanViewModel::class.java)
        funTypeViewModel =
            ViewModelProviders.of(this).get(FundTypeViewmodel::class.java)


        replaceFragmenty(
            fragment = VoCutOffTopBarFragment(6),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        btn_cancel.setOnClickListener {
            val intent = Intent(this, VoCutOffClosedLoanVotoShgSummary::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        }

        setLabel()
        fillRecyclerView()
    }

    override fun onBackPressed() {
        val intent = Intent(this, VoCutOffClosedLoanVotoShgSummary::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

    fun setLabel() {
        tblButton.visibility = View.GONE
        img_Add.visibility = View.GONE
        tv_fund_type.visibility = View.VISIBLE
        tv_fund.visibility = View.GONE
        tv_sno.text = LabelSet.getText("sr_no", R.string.sr_no)
        tv_shg_name.text = LabelSet.getText("shg_name", R.string.shg_name)
        tv_fund_type.text = LabelSet.getText("fund_type", R.string.fund_type)
        tv_closed_loan.text = LabelSet.getText("total_closed_loans", R.string.total_closed_loans)
        tv_loan_amount.text = LabelSet.getText("loan_amount_s", R.string.loan_amount_s)
        tv_total.text = LabelSet.getText("total", R.string.total)
        btn_cancel.text = LabelSet.getText("cancel", R.string.cancel)
        btn_save.text = LabelSet.getText("save", R.string.save)
    }

    private fun fillRecyclerView() {

        var list = voGenerateMeetingViewmodel.getClosedMemberLoanByMemberId(
            validate!!.RetriveSharepreferenceInt(
                VoSpData.vocurrentmeetingnumber
            ), validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            true,
            validate!!.RetriveSharepreferenceLong(VoSpData.voShgMemID)
        )

        getTotal(list)

        rvList.layoutManager = LinearLayoutManager(this)
        var voCutOffClosedLoanDetailAdapter =
            VoCutOffClosedLoanDetailAdapter(this, list)

        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = rvList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvList.layoutParams = params

        rvList.adapter = voCutOffClosedLoanDetailAdapter
    }

    fun getTotal(list: List<VoLoanListModel>) {

        var totalLoans = 0
        var totalLoanAmount = 0
        for (i in 0 until list.size) {
            totalLoanAmount += validate!!.returnIntegerValue(list.get(i).original_loan_amount.toString())
            if (validate!!.returnIntegerValue(list.get(i).loanNo.toString()) > 0) {
                totalLoans += 1
            }
            tv_total_closed_loan.text = totalLoans.toString()
            tv_totalamount.text = totalLoanAmount.toString()
        }
    }

    fun getFundTypeName(id: Int?): String? {
        var value: String? = null

        value = funTypeViewModel.getFundName(
            id!!,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
        )

        return value
    }

    fun getLoanno(appid: Long): Int {
        return voMemLoanViewModel.getLoanno(appid)
    }

}