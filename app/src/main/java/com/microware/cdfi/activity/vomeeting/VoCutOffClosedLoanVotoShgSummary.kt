package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.VoCutOffClosedLoanVotoShgSummaryAdapter
import com.microware.cdfi.fragment.VoCutOffTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.FundTypeViewmodel
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoGenerateMeetingViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoMemLoanViewModel
import kotlinx.android.synthetic.main.activity_vo_cut_off_closed_loan_voto_shg_summary.*

class VoCutOffClosedLoanVotoShgSummary : AppCompatActivity() {

    lateinit var voCutOffClosedLoanVotoShgSummaryAdapter: VoCutOffClosedLoanVotoShgSummaryAdapter
    var validate: Validate? = null
    lateinit var voGenerateMeetingViewmodel: VoGenerateMeetingViewmodel
    lateinit var voMemLoanViewModel: VoMemLoanViewModel
    lateinit var lookupViewmodel: LookupViewmodel
    var fundTypeViewModel: FundTypeViewmodel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vo_cut_off_closed_loan_voto_shg_summary)

        validate = Validate(this)

        voGenerateMeetingViewmodel = ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        voMemLoanViewModel = ViewModelProviders.of(this).get(VoMemLoanViewModel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        fundTypeViewModel = ViewModelProviders.of(this).get(FundTypeViewmodel::class.java)

        replaceFragmenty(
            fragment = VoCutOffTopBarFragment(6),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        btn_cancel.setOnClickListener {
            val intent = Intent(this, VoCutOffMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        }

        img_Add.setOnClickListener {
            validate!!.SaveSharepreferenceLong(VoSpData.voShgMemID, 0)
            var intent = Intent(this, VoCutOffCloseLoanVOtoSHG::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        }

        setLabel()
        fillRecyclerView()

    }

    fun setLabel() {
        tblButton.visibility = View.VISIBLE
        tv_sno.text = LabelSet.getText("sr_no", R.string.sr_no)
        tv_shg_name.text = LabelSet.getText("shg_name", R.string.shg_name)
        tv_fund_type.text = LabelSet.getText("fund_type", R.string.fund_type)
        tv_closed_loan.text = LabelSet.getText("total_closed_loans", R.string.total_closed_loans)
        tv_loan_amount.text = LabelSet.getText("loan_amount_s", R.string.loan_amount_s)
        tv_total.text = LabelSet.getText("total", R.string.total)
        btn_cancel.text = LabelSet.getText("cancel", R.string.cancel)
        btn_save.text = LabelSet.getText("save", R.string.save)
    }

    private fun fillRecyclerView() {

        var list = voGenerateMeetingViewmodel.getCutOffClosedMemberLoanDataByMtgNum(
            validate!!.RetriveSharepreferenceInt(
                VoSpData.vocurrentmeetingnumber
            ), validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            true
        )

        rvList.layoutManager = LinearLayoutManager(this)
        voCutOffClosedLoanVotoShgSummaryAdapter =
            VoCutOffClosedLoanVotoShgSummaryAdapter(this, list)

        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = rvList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvList.layoutParams = params

        rvList.adapter = voCutOffClosedLoanVotoShgSummaryAdapter
    }

    fun getTotalClosedLoanAmount(memid: Long?, mtgNum: Int?): Int {
        return voMemLoanViewModel.getTotalClosedLoanAmount(
            validate!!.returnIntegerValue(mtgNum.toString()),
            validate!!.returnLongValue(memid.toString()),
            true
        )
    }

    fun getTotalClosedLoanCount(memid: Long?, mtgNum: Int?): Int {
        return voMemLoanViewModel.getTotalClosedLoanCount(
            validate!!.returnIntegerValue(mtgNum.toString()),
            validate!!.returnLongValue(memid.toString()),
            true
        )
    }

    fun getFundTypeName(id: Int?): String? {
        var value: String? = null

        value = fundTypeViewModel!!.getFundName(
            id!!,validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
        )

        return value
    }

    fun getTotalValue() {
        var iValue1 = 0
        var iValue2 = 0
        val iCount = rvList.childCount


        for (i in 0 until iCount) {
            val gridChild =
                rvList.getChildAt(i) as? ViewGroup

            val tv_total_closed_loan = gridChild!!
                .findViewById<View>(R.id.tv_total_closed_loan) as? TextView

            val tv_loan_amount = gridChild
                .findViewById<View>(R.id.tv_loan_amount) as? TextView

            if (!tv_total_closed_loan!!.text.toString().isNullOrEmpty()) {
                iValue1 += validate!!.returnIntegerValue(tv_total_closed_loan.text.toString())
            }

            if (!tv_loan_amount!!.text.toString().isNullOrEmpty()) {
                iValue2 += validate!!.returnIntegerValue(tv_loan_amount.text.toString())
            }


        }
        tv_total_closed_loan.text = iValue1.toString()
        tv_totalamount.text = iValue2.toString()

    }

    override fun onBackPressed() {
        val intent = Intent(this, VoCutOffMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

}