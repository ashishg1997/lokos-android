package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.VoCutOffMeetingDetailAdapter
import com.microware.cdfi.fragment.VoCutOffTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.voviewmodel.VoMtgDetViewModel
import kotlinx.android.synthetic.main.activity_vo_cut_off_meeting_attendance.*
import kotlinx.android.synthetic.main.activity_vo_cut_off_meeting_attendance.rvList
import kotlinx.android.synthetic.main.buttons_vo.*

class VoCutOffMeetingAttendanceActivity : AppCompatActivity() {
    lateinit var cutOffMeetingDetailAdapter: VoCutOffMeetingDetailAdapter
    lateinit var vomtgDetViewmodel: VoMtgDetViewModel
    var validate: Validate? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vo_cut_off_meeting_attendance)
        vomtgDetViewmodel = ViewModelProviders.of(this).get(VoMtgDetViewModel::class.java)
        validate = Validate(this)

        setLabel()

        replaceFragmenty(
            fragment = VoCutOffTopBarFragment(4),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        btn_save.setOnClickListener {
            getTotalValue()
            setValue()
        }

        btn_cancel.setOnClickListener {
            val intent = Intent(this, VoCutOffMenuActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        fillRecyclerView()
        setValue()
    }

    private fun setLabel() {
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText("cancel", R.string.cancel)
        tv_srno.text = LabelSet.getText("sr_no", R.string.sr_no)
        tv_shgName.text = LabelSet.getText("shg_name", R.string.shg_name)
        tv_attendence.text = LabelSet.getText("attendance", R.string.attendance)
    }

    fun getTotalAttendanceValue() {
        var iValue = 0
        val iCount = rvList.childCount

        for (i in 0 until iCount) {
            val gridChild =
                rvList.getChildAt(i) as? ViewGroup

            val tv_total = gridChild!!
                .findViewById<View>(R.id.et_Attendance) as? EditText

            if (!tv_total!!.text.toString().isNullOrEmpty()) {
                iValue += validate!!.returnIntegerValue(tv_total.text.toString())
            }

        }
        tv_presentCount.text = iValue.toString()

    }

    fun getTotalValue() {
        var saveValue = 0

        val iCount = rvList.childCount


        for (i in 0 until iCount) {
            val gridChild =
                rvList.getChildAt(i) as? ViewGroup

            val et_Attendance = gridChild!!
                .findViewById<View>(R.id.et_Attendance) as? EditText

            val et_memId = gridChild
                .findViewById<View>(R.id.et_memId) as? EditText

            if (et_Attendance!!.length() > 0) {
                var iValue = et_Attendance.text.toString()
                var memberId = validate!!.returnLongValue(et_memId!!.text.toString())
                saveValue = saveData(iValue, memberId)

            }
        }

        if (saveValue > 0) {
            validate!!.CustomAlertVO(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this
            )
        }
    }

    fun saveData(iValue: String, memberId: Long): Int {
        var value = 0
        vomtgDetViewmodel.updateCutOffAttendance(
            iValue,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            memberId,
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime)
        )
        value = 1

        return value
    }

    fun setValue() {
        var TotalPresent = 0

        TotalPresent = vomtgDetViewmodel.getCutOffTotalPresent(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        tv_presentCount.text = "" + TotalPresent
    }

    private fun fillRecyclerView() {

        val list = vomtgDetViewmodel.getListDataMtgByMtgnum(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        cutOffMeetingDetailAdapter = VoCutOffMeetingDetailAdapter(this, list)

        rvList.layoutManager = LinearLayoutManager(this)
        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = rvList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvList.layoutParams = params
        rvList.adapter = cutOffMeetingDetailAdapter
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, VoCutOffMenuActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }
}