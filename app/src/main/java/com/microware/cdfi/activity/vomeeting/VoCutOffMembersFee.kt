package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.VoCutOffMembersFeeAdapter
import com.microware.cdfi.fragment.VoCutOffTopBarFragment
import com.microware.cdfi.utility.replaceFragmenty
import kotlinx.android.synthetic.main.activity_cut_off_members_fee.*
import kotlinx.android.synthetic.main.buttons_vo.*
import kotlinx.android.synthetic.main.vo_cut_off_toolbar.*

class VoCutOffMembersFee : AppCompatActivity() {
    lateinit var voCutOffMembersFeeAdapter : VoCutOffMembersFeeAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cut_off_members_fee)

        replaceFragmenty(
            fragment = VoCutOffTopBarFragment(11),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        btn_cancel.setOnClickListener(View.OnClickListener {
            val i = Intent(this, VoCutOffMenuActivity::class.java)
            startActivity(i)
        })

        fillRecyclerView()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val i = Intent(this, VoCutOffMenuActivity::class.java)
        startActivity(i)
    }

    private fun fillRecyclerView() {

        voCutOffMembersFeeAdapter =
            VoCutOffMembersFeeAdapter(this)

        rvList.layoutManager = LinearLayoutManager(this)

        rvList.adapter = voCutOffMembersFeeAdapter
    }
}