package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.VoCutOffMembersSavingAdapter
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.voentity.VoFinTxnDetMemEntity
import com.microware.cdfi.fragment.VoCutOffTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.voviewmodel.VoFinTxnDetMemViewModel
import com.microware.cdfi.viewModel.voviewmodel.VoGenerateMeetingViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoMtgDetViewModel
import kotlinx.android.synthetic.main.activity_cut_off_members_saving.*
import kotlinx.android.synthetic.main.amountdialog.view.*
import kotlinx.android.synthetic.main.buttons_vo.*

class VoCutOffMembersSavingActivity : AppCompatActivity() {
    lateinit var voCutOffMembersSavingAdapter: VoCutOffMembersSavingAdapter
    lateinit var vomtgDetViewmodel: VoMtgDetViewModel
    lateinit var vogenerateMeetingViewmodel: VoGenerateMeetingViewmodel
    lateinit var voFinTxnDetmemViewModel: VoFinTxnDetMemViewModel
    var validate: Validate? = null
    var voFinTxnDetMemEntity: VoFinTxnDetMemEntity? = null
    var TodayCompulsaryvalue = 0
    var TodayVoluntaryvalue = 0
    var voMtgDetViewModel: VoMtgDetViewModel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cut_off_members_saving)
        vogenerateMeetingViewmodel =
            ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        vomtgDetViewmodel = ViewModelProviders.of(this).get(VoMtgDetViewModel::class.java)
        voFinTxnDetmemViewModel =
            ViewModelProviders.of(this).get(VoFinTxnDetMemViewModel::class.java)
        voMtgDetViewModel = ViewModelProviders.of(this).get(VoMtgDetViewModel::class.java)

        validate = Validate(this)

        setLabel()
        replaceFragmenty(
            fragment = VoCutOffTopBarFragment(5),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        btn_save.setOnClickListener {
            getCompulsoryVoluntaryValue()
        }

        btn_cancel.setOnClickListener(View.OnClickListener {
            val i = Intent(this, VoCutOffMenuActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)
            finish()
        })

        fillRecyclerView()

        getTotalValue()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val i = Intent(this, VoCutOffMenuActivity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(i)
        finish()
    }

    private fun setLabel() {
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText("cancel", R.string.cancel)
        tv_sr_no.text = LabelSet.getText("sr_no", R.string.sr_no)
        tv_shg_name.text = LabelSet.getText("shg_name", R.string.shg_name)
        tv_compulasory_saving.text = LabelSet.getText(
            "compulsary_saving_",
            R.string.compulsary_saving_
        )
        tv_voluntary_saving.text = LabelSet.getText(
            "voluntary_saving_",
            R.string.voluntary_saving_
        )
    }

    fun getTotalCompulsoryValue() {
        var iValue = 0
        var iValueCom = 0
        val iCount = rvList.childCount


        for (i in 0 until iCount) {
            val gridChild =
                rvList.getChildAt(i) as? ViewGroup

            val tv_ctotal = gridChild!!
                .findViewById<View>(R.id.et_compulsory_saving_amount) as? EditText

            if (!tv_ctotal!!.text.toString().isNullOrEmpty()) {
                iValue += validate!!.returnIntegerValue(tv_ctotal.text.toString())
            }

        }
        tv_total_compulsory_saving.text = iValue.toString()

    }


    fun getTotalVoluntaryValue() {
        var iValue = 0
        val iCount = rvList.childCount

        for (i in 0 until iCount) {
            val gridChild =
                rvList.getChildAt(i) as? ViewGroup

            val tv_vtotal = gridChild!!
                .findViewById<View>(R.id.et_voluntary_saving_amount) as? EditText

            if (!tv_vtotal!!.text.toString().isNullOrEmpty()) {
                iValue += validate!!.returnIntegerValue(tv_vtotal.text.toString())
            }

        }
        tv_total_voluntary_saving.text = iValue.toString()

    }

    fun getTotalValue() {

        TodayCompulsaryvalue = validate!!.returnIntegerValue(
            getTotalCompSavingamount(
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
            ).toString()
        )




        if (TodayCompulsaryvalue > 0) {
            tv_total_compulsory_saving.text = TodayCompulsaryvalue.toString()
        } else {
            tv_total_compulsory_saving.text = "0"
        }

        TodayVoluntaryvalue = validate!!.returnIntegerValue(
            getTotalVolSavingamount(
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
            ).toString()
        )

        if (TodayVoluntaryvalue > 0) {
            tv_total_voluntary_saving.text = TodayVoluntaryvalue.toString()
        } else {
            tv_total_voluntary_saving.text = "0"
        }

    }

    fun getCompulsoryVoluntaryValue() {
        var saveValue = 0

        val iCount = rvList.childCount

        for (i in 0 until iCount) {
            val gridChild =
                rvList.getChildAt(i) as? ViewGroup

            val et_compulsory = gridChild!!
                .findViewById<View>(R.id.et_compulsory_saving_amount) as? EditText

            val et_memId = gridChild
                .findViewById<View>(R.id.et_memId) as? EditText

            val et_Voluntary = gridChild
                .findViewById<View>(R.id.et_voluntary_saving_amount) as? EditText

            if (et_compulsory!!.length() > 0) {
                var iCompulsary = validate!!.returnIntegerValue(et_compulsory.text.toString())
                var memId = validate!!.returnLongValue(et_memId!!.text.toString())
                var uid = 82

                saveValue = saveData(iCompulsary, memId, uid)
                voMtgDetViewModel!!.updateCompclosing(
                    memId,
                    validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                    iCompulsary
                )

            }

            if (et_Voluntary!!.length() > 0) {

                var memId = validate!!.returnLongValue(et_memId!!.text.toString())
                var uid = 3
                var iVoluntary = validate!!.returnIntegerValue(et_Voluntary.text.toString())

                saveValue = saveData(iVoluntary, memId, uid)
                voMtgDetViewModel!!.updatevolclosing(
                    memId,
                    validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                    iVoluntary
                )

            }
        }
        if (saveValue > 0) {

            CDFIApplication.database?.fedmtgDao()
                ?.updatevolsave(
                    validate!!.returnIntegerValue(tv_total_voluntary_saving.text.toString()),
                    validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                    validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
                )

            CDFIApplication.database?.fedmtgDao()
                ?.updatecompsave(
                    validate!!.returnIntegerValue(tv_total_compulsory_saving.text.toString()),
                    validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                    validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
                )

            fillRecyclerView()

            validate!!.CustomAlertVO(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this
            )
        }
    }

    private fun saveData(amount: Int, memId: Long, auid: Int): Int {
        var value = 0
        voFinTxnDetMemEntity = VoFinTxnDetMemEntity(
            0,
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            memId,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            auid,
            "RO",
            amount,
            1,
            0,
            "",
            "",

            "",
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            "",
            0,
            "",
            0, 0, 0, 1
        )
        voFinTxnDetmemViewModel.insertVoGroupLoanSchedule(voFinTxnDetMemEntity!!)

        value = 1

        return value
    }

    private fun fillRecyclerView() {

        val list = vomtgDetViewmodel.getListDataMtgByMtgnum(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        voCutOffMembersSavingAdapter = VoCutOffMembersSavingAdapter(this, list)

        rvList.layoutManager = LinearLayoutManager(this)
        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = rvList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvList.layoutParams = params
        rvList.adapter = voCutOffMembersSavingAdapter
    }

    fun getSavingamount(mtgNo: Int, cboId: Long, memId: Long, auid: Int): Int? {
        var camount = voFinTxnDetmemViewModel.getSavingamount(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid), memId, auid
        )
        var compAmount = 0

        if (!camount.isNullOrEmpty()) {
            for (i in camount.indices)
                compAmount = validate!!.returnIntegerValue(camount.get(i).amount.toString())
        }

        return compAmount
    }


    fun getTotalCompSavingamount(mtgNo: Int, cboId: Long): Int? {
        var totalcompamount: Int? = null
        totalcompamount = vogenerateMeetingViewmodel.getTotalCompulsoryamount(mtgNo, cboId)

        return totalcompamount
    }

    fun getTotalVolSavingamount(mtgNo: Int, cboId: Long): Int? {
        var totalvolamount: Int? = null
        totalvolamount = vogenerateMeetingViewmodel.getTotalVoluntaryamount(mtgNo, cboId)

        return totalvolamount
    }

}