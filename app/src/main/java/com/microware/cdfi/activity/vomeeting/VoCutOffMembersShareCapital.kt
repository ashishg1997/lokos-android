package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.VoCutOffMembersShareCapitalAdapter
import com.microware.cdfi.api.vomodel.VoShareCapitalModel
import com.microware.cdfi.entity.voentity.VoFinTxnDetMemEntity
import com.microware.cdfi.fragment.VoCutOffTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.voviewmodel.VoFinTxnDetMemViewModel
import com.microware.cdfi.viewModel.voviewmodel.VoGenerateMeetingViewmodel
import kotlinx.android.synthetic.main.activity_cut_off_members_share_capital.*
import kotlinx.android.synthetic.main.activity_cut_off_members_share_capital.rvList
import kotlinx.android.synthetic.main.activity_cut_off_members_share_capital.tv_share_capital
import kotlinx.android.synthetic.main.activity_cut_off_members_share_capital.tv_shg_name
import kotlinx.android.synthetic.main.activity_cut_off_members_share_capital.tv_sr_no
import kotlinx.android.synthetic.main.buttons_vo.*

class VoCutOffMembersShareCapital : AppCompatActivity() {
    var validate: Validate? = null
    var Todayvalue = 0

    lateinit var voCutOffMembersShareCapitalAdapter: VoCutOffMembersShareCapitalAdapter
    lateinit var voGenerateMeetingViewmodel: VoGenerateMeetingViewmodel
    lateinit var voFinTxnDetMemViewModel: VoFinTxnDetMemViewModel
    var voFinTxnDetMemEntity: VoFinTxnDetMemEntity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cut_off_members_share_capital)

        validate = Validate(this)

        voGenerateMeetingViewmodel = ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        voFinTxnDetMemViewModel = ViewModelProviders.of(this).get(VoFinTxnDetMemViewModel::class.java)

        if (validate!!.RetriveSharepreferenceInt(VoSpData.voReceiptType) == 2) {
            replaceFragmenty(
                fragment = VoCutOffTopBarFragment(10),
                allowStateLoss = true,
                containerViewId = R.id.mainContent
            )
        } else {
            replaceFragmenty(
                fragment = VoCutOffTopBarFragment(11),
                allowStateLoss = true,
                containerViewId = R.id.mainContent
            )
        }

        btn_cancel.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, VoCutOffMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        })

        btn_save.setOnClickListener {
            getShareCapitalValue()
        }

        setLabel()
        fillRecyclerView()

    }

    private fun fillRecyclerView() {
        Todayvalue = 0

        var list: List<VoShareCapitalModel>? = null
        list = voGenerateMeetingViewmodel.getShareCapitalListByReceiptType(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.voReceiptType)
        )

        if (!list.isNullOrEmpty()) {
            rvList.layoutManager = LinearLayoutManager(this)
            voCutOffMembersShareCapitalAdapter = VoCutOffMembersShareCapitalAdapter(this, list)
            val isize: Int
            isize = list.size
            val params: ViewGroup.LayoutParams = rvList.layoutParams
            val r = resources
            val px = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.spraying),
                r.displayMetrics
            )
            val hi = Math.round(px)
            var gridHeight = hi * isize
            params.height = gridHeight
            rvList.layoutParams = params
            rvList.adapter = voCutOffMembersShareCapitalAdapter
        }
    }

    private fun setLabel() {
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText("cancel", R.string.cancel)
        tv_sr_no.text = LabelSet.getText("sr_no", R.string.sr_no)
        tv_shg_name.text = LabelSet.getText("shg_name", R.string.shg_name)
        if (validate!!.RetriveSharepreferenceInt(VoSpData.voReceiptType) == 2) {
            tv_share_capital.text = LabelSet.getText("shgsharecapital", R.string.shgsharecapital)
        } else {
            tv_share_capital.text = LabelSet.getText("member_s_fee", R.string.member_s_fee)
        }
        tv_total.text = LabelSet.getText("total", R.string.total)
    }

    fun getTotalValue() {
        var iValue = 0
        val iCount = rvList.childCount

        for (i in 0 until iCount) {
            val gridChild =
                rvList.getChildAt(i) as? ViewGroup

            val tv_share_capital = gridChild!!
                .findViewById<View>(R.id.tv_share_capital) as? EditText

            if (!tv_share_capital!!.text.toString().isNullOrEmpty()) {
                iValue += validate!!.returnIntegerValue(tv_share_capital.text.toString())
            }

        }
        tv_Amount1.text = iValue.toString()
    }

    fun getTotalValue(iValue: Int) {
        Todayvalue += iValue
        tv_Amount1.text = Todayvalue.toString()
    }

    fun getShareCapitalValue() {
        var saveValue = 0
        var iValue = 0
        var memID: Long? = 0
        val iCount = rvList.childCount

        for (i in 0 until iCount) {
            val gridChild = rvList.getChildAt(i) as? ViewGroup

            val tv_share_capital =
                gridChild!!.findViewById<View>(R.id.tv_share_capital) as? EditText
            val tv_memid = gridChild.findViewById<View>(R.id.tv_memid) as? EditText

            if (tv_share_capital!!.length() > 0 && tv_memid!!.length() > 0) {
                iValue = validate!!.returnIntegerValue(tv_share_capital.text.toString())
                memID = validate!!.returnIntegerValue(tv_memid.text.toString()).toLong()

                saveValue = saveData(iValue, memID)
            }
        }

        if (saveValue > 0) {
            if (validate!!.RetriveSharepreferenceInt(VoSpData.voReceiptType) == 2) {
                replaceFragmenty(
                    fragment = VoCutOffTopBarFragment(10),
                    allowStateLoss = true,
                    containerViewId = R.id.mainContent
                )
            } else {
                replaceFragmenty(
                    fragment = VoCutOffTopBarFragment(11),
                    allowStateLoss = true,
                    containerViewId = R.id.mainContent
                )
            }


            validate!!.CustomAlertVO(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this
            )
        }
    }

    fun saveData(iValue: Int, memID: Long): Int {

        var value = 0

        //trans date missing
        voFinTxnDetMemEntity = VoFinTxnDetMemEntity(
            0,
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            memID,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.RetriveSharepreferenceInt(VoSpData.voReceiptType),
            "PO",/*enter type*/
            iValue,
            1,
            0,
            "",
            "",
            /*"",
            0,*/
            "",
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            "",
            0,
            "",
            0,0,0,1
        )

        voFinTxnDetMemViewModel.insertVoGroupLoanSchedule(voFinTxnDetMemEntity!!)

        value = 1

        return value
    }

    override fun onBackPressed() {
        val intent = Intent(this, VoCutOffMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

}