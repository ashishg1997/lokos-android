package com.microware.cdfi.activity.vomeeting

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProviders
import com.google.gson.JsonObject
import com.microware.cdfi.R
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.api.response.MeetingResponse
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.CboBankViewmodel
import com.microware.cdfi.viewModel.MasterBankViewmodel
import com.microware.cdfi.viewModel.ResponseViewModel
import com.microware.cdfi.viewModel.voviewmodel.*
import kotlinx.android.synthetic.main.activity_cut_off_menu.*
import kotlinx.android.synthetic.main.activity_cut_off_menu.ivAttendance
import kotlinx.android.synthetic.main.activity_cut_off_menu.tv_attendance
import kotlinx.android.synthetic.main.activity_cut_off_menu.tv_member_saving
import kotlinx.android.synthetic.main.activity_vo_regular_meeting_ttsactivity.*
import kotlinx.android.synthetic.main.bank_detail_notification.view.*
import kotlinx.android.synthetic.main.buttons_vocutoffmeeting_deleteandclose.*
import kotlinx.android.synthetic.main.customealertdialogelogin.*
import kotlinx.android.synthetic.main.customealertdialogelogin.view.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.layout_cutoff_meeting_menu.*
import kotlinx.android.synthetic.main.meeting_detail_item_zero.*
import kotlinx.android.synthetic.main.repay_toolbar_vomeeting.*
import kotlinx.android.synthetic.main.user_detail_status_new.*
import kotlinx.android.synthetic.main.user_detail_status_new.tv_code
import kotlinx.android.synthetic.main.user_detail_status_new.tv_count
import kotlinx.android.synthetic.main.user_detail_status_new.tv_date
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class VoCutOffMenuActivity : AppCompatActivity() {
    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: ApiInterface? = null
    var validate: Validate? = null
    var generateMeetingViewmodel: VoGenerateMeetingViewmodel? = null
    var voLoanViewmodel: VoLoanApplicationViewmodel? = null
    var vofinancialTransactionsMemViewmodel: VoFinTxnDetMemViewModel? = null
    var voLoanTxnMemViewmodel: VoMemLoanTxnViewModel? = null
    var voLoanGpViewmodel: VoGroupLoanViewModel? = null
    var voFintxnDetGrpViewmodel: VoFinTxnDetGrpViewModel? = null
    var voLoanGPTxnViewmodel: VoGroupLoanTxnViewModel? = null
    var voLoanMemberViewmodel: VoMemLoanViewModel? = null
    var vofinTxnViewmodel: VoFinTxnViewModel? = null
    var vomemberLoanScheduleViewmodel: VoMemLoanScheduleViewModel? = null
    var voGrpLoanScheduleViewModel: VoGroupLoanScheduleViewModel? = null
    var cboBankViewModel: CboBankViewmodel? = null
    var bankMasterViewmodel: MasterBankViewmodel? = null
    var voFinTxnDetMemViewModel: VoFinTxnDetMemViewModel? = null
    var voFinTxnDetGrpViewModel: VoFinTxnDetGrpViewModel? = null
    var voFinTxnViewModel: VoFinTxnViewModel? = null
    var voMemLoanViewModel: VoMemLoanViewModel? = null
    var voMemLoanTxnViewModel: VoMemLoanTxnViewModel? = null
    var voGroupLoanViewModel: VoGroupLoanViewModel? = null
    var voGroupLoanTxnViewModel: VoGroupLoanTxnViewModel? = null
    var responseViewModel: ResponseViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cut_off_menu)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()

        validate = Validate(this)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        voLoanViewmodel = ViewModelProviders.of(this).get(VoLoanApplicationViewmodel::class.java)
        vofinancialTransactionsMemViewmodel =
            ViewModelProviders.of(this).get(VoFinTxnDetMemViewModel::class.java)
        voLoanTxnMemViewmodel = ViewModelProviders.of(this).get(VoMemLoanTxnViewModel::class.java)
        voLoanGpViewmodel = ViewModelProviders.of(this).get(VoGroupLoanViewModel::class.java)
        voFintxnDetGrpViewmodel =
            ViewModelProviders.of(this).get(VoFinTxnDetGrpViewModel::class.java)
        voLoanGPTxnViewmodel = ViewModelProviders.of(this).get(VoGroupLoanTxnViewModel::class.java)
        voLoanMemberViewmodel = ViewModelProviders.of(this).get(VoMemLoanViewModel::class.java)
        vofinTxnViewmodel = ViewModelProviders.of(this).get(VoFinTxnViewModel::class.java)
        vomemberLoanScheduleViewmodel =
            ViewModelProviders.of(this).get(VoMemLoanScheduleViewModel::class.java)
        voGrpLoanScheduleViewModel =
            ViewModelProviders.of(this).get(VoGroupLoanScheduleViewModel::class.java)
        voMemLoanViewModel = ViewModelProviders.of(this).get(VoMemLoanViewModel::class.java)
        voMemLoanTxnViewModel = ViewModelProviders.of(this).get(VoMemLoanTxnViewModel::class.java)
        voGroupLoanViewModel = ViewModelProviders.of(this).get(VoGroupLoanViewModel::class.java)
        voGroupLoanTxnViewModel =
            ViewModelProviders.of(this).get(VoGroupLoanTxnViewModel::class.java)
        cboBankViewModel = ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        voFinTxnViewModel = ViewModelProviders.of(this).get(VoFinTxnViewModel::class.java)
        voFinTxnDetGrpViewModel =
            ViewModelProviders.of(this).get(VoFinTxnDetGrpViewModel::class.java)
        bankMasterViewmodel = ViewModelProviders.of(this).get(MasterBankViewmodel::class.java)
        voFinTxnDetMemViewModel =
            ViewModelProviders.of(this).get(VoFinTxnDetMemViewModel::class.java)
        responseViewModel = ViewModelProviders.of(this).get(ResponseViewModel::class.java)

        tv_title.text = LabelSet.getText("cut_off_menu", R.string.cut_off_menu)
        tv_code.text = validate!!.RetriveSharepreferenceLong(VoSpData.voShgcode).toString()
        tv_name.text = validate!!.RetriveSharepreferenceString(VoSpData.voShgName)
        tv_date.text =
            validate!!.convertDatetime(validate!!.RetriveSharepreferenceLong(VoSpData.voFormation_dt))
        tv_count.text = "" + validate!!.RetriveSharepreferenceInt(VoSpData.voMemberCount)

        ic_Back.setOnClickListener {
            val intent = Intent(this, VoMeetingListActivity1::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            finish()
        }

        lay_memberShareCapital.setOnClickListener {
            validate!!.SaveSharepreferenceInt(VoSpData.voReceiptType, 2)
            var intent = Intent(this, VoCutOffMembersShareCapital::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_memberFee.setOnClickListener {
            validate!!.SaveSharepreferenceInt(VoSpData.voReceiptType, 1)
            var intent = Intent(this, VoCutOffMembersShareCapital::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_attendence.setOnClickListener {
            var intent = Intent(this, VoCutOffMeetingAttendanceActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_memberSaving.setOnClickListener {
            var intent = Intent(this, VoCutOffMembersSavingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_closedLoan.setOnClickListener {
            var intent = Intent(this, CutOffVoMemberCloseLoan::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_activeLoan.setOnClickListener {
            var intent = Intent(this, VoCutOffSummaryOfActiveLoan::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_investment.setOnClickListener {
            var intent = Intent(this, VoCutOffVoInvestmentList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_loanClfToVo.setOnClickListener {
            var intent = Intent(this, VoCutOffVoLoanCLFtoVOList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_cashBalance.setOnClickListener {
            var intent = Intent(this, VoCuttOffVoCashBalance::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_bankBalance.setOnClickListener {
            var intent = Intent(this, VoCutOffVoBankBalanceList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_balanceSheet.setOnClickListener {
            var intent = Intent(this, VoCutOffVoBalanceSheet::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        btn_save.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(VoSpData.voCashinhand) >= 0) {
                CustomAlertCloseMeeting(
                    LabelSet.getText(
                        "are_u_sure_u_want_to_close",
                        R.string.are_u_sure_u_want_to_close
                    )
                )
            } else {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "verify_cash_in_hand",
                        R.string.verify_cash_in_hand
                    ), this
                )
            }
        }

        btn_cancel.setOnClickListener {
            CustomAlertDeleteMeeting(
                LabelSet.getText(
                    "want_to_delete_meeting",
                    R.string.want_to_delete_meeting
                )
            )
        }
        updatecashinbank()
        updatecashinhand()
        setLabelText()
        setValues()


        ll_speaker.visibility = View.VISIBLE
        ll_speaker.setOnClickListener {

            var SHG_name = tv_name.text.toString()
            var MeetingDate = tv_date.text.toString()
            var savingAmount = tv_memberSavingAmt.text.toString()
            var closedLoan_amount = tv_tvClosedLoanAmt.text.toString()
            var activeLoan_amount = tv_tvActiveLoanAmt.text.toString()
            var share_capital_amount = tv_tvMemShareCapAmt.text.toString()
            var investment = tv_voInvestmentAmt.text.toString()
            var borrow = tv_voLoanAmt.text.toString()
            var cashBalance = tv_voCashAmt.text.toString()
            var bankBalance = tv_tvBankAmt.text.toString()

            var intent = Intent(this, VOCutOffMeetingTTSActivity::class.java)
            intent.putExtra("SHG_name", SHG_name)
            intent.putExtra("MeetingDate", MeetingDate)
            intent.putExtra("savingAmount",savingAmount)
            intent.putExtra("closedLoan_amount",closedLoan_amount)
            intent.putExtra("activeLoan_amount", activeLoan_amount)
            intent.putExtra("share_capital_amount", share_capital_amount)
            intent.putExtra("investment", investment)
            intent.putExtra("borrow", borrow)
            intent.putExtra("cashBalance", cashBalance)
            intent.putExtra("bankBalance", bankBalance)
            startActivity(intent)
        }

    }

    private fun setLabelText() {
        tv_attendance.text = LabelSet.getText("attendance", R.string.attendance)
        tv_member_saving.text = LabelSet.getText("member_saving", R.string.member_saving)
        tv_closed_loan.text = LabelSet.getText(
            "closed_loan_vo_to_shg",
            R.string.closed_loan_vo_to_shg
        )
        tv_active_loan.text = LabelSet.getText(
            "active_loan_vo_to_shg",
            R.string.active_loan_vo_to_shg
        )
        tv_member_share_capital.text = LabelSet.getText(
            "member_s_share_capital",
            R.string.member_s_share_capital
        )
        tv_member_fee.text = LabelSet.getText("member_s_fee", R.string.member_s_fee)
        tv_vo_investment.text = LabelSet.getText("vo_s_investment", R.string.vo_s_investment)
        tv_vo_loan.text = LabelSet.getText("vo_s_loan", R.string.vo_s_loan)
        tv_vo_cash_balance.text = LabelSet.getText(
            "vo_s_cash_balance",
            R.string.vo_s_cash_balance
        )
        tv_vo_bank_balance.text = LabelSet.getText(
            "vo_s_bank_balance",
            R.string.vo_s_bank_balance
        )
        tv_vo_balance_sheet.text = LabelSet.getText(
            "vo_s_balance_sheet",
            R.string.vo_s_balance_sheet
        )
        btn_save.text = LabelSet.getText("close_meeting", R.string.close_meeting)
        btn_cancel.text = LabelSet.getText("delete_meeting", R.string.delete_meeting)
    }

    fun CustomAlertDeleteMeeting(str: String) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str
        mDialogView.btn_yes.background = resources.getDrawable(R.drawable.button_bg_vomeeting)
        mDialogView.btn_no.background = resources.getDrawable(R.drawable.button_bg_vomeeting)
        mDialogView.btn_yes.setOnClickListener {
            mAlertDialog.dismiss()

            DeleteMeeting()
          /*  var intent = Intent(this, VoMeetingListActivity1::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)*/

        }
        mDialogView.btn_no.setOnClickListener {
            mAlertDialog.dismiss()

        }
    }
    fun DeleteMeeting() {

        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText("deleting_meeting_data", R.string.deleting_meeting_data)
        )

        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
        val token = validate!!.returnStringValue(
            AESEncryption.decrypt(
                validate!!.RetriveSharepreferenceString(AppSP.token),
                validate!!.RetriveSharepreferenceString(AppSP.userid)
            )
        )
        val cboid = validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        val mtgNum = validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
        val callCount = apiInterface?.deleteVOMeeting(
            token,
            user,
            cboid,
            mtgNum
        )

        callCount?.enqueue(object : Callback<MeetingResponse> {
            override fun onFailure(callCount: Call<MeetingResponse>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<MeetingResponse>,
                response: Response<MeetingResponse>
            ) {

                progressDialog.dismiss()
                if (response.isSuccessful) {

                    try {
                        var msg = response.body()?.result
                        if (msg!!.equals("Meeting delete successfully!!")) {
                            deleteMeetingData()
                        }


                        //       CustomAlert(text)
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        //                                    progressDialog.dismiss()
                    }

                } else {
                    progressDialog.dismiss()
                    var resCode = 0
                    var resMsg = ""
                    if (response.code() == 403) {

                        CustomAlertlogin()
                    } else {
                        if (response.code() == 500 || response.code() == 503) {
                            resCode = response.code()
                            resMsg = response.message()
                        } else {
                            var jsonObject1 =
                                JSONObject(response.errorBody()!!.source().readUtf8().toString())

                            resCode =
                                validate!!.returnIntegerValue(
                                    jsonObject1.optString("responseCode").toString()
                                )
                            resMsg = validate!!.returnStringValue(
                                jsonObject1.optString("responseMsg").toString()
                            )

                        }
                        if (resCode == 404 && resMsg.equals("record not found in database")) {

                            deleteMeetingData()

                        } else {
                            validate!!.CustomAlertMsg(
                                this@VoCutOffMenuActivity,
                                responseViewModel,
                                resCode,
                                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                                resMsg
                            )
                        }
                    }

                }


            }

        })
    }
    fun CustomAlertlogin() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogelogin, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mDialogView.etUsername.setText(validate!!.RetriveSharepreferenceString(AppSP.userid))
        mDialogView.etPassword.setText(validate!!.returnStringValue(AESEncryption.decrypt(
            validate!!.RetriveSharepreferenceString(AppSP.Password),
            validate!!.RetriveSharepreferenceString(AppSP.userid)
        )))
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.btn_login.setBackgroundColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.txt_dialog_title2.text = LabelSet.getText("app_name",R.string.app_name)
        mDialogView.btn_login.text = LabelSet.getText("log_in",R.string.log_in)
        mDialogView.chk_showHide1.text = LabelSet.getText("show_password",R.string.show_password)
        mDialogView.btn_login.setOnClickListener {
            mAlertDialog.dismiss()
            importtoken(
                mDialogView.etUsername.text.toString(),
                validate!!.returnStringValue(
                    AESEncryption.encrypt(validate!!.returnStringValue(
                        mDialogView.etPassword.text.toString()),mDialogView.etUsername.text.toString()))
            )
        }
        mAlertDialog.chk_showHide1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                mAlertDialog.etPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                mAlertDialog.etPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
        }

    }

    fun importtoken(user: String, pass: String) {
        var grant_type = "password"
        var userId = user
        var password = pass
        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText(
                "authenticate_user",
                R.string.authenticate_user
            )
        )


        val callCount = apiInterface?.gettokenforce(
            grant_type,
            userId,
            password, validate!!.RetriveSharepreferenceInt(AppSP.State_Selected), 1,
            validate!!.RetriveSharepreferenceString(AppSP.Roleid)!!
        )
        callCount?.enqueue(object : Callback<JsonObject> {
            override fun onFailure(callCount: Call<JsonObject>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                progressDialog.dismiss()


                if (response.isSuccessful) {

                    var jsonObject1: JSONObject? = null
                    try {
                        jsonObject1 = JSONObject(response.body().toString())

                        val auth = jsonObject1.get("accessToken").toString()
                        if (auth.trim().length > 0) {
                            validate!!.SaveSharepreferenceString(
                                AppSP.token,
                                validate!!.returnStringValue(
                                    AESEncryption.encrypt(
                                        auth,validate!!.RetriveSharepreferenceString(AppSP.userid)))
                            )
                        }
                        validate!!.CustomAlertVO(
                            LabelSet.getText(
                                "userauthenticatedsuccessfully",
                                R.string.userauthenticatedsuccessfully
                            ), this@VoCutOffMenuActivity
                        )

                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Toast.makeText(this@VoCutOffMenuActivity, response.message(), Toast.LENGTH_LONG)
                            .show()


                    }

                } else {
                    Toast.makeText(this@VoCutOffMenuActivity, response.message(), Toast.LENGTH_LONG)
                        .show()
                }


            }

        })


    }
    fun deleteMeetingData() {
        /*Table1*/
        generateMeetingViewmodel!!.deleteVo_MtgData(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        /*Table2*/
        generateMeetingViewmodel!!.deleteVODetailData(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        /*Table3*/
        vofinancialTransactionsMemViewmodel!!.deleteFinancialTransactionMemberDetailData(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        /*Table4*/
        voFintxnDetGrpViewmodel!!.deleteGrpFinancialTxnDetailData(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        /*Table5*/
        vofinTxnViewmodel!!.deleteVOFinancialTxnData(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        /*Table6*/
        voLoanTxnMemViewmodel!!.deleteMemberLoanTxnData(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        /*Table7*/
        voLoanGpViewmodel!!.deleteGroupLoanData(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        /*Table8*/
        voLoanGPTxnViewmodel!!.deleteGroupLoanTxnData(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        /*Table9*/
        voLoanMemberViewmodel!!.deleteMemberLoan(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
        )
        /*Table10*/
        voLoanViewmodel!!.deleteVoLoanApplication(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        /*Table11*/
        vomemberLoanScheduleViewmodel!!.deleteMemberRepaidData(
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        /*Table 12*/
        vomemberLoanScheduleViewmodel!!.deleteMemberSubinstallmentData(
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        /*Table13*/
        voGrpLoanScheduleViewModel!!.deleteGroupRepaidData(
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        /*Table 14*/
        voGrpLoanScheduleViewModel!!.deleteGroupSubinstallmentData(
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        var intent = Intent(this, VoMeetingListActivity1::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)

    }

    private fun setValues() {

        val rs_string = LabelSet.getText("", R.string.rs_sign)

        // Attendance
        setSumValue(
            tv_shg_attendence_total, ivAttendance, generateMeetingViewmodel!!.getCutOffTotalPresent(
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
            ), ""
        )

        val totalCompulsarysaving = vofinancialTransactionsMemViewmodel!!.getsumcomp(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        val totalVoluntaryarysaving = vofinancialTransactionsMemViewmodel!!.getsumvol(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        val totalSaving = totalCompulsarysaving + totalVoluntaryarysaving
        setSumValue(tv_memberSavingAmt, ivMemberSavingAmt, totalSaving, rs_string)

        //closed Loan
        setSumValue(
            tv_tvClosedLoanAmt, ivClosedLoanAmt,
            generateMeetingViewmodel!!.getCutOffMemberLoanAmount(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                true
            ), rs_string
        )

        // ShareCapital
        setSumValue(
            tv_tvMemShareCapAmt,
            ivMemShareCapAmt,
            generateMeetingViewmodel!!.getTotalAmountByReceiptType
                (
                validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber), 2
            ),
            rs_string
        )

        //Membership Fee
        setSumValue(
            tv_memberFeeAmt, ivMemberFeeAmt,
            generateMeetingViewmodel!!.getTotalAmountByReceiptType(
                validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber), 1
            ), rs_string
        )

        // Active Loan
        setSumValue(
            tv_tvActiveLoanAmt,
            ivActiveLoanAmt,
            generateMeetingViewmodel!!.getCutOffMemberLoanAmount(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                false
            ),
            rs_string
        )

        //Investment

        setSumValue(
            tv_voInvestmentAmt, ivInvestmentAmt,
            generateMeetingViewmodel!!.getTotalInvestmentByMtgNum(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                listOf(59, 60, 61),
                "PO"
            ), rs_string
        )

        //BankBalance
        setSumValue(
            tv_tvBankAmt, ivBankAmt, generateMeetingViewmodel!!.gettotalinbank(
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
            ), rs_string
        )

        //VO LOAN (CLF TO VO)
        setSumValue(
            tv_voLoanAmt, ivLoanAmt,
            voLoanGPTxnViewmodel!!.getCutOffgroupLoanAmount(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
            ), rs_string
        )

        //cash balance
        setSumValue(
            tv_voCashAmt,
            ivCashAmt,
            validate!!.RetriveSharepreferenceInt(VoSpData.voCashinhand),
            rs_string
        )

    }

    fun updatecashinhand() {

        var openingcash = generateMeetingViewmodel!!.getupdatedpeningcash(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        var totshgcash = voFinTxnDetMemViewModel!!.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(1),
            listOf("RG", "RL", "RO"),
            ""
        )
        var totvocash = voFinTxnDetGrpViewModel!!.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(1),
            listOf("RG", "RL", "RO"),
            ""
        )

        var totshgcashdebit = voFinTxnDetMemViewModel!!.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(1),
            listOf("PG", "PL", "PO"),
            ""
        )
        var totvocashdebit = voFinTxnDetGrpViewModel!!.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(1),
            listOf("PG", "PL", "PO"),
            ""
        )
        var totmemloan = voMemLoanViewModel!!.getMemLoanDisbursedAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            "", listOf<Int>(1)
        )

        var totvoloan = voGroupLoanViewModel!!.getgrouptotloanamtinbank(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            "", listOf<Int>(1)
        )

        var totmemloantxn = voMemLoanTxnViewModel!!.getmemtotloanpaidinbank(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            "", listOf<Int>(1)
        )

        var totvoloantxn = voGroupLoanTxnViewModel!!.getgrptotloanpaidinbank(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            "", listOf<Int>(1)
        )


        /* var totcashwithdrawl = generateMeetingViewmodel.getsumwithdrwal(
             validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
             validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
 */
        var totalclosingbal =
            openingcash + totshgcash + totvocash + totmemloantxn + totvoloan - totshgcashdebit - totvocashdebit - totmemloan - totvoloantxn
        validate!!.SaveSharepreferenceInt(VoSpData.voCashinhand, totalclosingbal)
        //view.tv_cash_in_hand.text = totalclosingbal.toString()
        generateMeetingViewmodel!!.updateclosingcash(
            totalclosingbal,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
    }

    fun updatecashinbank() {
        var voBankList =
            cboBankViewModel!!.getBankdata1(validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID))
        for (i in voBankList!!.indices) {
            var accountno =
                voBankList.get(i).ifsc_code!!.dropLast(7) + voBankList.get(i).account_no


            var totshgcash = voFinTxnDetMemViewModel!!.getIncomeAmount(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                listOf<Int>(2, 3),
                listOf("RG", "RL", "RO"),
                accountno
            )

            var totvocash = voFinTxnDetGrpViewModel!!.getIncomeAmount(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                listOf<Int>(2, 3),
                listOf("RG", "RL", "RO"),
                accountno
            )
            var totshgcashdebit = voFinTxnDetMemViewModel!!.getIncomeAmount(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                listOf<Int>(2, 3),
                listOf("PG", "PL", "PO"),
                accountno
            )
            var totvocashdebit = voFinTxnDetGrpViewModel!!.getIncomeAmount(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                listOf<Int>(2, 3),
                listOf("PG", "PL", "PO"),
                accountno
            )
            var totopeningcash = voFinTxnViewModel!!.gettotalopeninginbank(
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                accountno
            )

            var totmemloan = voMemLoanViewModel!!.getMemLoanDisbursedAmount(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                accountno,
                listOf<Int>(2, 3)
            )

            var totvoloan = voGroupLoanViewModel!!.getgrouptotloanamtinbank(
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                accountno,
                listOf<Int>(2, 3)
            )

            var totmemloantxn = voMemLoanTxnViewModel!!.getmemtotloanpaidinbank(
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                accountno,
                listOf<Int>(2, 3)
            )

            var totvoloantxn = voGroupLoanTxnViewModel!!.getgrptotloanpaidinbank(
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                accountno,
                listOf<Int>(2, 3)
            )
            var totalclosingbal =
                totopeningcash + totshgcash + totvocash + totmemloantxn + totvoloan - totshgcashdebit - totvocashdebit - totmemloan - totvoloantxn

            voFinTxnViewModel!!.updateclosingbalbank(
                totalclosingbal,
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                accountno
            )
            //view.tv_bank_amount.text = totalclosingbal.toString()

        }


        /* var totcashwithdrawl = generateMeetingViewmodel.getsumwithdrwal(
             validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
             validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
 */

        /* generateMeetingViewmodel.updateclosingcash(
             totalclosingbal,
             validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
             validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
         )*/
    }

    fun setSumValue(textView: TextView, imageView: ImageView, value: Int, stringPrefix: String) {
        if (value != null && value > 0) {
            if (stringPrefix.trim().length > 0) {
                textView.text = stringPrefix + " " + validate!!.returnStringValue(value.toString())
            } else {
                textView.text = validate!!.returnStringValue(value.toString())
            }

            imageView.visibility = View.VISIBLE
        } else {
            textView.text = "0"
            imageView.visibility = View.GONE
        }
    }

    fun CustomAlertCloseMeeting(str: String) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str

        mDialogView.btn_yes.background = resources.getDrawable(R.drawable.button_bg_vomeeting)
        mDialogView.btn_no.background = resources.getDrawable(R.drawable.button_bg_vomeeting)
        mDialogView.btn_yes.setOnClickListener {
// PartialSaveData()

            generateMeetingViewmodel!!.closingMeeting(
                "C",
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                validate!!.RetriveSharepreferenceString(
                    AppSP.userid
                )!!
            )
            mAlertDialog.dismiss()
            var intent = Intent(this, VoMeetingListActivity1::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)


        }
        mDialogView.btn_no.setOnClickListener {
            mAlertDialog.dismiss()

        }
    }

    override fun onBackPressed() {
        var intent = Intent(this, VoMeetingListActivity1::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

}