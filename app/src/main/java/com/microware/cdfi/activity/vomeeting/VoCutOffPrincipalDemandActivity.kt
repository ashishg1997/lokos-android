package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.VoCutOffPrincipalDemandAdapter
import com.microware.cdfi.adapter.vomeetingadapter.VoPrincipalDemandAdapter
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import com.microware.cdfi.viewModel.voviewmodel.VoMemLoanScheduleViewModel
import kotlinx.android.synthetic.main.activity_vo_groupprincipal_demand.*
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.meeting_detail_item.*
import kotlinx.android.synthetic.main.repay_toolbar_vomeeting.*

class VoCutOffPrincipalDemandActivity: AppCompatActivity() {
    var validate: Validate? = null
    lateinit var voMemLoanScheduleViewModel: VoMemLoanScheduleViewModel
    var totalloanamt = 0
    var Todayvalue=0
    var Cumvalue=0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vo_groupprincipal_demand)
        voMemLoanScheduleViewModel = ViewModelProviders.of(this).get(VoMemLoanScheduleViewModel::class.java)
        validate = Validate(this)

        tv_title.text = LabelSet.getText(
            "loanschedule",
            R.string.loanschedule
        )
        tv_code.text = validate!!.RetriveSharepreferenceLong(VoSpData.voShgcode).toString()
        tv_nam.text = validate!!.RetriveSharepreferenceString(VoSpData.voShgName)
        tv_date.text =
            validate!!.convertDatetime(validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate))
        tv_count.text =  validate!!.RetriveSharepreferenceInt(VoSpData.voMemberCount).toString()

        tv_name.text = validate!!.RetriveSharepreferenceString(VoSpData.voMemberName)
      //  tv_memcode.text = validate!!.RetriveSharepreferenceLong(VoSpData.voMemberCode).toString()

        ic_Back.setOnClickListener {
            if(validate!!.RetriveSharepreferenceInt(VoSpData.voMeetingType) == 11 || validate!!.RetriveSharepreferenceInt(
                    VoSpData.voMeetingType) == 12) {
                var intent = Intent(this, VoCutOffMenuActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                finish()
                overridePendingTransition(0, 0)
            } else{
                var intent = Intent(this, VoMeetingMenuActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                finish()
                overridePendingTransition(0, 0)
            }
        }

        btn_cancel.setOnClickListener {
            if(validate!!.RetriveSharepreferenceInt(VoSpData.voMeetingType) == 11 || validate!!.RetriveSharepreferenceInt(
                    VoSpData.voMeetingType) == 12) {
                var intent = Intent(this, VoCutOffMenuActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                finish()
                overridePendingTransition(0, 0)
            } else {
                var intent = Intent(this, VoMeetingMenuActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                finish()
                overridePendingTransition(0, 0)
            }

        }

        btn_save.isEnabled =
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber) >= validate!!.RetriveSharepreferenceInt(VoSpData.vomaxmeetingnumber)

        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                updateschedule()
            }
        }

        setLabelText()
        fillRecyclerView()
        //updateVoSHGType()
    }

    override fun onBackPressed() {
        if(validate!!.RetriveSharepreferenceInt(VoSpData.voMeetingType) == 11 || validate!!.RetriveSharepreferenceInt(
                VoSpData.voMeetingType) == 12) {
            var intent = Intent(this, VoCutOffMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)
        }
        else{
            var intent = Intent(this, VoMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)
        }
    }

    fun setLabelText() {

        tv_installment_no.text = LabelSet.getText(
            "installment_no",
            R.string.installment_no
        )
        tv_installment_date.text = LabelSet.getText(
            "installment_date",
            R.string.installment_date
        )
        tv_principal_demand.text = LabelSet.getText(
            "principal_demand",
            R.string.principal_demand
        )
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )

    }

    private fun checkValidation(): Int {

        var value = 1
        if (!getTotalValue()) {
            validate!!.CustomAlert(
                LabelSet.getText(
                    "pleaseenterloanamtsame",
                    R.string.pleaseenterloanamtsame
                ), this

            )
            value = 0
            return value
        }
        return value
    }

    fun getTotalValue(): Boolean {
        var iValue = 0
        var iValueCum = 0
        val iCount = rvMemberList.childCount


        for (i in 0 until iCount) {
            val gridChild =
                rvMemberList.getChildAt(i) as? ViewGroup

            val et_principal_demand = gridChild!!
                .findViewById<View>(R.id.et_principal_demand) as? EditText
            val tv_principal_demand = gridChild
                .findViewById<View>(R.id.tv_principal_demand) as? TextView



            if (!et_principal_demand!!.text.toString().isNullOrEmpty()) {
                iValue = iValue + validate!!.returnIntegerValue(et_principal_demand.text.toString())
            }
            if (!tv_principal_demand!!.text.toString().isNullOrEmpty()) {
                iValueCum =
                    iValueCum + validate!!.returnIntegerValue(tv_principal_demand.text.toString())
                totalloanamt = iValueCum
            }


        }
        tv_TotalTodayValue.text = iValue.toString()
        tv_loanosvalue.text = iValueCum.toString()
        totalloanamt = iValueCum
        return iValue == iValueCum
    }

    fun getTotalValue(iValue: Int,flag:Int) {
        if(flag==1) {
            Todayvalue = Todayvalue + iValue
            tv_TotalTodayValue.text = Todayvalue.toString()
        }else if(flag==2){
            Cumvalue = Cumvalue + iValue
            tv_loanosvalue.text = Cumvalue.toString()
        }
    }

    private fun fillRecyclerView() {
        Todayvalue=0
        Cumvalue=0
        var list = voMemLoanScheduleViewModel.getMemberScheduledata(
            validate!!.RetriveSharepreferenceLong(VoSpData.voShgMemID),
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceInt(VoSpData.voLoanno)
        )
        rvMemberList.layoutManager = LinearLayoutManager(this)
        val voprincipalDemandAdapter =
            VoCutOffPrincipalDemandAdapter(this, list)
        rvMemberList.layoutManager = LinearLayoutManager(this)

        val isize: Int
        isize = list!!.size
        val params: ViewGroup.LayoutParams = rvMemberList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvMemberList.layoutParams = params
        rvMemberList.adapter = voprincipalDemandAdapter
    }

    fun updateschedule() {
        var saveValue = 0
        var totalValue = 0

        val iCount = rvMemberList.childCount


        for (i in 0 until iCount) {
            val gridChild =
                rvMemberList.getChildAt(i) as? ViewGroup

            val et_principal_demand = gridChild!!
                .findViewById<View>(R.id.et_principal_demand) as? EditText

            val tv_installment_no_value = gridChild
                .findViewById<View>(R.id.tv_installment_no_value) as? TextView


            var installmentno =
                validate!!.returnIntegerValue(tv_installment_no_value!!.text.toString())

            var prinicpaldemand =
                validate!!.returnIntegerValue(et_principal_demand!!.text.toString())

            var loandemandod = prinicpaldemand
            totalValue = totalValue + prinicpaldemand

            var loanos = totalloanamt - totalValue
            saveValue = saveData(prinicpaldemand, loandemandod, loanos, installmentno)


        }

        if (saveValue > 0) {

            if (validate!!.RetriveSharepreferenceInt(VoSpData.voMeetingType) == 11 || validate!!.RetriveSharepreferenceInt(
                    VoSpData.voMeetingType) == 12) {

                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "data_saved_successfully",
                        R.string.data_saved_successfully),
                    this, VoCutOffMenuActivity::class.java
                )
            } else{
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "data_saved_successfully",
                        R.string.data_saved_successfully),
                    this, VOtoSHGLoanSummary::class.java
                )
            }
        }
    }

    fun saveData(principaldemand: Int, loandemandos: Int, loanos: Int, installmentno: Int): Int {

        voMemLoanScheduleViewModel.updateLoanMemberSchedule(
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(VoSpData.voShgMemID),
            validate!!.RetriveSharepreferenceInt(VoSpData.voLoanno),
            installmentno, principaldemand, loandemandos, loanos)

        return 1
    }

    fun updateVoSHGType() {
        tv_count.setBackgroundResource(R.drawable.item_countactive)
        iv_stauscolor.setBackgroundResource(R.drawable.item_countactive)
        if (validate!!.RetriveSharepreferenceInt(VoSpData.voSHGType) == 1) {
            iv_stauscolor.setImageResource(R.drawable.ic_womangroup)
            iv_stauscolor.visibility = View.INVISIBLE
        } else {
            iv_stauscolor.visibility = View.VISIBLE
            if (validate!!.RetriveSharepreferenceInt(VoSpData.voTag) == 1) {
                iv_stauscolor.setImageResource(R.drawable.ic_pvtg)
            } else if (validate!!.RetriveSharepreferenceInt(VoSpData.voTag) == 2) {
                iv_stauscolor.setImageResource(R.drawable.ic_elderly)
            } else if (validate!!.RetriveSharepreferenceInt(VoSpData.voTag) == 3) {
                iv_stauscolor.setImageResource(R.drawable.ic_disable)
            } else if (validate!!.RetriveSharepreferenceInt(VoSpData.voTag) == 99) {
                iv_stauscolor.setImageResource(R.drawable.ic_other)
            }
        }}
}
