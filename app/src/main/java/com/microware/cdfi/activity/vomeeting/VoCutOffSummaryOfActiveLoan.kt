package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.VoCutOffSummaryOfActiveLoanAdapter
import com.microware.cdfi.entity.voentity.VoMemLoanEntity
import com.microware.cdfi.entity.voentity.VoMtgDetEntity
import com.microware.cdfi.fragment.VoCutOffTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoGenerateMeetingViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoMemLoanScheduleViewModel
import com.microware.cdfi.viewModel.voviewmodel.VoMemLoanViewModel
import com.microware.cdfi.viewModel.voviewmodel.VoMtgDetViewModel
import kotlinx.android.synthetic.main.activity_vo_cut_off_summary_of_active_loan_vo_to_shg.*

class VoCutOffSummaryOfActiveLoan : AppCompatActivity() {

    lateinit var voCutOffSummaryOfActiveLoanAdapter : VoCutOffSummaryOfActiveLoanAdapter
    var validate: Validate? = null
    var voMemLoanViewModel: VoMemLoanViewModel? = null
    var voMemLoanScheduleViewModel: VoMemLoanScheduleViewModel? = null
    var voGenerateMeetingViewmodel: VoGenerateMeetingViewmodel? = null
    var vomtgDetViewmodel: VoMtgDetViewModel? = null
    var lookupViewmodel: LookupViewmodel? = null
    var memberlist: List<VoMtgDetEntity>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vo_cut_off_summary_of_active_loan_vo_to_shg)

        validate = Validate(this)
        voMemLoanScheduleViewModel= ViewModelProviders.of(this).get(VoMemLoanScheduleViewModel::class.java)
        voMemLoanViewModel = ViewModelProviders.of(this).get(VoMemLoanViewModel::class.java)
        voGenerateMeetingViewmodel = ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        vomtgDetViewmodel = ViewModelProviders.of(this).get(VoMtgDetViewModel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)

        setLabelText()

        replaceFragmenty(
            fragment = VoCutOffTopBarFragment(9),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        fillmemberspinner()
        fillRecyclerView()

    }

    fun setLabelText(){
        tv_loans.text = LabelSet.getText("Loans", R.string.Loans)
        total_outstanding_ammount.text = LabelSet.getText("total_outstanding_amount", R.string.total_outstanding_amount)
        tv_interest_rate.text = LabelSet.getText("interest_rate", R.string.interest_rate)
        tv_installment_amount.text = LabelSet.getText("installment_amount_principal", R.string.installment_amount_principal)
    }

    private fun fillRecyclerView() {

        val list = voGenerateMeetingViewmodel!!.getCutOffClosedMemberLoanDataByMtgNum(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            false)

        rvList.layoutManager = LinearLayoutManager(this)
        voCutOffSummaryOfActiveLoanAdapter =
            VoCutOffSummaryOfActiveLoanAdapter(this, list)

        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = rvList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvList.layoutParams = params

        rvList.adapter = voCutOffSummaryOfActiveLoanAdapter
    }

    fun getTotalValue() {
        var iValue1 = 0
        var iValue2 = 0
        val iCount = rvList.childCount


        for (i in 0 until iCount) {
            val gridChild =
                rvList.getChildAt(i) as? ViewGroup

            val tv_total_outstanding = gridChild!!
                .findViewById<View>(R.id.tv_totalOutstandingAmount) as? TextView

            val tv_loan_disbrused = gridChild
                .findViewById<View>(R.id.tv_loan_disbrused) as? TextView

            if (!tv_total_outstanding!!.text.toString().isNullOrEmpty()) {
                iValue1 += validate!!.returnIntegerValue(tv_total_outstanding.text.toString())
            }

            if (!tv_loan_disbrused!!.text.toString().isNullOrEmpty()) {
                iValue2 += validate!!.returnIntegerValue(tv_loan_disbrused.text.toString())
            }


        }
        tv_sum_outstanding_Amount.text = iValue1.toString()
        tv_sum_total_loan.text = iValue2.toString()

    }

    private fun fillmemberspinner() {
        memberlist = vomtgDetViewmodel!!.getListDataMtgByMtgnum(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        val adapter: ArrayAdapter<String?>
        if (!memberlist.isNullOrEmpty()) {
            val isize = memberlist!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in memberlist!!.indices) {
                sValue[i + 1] = memberlist!![i].childCboName
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_shgName.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(memberlist!!.size + 1)
            sValue[0] = LabelSet.getText(
                "nomemberavailable",
                R.string.nomemberavailable
            )
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_shgName.adapter = adapter
        }
       // setMember()
    }

    fun returnmemid(): Long {

        var pos = spin_shgName.selectedItemPosition
        var id = 0L

        if (!memberlist.isNullOrEmpty()) {
            if (pos > 0)
                id = memberlist!!.get(pos - 1).memId
        }
        return id
    }

    fun returnmempos(): Int {

        var pos = 0

        if (!memberlist.isNullOrEmpty()) {
            if (pos > 0)
                pos = spin_shgName.selectedItemPosition
        }

        return pos
    }

    override fun onBackPressed() {
        var intent = Intent(this, VoCutOffMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

}