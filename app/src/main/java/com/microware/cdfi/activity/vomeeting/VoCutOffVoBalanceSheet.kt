package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.microware.cdfi.R
import com.microware.cdfi.fragment.VoCutOffTopBarFragment
import com.microware.cdfi.utility.replaceFragmenty
import kotlinx.android.synthetic.main.activity_cut_off_vo_balance_sheet.*


class VoCutOffVoBalanceSheet : AppCompatActivity() {
    var button : Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cut_off_vo_balance_sheet)

        replaceFragmenty(
            fragment = VoCutOffTopBarFragment(17),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        iv_downliabilities.setOnClickListener {
            lay_liabilities.visibility=View.VISIBLE
            iv_upliabilities.visibility=View.VISIBLE
            iv_downliabilities.visibility=View.GONE
            iv_downassets.visibility = View.VISIBLE
            iv_downsurplusloss.visibility = View.VISIBLE
            iv_upassets.visibility = View.GONE
            iv_upsurplusloss.visibility = View.GONE
            lay_assets.visibility = View.GONE
            iv_downliabilities.visibility = View.GONE
            lay_surplus_or_loss.visibility = View.GONE
        }

        iv_downassets.setOnClickListener {
            lay_assets.visibility = View.VISIBLE
            lay_surplus_or_loss.visibility = View.GONE
            lay_liabilities.visibility = View.GONE
            iv_upliabilities.visibility = View.GONE
            iv_downliabilities.visibility = View.VISIBLE
            iv_downassets.visibility = View.GONE
            iv_downsurplusloss.visibility = View.VISIBLE
            iv_upassets.visibility = View.VISIBLE
            iv_upsurplusloss.visibility = View.GONE
        }

        iv_downsurplusloss.setOnClickListener {
            lay_surplus_or_loss.visibility = View.VISIBLE
            lay_assets.visibility = View.GONE
            lay_liabilities.visibility = View.GONE
            iv_upliabilities.visibility = View.GONE
            iv_downliabilities.visibility = View.VISIBLE
            iv_downassets.visibility = View.VISIBLE
            iv_downsurplusloss.visibility = View.GONE
            iv_upassets.visibility = View.GONE
            iv_upsurplusloss.visibility = View.VISIBLE
        }

        iv_upliabilities.setOnClickListener {
            lay_liabilities.visibility = View.GONE
            iv_upliabilities.visibility = View.GONE
            iv_downliabilities.visibility = View.VISIBLE
        }

        iv_upassets.setOnClickListener {
            lay_assets.visibility = View.GONE
            iv_upassets.visibility = View.GONE
            iv_downassets.visibility = View.VISIBLE
        }

        iv_upsurplusloss.setOnClickListener {
            lay_surplus_or_loss.visibility = View.GONE
            iv_upsurplusloss.visibility = View.GONE
            iv_downsurplusloss.visibility = View.VISIBLE
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        val i = Intent(this, VoCutOffMenuActivity::class.java)
        startActivity(i)
    }

  /*  fun buttonClicked(view: View) {
        if (view.id == R.id.btn_hide_show) {
            // button liabilities action
            if (content_liabilities.visibility == View.VISIBLE) {
                content_liabilities.visibility = View.GONE
                btn_hide_show.setImageDrawable(resources.getDrawable(R.drawable.ic_arrow_down_2))
            } else {
                content_liabilities.visibility = View.VISIBLE
                btn_hide_show.setImageDrawable(resources.getDrawable(R.drawable.ic_arrow_up))
            }
        } else if (view.id == R.id.btn_hide_show_2) {
            //button assets action
            if (content_assets.visibility == View.VISIBLE) {
                content_assets.visibility = View.GONE
                btn_hide_show_2.setImageDrawable(resources.getDrawable(R.drawable.ic_arrow_down_2))
            } else {
                content_assets.visibility = View.VISIBLE
                btn_hide_show_2.setImageDrawable(resources.getDrawable(R.drawable.ic_arrow_up))
            }
        } else if (view.id == R.id.btn_hide_show_3) {
            //button surplus action
            if (content_surplus_loss.visibility == View.VISIBLE) {
                content_surplus_loss.visibility = View.GONE
                btn_hide_show_3.setImageDrawable(resources.getDrawable(R.drawable.ic_arrow_down_2))
            } else {
                content_surplus_loss.visibility = View.VISIBLE
                btn_hide_show_3.setImageDrawable(resources.getDrawable(R.drawable.ic_arrow_up))
            }
        }
    }*/
}