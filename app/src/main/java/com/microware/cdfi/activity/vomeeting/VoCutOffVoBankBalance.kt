package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ArrayAdapter
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.fragment.VoCutOffTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.voviewmodel.VoFinTxnViewModel
import com.microware.cdfi.viewModel.voviewmodel.VoGenerateMeetingViewmodel
import kotlinx.android.synthetic.main.activity_cut_off_vo_bank_balance.*
import kotlinx.android.synthetic.main.buttons_vo.*

class VoCutOffVoBankBalance : AppCompatActivity() {
    var validate: Validate? = null
    var voBankList: List<Cbo_bankEntity>? = null
    var Todayvalue = 0

    lateinit var voGenerateMeetingViewmodel: VoGenerateMeetingViewmodel
    lateinit var voFinTxnViewModel: VoFinTxnViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cut_off_vo_bank_balance)
        validate = Validate(this)

        voGenerateMeetingViewmodel =
            ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        voFinTxnViewModel =
            ViewModelProviders.of(this).get(VoFinTxnViewModel::class.java)

        replaceFragmenty(
            fragment = VoCutOffTopBarFragment(16),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        et_cash_at_bank.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value =
                    validate!!.returnIntegerValue(et_cheque_issued_but_amount_not_debited.text.toString())
                val value1 =
                    validate!!.returnIntegerValue(et_cheque_received_but_amount_not_credited.text.toString())
                val totalAmt = validate!!.returnIntegerValue(s.toString()) + value1 - value
                et_total_balance.setText(validate!!.returnStringValue(totalAmt.toString()))
            }

        })

        et_cheque_issued_but_amount_not_debited.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = validate!!.returnIntegerValue(et_cash_at_bank.text.toString())
                val value1 =
                    validate!!.returnIntegerValue(et_cheque_received_but_amount_not_credited.text.toString())
                val totalAmt = value + value1 - validate!!.returnIntegerValue(s.toString())
                et_total_balance.setText(validate!!.returnStringValue(totalAmt.toString()))

            }

        })

        et_cheque_received_but_amount_not_credited.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value =
                    validate!!.returnIntegerValue(et_cheque_issued_but_amount_not_debited.text.toString())
                val value1 = validate!!.returnIntegerValue(et_cash_at_bank.text.toString())
                val totalAmt = value1 + validate!!.returnIntegerValue(s.toString()) - value
                et_total_balance.setText(validate!!.returnStringValue(totalAmt.toString()))
            }

        })

        et_date_of_balance.setOnClickListener(View.OnClickListener {
            validate!!.datePickerwithminmaxdate(validate!!.RetriveSharepreferenceLong(VoSpData.voFormation_dt),validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),et_date_of_balance)
        })

        btn_cancel.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, VoCutOffVoBankBalanceList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        })

        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                saveData()
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "data_saved_successfully",
                        R.string.data_saved_successfully
                    ), this, VoCutOffVoBankBalanceList::class.java
                )

            }
        }

        ivAdd.setOnClickListener {
            clearAll()
        }

        setLabelText()
        fillbank()
        showData()
    }

    override fun onBackPressed() {
        val intent = Intent(this, VoCutOffVoBankBalanceList::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

    fun setLabelText() {
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText("cancel", R.string.cancel)
        tv_name_of_bank.text = LabelSet.getText("name_of_bank", R.string.name_of_bank)
        tv_cash_at_bank.text = LabelSet.getText("cash_at_bank", R.string.cash_at_bank)
        tv_cheque_issued_but_amount_not_debited.text = LabelSet.getText(
            "cheque_issued_but_amount_not_debited",
            R.string.cheque_issued_but_amount_not_debited
        )
        tv_cheque_received_but_amount_not_credited.text = LabelSet.getText(
            "cheque_received_but_amount_not_credited",
            R.string.cheque_received_but_amount_not_credited
        )
        tv_total_balance.text = LabelSet.getText("total_balance", R.string.total_balance)
        tv_date_of_balance.text = LabelSet.getText("date_of_balance", R.string.date_of_balance)
        et_cash_at_bank.hint = LabelSet.getText("amount", R.string.amount)
        et_cheque_issued_but_amount_not_debited.hint = LabelSet.getText("amount", R.string.amount)
        et_cheque_received_but_amount_not_credited.hint = LabelSet.getText(
            "amount",
            R.string.amount
        )
        et_total_balance.hint = LabelSet.getText("auto", R.string.auto)
        et_date_of_balance.hint = LabelSet.getText("date_format", R.string.date_format)
    }

    private fun checkValidation(): Int {
        var value = 1

        if (spin_bank_name.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this,
                spin_bank_name,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "name_of_bank",
                    R.string.name_of_bank
                )
            )
            value = 0
            return value
        } else if (et_cash_at_bank.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "cash_at_bank",
                    R.string.cash_at_bank
                ),
                this,
                et_cash_at_bank
            )
            value = 0
            return value
        } else if (et_cheque_issued_but_amount_not_debited.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "cheque_issued_but_amount_not_debited",
                    R.string.cheque_issued_but_amount_not_debited
                ),
                this,
                et_cheque_issued_but_amount_not_debited
            )
            value = 0
            return value
        } else if (et_cheque_received_but_amount_not_credited.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "cheque_received_but_amount_not_credited",
                    R.string.cheque_received_but_amount_not_credited
                ),
                this,
                et_cheque_received_but_amount_not_credited
            )
            value = 0
            return value
        } else if (et_date_of_balance.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "date_of_balance",
                    R.string.date_of_balance
                ),
                this,
                et_date_of_balance
            )
            value = 0
            return value
        }

        return value
    }

    fun fillbank() {
        voBankList =
            voGenerateMeetingViewmodel.getBankdata(
                validate!!.RetriveSharepreferenceString(
                    VoSpData.voSHGGUID
                )
            )

        val adapter: ArrayAdapter<String?>
        if (!voBankList.isNullOrEmpty()) {
            val isize = voBankList!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in voBankList!!.indices) {
                var lastthree =
                    voBankList!![i].account_no.substring(voBankList!![i].account_no.length - 3)
                sValue[i + 1] =
                    voBankList!![i].ifsc_code.toString().dropLast(7) + "XXXXX" + lastthree
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_bank_name.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_bank_name.adapter = adapter
        }

    }

    fun returaccount(): String {

        var pos = spin_bank_name.selectedItemPosition
        var id = ""

        if (!voBankList.isNullOrEmpty()) {
            if (pos > 0) id =
                voBankList!!.get(pos - 1).ifsc_code!!.dropLast(7) + voBankList!!.get(pos - 1).account_no
        }
        return id
    }

    fun setaccount(accountno: String): Int {

        var pos = 0

        if (!voBankList.isNullOrEmpty()) {
            for (i in voBankList!!.indices) {
                if (accountno.equals(
                        voBankList!!.get(i).ifsc_code!!.dropLast(7) + voBankList!!.get(
                            i
                        ).account_no
                    )
                )
                    pos = i + 1
            }
        }
        spin_bank_name.setSelection(pos)
        return pos
    }

    private fun saveData(){
        voGenerateMeetingViewmodel.updateCutOffBankBalance(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            returaccount(),
            validate!!.returnIntegerValue(et_cash_at_bank.text.toString()),
            validate!!.returnIntegerValue(et_cheque_issued_but_amount_not_debited.text.toString()),
            validate!!.returnIntegerValue(et_cheque_received_but_amount_not_credited.text.toString()),
            validate!!.returnIntegerValue(et_total_balance.text.toString()),
            validate!!.Daybetweentime(et_date_of_balance.text.toString()),
            validate!!.RetriveSharepreferenceString(AppSP.userid)!!,
            validate!!.Daybetweentime(validate!!.currentdatetime),
            validate!!.RetriveSharepreferenceString(AppSP.userid)!!

        )
    }


    private fun showData(){
        var list = voFinTxnViewModel.getListDataByMtgnum(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceString(VoSpData.voBankCode)!!)
        if(!list.isNullOrEmpty()){
            setaccount(validate!!.returnStringValue(list.get(0).bankCode))
            et_cash_at_bank.setText(validate!!.returnStringValue(list.get(0).zeroMtgCashBank.toString()))
            et_cheque_issued_but_amount_not_debited.setText(validate!!.returnStringValue(list.get(0).chequeIssuedNotRealized.toString()))
            et_cheque_received_but_amount_not_credited.setText(validate!!.returnStringValue(list.get(0).chequeReceivedNotCredited.toString()))
            et_date_of_balance.setText(validate!!.convertDatetime(validate!!.returnLongValue(list.get(0).balanceDate.toString())))
        }
    }


    fun clearAll() {
        spin_bank_name.setSelection(0)
        et_cash_at_bank.setText("")
        et_cheque_issued_but_amount_not_debited.setText("")
        et_cheque_received_but_amount_not_credited.setText("")
        et_total_balance.setText("")
        et_date_of_balance.setText("")
    }
}