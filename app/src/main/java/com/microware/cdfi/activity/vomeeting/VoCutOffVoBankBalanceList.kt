package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.VoCutOffVoBankBalanceAdapter
import com.microware.cdfi.fragment.VoCutOffTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.voviewmodel.VoGenerateMeetingViewmodel
import kotlinx.android.synthetic.main.layout_vo_cutoff_vo_bank_balance_list.*

class VoCutOffVoBankBalanceList : AppCompatActivity() {

    var validate: Validate? = null
    lateinit var voGenerateMeetingViewmodel: VoGenerateMeetingViewmodel
    var Todayvalue = 0
    var Cumvalue = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_vo_cutoff_vo_bank_balance_list)
        validate = Validate(this)

        voGenerateMeetingViewmodel =
            ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)

        replaceFragmenty(
            fragment = VoCutOffTopBarFragment(16),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        setLabelText()
        fillRecyclerView()

    }

    private fun fillRecyclerView() {
        var list = voGenerateMeetingViewmodel.getBankListDataByMtgnum(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        if (!list.isNullOrEmpty()) {
            rvList.layoutManager = LinearLayoutManager(this)
            val voCutOffVoBankBalanceAdapter = VoCutOffVoBankBalanceAdapter(this, list)
            val isize: Int
            isize = list.size
            val params: ViewGroup.LayoutParams = rvList.layoutParams
            val r = resources
            val px = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_PX,
                resources.getDimension(R.dimen.spraying),
                r.displayMetrics
            )
            val hi = Math.round(px)
            val gridHeight = hi * isize
            params.height = gridHeight
            rvList.layoutParams = params
            rvList.adapter = voCutOffVoBankBalanceAdapter
        }

    }

    private fun setLabelText() {
        tv_srno.text = LabelSet.getText("sr_no", R.string.sr_no)
        tv_bank_name.text = LabelSet.getText(
            "bank",
            R.string.bank
        )
        tv_total_Cash_in_hand.text = LabelSet.getText(
            "amount",
            R.string.amount
        )

        //   tv_receiptAmount.setText(LabelSet.getText("receipt_amount", R.string.receipt_amount))

        tvtotal.text = LabelSet.getText("total", R.string.total)
    }

    fun setaccount(accountno: String): String {

        var bankName = ""
        var voBankList =
            voGenerateMeetingViewmodel.getBankdata(validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID))

        if (!voBankList.isNullOrEmpty()) {
            for (i in voBankList.indices) {
                if (accountno.equals(
                        voBankList.get(i).ifsc_code!!.dropLast(7) + voBankList.get(
                            i
                        ).account_no
                    )
                )
                    bankName =
                        voBankList.get(i).ifsc_code!!.dropLast(7) + voBankList.get(i).account_no
            }
        }
        return bankName
    }

    fun getTotalValue() {
        var iValue1 = 0
        val iCount = rvList.childCount
        for (i in 0 until iCount) {
            val gridChild = rvList.getChildAt(i) as? ViewGroup
            val tv_value1 = gridChild!!.findViewById<View>(R.id.tv_amount) as? TextView

            if (!tv_value1!!.text.toString().isNullOrEmpty()) {
                iValue1 += validate!!.returnIntegerValue(tv_value1.text.toString())
            }

        }

        tv_TotalTodayValue.text = iValue1.toString()


    }

    fun getTotalValue1(iValue: Int) {
        Todayvalue += iValue
        tv_TotalTodayValue.text = Todayvalue.toString()
    }

    override fun onBackPressed() {
        val intent = Intent(this, VoCutOffMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()

    }
}