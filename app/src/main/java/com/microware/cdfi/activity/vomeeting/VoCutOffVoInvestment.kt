package com.microware.cdfi.activity.vomeeting

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProviders
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.google.gson.JsonObject
import com.microware.cdfi.R
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.*
import com.microware.cdfi.entity.voentity.VoFinTxnDetGrpEntity
import com.microware.cdfi.fragment.VoCutOffTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.CboBankViewmodel
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.MasterBankBranchViewModel
import com.microware.cdfi.viewModel.MasterBankViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoFinTxnDetGrpViewModel
import com.microware.cdfi.viewModel.voviewmodel.VoGenerateMeetingViewmodel
import kotlinx.android.synthetic.main.activity_cut_off_vo_investment.*
import kotlinx.android.synthetic.main.buttons_vo.*
import kotlinx.android.synthetic.main.customealertdialogelogin.*
import kotlinx.android.synthetic.main.customealertdialogelogin.view.*
import okhttp3.ResponseBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class VoCutOffVoInvestment : AppCompatActivity() {
    var validate: Validate? = null
    var dataspin_investment: List<MstVOCOAEntity>? = null
    var dataspin_placeinvestment: List<LookupEntity>? = null
    var voBankList: List<Cbo_bankEntity>? = null
    var voFinTxnDetGrpEntity: VoFinTxnDetGrpEntity? = null
    lateinit var voGenerateMeetingViewmodel: VoGenerateMeetingViewmodel
    var lookupViewmodel: LookupViewmodel? = null
    var vofintxnDetGrpViewModel: VoFinTxnDetGrpViewModel? = null

    var cboBankEntity: Cbo_bankEntity? = null
    var cboBankViewmodel: CboBankViewmodel? = null
    var dataspin_bank: List<BankEntity>? = null
    var dataspin_bankBranch: List<Bank_branchEntity>? = null
    var dataspin_branch: List<Bank_branchEntity>? = null
    var bankMasterViewModel: MasterBankViewmodel? = null
    var masterbankBranchViewmodel: MasterBankBranchViewModel? = null
    var sBankCode = 0
    var branch_id = 0
    var apiInterface: ApiInterface? = null
    internal lateinit var progressDialog: ProgressDialog


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cut_off_vo_investment)

        validate = Validate(this)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        vofintxnDetGrpViewModel =
            ViewModelProviders.of(this).get(VoFinTxnDetGrpViewModel::class.java)
        voGenerateMeetingViewmodel =
            ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        cboBankViewmodel = ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        bankMasterViewModel = ViewModelProviders.of(this).get(MasterBankViewmodel::class.java)
        masterbankBranchViewmodel =
            ViewModelProviders.of(this).get(MasterBankBranchViewModel::class.java)

        replaceFragmenty(
            fragment = VoCutOffTopBarFragment(12),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        et_date_of_investment.setOnClickListener {
            validate!!.datePickerwithminmaxdate(validate!!.RetriveSharepreferenceLong(VoSpData.voFormation_dt),validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),et_date_of_investment)
        }

        spin_bankName?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                if (position > 0) {
                    sBankCode = validate!!.returnMasterBankID(spin_bankName, dataspin_bank)
                    bindBankBranchSpinner(sBankCode)
                } else {
                    bindBankBranchSpinner(0)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        spin_bankBranch?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                if (position > 0) {
                    var branchifsc =
                        validate!!.returnMasterBankBranchifsc(spin_bankBranch, dataspin_bankBranch)
                    et_ifsc_code.setText(branchifsc)

                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // TODO Auto-generated method stub

            }
        }

        spin_place_of_investment?.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {

                override fun onItemSelected(
                    parent: AdapterView<*>, view: View?,
                    position: Int, id: Long
                ) {
                    if (position > 0) {
                        val placeId = validate!!.returnLookupcode(
                            spin_place_of_investment,
                            dataspin_placeinvestment
                        )
                        if (placeId == 8) {
                            lay_ifsc_code.visibility = View.VISIBLE
                            viewIfsc.visibility = View.VISIBLE
                            lay_name_of_bank.visibility = View.VISIBLE
                            viewBank.visibility = View.VISIBLE
                            lay_bank_branch.visibility = View.VISIBLE
                            viewBranch.visibility = View.VISIBLE
                        } else {
                            lay_ifsc_code.visibility = View.GONE
                            viewIfsc.visibility = View.GONE
                            lay_name_of_bank.visibility = View.GONE
                            viewBank.visibility = View.GONE
                            lay_bank_branch.visibility = View.GONE
                            viewBranch.visibility = View.GONE
                        }
                    } else {
                        lay_ifsc_code.visibility = View.GONE
                        viewIfsc.visibility = View.GONE
                        lay_name_of_bank.visibility = View.GONE
                        viewBank.visibility = View.GONE
                        lay_bank_branch.visibility = View.GONE
                        viewBranch.visibility = View.GONE
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // TODO Auto-generated method stub

                }
            }

        Imgsearch.setOnClickListener {

            spin_bankName.setSelection(0)
            branch_id = 0
            // spin_branch.setSelection(0)
            if (et_ifsc_code.text.toString().toUpperCase().trim().length == 11) {
                dataspin_branch =
                    masterbankBranchViewmodel!!.BankBranchlistifsc(
                        et_ifsc_code.text.toString().toUpperCase()
                    )

                if (!dataspin_branch.isNullOrEmpty()) {
                    branch_id = dataspin_branch!!.get(0).bank_branch_id
                    spin_bankName.setSelection(
                        validate!!.returnMasterBankpos(
                            validate!!.returnIntegerValue(dataspin_branch!!.get(0).bank_id.toString()),
                            dataspin_bank
                        )
                    )
                    bindBankBranchSpinner(validate!!.returnIntegerValue(dataspin_branch!!.get(0).bank_id.toString()))

                } else if (et_ifsc_code.text.toString().toUpperCase()
                        .trim().length == 11 && isNetworkConnected()
                ) {
                    importBankList(et_ifsc_code.text.toString().toUpperCase())

                } else {
                    validate!!.CustomAlertEditText(
                        LabelSet.getText(
                            "no_internet_msg",
                            R.string.no_internet_msg
                        ),
                        this,
                        et_ifsc_code
                    )
                }

            } else {
                validate!!.CustomAlertEditText(
                    LabelSet.getText(
                        "pleaseenetervalidifsc",
                        R.string.pleaseenetervalidifsc
                    ),
                    this,
                    et_ifsc_code
                )
            }
        }

        btn_cancel.setOnClickListener {
            val i = Intent(this, VoCutOffVoInvestmentList::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)
        }

        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                saveData()
            }
        }

        fillSpinner()
        showData()

    }

    private fun saveData() {

        var save = 0
        if (validate!!.RetriveSharepreferenceInt(VoSpData.voAuid) == 0) {

            voFinTxnDetGrpEntity = VoFinTxnDetGrpEntity(
                0,
                0,
                validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                validate!!.returnVOSubHeadcode(spin_type_of_investment, dataspin_investment),
                2,
                0,
                validate!!.returnLookupcode(spin_place_of_investment, dataspin_placeinvestment),
                "PO",
                validate!!.returnIntegerValue(et_amount.text.toString()),
                "",
                validate!!.Daybetweentime(et_date_of_investment.text.toString()),
                2,
                validate!!.returnStringValue(et_ifsc_code.text.toString()),
                /*"",
                0,*/
                "",
                validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                "", 0, "", 0,0,2
            )

            vofintxnDetGrpViewModel!!.insertVoGroupLoanSchedule(voFinTxnDetGrpEntity)

            save = 1

        } else {
            vofintxnDetGrpViewModel!!.updateIncomeAndExpenditure(
                validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceInt(VoSpData.voAuid),
                2,
                validate!!.returnLookupcode(spin_place_of_investment, dataspin_placeinvestment),
                "PO",
                validate!!.returnIntegerValue(et_amount.text.toString()),
                validate!!.Daybetweentime(et_date_of_investment.text.toString()),
                2,
                validate!!.returnStringValue(et_ifsc_code.text.toString()),
                "",
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime)
            )

            save = 2
        }

        if (save == 1) {
            validate!!.CustomAlertVO(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this, VoCutOffVoInvestmentList::class.java
            )
        } else {
            validate!!.CustomAlertVO(
                LabelSet.getText(
                    "data_updated_successfully",
                    R.string.data_updated_successfully
                ),
                this, VoCutOffVoInvestmentList::class.java
            )
        }


    }

    /* fun fillbank() {
         voBankList =
             voGenerateMeetingViewmodel!!.getBankdata(
                 validate!!.RetriveSharepreferenceString(
                     VoSpData.voSHGGUID
                 )
             )

         val adapter: ArrayAdapter<String?>
         if (!voBankList.isNullOrEmpty()) {
             val isize = voBankList!!.size
             val sValue = arrayOfNulls<String>(isize + 1)
             sValue[0] = LabelSet.getText("Select", R.string.Select)
             for (i in voBankList!!.indices) {
                 var lastthree =
                     voBankList!![i].account_no.substring(voBankList!![i].account_no.length - 3)
                 sValue[i + 1] =
                     voBankList!![i].ifsc_code.toString().dropLast(7) + "XXXXX" + lastthree
             }

             adapter = ArrayAdapter(
                 this,
                 R.layout.my_spinner_space, sValue
             )
             adapter.setDropDownViewResource(R.layout.my_spinner)
             spin_bankName.adapter = adapter
         } else {
             val sValue = arrayOfNulls<String>(1)
             sValue[0] = LabelSet.getText("Select", R.string.Select)
             adapter = ArrayAdapter(
                 this,
                 R.layout.my_spinner_space, sValue
             )
             adapter.setDropDownViewResource(R.layout.my_spinner)
             spin_bankName.adapter = adapter
         }

     }

     fun returaccount(): String {

         var pos = spin_bankName.getSelectedItemPosition()
         var id = ""

         if (!voBankList.isNullOrEmpty()) {
             if (pos > 0) id =
                 voBankList!!.get(pos - 1).ifsc_code!!.dropLast(7) + voBankList!!.get(pos - 1).account_no!!
         }
         return id
     }

     fun setaccount(accountno: String): Int {

         var pos = 0

         if (!voBankList.isNullOrEmpty()) {
             for (i in voBankList!!.indices!!) {
                 if (accountno.equals(
                         voBankList!!.get(i).ifsc_code!!.dropLast(7) + voBankList!!.get(
                             i
                         ).account_no
                     )
                 )
                     pos = i + 1
             }
         }
         spin_bankName.setSelection(pos)
         return pos
     } */

    private fun bindBankBranchSpinner(sBankCode: Int) {
        dataspin_bankBranch = masterbankBranchViewmodel!!.getBankMaster(sBankCode)
        validate!!.fillMasterBankBranchspinner(
            this,
            spin_bankBranch,
            dataspin_bankBranch,
            branch_id
        )
        if (branch_id > 0) {
            spin_bankBranch.setSelection(
                validate!!.returnMasterBankBranchpos(
                    branch_id,
                    dataspin_bankBranch
                )
            )
        }
    }

    private fun fillSpinner() {
        dataspin_investment = vofintxnDetGrpViewModel!!.getCoaSubHeadData(
            listOf(61, 60, 59),
            "PO",
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
        )
        dataspin_placeinvestment = lookupViewmodel!!.getplaceOfInvestment(
            91,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )
        dataspin_bank = bankMasterViewModel!!.getBankMaster()
        validate!!.fillVOCoaSubHeadspinner(this, spin_type_of_investment, dataspin_investment)
        validate!!.fillLookupspinner(this, spin_place_of_investment, dataspin_placeinvestment)
        validate!!.fillMasterBankspinner(this, spin_bankName, dataspin_bank)

    }

    private fun checkValidation(): Int {
        var value = 1
        if (spin_type_of_investment.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_type_of_investment,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "saving_investment",
                    R.string.saving_investment
                )

            )
            value = 0
            return value
        }
        else if (spin_place_of_investment.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_place_of_investment,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "place_of_investment",
                    R.string.place_of_investment
                )

            )
            value = 0
            return value
        } else if ((et_ifsc_code.text.toString().length == 0 || et_ifsc_code.text.toString().length != 11) && lay_ifsc_code.visibility == View.VISIBLE) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "pleaseenetervalidifsc",
                    R.string.pleaseenetervalidifsc
                ),
                this,
                et_ifsc_code
            )
            value = 0
            return value
        } else if (spin_bankName.selectedItemPosition == 0 && lay_name_of_bank.visibility == View.VISIBLE) {
            validate!!.CustomAlertSpinner(
                this,
                spin_bankName,
                LabelSet.getText("selectbank", R.string.selectbank)
            )
            value = 0
            return value
        } else if (spin_bankBranch.selectedItemPosition == 0 && lay_bank_branch.visibility == View.VISIBLE) {
            validate!!.CustomAlertSpinner(
                this,
                spin_bankBranch,
                LabelSet.getText(
                    "selectbranch",
                    R.string.selectbranch
                )
            )
            value = 0
            return value
        } else if (validate!!.returnIntegerValue(et_amount.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "pleaseenteramount",
                    R.string.pleaseenteramount
                ), this,
                et_amount
            )
            value = 0
            return value
        } else if (et_date_of_investment.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "date_of_investment_fd_amp_others",
                    R.string.date_of_investment_fd_amp_others
                ), this,
                et_date_of_investment
            )
            value = 0
            return value
        }



        return value
    }

    private fun showData() {
        var list = vofintxnDetGrpViewModel!!.getInvestmentdata(
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceInt(VoSpData.voAuid),
            "PO",
            validate!!.RetriveSharepreferenceInt(VoSpData.vosavingSource)
        )
        if (!list.isNullOrEmpty()) {
            et_ifsc_code.setText(validate!!.returnStringValue(list[0].bankCode))
            spin_place_of_investment.setSelection(
                validate!!.returnLookuppos(
                    list.get(0).amountToFrom,
                    dataspin_placeinvestment
                )
            )
            spin_type_of_investment.setSelection(
                validate!!.returnVOSubHeadpos(
                    list.get(0).auid,
                    dataspin_investment
                )
            )
            et_amount.setText(validate!!.returnStringValue(list.get(0).amount.toString()))
            et_date_of_investment.setText(
                validate!!.convertDatetime(
                    validate!!.returnLongValue(
                        list.get(
                            0
                        ).dateRealisation.toString()
                    )
                )
            )
//            setaccount(list[0].bankCode!!)

            spin_type_of_investment.isEnabled = false
            spin_place_of_investment.isEnabled = false

            if (et_ifsc_code.text.toString().length == 11) {

                spin_bankName.setSelection(0)
                branch_id = 0

                dataspin_branch =
                    masterbankBranchViewmodel!!.BankBranchlistifsc(
                        et_ifsc_code.text.toString().toUpperCase()
                    )

                if (!dataspin_branch.isNullOrEmpty()) {
                    branch_id = dataspin_branch!!.get(0).bank_branch_id
                    spin_bankName.setSelection(
                        validate!!.returnMasterBankpos(
                            validate!!.returnIntegerValue(dataspin_branch!!.get(0).bank_id.toString()),
                            dataspin_bank
                        )
                    )
                    bindBankBranchSpinner(
                        validate!!.returnIntegerValue(
                            dataspin_branch!!.get(
                                0
                            ).bank_id.toString()
                        )
                    )

                } else if (et_ifsc_code.text.toString().toUpperCase()
                        .trim().length == 11 && isNetworkConnected()
                ) {
                    importBankList(et_ifsc_code.text.toString().toUpperCase())

                } else {
                    validate!!.CustomAlertEditText(
                        LabelSet.getText(
                            "no_internet_msg",
                            R.string.no_internet_msg
                        ),
                        this@VoCutOffVoInvestment,
                        et_ifsc_code
                    )
                }

            }

        }
    }

    fun importBankList(ifscCode: String) {
        var progressDialog: ProgressDialog = ProgressDialog.show(
            this, LabelSet.getText("app_name", R.string.app_name),
            LabelSet.getText(
                "fetchingbankdata",
                R.string.fetchingbankdata
            )
        )
        progressDialog.setIcon(this.getDrawable(R.drawable.appicon))


        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
        val token = validate!!.returnStringValue(
            AESEncryption.decrypt(
                validate!!.RetriveSharepreferenceString(AppSP.token),
                validate!!.RetriveSharepreferenceString(AppSP.userid)
            )
        )

        val call1 = apiInterface?.getBankList(
            token,
            user, ifscCode
        )

        call1?.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(callCount: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
                Toast.makeText(this@VoCutOffVoInvestment, t.toString(), Toast.LENGTH_LONG).show()
            }

            override fun onResponse(
                call: Call<ResponseBody>,
                response: Response<ResponseBody>
            ) {
                //       Log.e("", response!!.body().toString())
                progressDialog.dismiss()
                if (response.isSuccessful) {
                    var alldata = response.body()!!.source().readUtf8().toString()
                    val objectMapper = ObjectMapper()

                    val langList: List<Bank_branchEntity> =
                        objectMapper.readValue(
                            alldata,
                            object : TypeReference<List<Bank_branchEntity>>() {})
                    if (langList != null) {
                        if (langList.size == 0) {
                            validate!!.CustomAlertVO(
                                LabelSet.getText(
                                    "branchnotavialable",
                                    R.string.branchnotavialable
                                ), this@VoCutOffVoInvestment
                            )
                        } else {
                            CDFIApplication.database?.masterbankbranchDao()
                                ?.insertbranch(
                                    langList
                                )
                            validate!!.CustomAlertVO(
                                LabelSet.getText(
                                    "branchdownlodedsuccessfully",
                                    R.string.branchdownlodedsuccessfully
                                ), this@VoCutOffVoInvestment
                            )

                        }
                    }



                    dataspin_branch =
                        masterbankBranchViewmodel!!.BankBranchlistifsc(
                            et_ifsc_code.text.toString().toUpperCase()
                        )
                    if (!dataspin_branch.isNullOrEmpty()) {
                        branch_id = dataspin_branch!!.get(0).bank_branch_id
                        spin_bankName.setSelection(
                            validate!!.returnMasterBankpos(
                                validate!!.returnIntegerValue(dataspin_branch!!.get(0).bank_id.toString()),
                                dataspin_bank
                            )
                        )
                    }


                } else {

                    when (response.code()) {

                        403 ->
                            CustomAlertlogin()
                        else -> {
                            validate!!.CustomAlertVO(
                                LabelSet.getText(
                                    "branchnotavialable",
                                    R.string.branchnotavialable
                                ), this@VoCutOffVoInvestment
                            )
                            Toast.makeText(
                                this@VoCutOffVoInvestment,
                                response.message(),
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }

                }
            }
        })


    }

    fun importtoken(user: String, pass: String) {
        var grant_type = "password"
        var userId = user
        var password = pass
        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText(
                "authenticate_user",
                R.string.authenticate_user
            )
        )


        val callCount = apiInterface?.gettokenforce(
            grant_type,
            userId,
            password, validate!!.RetriveSharepreferenceInt(AppSP.State_Selected), 1,
            validate!!.RetriveSharepreferenceString(AppSP.Roleid)!!
        )
        callCount?.enqueue(object : Callback<JsonObject> {
            override fun onFailure(callCount: Call<JsonObject>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                //       Log.e("", response!!.body().toString())
                progressDialog.dismiss()


                if (response.isSuccessful) {

                    var jsonObject1: JSONObject? = null
                    try {
                        jsonObject1 = JSONObject(response.body().toString())

                        val auth = jsonObject1.get("accessToken").toString()
                        if (auth.trim().length > 0) {
                            validate!!.SaveSharepreferenceString(
                                AppSP.token,
                                validate!!.returnStringValue(
                                    AESEncryption.encrypt(
                                        auth, validate!!.RetriveSharepreferenceString(AppSP.userid)
                                    )
                                )
                            )
                        }
                        validate!!.CustomAlertVO(
                            LabelSet.getText(
                                "userauthenticatedsuccessfully",
                                R.string.userauthenticatedsuccessfully
                            ), this@VoCutOffVoInvestment
                        )
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Toast.makeText(
                            this@VoCutOffVoInvestment,
                            response.message(),
                            Toast.LENGTH_LONG
                        ).show()


                    }

                } else {
                    Toast.makeText(this@VoCutOffVoInvestment, response.message(), Toast.LENGTH_LONG)
                        .show()
                }


            }

        })


    }

    fun CustomAlertlogin() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogelogin, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mDialogView.etUsername.setText(validate!!.RetriveSharepreferenceString(AppSP.userid))
        mDialogView.etPassword.setText(
            validate!!.returnStringValue(
                AESEncryption.decrypt(
                    validate!!.RetriveSharepreferenceString(AppSP.Password),
                    validate!!.RetriveSharepreferenceString(AppSP.userid)
                )
            )
        )
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.btn_login.setBackgroundColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.txt_dialog_title2.text = LabelSet.getText("app_name", R.string.app_name)
        mDialogView.btn_login.text = LabelSet.getText("log_in", R.string.log_in)
        mDialogView.chk_showHide1.text = LabelSet.getText("show_password", R.string.show_password)
        mDialogView.btn_login.setOnClickListener {
            mAlertDialog.dismiss()
            importtoken(
                mDialogView.etUsername.text.toString(),
                validate!!.returnStringValue(
                    AESEncryption.encrypt(
                        validate!!.returnStringValue(
                            mDialogView.etPassword.text.toString()
                        ), mDialogView.etUsername.text.toString()
                    )
                )
            )
        }
        mAlertDialog.chk_showHide1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                mAlertDialog.etPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                mAlertDialog.etPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
        }


    }

    private fun isNetworkConnected(): Boolean {
        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager //1
        val networkInfo = connectivityManager.activeNetworkInfo //2
        return networkInfo != null && networkInfo.isConnected //3
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val i = Intent(this, VoCutOffVoInvestmentList::class.java)
        i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(i)
        finish()
    }

}