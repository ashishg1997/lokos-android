package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.VoCutOffVoInvestmentAdapter
import com.microware.cdfi.entity.voentity.VoFinTxnDetGrpEntity
import com.microware.cdfi.fragment.VoCutOffTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoFinTxnDetGrpViewModel
import kotlinx.android.synthetic.main.activity_vo_cut_off_vo_investment_list.*

class VoCutOffVoInvestmentList : AppCompatActivity() {

    var validate: Validate? = null
    var vofintxnDetGrpViewModel: VoFinTxnDetGrpViewModel? = null
    var lookupViewmodel: LookupViewmodel? = null
    var Todayvalue = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vo_cut_off_vo_investment_list)

        vofintxnDetGrpViewModel = ViewModelProviders.of(this).get(VoFinTxnDetGrpViewModel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        validate = Validate(this)

        ivAdd.setOnClickListener {
            validate!!.SaveSharepreferenceInt(VoSpData.voAuid, 0)
            val intent = Intent(this, VoCutOffVoInvestment::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        replaceFragmenty(
            fragment = VoCutOffTopBarFragment(12),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        setLabel()
        fillRecyclerView()
    }

    private fun setLabel() {
        tv_investment.text = LabelSet.getText("investment", R.string.investment)
        tv_amount.text = LabelSet.getText("amount", R.string.amount)
        tv_place.text = LabelSet.getText("place", R.string.place)
        tv_date.text = LabelSet.getText("date", R.string.date)
    }

    private fun fillRecyclerView() {
        Todayvalue = 0
        vofintxnDetGrpViewModel!!.getInvestmentListByMtgNum(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf(59,60,61),"PO").observe(this, object :
            Observer<List<VoFinTxnDetGrpEntity>?> {
            override fun onChanged(list: List<VoFinTxnDetGrpEntity>?) {
                if (!list.isNullOrEmpty()) {
                    rvList.layoutManager = LinearLayoutManager(this@VoCutOffVoInvestmentList)
                    val voCutOffVoInvestmentAdapter = VoCutOffVoInvestmentAdapter(
                        this@VoCutOffVoInvestmentList,
                        list
                    )
                    val isize: Int
                    isize = list.size
                    val params: ViewGroup.LayoutParams = rvList.layoutParams
                    val r = resources
                    val px = TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_PX,
                        resources.getDimension(R.dimen.spraying),
                        r.displayMetrics
                    )
                    val hi = Math.round(px)
                    val gridHeight = hi * isize
                    params.height = gridHeight
                    rvList.layoutParams = params
                    rvList.adapter = voCutOffVoInvestmentAdapter

                }

            }
        })
    }

    fun getValue(keyCode: Int?,flag:Int): String? {
        var name: String? = null
        name = lookupViewmodel!!.getDestination(
            flag,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),
            keyCode
        )
        return name
    }

    fun returnSubHeadDescription(id: Int?): String? {
        var data = vofintxnDetGrpViewModel!!.getCoaSubHeadData(listOf(59,60,61),"PO",validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!)

        var value = ""
        if (!data.isNullOrEmpty()) {
            if (id!! > 0) {
                for (i in data.indices) {
                    if (id == data.get(i).uid)
                        value = validate!!.returnStringValue(data.get(i).description)
                }
            }
        }
        return value
    }

    fun getTotalValue1(iValue: Int) {
        Todayvalue = Todayvalue + iValue
        tv_TotalTodayValue.text = Todayvalue.toString()
    }
    override fun onBackPressed() {
        super.onBackPressed()
        val i = Intent(this, VoCutOffMenuActivity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(i)
        finish()
    }
}