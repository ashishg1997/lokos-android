package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.entity.voentity.VoMemLoanEntity
import com.microware.cdfi.entity.voentity.VoMemLoanScheduleEntity
import com.microware.cdfi.entity.voentity.VoMemLoanTxnEntity
import com.microware.cdfi.fragment.VoCutOffTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.voviewmodel.*
import kotlinx.android.synthetic.main.activity_cut_off_vo_loan_bank.*
import kotlinx.android.synthetic.main.activity_cut_off_vo_loan_bank.et_installment_amount
import kotlinx.android.synthetic.main.activity_cut_off_vo_loan_bank.et_interest_demand
import kotlinx.android.synthetic.main.activity_cut_off_vo_loan_bank.et_interest_paid
import kotlinx.android.synthetic.main.activity_cut_off_vo_loan_bank.et_loan_disbursmentDate
import kotlinx.android.synthetic.main.activity_cut_off_vo_loan_bank.et_no_of_installment
import kotlinx.android.synthetic.main.activity_cut_off_vo_loan_bank.et_outstanding_principal
import kotlinx.android.synthetic.main.activity_cut_off_vo_loan_bank.et_principal_overdue
import kotlinx.android.synthetic.main.activity_cut_off_vo_loan_bank.et_principal_repaid
import kotlinx.android.synthetic.main.activity_cut_off_vo_loan_bank.spin_repayment_frequency
import kotlinx.android.synthetic.main.activity_cut_off_vo_loan_bank.tv_installment_amount_principal
import kotlinx.android.synthetic.main.activity_cut_off_vo_loan_bank.tv_interest_demand
import kotlinx.android.synthetic.main.activity_cut_off_vo_loan_bank.tv_interest_paid
import kotlinx.android.synthetic.main.activity_cut_off_vo_loan_bank.tv_outstanding_principal
import kotlinx.android.synthetic.main.activity_cut_off_vo_loan_bank.tv_principal_overdue
import kotlinx.android.synthetic.main.activity_cut_off_vo_loan_bank.tv_principal_repaid
import kotlinx.android.synthetic.main.activity_cut_off_vo_loan_bank.tv_repayment_frequency
import kotlinx.android.synthetic.main.buttons_vo.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*

class VoCutOffVoLoanBank : AppCompatActivity() {
    var validate: Validate? = null
    var voMemLoanEntity: VoMemLoanEntity? = null
    var voMemLoanTxnEntity: VoMemLoanTxnEntity? = null
    lateinit var voMemLoanViewModel: VoMemLoanViewModel
    var dataspin_repayment_freq:List<LookupEntity>? = null
    lateinit var lookupViewmodel: LookupViewmodel
    var dataspin_loan_status:List<LookupEntity>? = null
    var dataspin_loan_type:List<LookupEntity>? = null
    lateinit var voMemLoanTxnViewModel: VoMemLoanTxnViewModel
    var voMemLoanScheduleEntity: VoMemLoanScheduleEntity? = null
    lateinit var voMemLoanScheduleViewModel: VoMemLoanScheduleViewModel
    lateinit var voMtgDetViewModel: VoMtgDetViewModel
    var voBankList: List<Cbo_bankEntity>? = null
    lateinit var voGenerateMeetingViewmodel: VoGenerateMeetingViewmodel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cut_off_vo_loan_bank)
        setLabelText()
        validate = Validate(this)
        voMemLoanViewModel = ViewModelProviders.of(this).get(VoMemLoanViewModel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        voMemLoanTxnViewModel = ViewModelProviders.of(this).get(VoMemLoanTxnViewModel::class.java)
        voMemLoanScheduleViewModel = ViewModelProviders.of(this).get(VoMemLoanScheduleViewModel::class.java)
        voMtgDetViewModel = ViewModelProviders.of(this).get(VoMtgDetViewModel::class.java)
        voGenerateMeetingViewmodel = ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)

        replaceFragmenty(
            fragment = VoCutOffTopBarFragment(18),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        et_no_of_installment.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = validate!!.returnIntegerValue(et_outstanding_principal.text.toString()) - validate!!.returnIntegerValue(et_principal_overdue.text.toString())
                var installment_num = validate!!.returnIntegerValue(et_no_of_installment.text.toString())
                if (installment_num > 0 && value > 0) {
                    val installmentamt = value / installment_num
                    et_installment_amount.setText(installmentamt.toString())
                }
            }

        })

        et_no_of_installment.filters = arrayOf(InputFilterMinMax (1, 60))

        et_outstanding_principal.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = validate!!.returnIntegerValue(et_outstanding_principal.text.toString()) - validate!!.returnIntegerValue(et_principal_overdue.text.toString())
                var installment_num = validate!!.returnIntegerValue(et_no_of_installment.text.toString())
                if (installment_num > 0 && value > 0) {
                    val installmentamt = value / installment_num
                    et_installment_amount.setText(installmentamt.toString())
                }

            }

        })


        et_loan_disbursmentDate.setOnClickListener{
            validate!!.datePicker(et_loan_disbursmentDate)
        }

        btn_cancel.setOnClickListener {
            val i = Intent(this, VoCutOffVoLoanBankList::class.java)
            startActivity(i)
        }

        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                saveData()
                var loanAmount =
                    validate!!.returnIntegerValue(et_outstanding_principal.text.toString()) - validate!!.returnIntegerValue(
                        et_principal_overdue.text.toString()
                    )
                insertScheduler(
                    loanAmount,
                    validate!!.returnIntegerValue(et_no_of_installment.text.toString())
                )
            }
        }
        fillSpinner()
        fillbank()
        fillBankLoandata()
       }

    override fun onBackPressed() {
        super.onBackPressed()
        val i = Intent(this, VoCutOffMenuActivity::class.java)
        startActivity(i)
    }


    fun setLabelText() {
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText("cancel", R.string.cancel)
        tv_name_of_bank_and_branch.text = LabelSet.getText(
            "name_of_bank_and_branch",
            R.string.name_of_bank_and_branch
        )
        tv_status_of_loan.text = LabelSet.getText("status_of_loan", R.string.status_of_loan)
        tv_loan_reference_no.text = LabelSet.getText(
            "loan_reference_no",
            R.string.loan_reference_no
        )
        tv_loan_disbursement_date.text = LabelSet.getText(
            "loan_disbursement_date",
            R.string.loan_disbursement_date
        )
        tv_type_of_loan.text = LabelSet.getText("type_of_loan", R.string.type_of_loan)
        tv_sanction_amount_ccl.text = LabelSet.getText(
            "sanction_amount_ccl",
            R.string.sanction_amount_ccl
        )
        tv_interest_rate.text = LabelSet.getText("interest_rate", R.string.interest_rate)
        tv_amount_received_withdrawn.text = LabelSet.getText(
            "amount_received_withdrawn",
            R.string.amount_received_withdrawn
        )
        tv_principal_repaid.text = LabelSet.getText("principal_repaid", R.string.principal_repaid)
        tv_outstanding_principal.text = LabelSet.getText(
            "outstanding_principal",
            R.string.outstanding_principal
        )
        tv_principal_overdue.text = LabelSet.getText(
            "principal_overdue",
            R.string.principal_overdue
        )
        tv_interest_demand.text = LabelSet.getText("interest_demand", R.string.interest_demand)
        tv_interest_paid.text = LabelSet.getText("interest_paid", R.string.interest_paid)
        tv_total_amount_overdue.text = LabelSet.getText("total_amount_overdue", R.string.total_amount_overdue)
        tv_installment_amount_principal.text = LabelSet.getText(
            "installment_amount_principal",
            R.string.installment_amount_principal
        )
        tv_repayment_frequency.text = LabelSet.getText(
            "repayment_frequency",
            R.string.repayment_frequency
        )
        et_loan_reference_no.hint = LabelSet.getText("type_here", R.string.type_here)
        et_loan_disbursmentDate.hint = LabelSet.getText("date_format", R.string.date_format)
        et_sanction_amount_ccl.hint = LabelSet.getText("type_here", R.string.type_here)
        et_interest_rate.hint = LabelSet.getText("type_here", R.string.type_here)
        et_amount_received_withdrawn.hint = LabelSet.getText("type_here", R.string.type_here)
        et_principal_repaid.hint = LabelSet.getText("type_here", R.string.type_here)
        et_outstanding_principal.hint = LabelSet.getText("type here", R.string.type_here)
        et_principal_overdue.hint = LabelSet.getText("type_here", R.string.type_here)
        et_interest_demand.hint = LabelSet.getText("type_here", R.string.type_here)
        et_interest_paid.hint = LabelSet.getText("type_here", R.string.type_here)
        et_total_amount_overdue.hint = LabelSet.getText("auto", R.string.auto)
        et_installment_amount.hint = LabelSet.getText("auto", R.string.auto)
    }

    fun saveData() {
        var loan_application_id: Long? = null
       // var memId =returnmemid()

        //field needed for sanction amount
        //memId
        //interestDemand
        //no interest overdue available in design
        //Amount Received/Amount Withdrawn
        voMemLoanEntity = VoMemLoanEntity(
            0,  //uid
            0,  //voMtgDetUid
            loan_application_id,  //loanApplicationId
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            0,  //memId
            validate!!.returnIntegerValue(et_loan_reference_no.text.toString()),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            0,  //installmentDate
            0,  //originalLoanAmount
            validate!!.returnIntegerValue(et_outstanding_principal.text.toString()),  //amount
            0, //loanpurpose
            0,  //fundtype
            validate!!.returnDoubleValue(et_interest_rate.text.toString()), //interestRate
            validate!!.returnIntegerValue(et_no_of_installment.text.toString()), //period
            validate!!.returnIntegerValue(et_principal_overdue.text.toString()), //principleOverdue
            0,  //interestOverdue
            true,  //completionflag
            validate!!.returnlookupcode(spin_type_of_loan, dataspin_loan_type), //loanType
            0,  //loanSource
            0,  //ModePayments
            returnaccount(), //bankCode
            "", //transanctionNo
            validate!!.returnlookupcode(spin_repayment_frequency, dataspin_repayment_freq), //installmentFreq
            0,  //moratoriumPeriod
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            "",
            0,
            "",
            0,
            validate!!.returnIntegerValue(et_principal_repaid.text.toString()), //principleRepaid
            validate!!.returnIntegerValue(et_interest_paid.text.toString()), //interestRepaid
            validate!!.Daybetweentime(et_loan_disbursmentDate.text.toString()) //disbrusmentdate
        )
        voMemLoanViewModel.insertVoMemLoan(voMemLoanEntity!!)

        voMemLoanTxnEntity = VoMemLoanTxnEntity(
            0,  //uid
            0,  //voMtgDetUid
            0,  //vomemloanuid
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            0,  //memId
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            0,
            0,
            validate!!.RetriveSharepreferenceInt(et_principal_repaid.text.toString()),
            validate!!.RetriveSharepreferenceInt(et_interest_paid.text.toString()),
            validate!!.returnIntegerValue(et_outstanding_principal.text.toString()),
            validate!!.RetriveSharepreferenceInt(et_interest_paid.text.toString()),
            true, //completionFlag
            0, //intAccruedOp
            0, //intAccrued
            0, //intAccruedCl
            0, //principalDemandOb
            validate!!.returnIntegerValue(et_principal_overdue.text.toString()),
            validate!!.returnIntegerValue(et_principal_overdue.text.toString()),
            0, //modePayment
            returnaccount(),  //bankCode
            "",  //transanctionNo
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            "",
            0,
            "",
            0,validate!!.returnIntegerValue(et_loan_reference_no.text.toString()),0.0,0
        )
        voMemLoanTxnViewModel.insert(voMemLoanTxnEntity!!)
    }

    private fun insertScheduler(amt: Int, installment: Int){
        var principaldemand = amt/installment
        var loanos = 0
        var saveValue = 0

     //   var memId = returnmemid()

        for (i in 0 until installment) {
            if (i == installment - 1) {
                loanos = 0
                principaldemand = amt - (principaldemand * i)
            } else {
                loanos = amt - (principaldemand * (i + 1))
            }
            var installMentDate = validate!!.addmonth(
                validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                (0 + i + 1),0
            )
            voMemLoanScheduleEntity = VoMemLoanScheduleEntity(
                0,
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                0, // memId
                validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
                validate!!.returnIntegerValue(et_loan_reference_no.text.toString()),
                principaldemand,
                principaldemand,
                loanos,
                0,
                i+1,
                0,
                installMentDate,
                true,
                0,
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                "", 0,
                "", 0
            )
            voMemLoanScheduleViewModel.insertVoMemLoanSchedule(voMemLoanScheduleEntity!!)
        }

        var principalDemand =
            voMemLoanScheduleViewModel.getPrincipalDemandByInstallmentNum(
                0,// memId,
                validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
                validate!!.returnIntegerValue(et_loan_reference_no.text.toString()),
                1
            )
        var totalprincipalDemand = principalDemand + validate!!.returnIntegerValue(et_principal_overdue.text.toString())

        voMemLoanScheduleViewModel.updateCutOffLoanMemberSchedule(
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            0,//memId,
            validate!!.returnIntegerValue(et_loan_reference_no.text.toString()),
            1,
            totalprincipalDemand
        )

        CustomAlertchangeschedule(
            LabelSet.getText(
                "doyouwanttochaneschedule",
                R.string.doyouwanttochaneschedule
            )
        )
    }

    fun CustomAlertchangeschedule(str: String) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str
        mDialogView.btn_yes.background = resources.getDrawable(R.drawable.button_bg_vomeeting)
        mDialogView.btn_no.background = resources.getDrawable(R.drawable.button_bg_vomeeting)
        mDialogView.btn_yes.setOnClickListener {
            //            PartialSaveData()
            mAlertDialog.dismiss()

            validate!!.SaveSharepreferenceInt(
                VoSpData.voLoanno,
                validate!!.returnIntegerValue(et_loan_reference_no.text.toString()))

            val intent = Intent(this, VoCutOffPrincipalDemandActivity::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)
        }

        mDialogView.btn_no.setOnClickListener {
            mAlertDialog.dismiss()

            val intent = Intent(this, VoCutOffVoLoanBankList::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)

        }
    }

    private fun fillBankLoandata() {

        val loanlist =
            voMemLoanViewModel.getLoanDetailData(validate!!.RetriveSharepreferenceInt(VoSpData.voLoanno),validate!!.RetriveSharepreferenceLong(
                VoSpData.voshgid),validate!!.RetriveSharepreferenceLong(VoSpData.voMemberId),validate!!.RetriveSharepreferenceString(
                VoSpData.vomtg_guid)!!)

        if (!loanlist.isNullOrEmpty()) {
            et_loan_reference_no.setText(loanlist.get(0).loanNo.toString())
            et_interest_rate.setText(validate!!.returnStringValue(loanlist.get(0).interestRate.toString()))
            et_no_of_installment.setText(validate!!.returnStringValue(loanlist.get(0).period.toString()))
            et_outstanding_principal.setText(validate!!.returnStringValue(loanlist.get(0).amount.toString()))
            et_principal_repaid.setText(validate!!.returnStringValue(loanlist.get(0).principalRepaid.toString()))
            et_principal_overdue.setText(validate!!.returnStringValue(loanlist.get(0).principalOverdue.toString()))
            et_interest_paid.setText(validate!!.returnStringValue(loanlist.get(0).interestRepaid.toString()))
            et_loan_disbursmentDate.setText(validate!!.convertDatetime(validate!!.returnLongValue(loanlist.get(0).disbursementDate.toString())))

            if(validate!!.returnIntegerValue(loanlist.get(0).period.toString())>0) {
                var installment_Amount =
                    (validate!!.returnIntegerValue(loanlist.get(0).amount.toString())-validate!!.returnIntegerValue(
                        loanlist.get(0).principalOverdue.toString())) / validate!!.returnIntegerValue(
                        loanlist.get(0).period.toString()
                    )
                et_installment_amount.setText(validate!!.returnStringValue(installment_Amount.toString()))
            }

            spin_repayment_frequency.setSelection(validate!!.returnlookupcodepos(loanlist.get(0).installmentFreq,dataspin_repayment_freq))

            spin_type_of_loan.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(
                        loanlist.get(0).loanType.toString()
                    ), dataspin_loan_type))

            spin_status_of_loan.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(
                        loanlist.get(0).completionFlag.toString()
                    ), dataspin_loan_status))

            setaccount(validate!!.returnStringValue(loanlist.get(0).bankCode))

            btn_savegray.visibility = View.VISIBLE
            btn_save.visibility = View.GONE

        } else {
            spin_repayment_frequency.setSelection(validate!!.returnlookupcodepos(3,dataspin_repayment_freq))

            var loanno = voMemLoanViewModel.getCutOffmaxLoanno(validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber))
            et_loan_reference_no.setText((loanno + 1).toString())

            btn_savegray.visibility = View.GONE
            btn_save.visibility = View.VISIBLE
        }

    }

        private fun fillSpinner() {

            dataspin_repayment_freq = lookupViewmodel.getlookup(
                19,
                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
            )

            dataspin_loan_status = lookupViewmodel.getlookup(
                5,
                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
            )

            dataspin_loan_type = lookupViewmodel.getlookup(
                88,
                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
            )

            validate!!.fillspinner(this, spin_repayment_frequency, dataspin_repayment_freq)
            validate!!.fillspinner(this, spin_status_of_loan, dataspin_loan_status)
            validate!!.fillspinner(this, spin_type_of_loan, dataspin_loan_type)

        }


    private fun checkValidation(): Int {

       /* var totalPrincipal = validate!!.returnIntegerValue(et_outstanding_principal.text.toString()) +
                validate!!.returnIntegerValue(et_principal_repaid.text.toString()) +
                validate!!.returnIntegerValue(et_principal_overdue.text.toString())*/

        var value = 1

        if (spin_name_of_bank_and_branch.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_name_of_bank_and_branch,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "name_of_bank_and_branch",
                    R.string.name_of_bank_and_branch
                )
            )
            value = 0
            return value
        }
        if(spin_status_of_loan.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_status_of_loan,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "status_of_loan",
                    R.string.status_of_loan
                ))
            value = 0
            return value
        }
        if (validate!!.returnIntegerValue(et_loan_reference_no.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "loan_reference_no",
                    R.string.loan_reference_no
                ), this,
                et_loan_reference_no
            )
            value = 0
            return value
        }
        if (validate!!.returnStringValue(et_loan_disbursmentDate.text.toString()).length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "loan_disbursement_date",
                    R.string.loan_disbursement_date
                ), this,
                et_loan_disbursmentDate
            )
            value = 0
            return value
        }
        if (spin_type_of_loan.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_type_of_loan,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "type_of_loan",
                    R.string.type_of_loan
                )

            )
            value = 0
            return value
        }
        if (validate!!.returnIntegerValue(et_sanction_amount_ccl.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "sanction_amount",
                    R.string.sanction_amount
                ), this,
                et_sanction_amount_ccl
            )
            value = 0
            return value
        }
        if (validate!!.returnDoubleValue(et_interest_rate.text.toString()) == 0.0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "existing_interest_rate",
                    R.string.interest_rate
                ), this,
                et_interest_rate
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_amount_received_withdrawn.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "amount_received_withdrawn",
                    R.string.amount_received_withdrawn
                ), this,
                et_amount_received_withdrawn
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_no_of_installment.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "pleaseenterintallment",
                    R.string.pleaseenterintallment
                ), this,
                et_no_of_installment
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_principal_repaid.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "principal_repaid",
                    R.string.principal_repaid
                ), this,
                et_principal_repaid
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_outstanding_principal.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "outstanding_principal",
                    R.string.outstanding_principal
                ), this,
                et_outstanding_principal
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_principal_overdue.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "principal_overdue",
                    R.string.principal_overdue
                ), this,
                et_principal_overdue
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_interest_demand.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "interest_demand",
                    R.string.interest_demand
                ), this,
                et_interest_demand
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_interest_paid.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "interest_paid",
                    R.string.interest_paid
                ), this,
                et_interest_paid
            )
            value = 0
            return value
        }

         if (validate!!.returnIntegerValue(et_installment_amount.text.toString()) == 0) {
             validate!!.CustomAlertEditText(
                 LabelSet.getText(
                     "please_enter",
                     R.string.please_enter
                 ) + " " + LabelSet.getText(
                     "installment_amount",
                     R.string.installment_amount
                 ), this,
                 et_installment_amount
             )
             value = 0
             return value
         }

        if (spin_repayment_frequency.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_repayment_frequency,
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "repayment_frequency",
                    R.string.repayment_frequency
                )
            )
            value = 0
            return value
        }


        return value
    }

   /* private fun returnmemid(): Long {
     var memId = voMtgDetViewModel!!.getMemId()
        return memId
    }*/

    fun fillbank() {
        voBankList =
            voGenerateMeetingViewmodel.getBankdata(
                validate!!.RetriveSharepreferenceString(
                    VoSpData.voSHGGUID
                )
            )

        val adapter: ArrayAdapter<String?>
        if (!voBankList.isNullOrEmpty()) {
            val isize = voBankList!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in voBankList!!.indices) {
                var lastthree =
                    voBankList!![i].account_no.substring(voBankList!![i].account_no.length - 3)
                sValue[i + 1] =
                    voBankList!![i].ifsc_code.toString().dropLast(7) + "XXXXX" + lastthree
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_name_of_bank_and_branch.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_name_of_bank_and_branch.adapter = adapter
        }
    }

    fun returnaccount(): String {

        var pos = spin_name_of_bank_and_branch.selectedItemPosition
        var id = ""

        if (!voBankList.isNullOrEmpty()) {
            if (pos > 0) id =
                voBankList!!.get(pos - 1).ifsc_code!!.dropLast(7) + voBankList!!.get(pos - 1).account_no
        }
        return id
    }

    fun setaccount(accountno: String): Int {

        var pos = 0

        if (!voBankList.isNullOrEmpty()) {
            for (i in voBankList!!.indices) {
                if (accountno.equals(
                        voBankList!!.get(i).ifsc_code!!.dropLast(7) + voBankList!!.get(
                            i
                        ).account_no
                    )
                )
                    pos = i + 1
            }
        }
        spin_name_of_bank_and_branch.setSelection(pos)
        return pos
    }
}