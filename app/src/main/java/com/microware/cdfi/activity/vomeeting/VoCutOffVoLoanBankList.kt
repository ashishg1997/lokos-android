package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.VoCutOffVoLoanbankAdapter
import com.microware.cdfi.entity.voentity.VoMemLoanEntity
import com.microware.cdfi.entity.voentity.VoMtgDetEntity
import com.microware.cdfi.fragment.VoCutOffTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoGenerateMeetingViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoMemLoanScheduleViewModel
import com.microware.cdfi.viewModel.voviewmodel.VoMemLoanViewModel
import com.microware.cdfi.viewModel.voviewmodel.VoMtgDetViewModel
import kotlinx.android.synthetic.main.activity_vo_cut_off_vo_loan_bank_list.*

class VoCutOffVoLoanBankList : AppCompatActivity() {
    lateinit var voCutOffVoLoanbankAdapter : VoCutOffVoLoanbankAdapter
    var validate: Validate? = null
    var voMemLoanViewModel: VoMemLoanViewModel? = null
    var voMemLoanScheduleViewModel: VoMemLoanScheduleViewModel? = null
    var voGenerateMeetingViewmodel: VoGenerateMeetingViewmodel? = null
    var dataspin_shg_name:List<VoMtgDetEntity>? = null
    var vomtgDetViewmodel: VoMtgDetViewModel? = null
    var lookupViewmodel: LookupViewmodel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vo_cut_off_vo_loan_bank_list)

        validate = Validate(this)
        voMemLoanScheduleViewModel=
            ViewModelProviders.of(this).get(VoMemLoanScheduleViewModel::class.java)
        voMemLoanViewModel =
            ViewModelProviders.of(this).get(VoMemLoanViewModel::class.java)

        voGenerateMeetingViewmodel =
            ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)

        vomtgDetViewmodel = ViewModelProviders.of(this).get(VoMtgDetViewModel::class.java)

        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)

          setLabelText()

        replaceFragmenty(
            fragment = VoCutOffTopBarFragment(18),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        iv_Add.setOnClickListener {

            validate!!.SaveSharepreferenceLong(VoSpData.voMemberId, 0)
            validate!!.SaveSharepreferenceLong(VoSpData.voMemberCode, 0)
            validate!!.SaveSharepreferenceInt(VoSpData.voLoanno,0)

            var intent = Intent(this, VoCutOffVoLoanBank::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        fillRecyclerView()
    }

    override fun onBackPressed() {
        if(validate!!.RetriveSharepreferenceInt(VoSpData.voMeetingType)==0) {
            var intent = Intent(this, VoMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        }else {
            var intent = Intent(this, VoCutOffMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        }
    }

    fun setLabelText(){
        tv_loans.text = LabelSet.getText("Loans", R.string.Loans)
        total_outstanding_ammount.text = LabelSet.getText("total_outstanding_amount", R.string.total_outstanding_amount)
        tv_interest_rate.text = LabelSet.getText("interest_rate", R.string.interest_rate)
        tv_installment_amount.text = LabelSet.getText("installment_amount_principal", R.string.installment_amount_principal)
    }


    private fun fillRecyclerView() {

        val list =  voMemLoanViewModel!!.getLoanDetail(validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            (validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)))

        getTotal(list!!)

        rvList.layoutManager = LinearLayoutManager(this)

        voCutOffVoLoanbankAdapter =
            VoCutOffVoLoanbankAdapter(this, list)

        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = rvList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvList.layoutParams = params
        rvList.adapter = voCutOffVoLoanbankAdapter

    }

    fun getTotal(list:List<VoMemLoanEntity>) {
        var totalAmount = 0
        var totalAmount1 = 0

        if (!list.isNullOrEmpty()) {
            for (i in 0 until list.size){
                totalAmount = totalAmount + validate!!.returnIntegerValue(list.get(i).amount.toString())
                var installment_Amount =
                    (validate!!.returnIntegerValue(list.get(i).amount.toString())-validate!!.returnIntegerValue(list.get(i).principalOverdue.toString())) / validate!!.returnIntegerValue(
                        list.get(i).period.toString()
                    )
                totalAmount1 = totalAmount1 + validate!!.returnIntegerValue(installment_Amount.toString())
            }
            tv_sum_outstanding_Amount.text = totalAmount.toString()
            tv_sum_installment_amount.text = totalAmount1.toString()
        }
    }

    fun setaccount(accountno: String): String {

        var bankName = ""
        var voBankList =
            voGenerateMeetingViewmodel!!.getBankdata(validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID))

        if (!voBankList.isNullOrEmpty()) {
            for (i in voBankList.indices) {
                if (accountno.equals(
                        voBankList.get(i).ifsc_code!!.dropLast(7) + voBankList.get(i).account_no
                    )
                )
                    bankName =
                        voBankList.get(i).ifsc_code!!.dropLast(7) + voBankList.get(i).account_no
            }
        }
        return bankName
    }
}