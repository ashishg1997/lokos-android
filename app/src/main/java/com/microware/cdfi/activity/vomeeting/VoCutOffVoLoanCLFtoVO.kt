package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.api.meetingmodel.FundTypeModel
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.entity.voentity.*
import com.microware.cdfi.fragment.VoCutOffTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.LoanProductViewModel
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.voviewmodel.*
import kotlinx.android.synthetic.main.activity_cut_off_vo_loan.*
import kotlinx.android.synthetic.main.buttons_vo.*
import kotlinx.android.synthetic.main.buttons_vo.btn_cancel
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*

class VoCutOffVoLoanCLFtoVO : AppCompatActivity() {

    var validate: Validate? = null
    var voMemLoanEntity: VoMemLoanEntity? = null
    var voGrpLoanEntity: VoGroupLoanEntity? = null
    var dataspin_loan_purpose: List<LookupEntity>? = null
    var dataspin_repayment_freq: List<LookupEntity>? = null
    var dataspin_loan_source: List<LookupEntity>? = null
    var dataspin_loan_status: List<LookupEntity>? = null
    var memberlist: List<VoMtgDetEntity>? = null
    var dataspin_fund: List<FundTypeModel>? = null
    lateinit var vogrpLoanViewModel: VoGroupLoanViewModel
    lateinit var lookupViewmodel: LookupViewmodel
    lateinit var voGrpLoanTxnViewModel: VoGroupLoanTxnViewModel
    lateinit var voGrpLoanScheduleViewModel: VoGroupLoanScheduleViewModel
    lateinit var vomtgDetViewmodel: VoMtgDetViewModel
    var vogrpLoanScheduleEntity: VoGroupLoanScheduleEntity? = null
    var voGrpLoanTxnEntity: VoGroupLoanTxnEntity? = null
    var loanProductViewmodel: LoanProductViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cut_off_vo_loan)
        validate = Validate(this)

        vogrpLoanViewModel = ViewModelProviders.of(this).get(VoGroupLoanViewModel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        voGrpLoanTxnViewModel = ViewModelProviders.of(this).get(VoGroupLoanTxnViewModel::class.java)
        voGrpLoanScheduleViewModel = ViewModelProviders.of(this).get(VoGroupLoanScheduleViewModel::class.java)
        vomtgDetViewmodel = ViewModelProviders.of(this).get(VoMtgDetViewModel::class.java)
        loanProductViewmodel = ViewModelProviders.of(this).get(LoanProductViewModel::class.java)

        replaceFragmenty(
            fragment = VoCutOffTopBarFragment(13),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        et_no_of_installment.filters = arrayOf(InputFilterMinMax(1, 60))

        et_loan_disbursmentDate.setText(validate!!.convertDatetime(
            validate!!.returnLongValue(validate!!.RetriveSharepreferenceLong(
                VoSpData.voCurrentMtgDate).toString())))

        et_loan_disbursmentDate.setOnClickListener {
            validate!!.datePicker(et_loan_disbursmentDate)
        }

        et_no_of_installment.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val value = validate!!.returnIntegerValue(et_outstanding_principal.text.toString()) - validate!!.returnIntegerValue(
                        et_principal_overdue.text.toString())
                val installment_num = validate!!.returnIntegerValue(et_no_of_installment.text.toString())
                if (installment_num > 0 && value > 0) {
                    val installmentamt = value / installment_num
                    et_installment_amount.setText(installmentamt.toString())
                }
            }

        })

        et_principal_overdue.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = validate!!.returnIntegerValue(et_outstanding_principal.text.toString()) - validate!!.returnIntegerValue(et_principal_overdue.text.toString())
                var installment_num = validate!!.returnIntegerValue(et_no_of_installment.text.toString())
                if (installment_num > 0 && value > 0) {
                    val installmentamt = value / installment_num
                    et_installment_amount.setText(installmentamt.toString())
                }
            }

        })

        et_outstanding_principal.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = validate!!.returnIntegerValue(et_outstanding_principal.text.toString()) - validate!!.returnIntegerValue(et_principal_overdue.text.toString())
                var installment_num = validate!!.returnIntegerValue(et_no_of_installment.text.toString())
                if (installment_num > 0 && value > 0) {
                    val installmentamt = value / installment_num
                    et_installment_amount.setText(installmentamt.toString())
                }

            }

        })

        et_amount_received.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var outstandingPrincipal =
                    validate!!.returnIntegerValue(et_amount_received.text.toString()) -
                            validate!!.returnIntegerValue(et_principal_repaid.text.toString())
                et_outstanding_principal.setText(validate!!.returnStringValue(outstandingPrincipal.toString()))

            }

        })

        et_principal_repaid.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var outstandingPrincipal =
                    validate!!.returnIntegerValue(et_amount_received.text.toString()) -
                            validate!!.returnIntegerValue(et_principal_repaid.text.toString())
                et_outstanding_principal.setText(validate!!.returnStringValue(outstandingPrincipal.toString()))
            }

        })

        btn_cancel.setOnClickListener {
            var intent = Intent(this, VoCutOffVoLoanCLFtoVOList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        }

        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                saveData(0)

                if (validate!!.returnlookupcode(spin_status_of_loan, dataspin_loan_status) == 1){
                    var loanAmount = validate!!.returnIntegerValue(et_outstanding_principal.text.toString()) - validate!!.returnIntegerValue(et_principal_overdue.text.toString())
                    insertScheduler(
                        loanAmount,
                        validate!!.returnIntegerValue(et_no_of_installment.text.toString()),0
                    )
                }else {
                    validate!!.CustomAlertVO(
                        LabelSet.getText(
                            "data_saved_successfully",
                            R.string.data_saved_successfully),
                        this, VoCutOffVoLoanCLFtoVOList::class.java
                    )
                }
            }
        }

        setLabelText()
        fillSpinner()
        showData()
    }

    private fun fillSpinner() {

        dataspin_loan_purpose = lookupViewmodel.getlookup(
            67,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_fund = loanProductViewmodel!!.getFundTypeBySource(
            4, 3
        )

        dataspin_repayment_freq = lookupViewmodel.getlookup(
            19,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_loan_source = lookupViewmodel.getlookup(
            70,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_loan_status = lookupViewmodel.getlookup(
            89,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        validate!!.fillspinner(this, spin_purpose, dataspin_loan_purpose)
        validate!!.fillFundType(this, spin_fund_type, dataspin_fund)
        validate!!.fillspinner(this, spin_repayment_frequency, dataspin_repayment_freq)
        validate!!.fillspinner(this, spin_source_of_loan, dataspin_loan_source)
        validate!!.fillspinner(this, spin_status_of_loan, dataspin_loan_status)
    }

    fun setLabelText() {
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText("cancel", R.string.cancel)
        tv_source_of_loan.text = LabelSet.getText("source_of_loan", R.string.source_of_loan)
        tv_status_of_loan.text = LabelSet.getText("status_of_loan", R.string.status_of_loan)
        tv_loan_no.text = LabelSet.getText("loan_no", R.string.loan_no)
        tv_loan_disbursement_date.text = LabelSet.getText(
            "loan_disbursement_date",
            R.string.loan_disbursement_date
        )
        tv_fund_type.text = LabelSet.getText("fund_type", R.string.fund_type)
        tv_sanction_amount.text = LabelSet.getText("sanction_amount", R.string.sanction_amount)
        tv_interest_rate.text = LabelSet.getText("interest_rate", R.string.interest_rate)
        tv_amount_received.text = LabelSet.getText("amount_received", R.string.amount_received)
        tv_principal_repaid.text = LabelSet.getText("principal_repaid", R.string.principal_repaid)
        tv_outstanding_principal.text = LabelSet.getText(
            "outstanding_principal",
            R.string.outstanding_principal
        )
        tv_principal_overdue.text = LabelSet.getText(
            "principal_overdue",
            R.string.principal_overdue
        )
        tv_interest_demand.text = LabelSet.getText("interest_demand", R.string.interest_demand)
        tv_interest_paid.text = LabelSet.getText("interest_paid", R.string.interest_paid)
        tv_interest_overdue.text = LabelSet.getText("interest_overdue", R.string.interest_overdue)
        tv_installment_amount_principal.text = LabelSet.getText(
            "installment_amount_principal",
            R.string.installment_amount_principal
        )
        tv_repayment_frequency.text = LabelSet.getText(
            "repayment_frequency",
            R.string.repayment_frequency
        )
        tv_no_of_installment.text = LabelSet.getText(
            "no_of_installment",
            R.string.no_of_installment
        )
        tv_purpose.text = LabelSet.getText("purpose", R.string.purpose)

        et_loan_no.hint = LabelSet.getText("type_here", R.string.type_here)
        et_loan_disbursmentDate.hint = LabelSet.getText("date_format", R.string.date_format)
        et_sanction_amount.hint = LabelSet.getText("type_here", R.string.type_here)
        et_interest_rate.hint = LabelSet.getText("type_here", R.string.type_here)
        et_amount_received.hint = LabelSet.getText("type_here", R.string.type_here)
        et_principal_repaid.hint = LabelSet.getText("type_here", R.string.type_here)
        et_outstanding_principal.hint = LabelSet.getText("type_here", R.string.type_here)
        et_principal_overdue.hint = LabelSet.getText("type_here", R.string.type_here)
        et_interest_demand.hint = LabelSet.getText("type_here", R.string.type_here)
        et_interest_paid.hint = LabelSet.getText("type_here", R.string.type_here)
        et_interest_overdue.hint = LabelSet.getText("type_here", R.string.type_here)
        et_installment_amount.hint = LabelSet.getText("type_here", R.string.type_here)
        et_no_of_installment.hint = LabelSet.getText("type_here", R.string.type_here)
    }

    private fun checkValidation(): Int {

        var totalPrincipal =
            validate!!.returnIntegerValue(et_outstanding_principal.text.toString()) +
                    validate!!.returnIntegerValue(et_principal_repaid.text.toString())
        var value = 1

        if (spin_source_of_loan.selectedItemPosition == 0 && lay_source_of_loan.visibility == View.VISIBLE) {
            validate!!.CustomAlertSpinner(
                this, spin_source_of_loan,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "source_of_loan",
                    R.string.source_of_loan
                )

            )
            value = 0
            return value
        }

        if (spin_status_of_loan.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_status_of_loan,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "status_of_loan",
                    R.string.status_of_loan
                )
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_loan_no.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "loan_no",
                    R.string.loan_no
                ), this,
                et_loan_no
            )
            value = 0
            return value
        }

        if (validate!!.returnStringValue(et_loan_disbursmentDate.text.toString()).length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "select_disbursement_date",
                    R.string.select_disbursement_date
                ), this,
                et_loan_disbursmentDate
            )
            value = 0
            return value
        }

        if (spin_fund_type.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_fund_type,
                LabelSet.getText(
                    "please_select_fund_type",
                    R.string.please_select_fund_type
                )
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_sanction_amount.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "sanction_amount",
                    R.string.sanction_amount
                ), this,
                et_sanction_amount
            )
            value = 0
            return value
        }

        if (validate!!.returnDoubleValue(et_interest_rate.text.toString()) == 0.0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "enter_interest_rate",
                    R.string.enter_interest_rate
                ), this,
                et_interest_rate
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_amount_received.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "amount_received",
                    R.string.amount_received
                ), this,
                et_amount_received
            )
            value = 0
            return value
        }

        if(spin_status_of_loan.selectedItemPosition==1 && validate!!.returnIntegerValue(et_outstanding_principal.text.toString())==0){
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "enter_outstanding_principal",
                    R.string.enter_outstanding_principal
                ), this,
                et_outstanding_principal
            )
            value = 0
            return value
        }

        if(spin_status_of_loan.selectedItemPosition==1 && validate!!.returnIntegerValue(et_amount_received.text.toString())!=totalPrincipal){
            validate!!.CustomAlert(
                LabelSet.getText(
                    "invalid_sum_principal",
                    R.string.invalid_sum_principal
                ), this)
            value = 0
            return value
        }

        if(spin_status_of_loan.selectedItemPosition==1 && validate!!.returnIntegerValue(et_amount_received.text.toString())<= validate!!.returnIntegerValue(et_principal_repaid.text.toString())){
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "compare_principal_repaid_amount_disbursed",
                    R.string.compare_principal_repaid_amount_disbursed
                ), this,
                et_principal_repaid
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_principal_repaid.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "principal_repaid",
                    R.string.principal_repaid
                ), this,
                et_principal_repaid
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_outstanding_principal.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "outstanding_principal",
                    R.string.outstanding_principal
                ), this,
                et_outstanding_principal
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_principal_overdue.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "principal_overdue",
                    R.string.principal_overdue
                ), this,
                et_principal_overdue
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_interest_demand.text.toString()) == 0 && lay_interest_demand.visibility== View.VISIBLE) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "interest_demand",
                    R.string.interest_demand
                ), this,
                et_interest_demand
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_interest_paid.text.toString()) == 0 && lay_interest_paid.visibility == View.VISIBLE) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "interest_paid",
                    R.string.interest_paid
                ), this,
                et_interest_paid
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_installment_amount.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "installment_amount_principal",
                    R.string.installment_amount_principal
                ), this,
                et_installment_amount
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_no_of_installment.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "pleaseenterintallment",
                    R.string.pleaseenterintallment
                ), this,
                et_no_of_installment
            )
            value = 0
            return value
        }

        if (spin_repayment_frequency.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_repayment_frequency,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "repayment_frequency",
                    R.string.repayment_frequency
                )

            )
            value = 0
            return value
        }

        if (spin_purpose.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_purpose,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "purpose",
                    R.string.purpose
                )
            )
            value = 0
            return value
        }
        return value
    }

    //status of loan,Sanction amount,interest demand, amount received and installment amount and memid?
    fun saveData(morotrum: Int) {
        val laonappid = validate!!.RetriveSharepreferenceLong(VoSpData.voshgid) + 1000 + validate!!.returnIntegerValue(et_loan_no.text.toString()) + 1

        var completionFlag:Boolean? = null
        if (validate!!.returnlookupcode(spin_status_of_loan, dataspin_loan_source) == 1){
            completionFlag = true
        }else if (validate!!.returnlookupcode(spin_status_of_loan, dataspin_loan_source) == 2){
            completionFlag = false
        }

        voGrpLoanEntity = VoGroupLoanEntity(
            0,
            laonappid,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.returnIntegerValue(et_loan_no.text.toString()),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.Daybetweentime(et_loan_disbursmentDate.text.toString()),
            validate!!.addmonth(validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                morotrum,validate!!.returnlookupcode(spin_repayment_frequency, dataspin_repayment_freq)),
            validate!!.returnIntegerValue(et_loan_no.text.toString()),
            validate!!.returnIntegerValue(et_outstanding_principal.text.toString()),
            validate!!.returnlookupcode(spin_purpose, dataspin_loan_purpose),
            validate!!.returnFundTypeId(spin_fund_type, dataspin_fund),
            validate!!.returnDoubleValue(et_interest_rate.text.toString()),
            validate!!.returnIntegerValue(et_no_of_installment.text.toString()),
            validate!!.returnIntegerValue(et_principal_overdue.text.toString()),
           validate!!.returnIntegerValue(et_amount_received.text.toString()),
            validate!!.returnIntegerValue(et_principal_repaid.text.toString()),
            validate!!.returnIntegerValue(et_interest_overdue.text.toString()),
            0,
            completionFlag,
            0,
            validate!!.returnlookupcode(spin_source_of_loan, dataspin_loan_source),
            0,
            1,
            "",
            "",
            validate!!.returnlookupcode(spin_repayment_frequency,dataspin_repayment_freq),
            morotrum,
            "",
            0,
            validate!!.returnlookupcode(spin_source_of_loan, dataspin_loan_source),
            "",
            validate!!.returnIntegerValue(et_sanction_amount.text.toString()),
            0,
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            "",
            0,
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            0,0,0,0,0,0.0,0,1
        )

        vogrpLoanViewModel.insertVoGroupLoan(voGrpLoanEntity!!)

        voGrpLoanTxnEntity = VoGroupLoanTxnEntity(
            0,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.returnIntegerValue(et_loan_no.text.toString()),
            validate!!.returnIntegerValue(et_amount_received.text.toString()),
            0,
            validate!!.returnIntegerValue(et_principal_repaid.text.toString()),
            0,
            validate!!.returnIntegerValue(et_outstanding_principal.text.toString()),
            0,
            completionFlag,
            0,
            validate!!.returnIntegerValue(et_interest_overdue.text.toString()),
            validate!!.returnIntegerValue(et_interest_overdue.text.toString()),
            0,
            validate!!.returnIntegerValue(et_principal_overdue.text.toString()),
            validate!!.returnIntegerValue(et_principal_overdue.text.toString()),
            1,
            "",
            "",
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            "",
            0,
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            0,0.0,0,0
        )
        voGrpLoanTxnViewModel.insertVoGroupLoanTxn(voGrpLoanTxnEntity!!)
    }

    private fun insertScheduler(amt: Int, installment: Int, morotrum: Int) {
        var principaldemand = amt / installment
        var loanos = 0

        for (i in 0 until installment) {
            if (i == installment - 1) {
                loanos = 0
                principaldemand = amt - (principaldemand * i)
            } else {
                loanos = amt - (principaldemand * (i + 1))
            }

            var installMentDate = validate!!.addmonth(
                validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                (morotrum + i + 1),validate!!.returnlookupcode(spin_repayment_frequency, dataspin_repayment_freq))

            vogrpLoanScheduleEntity = VoGroupLoanScheduleEntity(
                0,
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
                validate!!.returnIntegerValue(et_loan_no.text.toString()),
                principaldemand,
                principaldemand,
                loanos,
                0,
                i+1,
                0,
                installMentDate,
                0,
                true,
                0,
               null,
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                0,
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                0
            )

            voGrpLoanScheduleViewModel.insertVoGroupLoanSchedule(vogrpLoanScheduleEntity!!)
        }

        val principalDemand =
            voGrpLoanScheduleViewModel.getPrincipalDemandByInstallmentNum(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
                validate!!.returnIntegerValue(et_loan_no.text.toString()),
                1
            )
        val totalprincipalDemand =
            principalDemand + validate!!.returnIntegerValue(et_principal_overdue.text.toString())

        voGrpLoanScheduleViewModel.updateCutOffLoanSchedule(
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.returnIntegerValue(et_loan_no.text.toString()),
            1,
            totalprincipalDemand
        )

        CustomAlertchangeschedule(
            LabelSet.getText(
                "doyouwanttochaneschedule",
                R.string.doyouwanttochaneschedule
            )
        )
    }

    fun CustomAlertchangeschedule(str: String) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str
        mDialogView.btn_yes.background = resources.getDrawable(R.drawable.button_bg_vomeeting)
        mDialogView.btn_no.background = resources.getDrawable(R.drawable.button_bg_vomeeting)
        mDialogView.btn_yes.setOnClickListener {
            mAlertDialog.dismiss()
            /*validate!!.SaveSharepreferenceLong(VoSpData.voMemberId, returnmemid())*/
            validate!!.SaveSharepreferenceLong(VoSpData.voShgMemID, 0)
            validate!!.SaveSharepreferenceInt(
                VoSpData.voLoanno,
                validate!!.returnIntegerValue(et_loan_no.text.toString())
            )

            val intent = Intent(this, VoGroupPrincipalDemandActivity::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)
        }

        mDialogView.btn_no.setOnClickListener {
            mAlertDialog.dismiss()

            val intent = Intent(this, VoCutOffVoLoanCLFtoVOList::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)

        }
    }

    private fun showData() {

        var loanlist =
            vogrpLoanViewModel.getLoanDetailData(
                validate!!.RetriveSharepreferenceInt(VoSpData.voLoanno),
                validate!!.RetriveSharepreferenceLong(
                    VoSpData.voshgid
                ),
                validate!!.RetriveSharepreferenceString(
                    VoSpData.vomtg_guid
                )!!
            )
        if (!loanlist.isNullOrEmpty()) {
            et_loan_no.setText(loanlist.get(0).loanNo.toString())
            et_sanction_amount.setText(loanlist.get(0).sanctionedAmount.toString())
            et_no_of_installment.setText(validate!!.returnStringValue(loanlist.get(0).period.toString()))
            et_interest_rate.setText(validate!!.returnStringValue(loanlist.get(0).interestRate.toString()))
            et_amount_received.setText(validate!!.returnStringValue(loanlist.get(0).orignalLoanAmount.toString()))
            et_outstanding_principal.setText(validate!!.returnStringValue(loanlist.get(0).amount.toString()))
            et_principal_repaid.setText(validate!!.returnStringValue(loanlist.get(0).principalRepaid.toString()))
            et_principal_overdue.setText(validate!!.returnStringValue(loanlist.get(0).principalOverdue.toString()))
//            et_interest_paid.setText(validate!!.returnStringValue(loanlist.get(0).interestRepaid.toString()))
            et_interest_overdue.setText(validate!!.returnStringValue(loanlist.get(0).interestOverdue.toString()))
            et_loan_disbursmentDate.setText(
                validate!!.convertDatetime(
                    validate!!.returnLongValue(
                        loanlist.get(0).disbursementDate.toString()
                    )
                )
            )

            if (validate!!.returnIntegerValue(loanlist.get(0).period.toString()) > 0) {
                var installment_Amount =
                    (validate!!.returnIntegerValue(loanlist.get(0).amount.toString()) - validate!!.returnIntegerValue(
                        loanlist.get(0).principalOverdue.toString()
                    )) / validate!!.returnIntegerValue(
                        loanlist.get(0).period.toString()
                    )
                et_installment_amount.setText(validate!!.returnStringValue(installment_Amount.toString()))
            }
            /*if (validate!!.returnIntegerValue(loanlist!!.get(0).period.toString()) > 0) {
                var overdue_Amount =
                    validate!!.returnIntegerValue(loanlist!!.get(0).principalOverdue.toString()) + validate!!.returnIntegerValue(
                        loanlist!!.get(0).interestOverdue.toString()
                    )
                et_principal_overdue.setText(validate!!.returnStringValue(overdue_Amount.toString()))
            }*/
            spin_purpose.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(
                        loanlist.get(0).loanPurpose.toString()
                    ), dataspin_loan_purpose
                )
            )

            spin_source_of_loan.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(
                        loanlist.get(0).loanSource.toString()
                    ), dataspin_loan_source
                )
            )

            spin_repayment_frequency.setSelection(
                validate!!.returnlookupcodepos(
                    loanlist.get(0).installmentFreq,
                    dataspin_repayment_freq
                )
            )

            spin_fund_type.setSelection(
                validate!!.setFundType(
                    validate!!.returnIntegerValue(
                        loanlist.get(0).loanProductId.toString()
                    ), dataspin_fund
                )
            )
            var completionFlag = 0
            if (loanlist.get(0).completionFlag == true){
                completionFlag = 1
            }else if (loanlist.get(0).completionFlag == false){
                completionFlag = 2
            }

            spin_status_of_loan.setSelection(
                validate!!.returnlookupcodepos(
                    completionFlag, dataspin_loan_status
                )
            )

            btn_save.visibility = View.GONE
            btn_savegray.visibility = View.VISIBLE
        } else {
            spin_repayment_frequency.setSelection(
                validate!!.returnlookupcodepos(
                    3,
                    dataspin_repayment_freq
                )
            )

            var loanno = vogrpLoanViewModel.getCutOffmaxLoanno(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
            )
            et_loan_no.setText((loanno + 1).toString())

        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, VoCutOffVoLoanCLFtoVOList::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

}