package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.VoCutOffVoLoanClftoVoAdapter
import com.microware.cdfi.entity.voentity.VoGroupLoanEntity
import com.microware.cdfi.fragment.VoCutOffTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.FundTypeViewmodel
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.voviewmodel.*
import kotlinx.android.synthetic.main.activity_vo_cut_off_vo_loan_clf_to_vo.*
import kotlinx.android.synthetic.main.activity_vo_cut_off_vo_loan_clf_to_vo.ivAdd
import kotlinx.android.synthetic.main.activity_vo_cut_off_vo_loan_clf_to_vo.tv_interest_rate

class VoCutOffVoLoanCLFtoVOList : AppCompatActivity() {
    lateinit var voCutOffVoLoanClftoVoAdapter: VoCutOffVoLoanClftoVoAdapter
    var validate: Validate? = null
    var voGrpLoanViewModel: VoGroupLoanViewModel? = null
    var voGroupLoanScheduleViewModel: VoGroupLoanScheduleViewModel? = null
    var voGenerateMeetingViewmodel: VoGenerateMeetingViewmodel? = null
    var fundTypeViewModel: FundTypeViewmodel? = null
    var vomtgDetViewmodel: VoMtgDetViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vo_cut_off_vo_loan_clf_to_vo)

        validate = Validate(this)
        voGroupLoanScheduleViewModel = ViewModelProviders.of(this).get(VoGroupLoanScheduleViewModel::class.java)
        voGrpLoanViewModel = ViewModelProviders.of(this).get(VoGroupLoanViewModel::class.java)

        voGenerateMeetingViewmodel = ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)

        vomtgDetViewmodel = ViewModelProviders.of(this).get(VoMtgDetViewModel::class.java)
        fundTypeViewModel = ViewModelProviders.of(this).get(FundTypeViewmodel::class.java)

        setLabelText()

        replaceFragmenty(
            fragment = VoCutOffTopBarFragment(13),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        ivAdd.setOnClickListener {
            validate!!.SaveSharepreferenceInt(VoSpData.voLoanno, 0)
            validate!!.SaveSharepreferenceLong(VoSpData.voShgMemID, 0)
            var intent = Intent(this, VoCutOffVoLoanCLFtoVO::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        }

        fillRecyclerView()
    }

    fun setLabelText() {
        tv_loans.text = LabelSet.getText("Loans", R.string.Loans)
        total_outstanding_ammount.text = LabelSet.getText(
            "total_outstanding_amount",
            R.string.total_outstanding_amount
        )
        tv_interest_rate.text = LabelSet.getText("interest_rate", R.string.interest_rate)
        tv_installment_amount.text = LabelSet.getText(
            "installment_amount_principal",
            R.string.installment_amount_principal
        )
    }

    private fun fillRecyclerView() {
        val list = voGrpLoanViewModel!!.getLoanDetail(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
        )

        getTotal(list!!)

        rvList.layoutManager = LinearLayoutManager(this)

        voCutOffVoLoanClftoVoAdapter =
            VoCutOffVoLoanClftoVoAdapter(this, list)

        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = rvList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvList.layoutParams = params
        rvList.adapter = voCutOffVoLoanClftoVoAdapter

    }

    fun getTotal(list: List<VoGroupLoanEntity>) {
        var totalAmount = 0
        var totalAmount1 = 0

        if (!list.isNullOrEmpty()) {
            for (i in 0 until list.size) {
                totalAmount += validate!!.returnIntegerValue(list.get(i).amount.toString())
                var installment_Amount = 0
                if (validate!!.returnIntegerValue(list.get(i).period.toString()) > 0) {
                    installment_Amount =
                        (validate!!.returnIntegerValue(list.get(i).amount.toString()) - validate!!.returnIntegerValue(
                            list.get(i).principalOverdue.toString()
                        )) / validate!!.returnIntegerValue(
                            list.get(i).period.toString()
                        )
                }
                totalAmount1 += validate!!.returnIntegerValue(installment_Amount.toString())
            }
            tv_sum_outstanding_Amount.text = totalAmount.toString()
            tv_sum_installment_amount.text = totalAmount1.toString()
        }
    }

    fun getFundTypeName(id: Int?): String? {
        var value: String? = null

        value = fundTypeViewModel!!.getFundName(
            id!!,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
        )

        return value
    }

    override fun onBackPressed() {
        var intent = Intent(this, VoCutOffMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

}