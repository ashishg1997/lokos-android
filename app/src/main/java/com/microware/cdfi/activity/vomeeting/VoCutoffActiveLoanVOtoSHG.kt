package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.api.meetingmodel.FundTypeModel
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.entity.voentity.VoMemLoanEntity
import com.microware.cdfi.entity.voentity.VoMemLoanScheduleEntity
import com.microware.cdfi.entity.voentity.VoMemLoanTxnEntity
import com.microware.cdfi.entity.voentity.VoMtgDetEntity
import com.microware.cdfi.fragment.VoCutOffTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.LoanProductViewModel
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.voviewmodel.*
import kotlinx.android.synthetic.main.activity_vo_cutoff_active_loan_voto_shg.*
import kotlinx.android.synthetic.main.activity_vo_cutoff_active_loan_voto_shg.et_cheque_no_transactio_no
import kotlinx.android.synthetic.main.activity_vo_cutoff_active_loan_voto_shg.et_interest_paid
import kotlinx.android.synthetic.main.activity_vo_cutoff_active_loan_voto_shg.et_loan_no
import kotlinx.android.synthetic.main.activity_vo_cutoff_active_loan_voto_shg.et_no_of_installment
import kotlinx.android.synthetic.main.activity_vo_cutoff_active_loan_voto_shg.et_principal_overdue
import kotlinx.android.synthetic.main.activity_vo_cutoff_active_loan_voto_shg.et_principal_repaid
import kotlinx.android.synthetic.main.activity_vo_cutoff_active_loan_voto_shg.lay_bank
import kotlinx.android.synthetic.main.activity_vo_cutoff_active_loan_voto_shg.lay_chequeNO
import kotlinx.android.synthetic.main.activity_vo_cutoff_active_loan_voto_shg.spin_purpose
import kotlinx.android.synthetic.main.activity_vo_cutoff_active_loan_voto_shg.tv_interest_paid
import kotlinx.android.synthetic.main.activity_vo_cutoff_active_loan_voto_shg.tv_loan_no
import kotlinx.android.synthetic.main.activity_vo_cutoff_active_loan_voto_shg.tv_principal_overdue
import kotlinx.android.synthetic.main.buttons_vo.btn_cancel
import kotlinx.android.synthetic.main.buttons_vo.btn_save
import kotlinx.android.synthetic.main.buttons_vo.btn_savegray
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.layout_cutoff_loan_disbursement.*

class VoCutoffActiveLoanVOtoSHG : AppCompatActivity() {
    var validate: Validate? = null
    var voMemLoanEntity: VoMemLoanEntity? = null
    var dataspin_loan_purpose: List<LookupEntity>? = null
    var dataspin_repayment_freq: List<LookupEntity>? = null
    var dataspin_shg_name: List<VoMtgDetEntity>? = null
    lateinit var voMemLoanViewModel: VoMemLoanViewModel
    lateinit var lookupViewmodel: LookupViewmodel
    var voMemLoanTxnEntity: VoMemLoanTxnEntity? = null
    lateinit var voMemLoanTxnViewModel: VoMemLoanTxnViewModel
    var voMemLoanScheduleEntity: VoMemLoanScheduleEntity? = null
    var memberlist: List<VoMtgDetEntity>? = null
    lateinit var voMemLoanScheduleViewModel: VoMemLoanScheduleViewModel
    lateinit var vomtgDetViewmodel: VoMtgDetViewModel
    var dataspin_fund: List<FundTypeModel>? = null
    var loanProductViewmodel: LoanProductViewModel? = null
    var voGenerateMeetingViewmodel: VoGenerateMeetingViewmodel? = null
    var dataspin_modeofpayment: List<LookupEntity>? = null
    var voBankList: List<Cbo_bankEntity>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vo_cutoff_active_loan_voto_shg)
        voMemLoanViewModel = ViewModelProviders.of(this).get(VoMemLoanViewModel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        voMemLoanTxnViewModel = ViewModelProviders.of(this).get(VoMemLoanTxnViewModel::class.java)
        voMemLoanScheduleViewModel =
            ViewModelProviders.of(this).get(VoMemLoanScheduleViewModel::class.java)
        vomtgDetViewmodel = ViewModelProviders.of(this).get(VoMtgDetViewModel::class.java)
        loanProductViewmodel = ViewModelProviders.of(this).get(LoanProductViewModel::class.java)
        voGenerateMeetingViewmodel =
            ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)

        validate = Validate(this)
        setLabel()

        replaceFragmenty(
            fragment = VoCutOffTopBarFragment(8),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        et_no_of_installment.filters = arrayOf(InputFilterMinMax(1, 60))

        et_loan_disbursmentDate.setOnClickListener {
            validate!!.datePicker(et_loan_disbursmentDate)
        }

        et_no_of_installment.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value =
                    validate!!.returnIntegerValue(et_outstanding_principal.text.toString()) - validate!!.returnIntegerValue(
                        et_principal_overdue.text.toString()
                    )
                var installment_num =
                    validate!!.returnIntegerValue(et_no_of_installment.text.toString())
                if (installment_num > 0 && value > 0) {
                    val installmentamt = value / installment_num
                    et_installment_amount.setText(installmentamt.toString())
                }
            }

        })

        et_principal_overdue.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = validate!!.returnIntegerValue(et_outstanding_interest.text.toString())

                val total_overdue =
                    value + validate!!.returnIntegerValue(et_principal_overdue.text.toString())
                et_total_outstanding_including_due.setText(total_overdue.toString())
            }

        })

        et_outstanding_interest.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = validate!!.returnIntegerValue(et_principal_overdue.text.toString())

                val total_overdue =
                    value + validate!!.returnIntegerValue(et_outstanding_interest.text.toString())

                et_total_outstanding_including_due.setText(total_overdue.toString())
            }

        })

        et_outstanding_principal.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value =
                    validate!!.returnIntegerValue(et_outstanding_principal.text.toString()) - validate!!.returnIntegerValue(
                        et_principal_overdue.text.toString()
                    )
                var installment_num =
                    validate!!.returnIntegerValue(et_no_of_installment.text.toString())
                if (installment_num > 0 && value > 0) {
                    val installmentamt = value / installment_num
                    et_installment_amount.setText(installmentamt.toString())
                }

            }

        })

        et_total_loan_given.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var outstandingPrincipal =
                    validate!!.returnIntegerValue(et_total_loan_given.text.toString()) -
                            validate!!.returnIntegerValue(et_principal_repaid.text.toString())
                et_outstanding_principal.setText(validate!!.returnStringValue(outstandingPrincipal.toString()))

            }

        })

        et_principal_repaid.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var outstandingPrincipal =
                    validate!!.returnIntegerValue(et_total_loan_given.text.toString()) -
                            validate!!.returnIntegerValue(et_principal_repaid.text.toString())
                et_outstanding_principal.setText(validate!!.returnStringValue(outstandingPrincipal.toString()))
            }

        })

        spin_modePayment?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (validate!!.returnlookupcode(
                        spin_modePayment,
                        dataspin_modeofpayment
                    ) == 2
                ) {
                    lay_bank.visibility = View.VISIBLE
                    lay_bankView.visibility = View.VISIBLE
                    lay_chequeNO.visibility = View.VISIBLE
                    lay_chequeView.visibility = View.VISIBLE
                } else {
                    lay_bank.visibility = View.GONE
                    lay_bankView.visibility = View.GONE
                    lay_chequeNO.visibility = View.GONE
                    lay_chequeView.visibility = View.GONE
                    spin_bankName.setSelection(0)

                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }

        see_summary.setOnClickListener {
            val intent = Intent(this, VoCutOffSummaryOfActiveLoan::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        btn_cancel.setOnClickListener {
            val intent = Intent(this, VoCutOffSummaryOfActiveLoan::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                saveData()
                var loanAmount =
                    validate!!.returnIntegerValue(et_outstanding_principal.text.toString()) - validate!!.returnIntegerValue(
                        et_principal_overdue.text.toString()
                    )
                insertScheduler(
                    loanAmount,
                    validate!!.returnIntegerValue(et_no_of_installment.text.toString())
                )
            }
        }

        fillbank()
        fillSpinner()
        fillmemberspinner()
        fillActiveLoandata()

    }

    private fun setLabel() {
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText("cancel", R.string.cancel)
        tv_shg_name.text = LabelSet.getText("shg_name_", R.string.shg_name_)
        tv_fund_type.text = LabelSet.getText("fund_type", R.string.fund_type)
        tv_loan_no.text = LabelSet.getText("loan_no", R.string.loan_no)
        tv_loan_disbursmentDate.text = LabelSet.getText(
            "loan_disbursement_date",
            R.string.loan_disbursement_date
        )
        tv_purpose.text = LabelSet.getText("purpose", R.string.purpose)
        tv_existing_interest_rate.text = LabelSet.getText(
            "existing_interest_rate",
            R.string.existing_interest_rate
        )
        tv_repayment_frequency.text = LabelSet.getText(
            "repayment_frequency",
            R.string.repayment_frequency
        )
        tv_no_of_installment.text = LabelSet.getText(
            "no_of_installment",
            R.string.no_of_installment
        )
        tv_installment_amount_principal.text = LabelSet.getText(
            "installment_amount_principal",
            R.string.installment_amount_principal
        )
        tv_total_loan_given_amount.text = LabelSet.getText(
            "total_loan_given_amount",
            R.string.total_loan_given_amount
        )
        tv_principal_repaid.text = LabelSet.getText("principal_repaid", R.string.principal_repaid)
        tv_interest_demand.text = LabelSet.getText("interest_demand", R.string.interest_demand)
        tv_interest_paid.text = LabelSet.getText("interest_paid", R.string.interest_paid)
        tv_outstanding_principal.text = LabelSet.getText(
            "outstanding_principal",
            R.string.outstanding_principal
        )
        tv_principal_overdue.text = LabelSet.getText(
            "principal_overdue",
            R.string.principal_overdue
        )
        tv_outstanding_interest_if_any.text = LabelSet.getText(
            "outstanding_interest_if_any",
            R.string.outstanding_interest_if_any
        )
        tv_total_outstanding_including_due.text = LabelSet.getText(
            "total_outstanding_including_due",
            R.string.total_outstanding_including_due
        )
        see_summary.text = LabelSet.getText("see_summary", R.string.see_summary)
        et_loan_no.hint = LabelSet.getText("type_here", R.string.type_here)
        et_loan_disbursmentDate.hint = LabelSet.getText("date_format", R.string.date_format)
        et_existing_interest_rate.hint = LabelSet.getText("type_here", R.string.type_here)
        et_no_of_installment.hint = LabelSet.getText("type_here", R.string.type_here)
        et_installment_amount.hint = LabelSet.getText("auto", R.string.auto)
        et_total_loan_given.hint = LabelSet.getText("type_here", R.string.type_here)
        et_principal_repaid.hint = LabelSet.getText("type_here", R.string.type_here)
        et_interest_demand.hint = LabelSet.getText("type_here", R.string.type_here)
        et_interest_paid.hint = LabelSet.getText("type_here", R.string.type_here)
        et_outstanding_principal.hint = LabelSet.getText("type_here", R.string.type_here)
        et_outstanding_interest.hint = LabelSet.getText("type_here", R.string.type_here)
        et_total_outstanding_including_due.hint = LabelSet.getText("auto", R.string.auto)
        et_cheque_no_transactio_no.hint = LabelSet.getText("type_here", R.string.type_here)
        tv_modePayment.text = LabelSet.getText("mode_of_payment", R.string.mode_of_payment)
        tv_bankName.text = LabelSet.getText("name_of_bank", R.string.name_of_bank)
    }

    fun setMember(): Int {
        var pos = 0
        var name = ""
        if (!memberlist.isNullOrEmpty()) {
            for (i in memberlist!!.indices) {
                if (validate!!.RetriveSharepreferenceLong(VoSpData.voShgMemID) == memberlist!!.get(i).memId) {
                    pos = i + 1
                    name = memberlist!!.get(i).childCboName!!
                }

            }
        }
        spin_shgName.setSelection(pos)
        spin_shgName.isEnabled = false
        return pos
    }

    fun saveData() {
        var loan_application_id: Long? = null
        var memId = returnmemid()

        //interestDemand
        voMemLoanEntity = VoMemLoanEntity(
            0,  //uid
            0,  //voMtgDetUid
            loan_application_id,  //loanApplicationId
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            memId,  //memId
            validate!!.returnIntegerValue(et_loan_no.text.toString()),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.addmonth(validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),0,validate!!.returnlookupcode(spin_repayment_frequency, dataspin_repayment_freq)),  //installmentDate
            validate!!.returnIntegerValue(et_total_loan_given.text.toString()),  //originalLoanAmount
            validate!!.returnIntegerValue(et_outstanding_principal.text.toString()),  //amount
            validate!!.returnlookupcode(spin_purpose, dataspin_loan_purpose), //loanpurpose
            validate!!.returnFundTypeId(spin_fund_type, dataspin_fund),  //fundtype
            validate!!.returnDoubleValue(et_existing_interest_rate.text.toString()), //interestRate
            validate!!.returnIntegerValue(et_no_of_installment.text.toString()),  //period
            validate!!.returnIntegerValue(et_principal_overdue.text.toString()),  //principleOverdue
            validate!!.returnIntegerValue(et_outstanding_interest.text.toString()),  //interestOverdue
            false,  //completionflag
            validate!!.returnFundTypeId(spin_fund_type, dataspin_fund),  //loanType
            0,  //loanSource
            validate!!.returnlookupcode(spin_modePayment, dataspin_modeofpayment),  //ModePayments
            returaccount(), //bankCode
            validate!!.returnStringValue(et_cheque_no_transactio_no.text.toString()), //transanctionNo
            validate!!.returnlookupcode(spin_repayment_frequency, dataspin_repayment_freq),  //installmentFreq
            0,  //moratoriumPeriod
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            "",
            0,
            "",
            0,
            validate!!.returnIntegerValue(et_principal_repaid.text.toString()), //principleRepaid
            validate!!.returnIntegerValue(et_interest_paid.text.toString()), //interestRepaid
            validate!!.Daybetweentime(et_loan_disbursmentDate.text.toString()) //disbrusmentdate
       ,0,0,0.0,0,0,1 )
        voMemLoanViewModel.insertVoMemLoan(voMemLoanEntity!!)

        //insertvomemloantxn
        //interest overdue = outstanding interest
        voMemLoanTxnEntity = VoMemLoanTxnEntity(
            0,  //uid
            0,  //voMtgDetUid
            0,  //vomemloanuid
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            memId,  //memId
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.returnIntegerValue(et_total_loan_given.text.toString()),
            0,
            validate!!.returnIntegerValue(et_principal_repaid.text.toString()),
            validate!!.returnIntegerValue(et_interest_paid.text.toString()),
            validate!!.returnIntegerValue(et_outstanding_principal.text.toString()),
            validate!!.returnIntegerValue(et_interest_paid.text.toString()),
            false, //completionFlag
            0, //intAccruedOp
            validate!!.returnIntegerValue(et_outstanding_interest.text.toString()), //intAccruedCl
            validate!!.returnIntegerValue(et_outstanding_interest.text.toString()), //intAccruedCl
            0, //principalDemandOb
            validate!!.returnIntegerValue(et_principal_overdue.text.toString()),
            validate!!.returnIntegerValue(et_principal_overdue.text.toString()),
            validate!!.returnlookupcode(spin_modePayment, dataspin_modeofpayment), //modePayment
            returaccount(),  //bankCode
            validate!!.returnStringValue(et_cheque_no_transactio_no.text.toString()),  //transanctionNo
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            "",
            0,
            "",
            0,
            validate!!.returnIntegerValue(et_loan_no.text.toString()),0.0,0
        )
        voMemLoanTxnViewModel.insert(voMemLoanTxnEntity!!)
    }

    private fun insertScheduler(amt: Int, installment: Int) {
        var principaldemand = amt / installment
        var loanos = 0
        var saveValue = 0

        var memId = returnmemid()

        for (i in 0 until installment) {
            if (i == installment - 1) {
                loanos = 0
                principaldemand = amt - (principaldemand * i)
            } else {
                loanos = amt - (principaldemand * (i + 1))
            }
            var installMentDate = validate!!.addmonth(
                validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                (0 + i + 1), 0
            )
            voMemLoanScheduleEntity = VoMemLoanScheduleEntity(
                0,
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                memId,
                validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
                validate!!.returnIntegerValue(et_loan_no.text.toString()),
                principaldemand,
                principaldemand,
                loanos,
                0,
                i + 1,
                0,
                installMentDate,
                false,
                0,
               null,
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                "", 0,
                "", 0
            )
            voMemLoanScheduleViewModel.insertVoMemLoanSchedule(voMemLoanScheduleEntity!!)
        }

        var principalDemand =
            voMemLoanScheduleViewModel.getPrincipalDemandByInstallmentNum(
                memId,
                validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
                validate!!.returnIntegerValue(et_loan_no.text.toString()),
                1
            )
        var totalprincipalDemand =
            principalDemand + validate!!.returnIntegerValue(et_principal_overdue.text.toString())

        voMemLoanScheduleViewModel.updateCutOffLoanMemberSchedule(
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            memId,
            validate!!.returnIntegerValue(et_loan_no.text.toString()),
            1,
            totalprincipalDemand
        )

        CustomAlertchangeschedule(
            LabelSet.getText(
                "doyouwanttochaneschedule",
                R.string.doyouwanttochaneschedule
            )
        )
    }

    fun CustomAlertchangeschedule(str: String) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str
        mDialogView.btn_yes.background = resources.getDrawable(R.drawable.button_bg_vomeeting)
        mDialogView.btn_no.background = resources.getDrawable(R.drawable.button_bg_vomeeting)
        mDialogView.btn_yes.setOnClickListener {
            //            PartialSaveData()
            mAlertDialog.dismiss()
            validate!!.SaveSharepreferenceLong(VoSpData.voShgMemID, returnmemid())

            validate!!.SaveSharepreferenceInt(
                VoSpData.voLoanno,
                validate!!.returnIntegerValue(et_loan_no.text.toString())
            )

            validate!!.SaveSharepreferenceInt(VoSpData.voShgPos, returnmempos())

            val intent = Intent(this, VoCutOffPrincipalDemandActivity::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)
        }

        mDialogView.btn_no.setOnClickListener {
            mAlertDialog.dismiss()

            val intent = Intent(this, VoCutOffSummaryOfActiveLoan::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)

        }
    }

    private fun fillActiveLoandata() {

        val loanlist =
            voMemLoanViewModel.getLoanDetailData(
                validate!!.RetriveSharepreferenceInt(VoSpData.voLoanno),
                validate!!.RetriveSharepreferenceLong(
                    VoSpData.voshgid
                ),
                validate!!.RetriveSharepreferenceLong(VoSpData.voShgMemID),
                validate!!.RetriveSharepreferenceString(
                    VoSpData.vomtg_guid
                )!!
            )

        if (!loanlist.isNullOrEmpty()) {
            et_loan_no.setText(loanlist.get(0).loanNo.toString())
            et_no_of_installment.setText(validate!!.returnStringValue(loanlist.get(0).period.toString()))
            et_existing_interest_rate.setText(validate!!.returnStringValue(loanlist.get(0).interestRate.toString()))
            et_total_loan_given.setText(validate!!.returnStringValue(loanlist.get(0).originalLoanAmount.toString()))
            et_outstanding_principal.setText(validate!!.returnStringValue(loanlist.get(0).amount.toString()))
            et_principal_repaid.setText(validate!!.returnStringValue(loanlist.get(0).principalRepaid.toString()))
            et_principal_overdue.setText(validate!!.returnStringValue(loanlist.get(0).principalOverdue.toString()))
            et_outstanding_interest.setText(validate!!.returnStringValue(loanlist.get(0).interestOverdue.toString()))
            et_interest_paid.setText(validate!!.returnStringValue(loanlist.get(0).interestRepaid.toString()))
            et_cheque_no_transactio_no.setText(validate!!.returnStringValue(loanlist.get(0).transactionNo.toString()))
            et_loan_disbursmentDate.setText(
                validate!!.convertDatetime(
                    validate!!.returnLongValue(
                        loanlist.get(0).disbursementDate.toString()
                    )
                )
            )

            if (validate!!.returnIntegerValue(loanlist.get(0).period.toString()) > 0) {
                var installment_Amount =
                    (validate!!.returnIntegerValue(loanlist.get(0).amount.toString()) - validate!!.returnIntegerValue(
                        loanlist.get(0).principalOverdue.toString()
                    )) / validate!!.returnIntegerValue(
                        loanlist.get(0).period.toString()
                    )
                et_installment_amount.setText(validate!!.returnStringValue(installment_Amount.toString()))

            }

            if (validate!!.returnIntegerValue(loanlist.get(0).period.toString()) > 0) {
                var totaloutstandingincludingdue =
                    (validate!!.returnIntegerValue(loanlist.get(0).principalOverdue.toString()) + validate!!.returnIntegerValue(
                        loanlist.get(0).interestOverdue.toString()
                    ))

                et_total_outstanding_including_due.setText((totaloutstandingincludingdue.toString()))
            }

            spin_purpose.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(
                        loanlist.get(0).loanPurpose.toString()
                    ), dataspin_loan_purpose
                )
            )

            spin_repayment_frequency.setSelection(
                validate!!.returnlookupcodepos(
                    loanlist.get(0).installmentFreq,
                    dataspin_repayment_freq
                )
            )

            spin_fund_type.setSelection(
                validate!!.setFundType(
                    validate!!.returnIntegerValue(
                        loanlist.get(0).fundType.toString()
                    ), dataspin_fund
                )
            )

            spin_bankName.setSelection(
                setaccount(
                    loanlist.get(0).bankCode.toString()

                )
            )

            spin_modePayment.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(
                        loanlist.get(0).modePayment.toString()
                    ), dataspin_modeofpayment
                )
            )

            btn_savegray.visibility = View.VISIBLE
            btn_save.visibility = View.GONE

        } else {
            spin_repayment_frequency.setSelection(
                validate!!.returnlookupcodepos(
                    3,
                    dataspin_repayment_freq
                )
            )
            spin_modePayment.setSelection(
                validate!!.returnlookupcodepos(
                    1, dataspin_modeofpayment
                )
            )
            var loanno = voMemLoanViewModel.getCutOffmaxLoanno(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
            )

            et_loan_no.setText((loanno + 1).toString())

            btn_savegray.visibility = View.GONE
            btn_save.visibility = View.VISIBLE
        }

    }

    private fun checkValidation(): Int {

        val totalPrincipal =
            validate!!.returnIntegerValue(et_outstanding_principal.text.toString()) +
                    validate!!.returnIntegerValue(et_principal_repaid.text.toString())

        var value = 1

        if (spin_shgName.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_shgName,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "shg_name",
                    R.string.shg_name
                )
            )
            value = 0
            return value
        }

        if (spin_fund_type.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_fund_type,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "fund_type",
                    R.string.fund_type
                )
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_loan_no.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "pleaseenterloanno",
                    R.string.pleaseenterloanno
                ), this,
                et_loan_no
            )
            value = 0
            return value
        }

        if (validate!!.returnStringValue(et_loan_disbursmentDate.text.toString()).length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "loan_disbursement_date",
                    R.string.loan_disbursement_date
                ), this,
                et_loan_disbursmentDate
            )
            value = 0
            return value
        }

        if (spin_modePayment.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_modePayment,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "mode_of_payment",
                    R.string.mode_of_payment
                )

            )
            value = 0
            return value
        }

        if (lay_bank.visibility == View.VISIBLE && spin_bankName.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_bankName,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "source_bank",
                    R.string.source_bank
                )

            )
            value = 0
            return value
        }

        if (spin_purpose.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_purpose,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "purpose",
                    R.string.purpose
                )

            )
            value = 0
            return value
        }

        if (spin_repayment_frequency.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_repayment_frequency,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "repayment_frequency",
                    R.string.repayment_frequency
                )
            )
            value = 0
            return value
        }

        if (validate!!.returnlookupcode(spin_modePayment, dataspin_modeofpayment)== 1 &&validate!!.returnIntegerValue(et_outstanding_principal.text.toString()) > validate!!.RetriveSharepreferenceInt(
                VoSpData.voCashinhand)) {
            validate!!.CustomAlertSpinner(
                this,spin_modePayment,
                LabelSet.getText(
                    "inthecashbox",
                    R.string.inthecashbox
                )
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_no_of_installment.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "pleaseenterintallment",
                    R.string.pleaseenterintallment
                ), this,
                et_no_of_installment
            )
            value = 0
            return value
        }

      /*  var cashinbank= voGenerateMeetingViewmodel!!.getclosingbal(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid), returaccount())

        if(lay_bank.visibility == View.VISIBLE &&cashinbank<validate!!.returnIntegerValue(et_outstanding_principal.text.toString())){
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "intheselectedbank",
                    R.string.intheselectedbank
                ), this,
                et_outstanding_principal
            )
            value = 0
            return value
        }*/

        if (lay_chequeNO.visibility == View.VISIBLE && validate!!.returnStringValue(
                et_cheque_no_transactio_no.text.toString()
            ).length == 0
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + LabelSet.getText(
                    "cheque_no_transactio_no",
                    R.string.cheque_no_transactio_no
                ), this,
                et_cheque_no_transactio_no
            )
            value = 0
            return value
        }


        if (validate!!.returnIntegerValue(et_total_loan_given.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "total_loan_given_amount",
                    R.string.total_loan_given_amount
                ), this,
                et_total_loan_given
            )
            value = 0
            return value
        }

        if(validate!!.returnIntegerValue(et_total_loan_given.text.toString())<= validate!!.returnIntegerValue(et_principal_repaid.text.toString())){
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "compare_principal_repaid_total_loan",
                    R.string.compare_principal_repaid_total_loan
                ), this,
                et_principal_repaid
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_total_loan_given.text.toString()) != totalPrincipal) {
            validate!!.CustomAlert(
                LabelSet.getText(
                    "total_principal_disbursed1",
                    R.string.total_principal_disbursed1
                ), this
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_outstanding_principal.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "outstanding_principal",
                    R.string.outstanding_principal
                ), this,
                et_outstanding_principal
            )
            value = 0
            return value
        }

        if (validate!!.returnDoubleValue(et_existing_interest_rate.text.toString()) == 0.0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "enter_interest_rate",
                    R.string.enter_interest_rate
                ), this,
                et_existing_interest_rate
            )
            value = 0
            return value
        }

        if (validate!!.returnDoubleValue(et_existing_interest_rate.text.toString()) > 36) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "valid_interest",
                    R.string.valid_interest
                ), this,
                et_existing_interest_rate
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_installment_amount.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "installment_amount",
                    R.string.installment_amount
                ), this,
                et_installment_amount
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_principal_repaid.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "principal_repaid",
                    R.string.principal_repaid
                ), this,
                et_principal_repaid
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_interest_paid.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "interest_paid",
                    R.string.interest_paid
                ), this,
                et_interest_paid
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_principal_overdue.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "principal_overdue",
                    R.string.principal_overdue
                ), this,
                et_principal_overdue
            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_outstanding_interest.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "interest_paid",
                    R.string.outstanding_interest_if_any
                ), this,
                et_outstanding_interest
            )
            value = 0
            return value
        }


        return value
    }

    private fun fillSpinner() {

        dataspin_modeofpayment = lookupViewmodel.getlookup(
            61,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_loan_purpose = lookupViewmodel.getlookup(
            67,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_fund = loanProductViewmodel!!.getFundTypeBySource_Receipt(
            3, 2
        )

        dataspin_repayment_freq = lookupViewmodel.getlookup(
            19,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_shg_name = vomtgDetViewmodel.getListDataMtgByMtgnum(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        validate!!.fillspinner(this, spin_purpose, dataspin_loan_purpose)
        validate!!.fillFundType(this, spin_fund_type, dataspin_fund)
        validate!!.fillspinner(this, spin_repayment_frequency, dataspin_repayment_freq)
        validate!!.fillspinner(this, spin_modePayment, dataspin_modeofpayment)
    }

    fun fillbank() {
        voBankList = voGenerateMeetingViewmodel!!.getBankdata(
            validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID)
        )

        val adapter: ArrayAdapter<String?>
        if (!voBankList.isNullOrEmpty()) {
            val isize = voBankList!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in voBankList!!.indices) {
                var lastthree =
                    voBankList!![i].account_no.substring(voBankList!![i].account_no.length - 3)
                sValue[i + 1] =
                    voBankList!![i].ifsc_code.toString().dropLast(7) + "XXXXX" + lastthree
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_bankName.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_bankName.adapter = adapter
        }

    }

    fun returaccount(): String {

        var pos = spin_bankName.selectedItemPosition
        var id = ""

        if (!voBankList.isNullOrEmpty()) {
            if (pos > 0) id = voBankList!!.get(pos - 1).ifsc_code!!.dropLast(7) + voBankList!!.get(
                pos - 1
            ).account_no
        }
        return id
    }

    fun setaccount(accountno: String): Int {

        var pos = 0

        if (!voBankList.isNullOrEmpty()) {
            for (i in voBankList!!.indices) {
                if (accountno.equals(
                        voBankList!!.get(i).ifsc_code!!.dropLast(7) + voBankList!!.get(
                            i
                        ).account_no
                    )
                )
                    pos = i + 1
            }
        }
        return pos
    }

    private fun fillmemberspinner() {
        memberlist = vomtgDetViewmodel.getListDataMtgByMtgnum(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        val adapter: ArrayAdapter<String?>
        if (!memberlist.isNullOrEmpty()) {
            val isize = memberlist!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in memberlist!!.indices) {
                sValue[i + 1] = memberlist!![i].childCboName
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_shgName.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(memberlist!!.size + 1)
            sValue[0] = LabelSet.getText(
                "nomemberavailable",
                R.string.nomemberavailable
            )
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_shgName.adapter = adapter
        }
        setMember()
    }

    fun returnmemid(): Long {

        var pos = spin_shgName.selectedItemPosition
        var id = 0L

        if (!memberlist.isNullOrEmpty()) {
            if (pos > 0)
                id = memberlist!!.get(pos - 1).memId
        }
        return id
    }

    fun returnmempos(): Int {

        var pos = 0

        if (!memberlist.isNullOrEmpty()) {
            if (pos > 0)
                pos = spin_shgName.selectedItemPosition
        }
        return pos
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, VoCutOffSummaryOfActiveLoan::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()

    }

}