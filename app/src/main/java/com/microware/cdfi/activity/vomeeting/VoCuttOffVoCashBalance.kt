package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.voentity.VoFinTxnDetGrpEntity
import com.microware.cdfi.fragment.VoCutOffTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.voviewmodel.VoFinTxnDetGrpViewModel
import com.microware.cdfi.viewModel.voviewmodel.VoGenerateMeetingViewmodel
import kotlinx.android.synthetic.main.activity_cutt_off_vo_cash_balance.*
import kotlinx.android.synthetic.main.buttons_vo.*

class VoCuttOffVoCashBalance : AppCompatActivity() {
    var validate: Validate? = null

    lateinit var voGenerateMeetingViewmodel: VoGenerateMeetingViewmodel
    lateinit var voFinTxnDetGrpViewModel: VoFinTxnDetGrpViewModel
    var voFinTxnDetGrpEntity: VoFinTxnDetGrpEntity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cutt_off_vo_cash_balance)
        validate = Validate(this)

        voGenerateMeetingViewmodel =
            ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        voFinTxnDetGrpViewModel =
            ViewModelProviders.of(this).get(VoFinTxnDetGrpViewModel::class.java)

        replaceFragmenty(
            fragment = VoCutOffTopBarFragment(15),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        et_cash_in_hand.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = validate!!.returnIntegerValue(et_cash_in_transit.text.toString())
                val totalAmt = validate!!.returnIntegerValue(s.toString()) - value
                et_net_cash_in_hand.setText(totalAmt.toString())
            }

        })

        et_cash_in_transit.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = validate!!.returnIntegerValue(et_cash_in_hand.text.toString())
                val totalAmt = value - validate!!.returnIntegerValue(s.toString())
                et_net_cash_in_hand.setText(totalAmt.toString())

            }

        })

        et_date_of_balance.setOnClickListener(View.OnClickListener {
            validate!!.datePickerwithminmaxdate(validate!!.RetriveSharepreferenceLong(VoSpData.voFormation_dt),validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),et_date_of_balance)
        })

        btn_save.setOnClickListener {
            if (checkValidation() == 1) {
                SaveData()
            }
        }

        btn_cancel.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, VoCutOffMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        })

        setLabelText()
        showData()
    }

    fun setLabelText() {
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText("cancel", R.string.cancel)
        tv_cash_in_hand.text = LabelSet.getText("cash_in_hand", R.string.cash_in_hand)
        tv_cash_in_transit.text = LabelSet.getText("cash_in_transit", R.string.cash_in_transit)
        tv_net_cash_in_hand.text = LabelSet.getText("net_cash_in_hand", R.string.net_cash_in_hand)
        tv_date_of_balance.text = LabelSet.getText("date_of_balance", R.string.date_of_balance)
        et_cash_in_hand.hint = LabelSet.getText("type_here", R.string.type_here)
        et_cash_in_transit.hint = LabelSet.getText("type_here", R.string.type_here)
        et_net_cash_in_hand.hint = LabelSet.getText("auto", R.string.auto)
        et_date_of_balance.hint = LabelSet.getText("date_format", R.string.date_format)
    }

    private fun checkValidation(): Int {
        var value = 1

        if (et_cash_in_hand.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "cash_in_hand",
                    R.string.cash_in_hand
                ),
                this,
                et_cash_in_hand
            )
            value = 0
            return value
        } else if (et_cash_in_transit.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "cash_in_transit",
                    R.string.cash_in_transit
                ),
                this,
                et_cash_in_transit
            )
            value = 0
            return value
        } else if (validate!!.returnIntegerValue(et_cash_in_hand.text.toString()) < validate!!.returnIntegerValue(
                et_cash_in_transit.text.toString()
            )
        ) {
            validate!!.CustomAlert(
                LabelSet.getText(
                    "cash_in_hand_transit",
                    R.string.cash_in_hand_transit
                ), this
            )
            value = 0
            return value
        } else if (et_date_of_balance.text.toString().length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "date_of_balance",
                    R.string.date_of_balance
                ),
                this,
                et_date_of_balance
            )
            value = 0
            return value
        }

        return value
    }

    private fun SaveData() {

        voGenerateMeetingViewmodel.updateVoCutOffCash(
            validate!!.returnIntegerValue(et_cash_in_hand.text.toString()),
            validate!!.returnIntegerValue(et_cash_in_transit.text.toString()),
            validate!!.Daybetweentime(et_date_of_balance.text.toString()),
            validate!!.returnIntegerValue(et_net_cash_in_hand.text.toString()),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            validate!!.RetriveSharepreferenceString(AppSP.userid)!!

        )
        updateFinancialTransactionDetail()

    }

    private fun showData() {
        var list = voGenerateMeetingViewmodel.getMeetingDetailData(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        if (!list.isNullOrEmpty()) {
            et_cash_in_hand.setText(validate!!.returnStringValue(list.get(0).zeroMtgCashInHand.toString()))
            et_cash_in_transit.setText(validate!!.returnStringValue(list.get(0).zeroMtgCashInTransit.toString()))
            if (validate!!.returnLongValue(list.get(0).balanceDate.toString()) > 0) {
                et_date_of_balance.setText(
                    validate!!.convertDatetime(
                        validate!!.returnLongValue(
                            list.get(
                                0
                            ).balanceDate.toString()
                        )
                    )
                )
            } else {
                et_date_of_balance.setText(
                    validate!!.convertDatetime(
                        validate!!.returnLongValue(
                            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate)
                                .toString()
                        )
                    )
                )
            }
        }
    }

    fun updateFinancialTransactionDetail() {
        var iCount = voFinTxnDetGrpViewModel.getAdjustmentCashCount(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        if (iCount == 0) {
            if (validate!!.returnIntegerValue(et_net_cash_in_hand.text.toString()) < validate!!.RetriveSharepreferenceInt(
                    VoSpData.voCashinhand
                )) {
                var difference =
                    validate!!.RetriveSharepreferenceInt(VoSpData.voCashinhand) - validate!!.returnIntegerValue(
                        et_net_cash_in_hand.text.toString()
                    )
                voFinTxnDetGrpEntity = VoFinTxnDetGrpEntity(
                    0,
                    0,
                    validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
                    validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                    validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                    validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                    32,/*receipt goes here auid mstcoa*/
                    32,/*Receipt type goes here fundtype*/
                    0,
                    99,
                    "RG",/*enter type*/
                    difference,
                    "",
                    0,
                    1,
                    "",
                    /*"",
                    0,*/
                    "",
                    validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                    validate!!.RetriveSharepreferenceString(AppSP.userid),
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    "", 0, "", 0,0,2
                )
                voFinTxnDetGrpViewModel.insertVoGroupLoanSchedule(voFinTxnDetGrpEntity!!)

            } else if (validate!!.returnIntegerValue(et_net_cash_in_hand.text.toString()) > validate!!.RetriveSharepreferenceInt(
                    VoSpData.voCashinhand
                )
            ) {
                var difference =
                    validate!!.returnIntegerValue(et_net_cash_in_hand.text.toString()) - validate!!.RetriveSharepreferenceInt(
                        VoSpData.voCashinhand
                    )
                voFinTxnDetGrpEntity = VoFinTxnDetGrpEntity(
                    0,
                    0,
                    validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
                    validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                    validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                    validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                    39,/*receipt goes here auid mstcoa*/
                    39,/*Receipt type goes here fundtype*/
                    0,
                    99,
                    "PG",/*enter type*/
                    difference,
                    "",
                    0,
                    1,
                    "",
                    /*"",
                    0,*/
                    "",
                    validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                    validate!!.RetriveSharepreferenceString(AppSP.userid),
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    "",
                    0,
                    "",
                    0,0,2
                )
                voFinTxnDetGrpViewModel.insertVoGroupLoanSchedule(voFinTxnDetGrpEntity!!)
            }
        } else if (iCount > 0) {
            if (validate!!.returnIntegerValue(et_net_cash_in_hand.text.toString()) < validate!!.RetriveSharepreferenceInt(
                    VoSpData.voCashinhand
                )
            ) {
                var difference =
                    validate!!.RetriveSharepreferenceInt(VoSpData.voCashinhand) - validate!!.returnIntegerValue(
                        et_net_cash_in_hand.text.toString()
                    )

                //Trans date missing in table
                voFinTxnDetGrpViewModel.updateVoCashAdjustment(
                    validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                    validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                    32,
                    32,
                    99,
                    "RG",
                    difference,
//                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    0,
                    1,
                    "",
                    "",
                    validate!!.RetriveSharepreferenceString(AppSP.userid),
                    validate!!.Daybetweentime(validate!!.currentdatetime)
                )
            } else if (validate!!.returnIntegerValue(et_net_cash_in_hand.text.toString()) > validate!!.RetriveSharepreferenceInt(
                    VoSpData.voCashinhand
                )
            ) {
                var difference =
                    validate!!.returnIntegerValue(et_net_cash_in_hand.text.toString()) - validate!!.RetriveSharepreferenceInt(
                        VoSpData.voCashinhand
                    )
                //Trans date missing in table
                voFinTxnDetGrpViewModel.updateVoCashAdjustment(
                    validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                    validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                    39,
                    39,
                    99,
                    "PG",
                    difference,
//                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    0,
                    1,
                    "",
                    "",
                    validate!!.RetriveSharepreferenceString(AppSP.userid),
                    validate!!.Daybetweentime(validate!!.currentdatetime)
                )
            }
        }

        replaceFragmenty(
            fragment = VoCutOffTopBarFragment(15),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )
        validate!!.CustomAlertVO(
            LabelSet.getText(
                "data_saved_successfully",
                R.string.data_saved_successfully
            ), this
        )
    }

    override fun onBackPressed() {
        val intent = Intent(this, VoCutOffMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }
}