package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.entity.*
import com.microware.cdfi.entity.voentity.*
import com.microware.cdfi.fragment.VoMeetingTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.*
import com.microware.cdfi.viewModel.voviewmodel.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.vogenerate_meeting.*

class VoGenerateMeetingActivity : AppCompatActivity() {
    var validate: Validate? = null
    var lookup: LookupEntity? = null
    var generateMeetingViewmodel: VoGenerateMeetingViewmodel? = null
    var lookupViewmodel: LookupViewmodel? = null
    var datameetingtype: List<LookupEntity>? = null
    var voLastMeetingNumber: Int = 0
    var voMaxMeetingDate: Long = 0
    lateinit var voLoanGpViewmodel: VoGroupLoanViewModel
    lateinit var voLoanGpTxnViewmodel: VoGroupLoanTxnViewModel
    lateinit var voMtgGrpLoanScheduleViewmodel: VoGroupLoanScheduleViewModel
    var voMtgFinTxnViewmodel: VoFinTxnViewModel? = null
    var voLoanTxnMemViewmodel: VoMemLoanTxnViewModel? = null
    var voLoanMemberScheduleViewmodel: VoMemLoanScheduleViewModel? = null
    var voLoanMemberViewmodel: VoMemLoanViewModel? = null
    var voCoaMappingViewmodel: VoCoaMappingViewmodel? = null
    var mstVOCOAViewmodel: MstVOCOAViewmodel? = null
    var cboBankViewmodel: CboBankViewmodel? = null
    var vocoaMappingViewModel: VoCoaMappingViewmodel? = null

    //    var fedrationViewModel: FedrationViewModel? = null
    var mappedVoViewmodel: MappedShgViewmodel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.vogenerate_meeting)
        validate = Validate(this)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        voLoanGpViewmodel = ViewModelProviders.of(this).get(VoGroupLoanViewModel::class.java)
        voMtgGrpLoanScheduleViewmodel =
            ViewModelProviders.of(this).get(VoGroupLoanScheduleViewModel::class.java)
        voLoanGpTxnViewmodel = ViewModelProviders.of(this).get(VoGroupLoanTxnViewModel::class.java)
        voMtgFinTxnViewmodel = ViewModelProviders.of(this).get(VoFinTxnViewModel::class.java)
        voLoanTxnMemViewmodel = ViewModelProviders.of(this).get(VoMemLoanTxnViewModel::class.java)
        voLoanMemberScheduleViewmodel =
            ViewModelProviders.of(this).get(VoMemLoanScheduleViewModel::class.java)
        voLoanMemberViewmodel = ViewModelProviders.of(this).get(VoMemLoanViewModel::class.java)
//       fedrationViewModel = ViewModelProviders.of(this).get(FedrationViewModel::class.java)
        mappedVoViewmodel = ViewModelProviders.of(this).get(MappedShgViewmodel::class.java)
        voCoaMappingViewmodel = ViewModelProviders.of(this).get(VoCoaMappingViewmodel::class.java)
        mstVOCOAViewmodel = ViewModelProviders.of(this).get(MstVOCOAViewmodel::class.java)
        cboBankViewmodel = ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        vocoaMappingViewModel = ViewModelProviders.of(this).get(VoCoaMappingViewmodel::class.java)

        voLastMeetingNumber = validate!!.RetriveSharepreferenceInt(VoSpData.VoLastMeetingNumber)
        voMaxMeetingDate = validate!!.RetriveSharepreferenceLong(VoSpData.VoLastMeetingDate)

        replaceFragmenty(
            fragment = VoMeetingTopBarFragment(1),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        et_date.setOnClickListener {
            validate!!.datePickerwithmindate(
                validate!!.RetriveSharepreferenceLong(VoSpData.voFormation_dt),
                et_date
            )

        }

        val totalmtg =
            generateMeetingViewmodel!!.getmeetingCount(
                validate!!.RetriveSharepreferenceLong(
                    VoSpData.voshgid
                ).toString()
            )

        /* btn_generate_open.setOnClickListener {
             var intent = Intent(this, VoMeetingMenuActivity::class.java)
             intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
             startActivity(intent)
             overridePendingTransition(0, 0)
         }*/

        btn_generate_open.setOnClickListener {
            var strMsg = LabelSet.getText("generate_cutoff", R.string.generate_cutoff)
            if (sCheckValidation() == 1) {
                var meetingCount = generateMeetingViewmodel!!.getMeetingCount(
                    validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
                )
                var maxMeetingNum = generateMeetingViewmodel!!.getMaxMeetingNum(
                    validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
                )
                var meetingGap =
                    validate!!.returnIntegerValue(et_new_meeting_no.text.toString()) - maxMeetingNum
                if (meetingGap >= 12) {
                    if (maxMeetingNum == 0) {
                        CustomAlertGenerateMeeting(strMsg, totalmtg, 11)
                    } else {
                        CustomAlertGenerateMeeting(strMsg, totalmtg, 12)
                    }
                } else {
                    if (sCheckValid_RegularMeeting() == 1) {
                        generatemeeting(totalmtg)
                        validate!!.SaveSharepreferenceInt(VoSpData.voMeetingType, 0)
                        var intent = Intent(this, VoMeetingMenuActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                    }
                }
            }
        }

        btn_cancelmeeting.setOnClickListener {
            var intent = Intent(this, VoMeetingListActivity1::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }
        var list =
            voCoaMappingViewmodel!!.getcboData(validate!!.RetriveSharepreferenceLong(VoSpData.voshgid))
        if (list.isNullOrEmpty()) {
            insertcoadata()
        }
        setLabelText()
//        fillradio()
        showData(totalmtg)

    }

    fun insertcoadata() {
        var coaData = mstVOCOAViewmodel!!.getAlllist(
            listOf("RG", "RL", "RO", "PG", "PL", "PO"),
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
        )

        for (i in coaData!!.indices) {
            var vocoaMappingEntity = VoCoaMappingEntity(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                coaData.get(i).uid,
                returndefaaultaccount()
            )
            vocoaMappingViewModel!!.insert(vocoaMappingEntity)
        }
    }

    fun returndefaaultaccount(): String {
        var accountNo = ""
        val bankList = cboBankViewmodel!!.getcboBankdataModel(
            validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID),
            1
        )
        if (!bankList.isNullOrEmpty()) {
            for (i in bankList.indices) {
                if (bankList.get(i).is_default == 1) {
                    accountNo =
                        bankList.get(i).ifsc_code!!.dropLast(7) + bankList.get(i).account_no!!
                    break
                }
            }
        }
        return accountNo
    }

    private fun showData(totalmtg: Int) {
        et_new_meeting_no.setText("" + (voLastMeetingNumber + 1))
        var meetingfrequency = validate!!.RetriveSharepreferenceInt(VoSpData.vomeetingfrequency)
        if (voMaxMeetingDate == 0L) {
            et_date.setText(
                validate!!.convertDatetime(
                    validate!!.RetriveSharepreferenceLong(
                        VoSpData.voFormation_dt
                    )
                )
            )
        } else {
            var day = validate!!.RetriveSharepreferenceInt(VoSpData.vomeeting_on)
            var meetingfrequencyvalue =
                validate!!.RetriveSharepreferenceInt(VoSpData.vomeetingfrequencyvalue)
            et_last_meeting_no.setText("" + validate!!.RetriveSharepreferenceInt(VoSpData.VoLastMeetingNumber))
            et_last_meeting_date.setText(
                validate!!.convertDatetime(
                    validate!!.RetriveSharepreferenceLong(
                        VoSpData.VoLastMeetingDate
                    )
                )
            )
            if (meetingfrequency == 1) {

                et_date.setText(validate!!.setday(voMaxMeetingDate + (7 * 24 * 60 * 60), day))

            } else if (meetingfrequency == 2) {
                et_date.setText(validate!!.convertDatetime(voMaxMeetingDate + (14 * 24 * 60 * 60)))

            } else if (meetingfrequency == 3) {
                if (meetingfrequencyvalue == 6) {
                    et_date.setText(
                        validate!!.setdate(
                            voMaxMeetingDate + (28 * 24 * 60 * 60),
                            day
                        )
                    )
                } else {
                    et_date.setText(
                        validate!!.setday(
                            voMaxMeetingDate + (28 * 24 * 60 * 60),
                            day
                        )
                    )
                }

            }
        }
    }

    private fun setLabelText() {
//        tv_meeting_type.setText(LabelSet.getText("meeting_type", R.string.meeting_type))
        tv_last_meeting_no.text = LabelSet.getText("last_meeting_no", R.string.last_meeting_no)
        tv_last_meeting_date.text = LabelSet.getText(
            "last_meeting_date",
            R.string.last_meeting_date
        )
        tv_new_meeting_no.text = LabelSet.getText("new_meeting_no", R.string.new_meeting_no)
        tv_date.text = LabelSet.getText("meeting_date", R.string.meeting_date)
        et_new_meeting_no.hint = LabelSet.getText("type_here", R.string.type_here)
        et_date.hint = LabelSet.getText("date_format", R.string.date_format)
        btn_generate_open.text = LabelSet.getText("generate_open", R.string.generate_open)
        btn_cancelmeeting.text = LabelSet.getText("cancel", R.string.cancel)
    }

    /*  private fun fillradio() {

          datameetingtype = lookupViewmodel!!.getlookup(
              77,
              validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
          )

          validate!!.fillspinner(
              this, spinMtgtype, datameetingtype
          )
      }*/

    fun sCheckValidation(): Int {
        var iValue = 1
        var maxMeetingNum = generateMeetingViewmodel!!.getMaxMeetingNum(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        var meetingGap =
            validate!!.returnIntegerValue(et_new_meeting_no.text.toString()) - maxMeetingNum
        if (validate!!.returnIntegerValue(et_new_meeting_no.text.toString()) <= maxMeetingNum) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "meeting_num_less_last_meeting_num",
                    R.string.meeting_num_less_last_meeting_num
                ), this, et_new_meeting_no
            )
            iValue = 0
        } else if (meetingGap > 1 && meetingGap < 12) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "valid_meeting_num",
                    R.string.valid_meeting_num
                ), this, et_new_meeting_no
            )
            iValue = 0
        }
        return iValue
    }

    fun sCheckValid_RegularMeeting(): Int {
        var iValue = 1
        var maxMeetingNum = generateMeetingViewmodel!!.getMaxMeetingNum(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        var meetingGap =
            validate!!.returnIntegerValue(et_new_meeting_no.text.toString()) - maxMeetingNum
        if (meetingGap > 1) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "valid_meeting_num",
                    R.string.valid_meeting_num
                ), this, et_new_meeting_no
            )
            iValue = 0
        }
        return iValue
    }

    fun CustomAlertGenerateMeeting(str: String, totalmtg: Int, meetingType: Int) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this).setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str
        mDialogView.btn_yes.background = resources.getDrawable(R.drawable.button_bg_vomeeting)
        mDialogView.btn_no.background = resources.getDrawable(R.drawable.button_bg_vomeeting)
        mDialogView.btn_yes.setOnClickListener {
            mAlertDialog.dismiss()

            validate!!.SaveSharepreferenceInt(VoSpData.voMeetingType, meetingType)
            generateZeroMeeting(totalmtg, meetingType)
            var intent = Intent(this, VoCutOffMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)

        }

        mDialogView.btn_no.setOnClickListener {
            mAlertDialog.dismiss()
            if (sCheckValid_RegularMeeting() == 1) {
                generatemeeting(totalmtg)
                validate!!.SaveSharepreferenceInt(VoSpData.voMeetingType, 0)
                var intent = Intent(this, VoMeetingMenuActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
        }
    }

    private fun generatemeeting(totalmtg: Int) {

        var Mtgdata = generateMeetingViewmodel!!.getgroupMeetingsdata(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid).toString()
        )

        var meetingfrequency =
            validate!!.RetriveSharepreferenceInt(VoSpData.vomeetingfrequency).toString()
        var meetingfrequencyvalue =
            validate!!.RetriveSharepreferenceInt(VoSpData.vomeetingfrequencyvalue)
        var meeting_on = validate!!.RetriveSharepreferenceInt(VoSpData.vomeeting_on)
        var currentmeetingnumber = validate!!.returnIntegerValue(et_new_meeting_no.text.toString())

        if (totalmtg == 0) {

            var meeting_dayDiff =
                voMaxMeetingDate - validate!!.Daybetweentime(et_date.text.toString())

            var meetingguid = validate!!.random()
            var vomtgEntity = VomtgEntity(
                0,
                validate!!.returnLongValue(Mtgdata?.get(0)?.federation_id.toString()),
                meetingguid,
                0,
                currentmeetingnumber,
                validate!!.Daybetweentime(et_date.text.toString()),
                "O",
                meetingfrequency,
                "",
                0,
                Mtgdata?.get(0)?.closingBalance,
                Mtgdata?.get(0)?.closingBalance,
                0,
                0,
                1,
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                0,
                0,
                0,
                Mtgdata?.get(0)?.savCompCb,
                0,
                0,
                0,
                Mtgdata?.get(0)?.savVolCb,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                Mtgdata?.get(0)?.closingBalanceCash,
                Mtgdata?.get(0)?.closingBalanceCash,
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                "",
                0,
                "",
                0, 0, 0, 0
            )
            generateMeetingViewmodel!!.insert(vomtgEntity)
            validate!!.SaveSharepreferenceInt(VoSpData.vocurrentmeetingnumber, currentmeetingnumber)
            validate!!.SaveSharepreferenceInt(VoSpData.vomaxmeetingnumber, currentmeetingnumber)
            validate!!.SaveSharepreferenceLong(
                VoSpData.voCurrentMtgDate,
                validate!!.Daybetweentime(et_date.text.toString())
            )
            validate!!.SaveSharepreferenceString(VoSpData.vomtg_guid, meetingguid)
            insertData_dtMtgDetaildata(
                meetingguid,
                currentmeetingnumber,
                validate!!.Daybetweentime(et_date.text.toString()),
                totalmtg
            )

        } else {
            var meeting_dayDiff =
                voMaxMeetingDate - validate!!.Daybetweentime(et_date.text.toString())

            var meetingguid = validate!!.random()
            var vomtgEntity = VomtgEntity(
                0,
                validate!!.returnLongValue(Mtgdata?.get(0)?.federation_id.toString()),
                meetingguid,
                0,
                currentmeetingnumber,
                validate!!.Daybetweentime(et_date.text.toString()),
                "O",
                meetingfrequency,
                "",
                0,
                Mtgdata?.get(0)?.closingBalance,
                Mtgdata?.get(0)?.closingBalance,
                0,
                0,
                1,
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                0,
                0,
                0,
                Mtgdata?.get(0)?.savCompCb,
                0,
                0,
                0,
                Mtgdata?.get(0)?.savVolCb,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                Mtgdata?.get(0)?.closingBalanceCash,
                Mtgdata?.get(0)?.closingBalanceCash,
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                "",
                0,
                "",
                0, 0, 0, 0
            )
            generateMeetingViewmodel!!.insert(vomtgEntity)
            validate!!.SaveSharepreferenceInt(VoSpData.vocurrentmeetingnumber, currentmeetingnumber)
            validate!!.SaveSharepreferenceInt(VoSpData.vomaxmeetingnumber, currentmeetingnumber)
            validate!!.SaveSharepreferenceLong(
                VoSpData.voCurrentMtgDate,
                validate!!.Daybetweentime(et_date.text.toString())
            )
            validate!!.SaveSharepreferenceString(VoSpData.vomtg_guid, meetingguid)
            insertData_dtMtgDetaildata(
                meetingguid,
                currentmeetingnumber,
                validate!!.Daybetweentime(et_date.text.toString()),
                totalmtg
            )

        }
    }

    private fun generateZeroMeeting(totalmtg: Int, meetingType: Int) {
        var Mtgdata = generateMeetingViewmodel!!.getgroupMeetingsdata(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid).toString()
        )
        var meetingfrequency =
            validate!!.RetriveSharepreferenceInt(VoSpData.vomeetingfrequency).toString()
        var meetingfrequencyvalue =
            validate!!.RetriveSharepreferenceInt(VoSpData.vomeetingfrequencyvalue)
        var meeting_on = validate!!.RetriveSharepreferenceInt(VoSpData.vomeeting_on)
        var currentmeetingnumber = validate!!.returnIntegerValue(et_new_meeting_no.text.toString())

        var meetingguid = validate!!.random()
        var vomtgEntity = VomtgEntity(
            0,
            validate!!.returnLongValue(Mtgdata?.get(0)?.federation_id.toString()),
            meetingguid,
            meetingType,
            currentmeetingnumber,
            validate!!.Daybetweentime(et_date.text.toString()),
            "O",
            meetingfrequency,
            "",
            0,
            Mtgdata?.get(0)?.closingBalance,
            Mtgdata?.get(0)?.closingBalance,
            0,
            0,
            1,
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            0,
            0,
            0,
            Mtgdata?.get(0)?.savCompCb,
            0,
            0,
            0,
            Mtgdata?.get(0)?.savVolCb,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            Mtgdata?.get(0)?.closingBalanceCash,
            Mtgdata?.get(0)?.closingBalanceCash,
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            "",
            0,
            "",
            0, 0, 0, 0
        )
        generateMeetingViewmodel!!.insert(vomtgEntity)
        validate!!.SaveSharepreferenceInt(VoSpData.vocurrentmeetingnumber, currentmeetingnumber)
        validate!!.SaveSharepreferenceInt(VoSpData.vomaxmeetingnumber, currentmeetingnumber)
        validate!!.SaveSharepreferenceLong(
            VoSpData.voCurrentMtgDate,
            validate!!.Daybetweentime(et_date.text.toString())
        )
        validate!!.SaveSharepreferenceString(VoSpData.vomtg_guid, "")
        validate!!.SaveSharepreferenceString(VoSpData.vomtg_guid, meetingguid)

        insertCutoff_dtMtgDetaildata(
            meetingguid,
            currentmeetingnumber,
            validate!!.Daybetweentime(et_date.text.toString()),
            totalmtg
        )

    }

    private fun insertData_dtMtgDetaildata(
        meetingguid: String,
        mtgNo: Int,
        mtgDate: Long,
        totalMeetings: Int
    ) {
//        var memberList = mappedVoViewmodel!!.getShgNameList(validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID)!!)
        val memberList =
            mappedVoViewmodel!!.getShgNameList(validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID)!!)

        var vomtgDetailList: List<VoMtgDetEntity>? = null

        if (totalMeetings > 0) {
            vomtgDetailList = generateMeetingViewmodel!!.getMtgByMtgnum(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid), (mtgNo - 1)
            )
            if (!memberList.isNullOrEmpty()) {
                for (i in 0 until memberList.size) {

                    var mtgDetailList = vomtgDetailList.filter { it ->
                        it.memId == memberList.get(i).shg_id /*here shg_id replace with mem_id */
                    }

                    var vomtgDetEntity: VoMtgDetEntity? = null
                    if (!mtgDetailList.isNullOrEmpty()) {
                        vomtgDetEntity = VoMtgDetEntity(
                            0,
                            0,
                            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                            memberList[i].shg_id!!,/*mem id here */
                            meetingguid,
                            mtgNo,
                            mtgDate,
                            i,
                            memberList[i].shg_name,
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            0,
                            0,
                            "",
                            0,mtgDetailList.get(0).savCompCb,0,mtgDetailList.get(0).savCompCb,mtgDetailList.get(0).savVolCb,0,mtgDetailList.get(0).savVolCb,0,
                            validate!!.RetriveSharepreferenceString(AppSP.userid),
                            validate!!.Daybetweentime(validate!!.currentdatetime),
                            validate!!.RetriveSharepreferenceString(AppSP.userid),
                            validate!!.Daybetweentime(validate!!.currentdatetime),
                            "",
                            0, memberList[i].settlement_status
                        )
                        generateMeetingViewmodel!!.insertvomtgDet(vomtgDetEntity)
                    } else {
                        vomtgDetEntity = VoMtgDetEntity(
                            0,
                            0,
                            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                            memberList[i].shg_id!!,/*mem id here */
                            meetingguid,
                            mtgNo,
                            mtgDate,
                            i,
                            memberList[i].shg_name,
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            0,
                            0,
                            "",
                            0,0,0,0,0,0,0,0,
                            validate!!.RetriveSharepreferenceString(AppSP.userid),
                            validate!!.Daybetweentime(validate!!.currentdatetime),
                            validate!!.RetriveSharepreferenceString(AppSP.userid),
                            validate!!.Daybetweentime(validate!!.currentdatetime),
                            "",
                            0, memberList[i].settlement_status
                        )

                        generateMeetingViewmodel!!.insertvomtgDet(vomtgDetEntity)
                    }

                    val voLoanTxnMemlist = voLoanTxnMemViewmodel!!.getmeetingLoanTxnMemdata(
                        validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                        memberList[i].shg_id!! /*memeber id to shg_id replace*/,
                        voLastMeetingNumber
                    )

                    if (!voLoanTxnMemlist.isNullOrEmpty()) {
                        for (i in 0 until voLoanTxnMemlist.size) {
                            val principaldemand =
                                voLoanMemberScheduleViewmodel!!.getprincipaldemand(
                                    voLoanTxnMemlist.get(i).loanNo,
                                    voLoanTxnMemlist.get(i).memId,
                                    mtgDate,
                                    validate!!.Daybetweentime(et_last_meeting_date.text.toString())
                                )
                            val loanos = voLoanMemberScheduleViewmodel!!.gettotaloutstanding(
                                 voLoanTxnMemlist.get(i).loanNo,
                                voLoanTxnMemlist.get(i).memId
                            )
                            val insrate =
                                voLoanMemberViewmodel!!.getinterestrate(  voLoanTxnMemlist.get(i).loanNo,voLoanTxnMemlist.get(i).memId)
                            val loanosint = ((loanos * insrate / (100 * 365)) * validate!!.getday(
                                et_date.text.toString(),
                                et_last_meeting_date.text.toString()
                            ) + 0.5).toInt()
                            var intrestclosing =
                                loanosint + validate!!.returnIntegerValue(voLoanTxnMemlist.get(i).intAccruedCl.toString())
                            var voLoanTxnMemEntity: VoMemLoanTxnEntity? = null

                            voLoanTxnMemEntity = VoMemLoanTxnEntity(
                                0,
                                0,
                                0,
                                voLoanTxnMemlist.get(i).cboId,
                                voLoanTxnMemlist.get(i).memId,
                                voLoanTxnMemlist.get(i).mtgGuid,
                                mtgNo,
                                mtgDate,
                                loanos,
                                voLoanTxnMemlist.get(i).loanClInt,
                                0,
                                0,
                                loanos,
                                voLoanTxnMemlist.get(i).loanClInt,
                                voLoanTxnMemlist.get(i).completionFlag,
                                voLoanTxnMemlist.get(i).intAccruedCl,
                                loanosint,
                                intrestclosing,
                                voLoanTxnMemlist.get(i).principalDemandCb,
                                principaldemand,
                                voLoanTxnMemlist.get(i).principalDemandCb,
                                0,
                                voLoanTxnMemlist.get(i).bankCode,
                                "",
                                validate!!.RetriveSharepreferenceString(AppSP.userid),
                                validate!!.Daybetweentime(validate!!.currentdatetime),
                                "",
                                0,
                                "",
                                0, voLoanTxnMemlist.get(i).loanNo,0.0,0
                            )
                            voLoanTxnMemViewmodel!!.insert(voLoanTxnMemEntity)
                            /* if (i == 0) {
                                generateMeetingViewmodel!!.updateloandata1(
                                    voLoanTxnMemlist.get(i).memId!!,
                                    mtgNo,
                                    voLoanTxnMemlist.get(i).loan_no,
                                    loanos,
                                    intrestclosing
                                )
                            } else if (i == 1) {
                                generateMeetingViewmodel!!.updateloandata2(
                                    voLoanTxnMemlist.get(i).memId!!,
                                    mtgNo,
                                    voLoanTxnMemlist.get(i).loan_no,
                                    loanos,
                                    intrestclosing
                                )
                            } else if (i == 2) {
                                generateMeetingViewmodel!!.updateloandata3(
                                    voLoanTxnMemlist.get(i).memId!!,
                                    mtgNo,
                                    voLoanTxnMemlist.get(i).loan_no,
                                    loanos,
                                    intrestclosing
                                )
                            } else if (i == 3) {
                                generateMeetingViewmodel!!.updateloandata4(
                                    voLoanTxnMemlist.get(i).memId!!,
                                    mtgNo,
                                    voLoanTxnMemlist.get(i).loan_no,
                                    loanos,
                                    intrestclosing
                                )
                            } else if (i == 4) {
                                generateMeetingViewmodel!!.updateloandata5(
                                    voLoanTxnMemlist.get(i).memId!!,
                                    mtgNo,
                                    voLoanTxnMemlist.get(i).loan_no,
                                    loanos,
                                    intrestclosing
                                )
                            }
                        }*/

                        }

                    }

                    var txnlist = voLoanGpTxnViewmodel.getListDataByMtgnum(
                        voLastMeetingNumber, validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
                    )

                    if (!txnlist.isNullOrEmpty()) {
                        for (i in 0 until txnlist.size) {
                            val principaldemand =
                                voMtgGrpLoanScheduleViewmodel.getprincipaldemand(
                                    txnlist.get(i).loanNo,
                                    txnlist.get(i).cboId,
                                    mtgDate,
                                    validate!!.Daybetweentime(et_last_meeting_date.text.toString())
                                )
                            val loanos = voMtgGrpLoanScheduleViewmodel.gettotaloutstanding(
                                txnlist.get(i).loanNo,
                                txnlist.get(i).cboId
                            )
                            val insrate =
                                voLoanGpViewmodel.getinterestrate(txnlist.get(i).loanNo)
                            val loanosint = ((loanos * insrate / (100 * 365)) * validate!!.getday(
                                et_date.text.toString(),
                                et_last_meeting_date.text.toString()
                            ) + 0.5).toInt()
                            var intrestclosing =
                                loanosint + validate!!.returnIntegerValue(txnlist.get(i).intAccruedCl.toString())

                            var voLoanGpTxnEntity: VoGroupLoanTxnEntity? = null

                            voLoanGpTxnEntity = VoGroupLoanTxnEntity(
                                0,
                                txnlist.get(i).cboId,
                                meetingguid,
                                mtgNo,
                                mtgDate,
                                txnlist.get(i).loanNo,
                                loanos,
                                txnlist.get(i).loanClInt,
                                0,
                                0,
                                loanos,
                                txnlist.get(i).loanClInt,
                                txnlist.get(i).completionFlag,
                                txnlist.get(i).intAccruedCl,
                                loanosint,
                                intrestclosing,
                                txnlist.get(i).principalDemandCb,
                                principaldemand,
                                txnlist.get(i).principalDemandCb,
                                0,
                                "",
                                "",
                                validate!!.RetriveSharepreferenceString(AppSP.userid),
                                validate!!.Daybetweentime(validate!!.currentdatetime),
                                validate!!.RetriveSharepreferenceString(AppSP.userid),
                                validate!!.Daybetweentime(validate!!.currentdatetime),
                                "",
                                0,0.0,0,0
                            )
                            voLoanGpTxnViewmodel.insertVoGroupLoanTxn(voLoanGpTxnEntity)
                        }
                    }

                    var shgBankList = generateMeetingViewmodel!!.getBankdata(
                        validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID)
                    )
                    for (i in 0 until shgBankList.size) {
                        var vofinTxnEntity: VoFinTxnEntity? = null
                        var bankcode =
                            shgBankList.get(i).ifsc_code!!.dropLast(7) + shgBankList.get(i).account_no

                        var gpfintxnlist = voMtgFinTxnViewmodel!!.getListDataByMtgnum(
                            voLastMeetingNumber,
                            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                            bankcode
                        )
                        var closingbal = 0
                        var closingbalcash = 0
                        if (!gpfintxnlist.isNullOrEmpty()) {
                            closingbal =
                                validate!!.returnIntegerValue(gpfintxnlist.get(0).closingBalance.toString())
                            closingbalcash =
                                validate!!.returnIntegerValue(gpfintxnlist.get(0).closingBalanceCash.toString())
                        }
                        vofinTxnEntity = VoFinTxnEntity(
                            0,
                            0,
                            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                            meetingguid,
                            mtgNo,
                            bankcode,
                            closingbal,
                            closingbal,
                            0,
                            0,
                            0,
                            0,
                            closingbalcash,
                            closingbalcash,
                            validate!!.RetriveSharepreferenceString(AppSP.userid),
                            validate!!.Daybetweentime(validate!!.currentdatetime),
                            validate!!.RetriveSharepreferenceString(AppSP.userid),
                            validate!!.Daybetweentime(validate!!.currentdatetime),
                            "",
                            0, 0, 0, 0, 0
                        )
                        voMtgFinTxnViewmodel!!.insertVoFinTxn(vofinTxnEntity)
                    }

                }

            }
        } else {
            if (!memberList.isNullOrEmpty()) {
                for (i in 0 until memberList.size) {
                    var vomtgDetEntity: VoMtgDetEntity? = null

                    vomtgDetEntity = VoMtgDetEntity(
                        0,
                        0,
                        validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                        memberList[i].shg_id!!, /*mem id here */
                        meetingguid,
                        mtgNo,
                        mtgDate,
                        i,
                        memberList[i].shg_name!!,
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        0,
                        0,
                        "",
                        0,0,0,0,0,0,0,0,
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        "",
                        0, memberList[i].settlement_status
                    )
                    generateMeetingViewmodel!!.insertvomtgDet(vomtgDetEntity)
                    var voLoanTxnMemlist = voLoanTxnMemViewmodel!!.getmeetingLoanTxnMemdata(
                        validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                        memberList.get(i).shg_id!!,/*member_id replace with shg_id*/
                        voLastMeetingNumber

                    )
                    if (!voLoanTxnMemlist.isNullOrEmpty()) {
                        for (i in 0 until voLoanTxnMemlist.size) {
                            val principaldemand =
                                voLoanMemberScheduleViewmodel!!.getprincipaldemand(
                                     voLoanTxnMemlist.get(i).loanNo,
                                    voLoanTxnMemlist.get(i).memId,
                                    mtgDate,
                                    validate!!.Daybetweentime(et_last_meeting_date.text.toString())
                                )
                            val loanos = voLoanMemberScheduleViewmodel!!.gettotaloutstanding(
                                 voLoanTxnMemlist.get(i).loanNo,
                                voLoanTxnMemlist.get(i).memId
                            )
                            val insrate =
                                voLoanMemberViewmodel!!.getinterestrate(
                                    voLoanTxnMemlist.get(i).loanNo,
                                    voLoanTxnMemlist.get(i).memId)
                            val loanosint =
                                ((loanos * insrate / (100 * 365)) * validate!!.getday(
                                    et_date.text.toString(),
                                    et_last_meeting_date.text.toString()
                                ) + 0.5).toInt()

                            var voLoanTxnMemEntity: VoMemLoanTxnEntity? = null

                            voLoanTxnMemEntity = VoMemLoanTxnEntity(
                                0,
                                0,
                                0,
                                voLoanTxnMemlist.get(i).cboId,
                                voLoanTxnMemlist.get(i).memId,
                                voLoanTxnMemlist.get(i).mtgGuid,
                                mtgNo,
                                mtgDate,
                                loanos,
                                voLoanTxnMemlist.get(i).loanClInt,
                                0,
                                0,
                                loanos,
                                voLoanTxnMemlist.get(i).loanClInt,
                                voLoanTxnMemlist.get(i).completionFlag,
                                voLoanTxnMemlist.get(i).intAccruedCl,
                                loanosint,
                                voLoanTxnMemlist.get(i).intAccruedCl,
                                voLoanTxnMemlist.get(i).principalDemandCb,
                                principaldemand,
                                voLoanTxnMemlist.get(i).principalDemandCb,
                                0,
                                voLoanTxnMemlist.get(i).bankCode,
                                "",
                                validate!!.RetriveSharepreferenceString(AppSP.userid),
                                validate!!.Daybetweentime(validate!!.currentdatetime),
                                "",
                                0,
                                "",
                                0, voLoanTxnMemlist.get(i).loanNo,0.0,0
                            )
                            voLoanTxnMemViewmodel!!.insert(voLoanTxnMemEntity)

                        }

                    }
                }
            }

            var txnlist = voLoanGpTxnViewmodel.getListDataByMtgnum(
                voLastMeetingNumber,
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
            )

            if (!txnlist.isNullOrEmpty()) {
                for (i in 0 until txnlist.size) {
                    val principaldemand = voMtgGrpLoanScheduleViewmodel.getprincipaldemand(
                        txnlist.get(i).loanNo,
                        txnlist.get(i).cboId,
                        mtgDate,
                        validate!!.Daybetweentime(et_last_meeting_date.text.toString())
                    )
                    val loanos = voMtgGrpLoanScheduleViewmodel.gettotaloutstanding(
                        txnlist.get(i).loanNo,
                        txnlist.get(i).cboId
                    )
                    val insrate = voLoanGpViewmodel.getinterestrate(txnlist.get(i).loanNo)
                    val loanosint = ((loanos * insrate / (100 * 365)) * validate!!.getday(
                        et_date.text.toString(),
                        et_last_meeting_date.text.toString()
                    ) + 0.5).toInt()

                    var voLoanGpTxnEntity: VoGroupLoanTxnEntity? = null

                    voLoanGpTxnEntity = VoGroupLoanTxnEntity(
                        0,
                        txnlist.get(i).cboId,
                        meetingguid,
                        mtgNo,
                        mtgDate,
                        txnlist.get(i).loanNo,
                        loanos,
                        txnlist.get(i).loanClInt,
                        0,
                        0,
                        loanos,
                        txnlist.get(i).loanClInt,
                        txnlist.get(i).completionFlag,
                        txnlist.get(i).intAccruedCl,
                        loanosint.toInt(),
                        txnlist.get(i).intAccruedCl,
                        txnlist.get(i).principalDemandCb,
                        principaldemand,
                        txnlist.get(i).principalDemandCb,
                        0,
                        "",
                        "",
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        "",
                        0,0.0,0,0
                    )
                    voLoanGpTxnViewmodel.insertVoGroupLoanTxn(voLoanGpTxnEntity)
                }
            }
            var shgBankList = generateMeetingViewmodel!!.getBankdata(
                validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID)
            )
            for (i in 0 until shgBankList.size) {
                var vofinTxnEntity: VoFinTxnEntity? = null
                var bankcode =
                    shgBankList.get(i).ifsc_code!!.dropLast(7) + shgBankList.get(i).account_no
                vofinTxnEntity = VoFinTxnEntity(
                    0,
                    0,
                    validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                    meetingguid,
                    mtgNo,
                    bankcode,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    validate!!.RetriveSharepreferenceString(AppSP.userid),
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    validate!!.RetriveSharepreferenceString(AppSP.userid),
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    "",
                    0, 0, 0, 0, 0
                )
                voMtgFinTxnViewmodel!!.insertVoFinTxn(vofinTxnEntity)
            }
        }

    }

    private fun insertCutoff_dtMtgDetaildata(
        meetingguid: String,
        mtgNo: Int,
        mtgDate: Long,
        totalMeetings: Int
    ) {
        var memberList =
            mappedVoViewmodel!!.getShgNameList(validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID)!!)

        if (!memberList.isNullOrEmpty()) {
            for (i in 0 until memberList.size) {

                var vomtgDetEntity: VoMtgDetEntity? = null

                vomtgDetEntity = VoMtgDetEntity(
                    0,
                    0,
                    validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                    memberList[i].shg_id!!,/*mem id here */
                    meetingguid,
                    mtgNo,
                    mtgDate,
                    i,
                    memberList[i].shg_name,
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    0,
                    0,
                    "",
                    0,0,0,0,0,0,0,0,
                    validate!!.RetriveSharepreferenceString(AppSP.userid),
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    validate!!.RetriveSharepreferenceString(AppSP.userid),
                    validate!!.Daybetweentime(validate!!.currentdatetime),
                    "",
                    0, memberList[i].settlement_status
                )
                generateMeetingViewmodel!!.insertvomtgDet(vomtgDetEntity)
            }
        }

        var shgBankList = generateMeetingViewmodel!!.getBankdata(
            validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID)
        )
        for (i in 0 until shgBankList.size) {
            var vofinTxnEntity: VoFinTxnEntity? = null

            var bankcode =
                shgBankList.get(i).ifsc_code!!.dropLast(7) + shgBankList.get(i).account_no
            vofinTxnEntity = VoFinTxnEntity(
                0,
                0,
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                meetingguid,
                mtgNo,
                bankcode,
                0,
                0,
                0,
                0,
                0, 0,
                0,
                0,
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                "",
                0, 0, 0, 0, 0
            )
            voMtgFinTxnViewmodel!!.insertVoFinTxn(vofinTxnEntity)
        }

    }

    override fun onBackPressed() {
        var intent = Intent(this, VoMeetingListActivity1::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

}