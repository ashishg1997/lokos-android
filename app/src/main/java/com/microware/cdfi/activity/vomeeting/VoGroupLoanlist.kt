package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.VoGroupLoanListAdapter
import com.microware.cdfi.fragment.VoReceiptsTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoGenerateMeetingViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoGroupLoanTxnViewModel
import com.microware.cdfi.viewModel.voviewmodel.VoGroupLoanViewModel
import kotlinx.android.synthetic.main.vo_group_loanlist.*

class VoGroupLoanlist : AppCompatActivity() {
    var validate: Validate? = null
    lateinit var voGenerateMeetingViewmodel: VoGenerateMeetingViewmodel
    lateinit var voGroupLoanTxnViewModel: VoGroupLoanTxnViewModel
    lateinit var voGroupLoanViewModel: VoGroupLoanViewModel
    lateinit var lookupViewmodel: LookupViewmodel
    var today = 0
    var paid = 0
    var nextpayable = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.vo_group_loanlist)

        validate = Validate(this)
        voGenerateMeetingViewmodel =
            ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        voGroupLoanTxnViewModel =
            ViewModelProviders.of(this).get(VoGroupLoanTxnViewModel::class.java)
        lookupViewmodel =
            ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        voGroupLoanViewModel =
            ViewModelProviders.of(this).get(VoGroupLoanViewModel::class.java)

        img_Add.isEnabled =
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber) >= validate!!.RetriveSharepreferenceInt(VoSpData.vomaxmeetingnumber)
        img_Add.setOnClickListener {

            var intent = Intent(this, SRLMtoVOLoanReceipts::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        replaceFragmenty(
            fragment = VoReceiptsTopBarFragment(15),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        setLabelText()
        fillRecyclerView()
    }

    override fun onBackPressed() {
        var intent = Intent(this, VoMeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

    fun setLabelText() {
        tv_loan_source.text = LabelSet.getText(
            "loan_source",
            R.string.loan_source
        )
        tv_paid.text = LabelSet.getText("paid", R.string.paid)
        tv_today_s_demand.text = LabelSet.getText(
            "todays_payable",
            R.string.todays_payable
        )
        tv_next_demand.text = LabelSet.getText(
            "next_payable_amount",
            R.string.next_payable_amount
        )
        tvTotal.text = LabelSet.getText(
            "total",
            R.string.total
        )

    }

    private fun fillRecyclerView() {
        today = 0
        paid = 0
        nextpayable = 0
        var list = voGroupLoanViewModel.getListDataByMtgnum(
            validate!!.RetriveSharepreferenceInt(
                VoSpData.vocurrentmeetingnumber
            ),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(
                VoSpData.LoanSource
            ), validate!!.RetriveSharepreferenceInt(VoSpData.ShortDescription)


        )
        var repaymentDetailAdapter = VoGroupLoanListAdapter(this, list)

        rvList.layoutManager = LinearLayoutManager(this)

        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = rvList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvList.layoutParams = params
        rvList.adapter = repaymentDetailAdapter
    }

    fun returnloanlist( flag: Int, loanno: Int): String {
        var loandata = voGroupLoanViewModel.getLoandata(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            loanno, validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
        )
        if (!loandata.isNullOrEmpty()){
            var listdata = lookupViewmodel.getlookup(
                flag,
                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
            )
            return validate!!.returnlookupcodevalue(loandata.get(0).institution, listdata)
        }
        return ""
    }

    fun getTotalValue(iValue: Int, flag: Int) {
        if (flag == 1) {
            today = today + iValue
            tv_value1.text = today.toString()
        } else if (flag == 2) {
            paid = paid + iValue
            tv_value2.text = paid.toString()
        } else if (flag == 3) {
            nextpayable = nextpayable + iValue
            tv_value3.text = nextpayable.toString()
        }
    }

/*fun returnlist(memberid: Long): List<LoanRepaymentListModel>? {
    return dtLoanTxnMemViewmodel!!.getmemberloan(
        memberid,
        validate!!.RetriveSharepreferenceInt(
            MeetingSP.currentmeetingnumber
        )
    )
}*/
}