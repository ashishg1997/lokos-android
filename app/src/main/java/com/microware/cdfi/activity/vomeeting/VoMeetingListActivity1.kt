package com.microware.cdfi.activity.vomeeting

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.JsonObject
import com.microware.cdfi.R
import com.microware.cdfi.activity.vo.VoDrawerActivity
import com.microware.cdfi.adapter.vomeetingadapter.VoLstAdapter
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.api.vomodel.VoMeetingModel
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.voentity.VomtgEntity
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.FedrationViewModel
import com.microware.cdfi.viewModel.ResponseViewModel
import com.microware.cdfi.viewModel.voviewmodel.VoGenerateMeetingViewmodel
import kotlinx.android.synthetic.main.customealertdialogelogin.*
import kotlinx.android.synthetic.main.customealertdialogelogin.view.*
import kotlinx.android.synthetic.main.repay_toolbar_vomeeting.*
import kotlinx.android.synthetic.main.vo_list.*
import org.apache.http.conn.ConnectTimeoutException
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException

class VoMeetingListActivity1 : AppCompatActivity() {
    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: ApiInterface? = null
    var generateMeetingViewmodel: VoGenerateMeetingViewmodel? = null
    var fedrationViewModel: FedrationViewModel? = null
    var responseViewModel: ResponseViewModel? = null
    var validate: Validate? = null
    var cboType = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.vo_list)

        tv_title.text = LabelSet.getText("vo_list", R.string.vo_list)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        responseViewModel = ViewModelProviders.of(this).get(ResponseViewModel::class.java)

        validate = Validate(this)
        if (validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType) == 2) {
            cboType = 2
        } else {
            cboType = 1
        }

        ic_Back.setOnClickListener {
            val intent = Intent(this, VoDrawerActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        fedrationViewModel = ViewModelProviders.of(this).get(FedrationViewModel::class.java)

        fillLoanRecycler()
        setLabelText()
    }

    fun returnmeetingdata(shgid: Long): List<VomtgEntity>? {
        return generateMeetingViewmodel!!.getMtglistdata(shgid)
    }

    fun getEcMembercount(shgguid: String): Int {
        return fedrationViewModel!!.getcount(shgguid)
    }

    fun getMappedShgCount(voguid: String): Int {
        return fedrationViewModel!!.getMappedShgCount(voguid)
    }

    fun getMappedVoCount(voguid: String): Int {
        return fedrationViewModel!!.getMappedVoCount(voguid)
    }

    fun getMeetingCountByCboId(federationId: Long): Int {
        return validate!!.returnIntegerValue(
            generateMeetingViewmodel!!.getMeetingCount(federationId).toString()
        )
    }

    fun openmeeting(federationId: Long, mtgno: Int) {
        return generateMeetingViewmodel!!.openMeeting("O", federationId, mtgno)
    }

    fun returnmeetingstatus(federationId: Long, Mtgno: Int): String {
        return generateMeetingViewmodel!!.returnmeetingstatus(federationId, Mtgno)
    }

    private fun fillLoanRecycler() {
        val listData = generateMeetingViewmodel!!.getgroupMeetingsdatalist(cboType) as ArrayList<VoMeetingModel>
        if (!listData.isNullOrEmpty()) {
            rvList.layoutManager = LinearLayoutManager(this)
            rvList.adapter = VoLstAdapter(this, listData, cboType)
        }
    }

    private fun setLabelText() {
        tv_village.text = LabelSet.getText("village", R.string.village)
    }

    fun importVoMeetinglist() {
        progressDialog = ProgressDialog.show(
            this, LabelSet.getText("app_name", R.string.app_name),
            LabelSet.getText("DataLoading", R.string.DataLoading)
        )
        progressDialog.setIcon(this.getDrawable(R.drawable.appicon))
        var idownload = 0
        var msg = ""
        var code = 0
        object : Thread() {

            override fun run() {
                try {
                    val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                    val token = validate!!.returnStringValue(
                        AESEncryption.decrypt(
                            validate!!.RetriveSharepreferenceString(AppSP.token),
                            validate!!.RetriveSharepreferenceString(AppSP.userid)
                        )
                    )
                    val voID = validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
                    var role = validate!!.RetriveSharepreferenceString(AppSP.Roleid)

                    val call1 = apiInterface?.getVoMeeinglist(
                        token,
                        user,
                        voID,
                        1,
                        role!!
                    )

                    val res1 = call1?.execute()

                    if (res1!!.isSuccessful) {
                        try {
                            deleteMeetingData()
                            msg = "" + res1.code() + " " + res1.message()

                            if (res1.body()?.downLoadVoMtgList != null) {
                                try {
                                    var downLoadVoMtgList = res1.body()?.downLoadVoMtgList
                                    for (i in 0 until downLoadVoMtgList!!.size) {

                                        var voMeeting = downLoadVoMtgList[i].voMeeting
                                        var voLoanApplicationList = downLoadVoMtgList[i].voLoanApplicationList
                                        var voMemberLoan = downLoadVoMtgList[i].voMemberLoanList
                                        var voGroupLoan = downLoadVoMtgList[i].voGroupLoanList

                                        var vomeetingList = MappingData.returnvomtgObjentity(voMeeting)
                                        var voMemberLoanList = MappingData.returnvomemLoanObjentity(voMemberLoan)
                                        var voGroupLoanList = MappingData.returnvogroupLoanmtgObjentity(voGroupLoan)


                                        var voMeetingDetailsList = voMeeting!!.voMeetingDetailsList
                                        var voFinanceTransactionList = voMeeting.voFinanceTransactionList
                                        var voFinanceTransactionDetailGroupList = voMeeting.voFinanceTransactionDetailGroupList
                                        var voGroupLoanTransactionList = voMeeting.voGroupLoanTransactionList

                                        for(j in 0 until voMeetingDetailsList?.size!!){
                                            voMeetingDetailsList[j].cboId = downLoadVoMtgList[i].voMeeting!!.cboId
                                            voMeetingDetailsList[j].mtgNo = downLoadVoMtgList[i].voMeeting!!.mtgNo
                                            voMeetingDetailsList[j].mtgGuid = downLoadVoMtgList[i].voMeeting!!.mtgGuid
                                            voMeetingDetailsList[j].mtgDate = downLoadVoMtgList[i].voMeeting!!.mtgDate
                                        }

                                        for(j in 0 until voFinanceTransactionList?.size!!){
                                            voFinanceTransactionList[j].cboId = downLoadVoMtgList[i].voMeeting!!.cboId!!
                                            voFinanceTransactionList[j].mtgNo = downLoadVoMtgList[i].voMeeting!!.mtgNo!!
                                            voFinanceTransactionList[j].mtgGuid = downLoadVoMtgList[i].voMeeting!!.mtgGuid
                                            //      voFinanceTransactionList[j].mtgDate = downLoadVoMtgList[i].voMeeting!!.mtgDate
                                        }

                                        for(j in 0 until voFinanceTransactionDetailGroupList?.size!!){
                                            voFinanceTransactionDetailGroupList[j].cboId = downLoadVoMtgList[i].voMeeting!!.cboId!!
                                            voFinanceTransactionDetailGroupList[j].mtgNo = downLoadVoMtgList[i].voMeeting!!.mtgNo!!
                                            voFinanceTransactionDetailGroupList[j].mtgGuid = downLoadVoMtgList[i].voMeeting!!.mtgGuid
                                            voFinanceTransactionDetailGroupList[j].mtgDate = downLoadVoMtgList[i].voMeeting!!.mtgDate
                                        }

                                        for(j in 0 until voGroupLoanTransactionList?.size!!){
                                            voGroupLoanTransactionList[j].cboId = downLoadVoMtgList[i].voMeeting!!.cboId!!
                                            voGroupLoanTransactionList[j].mtgNo = downLoadVoMtgList[i].voMeeting!!.mtgNo!!
                                            voGroupLoanTransactionList[j].mtgGuid = downLoadVoMtgList[i].voMeeting!!.mtgGuid
                                            voGroupLoanTransactionList[j].mtgDate = downLoadVoMtgList[i].voMeeting!!.mtgDate
                                        }

                                        var voMtgDetailList = MappingData.returnvomtgDetailObjentity(voMeetingDetailsList)
                                        //        var shgMeetingDetailsList = MappingData.returnshgMeetingDetailObjentity(shgMeetingDetailsListData)


                                        CDFIApplication.database?.fedmtgDao()
                                            ?.insertFedmtg(vomeetingList)

                                        if (!voMtgDetailList.isNullOrEmpty()) {
                                            CDFIApplication.database?.voMtgDetDao()
                                                ?.insertAllVoMtgDet(voMtgDetailList)
                                        }

                                        if (!voFinanceTransactionList.isNullOrEmpty()) {
                                            CDFIApplication.database?.voFinTxnDao()
                                                ?.insertAllVoFinTxn(voFinanceTransactionList)
                                        }

                                        if (!voFinanceTransactionDetailGroupList.isNullOrEmpty()) {
                                            CDFIApplication.database?.voFinTxnDetGrpDao()
                                                ?.insertAllVoGroupLoanSchedule(
                                                    voFinanceTransactionDetailGroupList
                                                )
                                        }

                                        if (!voGroupLoanTransactionList.isNullOrEmpty()) {
                                            CDFIApplication.database?.voGroupLoanTxnDao()
                                                ?.insertAllVoGroupLoanTxn(voGroupLoanTransactionList)
                                        }

                                        if (!voLoanApplicationList.isNullOrEmpty()) {
                                            CDFIApplication.database?.voFedLoanAppDao()
                                                ?.insertAllFedLoanApplicationData(
                                                    voLoanApplicationList
                                                )
                                        }

                                        if (!voMemberLoanList.isNullOrEmpty()) {
                                            CDFIApplication.database?.voMemLoanDao()
                                                ?.insertAllVoMemLoan(voMemberLoanList)
                                        }

                                        if (!voGroupLoanList.isNullOrEmpty()) {
                                            CDFIApplication.database?.voGroupLoanDao()
                                                ?.insertAllVoGroupLoan(voGroupLoanList)
                                        }

                                        for (l in 0 until voMemberLoan!!.size) {
                                            var voMemberLoanScheduleList = voMemberLoan[l].voMemberLoanScheduleList
                                            if (!voMemberLoanScheduleList.isNullOrEmpty()) {
                                                for (j in 0 until voMemberLoanScheduleList.size) {
                                                    voMemberLoanScheduleList[j].cboId =
                                                        voMemberLoan[l].cboId!!
                                                    voMemberLoanScheduleList[j].mtgNo =
                                                        voMemberLoan[l].mtgNo
                                                    voMemberLoanScheduleList[j].mtgGuid =
                                                        voMemberLoan[l].mtgGuid
                                                }
                                                CDFIApplication.database?.voMemLoanScheduleDao()
                                                    ?.insertAllVoMemLoanSchedule(
                                                        voMemberLoanScheduleList
                                                    )
                                            }
                                        }

                                        for (k in 0 until voGroupLoan!!.size) {
                                            var voGroupLoanScheduleList = voGroupLoan[k].voGroupLoanScheduleList
                                            if (!voGroupLoanScheduleList.isNullOrEmpty()) {
                                                for (j in 0 until voGroupLoanScheduleList.size) {
                                                    voGroupLoanScheduleList[j].cboId =
                                                        voGroupLoan[k].cboId!!
                                                    voGroupLoanScheduleList[j].mtgNo =
                                                        voGroupLoan[k].mtgNo
                                                    voGroupLoanScheduleList[j].mtgGuid =
                                                        voGroupLoan[k].mtgGuid
                                                }
                                                CDFIApplication.database?.voGroupLoanScheduleDao()
                                                    ?.insertAllVoGroupLoanSchedule(
                                                        voGroupLoanScheduleList
                                                    )
                                            }
                                        }

                                        for (l in 0 until voMeetingDetailsList.size) {
                                            var voMemberLoanTxnList =
                                                voMeetingDetailsList[l].voMemberLoanTransactionList
                                            var voFinTxnDetailMemberList =
                                                voMeetingDetailsList[l].voFinanceTransactionDetailMemberList
                                            var voMemberSettlementList =
                                                voMeetingDetailsList[l].voMemberSettlementList

                                            if (!voMemberLoanTxnList.isNullOrEmpty()) {
                                                CDFIApplication.database?.voMemLoanTxnDao()
                                                    ?.insertAllVoMemLoanTxn(voMemberLoanTxnList)
                                            }

                                            if (!voFinTxnDetailMemberList.isNullOrEmpty()) {
                                                CDFIApplication.database?.voFinTxnDetMemDao()
                                                    ?.insertAllData(voFinTxnDetailMemberList)
                                            }

                                            if (!voMemberSettlementList.isNullOrEmpty()) {
                                                CDFIApplication.database?.vomemSettlementDao()
                                                    ?.insertSettlement(voMemberSettlementList)
                                            }

                                        }

                                    }
                                } catch (ex: Exception) {
                                    ex.printStackTrace()
                                }
                            }
                            idownload = 1
                            progressDialog.dismiss()

                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }

                    } else {
                        var resMsg = ""
                        if (res1.code() == 403) {
                            code = res1.code()
                            msg = res1.message()
                        } else {
                            if (res1.errorBody()?.contentLength() == 0L || res1.errorBody()
                                    ?.contentLength()!! < 0L
                            ) {
                                code = res1.code()
                                resMsg = res1.message()
                            } else {
                                var jsonObject1 =
                                    JSONObject(res1.errorBody()!!.source().readUtf8().toString())

                                code =
                                    validate!!.returnIntegerValue(
                                        jsonObject1.optString("responseCode").toString()
                                    )
                                resMsg = validate!!.returnStringValue(
                                    jsonObject1.optString("responseMsg").toString()
                                )
                            }
                            msg = "" + validate!!.alertMsg(
                                this@VoMeetingListActivity1,
                                responseViewModel,
                                code,
                                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                                resMsg
                            )

                        }

                    }

                    runOnUiThread {
                        if (idownload == 1) {
                            progressDialog.dismiss()
                            val text = LabelSet.getText(
                                "Datadownloadedsuccessfully",
                                R.string.Datadownloadedsuccessfully
                            )
                            validate!!.CustomAlert(text, this@VoMeetingListActivity1)
                            fillLoanRecycler()
                        } else {
                            progressDialog.dismiss()
                            if (code == 403) {
                                Toast.makeText(
                                    this@VoMeetingListActivity1,
                                    msg,
                                    Toast.LENGTH_LONG
                                )
                                    .show()
                                CustomAlertlogin()
                            } else {
                                val text = msg
                                validate!!.CustomAlert(text, this@VoMeetingListActivity1)
                                Toast.makeText(
                                    this@VoMeetingListActivity1,
                                    msg,
                                    Toast.LENGTH_LONG
                                )
                                    .show()
                            }
                        }

                    }

                } catch (bug: SocketTimeoutException) {
                    progressDialog.dismiss()
                } catch (bug: ConnectTimeoutException) {
                    progressDialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                    progressDialog.dismiss()
                }
            }

        }.start()
    }

    fun deleteMeetingData() {
        /*Table1*/
        CDFIApplication.database?.fedmtgDao()
            ?.deleteVo_MtgByVoId(validate!!.RetriveSharepreferenceLong(VoSpData.voshgid))
        /*Table2*/
        CDFIApplication.database?.voMtgDetDao()
            ?.deleteVo_MtgByVoId(validate!!.RetriveSharepreferenceLong(VoSpData.voshgid))
        /*Table3*/
        CDFIApplication.database?.voFinTxnDao()
            ?.deleteVO_FinancialTxnData(validate!!.RetriveSharepreferenceLong(VoSpData.voshgid))
        /*Table4*/
        CDFIApplication.database?.voFinTxnDetGrpDao()
            ?.deleteVO_FinancialTxnDetGrpData(validate!!.RetriveSharepreferenceLong(VoSpData.voshgid))
        /*Table5*/
        CDFIApplication.database?.voGroupLoanTxnDao()
            ?.deleteVO_GroupLoanTxnData(validate!!.RetriveSharepreferenceLong(VoSpData.voshgid))
        /*Table6*/
        CDFIApplication.database?.voFedLoanAppDao()
            ?.deleteVO_LoanAppData(validate!!.RetriveSharepreferenceLong(VoSpData.voshgid))
        /*Table7*/
        CDFIApplication.database?.voMemLoanDao()
            ?.deleteVO_MemLoanData(validate!!.RetriveSharepreferenceLong(VoSpData.voshgid))
        /*Table8*/
        CDFIApplication.database?.voMemLoanScheduleDao()
            ?.deleteVO_MemLoanSceduleData(validate!!.RetriveSharepreferenceLong(VoSpData.voshgid))
        /*Table9*/
        CDFIApplication.database?.voGroupLoanDao()
            ?.deleteVO_GroupLoanData(validate!!.RetriveSharepreferenceLong(VoSpData.voshgid))
        /*Table10*/
        CDFIApplication.database?.voGroupLoanScheduleDao()
            ?.deleteVO_GroupScenduleLoanData(validate!!.RetriveSharepreferenceLong(VoSpData.voshgid))
        /*Table11*/
        CDFIApplication.database?.voMemLoanTxnDao()
            ?.deleteVO_memLoanTxnData(validate!!.RetriveSharepreferenceLong(VoSpData.voshgid))
        /*Table12*/
        CDFIApplication.database?.voFinTxnDetMemDao()
            ?.deleteVO_memLoanTxnData(validate!!.RetriveSharepreferenceLong(VoSpData.voshgid))
    }

    fun CustomAlertlogin() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogelogin, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        val password = validate!!.returnStringValue(
            AESEncryption.decrypt(
                validate!!.RetriveSharepreferenceString(AppSP.Password),
                validate!!.RetriveSharepreferenceString(AppSP.userid)
            )
        )
        mDialogView.etUsername.setText(validate!!.RetriveSharepreferenceString(AppSP.userid))
        mDialogView.etPassword.setText(password)
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_dialog_title2.text = LabelSet.getText("app_name", R.string.app_name)
        mDialogView.btn_login.text = LabelSet.getText("log_in", R.string.log_in)
        mDialogView.chk_showHide1.text = LabelSet.getText("show_password", R.string.show_password)
        mDialogView.btn_login.setOnClickListener {
            mAlertDialog.dismiss()
            importtoken(
                mDialogView.etUsername.text.toString(),
                validate!!.returnStringValue(
                    AESEncryption.encrypt(
                        validate!!.returnStringValue(
                            mDialogView.etPassword.text.toString()
                        ), mDialogView.etUsername.text.toString()
                    )
                )
            )
        }
        mAlertDialog.chk_showHide1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                mAlertDialog.etPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                mAlertDialog.etPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
        }

    }

    fun importtoken(user: String, pass: String) {
        var grant_type = "password"
        var userId = user
        var password = pass
        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText("authenticate_user", R.string.authenticate_user)
        )
        val callCount = apiInterface?.gettokenforce(
            grant_type,
            userId,
            password, validate!!.RetriveSharepreferenceInt(AppSP.State_Selected), 1,
            validate!!.RetriveSharepreferenceString(AppSP.Roleid)!!
        )
        callCount?.enqueue(object : Callback<JsonObject> {
            override fun onFailure(callCount: Call<JsonObject>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                //       Log.e("", response!!.body().toString())
                progressDialog.dismiss()


                if (response.isSuccessful) {

                    var jsonObject1: JSONObject? = null
                    try {
                        jsonObject1 = JSONObject(response.body().toString())

                        val auth = jsonObject1.get("accessToken").toString()
                        if (auth.trim().length > 0) {
                            validate!!.SaveSharepreferenceString(
                                AppSP.token,
                                validate!!.returnStringValue(
                                    AESEncryption.encrypt(
                                        auth, validate!!.RetriveSharepreferenceString(AppSP.userid)
                                    )
                                )
                            )
                        }
                        validate!!.CustomAlert(
                            LabelSet.getText(
                                "userauthenticatedsuccessfully",
                                R.string.userauthenticatedsuccessfully
                            ), this@VoMeetingListActivity1
                        )
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        var jsonObject1 =
                            JSONObject(response.errorBody()!!.source().readUtf8().toString())

                        var resCode =
                            validate!!.returnIntegerValue(
                                jsonObject1.optString("responseCode").toString()
                            )
                        validate!!.CustomAlertMsg(
                            this@VoMeetingListActivity1,
                            responseViewModel,
                            resCode,
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                            validate!!.returnStringValue(
                                jsonObject1.optString("responseMsg").toString()
                            )
                        )
                    }

                } else {
                    var jsonObject1 =
                        JSONObject(response.errorBody()!!.source().readUtf8().toString())

                    var resCode =
                        validate!!.returnIntegerValue(
                            jsonObject1.optString("responseCode").toString()
                        )
                    validate!!.CustomAlertMsg(
                        this@VoMeetingListActivity1,
                        responseViewModel,
                        resCode,
                        validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                        validate!!.returnStringValue(
                            jsonObject1.optString("responseMsg").toString()
                        )
                    )
                }


            }

        })


    }

    override fun onBackPressed() {
        val intent = Intent(this, VoDrawerActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
        overridePendingTransition(0, 0)
    }
}