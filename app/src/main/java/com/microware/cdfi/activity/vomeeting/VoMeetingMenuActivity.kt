package com.microware.cdfi.activity.vomeeting

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProviders
import com.google.gson.JsonObject
import com.microware.cdfi.R
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.api.response.MeetingResponse
import com.microware.cdfi.entity.ShgFinancialTxnVouchersEntity
import com.microware.cdfi.entity.voentity.VomtgEntity
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.ResponseViewModel
import com.microware.cdfi.viewModel.VouchersViewModel
import com.microware.cdfi.viewModel.voviewmodel.*
import kotlinx.android.synthetic.main.customealertdialogelogin.*
import kotlinx.android.synthetic.main.customealertdialogelogin.view.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.user_detail_status_new.*
import kotlinx.android.synthetic.main.repay_toolbar_vomeeting.*
import kotlinx.android.synthetic.main.user_meeting_status.*
import kotlinx.android.synthetic.main.vomeeting_menu.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class VoMeetingMenuActivity : AppCompatActivity() {
    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: ApiInterface? = null
    var validate: Validate? = null
    lateinit var generateMeetingViewmodel: VoGenerateMeetingViewmodel
    var voLoanViewmodel: VoLoanApplicationViewmodel? = null
    var vofinancialTransactionsMemViewmodel: VoFinTxnDetMemViewModel? = null
    var voLoanTxnMemViewmodel: VoMemLoanTxnViewModel? = null
    var voLoanGpViewmodel: VoGroupLoanViewModel? = null
    var voFintxnDetGrpViewmodel: VoFinTxnDetGrpViewModel? = null
    var voLoanGPTxnViewmodel: VoGroupLoanTxnViewModel? = null
    var voLoanMemberViewmodel: VoMemLoanViewModel? = null
    var vofinTxnViewmodel: VoFinTxnViewModel? = null
    var vomemberLoanScheduleViewmodel: VoMemLoanScheduleViewModel? = null
    var voGrpLoanScheduleViewModel: VoGroupLoanScheduleViewModel? = null
    var mtglist: List<VomtgEntity>? = null
    var voMtgDetViewModel: VoMtgDetViewModel? = null
    var vouchersViewModel: VouchersViewModel? = null
    var responseViewModel: ResponseViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.vomeeting_menu)
        validate = Validate(this)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()

        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        voLoanViewmodel = ViewModelProviders.of(this).get(VoLoanApplicationViewmodel::class.java)
        vofinancialTransactionsMemViewmodel =
            ViewModelProviders.of(this).get(VoFinTxnDetMemViewModel::class.java)
        voLoanTxnMemViewmodel = ViewModelProviders.of(this).get(VoMemLoanTxnViewModel::class.java)
        voLoanGpViewmodel = ViewModelProviders.of(this).get(VoGroupLoanViewModel::class.java)
        voFintxnDetGrpViewmodel =
            ViewModelProviders.of(this).get(VoFinTxnDetGrpViewModel::class.java)
        voLoanGPTxnViewmodel = ViewModelProviders.of(this).get(VoGroupLoanTxnViewModel::class.java)
        voLoanMemberViewmodel = ViewModelProviders.of(this).get(VoMemLoanViewModel::class.java)
        vofinTxnViewmodel = ViewModelProviders.of(this).get(VoFinTxnViewModel::class.java)
        vomemberLoanScheduleViewmodel =
            ViewModelProviders.of(this).get(VoMemLoanScheduleViewModel::class.java)
        voGrpLoanScheduleViewModel =
            ViewModelProviders.of(this).get(VoGroupLoanScheduleViewModel::class.java)
        voMtgDetViewModel = ViewModelProviders.of(this).get(VoMtgDetViewModel::class.java)
        vouchersViewModel = ViewModelProviders.of(this).get(VouchersViewModel::class.java)
        responseViewModel = ViewModelProviders.of(this).get(ResponseViewModel::class.java)

        tv_title.text = LabelSet.getText("meeting_menu", R.string.meeting_menu)
        tv_code.text = validate!!.RetriveSharepreferenceLong(VoSpData.voShgcode).toString()
        tv_name.text = validate!!.RetriveSharepreferenceString(VoSpData.voShgName)
        tv_date.text =
            validate!!.convertDatetime(validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate))
        tv_count.text = "" + validate!!.RetriveSharepreferenceInt(VoSpData.voMemberCount)

        ic_Back.setOnClickListener {
            val intent = Intent(this, VoMeetingListActivity1::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
        }

        mtglist =
            generateMeetingViewmodel.getMtglist(validate!!.RetriveSharepreferenceLong(VoSpData.voshgid))

        spin_meeting_no.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {
                if (position > 0) {

                    et_meeting_date.setText(
                        validate!!.convertDatetime(
                            validate!!.returnvoMeetingdate(
                                spin_meeting_no,
                                mtglist
                            )
                        )
                    )
                    validate!!.SaveSharepreferenceLong(
                        VoSpData.voCurrentMtgDate,
                        validate!!.returnvoMeetingdate(spin_meeting_no, mtglist)
                    )
                    validate!!.SaveSharepreferenceInt(
                        VoSpData.vocurrentmeetingnumber,
                        validate!!.returnvoMeetingNum(spin_meeting_no, mtglist)
                    )

                    tv_date.text = validate!!.convertDatetime(
                        validate!!.RetriveSharepreferenceLong(
                            VoSpData.voCurrentMtgDate
                        )
                    )

                    btn_delete.isEnabled =
                        validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber) >= validate!!.RetriveSharepreferenceInt(
                            VoSpData.vomaxmeetingnumber
                        )
                    setValues()
                } else {
                    et_meeting_date.setText("")
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }

        validate!!.fillVomeetingspinner(this, spin_meeting_no, mtglist)

        spin_meeting_no.setSelection(
            validate!!.returnvomeetingpos(
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                mtglist
            )
        )

        lay_shg_attendance.setOnClickListener {
            var intent = Intent(this, VoSHGAttendanceActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_ec_attendance.setOnClickListener {
            var intent = Intent(this, Ec_MemberAttendanceActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_shg_vo.setOnClickListener {
            validate!!.SaveSharepreferenceInt(VoSpData.voFormType, 1)
            validate!!.SaveSharepreferenceInt(VoSpData.voOrgType, 1)
            validate!!.SaveSharepreferenceInt(VoSpData.LoanSource, 3)
            var intent = Intent(this, SHGtoVOReceipts::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_clfTo_vo.setOnClickListener {
            validate!!.SaveSharepreferenceInt(VoSpData.voFormType, 2)
            validate!!.SaveSharepreferenceInt(VoSpData.voOrgType, 2)
            validate!!.SaveSharepreferenceInt(VoSpData.LoanSource, 4)
            var intent = Intent(this, CLFtoVOReceipts::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_srmTo_vo.setOnClickListener {
            validate!!.SaveSharepreferenceInt(VoSpData.voFormType, 3)
            validate!!.SaveSharepreferenceInt(VoSpData.voOrgType, 3)
            validate!!.SaveSharepreferenceInt(VoSpData.LoanSource, 5)
            var intent = Intent(this, CLFtoVOReceipts::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        lay_otherTo_vo.setOnClickListener {
            validate!!.SaveSharepreferenceInt(VoSpData.voFormType, 4)
            validate!!.SaveSharepreferenceInt(VoSpData.voOrgType, 4)
            validate!!.SaveSharepreferenceInt(VoSpData.LoanSource, 99)
            var intent = Intent(this, CLFtoVOReceipts::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        btnVoToSHG.setOnClickListener {
            validate!!.SaveSharepreferenceInt(VoSpData.voOrgType, 1)
            validate!!.SaveSharepreferenceInt(VoSpData.voFormType, 5)
            validate!!.SaveSharepreferenceInt(VoSpData.LoanSource, 3)
            var intent = Intent(this, SHGtoVOReceipts::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tblVotoClf.setOnClickListener {
            validate!!.SaveSharepreferenceInt(VoSpData.voFormType, 6)
            validate!!.SaveSharepreferenceInt(VoSpData.voOrgType, 2)
            validate!!.SaveSharepreferenceInt(VoSpData.LoanSource, 4)
            var intent = Intent(this, CLFtoVOReceipts::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tblVotoOther.setOnClickListener {
            validate!!.SaveSharepreferenceInt(VoSpData.voFormType, 7)
            validate!!.SaveSharepreferenceInt(VoSpData.voOrgType, 4)
            validate!!.SaveSharepreferenceInt(VoSpData.LoanSource, 99)
            var intent = Intent(this, CLFtoVOReceipts::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tblReceiptAndIncome.setOnClickListener {
            var intent = Intent(this, ReceiptsAndIncomeSummary::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tbl_expenditure_and_payment.setOnClickListener {
            var intent = Intent(this, ExpenditureAndPaymentSummary::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tblBankSummery.setOnClickListener {
            var intent = Intent(this, BankSummary::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tblCashBox.setOnClickListener {
            var intent = Intent(this, CashBox::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tblTxnnSummerySHGAndVo.setOnClickListener {
            var intent = Intent(this, SHGVoSHGTransactionSummary::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        tblTxnPaymentSummeryAll.setOnClickListener {
            var intent = Intent(this, TransactionSummaryAll::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        btn_close.setOnClickListener {

            if (validate!!.RetriveSharepreferenceInt(VoSpData.voCashinhand) >= 0) {
                createVouchers(
                    validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                    validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
                )
                CustomAlertCloseMeeting(
                    LabelSet.getText(
                        "are_u_sure_u_want_to_close",
                        R.string.are_u_sure_u_want_to_close
                    )
                )
            } else {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "verify_cash_in_hand",
                        R.string.verify_cash_in_hand
                    ), this
                )
            }

        }

        btn_delete.setOnClickListener {
            CustomAlertDeleteMeeting(
                LabelSet.getText(
                    "want_to_delete_meeting",
                    R.string.want_to_delete_meeting
                )
            )
        }

        setLabelText()
        setValues()

        ll_speaker.visibility = View.VISIBLE

        ll_speaker.setOnClickListener {
            var vo_name = tv_name.text.toString()
            var MeetingDate = tv_date.text.toString()

            var receipt_shg = tv_amount1.text.toString()
            var receipt_clf = tv_amount2.text.toString()
            var receipt_srlm = tv_amount3.text.toString()
            var receipt_other = tv_amount4.text.toString()
            var payment_shg = tv_amount5.text.toString()
            var payment_clf = tv_amount6.text.toString()
            var payment_other = tv_amount7.text.toString()

            var attendance = tv_ec_member_attendence_total.text.toString()
            var attendancedata = attendance.split("/")
            var presentMember = attendancedata[0]
            var totalMember = attendancedata[1]


            var intent = Intent(this, VoRegularMeetingTTSActivity::class.java)
            intent.putExtra("SHG_name", vo_name)
            intent.putExtra("MeetingDate", MeetingDate)
            intent.putExtra("totalMember", totalMember)
            intent.putExtra("presentMember", presentMember)
            intent.putExtra("receipt_shg", receipt_shg)
            intent.putExtra("receipt_clf", receipt_clf)
            intent.putExtra("receipt_srlm", receipt_srlm)
            intent.putExtra("receipt_other", receipt_other)
            intent.putExtra("payment_shg", payment_shg)
            intent.putExtra("payment_clf", payment_clf)
            intent.putExtra("payment_other", payment_other)
            startActivity(intent)
        }
    }

    fun CustomAlertCloseMeeting(str: String) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str

        mDialogView.btn_yes.background = resources.getDrawable(R.drawable.button_bg_vomeeting)
        mDialogView.btn_no.background = resources.getDrawable(R.drawable.button_bg_vomeeting)
        mDialogView.btn_yes.setOnClickListener {
// PartialSaveData()

            generateMeetingViewmodel.closingMeeting(
                "C",
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                validate!!.RetriveSharepreferenceString(
                    AppSP.userid
                )!!
            )
            mAlertDialog.dismiss()
            var intent = Intent(this, VoMeetingListActivity1::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)


        }
        mDialogView.btn_no.setOnClickListener {
            mAlertDialog.dismiss()

        }
    }

    private fun setLabelText() {
        tv_meetingNo.text = LabelSet.getText("meeting_no", R.string.meeting_no)
        tvMeetingDate.text = LabelSet.getText("meeting_date", R.string.meeting_date)
        tv_shg_attendence.text = LabelSet.getText("shg_attendance", R.string.shg_attendance)
        tv_ecmember_attendance.text = LabelSet.getText(
            "ec_member_attendance",
            R.string.ec_member_attendance
        )
        tv_VoReceipt.text = LabelSet.getText("vo_receipts", R.string.vo_receipts)
        tv_shg_to_vo.text = LabelSet.getText("shg_to_vo", R.string.shg_to_vo)
        tv_clf_to_vo.text = LabelSet.getText("clf_to_vo", R.string.clf_to_vo)
        tv_srlm_to_vo.text = LabelSet.getText("srlm_to_vo", R.string.srlm_to_vo)
        tv_others_to_vo.text = LabelSet.getText("others_to_vo", R.string.others_to_vo)
        tvTotal.text = LabelSet.getText("total", R.string.total)
        tv_voPayment.text = LabelSet.getText("vo_payments", R.string.vo_payments)
        tv_vo_to_shg.text = LabelSet.getText("vo__to_shg", R.string.vo__to_shg)
        tv_vo_to_clf.text = LabelSet.getText("vo_to_clf", R.string.vo_to_clf)
        tv_vo_to_others.text = LabelSet.getText("vo__to_others", R.string.vo__to_others)
        tv_total1.text = LabelSet.getText("total", R.string.total)
        tv_receipt_and_income.text = LabelSet.getText(
            "recepient_and_income1",
            R.string.recepient_and_income1
        )
        tv_expenditure_and_payment.text = LabelSet.getText(
            "expenditure_and_payment",
            R.string.expenditure_and_payment
        )
        tv_transaction_summary.text = LabelSet.getText(
            "transaction_summary_n_between_shg_and_vo",
            R.string.transaction_summary_n_between_shg_and_vo
        )
        tv_transaction_summary_all.text = LabelSet.getText(
            "transaction_summary_n_all",
            R.string.transaction_summary_n_all
        )
        tv_bank.text = LabelSet.getText("bank", R.string.bank)
        tv_cashBox.text = LabelSet.getText("cashBox", R.string.cashBox)
        btn_close.text = LabelSet.getText("close_meeting", R.string.close_meeting)
        btn_delete.text = LabelSet.getText("delete_meeting", R.string.delete_meeting)
    }

    fun CustomAlertDeleteMeeting(str: String) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str
        mDialogView.btn_yes.background = resources.getDrawable(R.drawable.button_bg_vomeeting)
        mDialogView.btn_no.background = resources.getDrawable(R.drawable.button_bg_vomeeting)
        mDialogView.btn_yes.setOnClickListener {
            mAlertDialog.dismiss()

            DeleteMeeting()
            /*var intent = Intent(this, VoMeetingListActivity1::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)*/

        }
        mDialogView.btn_no.setOnClickListener {
            mAlertDialog.dismiss()

        }
    }

    fun DeleteMeeting() {

        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText("deleting_meeting_data", R.string.deleting_meeting_data)
        )

        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
        val token = validate!!.returnStringValue(
            AESEncryption.decrypt(
                validate!!.RetriveSharepreferenceString(AppSP.token),
                validate!!.RetriveSharepreferenceString(AppSP.userid)
            )
        )
        val cboid = validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        val mtgNum = validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
        val callCount = apiInterface?.deleteVOMeeting(
            token,
            user,
            cboid,
            mtgNum
        )

        callCount?.enqueue(object : Callback<MeetingResponse> {
            override fun onFailure(callCount: Call<MeetingResponse>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<MeetingResponse>,
                response: Response<MeetingResponse>
            ) {

                progressDialog.dismiss()
                if (response.isSuccessful) {

                    try {
                        var msg = response.body()?.result
                        if (msg!!.equals("Meeting delete successfully!!")) {
                            deleteMeetingData()
                        }


                        //       CustomAlert(text)
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        //                                    progressDialog.dismiss()
                    }

                } else {
                    progressDialog.dismiss()
                    var resCode = 0
                    var resMsg = ""
                    if (response.code() == 403) {

                        CustomAlertlogin()
                    } else {
                        if (response.code() == 500 || response.code() == 503) {
                            resCode = response.code()
                            resMsg = response.message()
                        } else {
                            var jsonObject1 =
                                JSONObject(response.errorBody()!!.source().readUtf8().toString())

                            resCode =
                                validate!!.returnIntegerValue(
                                    jsonObject1.optString("responseCode").toString()
                                )
                            resMsg = validate!!.returnStringValue(
                                jsonObject1.optString("responseMsg").toString()
                            )

                        }
                        if (resCode == 404 && resMsg.equals("record not found in database")) {

                            deleteMeetingData()

                        } else {
                            validate!!.CustomAlertMsg(
                                this@VoMeetingMenuActivity,
                                responseViewModel,
                                resCode,
                                validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                                resMsg
                            )
                        }
                    }

                }


            }

        })
    }
    fun CustomAlertlogin() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogelogin, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mDialogView.etUsername.setText(validate!!.RetriveSharepreferenceString(AppSP.userid))
        mDialogView.etPassword.setText(validate!!.returnStringValue(AESEncryption.decrypt(
            validate!!.RetriveSharepreferenceString(AppSP.Password),
            validate!!.RetriveSharepreferenceString(AppSP.userid)
        )))
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.btn_login.setBackgroundColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.txt_dialog_title2.text = LabelSet.getText("app_name",R.string.app_name)
        mDialogView.btn_login.text = LabelSet.getText("log_in",R.string.log_in)
        mDialogView.chk_showHide1.text = LabelSet.getText("show_password",R.string.show_password)
        mDialogView.btn_login.setOnClickListener {
            mAlertDialog.dismiss()
            importtoken(
                mDialogView.etUsername.text.toString(),
                validate!!.returnStringValue(
                    AESEncryption.encrypt(validate!!.returnStringValue(
                        mDialogView.etPassword.text.toString()),mDialogView.etUsername.text.toString()))
            )
        }
        mAlertDialog.chk_showHide1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                mAlertDialog.etPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                mAlertDialog.etPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
        }

    }

    fun importtoken(user: String, pass: String) {
        var grant_type = "password"
        var userId = user
        var password = pass
        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText(
                "authenticate_user",
                R.string.authenticate_user
            )
        )


        val callCount = apiInterface?.gettokenforce(
            grant_type,
            userId,
            password, validate!!.RetriveSharepreferenceInt(AppSP.State_Selected), 1,
            validate!!.RetriveSharepreferenceString(AppSP.Roleid)!!
        )
        callCount?.enqueue(object : Callback<JsonObject> {
            override fun onFailure(callCount: Call<JsonObject>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                progressDialog.dismiss()


                if (response.isSuccessful) {

                    var jsonObject1: JSONObject? = null
                    try {
                        jsonObject1 = JSONObject(response.body().toString())

                        val auth = jsonObject1.get("accessToken").toString()
                        if (auth.trim().length > 0) {
                            validate!!.SaveSharepreferenceString(
                                AppSP.token,
                                validate!!.returnStringValue(
                                    AESEncryption.encrypt(
                                        auth,validate!!.RetriveSharepreferenceString(AppSP.userid)))
                            )
                        }
                        validate!!.CustomAlertVO(
                            LabelSet.getText(
                                "userauthenticatedsuccessfully",
                                R.string.userauthenticatedsuccessfully
                            ), this@VoMeetingMenuActivity
                        )

                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Toast.makeText(this@VoMeetingMenuActivity, response.message(), Toast.LENGTH_LONG)
                            .show()


                    }

                } else {
                    Toast.makeText(this@VoMeetingMenuActivity, response.message(), Toast.LENGTH_LONG)
                        .show()
                }


            }

        })


    }
    fun deleteMeetingData() {
        /*Table1*/
        generateMeetingViewmodel.deleteVo_MtgData(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        /*Table2*/
        generateMeetingViewmodel.deleteVODetailData(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        /*Table3*/
        vofinancialTransactionsMemViewmodel!!.deleteFinancialTransactionMemberDetailData(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        /*Table4*/
        voFintxnDetGrpViewmodel!!.deleteGrpFinancialTxnDetailData(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        /*Table5*/
        vofinTxnViewmodel!!.deleteVOFinancialTxnData(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        /*Table6*/
        voLoanTxnMemViewmodel!!.deleteMemberLoanTxnData(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        /*Table7*/
        voLoanGpViewmodel!!.deleteGroupLoanData(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        /*Table8*/
        voLoanGPTxnViewmodel!!.deleteGroupLoanTxnData(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        /*Table9*/
        voLoanMemberViewmodel!!.deleteMemberLoan(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
        )
        /*Table10*/
        voLoanViewmodel!!.deleteVoLoanApplication(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        /*Table11*/
        vomemberLoanScheduleViewmodel!!.deleteMemberRepaidData(
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        /*Table 12*/
        vomemberLoanScheduleViewmodel!!.deleteMemberSubinstallmentData(
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        /*Table13*/
        voGrpLoanScheduleViewModel!!.deleteGroupRepaidData(
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        /*Table 14*/
        voGrpLoanScheduleViewModel!!.deleteGroupSubinstallmentData(
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
        var intent = Intent(this, VoMeetingListActivity1::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)

    }

    private fun setValues() {

        val rs_string = LabelSet.getText("", R.string.rs_sign)

        //shgAttendence
        setSumValue(
            tv_shg_attendence_total, ivShgAttendance,
            voMtgDetViewModel!!.getshgsumattendance(
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
            ), ""
        )
        //ecMember Attendance
        val totalPresent = voMtgDetViewModel!!.getECPresent(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        val totalshg = voMtgDetViewModel!!.getTotalPresentAbsent(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        val total =
            validate!!.returnStringValue(totalPresent.toString()) + "/" + validate!!.returnStringValue(
                totalshg.toString()
            )
        if (!total.isNullOrEmpty()) {
            tv_ec_member_attendence_total.text = total
            ivEcMember.visibility = View.VISIBLE
        } else {
            tv_ec_member_attendence_total.text = ""
            ivEcMember.visibility = View.GONE
        }

        /*  setSumValue(
              tv_ec_member_attendence_total, ivEcMember,
              0, ""
          )*/

        // shg to VO Receipt
        setSumValue(
            tv_amount1, ivShgToVo,
            vofinancialTransactionsMemViewmodel!!.getTotalFinTxnMemAmount(
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid), 1, listOf("RG", "RL", "RO")
            ) + generateMeetingViewmodel.gettotloanpaid(
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)

            ), rs_string
        )

        //clf to VoReceipt
        setSumValue(
            tv_amount2, ivClftoVo,
            voFintxnDetGrpViewmodel!!.getTotalFinTxnGrpAmount(
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid), 2, listOf("RG", "RL", "RO")
            ) + voLoanGpViewmodel!!.gettotamtbyloansource(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                4
            ), rs_string
        )

        //srlm to Vo Receipt
        setSumValue(
            tv_amount3, ivSrlmtoVo,
            voFintxnDetGrpViewmodel!!.getTotalFinTxnGrpAmount(
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid), 3, listOf("RG", "RL", "RO")
            ) + voLoanGpViewmodel!!.gettotamtbyloansource(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                5
            ), rs_string
        )

        //other to voReceipt
        setSumValue(
            tv_amount4, ivOthertoVo,
            voFintxnDetGrpViewmodel!!.getTotalFinTxnGrpAmount(
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid), 4, listOf("RG", "RL", "RO")
            ) + voLoanGpViewmodel!!.gettotamtbyloansource(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                99
            ), rs_string
        )
        //VO to SHGPayment
        setSumValue(
            tv_amount5, ivVotoshg,
            vofinancialTransactionsMemViewmodel!!.getTotalFinTxnMemAmount(
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid), 1, listOf("PG", "PL", "PO")
            ) + voLoanMemberViewmodel!!.gettotcboamt(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
            ), rs_string
        )

        //VO to CLFPayment
        setSumValue(
            tv_amount6, ivVotoclf,
            voFintxnDetGrpViewmodel!!.getTotalFinTxnGrpAmount(
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid), 2, listOf("PG", "PL", "PO")
            ) + generateMeetingViewmodel.getgrouploanpaidbycbo(
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                4
            ), rs_string
        )

        //Vo to OtherPayment
        setSumValue(
            tv_amount7, ivVotoother,
            voFintxnDetGrpViewmodel!!.getTotalFinTxnGrpAmount(
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid), 4, listOf("PG", "PL", "PO")
            ) + generateMeetingViewmodel.getgrouploanpaidbycbo(
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                99
            ), rs_string
        )

        //Bank
        setSumValue(
            tv_amount12, ivbank, generateMeetingViewmodel.gettotalinbank(
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
            ), rs_string
        )

        //cash
        setSumValue(
            tv_amount13,
            ivCash,
            validate!!.RetriveSharepreferenceInt(VoSpData.voCashinhand),
            rs_string
        )

        /*  //Transaction summery all
          setSumValue(
              tv_amount11,
              ivTxnsummeryAll,
              0,
              rs_string
          )

          //Transaction summery shg to vo
          setSumValue(
              tv_amount9,
              ivTxnSummerySHG,0,
              rs_string
          )
  */
    }

    fun setSumValue(textView: TextView, imageView: ImageView, value: Int, stringPrefix: String) {
        if (value != null && value > 0) {
            if (stringPrefix.trim().length > 0) {
                textView.text = stringPrefix + " " + validate!!.returnStringValue(value.toString())
            } else {
                textView.text = validate!!.returnStringValue(value.toString())
            }

            imageView.visibility = View.VISIBLE
        } else {
            textView.text = "0"
            imageView.visibility = View.GONE
        }
    }

    override fun onBackPressed() {
        var intent = Intent(this, VoMeetingListActivity1::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

    private fun createVouchers(cboId: Long, mtgNum: Int) {
        var shgMemberloanList = voLoanMemberViewmodel!!.getLoanDetail(cboId, mtgNum)
        var grpLoanlist = voLoanGpViewmodel!!.getListDatavoucher(mtgNum, cboId)
        var grpLoanTxnList = voLoanGPTxnViewmodel!!.getListDataByMtgnum(mtgNum, cboId)
        var memberLoanTxnList = voLoanTxnMemViewmodel!!.getListDataByMtgnum(mtgNum, cboId)
        val voFinanceTxnDetailGrpList =
            voFintxnDetGrpViewmodel!!.gettxnvoucherlist(cboId, mtgNum)
        var voFinanceTxnDetailmemList =
            vofinancialTransactionsMemViewmodel!!.gettxnvoucherlist(cboId, mtgNum)

        if (!shgMemberloanList.isNullOrEmpty()) {
            for (i in shgMemberloanList.indices) {
                /*validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType)== 0 && */
                if (validate!!.returnIntegerValue(shgMemberloanList.get(i).amount.toString()) > 0) {
                    var voucherNum = generateVoucherNum("PL")
                    var vouchersEntity = ShgFinancialTxnVouchersEntity(
                        cboId,
                        shgMemberloanList.get(i).memId,
                        mtgNum,
                        shgMemberloanList.get(i).modePayment,
                        shgMemberloanList.get(i).bankCode,
                        mtgNum,
                        shgMemberloanList.get(i).transactionNo,
                        shgMemberloanList.get(i).transactionNo,
                        shgMemberloanList.get(i).amount,
                        validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                        voucherNum,
                        2,
                        75,
                        0,
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        null
                    )
                    vouchersViewModel!!.insert(vouchersEntity)
                }
            }
        }

        if (!memberLoanTxnList.isNullOrEmpty()) {
            for (i in memberLoanTxnList.indices) {
                if (validate!!.returnIntegerValue(memberLoanTxnList.get(i).loanPaid.toString()) > 0 || validate!!.returnIntegerValue(
                        memberLoanTxnList.get(i).loanPaidInt.toString()
                    ) > 0
                ) {
                    var total_paid =
                        validate!!.returnIntegerValue(memberLoanTxnList.get(i).loanPaid.toString()) + validate!!.returnIntegerValue(
                            memberLoanTxnList.get(i).loanPaidInt.toString()
                        )
                    var voucherNum = generateVoucherNum("RL")
                    var vouchersEntity = ShgFinancialTxnVouchersEntity(
                        cboId,
                        memberLoanTxnList.get(i).memId,
                        mtgNum,
                        memberLoanTxnList.get(i).modePayment,
                        memberLoanTxnList.get(i).bankCode,
                        mtgNum,
                        memberLoanTxnList.get(i).transactionNo,
                        memberLoanTxnList.get(i).transactionNo,
                        total_paid,
                        validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                        voucherNum,
                        2,
                        70,
                        0,
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        null
                    )
                    vouchersViewModel!!.insert(vouchersEntity)
                }
            }
        }

        if (!grpLoanlist.isNullOrEmpty()) {
            for (i in grpLoanlist.indices) {
                /*validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType)== 0 && */
                if (validate!!.returnIntegerValue(grpLoanlist.get(i).amount.toString()) > 0) {
                    var voucherNum = generateVoucherNum("RL")
                    var vouchersEntity = ShgFinancialTxnVouchersEntity(
                        cboId,
                        0,
                        mtgNum,
                        grpLoanlist.get(i).modePayment,
                        grpLoanlist.get(i).bankCode,
                        mtgNum,
                        grpLoanlist.get(i).transactionNo,
                        grpLoanlist.get(i).transactionNo,
                        grpLoanlist.get(i).amount,
                        validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                        voucherNum,
                        1,
                        72,
                        0,
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        null
                    )
                    vouchersViewModel!!.insert(vouchersEntity)
                }
            }
        }
        if (!grpLoanTxnList.isNullOrEmpty()) {
            for (i in grpLoanTxnList.indices) {
                if (validate!!.returnIntegerValue(grpLoanTxnList.get(i).loanPaid.toString()) > 0 || validate!!.returnIntegerValue(
                        grpLoanTxnList.get(i).loanPaidInt.toString()
                    ) > 0
                ) {
                    var total_paid =
                        validate!!.returnIntegerValue(grpLoanTxnList.get(i).loanPaid.toString()) + validate!!.returnIntegerValue(
                            grpLoanTxnList.get(i).loanPaidInt.toString()
                        )
                    var voucherNum = generateVoucherNum("PL")
                    var vouchersEntity = ShgFinancialTxnVouchersEntity(
                        cboId,
                        0,
                        mtgNum,
                        grpLoanTxnList.get(i).modePayment,
                        grpLoanTxnList.get(i).bankCode,
                        mtgNum,
                        grpLoanTxnList.get(i).transactionNo,
                        grpLoanTxnList.get(i).transactionNo,
                        total_paid,
                        validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                        voucherNum,
                        1,
                        76,
                        0,
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        null
                    )
                    vouchersViewModel!!.insert(vouchersEntity)
                }
            }
        }
        if (!voFinanceTxnDetailGrpList.isNullOrEmpty()) {
            for (i in voFinanceTxnDetailGrpList.indices) {
                if (validate!!.returnIntegerValue(voFinanceTxnDetailGrpList.get(i).amount.toString()) > 0) {
                    var voucherNum =
                        generateVoucherNum(voFinanceTxnDetailGrpList.get(i).type.toString())
                    var vouchersEntity = ShgFinancialTxnVouchersEntity(
                        cboId,
                        0,
                        mtgNum,
                        voFinanceTxnDetailGrpList.get(i).modePayment,
                        voFinanceTxnDetailGrpList.get(i).bankCode,
                        mtgNum,
                        voFinanceTxnDetailGrpList.get(i).transactionNo,
                        voFinanceTxnDetailGrpList.get(i).transactionNo,
                        voFinanceTxnDetailGrpList.get(i).amount,
                        validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                        voucherNum,
                        1,
                        voFinanceTxnDetailGrpList.get(i).auid,
                        0,
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        null
                    )
                    vouchersViewModel!!.insert(vouchersEntity)
                }
            }
        }

        if (!voFinanceTxnDetailmemList.isNullOrEmpty()) {
            for (i in voFinanceTxnDetailmemList.indices) {
                if (validate!!.returnIntegerValue(voFinanceTxnDetailmemList.get(i).amount.toString()) > 0) {
                    var voucherNum =
                        generateVoucherNum(voFinanceTxnDetailmemList.get(i).type.toString())
                    var vouchersEntity = ShgFinancialTxnVouchersEntity(
                        cboId,
                        voFinanceTxnDetailmemList.get(i).memId,
                        mtgNum,
                        voFinanceTxnDetailmemList.get(i).modePayment,
                        voFinanceTxnDetailmemList.get(i).bankCode,
                        mtgNum,
                        voFinanceTxnDetailmemList.get(i).transactionNo,
                        voFinanceTxnDetailmemList.get(i).transactionNo,
                        voFinanceTxnDetailmemList.get(i).amount,
                        validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                        voucherNum,
                        1,
                        voFinanceTxnDetailmemList.get(i).auid,
                        0,
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        validate!!.Daybetweentime(validate!!.currentdatetime),
                        validate!!.RetriveSharepreferenceString(AppSP.userid),
                        null
                    )
                    vouchersViewModel!!.insert(vouchersEntity)
                }
            }
        }
    }

    fun generateVoucherNum(type: String): String {
        //validate!!.convertDatetime(validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate))
        var voucherNum: String? = null
        var num = validate!!.returnIntegerValue(
            vouchersViewModel!!.getMaxVoucherNum(
                validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate)
            ).toString()
        )
        var maxVoucherNum = (num + 1).toString()
        if (maxVoucherNum.trim().length == 1) {
            maxVoucherNum = "000" + maxVoucherNum
        } else if (maxVoucherNum.trim().length == 2) {
            maxVoucherNum = "00" + maxVoucherNum
        } else if (maxVoucherNum.trim().length == 3) {
            maxVoucherNum = "0" + maxVoucherNum
        }
        voucherNum =
            validate!!.convertDatetime(validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate)) + "-" + type + "-" + maxVoucherNum
        return voucherNum
    }
}