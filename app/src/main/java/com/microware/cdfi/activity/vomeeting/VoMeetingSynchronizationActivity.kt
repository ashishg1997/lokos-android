package com.microware.cdfi.activity.vomeeting

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.microware.cdfi.R
import com.microware.cdfi.activity.vo.VoDrawerActivity
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.api.meetingdatamodel.*
import com.microware.cdfi.api.vomeetingmodel.*
import com.microware.cdfi.entity.voentity.VomtgEntity
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.ResponseViewModel
import com.microware.cdfi.viewModel.voviewmodel.*
import kotlinx.android.synthetic.main.activity_vo_meeting_synchronization.*
import kotlinx.android.synthetic.main.customealertdialogelogin.*
import kotlinx.android.synthetic.main.customealertdialogelogin.view.*
import okhttp3.MediaType
import okhttp3.RequestBody
import org.apache.http.conn.ConnectTimeoutException
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException

class VoMeetingSynchronizationActivity : AppCompatActivity() {
    internal lateinit var progressDialog: ProgressDialog
    var  apiInterface: ApiInterface? = null
    var validate: Validate? = null

    var generateMeetingViewmodel: VoGenerateMeetingViewmodel? = null
    var voLoanApplicationViewmodel: VoLoanApplicationViewmodel? = null
    var voMemLoanViewModel: VoMemLoanViewModel? = null
    var voGroupLoanViewModel: VoGroupLoanViewModel? = null
    var responseViewModel: ResponseViewModel? = null
    var voGroupLoanScheduleViewModel: VoGroupLoanScheduleViewModel? = null
    var voMemLoanScheduleViewModel: VoMemLoanScheduleViewModel? = null

    var voMtgDetViewModel: VoMtgDetViewModel? = null
    var voFinTxnViewModel: VoFinTxnViewModel? = null
    var voFinTxnDetGrpViewModel: VoFinTxnDetGrpViewModel? = null
    var voGroupLoanTxnViewModel: VoGroupLoanTxnViewModel? = null
    var voMemLoanTxnViewModel: VoMemLoanTxnViewModel? = null
    var voFinTxnDetMemViewModel: VoFinTxnDetMemViewModel? = null
    var voMemSettlementViewmodel: VoMemSettlementViewmodel? = null

    var iDownload = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vo_meeting_synchronization)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        validate = Validate(this)

        generateMeetingViewmodel = ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        voLoanApplicationViewmodel = ViewModelProviders.of(this).get(VoLoanApplicationViewmodel::class.java)
        responseViewModel = ViewModelProviders.of(this).get(ResponseViewModel::class.java)
        voMemLoanViewModel = ViewModelProviders.of(this).get(VoMemLoanViewModel::class.java)
        voGroupLoanViewModel = ViewModelProviders.of(this).get(VoGroupLoanViewModel::class.java)
        voGroupLoanScheduleViewModel = ViewModelProviders.of(this).get(VoGroupLoanScheduleViewModel::class.java)
        voMemLoanScheduleViewModel = ViewModelProviders.of(this).get(VoMemLoanScheduleViewModel::class.java)
        voMtgDetViewModel = ViewModelProviders.of(this).get(VoMtgDetViewModel::class.java)
        voFinTxnViewModel = ViewModelProviders.of(this).get(VoFinTxnViewModel::class.java)
        voFinTxnDetGrpViewModel = ViewModelProviders.of(this).get(VoFinTxnDetGrpViewModel::class.java)
        voGroupLoanTxnViewModel = ViewModelProviders.of(this).get(VoGroupLoanTxnViewModel::class.java)
        voMemLoanTxnViewModel = ViewModelProviders.of(this).get(VoMemLoanTxnViewModel::class.java)
        voFinTxnDetMemViewModel = ViewModelProviders.of(this).get(VoFinTxnDetMemViewModel::class.java)
        voMemSettlementViewmodel = ViewModelProviders.of(this).get(VoMemSettlementViewmodel::class.java)


        val voMeetinglist = generateMeetingViewmodel!!.getVoMeetingList()

        tv_upload_meeting.text = LabelSet.getText(
            "upload_meeting",
            R.string.upload_meeting
        ) + " (" + voMeetinglist.size + ")"
        tbl_download.setOnClickListener {
            layout_download.visibility= View.VISIBLE
            layout_upload.visibility= View.GONE
            tbl_UploadMsg.visibility= View.GONE
            tbl_download.background=getDrawable(R.drawable.color_primary_bg)
            tbl_upload.background=getDrawable(R.drawable.light_gray_bg)
            img_download.setImageResource(R.drawable.ic_white_download)
            img_upload.setImageResource(R.drawable.ic_gray)
            tv_download.setTextColor(getColor(R.color.white))
            tv_upload.setTextColor(getColor(R.color.black))
            layout_upload.visibility= View.GONE

        }
        tbl_upload.setOnClickListener {
            layout_download.visibility= View.GONE
            layout_upload.visibility= View.VISIBLE
            tbl_UploadMsg.visibility= View.VISIBLE
            tbl_download.background=getDrawable(R.drawable.light_gray_bg)
            tbl_upload.background=getDrawable(R.drawable.color_primary_bg)
            img_download.setImageResource(R.drawable.ic_downloadgray)
            img_upload.setImageResource(R.drawable.ic_white)
            tv_download.setTextColor(getColor(R.color.black))
            tv_upload.setTextColor(getColor(R.color.white))

        }
        layout_upload_meeting.setOnClickListener {
            if (validate!!.isNetworkConnected(this)) {
                val voMeetinglistData = generateMeetingViewmodel!!.getVoMeetingList()
                if (!voMeetinglistData.isNullOrEmpty()) {
                      exportData(voMeetinglistData)
                } else {
                    val text = LabelSet.getText("nothing_upload", R.string.nothing_upload)
                    validate!!.CustomAlertVO(text, this)
                }
            }else{
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "no_internet_msg",
                        R.string.no_internet_msg
                    ), this
                )
            }
        }

    }

    fun exportData(voMeetinglist: List<VomtgEntity>){
        progressDialog = ProgressDialog.show(
            this, LabelSet.getText("app_name", R.string.app_name),
            LabelSet.getText("DataUploading", R.string.DataUploading)
        )
        progressDialog.setIcon(this.getDrawable(R.drawable.appicon))
        var iupload = 0
        var msg = ""
        var code = 0
        object : Thread() {

            override fun run() {
                try {
                    for (j in voMeetinglist.indices){

                        var voMeetinglistentity = voMeetinglist.get(j)

                        /*VoGroupLoan*/
                        val voGroupLoanList =
                            voGroupLoanViewModel!!.getUploadGroupLoanList(voMeetinglist[j].cboId)

                        var voGroupLoanListmodel: List<VoGroupLoanDataModel>? = null
                        voGroupLoanListmodel = MappingData.returnGroupLoandataObj(voGroupLoanList)

                        for (i in 0 until voGroupLoanList.size){
                            val groupLoan: VoGroupLoanDataModel = voGroupLoanListmodel.get(i)
                            /*VoGroupLoan Schedule*/
                            val voGroupLoanScheduleList =
                                voGroupLoanScheduleViewModel!!.getLoanScheduleDetail(
                                    voGroupLoanList[j].cboId,
                                    voGroupLoanList[j].loanNo)
                            groupLoan.voGroupLoanScheduleList = voGroupLoanScheduleList

                        }

                        /*VOMemLoan*/
                        val voMemLoanList =
                            voMemLoanViewModel!!.getuploadlist(voMeetinglist[j].cboId)

                        var voMemLoanListmodel: List<VoMemLoanDataModel>? = null
                        voMemLoanListmodel = MappingData.returnmemberLoandataObj(voMemLoanList)

                        for (i in 0 until voMemLoanList!!.size){
                            val memberLoan: VoMemLoanDataModel = voMemLoanListmodel.get(i)
                            /*VoMemberLoan Schedule*/
                            val vomemberLoanList =
                                voMemLoanScheduleViewModel!!.getLoanScheduleDetail(
                                    voGroupLoanList[j].cboId,
                                    voGroupLoanList[j].loanNo)
                            memberLoan.voMemberLoanScheduleList = vomemberLoanList

                        }

                        var voUploadModel: VoMtgDataModel = VoMtgDataModel()
                        voUploadModel = MappingData.returnmeetingdataObj(voMeetinglistentity)

                        val voLoanApplicationList =
                            voLoanApplicationViewmodel!!.getLoanApplicationlist(
                                voMeetinglist[j].mtgNo,
                                voMeetinglist[j].cboId)

                        var uploaddatalist = VoMeetingUploadData(
                            voUploadModel,voLoanApplicationList,voMemLoanListmodel,voGroupLoanListmodel
                        )

                        /*vo Financial TXN list*/
                        val voFinanceTransactionList = voFinTxnViewModel!!.getListData(
                            voMeetinglist[j].mtgNo,
                            voMeetinglist[j].cboId
                        )

                        /*vo Financial TXN Group detail list*/
                        val voFinanceTransactionDetailGroupList = voFinTxnDetGrpViewModel!!.getVoFinTxnDetGrpListData(
                            voMeetinglist[j].mtgNo,
                            voMeetinglist[j].cboId
                        )

                        /*Vo GroupLoan Txn List*/
                        val voGroupLoanTransactionList = voGroupLoanTxnViewModel!!.getListDataByMtgnum(
                            voMeetinglist[j].mtgNo,
                            voMeetinglist[j].cboId
                        )

                        /*Vo Meeting Detail List*/
                        val mtgDetailList = voMtgDetViewModel!!.getListDataMtgByMtgnum(
                            voMeetinglist[j].mtgNo,
                            voMeetinglist[j].cboId
                        )
                        var vomtgDetailList: List<VoMtgDetailDataModel>? = null
                        vomtgDetailList = MappingData.returnlvomtgDetaidataObj(mtgDetailList)

                        for (k in 0 until mtgDetailList.size){
                            val memberdetailLoan: VoMtgDetailDataModel = vomtgDetailList.get(k)
                            val voMemberLoanTransactionList =
                                voMemLoanTxnViewModel!!.getmeetingLoanTxnMemdata(
                                    mtgDetailList[j].cboId,
                                    mtgDetailList[j].memId,
                                    mtgDetailList[j].mtgNo)

                            val voFinanceTransactionDetailMemberList =
                                voFinTxnDetMemViewModel!!.getFinTxnMemData(
                                    mtgDetailList[j].cboId,
                                    mtgDetailList[j].memId,
                                    mtgDetailList[j].mtgNo)

                            val voMemberSettlementList =
                                voMemSettlementViewmodel!!.getSettlementuploaddata(
                                    mtgDetailList[j].cboId,
                                    mtgDetailList[j].memId,
                                    mtgDetailList[j].mtgNo
                                )

                            memberdetailLoan.voMemberLoanTransactionList = voMemberLoanTransactionList
                            memberdetailLoan.voFinanceTransactionDetailMemberList = voFinanceTransactionDetailMemberList
                            memberdetailLoan.voMemberSettlementList = voMemberSettlementList
                        }

                        voUploadModel.voMeetingDetailsList = vomtgDetailList
                        voUploadModel.voFinanceTransactionList = voFinanceTransactionList
                        voUploadModel.voFinanceTransactionDetailGroupList = voFinanceTransactionDetailGroupList
                        voUploadModel.voGroupLoanTransactionList = voGroupLoanTransactionList

                        var uploaddataModel: VoMeetingUploadData = VoMeetingUploadData()
                        uploaddataModel = MappingData.returnvomeetingObj(uploaddatalist)

                        var gsonbuilder = GsonBuilder()
                        gsonbuilder.serializeNulls()
                        val gson: Gson = gsonbuilder.setPrettyPrinting().create()
                        val json = gson.toJson(uploaddataModel)

                        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                        val token =  validate!!.returnStringValue(AESEncryption.decrypt(
                            validate!!.RetriveSharepreferenceString(AppSP.token),
                            validate!!.RetriveSharepreferenceString(AppSP.userid)))

                        val file = RequestBody.create(
                            MediaType.parse("application/json; charset=utf-8"),
                            json
                        )

                        val call1 = apiInterface!!.uploadVoMeetingData(
                            "application/json",
                            token,
                            user,
                            file
                        )

                        val res = call1.execute()
                        if (res!!.isSuccessful) {
                            iupload = 0
                            msg = res.body()?.response!!
                            generateMeetingViewmodel!!.updateMeetinguploadStatus(
                                voMeetinglist[j].cboId,
                                voMeetinglist[j].mtgNo,
                                validate!!.Daybetweentime(validate!!.currentdatetime))

                        }else{
                            msg = "" + res.code() + " " + res.message()
                            code = res.code()
                            iupload = 1
                        }

                    }

                    runOnUiThread {
                        if (iupload == 0) {
                            progressDialog.dismiss()
                            val text = msg
                            validate!!.CustomAlertVO(text, this@VoMeetingSynchronizationActivity)
                            var voPendingMeetinglist = generateMeetingViewmodel!!.getVoMeetingList()
                            tv_upload_meeting.text = LabelSet.getText(
                                "upload_meeting",
                                R.string.upload_meeting
                            ) + " (" + voPendingMeetinglist.size + ")"
                        } else if (iupload == -1) {
                            progressDialog.dismiss()
                            val text = LabelSet.getText("nothing_upload", R.string.nothing_upload)
                            validate!!.CustomAlertVO(text, this@VoMeetingSynchronizationActivity)
                            Toast.makeText(
                                this@VoMeetingSynchronizationActivity,
                                msg,
                                Toast.LENGTH_LONG
                            ).show()
                        } else {
                            progressDialog.dismiss()
                            if (code == 403) {
                                Toast.makeText(
                                    this@VoMeetingSynchronizationActivity,
                                    msg,
                                    Toast.LENGTH_LONG
                                ).show()
                                CustomAlertlogin()
                            } else {
                                validate!!.CustomAlertVO(msg, this@VoMeetingSynchronizationActivity)
                                Toast.makeText(
                                    this@VoMeetingSynchronizationActivity,
                                    msg,
                                    Toast.LENGTH_LONG
                                )
                                    .show()
                            }
                        }

                    }

                }catch (bug: SocketTimeoutException) {
                    progressDialog.dismiss()
//                                Toast.makeText(activity!!, "Socket Timeout", Toast.LENGTH_LONG).show();
                } catch (bug: ConnectTimeoutException) {
                    progressDialog.dismiss()
                    // Toast.makeText(getApplicationContext(), "Connection Timeout", Toast.LENGTH_LONG).show();
                } catch (e: Exception) {
                    e.printStackTrace()
                    progressDialog.dismiss()
                }
            }

        }.start()
    }

    fun importtoken(user: String, pass: String) {
        var grant_type = "password"
        var userId = user
        var password = pass
        progressDialog = ProgressDialog.show(
            this, "",
            LabelSet.getText("authenticate_user", R.string.authenticate_user)
        )
        val callCount = apiInterface?.gettokenforce(
            grant_type,
            userId,
            password, validate!!.RetriveSharepreferenceInt(AppSP.State_Selected), 1,
            validate!!.RetriveSharepreferenceString(AppSP.Roleid)!!)
        callCount?.enqueue(object : Callback<JsonObject> {
            override fun onFailure(callCount: Call<JsonObject>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                //       Log.e("", response!!.body().toString())
                progressDialog.dismiss()


                if (response.isSuccessful) {

                    var jsonObject1: JSONObject? = null
                    try {
                        jsonObject1 = JSONObject(response.body().toString())

                        val auth = jsonObject1.get("accessToken").toString()
                        if (auth.trim().length > 0) {
                            validate!!.SaveSharepreferenceString(
                                AppSP.token,
                                validate!!.returnStringValue(
                                    AESEncryption.encrypt(
                                        auth,validate!!.RetriveSharepreferenceString(AppSP.userid)))
                            )
                        }
                        validate!!.CustomAlertVO(
                            LabelSet.getText(
                                "userauthenticatedsuccessfully",
                                R.string.userauthenticatedsuccessfully
                            ), this@VoMeetingSynchronizationActivity
                        )
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        var jsonObject1 =
                            JSONObject(response.errorBody()!!.source().readUtf8().toString())

                        var resCode =
                            validate!!.returnIntegerValue(
                                jsonObject1.optString("responseCode").toString()
                            )
                        validate!!.CustomAlertMsgVO(
                            this@VoMeetingSynchronizationActivity,
                            responseViewModel,
                            resCode,
                            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                            validate!!.returnStringValue(
                                jsonObject1.optString("responseMsg").toString()
                            )
                        )
                    }

                } else {
                    var jsonObject1 =
                        JSONObject(response.errorBody()!!.source().readUtf8().toString())

                    var resCode =
                        validate!!.returnIntegerValue(
                            jsonObject1.optString("responseCode").toString()
                        )
                    validate!!.CustomAlertMsgVO(
                        this@VoMeetingSynchronizationActivity,
                        responseViewModel,
                        resCode,
                        validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!,
                        validate!!.returnStringValue(
                            jsonObject1.optString("responseMsg").toString()
                        )
                    )
                }


            }

        })


    }

    fun CustomAlertlogin() {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogelogin, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        val password =  validate!!.returnStringValue(
            AESEncryption.decrypt(
            validate!!.RetriveSharepreferenceString(AppSP.Password),
            validate!!.RetriveSharepreferenceString(AppSP.userid)))
        mDialogView.etUsername.setText(validate!!.RetriveSharepreferenceString(AppSP.userid))
        mDialogView.etPassword.setText(password)
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_dialog_title2.text = LabelSet.getText("app_name",R.string.app_name)
        mDialogView.btn_login.text = LabelSet.getText("log_in",R.string.log_in)
        mDialogView.btn_login.setBackgroundColor(resources.getColor(R.color.colorPrimary1))
        mDialogView.chk_showHide1.text = LabelSet.getText("show_password",R.string.show_password)
        mDialogView.btn_login.setOnClickListener {
            mAlertDialog.dismiss()
            importtoken(
                mDialogView.etUsername.text.toString(),
                validate!!.returnStringValue(
                    AESEncryption.encrypt(validate!!.returnStringValue(
                        mDialogView.etPassword.text.toString()),mDialogView.etUsername.text.toString()))
            )
        }
        mAlertDialog.chk_showHide1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                mAlertDialog.etPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                mAlertDialog.etPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
        }

    }

    override fun onBackPressed() {
        val intent = Intent(this,VoDrawerActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }
}