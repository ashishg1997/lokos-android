package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.api.meetingmodel.FundTypeModel
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.entity.voentity.VoMemLoanEntity
import com.microware.cdfi.entity.voentity.VoMemLoanScheduleEntity
import com.microware.cdfi.entity.voentity.VoMemLoanTxnEntity
import com.microware.cdfi.fragment.VoReceiptsTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.LoanProductViewModel
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.voviewmodel.*
import kotlinx.android.synthetic.main.buttons_vo.*
import kotlinx.android.synthetic.main.customealertdialogepartial.view.*
import kotlinx.android.synthetic.main.loan_repayment_vo_to_shg.*

class VoMemberLoanReschedule : AppCompatActivity() {
    var validate: Validate? = null
    var voMemLoanEntity: VoMemLoanEntity? = null
    var voMemLoanTxnEntity: VoMemLoanTxnEntity? = null
    var voMemLoanScheduleEntity: VoMemLoanScheduleEntity? = null
    lateinit var voMemLoanViewModel: VoMemLoanViewModel
    lateinit var voLoanApplicationViewmodel: VoLoanApplicationViewmodel
    lateinit var voMemLoanTxnViewModel: VoMemLoanTxnViewModel
    lateinit var voMemLoanScheduleViewModel: VoMemLoanScheduleViewModel
    lateinit var lookupViewmodel: LookupViewmodel
    var dataspin_repayment_frequency: List<LookupEntity>? = null
    var dataspin_modeofpayment: List<LookupEntity>? = null
    var voBankList: List<Cbo_bankEntity>? = null
    var voGenerateMeetingViewmodel: VoGenerateMeetingViewmodel? = null
    var loanProductViewmodel: LoanProductViewModel? = null
    var voFinTxnViewModel: VoFinTxnViewModel? = null
    var dataspin_fund: List<FundTypeModel>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.vomemberloanreschedule)
        btn_save.text = LabelSet.getText("confirm", R.string.confirm)
        validate = Validate(this)
        voMemLoanViewModel = ViewModelProviders.of(this).get(VoMemLoanViewModel::class.java)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        voMemLoanTxnViewModel = ViewModelProviders.of(this).get(VoMemLoanTxnViewModel::class.java)
        voMemLoanScheduleViewModel =
            ViewModelProviders.of(this).get(VoMemLoanScheduleViewModel::class.java)
        voLoanApplicationViewmodel =
            ViewModelProviders.of(this).get(VoLoanApplicationViewmodel::class.java)
        voGenerateMeetingViewmodel =
            ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        loanProductViewmodel = ViewModelProviders.of(this).get(LoanProductViewModel::class.java)
        voFinTxnViewModel = ViewModelProviders.of(this).get(VoFinTxnViewModel::class.java)

        replaceFragmenty(
            fragment = VoReceiptsTopBarFragment(35),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        setLabel()
        fillbank()
        fillSpinner()
        showData()

        spin_modePayment?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                if (validate!!.returnlookupcode(
                        spin_modePayment,
                        dataspin_modeofpayment
                    ) == 2
                ) {
                    lay_bankSource.visibility = View.VISIBLE
                    lay_bankView.visibility = View.VISIBLE
                    lay_chequeNO.visibility = View.VISIBLE
                } else {
                    lay_bankSource.visibility = View.GONE
                    lay_bankView.visibility = View.GONE
                    lay_chequeNO.visibility = View.GONE
                    spin_bank.setSelection(0)

                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }
        /*spin_fund_type?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>, view: View?,
                position: Int, id: Long
            ) {

                fillfunddata(position)

            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }
*/
        et_sanction_date.setOnClickListener {
            validate!!.datePicker(et_sanction_date)
        }

        et_date_of_amount_paid.setOnClickListener {
            validate!!.datePicker(et_date_of_amount_paid)
        }

        et_loan_tenure.filters = arrayOf(InputFilterMinMax(1, 60))

        et_loan_tenure.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = validate!!.returnIntegerValue(et_amount_paid.text.toString())
                var loantenure = validate!!.returnIntegerValue(et_loan_tenure.text.toString())
                if (loantenure > 0 && value > 0) {
                    val installmentamt = value / loantenure
                    et_installment_amount.setText(installmentamt.toString())
                }

            }

        })

        et_amount_paid.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value = validate!!.returnIntegerValue(et_amount_paid.text.toString())
                var loantenure = validate!!.returnIntegerValue(et_loan_tenure.text.toString())
                if (loantenure > 0 && value > 0) {
                    val installmentamt = value / loantenure
                    et_installment_amount.setText(installmentamt.toString())
                }

            }

        })

        btn_cancel.setOnClickListener {
            var intent = Intent(this, VOtoSHGLoanSummary::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        btn_save.setOnClickListener {

            if (checkValidation() == 1) {
                /*saveData(validate!!.returnIntegerValue(et_moratorium_period.text.toString()))
                insertScheduler(
                    (validate!!.returnIntegerValue(et_amount_paid.text.toString())),
                    validate!!.returnIntegerValue(et_loan_tenure.text.toString()),
                    validate!!.returnIntegerValue(et_moratorium_period.text.toString())
                )*/
            }

        }

        fillData()
       // spin_fund_type.isEnabled=false

    }

    fun fillfunddata() {

            if (!dataspin_fund.isNullOrEmpty() ) {
                et_interest_rate.setText(validate!!.returnStringValue(dataspin_fund!![0].interestDefault.toString()))
                et_loan_tenure.setText(validate!!.returnStringValue(dataspin_fund!![0].defaultPeriod.toString()))

            } else {
                et_interest_rate.setText("0")
                et_loan_tenure.setText("0")

            }


    }
    fun setminmaxlimit() {
        if (!dataspin_fund.isNullOrEmpty()) {
            var maxamt = validate!!.returnIntegerValue(dataspin_fund!![0].maxAmount.toString())
            var maxperiod =
                validate!!.returnIntegerValue(dataspin_fund!![0].maxPeriod.toString())
            var maxint = validate!!.returnDoubleValue(dataspin_fund!![0].maxInterest.toString())
            /*if(maxint>0.0) {
                et_interest_rate.filters = arrayOf<InputFilter>(DecimalDigitsInputFilter(3, 2))
            }*/
            if (maxamt > 0.0) {
                et_amount_paid.filters = arrayOf(InputFilterMinMax(0, maxamt))
            }
            if (maxperiod > 0.0) {
                et_loan_tenure.filters = arrayOf(InputFilterMinMax(0, maxperiod))
            }
        }


    }
    fun fillbank() {
        voBankList = voGenerateMeetingViewmodel!!.getBankdata(
            validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID)
        )

        val adapter: ArrayAdapter<String?>
        if (!voBankList.isNullOrEmpty()) {
            val isize = voBankList!!.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in voBankList!!.indices) {
                var lastthree =
                    voBankList!![i].account_no.substring(voBankList!![i].account_no.length - 3)
                sValue[i + 1] =
                    voBankList!![i].ifsc_code.toString().dropLast(7) + "XXXXX" + lastthree
            }

            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_bank.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                this,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin_bank.adapter = adapter
        }

    }

    fun returaccount(): String {

        var pos = spin_bank.selectedItemPosition
        var id = ""

        if (!voBankList.isNullOrEmpty()) {
            if (pos > 0) id = voBankList!!.get(pos - 1).ifsc_code!!.dropLast(7) + voBankList!!.get(
                pos - 1
            ).account_no
        }
        return id
    }

    fun setaccount(accountno: String): Int {

        var pos = 0

        if (!voBankList.isNullOrEmpty()) {
            for (i in voBankList!!.indices) {
                if (accountno.equals(
                        voBankList!!.get(i).ifsc_code!!.dropLast(7) + voBankList!!.get(
                            i
                        ).account_no
                    )
                )
                    pos = i + 1
            }
        }
        return pos
    }

    private fun setLabel() {
        btn_save.text = LabelSet.getText("confirm", R.string.confirm)
        btn_cancel.text = LabelSet.getText("cancel", R.string.cancel)
        tv_fund_type.text = LabelSet.getText("fund_type", R.string.fund_type)
        tv_loan_no.text = LabelSet.getText("loan_no", R.string.loan_no)
        tv_sanction_amount.text = LabelSet.getText("sanction_amount", R.string.sanction_amount)
        tv_sanction_date.text = LabelSet.getText("sanction_date", R.string.sanction_date)
        tv_amount_paid.text = LabelSet.getText("amount_paid", R.string.amount_paid)
        tv_date_of_amount_paid.text = LabelSet.getText(
            "date_of_amount_paid",
            R.string.date_of_amount_paid
        )
        tv_interest_rate.text = LabelSet.getText("interest_rate", R.string.interest_rate)
        tv_loan_tenure.text = LabelSet.getText("loan_tenure", R.string.loan_tenure)
        tv_loan_repayment_frequency.text = LabelSet.getText(
            "loan_repayment_frequency",
            R.string.loan_repayment_frequency
        )
        tv_installment_amount.text = LabelSet.getText(
            "installment_amount_principal",
            R.string.installment_amount_principal
        )
        tv_moratorium_period.text = LabelSet.getText(
            "moratorium_in_months",
            R.string.moratorium_in_months
        )
        et_loan_no.hint = LabelSet.getText("auto", R.string.auto)
        et_sanction_amount.hint = LabelSet.getText("auto", R.string.auto)
        et_sanction_date.hint = LabelSet.getText("date_format", R.string.date_format)
        et_amount_paid.hint = LabelSet.getText("enter_amount", R.string.enter_amount)
        et_date_of_amount_paid.hint = LabelSet.getText("date_format", R.string.date_format)
        et_interest_rate.hint = LabelSet.getText("type_here", R.string.type_here)
        et_loan_tenure.hint = LabelSet.getText("type_here", R.string.type_here)
        et_installment_amount.hint = LabelSet.getText("type", R.string.type)
        et_moratorium_period.hint = LabelSet.getText(
            "enter_not_more_than_permissible_limit",
            R.string.enter_not_more_than_permissible_limit
        )

        tv_modePayment.text = LabelSet.getText("mode_of_payment", R.string.mode_of_payment)
        tv_bankSource.text = LabelSet.getText("source_bank", R.string.source_bank)
        tv_cheque_no_transactio_no.text =
            LabelSet.getText("cheque_no_transactio_no", R.string.cheque_no_transactio_no)
        et_cheque_no_transactio_no.hint = LabelSet.getText("type_here", R.string.type_here)
    }

    private fun checkValidation(): Int {

        var value = 1
        if (spin_fund_type.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_fund_type,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "fund_type",
                    R.string.fund_type
                )

            )
            value = 0
            return value
        }
        if (validate!!.returnIntegerValue(et_sanction_amount.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "sanction_amount",
                    R.string.sanction_amount
                ), this,
                et_sanction_amount
            )
            value = 0
            return value
        }

        if (validate!!.returnlookupcode(
                spin_modePayment,
                dataspin_modeofpayment
            ) == 1 && validate!!.returnIntegerValue(et_amount_paid.text.toString()) > validate!!.RetriveSharepreferenceInt(
                VoSpData.voCashinhand
            )
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "inthecashbox",
                    R.string.inthecashbox
                ), this,
                et_amount_paid
            )
            value = 0
            return value
        }
        var bankamt = voFinTxnViewModel!!.gettotalclosinginbank(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            returaccount()
        )
        if ((validate!!.returnlookupcode(
                spin_modePayment,
                dataspin_modeofpayment
            ) == 2 || validate!!.returnlookupcode(
                spin_modePayment,
                dataspin_modeofpayment
            ) == 3) && validate!!.returnIntegerValue(et_amount_paid.text.toString()) > bankamt
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "intheselectedbank",
                    R.string.intheselectedbank
                ), this,
                et_amount_paid
            )
            value = 0
            return value
        }

        if (spin_modePayment.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_modePayment,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "mode_of_payment",
                    R.string.mode_of_payment
                )

            )
            value = 0
            return value
        }
        if (lay_bankSource.visibility == View.VISIBLE && spin_bank.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_bank,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "source_bank",
                    R.string.source_bank
                )

            )
            value = 0
            return value
        }

        if (lay_chequeNO.visibility == View.VISIBLE && validate!!.returnStringValue(
                et_cheque_no_transactio_no.text.toString()
            ).length == 0
        ) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + LabelSet.getText(
                    "cheque_no_transactio_no",
                    R.string.cheque_no_transactio_no
                ), this,
                et_cheque_no_transactio_no
            )
            value = 0
            return value
        }

        if (validate!!.returnStringValue(et_sanction_date.text.toString()).length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + LabelSet.getText(
                    "sanction_date",
                    R.string.sanction_date
                ), this,
                et_sanction_date
            )

            value = 0
            return value
        }

        if (validate!!.returnStringValue(et_amount_paid.text.toString()).length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + LabelSet.getText(
                    "amount_paid",
                    R.string.amount_paid
                ), this,
                et_amount_paid
            )

            value = 0
            return value
        }

        if (validate!!.returnStringValue(et_date_of_amount_paid.text.toString()).length == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + LabelSet.getText(
                    "date_of_amount_paid",
                    R.string.date_of_amount_paid
                ), this,
                et_date_of_amount_paid
            )

            value = 0
            return value
        }

        if (validate!!.returnDoubleValue(et_interest_rate.text.toString()) == 0.0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + LabelSet.getText(
                    "interest_rate",
                    R.string.interest_rate
                ), this,
                et_interest_rate
            )

            value = 0
            return value
        }


        if (validate!!.returnDoubleValue(et_loan_tenure.text.toString()) == 0.0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + LabelSet.getText(
                    "loan_tenure",
                    R.string.loan_tenure
                ), this,
                et_loan_tenure
            )

            value = 0
            return value
        }

        if (spin_loan_repayment_frequency.selectedItemPosition == 0) {
            validate!!.CustomAlertSpinner(
                this, spin_loan_repayment_frequency,
                LabelSet.getText(
                    "please_select",
                    R.string.please_select
                ) + " " + LabelSet.getText(
                    "loan_repayment_frequency",
                    R.string.loan_repayment_frequency
                )

            )
            value = 0
            return value
        }

        if (validate!!.returnIntegerValue(et_installment_amount.text.toString()) == 0) {
            validate!!.CustomAlertEditText(
                LabelSet.getText(
                    "please_enter",
                    R.string.please_enter
                ) + " " + LabelSet.getText(
                    "installment_amount",
                    R.string.installment_amount
                ), this,
                et_installment_amount
            )
            value = 0
            return value
        }

        return value
    }

    /*private fun saveData(morotrum: Int) {

        var loanappid =
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid) + 1000 + validate!!.returnIntegerValue(
                et_loan_no.text.toString()
            ) + 1

        // et_sanction_date , et_installment_amount needs to calculated
        // et_sanction_amount
        // et_sanction date to be auto calculated

        voMemLoanEntity = VoMemLoanEntity(
            0,
            0,
            validate!!.RetriveSharepreferenceLong(VoSpData.voLoanappid),
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceLong(VoSpData.voShgMemID),
            validate!!.returnIntegerValue(et_loan_no.text.toString()),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            0,
            validate!!.returnIntegerValue(et_sanction_amount.text.toString()),
            validate!!.returnIntegerValue(et_amount_paid.text.toString()),
            0,//loan_purpose
            validate!!.returnFundTypeId(spin_fund_type, dataspin_fund),*//*fund type*//*
            validate!!.returnDoubleValue(et_interest_rate.text.toString()),
            validate!!.returnIntegerValue(et_loan_tenure.text.toString()),
            0,
            0,
            false, //completionFlag
            0,
            validate!!.RetriveSharepreferenceInt(VoSpData.LoanSource),
            validate!!.returnlookupcode(spin_modePayment, dataspin_modeofpayment),
            returaccount(), //bankCode
            et_cheque_no_transactio_no.text.toString(),
            validate!!.returnlookupcode(
                spin_loan_repayment_frequency,
                dataspin_repayment_frequency
            ),
            morotrum, //morotorium
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            "",
            0,
            "",
            0,
            0,
            0,
            validate!!.Daybetweentime(et_date_of_amount_paid.text.toString()),
            validate!!.Daybetweentime(et_sanction_date.text.toString()),0,0.0,0,1,1
        )
        voMemLoanViewModel.insertVoMemLoan(voMemLoanEntity!!)

        voMemLoanTxnEntity = VoMemLoanTxnEntity(
            0,  //uid
            0,  //voMtgDetUid
            0,  //vomemloanuid
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceLong(VoSpData.voShgMemID),  //memId
            validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
            validate!!.returnIntegerValue(et_amount_paid.text.toString()),
            0,   //loanOpInt
            0,
            0,
            validate!!.returnIntegerValue(et_amount_paid.text.toString()),  //loancl
            0,
            false, //completionFlag
            0, //intAccruedOp
            0, //intAccrued
            0, //intAccruedCl
            0, //principalDemandOb
            0,
            0,
            validate!!.returnlookupcode(spin_modePayment, dataspin_modeofpayment),
            returaccount(), //bankCode
            et_cheque_no_transactio_no.text.toString(),  //transanctionNo
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime),
            "",
            0,
            "",
            0,
            validate!!.returnIntegerValue(et_loan_no.text.toString()),0.0,0
        )
        voMemLoanTxnViewModel!!.insert(voMemLoanTxnEntity!!)
        if (validate!!.RetriveSharepreferenceInt(VoSpData.voMeetingType) == 0) {
            voLoanApplicationViewmodel!!.updatevoshgloanapplication(
                validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
                validate!!.RetriveSharepreferenceLong(VoSpData.voShgMemID),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voLoanappid),
                validate!!.returnIntegerValue(et_amount_paid.text.toString())

            )

        }
    }

    private fun insertScheduler(amt: Int, installment: Int, morotrum: Int) {
        var principaldemand = amt / installment
        var loanos = 0
        var saveValue = 0

        //   var memId = returnmemid()

        for (i in 0 until installment) {
            if (i == installment - 1) {
                loanos = 0
                principaldemand = amt - (principaldemand * i)
            } else {
                loanos = amt - (principaldemand * (i + 1))
            }
            var installMentDate = validate!!.addmonth(
                validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate),
                (morotrum + i + 1), 0
            )
            voMemLoanScheduleEntity = VoMemLoanScheduleEntity(
                0,
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceLong(VoSpData.voShgMemID), // memId
                validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
                validate!!.returnIntegerValue(et_loan_no.text.toString()),
                principaldemand,
                principaldemand,
                loanos,
                0,
                i + 1,
                0,
                installMentDate,
                false,
                0,
               null,
                validate!!.RetriveSharepreferenceString(AppSP.userid),
                validate!!.Daybetweentime(validate!!.currentdatetime),
                "", 0,
                "", 0
            )
            voMemLoanScheduleViewModel!!.insertVoMemLoanSchedule(voMemLoanScheduleEntity!!)
        }

        var principalDemand =
            voMemLoanScheduleViewModel!!.getPrincipalDemandByInstallmentNum(
                0,// memId,
                validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
                validate!!.returnIntegerValue(et_loan_no.text.toString()),
                1
            )

        //principle overdue not available
        // var totalprincipalDemand = principalDemand + validate!!.returnIntegerValue(et_principal_overdue.text.toString())

        var totalprincipalDemand = 1

        *//* voMemLoanScheduleViewModel!!.updateCutOffLoanMemberSchedule(
             validate!!.RetriveSharepreferenceString(VoSpData.vomtg_guid)!!,
             0,//memId,
             validate!!.returnIntegerValue(et_loan_no.text.toString()),
             1,
             principalDemand
         )*//*

        CustomAlertchangeschedule(
            LabelSet.getText(
                "doyouwanttochaneschedule",
                R.string.doyouwanttochaneschedule
            )
        )
    }
*/
    fun CustomAlertchangeschedule(str: String) {
        val mDialogView =
            LayoutInflater.from(this).inflate(R.layout.customealertdialogepartial, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str
        mDialogView.btn_yes.background = resources.getDrawable(R.drawable.button_bg_vomeeting)
        mDialogView.btn_no.background = resources.getDrawable(R.drawable.button_bg_vomeeting)
        mDialogView.btn_yes.setOnClickListener {
            //            PartialSaveData()
            mAlertDialog.dismiss()
            validate!!.SaveSharepreferenceInt(
                VoSpData.voLoanno,
                validate!!.returnIntegerValue(et_loan_no.text.toString())
            )

            val intent = Intent(this, VoCutOffPrincipalDemandActivity::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)

        }
        mDialogView.btn_no.setOnClickListener {
            val intent = Intent(this, VOtoSHGLoanSummary::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(0, 0)
            mAlertDialog.dismiss()
        }
    }

    fun fillData() {
        val list = voLoanApplicationViewmodel.getmemberLoanApplication(
            validate!!.RetriveSharepreferenceLong(VoSpData.voLoanappid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voShgMemID)
        )
        if (!list.isNullOrEmpty()) {
            val sanctionAmount = validate!!.returnIntegerValue(list.get(0).amtSanction.toString())

            if (sanctionAmount > 0) {
                et_sanction_amount.setText(sanctionAmount.toString())
            }

        }
    }

    private fun fillSpinner() {

        dataspin_repayment_frequency = lookupViewmodel.getlookup(
            19, validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_modeofpayment = lookupViewmodel.getlookup(
            61,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_fund = loanProductViewmodel!!.getFundTypeBySource_Receiptwithtypeid(
            3, 2,validate!!.RetriveSharepreferenceInt(VoSpData.ShortDescription)
        )
        validate!!.fillFundType(this, spin_fund_type, dataspin_fund)
        validate!!.fillspinner(this, spin_modePayment, dataspin_modeofpayment)
        validate!!.fillspinner(this, spin_loan_repayment_frequency, dataspin_repayment_frequency)

    }

    fun showData() {
        val loanlist = voMemLoanViewModel.getLoanDetailData(
            validate!!.RetriveSharepreferenceInt(VoSpData.voLoanno),
            validate!!.RetriveSharepreferenceLong(
                VoSpData.voshgid
            ),
            validate!!.RetriveSharepreferenceLong(VoSpData.voShgMemID),
            validate!!.RetriveSharepreferenceString(
                VoSpData.vomtg_guid
            )!!
        )

        if (!loanlist.isNullOrEmpty()) {
            et_loan_no.setText(loanlist.get(0).loanNo.toString())
            et_interest_rate.setText(validate!!.returnStringValue(loanlist.get(0).interestRate.toString()))
            et_amount_paid.setText(validate!!.returnStringValue(loanlist.get(0).amount.toString()))
            et_sanction_date.setText(
                validate!!.convertDatetime(
                    validate!!.returnLongValue(
                        loanlist.get(0).sanctionDate.toString()
                    )
                )
            )
            et_date_of_amount_paid.setText(
                validate!!.convertDatetime(
                    validate!!.returnLongValue(
                        loanlist.get(0).disbursementDate.toString()
                    )
                )
            )
            et_loan_tenure.setText(validate!!.returnStringValue(loanlist.get(0).period.toString()))
            et_moratorium_period.setText(validate!!.returnStringValue(loanlist.get(0).moratoriumPeriod.toString()))

            //principle overdue not available

            if (validate!!.returnIntegerValue(loanlist.get(0).period.toString()) > 0) {
                var installment_Amount =
                    (validate!!.returnIntegerValue(loanlist.get(0).amount.toString()) / validate!!.returnIntegerValue(
                        loanlist.get(0).period.toString()
                    ))
                et_installment_amount.setText(validate!!.returnStringValue(installment_Amount.toString()))
            }

            spin_loan_repayment_frequency.setSelection(
                validate!!.returnlookupcodepos(
                    loanlist.get(0).installmentFreq,
                    dataspin_repayment_frequency
                )
            )

            spin_fund_type.setSelection(
                validate!!.setFundType(
                    validate!!.returnIntegerValue(
                        loanlist.get(0).fundType.toString()
                    ), dataspin_fund
                )
            )/*fund type*/

            spin_modePayment.setSelection(
                validate!!.returnlookupcodepos(
                    validate!!.returnIntegerValue(
                        loanlist.get(0).modePayment.toString()
                    ), dataspin_modeofpayment
                )
            )
            spin_bank.setSelection(
                setaccount(
                    loanlist.get(0).bankCode.toString()

                )
            )

            et_cheque_no_transactio_no.setText(loanlist[0].transactionNo)

            btn_savegray.visibility = View.VISIBLE
            btn_save.visibility = View.GONE

        } else {
            spin_modePayment.setSelection(
                validate!!.returnlookupcodepos(
                    1, dataspin_modeofpayment
                )
            )

            spin_loan_repayment_frequency.setSelection(
                validate!!.returnlookupcodepos(
                    3,
                    dataspin_repayment_frequency
                )
            )

            var loanno = voMemLoanViewModel.getmaxLoanno(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
            )

            et_loan_no.setText(
                (loanno + 1).toString()
            )
            et_sanction_date.setText(
                validate!!.convertDatetime(
                    validate!!.RetriveSharepreferenceLong(
                        VoSpData.voCurrentMtgDate
                    )
                )
            )
            et_date_of_amount_paid.setText(
                validate!!.convertDatetime(
                    validate!!.RetriveSharepreferenceLong(
                        VoSpData.voCurrentMtgDate
                    )
                )
            )
            spin_fund_type.setSelection(
                validate!!.setFundType(
                    validate!!.RetriveSharepreferenceInt(
                       VoSpData.ShortDescription
                    ), dataspin_fund
                )
            )
            fillfunddata()
            setminmaxlimit()
            btn_savegray.visibility = View.GONE
            btn_save.visibility = View.VISIBLE
        }

    }

    override fun onBackPressed() {
        var intent = Intent(this, VOtoSHGLoanSummary::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

}
