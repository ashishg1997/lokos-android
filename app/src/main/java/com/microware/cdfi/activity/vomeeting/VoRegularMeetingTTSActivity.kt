package com.microware.cdfi.activity.vomeeting

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.speech.tts.UtteranceProgressListener
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.entity.MstVOCOAEntity
import com.microware.cdfi.entity.voentity.VoFinTxnDetMemEntity
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.CboBankViewmodel
import com.microware.cdfi.viewModel.MappedShgViewmodel
import com.microware.cdfi.viewModel.MappedVoViewmodel
import com.microware.cdfi.viewModel.MstVOCOAViewmodel
import com.microware.cdfi.viewModel.voviewmodel.*
import kotlinx.android.synthetic.main.activity_vo_regular_meeting_ttsactivity.*
import kotlinx.android.synthetic.main.bank_summary.*
import kotlinx.android.synthetic.main.cash_box.*
import kotlinx.android.synthetic.main.repay_toolbar.*
import kotlinx.android.synthetic.main.repay_toolbar.ic_Back
import kotlinx.android.synthetic.main.repay_toolbar.tv_title
import kotlinx.android.synthetic.main.repay_toolbar_vomeeting.*
import java.util.*

class VoRegularMeetingTTSActivity : AppCompatActivity(), TextToSpeech.OnInitListener {


    var translatedData = ""
    private val REQ_TTS_STATUS_CHECK = 0
    private val TAG = "TTS Demo"
    private var mTts: TextToSpeech? = null
    private var uttCount = 0
    private var lastUtterance = -1
    private val params = HashMap<String, String>()

    var SHG_name = ""
    var MeetingDate = ""
    var totalMember = ""
    var presentMember = ""
    var bank_balance = 0
    var bank_credit_balance = 0
    var bank_debit_balance = 0
    var cash_balance = 0
    var cash_credit_balance = 0
    var cash_debit_balance = 0
    var validate: Validate? = null


    var receipt_shg = ""
    var receipt_clf = ""
    var receipt_srlm = ""
    var receipt_other = ""
    var payment_shg = ""
    var payment_clf = ""
    var payment_other = ""

    var finaldataString = ""
    var sentence1 = ""
    var summarySentence = ""
    var attendanceSentence = ""
    var AbsentattendanceSentence = ""
    var totalreceiptSentence = ""
    var totalpaymentSentence = ""
    var receiptFromShgSentence = ""
    var receiptFromShgMemberSentence = ""
    var receiptFromClfSentence = ""
    var receiptFromSrlmSentence = ""
    var receiptFromOtherSentence = ""
    var receiptFrom = ""
    var paymentFrom = ""
    var paymentFromShgSentence = ""
    var paymentFromShgMemberSentence = ""
    var paymentFromClfSentence = ""
    var paymentFromOtherSentence = ""

    var cash_balanceSentence = ""
    var cash_debitSentence = ""
    var cash_creditSentence = ""
    var bank_balanceSentence = ""
    var bank_debitSentence = ""
    var bank_creditSentence = ""
    var totalCollectionSentence = ""


    var attendanceData = ""
    var amountReceiveData = ""
    var amountPaidData = ""
    var bankData = ""
    var cashData = ""


    lateinit var cboBankViewmodel: CboBankViewmodel
    lateinit var voFinTxnDetMemViewModel: VoFinTxnDetMemViewModel
    lateinit var voFinTxnDetGrpViewModel: VoFinTxnDetGrpViewModel
    lateinit var voFinTxnViewModel: VoFinTxnViewModel
    lateinit var voMemLoanTxnViewModel: VoMemLoanTxnViewModel
    lateinit var voMemLoanViewModel: VoMemLoanViewModel
    lateinit var voGroupLoanTxnViewModel: VoGroupLoanTxnViewModel
    lateinit var voGroupLoanViewModel: VoGroupLoanViewModel
    lateinit var vomtgDetViewmodel: VoMtgDetViewModel
    var voBankList: List<Cbo_bankEntity>? = null
    lateinit var voGenerateMeetingViewmodel: VoGenerateMeetingViewmodel
    var mappedshgViewmodel: MappedShgViewmodel? = null
    var mappedVoViewmodel: MappedVoViewmodel? = null

    var mstvoCOAViewmodel: MstVOCOAViewmodel? = null
    var cboBankViewModel: CboBankViewmodel? = null
    var languageCode = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vo_regular_meeting_ttsactivity)

        tv_title.text = "Meeting Information"
        var intentData = intent
        SHG_name = intentData.getStringExtra("SHG_name").toString()
        MeetingDate = intentData.getStringExtra("MeetingDate").toString()
        totalMember = intentData.getStringExtra("totalMember").toString()

        receipt_shg = intentData.getStringExtra("receipt_shg").toString()
        receipt_clf = intentData.getStringExtra("receipt_clf").toString()
        receipt_srlm = intentData.getStringExtra("receipt_srlm").toString()
        receipt_other = intentData.getStringExtra("receipt_other").toString()
        payment_shg = intentData.getStringExtra("payment_shg").toString()
        payment_clf = intentData.getStringExtra("payment_clf").toString()
        payment_other = intentData.getStringExtra("payment_other").toString()
        presentMember = intentData.getStringExtra("presentMember").toString()

        tv_vo_name.text = SHG_name
        tv_vo_date.text = "Date : " + MeetingDate

        validate = Validate(this)
        voGenerateMeetingViewmodel =
            ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        cboBankViewmodel =
            ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        voFinTxnDetMemViewModel =
            ViewModelProviders.of(this).get(VoFinTxnDetMemViewModel::class.java)
        voFinTxnDetGrpViewModel =
            ViewModelProviders.of(this).get(VoFinTxnDetGrpViewModel::class.java)
        voFinTxnViewModel =
            ViewModelProviders.of(this).get(VoFinTxnViewModel::class.java)
        voMemLoanTxnViewModel =
            ViewModelProviders.of(this).get(VoMemLoanTxnViewModel::class.java)
        voMemLoanViewModel =
            ViewModelProviders.of(this).get(VoMemLoanViewModel::class.java)
        voGroupLoanTxnViewModel =
            ViewModelProviders.of(this).get(VoGroupLoanTxnViewModel::class.java)
        voGroupLoanViewModel =
            ViewModelProviders.of(this).get(VoGroupLoanViewModel::class.java)
        voBankList =
            cboBankViewmodel.getBankdata1(validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID))
        mstvoCOAViewmodel = ViewModelProviders.of(this).get(MstVOCOAViewmodel::class.java)
        vomtgDetViewmodel = ViewModelProviders.of(this).get(VoMtgDetViewModel::class.java)
        mappedshgViewmodel = ViewModelProviders.of(this).get(MappedShgViewmodel::class.java)
        mappedVoViewmodel = ViewModelProviders.of(this).get(MappedVoViewmodel::class.java)


        if (voBankList!!.size > 0) {
            for (i in 0 until voBankList!!.size) {
                var balance = getTotalBalance(voBankList!!.get(i).bank_code!!)
                var credit = getTotalCreditBalance(voBankList!!.get(i).bank_code!!)
                var debit = getTotalDebitBalance(voBankList!!.get(i).bank_code!!)
                bank_balance = bank_balance + balance
                bank_credit_balance = bank_credit_balance + credit
                bank_debit_balance = bank_debit_balance + debit
            }
        }

        cash_balance = getClosingBalance()
        cash_credit_balance = getTotalCreditBalance()
        cash_debit_balance = getTotalDebitBalance()


        ic_Back.setOnClickListener {
            onBackPressed()
        }

        val checkIntent = Intent()
        checkIntent.action = TextToSpeech.Engine.ACTION_CHECK_TTS_DATA
        startActivityForResult(checkIntent, REQ_TTS_STATUS_CHECK)

        languageCode = validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
        getMeetingSummaryData()

        ic_Back.setOnClickListener {
            onBackPressed()
        }

        tv_summary_play1.setOnClickListener {
            if (mTts != null) {
                mTts!!.stop()

                var testdata = finaldataString.replace("\n", "")
                if (testdata.isNullOrEmpty()) {
                    doSpeak("Meeting Summary not available")
                    alertDialog("Overview", "\n\n" + "Meeting Summary not available")
                } else {
                    doSpeak(finaldataString)
                    finaldataString = finaldataString.replace("=", "")
                    finaldataString = finaldataString.replace("'", "")
                    alertDialog("Overview", finaldataString)
                }
            }
        }

        iv_attendance.setOnClickListener {
            if (mTts != null) {
                mTts!!.stop()

                var testdata = attendanceData.replace("\n", "")
                if (testdata.isNullOrEmpty()) {
                    doSpeak("Meeting Summary not available")
                    alertDialog("Overview", "\n\n" + "Meeting Summary not available")
                } else {
                    doSpeak(attendanceData)
                    attendanceData = attendanceData.replace("=", "")
                    attendanceData = attendanceData.replace("'", "")
                    alertDialog("Attendance", attendanceData)
                }
            }
        }

        iv_receive.setOnClickListener {
            if (mTts != null) {
                mTts!!.stop()

                var testdata = amountReceiveData.replace("\n", "")
                if (testdata.isNullOrEmpty()) {
                    doSpeak("Attendance not available")
                    alertDialog("Amount Receive", "\n\n" + "Attendance not available")
                } else {
                    doSpeak(amountReceiveData)
                    amountReceiveData = amountReceiveData.replace("=", "")
                    amountReceiveData = amountReceiveData.replace("'", "")
                    alertDialog("Amount Receive", amountReceiveData)
                }
            }
        }

        iv_paid.setOnClickListener {
            if (mTts != null) {
                mTts!!.stop()

                var testdata = amountPaidData.replace("\n", "")
                if (testdata.isNullOrEmpty()) {
                    doSpeak("Attendance not available")
                    alertDialog("Amount Paid", "\n\n" + "Attendance not available")
                } else {
                    doSpeak(amountPaidData)
                    amountPaidData = amountPaidData.replace("=", "")
                    amountPaidData = amountPaidData.replace("'", "")
                    alertDialog("Amount Paid", amountPaidData)
                }
            }
        }

        iv_bankCollection.setOnClickListener {
            if (mTts != null) {
                mTts!!.stop()

                var testdata = bankData.replace("\n", "")
                if (testdata.isNullOrEmpty()) {
                    doSpeak("Attendance not available")
                    alertDialog("Bank Collection", "\n\n" + "Attendance not available")
                } else {
                    doSpeak(bankData)
                    bankData = bankData.replace("=", "")
                    bankData = bankData.replace("'", "")
                    alertDialog("Bank Collection", bankData)
                }
            }
        }

        iv_cashCollection.setOnClickListener {
            if (mTts != null) {
                mTts!!.stop()

                var testdata = cashData.replace("\n", "")
                if (testdata.isNullOrEmpty()) {
                    doSpeak("Attendance not available")
                    alertDialog("Bank Collection", "\n\n" + "Attendance not available")
                } else {
                    doSpeak(cashData)
                    cashData = cashData.replace("=", "")
                    cashData = cashData.replace("'", "")
                    alertDialog("Cash Collection", cashData)
                }
            }
        }


    }

    private fun getMeetingSummaryData() {

        var memberList = vomtgDetViewmodel.getListDataMtgByMtgnum(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        var fundreceiptList = mstvoCOAViewmodel!!.getCoaSubHeadData(
            validate!!.RetriveSharepreferenceInt(VoSpData.voOrgType),
            listOf("RG", "RL", "RO"),
            "en"
        )

        var fundpaymentList = mstvoCOAViewmodel!!.getCoaSubHeadData(
            validate!!.RetriveSharepreferenceInt(VoSpData.voOrgType),
            listOf("PG", "PL", "PO"),
           "en"
        )!!

       var shglist = mappedshgViewmodel!!.getShgNameList(validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID)!!)
       var volist = mappedVoViewmodel!!.getVoNameList(validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID)!!)


        var meetno = validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)

        finaldataString = MeetingTTS.VORegularMeeting(
            meetno,
            languageCode,
            this@VoRegularMeetingTTSActivity,
            SHG_name,
            MeetingDate,
            totalMember,
            presentMember,
            bank_balance,
            bank_credit_balance,
            bank_debit_balance,
            cash_balance,
            cash_credit_balance,
            cash_debit_balance,
            receipt_shg,
            receipt_clf,
            receipt_srlm,
            receipt_other,
            payment_shg,
            payment_clf,
            payment_other,
            fundreceiptList!!,
            fundpaymentList,
            shglist!!,
            volist!!,
            memberList
        )

        var splitData = finaldataString.split("@#@")
        sentence1 = splitData.get(0)
        summarySentence = splitData.get(1)
        attendanceSentence = splitData.get(2)
        AbsentattendanceSentence = splitData.get(3)
        totalreceiptSentence = splitData.get(4)
        receiptFromShgSentence = splitData.get(5)
        receiptFromShgMemberSentence = splitData.get(6)
        receiptFromClfSentence = splitData.get(7)
        receiptFromSrlmSentence = splitData.get(8)
        receiptFromOtherSentence = splitData.get(9)
        totalpaymentSentence = splitData.get(10)
        paymentFromShgSentence = splitData.get(11)
        paymentFromShgMemberSentence = splitData.get(12)
        paymentFromClfSentence = splitData.get(13)
        paymentFromOtherSentence = splitData.get(14)
        totalCollectionSentence = splitData.get(15)
        cash_balanceSentence = splitData.get(16)
        cash_debitSentence = splitData.get(17)
        cash_creditSentence = splitData.get(18)
        bank_balanceSentence = splitData.get(19)
        bank_debitSentence = splitData.get(20)
        bank_creditSentence = splitData.get(21)


        attendanceData = summarySentence + attendanceSentence + AbsentattendanceSentence
        amountReceiveData =
            totalreceiptSentence + receiptFromShgSentence + receiptFromShgMemberSentence + receiptFromClfSentence + receiptFromSrlmSentence + receiptFromOtherSentence
        amountPaidData =
            totalpaymentSentence + paymentFromShgSentence + paymentFromShgMemberSentence +  paymentFromClfSentence + paymentFromOtherSentence
        bankData =
            totalCollectionSentence + bank_balanceSentence + bank_debitSentence + bank_creditSentence
        cashData =
            totalCollectionSentence + cash_balanceSentence + cash_debitSentence + cash_creditSentence


        finaldataString =
            sentence1 + summarySentence + attendanceSentence + AbsentattendanceSentence + totalreceiptSentence + receiptFromShgSentence + receiptFromShgMemberSentence + receiptFromClfSentence + receiptFromSrlmSentence + receiptFromOtherSentence + totalpaymentSentence + paymentFromShgSentence +
                    paymentFromShgMemberSentence + paymentFromClfSentence + paymentFromOtherSentence + totalCollectionSentence +
                    cash_balanceSentence + cash_debitSentence + cash_creditSentence +
                    bank_balanceSentence + bank_debitSentence + bank_creditSentence
    }

    fun getTotalBalance(bankCode: String): Int {
        var balance = 0
        balance = voFinTxnViewModel.gettotalclosinginbank(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            bankCode
        )
        return balance.toInt()
    }

    fun getTotalDebitBalance(bankCode: String): Int {
        var balance = 0
        var totshgcashdebit = voFinTxnDetMemViewModel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(2, 3),
            listOf("PG", "PL", "PO"),
            bankCode
        )
        var totvocashdebit = voFinTxnDetGrpViewModel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(2, 3),
            listOf("PG", "PL", "PO"),
            bankCode
        )

        val grptotloanpaid = voGroupLoanTxnViewModel.getgrptotloanpaidinbank(

            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            bankCode, listOf<Int>(2, 3)
        )

        val loanDisbursedAmt = voMemLoanViewModel.getMemLoanDisbursedAmount(

            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            bankCode, listOf<Int>(2, 3)
        )
        balance = totshgcashdebit + totvocashdebit + grptotloanpaid + loanDisbursedAmt
        return balance
    }

    fun getTotalCreditBalance(bankCode: String): Int {
        var balance = 0
        var totshgcash = voFinTxnDetMemViewModel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(2, 3),
            listOf("RG", "RL", "RO"),
            bankCode
        )

        var totvocash = voFinTxnDetGrpViewModel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(2, 3),
            listOf("RG", "RL", "RO"),
            bankCode
        )
        val memtotloanpaid = voMemLoanTxnViewModel.getmemtotloanpaidinbank(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            bankCode, listOf<Int>(2, 3)
        )

        val loanDisbursedAmt = voGroupLoanViewModel.getgrouptotloanamtinbank(

            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            bankCode, listOf<Int>(2, 3)
        )
        balance = totshgcash + totvocash + memtotloanpaid + loanDisbursedAmt
        return balance
    }

    fun getClosingBalance(): Int {
        var balance = 0
        balance = voGenerateMeetingViewmodel.getClosingBalanceCash(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        return balance
    }

    fun getTotalDebitBalance(): Int {
        var balance = 0
        var totshgcashdebit = voFinTxnDetMemViewModel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(1),
            listOf("PG", "PL", "PO"),
            ""
        )
        var totvocashdebit = voFinTxnDetGrpViewModel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(1),
            listOf("PG", "PL", "PO"),
            ""
        )

        val grptotloanpaid = voGroupLoanTxnViewModel.getgrptotloanpaidinbank(

            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            "",
            listOf<Int>(1)
        )

        val loanDisbursedAmt = voMemLoanViewModel.getMemLoanDisbursedAmount(

            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            "",
            listOf<Int>(1)
        )

        balance = totshgcashdebit + totvocashdebit + grptotloanpaid + loanDisbursedAmt

        return balance
    }

    fun getTotalCreditBalance(): Int {
        var balance = 0
        var totshgcash = voFinTxnDetMemViewModel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(1),
            listOf("RG", "RL", "RO"),
            ""
        )
        var totvocash = voFinTxnDetGrpViewModel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(1),
            listOf("RG", "RL", "RO"),
            ""
        )
        val memtotloanpaid = voMemLoanTxnViewModel.getmemtotloanpaidinbank(

            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            "",
            listOf<Int>(1)
        )
        val loanDisbursedAmt = voGroupLoanViewModel.getgrouptotloanamtinbank(

            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            "",
            listOf<Int>(1)
        )
        balance = totshgcash + totvocash + memtotloanpaid + loanDisbursedAmt
        return balance
    }

    fun getAmount(uid: Int, shgID: Long): List<VoFinTxnDetMemEntity>? {
        return voFinTxnDetMemViewModel.getauidlist(
            uid,
            shgID,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
        )
    }


    fun doSpeak(word: String) {
        val st = StringTokenizer(word.toString(), ",.=")
        while (st.hasMoreTokens()) {
            params[TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID] = uttCount++.toString()
            mTts!!.setPitch(1.toFloat())
            mTts!!.setSpeechRate(0.94.toFloat())
            mTts!!.speak(st.nextToken(), TextToSpeech.QUEUE_ADD, params)
//            mTts!!.setOnUtteranceCompletedListener(object :
//                TextToSpeech.OnUtteranceCompletedListener {
//                override fun onUtteranceCompleted(utteranceId: String?) {
//                    Log.v(TAG, "Got completed message for uttId: $utteranceId")
//                }
//            })

            mTts!!.setOnUtteranceProgressListener(object : UtteranceProgressListener() {
                override fun onStart(utteranceId: String) {

                    if (mTts != null) {
//                        tv_summary_play.text = "STOP"
                    }
                }

                override fun onDone(utteranceId: String) {

                    if (mTts != null && utteranceId.equals((uttCount - 1).toString())) {
                        mTts!!.stop()
                        tv_summary_play1.text = "PLAY"

//                        if (mTts == null){
//
//                        }
                    }
                }

                override fun onError(utteranceId: String) {
                    Log.i("TextToSpeech", "On Error")
                }
            })
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQ_TTS_STATUS_CHECK) {
            when (resultCode) {
                TextToSpeech.Engine.CHECK_VOICE_DATA_PASS -> {
                    mTts = TextToSpeech(this, this)
                }
                TextToSpeech.Engine.CHECK_VOICE_DATA_BAD_DATA, TextToSpeech.Engine.CHECK_VOICE_DATA_MISSING_DATA, TextToSpeech.Engine.CHECK_VOICE_DATA_MISSING_VOLUME -> {
                    val installIntent = Intent()
                    installIntent.action = TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA
                    startActivity(installIntent)
                }
                TextToSpeech.Engine.CHECK_VOICE_DATA_FAIL -> Log.e(TAG, "Got a failure.")
                else -> Log.e(TAG, "Got a failure.")
            }

            val available = data!!.getStringArrayListExtra("availableVoices")
            Log.v("languages count", available!!.size.toString())
            val iter: Iterator<String> = available.iterator()
            while (iter.hasNext()) {
                val lang = iter.next()
                val locale = Locale(lang)
            }
            val locales = Locale.getAvailableLocales()
            Log.v(TAG, "available locales:")
            for (i in locales.indices) Log.v(TAG, "locale: " + locales[i].displayName)
            val defloc = Locale.getDefault()
            Log.v(TAG, "current locale: " + defloc.displayName)
        }
    }

    override fun onInit(status: Int) {
        if (status == TextToSpeech.SUCCESS) {
            mTts!!.language = Locale(getDownloadLanguageISO(languageCode), "IND", "variant")
        }

    }


    override fun onPause() {
        super.onPause()
        if (mTts != null) mTts!!.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mTts != null) mTts!!.shutdown()
    }

    override fun onResume() {
        super.onResume()
        if (mTts != null) {
            mTts!!.stop()
            tv_summary_play1.text = "PLAY"
        }
    }


    private fun alertDialog(titleData: String, paragraphData: String) {
        val mDialogView: View = LayoutInflater.from(this@VoRegularMeetingTTSActivity)
            .inflate(R.layout.paragraph_text_ui, null)
        val mBuilder = android.app.AlertDialog.Builder(this@VoRegularMeetingTTSActivity)
            .setView(mDialogView)
        var mAlertDialog = mBuilder.show()
//        mAlertDialog!!.setCancelable(false)
        val title = mDialogView.findViewById<TextView>(R.id.title)
        val tv_paragraph = mDialogView.findViewById<TextView>(R.id.tv_paragraph)
        val iv_cancel = mDialogView.findViewById<ImageView>(R.id.iv_cancel)

        iv_cancel.setOnClickListener {
            if (mTts != null) {
                mTts!!.stop()
//                doSpeak(summaryData!!)
            }
            mAlertDialog!!.dismiss()
        }

        title.text = titleData
        tv_paragraph.text = paragraphData

        mAlertDialog!!.setOnDismissListener(object : DialogInterface.OnDismissListener {
            override fun onDismiss(dialog: DialogInterface?) {
                if (mTts != null) {
                    mTts!!.stop()
                }
                mAlertDialog.dismiss()
            }
        })
    }

    fun getTotalValue(list: List<MstVOCOAEntity>, shgID: Long): String {
        var totalAmount = 0
        for (i in 0 until list.size) {
            val amount = voFinTxnDetMemViewModel.getAmount(
                list[i].uid,
                shgID,
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
            )
            totalAmount = totalAmount + validate!!.returnIntegerValue(amount.toString())
        }

        return  totalAmount.toString()
    }

}