package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.VoSHGAttendanceAdapter
import com.microware.cdfi.fragment.VoMeetingTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.voviewmodel.VoMtgDetViewModel
import kotlinx.android.synthetic.main.buttons_vo.btn_cancel
import kotlinx.android.synthetic.main.buttons_vo.btn_save
import kotlinx.android.synthetic.main.shg_attendance.*
import kotlinx.android.synthetic.main.shg_attendance.rvList
import kotlinx.android.synthetic.main.shg_attendance.tv_absent_count
import kotlinx.android.synthetic.main.shg_attendance.tv_present_count


class VoSHGAttendanceActivity : AppCompatActivity() {
    var validate: Validate? = null
    var cboType = 0
//    lateinit var mappedShgViewmodel: MappedShgViewmodel
//    lateinit var mappedVoViewmodel: MappedVoViewmodel
    lateinit var vomtgDetViewmodel: VoMtgDetViewModel
    lateinit var attendenceDetailAdapter: VoSHGAttendanceAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.shg_attendance)

        validate = Validate(this)

//        mappedShgViewmodel = ViewModelProviders.of(this).get(MappedShgViewmodel::class.java)
//        mappedVoViewmodel = ViewModelProviders.of(this).get(MappedVoViewmodel::class.java)
        vomtgDetViewmodel = ViewModelProviders.of(this).get(VoMtgDetViewModel::class.java)

        if (validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType) == 2) {
            cboType = 2
        } else {
            cboType = 1
        }

        replaceFragmenty(
            fragment = VoMeetingTopBarFragment(3),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )

        btn_save.isEnabled =
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber) >= validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
        btn_save.setOnClickListener {
            getTotalValue()
            setValue()
        }

        btn_cancel.setOnClickListener {
            val intent = Intent(this, VoMeetingMenuActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }

        setLabelText()
        fillRecyclerView()
        setValue()

    }

    fun setLabelText() {
        tv_sr_no.text = LabelSet.getText("sr_no", R.string.sr_no)

        tv_shg_name.text = LabelSet.getText(
            "shg_name",
            R.string.shg_name
        )
        tv_attendance.text = LabelSet.getText(
            "attendance",
            R.string.attendance
        )

        tv_meeting_attendance.text = LabelSet.getText(
            "meeting_attendance",
            R.string.meeting_attendance
        )

        btn_save.text = LabelSet.getText("save", R.string.save)

        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }

    private fun fillRecyclerView() {

//        var list = mappedShgViewmodel.getShgNameList(
//            validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID)!!)

        val list = vomtgDetViewmodel.getListDataMtgByMtgnum(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        attendenceDetailAdapter = VoSHGAttendanceAdapter(this,list)
        rvList.layoutManager = LinearLayoutManager(this)
        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = rvList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvList.layoutParams = params
        rvList.adapter = attendenceDetailAdapter
    }

    fun getTotalValue() {
        var saveValue = 0

        val iCount = rvList.childCount
        var totalPresent = 0
        var totalAbsent = 0

        for (i in 0 until iCount) {

            val gridChild =
                rvList.getChildAt(i) as? ViewGroup

            val et_attendance = gridChild!!
                .findViewById<View>(R.id.et_attendance) as? EditText

          /*  val et_GroupMeetingCode = gridChild!!
                .findViewById<View>(R.id.et_GroupMeetingCode) as? EditText*/

            val et_memId = gridChild
                .findViewById<View>(R.id.et_memId) as? EditText

           if (et_attendance!!.length() > 0) {
                var iValue = et_attendance.text.toString()
                var  memberId = validate!!.returnLongValue(et_memId!!.text.toString())
               // var GroupMeetingCode = validate!!.returnLongValue(et_GroupMeetingCode!!.text.toString())
                saveValue=saveData(iValue,memberId)
           }
        }

        if(saveValue > 0){
            validate!!.CustomAlertVO(
                LabelSet.getText(
                    "data_saved_successfully",
                    R.string.data_saved_successfully
                ),
                this
            )
        }
    }

    fun saveData(iValue: String,memberId:Long):Int {
        var value=0
        vomtgDetViewmodel.updateAttendance(
            iValue,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            memberId,
            validate!!.RetriveSharepreferenceString(AppSP.userid),
            validate!!.Daybetweentime(validate!!.currentdatetime)
        )
        value=1

        return value
    }

    fun setValue(){
        var TotalPresent = 0
        var TotalAbsent = 0
        if(validate!!.RetriveSharepreferenceInt(VoSpData.voMeetingType)==0) {
            tv_absent_count.visibility = View.VISIBLE
            tv_present_count.visibility = View.VISIBLE
            tv_present.visibility = View.VISIBLE
            tv_absent.visibility = View.VISIBLE

            TotalPresent = vomtgDetViewmodel.getTotalPresentAndAbsent(
                "1",
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
            )
            TotalAbsent = vomtgDetViewmodel.getTotalPresentAndAbsent(
                "2",
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
            )

            tv_present_count.text = ""+TotalPresent
            tv_absent_count.text = ""+TotalAbsent

        }else if(validate!!.RetriveSharepreferenceInt(VoSpData.voMeetingType)==11 || validate!!.RetriveSharepreferenceInt(VoSpData.voMeetingType)==12) {
            tv_absent_count.visibility = View.GONE
            lay_present.visibility = View.GONE
            tv_present.visibility = View.INVISIBLE
            tv_absent.visibility = View.INVISIBLE

            tv_present_count.text = ""
            tv_absent_count.text = ""
        }

    }

    override fun onBackPressed() {
        var intent = Intent(this, VoMeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

}