package com.microware.cdfi.activity.vomeeting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.adapter.vomeetingadapter.VoSettlementDetailAdapter
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.fragment.VoReceiptsTopBarFragment
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoGenerateMeetingViewmodel
import kotlinx.android.synthetic.main.activity_vosettlementlist.*
import kotlinx.android.synthetic.main.buttons.*

class VoSettlementListActivity : AppCompatActivity() {
    var lookupViewmodel: LookupViewmodel? = null
    var validate: Validate? = null
    lateinit var settlementDetailAdapter:VoSettlementDetailAdapter
    lateinit var generateMeetingViewmodel: VoGenerateMeetingViewmodel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vosettlementlist)
        validate=Validate(this)
        lookupViewmodel = ViewModelProviders.of(this).get(LookupViewmodel::class.java)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)


        btn_cancel.setOnClickListener {
            var intent = Intent(this, VoMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(0, 0)
        }

        replaceFragmenty(
            fragment = VoReceiptsTopBarFragment(35),
            allowStateLoss = true,
            containerViewId = R.id.mainContent
        )
        
        setLabelText()
        fillRecyclerView()

    }

    override fun onBackPressed() {
        var intent = Intent(this, VoMeetingMenuActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        overridePendingTransition(0,0)
        finish()
    }

    fun setLabelText() {
        tv_srno.text = LabelSet.getText("sr_no", R.string.sr_no)
        tv_shgName.text = LabelSet.getText(
            "shg_name",
            R.string.shg_name
        )
        tv_total_saved.text = LabelSet.getText(
            "total_saved",
            R.string.total_saved
        )
        tv_withdrawl_n_for_saving.text = LabelSet.getText(
            "withdrawl_n_for_saving",
            R.string.withdrawl_n_for_saving
        )
        btn_save.text = LabelSet.getText("save", R.string.save)
        btn_cancel.text = LabelSet.getText(
            "cancel",
            R.string.cancel
        )
    }

    private fun fillRecyclerView() {
        var list = generateMeetingViewmodel.getListinactivemember(validate!!.RetriveSharepreferenceInt(
            VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid))

        settlementDetailAdapter = VoSettlementDetailAdapter(this,list)

        rvList.layoutManager = LinearLayoutManager(this)

        val isize: Int
        isize = list.size
        val params: ViewGroup.LayoutParams = rvList.layoutParams
        val r = resources
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, resources.getDimension(R.dimen.spraying),
            r.displayMetrics
        )
        val hi = Math.round(px)
        var gridHeight = hi * isize
        params.height = gridHeight
        rvList.layoutParams = params
        rvList.adapter = settlementDetailAdapter
    }

    fun getTotalValue() {
        var iValue = 0
        val iCount = rvList.childCount

        for (i in 0 until iCount) {
            val gridChild =
                rvList.getChildAt(i) as? ViewGroup

            val tv_count = gridChild!!
                .findViewById<View>(R.id.tv_count) as? TextView

            if (tv_count!!.length() > 0) {
                iValue = iValue + validate!!.returnIntegerValue(tv_count.text.toString())
            }


        }
        tv_TotalTodayValue.text = iValue.toString()

    }
}