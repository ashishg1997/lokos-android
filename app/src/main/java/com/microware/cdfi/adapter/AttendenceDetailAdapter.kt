package com.microware.cdfi.adapter

import android.content.Context
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.AttendnceDetailActivity
import com.microware.cdfi.entity.DtmtgDetEntity
import com.microware.cdfi.utility.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.attendences_detail_item.view.*
import java.io.File

class AttendenceDetailAdapter(
    var context: Context,
    var list: List<DtmtgDetEntity>
) :
    RecyclerView.Adapter<AttendenceDetailAdapter.ViewHolder>() {
    var validate: Validate? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        var view =
            LayoutInflater.from(context).inflate(R.layout.attendences_detail_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_memberName.text = list.get(position).member_name.toString()
        holder.tv_srno.text = "" + (position + 1)
        if(validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType)==0){
            holder.tblpresent_absent.visibility = View.VISIBLE
            holder.et_Attendance.visibility = View.GONE
            if (validate!!.returnIntegerValue(list.get(position).attendance) == 1) {
                holder.btn_absent.setBackgroundDrawable(context.getDrawable(R.drawable.absentlight))
                holder.btn_present.setBackgroundDrawable(context.getDrawable(R.drawable.present_count))
            } else if (validate!!.returnIntegerValue(list.get(position).attendance) == 2) {
                holder.btn_absent.setBackgroundDrawable(context.getDrawable(R.drawable.absent_count))
                holder.btn_present.setBackgroundDrawable(context.getDrawable(R.drawable.presentlight))
            } else {
                holder.et_Attendance.setText("1")
                holder.btn_absent.setBackgroundDrawable(context.getDrawable(R.drawable.absentlight))
                holder.btn_present.setBackgroundDrawable(context.getDrawable(R.drawable.present_count))
                holder.et_GroupMCode.setText(list.get(position).group_m_code.toString())
            }
        }else if(validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType)==11 || validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType)==12){
            holder.tblpresent_absent.visibility = View.GONE
            holder.et_Attendance.visibility = View.VISIBLE
            holder.et_Attendance.setText(list.get(position).attendance)
            holder.et_GroupMCode.setText(list.get(position).group_m_code.toString())

        }
        val mediaStorageDirectory = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
            AppSP.IMAGE_DIRECTORY_NAME
        )
        var mediaFile1 = File(
            mediaStorageDirectory.path + File.separator + (context as AttendnceDetailActivity).getImage(
                list.get(position).mem_id
            )
        )
        Picasso.with(context).load(mediaFile1).error(R.drawable.ic_womensvg)
            .into(holder.iv_member_img)
        setLabelText(holder)
        if(validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) == 11 || validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) == 12) {
            var maxAllowedValue = (context as AttendnceDetailActivity).getSecondLastMeetingNum()
            holder.et_Attendance.filters = arrayOf(InputFilterMinMax (1, maxAllowedValue))
        }


        holder.btn_present.setOnClickListener(View.OnClickListener {
            if(validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType)==0) {
                holder.et_Attendance.setText("1")
                holder.btn_absent.setBackgroundResource(R.drawable.absentlight)
                holder.btn_present.setBackgroundResource(R.drawable.present_count)
                holder.et_GroupMCode.setText(list.get(position).group_m_code.toString())
            }
        })

        holder.btn_absent.setOnClickListener(View.OnClickListener {
            if(validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType)==0) {
                holder.et_Attendance.setText("2")
                holder.btn_absent.setBackgroundResource(R.drawable.absent_count)
                holder.btn_present.setBackgroundResource(R.drawable.presentlight)
                holder.et_GroupMCode.setText(list.get(position).group_m_code.toString())
            }
        })

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_memberName = view.tv_memberName
        var btn_present = view.btn_present
        var btn_absent = view.btn_absent
        var et_Attendance = view.et_Attendance
        var et_GroupMCode = view.et_GroupMCode
        var tv_srno = view.tv_srno
        var iv_member_img = view.iv_member_img
        var tblpresent_absent = view.tblpresent_absent

    }

    fun setLabelText(view: ViewHolder) {

    }

}