package com.microware.cdfi.adapter

import android.content.Context
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.AttendnceZeroDetailActivity
import com.microware.cdfi.entity.DtmtgDetEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.InputFilterMinMax
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.attendences_zero_detail_item.view.*
import java.io.File

class AttendenceZeroDetailAdapter(
    var context: Context,
    var list: List<DtmtgDetEntity>
) :
    RecyclerView.Adapter<AttendenceZeroDetailAdapter.ViewHolder>() {
    var validate:Validate?=null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate= Validate(context)
        var view =
            LayoutInflater.from(context).inflate(R.layout.attendences_zero_detail_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_memberName.text = list.get(position).member_name.toString()
        holder.tv_srno.text = ""+(position+1)

        holder.et_Attendance.setText(validate!!.returnStringValue(list.get(position).zero_mtg_attn.toString()))
        holder.et_GroupMCode.setText(list.get(position).group_m_code.toString())

        val mediaStorageDirectory = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
            AppSP.IMAGE_DIRECTORY_NAME
        )
        var mediaFile1 = File(mediaStorageDirectory.path + File.separator + (context as AttendnceZeroDetailActivity).getImage(
            list.get(position).mem_id))
        Picasso.with(context).load(mediaFile1) .error(R.drawable.ic_womensvg).into(holder.iv_member_img)
        setLabelText(holder)

     //   var maxAllowedValue = (context as AttendnceZeroDetailActivity).getSecondLastMeetingNum()
        holder.et_Attendance.filters = arrayOf(InputFilterMinMax (1, validate!!.RetriveSharepreferenceInt(
            MeetingSP.currentmeetingnumber)))
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_memberName = view.tv_memberName
        var et_Attendance = view.et_Attendance
        var tv_srno = view.tv_srno
        var et_GroupMCode = view.et_GroupMCode
        var iv_member_img = view.iv_member_img

    }

    fun setLabelText(view: ViewHolder) {

    }
}