package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.BankTransferActivity
import com.microware.cdfi.activity.meeting.CutOffGroupBankBalanceActivity
import com.microware.cdfi.api.meetingmodel.BankTransactionModel
import com.microware.cdfi.entity.ShgFinancialTxnDetailEntity
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.bank_transfer_list_item.view.*

class BankTransferAdapter(
    var context: Context,
    var list: List<BankTransactionModel>
) :
    RecyclerView.Adapter<BankTransferAdapter.ViewHolder>() {
    var validate: Validate? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        val view = LayoutInflater.from(context)
            .inflate(R.layout.bank_transfer_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_srno.text = (position + 1).toString()
        holder.tv_bankFrom.text = list[position].bank_from
        holder.tv_bankTo.text = list[position].bank_to
        holder.tv_amount.text = validate!!.returnStringValue(list[position].amount.toString())
        holder.cardView.setOnClickListener {
            validate!!.SaveSharepreferenceInt(MeetingSP.Auid,list[position].auid)
            validate!!.SaveSharepreferenceString(MeetingSP.bankFrom,list[position].bank_from)
            validate!!.SaveSharepreferenceString(MeetingSP.bankTo,list[position].bank_to)
            validate!!.SaveSharepreferenceString(MeetingSP.amountTransferred,list[position].amount.toString())
            val intent = Intent(context, BankTransferActivity::class.java)
            context.startActivity(intent)
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_srno = view.tv_srno
        var tv_bankFrom = view.tv_bankFrom
        var tv_bankTo = view.tv_bankTo
        var tv_amount = view.tv_amount
        var cardView = view.cardView
    }
}