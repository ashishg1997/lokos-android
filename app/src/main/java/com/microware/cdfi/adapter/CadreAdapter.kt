package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.CadreActivity
import com.microware.cdfi.activity.CadreListActivity
import com.microware.cdfi.entity.cadreShgMemberEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.cadre_list_item.view.*

class CadreAdapter(var context: Context, var listData: List<cadreShgMemberEntity>) :
    RecyclerView.Adapter<CadreAdapter.ViewHolder>() {

    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.cadre_list_item, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return listData.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val categoryType = (context as CadreListActivity).getCategoryName(validate!!.returnIntegerValue(listData[position].cadre_cat_code.toString()))
        if (!categoryType.isNullOrEmpty()){
            holder.tvCategoryType.text = categoryType
        }else{
            holder.tvCategoryType.text = ""
        }

        val roleType = (context as CadreListActivity).getRoleName(validate!!.returnIntegerValue(listData[position].cadre_role_code.toString()))
        if (!roleType.isNullOrEmpty()){
            holder.tv_cadreType.text = roleType
        }else{
            holder.tv_cadreType.text = ""
        }

        holder.tvJoiningDate.text = validate!!.convertDatetime(validate!!.returnLongValue(listData[position].date_joining.toString()))
        holder.tv_leavingDate.text = validate!!.convertDatetime(validate!!.returnLongValue(listData[position].date_leaving.toString()))
        holder.tblLayout.setOnClickListener {
            validate!!.SaveSharepreferenceString(AppSP.CADRE_GUID,listData[position].cadre_guid)
            val intent = Intent(context,CadreActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            context.startActivity(intent)
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tblLayout = view.tblLayout
        var tvCategoryType = view.tvCategoryType
        var tv_cadreType = view.tv_cadreType
        var tvJoiningDate = view.tvJoiningDate
        var tv_leavingDate = view.tv_leavingDate
    }
}