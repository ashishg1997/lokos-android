package com.microware.cdfi.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import kotlinx.android.synthetic.main.cashbox_detailitem.view.*

class CashBoxAdapter(var context: Context) :
    RecyclerView.Adapter<CashBoxAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.cashbox_detailitem, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return 7
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        setLabelText(holder)

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_memberName = view.tv_memberName
        var tv_name = view.tv_name
        var tv_amount = view.tv_amount

    }

    fun setLabelText(view: ViewHolder) {

    }
}