package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.AddressDetailActivity
import com.microware.cdfi.activity.AddressDetailListActivity
import com.microware.cdfi.entity.AddressEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.address_detailitem.view.*

class CboAddressAdapter(var context: Context, var addressEntity: List<AddressEntity>) :
    RecyclerView.Adapter<CboAddressAdapter.ViewHolder>() {

    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.address_detailitem, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        if(addressEntity.isNullOrEmpty()){
            return 0
        }else {
            return addressEntity.size
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvaddress.text = validate!!.returnStringValue(addressEntity.get(position).address_line1)
        holder.tvpincode.text = validate!!.returnStringValue(addressEntity.get(position).postal_code.toString())
        var villageName = (context as AddressDetailListActivity).getVillageName(
            validate!!.returnIntegerValue(addressEntity.get(position).village_id.toString())
        )
        if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) == 1){
            holder.ivDelete.visibility = View.GONE
        }else {
            holder.ivDelete.visibility = View.VISIBLE
        }
        holder.tvVillage.text = villageName

        setLabelText(holder)
        holder.ivEdit.setOnClickListener {
            validate!!.SaveSharepreferenceString(
                AppSP.ADDRESSGUID,
                addressEntity.get(position).address_guid
            )
            var inetnt = Intent(context, AddressDetailActivity::class.java)
            context.startActivity(inetnt)
        }

        holder.ivDelete.setOnClickListener {
            if (validate!!.returnLongValue(addressEntity.get(position).last_uploaded_date.toString()) > 0
                || validate!!.returnLongValue(addressEntity.get(position).cbo_address_id.toString())>0) {
                (context as AddressDetailListActivity).CustomAlert(
                    addressEntity.get(position).address_guid,1)
        }else {
                (context as AddressDetailListActivity).CustomAlert(addressEntity.get(position).address_guid,0)
            }
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tvtype = view.tvtype
        var tvaddress = view.tvaddress
        var tvVillage = view.tvVillage
        var tvpincode = view.tvpincode
        var ivEdit = view.ivEdit
        var ivDelete = view.ivDelete
        var tv_address = view.tv_address
        var tv_village = view.tv_village
        var tv_Pincode = view.tv_Pincode

    }

    fun setLabelText(view: ViewHolder) {
        view.tv_address.text = LabelSet.getText(
            "address",
            R.string.address
        )
        view.tv_village.text = LabelSet.getText(
            "village1",
            R.string.village1
        )
        view.tv_Pincode.text = LabelSet.getText(
            "pincode",
            R.string.pincode
        )
    }
}