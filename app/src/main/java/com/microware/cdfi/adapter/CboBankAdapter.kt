package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.BankDetailActivity
import com.microware.cdfi.activity.BankDetailListActivity
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.bankdetail_listitem.view.*
import java.io.File

class CboBankAdapter(var context : Context, var bankEntity: List<Cbo_bankEntity>): RecyclerView.Adapter<CboBankAdapter.ViewHolder>() {

    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.bankdetail_listitem, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return bankEntity.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var bankName = (context as BankDetailListActivity).getBankName(validate!!.returnIntegerValue(bankEntity.get(position).bank_id.toString()))
        var Branchname = (context as BankDetailListActivity).getBranchname(validate!!.returnIntegerValue(bankEntity.get(position).bank_branch_id.toString()))
        holder.tvbankname.text = bankName
        var defaultSatus = validate!!.returnIntegerValue(bankEntity.get(position).is_default.toString())
        if(defaultSatus == 1){
            holder.ivdefault.visibility = View.VISIBLE
        }else{
            holder.ivdefault.visibility = View.GONE
        }
        holder.tvbranch.text = Branchname
        holder.tvifsc.text = validate!!.returnStringValue(bankEntity.get(position).ifsc_code)
        holder.tvaccnum.text = validate!!.returnStringValue(bankEntity.get(position).account_no)
        holder.tvopDate.text = validate!!.convertDatetime(validate!!.returnLongValue(bankEntity.get(position).account_opening_date.toString()))
        if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) == 1){
            holder.ivDelete.visibility = View.GONE
        }else {
            holder.ivDelete.visibility = View.VISIBLE
        }

        holder.ivEdit.setOnClickListener {
            validate!!.SaveSharepreferenceInt(AppSP.ShgBankStatus,validate!!.returnIntegerValue(bankEntity.get(position).activation_status.toString()))
            validate!!.SaveSharepreferenceString(AppSP.BankGUID,validate!!.returnStringValue(bankEntity.get(position).bank_guid))
            var inetnt = Intent(context, BankDetailActivity::class.java)
            context.startActivity(inetnt)
        }
        val mediaStorageDirectory = File(
            context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
            AppSP.IMAGE_DIRECTORY_NAME
        )

        var mediaFile1 = File(mediaStorageDirectory.path + File.separator + validate!!.returnStringValue(
            bankEntity.get(position).passbook_firstpage))
        Picasso.with(context).load(mediaFile1).into(holder.ImgFrntpage)

        holder.ivDelete.setOnClickListener {
            if (validate!!.returnLongValue(bankEntity.get(position).last_uploaded_date.toString()) > 0
                || validate!!.returnLongValue(bankEntity.get(position).cbo_bank_id.toString()) > 0){
                (context as BankDetailListActivity).CustomAlert(bankEntity.get(position).bank_guid,1)
            }else {
                (context as BankDetailListActivity).CustomAlert(bankEntity.get(position).bank_guid,0)
            }
        }
        setLabelText(holder)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tvbankname = view.tvbankname
        var tvbranch = view.tvbranch
        var ivdefault = view.ivdefault
        var tvifsc = view.tvifsc
        var ImgFrntpage = view.ImgFrntpage
        var tvaccnum = view.tvaccnum
        var tvopDate = view.tvopDate
        var tvadefault = view.tvadefault
        var ivEdit = view.ivEdit
        var ivDelete = view.ivDelete
        var tv_bankName = view.tv_bankName
        var tv_bankBranch = view.tv_bankBranch
        var tvIFSC = view.tvIFSC
        var tvAccountNo = view.tvAccountNo
        var tvUploadPassbook = view.tvUploadPassbook
        var tv_AccountOpenDate = view.tv_AccountOpenDate
    }
    fun setLabelText(view: ViewHolder) {
        view.tv_bankName.text = LabelSet.getText(
            "bank_name",
            R.string.bank_name
        )
        view.tv_bankBranch.text = LabelSet.getText(
            "bank_branch",
            R.string.bank_branch
        )
        view.tvIFSC.text = LabelSet.getText(
            "ifsc_code",
            R.string.ifsc_code
        )
        view.tvAccountNo.text = LabelSet.getText(
            "account_no",
            R.string.account_no
        )
        view.tvUploadPassbook.text = LabelSet.getText(
            "upload_passbook_first_page",
            R.string.upload_passbook_first_page
        )
        view.tv_AccountOpenDate.text = LabelSet.getText(
            "opening_date",
            R.string.opening_date
        )
    }

}