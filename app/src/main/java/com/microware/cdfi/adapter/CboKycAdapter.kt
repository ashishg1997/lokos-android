package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.KYCDetailListActivity
import com.microware.cdfi.activity.KycDetailActivity
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.Cbo_kycEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.kycdetail_listitem.view.*

class CboKycAdapter(var context : Context,var kycEntity: List<Cbo_kycEntity>): RecyclerView.Adapter<CboKycAdapter.ViewHolder>() {

    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.kycdetail_listitem, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return kycEntity.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvcode.text = kycEntity.get(position).cbo_id.toString()
//        holder.tvkycid.setText(validate!!.returnStringValue(kycEntity.get(position).kyc_type.toString()))
        holder.tvkycnum.text = kycEntity.get(position).kyc_number
        var Doctype = (context as KYCDetailListActivity).getDocumentTYpeName(kycEntity.get(position).kyc_type)
        holder.tvkycid.text = Doctype

        holder.ivEdit.setOnClickListener {
            validate!!.SaveSharepreferenceString(AppSP.KYCGUID,kycEntity.get(position).kyc_guid)
            var intent = Intent(context, KycDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            context.startActivity(intent)
        }

        holder.ivDelete.setOnClickListener {
            CDFIApplication.database?.cbokycDao()?.deleteKycData(kycEntity.get(position).kyc_guid)
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tvcode = view.tvcode
        var tvkycid = view.tvkycid
        var tvkycnum = view.tvkycnum
        var tvvalidfrom = view.tvvalidfrom
        var tvvalidtill = view.tvvalidtill
        var ivEdit = view.ivEdit
        var ivDelete = view.ivDelete
    }

}