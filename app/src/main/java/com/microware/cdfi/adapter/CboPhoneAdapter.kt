package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.PhoneDetailActivity
import com.microware.cdfi.activity.PhoneDetailListActivity
import com.microware.cdfi.entity.Cbo_phoneEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.phone_listitem.view.*

class CboPhoneAdapter(var context : Context, var phoneEntity: List<Cbo_phoneEntity>): RecyclerView.Adapter<CboPhoneAdapter.ViewHolder>() {

    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.phone_listitem, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return phoneEntity.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var member_Name = (context as PhoneDetailListActivity).returnMemberName(validate!!.returnStringValue(
            phoneEntity.get(position).member_guid))
        holder.tvBelongs.text = member_Name
        holder.tvPhone.text = validate!!.returnStringValue(phoneEntity.get(position).mobile_no)
        if(validate!!.returnLongValue(phoneEntity.get(0).valid_from.toString())>0) {
            holder.tvValidform.text = validate!!.convertDatetime(phoneEntity.get(position).valid_from!!)
        }
        if(validate!!.returnLongValue(phoneEntity.get(0).valid_till.toString())>0) {
            holder.tvValidtill.text = validate!!.convertDatetime(phoneEntity.get(position).valid_till!!)
        }
        if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) == 1){
            holder.ivDelete.visibility = View.GONE
        }else {
            holder.ivDelete.visibility = View.VISIBLE
        }

        holder.ivEdit.setOnClickListener {
            validate!!.SaveSharepreferenceString(AppSP.PhoneGUID,phoneEntity.get(position).phone_guid)
            var inetnt = Intent(context, PhoneDetailActivity::class.java)
            context.startActivity(inetnt)
        }

        holder.ivDelete.setOnClickListener {
            if(validate!!.returnLongValue(phoneEntity.get(position).last_uploaded_date.toString())>0
                || validate!!.returnLongValue(phoneEntity.get(position).cbo_phone_id.toString())>0) {
                (context as PhoneDetailListActivity).CustomAlert(phoneEntity.get(position).phone_guid,1)
            }else {
                (context as PhoneDetailListActivity).CustomAlert(phoneEntity.get(position).phone_guid,0)
            }
        }
        setLabelText(holder)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tvPhone = view.tvPhone
        var tvBelongs = view.tvBelongs
        var tvstatus = view.tvstatus
        var tvValidform = view.tvValidform
        var tvValidtill = view.tvValidtill
        var tvDefault = view.tvDefault
        var ivEdit = view.ivEdit
        var ivDelete = view.ivDelete
        var tv_belongto = view.tv_belongto
        var tv_mob = view.tv_mob
    }
    fun setLabelText(view: ViewHolder) {
        view.tv_belongto.text = LabelSet.getText(
            "belongs_to_member",
            R.string.belongs_to_member
        )
        view.tv_mob.text = LabelSet.getText(
            "phone_no",
            R.string.phone_no
        )
    }
}