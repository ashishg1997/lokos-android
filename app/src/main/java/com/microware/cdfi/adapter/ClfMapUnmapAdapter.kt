package com.microware.cdfi.adapter

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vo.CLFMapCBOActivity
import com.microware.cdfi.activity.vo.VOMapCBOActivity
import com.microware.cdfi.api.model.ClfMappingDataModel
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.FedrationViewModel
import com.microware.cdfi.viewModel.LookupViewmodel
import kotlinx.android.synthetic.main.layout_date_dialog.*
import kotlinx.android.synthetic.main.layout_mapping_cardview.view.*

class ClfMapUnmapAdapter(var context: Context,
                         var listUnmapData: List<ClfMappingDataModel>?,
                         var listUnmap: List<ClfMappingDataModel>?,
                         var cbo_id:Long, var iFlag:Int,
                         var federationViewModel: FedrationViewModel,
                         var cbo_guid:String?,
                         var clfMapCboActivity: CLFMapCBOActivity,
                         var lookupViewmodel: LookupViewmodel?) :
    RecyclerView.Adapter<ClfMapUnmapAdapter.ViewHolder>() {

    var validate: Validate? = null
    var selected_date = 0L
    var dataspin_status: List<LookupEntity>? = null
    var dataspin_inactivestatus: List<LookupEntity>? = null
    var dataspin_settlement: List<LookupEntity>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.layout_mapping_cardview, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        if(listUnmapData.isNullOrEmpty()){
            return 0
        }else {
            return listUnmapData?.size!!
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvfederationName.text = validate!!.returnStringValue(listUnmapData!!.get(position).federation_name)
        holder.tvJoiningDate.text = validate!!.convertDatetime(
            validate!!.returnLongValue(listUnmapData?.get(position)?.federation_formation_date.toString()))

        holder.ivCheck.setOnCheckedChangeListener { buttonView, isChecked ->

            if (isChecked) {
                if(iFlag == 0){
                    OpenDateDialog(position)
                }else {
                    var ecCount = federationViewModel.getEcCount(
                        validate!!.returnLongValue(
                            listUnmapData?.get(position)?.federation_id.toString()
                        ), cbo_guid!!
                    )
                    if (ecCount == 0) {
                        OpenDateDialog(position)
                    }else {
                        var msg = LabelSet.getText(
                            "",
                            R.string.selected_shg_unmapped
                        )
                        validate!!.CustomAlert(msg,context as CLFMapCBOActivity)
                        holder.ivCheck.isChecked = false

                    }
                }
            } else {
                if (iFlag == 0) {
                    if (listUnmap != null) {
                        for (i in 0 until listUnmap?.size!!) {
                            if (listUnmap?.get(i)?.guid == listUnmapData?.get(position)?.guid) {
                                listUnmap?.get(i)?.parent_cbo_code = 0
                                listUnmap?.get(i)?.is_edited = 0
                                listUnmap?.get(i)?.is_active = 0

                            }

                        }
                    }
                } else {
                    if(listUnmap!=null){
                        for (i in 0 until listUnmap?.size!!) {
                            if (listUnmap?.get(i)?.guid == listUnmapData?.get(position)?.guid) {
                                listUnmap?.get(i)?.parent_cbo_code = cbo_id
                                listUnmap?.get(i)?.is_edited = 0
                                listUnmap?.get(i)?.is_active = 0
                                break
                            }
                        }
                    }
                }
            }


        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tvfederationName = view.tvfederationName
        var tvJoiningDate = view.tvJoiningDate
        var ivCheck = view.ivCheck


    }
    fun OpenDateDialog(position:Int) {

        val mDialogView =
            LayoutInflater.from(context).inflate(R.layout.layout_date_dialog, null)
        val mBuilder = android.app.AlertDialog.Builder(context)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.et_fromDate.setOnClickListener {
            validate!!.datePickerwithmindate(
                listUnmapData?.get(position)?.federation_formation_date!!,
                mAlertDialog.et_fromDate
            )
        }
        if(iFlag == 0){
            mAlertDialog.tv_fromDate.text = LabelSet.getText(
                "date_of_joining",
                R.string.date_of_joining
            )
        }else {
            mAlertDialog.tv_fromDate.text = LabelSet.getText(
                "date_leaving",
                R.string.date_leaving
            )
        }

        var current_dt = validate!!.Daybetweentime(validate!!.currentdatetime)
        mAlertDialog.et_fromDate.setText(validate!!.convertDatetime(
            validate!!.returnLongValue(current_dt.toString())))

        mAlertDialog.btn_ok.setOnClickListener {
            selected_date = validate!!.Daybetweentime(mAlertDialog.et_fromDate.text.toString())
            if (checkValidation(mAlertDialog) == 1){
                if(iFlag == 0){
                    if(listUnmap!=null) {
                        for (i in 0 until listUnmap?.size!!) {
                            if (listUnmap?.get(i)?.guid == listUnmapData?.get(position)?.guid) {
                                listUnmap?.get(i)?.parent_cbo_code = cbo_id
                                listUnmap?.get(i)?.cooption_date = selected_date
                                listUnmap?.get(i)?.federation_revival_date = 0
                                listUnmap?.get(i)?.is_edited = 1
                                listUnmap?.get(i)?.is_active = 1
                                listUnmap?.get(i)?.status = 1
                                listUnmap?.get(i)?.leaving_reason = 0
                                listUnmap?.get(i)?.settlement_status = 0
                                break
                            }

                        }
                    }
                }else {
                    if(listUnmap!=null){
                        for (i in 0 until listUnmap?.size!!) {
                            if (listUnmap?.get(i)?.guid == listUnmapData?.get(position)?.guid) {
                                listUnmap?.get(i)?.parent_cbo_code = 0
                                listUnmap?.get(i)?.cooption_date = 0
                                listUnmap?.get(i)?.federation_revival_date = selected_date
                                listUnmap?.get(i)?.is_edited = 1
                                listUnmap?.get(i)?.is_active = 0
                                listUnmap?.get(i)?.status = 2
                                listUnmap?.get(i)?.leaving_reason = validate?.returnlookupcode(mAlertDialog.spin_inactivereason,dataspin_inactivestatus)
                                listUnmap?.get(i)?.settlement_status = validate?.returnlookupcode(mAlertDialog.spin_settlementstatus,dataspin_settlement)
                                break
                            }

                        }
                    }
                }
            }
            mAlertDialog.dismiss()
        }

        fillSpinner(mAlertDialog)

    }

    private fun fillSpinner(mAlertDialog: AlertDialog?) {
        dataspin_status = lookupViewmodel!!.getlookup(
            5,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_inactivestatus = lookupViewmodel!!.getlookup(
            41,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        dataspin_settlement = lookupViewmodel!!.getlookup(
            27,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)
        )

        validate!!.fillspinner(clfMapCboActivity,mAlertDialog!!.spin_status,dataspin_status)
        validate!!.fillspinner(clfMapCboActivity,
            mAlertDialog.spin_inactivereason,dataspin_inactivestatus)
        validate!!.fillspinner(clfMapCboActivity,
            mAlertDialog.spin_settlementstatus,dataspin_settlement)
    }

    private fun checkValidation(mAlertDialog: AlertDialog?):Int{
        var value = 1
        if (mAlertDialog!!.spin_status.selectedItemPosition == 0 && mAlertDialog.layStatus.visibility == View.VISIBLE){
            validate!!.CustomAlertSpinner(
                clfMapCboActivity,
                mAlertDialog.spin_status,
                LabelSet.getText(
                    "select_status",
                    R.string.select_status
                )
            )
            value = 0
            return value
        }else if (mAlertDialog.spin_inactivereason.selectedItemPosition == 0){
            validate!!.CustomAlertSpinner(
                clfMapCboActivity,
                mAlertDialog.spin_inactivereason,
                LabelSet.getText(
                    "validleavingreason",
                    R.string.validleavingreason
                )
            )
            value = 0
            return value
        }else if (mAlertDialog.spin_settlementstatus.selectedItemPosition == 0){
            validate!!.CustomAlertSpinner(
                clfMapCboActivity,
                mAlertDialog.spin_settlementstatus,
                LabelSet.getText(
                    "validsettlementstatus",
                    R.string.validsettlementstatus
                )
            )
            value = 0
            return value
        }

        return value
    }

}