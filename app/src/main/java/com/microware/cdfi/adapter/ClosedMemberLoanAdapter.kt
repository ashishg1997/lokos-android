package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.CutOffClosedMemberLoan
import com.microware.cdfi.activity.meeting.CutOffClosedMemberLoanDetailList
import com.microware.cdfi.activity.meeting.CutOffClosedMemberLoanList
import com.microware.cdfi.api.meetingmodel.LoanListModel
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.loan_demand_summary_item.view.*

class ClosedMemberLoanAdapter(var context: Context, var list: List<LoanListModel>) :
    RecyclerView.Adapter<ClosedMemberLoanAdapter.ViewHolder>() {
    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view =
            LayoutInflater.from(context).inflate(R.layout.loan_demand_summary_item, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        setLabelText(holder)
        holder.tv_srno.visibility = View.VISIBLE
        holder.tv_srno.text = (position + 1).toString()
        holder.tv_memberName.text = list.get(position).member_name.toString()
        var totalLoanAmount = (context as CutOffClosedMemberLoanList).getTotalClosedLoanAmount(list.get(position).mem_id,list.get(position).mtg_no)
        var totalLoanCount = (context as CutOffClosedMemberLoanList).getTotalClosedLoanCount(list.get(position).mem_id,list.get(position).mtg_no)
        holder.tv_value2.visibility = View.GONE
        holder.tv_value1.text = totalLoanCount.toString()
        holder.tv_value2.text = "0"


        holder.tv_value3.text = totalLoanAmount.toString()

        holder.img_edit.isEnabled = true

        holder.tv_value3.setOnClickListener(View.OnClickListener {
            validate!!.SaveSharepreferenceLong(
                MeetingSP.MemberCode,
                list.get(position).group_m_code!!
            )
            validate!!.SaveSharepreferenceString(
                MeetingSP.MemberName,
                list.get(position).member_name!!
            )

            validate!!.SaveSharepreferenceInt(
                MeetingSP.Loanno,
                validate!!.returnIntegerValue(list.get(position).loan_no.toString())
            )

            /*  var intent = Intent(context, PrincipalDemandActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            context.startActivity(intent)*/
        })
        holder.img_edit.setOnClickListener(View.OnClickListener {
            validate!!.SaveSharepreferenceLong(MeetingSP.Memberid, list.get(position).mem_id!!)
            validate!!.SaveSharepreferenceLong(
                MeetingSP.MemberCode,
                list.get(position).group_m_code!!
            )
            validate!!.SaveSharepreferenceInt(
                MeetingSP.Loanno,
                validate!!.returnIntegerValue(list.get(position).loan_no.toString())
            )
            var inetnt = Intent(context, CutOffClosedMemberLoan::class.java)
            context.startActivity(inetnt)
        })

        holder.tv_value1.setOnClickListener(View.OnClickListener {
            validate!!.SaveSharepreferenceLong(MeetingSP.Memberid, list.get(position).mem_id!!)
            validate!!.SaveSharepreferenceLong(
                MeetingSP.MemberCode,
                list.get(position).group_m_code!!
            )
            validate!!.SaveSharepreferenceInt(
                MeetingSP.Loanno,
                validate!!.returnIntegerValue(list.get(position).loan_no.toString())
            )
            var inetnt = Intent(context, CutOffClosedMemberLoanDetailList::class.java)
            context.startActivity(inetnt)
        })

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_srno = view.tv_srno
        var tv_memberName = view.tv_memberName
        var tv_value1 = view.tv_value1
        var tv_value2 = view.tv_value2
        var tv_value3 = view.tv_value3
        var img_edit = view.img_edit

    }

    fun setLabelText(view: ViewHolder) {

    }
}