package com.microware.cdfi.adapter

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.CompulsorySavingActivity
import com.microware.cdfi.entity.DtmtgDetEntity
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.compulsary_saving_item.view.*

class CumpulsorySavingAdapter(var context: Context,
                              var list: List<DtmtgDetEntity>,val total:Int) :
    RecyclerView.Adapter<CumpulsorySavingAdapter.ViewHolder>() {
    var validate:Validate?=null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate= Validate(context)
        var view = LayoutInflater.from(context).inflate(R.layout.compulsary_saving_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_memberName.text = list.get(position).member_name.toString()
        holder.tv_srno.text = (position + 1).toString()

        holder.et_SavCompOb.setText(list.get(position).sav_comp_ob.toString())
        holder.et_GroupMCode.setText(list.get(position).group_m_code.toString())
        var memberAdjustment = (validate!!.returnIntegerValue((context as CompulsorySavingActivity).getAdjustmentSaving(list.get(position).mtg_no,list.get(position).mem_id,68).toString()))

        if(list.get(position).sav_comp!! > 0){
               holder.tv_count.setText(list.get(position).sav_comp.toString())
              // holder.tv_cum2.setText(savecomp.toString())
            var value=validate!!.returnIntegerValue(holder.tv_count.text.toString())
            var sav_comp_ob=validate!!.returnIntegerValue(list.get(position).sav_comp_ob.toString())
            holder.tv_cum2.text = (value+sav_comp_ob+memberAdjustment).toString()
            (context as CompulsorySavingActivity).getTotalValue(list.get(position).sav_comp!!,1)
            (context as CompulsorySavingActivity).getTotalValue(list.get(position).sav_comp_cb!!,2)


        }else{
            if(total==0){
                if(validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType)==0) {
                    if(validate!!.returnIntegerValue(list.get(position).attendance.toString())==2 ){
                        holder.tv_count.setText("0")
                    }else{
                        holder.tv_count.setText(
                            validate!!.RetriveSharepreferenceInt(MeetingSP.Month_comp_saving).toString()
                        )
                    }

                }else {
                    holder.tv_count.setText("0")
                }
                var value=validate!!.returnIntegerValue(holder.tv_count.text.toString())
                var sav_comp_ob=validate!!.returnIntegerValue(list.get(position).sav_comp_ob.toString())
                holder.tv_cum2.text = (value+sav_comp_ob+memberAdjustment).toString()
               // (context as CompulsorySavingActivity).getTotalValue()
                (context as CompulsorySavingActivity).getTotalValue(validate!!.returnIntegerValue(holder.tv_count.text.toString()),1)
                (context as CompulsorySavingActivity).getTotalValue(validate!!.returnIntegerValue(holder.tv_cum2.text.toString()),2)

            }else{
            holder.tv_count.setText("0")
                var savecomp=validate!!.returnIntegerValue(list.get(position).sav_comp_ob.toString())+validate!!.returnIntegerValue(list.get(position).sav_comp.toString())
                if(savecomp > 0){
                       // holder.tv_cum2.setText(savecomp.toString())
                    var value=validate!!.returnIntegerValue(holder.tv_count.text.toString())
                    var sav_comp_ob=validate!!.returnIntegerValue(list.get(position).sav_comp_ob.toString())

                    holder.tv_cum2.text = (value+sav_comp_ob+memberAdjustment).toString()
                    (context as CompulsorySavingActivity).getTotalValue(list.get(position).sav_comp_cb!!,2)

                }else{
                    holder.tv_cum2.text = "0"
                }
            }
        }

        if(validate!!.returnIntegerValue(list.get(position).attendance.toString())==2 && validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType)==0){
            holder.tv_memberName.setTextColor(context.resources.getColor(R.color.red))
        }else{
            holder.tv_memberName.setTextColor(context.resources.getColor(R.color.black))
        }


        setLabelText(holder)



        holder.tv_count.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                s.toString()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value=validate!!.returnIntegerValue(holder.tv_count.text.toString())
                var sav_comp_ob=validate!!.returnIntegerValue(list.get(position).sav_comp_ob.toString())
                holder.tv_cum2.text = (value+sav_comp_ob+memberAdjustment).toString()
                (context as CompulsorySavingActivity).getTotalValue()
            }

        })

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_srno = view.tv_srno
        var tv_memberName = view.tv_memberName
        var tv_count = view.tv_count
        var tv_cum2 = view.tv_cum2
        var et_GroupMCode = view.et_GroupMCode
        var et_SavCompOb = view.et_SavCompOb

    }

    fun setLabelText(view: ViewHolder) {

    }
}