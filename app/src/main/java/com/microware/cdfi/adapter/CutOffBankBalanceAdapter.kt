package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.CutOffBankBalanceList
import com.microware.cdfi.activity.meeting.CutOffGroupBankBalanceActivity
import com.microware.cdfi.entity.DtMtgFinTxnEntity
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.layout_cutoff_bank_balance_list_item.view.*

class CutOffBankBalanceAdapter(var context: Context, var list: List<DtMtgFinTxnEntity>) :
    RecyclerView.Adapter<CutOffBankBalanceAdapter.ViewHolder>() {
    var validate: Validate? = null
    var amount = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.layout_cutoff_bank_balance_list_item, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_srno.text = (position + 1).toString()
            val bankName = (context as CutOffBankBalanceList).setaccount(list[position].bank_code)
            if (!bankName.isNullOrEmpty()) {
                holder.tv_bankName.text = bankName
            }

        if (validate!!.returnIntegerValue(list[position].closing_balance.toString()) > 0) {
            holder.tv_amount.text = "" + list[position].closing_balance
        }

        holder.ivEdit.setOnClickListener {

         validate!!.SaveSharepreferenceString(MeetingSP.bankCode,list[position].bank_code)
            val intent = Intent(context, CutOffGroupBankBalanceActivity::class.java)
            context.startActivity(intent)
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_srno = view.tv_srno
        var tv_bankName = view.tv_bankName
      //  var tv_receiptType = view.tv_receiptType
        var tv_amount = view.tv_amount
        var ivEdit = view.ivEdit
    }
}