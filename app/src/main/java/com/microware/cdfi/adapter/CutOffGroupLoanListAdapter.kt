package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.CutOffGroupLoanlist
import com.microware.cdfi.activity.meeting.GroupPrincipalDemandActivity
import com.microware.cdfi.entity.DtLoanGpEntity
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.cut_off_loanlist_detail_item.view.*

class CutOffGroupLoanListAdapter (
    var context: Context,
    val list: List<DtLoanGpEntity>
) :
    RecyclerView.Adapter<CutOffGroupLoanListAdapter.ViewHolder>() {
    var validate: Validate?=null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate= Validate(context)
        var view = LayoutInflater.from(context).inflate(R.layout.cut_off_loanlist_detail_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        setLabelText(holder)
        var disburseamt = 0
        if(list.get(position).completion_flag == false) {
            disburseamt = validate!!.returnIntegerValue(list.get(position).amount.toString())
        }else if(list.get(position).completion_flag == true) {
            disburseamt = validate!!.returnIntegerValue(list.get(position).orignal_loan_amount.toString())
        }
        var disbursement_date = validate!!.convertDatetime(validate!!.returnLongValue(list.get(position).disbursement_date.toString()))
        holder.tv_disbursement_date.text = disbursement_date
        holder.tv_source.text = (context as CutOffGroupLoanlist).returnLookupValue(
            91,validate!!.returnIntegerValue(list.get(position).institution.toString()))
        holder.tv_fund_type.text = (context as CutOffGroupLoanlist).setFundName(
            validate!!.returnIntegerValue(list.get(position).institution.toString()),validate!!.returnIntegerValue(list.get(position).loan_product_id.toString()))
        holder.tv_disbursed_amount.text = disburseamt.toString()
        (context as CutOffGroupLoanlist).getTotalValue(disburseamt,3)


        holder.cardView.setOnClickListener {
            if(list.get(position).completion_flag == false) {
                validate!!.SaveSharepreferenceString(
                    MeetingSP.Institution,
                    validate!!.returnStringValue(holder.tv_source.text.toString())
                )
                validate!!.SaveSharepreferenceInt(
                    MeetingSP.Loanno,
                    validate!!.returnIntegerValue(list.get(position).loan_no.toString())
                )
                var intent = Intent(context, GroupPrincipalDemandActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                context.startActivity(intent)
            }else {
                validate!!.CustomAlert(LabelSet.getText("closed_loan",R.string.closed_loan),context as CutOffGroupLoanlist)
            }
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val cardView=view.cardView
        val tv_source=view.tv_source
        val tv_disbursement_date=view.tv_disbursement_date
        // val tv_loans=view.tv_loans
        val tv_fund_type=view.tv_fund_type
        val tv_disbursed_amount=view.tv_disbursed_amount
        val img_edit=view.img_edit
    }

    fun setLabelText(view: ViewHolder) {

    }
}