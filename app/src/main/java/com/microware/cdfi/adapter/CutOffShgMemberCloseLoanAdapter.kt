package com.microware.cdfi.adapter

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.CompulsorySavingActivity
import com.microware.cdfi.activity.meeting.CutOffShgMemberCloseLoan
import com.microware.cdfi.api.meetingmodel.LoanListModel
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.cutoff_shg_memberclose_loan_item.view.*

class CutOffShgMemberCloseLoanAdapter(var context: Context,
                                      var list: List<LoanListModel>) :
    RecyclerView.Adapter<CutOffShgMemberCloseLoanAdapter.ViewHolder>() {
    var validate: Validate?=null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate= Validate(context)
        var view = LayoutInflater.from(context).inflate(R.layout.cutoff_shg_memberclose_loan_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_memberName.text = list.get(position).member_name.toString()
        holder.tv_srno.text = (position + 1).toString()

        holder.et_LoanNo.setText(list.get(position).loan_no.toString())
        holder.et_memId.setText(list.get(position).mem_id.toString())

        var totalLoanAmount = (context as CutOffShgMemberCloseLoan).getTotalClosedLoanAmount(list.get(position).mem_id,list.get(position).mtg_no)
        var totalLoanCount = (context as CutOffShgMemberCloseLoan).getTotalClosedLoanCount(list.get(position).mem_id,list.get(position).mtg_no)

        holder.et_totalLoan.setText(totalLoanCount.toString())
        holder.et_totalAmount.setText(totalLoanAmount.toString())

        (context as CutOffShgMemberCloseLoan).getTotalValue(totalLoanCount,1)
        (context as CutOffShgMemberCloseLoan).getTotalValue(totalLoanAmount,2)


        holder.et_totalLoan.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                (context as CutOffShgMemberCloseLoan).getTotalLoanValue()
                if (validate!!.returnIntegerValue(s.toString()) == 0){
                    holder.et_totalAmount.setText("0")
                }
            }

        })

        holder.et_totalAmount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                (context as CutOffShgMemberCloseLoan).getTotalAmountValue()
            }

        })

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_srno = view.tv_srno
        var tv_memberName = view.tv_memberName
        var et_totalLoan = view.et_totalLoan
        var et_totalAmount = view.et_totalAmount
        var et_LoanNo = view.et_LoanNo
        var et_memId = view.et_memId

    }

}