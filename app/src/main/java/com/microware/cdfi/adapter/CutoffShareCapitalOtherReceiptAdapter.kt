package com.microware.cdfi.adapter

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.CutOffShareCapitalOtherReceiptListActivity
import com.microware.cdfi.api.meetingmodel.ShareCapitalModel
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.cut_off_share_capital_receipt_list_item.view.*

class CutoffShareCapitalOtherReceiptAdapter(
    var context: Context,
    var list: List<ShareCapitalModel>
) :
    RecyclerView.Adapter<CutoffShareCapitalOtherReceiptAdapter.ViewHolder>() {
    var validate: Validate? = null
    var amount = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.cut_off_share_capital_receipt_list_item, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_srno.text = (position + 1).toString()
        holder.tv_member_name.text = list[position].member_name.toString()
        holder.tv_memid.setText(list[position].mem_id.toString())
        (context as CutOffShareCapitalOtherReceiptListActivity).getTotalValue(
            validate!!.returnIntegerValue(
                list[position].amount.toString()
            )
        )

        if (validate!!.returnIntegerValue(list[position].amount.toString()) == 0) {
            holder.tv_amount.setText("0")
        } else {
            holder.tv_amount.setText(validate!!.returnStringValue(list[position].amount.toString()))
        }

        holder.tv_amount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                s.toString()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                (context as CutOffShareCapitalOtherReceiptListActivity).getTotalValue()
            }

        })

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_srno = view.tv_srno
        var tv_member_name = view.tv_member_name
        var tv_amount = view.tv_count
        var tv_memid = view.tv_memid
    }

}