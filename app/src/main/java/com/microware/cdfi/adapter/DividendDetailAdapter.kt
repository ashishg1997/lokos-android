package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import kotlinx.android.synthetic.main.dividend_detail_item.view.*

class DividendDetailAdapter(var context: Context) :
    RecyclerView.Adapter<DividendDetailAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.dividend_detail_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return 7
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        setLabelText(holder)

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_memberName=view.tv_memberName
        val tv_srno=view.tv_srno
        val tv_Withdrawl=view.tv_Withdrawl
    }

    fun setLabelText(view: ViewHolder) {

    }
}