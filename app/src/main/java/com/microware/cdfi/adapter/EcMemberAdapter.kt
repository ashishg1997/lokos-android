package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vo.VOEcActivity
import com.microware.cdfi.activity.vo.VoEcListActivity
import com.microware.cdfi.entity.Executive_memberEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.ExecutiveMemberViewmodel
import kotlinx.android.synthetic.main.ec_member__adapter.view.*
import kotlinx.android.synthetic.main.ecmember_card.view.*

class EcMemberAdapter(var context : Context, var executiveEntity: List<Executive_memberEntity>,var executiveViewmodel: ExecutiveMemberViewmodel): RecyclerView.Adapter<EcMemberAdapter.ViewHolder>() {

    var validate: Validate? = null
    var cboName = ""
    var cboGuid = ""
    var memberName = ""
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.ecmember_card, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return executiveEntity.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if(validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType)==2){
            cboName = (context as VoEcListActivity).setVoName(validate!!.returnLongValue(
                executiveEntity.get(position).ec_cbo_code.toString()))
        }else if(validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType)==1){
            cboName = (context as VoEcListActivity).setShgName(validate!!.returnLongValue(
                executiveEntity.get(position).ec_cbo_code.toString()))
        }

        var executive_count = executiveViewmodel.getExecutive_count(validate!!.returnStringValue(
            executiveEntity.get(position).cbo_guid),validate!!.returnLongValue(
            executiveEntity.get(position).ec_cbo_code.toString()))
        holder.tv_ShgName.text = cboName
        holder.tvTotal.text = LabelSet.getText(
            "total",
            R.string.total
        )+":"+" "+ validate!!.returnStringValue(executive_count.toString())
        holder.tv_Name.text = LabelSet.getText(
            "name",
            R.string.name
        )
        holder.tv_nomination.text = LabelSet.getText(
            "nomination",
            R.string.nomination
        )
        holder.tvPost.text = LabelSet.getText(
            "post_vo",
            R.string.post_vo
        )

        holder.add_ec.setOnClickListener {
            var stringValue = LabelSet.getText(
                "cbo_max5_member_add",
                R.string.cbo_max5_member_add
            )
            var EcCboCode = validate!!.returnLongValue(executiveEntity.get(position).ec_cbo_code.toString())
            var cboMemberCount = (context as VoEcListActivity).getCboMemberCount(EcCboCode)
            if(cboMemberCount<5) {
                validate!!.SaveSharepreferenceString(AppSP.ECMemGuid, "")
                validate!!.SaveSharepreferenceLong(AppSP.EcCboCode, EcCboCode)
                var inetnt = Intent(context, VOEcActivity::class.java)
                context.startActivity(inetnt)
            }else {
                validate!!.CustomAlert(stringValue, context as VoEcListActivity)
            }
        }
        holder.tbl_shgName.setOnClickListener{
            holder.imageArrow.rotation=180f

            if (holder.linear_member.visibility==VISIBLE)
            {
                holder.linear_member.visibility=GONE
                holder.imageArrow.rotation=0f

            }else{

                holder.linear_member.visibility=VISIBLE



            }
        }

        var executivelist = executiveViewmodel.getExecutiveMemberbyCode(validate!!.returnStringValue(
            executiveEntity.get(position).cbo_guid),validate!!.returnLongValue(
            executiveEntity.get(position).ec_cbo_code.toString()))

        setData(holder.lyChildList,executivelist,holder.linear_member,holder.imageArrow)

    }

    private fun setData(
        linearChildList: LinearLayout,
        executivelist: List<Executive_memberEntity>?,
        lay_Member: LinearLayout,
        img_Arrow: ImageView
    ) {

        linearChildList.removeAllViews()
        for (i in 0 until executivelist!!.size) {
            val mInflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val convertView: View =
                mInflater.inflate(R.layout.ec_member__adapter, linearChildList, false)
            var memberCode = validate!!.returnLongValue(executivelist.get(i).ec_member_code.toString())
            if(validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType)==1){
                cboGuid = (context as VoEcListActivity).getShg_guid(
                    validate!!.returnLongValue(executivelist.get(i).ec_cbo_code.toString())
                )
                memberName = (context as VoEcListActivity).setMemberName(memberCode,cboGuid)
            }else if(validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType)==2){
                cboGuid = (context as VoEcListActivity).getVo_guid(
                    validate!!.returnLongValue(executivelist.get(i).ec_cbo_code.toString())
                )
                memberName = (context as VoEcListActivity).setClfMemberName(memberCode,cboGuid)
            }


            var  designation = (context as VoEcListActivity).getLookupValue(validate!!.returnIntegerValue(executivelist.get(i).designation.toString()))
            linearChildList.addView(convertView)

            convertView.tvmember_name.text = memberName

            convertView.tvdateValue.text = validate!!.convertDatetime(validate!!.returnLongValue(
                executivelist.get(i).joining_date.toString()))
            convertView.tvPostvalue.text = designation

            var issignatory  =  validate!!.returnIntegerValue(executivelist.get(i).is_signatory.toString())
            if(issignatory == 1){
                convertView.iv_isSignatory.visibility = VISIBLE
            }else{
                convertView.iv_isSignatory.visibility = INVISIBLE
            }
            convertView.ivEdit.setOnClickListener {
                validate!!.SaveSharepreferenceString(AppSP.ECMemGuid,executivelist.get(i).guid)
                var inetnt = Intent(context, VOEcActivity::class.java)
                context.startActivity(inetnt)
            }

            convertView.ivDelete.setOnClickListener {
                var ecScCount = executiveViewmodel.getScMember_count(executivelist.get(i).guid,validate!!.returnLongValue(executivelist.get(i).ec_member_code.toString()))
                if(ecScCount==0) {
                    if (validate!!.returnLongValue(executivelist.get(i).last_uploaded_date.toString()) > 0) {
                        (context as VoEcListActivity).CustomAlert(
                            executivelist.get(i).guid,
                            1
                        )
                    } else {
                        (context as VoEcListActivity).CustomAlert(
                            executivelist.get(i).guid,
                            0
                        )
                    }
                    lay_Member.visibility= View.GONE
                    img_Arrow.rotation=0f
                }else {
                    validate!!.CustomAlert(LabelSet.getText(
                        "ec_not_deleted",
                        R.string.ec_not_deleted
                    ),context as VoEcListActivity)
                }
            }
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tv_ShgName = view.tv_ShgName
        var tvTotal = view.tvTotal
        var tv_Name = view.tv_Name
        var tv_nomination = view.tv_nomination
        var tvPost = view.tvPost
        var lyChildList = view.lyChildList
        var tblHeading = view.tblHeading
        var linear_member = view.linear_member
        var tbl_shgName = view.tbl_shgName
        var imageArrow = view.imageArrow
        var add_ec = view.add_ec
    }
}