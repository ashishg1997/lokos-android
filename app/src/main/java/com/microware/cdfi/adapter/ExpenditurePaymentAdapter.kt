package com.microware.cdfi.adapter
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.ExpenditurePaymentSummeryDetailActivity
import com.microware.cdfi.activity.meeting.ExpenditurePaymentSummeryListActivity
import com.microware.cdfi.entity.ShgFinancialTxnDetailEntity
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.expenditure_payment_list_item.view.*

class ExpenditurePaymentAdapter(
    var context: Context,
    var list: List<ShgFinancialTxnDetailEntity>
) :
    RecyclerView.Adapter<ExpenditurePaymentAdapter.ViewHolder>() {
    var validate: Validate? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        val view = LayoutInflater.from(context)
            .inflate(R.layout.expenditure_payment_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_amount.text = list[position].amount.toString()
        val recyptName = (context as ExpenditurePaymentSummeryListActivity).getRecepitType(
            validate!!.returnIntegerValue(list[position].auid.toString())
        )

        var source = (context as ExpenditurePaymentSummeryListActivity).getValue(validate!!.returnIntegerValue(list[position].amount_to_from.toString()),75)
        if(!source.isNullOrEmpty()){
            holder.tv_source.text = source
        }
        if (!recyptName.isNullOrEmpty()) {
            holder.tv_particulars.text = recyptName
        }

//        if (list.size - 1 == position) {
//            (context as ExpenditurePaymentSummeryListActivity).getTotalValue()
//        }

        (context as ExpenditurePaymentSummeryListActivity).getTotalValue1(validate!!.returnIntegerValue(list[position].amount.toString()))

        holder.ivEdit.setOnClickListener {
//            validate!!.SaveSharepreferenceString(MeetingSP.mtg_guid, list[position].mtg_guid)
//            validate!!.SaveSharepreferenceLong(MeetingSP.shgid, list[position].cbo_id)
//            validate!!.SaveSharepreferenceInt(MeetingSP.currentmeetingnumber, list[position].mtg_no)
            validate!!.SaveSharepreferenceInt(
                MeetingSP.Auid, validate!!.returnIntegerValue(list[position].auid.toString())
            )
            validate!!.SaveSharepreferenceInt(
                MeetingSP.savingSource, validate!!.returnIntegerValue(list[position].amount_to_from.toString())
            )
            val intent = Intent(context, ExpenditurePaymentSummeryDetailActivity::class.java)
            context.startActivity(intent)
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_particulars = view.tv_particulars
        var tv_source = view.tv_source
        var tv_amount = view.tv_amount
        var ivEdit = view.ivEdit
    }
}