package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.GroupLoanlist
import com.microware.cdfi.activity.meeting.GroupPrincipalDemandActivity
import com.microware.cdfi.entity.DtLoanGpEntity
import com.microware.cdfi.utility.*
import kotlinx.android.synthetic.main.loanlist_detail_item.view.*

class GroupLoanListAdapter(
    var context: Context,
    val list: List<DtLoanGpEntity>
) :
    RecyclerView.Adapter<GroupLoanListAdapter.ViewHolder>() {
var validate:Validate?=null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate=Validate(context)
        var view = LayoutInflater.from(context).inflate(R.layout.loanlist_detail_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        setLabelText(holder)

        val santionamt=validate!!.returnIntegerValue(list.get(position).sanctioned_amount.toString())
        val cclimit=validate!!.returnIntegerValue(list.get(position).drawing_limit.toString())
        val disburseamt=validate!!.returnIntegerValue(list.get(position).amount.toString())
        holder.tv_value1.text = santionamt.toString()
        (context as GroupLoanlist).getTotalValue(santionamt,1)
        holder.tv_source.text = (context as GroupLoanlist).returnloanlist(91,validate!!.returnIntegerValue(list.get(position).loan_no.toString()))
        holder.tv_value2.text = cclimit.toString()
        holder.tv_value3.text = disburseamt.toString()
        (context as GroupLoanlist).getTotalValue(cclimit,2)
        (context as GroupLoanlist).getTotalValue(disburseamt,3)


        holder.cardView.setOnClickListener {
            validate!!.SaveSharepreferenceString(MeetingSP.Institution, validate!!.returnStringValue(holder.tv_source.text.toString()))
            validate!!.SaveSharepreferenceInt(MeetingSP.Loanno, validate!!.returnIntegerValue(list.get(position).loan_no.toString()))
            var intent = Intent(context, GroupPrincipalDemandActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            context.startActivity(intent)
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val cardView=view.cardView
        val tv_source=view.tv_source
        val tv_value1=view.tv_value1
       // val tv_loans=view.tv_loans
        val tv_value2=view.tv_value2
        val tv_value3=view.tv_value3
        val img_edit=view.img_edit
    }

    fun setLabelText(view: ViewHolder) {

    }
}