package com.microware.cdfi.adapter

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.GroupPrincipalDemandActivity
import com.microware.cdfi.entity.DtMtgGrpLoanScheduleEntity
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.groupprincipal_demand_detail_item.view.*

class GroupPrincipalDemandAdapter(
    var context: Context,
    val list: List<DtMtgGrpLoanScheduleEntity>?
) :
    RecyclerView.Adapter<GroupPrincipalDemandAdapter.ViewHolder>() {
    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        var view = LayoutInflater.from(context)
            .inflate(R.layout.groupprincipal_demand_detail_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        setLabelText(holder)
        holder.tv_installment_no_value.text = list!!.get(position).installment_no.toString()
        holder.tv_installment_date_value.text =
            validate!!.convertDatetime(list.get(position).installment_date)
        holder.tv_principal_demand.text = list.get(position).principal_demand.toString()
        holder.et_principal_demand.setText(list.get(position).principal_demand.toString())
        holder.tv_loanos_value.text = list.get(position).loan_os.toString()
            (context as GroupPrincipalDemandActivity).getTotalValue(list.get(position).principal_demand!!,1)
            (context as GroupPrincipalDemandActivity).getTotalValue(list.get(position).principal_demand!!,2)

        holder.et_principal_demand.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                (context as GroupPrincipalDemandActivity).getTotalValue()
            }

        })

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var cardView = view.cardView
        var et_principal_demand = view.et_principal_demand
        var tv_principal_demand = view.tv_principal_demand
        var tv_installment_date_value = view.tv_installment_date_value
        var tv_installment_no_value = view.tv_installment_no_value
        var tv_loanos_value = view.tv_loanos_value

    }

    fun setLabelText(view: ViewHolder) {

    }
}