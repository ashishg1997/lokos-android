package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.GroupRepaymentDetailActivity
import com.microware.cdfi.activity.meeting.LoanWiseRepaymentByGroupActivity
import com.microware.cdfi.utility.*
import com.microware.cdfi.entity.DtLoanGpTxnEntity
import kotlinx.android.synthetic.main.grouprepayment_detail_item.view.*

class GroupRepaymentDetailAdapter(
    var context: Context,
    val list: List<DtLoanGpTxnEntity>
) :
    RecyclerView.Adapter<GroupRepaymentDetailAdapter.ViewHolder>() {
    var validate:Validate?=null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate=Validate(context)
        var view = LayoutInflater.from(context).inflate(R.layout.grouprepayment_detail_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        setLabelText(holder)

        val totpayble=validate!!.returnIntegerValue(list.get(position).principal_demand_ob.toString())+validate!!.returnIntegerValue(list.get(position).principal_demand.toString())
        val totintrest=validate!!.returnIntegerValue(list.get(position).int_accrued_op.toString())+validate!!.returnIntegerValue(list.get(position).int_accrued.toString())
        var todaysPayable=totintrest+totpayble
        holder.tv_value1.text = todaysPayable.toString()
        (context as GroupRepaymentDetailActivity).getTotalValue(totintrest+totpayble,1)
        holder.tv_source.text = (context as GroupRepaymentDetailActivity).returnloanlist(66,validate!!.returnIntegerValue(list.get(position).loan_no.toString()))

        holder.tv_value2.text = (validate!!.returnIntegerValue(list.get(position).loan_paid.toString())+validate!!.returnIntegerValue(list.get(position).loan_paid_int.toString())).toString()


        (context as GroupRepaymentDetailActivity).getTotalValue((validate!!.returnIntegerValue(list.get(position).loan_paid.toString())+validate!!.returnIntegerValue(list.get(position).loan_paid_int.toString())),2)

        var nextdemand = (context as GroupRepaymentDetailActivity).getnextdemand(
            validate!!.returnLongValue(
                list.get(position).cbo_id.toString()
            ), validate!!.returnIntegerValue(list.get(position).loan_no.toString())
        )
        (context as GroupRepaymentDetailActivity).getTotalValue(nextdemand, 3)

        holder.tv_value3.text = nextdemand.toString()

        if (todaysPayable > validate!!.returnIntegerValue(holder.tv_value2.text.toString())) {
            holder.tv_value2.setTextColor(context.getColor(R.color.meetingtabloancolor))
            holder.tv_value3.setTextColor(context.getColor(R.color.meetingtabloancolor))
        } else if (todaysPayable>0 && todaysPayable <= validate!!.returnIntegerValue(holder.tv_value2.text.toString())) {
            holder.tv_value2.setTextColor(context.getColor(R.color.Lightgreen))
            holder.tv_value3.setTextColor(context.getColor(R.color.colorAccent))

        } else {
            holder.tv_value2.setTextColor(context.getColor(R.color.colorAccent))
            holder.tv_value3.setTextColor(context.getColor(R.color.colorAccent))

        }
        holder.cardView.setOnClickListener {
            validate!!.SaveSharepreferenceString(MeetingSP.Institution, validate!!.returnStringValue(holder.tv_source.text.toString()))
            var loan_source = (context as GroupRepaymentDetailActivity).returnloanlist(66,validate!!.returnIntegerValue(list.get(position).loan_no.toString()))
            validate!!.SaveSharepreferenceString(MeetingSP.loanSource, loan_source)
            validate!!.SaveSharepreferenceInt(MeetingSP.Loanno, validate!!.returnIntegerValue(list.get(position).loan_no.toString()))
            var intent = Intent(context, LoanWiseRepaymentByGroupActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            context.startActivity(intent)
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val cardView=view.cardView
        val tv_source=view.tv_source
        val tv_value1=view.tv_value1
        // val tv_loans=view.tv_loans
        val tv_value2=view.tv_value2
        val tv_value3=view.tv_value3
        val img_edit=view.img_edit
    }

    fun setLabelText(view: ViewHolder) {

    }
}