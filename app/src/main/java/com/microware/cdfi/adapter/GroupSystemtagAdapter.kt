package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.GroupSystemTag
import com.microware.cdfi.activity.GroupSystemTagList
import com.microware.cdfi.entity.SystemTagEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.group_system_tagitem.view.*

class GroupSystemtagAdapter(var context: Context, var SystemTagEntity: List<SystemTagEntity>) :
    RecyclerView.Adapter<GroupSystemtagAdapter.ViewHolder>() {

    var validate: Validate? = null

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tv_system_tag_id = view.tv_system_tag_id
        var tv_id = view.tv_id

        var ivEdit = view.ivEdit
        var cardView = view.cardView
        var ivDelete = view.ivDelete
        var tvSystemID = view.tvSystemID
        var tvID = view.tvID
    }
    fun setLabelText(view: ViewHolder) {
        view.tvSystemID.text = LabelSet.getText(
            "system_tag_id",
            R.string.system_tag_id
        )
        view.tvID.text = LabelSet.getText("id", R.string.id)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        var view =
            LayoutInflater.from(context).inflate(R.layout.group_system_tagitem, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return SystemTagEntity.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var systemid = (context as GroupSystemTagList).returnlookupvalue(validate!!.returnIntegerValue(SystemTagEntity.get(position).system_type.toString()))
        holder.tv_system_tag_id.text = systemid
        holder.tv_id.text = validate!!.returnStringValue(SystemTagEntity.get(position).system_id.toString())

        if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) == 1){
            holder.ivDelete.visibility = View.GONE
        }else {
            holder.ivDelete.visibility = View.VISIBLE
        }

        holder.ivDelete.setOnClickListener {
            if(validate!!.returnLongValue(SystemTagEntity.get(position).last_uploaded_date.toString())>0
                || validate!!.returnLongValue(SystemTagEntity.get(position).systemtags_id.toString())>0) {
                (context as GroupSystemTagList).CustomAlert(SystemTagEntity.get(position).system_tag_guid,1)
            }else {
                (context as GroupSystemTagList).CustomAlert(SystemTagEntity.get(position).system_tag_guid,0)
            }
        }
        holder.ivEdit.setOnClickListener {
            validate!!.SaveSharepreferenceString(AppSP.SystemtagGUID,SystemTagEntity.get(position).system_tag_guid)
            var intent = Intent(context,GroupSystemTag::class.java)
            context.startActivity(intent)
        }
        setLabelText(holder)

    }

}