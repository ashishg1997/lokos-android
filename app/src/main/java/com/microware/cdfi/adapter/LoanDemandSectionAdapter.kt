package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.LoanDemandActivity
import com.microware.cdfi.activity.meeting.LoanDemandSectionActivity
import com.microware.cdfi.activity.meeting.LoanDemandSummaryActivity
import com.microware.cdfi.activity.meeting.LoanDisbursementActivity
import com.microware.cdfi.entity.DtLoanApplicationEntity
import com.microware.cdfi.entity.DtmtgDetEntity
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.loan_demand_section_item.view.*

class LoanDemandSectionAdapter(var context: Context, var list: List<DtLoanApplicationEntity>) :
    RecyclerView.Adapter<LoanDemandSectionAdapter.ViewHolder>() {
    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.loan_demand_section_item, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        setLabelText(holder)
        var post=(context as LoanDemandSectionActivity).returnpost(list.get(position).mem_id)
        if(post==2||post==3||post==1){
            var postname=(context as LoanDemandSectionActivity).getlookupValue(post,1)
            holder.tv_memberName.text = (context as LoanDemandSectionActivity).setMember(
                list.get(
                    position
                ).mem_id
            )+" ("+postname+")"
        }else{
            holder.tv_memberName.text = (context as LoanDemandSectionActivity).setMember(
                list.get(
                    position
                ).mem_id
            )
        }
        /*holder.tv_memberName.text = (context as LoanDemandSectionActivity).setMember(
            list.get(
                position
            ).mem_id
        )*/
        holder.tv_value2.text = list.get(position).amt_demand.toString()
        holder.tv_value3.text = validate!!.convertDatetime(list.get(position).tentative_date)
        holder.tv_requestdate.text = validate!!.convertDatetime(list.get(position).request_date)
        holder.tv_value1.text = (context as LoanDemandSectionActivity).returnvalue(validate!!.returnIntegerValue(list.get(position).loan_purpose.toString()),67)
        holder.tv_value4.text = list.get(position).loan_request_priority.toString()
       // holder.tv_srno.setText((position + 1).toString())
        holder.cardView.setOnClickListener(View.OnClickListener {
            validate!!.SaveSharepreferenceLong(MeetingSP.Loanappid,validate!!.returnLongValue(list.get(position).loan_application_id.toString()))
            validate!!.SaveSharepreferenceLong(MeetingSP.Memberid,validate!!.returnLongValue(list.get(position).mem_id.toString()))
            var inetnt = Intent(context, LoanDemandActivity::class.java)
            context.startActivity(inetnt)
        })

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var cardView = view.cardView
        var tv_memberName = view.tv_memberName
        var tv_value1 = view.tv_value1
        var tv_value2 = view.tv_value2
        var tv_value3 = view.tv_value3
        var tv_requestdate = view.tv_requestdate
        var tv_value4 = view.tv_value4

    }

    fun setLabelText(view: ViewHolder) {

    }
}