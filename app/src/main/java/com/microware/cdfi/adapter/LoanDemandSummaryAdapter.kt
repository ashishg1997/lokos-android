package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.LoanDemandSummaryActivity
import com.microware.cdfi.activity.meeting.LoanDisbursementActivity
import com.microware.cdfi.activity.meeting.PrincipalDemandActivity
import com.microware.cdfi.api.meetingmodel.LoanListModel
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.activity_loan_demand_summary.*
import kotlinx.android.synthetic.main.loan_demand_summary_item.view.*

class LoanDemandSummaryAdapter(var context: Context, var list: List<LoanListModel>) :
    RecyclerView.Adapter<LoanDemandSummaryAdapter.ViewHolder>() {
    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view =
            LayoutInflater.from(context).inflate(R.layout.loan_demand_summary_item, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        setLabelText(holder)
        holder.tv_srno.text = (position + 1).toString()
        var post=(context as LoanDemandSummaryActivity).returnpost(list.get(position).mem_id!!)
        if(post==2||post==3||post==1){
            var postname=(context as LoanDemandSummaryActivity).getlookupValue(post,1)
            holder.tv_memberName.text = list.get(position).member_name.toString()+" ("+postname+")"
        }else{
            holder.tv_memberName.text = list.get(position).member_name.toString()
        }
        if (validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) == 11 || validate!!.RetriveSharepreferenceInt(
                MeetingSP.MeetingType
            ) == 12
        ) {
            holder.tv_value1.visibility = View.GONE
            holder.tv_value2.visibility = View.GONE
            holder.tv_value1.text = "0"
            holder.tv_value2.text = "0"
        } else {
            holder.tv_value1.visibility = View.VISIBLE
            holder.tv_value2.visibility = View.VISIBLE
            holder.tv_value1.text = "0"
            holder.tv_value2.text =
                validate!!.returnIntegerValue(list.get(position).amt_demand.toString()).toString()
        }

        holder.tv_value3.text =
            validate!!.returnIntegerValue(list.get(position).amt_disbursed.toString()).toString()
        if(validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) ==0){

            holder.img_edit.isEnabled = !(validate!!.returnIntegerValue(list.get(position).amt_demand.toString()) == 0 && validate!!.returnIntegerValue(
                holder.tv_value3.text.toString()
            ) == 0)
        }else if(validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) ==11 || validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) ==12){
            holder.img_edit.isEnabled = true
        }

        if (list.size-1 == position) {
            (context as LoanDemandSummaryActivity).getTotalValue()
        }
        holder.tv_value3.setOnClickListener(View.OnClickListener {
            validate!!.SaveSharepreferenceLong(MeetingSP.Memberid, list.get(position).mem_id!!)
            validate!!.SaveSharepreferenceLong(MeetingSP.MemberCode, list.get(position).group_m_code!!)
            validate!!.SaveSharepreferenceString(MeetingSP.MemberName, list.get(position).member_name!!)
            if(validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) ==11 || validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) ==12) {
                validate!!.SaveSharepreferenceInt(
                    MeetingSP.Loanno,validate!!.returnIntegerValue(list.get(position).loan_no.toString()))
            }else {
                validate!!.SaveSharepreferenceInt(
                    MeetingSP.Loanno,
                    (context as LoanDemandSummaryActivity).getLoanno(
                        validate!!.returnLongValue(
                            list.get(position).loan_application_id.toString())))
            }
            var intent = Intent(context, PrincipalDemandActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            context.startActivity(intent)
        })
        holder.img_edit.setOnClickListener(View.OnClickListener {
            validate!!.SaveSharepreferenceLong(MeetingSP.Memberid, list.get(position).mem_id!!)
            validate!!.SaveSharepreferenceLong(MeetingSP.MemberCode, list.get(position).group_m_code!!)
            if(validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) ==11 || validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) ==12) {
                validate!!.SaveSharepreferenceInt(
                    MeetingSP.Loanno,validate!!.returnIntegerValue(list.get(position).loan_no.toString()))
            }else {
                validate!!.SaveSharepreferenceLong(
                    MeetingSP.Loanappid,
                    validate!!.returnLongValue(list.get(position).loan_application_id.toString())
                )
            }
            var inetnt = Intent(context, LoanDisbursementActivity::class.java)
            context.startActivity(inetnt)
        })

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_srno = view.tv_srno
        var tv_memberName = view.tv_memberName
        var tv_value1 = view.tv_value1
        var tv_value2 = view.tv_value2
        var tv_value3 = view.tv_value3
        var img_edit = view.img_edit

    }

    fun setLabelText(view: ViewHolder) {

    }
}