package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.LoanDisbursement1Activity
import com.microware.cdfi.activity.meeting.LoanDisbursementActivity
import com.microware.cdfi.activity.meeting.PrincipalDemandActivity
import com.microware.cdfi.entity.DtmtgDetEntity
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.loan_disbursement_item.view.*

class LoanDisbursementAdapter(val context: Context, var memberlist: List<DtmtgDetEntity>) :
    RecyclerView.Adapter<LoanDisbursementAdapter.ViewHolder>() {
var validate:Validate?=null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate= Validate(context)
        var view = LayoutInflater.from(context).inflate(R.layout.loan_disbursement_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return memberlist.size

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        setLabelText(holder)

        holder.tv_srno.text = (position+1).toString()
        holder.tv_memberName.text = memberlist.get(position).member_name.toString()
        holder.tv_value1.text = (context as LoanDisbursement1Activity).gettotalloanamt(
            memberlist.get(
                position
            ).mem_id,validate!!.RetriveSharepreferenceString(MeetingSP.mtg_guid)!!).toString()



        holder.cardView.setOnClickListener(View.OnClickListener {
          /*  validate!!.SaveSharepreferenceLong(MeetingSP.Memberid,memberlist.get(position).mem_id!!)
            validate!!.SaveSharepreferenceInt(MeetingSP.Loanno,23)
            var intent = Intent(context, PrincipalDemandActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            context.startActivity(intent)*/
        })

        context.callMethodWithPosition(position)


    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_srno=view.tv_srno
        val tv_memberName=view.tv_memberName
        val cardView=view.cardView
        val tv_value1=view.tv_value1
        val tv_value2=view.tv_value2
        val tv_value3=view.tv_value3
        val img_interest_rate=view.img_interest_rate
    }

    fun setLabelText(view: ViewHolder) {

    }
}