package com.microware.cdfi.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.LoanWiseRepaymentByGroupActivity
import com.microware.cdfi.entity.DtLoanGpTxnEntity
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.loan_wise_repaymentbygroup_listitem.view.*

class LoanWiseRepaymentByGroupAdapter(
    var context: Context,
    val list: List<DtLoanGpTxnEntity>?
) : RecyclerView.Adapter<LoanWiseRepaymentByGroupAdapter.ViewHolder>() {

    var validate: Validate? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate= Validate(context)
        var view = LayoutInflater.from(context).inflate(R.layout.loan_wise_repaymentbygroup_listitem, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.tv_loan_no1.text =
            validate!!.returnIntegerValue(list!!.get(position).loan_no.toString()).toString()
        holder.tv_outstanding1.text =
            validate!!.returnIntegerValue(list.get(position).loan_op.toString()).toString()+"("+(context as LoanWiseRepaymentByGroupActivity).getremaininginstallment(validate!!.returnIntegerValue(
                list.get(position).loan_no.toString()))+")"
        holder.tv_original_loan_amount1.text = (context as LoanWiseRepaymentByGroupActivity).getloanamount(validate!!.returnIntegerValue(
            list.get(position).loan_no.toString())) .toString()
        val totcurrentdue=validate!!.returnIntegerValue(list.get(position).principal_demand.toString())+validate!!.returnIntegerValue(
            list.get(position).int_accrued.toString())
        val totdue=validate!!.returnIntegerValue(list.get(position).principal_demand_ob.toString())+validate!!.returnIntegerValue(
            list.get(position).int_accrued_op.toString())
        holder.tv_current_due1.text = totcurrentdue.toString()
        holder.total_due1.text = (totcurrentdue+totdue).toString()
        if(list.get(position).completion_flag!!){
            holder.ll_loanwise.background=context.getDrawable(R.drawable.item_rectangle)
        }else{
            holder.ll_loanwise.background=context.getDrawable(R.drawable.textboxback1)
        }
     //   (context as LoanWiseRepaymentByMemberActivity).getTotalValue(totcurrentdue+totdue,1)


    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val ll_loanwise=view.ll_loanwise
        val tv_loan_no1=view.tv_loan_no1
        val tv_outstanding1=view.tv_outstanding1
        val tv_original_loan_amount1=view.tv_original_loan_amount1
        val tv_current_due1=view.tv_current_due1
        val total_due1=view.total_due1
    }

}