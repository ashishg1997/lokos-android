package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.MCPPreparationActivity
import com.microware.cdfi.activity.meeting.MCPPreparationListActivity
import com.microware.cdfi.entity.ShgMcpEntity
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.mcp_prepration_list_item.view.*

class McpPreprationAdapter (var context: Context, var list: List<ShgMcpEntity>) :
    RecyclerView.Adapter<McpPreprationAdapter.ViewHolder>() {
    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.mcp_prepration_list_item, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        setLabelText(holder)
        holder.tblPriorityValue.visibility = View.GONE
        holder.tv_memberName.text = (context as MCPPreparationListActivity).setMember(list[position].mem_id)
        holder.tv_value2.text = list[position].amt_demand.toString()
        holder.tv_value3.text = validate!!.convertDatetime(list[position].tentative_date)
        holder.tv_requestdate.text = validate!!.convertDatetime(list[position].request_date)
        holder.tv_value1.text = (context as MCPPreparationListActivity).returnvalue(validate!!.returnIntegerValue(list[position].loan_purpose.toString()),67)
        holder.tv_value4.text = list.get(position).loan_request_priority.toString()

        holder.cardView.setOnClickListener {
            validate!!.SaveSharepreferenceLong(MeetingSP.Memberid,list[position].mem_id)
            validate!!.SaveSharepreferenceLong(MeetingSP.Mcpid,list[position].mcp_id)
            val intent = Intent(context, MCPPreparationActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            context.startActivity(intent)
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var cardView = view.cardView
        var tv_memberName = view.tv_memberName
        var tv_value1 = view.tv_value1
        var tv_value2 = view.tv_value2
        var tv_value3 = view.tv_value3
        var tv_value4 = view.tv_value4
        var tblPriorityValue = view.tblPriorityValue
        var tv_requestdate = view.tv_requestdate

    }

    fun setLabelText(view: ViewHolder) {

    }
}