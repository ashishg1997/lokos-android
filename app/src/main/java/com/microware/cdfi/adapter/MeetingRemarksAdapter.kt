package com.microware.cdfi.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.entity.DtmtgEntity
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.layout_meeting_remarks_item.view.*

class MeetingRemarksAdapter(
    var context: Context,
    var mtgEntity: List<DtmtgEntity>
) : RecyclerView.Adapter<MeetingRemarksAdapter.ViewHolder>() {

    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context)
            .inflate(R.layout.layout_meeting_remarks_item, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mtgEntity.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if(validate!!.RetriveSharepreferenceInt(MeetingSP.isCheckerRemark) == 1){
            if (validate!!.returnStringValue(mtgEntity.get(position).checker_remarks)
                    .trim().length > 0) {
                var mtgNum = validate!!.returnStringValue(mtgEntity.get(position).mtg_no.toString())
                holder.tv_srno.text = (position + 1).toString()
                holder.tv_mtg_num.text = mtgNum
                holder.tv_remarks.text =
                    validate!!.returnStringValue(mtgEntity.get(position).checker_remarks)
            }else if(validate!!.returnIntegerValue(mtgEntity.get(position).approval_status.toString())==2){
                var mtgNum = validate!!.returnStringValue(mtgEntity.get(position).mtg_no.toString())
                holder.tv_srno.text = (position + 1).toString()
                holder.tv_mtg_num.text = mtgNum
                holder.tv_remarks.text = LabelSet.getText("",R.string.approve)
            }
        }else if(validate!!.RetriveSharepreferenceInt(MeetingSP.isCheckerRemark) == 0) {
            if (validate!!.returnStringValue(mtgEntity.get(position).transaction_status)
                    .trim().length > 0
            ) {
                var mtgNum = validate!!.returnStringValue(mtgEntity.get(position).mtg_no.toString())
                holder.tv_srno.text = (position + 1).toString()
                holder.tv_mtg_num.text = mtgNum
                holder.tv_remarks.text =
                    validate!!.returnStringValue(mtgEntity.get(position).transaction_status)
            }
        }
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tv_srno = view.tv_srno
        var tv_mtg_num = view.tv_mtg_num
        var tv_remarks = view.tv_remarks

    }
}
