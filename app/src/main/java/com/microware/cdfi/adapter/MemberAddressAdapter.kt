package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.MemberAddressDetail
import com.microware.cdfi.activity.MemberAddressListActivity
import com.microware.cdfi.entity.member_addressEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.memberaddress_listitem.view.*

class MemberAddressAdapter(var context : Context, var addressEntity: List<member_addressEntity>): RecyclerView.Adapter<MemberAddressAdapter.ViewHolder>() {

    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.memberaddress_listitem, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return addressEntity.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvaddress.text = validate!!.returnStringValue(addressEntity.get(position).address_line1.toString())+","+validate!!.returnStringValue(
            addressEntity.get(position).address_line2.toString())
        holder.tvpincode.text = validate!!.returnStringValue(addressEntity.get(position).postal_code.toString())

        var villageName = (context as MemberAddressListActivity).getVillageName(addressEntity.get(position).village_id)
        var addressType = (context as MemberAddressListActivity).getLookupValue(addressEntity.get(position).address_type!!)
        holder.tvtype.text = validate!!.returnStringValue(addressType)
        holder.tvVillage.text = validate!!.returnStringValue(villageName)

        if (validate!!.RetriveSharepreferenceInt(AppSP.MemberLockRecord) == 1){
            holder.ivDelete.visibility = View.GONE
        }else {
            holder.ivDelete.visibility = View.VISIBLE
        }

        holder.ivEdit.setOnClickListener {
            validate!!.SaveSharepreferenceString(AppSP.MemberAddressGUID,addressEntity.get(position).address_guid)
            var inetnt = Intent(context, MemberAddressDetail::class.java)
            context.startActivity(inetnt)
        }

        holder.ivDelete.setOnClickListener {
            if (validate!!.returnLongValue(addressEntity.get(position).last_uploaded_date.toString()) > 0
                || validate!!.returnLongValue(addressEntity.get(position).member_address_id.toString()) > 0) {
                (context as MemberAddressListActivity).CustomAlert(
                    addressEntity.get(position).address_guid,1)
            }else {
                (context as MemberAddressListActivity).CustomAlert(addressEntity.get(position).address_guid,0)
            }
        }
        setLabelText(holder)

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tvtype = view.tvtype
        var tvaddress = view.tvaddress
        var tvVillage = view.tvvillage
        var tvpincode = view.tvpincode
        var ivEdit = view.ivEdit
        var ivDelete = view.ivDelete
        var tvAddressType = view.tvAddressType
        var tvAddress = view.tvAddress
        var tv_Pincode = view.tvPinCode

    }

    fun setLabelText(view: ViewHolder) {
        view.tvAddressType.text = LabelSet.getText(
            "address_type",
            R.string.address_type
        )
        view.tvAddress.text = LabelSet.getText(
            "address",
            R.string.address
        )
        view.tv_Pincode.text = LabelSet.getText(
            "pincode",
            R.string.pincode
        )
    }
}