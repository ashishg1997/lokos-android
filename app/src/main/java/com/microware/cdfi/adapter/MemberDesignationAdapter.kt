package com.microware.cdfi.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.MemberDesignationActvity
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.entity.MemberDesignationEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.MemberDesignationViewmodel
import kotlinx.android.synthetic.main.member_designation__item.view.*

class ShgMemberListAdapter(val context: MemberDesignationActvity, val listData: List<LookupEntity>?) :
    RecyclerView.Adapter<ShgMemberListAdapter.ViewHolder>() {
    var validate: Validate? = null
    var memberDesignationViewmodel: MemberDesignationViewmodel? = null


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_president = view.tv_president
        var tvpresident = view.tvpresident
        var tvdate = view.tvdate
        var tvpresidentGUID = view.tvpresidentGUID
        var ivEdit = view.ivEdit
        var tv_date = view.tv_date
        var ivsignatory = view.ivsignatory
        var tv_signatory = view.tv_signatory
    }
    fun setLabelText(view: ViewHolder) {
        view.tv_date.text = LabelSet.getText(
            "date",
            R.string.date
        )
        view.tv_signatory.text = LabelSet.getText("signatory", R.string.signatory)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)


        var view = LayoutInflater.from(context).inflate(R.layout.member_designation__item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_president.text = listData!!.get(position).key_value
//        holder.tvdate.setText(listData!!.get(position).)
        var memberDesignationEntity:MemberDesignationEntity=   CDFIApplication.database?.memberDesignationDao()!!.getMemberDesignation(validate!!.RetriveSharepreferenceString(
            AppSP.SHGGUID).toString(), listData.get(position).lookup_code)
        if (memberDesignationEntity!=null)
        {
            holder.tvdate.text = validate!!.convertDatetime(validate!!.returnLongValue(memberDesignationEntity.from_date.toString()))
            holder.tvpresidentGUID.text = memberDesignationEntity.member_guid
            holder.tvpresident.text = CDFIApplication.database?.memberDao()!!.getMemberName(memberDesignationEntity.member_guid)
            if(validate!!.returnIntegerValue(memberDesignationEntity.is_signatory.toString())==1){
                holder.ivsignatory.visibility=View.VISIBLE
            }else{
                holder.ivsignatory.visibility=View.INVISIBLE
            }
        }else{
            holder.ivsignatory.visibility=View.INVISIBLE
        }
        holder.ivEdit.setOnClickListener{
            if (validate!!.RetriveSharepreferenceInt(AppSP.LockRecord) == 1) {
                validate!!.CustomAlert(context.getString(R.string.thisgroupislocked),context)
            }else{
                context.OpenDesignationDialog(
                    holder.tvpresidentGUID.text.toString(),
                    listData.get(position).lookup_code!!,
                    listData.get(position).key_value.toString()
                )
            }
        }
        setLabelText(holder)

    }

    override fun getItemCount(): Int {
        return listData!!.size
    }


}