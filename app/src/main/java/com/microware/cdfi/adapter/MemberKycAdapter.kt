package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.MemberIdActivity
import com.microware.cdfi.activity.MemberIdListActivity
import com.microware.cdfi.entity.Member_KYC_Entity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.member_system_listitem.view.*

class MemberKycAdapter(var context: Context, var kycEntity: List<Member_KYC_Entity>) :
    RecyclerView.Adapter<MemberKycAdapter.ViewHolder>() {

    var validate: Validate? = null

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tvcode = view.tvcode
        var tvkycid = view.tvkycid
        var tvkycnum = view.tvkycnum
        var tvvalidfrom = view.tvvalidfrom
        var tvvalidtill = view.tvvalidtill
        var ivEdit = view.ivEdit
        var cardView = view.cardView
        var ivDelete = view.ivDelete
        var tvKycDocumentType = view.tvKycDocumentType
        var tvKYCno = view.tvKYCno
    }
    fun setLabelText(view: ViewHolder) {
        view.tvKycDocumentType.text = LabelSet.getText(
            "kyc_id_type",
            R.string.kyc_id_type
        )
        view.tvKYCno.text = LabelSet.getText(
            "kyc_no",
            R.string.kyc_no
        )
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        var view =
            LayoutInflater.from(context).inflate(R.layout.member_system_listitem, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return kycEntity.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvcode.text = validate!!.returnStringValue(kycEntity.get(position).member_code.toString())
//        holder.tvkycid.setText(validate!!.returnStringValue(kycEntity.get(position).kyc_type.toString()))
        if(kycEntity.get(position).kyc_type==2){
            var kycnostr=validate!!.returnStringValue(kycEntity.get(position).kyc_number)
            if(kycnostr.trim().length<2){
                holder.tvkycnum.text = kycnostr
            }else {
                var kycno=kycnostr.substring(kycnostr.length-4)
                holder.tvkycnum.text = "XXXXXXXX"+kycno
            }

        }else{
            holder.tvkycnum.text = kycEntity.get(position).kyc_number
        }

        var Doctype = (context as MemberIdListActivity).getLookupValue(kycEntity.get(position).kyc_type!!)
        holder.tvkycid.text = (validate!!.returnStringValue(Doctype))

        if (validate!!.RetriveSharepreferenceInt(AppSP.MemberLockRecord) == 1){
            holder.ivDelete.visibility = View.GONE
        }else {
            holder.ivDelete.visibility = View.VISIBLE
        }

//        holder.ivDelete.setOnClickListener {
//            CDFIApplication.database?.memberkycDao()
//                ?.deleteKycData(kycEntity.get(position).kyc_guid)
//        }
        holder.cardView.setOnClickListener {
            validate!!.SaveSharepreferenceString(AppSP.MEMBERKYCGUID,kycEntity.get(position).kyc_guid)
            validate!!.SaveSharepreferenceInt(AppSP.KycEntrySource,validate!!.returnIntegerValue(kycEntity.get(position).entry_source.toString()))
            validate!!.SaveSharepreferenceInt(AppSP.MemberKycStatus,validate!!.returnIntegerValue(kycEntity.get(position).activation_status.toString()))
            var intent = Intent(context,MemberIdActivity::class.java)
            context.startActivity(intent)
        }

        holder.ivDelete.setOnClickListener {
            if (validate!!.returnLongValue(kycEntity.get(position).last_uploaded_date.toString()) > 0
                || validate!!.returnLongValue(kycEntity.get(position).member_kyc_details_id.toString()) > 0) {
                (context as MemberIdListActivity).CustomAlert(
                    kycEntity.get(position).kyc_guid,1)
            }else {
                (context as MemberIdListActivity).CustomAlert(kycEntity.get(position).kyc_guid,0)
            }
        }
        setLabelText(holder)

    }

}