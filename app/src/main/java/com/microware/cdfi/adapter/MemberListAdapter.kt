package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.*
import com.microware.cdfi.entity.MemberEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.member_list_item.view.*
import java.io.File

class MemberListAdapter(val context: Context, var memberEntity: List<MemberEntity>) :
    RecyclerView.Adapter<MemberListAdapter.ViewHolder>() {

    var validate: Validate? = null
    var firstword:String? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.member_list_item, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return memberEntity.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if(validate!!.returnLongValue(memberEntity.get(position).member_code.toString())>0){
            holder.tv_code.text = validate!!.returnStringValue(memberEntity.get(position).member_code.toString())
        }else{
            holder.tv_code.text = "..."
        }
        if(memberEntity.get(position).is_verified!! >0L && memberEntity.get(position).is_verified!=9){
            holder.tv_code.setTextColor(Color.parseColor("#FEAE1E"))
        }else {
            holder.tv_code.setTextColor(Color.parseColor("#000000"))
        }

        holder.tv_date.text = validate!!.convertDatetime(memberEntity.get(position).joining_date)
        holder.tv_nam.text =  validate!!.returnStringcapitalize(memberEntity.get(position).member_name)
        holder.tv_fathername.text =  validate!!.returnStringcapitalize(memberEntity.get(position).relation_name)
        var kycstatus = (context as MemberListActivity).returnkycstatus(validate!!.returnStringValue(memberEntity.get(position).member_guid))

        if(kycstatus == 1) {
            holder.iv_stauscolor.setImageResource(R.drawable.ic_right)
        }else{
            holder.iv_stauscolor.setImageResource(R.drawable.ic__question)
        }
        var islock=0
        if (validate!!.returnIntegerValue(memberEntity.get(position).status.toString()) == 2 || (validate!!.returnIntegerValue(memberEntity.get(position).approve_status.toString()) == 1 && validate!!.returnIntegerValue(
                memberEntity.get(position).is_edited.toString()) == 0)) {
            holder.imglock.visibility = View.VISIBLE
            islock=1
        } else {
            holder.imglock.visibility = View.GONE
            islock=0
        }
        if(!validate!!.returnStringValue(memberEntity.get(position).checker_remark).isEmpty()) {
            holder.imgactivationstatus.visibility=View.VISIBLE
        }else{
            holder.imgactivationstatus.visibility=View.GONE
        }
        if(validate!!.returnIntegerValue(memberEntity.get(position).status.toString()) == 0) {
            holder.tv_status.setBackgroundResource(R.color.red)
        }else if(validate!!.returnIntegerValue(memberEntity.get(position).is_edited.toString()) == 1){
            holder.tv_status.setBackgroundResource(R.color.colorPrimary)
        }else {
            holder.tv_status.setBackgroundResource(R.color.khakigreen1)
        }
        val mediaStorageDirectory = File(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
            AppSP.IMAGE_DIRECTORY_NAME)

        var mediaFile1 = File(mediaStorageDirectory.path + File.separator + memberEntity.get(position).member_image)
        Picasso.with(context).load(mediaFile1) .error(R.drawable.ic_woman).into(holder.IVimage)
        holder.cardView.setOnClickListener {
            validate!!.SaveSharepreferenceString(AppSP.MEMBERGUID,memberEntity.get(position).member_guid)
            validate!!.SaveSharepreferenceString(AppSP.memebrname,memberEntity.get(position).member_name!!)
            validate!!.SaveSharepreferenceInt(AppSP.Seqno, validate!!.returnIntegerValue(memberEntity.get(position).seq_no.toString()))
            validate!!.SaveSharepreferenceLong(AppSP.MemberDob,   validate!!.returnLongValue(memberEntity.get(position).dob.toString()))
            validate!!.SaveSharepreferenceLong(AppSP.MEMBERJOININGDATE,   validate!!.returnLongValue(memberEntity.get(position).joining_date.toString()))
            validate!!.SaveSharepreferenceInt(AppSP.MemberLockRecord,   islock)
            val i = Intent(context, MemberDetailActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            context.startActivity(i)
        }
        holder.imginfo.setOnClickListener {
            validate!!.SaveSharepreferenceString(AppSP.MEMBERGUID,memberEntity.get(position).member_guid)
            validate!!.SaveSharepreferenceString(AppSP.memebrname,memberEntity.get(position).member_name!!)
            validate!!.SaveSharepreferenceInt(AppSP.Seqno, validate!!.returnIntegerValue(memberEntity.get(position).seq_no.toString()))
            validate!!.SaveSharepreferenceLong(AppSP.MemberDob,   validate!!.returnLongValue(memberEntity.get(position).dob.toString()))
            validate!!.SaveSharepreferenceInt(AppSP.MemberLockRecord,   islock)
            val i = Intent(context, ShgMemberSummery::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            context.startActivity(i)
        }
        holder.imgactivationstatus.setOnClickListener {
            validate!!.SaveSharepreferenceString(AppSP.MEMBERGUID,memberEntity.get(position).member_guid)
            validate!!.SaveSharepreferenceString(AppSP.memebrname,memberEntity.get(position).member_name!!)
            validate!!.SaveSharepreferenceInt(AppSP.Seqno, validate!!.returnIntegerValue(memberEntity.get(position).seq_no.toString()))
            validate!!.SaveSharepreferenceLong(AppSP.MemberDob,   validate!!.returnLongValue(memberEntity.get(position).dob.toString()))

            val i = Intent(context, MemberRemarksListActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            context.startActivity(i)
        }

        holder.IVimage.setOnClickListener {
            context.ShowImage(validate!!.returnStringValue(memberEntity.get(position).member_image))
        }
        holder.imgdelete.setOnClickListener {
            if((validate!!.returnLongValue(memberEntity.get(position).last_uploaded_date.toString())>0
                || validate!!.returnLongValue(memberEntity.get(position).member_id.toString())>0) && islock==0) {
                context.CustomAlert(
                    validate!!.returnStringValue(
                        memberEntity.get(
                            position
                        ).member_guid
                    ), 1
                )
            }else if((validate!!.returnLongValue(memberEntity.get(position).last_uploaded_date.toString())==0L
                        || validate!!.returnLongValue(memberEntity.get(position).member_id.toString())==0L) && islock==0) {
                context.CustomAlert(
                    validate!!.returnStringValue(
                        memberEntity.get(
                            position
                        ).member_guid
                    ), 0
                )
            }else if(islock==1){
                validate!!.CustomAlert(LabelSet.getText(
                    "record_not_deleted",
                    R.string.record_not_deleted
                ), context
                )
            }
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tv_status = view.tv_status
        var imginfo = view.imginfo
        var cardView = view.cardView
        var tv_code = view.tv_code
        var tv_nam = view.tv_nam
        var tv_fathername = view.tv_fathername
        var tv_date = view.tv_date
        var IVimage = view.IVimage
        var tv_kyc = view.tv_kyc
        var iv_stauscolor = view.iv_stauscolor
        var imgactivationstatus = view.imgactivationstatus
        var imglock = view.imglock
        var imgdelete = view.imgdelete
    }

}