package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import kotlinx.android.synthetic.main.member_loan_repayment_item.view.*

class MemberLoanRepaymentAdapter(var context: Context) :
    RecyclerView.Adapter<MemberLoanRepaymentAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.member_loan_repayment_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return 7
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        setLabelText(holder)

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_srno = view.tv_srno
        var tv_memberName = view.tv_memberName
        var tv_value1 = view.tv_value1
        var tv_value2 = view.tv_value2
        var tv_value3 = view.tv_value3
        var img_edit = view.img_edit

    }

    fun setLabelText(view: ViewHolder) {

    }
}