package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.MemberMeetingSummaryActivity
import com.microware.cdfi.entity.DtLoanMemberEntity
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.member_meeting_summery_list_item.view.*

class MemberMeetingSummaryAdapter(var context: Context, var list: List<DtLoanMemberEntity>) :
    RecyclerView.Adapter<MemberMeetingSummaryAdapter.ViewHolder>() {
    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context)
            .inflate(R.layout.member_meeting_summery_list_item, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_amt.text = validate!!.returnStringValue(list[position].amount.toString())
        holder.tv_purpose.text = (context as MemberMeetingSummaryActivity).returnvalue(validate!!.returnIntegerValue(list.get(position).loan_purpose.toString()),67)
        holder.tv_period.text = validate!!.returnStringValue(list[position].period.toString())
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_purpose = view.tv_purpose
        val tv_amt = view.tv_amt
        val tv_period = view.tv_period
    }
}