package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.MemberPhoneDetail
import com.microware.cdfi.activity.MemeberPhoneListActivity
import com.microware.cdfi.entity.MemberPhoneDetailEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.member_phone_listitem.view.*

class MemberPhoneAdapter(var context : Context, var phoneEntity: List<MemberPhoneDetailEntity>): RecyclerView.Adapter<MemberPhoneAdapter.ViewHolder>() {

    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.member_phone_listitem, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return phoneEntity.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvOwnership.text = (context as MemeberPhoneListActivity).getLookupValue(validate
        !!.returnIntegerValue(phoneEntity.get(position).phone_ownership))
        holder.tvPhone.text = validate!!.returnStringValue(phoneEntity.get(position).phone_no)
        if(validate!!.returnLongValue(phoneEntity.get(position).valid_from.toString())>0) {
            holder.tvValidform.text = validate!!.convertDatetime(phoneEntity.get(position).valid_from)
        }
        if(validate!!.returnLongValue(phoneEntity.get(position).valid_till.toString())>0) {
            holder.tvValidtill.text = validate!!.convertDatetime(phoneEntity.get(position).valid_till)
        }
        if (validate!!.RetriveSharepreferenceInt(AppSP.MemberLockRecord) == 1){
            holder.ivDelete.visibility = View.GONE
        }else {
            holder.ivDelete.visibility = View.VISIBLE
        }
        holder.ivEdit.setOnClickListener {
            validate!!.SaveSharepreferenceString(AppSP.MemberPhoneGUID,phoneEntity.get(position).phone_guid)
            var inetnt = Intent(context, MemberPhoneDetail::class.java)
            context.startActivity(inetnt)
        }

        holder.ivDelete.setOnClickListener {
            if((context as MemeberPhoneListActivity).getissigntorydata(phoneEntity.get(position).member_guid!!)>0){
             validate!!.CustomAlert(LabelSet.getText(
                 "youcannotdelete",
                 R.string.youcannotdelete
             ),context as MemeberPhoneListActivity)
            }else{
            if (validate!!.returnLongValue(phoneEntity.get(position).last_uploaded_date.toString()) > 0
                || validate!!.returnLongValue(phoneEntity.get(position).member_phone_details_id.toString()) > 0) {
                (context as MemeberPhoneListActivity).CustomAlert(
                    phoneEntity.get(position).phone_guid,1)
            }else {
                (context as MemeberPhoneListActivity).CustomAlert(phoneEntity.get(position).phone_guid,0)
            }}
        }
        setLabelText(holder)

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tvPhone = view.tvPhone
        var tvOwnership = view.tvOwnership
        var tvValidform = view.tvValidform
        var tvValidtill = view.tvValidtill
        var ivEdit = view.ivEdit
        var ivDelete = view.ivDelete
        var tvMemberBelongs = view.tvMemberBelongs
        var tv_MOb = view.tv_MOb
    }
    fun setLabelText(view: ViewHolder) {
        view.tvMemberBelongs.text = LabelSet.getText(
            "belongs_to_memberrelation",
            R.string.belongs_to_memberrelation
        )
        view.tv_MOb.text = LabelSet.getText(
            "phone_no",
            R.string.phone_no
        )
    }

}