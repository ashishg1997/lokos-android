package com.microware.cdfi.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.entity.MemberEntity
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.Memberviewmodel
import com.microware.cdfi.viewModel.SHGViewmodel
import kotlinx.android.synthetic.main.layout_rjection_card_adapter.view.*
import kotlinx.android.synthetic.main.layout_rjection_remarks_cards.view.*

class MemberRejectionRemarksAdapter(
    var context: Context,
    var memberEntity: List<MemberEntity>,
    var memberViewmodel: Memberviewmodel
) : RecyclerView.Adapter<MemberRejectionRemarksAdapter.ViewHolder>() {

    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context)
            .inflate(R.layout.layout_rjection_remarks_cards, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return memberEntity.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var memberName = validate!!.returnStringValue(memberEntity.get(position).member_name)
        holder.tv_ShgName.text = memberName

        if (validate!!.returnStringValue(memberEntity.get(position).checker_remark).trim().length > 0) {
            var checkerlist =
                validate!!.returnStringValue(memberEntity.get(position).checker_remark).split("#")
            holder.linear_member.visibility = View.VISIBLE
            holder.tv_Name.text = context.getString(R.string.header)
            /*   holder.tv_nomination.setText(context.getString(R.string.value))
               holder.tv_description.setText(context.getString(R.string.remarks))*/
            holder.lyChildList.removeAllViews()
            for (i in 0 until checkerlist.size) {
                val mInflater =
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val convertView: View =
                    mInflater.inflate(
                        R.layout.layout_rjection_card_adapter,
                        holder.lyChildList,
                        false
                    )
                holder.lyChildList.addView(convertView)
                try {
                    var identity = validate!!.sSplitValue(checkerlist.get(i), 0)
                    var identity_reference = validate!!.sSplitValue(checkerlist.get(i), 1)
                    convertView.tvmember_name.text = validate!!.returnStringValue(identity)
                    convertView.tvdateValue.text = validate!!.returnStringValue(identity_reference)
                    var identity_remarks = validate!!.sSplitValue(checkerlist.get(i), 2)
                    convertView.tvPostvalue.text = validate!!.returnStringValue(identity_remarks)
                } catch (e: Exception) {

                }

            }


        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tv_ShgName = view.tv_ShgName
        var tv_Name = view.tv_Name
        var tv_nomination = view.tv_nomination
        var tv_description = view.tv_description
        var lyChildList = view.lyChildList
        var tblHeading = view.tblHeading
        var linear_member = view.linear_member
        var tbl_shgName = view.tbl_shgName
        var tvmember_name = view.tvmember_name
        var tvdateValue = view.tvdateValue
        var tvPostvalue = view.tvPostvalue
    }
}