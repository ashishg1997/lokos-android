package com.microware.cdfi.adapter

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.MemberSavingZeroActivity
import com.microware.cdfi.entity.DtmtgDetEntity
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.member_saving_zero_item.view.*

class MemberSavingZeroAdapter(var context: Context,
                              var list: List<DtmtgDetEntity>, val total:Int) :
    RecyclerView.Adapter<MemberSavingZeroAdapter.ViewHolder>() {
    var validate:Validate?=null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate= Validate(context)
        var view = LayoutInflater.from(context).inflate(R.layout.member_saving_zero_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_memberName.text = list.get(position).member_name.toString()
        holder.tv_srno.text = (position + 1).toString()

        holder.et_GroupMCode.setText(list.get(position).group_m_code.toString())

        if(list.get(position).sav_comp!! > 0){
            holder.et_compulasory_amount.setText(list.get(position).sav_comp.toString())
            (context as MemberSavingZeroActivity).getTotalValue(list.get(position).sav_comp!!,1)
        }else{

                holder.et_compulasory_amount.setText("0")
        }
        holder.et_voluntary_saving_amount.isEnabled = validate!!.RetriveSharepreferenceInt(MeetingSP.isVoluntarySaving)==1
        if(validate!!.returnIntegerValue(list.get(position).zero_mtg_attn.toString())==0){
            holder.tv_memberName.setTextColor(context.resources.getColor(R.color.red))
        }else{
            holder.tv_memberName.setTextColor(context.resources.getColor(R.color.black))
        }
        if(list.get(position).sav_vol!! > 0){
            holder.et_voluntary_saving_amount.setText(list.get(position).sav_vol.toString())
            (context as MemberSavingZeroActivity).getTotalValue(list.get(position).sav_vol!!,2)
        }else{

                holder.et_voluntary_saving_amount.setText("0")
        }

        if(validate!!.returnIntegerValue(list.get(position).zero_mtg_attn.toString())==0){
            holder.tv_memberName.setTextColor(context.resources.getColor(R.color.red))
        }else{
            holder.tv_memberName.setTextColor(context.resources.getColor(R.color.black))
        }


        setLabelText(holder)



        holder.et_compulasory_amount.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                (context as MemberSavingZeroActivity).getTotalValue()
            }

        })

        holder.et_voluntary_saving_amount.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                (context as MemberSavingZeroActivity).getTotalVoluntaryValue()
            }

        })

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_srno = view.tv_srno
        var tv_memberName = view.tv_memberName
        var et_compulasory_amount = view.et_compulasory_amount
        var et_voluntary_saving_amount = view.et_voluntary_saving_amount
        var et_GroupMCode = view.et_GroupMCode

    }

    fun setLabelText(view: ViewHolder) {

    }
}