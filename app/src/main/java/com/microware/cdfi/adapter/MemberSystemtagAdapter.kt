package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.*
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.MemberSystemTagEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.member_systemtag_listitem.view.*

class MemberSystemtagAdapter(var context: Context, var SystemTagEntity: List<MemberSystemTagEntity>) :
    RecyclerView.Adapter<MemberSystemtagAdapter.ViewHolder>() {

    var validate: Validate? = null

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tv_system_tag_id = view.tv_system_tag_id
        var tv_id = view.tv_id

        var ivEdit = view.ivEdit
        var cardView = view.cardView
        var ivDelete = view.ivDelete
        var tvSystemID = view.tvSystemID
        var tvID = view.tvID
    }
    fun setLabelText(view: ViewHolder) {
        view.tvSystemID.text = LabelSet.getText(
            "system_tag_id",
            R.string.system_tag_id
        )
        view.tvID.text = LabelSet.getText("id", R.string.id)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        var view =
            LayoutInflater.from(context).inflate(R.layout.member_systemtag_listitem, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return SystemTagEntity.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var systemid = (context as MemberGroupTagList).returnlookupvalue(SystemTagEntity.get(position).system_type!!)
        holder.tv_system_tag_id.text = (validate!!.returnStringValue(systemid))
        holder.tv_id.text = (validate!!.returnStringValue(SystemTagEntity.get(position).system_id.toString()))

        if (validate!!.RetriveSharepreferenceInt(AppSP.MemberLockRecord) == 1){
            holder.ivDelete.visibility = View.GONE
        }else {
            holder.ivDelete.visibility = View.VISIBLE
        }

        holder.ivDelete.setOnClickListener {
            CDFIApplication.database?.memberSystemtagDao()
                ?.deleteSystemtagData(SystemTagEntity.get(position).system_tag_guid)
            (context as MemberGroupTagList).fillData()
        }
        holder.ivEdit.setOnClickListener {
            validate!!.SaveSharepreferenceString(AppSP.MemberSystemtagGUID,SystemTagEntity.get(position).system_tag_guid)
            var intent = Intent(context,MemberGroupTagActivity::class.java)
            context.startActivity(intent)
        }

        holder.ivDelete.setOnClickListener {
            if (validate!!.returnLongValue(SystemTagEntity.get(position).last_uploaded_date.toString()) > 0
                || validate!!.returnLongValue(SystemTagEntity.get(position).systemtags_id.toString()) > 0) {
                (context as MemberGroupTagList).CustomAlert(
                    SystemTagEntity.get(position).system_tag_guid,1)
            }else {
                (context as MemberGroupTagList).CustomAlert(SystemTagEntity.get(position).system_tag_guid,0)
            }
        }
        setLabelText(holder)

    }

}