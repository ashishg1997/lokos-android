package com.microware.cdfi.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import kotlinx.android.synthetic.main.others_to_vo_receipt_item.view.*


class OtherstoVoReceiptAdapter(var context: Context) :
    RecyclerView.Adapter<OtherstoVoReceiptAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.others_to_vo_receipt_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return 7
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        setLabelText(holder)

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_srno=view.tv_srno
        val tv_type_receipt=view.tv_type_receipt
        val tv_Mode=view.tv_Mode
        val cardView=view.cardView
        val tv_amount_value=view.tv_amount_value
        val img_edit=view.img_edit
    }

    fun setLabelText(view: ViewHolder) {

    }
}