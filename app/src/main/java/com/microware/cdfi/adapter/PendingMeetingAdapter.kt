package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.MeetingApprovalActivity
import com.microware.cdfi.api.meetinguploadmodel.MeetingApprovalListModel
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.pending_meeting_item.view.*

class PendingMeetingAdapter(var context: Context, var approvalList: List<MeetingApprovalListModel>) :
    RecyclerView.Adapter<PendingMeetingAdapter.ViewHolder>() {

    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        var view = LayoutInflater.from(context).inflate(R.layout.pending_meeting_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return approvalList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_shgName.text = approvalList[position].shgName
        holder.tv_meeetingDate.text = validate!!.convertDatetime(approvalList[position].mtgDate)
        holder.tv_meeetingNo.text = validate!!.returnStringValue(approvalList[position].mtgNo.toString())
        holder.tv_pendingMeeting.text = validate!!.returnStringValue(approvalList[position].pendingMtgCount.toString())
        holder.cardView.setOnClickListener {
            validate!!.SaveSharepreferenceInt(MeetingSP.currentmeetingnumber,approvalList[position].mtgNo)
            validate!!.SaveSharepreferenceLong(MeetingSP.shgid,approvalList[position].cboId)
            validate!!.SaveSharepreferenceString(AppSP.ShgName,approvalList[position].shgName)
            validate!!.SaveSharepreferenceLong(MeetingSP.CurrentMtgDate,approvalList[position].mtgDate)
            var intent = Intent(context, MeetingApprovalActivity::class.java)
            context.startActivity(intent)
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_shgName = view.tv_shgName
        var tv_meeetingDate = view.tv_meeetingDate
        var tv_meeetingNo = view.tv_meeetingNo
        var tv_pendingMeeting = view.tv_pendingMeeting
        var cardView = view.cardView


    }

}