package com.microware.cdfi.adapter

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.CompulsorySavingActivity
import com.microware.cdfi.activity.meeting.PeneltyDetailActivity
import com.microware.cdfi.activity.meeting.VoluntorySavingActivity
import com.microware.cdfi.entity.DtmtgDetEntity
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.compulsary_saving_item.view.*
import kotlinx.android.synthetic.main.penelty_detail_item.view.*
import kotlinx.android.synthetic.main.penelty_detail_item.view.et_GroupMCode
import kotlinx.android.synthetic.main.penelty_detail_item.view.tv_count
import kotlinx.android.synthetic.main.penelty_detail_item.view.tv_memberName
import kotlinx.android.synthetic.main.penelty_detail_item.view.tv_srno

class PeneltyDetailAdapter(var context: Context,var list: List<DtmtgDetEntity>) :
    RecyclerView.Adapter<PeneltyDetailAdapter.ViewHolder>() {
    var validate: Validate?=null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.penelty_detail_item, parent, false)
        validate=Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.tv_memberName.text = list.get(position).member_name.toString()
        holder.tv_srno.text = (position + 1).toString()

        holder.tv_count.setText(list.get(position).penalty.toString())
        holder.et_GroupMCode.setText(list.get(position).group_m_code.toString())

        if(list.get(position).penalty!! > 0){
            (context as PeneltyDetailActivity).getTotalValue(list.get(position).penalty!!,1)
            holder.tv_count.setText(list.get(position).penalty.toString())
        }else{
            holder.tv_count.setText("")
        }
        if(validate!!.returnIntegerValue(list.get(position).attendance.toString())==2){
            holder.tv_memberName.setTextColor(context.resources.getColor(R.color.red))
        }else{
            holder.tv_memberName.setTextColor(context.resources.getColor(R.color.black))
        }
        setLabelText(holder)

        holder.tv_count.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                (context as PeneltyDetailActivity).getTotalValue()
            }
        })

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_srno = view.tv_srno
        var tv_memberName = view.tv_memberName
        var tv_count = view.tv_count
        var et_GroupMCode = view.et_GroupMCode

    }

    fun setLabelText(view: ViewHolder) {

    }
}