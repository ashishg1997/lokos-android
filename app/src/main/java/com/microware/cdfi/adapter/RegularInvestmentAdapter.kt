package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.RegularMeetingInvestmentActivity
import com.microware.cdfi.activity.meeting.RegularMeetingInvestmentListActivity
import com.microware.cdfi.entity.ShgFinancialTxnDetailEntity
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.layout_group_investment_item.view.*

class RegularInvestmentAdapter(
    var context: Context,
    var list: List<ShgFinancialTxnDetailEntity>
) :
    RecyclerView.Adapter<RegularInvestmentAdapter.ViewHolder>() {
    var validate: Validate? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        var view =
            LayoutInflater.from(context).inflate(R.layout.layout_group_investment_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_sr_no.text = (position+1).toString()
        holder.tv_amount.text = list[position].amount.toString()
        val savingType = (context as RegularMeetingInvestmentListActivity).returnSubHeadDescription(
            validate!!.returnIntegerValue(list[position].auid.toString()))
        val savingSource = (context as RegularMeetingInvestmentListActivity).getValue(
            validate!!.returnIntegerValue(list[position].amount_to_from.toString()),91)

        if (!savingType.isNullOrEmpty()) {
            holder.tv_type_of_saving.text = savingType
        }

        if (!savingSource.isNullOrEmpty()) {
            holder.tv_saving_source.text = savingSource
        }

        (context as RegularMeetingInvestmentListActivity).getTotalValue1(validate!!.returnIntegerValue(list[position].amount.toString()))

        holder.ivEdit.setOnClickListener {
            validate!!.SaveSharepreferenceString(MeetingSP.mtg_guid, list[position].mtg_guid)
            validate!!.SaveSharepreferenceLong(MeetingSP.shgid, list[position].cbo_id)
            validate!!.SaveSharepreferenceInt(MeetingSP.currentmeetingnumber, list[position].mtg_no)
            validate!!.SaveSharepreferenceInt(
                MeetingSP.Auid, validate!!.returnIntegerValue(list[position].auid.toString())
            )
            validate!!.SaveSharepreferenceInt(
                MeetingSP.savingSource, validate!!.returnIntegerValue(list[position].amount_to_from.toString())
            )
            val intent = Intent(context, RegularMeetingInvestmentActivity::class.java)
            context.startActivity(intent)
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_sr_no = view.tv_sr_no
        var tv_type_of_saving = view.tv_type_of_saving
        var tv_saving_source = view.tv_saving_source
        var tv_amount = view.tv_amount
        var ivEdit = view.ivEdit
    }
}