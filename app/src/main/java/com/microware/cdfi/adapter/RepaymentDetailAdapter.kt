package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.LoanWiseRepaymentByMemberActivity
import com.microware.cdfi.activity.meeting.RepaymentDetailActivity
import com.microware.cdfi.utility.*
import com.microware.cdfi.api.meetingmodel.LoanRepaymentListModel
import kotlinx.android.synthetic.main.repayment_detail_item.view.*

class RepaymentDetailAdapter(
    var context: Context,
    val list: List<LoanRepaymentListModel>
) :
    RecyclerView.Adapter<RepaymentDetailAdapter.ViewHolder>() {
    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        var view =
            LayoutInflater.from(context).inflate(R.layout.repayment_detail_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        setLabelText(holder)

        var loanlist = (context as RepaymentDetailActivity).returnlist(
            validate!!.returnLongValue(
                list.get(position).mem_id.toString()
            )
        )
        if (!loanlist.isNullOrEmpty()) {
            holder.tv_loans.text =
                validate!!.returnIntegerValue(loanlist.get(0).loan_no.toString()).toString()
            val totpayble =
                validate!!.returnIntegerValue(loanlist.get(0).principal_demand_ob.toString()) + validate!!.returnIntegerValue(
                    loanlist.get(0).principal_demand.toString()
                )
            val totintrest =
                validate!!.returnIntegerValue(loanlist.get(0).int_accrued_op.toString()) + validate!!.returnIntegerValue(
                    loanlist.get(0).int_accrued.toString()
                )
            var todaysPayable = totintrest + totpayble
            var totalLoanPaid = validate!!.returnIntegerValue(loanlist.get(0).loan_paid.toString()) + validate!!.returnIntegerValue(
                loanlist.get(0).loan_paid_int.toString()
            )
            if (todaysPayable < 0) {
                holder.tv_value1.text = "0"
            } else {
                holder.tv_value1.text = todaysPayable.toString()
            }
            (context as RepaymentDetailActivity).getTotalValue(todaysPayable, 1)
            holder.tv_value2.text = totalLoanPaid.toString()

            if ( validate!!.returnIntegerValue(holder.tv_loans.text.toString())>0) {
                holder.tv_loans.background = context.getDrawable(R.drawable.item_countred)
                holder.tv_loans.setTextColor(context.getColor(R.color.meetingtabloancolor))
            }else{
                holder.tv_loans.background = context.getDrawable(R.drawable.item_count)
                holder.tv_loans.setTextColor(context.getColor(R.color.colorAccent))

            }
            if (todaysPayable > validate!!.returnIntegerValue(holder.tv_value2.text.toString())) {
                holder.tv_value2.setTextColor(context.getColor(R.color.meetingtabloancolor))
                holder.tv_value3.setTextColor(context.getColor(R.color.meetingtabloancolor))
            } else if (todaysPayable>0 && todaysPayable <= validate!!.returnIntegerValue(holder.tv_value2.text.toString())) {
                holder.tv_value2.setTextColor(context.getColor(R.color.Lightgreen))
                holder.tv_value3.setTextColor(context.getColor(R.color.colorAccent))

            } else {
                holder.tv_value2.setTextColor(context.getColor(R.color.colorAccent))
                holder.tv_value3.setTextColor(context.getColor(R.color.colorAccent))

            }
            (context as RepaymentDetailActivity).getTotalValue(
                (validate!!.returnIntegerValue(
                    loanlist.get(0).loan_paid.toString()
                ) + validate!!.returnIntegerValue(loanlist.get(0).loan_paid_int.toString())), 2
            )

        } else {
            holder.tv_loans.text = "0"
            holder.tv_value1.text = "0"
            holder.tv_value2.text = "0"
        }
        holder.tv_memberName.text = list.get(position).member_name.toString()
        var nextdemand = (context as RepaymentDetailActivity).getnextdemand(
            validate!!.returnLongValue(
                list.get(position).mem_id.toString()
            ), validate!!.returnIntegerValue(list.get(position).loan_no.toString())
        )
        (context as RepaymentDetailActivity).getTotalValue(nextdemand, 3)

        holder.tv_value3.text = nextdemand.toString()
        holder.cardView.setOnClickListener {
            validate!!.SaveSharepreferenceLong(MeetingSP.Memberid, list.get(position).mem_id!!)
            validate!!.SaveSharepreferenceLong(
                MeetingSP.MemberCode,
                list.get(position).group_m_code!!
            )
            validate!!.SaveSharepreferenceString(
                MeetingSP.MemberName,
                list.get(position).member_name!!
            )
            var intent = Intent(context, LoanWiseRepaymentByMemberActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            context.startActivity(intent)
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val cardView = view.cardView
        val tv_memberName = view.tv_memberName
        val tv_value1 = view.tv_value1
        val tv_loans = view.tv_loans
        val tv_value2 = view.tv_value2
        val tv_value3 = view.tv_value3
        val img_edit = view.img_edit
    }

    fun setLabelText(view: ViewHolder) {

    }
}