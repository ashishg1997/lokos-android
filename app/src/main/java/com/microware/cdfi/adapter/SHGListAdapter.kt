package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.PopupWindow
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.*
import com.microware.cdfi.entity.SHGEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.shg_item_dialog.view.*
import kotlinx.android.synthetic.main.shg_listitem.view.*


class SHGListAdapter(val context: Context, val shgList: List<SHGEntity>?) :
    RecyclerView.Adapter<SHGListAdapter.ViewHolder>() {
    var validate: Validate? = null
    var firstword: String? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.shg_listitem, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return shgList?.size!!
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if(validate!!.returnIntegerValue(shgList?.get(position)?.is_verified.toString()) >0 && validate!!.returnIntegerValue(shgList?.get(position)?.is_verified.toString())!=9){
            holder.tv_code.setTextColor(Color.parseColor("#FEAE1E"))
        }else {
            holder.tv_code.setTextColor(Color.parseColor("#000000"))
        }
        if (validate!!.returnIntegerValue(shgList?.get(position)?.shg_code) > 0) {
            holder.tv_code.text =
                validate!!.returnStringValue(shgList?.get(position)?.shg_code)
        } else {
            holder.tv_code.text = "..."
        }
        holder.tv_nam.text = validate!!.returnStringcapitalize(shgList?.get(position)?.shg_name)
        if (!validate!!.returnStringValue(shgList?.get(position)?.checker_remark).isEmpty()) {
            holder.imgactivationstatus.visibility = View.VISIBLE
        } else {
            holder.imgactivationstatus.visibility = View.GONE
        }
        if (validate!!.returnIntegerValue(shgList?.get(position)?.activation_status.toString()) == 2) {
            //holder.imgactivationstatus.visibility=View.GONE
            if (validate!!.returnIntegerValue(shgList?.get(position)?.status.toString()) == 1) {
                if (validate!!.returnIntegerValue(shgList?.get(position)?.shg_type_code.toString()) == 1) {
                    holder.tv_count.setBackgroundResource(R.drawable.item_countactive)
                    holder.iv_stauscolor.setBackgroundResource(R.drawable.item_countactive)
                    holder.iv_stauscolor.setImageResource(R.drawable.ic_womangroup)
                    holder.iv_stauscolor.visibility = View.INVISIBLE

                } else {
                    holder.iv_stauscolor.visibility = View.VISIBLE
                    if (validate!!.returnIntegerValue(shgList?.get(position)?.tags.toString()) == 1) {
                        holder.tv_count.setBackgroundResource(R.drawable.item_countactive)
                        holder.iv_stauscolor.setBackgroundResource(R.drawable.item_countactive)
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_pvtg)

                    } else if (validate!!.returnIntegerValue(shgList?.get(position)?.tags.toString()) == 2) {
                        holder.tv_count.setBackgroundResource(R.drawable.item_countactive)
                        holder.iv_stauscolor.setBackgroundResource(R.drawable.item_countactive)
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_elderly)

                    } else if (validate!!.returnIntegerValue(shgList?.get(position)?.tags.toString()) == 3) {
                        holder.tv_count.setBackgroundResource(R.drawable.item_countactive)
                        holder.iv_stauscolor.setBackgroundResource(R.drawable.item_countactive)
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_disable)

                    } else if (validate!!.returnIntegerValue(shgList?.get(position)?.tags.toString()) == 99) {
                        holder.tv_count.setBackgroundResource(R.drawable.item_countactive)
                        holder.iv_stauscolor.setBackgroundResource(R.drawable.item_countactive)
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_other)

                    }
                }
            } else {
                if (validate!!.returnIntegerValue(shgList?.get(position)?.shg_type_code.toString()) == 1) {
                    holder.tv_count.setBackgroundResource(R.drawable.item_count)
                    holder.iv_stauscolor.setBackgroundResource(R.drawable.item_count)
                    holder.iv_stauscolor.setImageResource(R.drawable.ic_womangroup)
                    holder.iv_stauscolor.visibility = View.INVISIBLE
                } else {
                    holder.iv_stauscolor.visibility = View.VISIBLE
                    if (validate!!.returnIntegerValue(shgList?.get(position)?.tags.toString()) == 1) {
                        holder.tv_count.setBackgroundResource(R.drawable.item_count)
                        holder.iv_stauscolor.setBackgroundResource(R.drawable.item_count)
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_pvtg)

                    } else if (validate!!.returnIntegerValue(shgList?.get(position)?.tags.toString()) == 2) {
                        holder.tv_count.setBackgroundResource(R.drawable.item_count)
                        holder.iv_stauscolor.setBackgroundResource(R.drawable.item_count)
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_elderly)

                    } else if (validate!!.returnIntegerValue(shgList?.get(position)?.tags.toString()) == 3) {
                        holder.tv_count.setBackgroundResource(R.drawable.item_count)
                        holder.iv_stauscolor.setBackgroundResource(R.drawable.item_count)
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_disable)

                    } else if (validate!!.returnIntegerValue(shgList?.get(position)?.tags.toString()) == 99) {
                        holder.tv_count.setBackgroundResource(R.drawable.item_count)
                        holder.iv_stauscolor.setBackgroundResource(R.drawable.item_count)
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_other)

                    }
                }
            }


        } else if (validate!!.returnIntegerValue(shgList?.get(position)?.activation_status.toString()) == 3) {
            // holder.imgactivationstatus.visibility=View.VISIBLE

            if (validate!!.returnIntegerValue(shgList?.get(position)?.status.toString()) == 1) {
                if (validate!!.returnIntegerValue(shgList?.get(position)?.shg_type_code.toString()) == 1) {
                    holder.tv_count.setBackgroundResource(R.drawable.item_countreject)
                    holder.iv_stauscolor.setBackgroundResource(R.drawable.item_countreject)
                    holder.iv_stauscolor.setImageResource(R.drawable.ic_womangroup)
                    holder.iv_stauscolor.visibility = View.INVISIBLE

                } else {
                    holder.iv_stauscolor.visibility = View.VISIBLE
                    if (validate!!.returnIntegerValue(shgList?.get(position)?.tags.toString()) == 1) {
                        holder.tv_count.setBackgroundResource(R.drawable.item_countreject)
                        holder.iv_stauscolor.setBackgroundResource(R.drawable.item_countreject)
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_pvtg)

                    } else if (validate!!.returnIntegerValue(shgList?.get(position)?.tags.toString()) == 2) {
                        holder.tv_count.setBackgroundResource(R.drawable.item_countreject)
                        holder.iv_stauscolor.setBackgroundResource(R.drawable.item_countreject)
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_elderly)

                    } else if (validate!!.returnIntegerValue(shgList?.get(position)?.tags.toString()) == 3) {
                        holder.tv_count.setBackgroundResource(R.drawable.item_countreject)
                        holder.iv_stauscolor.setBackgroundResource(R.drawable.item_countreject)
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_disable)

                    } else if (validate!!.returnIntegerValue(shgList?.get(position)?.tags.toString()) == 99) {
                        holder.tv_count.setBackgroundResource(R.drawable.item_countreject)
                        holder.iv_stauscolor.setBackgroundResource(R.drawable.item_countreject)
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_other)

                    }
                }
            } else {
                if (validate!!.returnIntegerValue(shgList?.get(position)?.shg_type_code.toString()) == 1) {
                    holder.tv_count.setBackgroundResource(R.drawable.item_count)
                    holder.iv_stauscolor.setBackgroundResource(R.drawable.item_count)
                    holder.iv_stauscolor.setImageResource(R.drawable.ic_womangroup)
                    holder.iv_stauscolor.visibility = View.INVISIBLE
                } else {
                    holder.iv_stauscolor.visibility = View.VISIBLE
                    if (validate!!.returnIntegerValue(shgList?.get(position)?.tags.toString()) == 1) {
                        holder.tv_count.setBackgroundResource(R.drawable.item_count)
                        holder.iv_stauscolor.setBackgroundResource(R.drawable.item_count)
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_pvtg)

                    } else if (validate!!.returnIntegerValue(shgList?.get(position)?.tags.toString()) == 2) {
                        holder.tv_count.setBackgroundResource(R.drawable.item_count)
                        holder.iv_stauscolor.setBackgroundResource(R.drawable.item_count)
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_elderly)

                    } else if (validate!!.returnIntegerValue(shgList?.get(position)?.tags.toString()) == 3) {
                        holder.tv_count.setBackgroundResource(R.drawable.item_count)
                        holder.iv_stauscolor.setBackgroundResource(R.drawable.item_count)
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_disable)

                    } else if (validate!!.returnIntegerValue(shgList?.get(position)?.tags.toString()) == 99) {
                        holder.tv_count.setBackgroundResource(R.drawable.item_count)
                        holder.iv_stauscolor.setBackgroundResource(R.drawable.item_count)
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_other)

                    }
                }
            }


        } else {
            // holder.imgactivationstatus.visibility=View.GONE

            if (validate!!.returnIntegerValue(shgList?.get(position)?.shg_type_code.toString()) == 1) {
                holder.tv_count.setBackgroundResource(R.drawable.item_countpending)
                holder.iv_stauscolor.setBackgroundResource(R.drawable.item_countpending)
                holder.iv_stauscolor.setImageResource(R.drawable.ic_womangroup)
                holder.iv_stauscolor.visibility = View.INVISIBLE
            } else {
                holder.iv_stauscolor.visibility = View.VISIBLE
                if (validate!!.returnIntegerValue(shgList?.get(position)?.tags.toString()) == 1) {
                    holder.tv_count.setBackgroundResource(R.drawable.item_countpending)
                    holder.iv_stauscolor.setBackgroundResource(R.drawable.item_countpending)
                    holder.iv_stauscolor.setImageResource(R.drawable.ic_pvtg)

                } else if (validate!!.returnIntegerValue(shgList?.get(position)?.tags.toString()) == 2) {
                    holder.tv_count.setBackgroundResource(R.drawable.item_countpending)
                    holder.iv_stauscolor.setBackgroundResource(R.drawable.item_countpending)
                    holder.iv_stauscolor.setImageResource(R.drawable.ic_elderly)

                } else if (validate!!.returnIntegerValue(shgList?.get(position)?.tags.toString()) == 3) {
                    holder.tv_count.setBackgroundResource(R.drawable.item_countpending)
                    holder.iv_stauscolor.setBackgroundResource(R.drawable.item_countpending)
                    holder.iv_stauscolor.setImageResource(R.drawable.ic_disable)

                } else if (validate!!.returnIntegerValue(shgList?.get(position)?.tags.toString()) == 99) {
                    holder.tv_count.setBackgroundResource(R.drawable.item_countpending)
                    holder.iv_stauscolor.setBackgroundResource(R.drawable.item_countpending)
                    holder.iv_stauscolor.setImageResource(R.drawable.ic_other)
                }
            }


        }
        if (validate!!.returnIntegerValue(shgList?.get(position)?.status.toString()) == 0) {
            holder.tv_status.setBackgroundResource(R.color.red)
        } else if (validate!!.returnIntegerValue(shgList?.get(position)?.is_edited.toString()) == 1) {
            holder.tv_status.setBackgroundResource(R.color.colorPrimary)
        } else {
            holder.tv_status.setBackgroundResource(R.color.khakigreen1)
        }
        holder.tv_date.text =
            validate!!.convertDatetime(validate!!.returnLongValue(shgList?.get(position)?.shg_formation_date.toString()))
        val name = validate!!.returnStringValue(shgList?.get(position)?.shg_name)
        var count = (context as ShgListActivity).getmembercount(shgList?.get(position)?.guid!!)

        var cbotype =
            validate!!.returnIntegerValue(shgList.get(position).shg_type_code.toString())
        if (!name.isNullOrEmpty()) {
            firstword = name.substring(0, 1).toUpperCase()
        }

        if (count > 0) {
            holder.tv_count.text = "" + count

        } else {
            holder.tv_count.text = "0"
        }
        var islock = 0
       /* if (validate!!.returnIntegerValue( shgList.get(position).is_locked.toString()) == 1){
            holder.imglock.visibility = View.VISIBLE
            islock = 1
        } else {
            holder.imglock.visibility = View.GONE
            islock = 0
        }*/
        if (validate!!.returnIntegerValue(shgList.get(position).status.toString()) == 2 ||
            (validate!!.returnIntegerValue(shgList.get(position).approve_status.toString()) == 1 &&
                    validate!!.returnIntegerValue(shgList.get(position).is_edited.toString()) == 0 &&
                    validate!!.returnIntegerValue( shgList.get(position).is_locked.toString()) == 1)) {
            holder.imglock.visibility = View.VISIBLE
            islock = 1
        } else {
            holder.imglock.visibility = View.GONE
            islock = 0
        }
        holder.tv_firstword.text = firstword

        holder.relative.setOnClickListener {
            if (validate!!.returnIntegerValue(shgList.get(position).status.toString()) == 0) {
                validate!!.CustomAlert(
                    context.getString(R.string.download_shg_data),
                    context
                )
            } else {
                validate!!.SaveSharepreferenceString(AppSP.SHGGUID, shgList.get(position).guid)
                validate!!.SaveSharepreferenceString(
                    AppSP.ShgName,
                    validate!!.returnStringValue(shgList.get(position).shg_name)
                )
                validate!!.SaveSharepreferenceString(
                    AppSP.Shgcode,
                    validate!!.returnStringValue(shgList.get(position).shg_code.toString())
                )
                validate!!.SaveSharepreferenceInt(
                    AppSP.CboType,
                    validate!!.returnIntegerValue(cbotype.toString())
                )
                validate!!.SaveSharepreferenceInt(
                    AppSP.Tag,
                    validate!!.returnIntegerValue(shgList.get(position).tags.toString())
                )
                validate!!.SaveSharepreferenceInt(
                    AppSP.LockRecord,
                    islock
                )
                validate!!.SaveSharepreferenceLong(
                    AppSP.Formation_dt,
                    validate!!.returnLongValue(shgList.get(position).shg_formation_date.toString())
                )

                val i = Intent(context, MemberListActivity::class.java)
                i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                context.startActivity(i)
            }
        }

        holder.tblFirst.setOnClickListener {
            /* validate!!.SaveSharepreferenceString(AppSP.SHGGUID, shgList?.get(position)?.guid)
             validate!!.SaveSharepreferenceString(AppSP.ShgName, shgList?.get(position)?.shg_name!!)
             validate!!.SaveSharepreferenceString(AppSP.Shgcode, validate!!.returnStringValue(shgList?.get(position)?.shg_code.toString()))
             validate!!.SaveSharepreferenceInt(AppSP.CboType, cbotype!!.toInt())
             val i = Intent(context, BasicDetailActivity::class.java)
             i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
             context.startActivity(i)*/
        }
        holder.tv_nam.setOnClickListener {
            validate!!.SaveSharepreferenceString(AppSP.SHGGUID, shgList.get(position).guid)
            validate!!.SaveSharepreferenceString(AppSP.ShgName, name)
            validate!!.SaveSharepreferenceString(
                AppSP.Shgcode,
                validate!!.returnStringValue(shgList.get(position).shg_code.toString())
            )
            validate!!.SaveSharepreferenceInt(
                AppSP.CboType,
                validate!!.returnIntegerValue(cbotype.toString())
            )
            validate!!.SaveSharepreferenceInt(
                AppSP.Tag,
                validate!!.returnIntegerValue(shgList.get(position).tags.toString())
            )
            validate!!.SaveSharepreferenceInt(AppSP.LockRecord, islock)
            validate!!.SaveSharepreferenceLong(
                AppSP.Formation_dt,
                validate!!.returnLongValue(shgList.get(position).shg_formation_date.toString())
            )

            if (validate!!.returnIntegerValue(shgList.get(position).status.toString()) == 0) {
                validate!!.CustomAlert(
                    context.getString(R.string.download_shg_data),
                    context
                )
            } else {
                val i = Intent(context, BasicDetailActivity::class.java)
                i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                context.startActivity(i)
            }
        }

        holder.imgactivationstatus.setOnClickListener {
            validate!!.SaveSharepreferenceString(AppSP.SHGGUID, shgList.get(position).guid)
            validate!!.SaveSharepreferenceString(AppSP.ShgName, validate!!.returnStringValue(shgList.get(position).shg_name))
            val i = Intent(context, ShgRemarksListActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            context.startActivity(i)
        }
        holder.optionBtn.setOnClickListener {
            var popupwindow: PopupWindow = popupDisplay(
                shgList.get(position).guid,
                name,
                validate!!.returnStringValue(shgList.get(position).shg_code.toString()),
                validate!!.returnIntegerValue(cbotype.toString()),
                validate!!.returnLongValue(shgList.get(position).shg_id.toString()),
                validate!!.returnIntegerValue(shgList.get(position).tags.toString()),
                validate!!.returnIntegerValue(shgList.get(position).status.toString()),
                validate!!.returnStringValue(shgList.get(position).checker_remark),
                islock,
                validate!!.returnLongValue(shgList.get(position).shg_formation_date.toString()),
                validate!!.returnLongValue(shgList.get(position).last_uploaded_date.toString())

            )
            val values = IntArray(2)
            holder.layout.getLocationInWindow(values)
            val positionOfIcon = values[1]

            //Get the height of 2/3rd of the height of the screen
            //Get the height of 2/3rd of the height of the screen
            val displayMetrics: DisplayMetrics = context.getResources().displayMetrics
            val height: Int = displayMetrics.heightPixels * 2 / 3

            //If the position of menu icon is in the bottom 2/3rd part of the screen then we provide menu height as offset  but in negative as we want to open our menu to the top
            //If the position of menu icon is in the bottom 2/3rd part of the screen then we provide menu height as offset  but in negative as we want to open our menu to the top
            if (positionOfIcon > height) {
                popupwindow.showAsDropDown(holder.layout, -20, -200)
            } else {
                popupwindow.showAsDropDown(holder.layout, -20, 0)
            }

        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tv_status = view.tv_status
        var relative = view.relative
        var cardView = view.cardView
        var tv_firstword = view.tv_firstword
        var tv_code = view.tv_code
        var tv_nam = view.tv_nam
        var imgactivationstatus = view.imgactivationstatus
        var tv_date = view.tv_date
        var tv_count = view.tv_count
        var iv_stauscolor = view.iv_stauscolor
        var optionBtn = view.optionBtn
        var imglock = view.imglock
        var layout = view.layout
        var tblFirst = view.tblFirst
    }

    fun popupDisplay(
        guid: String,
        shgName: String,
        shgGroupCode: String,
        cbotype: Int?,
        shgid: Long,
        tag: Int,
        status: Int,
        remarks: String,
        lock: Int,
        formationdate: Long,
        last_uploaded_date: Long
    ): PopupWindow {
        val popupWindow = PopupWindow(context)
        // inflate your layout or dynamically add view
        val inflater =
            context.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.shg_item_dialog, null, false)
        popupWindow.isFocusable = true
        popupWindow.width = WindowManager.LayoutParams.WRAP_CONTENT
        popupWindow.height = WindowManager.LayoutParams.WRAP_CONTENT
        popupWindow.setBackgroundDrawable(ColorDrawable(context.resources.getColor(R.color.white)))

        view.tvShg.text = LabelSet.getText(
            "shg_profile",
            R.string.shg_profile
        )
        view.tvShgsummary.text = LabelSet.getText(
            "summary",
            R.string.summary
        )
        view.tv_meeting.text = LabelSet.getText(
            "meeting",
            R.string.meeting
        )
        view.tv_clear.text = LabelSet.getText(
            "delete_record",
            R.string.delete_record
        )
        view.tv_sync.text = LabelSet.getText(
            "sync_data",
            R.string.sync_data
        )
        view.tv_remarks.text = LabelSet.getText(
            "queue_status",
            R.string.queue_status
        )

        view.tbl_shgprofile.setOnClickListener {
            //shg
            view.tbl_shgprofile.background =
                context.resources.getDrawable(R.drawable.dialog_selected_bg)
            view.iVshg.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary))
            view.tvShg.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
            //meeting
            view.tbl_meeting.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.transperent
                )
            )
            view.ivMeetingpopup.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.dialog_text_clr
                )
            )
            view.tv_meeting.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))
            //Rejection Remarks
            view.tbl_Remarks.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.transperent
                )
            )
            view.iVRejection.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.dialog_text_clr
                )
            )
            view.tv_remarks.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))
            //sync
            view.tbl_sync.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.iVsync.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_sync.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            //clear
            view.tbl_clear.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.ivClear.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_clear.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))
            validate!!.SaveSharepreferenceString(AppSP.SHGGUID, guid)
            validate!!.SaveSharepreferenceString(AppSP.ShgName, shgName)
            validate!!.SaveSharepreferenceString(AppSP.Shgcode, shgGroupCode)
            validate!!.SaveSharepreferenceInt(AppSP.CboType, cbotype!!)
            validate!!.SaveSharepreferenceInt(AppSP.Tag, tag)
            validate!!.SaveSharepreferenceInt(AppSP.LockRecord, lock)
            validate!!.SaveSharepreferenceLong(AppSP.Formation_dt, formationdate)

            if (status == 0) {
                validate!!.CustomAlert(
                    context.getString(R.string.download_shg_data),
                    context as ShgListActivity
                )
            } else {
                val i = Intent(context, BasicDetailActivity::class.java)
                i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                context.startActivity(i)
            }
        }

        view.tbl_meeting.setOnClickListener {
            //shg
            view.tbl_shgprofile.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.transperent
                )
            )
            view.iVshg.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tvShg.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))
            //meeting
            view.tbl_meeting.background =
                context.resources.getDrawable(R.drawable.dialog_selected_bg)
            view.ivMeetingpopup.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.colorPrimary
                )
            )
            view.tv_meeting.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
            //Rejection Remarks
            view.tbl_Remarks.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.transperent
                )
            )
            view.iVRejection.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.dialog_text_clr
                )
            )
            view.tv_remarks.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))
            //sync
            view.tbl_sync.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.iVsync.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_sync.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            //clear
            view.tbl_clear.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.ivClear.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_clear.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))


         /*   val i = Intent(context, MeetingMenuActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            context.startActivity(i)*/
        }

        view.tbl_Remarks.setOnClickListener {

            //Rejection Remarks

            view.tbl_Remarks.background =
                context.resources.getDrawable(R.drawable.dialog_selected_bg)
            view.iVRejection.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary))
            view.tv_remarks.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))

            //shg
            view.tbl_shgprofile.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.transperent
                )
            )
            view.iVshg.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tvShg.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            //meeting
            view.tbl_meeting.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.transperent
                )
            )
            view.ivMeetingpopup.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.dialog_text_clr
                )
            )
            view.tv_meeting.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            //sync
            view.tbl_sync.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.iVsync.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_sync.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            //clear
            view.tbl_clear.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.ivClear.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_clear.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            validate!!.SaveSharepreferenceString(AppSP.SHGGUID, guid)
            validate!!.SaveSharepreferenceString(AppSP.ShgName, shgName)
            val i = Intent(context, UpdateQueueStatusActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            context.startActivity(i)
        }

        view.tbl_clear.setOnClickListener {

            //clear
            view.tbl_clear.background =
                context.resources.getDrawable(R.drawable.dialog_selected_bg)
            view.ivClear.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary))
            view.tv_clear.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))

            //shg
            view.tbl_shgprofile.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.transperent
                )
            )
            view.iVshg.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tvShg.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            //Rejection Remarks
            view.tbl_Remarks.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.transperent
                )
            )
            view.iVRejection.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_remarks.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            //meeting
            view.tbl_meeting.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.transperent
                )
            )
            view.ivMeetingpopup.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.dialog_text_clr
                )
            )
            view.tv_meeting.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            //sync
            view.tbl_sync.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.iVsync.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_sync.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            popupWindow.dismiss()
            validate!!.SaveSharepreferenceString(AppSP.SHGGUID, guid)
            validate!!.SaveSharepreferenceString(AppSP.ShgName, shgName)
            if((last_uploaded_date>0 || shgid>0)&& lock ==0){
                (context as ShgListActivity).CustomAlert(guid,1)
            }else if((last_uploaded_date==0L && shgid == 0L) && lock ==0){
                (context as ShgListActivity).CustomAlert(guid,0)
            }else if(lock==1){
                validate!!.CustomAlert(LabelSet.getText(
                    "record_not_deleted",
                    R.string.record_not_deleted
                ),context as ShgListActivity)
            }
        }

        view.tbl_sync.setOnClickListener {

            //shg
            view.tbl_shgprofile.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.transperent
                )
            )
            view.iVshg.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tvShg.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))
            //meeting
            view.tbl_sync.background = context.resources.getDrawable(R.drawable.dialog_selected_bg)
            view.iVsync.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary))
            view.tv_sync.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
            //Rejection Remarks
            view.tbl_Remarks.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.transperent
                )
            )
            view.iVRejection.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.dialog_text_clr
                )
            )
            view.tv_remarks.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))
            //sync
            view.tbl_meeting.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.transperent
                )
            )
            view.ivMeetingpopup.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.dialog_text_clr
                )
            )
            view.tv_meeting.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            //clear
            view.tbl_clear.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.ivClear.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_clear.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))
            popupWindow.dismiss()
            (context as ShgListActivity).getshgdata(shgid)
        }
        view.tbl_shgsummary.setOnClickListener {

            //shg
            view.tbl_shgprofile.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.transperent
                )
            )
            view.iVshg.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tvShg.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))
            //Rejection Remarks
            view.tbl_Remarks.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.transperent
                )
            )
            view.iVRejection.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.dialog_text_clr
                )
            )
            view.tv_remarks.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))
            //Summary
            view.tbl_shgsummary.background =
                context.resources.getDrawable(R.drawable.dialog_selected_bg)
            view.iVshgsummary.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary))
            view.tvShgsummary.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
            //sync
            view.tbl_sync.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.iVsync.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_sync.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            //sync
            view.tbl_meeting.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.transperent
                )
            )
            view.ivMeetingpopup.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.dialog_text_clr
                )
            )
            view.tv_meeting.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            //clear
            view.tbl_clear.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.ivClear.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_clear.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))
            popupWindow.dismiss()
            validate!!.SaveSharepreferenceString(AppSP.SHGGUID, guid)
            validate!!.SaveSharepreferenceString(AppSP.ShgName, shgName)
            validate!!.SaveSharepreferenceString(AppSP.Shgcode, shgGroupCode)
            validate!!.SaveSharepreferenceInt(AppSP.CboType, cbotype!!)
            validate!!.SaveSharepreferenceInt(AppSP.Tag, tag)
            validate!!.SaveSharepreferenceInt(AppSP.LockRecord, lock)
            validate!!.SaveSharepreferenceLong(AppSP.Formation_dt, formationdate)

            if (status == 0) {
                validate!!.CustomAlert(
                    context.getString(R.string.download_shg_data),
                    context as ShgListActivity
                )
            } else {
                val i = Intent(context, SummaryActivity::class.java)
                i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                context.startActivity(i)
            }
        }

        popupWindow.contentView = view

        return popupWindow
    }
}