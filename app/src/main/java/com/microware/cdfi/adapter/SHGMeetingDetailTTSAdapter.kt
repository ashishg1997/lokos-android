package com.microware.cdfi.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.entity.DtmtgDetEntity

class SHGMeetingDetailTTSAdapter(
    var context: Context,
    var datalist: List<DtmtgDetEntity>?,
    var listener: OnItemClickListener
) : RecyclerView.Adapter<SHGMeetingDetailTTSAdapter.SHGMeetingDetailTTSHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SHGMeetingDetailTTSHolder {
        var inflater = LayoutInflater.from(context)
        var view = inflater.inflate(R.layout.shg_meeting_tts_item, parent, false)
        return SHGMeetingDetailTTSHolder(view)
    }

    override fun onBindViewHolder(holder: SHGMeetingDetailTTSHolder, position: Int) {
        holder.name.text = datalist!!.get(position).member_name!!
        holder.speaker.setOnClickListener {
            listener.onItemClick(datalist!!.get(position), position)
        }
    }

    override fun getItemCount(): Int {
        return datalist!!.size
    }

    class SHGMeetingDetailTTSHolder(view: View) : RecyclerView.ViewHolder(view) {
        var name = view.findViewById<TextView>(R.id.tv_user_name)
        var speaker = view.findViewById<ImageView>(R.id.iv_user_speaker)

    }

    interface OnItemClickListener {
        fun onItemClick(dataList: DtmtgDetEntity, pos: Int)
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
}