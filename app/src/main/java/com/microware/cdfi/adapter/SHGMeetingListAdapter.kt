package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.PopupWindow
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.*
import com.microware.cdfi.api.model.SHGMeetingModel
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.meeting_detail_item.*
import kotlinx.android.synthetic.main.shgmeeting_item_dialog.view.*
import kotlinx.android.synthetic.main.shgmeeting_list_item.view.*


class SHGMeetingListAdapter(
    val context: Context,
    val shgList: List<SHGMeetingModel>
) :
    RecyclerView.Adapter<SHGMeetingListAdapter.ViewHolder>() {
    var validate: Validate? = null
    var firstword: String? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view =
            LayoutInflater.from(context).inflate(R.layout.shgmeeting_list_item, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return shgList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if (validate!!.returnLongValue(shgList.get(position).shg_code.toString()) > 0) {
            holder.tv_code.text =
                "" + shgList.get(position).shg_code
        } else {
            holder.tv_code.text = "..."
        }
        holder.tv_nam.text = validate!!.returnStringcapitalize(shgList.get(position).shg_name)
        if (validate!!.returnIntegerValue(shgList.get(position).activation_status.toString()) == 2) {
            if (validate!!.returnIntegerValue(shgList.get(position).status.toString()) == 1) {
                //Pawan //
                holder.tv_count.setBackgroundResource(R.drawable.item_countactive)
                holder.iv_stauscolor.setBackgroundResource(R.drawable.item_countactive)
                //Pawan ///
                if (validate!!.returnIntegerValue(shgList.get(position).shg_type_code.toString()) == 1) {
                    holder.iv_stauscolor.setImageResource(R.drawable.ic_womangroup)
                    holder.iv_stauscolor.visibility = View.INVISIBLE
                } else {
                    holder.iv_stauscolor.visibility = View.VISIBLE
                    if (validate!!.returnIntegerValue(shgList.get(position).tags.toString()) == 1) {
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_pvtg)
                    } else if (validate!!.returnIntegerValue(shgList.get(position).tags.toString()) == 2) {
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_elderly)
                    } else if (validate!!.returnIntegerValue(shgList.get(position).tags.toString()) == 3) {
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_disable)
                    } else if (validate!!.returnIntegerValue(shgList.get(position).tags.toString()) == 99) {
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_other)
                    }
                }
            } else {
                //Pawan //
                holder.tv_count.setBackgroundResource(R.drawable.item_count)
                holder.iv_stauscolor.setBackgroundResource(R.drawable.item_count)
                //Pawan //
                if (validate!!.returnIntegerValue(shgList.get(position).shg_type_code.toString()) == 1) {
                    holder.iv_stauscolor.setImageResource(R.drawable.ic_womangroup)
                    holder.iv_stauscolor.visibility = View.INVISIBLE
                } else {
                    holder.iv_stauscolor.visibility = View.VISIBLE
                    if (validate!!.returnIntegerValue(shgList.get(position).tags.toString()) == 1) {
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_pvtg)
                    } else if (validate!!.returnIntegerValue(shgList.get(position).tags.toString()) == 2) {
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_elderly)
                    } else if (validate!!.returnIntegerValue(shgList.get(position).tags.toString()) == 3) {
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_disable)
                    } else if (validate!!.returnIntegerValue(shgList.get(position).tags.toString()) == 99) {
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_other)
                    }
                }
            }
        } else if (validate!!.returnIntegerValue(shgList.get(position).activation_status.toString()) == 3) {
            if (validate!!.returnIntegerValue(shgList.get(position).status.toString()) == 1) {
                //Pawan //
                holder.tv_count.setBackgroundResource(R.drawable.item_countreject)
                holder.iv_stauscolor.setBackgroundResource(R.drawable.item_countreject)
                //Pawan //
                if (validate!!.returnIntegerValue(shgList.get(position).shg_type_code.toString()) == 1) {
                    holder.iv_stauscolor.setImageResource(R.drawable.ic_womangroup)
                    holder.iv_stauscolor.visibility = View.INVISIBLE
                } else {
                    holder.iv_stauscolor.visibility = View.VISIBLE
                    if (validate!!.returnIntegerValue(shgList.get(position).tags.toString()) == 1) {
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_pvtg)
                    } else if (validate!!.returnIntegerValue(shgList.get(position).tags.toString()) == 2) {
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_elderly)
                    } else if (validate!!.returnIntegerValue(shgList.get(position).tags.toString()) == 3) {
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_disable)
                    } else if (validate!!.returnIntegerValue(shgList.get(position).tags.toString()) == 99) {
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_other)
                    }
                }
            } else {
                //Pawan //
                holder.tv_count.setBackgroundResource(R.drawable.item_count)
                holder.iv_stauscolor.setBackgroundResource(R.drawable.item_count)
                //Pawan //
                if (validate!!.returnIntegerValue(shgList.get(position).shg_type_code.toString()) == 1) {
                    holder.iv_stauscolor.setImageResource(R.drawable.ic_womangroup)
                    holder.iv_stauscolor.visibility = View.INVISIBLE
                } else {
                    holder.iv_stauscolor.visibility = View.VISIBLE
                    if (validate!!.returnIntegerValue(shgList.get(position).tags.toString()) == 1) {
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_pvtg)
                    } else if (validate!!.returnIntegerValue(shgList.get(position).tags.toString()) == 2) {
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_elderly)
                    } else if (validate!!.returnIntegerValue(shgList.get(position).tags.toString()) == 3) {
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_disable)
                    } else if (validate!!.returnIntegerValue(shgList.get(position).tags.toString()) == 99) {
                        holder.iv_stauscolor.setImageResource(R.drawable.ic_other)
                    }
                }
            }
        } else {
            //Pawan //
            holder.tv_count.setBackgroundResource(R.drawable.item_countpending)
            holder.iv_stauscolor.setBackgroundResource(R.drawable.item_countpending)
            //Pawan //
            if (validate!!.returnIntegerValue(shgList.get(position).shg_type_code.toString()) == 1) {
                holder.iv_stauscolor.setImageResource(R.drawable.ic_womangroup)
                holder.iv_stauscolor.visibility = View.INVISIBLE
            } else {
                holder.iv_stauscolor.visibility = View.VISIBLE
                if (validate!!.returnIntegerValue(shgList.get(position).tags.toString()) == 1) {
                    holder.iv_stauscolor.setImageResource(R.drawable.ic_pvtg)
                } else if (validate!!.returnIntegerValue(shgList.get(position).tags.toString()) == 2) {
                    holder.iv_stauscolor.setImageResource(R.drawable.ic_elderly)
                } else if (validate!!.returnIntegerValue(shgList.get(position).tags.toString()) == 3) {
                    holder.iv_stauscolor.setImageResource(R.drawable.ic_disable)
                } else if (validate!!.returnIntegerValue(shgList.get(position).tags.toString()) == 99) {
                    holder.iv_stauscolor.setImageResource(R.drawable.ic_other)
                }
            }
        }
        if (validate!!.returnIntegerValue(shgList.get(position).status.toString()) == 0) {
            holder.tv_status.setBackgroundResource(R.color.red)
        } else if (validate!!.returnIntegerValue(shgList.get(position).is_edited.toString()) == 1) {
            holder.tv_status.setBackgroundResource(R.color.colorPrimary)
        } else {
            holder.tv_status.setBackgroundResource(R.color.khakigreen1)
        }
        var mtglist =
            (context as SHGMeetingListActivity).returnmeetingdata(shgList.get(position).shg_id)
        if (!mtglist.isNullOrEmpty()) {
            holder.tv_date.text = validate!!.convertDatetime(mtglist.get(0).mtg_date)
            holder.tv_id.text = mtglist.get(0).mtg_no.toString()
            holder.tv_guid.text = mtglist.get(0).mtg_guid
            holder.tv_mtgType.text =
                validate!!.returnStringValue(mtglist.get(0).mtg_type.toString())
        } else {
            holder.tv_date.text = ""
            holder.tv_id.text = ""
            holder.tv_guid.text = ""
            holder.tv_mtgType.text = ""
        }
        val name = validate!!.returnStringValue(shgList.get(position).shg_name)
        var count =
            context.getmembercount(shgList.get(position).guid)
        var cbotype =
            validate!!.returnIntegerValue(shgList.get(position).shg_type_code.toString())
        if (!name.isNullOrEmpty()) {
            firstword = name.substring(0, 1).toUpperCase()
        }
        if (count > 0) {
            holder.tv_count.text = "" + count
        } else {
            holder.tv_count.text = "0"
        }
        var islock = 0
        if (validate!!.returnIntegerValue(shgList.get(position).approve_status.toString()) == 1 && validate!!.returnIntegerValue(
                shgList.get(position).is_edited.toString()
            ) == 0
        ) {
            holder.imglock.visibility = View.VISIBLE
            islock = 1
        } else {
            holder.imglock.visibility = View.GONE
            islock = 0
        }
        holder.tv_firstword.text = firstword

        if (validate!!.returnStringValue(shgList.get(position).checker_remarks.toString())
                .trim().length > 0
        ) {
            holder.imgmeetingtatus.visibility = View.VISIBLE
        } else {
            holder.imgmeetingtatus.visibility = View.GONE
        }

        holder.imgmeetingtatus.setOnClickListener {
            validate!!.SaveSharepreferenceLong(
                MeetingSP.shgid,
                validate!!.returnLongValue(shgList.get(position).shg_id.toString())
            )
            validate!!.SaveSharepreferenceInt(MeetingSP.isCheckerRemark, 1)
            val i = Intent(context, MeetingCheckerRemarks::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            context.startActivity(i)
        }

        holder.optionBtn.setOnClickListener {
            var popupwindow: PopupWindow = popupDisplay(
                shgList.get(position).shg_name!!,
                validate!!.returnLongValue(shgList.get(position).shg_code.toString()),
                validate!!.returnLongValue(shgList.get(position).shg_id.toString()),
                validate!!.returnIntegerValue(shgList.get(position).meeting_frequency.toString()),
                validate!!.returnIntegerValue(shgList.get(position).meeting_frequency_value.toString()),
                validate!!.returnIntegerValue(shgList.get(position).meeting_on.toString()),
                validate!!.returnIntegerValue(holder.tv_id.text.toString()),
                validate!!.Daybetweentime(holder.tv_date.text.toString()),
                islock,
                validate!!.returnLongValue(shgList.get(position).shg_formation_date.toString()),
                shgList.get(position).guid,
                validate!!.returnStringValue(holder.tv_guid.text.toString()),
                count,
                validate!!.returnIntegerValue(shgList.get(position).shg_type_code.toString()),
                validate!!.returnIntegerValue(shgList.get(position).tags.toString()),
                validate!!.returnIntegerValue(shgList.get(position).month_comp_saving.toString()),
                validate!!.returnIntegerValue(holder.tv_mtgType.text.toString()),
                validate!!.returnIntegerValue(shgList.get(position).is_voluntary_saving.toString())
            )
            val values = IntArray(2)
            holder.layout.getLocationInWindow(values)
            val positionOfIcon = values[1]

            //Get the height of 2/3rd of the height of the screen
            //Get the height of 2/3rd of the height of the screen
            val displayMetrics: DisplayMetrics = context.getResources().displayMetrics
            val height: Int = displayMetrics.heightPixels * 2 / 3

            //If the position of menu icon is in the bottom 2/3rd part of the screen then we provide menu height as offset  but in negative as we want to open our menu to the top
            //If the position of menu icon is in the bottom 2/3rd part of the screen then we provide menu height as offset  but in negative as we want to open our menu to the top
            if (positionOfIcon > height) {
                popupwindow.showAsDropDown(holder.layout, -20, -300)
            } else {
                popupwindow.showAsDropDown(holder.layout, -20, 0)
            }

        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_status = view.tv_status
        var relative = view.relative
        var cardView = view.cardView
        var tv_firstword = view.tv_firstword
        var tv_code = view.tv_code
        var tv_nam = view.tv_nam
        var tv_date = view.tv_date
        var tv_id = view.tv_id
        var tv_guid = view.tv_guid
        var tv_mtgType = view.tv_mtgType
        var tv_count = view.tv_count
        var iv_stauscolor = view.iv_stauscolor
        var optionBtn = view.optionBtn
        var layout = view.layout
        var imglock = view.imglock
        var tblFirst = view.tblFirst
        var imgmeetingtatus = view.imgmeetingtatus
    }

    fun popupDisplay(
        shgName: String,
        shgcode: Long,
        shg_id: Long,
        meetingfrequency: Int,
        meetingfrequencyvalue: Int,
        meeting_on: Int,
        mtgno: Int,
        mtgdate: Long,
        islock: Int,
        formationdate: Long,
        guid: String,
        mtgguid: String,
        count: Int,
        shgtype: Int,
        tag: Int,
        month_comp_saving: Int,
        mtgType: Int,
        voluntary_saving: Int
    ): PopupWindow {
        val popupWindow = PopupWindow(context)
        // inflate your layout or dynamically add view
        val inflater =
            context.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.shgmeeting_item_dialog, null, false)
        popupWindow.isFocusable = true
        popupWindow.width = WindowManager.LayoutParams.WRAP_CONTENT
        popupWindow.height = WindowManager.LayoutParams.WRAP_CONTENT
        popupWindow.setBackgroundDrawable(ColorDrawable(context.resources.getColor(R.color.white)))

        view.tvgeneratemeeting.text = LabelSet.getText(
            "generate_meeting",
            R.string.generate_meeting
        )
        view.tvopen_meeting.text = LabelSet.getText(
            "open_meeting",
            R.string.open_meeting
        )
        view.tv_sync.text = LabelSet.getText(
            "download_meeting",
            R.string.download_meeting
        )
        view.tv_queueStatus.text = LabelSet.getText(
            "view_queue_status",
            R.string.view_queue_status
        )

        fun setBackGroundColur(iScreenFlag: Int) {
            //genratemeeting
            if (iScreenFlag == 1) {
                view.tbl_generatemeeting.background =
                    context.resources.getDrawable(R.drawable.dialog_selected_bg)
            } else {
                view.tbl_generatemeeting.setBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        R.color.transperent
                    )
                )
            }
            view.iVshg.setColorFilter(
                ContextCompat.getColor(
                    context,
                    if (iScreenFlag == 1) R.color.colorPrimary else R.color.dialog_text_clr
                )
            )
            view.tvgeneratemeeting.setTextColor(
                ContextCompat.getColor(
                    context,
                    if (iScreenFlag == 1) R.color.colorPrimary else R.color.dialog_text_clr
                )
            )

            //openmeeting
            if (iScreenFlag == 2) {
                view.tbl_open_meeting.background =
                    context.resources.getDrawable(R.drawable.dialog_selected_bg)
            } else {
                view.tbl_open_meeting.setBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        R.color.transperent
                    )
                )
            }
            view.iVshgsummary.setColorFilter(
                ContextCompat.getColor(
                    context,
                    if (iScreenFlag == 2) R.color.colorPrimary else R.color.dialog_text_clr
                )
            )
            view.tvopen_meeting.setTextColor(
                ContextCompat.getColor(
                    context,
                    if (iScreenFlag == 2) R.color.colorPrimary else R.color.dialog_text_clr
                )
            )

            //sync
            if (iScreenFlag == 3) {
                view.tbl_sync.background =
                    context.resources.getDrawable(R.drawable.dialog_selected_bg)
            } else {
                view.tbl_sync.setBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        R.color.transperent
                    )
                )
            }
            view.iVsync.setColorFilter(
                ContextCompat.getColor(
                    context,
                    if (iScreenFlag == 3) R.color.colorPrimary else R.color.dialog_text_clr
                )
            )
            view.tv_sync.setTextColor(
                ContextCompat.getColor(
                    context,
                    if (iScreenFlag == 3) R.color.colorPrimary else R.color.dialog_text_clr
                )
            )
        }

        view.tbl_generatemeeting.setOnClickListener {

            setBackGroundColur(1)

            validate!!.SaveSharepreferenceString(MeetingSP.ShgName, shgName)
            validate!!.SaveSharepreferenceLong(MeetingSP.Shgcode, shgcode)
            validate!!.SaveSharepreferenceLong(MeetingSP.shgid, shg_id)
            validate!!.SaveSharepreferenceInt(MeetingSP.meetingfrequency, meetingfrequency)
            validate!!.SaveSharepreferenceInt(
                MeetingSP.meetingfrequencyvalue,
                meetingfrequencyvalue
            )
            validate!!.SaveSharepreferenceInt(MeetingSP.meeting_on, meeting_on)
            validate!!.SaveSharepreferenceInt(MeetingSP.GroupLastMeetingNumber, mtgno)
            validate!!.SaveSharepreferenceInt(MeetingSP.maxmeetingnumber, mtgno)
            validate!!.SaveSharepreferenceLong(MeetingSP.GroupLastMeetingDate, mtgdate)
            validate!!.SaveSharepreferenceInt(MeetingSP.LockRecord, islock)
            validate!!.SaveSharepreferenceInt(MeetingSP.MemberCount, count)
            validate!!.SaveSharepreferenceLong(MeetingSP.Formation_dt, formationdate)
            validate!!.SaveSharepreferenceString(MeetingSP.SHGGUID, guid)
            validate!!.SaveSharepreferenceString(MeetingSP.mtg_guid, mtgguid)
            validate!!.SaveSharepreferenceInt(MeetingSP.SHGType, shgtype)
            validate!!.SaveSharepreferenceInt(MeetingSP.Tag, tag)
            validate!!.SaveSharepreferenceInt(MeetingSP.Month_comp_saving, month_comp_saving)
            validate!!.SaveSharepreferenceInt(MeetingSP.isVoluntarySaving, voluntary_saving)
            var mtg_count = (context as SHGMeetingListActivity).getMeetingCountByCboId(shg_id)
            if (mtg_count > 0) {
                var mtgstatus = context.returnmeetingstatus(shg_id, mtgno)
                if (mtgstatus.equals("C", ignoreCase = true)) {
                    if(context.sCheckValidation(meetingfrequency,shg_id)==1){
                        val i = Intent(context, GenerateMeetingActivity::class.java)
                        i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                        context.startActivity(i)
                    }
                } else {
                    validate!!.CustomAlert(
                        LabelSet.getText(
                            "pleaseclosemeeting",
                            R.string.pleaseclosemeeting
                        ), context
                    )
                }
            } else {
                val i = Intent(context, GenerateMeetingActivity::class.java)
                i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                context.startActivity(i)
            }
        }

        view.tbl_open_meeting.setOnClickListener {

            setBackGroundColur(2)

            validate!!.SaveSharepreferenceString(MeetingSP.ShgName, shgName)
            validate!!.SaveSharepreferenceLong(MeetingSP.Shgcode, shgcode)
            validate!!.SaveSharepreferenceLong(MeetingSP.shgid, shg_id)
            validate!!.SaveSharepreferenceInt(MeetingSP.meetingfrequency, meetingfrequency)
            validate!!.SaveSharepreferenceInt(
                MeetingSP.meetingfrequencyvalue,
                meetingfrequencyvalue
            )
            validate!!.SaveSharepreferenceInt(MeetingSP.meeting_on, meeting_on)
            validate!!.SaveSharepreferenceInt(MeetingSP.GroupLastMeetingNumber, mtgno)
            validate!!.SaveSharepreferenceLong(MeetingSP.GroupLastMeetingDate, mtgdate)
            validate!!.SaveSharepreferenceLong(MeetingSP.CurrentMtgDate, mtgdate)
            validate!!.SaveSharepreferenceInt(MeetingSP.LockRecord, islock)
            validate!!.SaveSharepreferenceInt(MeetingSP.MemberCount, count)
            validate!!.SaveSharepreferenceLong(MeetingSP.Formation_dt, formationdate)
            validate!!.SaveSharepreferenceString(MeetingSP.SHGGUID, guid)
            validate!!.SaveSharepreferenceInt(MeetingSP.currentmeetingnumber, mtgno)
            validate!!.SaveSharepreferenceInt(MeetingSP.maxmeetingnumber, mtgno)
            validate!!.SaveSharepreferenceInt(MeetingSP.SHGType, shgtype)
            validate!!.SaveSharepreferenceInt(MeetingSP.Tag, tag)
            validate!!.SaveSharepreferenceInt(MeetingSP.Month_comp_saving, month_comp_saving)
            validate!!.SaveSharepreferenceInt(MeetingSP.MeetingType, mtgType)
            validate!!.SaveSharepreferenceString(MeetingSP.mtg_guid, mtgguid)
            validate!!.SaveSharepreferenceInt(MeetingSP.isVoluntarySaving, voluntary_saving)
            var mtg_count = (context as SHGMeetingListActivity).getMeetingCountByCboId(shg_id)
            if (mtg_count > 0) {
                context.openmeeting(shg_id, mtgno)
                if (mtgType == 0) {
                    val i = Intent(context, MeetingMenuActivity::class.java)
                    i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                    context.startActivity(i)
                } else if (mtgType == 11 || mtgType == 12) {
                    val i = Intent(context, CutOffMeetingMenuActivity::class.java)
                    i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                    context.startActivity(i)
                }
            } else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "meeting_not_exists",
                        R.string.meeting_not_exists
                    ), context
                )

            }
        }

        view.tbl_sync.setOnClickListener {

            popupWindow.dismiss()
            setBackGroundColur(3)
            validate!!.SaveSharepreferenceLong(MeetingSP.shgid, shg_id)
            if (validate!!.isNetworkConnected(context as SHGMeetingListActivity)) {
                context.importSHGMeetinglist()
            } else {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "no_internet_msg",
                        R.string.no_internet_msg
                    ), context
                )

            }


        }

        view.tbl_approval_status.setOnClickListener {

            popupWindow.dismiss()
            setBackGroundColur(4)
            validate!!.SaveSharepreferenceLong(MeetingSP.shgid, shg_id)
            if (validate!!.isNetworkConnected(context as SHGMeetingListActivity)) {
                context.importMeeting_Approval_Rejection_status()
            }else{
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "no_internet_msg",
                        R.string.no_internet_msg
                    ), context
                )

            }

        }

        view.tbl_queueStatus.setOnClickListener {

            popupWindow.dismiss()
            setBackGroundColur(5)
            validate!!.SaveSharepreferenceString(MeetingSP.ShgName, shgName)
            validate!!.SaveSharepreferenceLong(MeetingSP.Shgcode, shgcode)
            validate!!.SaveSharepreferenceLong(MeetingSP.CurrentMtgDate, mtgdate)
            validate!!.SaveSharepreferenceInt(MeetingSP.MemberCount, count)
            validate!!.SaveSharepreferenceLong(MeetingSP.shgid, shg_id)
            validate!!.SaveSharepreferenceInt(MeetingSP.isCheckerRemark, 0)

            val i = Intent(context, MeetingCheckerRemarks::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            context.startActivity(i)
        }
        popupWindow.contentView = view

        return popupWindow
    }


}