package com.microware.cdfi.adapter

import android.content.Context
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.ScenarioOneLoanRepaymentGroupActivity
import com.microware.cdfi.entity.DtLoanGpTxnEntity
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.scenariooneloanrepaymentgroup_listitem.view.*

class ScenarioOneLoanRepaymentGroupAdapter (var context: Context, val list: List<DtLoanGpTxnEntity>?) :
    RecyclerView.Adapter<ScenarioOneLoanRepaymentGroupAdapter.ViewHolder>() {
    var validate: Validate?=null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        val view = LayoutInflater.from(context).inflate(R.layout.scenariooneloanrepaymentgroup_listitem, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_loan_no.text =
            validate!!.returnIntegerValue(list!!.get(position).loan_no.toString()).toString()
        holder.tv_outstanding.text =
            validate!!.returnIntegerValue(list.get(position).loan_op.toString()).toString()+"("+(context as ScenarioOneLoanRepaymentGroupActivity).getremaininginstallment(validate!!.returnIntegerValue(
                list.get(position).loan_no.toString()))+")"

        var demand=validate!!.returnIntegerValue(list.get(position).principal_demand_ob.toString())+validate!!.returnIntegerValue(list.get(position).principal_demand.toString())

        var intdemand=validate!!.returnIntegerValue(list.get(position).int_accrued_op.toString())+validate!!.returnIntegerValue(list.get(position).int_accrued.toString())
        var totpaid=validate!!.returnIntegerValue(list.get(position).loan_paid.toString())+validate!!.returnIntegerValue(list.get(position).loan_paid_int.toString())
        holder.tv_principal_demand.text = LabelSet.getText(
            "rs",
            R.string.rs
        )+demand
        val areartext=LabelSet.getText(
            "rs",
            R.string.rs
        ) + validate!!.returnIntegerValue(list.get(position).principal_demand.toString())+
                " <font color=#FC6042> + "+validate!!.returnIntegerValue(list.get(position).principal_demand_ob.toString())+"</font>"
        val intareartext=LabelSet.getText(
            "rs",
            R.string.rs
        ) + validate!!.returnIntegerValue(list.get(position).int_accrued.toString())+
                " <font color=#FC6042> + "+validate!!.returnIntegerValue(list.get(position).int_accrued_op.toString())+"</font>"
        holder.tv_current_arrear.text = Html.fromHtml(areartext)


        holder.tv_interest_demand.text = LabelSet.getText(
            "rs",
            R.string.rs
        )+intdemand
        holder.tv_current_arrear1.text = Html.fromHtml(intareartext)
        holder.tv_total.text = LabelSet.getText("rs", R.string.rs)+(demand+intdemand)
        holder.btnPay.text = LabelSet.getText("rs", R.string.rs)+(demand+intdemand)
        if(totpaid>0){
            holder.et_principal_demand.setText(totpaid.toString())
        }else {
            holder.et_principal_demand.setText("")
        }

        holder.tv_totintrest.text = LabelSet.getText(
            "rs",
            R.string.rs
        )+(intdemand)+" "+ LabelSet.getText(
            "asintrest",
            R.string.asintrest
        )

        (context as ScenarioOneLoanRepaymentGroupActivity).getTotalValue(demand,intdemand,demand+intdemand,totpaid)

        holder.et_principal_demand.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var amt = validate!!.returnIntegerValue(list.get(position).loan_op.toString())+validate!!.returnIntegerValue(list.get(position).int_accrued_op.toString())+validate!!.returnIntegerValue(list.get(position).int_accrued.toString())

                if(amt<validate!!.returnIntegerValue(s.toString())){
                    holder.et_principal_demand.setText("")
                }
                (context as ScenarioOneLoanRepaymentGroupActivity).getTotalValue()
            }

        })
        if(validate!!.returnIntegerValue(list.get(position).mode_payment.toString())==1){
            holder.tvCash.visibility= View.VISIBLE
            holder.tvBank.visibility= View.GONE
        }else if(validate!!.returnIntegerValue(list.get(position).mode_payment.toString())==2||validate!!.returnIntegerValue(list.get(position).mode_payment.toString())==3){
            holder.tvaccountno.text = validate!!.returnStringValue(list.get(position).bank_code)
            holder.tvtransactionno.text = validate!!.returnStringValue(list.get(position).transaction_no)
            holder.tvCash.visibility= View.GONE
            holder.tvBank.visibility= View.VISIBLE

        }
        holder.tvBank.setOnClickListener {
            holder.tvCash.visibility= View.VISIBLE
            holder.tvBank.visibility= View.GONE
            holder.tvaccountno.text = ""
            holder.tvtransactionno.text = ""
        }
        holder.tvCash.setOnClickListener {
            (context as ScenarioOneLoanRepaymentGroupActivity).CustomAlertBankDialog(holder.tvaccountno,holder.tvtransactionno, holder.tvCash, holder.tvBank)

        }
        if(list.get(position).completion_flag!!){
            holder.ll_loans.background=context.getDrawable(R.drawable.item_rectangle)
            holder.ll_principaldemand.background=context.getDrawable(R.drawable.item_rectangle)
            holder.ll_intdemand.background=context.getDrawable(R.drawable.item_rectangle)
            holder.ll_total.background=context.getDrawable(R.drawable.item_rectangle)
            holder.ll_pay.background=context.getDrawable(R.drawable.item_rectangle)
        }else{
            holder.ll_loans.background=context.getDrawable(R.drawable.textboxback1)
            holder.ll_principaldemand.background=context.getDrawable(R.drawable.textboxback1)
            holder.ll_intdemand.background=context.getDrawable(R.drawable.textboxback1)
            holder.ll_total.background=context.getDrawable(R.drawable.textboxback1)
            holder.ll_pay.background=context.getDrawable(R.drawable.textboxback1)
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val ll_loans = view.ll_loans
        val ll_principaldemand = view.ll_principaldemand
        val ll_intdemand = view.ll_intdemand
        val ll_total = view.ll_total
        val ll_pay = view.ll_pay
        val tv_loan_no=view.tv_loan_no
        val tv_outstanding=view.tv_outstanding
        val tv_principal_demand=view.tv_principal_demand
        val tv_current_arrear=view.tv_current_arrear
        val tv_interest_demand=view.tv_interest_demand
        val tv_current_arrear1=view.tv_current_arrear1
        val tv_total=view.tv_total
        val btnPay=view.btnPay
        val tvCash=view.tvCash
        val tvBank=view.tvBank
        val et_principal_demand=view.et_principal_demand
        val tv_totintrest=view.tv_totintrest
        val tvaccountno=view.tvaccountno
        val tvtransactionno=view.tvtransactionno
    }

}