package com.microware.cdfi.adapter

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.SettlementActivity
import com.microware.cdfi.activity.meeting.SettlementListActivity
import com.microware.cdfi.entity.DtmtgDetEntity
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.repay_toolbar.view.*
import kotlinx.android.synthetic.main.settlement_detail_item.view.*

class SettlementDetailAdapter(
    var context: Context,
    var list: List<DtmtgDetEntity>
) :
    RecyclerView.Adapter<SettlementDetailAdapter.ViewHolder>() {
    var validate:Validate?=null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate= Validate(context)
        var view =
            LayoutInflater.from(context).inflate(R.layout.settlement_detail_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_memberName.text = list.get(position).member_name.toString()
        holder.tv_srno.text = (position + 1).toString()
        var tot=validate!!.returnIntegerValue(list.get(position).sav_comp_cb.toString())+validate!!.returnIntegerValue(list.get(position).sav_vol_cb.toString())
        holder.tv_totsaved.text = tot.toString()
        holder.tv_withdrawl.text = validate!!.returnIntegerValue(list.get(position).sav_comp_withdrawal.toString()).toString()
        setLabelText(holder)
        (context as SettlementListActivity).getTotalValue(tot, 1)
        (context as SettlementListActivity).getTotalValue(validate!!.returnIntegerValue(list.get(position).sav_comp_withdrawal.toString()), 2)

        validate!!.SaveSharepreferenceString(MeetingSP.MemberName,list.get(position).member_name.toString())

        holder.tv_withdrawl.setOnClickListener(View.OnClickListener {
            validate!!.SaveSharepreferenceLong(MeetingSP.Memberid,validate!!.returnLongValue(list.get(position).mem_id.toString()))
            var inetnt = Intent(context, SettlementActivity::class.java)
            context.startActivity(inetnt) // (context as SettlementActivity).CustomAlert()
        })

      /*  holder.tv_count.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                (context as WidthdrawalDetailActivity).getTotalValue()
            }

        })*/

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_srno = view.tv_srno
        val tv_memberName = view.tv_memberName
        val tv_totsaved = view.tv_totsaved
        val tv_withdrawl = view.tv_withdrawl
    }

    fun setLabelText(view: ViewHolder) {

    }


}