package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.ShareCapitalOtherPaymentDetailActivity
import com.microware.cdfi.activity.meeting.ShareCapitalOtherPaymentListActivity
import com.microware.cdfi.api.meetingmodel.ShareCapitalModel
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.share_capital_receipt_list_item.view.*

class ShareCapitalOtherPaymentAdapter (var context: Context, var list: List<ShareCapitalModel>) :
    RecyclerView.Adapter<ShareCapitalOtherPaymentAdapter.ViewHolder>() {
    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.share_capital_payment_list_item, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_srno.text = (position + 1).toString()
        holder.tv_memberName.text = list[position].member_name.toString()

        var payment = validate!!.returnIntegerValue(list[position].auid.toString())
        if(payment>0) {
            val paymnetName = (context as ShareCapitalOtherPaymentListActivity).getPaymentType(
                validate!!.returnIntegerValue(list[position].auid.toString())
            )
            if (!paymnetName.isNullOrEmpty()) {
                holder.tv_receiptType.text = paymnetName
            }
        }else{
            holder.tv_receiptType.text = ""
        }

        if (validate!!.returnIntegerValue(list[position].amount.toString()) == 0) {
            holder.tv_amount.text = "0"
            holder.tv_srno.setTextColor(context.resources.getColor(R.color.grey))
            holder.tv_receiptType.setTextColor(context.resources.getColor(R.color.grey))
            holder.tv_memberName.setTextColor(context.resources.getColor(R.color.grey))
            holder.tv_amount.setTextColor(context.resources.getColor(R.color.grey))
            holder.ivEdit.isEnabled = false
        } else {
            holder.tv_amount.text = list[position].amount.toString()
            holder.tv_srno.setTextColor(context.resources.getColor(R.color.colorAccent))
            holder.tv_receiptType.setTextColor(context.resources.getColor(R.color.colorAccent))
            holder.tv_memberName.setTextColor(context.resources.getColor(R.color.colorAccent))
            holder.tv_amount.setTextColor(context.resources.getColor(R.color.colorAccent))
            holder.ivEdit.isEnabled = true
        }

        holder.ivAdd.setOnClickListener {
            validate!!.SaveSharepreferenceString(MeetingSP.mtg_guid, list[position].mtg_guid!!)
//            validate!!.SaveSharepreferenceInt(MeetingSP.currentmeetingnumber, validate!!.returnIntegerValue(list[position].mtgno.toString()))
            validate!!.SaveSharepreferenceLong(MeetingSP.shgid, list[position].cbo_id!!)
            validate!!.SaveSharepreferenceLong(MeetingSP.Memberid, list[position].mem_id!!)
            validate!!.SaveSharepreferenceInt(MeetingSP.Auid, 0)
            val intent = Intent(context, ShareCapitalOtherPaymentDetailActivity::class.java)
            context.startActivity(intent)
        }

        holder.ivEdit.setOnClickListener {
            validate!!.SaveSharepreferenceString(MeetingSP.mtg_guid, list[position].mtg_guid!!)
//            validate!!.SaveSharepreferenceInt(MeetingSP.currentmeetingnumber, validate!!.returnIntegerValue(list[position].mtgno.toString()))
            validate!!.SaveSharepreferenceLong(MeetingSP.shgid, list[position].cbo_id!!)
            validate!!.SaveSharepreferenceLong(MeetingSP.Memberid, list[position].mem_id!!)
            validate!!.SaveSharepreferenceInt(
                MeetingSP.Auid,
                validate!!.returnIntegerValue(list[position].auid.toString())
            )
            val intent = Intent(context, ShareCapitalOtherPaymentDetailActivity::class.java)
            context.startActivity(intent)
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_srno = view.tv_srno
        var tv_memberName = view.tv_memberName
        var tv_receiptType = view.tv_receiptType
        var tv_amount = view.tv_amount
        var ivEdit = view.ivEdit
        var ivAdd = view.ivAdd
    }
}