package com.microware.cdfi.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import kotlinx.android.synthetic.main.shg_to_vo_receipt_item.view.*


class ShgtoVoReceiptAdapter(var context: Context) :
    RecyclerView.Adapter<ShgtoVoReceiptAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.shg_to_vo_receipt_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return 7
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        setLabelText(holder)

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_srno=view.tv_srno
        val tv_receipthead=view.tv_receipthead
        val cardView=view.cardView
        val tv_amount_value=view.tv_amount_value
        val img_edit=view.img_edit
    }

    fun setLabelText(view: ViewHolder) {

    }
}