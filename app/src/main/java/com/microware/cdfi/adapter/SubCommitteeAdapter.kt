package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vo.VoSCMemberActivity
import com.microware.cdfi.activity.vo.VoSubCommiteeDetail
import com.microware.cdfi.activity.vo.VoSubCommiteeList
import com.microware.cdfi.entity.subcommitee_memberEntity
import com.microware.cdfi.entity.subcommitteeEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.Subcommitee_memberViewModel
import com.microware.cdfi.viewModel.SubcommitteeViewModel
import kotlinx.android.synthetic.main.layout_subcommittee_adapter.view.*
import kotlinx.android.synthetic.main.layout_subcommittee_adapter.view.add_ec
import kotlinx.android.synthetic.main.layout_subcommittee_adapter.view.imageArrow
import kotlinx.android.synthetic.main.layout_subcommittee_adapter.view.linear_member
import kotlinx.android.synthetic.main.layout_subcommittee_adapter.view.lyChildList
import kotlinx.android.synthetic.main.layout_subcommittee_adapter.view.tblHeading
import kotlinx.android.synthetic.main.layout_subcommittee_adapter.view.tbl_shgName
import kotlinx.android.synthetic.main.layout_subcommittee_adapter.view.tv_ShgName
import kotlinx.android.synthetic.main.sc_member__adapter.view.*

class SubCommitteeAdapter(var context : Context, var committeeEntity: List<subcommitteeEntity>, var committeeViewModel: SubcommitteeViewModel,var scMemberViewmodel: Subcommitee_memberViewModel): RecyclerView.Adapter<SubCommitteeAdapter.ViewHolder>() {

    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.layout_subcommittee_adapter, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return committeeEntity.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var committeeName = (context as VoSubCommiteeList).returnCommitteeName(validate!!.returnIntegerValue(
            committeeEntity.get(position).subcommitee_type_id.toString()))
     //   var executive_count = executiveViewmodel!!.getExecutive_count(validate!!.returnStringValue(executiveEntity!!.get(position).cbo_guid),validate!!.returnLongValue(executiveEntity!!.get(position).ec_cbo_code.toString()))
        holder.tv_ShgName.text = committeeName

        holder.tv_Name.text = LabelSet.getText(
            "name",
            R.string.name
        )
        holder.tv_nomination.text = LabelSet.getText(
            "nomination",
            R.string.nomination
        )

        holder.tv_formation.text = LabelSet.getText(
            "formation_date",
            R.string.formation_date
        )+":"+" " + validate!!.convertDatetime(validate!!.returnLongValue(committeeEntity.get(position).fromdate.toString()))
     //   holder.tvTotal.setText(context.getString(R.string.total)+":"+" "+ validate!!.returnStringValue(executive_count.toString()))

        holder.add_ec.setOnClickListener {
            validate!!.SaveSharepreferenceString(AppSP.SubCommGuid,validate!!.returnStringValue(
                committeeEntity.get(position).subcommitee_guid))
            validate!!.SaveSharepreferenceInt(AppSP.SCTypeId,validate!!.returnIntegerValue(
                committeeEntity.get(position).subcommitee_type_id.toString()))
            validate!!.SaveSharepreferenceLong(AppSP.SCMemebrCode,0L)
            var inetnt = Intent(context, VoSCMemberActivity::class.java)
            context.startActivity(inetnt)
        }
        holder.linear_card.setOnClickListener{
            validate!!.SaveSharepreferenceString(AppSP.SubCommGuid,validate!!.returnStringValue(
                committeeEntity.get(position).subcommitee_guid))
            var inetnt = Intent(context, VoSubCommiteeDetail::class.java)
            context.startActivity(inetnt)

        }

        holder.imageArrow.setOnClickListener{
            validate!!.SaveSharepreferenceString(AppSP.SubCommGuid,validate!!.returnStringValue(
                committeeEntity.get(position).subcommitee_guid))
            validate!!.SaveSharepreferenceInt(AppSP.SCTypeId,validate!!.returnIntegerValue(
                committeeEntity.get(position).subcommitee_type_id.toString()))
            validate!!.SaveSharepreferenceLong(AppSP.scFromDate,validate!!.returnLongValue(
                committeeEntity.get(position).fromdate.toString()))

            holder.imageArrow.rotation=180f

            if (holder.linear_member.visibility== View.VISIBLE)
            {
                holder.linear_member.visibility= View.GONE
                holder.imageArrow.rotation=0f

            }else{

                holder.linear_member.visibility= View.VISIBLE

            }
        }
        var scMemberList = scMemberViewmodel.getCommitteeMemberdetaildata(validate!!.returnStringValue(
            committeeEntity.get(position).subcommitee_guid))

        setData(scMemberList,holder.lyChildList,holder.linear_member,holder.imageArrow)
    }

    fun setData(scMemberList: List<subcommitee_memberEntity>?, linearChildList: LinearLayout,lay_member: LinearLayout,img_Arrow:ImageView) {

        linearChildList.removeAllViews()
        for (i in 0 until scMemberList!!.size) {
            val mInflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val convertView: View =
                mInflater.inflate(R.layout.sc_member__adapter, linearChildList, false)
            var memberName = (context as VoSubCommiteeList).setMember(validate!!.returnLongValue(scMemberList.get(i).ec_member_code.toString()))
            linearChildList.addView(convertView)
            convertView.tvmember_name.text = memberName

            convertView.tvdateValue.text = validate!!.convertDatetime(validate!!.returnLongValue(scMemberList.get(i).fromdate.toString()))


            convertView.ivEdit.setOnClickListener {
                validate!!.SaveSharepreferenceLong(AppSP.SCMemebrCode,validate!!.returnLongValue(scMemberList.get(i).ec_member_code.toString()))
                var inetnt = Intent(context, VoSCMemberActivity::class.java)
                context.startActivity(inetnt)
            }

            convertView.ivDelete.setOnClickListener {
                if(validate!!.returnLongValue(scMemberList.get(i).last_uploaded_date.toString())>0){
                    (context as VoSubCommiteeList).CustomAlert(validate!!.returnStringValue(scMemberList.get(i).ec_member_guid),1)
                }else {
                    (context as VoSubCommiteeList).CustomAlert(validate!!.returnStringValue(scMemberList.get(i).ec_member_guid),0)
                }
                lay_member.visibility= View.GONE
                img_Arrow.rotation=0f
            }
        }

    }
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tv_ShgName = view.tv_ShgName
        var tv_formation = view.tv_formation
        var lyChildList = view.lyChildList
        var tblHeading = view.tblHeading
        var tv_Name = view.tv_Name
        var tv_nomination = view.tv_nomination
        var linear_member = view.linear_member
        var linear_card = view.linear_card
        var tbl_shgName = view.tbl_shgName
        var imageArrow = view.imageArrow
        var add_ec = view.add_ec
    }
}