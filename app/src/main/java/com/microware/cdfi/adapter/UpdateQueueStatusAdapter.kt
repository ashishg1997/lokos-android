package com.microware.cdfi.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.UpdateQueueStatusActivity
import com.microware.cdfi.entity.MemberEntity
import com.microware.cdfi.entity.TransactionEntity
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.layout_staus_update_card_item.view.*
import kotlinx.android.synthetic.main.queue_listitem.view.*

class UpdateQueueStatusAdapter(
    var context: Context, var listData: List<TransactionEntity>) :
    RecyclerView.Adapter<UpdateQueueStatusAdapter.ViewHolder>() {

    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.queue_listitem, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listData.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (listData[position].transaction_type == 2){
            holder.lyChildList.visibility = View.VISIBLE
            holder.tbl_Remarks.visibility = View.GONE
            holder.lyChildList.removeAllViews()
            val list = (context as UpdateQueueStatusActivity).getmemberList(listData[position].shg_guid!!)
            for (i in 0 until list.size){
                val mInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val convertView: View =
                    mInflater.inflate(R.layout.layout_staus_update_card_item, holder.lyChildList, false)
                holder.lyChildList.addView(convertView)
                try {
                    val memberName = convertView.tvMemberName
                    val memberRemark = convertView.tv_remarks1
                    memberName.text = list[i].member_name
                    memberRemark.text = list[i].remarks
                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }
        }else{
            holder.lyChildList.visibility = View.GONE
            holder.tbl_Remarks.visibility = View.VISIBLE
        }

        val shgName = (context as UpdateQueueStatusActivity).getshgName(listData[position].shg_guid!!)
        holder.tvShgName.text = shgName
        holder.tv_remarks.text = listData[position].remarks

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvShgName = view.tvShgName
        val tv_remarks = view.tv_remarks
        val lyChildList = view.lyChildList
        val tbl_Remarks = view.tbl_Remarks

    }
}