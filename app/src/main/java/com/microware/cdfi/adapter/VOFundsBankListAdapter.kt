package com.microware.cdfi.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vo.FundsWithBankActivity
import com.microware.cdfi.entity.VoCoaMappingEntity
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.list_item_funds_bank.view.*

class VOFundsBankListAdapter(
    var context: Context,
    var list: List<VoCoaMappingEntity>
) : RecyclerView.Adapter<VOFundsBankListAdapter.ViewHolder>() {
    var valdiate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        valdiate = Validate(context)
        val view = LayoutInflater.from(context).inflate(R.layout.list_item_funds_bank, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var colorRes = 0
        holder.tvFunds.text= (context as FundsWithBankActivity).getname(list[position].auid)
        val bankCode = valdiate!!.returnStringValue(list[position].bankcode)

        if (!bankCode.isNullOrEmpty()){
            holder.tvbank.text= bankCode
            when ((context as FundsWithBankActivity).getbankpos(valdiate!!.returnStringValue(bankCode)) ) {
                0 -> colorRes = R.color.color1
                1 -> colorRes = R.color.color2
                2 -> colorRes = R.color.color3
                3 -> colorRes = R.color.color4
                4 -> colorRes = R.color.color5
                5 -> colorRes = R.color.color6
                6 -> colorRes = R.color.color7
                else -> colorRes = R.color.grey
            }
        }else{
            colorRes = R.color.light_gray
        }

        holder.ivPayLocation.setColorFilter(
            ContextCompat.getColor(context, colorRes),
            android.graphics.PorterDuff.Mode.SRC_IN
        )

        holder.ivPayLocation.setOnClickListener {
            (context as FundsWithBankActivity).CustomAlertBankDialog(list[position].auid)
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var cardView = view.cardView
        var tvFunds = view.tvFunds
        var tvbank = view.tvbank
        var ivPayLocation = view.ivPayLocation

    }

}