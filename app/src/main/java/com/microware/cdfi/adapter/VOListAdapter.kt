package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.PopupWindow
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vo.VOListActivity
import com.microware.cdfi.activity.vo.VORemarksListActivity
import com.microware.cdfi.activity.vo.VoBasicDetailActivity
import com.microware.cdfi.entity.FederationEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.shg_item_dialog.view.*
import kotlinx.android.synthetic.main.vo_list_item.view.*

class VOListAdapter(val context: Context, val fedrationList: List<FederationEntity>?,val cboType:Int) :
    RecyclerView.Adapter<VOListAdapter.ViewHolder>() {
    var validate: Validate? = null
    var firstword:String? = null

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tv_status = view.tv_status
        var tv_code = view.tv_code
        var cardView = view.cardView
        var tv_firstword = view.tv_firstword
        var tv_nam = view.tv_nam
        var tv_date = view.tv_date
        var tv_count = view.tv_count
        var optionBtn = view.optionBtn
        var layout = view.layout
        var imglock = view.imglock
        var imgactivationstatus = view.imgactivationstatus
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        val view = LayoutInflater.from(context).inflate(R.layout.vo_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if (validate!!.returnIntegerValue(fedrationList!!.get(position).approve_status.toString()) == 3) {
            holder.imgactivationstatus.visibility = View.VISIBLE
        } else {
            holder.imgactivationstatus.visibility = View.GONE
        }
        var islock = 0
        if (validate!!.returnIntegerValue(fedrationList.get(position).approve_status.toString()) == 1
            && validate!!.returnIntegerValue(fedrationList.get(position).is_edited.toString()) == 0) {
            holder.imglock.visibility = View.VISIBLE
            islock = 1
        } else {
            holder.imglock.visibility = View.GONE
            islock = 0
        }
        holder.tv_nam.text = fedrationList.get(position).federation_name
        if (validate!!.returnLongValue(fedrationList.get(position).federation_code.toString()) > 0L) {
            holder.tv_code.text =
                validate!!.returnStringValue(fedrationList.get(position).federation_code.toString())
        } else {
            holder.tv_code.text = "..."
        }
        holder.tv_date.text = validate!!.convertDatetime(fedrationList.get(position).federation_formation_date!!)
        val name = fedrationList.get(position).federation_name

        if(validate!!.returnStringValue(fedrationList.get(position).checker_remark).trim().length>0){
            holder.imgactivationstatus.visibility = View.VISIBLE
        }else {
            holder.imgactivationstatus.visibility = View.GONE
        }
        val count = (context as VOListActivity).getEcMembercount(fedrationList.get(position).guid)
        val mappedshgcount = (context).getMappedShgCount(fedrationList.get(position).guid)
        val mapped_vo_count = (context).getMappedVoCount(fedrationList.get(position).guid)
        if (cboType == 1 && mappedshgcount > 0) {
            holder.tv_count.text = mappedshgcount.toString()

        } else if(cboType == 2 && mapped_vo_count>0){
            holder.tv_count.text = mapped_vo_count.toString()
        }else {
            holder.tv_count.text = "0"
        }
        if (validate!!.returnIntegerValue(fedrationList.get(position).activation_status.toString()) == 2) {
            //holder.imgactivationstatus.visibility=View.GONE
            if (validate!!.returnIntegerValue(fedrationList.get(position).status.toString()) == 1) {
                holder.tv_count.setBackgroundResource(R.drawable.item_countactive)
            }


        } else if (validate!!.returnIntegerValue(fedrationList.get(position).activation_status.toString()) == 3) {
            // holder.imgactivationstatus.visibility=View.VISIBLE

            if (validate!!.returnIntegerValue(fedrationList.get(position).status.toString()) == 1) {
                holder.tv_count.setBackgroundResource(R.drawable.item_countreject)
            }


        } else {
            holder.tv_count.setBackgroundResource(R.drawable.item_countpending)
        }

        if (validate!!.returnIntegerValue(fedrationList.get(position).status.toString()) == 0) {
            holder.tv_status.setBackgroundResource(R.color.red)
        } else if (validate!!.returnIntegerValue(fedrationList.get(position).is_edited.toString()) == 1||validate!!.returnIntegerValue(fedrationList.get(position).is_edited.toString()) == -1) {
            holder.tv_status.setBackgroundResource(R.color.colorPrimary)
        } else if (validate!!.returnIntegerValue(fedrationList.get(position).is_edited.toString()) == 0){
            holder.tv_status.setBackgroundResource(R.color.khakigreen1)
        }
        if (!name.isNullOrEmpty()) {
            firstword = name.substring(0, 1).toUpperCase()
        }

        holder.tv_firstword.text = firstword

        holder.tv_nam.setOnClickListener {
            validate!!.SaveSharepreferenceString(AppSP.FedrationGUID,fedrationList.get(position).guid)
            validate!!.SaveSharepreferenceLong(AppSP.FedrationCode,validate!!.returnLongValue(fedrationList.get(position).federation_id.toString()))
            validate!!.SaveSharepreferenceString(AppSP.FedrationName,validate!!.returnStringValue(fedrationList.get(position).federation_name))
            validate!!.SaveSharepreferenceLong(AppSP.Fedration_id,validate!!.returnLongValue(fedrationList.get(position).federation_id.toString()))
            validate!!.SaveSharepreferenceLong(AppSP.Formation_dt,validate!!.returnLongValue(fedrationList.get(position).federation_formation_date.toString()))
            validate!!.SaveSharepreferenceInt(AppSP.LockRecord,islock)
            validate!!.SaveSharepreferenceInt(AppSP.mappingRefreshed, 0)
            validate!!.SaveSharepreferenceInt(AppSP.isEdited,validate!!.returnIntegerValue(fedrationList.get(position).is_edited.toString()))

            if (validate!!.returnIntegerValue(fedrationList.get(position).status.toString()) == 0) {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "download_vo_data",
                        R.string.download_vo_data
                    ),
                    context
                )
            } else {
                if(cboType == 1 && ((count>0 && mappedshgcount>0) || count==0)) {

                    val i = Intent(context, VoBasicDetailActivity::class.java)
                    i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                    context.startActivity(i)
                }else if(cboType == 2 && ((count>0 && mapped_vo_count>0) || count==0)) {
                    val i = Intent(context, VoBasicDetailActivity::class.java)
                    i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                    context.startActivity(i)
                }else {
                    if(cboType ==1) {
                        validate!!.CustomAlertVO(
                            LabelSet.getText(
                                "download_mapping_data",
                                R.string.download_mapping_data
                            ),
                            context
                        )
                    }else if(cboType == 2) {
                        validate!!.CustomAlertVO(
                            LabelSet.getText(
                                "download_mapping_VOdata",
                                R.string.download_mapping_VOdata
                            ),
                            context
                        )
                    }
                }
            }
        }
        holder.imgactivationstatus.setOnClickListener {

            validate!!.SaveSharepreferenceString(AppSP.FedrationGUID, fedrationList.get(position).guid)
            validate!!.SaveSharepreferenceString(AppSP.FedrationName, validate!!.returnStringValue(fedrationList.get(position).federation_name))

            val i = Intent(context, VORemarksListActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            context.startActivity(i)
        }
        holder.optionBtn.setOnClickListener {
            validate!!.SaveSharepreferenceString(AppSP.FedrationGUID,fedrationList.get(position).guid)
            validate!!.SaveSharepreferenceLong(AppSP.FedrationCode,validate!!.returnLongValue(fedrationList.get(position).federation_id.toString()))
            validate!!.SaveSharepreferenceString(AppSP.FedrationName,validate!!.returnStringValue(fedrationList.get(position).federation_name))
            validate!!.SaveSharepreferenceLong(AppSP.Fedration_id,validate!!.returnLongValue(fedrationList.get(position).federation_id.toString()))
            validate!!.SaveSharepreferenceLong(AppSP.Formation_dt,validate!!.returnLongValue(fedrationList.get(position).federation_formation_date.toString()))
            validate!!.SaveSharepreferenceInt(AppSP.LockRecord,islock)
            validate!!.SaveSharepreferenceInt(AppSP.isEdited,validate!!.returnIntegerValue(fedrationList.get(position).is_edited.toString()))
            val popupwindow: PopupWindow = popupDisplay(fedrationList.get(position).guid,
                validate!!.returnStringValue(fedrationList.get(position).federation_name),
                validate!!.returnLongValue(fedrationList.get(position).federation_code.toString()),
                validate!!.returnLongValue(fedrationList.get(position).federation_id.toString()),
                validate!!.returnIntegerValue(fedrationList.get(position).status.toString()),
                validate!!.returnIntegerValue(fedrationList.get(position).activation_status.toString()),
                islock,
                validate!!.returnLongValue(fedrationList.get(position).federation_formation_date.toString()),
                validate!!.returnLongValue(fedrationList.get(position).last_uploaded_date.toString()),
                count,mappedshgcount,mapped_vo_count
            )
            val values = IntArray(2)
            holder.layout.getLocationInWindow(values)
            val positionOfIcon = values[1]

            //Get the height of 2/3rd of the height of the screen
            //Get the height of 2/3rd of the height of the screen
            val displayMetrics: DisplayMetrics = context.getResources().displayMetrics
            val height: Int = displayMetrics.heightPixels * 2 / 3

            //If the position of menu icon is in the bottom 2/3rd part of the screen then we provide menu height as offset  but in negative as we want to open our menu to the top
            //If the position of menu icon is in the bottom 2/3rd part of the screen then we provide menu height as offset  but in negative as we want to open our menu to the top
            if (positionOfIcon > height) {
                popupwindow.showAsDropDown(holder.layout, -20, -200)
            } else {
                popupwindow.showAsDropDown(holder.layout, -20, 0)
            }
        }

    }

    override fun getItemCount(): Int {
        return fedrationList!!.size
    }

    fun popupDisplay(guid: String,
                     federationName: String,
                     federationCode: Long,
                     federationid: Long,
                     status: Int,
                     activation_status: Int,
                     lock:Int,
                     formationdate: Long,
                     last_uploaded_date: Long,
                     count:Int,mappedshgcount:Int,mapped_vo_count:Int
    ): PopupWindow {
        val popupWindow = PopupWindow(context)
        // inflate your layout or dynamically add view
        val inflater = context.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.shg_item_dialog, null, false)
        popupWindow.isFocusable = true
        popupWindow.width = WindowManager.LayoutParams.WRAP_CONTENT
        popupWindow.height = WindowManager.LayoutParams.WRAP_CONTENT
        popupWindow.setBackgroundDrawable(ColorDrawable(context.resources.getColor(R.color.white)))
        if(validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType)==2){
            view.tvShg.text = LabelSet.getText(
                "clf_profile",
                R.string.clf_profile
            )
        }else {
            view.tvShg.text = LabelSet.getText(
                "fedration_profile",
                R.string.fedration_profile
            )
        }
        view.tvShgsummary.text = LabelSet.getText(
            "summary",
            R.string.summary
        )
        view.tv_meeting.text = LabelSet.getText(
            "meeting",
            R.string.meeting
        )
        view.tv_sync.text = LabelSet.getText(
            "download_mapped_data",
            R.string.download_mapped_data
        )
        view.tv_clear.text = LabelSet.getText(
            "delete_record",
            R.string.delete_record
        )
        view.tbl_shgprofile.setOnClickListener {
            //shg
            view.tbl_shgprofile.background = context.resources.getDrawable(R.drawable.dialog_selected_bg)
            view.iVshg.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary1))
            view.tvShg.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary1))
            //meeting
            view.tbl_meeting.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.ivMeetingpopup.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_meeting.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            //sync
            view.tbl_sync.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.iVsync.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_sync.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            //clear
            view.tbl_clear.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.ivClear.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_clear.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            if (status == 0) {
                validate!!.CustomAlert(
                    LabelSet.getText(
                        "download_vo_data",
                        R.string.download_vo_data
                    ),
                    context as VOListActivity
                )
            } else {
                if(cboType == 1 && ((count>0 && mappedshgcount>0) || count==0)) {

                    val i = Intent(context, VoBasicDetailActivity::class.java)
                    i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                    context.startActivity(i)
                }else if(cboType == 2 && ((count>0 && mapped_vo_count>0) || count==0)) {
                    val i = Intent(context, VoBasicDetailActivity::class.java)
                    i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                    context.startActivity(i)
                }else {
                    if(cboType ==1) {
                        validate!!.CustomAlert(
                            LabelSet.getText(
                                "download_mapping_data",
                                R.string.download_mapping_data
                            ),
                            context as VOListActivity
                        )
                    }else if(cboType == 2) {
                        validate!!.CustomAlert(
                            LabelSet.getText(
                                "download_mapping_VOdata",
                                R.string.download_mapping_VOdata
                            ),
                            context as VOListActivity
                        )
                    }
                }
            }


        }

        view.tbl_meeting.setOnClickListener {
            //shg
            view.tbl_shgprofile.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.iVshg.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tvShg.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))
            //meeting
            view.tbl_meeting.background = context.resources.getDrawable(R.drawable.dialog_selected_bg)
            view.ivMeetingpopup.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary1))
            view.tv_meeting.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary1))

            //sync
            view.tbl_sync.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.iVsync.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_sync.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            //clear
            view.tbl_clear.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.ivClear.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_clear.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

        }

        view.tbl_sync.setOnClickListener {

            //shg
            view.tbl_shgprofile.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.iVshg.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tvShg.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))
            //meeting
            view.tbl_sync.background = context.resources.getDrawable(R.drawable.dialog_selected_bg)
            view.iVsync.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary1))
            view.tv_sync.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary1))

            //sync
            view.tbl_meeting.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.ivMeetingpopup.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_meeting.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            //clear
            view.tbl_clear.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.ivClear.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_clear.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            popupWindow.dismiss()

            if(validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType)==2) {
                (context as VOListActivity).importCLF_VO_Mapping(
                    validate!!.RetriveSharepreferenceString(
                        AppSP.FedrationGUID
                    )!!)

            }else if(validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType)==1){
                (context as VOListActivity).importVO_SHG_Mapping(
                    validate!!.RetriveSharepreferenceString(
                        AppSP.FedrationGUID
                    )!!
                )
            }
        }

        view.tbl_clear.setOnClickListener {
            //shg
            view.tbl_shgprofile.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.iVshg.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tvShg.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))
            //meeting
            view.tbl_clear.background = context.resources.getDrawable(R.drawable.dialog_selected_bg)
            view.ivClear.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary1))
            view.tv_clear.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary1))

            //sync
            view.tbl_meeting.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.ivMeetingpopup.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_meeting.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            //clear
            view.tbl_sync.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.iVsync.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_sync.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            popupWindow.dismiss()
            validate!!.SaveSharepreferenceString(AppSP.FedrationGUID, guid)
            validate!!.SaveSharepreferenceString(AppSP.FedrationName, federationName)
            if(last_uploaded_date>0 && lock ==0){
                (context as VOListActivity).CustomAlert(guid,1)
            }else if(last_uploaded_date==0L && lock ==0){
                (context as VOListActivity).CustomAlert(guid,0)
            }else if(lock==1){
                validate!!.CustomAlert(LabelSet.getText(
                    "record_not_deleted",
                    R.string.record_not_deleted
                ),context as VOListActivity)
            }
        }

        popupWindow.contentView = view

        return popupWindow
    }

}