package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vo.VoAddressDetail
import com.microware.cdfi.activity.vo.VoAddressList
import com.microware.cdfi.entity.AddressEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.address_detailitem.view.*

class VoAddressAdapter (var context : Context, var addressEntity: List<AddressEntity>): RecyclerView.Adapter<VoAddressAdapter.ViewHolder>() {

    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.address_detailitem, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return addressEntity.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        holder.tvtype.setText(addressEntity!!.get(position).address_type)
        holder.tv_address.text = LabelSet.getText(
            "address",
            R.string.nameinbankpassbook
        )

        holder.tv_Pincode.text = LabelSet.getText(
            "pincode",
            R.string.pincode
        )
        holder.tvaddress.text = addressEntity.get(position).address_line1
        holder.tvpincode.text = addressEntity.get(position).postal_code.toString()
        var villageName = (context as VoAddressList).getVillageName(addressEntity.get(position).village_id)
        var panchayatName = (context as VoAddressList).getPanchayatName(addressEntity.get(position).panchayat_id)
        if(validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType)==2){
            holder.tvVillage.text = panchayatName
            holder.tv_village.text = LabelSet.getText(
                "grampanchayat",
                R.string.grampanchayat
            )
        }else {
            holder.tvVillage.text = panchayatName
            holder.tv_village.text = LabelSet.getText(
                "grampanchayat",
                R.string.grampanchayat
            )
        }


        holder.ivEdit.setOnClickListener {
            validate!!.SaveSharepreferenceString(AppSP.VoAddressGUID,addressEntity.get(position).address_guid)
            var inetnt = Intent(context, VoAddressDetail::class.java)
            context.startActivity(inetnt)
        }
        holder.ivDelete.setOnClickListener {
            if (validate!!.returnLongValue(addressEntity.get(position).last_uploaded_date.toString()) > 0) {
                (context as VoAddressList).CustomAlert(
                    addressEntity.get(position).address_guid,1)
            }else {
                (context as VoAddressList).CustomAlert(addressEntity.get(position).address_guid,0)
            }
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tvtype = view.tvtype
        var tvaddress = view.tvaddress
        var tv_address = view.tv_address
        var tv_village = view.tv_village
        var tvVillage = view.tvVillage
        var tv_Pincode = view.tv_Pincode
        var tvpincode = view.tvpincode
        var ivEdit = view.ivEdit
        var ivDelete = view.ivDelete
    }
}