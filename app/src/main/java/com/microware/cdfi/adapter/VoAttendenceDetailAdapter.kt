package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import kotlinx.android.synthetic.main.attendences_detail_item_vomeeting.view.*

class VoAttendenceDetailAdapter(var context: Context) :
    RecyclerView.Adapter<VoAttendenceDetailAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.attendences_detail_item_vomeeting, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return 7
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        setLabelText(holder)

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_memberName = view.tv_memberName

    }

    fun setLabelText(view: ViewHolder) {

    }
}