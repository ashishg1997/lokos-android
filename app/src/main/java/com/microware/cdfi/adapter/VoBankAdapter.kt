package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vo.VOBankDetailActivity
import com.microware.cdfi.activity.vo.VoBankListActivity
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.bankdetail_listitem.view.*
import java.io.File

class VoBankAdapter(var context : Context, var bankEntity: List<Cbo_bankEntity>): RecyclerView.Adapter<VoBankAdapter.ViewHolder>() {

    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.bankdetail_listitem, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return bankEntity.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var bankName = (context as VoBankListActivity).getBankName(bankEntity.get(position).bank_id)
        var Branchname = (context as VoBankListActivity).getBranchname(bankEntity.get(position).bank_branch_id!!)

        holder.tv_bankName.text = LabelSet.getText(
            "bank_name",
            R.string.bank_name
        )
        holder.tv_bankBranch.text = LabelSet.getText(
            "bank_branch",
            R.string.bank_branch
        )
        holder.tvIFSC.text = LabelSet.getText(
            "ifsc_code",
            R.string.ifsc_code
        )
        holder.tvAccountNo.text = LabelSet.getText(
            "account_no",
            R.string.account_no
        )
        holder.tvUploadPassbook.text = LabelSet.getText(
            "upload_passbook_first_page",
            R.string.upload_passbook_first_page
        )
        holder.tv_AccountOpenDate.text = LabelSet.getText(
            "opening_date",
            R.string.opening_date
        )

        holder.tvbankname.text = bankName

        holder.tvbranch.text = Branchname
        holder.tvifsc.text = bankEntity.get(position).ifsc_code
        holder.tvaccnum.text = bankEntity.get(position).account_no
        holder.tvopDate.text = validate!!.convertDatetime(bankEntity.get(position).account_opening_date!!)
        var defaultSatus = validate!!.returnIntegerValue(bankEntity.get(position).is_default.toString())
        if(defaultSatus == 1){
            holder.ivdefault.visibility = View.VISIBLE
        }else{
            holder.ivdefault.visibility = View.GONE
        }
        val mediaStorageDirectory = File(
            context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
            AppSP.IMAGE_DIRECTORY_NAME
        )

        var mediaFile1 = File(mediaStorageDirectory.path + File.separator + validate!!.returnStringValue(
            bankEntity.get(position).passbook_firstpage))
        Picasso.with(context).load(mediaFile1).into(holder.ImgFrntpage)
        holder.ivDelete.setOnClickListener {
            if (validate!!.returnLongValue(bankEntity.get(position).last_uploaded_date.toString()) > 0){
                (context as VoBankListActivity).CustomAlert(bankEntity.get(position).bank_guid,1)
            }else {
                (context as VoBankListActivity).CustomAlert(bankEntity.get(position).bank_guid,0)
            }
        }
        holder.ivEdit.setOnClickListener {
            validate!!.SaveSharepreferenceString(AppSP.VOBankGUID,bankEntity.get(position).bank_guid)
            var inetnt = Intent(context, VOBankDetailActivity::class.java)
            context.startActivity(inetnt)
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tv_bankName = view.tv_bankName
        var tvbankname = view.tvbankname
        var ivdefault = view.ivdefault
        var tv_bankBranch = view.tv_bankBranch
        var tvbranch = view.tvbranch
        var tvIFSC = view.tvIFSC
        var tvifsc = view.tvifsc
        var tvacctype = view.tvacctype
        var tvAccountNo = view.tvAccountNo
        var tvaccnum = view.tvaccnum
        var tv_AccountOpenDate = view.tv_AccountOpenDate
        var tvopDate = view.tvopDate
        var tvadefault = view.tvadefault
        var ivEdit = view.ivEdit
        var ivDelete = view.ivDelete
        var tvUploadPassbook = view.tvUploadPassbook
        var ImgFrntpage = view.ImgFrntpage
    }

}