package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vo.VOKycDetailActivity
import com.microware.cdfi.activity.vo.VoKycDetailList
import com.microware.cdfi.entity.Cbo_kycEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.kycdetail_listitem.view.*

class VoKycAdapter(var context : Context, var kycEntity: List<Cbo_kycEntity>): RecyclerView.Adapter<VoKycAdapter.ViewHolder>() {

    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.kycdetail_listitem, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return kycEntity.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.tvGroupCode.text = LabelSet.getText(
            "group_code",
            R.string.group_code
        )
        holder.tvDocument.text = LabelSet.getText(
            "kyc_id_type",
            R.string.kyc_id_type
        )
        holder.tvKYC_Num.text = LabelSet.getText(
            "kyc_no",
            R.string.kyc_no
        )

        holder.tvcode.text = validate!!.returnStringValue(kycEntity.get(position).cbo_id.toString())
      //  holder.tvkycid.setText(validate!!.returnStringValue(kycEntity.get(position).kyc_type.toString()))
        holder.tvkycnum.text = validate!!.returnStringValue(kycEntity.get(position).kyc_number)
        var Doctype = (context as VoKycDetailList).getLookupValue(validate!!.returnIntegerValue(kycEntity.get(position).kyc_type.toString()))
        holder.tvkycid.text = Doctype
        holder.ivEdit.setOnClickListener {
            validate!!.SaveSharepreferenceString(AppSP.VokycGUID,kycEntity.get(position).kyc_guid)
            var intent = Intent(context, VOKycDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            context.startActivity(intent)
        }

        holder.ivDelete.setOnClickListener {
            if(validate!!.returnLongValue(kycEntity.get(position).last_uploaded_date.toString())>0) {
                (context as VoKycDetailList).CustomAlert(kycEntity.get(position).kyc_guid,1)
            }else {
                (context as VoKycDetailList).CustomAlert(kycEntity.get(position).kyc_guid,0)
            }
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tvGroupCode = view.tvGroupCode
        var tvcode = view.tvcode
        var tvDocument = view.tvDocument
        var tvkycid = view.tvkycid
        var tvKYC_Num = view.tvKYC_Num
        var tvkycnum = view.tvkycnum
        var tvvalidfrom = view.tvvalidfrom
        var tvvalidtill = view.tvvalidtill
        var ivEdit = view.ivEdit
        var ivDelete = view.ivDelete
    }

}