package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.PopupWindow
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.*
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.vomeeting_list_item.view.*


class VoMeetingListAdapter(val context: Context) :
    RecyclerView.Adapter<VoMeetingListAdapter.ViewHolder>() {
    var validate: Validate? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.vomeeting_list_item, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return 5
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {



        holder.optionBtn.setOnClickListener {
            var popupwindow: PopupWindow = popupDisplay()
            val values = IntArray(2)
            holder.layout.getLocationInWindow(values)
            val positionOfIcon = values[1]

            //Get the height of 2/3rd of the height of the screen
            //Get the height of 2/3rd of the height of the screen
            val displayMetrics: DisplayMetrics = context.resources.displayMetrics
            val height: Int = displayMetrics.heightPixels * 2 / 3

            //If the position of menu icon is in the bottom 2/3rd part of the screen then we provide menu height as offset  but in negative as we want to open our menu to the top
            //If the position of menu icon is in the bottom 2/3rd part of the screen then we provide menu height as offset  but in negative as we want to open our menu to the top
            if (positionOfIcon > height) {
                popupwindow.showAsDropDown(holder.layout, -20, -200)
            } else {
                popupwindow.showAsDropDown(holder.layout, -20, 0)
            }


        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tv_status = view.tv_status
        var cardView = view.cardView
        var tv_firstword = view.tv_firstword
        var tv_code = view.tv_code
        var tv_nam = view.tv_nam
        var tv_date = view.tv_date
        var tv_count = view.tv_count
        var optionBtn = view.optionBtn
        var layout = view.layout
        var tblFirst = view.tblFirst
    }

    fun popupDisplay(): PopupWindow {
        val popupWindow = PopupWindow(context)
        // inflate your layout or dynamically add view
        val inflater =
            context.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.shg_item_dialog, null, false)
        popupWindow.isFocusable = true
        popupWindow.width = WindowManager.LayoutParams.WRAP_CONTENT
        popupWindow.height = WindowManager.LayoutParams.WRAP_CONTENT
        popupWindow.setBackgroundDrawable(ColorDrawable(context.resources.getColor(R.color.white)))

        /*view.tvShg.setText(LabelSet.getText("shg_profile",R.string.shg_profile))
        view.tvShgsummary.setText(LabelSet.getText("generate_meeting",R.string.generate_meeting))
        view.tv_meeting.setText(LabelSet.getText("open_meeting",R.string.open_meeting))
        view.tv_sync.setText(LabelSet.getText("sync_data",R.string.sync_data))*/

        /*view.tbl_shgprofile.setOnClickListener {
            //shg
            view.tbl_shgprofile.background =
                context.resources.getDrawable(R.drawable.dialog_selected_bg)
            view.iVshg.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary1))
            view.tvShg.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary1))
            //meeting
            view.tbl_meeting.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.transperent
                )
            )
            view.ivMeetingpopup.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.dialog_text_clr
                )
            )
            view.tv_meeting.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))
            //Rejection Remarks
            view.tbl_Remarks.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.transperent
                )
            )
            view.iVRejection.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.dialog_text_clr
                )
            )
            view.tv_remarks.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))
            //sync
            view.tbl_sync.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.iVsync.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_sync.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            //clear
            view.tbl_clear.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.ivClear.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_clear.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

                val i = Intent(context, BasicDetailActivity::class.java)
                i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                context.startActivity(i)
        }

        view.tbl_meeting.setOnClickListener {
            //shg
            view.tbl_shgprofile.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.transperent
                )
            )
            view.iVshg.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tvShg.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))
            //meeting
            view.tbl_meeting.background =
                context.resources.getDrawable(R.drawable.dialog_selected_bg)
            view.ivMeetingpopup.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.colorPrimary
                )
            )
            view.tv_meeting.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary1))
            //Rejection Remarks
            view.tbl_Remarks.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.transperent
                )
            )
            view.iVRejection.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.dialog_text_clr
                )
            )
            view.tv_remarks.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))
            //sync
            view.tbl_sync.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.iVsync.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_sync.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            //clear
            view.tbl_clear.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.ivClear.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_clear.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))


            val i = Intent(context, MeetingMenuActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            context.startActivity(i)
        }

        view.tbl_Remarks.setOnClickListener {

            //Rejection Remarks

            view.tbl_Remarks.background =
                context.resources.getDrawable(R.drawable.dialog_selected_bg)
            view.iVRejection.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary1))
            view.tv_remarks.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary1))

            //shg
            view.tbl_shgprofile.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.transperent
                )
            )
            view.iVshg.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tvShg.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            //meeting
            view.tbl_meeting.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.transperent
                )
            )
            view.ivMeetingpopup.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.dialog_text_clr
                )
            )
            view.tv_meeting.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            //sync
            view.tbl_sync.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.iVsync.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_sync.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            //clear
            view.tbl_clear.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.ivClear.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_clear.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            val i = Intent(context, ShgRemarksListActivity::class.java)
            i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            context.startActivity(i)
        }

        view.tbl_clear.setOnClickListener {

            //clear
            view.tbl_clear.background =
                context.resources.getDrawable(R.drawable.dialog_selected_bg)
            view.ivClear.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary1))
            view.tv_clear.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary1))

            //shg
            view.tbl_shgprofile.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.transperent
                )
            )
            view.iVshg.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tvShg.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            //Rejection Remarks
            view.tbl_Remarks.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.transperent
                )
            )
            view.iVRejection.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_remarks.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            //meeting
            view.tbl_meeting.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.transperent
                )
            )
            view.ivMeetingpopup.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.dialog_text_clr
                )
            )
            view.tv_meeting.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            //sync
            view.tbl_sync.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.iVsync.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_sync.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            popupWindow.dismiss()
        }

        view.tbl_sync.setOnClickListener {

            //shg
            view.tbl_shgprofile.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.transperent
                )
            )
            view.iVshg.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tvShg.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))
            //meeting
            view.tbl_sync.background = context.resources.getDrawable(R.drawable.dialog_selected_bg)
            view.iVsync.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary1))
            view.tv_sync.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary1))
            //Rejection Remarks
            view.tbl_Remarks.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.transperent
                )
            )
            view.iVRejection.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.dialog_text_clr
                )
            )
            view.tv_remarks.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))
            //sync
            view.tbl_meeting.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.transperent
                )
            )
            view.ivMeetingpopup.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.dialog_text_clr
                )
            )
            view.tv_meeting.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            //clear
            view.tbl_clear.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.ivClear.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_clear.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))
            popupWindow.dismiss()
//            (context as ShgListActivity).getshgdata(shgid)
        }
        view.tbl_shgsummary.setOnClickListener {

            //shg
            view.tbl_shgprofile.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.transperent
                )
            )
            view.iVshg.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tvShg.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))
            //Rejection Remarks
            view.tbl_Remarks.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.transperent
                )
            )
            view.iVRejection.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.dialog_text_clr
                )
            )
            view.tv_remarks.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))
            //Summary
            view.tbl_shgsummary.background =
                context.resources.getDrawable(R.drawable.dialog_selected_bg)
            view.iVshgsummary.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary1))
            view.tvShgsummary.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary1))
            //sync
            view.tbl_sync.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.iVsync.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_sync.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            //sync
            view.tbl_meeting.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.transperent
                )
            )
            view.ivMeetingpopup.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.dialog_text_clr
                )
            )
            view.tv_meeting.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))

            //clear
            view.tbl_clear.setBackgroundColor(ContextCompat.getColor(context, R.color.transperent))
            view.ivClear.setColorFilter(ContextCompat.getColor(context, R.color.dialog_text_clr))
            view.tv_clear.setTextColor(ContextCompat.getColor(context, R.color.dialog_text_clr))
            popupWindow.dismiss()
                val i = Intent(context, GenerateMeetingActivity::class.java)
                i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                context.startActivity(i)
        }*/

        popupWindow.contentView = view

        return popupWindow
    }
}