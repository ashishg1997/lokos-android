package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vo.VoPhoneDetail
import com.microware.cdfi.activity.vo.VoPhoneDetailListActivity
import com.microware.cdfi.entity.Cbo_phoneEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.phone_listitem.view.*

class VoPhoneAdapter(var context : Context, var phoneEntity: List<Cbo_phoneEntity>): RecyclerView.Adapter<VoPhoneAdapter.ViewHolder>() {

    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.phone_listitem, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return phoneEntity.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var member_Name = (context as VoPhoneDetailListActivity).returnMemberName(validate!!.returnStringValue(
            phoneEntity.get(position).member_guid))
        holder.tvBelongs.text = member_Name
        holder.tvPhone.text = validate!!.returnStringValue(phoneEntity.get(position).mobile_no)
        holder.tvValidform.text = validate!!.convertDatetime(validate!!.returnLongValue(phoneEntity.get(position).valid_from.toString()))
        holder.tvValidtill.text = validate!!.convertDatetime(validate!!.returnLongValue(phoneEntity.get(position).valid_till.toString()))

        holder.ivEdit.setOnClickListener {
            validate!!.SaveSharepreferenceString(AppSP.VoPhoneGUID,phoneEntity.get(position).phone_guid)
            var inetnt = Intent(context, VoPhoneDetail::class.java)
            context.startActivity(inetnt)
        }
        holder.ivDelete.setOnClickListener {
            if(validate!!.returnLongValue(phoneEntity.get(position).last_uploaded_date.toString())>0) {
                (context as VoPhoneDetailListActivity).CustomAlert(phoneEntity.get(position).phone_guid,1)
            }else {
                (context as VoPhoneDetailListActivity).CustomAlert(phoneEntity.get(position).phone_guid,0)
            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tvPhone = view.tvPhone
        var tvBelongs = view.tvBelongs
        var tvstatus = view.tvstatus
        var tvValidform = view.tvValidform
        var tvValidtill = view.tvValidtill
        var tvDefault = view.tvDefault
        var ivEdit = view.ivEdit
        var ivDelete = view.ivDelete
    }


}