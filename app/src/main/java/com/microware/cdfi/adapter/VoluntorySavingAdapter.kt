package com.microware.cdfi.adapter

import android.content.Context
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.CompulsorySavingActivity
import com.microware.cdfi.activity.meeting.VoluntorySavingActivity
import com.microware.cdfi.entity.DtmtgDetEntity
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.voluntary_saving_item.view.*

class VoluntorySavingAdapter(var context: Context,
                             var list: List<DtmtgDetEntity>) :
    RecyclerView.Adapter<VoluntorySavingAdapter.ViewHolder>() {
    var validate:Validate?=null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate=Validate(context)
        var view = LayoutInflater.from(context).inflate(R.layout.voluntary_saving_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_memberName.text = list.get(position).member_name.toString()
        holder.tv_srno.text = (position + 1).toString()

        holder.et_SavVolOb.setText(list.get(position).sav_vol_ob.toString())
        holder.et_GroupMCode.setText(list.get(position).group_m_code.toString())

        if(list.get(position).sav_vol!! > 0){
            (context as VoluntorySavingActivity).getTotalValue(list.get(position).sav_vol!!,1)
            holder.tv_count.setText(list.get(position).sav_vol.toString())
        }else{
            holder.tv_count.setText("0")
        }
        var memberAdjustment = (validate!!.returnIntegerValue((context as VoluntorySavingActivity).getAdjustmentSaving(list.get(position).mtg_no,list.get(position).mem_id,69).toString()))
        if(validate!!.returnIntegerValue(list.get(position).attendance.toString())==2 && validate!!.RetriveSharepreferenceInt(
                MeetingSP.MeetingType)==0){
            holder.tv_memberName.setTextColor(context.resources.getColor(R.color.red))
        }else{
            holder.tv_memberName.setTextColor(context.resources.getColor(R.color.black))
        }

        var saveVol=validate!!.returnIntegerValue(list.get(position).sav_vol_ob.toString())+validate!!.returnIntegerValue(list.get(position).sav_vol.toString())
        if(saveVol > 0){
            (context as VoluntorySavingActivity).getTotalValue(list.get(position).sav_vol_cb!!,2)
            holder.tv_cum2.text = (memberAdjustment+saveVol).toString()
        }else{
            holder.tv_cum2.text = "0"
        }

        setLabelText(holder)

        holder.tv_count.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                var value=validate!!.returnIntegerValue(holder.tv_count.text.toString())
                var sav_vol_ob=validate!!.returnIntegerValue(list.get(position).sav_vol_ob.toString())
                holder.tv_cum2.text = (value+sav_vol_ob+memberAdjustment).toString()
                (context as VoluntorySavingActivity).getTotalValue()
            }

        })

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_srno = view.tv_srno
        var tv_memberName = view.tv_memberName
        var tv_count = view.tv_count
        var tv_cum2 = view.tv_cum2
        var et_GroupMCode = view.et_GroupMCode
        var et_SavVolOb = view.et_SavVolOb
    }

    fun setLabelText(view: ViewHolder) {

    }
}