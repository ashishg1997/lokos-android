package com.microware.cdfi.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.VoucherActivity
import com.microware.cdfi.entity.ShgFinancialTxnVouchersEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.MstCOAViewmodel
import kotlinx.android.synthetic.main.voucher_list_layout.view.*

class VoucherAdapter(
    var context: VoucherActivity,
    var voucherEntity: List<ShgFinancialTxnVouchersEntity>,
    val mstcoaViewModel: MstCOAViewmodel?
) : RecyclerView.Adapter<VoucherAdapter.ViewHolder>() {

    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.voucher_list_layout, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return voucherEntity.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvName.text = mstcoaViewModel!!.getCOADesValue(
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),
            voucherEntity.get(position).auid
        )
        holder.tv_bank_s.text = voucherEntity.get(position).bank_code

        holder.ll_main.setOnClickListener {
            context.OpenVoucherDialog(voucherEntity.get(position).voucher_date,
                holder.tvName.text.toString(),voucherEntity.get(position).amount,voucherEntity.get(position).cbo_id,voucherEntity.get(position).voucher_no)

        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tvName = view.tvName
        var tv_bank_s = view.tv_bank_s
        var ll_main = view.ll_main

    }


}