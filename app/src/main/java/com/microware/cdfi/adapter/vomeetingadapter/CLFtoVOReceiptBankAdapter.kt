package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.CLFtoVOReceipts
import com.microware.cdfi.api.vomodel.VoChangePaymentBankDataModel
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import com.microware.cdfi.viewModel.voviewmodel.VoFinTxnDetGrpViewModel
import kotlinx.android.synthetic.main.clfto_vo_receipts_banklist_item.view.*

class CLFtoVOReceiptBankAdapter(
    val context: Context,
    val listData: List<VoChangePaymentBankDataModel>?,
    val voFinTxnDetGrpViewModel: VoFinTxnDetGrpViewModel?

) :
    RecyclerView.Adapter<CLFtoVOReceiptBankAdapter.ViewHolder>() {

    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        var view = LayoutInflater.from(context)
            .inflate(R.layout.clfto_vo_receipts_banklist_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listData!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        setLabel(holder)
        var lastfour = ""
        var name = ""
        if (validate!!.returnIntegerValue(listData!![position].modePayment.toString()) == 2) {
            lastfour =
                listData[position].account_no!!.substring(listData[position].account_no!!.length - 4)

            name = (context as CLFtoVOReceipts).getBankName(
                validate!!.returnIntegerValue(
                    listData.get(position).bank_id.toString()
                )
            )!!
        } else {
            lastfour = ""
            name = "Cash"
        }

        if (listData.size - 1 == position) {
            holder.tblLayout.setCardBackgroundColor(context.getColor(R.color.green))
            holder.tbl_totamount.setBackgroundColor(context.getColor(R.color.green))
            holder.tv_bank_name.text = name
        } else {
            when (position) {
                0 -> {
                    holder.tblLayout.setCardBackgroundColor(context.getColor(R.color.color1))
                    holder.tbl_totamount.setBackgroundColor(context.getColor(R.color.color1))
                }
                1 -> {
                    holder.tblLayout.setCardBackgroundColor(context.getColor(R.color.color2))
                    holder.tbl_totamount.setBackgroundColor(context.getColor(R.color.color2))
                }
                2 -> {
                    holder.tblLayout.setCardBackgroundColor(context.getColor(R.color.color3))
                    holder.tbl_totamount.setBackgroundColor(context.getColor(R.color.color3))
                }
                3 -> {
                    holder.tblLayout.setCardBackgroundColor(context.getColor(R.color.color4))
                    holder.tbl_totamount.setBackgroundColor(context.getColor(R.color.color4))
                }
                4 -> {
                    holder.tblLayout.setCardBackgroundColor(context.getColor(R.color.color5))
                    holder.tbl_totamount.setBackgroundColor(context.getColor(R.color.color5))
                }
                5 -> {
                    holder.tblLayout.setCardBackgroundColor(context.getColor(R.color.color6))
                    holder.tbl_totamount.setBackgroundColor(context.getColor(R.color.color6))
                }
                6 -> {
                    holder.tblLayout.setCardBackgroundColor(context.getColor(R.color.color7))
                    holder.tbl_totamount.setBackgroundColor(context.getColor(R.color.color7))
                }
                else -> {
                    holder.tblLayout.setCardBackgroundColor(context.getColor(R.color.colordefault))
                    holder.tbl_totamount.setBackgroundColor(context.getColor(R.color.colordefault))
                }
            }
            holder.tv_bank_name.text = name + " (XXXX XXXX XXXX $lastfour)"
        }


        holder.btn_dropup.setOnClickListener {
            holder.layoutExpend.visibility = View.GONE
            holder.btn_dropup.visibility = View.GONE
            holder.btn_dropdown.visibility = View.VISIBLE
        }

        holder.btn_dropdown.setOnClickListener {
            holder.layoutExpend.visibility = View.VISIBLE
            holder.btn_dropup.visibility = View.VISIBLE
            holder.btn_dropdown.visibility = View.GONE
        }

        holder.tv_clickReceive.setOnClickListener {
            if (listData.size - 1 == position) {
                (context as CLFtoVOReceipts).updatefindetailmemcash()
            } else {
                (context as CLFtoVOReceipts).customReceiveDialog(
                    listData[position].ifsc_code!!.dropLast(7)+listData[position].account_no!!

                )
            }
        }

        if (listData.size - 1 == position) {
            when {
                validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 2 -> {
                    fillRecyclerView(
                        holder,
                        "", listOf("RG","RL","RO")
                    )
                }
                validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 3 -> {
                    fillRecyclerView(
                        holder,
                        "", listOf("RG","RL","RO")
                    )
                }
                validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 4 -> {
                    fillRecyclerView(
                        holder,
                        "", listOf("RG","RL","RO")
                    )
                }
                validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 6 -> {
                    fillRecyclerView(
                        holder,
                        "", listOf("PG","PL","PO")
                    )
                }
                validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 7 -> {
                    fillRecyclerView(
                        holder,
                        "", listOf("PG","PL","PO")
                    )
                }
            }

        } else {
            when {
                validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 2 -> {
                    fillRecyclerView(
                        holder,
                        listData[position].ifsc_code!!.dropLast(7) + listData[position].account_no!!, listOf("RG","RL","RO")
                    )
                }
                validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 3 -> {
                    fillRecyclerView(
                        holder,
                        listData[position].ifsc_code!!.dropLast(7) + listData[position].account_no!!, listOf("RG","RL","RO")
                    )
                }
                validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 4 -> {
                    fillRecyclerView(
                        holder,
                        listData[position].ifsc_code!!.dropLast(7) + listData[position].account_no!!, listOf("RG","RL","RO")
                    )
                }
                validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 6 -> {
                    fillRecyclerView(
                        holder,
                        listData[position].ifsc_code!!.dropLast(7) + listData[position].account_no!!, listOf("PG","PL","PO")
                    )
                }
                validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 7 -> {
                    fillRecyclerView(
                        holder,
                        listData[position].ifsc_code!!.dropLast(7) + listData[position].account_no!!, listOf("PG","PL","PO")
                    )
                }
            }

        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var cardView = view.cardView
        var tv_bank_name = view.tv_bank_name
        var btn_dropup = view.btn_dropup
        var btn_dropdown = view.btn_dropdown
        var layoutExpend = view.layoutExpend
        var rvBankListItem = view.rvBankListItem
        var tv_totalamount = view.tv_totalamount
        var tv_clickReceive = view.tv_clickReceive
        var tv_amountrecieved = view.tv_amountrecieved
        var tblLayout = view.tblLayout
        var tbl_totamount = view.tbl_totamount
        var tv_total = view.tv_total
        var tv_totaltext = view.tv_totaltext

    }

    private fun fillRecyclerView(
        holder: ViewHolder,
        accountNo: String,
        type: List<String>

        ) {
        val listData = voFinTxnDetGrpViewModel!!.getVoFinTxnDetgrpdatabyBank(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            accountNo,
            validate!!.RetriveSharepreferenceInt(VoSpData.voOrgType),type
        )
        if (!listData.isNullOrEmpty()) {
            holder.rvBankListItem.layoutManager = LinearLayoutManager(context)
            holder.rvBankListItem.adapter = CLFtoVoReceiptBankItemAdapter(
                context,
                listData,
                holder.tv_totalamount,
                holder.tv_amountrecieved
            )
        }
    }

    private fun setLabel(view:ViewHolder){
        view.tv_totaltext.text = LabelSet.getText("total_amount", R.string.total_amount)
        view.tv_clickReceive.text = LabelSet.getText("click_to_receive", R.string.click_to_receive)
        view.tv_total.text = LabelSet.getText("total_amount_received", R.string.total_amount_received)
    }


}