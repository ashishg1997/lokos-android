package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.CLFtoVOReceipts
import com.microware.cdfi.entity.MstCOAEntity
import com.microware.cdfi.entity.voentity.VoFinTxnDetGrpEntity
import com.microware.cdfi.entity.voentity.VoFinTxnDetMemEntity
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.clfto_vo_receipts_bank_item.view.*

class CLFtoVoReceiptBankItemAdapter(
    var context: Context,
    val listData: List<VoFinTxnDetGrpEntity>?,
    var tvTotalamount: TextView,
    var tvamount: TextView
) :
    RecyclerView.Adapter<CLFtoVoReceiptBankItemAdapter.ViewHolder>() {

    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        var view = LayoutInflater.from(context).inflate(R.layout.clfto_vo_receipts_bank_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listData!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_fund.text =  (context as CLFtoVOReceipts).getname(listData!![position].auid)
        holder.tv_amount.text = validate!!.returnStringValue(listData[position].amount.toString())
        tvTotalamount.text =
            (validate!!.returnIntegerValue(tvTotalamount.text.toString()) + validate!!.returnIntegerValue(
                listData[position].amount.toString()
            )).toString()
        tvamount.text =
            (validate!!.returnIntegerValue(tvamount.text.toString()) + validate!!.returnIntegerValue(
                listData[position].amount.toString()
            )).toString()

        if (validate!!.returnIntegerValue(listData[position].isEdited.toString())==0){
            holder.ivchk.visibility=View.VISIBLE
        }else{
            holder.ivchk.visibility=View.INVISIBLE
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_fund = view.tv_fund
        var tv_amount = view.tv_amount
        var ivchk = view.ivchk
    }


}