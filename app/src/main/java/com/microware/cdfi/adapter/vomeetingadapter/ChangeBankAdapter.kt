package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupWindow
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import com.microware.cdfi.viewModel.MasterBankViewmodel
import kotlinx.android.synthetic.main.change_payment_item.view.*

class ChangeBankAdapter(
    var context: Context,
    val listData: List<Cbo_bankEntity>?,
    var masterBankViewmodel: MasterBankViewmodel,
    var tvbankcode: TextView,
    var popupWindow: PopupWindow,
    var tvamount: TextView
) :
    RecyclerView.Adapter<ChangeBankAdapter.ViewHolder>() {

    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        var view = LayoutInflater.from(context)
            .inflate(R.layout.change_payment_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listData!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var colorRes = 0

            when (position ) {
                0 -> colorRes = R.color.color1
                1 -> colorRes = R.color.color2
                2 -> colorRes = R.color.color3
                3 -> colorRes = R.color.color4
                4 -> colorRes = R.color.color5
                5 -> colorRes = R.color.color6
                6 -> colorRes = R.color.color7
                else -> colorRes = R.color.colordefault

        }

        holder.img_bank.setColorFilter(
            ContextCompat.getColor(context, colorRes),
            android.graphics.PorterDuff.Mode.SRC_IN
        )


            val name = getBankName(
                validate!!.returnIntegerValue(
                    listData!![position].bank_id.toString()
                )
            )
            val lastfour =
                listData[position].account_no.substring(listData[position].account_no.length - 4)
            holder.tv_banks.text = "$name(XXXX $lastfour)"


        holder.cardView.setOnClickListener {


            var colorRes = 0

                when (position ) {
                    0 -> colorRes = R.color.color1
                    1 -> colorRes = R.color.color2
                    2 -> colorRes = R.color.color3
                    3 -> colorRes = R.color.color4
                    4 -> colorRes = R.color.color5
                    5 -> colorRes = R.color.color6
                    6 -> colorRes = R.color.color7
                    else -> colorRes = R.color.colordefault
                }
                /*ivActualPayLocation.setColorFilter(
                    ContextCompat.getColor(context, colorRes),
                    android.graphics.PorterDuff.Mode.SRC_IN
                )*/
            tvbankcode.text = "$name(XXXX $lastfour)"



            popupWindow.dismiss()

        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var cardView = view.cardView
        var img_bank = view.img_bank
        var tv_banks = view.tv_banks

    }

    fun getBankName(bankID: Int?): String? {
        var value: String? = null
        value = masterBankViewmodel.getBankName(bankID!!)
        return value
    }


}