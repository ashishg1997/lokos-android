package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.OthertoVoReceipts
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.MstCOAViewmodel
import kotlinx.android.synthetic.main.change_payment_item.view.*

class ChangeOtherstoVoReceiptPaymentAdapter(
    var context: Context,
    val listData: List<Cbo_bankEntity>?,
    val mstCOAViewmodel: MstCOAViewmodel?
) :
    RecyclerView.Adapter<ChangeOtherstoVoReceiptPaymentAdapter.ViewHolder>() {

    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        var view = LayoutInflater.from(context)
            .inflate(R.layout.change_payment_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listData!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val name = (context as OthertoVoReceipts).getBankName(
            validate!!.returnIntegerValue(
                listData!!.get(position).bank_id.toString()
            )
        )
        holder.tv_banks.text = name

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var cardView = view.cardView
        var img_bank = view.img_bank
        var tv_banks = view.tv_banks

    }


}