package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.CutOffVoMemberCloseLoan
import com.microware.cdfi.entity.voentity.VoMtgDetEntity
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.cutoff_vo_memberclose_loan_item.view.*

class CutOffVoMemberCloseLoanAdapter(
    var context: Context,
    var list: List<VoMtgDetEntity>
) :
    RecyclerView.Adapter<CutOffVoMemberCloseLoanAdapter.ViewHolder>() {
    var validate: Validate?=null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate= Validate(context)
        var view = LayoutInflater.from(context).inflate(R.layout.cutoff_vo_memberclose_loan_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_memberName.text = list.get(position).childCboName.toString()
        holder.tv_srno.text = (position + 1).toString()

       // holder.et_LoanNo.setText(list.get(position).noOfLoans.toString())
        holder.et_memId.setText(list.get(position).memId.toString())

        var totalLoanAmount = (context as CutOffVoMemberCloseLoan).getTotalClosedLoanAmount(list.get(position).memId,list.get(position).mtgNo)
        var totalLoanCount = (context as CutOffVoMemberCloseLoan).getTotalClosedLoanCount(list.get(position).memId,list.get(position).mtgNo)

        holder.et_totalLoan.setText(totalLoanCount.toString())
        holder.et_totalAmount.setText(totalLoanAmount.toString())

        (context as CutOffVoMemberCloseLoan).getTotalValue(totalLoanCount,1)
        (context as CutOffVoMemberCloseLoan).getTotalValue(totalLoanAmount,2)


        holder.et_totalLoan.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                (context as CutOffVoMemberCloseLoan).getTotalLoanValue()
                if (validate!!.returnIntegerValue(s.toString()) == 0){
                    holder.et_totalAmount.setText("0")
                }
            }

        })

        holder.et_totalAmount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                (context as CutOffVoMemberCloseLoan).getTotalAmountValue()
            }

        })

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_srno = view.tv_srno
        var tv_memberName = view.tv_memberName
        var et_totalLoan = view.et_totalLoan
        var et_totalAmount = view.et_totalAmount
        var et_LoanNo = view.et_LoanNo
        var et_memId = view.et_memId

    }

}