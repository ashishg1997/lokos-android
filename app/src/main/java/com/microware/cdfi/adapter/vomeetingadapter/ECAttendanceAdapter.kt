package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.Ec_MemberAttendanceActivity
import com.microware.cdfi.api.vomodel.VoECMemberDataModel
import com.microware.cdfi.entity.voentity.VoMtgDetEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import com.microware.cdfi.viewModel.ExecutiveMemberViewmodel
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.VOShgMemberViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoMtgDetViewModel
import kotlinx.android.synthetic.main.ec_attendance.view.*

class ECAttendanceAdapter(
    var context: Context,
    val list: List<VoMtgDetEntity>,
    var executiveMemberViewmodel: ExecutiveMemberViewmodel,
    var lookupViewmodel: LookupViewmodel
) :
    RecyclerView.Adapter<ECAttendanceAdapter.ViewHolder>() {
    var validate: Validate? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        var view = LayoutInflater.from(context).inflate(R.layout.ec_attendance, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.tv_shgName.text = list[position].childCboName

        holder.lay_ec_attendance.setOnClickListener {
            holder.btn_dropdown.rotation = 180f

            if (holder.layoutExpend.visibility == View.VISIBLE) {
                holder.layoutExpend.visibility = View.GONE
                holder.btn_dropdown.rotation = 0f
            } else {
                holder.layoutExpend.visibility = View.VISIBLE
            }
        }

        fillRecyclerView(holder, list[position].memId)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_shgName = view.tv_shg_name
        var btn_dropup = view.btn_dropup
        var btn_dropdown = view.btn_dropdown
        var layoutExpend = view.layoutExpend
        var rvList = view.rvListMember
        var lay_ec_attendance = view.lay_ec_attendance

    }

    private fun fillRecyclerView(holder: ViewHolder, memId: Long) {

        val list = executiveMemberViewmodel.getAllExecutivedetaildata1(
            memId,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf(1, 2, 3, 4, 5)
        )
        if (list != null && list.size > 0) {
            val iGen = list.size
            val model = VoECMemberDataModel(
                list.get(0).cbo_id,
                list.get(0).cbo_guid,
                list.get(0).member_id,
                6,
                list.get(0).member_guid,
                1234567,
                "Other",
                list.get(0).ec_cbo_code,
                "",
                "",
                "",
                "",
                "",
                list.get(0).attendanceOther
            )
            list.add(iGen, model)
            holder.rvList.layoutManager = LinearLayoutManager(context)
            holder.rvList.adapter =
                ECMemberAttendanceAdapter(context, list, lookupViewmodel)
        }
    }

}