package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.api.vomodel.VoECMemberDataModel
import com.microware.cdfi.entity.Executive_memberEntity
import com.microware.cdfi.entity.voentity.VoMtgDetEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import com.microware.cdfi.viewModel.LookupViewmodel
import com.microware.cdfi.viewModel.VOShgMemberViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoMtgDetViewModel
import kotlinx.android.synthetic.main.ec_attendance_list_item.view.*

class ECMemberAttendanceAdapter(
    var context: Context,
    var list: List<VoECMemberDataModel>,
    var lookupViewmodel: LookupViewmodel
) :
    RecyclerView.Adapter<ECMemberAttendanceAdapter.ViewHolder>() {
    var validate: Validate? = null
    var other1 = 0
    var other2 = 0
    var other3 = 0
    var otherTotal = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        var view =
            LayoutInflater.from(context).inflate(R.layout.ec_attendance_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val designation = lookupViewmodel.getDestination(
            49,
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode),
            list[position].designation!!
        )

        holder.tv_member_Name.text = list[position].member_name

        holder.tv_member_Designation.text = "($designation)"

        holder.et_mem_id.setText(list[position].ec_cbo_code.toString())

        if (list[position].member_name.equals("Other", true)) {
            holder.btn_absent.visibility = View.GONE
            holder.btn_present.visibility = View.GONE
            holder.btn_abt_other_1.visibility = View.VISIBLE
            holder.btn_abt_other_2.visibility = View.VISIBLE
            holder.btn_abt_other_3.visibility = View.VISIBLE
        } else {
            holder.btn_absent.visibility = View.VISIBLE
            holder.btn_present.visibility = View.VISIBLE
            holder.btn_abt_other_1.visibility = View.GONE
            holder.btn_abt_other_2.visibility = View.GONE
            holder.btn_abt_other_3.visibility = View.GONE
        }

        holder.btn_present.setOnClickListener {
            holder.btn_absent.setBackgroundResource(R.drawable.absentlight)
            holder.btn_present.setBackgroundResource(R.drawable.present_count)
            holder.et_attendance.setText("1")
        }

        holder.btn_absent.setOnClickListener {
            holder.btn_absent.setBackgroundResource(R.drawable.absent_count)
            holder.btn_present.setBackgroundResource(R.drawable.presentlight)
            holder.et_attendance.setText("2")
        }

        holder.btn_pre_other_1.setOnClickListener {
            holder.btn_pre_other_1.visibility = View.GONE
            holder.btn_abt_other_1.visibility = View.VISIBLE
            other1 = 0
            otherAttendance(holder)
        }

        holder.btn_abt_other_1.setOnClickListener {
            holder.btn_abt_other_1.visibility = View.GONE
            holder.btn_pre_other_1.visibility = View.VISIBLE
            other1 = 1
            otherAttendance(holder)
        }

        holder.btn_pre_other_2.setOnClickListener {
            holder.btn_pre_other_2.visibility = View.GONE
            holder.btn_abt_other_2.visibility = View.VISIBLE
            other2 = 0
            otherAttendance(holder)
        }

        holder.btn_abt_other_2.setOnClickListener {
            holder.btn_abt_other_2.visibility = View.GONE
            holder.btn_pre_other_2.visibility = View.VISIBLE
            other2 = 1
            otherAttendance(holder)
        }

        holder.btn_pre_other_3.setOnClickListener {
            holder.btn_pre_other_3.visibility = View.GONE
            holder.btn_abt_other_3.visibility = View.VISIBLE
            other3 = 0
            otherAttendance(holder)
        }

        holder.btn_abt_other_3.setOnClickListener {
            holder.btn_abt_other_3.visibility = View.GONE
            holder.btn_pre_other_3.visibility = View.VISIBLE
            other3 = 1
            otherAttendance(holder)
        }

        show(holder, position)


    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_member_Name = view.tv_member_Name
        var tv_member_Designation = view.tv_member_Designation
        var btn_present = view.btn_present
        var btn_absent = view.btn_absent
        var et_attendance = view.et_attendance
        var et_attendance_other = view.et_attendance_other
        var et_mem_id = view.et_mem_id
        var tblpresent_absent = view.tblpresent_absent
        var btn_pre_other_1 = view.btn_pre_other_1
        var btn_pre_other_2 = view.btn_pre_other_2
        var btn_pre_other_3 = view.btn_pre_other_3
        var btn_abt_other_1 = view.btn_abt_other_1
        var btn_abt_other_2 = view.btn_abt_other_2
        var btn_abt_other_3 = view.btn_abt_other_3

    }

    private fun show(holder: ViewHolder, position: Int) {

        if (list[position].designation == 1) {
            if (validate!!.returnIntegerValue(list.get(position).ec1) == 1) {
                holder.btn_absent.setBackgroundResource(R.drawable.absentlight)
                holder.btn_present.setBackgroundResource(R.drawable.present_count)
            } else if (validate!!.returnIntegerValue(list.get(position).ec1) == 2){
                holder.btn_absent.setBackgroundResource(R.drawable.absent_count)
                holder.btn_present.setBackgroundResource(R.drawable.presentlight)
            } else{
                holder.btn_absent.setBackgroundResource(R.drawable.absentlight)
                holder.btn_present.setBackgroundResource(R.drawable.presentlight)
            }
        }

        if (list[position].designation == 2) {
            if (validate!!.returnIntegerValue(list.get(position).ec2) == 1) {
                holder.btn_absent.setBackgroundResource(R.drawable.absentlight)
                holder.btn_present.setBackgroundResource(R.drawable.present_count)
            } else if (validate!!.returnIntegerValue(list.get(position).ec2) == 2){
                holder.btn_absent.setBackgroundResource(R.drawable.absent_count)
                holder.btn_present.setBackgroundResource(R.drawable.presentlight)
            } else{
                holder.btn_absent.setBackgroundResource(R.drawable.absentlight)
                holder.btn_present.setBackgroundResource(R.drawable.presentlight)
            }
        }

        if (list[position].designation == 3) {
            if (validate!!.returnIntegerValue(list.get(position).ec3) == 1) {
                holder.btn_absent.setBackgroundResource(R.drawable.absentlight)
                holder.btn_present.setBackgroundResource(R.drawable.present_count)
            } else if (validate!!.returnIntegerValue(list.get(position).ec3) == 2){
                holder.btn_absent.setBackgroundResource(R.drawable.absent_count)
                holder.btn_present.setBackgroundResource(R.drawable.presentlight)
            } else{
                holder.btn_absent.setBackgroundResource(R.drawable.absentlight)
                holder.btn_present.setBackgroundResource(R.drawable.presentlight)
            }
        }

        if (list[position].designation == 4) {
            if (validate!!.returnIntegerValue(list.get(position).ec4) == 1) {
                holder.btn_absent.setBackgroundResource(R.drawable.absentlight)
                holder.btn_present.setBackgroundResource(R.drawable.present_count)
            } else if (validate!!.returnIntegerValue(list.get(position).ec4) == 2){
                holder.btn_absent.setBackgroundResource(R.drawable.absent_count)
                holder.btn_present.setBackgroundResource(R.drawable.presentlight)
            } else{
                holder.btn_absent.setBackgroundResource(R.drawable.absentlight)
                holder.btn_present.setBackgroundResource(R.drawable.presentlight)
            }
        }

        if (list[position].designation == 5) {
            if (validate!!.returnIntegerValue(list.get(position).ec5) == 1) {
                holder.btn_absent.setBackgroundResource(R.drawable.absentlight)
                holder.btn_present.setBackgroundResource(R.drawable.present_count)
            } else if (validate!!.returnIntegerValue(list.get(position).ec5) == 2){
                holder.btn_absent.setBackgroundResource(R.drawable.absent_count)
                holder.btn_present.setBackgroundResource(R.drawable.presentlight)
            } else{
                holder.btn_absent.setBackgroundResource(R.drawable.absentlight)
                holder.btn_present.setBackgroundResource(R.drawable.presentlight)
            }
        }

        if (list[position].designation == 6) {
            var attendaceOther = list.get(position).attendanceOther
            if (attendaceOther == 1) {
                holder.btn_pre_other_1.visibility = View.VISIBLE
                holder.btn_pre_other_2.visibility = View.GONE
                holder.btn_pre_other_3.visibility = View.GONE
                holder.btn_abt_other_1.visibility = View.GONE
                holder.btn_abt_other_2.visibility = View.VISIBLE
                holder.btn_abt_other_3.visibility = View.VISIBLE
                other1 = 1
                other2 = 0
                other3 = 0
            } else if (attendaceOther == 2) {
                holder.btn_pre_other_1.visibility = View.VISIBLE
                holder.btn_pre_other_2.visibility = View.VISIBLE
                holder.btn_pre_other_3.visibility = View.GONE
                holder.btn_abt_other_1.visibility = View.GONE
                holder.btn_abt_other_2.visibility = View.GONE
                holder.btn_abt_other_3.visibility = View.VISIBLE
                other1 = 1
                other2 = 1
                other3 = 0
            } else if (attendaceOther == 3) {
                holder.btn_pre_other_1.visibility = View.VISIBLE
                holder.btn_pre_other_2.visibility = View.VISIBLE
                holder.btn_pre_other_3.visibility = View.VISIBLE
                holder.btn_abt_other_1.visibility = View.GONE
                holder.btn_abt_other_2.visibility = View.GONE
                holder.btn_abt_other_3.visibility = View.GONE
                other1 = 1
                other2 = 1
                other3 = 1
            } else {
                holder.btn_pre_other_1.visibility = View.GONE
                holder.btn_pre_other_2.visibility = View.GONE
                holder.btn_pre_other_3.visibility = View.GONE
                holder.btn_abt_other_1.visibility = View.VISIBLE
                holder.btn_abt_other_2.visibility = View.VISIBLE
                holder.btn_abt_other_3.visibility = View.VISIBLE
            }
        }

    }

    private fun otherAttendance(holder: ViewHolder) {
        otherTotal = other1 + other2 + other3
        if (otherTotal == 1) {
            holder.et_attendance_other.setText("1")
        } else if (otherTotal == 2) {
            holder.et_attendance_other.setText("2")
        } else if (otherTotal == 3) {
            holder.et_attendance_other.setText("3")
        } else {
            holder.et_attendance_other.setText("0")
        }
    }

}