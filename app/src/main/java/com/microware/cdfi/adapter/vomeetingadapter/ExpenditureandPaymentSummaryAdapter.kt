package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.ExpenditureAndPayment
import com.microware.cdfi.activity.vomeeting.ExpenditureAndPaymentSummary
import com.microware.cdfi.entity.voentity.VoFinTxnDetMemEntity
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import kotlinx.android.synthetic.main.expenditure_and_payment_summary_item_row.view.*

class ExpenditureandPaymentSummaryAdapter(var context: Context,  var list: List<VoFinTxnDetMemEntity>) :
    RecyclerView.Adapter<ExpenditureandPaymentSummaryAdapter.ViewHolder>() {
    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        var view = LayoutInflater.from(context).inflate(R.layout.expenditure_and_payment_summary_item_row, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.amount.text = list[position].amount.toString()

        val paymentType = (context as ExpenditureAndPaymentSummary).getRecepitType(
            validate!!.returnIntegerValue(list[position].auid.toString())
        )

        if (!paymentType.isNullOrEmpty()) {
            holder.particulars.text = paymentType
        }

        (context as ExpenditureAndPaymentSummary).getTotalValue(validate!!.returnIntegerValue(list[position].amount.toString()))

        holder.card_view.setOnClickListener {

            validate!!.SaveSharepreferenceInt(VoSpData.voAuid, validate!!.returnIntegerValue(list[position].auid.toString()))

            val intent = Intent(context, ExpenditureAndPayment::class.java)
            context.startActivity(intent)
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var particulars = view.tv_particulars
        var amount = view.tv_amount
        var card_view = view.card_view


    }

}