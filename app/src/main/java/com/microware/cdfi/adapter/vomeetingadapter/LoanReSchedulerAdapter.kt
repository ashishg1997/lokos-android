package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import kotlinx.android.synthetic.main.loan_rescheduler_row_item.view.*

class LoanReSchedulerAdapter(var context: Context) :
    RecyclerView.Adapter<LoanReSchedulerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.loan_rescheduler_row_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return 5
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        fillAdapterData(holder)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var next_installment_date = view.next_installment_date
        var principal_repayment = view.principal_repayment
        var edit = view.edit
        var interest = view.interest
        var total_payment = view.total_payment
        var balance = view.balance
        var card_view= view.card_view


    }


    fun fillAdapterData(view: ViewHolder) {


        view.next_installment_date.text = "1/03/2021"
        view.principal_repayment.text = "500"
        view.interest.text = "120"
        view.total_payment.text = "620"
        view.balance.text = "11,500"


    }
}