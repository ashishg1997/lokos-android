package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.CLFtoVoLoanReceipts
import com.microware.cdfi.activity.vomeeting.CLFtoVoLoanReceiptsSummaryActivity
import com.microware.cdfi.activity.vomeeting.VoGroupPrincipalDemandActivity
import com.microware.cdfi.entity.voentity.VoGroupLoanEntity
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import kotlinx.android.synthetic.main.clf_vo_loan_summary_row_item.view.*

class LoanReceiptSummaryAdapter(var context: Context, var list: List<VoGroupLoanEntity>) :
    RecyclerView.Adapter<LoanReceiptSummaryAdapter.ViewHolder>() {
    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view =
            LayoutInflater.from(context)
                .inflate(R.layout.clf_vo_loan_summary_row_item, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        setLabelText(holder)
        holder.tv_loan.text = list[position].loanNo.toString()

        var fundName =
            (context as CLFtoVoLoanReceiptsSummaryActivity).getRecepitType(
                validate!!.RetriveSharepreferenceInt(
                    VoSpData.voAuid
                )
            )

        holder.tv_funds.text = fundName

        holder.tv_loan_payment.text =
            validate!!.returnIntegerValue(list[position].amount.toString()).toString()

        holder.tv_loan_payment.setOnClickListener(View.OnClickListener {
            validate!!.SaveSharepreferenceInt(
                VoSpData.voLoanno,
                validate!!.returnIntegerValue(list.get(position).loanNo.toString())
            )
            var intent = Intent(context, VoGroupPrincipalDemandActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            context.startActivity(intent)
        })

        holder.ivEdit.setOnClickListener(View.OnClickListener {
            validate!!.SaveSharepreferenceInt(
                VoSpData.voLoanno,
                validate!!.returnIntegerValue(list.get(position).loanNo.toString())
            )
            var intent = Intent(context, CLFtoVoLoanReceipts::class.java)
            context.startActivity(intent)
        })


    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_loan = view.tv_loan
        var tv_funds = view.tv_funds
        var tv_loan_payment = view.loan_payment
        var ivEdit = view.ivEdit

    }

    fun setLabelText(view: ViewHolder) {

    }
}