package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import kotlinx.android.synthetic.main.loan_scheduler_row_item.view.*

class LoanSchedulerAdapter(var context: Context) :
    RecyclerView.Adapter<LoanSchedulerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.loan_scheduler_row_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return 5
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var installment_date = view.tv_installment_date
        var principal_repayment = view.tv_principal_repayment
        var edit = view.edit
        var interest = view.tv_interest
        var total_payment = view.tv_total_payment
        var balance = view.tv_balance


    }
}