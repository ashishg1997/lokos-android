package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.PopupWindow
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.entity.MstCOAEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import com.microware.cdfi.viewModel.CboBankViewmodel
import com.microware.cdfi.viewModel.MstCOAViewmodel
import kotlinx.android.synthetic.main.otherto_vo_receipts_list_item.view.*
import kotlinx.android.synthetic.main.vomeeting_bank_item_dialog.view.*

class OthertoVOReceiptsAdapter(var context: Context,
                               var listData:List<MstCOAEntity>,
                               val cboBankViewmodel: CboBankViewmodel?,
                               val mstCOAViewmodel: MstCOAViewmodel?) :
    RecyclerView.Adapter<OthertoVOReceiptsAdapter.ViewHolder>() {

    var validate: Validate? = null
    var cboType = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        if (validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType) == 2) {
            cboType = 2
        } else {
            cboType = 1
        }

        var view = LayoutInflater.from(context).inflate(R.layout.otherto_vo_receipts_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listData.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvFunds.text = listData[position].description

        holder.ivActualPayLocation.setOnClickListener {
            var popupwindow: PopupWindow = popupDisplay()
            val values = IntArray(2)
            holder.tvFunds.getLocationInWindow(values)
            val positionOfIcon = values[1]
            val displayMetrics: DisplayMetrics = context.resources.displayMetrics
            val height: Int = displayMetrics.heightPixels * 2 / 3
            if (positionOfIcon > height) {
                popupwindow.showAsDropDown(holder.tvFunds, -20, -200)
            } else {
                popupwindow.showAsDropDown(holder.tvFunds, -20, 0)
            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    var cardView = view.cardView
    var ivdifBank = view.ivdifBank
    var tvFunds = view.tvFunds
    var tvamount = view.tvamount
    var ivEdit = view.ivEdit
    var ivActualPayLocation = view.ivActualPayLocation

    }

    fun popupDisplay(): PopupWindow {
        val popupWindow = PopupWindow(context)
        // inflate your layout or dynamically add view
        val inflater =
            context.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.vomeeting_bank_item_dialog, null, false)
        popupWindow.isFocusable = true
        popupWindow.width = WindowManager.LayoutParams.WRAP_CONTENT
        popupWindow.height = WindowManager.LayoutParams.WRAP_CONTENT
        popupWindow.setBackgroundDrawable(ColorDrawable(context.resources.getColor(R.color.white)))
        fillRecyclerView2(view)

        popupWindow.contentView = view

        return popupWindow
    }

    private fun fillRecyclerView2(view: View) {
        val bankList = cboBankViewmodel!!.getcboBankdata(
            validate!!.RetriveSharepreferenceString(
                VoSpData.voSHGGUID
            ), cboType
        )
        if (!bankList.isNullOrEmpty()) {
            view.rvList.layoutManager = LinearLayoutManager(context)
            view.rvList.adapter = ChangeOtherstoVoReceiptPaymentAdapter(context, bankList, mstCOAViewmodel)
        }
    }

}