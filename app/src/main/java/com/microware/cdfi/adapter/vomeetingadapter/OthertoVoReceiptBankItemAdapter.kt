package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.entity.MstCOAEntity
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.shgto_vo_receipts_bank_item.view.*

class OthertoVoReceiptBankItemAdapter(val context: Context, val listData: List<MstCOAEntity>) :
    RecyclerView.Adapter<OthertoVoReceiptBankItemAdapter.ViewHolder>() {

    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        var view = LayoutInflater.from(context).inflate(R.layout.shgto_vo_receipts_bank_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return listData.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_fund.text = listData[position].description
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_fund = view.tv_fund
        var tv_amount = view.tv_amount

    }


}