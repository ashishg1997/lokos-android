package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.ReceiptsAndIncome
import com.microware.cdfi.activity.vomeeting.ReceiptsAndIncomeSummary
import com.microware.cdfi.entity.voentity.VoFinTxnDetGrpEntity
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import kotlinx.android.synthetic.main.receipts_and_income_summary_item_row.view.*

class ReceiptAndIncomeSummaryAdapter(var context: Context, var list: List<VoFinTxnDetGrpEntity>) :
    RecyclerView.Adapter<ReceiptAndIncomeSummaryAdapter.ViewHolder>() {
    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context)
            .inflate(R.layout.receipts_and_income_summary_item_row, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val recyptName = (context as ReceiptsAndIncomeSummary).getRecepitType(
            validate!!.returnIntegerValue(list[position].auid.toString())
        )

        holder.tv_particulars.text = recyptName

        holder.tv_amount.text = list[position].amount.toString()

        (context as ReceiptsAndIncomeSummary).getTotalValue1(validate!!.returnIntegerValue(list[position].amount.toString()))

        holder.card_view.setOnClickListener {
            validate!!.SaveSharepreferenceInt(
                VoSpData.voAuid, validate!!.returnIntegerValue(list[position].auid.toString())
            )

            validate!!.SaveSharepreferenceInt(
                VoSpData.vosavingSource,
                validate!!.returnIntegerValue(list[position].amountToFrom.toString())
            )
            val intent = Intent(context, ReceiptsAndIncome::class.java)
            context.startActivity(intent)
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_particulars = view.tv_particulars
        var tv_amount = view.tv_amount
        var card_view = view.card_view


    }
}