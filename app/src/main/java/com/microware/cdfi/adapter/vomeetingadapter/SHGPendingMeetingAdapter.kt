package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import kotlinx.android.synthetic.main.shg_meeting_pending_row_item.view.*
import kotlinx.android.synthetic.main.vo_loan_repayment_summary_vo_to_clf_row_item.view.*
import java.math.BigInteger
import java.text.Format
import java.text.NumberFormat
import java.util.*

class SHGPendingMeetingAdapter(var context: Context) :
    RecyclerView.Adapter<SHGPendingMeetingAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.shg_meeting_pending_row_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return 4
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        fillAdapterData(holder)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var shgName = view.tv_shg_name
        var meetingDate = view.tv_meeting_date
        var meeting_no = view.tv_meeting_no
        var total_pending_meeting = view.tv_total_pending_meeting



    }


    fun fillAdapterData(view: ViewHolder) {
        // var  mynumber=nf.parse("134");
        view.shgName.text= "Adarsh Mahila Sangam"
        view.meetingDate.text="12/05/2021"
        view.meeting_no.text="03"
        view.total_pending_meeting.text="01"



    }
}