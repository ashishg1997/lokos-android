package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.SHGVoSHGTransactionSummary
import com.microware.cdfi.entity.voentity.VoMemLoanEntity
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.shgvoshg_transaction_summery_list_item.view.*

class SHGVoShgTransactionSummaryAdapter(var context: Context, var list: List<VoMemLoanEntity>) :
    RecyclerView.Adapter<SHGVoShgTransactionSummaryAdapter.ViewHolder>() {
    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context)
            .inflate(R.layout.shgvoshg_transaction_summery_list_item, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        setLabelText(holder)

        holder.tv_amt.text = validate!!.returnStringValue(list[position].amount.toString())
        holder.tv_fund_type.text = (context as SHGVoSHGTransactionSummary).returnFundName(
            validate!!.returnIntegerValue(
                list.get(position).fundType.toString()
            )
        )
        holder.tv_period.text = validate!!.returnStringValue(list[position].period.toString())
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_fund_type = view.tv_fund_type
        val tv_amt = view.tv_amt
        val tv_period = view.tv_period
        val tv_total_loan_repayment = view.tv_total_loan_repayment
        val tv_interest_tot = view.tv_interest_tot
        val tv_text_amt = view.tv_text_amt
        val tv_period_month = view.tv_period_month
    }

    fun setLabelText(view: ViewHolder) {
        view.tv_total_loan_repayment.text = LabelSet.getText(
            "loan_disbursement",
            R.string.loan_disbursement
        )
        view.tv_interest_tot.text = LabelSet.getText("fund_type", R.string.fund_type)
        view.tv_text_amt.text = LabelSet.getText("amount", R.string.amount)
        view.tv_period_month.text = LabelSet.getText("period_months", R.string.period_months)
    }
}