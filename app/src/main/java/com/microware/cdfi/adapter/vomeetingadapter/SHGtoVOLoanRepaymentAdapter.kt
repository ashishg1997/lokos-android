package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.SHGtoVOLoanRepayment
import com.microware.cdfi.entity.voentity.VoMemLoanTxnEntity
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import kotlinx.android.synthetic.main.shg_to_vo_loan_repayment_list_item.view.*

class SHGtoVOLoanRepaymentAdapter(var context: Context, val list: List<VoMemLoanTxnEntity>?) :
    RecyclerView.Adapter<SHGtoVOLoanRepaymentAdapter.ViewHolder>() {
    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        var view = LayoutInflater.from(context)
            .inflate(R.layout.shg_to_vo_loan_repayment_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var colorRes = 0
        holder.tv_loan_no.text =
            validate!!.returnIntegerValue(list!!.get(position).loanNo.toString()).toString()
        holder.tvModePayment.text =
            validate!!.returnIntegerValue(list.get(position).modePayment.toString()).toString()
        holder.tv_outstanding.text =
            validate!!.returnIntegerValue(list.get(position).loanOp.toString())
                .toString() + "(" + (context as SHGtoVOLoanRepayment).getremaininginstallment(
                validate!!.returnIntegerValue(
                    list.get(position).loanNo.toString()
                )
            ) + ")"

        var demand =
            validate!!.returnIntegerValue(list.get(position).principalDemandOb.toString()) + validate!!.returnIntegerValue(
                list.get(position).principalDemand.toString()
            )

        var intdemand =
            validate!!.returnIntegerValue(list.get(position).intAccruedOp.toString()) + validate!!.returnIntegerValue(
                list.get(position).intAccrued.toString()
            )
        var totpaid =
            validate!!.returnIntegerValue(list.get(position).loanPaid.toString()) + validate!!.returnIntegerValue(
                list.get(position).loanPaidInt.toString()
            )
        holder.tv_principal_demand.text = LabelSet.getText(
            "rs",
            R.string.rs
        ) + demand
        holder.tv_current_arrear.text = LabelSet.getText(
            "rs",
            R.string.rs
        ) + validate!!.returnIntegerValue(list.get(position).principalDemand.toString()) + "+ " + LabelSet.getText(
            "rs",
            R.string.rs
        ) + validate!!.returnIntegerValue(list.get(position).principalDemandCb.toString())

        holder.tv_interest_demand.text = LabelSet.getText(
            "rs",
            R.string.rs
        ) + intdemand
        holder.tv_current_arrear1.text = LabelSet.getText(
            "rs",
            R.string.rs
        ) + validate!!.returnIntegerValue(list.get(position).intAccrued.toString()) + "+  " + LabelSet.getText(
            "rs",
            R.string.rs
        ) + validate!!.returnIntegerValue(list.get(position).intAccruedOp.toString())
        holder.tv_total.text = LabelSet.getText("rs", R.string.rs) + (demand + intdemand)
//        holder.btnPay.text = LabelSet.getText("rs", R.string.rs)+(demand+intdemand)
        if (totpaid > 0) {
            holder.et_principal_demand.setText(totpaid.toString())
        } else {
            holder.et_principal_demand.setText("")
        }
        holder.tv_totintrest.text = LabelSet.getText(
            "rs",
            R.string.rs
        ) + (intdemand) + " " + LabelSet.getText(
            "asintrest",
            R.string.asintrest
        )
        if (validate!!.returnIntegerValue(list.get(position).modePayment.toString()) > 0) {
            if (validate!!.returnIntegerValue(list.get(position).modePayment.toString()) == 1) {
                colorRes = R.color.green
            } else if (validate!!.returnIntegerValue(list.get(position).modePayment.toString()) == 2 || validate!!.returnIntegerValue(
                    list.get(position).modePayment.toString()
                ) == 3
            ) {
                holder.tvaccountno.text = validate!!.returnStringValue(list.get(position).bankCode)
                holder.tvtransactionno.text =
                    validate!!.returnStringValue(list.get(position).transactionNo)
                when ((context as SHGtoVOLoanRepayment).getbankpos(
                    validate!!.returnStringValue(
                        list[0].bankCode.toString()
                    )
                )) {
                    0 -> colorRes = R.color.color1
                    1 -> colorRes = R.color.color2
                    2 -> colorRes = R.color.color3
                    3 -> colorRes = R.color.color4
                    4 -> colorRes = R.color.color5
                    5 -> colorRes = R.color.color6
                    6 -> colorRes = R.color.color7
                    else -> colorRes = R.color.colordefault
                }

            }
        } else {
            if (validate!!.RetriveSharepreferenceInt(VoSpData.voModePayment) > 1){
                holder.tvaccountno.text = validate!!.RetriveSharepreferenceString(VoSpData.voBankCode)
                when ((context as SHGtoVOLoanRepayment).getbankpos(
                    validate!!.RetriveSharepreferenceString(VoSpData.voBankCode)!!
                )) {
                    0 -> colorRes = R.color.color1
                    1 -> colorRes = R.color.color2
                    2 -> colorRes = R.color.color3
                    3 -> colorRes = R.color.color4
                    4 -> colorRes = R.color.color5
                    5 -> colorRes = R.color.color6
                    6 -> colorRes = R.color.color7
                    else -> colorRes = R.color.colordefault
                }
            }else{
                colorRes = R.color.green
                holder.tvaccountno.text =""
            }
        }
        holder.ivSelectBank.setColorFilter(
            ContextCompat.getColor(context, colorRes),
            android.graphics.PorterDuff.Mode.SRC_IN
        )

        holder.ivSelectBank.setOnClickListener {
            if (validate!!.RetriveSharepreferenceInt(VoSpData.voModePayment) > 1) {
                (context as SHGtoVOLoanRepayment).CustomAlertBankDialog(
                    holder.tvaccountno,
                    holder.tvtransactionno, holder.ivSelectBank
                )
            }
        }
        holder.et_principal_demand.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if(validate!!.returnIntegerValue(list.get(position).loanOp.toString())<
                    validate!!.returnIntegerValue(s.toString())){
                    holder.et_principal_demand.setText("")
                }
                (context as SHGtoVOLoanRepayment).getTotalValue()
            }

        })
        (context as SHGtoVOLoanRepayment).getTotalValue(
            demand,
            intdemand,
            demand + intdemand,
            totpaid
        )

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_loan_no = view.tv_loan_no
        val tv_outstanding = view.tv_outstanding
        val tv_principal_demand = view.tv_principal_demand
        val tv_current_arrear = view.tv_current_arrear
        val tv_interest_demand = view.tv_interest_demand
        val tv_current_arrear1 = view.tv_current_arrear1
        val tv_total = view.tv_total
        val et_principal_demand = view.et_principal_demand
        val tv_totintrest = view.tv_totintrest
        val ivSelectBank = view.ivSelectBank
        val tvaccountno = view.tvaccountno
        val tvtransactionno = view.tvtransactionno
        val tvModePayment = view.tvModePayment
    }


}