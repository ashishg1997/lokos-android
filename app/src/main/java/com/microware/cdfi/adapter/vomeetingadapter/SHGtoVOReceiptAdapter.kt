package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.PopupWindow
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.LoanDueStatusSHGtoVOActivity
import com.microware.cdfi.activity.vomeeting.SHGtoVOReceipts
import com.microware.cdfi.activity.vomeeting.VOtoSHGLoanSummary
import com.microware.cdfi.entity.MstVOCOAEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import com.microware.cdfi.viewModel.CboBankViewmodel
import com.microware.cdfi.viewModel.MasterBankViewmodel
import kotlinx.android.synthetic.main.shgto_vo_receipts_list_item.view.*
import kotlinx.android.synthetic.main.vomeeting_bank_item_dialog.view.*

class SHGtoVOReceiptAdapter(
    var context: Context,
    var list: List<MstVOCOAEntity>,
    val cboBankViewModel: CboBankViewmodel?,
    val bankMasterViewmodel: MasterBankViewmodel?,
    val shgID: Long,
    var shgname: String
) :
    RecyclerView.Adapter<SHGtoVOReceiptAdapter.ViewHolder>() {

    var validate: Validate? = null
    var cboType = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        var view = LayoutInflater.from(context)
            .inflate(R.layout.shgto_vo_receipts_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType) == 2) {
            cboType = 2
        } else {
            cboType = 1
        }

        /*if (list[position].type.equals("RO") || list[position].type.equals("RG") || list[position].type.equals(
                "PG"
            ) || list[position].type.equals("PO")
        ) {
            holder.ivEdit.visibility = View.VISIBLE
        } else {
            holder.ivEdit.visibility = View.GONE
        }*/


        holder.tvFunds.text = list[position].description
        var defaultbankcode = (context as SHGtoVOReceipts).returndefaaultaccount()
        val amountlist = (context as SHGtoVOReceipts).getAmount(list[position].uid, shgID)
        if (!amountlist.isNullOrEmpty()) {
            holder.tvamount.text = validate!!.returnStringValue(amountlist[0].amount.toString())
            if (validate!!.returnIntegerValue(amountlist[0].isEdited.toString()) == 0) {
                holder.ivchk.visibility = View.VISIBLE

            } else {
                holder.ivchk.visibility = View.INVISIBLE
            }
            holder.tvbankcode.text = validate!!.returnStringValue(amountlist[0].bankCode.toString())
            var colorRes = 0
            if (validate!!.returnIntegerValue(amountlist[0].modePayment.toString()) == 1) {
                colorRes = R.color.green
            } else {
                when ((context as SHGtoVOReceipts).getbankpos(
                    validate!!.returnStringValue(
                        amountlist[0].bankCode.toString()
                    )
                )) {
                    0 -> colorRes = R.color.color1
                    1 -> colorRes = R.color.color2
                    2 -> colorRes = R.color.color3
                    3 -> colorRes = R.color.color4
                    4 -> colorRes = R.color.color5
                    5 -> colorRes = R.color.color6
                    6 -> colorRes = R.color.color7
                    else -> colorRes = R.color.colordefault
                }
            }

            holder.ivActualPayLocation.setColorFilter(
                ContextCompat.getColor(context, colorRes),
                android.graphics.PorterDuff.Mode.SRC_IN
            )
        } else {
            holder.tvamount.text = "0"
            holder.tvbankcode.text = defaultbankcode

                holder.ivchk.visibility = View.INVISIBLE


        }
        if (list[position].type.equals("PL")) {
            var amount = (context as SHGtoVOReceipts).getFundlaonamount(
                shgID,
                validate!!.returnIntegerValue(list[position].short_description.toString())
            )
            if (validate!!.returnIntegerValue(amount.toString()) > 0) {
                holder.tvamount.text = validate!!.returnStringValue(amount.toString())
            } else {
                holder.tvamount.text = "0"
            }
        }
        if (list[position].type.equals("RL")) {
            var amount = (context as SHGtoVOReceipts).getFundlaonrepaymentamount(
                shgID,
                validate!!.returnIntegerValue(list[position].short_description.toString())
            )
            if (validate!!.returnDoubleValue(amount.toString()) > 0.0) {
                holder.tvamount.text = validate!!.returnStringValue(amount.toString())
            } else {
                holder.tvamount.text = "0"
            }
        }

        var bankcode=(context as SHGtoVOReceipts).returnbankcode(list[position].uid)
        if (!bankcode.isNullOrEmpty()) {
            var defaultcolorRes = 0
            when ((context as SHGtoVOReceipts).getbankpos(
                bankcode
            ) ){
                0 -> defaultcolorRes = R.color.color1
                1 -> defaultcolorRes = R.color.color2
                2 -> defaultcolorRes = R.color.color3
                3 -> defaultcolorRes = R.color.color4
                4 -> defaultcolorRes = R.color.color5
                5 -> defaultcolorRes = R.color.color6
                6 -> defaultcolorRes = R.color.color7
                else -> defaultcolorRes = R.color.color1
            }

            holder.tvbankcode.text = bankcode
            holder.ivdifBank.setColorFilter(
                ContextCompat.getColor(context, defaultcolorRes),
                android.graphics.PorterDuff.Mode.SRC_IN
            )
        }
        holder.tvFunds.setOnClickListener {
            validate!!.SaveSharepreferenceLong(VoSpData.voShgMemID, shgID)
            validate!!.SaveSharepreferenceInt(VoSpData.voAuid, list[position].uid)
            validate!!.SaveSharepreferenceString(VoSpData.voMemberName, shgname)
            if (validate!!.returnStringValue(holder.tvbankcode.text.toString()).length > 0) {
                validate!!.SaveSharepreferenceInt(VoSpData.voModePayment, 2)
                validate!!.SaveSharepreferenceString(
                    VoSpData.voBankCode,
                    validate!!.returnStringValue(holder.tvbankcode.text.toString())
                )
            } else {
                validate!!.SaveSharepreferenceInt(VoSpData.voModePayment, 1)
                validate!!.SaveSharepreferenceString(VoSpData.voBankCode, "")

            }
            if (list[position].type.equals("RO") || list[position].type.equals("RG") || list[position].type.equals(
                    "PG"
                ) || list[position].type.equals("PO")
            ) {
                (context as SHGtoVOReceipts).CustomAlertAmountDialog(
                    list[position].uid,
                    list[position].type!!,
                    shgID,
                    validate!!.returnStringValue(holder.tvbankcode.text.toString()), shgname
                )
            } else if (list[position].type.equals("RL")) {
                validate!!.SaveSharepreferenceInt(VoSpData.LoanSource, 3)
                validate!!.SaveSharepreferenceInt(VoSpData.LoanSource, validate!!.returnIntegerValue(list[position].short_description.toString()))
                val intent = Intent(context, LoanDueStatusSHGtoVOActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                context.startActivity(intent)
            } else if (list[position].type.equals("PL")) {
                validate!!.SaveSharepreferenceInt(VoSpData.ShortDescription, validate!!.returnIntegerValue(list[position].short_description.toString()))
                validate!!.SaveSharepreferenceInt(VoSpData.LoanSource, 3)
                val intent = Intent(context, VOtoSHGLoanSummary::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                context.startActivity(intent)
            }
        }

        holder.ivActualPayLocation.setOnClickListener {
            var popupwindow: PopupWindow =
                popupDisplay(holder.tvbankcode, holder.ivActualPayLocation)
            val values = IntArray(2)
            holder.tvFunds.getLocationInWindow(values)
            val positionOfIcon = values[1]
            val displayMetrics: DisplayMetrics = context.resources.displayMetrics
            val height: Int = displayMetrics.heightPixels * 2 / 3
            if (positionOfIcon > height) {
                popupwindow.showAsDropDown(holder.tvFunds, -20, -200)
            } else {
                popupwindow.showAsDropDown(holder.tvFunds, -20, 0)
            }
        }

        /*holder.ivEdit.setOnClickListener {
            (context as SHGtoVOReceipts).CustomAlertAmountDialog(
                list[position].uid,
                list[position].type!!,
                shgID,
                validate!!.returnStringValue(holder.tvbankcode.text.toString()), shgname
            )
        }*/

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var cardView = view.cardView
        var ivdifBank = view.ivdifBank
        var tvFunds = view.tvFunds
        var tvamount = view.tvamount
        var tvbankcode = view.tvbankcode
        var ivchk = view.ivchk
        var ivActualPayLocation = view.ivActualPayLocation

    }

    fun popupDisplay(tvbankcode: TextView, ivActualPayLocation: ImageView): PopupWindow {
        val popupWindow = PopupWindow(context)
        // inflate your layout or dynamically add view
        val inflater =
            context.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.vomeeting_bank_item_dialog, null, false)
        popupWindow.isFocusable = true
        popupWindow.width = WindowManager.LayoutParams.WRAP_CONTENT
        popupWindow.height = WindowManager.LayoutParams.WRAP_CONTENT
        popupWindow.setBackgroundDrawable(ColorDrawable(context.resources.getColor(R.color.white)))
        view.tvchangepayment.text =
            LabelSet.getText("change_payment_mode", R.string.change_payment_mode)
        fillRecyclerView2(view, tvbankcode, popupWindow, ivActualPayLocation)

        popupWindow.contentView = view

        return popupWindow
    }

    private fun fillRecyclerView2(
        view: View,
        tvbankcode: TextView,
        popupWindow: PopupWindow,
        ivActualPayLocation: ImageView
    ) {
        val bankList = cboBankViewModel!!.getcboBankdataModel(
            validate!!.RetriveSharepreferenceString(
                VoSpData.voSHGGUID
            ), cboType
        )
        if (!bankList.isNullOrEmpty()) {
            view.rvList.layoutManager = LinearLayoutManager(context)
            view.rvList.adapter = ChangePaymentAdapter(
                context,
                bankList,
                bankMasterViewmodel!!,
                tvbankcode,
                popupWindow, ivActualPayLocation
            )
        }
    }
    /*fun showproduct(layout: LinearLayout, productlist: List<OrderProduct>) {
        layout.removeAllViews()
        for (i in productlist.indices){
            val view = LayoutInflater.from(context).inflate(R.layout.dynamic_orderitem, null)
            val tvDish = view.findViewById<TextView>(R.id.tvDish)
            tvDish.setText(productlist.get(i).quantity.toString() + " * " + productlist.get(i).product_detail.name)
            layout.addView(view)
        }
    }*/

}