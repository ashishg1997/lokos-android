package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.SHGtoVOReceipts
import com.microware.cdfi.api.vomodel.VoChangePaymentBankDataModel
import kotlinx.android.synthetic.main.shgto_vo_receipts_banklist_item.view.*

import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import com.microware.cdfi.viewModel.voviewmodel.VoFinTxnDetMemViewModel

class SHGtoVOReceiptBankAdapter(
    val context: Context,
    val listData: List<VoChangePaymentBankDataModel>?,
    val voFinTxnDetMemViewModel: VoFinTxnDetMemViewModel?,
    val shgID: Long
) :
    RecyclerView.Adapter<SHGtoVOReceiptBankAdapter.ViewHolder>() {

    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        var view = LayoutInflater.from(context)
            .inflate(R.layout.shgto_vo_receipts_banklist_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listData!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var lastfour = ""
        var name = ""
        if (validate!!.returnIntegerValue(listData!![position].modePayment.toString()) == 2) {
            lastfour =
                listData[position].account_no!!.substring(listData[position].account_no!!.length - 4)

            name = (context as SHGtoVOReceipts).getBankName(
                validate!!.returnIntegerValue(
                    listData.get(position).bank_id.toString()
                )
            )
        } else {
            lastfour = ""
            name = "Cash"
        }
        var colorRes = 0
        if (listData.size - 1 == position) {
            holder.tblLayout.setCardBackgroundColor(context.getColor(R.color.green))
            holder.tbl_totamount.setBackgroundColor(context.getColor(R.color.green))
            holder.tv_bank_name.text = name
        } else {
            when (position) {
                0 -> {
                    holder.tblLayout.setCardBackgroundColor(context.getColor(R.color.color1))
                    holder.tbl_totamount.setBackgroundColor(context.getColor(R.color.color1))
                }
                1 -> {
                    holder.tblLayout.setCardBackgroundColor(context.getColor(R.color.color2))
                    holder.tbl_totamount.setBackgroundColor(context.getColor(R.color.color2))
                }
                2 -> {
                    holder.tblLayout.setCardBackgroundColor(context.getColor(R.color.color3))
                    holder.tbl_totamount.setBackgroundColor(context.getColor(R.color.color3))
                }
                3 -> {
                    holder.tblLayout.setCardBackgroundColor(context.getColor(R.color.color4))
                    holder.tbl_totamount.setBackgroundColor(context.getColor(R.color.color4))
                }
                4 -> {
                    holder.tblLayout.setCardBackgroundColor(context.getColor(R.color.color5))
                    holder.tbl_totamount.setBackgroundColor(context.getColor(R.color.color5))
                }
                5 -> {
                    holder.tblLayout.setCardBackgroundColor(context.getColor(R.color.color6))
                    holder.tbl_totamount.setBackgroundColor(context.getColor(R.color.color6))
                }
                6 -> {
                    holder.tblLayout.setCardBackgroundColor(context.getColor(R.color.color7))
                    holder.tbl_totamount.setBackgroundColor(context.getColor(R.color.color7))
                }
                else -> {
                    holder.tblLayout.setCardBackgroundColor(context.getColor(R.color.colordefault))
                    holder.tbl_totamount.setBackgroundColor(context.getColor(R.color.colordefault))
                }

            }
            holder.tv_bank_name.text = name + " (XXXX XXXX XXXX $lastfour)"
        }


        // holder.tblLayout.setCardBackgroundColor(colorRes)


        holder.btn_dropup.setOnClickListener {
            holder.layoutExpend.visibility = View.GONE
            holder.btn_dropup.visibility = View.GONE
            holder.btn_dropdown.visibility = View.VISIBLE
        }

        holder.btn_dropdown.setOnClickListener {
            holder.layoutExpend.visibility = View.VISIBLE
            holder.btn_dropup.visibility = View.VISIBLE
            holder.btn_dropdown.visibility = View.GONE
        }

        holder.tv_clickReceive.setOnClickListener {
            if (listData.size - 1 == position) {
                (context as SHGtoVOReceipts).updatefindetailmemcash(
                    shgID,
                    listData[position].account_no!!,
                    validate!!.returnIntegerValue(holder.tv_totalamount.text.toString())
                )
            } else {
                (context as SHGtoVOReceipts).customReceiveDialog(
                    shgID,
                    listData[position].ifsc_code!!.dropLast(7) + listData[position].account_no!!
                )
            }
        }

        if (listData.size - 1 == position) {
            if (validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 1) {
                fillRecyclerView(
                    holder,
                    shgID,
                    "", listOf("RG", "RL", "RO")
                )
            } else if (validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 5) {
                fillRecyclerView(
                    holder,
                    shgID,
                    "", listOf("PG", "PL", "PO")
                )
            }
        } else {
            if (validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 1) {
                fillRecyclerView(
                    holder,
                    shgID,
                    listData[position].ifsc_code!!.dropLast(7) + listData[position].account_no!!,
                    listOf("RG", "RL", "RO")
                )
            } else if (validate!!.RetriveSharepreferenceInt(VoSpData.voFormType) == 5) {
                fillRecyclerView(
                    holder,
                    shgID,
                    listData[position].ifsc_code!!.dropLast(7) + listData[position].account_no!!,
                    listOf("PG", "PL", "PO")
                )
            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var cardView = view.cardView
        var tv_bank_name = view.tv_bank_name
        var btn_dropup = view.btn_dropup
        var btn_dropdown = view.btn_dropdown
        var layoutExpend = view.layoutExpend
        var rvBankListItem = view.rvBankListItem
        var tv_totalamount = view.tv_totalamount
        var tv_clickReceive = view.tv_clickReceive
        var tv_amountrecieved = view.tv_amountrecieved
        var tblLayout = view.tblLayout
        var tbl_totamount = view.tbl_totamount

    }

    private fun fillRecyclerView(
        holder: ViewHolder,
        shgID: Long,
        accountNo: String,
        type: List<String>
    ) {
        val listData = voFinTxnDetMemViewModel!!.getVoFinTxnDetMemdata(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            shgID,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            accountNo, type
        )
        if (!listData.isNullOrEmpty()) {
            holder.rvBankListItem.layoutManager = LinearLayoutManager(context)
            holder.rvBankListItem.adapter = SHGtoVoReceiptBankItemAdapter(
                context,
                listData,
                holder.tv_totalamount,
                holder.tv_amountrecieved
            )
        }

    }


}