package com.microware.cdfi.adapter.vomeetingadapter

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.SRLMtoVOReceipts
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.MstCOAViewmodel
import kotlinx.android.synthetic.main.buttons_vo.view.*
import kotlinx.android.synthetic.main.receive_dialog.view.*
import kotlinx.android.synthetic.main.srlmto_vo_receipts_banklist_item.view.*

class SRLMtoVOReceiptBankAdapter(
    val context: Context,
    val listData: List<Cbo_bankEntity>?,
    val mstCOAViewmodel: MstCOAViewmodel?
) :
    RecyclerView.Adapter<SRLMtoVOReceiptBankAdapter.ViewHolder>() {

    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        var view = LayoutInflater.from(context)
            .inflate(R.layout.srlmto_vo_receipts_banklist_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listData!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val lastfour =
            listData!![position].account_no.substring(listData[position].account_no.length - 4)
        val name = (context as SRLMtoVOReceipts).getBankName(
            validate!!.returnIntegerValue(
                listData.get(position).bank_id.toString()
            )
        )
        holder.tv_bank_name.text = name + " (XXXX XXXX XXXX $lastfour)"

        holder.btn_dropup.setOnClickListener {
            holder.layoutExpend.visibility = View.GONE
            holder.btn_dropup.visibility = View.GONE
            holder.btn_dropdown.visibility = View.VISIBLE
        }

        holder.btn_dropdown.setOnClickListener {
            holder.layoutExpend.visibility = View.VISIBLE
            holder.btn_dropup.visibility = View.VISIBLE
            holder.btn_dropdown.visibility = View.GONE
        }

        holder.tv_clickReceive.setOnClickListener {
            customReceiveDialog()
        }


        fillRecyclerView(holder)

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var cardView = view.cardView
        var tv_bank_name = view.tv_bank_name
        var btn_dropup = view.btn_dropup
        var btn_dropdown = view.btn_dropdown
        var layoutExpend = view.layoutExpend
        var rvBankListItem = view.rvBankListItem
        var tv_totalamount = view.tv_totalamount
        var tv_clickReceive = view.tv_clickReceive
        var tv_amount = view.tv_amount

    }

    private fun fillRecyclerView(holder: ViewHolder) {
        val listData = mstCOAViewmodel!!.getReceiptCoaSubHeadData(
            "OR",
            "OI",
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
        )
        if (!listData.isNullOrEmpty()) {
            holder.rvBankListItem.layoutManager = LinearLayoutManager(context)
            holder.rvBankListItem.adapter = SRLMtoVoReceiptBankItemAdapter(context, listData)
        }
    }

    fun customReceiveDialog() {
        val mDialogView =
            LayoutInflater.from(context).inflate(R.layout.receive_dialog, null)
        val mBuilder = AlertDialog.Builder(context).setView(mDialogView)
        val mAlertDialog = mBuilder.show()

        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.WHITE))

        setLabel(mDialogView)

        mDialogView.btn_save.setOnClickListener {
            mAlertDialog.dismiss()
        }

        mDialogView.btn_cancel.setOnClickListener {
            mAlertDialog.dismiss()
        }

        mDialogView.btn_Add.setOnClickListener {
            mAlertDialog.dismiss()
        }

    }

    private fun setLabel(view: View) {
        view.btn_save.text = LabelSet.getText("save", R.string.save)
        view.btn_cancel.text = LabelSet.getText("cancel", R.string.cancel)

        view.tv_cheque.text = LabelSet.getText(
            "cheque_payslip_rtgs_neft_imps_upi",
            R.string.cheque_payslip_rtgs_neft_imps_upi
        )
        view.tv_cheque_no_transactio_no.text = LabelSet.getText(
            "cheque_no_transactio_no",
            R.string.cheque_no_transactio_no
        )



        view.et_cheque_no.hint = LabelSet.getText("type", R.string.type)
    }

}