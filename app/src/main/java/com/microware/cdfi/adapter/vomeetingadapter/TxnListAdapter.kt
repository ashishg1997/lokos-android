package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.SHGtoVOReceipts
import com.microware.cdfi.entity.voentity.VoFinTxnDetMemEntity
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.txn_list_item.view.*

class TxnListAdapter(
    var context: Context,
    val listData: List<VoFinTxnDetMemEntity>?
) :
    RecyclerView.Adapter<TxnListAdapter.ViewHolder>() {

    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        var view = LayoutInflater.from(context)
            .inflate(R.layout.txn_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listData!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        holder.tvamount.text = validate!!.returnStringValue(listData!!.get(position).amount.toString())
        holder.tvFunds.text = (context as SHGtoVOReceipts).getname(listData[position].auid)


    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var cardView = view.cardView
        var tvFunds = view.tvFunds
        var tvamount = view.tvamount

    }




}