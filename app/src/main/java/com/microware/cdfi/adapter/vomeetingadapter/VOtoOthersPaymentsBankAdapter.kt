package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.VOtoOtherPayments
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.MstCOAViewmodel
import kotlinx.android.synthetic.main.vo_to_others_payments_row_banklistitem.view.*

class VOtoOthersPaymentsBankAdapter(
    val context: Context,
    val listData: List<Cbo_bankEntity>?,
    val mstCOAViewmodel: MstCOAViewmodel?
) :
    RecyclerView.Adapter<VOtoOthersPaymentsBankAdapter.ViewHolder>() {

    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        var view = LayoutInflater.from(context)
            .inflate(R.layout.vo_to_others_payments_row_banklistitem, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listData!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val lastfour=listData!![position].account_no.substring(listData[position].account_no.length-4)
        val name = (context as VOtoOtherPayments).getBankName(validate!!.returnIntegerValue(listData.get(position).bank_id.toString()))
        holder.tv_bank_name.text = name + " (XXXX XXXX XXXX $lastfour)"

        holder.btn_dropup.setOnClickListener {
            holder.layoutExpend.visibility = View.GONE
            holder.btn_dropup.visibility = View.GONE
            holder.btn_dropdown.visibility = View.VISIBLE
        }

        holder.btn_dropdown.setOnClickListener {
            holder.layoutExpend.visibility = View.VISIBLE
            holder.btn_dropup.visibility = View.VISIBLE
            holder.btn_dropdown.visibility = View.GONE
        }

        holder.tv_clickReceive.setOnClickListener {

        }


        fillRecyclerView(holder)

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var cardView = view.cardView
        var tv_bank_name = view.tv_bank_name
        var btn_dropup = view.btn_dropup
        var btn_dropdown = view.btn_dropdown
        var layoutExpend = view.layoutExpend
        var rvBankListItem = view.rvBankListItem
        var tv_totalamount = view.tv_totalamount
        var tv_clickReceive = view.tv_clickReceive
        var tv_amount = view.tv_amount

    }

    private fun fillRecyclerView(holder: ViewHolder) {
        var listData = mstCOAViewmodel!!.getReceiptCoaSubHeadData(
            "OP",
            "OE",
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
        )
        if (!listData.isNullOrEmpty()) {
            holder.rvBankListItem.layoutManager = LinearLayoutManager(context)
            holder.rvBankListItem.adapter = VoToShgPaymentBankItemAdapter(context, listData)
        }
    }


}