package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.VOtoSHGPayments
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.MstCOAViewmodel
import kotlinx.android.synthetic.main.vo_to_shg_payment_row.view.*

class VOtoSHGPaymentsBankAdapter(
    val context: Context,
    val listData: List<Cbo_bankEntity>?,
    val mstCOAViewmodel: MstCOAViewmodel?
) :
    RecyclerView.Adapter<VOtoSHGPaymentsBankAdapter.ViewHolder>() {

    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        var view = LayoutInflater.from(context)
            .inflate(R.layout.vo_to_shg_payment_row, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listData!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val lastfour =
            listData!![position].account_no.substring(listData[position].account_no.length - 4)
        val name = (context as VOtoSHGPayments).getBankName(
            validate!!.returnIntegerValue(
                listData.get(position).bank_id.toString()
            )
        )
        holder.tv_bank_name.text = name + " (XXXX XXXX XXXX $lastfour)"

        when (position) {
            0 ->  holder.tblLayout.setCardBackgroundColor( context.getColor(R.color.color1))
            1 -> holder.tblLayout.setCardBackgroundColor( context.getColor(R.color.color2))
            2 -> holder.tblLayout.setCardBackgroundColor( context.getColor(R.color.color3))
            3 -> holder.tblLayout.setCardBackgroundColor( context.getColor(R.color.color4))
            4 -> holder.tblLayout.setCardBackgroundColor( context.getColor(R.color.color5))
            5 -> holder.tblLayout.setCardBackgroundColor( context.getColor(R.color.color6))
            6 -> holder.tblLayout.setCardBackgroundColor( context.getColor(R.color.color7))
            else -> holder.tblLayout.setCardBackgroundColor( context.getColor(R.color.colordefault))

        }
        holder.btn_dropup.setOnClickListener {
            holder.layoutExpend.visibility = View.GONE
            holder.btn_dropup.visibility = View.GONE
            holder.btn_dropdown.visibility = View.VISIBLE
        }

        holder.btn_dropdown.setOnClickListener {
            holder.layoutExpend.visibility = View.VISIBLE
            holder.btn_dropup.visibility = View.VISIBLE
            holder.btn_dropdown.visibility = View.GONE
        }

        holder.tv_clickReceive.setOnClickListener {

        }


        fillRecyclerView(holder)

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var cardView = view.cardView
        var tv_bank_name = view.tv_bank_name
        var btn_dropup = view.btn_dropup
        var btn_dropdown = view.btn_dropdown
        var layoutExpend = view.layoutExpend
        var rvBankListItem = view.rvBankListItem
        var tv_totalamount = view.tv_totalamount
        var tv_clickReceive = view.tv_clickReceive
        var tv_amount = view.tv_amount
        var tblLayout = view.tblLayout

    }

    private fun fillRecyclerView(holder: ViewHolder) {
        val listData = mstCOAViewmodel!!.getReceiptCoaSubHeadData(
            "OP",
            "OE",
            validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!
        )
        if (!listData.isNullOrEmpty()) {
            holder.rvBankListItem.layoutManager = LinearLayoutManager(context)
            holder.rvBankListItem.adapter = VoToShgPaymentBankItemAdapter(context,listData)
        }
    }


}