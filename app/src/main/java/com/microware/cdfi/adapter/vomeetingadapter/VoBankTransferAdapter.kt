package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.VoBankTransferActivity
import com.microware.cdfi.api.meetingmodel.BankTransactionModel
import com.microware.cdfi.api.vomodel.VOBankTransactionModel
import com.microware.cdfi.utility.VoSpData
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.vobank_transfer_list_item.view.*

class VoBankTransferAdapter(
    var context: Context,
    var list: List<VOBankTransactionModel>
) :
    RecyclerView.Adapter<VoBankTransferAdapter.ViewHolder>() {
    var validate: Validate? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        val view = LayoutInflater.from(context)
            .inflate(R.layout.vobank_transfer_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_srno.text = (position + 1).toString()
        holder.tv_bankFrom.text = list[position].bank_from
        holder.tv_bankTo.text = list[position].bank_to
        holder.tv_amount.text = validate!!.returnStringValue(list[position].amount.toString())
        holder.cardView.setOnClickListener {
            validate!!.SaveSharepreferenceInt(VoSpData.voAuid,list[position].auid)
            validate!!.SaveSharepreferenceString(VoSpData.bankFrom,list[position].bank_from)
            validate!!.SaveSharepreferenceString(VoSpData.bankTo,list[position].bank_to)
            validate!!.SaveSharepreferenceString(VoSpData.amountTransferred,list[position].amount.toString())
            val intent = Intent(context, VoBankTransferActivity::class.java)
            context.startActivity(intent)
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_srno = view.tv_srno
        var tv_bankFrom = view.tv_bankFrom
        var tv_bankTo = view.tv_bankTo
        var tv_amount = view.tv_amount
        var cardView = view.cardView
    }

}