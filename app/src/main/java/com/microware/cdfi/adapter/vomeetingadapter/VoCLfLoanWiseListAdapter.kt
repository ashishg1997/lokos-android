package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.VOLoanwisedustatus
import com.microware.cdfi.entity.voentity.VoGroupLoanTxnEntity
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.vo_clf_loan_wise_item_row.view.*

class VoCLfLoanWiseListAdapter(var context: Context,val list: List<VoGroupLoanTxnEntity>?) :
    RecyclerView.Adapter<VoCLfLoanWiseListAdapter.ViewHolder>() {
    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        var view = LayoutInflater.from(context).inflate(R.layout.vo_clf_loan_wise_item_row, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_loan_no1.text =
            validate!!.returnIntegerValue(list!!.get(position).loanNo.toString()).toString()
        holder.tv_outstanding1.text =
            validate!!.returnIntegerValue(list.get(position).loanOp.toString()).toString()+"("+(context as VOLoanwisedustatus).getremaininginstallment(validate!!.returnIntegerValue(
                list.get(position).loanNo.toString()))+")"
        holder.tv_original_loan_amount1.text = (context as VOLoanwisedustatus).getloanamount(validate!!.returnIntegerValue(
            list.get(position).loanNo.toString())) .toString()
        var totcurrentdue=validate!!.returnIntegerValue(list.get(position).principalDemand.toString())+validate!!.returnIntegerValue(
            list.get(position).intAccrued.toString())
        var totdue=validate!!.returnIntegerValue(list.get(position).principalDemandOb.toString())+validate!!.returnIntegerValue(
            list.get(position).intAccruedOp.toString())
        holder.tv_current_due1.text = totcurrentdue.toString()
        holder.total_due1.text = (totcurrentdue+totdue).toString()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_loan_no1=view.tv_loan_no1
        val tv_outstanding1=view.tv_outstanding1
        val tv_original_loan_amount1=view.tv_original_loan_amount1
        val tv_current_due1=view.tv_current_due1
        val total_due1=view.total_due1
    }

}