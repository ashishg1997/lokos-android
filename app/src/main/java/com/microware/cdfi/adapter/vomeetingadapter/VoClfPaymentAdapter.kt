package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.entity.MstCOAEntity
import kotlinx.android.synthetic.main.vo_to_clfpayment_list_item.view.*

class VoClfPaymentAdapter(var context: Context,var list: List<MstCOAEntity>?) :
    RecyclerView.Adapter<VoClfPaymentAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.vo_to_clfpayment_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvFunds.text=list!![position].description
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var cardView = view.cardView
        var ivdifBank = view.ivdifBank
        var tvFunds = view.tvFunds
        var tvamount = view.tvamount
        var ivEdit = view.ivEdit
        var ivActualPayLocation = view.ivActualPayLocation

    }
}