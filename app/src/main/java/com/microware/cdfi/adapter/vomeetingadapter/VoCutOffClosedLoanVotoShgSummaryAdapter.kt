package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.CutOffClosedMemberLoan
import com.microware.cdfi.activity.meeting.CutOffClosedMemberLoanList
import com.microware.cdfi.activity.vomeeting.VoCutOffCloseLoanVOtoSHG
import com.microware.cdfi.activity.vomeeting.VoCutOffClosedLoanDetailList
import com.microware.cdfi.activity.vomeeting.VoCutOffClosedLoanVotoShgSummary
import com.microware.cdfi.api.vomodel.VoLoanListModel
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import kotlinx.android.synthetic.main.cut_off_closed_loan_summary_item.view.*

class VoCutOffClosedLoanVotoShgSummaryAdapter(
    var context: Context,
    var list: List<VoLoanListModel>
) :
    RecyclerView.Adapter<VoCutOffClosedLoanVotoShgSummaryAdapter.ViewHolder>() {
    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.cut_off_closed_loan_summary_item, parent, false)
        validate = Validate(context)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.tv_srno.text = (position + 1).toString()
        holder.tv_memberName.text = list.get(position).childCboName.toString()
        val totalLoanAmount = (context as VoCutOffClosedLoanVotoShgSummary).getTotalClosedLoanAmount(
                list.get(position).memId,
                list.get(position).mtgNo)

        val totalLoanCount = (context as VoCutOffClosedLoanVotoShgSummary).getTotalClosedLoanCount(
            list.get(position).memId,
            list.get(position).mtgNo)

//        val fundName = (context as VoCutOffClosedLoanVotoShgSummary).getFundTypeName(validate!!.returnIntegerValue(list.get(position).fundType.toString()))

        holder.tv_total_closed_loan.text = totalLoanCount.toString()

        holder.tv_loan_amount.text = totalLoanAmount.toString()

//        holder.tv_fund_type.setText(fundName)

        (context as VoCutOffClosedLoanVotoShgSummary).getTotalValue()

        holder.img_edit.setOnClickListener {
            validate!!.SaveSharepreferenceLong(VoSpData.voShgMemID, list.get(position).memId!!)
            validate!!.SaveSharepreferenceInt(
                VoSpData.voLoanno,
                validate!!.returnIntegerValue(list.get(position).loanNo.toString())
            )
            var inetnt = Intent(context, VoCutOffCloseLoanVOtoSHG::class.java)
            context.startActivity(inetnt)
        }

        holder.tv_total_closed_loan.setOnClickListener {
            validate!!.SaveSharepreferenceLong(VoSpData.voShgMemID, list.get(position).memId!!)
            validate!!.SaveSharepreferenceInt(
                VoSpData.voLoanno, validate!!.returnIntegerValue(list.get(position).loanNo.toString())
            )
            var inetnt = Intent(context, VoCutOffClosedLoanDetailList::class.java)
            context.startActivity(inetnt)
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_srno = view.tv_srno
        var tv_memberName = view.tv_memberName
        var tv_fund_type = view.tv_fund_type
        var tv_total_closed_loan = view.tv_total_closed_loan
        var tv_loan_amount = view.tv_loan_amount
        var img_edit = view.img_edit
        var cardView = view.cardView
    }
}