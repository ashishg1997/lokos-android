package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.VoCutOffMeetingAttendanceActivity
import com.microware.cdfi.entity.voentity.VoMtgDetEntity
import com.microware.cdfi.utility.InputFilterMinMax
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import kotlinx.android.synthetic.main.cut_off_meeting_detail_item.view.*


class VoCutOffMeetingDetailAdapter(var context: Context,var list: List<VoMtgDetEntity>):
    RecyclerView.Adapter<VoCutOffMeetingDetailAdapter.ViewHolder>() {
    var validate: Validate?=null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate= Validate(context)
        var view = LayoutInflater.from(context).inflate(R.layout.cut_off_meeting_detail_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_shg_name.text = list.get(position).childCboName.toString()
        holder.tv_sr_no.text = "" + (position + 1)

        if (list.get(position).zeroMtgAttn!!>0) {
            holder.et_Attendance.setText(validate!!.returnStringValue(list.get(position).zeroMtgAttn.toString()))
        }else{
            holder.et_Attendance.setText("0")
        }
        holder.et_memId.setText(list.get(position).memId.toString())

        holder.et_Attendance.filters = arrayOf(
            InputFilterMinMax (1, validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber))
        )

        holder.et_Attendance.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                (context as VoCutOffMeetingAttendanceActivity).getTotalAttendanceValue()
            }

        })
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_shg_name=view.tv_shg_name
        var tv_sr_no=view.tv_sr_no
        var et_Attendance = view.et_Attendance
        var et_memId = view.et_memId
    }

}