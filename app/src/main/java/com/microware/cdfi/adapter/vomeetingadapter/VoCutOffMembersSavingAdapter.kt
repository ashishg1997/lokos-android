package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.VoCutOffMembersSavingActivity
import com.microware.cdfi.entity.voentity.VoMtgDetEntity
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import kotlinx.android.synthetic.main.cut_off_member_saving_items.view.*

class VoCutOffMembersSavingAdapter(var context: Context,var list: List<VoMtgDetEntity>) :
    RecyclerView.Adapter<VoCutOffMembersSavingAdapter.ViewHolder>() {
    var validate: Validate?=null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate= Validate(context)
        var view = LayoutInflater.from(context).inflate(R.layout.cut_off_member_saving_items, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_shg_name.text = list.get(position).childCboName.toString()
        holder.tv_sr_no.text = "" + (position + 1)
        holder.et_memId.setText(list.get(position).memId.toString())

           var compAmount= (context as VoCutOffMembersSavingActivity).getSavingamount(validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
               validate!!.RetriveSharepreferenceLong(VoSpData.voshgid), list.get(position).memId,82)

        if (compAmount!!>0) {
            holder.et_compulasory_amount.setText(compAmount.toString())
        } else{
                 holder.et_compulasory_amount.setText("0")
               }


        var volAmount= (context as VoCutOffMembersSavingActivity).getSavingamount(validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid), list.get(position).memId,3)

        if(volAmount!!>0) {
            holder.et_voluntary_saving_amount.setText(volAmount.toString())
        } else{
            holder.et_voluntary_saving_amount.setText("0")
        }

        if(validate!!.returnIntegerValue(list.get(position).zeroMtgAttn.toString())==0){
            holder.tv_shg_name.setTextColor(context.resources.getColor(R.color.red))
        }else{
            holder.tv_shg_name.setTextColor(context.resources.getColor(R.color.black))
        }

        holder.et_compulasory_amount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                (context as VoCutOffMembersSavingActivity).getTotalCompulsoryValue()
            }

        })

        holder.et_voluntary_saving_amount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                (context as VoCutOffMembersSavingActivity).getTotalVoluntaryValue()
            }

        })
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        var tv_shg_name=view.tv_shg_Name
        var tv_sr_no=view.tv_sr_no
        var et_compulasory_amount=view.et_compulsory_saving_amount
        var et_voluntary_saving_amount=view.et_voluntary_saving_amount
        var et_memId=view.et_memId
    }
}