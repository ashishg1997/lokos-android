package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.VoCutOffMembersShareCapital
import com.microware.cdfi.api.vomodel.VoShareCapitalModel
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.cut_off_members_share_capital_items.view.*

class VoCutOffMembersShareCapitalAdapter(var context: Context, var list: List<VoShareCapitalModel>) :
    RecyclerView.Adapter<VoCutOffMembersShareCapitalAdapter.ViewHolder>() {
    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context)
            .inflate(R.layout.cut_off_members_share_capital_items, parent, false)

        validate = Validate(context)

        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.tv_srno.text = "" + (position + 1)
        holder.tv_memberName.text = list.get(position).childCboName.toString()
        holder.tv_memid.setText(list[position].memId.toString())

        (context as VoCutOffMembersShareCapital).getTotalValue(
            validate!!.returnIntegerValue(
                list[position].amount.toString()
            )
        )

        if (validate!!.returnIntegerValue(list[position].amount.toString()) == 0) {
            holder.tv_share_capital.setText("0")
        } else {
            holder.tv_share_capital.setText(validate!!.returnStringValue(list[position].amount.toString()))
        }

        holder.tv_share_capital.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                s.toString()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                (context as VoCutOffMembersShareCapital).getTotalValue()
            }

        })

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tv_srno = view.tv_srno
        var tv_memberName = view.tv_memberName
        var tv_share_capital = view.tv_share_capital
        var tv_memid = view.tv_memid
    }
}