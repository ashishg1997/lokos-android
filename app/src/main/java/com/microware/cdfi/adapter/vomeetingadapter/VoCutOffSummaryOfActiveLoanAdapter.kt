package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.VoCutOffPrincipalDemandActivity
import com.microware.cdfi.activity.vomeeting.VoCutOffSummaryOfActiveLoan
import com.microware.cdfi.activity.vomeeting.VoCutoffActiveLoanVOtoSHG
import com.microware.cdfi.api.vomodel.VoLoanListModel
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import kotlinx.android.synthetic.main.cut_off_active_loan_vo_to_shg_summary_item.view.*

class VoCutOffSummaryOfActiveLoanAdapter(var context: Context, var list: List<VoLoanListModel>) :
    RecyclerView.Adapter<VoCutOffSummaryOfActiveLoanAdapter.ViewHolder>() {
    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.cut_off_active_loan_vo_to_shg_summary_item, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_srno.text = (position + 1).toString()

        holder.tv_shg_name.text = list.get(position).childCboName.toString()

        holder.tv_total_outstanding_amount.text =
            validate!!.returnIntegerValue(list.get(position).amtDisbursed.toString()).toString()

        holder.tv_loan_disbrused.text= validate!!.returnIntegerValue(list.get(position).original_loan_amount.toString()).toString()

        (context as VoCutOffSummaryOfActiveLoan).getTotalValue()

        holder.tv_total_outstanding_amount.setOnClickListener {
            validate!!.SaveSharepreferenceLong(VoSpData.voShgMemID, list.get(position).memId!!)

            validate!!.SaveSharepreferenceInt(VoSpData.voLoanno,
                validate!!.returnIntegerValue(list.get(position).loanNo.toString())
            )
            validate!!.SaveSharepreferenceString(
                VoSpData.voMemberName, list.get(position).childCboName.toString()
            )

            var intent = Intent(context, VoCutOffPrincipalDemandActivity::class.java)
            context.startActivity(intent)
        }

        holder.img_edit.setOnClickListener {
            validate!!.SaveSharepreferenceLong(VoSpData.voShgMemID, list.get(position).memId!!)
            validate!!.SaveSharepreferenceInt(
                VoSpData.voLoanno,
                validate!!.returnIntegerValue(list.get(position).loanNo.toString())
            )
            var intent = Intent(context, VoCutoffActiveLoanVOtoSHG::class.java)
            context.startActivity(intent)
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_srno = view.tv_srno
        var tv_shg_name = view.tv_shg_name
        var tv_total_outstanding_amount = view.tv_totalOutstandingAmount
        var tv_loan_disbrused = view.tv_loan_disbrused
        var cardView = view.cardView
        var img_edit = view.img_edit

    }

}