package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.VoCutOffVoBankBalance
import com.microware.cdfi.activity.vomeeting.VoCutOffVoBankBalanceList
import com.microware.cdfi.entity.voentity.VoFinTxnEntity
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import kotlinx.android.synthetic.main.cut_off_vo_bank_balance_items.view.*

class VoCutOffVoBankBalanceAdapter(var context: Context, var list: List<VoFinTxnEntity>) :
    RecyclerView.Adapter<VoCutOffVoBankBalanceAdapter.ViewHolder>() {
    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context)
            .inflate(R.layout.cut_off_vo_bank_balance_items, parent, false)
        validate = Validate(context)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_srno.text = (position + 1).toString()
        val bankName =
            (context as VoCutOffVoBankBalanceList).setaccount(list[position].bankCode)

        if (bankName.isNotEmpty()) {
            holder.tv_bankName.text = bankName
        }

        holder.tv_bankName.text = list[position].bankCode

        if (validate!!.returnIntegerValue(list[position].closingBalance.toString()) > 0) {
            holder.tv_amount.text = "" + list[position].closingBalance
        } else {
            holder.tv_amount.text = "0"
        }

        (context as VoCutOffVoBankBalanceList).getTotalValue1(validate!!.returnIntegerValue(list[position].closingBalance.toString()))

        holder.cardView.setOnClickListener {
            validate!!.SaveSharepreferenceString(VoSpData.voBankCode, list[position].bankCode)
            val intent = Intent(context, VoCutOffVoBankBalance::class.java)
            context.startActivity(intent)

        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_srno = view.tv_srno
        var tv_bankName = view.tv_bankName
        var tv_amount = view.tv_amount
        var ivEdit = view.ivEdit
        var cardView = view.cardView
    }
}