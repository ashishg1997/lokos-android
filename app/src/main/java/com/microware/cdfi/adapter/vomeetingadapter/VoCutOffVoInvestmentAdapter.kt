package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.GroupInvestmentActivity
import com.microware.cdfi.activity.meeting.GroupInvestmentListActivity
import com.microware.cdfi.activity.vomeeting.VoCutOffVoInvestment
import com.microware.cdfi.activity.vomeeting.VoCutOffVoInvestmentList
import com.microware.cdfi.entity.voentity.VoFinTxnDetGrpEntity
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import kotlinx.android.synthetic.main.cut_off_vo_investment_items.view.*

class VoCutOffVoInvestmentAdapter(var context: Context,var list: List<VoFinTxnDetGrpEntity>) :
    RecyclerView.Adapter<VoCutOffVoInvestmentAdapter.ViewHolder>() {

    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.cut_off_vo_investment_items, parent, false)
        validate = Validate(context)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val type = (context as VoCutOffVoInvestmentList).returnSubHeadDescription(
            validate!!.returnIntegerValue(list[position].auid.toString()))

        val place = (context as VoCutOffVoInvestmentList).getValue(
            validate!!.returnIntegerValue(list[position].amountToFrom.toString()),91)

        if (!type.isNullOrEmpty()) {
            holder.tv_investment.text = type
        }

        if (!place.isNullOrEmpty()) {
            holder.tv_place.text = place
        }

        holder.tv_date.text =  validate!!.convertDatetime(
            validate!!.returnLongValue(
                list.get(
                    position
                ).dateRealisation.toString()
            )
        )


        holder.tv_amount.text = list[position].amount.toString()
        (context as VoCutOffVoInvestmentList).getTotalValue1(validate!!.returnIntegerValue(list[position].amount.toString()))

        holder.cardView.setOnClickListener {
            validate!!.SaveSharepreferenceString(VoSpData.vomtg_guid, list[position].mtgGuid)
            validate!!.SaveSharepreferenceLong(VoSpData.voshgid, list[position].cboId)
            validate!!.SaveSharepreferenceInt(VoSpData.vocurrentmeetingnumber, list[position].mtgNo)
            validate!!.SaveSharepreferenceInt(VoSpData.voAuid, validate!!.returnIntegerValue(list[position].auid.toString()))
            validate!!.SaveSharepreferenceInt(VoSpData.vosavingSource, validate!!.returnIntegerValue(list[position].amountToFrom.toString()))
            val intent = Intent(context, VoCutOffVoInvestment::class.java)
            context.startActivity(intent)
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_investment = view.tv_investment
        val tv_amount = view.tv_amount
        val tv_place = view.tv_place
        val tv_date = view.tv_date
        val cardView = view.cardView
    }
}