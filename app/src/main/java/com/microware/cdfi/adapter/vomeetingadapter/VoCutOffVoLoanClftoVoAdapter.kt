package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.VoCutOffVoLoanCLFtoVO
import com.microware.cdfi.activity.vomeeting.VoCutOffVoLoanCLFtoVOList
import com.microware.cdfi.entity.voentity.VoGroupLoanEntity
import com.microware.cdfi.entity.voentity.VoMemLoanEntity
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import kotlinx.android.synthetic.main.cut_off_vo_loan_clf_to_vo_items.view.*

class VoCutOffVoLoanClftoVoAdapter(var context: Context, var list: List<VoGroupLoanEntity>) :
    RecyclerView.Adapter<VoCutOffVoLoanClftoVoAdapter.ViewHolder>() {
    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context)
            .inflate(R.layout.cut_off_vo_loan_clf_to_vo_items, parent, false)
        validate = Validate(context)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var installment_Amount = 0
        if (validate!!.returnIntegerValue(list.get(position).period.toString()) > 0){
            installment_Amount =
                (validate!!.returnIntegerValue(list.get(position).amount.toString())-validate!!.returnIntegerValue(
                    list.get(position).principalOverdue.toString())) / validate!!.returnIntegerValue(
                    list.get(position).period.toString()
                )
        }

        var fundName = (context as VoCutOffVoLoanCLFtoVOList).getFundTypeName(list.get(position).loanProductId)

        holder.tv_loans.text = fundName

        holder.tv_total_outstanding_amount.text = list.get(position).amount.toString()
        holder.tv_interest_rate.text = list.get(position).interestRate.toString()
        holder.tv_installment_amount.text = installment_Amount.toString()


        holder.cardView.setOnClickListener {
            validate!!.SaveSharepreferenceInt(VoSpData.voLoanno, list.get(position).loanNo)
            val i = Intent(context, VoCutOffVoLoanCLFtoVO::class.java)
            context.startActivity(i)
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_loans = view.tv_loans
        var tv_total_outstanding_amount = view.tv_total_outstanding_amount
        var tv_interest_rate = view.tv_interest_rate
        var tv_installment_amount = view.tv_installment_amount
        var cardView = view.cardView
    }
}