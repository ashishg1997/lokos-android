package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.VoCutOffVoLoanBank
import com.microware.cdfi.activity.vomeeting.VoCutOffVoLoanBankList
import com.microware.cdfi.entity.voentity.VoMemLoanEntity
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import kotlinx.android.synthetic.main.cut_off_vo_loan_bank_items.view.*

class VoCutOffVoLoanbankAdapter(var context: Context,var list: List<VoMemLoanEntity>) :
    RecyclerView.Adapter<VoCutOffVoLoanbankAdapter.ViewHolder>() {
    var validate: Validate? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.cut_off_vo_loan_bank_items, parent, false)
        validate = Validate(context)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val installment_Amount =
            (validate!!.returnIntegerValue(list.get(position).amount.toString())-validate!!.returnIntegerValue(
                list.get(position).principalOverdue.toString())) / validate!!.returnIntegerValue(
                list.get(position).period.toString()
            )

        val bankName =
            (context as VoCutOffVoLoanBankList).setaccount(list[position].bankCode!!)

        if (bankName.isNotEmpty()) {
            holder.tv_loans.text = bankName
        }
        else {
            holder.tv_loans.text = ""
        }


        if (list.get(position).amount!!>0) {
            holder.tv_total_outstanding_amount.text = list.get(position).amount.toString()
        } else{
            holder.tv_total_outstanding_amount.text = "0"
        }

        if (list.get(position).interestRate!!>0) {
            holder.tv_interest_rate.text = list.get(position).interestRate.toString()
        } else{
            holder.tv_interest_rate.text = "0.0"
        }

        if (installment_Amount>0) {
            holder.tv_installment_amount.text = installment_Amount.toString()
        } else{
            holder.tv_installment_amount.text = "0"
        }

        holder.cardView.setOnClickListener {
            validate!!.SaveSharepreferenceLong(VoSpData.voMemberId, list[position].memId)
            validate!!.SaveSharepreferenceInt(VoSpData.voLoanno, list[position].loanNo)
            val intent = Intent(context, VoCutOffVoLoanBank::class.java)
            context.startActivity(intent)

        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_loans = view.tv_loans
        var tv_total_outstanding_amount = view.tv_total_outstanding_amount
        var tv_interest_rate = view.tv_interest_rate
        var tv_installment_amount = view.tv_installment_amount
        var cardView = view.card_view
    }
}