package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.VoGroupLoanlist
import com.microware.cdfi.activity.vomeeting.VoGroupPrincipalDemandActivity
import com.microware.cdfi.entity.voentity.VoGroupLoanEntity
import com.microware.cdfi.utility.*
import kotlinx.android.synthetic.main.vo_loanlist_detail_item.view.*

class VoGroupLoanListAdapter(
    var context: Context,
    val list: List<VoGroupLoanEntity>
) :
    RecyclerView.Adapter<VoGroupLoanListAdapter.ViewHolder>() {
var validate:Validate?=null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate=Validate(context)
        var view = LayoutInflater.from(context).inflate(R.layout.vo_loanlist_detail_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        setLabelText(holder)

        val santionamt=validate!!.returnIntegerValue(list.get(position).sanctionedAmount.toString())
        val cclimit=validate!!.returnIntegerValue(list.get(position).drawingLimit.toString())
        val disburseamt=validate!!.returnIntegerValue(list.get(position).amount.toString())
        holder.tv_value1.text = santionamt.toString()
        (context as VoGroupLoanlist).getTotalValue(santionamt,1)
        holder.tv_source.text = (context as VoGroupLoanlist).returnloanlist(66,validate!!.returnIntegerValue(list.get(position).loanNo.toString()))
        holder.tv_value2.text = cclimit.toString()
        holder.tv_value3.text = disburseamt.toString()
        (context as VoGroupLoanlist).getTotalValue(cclimit,2)
        (context as VoGroupLoanlist).getTotalValue(disburseamt,3)


        holder.cardView.setOnClickListener {
//            validate!!.SaveSharepreferenceString(MeetingSP.Institution, validate!!.returnStringValue(holder.tv_source.text.toString()))
            validate!!.SaveSharepreferenceInt(VoSpData.voLoanno, validate!!.returnIntegerValue(list.get(position).loanNo.toString()))
            var intent = Intent(context, VoGroupPrincipalDemandActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            context.startActivity(intent)
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val cardView=view.cardView
        val tv_source=view.tv_source
        val tv_value1=view.tv_value1
       // val tv_loans=view.tv_loans
        val tv_value2=view.tv_value2
        val tv_value3=view.tv_value3
        val img_edit=view.img_edit
    }

    fun setLabelText(view: ViewHolder) {

    }

}