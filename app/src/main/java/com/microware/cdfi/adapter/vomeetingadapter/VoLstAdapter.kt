package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.PopupWindow
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.SHGMeetingListActivity
import com.microware.cdfi.activity.vo.VOListActivity
import com.microware.cdfi.activity.vomeeting.VoCutOffMenuActivity
import com.microware.cdfi.activity.vomeeting.VoGenerateMeetingActivity
import com.microware.cdfi.activity.vomeeting.VoMeetingListActivity1
import com.microware.cdfi.activity.vomeeting.VoMeetingMenuActivity
import com.microware.cdfi.api.vomodel.VoMeetingModel
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import kotlinx.android.synthetic.main.vomeeting_item_dialog.view.*
import kotlinx.android.synthetic.main.vomeeting_list_item.view.*

class VoLstAdapter(val context: Context, val lstData: List<VoMeetingModel>, val cboType: Int) :
    RecyclerView.Adapter<VoLstAdapter.ViewHolder>() {

    var validate: Validate? = null
    var firstword: String? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        var view = LayoutInflater.from(context).inflate(R.layout.vomeeting_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return lstData.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (validate!!.returnLongValue(lstData[position].federation_code.toString()) > 0) {
            holder.tv_code.text =
                "" + lstData[position].federation_code
        } else {
            holder.tv_code.text = "..."
        }

        holder.tv_nam.text = validate!!.returnStringcapitalize(lstData[position].federation_name)

        if (validate!!.returnIntegerValue(lstData.get(position).activation_status.toString()) == 2) {
            //holder.imgactivationstatus.visibility=View.GONE
            if (validate!!.returnIntegerValue(lstData.get(position).status.toString()) == 1) {
                holder.tv_count.setBackgroundResource(R.drawable.item_countactive)
            }
        } else if (validate!!.returnIntegerValue(lstData.get(position).activation_status.toString()) == 3) {
            // holder.imgactivationstatus.visibility=View.VISIBLE

            if (validate!!.returnIntegerValue(lstData.get(position).status.toString()) == 1) {
                holder.tv_count.setBackgroundResource(R.drawable.item_countreject)
            }
        } else {
            holder.tv_count.setBackgroundResource(R.drawable.item_countpending)
        }

        if (validate!!.returnIntegerValue(lstData[position].status.toString()) == 0) {
            holder.tv_status.setBackgroundResource(R.color.red)
        } else if (validate!!.returnIntegerValue(lstData[position].is_edited.toString()) == 1) {
            holder.tv_status.setBackgroundResource(R.color.colorPrimary)
        } else {
            holder.tv_status.setBackgroundResource(R.color.khakigreen1)
        }

        val mtglist =
            (context as VoMeetingListActivity1).returnmeetingdata(lstData[position].federation_id)
        if (!mtglist.isNullOrEmpty()) {
            holder.tv_date.text = validate!!.convertDatetime(mtglist.get(0).mtgDate)
            holder.tv_id.text = mtglist.get(0).mtgNo.toString()
            holder.tv_guid.text = mtglist.get(0).mtgGuid
            holder.tv_mtgType.text = validate!!.returnStringValue(mtglist.get(0).mtgType.toString())
        } else {
            holder.tv_date.text = ""
            holder.tv_id.text = ""
            holder.tv_guid.text = ""
            holder.tv_mtgType.text = ""
        }

        val name = validate!!.returnStringValue(lstData.get(position).federation_name)
        if (!name.isNullOrEmpty()) {
            firstword = name.substring(0, 1).toUpperCase()
        }

        val mappedshgcount = (context).getMappedShgCount(lstData.get(position).guid)
        val mapped_vo_count = (context).getMappedVoCount(lstData.get(position).guid)
        if (cboType == 1 && mappedshgcount > 0) {
            holder.tv_count.text = mappedshgcount.toString()
        } else if (cboType == 2 && mapped_vo_count > 0) {
            holder.tv_count.text = mapped_vo_count.toString()
        } else {
            holder.tv_count.text = "0"
        }

        var islock = 0
        if (validate!!.returnIntegerValue(lstData.get(position).approve_status.toString()) == 1 && validate!!.returnIntegerValue(
                lstData.get(position).is_edited.toString()
            ) == 0
        ) {
            holder.imglock.visibility = View.VISIBLE
            islock = 1
        } else {
            holder.imglock.visibility = View.GONE
            islock = 0
        }

        holder.tv_firstword.text = firstword

        holder.optionBtn.setOnClickListener {
            var count = 0
            if (cboType == 1 && mappedshgcount > 0) {
                count = mappedshgcount
            } else if (cboType == 2 && mapped_vo_count > 0) {
                count = mapped_vo_count
            } else {
                count = 0
            }
            var popupwindow: PopupWindow = popupDisplay(
                lstData.get(position).federation_name!!,
                validate!!.returnLongValue(lstData.get(position).federation_code.toString()),
                validate!!.returnLongValue(lstData.get(position).federation_id.toString()),
                validate!!.returnIntegerValue(lstData.get(position).meeting_frequency.toString()),
                validate!!.returnIntegerValue(lstData.get(position).meeting_frequency_value.toString()),
                validate!!.returnIntegerValue(lstData.get(position).meeting_on.toString()),
                validate!!.returnIntegerValue(holder.tv_id.text.toString()),
                validate!!.Daybetweentime(holder.tv_date.text.toString()),
                islock,
                validate!!.returnLongValue(lstData.get(position).federation_formation_date.toString()),
                lstData.get(position).guid,
                validate!!.returnStringValue(holder.tv_guid.text.toString()),
                count,
                validate!!.returnIntegerValue(lstData.get(position).federation_type_code.toString()),
                /* validate!!.returnIntegerValue(lstData.get(position).tags.toString()),*/
                validate!!.returnIntegerValue(lstData.get(position).month_comp_saving.toString()),
                validate!!.returnIntegerValue(holder.tv_mtgType.text.toString()),
                validate!!.returnIntegerValue(lstData.get(position).is_voluntary_saving.toString())
            )
            val values = IntArray(2)
            holder.layout.getLocationInWindow(values)
            val positionOfIcon = values[1]
            val displayMetrics: DisplayMetrics = context.getResources().displayMetrics
            val height: Int = displayMetrics.heightPixels * 2 / 3
            if (positionOfIcon > height) {
                popupwindow.showAsDropDown(holder.layout, -20, -200)
            } else {
                popupwindow.showAsDropDown(holder.layout, -20, 0)
            }
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_status = view.tv_status

        //   var cardView = view.card_view
        var tv_firstword = view.tv_firstword
        var tv_code = view.tv_code
        var tv_nam = view.tv_nam
        var tv_date = view.tv_date
        var tv_count = view.tv_count
        var optionBtn = view.optionBtn
        var layout = view.layout
        var tv_id = view.tv_id
        var tv_guid = view.tv_guid
        var tv_mtgType = view.tv_mtgType
        var imglock = view.imglock

    }

    fun popupDisplay(
        federationName: String,
        federationcode: Long,
        federation_id: Long,
        meetingfrequency: Int,
        meetingfrequencyvalue: Int,
        meeting_on: Int,
        mtgno: Int,
        mtgdate: Long,
        islock: Int,
        formationdate: Long,
        guid: String,
        mtgguid: String,
        count: Int,
        federationtype: Int,
        /*tag: Int,*/
        month_comp_saving: Int,
        mtgType: Int,
        voluntary_saving: Int
    ): PopupWindow {
        val popupWindow = PopupWindow(context)
        // inflate your layout or dynamically add view
        val inflater =
            context.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.vomeeting_item_dialog, null, false)
        popupWindow.isFocusable = true
        popupWindow.width = WindowManager.LayoutParams.WRAP_CONTENT
        popupWindow.height = WindowManager.LayoutParams.WRAP_CONTENT
        popupWindow.setBackgroundDrawable(ColorDrawable(context.resources.getColor(R.color.white)))

        view.tvgeneratemeeting.text = LabelSet.getText(
            "generate_meeting",
            R.string.generate_meeting
        )

        view.tvopen_meeting.text = LabelSet.getText(
            "open_meeting",
            R.string.open_meeting
        )
        view.tv_sync.text = LabelSet.getText(
            "download_meeting",
            R.string.download_meeting
        )

        fun setBackGroundColur(iScreenFlag: Int) {
            //genratemeeting
            if (iScreenFlag == 1) {
                view.tbl_generatemeeting.background =
                    context.resources.getDrawable(R.drawable.dialog_selected_bg)
            } else {
                view.tbl_generatemeeting.setBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        R.color.transperent
                    )
                )
            }
            view.iVshg.setColorFilter(
                ContextCompat.getColor(
                    context,
                    if (iScreenFlag == 1) R.color.colorPrimary1 else R.color.dialog_text_clr
                )
            )
            view.tvgeneratemeeting.setTextColor(
                ContextCompat.getColor(
                    context,
                    if (iScreenFlag == 1) R.color.colorPrimary1 else R.color.dialog_text_clr
                )
            )

            //openmeeting
            if (iScreenFlag == 2) {
                view.tbl_open_meeting.background =
                    context.resources.getDrawable(R.drawable.dialog_selected_bg)
            } else {
                view.tbl_open_meeting.setBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        R.color.transperent
                    )
                )
            }
            view.iVshgsummary.setColorFilter(
                ContextCompat.getColor(
                    context,
                    if (iScreenFlag == 2) R.color.colorPrimary1 else R.color.dialog_text_clr
                )
            )
            view.tvopen_meeting.setTextColor(
                ContextCompat.getColor(
                    context,
                    if (iScreenFlag == 2) R.color.colorPrimary1 else R.color.dialog_text_clr
                )
            )

            //sync
            if (iScreenFlag == 3) {
                view.tbl_sync.background =
                    context.resources.getDrawable(R.drawable.dialog_selected_bg)
            } else {
                view.tbl_sync.setBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        R.color.transperent
                    )
                )
            }
            view.iVsync.setColorFilter(
                ContextCompat.getColor(
                    context,
                    if (iScreenFlag == 3) R.color.colorPrimary1 else R.color.dialog_text_clr
                )
            )
            view.tv_sync.setTextColor(
                ContextCompat.getColor(
                    context,
                    if (iScreenFlag == 3) R.color.colorPrimary1 else R.color.dialog_text_clr
                )
            )
        }

        view.tbl_generatemeeting.setOnClickListener {
            setBackGroundColur(1)
            validate!!.SaveSharepreferenceString(VoSpData.voShgName, federationName)
            validate!!.SaveSharepreferenceLong(VoSpData.voShgcode, federationcode)
            validate!!.SaveSharepreferenceLong(VoSpData.voshgid, federation_id)
            validate!!.SaveSharepreferenceInt(VoSpData.vomeetingfrequency, meetingfrequency)
            validate!!.SaveSharepreferenceInt(
                VoSpData.vomeetingfrequencyvalue,
                meetingfrequencyvalue
            )
            validate!!.SaveSharepreferenceInt(VoSpData.vomeeting_on, meeting_on)
            validate!!.SaveSharepreferenceInt(VoSpData.VoLastMeetingNumber, mtgno)
            validate!!.SaveSharepreferenceInt(VoSpData.vomaxmeetingnumber, mtgno)
            validate!!.SaveSharepreferenceLong(VoSpData.VoLastMeetingDate, mtgdate)
            validate!!.SaveSharepreferenceInt(VoSpData.voLockRecord, islock)
            validate!!.SaveSharepreferenceInt(VoSpData.voMemberCount, count)
            validate!!.SaveSharepreferenceLong(VoSpData.voFormation_dt, formationdate)
            validate!!.SaveSharepreferenceString(VoSpData.voSHGGUID, guid)
            validate!!.SaveSharepreferenceString(VoSpData.vomtg_guid, mtgguid)
            validate!!.SaveSharepreferenceInt(VoSpData.voSHGType, federationtype)
            /* validate!!.SaveSharepreferenceInt(VoSpData.voTag, 1)*/
            validate!!.SaveSharepreferenceInt(VoSpData.voMonth_comp_saving, month_comp_saving)
            validate!!.SaveSharepreferenceInt(VoSpData.voisVoluntarySaving, voluntary_saving)

            var mtg_count =
                (context as VoMeetingListActivity1).getMeetingCountByCboId(federation_id)
            val count = (context).getEcMembercount(guid)
            val mappedshgcount = (context).getMappedShgCount(guid)
            if (count > 0 && mappedshgcount > 0) {
                if (mtg_count > 0) {
                    var mtgstatus = context.returnmeetingstatus(
                        federation_id,
                        mtgno
                    )
                    if (mtgstatus.equals("C", ignoreCase = true)) {
                        val i = Intent(context, VoGenerateMeetingActivity::class.java)
                        i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                        context.startActivity(i)
                    } else {
                        validate!!.CustomAlertVO(
                            LabelSet.getText("pleaseclosemeeting", R.string.pleaseclosemeeting),
                            context
                        )
                    }
                } else {
                    val i = Intent(context, VoGenerateMeetingActivity::class.java)
                    i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                    context.startActivity(i)
                }
            } else {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "download_mapping_data",
                        R.string.download_mapping_data
                    ),
                    context
                )

            }
        }

        view.tbl_open_meeting.setOnClickListener {
            setBackGroundColur(2)
            validate!!.SaveSharepreferenceString(VoSpData.voShgName, federationName)
            validate!!.SaveSharepreferenceLong(VoSpData.voShgcode, federationcode)
            validate!!.SaveSharepreferenceLong(VoSpData.voshgid, federation_id)
            validate!!.SaveSharepreferenceInt(VoSpData.vomeetingfrequency, meetingfrequency)
            validate!!.SaveSharepreferenceInt(
                VoSpData.vomeetingfrequencyvalue,
                meetingfrequencyvalue
            )
            validate!!.SaveSharepreferenceInt(VoSpData.vocurrentmeetingnumber,mtgno)
            validate!!.SaveSharepreferenceInt(VoSpData.vomeeting_on, meeting_on)
            validate!!.SaveSharepreferenceInt(VoSpData.VoLastMeetingNumber, mtgno)
            validate!!.SaveSharepreferenceInt(VoSpData.vomaxmeetingnumber, mtgno)
            validate!!.SaveSharepreferenceLong(VoSpData.VoLastMeetingDate, mtgdate)
            validate!!.SaveSharepreferenceInt(VoSpData.voLockRecord, islock)
            validate!!.SaveSharepreferenceInt(VoSpData.voMemberCount, count)
            validate!!.SaveSharepreferenceLong(VoSpData.voFormation_dt, formationdate)
            validate!!.SaveSharepreferenceString(VoSpData.voSHGGUID, guid)
            validate!!.SaveSharepreferenceString(VoSpData.vomtg_guid, mtgguid)
            validate!!.SaveSharepreferenceInt(VoSpData.voSHGType, federationtype)
            /* validate!!.SaveSharepreferenceInt(VoSpData.voTag, 1)*/
            validate!!.SaveSharepreferenceInt(VoSpData.voMonth_comp_saving, month_comp_saving)
            validate!!.SaveSharepreferenceInt(VoSpData.voisVoluntarySaving, voluntary_saving)
            var mtg_count =
                (context as VoMeetingListActivity1).getMeetingCountByCboId(federation_id)
            if (mtg_count > 0) {
                context.openmeeting(federation_id, mtgno)
                if (mtgType == 0) {
                    val i = Intent(context, VoMeetingMenuActivity::class.java)
                    i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                    context.startActivity(i)
                } else if (mtgType == 11 || mtgType == 12) {
                    val i = Intent(context, VoCutOffMenuActivity::class.java)
                    i.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                    context.startActivity(i)
                }
            } else {
                validate!!.CustomAlertVO(
                    LabelSet.getText(
                        "meeting_not_exists_vo",
                        R.string.meeting_not_exists_vo
                    ), context
                )

            }


        }

        view.tbl_sync.setOnClickListener {
            popupWindow.dismiss()
            setBackGroundColur(3)
            validate!!.SaveSharepreferenceLong(VoSpData.voshgid, federation_id)
            (context as VoMeetingListActivity1).importVoMeetinglist()
        }

        popupWindow.contentView = view

        return popupWindow
    }

}