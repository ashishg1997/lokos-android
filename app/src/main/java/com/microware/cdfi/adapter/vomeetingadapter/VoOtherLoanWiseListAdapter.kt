package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import kotlinx.android.synthetic.main.vo_to_others_loan_wise_due_row.view.*
import java.math.BigInteger
import java.text.Format
import java.text.NumberFormat
import java.util.*

class VoOtherLoanWiseListAdapter(var context: Context) :
    RecyclerView.Adapter<VoOtherLoanWiseListAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context)
            .inflate(R.layout.vo_to_others_loan_wise_due_row, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return 5
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_loan_no1 = view.tv_loan_no1
        var tv_outstanding1 = view.tv_outstanding1
        var tv_original_loan_amount1 = view.tv_original_loan_amount1
        var tv_current_due1 = view.tv_current_due1
        var total_due1 = view.total_due1

    }

    fun getIndianRupee(value: String?): String? {
        val format: Format = NumberFormat.getCurrencyInstance(Locale("en", "in"))
        return format.format(BigInteger(value)).replace("\\.00".toRegex(), "")

    }

    fun fillAdapterData(view: ViewHolder) {
        val amount1 = 10000
        val amount2 = 50000
        view.tv_loan_no1.text = 314.toString()
        view.tv_outstanding1.text = getIndianRupee(amount1.toString())
        view.tv_original_loan_amount1.text = getIndianRupee(amount1.toString())
        view.tv_current_due1.text = getIndianRupee(amount2.toString())
        view.total_due1.text = getIndianRupee(amount2.toString())
    }
}