package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.entity.MstCOAEntity
import kotlinx.android.synthetic.main.vo_other_payment_row_item.view.*

class VoOtherPaymentAdapter(var context: Context,var list: List<MstCOAEntity>?) :
    RecyclerView.Adapter<VoOtherPaymentAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.vo_other_payment_row_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvFunds.text= list?.get(position)!!.description
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var cardView = view.cardView
        var ivdifBank = view.ivdifBank
        var tvFunds = view.tvFunds
        var tvamount = view.tvamount
        var ivEdit = view.ivEdit
        var ivActualPayLocation = view.ivActualPayLocation

    }
}