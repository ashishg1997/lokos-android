package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.VoCutOffPrincipalDemandActivity
import com.microware.cdfi.entity.voentity.VoMemLoanScheduleEntity
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.vo_principal_demand_detail_item.view.*

class VoPrincipalDemandAdapter(
    var context: Context,
    val list: List<VoMemLoanScheduleEntity>?
) :
    RecyclerView.Adapter<VoPrincipalDemandAdapter.ViewHolder>() {
    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        var view = LayoutInflater.from(context)
            .inflate(R.layout.vo_principal_demand_detail_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        setLabelText(holder)
        holder.tv_installment_no_value.text = list!!.get(position).installmentNo.toString()
        holder.tv_installment_date_value.text =
            validate!!.convertDatetime(list.get(position).installmentDate)
        holder.tv_principal_demand.text = list.get(position).principalDemand.toString()
        holder.et_principal_demand.setText(list.get(position).principalDemand.toString())
        holder.tv_loanos_value.text = list.get(position).loanOs.toString()

            (context as VoCutOffPrincipalDemandActivity).getTotalValue(list.get(position).principalDemand!!,1)
            (context as VoCutOffPrincipalDemandActivity).getTotalValue(list.get(position).principalDemand!!,2)

        holder.et_principal_demand.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                (context as VoCutOffPrincipalDemandActivity).getTotalValue()
            }

        })

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var cardView = view.cardView
        var et_principal_demand = view.et_principal_demand
        var tv_principal_demand = view.tv_principal_demand
        var tv_installment_date_value = view.tv_installment_date_value
        var tv_installment_no_value = view.tv_installment_no_value
        var tv_loanos_value = view.tv_loanos_value

    }

    fun setLabelText(view: ViewHolder) {

    }
}