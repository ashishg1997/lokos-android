package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.SHGtoVOLoanRepayment
import com.microware.cdfi.api.vomodel.VoLoanRepaymentListModel
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.vo_loanrepayment_detail_item.view.*

class VoRepaymentDetailAdapter(
    var context: Context,
    val list: List<VoLoanRepaymentListModel>
) :
    RecyclerView.Adapter<VoRepaymentDetailAdapter.ViewHolder>() {
    var validate: Validate? = null

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val cardView = view.cardView
        val tv_value1 = view.tv_value1
        val tv_loans = view.tv_loans
        val tv_value2 = view.tv_value2
        val tv_value3 = view.tv_value3
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
       validate = Validate(context)
        val view = LayoutInflater.from(context).inflate(R.layout.vo_loanrepayment_detail_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder:ViewHolder, position: Int) {

        var loanlist = (context as SHGtoVOLoanRepayment).returnlist(
            validate!!.returnLongValue(
                list.get(position).memId.toString()
            )
        )
        if (!loanlist.isNullOrEmpty()) {
            holder.tv_loans.text =
                validate!!.returnIntegerValue(loanlist.get(0).loanNo.toString()).toString()
            val totpayble =
                validate!!.returnIntegerValue(loanlist.get(0).principal_demand_ob.toString()) + validate!!.returnIntegerValue(
                    loanlist.get(0).principal_demand.toString()
                )
            val totintrest =
                validate!!.returnIntegerValue(loanlist.get(0).int_accrued_op.toString()) + validate!!.returnIntegerValue(
                    loanlist.get(0).int_accrued.toString()
                )
            if ((totintrest + totpayble) < 0) {
                holder.tv_value1.text = "0"
            } else {
                holder.tv_value1.text = (totintrest + totpayble).toString()
            }

            holder.tv_value2.text =
                (validate!!.returnIntegerValue(loanlist.get(0).loan_paid.toString()) + validate!!.returnIntegerValue(
                    loanlist.get(0).loan_paid_int.toString()
                )).toString()

            if ( validate!!.returnIntegerValue(holder.tv_loans.text.toString())>0) {
                holder.tv_loans.background = context.getDrawable(R.drawable.item_countred1)
                holder.tv_loans.setTextColor(context.getColor(R.color.colorPrimary1))
            }else{
                holder.tv_loans.background = context.getDrawable(R.drawable.item_count)
                holder.tv_loans.setTextColor(context.getColor(R.color.colorAccent))

            }
            if (totintrest + totpayble > validate!!.returnIntegerValue(holder.tv_value2.text.toString())) {
                holder.tv_value2.setTextColor(context.getColor(R.color.meetingtabloancolor))
                holder.tv_value3.setTextColor(context.getColor(R.color.meetingtabloancolor))
            } else if (totintrest + totpayble < validate!!.returnIntegerValue(holder.tv_value2.text.toString())) {
                holder.tv_value2.setTextColor(context.getColor(R.color.Lightgreen))
                holder.tv_value3.setTextColor(context.getColor(R.color.colorAccent))

            } else {
                holder.tv_value2.setTextColor(context.getColor(R.color.colorAccent))
                holder.tv_value3.setTextColor(context.getColor(R.color.colorAccent))

            }
        } else {
            holder.tv_loans.text = "0"
            holder.tv_value1.text = "0"
            holder.tv_value2.text = "0"
        }

        var nextdemand = (context as SHGtoVOLoanRepayment).getnextdemand(
            validate!!.returnLongValue(
                list.get(position).memId.toString()
            ), validate!!.returnIntegerValue(list.get(position).loanNo.toString())
        )
        holder.tv_value3.text = nextdemand.toString()

    }

    override fun getItemCount(): Int {
        return list.size
    }


}