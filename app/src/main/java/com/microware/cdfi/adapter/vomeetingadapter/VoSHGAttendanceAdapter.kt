package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.entity.voentity.VoMtgDetEntity
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import kotlinx.android.synthetic.main.attendences_detail_item.view.*
import kotlinx.android.synthetic.main.shg_attendance_items.view.*
import kotlinx.android.synthetic.main.shg_attendance_items.view.btn_absent
import kotlinx.android.synthetic.main.shg_attendance_items.view.btn_present
import kotlinx.android.synthetic.main.shg_attendance_items.view.tblpresent_absent

class VoSHGAttendanceAdapter(var context: Context, var list: List<VoMtgDetEntity>) :
    RecyclerView.Adapter<VoSHGAttendanceAdapter.ViewHolder>() {
    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        val view = LayoutInflater.from(context).inflate(R.layout.shg_attendance_items, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.tv_shg_Name.text = list.get(position).childCboName.toString()
        holder.tv_sr_no.text = "" + (position + 1)

        holder.et_memId.setText(list.get(position).memId.toString())

        if(validate!!.RetriveSharepreferenceInt(VoSpData.voMeetingType)==0){
            holder.tblpresent_absent.visibility = View.VISIBLE
            holder.et_attendance.visibility = View.GONE

            if (validate!!.returnIntegerValue(list.get(position).attendance.toString()) == 1) {
                holder.btn_absent.setBackgroundDrawable(context.getDrawable(R.drawable.absentlight))
                holder.btn_present.setBackgroundDrawable(context.getDrawable(R.drawable.present_count))
            }
            else if (validate!!.returnIntegerValue(list.get(position).attendance.toString()) == 2) {
                holder.btn_absent.setBackgroundDrawable(context.getDrawable(R.drawable.absent_count))
                holder.btn_present.setBackgroundDrawable(context.getDrawable(R.drawable.presentlight))
            }
            else {
                holder.et_attendance.setText("1")
                holder.btn_absent.setBackgroundDrawable(context.getDrawable(R.drawable.absentlight))
                holder.btn_present.setBackgroundDrawable(context.getDrawable(R.drawable.present_count))
            }

        }
        /*else if(validate!!.RetriveSharepreferenceInt(VoSpData.voMeetingType)==11 || validate!!.RetriveSharepreferenceInt(VoSpData.voMeetingType)==12){
            holder.tblpresent_absent.visibility = View.GONE
            holder.et_attendance.visibility = View.VISIBLE
            holder.et_attendance.setText(list.get(position).attendance!!)
        }*/

        holder.btn_present.setOnClickListener(View.OnClickListener {
            if(validate!!.RetriveSharepreferenceInt(VoSpData.voMeetingType)==0) {
                holder.et_attendance.setText("1")
                holder.btn_absent.setBackgroundResource(R.drawable.absentlight)
                holder.btn_present.setBackgroundResource(R.drawable.present_count)
            }
        })

        holder.btn_absent.setOnClickListener(View.OnClickListener {
            if(validate!!.RetriveSharepreferenceInt(VoSpData.voMeetingType)==0) {
                holder.et_attendance.setText("2")
                holder.btn_absent.setBackgroundResource(R.drawable.absent_count)
                holder.btn_present.setBackgroundResource(R.drawable.presentlight)

            }
        })


    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_shg_Name = view.tv_shg_Name
        var tv_sr_no = view.tv_sr_no
        var btn_present = view.btn_present
        var btn_absent = view.btn_absent
        var  et_attendance = view.et_attendance
        var tblpresent_absent= view.tblpresent_absent
        var et_GroupMeetingCode = view.et_GroupMeetingCode
        var et_memId = view.et_memId
    }

}