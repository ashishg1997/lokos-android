package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.VoSettlementActivity
import com.microware.cdfi.entity.voentity.VoMtgDetEntity
import com.microware.cdfi.utility.VoSpData
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.vo_settlement_detail_item.view.*

class VoSettlementDetailAdapter(
    var context: Context,
    var list: List<VoMtgDetEntity>
) :
    RecyclerView.Adapter<VoSettlementDetailAdapter.ViewHolder>() {
    var validate:Validate?=null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate= Validate(context)
        var view = LayoutInflater.from(context).inflate(R.layout.vo_settlement_detail_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_shgName.text = list.get(position).childCboName.toString()
        holder.tv_srno.text = (position + 1).toString()
        var tot=validate!!.returnIntegerValue(list.get(position).savCompCb.toString())+validate!!.returnIntegerValue(list.get(position).savVolCb.toString())
        holder.tv_totsaved.text = tot.toString()
        /*replace with save comp withdrawal = savVolWithdrawal*/
        holder.tv_withdrawl.text = validate!!.returnIntegerValue(list.get(position).savVolWithdrawal.toString()).toString()
        setLabelText(holder)

        validate!!.SaveSharepreferenceString(VoSpData.voMemberName,list.get(position).childCboName.toString())

        holder.tv_withdrawl.setOnClickListener(View.OnClickListener {
            validate!!.SaveSharepreferenceLong(VoSpData.voMemberId,validate!!.returnLongValue(list.get(position).memId.toString()))
            var inetnt = Intent(context, VoSettlementActivity::class.java)
            context.startActivity(inetnt)
        })


    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_srno = view.tv_srno
        val tv_shgName = view.tv_shgName
        val tv_totsaved = view.tv_totsaved
        val tv_withdrawl = view.tv_withdrawl
    }

    fun setLabelText(view: ViewHolder) {

    }


}