package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.VOtoSHGLoanRequest
import com.microware.cdfi.activity.vomeeting.VOtoSHGLoanRequestSummary
import com.microware.cdfi.entity.voentity.VoLoanApplicationEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import kotlinx.android.synthetic.main.vo_shg_loan_request_summary_row_item.view.*


class VoShgLoanRequestSummaryAdapter(var context: Context , var list: List<VoLoanApplicationEntity>) :
    RecyclerView.Adapter<VoShgLoanRequestSummaryAdapter.ViewHolder>() {

    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate = Validate(context)
        var view = LayoutInflater.from(context).inflate(R.layout.vo_shg_loan_request_summary_row_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val fundType = (context as VOtoSHGLoanRequestSummary).getRecepitType(validate!!.RetriveSharepreferenceInt(VoSpData.voAuid))
        if (!fundType.isNullOrEmpty()){
            holder.fund_type.text = fundType
        }

        holder.loan_amount.text = validate!!.returnStringValue(list[position].amtSanction.toString())
        holder.priority_no.text = validate!!.returnStringValue(list[position].loanRequestPriority.toString())
        holder.request_date.text = validate!!.convertDatetime(list.get(position).requestDate)
        holder.request_valid_date.text = validate!!.convertDatetime(list.get(position).approvalDate)

        holder.shg_name.text = (context as VOtoSHGLoanRequestSummary).setMember(
            list.get(position).memId
        )

        holder.cardView.setOnClickListener {
            validate!!.SaveSharepreferenceLong(VoSpData.voLoanappid, validate!!.returnLongValue(list.get(position).loanApplicationId.toString()))

            var intent = Intent(context, VOtoSHGLoanRequest::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            context.startActivity(intent)
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var shg_name = view.shg_name
        var loan_amount = view.loan_amount
        var cardView = view.cardView
        var request_date = view.request_date
        var fund_type = view.fund_type
        var request_valid_date=view.request_valid_date
        var priority_no=view.priority_no

    }


}