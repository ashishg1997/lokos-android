package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.*
import com.microware.cdfi.api.vomodel.VoLoanListModel
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import kotlinx.android.synthetic.main.vo_shg_loan_summary_row_item.view.*

class VoShgLoanSummaryAdapter(var context: Context, var list: List<VoLoanListModel>) :
    RecyclerView.Adapter<VoShgLoanSummaryAdapter.ViewHolder>() {
    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context)
            .inflate(R.layout.vo_shg_loan_summary_row_item, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.ivEdit.isEnabled =
            (validate!!.returnIntegerValue(list.get(position).amtDemand.toString())) != 0

        holder.sr_no.text =  (position+1).toString()
      //  holder.members_name.setText(list.get(position).childCboName.toString())
        holder.loan_request.text =  validate!!.returnIntegerValue(list.get(position).amtSanction.toString()).toString()
        holder.loan_payment.text =  validate!!.returnIntegerValue(list.get(position).amtDisbursed.toString()).toString()

       /* var amountOutstanding = (context as VOtoSHGLoanSummary).gettotaloutstanding(
            list.get(position).mtgNo!!,
            list.get(position).cboId!!,
            list.get(position).memId!!
        )

        if (amountOutstanding>0) {

            *//*holder.loan_outstanding.setText(
                amountOutstanding.toString()
            )*//*
            holder.loan_outstanding.setText("0")
        } else{
            holder.loan_outstanding.setText("0")
        }*/


      //  if (list.size-1 == position) {
            (context as VOtoSHGLoanSummary).getTotalValue(0,validate!!.returnIntegerValue(list.get(position).amtSanction.toString()),
                    validate!!.returnIntegerValue(list.get(position).amtDisbursed.toString()))
      //  }


        holder.ivEdit.setOnClickListener {
            validate!!.SaveSharepreferenceLong(VoSpData.voShgMemID, list.get(position).memId!!)

            validate!!.SaveSharepreferenceLong(VoSpData.voLoanappid, validate!!.returnLongValue(list.get(position).loanApplicationId.toString()))

            validate!!.SaveSharepreferenceString(VoSpData.voMemberName, list.get(position).childCboName!!)

            var loanNo = (context as VOtoSHGLoanSummary).getLoanno(validate!!.returnLongValue(list.get(position).loanApplicationId.toString()))

            validate!!.SaveSharepreferenceInt(VoSpData.voLoanno, loanNo)

            var intent = Intent(context, VOtoSHGLoanPayment::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            context.startActivity(intent)

        }

        holder.loan_request.setOnClickListener {

            var loanNo = (context as VOtoSHGLoanSummary).getLoanno(validate!!.returnLongValue(list.get(position).loanApplicationId.toString()))

            validate!!.SaveSharepreferenceInt(VoSpData.voLoanno, loanNo)
            validate!!.SaveSharepreferenceString(VoSpData.voMemberName, list.get(position).childCboName!!)
            validate!!.SaveSharepreferenceLong(VoSpData.voLoanappid, validate!!.returnLongValue(list.get(position).loanApplicationId.toString()))

            var intent = Intent(context, VOtoSHGLoanRequestSummary::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            context.startActivity(intent)

        }

        holder.loan_payment.setOnClickListener {
            var loanNo = (context as VOtoSHGLoanSummary).getLoanno(validate!!.returnLongValue(list.get(position).loanApplicationId.toString()))

            validate!!.SaveSharepreferenceInt(VoSpData.voLoanno, loanNo)
            validate!!.SaveSharepreferenceString(VoSpData.voMemberName, list.get(position).childCboName!!)

            var intent = Intent(context, VoCutOffPrincipalDemandActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            context.startActivity(intent)

        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var sr_no = view.sr_no
        var loan_outstanding = view.loan_outstanding
        var loan_request = view.loan_request
        var loan_payment = view.loan_payment
        var ivEdit = view.ivEdit


    }

}