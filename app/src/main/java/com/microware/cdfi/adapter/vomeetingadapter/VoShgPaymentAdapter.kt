package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.VOtoSHGLoanSummary
import com.microware.cdfi.entity.MstCOAEntity
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import kotlinx.android.synthetic.main.vo_shg_payment_row_item.view.*

class VoShgPaymentAdapter(var context: Context,var list: List<MstCOAEntity>?) :
    RecyclerView.Adapter<VoShgPaymentAdapter.ViewHolder>() {
    var validate: Validate? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.vo_shg_payment_row_item, parent, false)
        validate = Validate(context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvFunds.text=list!![position].description

        holder.tvFunds.setOnClickListener {
            validate!!.SaveSharepreferenceInt(VoSpData.voAuid,list!![position].uid)
            val intent = Intent(context,VOtoSHGLoanSummary::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            context.startActivity(intent)
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var cardView = view.cardView
        var ivdifBank = view.ivdifBank
        var tvFunds = view.tvFunds
        var tvamount = view.tvamount
        var ivEdit = view.ivEdit
        var ivActualPayLocation = view.ivActualPayLocation

    }

}