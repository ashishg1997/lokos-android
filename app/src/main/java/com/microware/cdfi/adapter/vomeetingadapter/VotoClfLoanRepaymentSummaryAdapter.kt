package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.VOLoanRepaymentSummary
import com.microware.cdfi.activity.vomeeting.VOLoanwisedustatus
import com.microware.cdfi.entity.voentity.VoGroupLoanTxnEntity
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import kotlinx.android.synthetic.main.vo_loan_repayment_summary_vo_to_clf_row_item.view.*

class VotoClfLoanRepaymentSummaryAdapter(var context: Context,val listData : List<VoGroupLoanTxnEntity>) :
    RecyclerView.Adapter<VotoClfLoanRepaymentSummaryAdapter.ViewHolder>() {

    var validate: Validate?=null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        validate=Validate(context)
        var view = LayoutInflater.from(context).inflate(R.layout.vo_loan_repayment_summary_vo_to_clf_row_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listData.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val totpayble=validate!!.returnIntegerValue(listData.get(position).principalDemandOb.toString())+validate!!.returnIntegerValue(listData.get(position).principalDemand.toString())
        val totintrest=validate!!.returnIntegerValue(listData.get(position).intAccruedOp.toString())+validate!!.returnIntegerValue(listData.get(position).intAccrued.toString())
        holder.tvTodayPayableAmt.text = (totintrest+totpayble).toString()
        holder.tvPaidAmt.text = (validate!!.returnIntegerValue(listData.get(position).loanPaid.toString())+validate!!.returnIntegerValue(listData.get(position).loanPaidInt.toString())).toString()
        holder.tvNxtPayableAmt.text = "0"
        holder.tvLoanSource.text = (context as VOLoanRepaymentSummary).returnloanlist(validate!!.returnIntegerValue(listData.get(position).loanNo.toString()))
        (context as VOLoanRepaymentSummary).getTotalValue(totintrest+totpayble,1)
        (context as VOLoanRepaymentSummary).getTotalValue((validate!!.returnIntegerValue(listData.get(position).loanPaid.toString())+validate!!.returnIntegerValue(listData.get(position).loanPaidInt.toString())),2)

        holder.cardView.setOnClickListener {
            validate!!.SaveSharepreferenceInt(VoSpData.voLoanno, validate!!.returnIntegerValue(listData.get(position).loanNo.toString()))
            var intent = Intent(context, VOLoanwisedustatus::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            context.startActivity(intent)
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var cardView = view.cardView
        var tvLoanSource = view.tvLoanSource
        var tvTodayPayableAmt = view.tvTodayPayableAmt
        var tvPaidAmt = view.tvPaidAmt
        var tvNxtPayableAmt = view.tvNxtPayableAmt
        var img_edit = view.img_edit


    }

}