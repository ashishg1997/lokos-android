package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import kotlinx.android.synthetic.main.vo_loan_repayment_vo_to_others_row_item.view.*
import java.math.BigInteger
import java.text.Format
import java.text.NumberFormat
import java.util.*

class VotoOthersLoanRepaymentAdapter(var context: Context) :
    RecyclerView.Adapter<VotoOthersLoanRepaymentAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.vo_loan_repayment_vo_to_others_row_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return 3
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        fillAdapterData(holder)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var loan_no = view.loan_no
        var outstanding = view.outstanding
        var principal_demand = view.principal_demand
//        var current = view.current
//        var arrear = view.arrear
        var intrest_demand = view.intrest_demand
//        var arrear_1 = view.arrear_1
//        var current_1 = view.current_1
        var total = view.total
        var pay=view.pay
        var intrest=view.interest

    }

    fun getIndianRupee(value: String?): String? {
        val format: Format = NumberFormat.getCurrencyInstance(Locale("en", "in"))
        return format.format(BigInteger(value)).replace("\\.00".toRegex(), "")

    }

    fun fillAdapterData(view: ViewHolder) {
        val amount1=5000
        val amount2=1000

        view.loan_no.text= "134"
        view.outstanding.text=getIndianRupee("10000")
        view.principal_demand.text=getIndianRupee(amount1.toString())
//        view.current.text=getIndianRupee(amount1.toString())
//        view.arrear.text=getIndianRupee(amount1.toString())
        view.intrest_demand.text=getIndianRupee(amount1.toString())
//        view.arrear_1.text=getIndianRupee(amount1.toString())
//        view.current_1.text=getIndianRupee(amount1.toString())
        view.total.text=getIndianRupee(amount1.toString())
        view.pay.text=getIndianRupee(amount1.toString())
        view.intrest.text=getIndianRupee(amount2.toString())


    }
}