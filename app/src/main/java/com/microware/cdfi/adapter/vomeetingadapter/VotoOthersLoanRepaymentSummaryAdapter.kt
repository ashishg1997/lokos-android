package com.microware.cdfi.adapter.vomeetingadapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import kotlinx.android.synthetic.main.vo_loan_repayment_summary_vo_to_others_row_item.view.*
import java.math.BigInteger
import java.text.Format
import java.text.NumberFormat
import java.util.*

class VotoOthersLoanRepaymentSummaryAdapter(var context: Context) :
    RecyclerView.Adapter<VotoOthersLoanRepaymentSummaryAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.vo_loan_repayment_summary_vo_to_others_row_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return 2
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        fillAdapterData(holder)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var loan_source = view.loan_source
        var today_payable = view.today_payable
        var paid = view.paid
        var payable = view.payable
        var open = view.open


    }

    fun getIndianRupee(value: String?): String? {
        val format: Format = NumberFormat.getCurrencyInstance(Locale("en", "in"))
        return format.format(BigInteger(value)).replace("\\.00".toRegex(), "")

    }
   val list= arrayListOf<Int>(134,10000,10)
    var nf: NumberFormat = NumberFormat.getInstance(Locale.UK)
    fun fillAdapterData(view: ViewHolder) {
      // var  mynumber=nf.parse("134");
        view.loan_source.text= "Bank"
        view.today_payable.text=getIndianRupee("30000")
        view.paid.text=getIndianRupee("40000")
        view.payable.text=getIndianRupee("40000")



    }
}