package com.microware.cdfi.api.AdharModels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CertificateData {
    UidData UidData;

    public com.microware.cdfi.api.AdharModels.UidData getUidData() {
        return UidData;
    }

    public void setUidData(com.microware.cdfi.api.AdharModels.UidData uidData) {
        UidData = uidData;
    }
}
