package com.microware.cdfi.api.AdharModels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KeyInfo {
    X509Data X509Data;

    public com.microware.cdfi.api.AdharModels.X509Data getX509Data() {
        return X509Data;
    }

    public void setX509Data(com.microware.cdfi.api.AdharModels.X509Data x509Data) {
        X509Data = x509Data;
    }
}
