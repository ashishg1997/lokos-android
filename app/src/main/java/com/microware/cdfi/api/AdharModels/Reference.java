package com.microware.cdfi.api.AdharModels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Reference {
    DigestMethod DigestMethod;
    String DigestValue;
    //Transforms Transforms;
    String URI;

    public com.microware.cdfi.api.AdharModels.DigestMethod getDigestMethod() {
        return DigestMethod;
    }

    public void setDigestMethod(com.microware.cdfi.api.AdharModels.DigestMethod digestMethod) {
        DigestMethod = digestMethod;
    }

    public String getDigestValue() {
        return DigestValue;
    }

    public void setDigestValue(String digestValue) {
        DigestValue = digestValue;
    }


    public String getURI() {
        return URI;
    }

    public void setURI(String URI) {
        this.URI = URI;
    }
}
