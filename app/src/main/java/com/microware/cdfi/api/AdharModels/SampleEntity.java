package com.microware.cdfi.api.AdharModels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;


public class SampleEntity implements Serializable {
    CertificateData CertificateData;
    Signature Signature;

    public com.microware.cdfi.api.AdharModels.CertificateData getCertificateData() {
        return CertificateData;
    }

    public void setCertificateData(com.microware.cdfi.api.AdharModels.CertificateData certificateData) {
        CertificateData = certificateData;
    }

    public com.microware.cdfi.api.AdharModels.Signature getSignature() {
        return Signature;
    }

    public void setSignature(com.microware.cdfi.api.AdharModels.Signature signature) {
        Signature = signature;
    }
}
