package com.microware.cdfi.api.AdharModels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Signature {
    KeyInfo KeyInfo;
    String SignatureValue;
    SignedInfo SignedInfo;



    public String getSignatureValue() {
        return SignatureValue;
    }

    public void setSignatureValue(String signatureValue) {
        SignatureValue = signatureValue;
    }

    public com.microware.cdfi.api.AdharModels.KeyInfo getKeyInfo() {
        return KeyInfo;
    }

    public void setKeyInfo(com.microware.cdfi.api.AdharModels.KeyInfo keyInfo) {
        KeyInfo = keyInfo;
    }

    public com.microware.cdfi.api.AdharModels.SignedInfo getSignedInfo() {
        return SignedInfo;
    }

    public void setSignedInfo(com.microware.cdfi.api.AdharModels.SignedInfo signedInfo) {
        SignedInfo = signedInfo;
    }
}
