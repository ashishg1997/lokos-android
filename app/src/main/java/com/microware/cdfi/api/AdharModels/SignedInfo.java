package com.microware.cdfi.api.AdharModels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SignedInfo {
    CanonicalizationMethod CanonicalizationMethod;
    Reference Reference;
    SignatureMethod SignatureMethod;

    public com.microware.cdfi.api.AdharModels.CanonicalizationMethod getCanonicalizationMethod() {
        return CanonicalizationMethod;
    }

    public void setCanonicalizationMethod(com.microware.cdfi.api.AdharModels.CanonicalizationMethod canonicalizationMethod) {
        CanonicalizationMethod = canonicalizationMethod;
    }

    public com.microware.cdfi.api.AdharModels.Reference getReference() {
        return Reference;
    }

    public void setReference(com.microware.cdfi.api.AdharModels.Reference reference) {
        Reference = reference;
    }

    public com.microware.cdfi.api.AdharModels.SignatureMethod getSignatureMethod() {
        return SignatureMethod;
    }

    public void setSignatureMethod(com.microware.cdfi.api.AdharModels.SignatureMethod signatureMethod) {
        SignatureMethod = signatureMethod;
    }
}
