package com.microware.cdfi.api.AdharModels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Transforms {
    String Transform;

    public String getTransform() {
        return Transform;
    }

    public void setTransform(String transform) {
        Transform = transform;
    }
}
