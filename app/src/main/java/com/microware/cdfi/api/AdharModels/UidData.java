package com.microware.cdfi.api.AdharModels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UidData {
    LData LData;
    String Pht;
    Poa Poa;
    Poi Poi;
    String tkn;
    String uid;

    public com.microware.cdfi.api.AdharModels.LData getLData() {
        return LData;
    }

    public void setLData(com.microware.cdfi.api.AdharModels.LData LData) {
        this.LData = LData;
    }

    public String getPht() {
        return Pht;
    }

    public void setPht(String pht) {
        Pht = pht;
    }

    public com.microware.cdfi.api.AdharModels.Poa getPoa() {
        return Poa;
    }

    public void setPoa(com.microware.cdfi.api.AdharModels.Poa poa) {
        Poa = poa;
    }

    public com.microware.cdfi.api.AdharModels.Poi getPoi() {
        return Poi;
    }

    public void setPoi(com.microware.cdfi.api.AdharModels.Poi poi) {
        Poi = poi;
    }

    public String getTkn() {
        return tkn;
    }

    public void setTkn(String tkn) {
        this.tkn = tkn;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
