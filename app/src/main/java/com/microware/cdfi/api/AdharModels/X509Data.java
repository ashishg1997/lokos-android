package com.microware.cdfi.api.AdharModels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class X509Data {
    String X509Certificate;
    String X509SubjectName;

    public String getX509Certificate() {
        return X509Certificate;
    }

    public void setX509Certificate(String x509Certificate) {
        X509Certificate = x509Certificate;
    }

    public String getX509SubjectName() {
        return X509SubjectName;
    }

    public void setX509SubjectName(String x509SubjectName) {
        X509SubjectName = x509SubjectName;
    }
}
