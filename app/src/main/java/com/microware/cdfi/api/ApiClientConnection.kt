package com.microware.cdfi.api

import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.application.CDFIApplication.Companion.context
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.Config
import com.microware.cdfi.utility.Validate
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiClientConnection {


    fun createApiInterface(): ApiInterface? {
        apiInterface = null
        var validate: Validate? = null

        if (apiInterface == null) {

            var header = ""

//            var Base_URL = Config().BASE_URL_JH
            var Base_URL = Config().BASE_URL

            validate = Validate(CDFIApplication.context!!)
            var role = validate.RetriveSharepreferenceString(AppSP.Roleid)
            if(validate.RetriveSharepreferenceInt(AppSP.State_Selected) == 29) {
                header = "dynamic_lokos_up"
            } else if(validate.RetriveSharepreferenceInt(AppSP.State_Selected) == 10) {
                header = "dynamic_lokos_ha"
            } else if(validate.RetriveSharepreferenceInt(AppSP.State_Selected) == 33) {//UK 5
                header = "dynamic_lokos_uk"
            } else if(validate.RetriveSharepreferenceInt(AppSP.State_Selected) == 19) {
                header = "dynamic_lokos_mg"
            } else if(validate.RetriveSharepreferenceInt(AppSP.State_Selected) == 32) {
                header = "dynamic_lokos"
            }else {
                header = "dynamic_lokos"
            }

            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

            val httpBuilder = OkHttpClient.Builder()
                .connectTimeout(80, TimeUnit.SECONDS)
                .readTimeout(80, TimeUnit.SECONDS)

            httpBuilder.addInterceptor(loggingInterceptor)

            httpBuilder.addInterceptor(
                Interceptor { chain ->
                    val builder = chain.request().newBuilder()
                    builder.header("X-Tenant-Identifier", header)
                    builder.header("role", role)
                    return@Interceptor chain.proceed(builder.build())
                }
            )

            header = "dynamic_lokos_up"
            Base_URL = Config().BASE_URL

            val builder = Retrofit.Builder()
                .baseUrl(Base_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpBuilder.build())

            val retrofit = builder.build()
            apiInterface = retrofit.create(ApiInterface::class.java)


        }
        return apiInterface
    }



    companion object {

        private var apiClientConnection: ApiClientConnection? = null
        private var apiInterface: ApiInterface? = null
        private val distanceMatrixInterface: ApiInterface? = null

        val instance: ApiClientConnection
            get() {
                apiClientConnection = null
                if (apiClientConnection == null) {
                    apiClientConnection = ApiClientConnection()
                }
                return apiClientConnection!!
            }
    }


}