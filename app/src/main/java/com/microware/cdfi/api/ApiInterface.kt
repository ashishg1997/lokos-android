package com.microware.cdfi.api

import com.google.gson.JsonObject
import com.microware.cdfi.api.meetingdatamodel.DownloadMeetingStatus
import com.microware.cdfi.api.meetinguploadmodel.MeetingApprovalCountModel
import com.microware.cdfi.api.meetinguploadmodel.MeetingApprovalListModel
import com.microware.cdfi.api.meetinguploadmodel.MeetingSummaryModel
import com.microware.cdfi.api.model.PanchayatModel
import com.microware.cdfi.api.response.*
import com.microware.cdfi.clf.models.create_meeting.CreateMeeting
import com.microware.cdfi.clf.models.create_meeting.CreateMeetingResponse
import com.microware.cdfi.clf.models.get_meeting_participants.GetMeetingParticipants
import com.microware.cdfi.clf.models.meeting_config.MeetingConfigResponse
import com.microware.cdfi.clf.models.meetings.GetMeetingsResponse
import com.microware.cdfi.clf.models.save_attendance.SaveAttendance
import com.microware.cdfi.clf.models.save_attendance.SaveAttendanceResponse
import com.microware.cdfi.clf.models.saving.*
import com.microware.cdfi.entity.Bank_branchEntity
import com.microware.cdfi.entity.PanchayatEntity
import com.microware.cdfi.entity.VillageEntity
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {


    @POST("identity/v1/token")
    fun gettoken(@Query("grant_type") grant_type: String, @Query("username") username: String, @Query("password") password: String,@Query("stateid") stateid: Int): Call<JsonObject>


    @POST("identity/v1/token")
    fun gettokenforce(@Query("grant_type") grant_type: String, @Query("username") username: String, @Query("password") password: String,@Query("stateid") stateid: Int,@Query("forceLogin") forceLogin: Int,@Query("role") roleid: String): Call<JsonObject>


    @GET("group/v1/master/getAllMasterDataByLanguage")
    fun getAllMasterDatabylanguagecode(@Query("languageId")  languageId:String?,@Header("Authorization") authorization:String, @Header("User") user:String):Call<MastersResponse>


    @GET("group/v1/shg/getShgDetailById")
    fun getSHGData(@Header("Content-Type") contenttype: String,@Header("Authorization") authorization:String,@Header("User") user:String,@Query("shgId") shgId: Long):Call<ShgDownloadResponse>

    @GET("group/v1/shg/nonSyncShgsByActivationStatus")
    fun getnonSyncShgsByActivationStatus(@Header("Authorization") authorization:String,@Header("User") user:String):Call<MastersResponse>

    @GET("group/v1/shg/downloadShg")
    fun getShglist(@Header("Authorization") authorization:String,@Header("User") user:String):Call<ShgListResponse>

    @POST("group/v1/shg/upload")
    fun postUploadImageBody(@Header("Content-Type") contenttype: String, @Header("User") userId: String, @Header("Authorization") authorization:String, @Body file:MultipartBody): Call<MastersResponse>

    @GET("group/v1/master/getAllBankBranchByIFSCCode/{ifscCode}")
    fun getBankList(@Header("Authorization") authorization:String,@Header("User") user:String, @Path("ifscCode")  ifscCode:String): Call<ResponseBody>

    @PUT("identity/v1/users/updatePassword")
    fun changepassword(@Header("Content-Type") contenttype: String,@Header("Authorization") authorization:String?,@Header("User") user:String?,@Body sData: RequestBody): Call<ResponseBody>

    @GET("group/v1/federation/cboMappingByGUID")
    fun getVO_Shg_Mappingdata(@Header("Authorization") authorization:String,@Header("User") user:String,@Query("cbo_guid") cbo_guid: String,@Query("cbo_type") cbo_type:Int):Call<MastersResponse>

    @FormUrlEncoded
    @POST("oauth2/1/token")
    fun getAuth(
        @Field("code") code: String?,
        @Field("grant_type") grant_type: String?,
        @Field("client_id") client_id: String?,
        @Field("client_secret") client_secret: String?,
        @Field("redirect_uri") redirect_uri: String?
    ): Call<JsonObject?>?

    @GET("oauth2/2/xml/eaadhaar")
    fun getAllData(@Header("Authorization") token: String?): Call<ResponseBody?>?


    @GET("group/v1/federation/downloadStatus")
    fun getFederationlist(@Header("Authorization") authorization:String,@Header("User") user:String,@Query("cbo_type") cbo_type:Int):Call<FederationListResponse>


    @GET("group/v1/federation/getFederationById")
    fun getFederationData(@Header("Content-Type") contenttype: String,@Header("Authorization") authorization:String,@Header("User") user:String,@Query("federationId") federationId: Long):Call<VoDownloadResponse>


    @POST("group/v1/federation/createOrUpdateECMember")
    fun uploadEcMemberdata(@Header("Content-Type") contenttype: String,@Header("User") userId: String,@Header("Authorization") authorization:String,@Body sData: RequestBody):Call<MastersResponse>

    @POST("group/v1/federation/createOrUpdateSubCommittee")
    fun uploadSCdata(@Header("Content-Type") contenttype: String,@Header("User") userId: String,@Header("Authorization") authorization:String,@Body sData: RequestBody):Call<MastersResponse>

    @GET("group/v1/federation/nonSyncFederationByAS")
    fun getnonSyncFederationByAS(@Header("Authorization") authorization:String,@Header("User") user:String,@Query("cbo_type") cbo_type:Int):Call<MastersResponse>

    @GET("group/v1/federation/cboMappingByGUID")
    fun getCLF_VO_Shg_Mappingdata(@Header("Authorization") authorization:String,@Header("User") user:String,@Query("cbo_guid") cbo_guid: String,@Query("cbo_type") cbo_type:Int):Call<MastersResponse>


    @POST("group/v1/federation/")
    fun uploadfederationdata(@Header("Content-Type") contenttype: String,@Header("User") userId: String,@Header("Authorization") authorization:String,@Body file: MultipartBody):Call<MastersResponse>

    @GET("group/v1/shg/downloadTransactionStatus")
    fun getdownloadTransactionStatus(@Header("Authorization") authorization:String,@Header("User") user:String):Call<SHGResponse>

    @PUT("group/v1/shg/searchShgAngular")
    fun getMappingData(@Header("Content-Type") contenttype: String,@Header("User") userId: String,@Header("Authorization") authorization:String,@Body sData: RequestBody):Call<MappingResponse>

    @POST("group/v1/federation/createOrUpdateCboMapping")
    fun uploadMappingdata(@Header("Content-Type") contenttype: String,@Header("User") userId: String,@Header("Authorization") authorization:String,@Body sData: RequestBody):Call<MastersResponse>

    @PUT("group/v1/federation/search_federation")
    fun getCLFMappingData(@Header("Content-Type") contenttype: String,@Header("User") userId: String,@Header("Authorization") authorization:String,@Body sData: RequestBody):Call<ClfMappingResponse>

    @POST("/group/v1/federation/save")
    fun getFederationId(@Header("Content-Type") contenttype: String,@Header("User") userId: String,@Header("Authorization") authorization:String,@Body sData: RequestBody):Call<MastersResponse>

    @GET("group/v1/master/getAllVillagesByPanchayat/{pachayatId}")
    fun getAllVillagesByPanchayat(@Header("Content-Type") contenttype: String,@Header("Authorization") authorization:String,@Header("User") user:String, @Path("pachayatId")  pachayatId:Int): Call<List<VillageEntity>>

    @GET("group/v1/master/getAllPanchayatByBlock/{blockId}")
    fun getAllPanchayatByBlock(@Header("Content-Type") contenttype: String,@Header("Authorization") authorization:String,@Header("User") user:String, @Path("blockId")  blockId:Int): Call<List<PanchayatEntity>>

    @POST("meeting/v1/meeting/upload")
    fun uploadMeetingData(
        @Header("Content-Type") contenttype: String,
        @Header("Authorization") authorization: String,
        @Header("User") user: String,
        @Body sData: RequestBody
    ): Call<MeetingResponse>


    @GET("meeting/v1/meeting/MeetingsBySHG")
    fun getShgMeeinglist(@Header("Authorization") authorization:String,@Header("User") user:String,@Query("shgId") shgId:Long,@Query("noOfMeetings") noOfMeetings:Int):Call<MeetingResponse>

    @GET("meeting/v1/meeting/meeting-approval-list")
    fun getMeetingApprovalList(@Header("Authorization") authorization:String,@Header("User") user:String):Call<List<MeetingApprovalListModel>>

    @GET("meeting/v1/meeting/meeting-count")
    fun getMeetingApprovalCount(@Header("Authorization") authorization:String,@Header("User") user:String):Call<MeetingApprovalCountModel>

    @GET("meeting/v1/meeting/meeting-summary/{shgId}/{meetingNo}")
    fun getMeetingSummary(@Header("Content-Type") contenttype: String,@Header("Authorization") authorization:String,@Header("User") user:String, @Path("shgId")  shgId:Long, @Path("meetingNo")  meetingNo:Int): Call<MeetingSummaryModel>

    @PUT("meeting/v1/meeting/checker-status/{uid}")
    fun updateMeetingStatus(@Header("Content-Type") contenttype: String,@Header("User") userId: String,@Header("Authorization") authorization:String, @Path("uid")  uid:Int,@Query("approvalStatus") approvalStatus:Int,@Query("remarks") remarks:String):Call<MeetingResponse>

    @GET("meeting/v1/meeting/status/{shgId}")
    fun getMeetingApprove_Reject_Status(@Header("Authorization") authorization:String,@Header("User") user:String, @Path("shgId")  shgId:Long): Call<List<DownloadMeetingStatus>>

    @DELETE("meeting/v1/meeting/deleteMeeting/{cbo_id}/{mtg_no}")
    fun deleteMeeting(@Header("Authorization") authorization:String,@Header("user") user:String, @Path("cbo_id")  cbo_id:Long, @Path("mtg_no")  mtg_no:Int): Call<MeetingResponse>

    @DELETE("meeting/v1/vo/meeting/deleteMeeting/{cbo_id}/{mtg_no}")
    fun deleteVOMeeting(@Header("Authorization") authorization:String,@Header("user") user:String, @Path("cbo_id")  cbo_id:Long, @Path("mtg_no")  mtg_no:Int): Call<MeetingResponse>

    @POST("meeting/v1/vo/meeting/upload")
    fun uploadVoMeetingData(
        @Header("Content-Type") contenttype: String,
        @Header("Authorization") authorization: String,
        @Header("User") user: String,
        @Body sData: RequestBody
    ): Call<VoDownloadMeetingResponse>

    @GET("meeting/v1/vo/meeting/MeetingsByVo")
    fun getVoMeeinglist(@Header("Authorization") authorization:String,@Header("user")
    user:String,@Query("voId") voId:Long,@Query("meetingNo") noOfMeetings:Int,
                        @Query("role") role:String):Call<VoDownloadMeetingResponse>

    //nouman

    // added by nouman
    @GET("meeting/v1/clf/564/meeting/config")
    fun getMeetingConfig(
        @Header("Content-Type") contentType: String,
        @Header("Authorization") authorization: String,
        @Header("User") user: String
    ): Call<MeetingConfigResponse>


    @GET("meeting/v1/clf/564/meeting")
    fun getMeetings(
        @Header("Content-Type") contentType: String,
        @Header("Authorization") authorization: String,
        @Header("User") user: String,
        @Query("fromMeetingNo") fromMeetingNo: String,
        @Query("toMeetingNo") toMeetingNo: String,
        @Query("fetchNMeetings") fetchNMeetings: String,
        @Query("page") page: String
    ): Call<ArrayList<GetMeetingsResponse>>


    @GET("meeting/v1/clf/bank/list")
    fun getBankListData(
        @Header("Content-Type") contentType: String,
        @Header("Authorization") authorization: String,
        @Header("User") user: String
    ): Call<ArrayList<GetBankListResponseItem>>

    @GET("meeting/v1/clf/564/bank/accounts")
    fun getBankAccountListData(
        @Header("Content-Type") contentType: String,
        @Header("Authorization") authorization: String,
        @Header("User") user: String
    ): Call<ArrayList<GetBankAccountListResponseItem>>

    @GET("meeting/v1/clf/bank/{bankid}/branch/list")
    fun getBranchListData(
        @Header("Content-Type") contentType: String,
        @Header("Authorization") authorization: String,
        @Header("User") user: String,
        @Path("bankid") uid: Int,
        @Query("keyword") keyword: String
    ): Call<ArrayList<GetBranchListResponseItem>>

    @POST("meeting/v1/clf/564/meeting")
    fun createClfMeeting(
        @Header("Content-Type") contentType: String,
        @Header("Authorization") authorization: String,
        @Header("User") user: String,
        @Body createMeeting: CreateMeeting
    ): Call<CreateMeetingResponse>

    @GET("meeting/v1/clf/564/meeting/{uid}/attendance/participants")
    fun getMeetingParticipants(
        @Header("Content-Type") contentType: String,
        @Header("Authorization") authorization: String,
        @Header("User") user: String,
        @Path("uid") uid: Int
    ): Call<GetMeetingParticipants>

    @GET("meeting/v1/clf/564/meeting/{uid}/savings")
    fun getMeetingSaving(
        @Header("Content-Type") contentType: String,
        @Header("Authorization") authorization: String,
        @Header("User") user: String,
        @Path("uid") uid: Int
    ): Call<ArrayList<GetSavingResponse>>

    @GET("meeting/v1/clf/564/meeting/{uid}/savings/{saveid}")
    fun getMeetingVoSavingList(
        @Header("Content-Type") contentType: String,
        @Header("Authorization") authorization: String,
        @Header("User") user: String,
        @Path("uid") uid: Int,
        @Path("saveid") saveid: Int
    ): Call<ArrayList<GetMeetingVoSavingListResponseItem>>

    @POST("meeting/v1/clf/564/meeting/savings")
    fun createClfSaving(
        @Header("Content-Type") contentType: String,
        @Header("Authorization") authorization: String,
        @Header("User") user: String,
        @Body clfSavingReceipt: ArrayList<SavingRequestModelItem>
    ): Call<ClfSavingResponse>

    @PUT("meeting/v1/clf/564/meeting/savings/{id}")
    fun updateClfSaving(
        @Header("Content-Type") contentType: String,
        @Header("Authorization") authorization: String,
        @Header("User") user: String,
        @Body clfSavingReceipt: ArrayList<SavingRequestModelItem>,
        @Path("id") saveid: Int
    ): Call<ClfSavingResponse>

    @PUT("meeting/v1/clf/564/meeting/{meetingId}/attendance")
    fun saveClfAttendance(
        @Header("Content-Type") contentType: String,
        @Header("Authorization") authorization: String,
        @Header("User") user: String,
        @Path("meetingId") meetingId: Int,
        @Body saveAttendance: SaveAttendance
    ): Call<SaveAttendanceResponse>

}

