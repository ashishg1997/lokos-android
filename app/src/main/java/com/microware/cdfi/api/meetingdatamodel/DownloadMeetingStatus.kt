package com.microware.cdfi.api.meetingdatamodel

data class DownloadMeetingStatus (
    var uid:Int,
    var cboId:Long,
    var mtgGuid:String,
    var mtgType:Int,
    var mtgNo:Int,
    var approvalStatus:Int,
    var checkerRemarks:String
)