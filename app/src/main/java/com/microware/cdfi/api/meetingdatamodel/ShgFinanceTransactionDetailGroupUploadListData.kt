package com.microware.cdfi.api.meetingdatamodel

import com.google.gson.annotations.SerializedName

data class ShgFinanceTransactionDetailGroupUploadListData(

    @SerializedName("auid") var auid: Int? = 0,
    @SerializedName("fundType") var fund_type: Int? = 0,
    @SerializedName("amountToFrom") var amount_to_from: Int? = 0,
    @SerializedName("type") var type: String? = "",
    @SerializedName("amount") var amount: Int? = 0,
    @SerializedName("transDate") var trans_date: Long? = 0,
    @SerializedName("dateRealisation") var date_realisation: Long? = 0,
    @SerializedName("modePayment") var mode_payment: Int? = 0,
    @SerializedName("bankCode") var bank_code: String? = "",
    @SerializedName("transactionNo") var transaction_no: String? = ""
)