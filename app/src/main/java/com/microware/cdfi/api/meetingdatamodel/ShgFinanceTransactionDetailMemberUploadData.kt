package com.microware.cdfi.api.meetingdatamodel

import com.google.gson.annotations.SerializedName

data class ShgFinanceTransactionDetailMemberUploadData(

    @SerializedName("bankCode") var bank_code: String? = "",
    @SerializedName("auid") var auid: Int? = 0,
    @SerializedName("type") var type: String? = "",
    @SerializedName("amount") var amount: Int? = 0,
    @SerializedName("transDate") var trans_date: Long? = 0,
    @SerializedName("dateRealisation") var date_realisation: Long? = 0,
    @SerializedName("modePayment") var mode_payment: Int? = 0,
    @SerializedName("transactionNo") var transaction_no: String? = "",
    @SerializedName("createdBy") var created_by: String? = "",
    @SerializedName("createdOn") var created_on: Long? = 0,
    @SerializedName("updatedBy") var updated_by: String? = null,
    @SerializedName("updatedOn") var updated_on: Long? = null

)