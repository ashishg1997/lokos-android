package com.microware.cdfi.api.meetingdatamodel

import com.google.gson.annotations.SerializedName

data class ShgFinanceTransactionUploadListData(

    @SerializedName("bankCode") var bank_code: String? = "",
    @SerializedName("openingBalance") var opening_balance: Int? = 0,
    @SerializedName("closingBalance") var closing_balance: Int? = 0,
    @SerializedName("otherDeposits") var other_deposits: Int? = 0,
    @SerializedName("otherWithdrawals") var other_withdrawals: Int? = 0,
    @SerializedName("depositedCash") var deposited_cash: Int? = 0,
    @SerializedName("withdrawnCash") var withdrawn_cash: Int? = 0,
    @SerializedName("openingBalanceCash") var opening_balance_cash: Int? = 0,
    @SerializedName("closingBalanceCash") var closing_balance_cash: Int? = 0,
    @SerializedName("zeroMtgCashBank") var zero_mtg_cash_bank: Int? = 0,
    @SerializedName("chequeIssuedNotRealized") var cheque_issued_not_realized: Int? = 0,
    @SerializedName("chequeReceivedNotCredited") var cheque_received_not_credited: Int? = 0,
    @SerializedName("balanceDate") var balance_date: Long? = 0
)