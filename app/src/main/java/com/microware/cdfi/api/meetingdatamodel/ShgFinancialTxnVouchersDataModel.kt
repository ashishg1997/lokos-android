package com.microware.cdfi.api.meetingdatamodel

import com.google.gson.annotations.SerializedName

data class ShgFinancialTxnVouchersDataModel(
    @SerializedName("cboId") var cbo_id: Long? = 0,
    @SerializedName("memberId") var member_id: Long? = 0,
    @SerializedName("mtgNo") var mtg_no: Int? = 0,
    @SerializedName("modePayment") var mode_payment: Int? = 0,
    @SerializedName("bankCode") var bank_code: String? = "",
    @SerializedName("refMtgNo") var ref_mtg_no: Int? = 0,
    @SerializedName("transactionNo") var transaction_no: String? = "",
    @SerializedName("chequeNo") var cheque_no: String? = "",
    @SerializedName("amount") var amount: Int? = 0,
    @SerializedName("voucherDate") var voucher_date: Long? = 0,
    @SerializedName("voucherNo") var voucher_no: String? = "",
    @SerializedName("voucherType") var voucher_type: Int? = 0,
    @SerializedName("auid") var auid: Int? = 0,
    @SerializedName("dateRealisation") var date_realisation: Long? = 0,
    @SerializedName("createdBy") var created_by: String? = "",
    @SerializedName("createdOn") var created_on: Long? = 0,
    @SerializedName("updatedBy") var updated_by: String? = null,
    @SerializedName("updatedOn") var updated_on: Long? =null
)