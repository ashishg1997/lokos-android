package com.microware.cdfi.api.meetingdatamodel

import com.google.gson.annotations.SerializedName

data class ShgGroupLoanScheduleUploadData(

    @SerializedName("loanNo") var loan_no: Int? = 0,
    @SerializedName("principalDemand") var principal_demand: Int? = 0,
    @SerializedName("loanDemandOs") var loan_demand_os: Int? = 0,
    @SerializedName("loanOs") var loan_os: Int? = 0,
    @SerializedName("loanPaid") var loan_paid: Int? = 0,
    @SerializedName("installmentNo") var installment_no: Int? = 0,
    @SerializedName("subInstallmentNo") var sub_installment_no: Int? = 0,
    @SerializedName("installmentDate") var installment_date: Long? = 0,
    @SerializedName("loanDate") var loan_date: Long? = 0,
    @SerializedName("repaid") var repaid: Int? = 0,
    @SerializedName("mtgGuid") var mtg_guid: String? = "",
    @SerializedName("mtgNo") var mtg_no: Int? = null,
    @SerializedName("lastPaidDate") var last_paid_date: Long? = 0,
    @SerializedName("createdBy") var created_by: String? = "",
    @SerializedName("createdOn") var created_on: Long? = 0,
    @SerializedName("updatedBy") var updated_by1: String? = null,
    @SerializedName("updatedOn") var updated_on1: Long? = null
)