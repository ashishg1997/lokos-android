package com.microware.cdfi.api.meetingdatamodel

import com.google.gson.annotations.SerializedName

data class ShgMemberLoanListUploadData(

    @SerializedName("cboId") var cbo_id: Long? = 0,
    @SerializedName("loanApplicationId") var loan_application_id: Long? = null,
    @SerializedName("memId") var mem_id: Long? = 0,
    @SerializedName("loanNo") var loan_no: Long? = 0,
    @SerializedName("installmentDate") var installment_date: Long? = 0,
    @SerializedName("amount") var amount: Int? = 0,
    @SerializedName("mtgNo") var mtg_no: Int? = 0,
    @SerializedName("mtgDate") var mtg_date: Long? = 0,
    @SerializedName("mtgGuid") var mtg_guid: String? = "",
    @SerializedName("loanPurpose") var loan_purpose: Int? = 0,
    @SerializedName("loanProductId") var loan_product_id: Int? = null,
    @SerializedName("interestRate") var interest_rate: Double? = 0.0,
    @SerializedName("period") var period: Int? = 0,
    @SerializedName("principalOverdue") var principal_overdue: Int? = 0,
    @SerializedName("interestOverdue") var interest_overdue: Int? = 0,
    @SerializedName("completionFlag") var completion_flag: Boolean? = null,
    @SerializedName("loanType") var loan_type: Int? = 0,
    @SerializedName("loanSource") var loan_source: Int? = 0,
    @SerializedName("modePayment") var modepayment: Int? = 0,
    @SerializedName("bankCode") var bank_code: String? = "",
    @SerializedName("transactionNo") var transaction_no: String? = "",
    @SerializedName("installmentFreq") var installment_freq: Int? = 0,
    @SerializedName("moratoriumPeriod") var moratorium_period: Int? = 0,
    @SerializedName("originalLoanAmount") var original_loan_amount: Int? = 0,
    @SerializedName("principalRepaid") var principal_repaid: Int? = 0,
    @SerializedName("interestRepaid") var interest_repaid: Int? = 0,
    @SerializedName("disbursementDate") var disbursement_date: Int? = 0,
    @SerializedName("shgMemberLoanScheduleList")
    var shgMemberLoanScheduleList: List<ShgMeberLoanScheduleUploadData>? = null,
    @SerializedName("noOfLoans") var no_of_loans: Int? = 0,
    @SerializedName("originalInterestRate") var original_interest_rate: Double? = 0.0,
    @SerializedName("originalPeriod") var original_period: Int? = 0,
    @SerializedName("rescheduleReason") var reschedule_reason: Int? = 0

)