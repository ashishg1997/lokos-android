package com.microware.cdfi.api.meetingdatamodel

import com.google.gson.annotations.SerializedName

data class ShgMemberLoanTransactionUploadListData(

    @SerializedName("loanNo") var loan_no: Int? = 0,
    @SerializedName("loanOp") var loan_op: Int? = 0,
    @SerializedName("loanOpInt") var loan_op_int: Int? = 0,
    @SerializedName("loanPaid") var loan_paid: Int? = 0,
    @SerializedName("loanPaidInt") var loan_paid_int: Int? = 0,
    @SerializedName("loanCl") var loan_cl: Int? = 0,
    @SerializedName("loanClInt") var loan_cl_int: Int? = 0,
    @SerializedName("completionFlag") var completion_flag: Boolean? = null,
    @SerializedName("intAccruedOp") var int_accrued_op: Int? = 0,
    @SerializedName("intAccrued") var int_accrued: Int? = 0,
    @SerializedName("intAccruedCl") var int_accrued_cl: Int? = 0,
    @SerializedName("principalDemandOb") var principal_demand_ob: Int? = 0,
    @SerializedName("principalDemand") var principal_demand: Int? = 0,
    @SerializedName("principalDemandCb") var principal_demand_cb: Int? = 0,
    @SerializedName("modePayment") var mode_payment: Int? = 0,
    @SerializedName("bankCode") var bank_code: String? = "",
    @SerializedName("transactionNo") var transaction_no: String? = "",
    @SerializedName("interestRate") var interest_rate: Double? = 0.0,
    @SerializedName("period") var period: Int? = 0
)