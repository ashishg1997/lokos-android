package com.microware.cdfi.api.meetingdatamodel

import com.google.gson.annotations.SerializedName

data class ShgMemberSettlementListDataModel(
    @SerializedName("uid") var uid: Int? = 0,
    @SerializedName("cboId") var cbo_id: Long? = 0,
    @SerializedName("mtgGuid") var mtg_guid: String? = "",
    @SerializedName("mtgNo") var mtg_no: Int? = 0,
    @SerializedName("mtgDate") var mtg_date: Long? = 0,
    @SerializedName("memId") var mem_id: Long? = 0,
    @SerializedName("savComp") var sav_comp: Int? = 0,
    @SerializedName("savVol") var sav_vol: Int? = 0,
    @SerializedName("shareCapital") var sharecapital: Int? = 0,
    @SerializedName("memberSurplus") var member_surplus: Int? = 0,
    @SerializedName("loanOutstanding") var loan_outstanding: Int? = 0,
    @SerializedName("memberDeficit") var member_deficit: Int? = 0,
    @SerializedName("availableAmt") var available_amt: Int? = 0,
    @SerializedName("paidAmt") var paid_amt: Int? = 0,
    @SerializedName("receiverName") var receiver_name: String? = "",
    @SerializedName("receiverRelation") var receiver_relation: Int? = 0,
    @SerializedName("reason") var reason: Int? = 0,
    @SerializedName("paymentDate") var payment_date: Long? = 0,
    @SerializedName("modePayment") var mode_payment: Int? = 0,
    @SerializedName("bankCode") var bank_code: String? = "",
    @SerializedName("transactionNo") var transaction_no: String? = "",
    @SerializedName("narration") var narration: String? = "",
    @SerializedName("createdBy") var created_by: String? = "",
    @SerializedName("createdOn") var created_on: Long? = 0,
    @SerializedName("updatedBy") var updated_by: String? = null,
    @SerializedName("updatedOn") var updated_on: Long? = null,
    @SerializedName("other") var other: Int? = 0,
    @SerializedName("otherSpecify") var other_specify: String? = "",
    @SerializedName("settlementStatus") var settlement_status: Int? = 0

)