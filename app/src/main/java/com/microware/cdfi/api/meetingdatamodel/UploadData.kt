package com.microware.cdfi.api.meetingdatamodel

import com.google.gson.annotations.SerializedName
import com.microware.cdfi.api.meetinguploadmodel.ShgMcpData

data class UploadData(

    @SerializedName("transactionId")
    var transaction_id: String? = "",

    @SerializedName("shgMeeting")
    var shgMeeting: UploadMeetingData? = null,

    @SerializedName("shgGroupLoanList")
    var shgGroupLoanList: List<ShgGroupLoanUploadData>? = null,

    @SerializedName("shgMemberLoanList")
    var shgMemberLoanList: List<ShgMemberLoanListUploadData>? = null,

    @SerializedName("shgLoanApplicationList")
    var shgLoanApplicationList: List<ShgLoanApplicationListUploadData>? = null,

    @SerializedName("shgMcpList")
    var shgMcpList: List<ShgMcpData>? = null /*Need to be check columns not in json */


)