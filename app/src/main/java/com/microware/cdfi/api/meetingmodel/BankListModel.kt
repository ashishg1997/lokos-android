package com.microware.cdfi.api.meetingmodel

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class BankListModel (
    val bank_id : Int?,
    val bank_code : String?,
    val bank_name : String?,
    val account_no : String?
)