package com.microware.cdfi.api.meetingmodel

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class BankTransactionModel (
    val cbo_id:Long,
    val mtg_no:Long,
    val bank_from:String,
    val bank_to:String,
    val trans_date:Long,
    val amount:Int,
    val auid:Int)