package com.microware.cdfi.api.meetingmodel

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class FundTypeModel(
    val fundType: String,
    val fundTypeId: Int,
    val interestDefault: Double? = 0.0,
    val defaultPeriod: Int? = 0,
    val minPeriod: Int? = 0,
    val maxPeriod: Int? = 0,
    val minInterest: Double? = 0.0,
    val maxInterest: Double? = 0.0,
    val minAmount: Int? = 0,
    val maxAmount: Int? = 0
)