package com.microware.cdfi.api.meetingmodel

import androidx.room.ColumnInfo
import androidx.room.Ignore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonIgnoreType
import com.microware.cdfi.entity.*
import org.modelmapper.internal.bytebuddy.implementation.bind.annotation.IgnoreForBinding

@JsonIgnoreProperties(ignoreUnknown = true)
data class LoanListModel(

    val uid: Int?,
    val cbo_id: Long?,
    val mem_id: Long?,
    val mtg_guid: String?,
    val mtg_no: Int?,
    val mtg_date: Long?,
    val group_m_code: Long?,
    val member_name: String?,
    val loan_application_id: Long?,
    val loan_no: Int?,
    val request_date: Long?,
    val loan_fee: Int?,
    val amt_demand: Int?,
    val amt_sanction: Int?,
    val amt_disbursed: Int?,
    val approval_date: Long?,
    val tentative_date: Long?,
    val original_loan_amount: Int?

)
