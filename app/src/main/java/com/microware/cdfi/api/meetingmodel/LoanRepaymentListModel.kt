package com.microware.cdfi.api.meetingmodel

import androidx.room.ColumnInfo
import androidx.room.Ignore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonIgnoreType
import com.microware.cdfi.entity.*
import org.modelmapper.internal.bytebuddy.implementation.bind.annotation.IgnoreForBinding

@JsonIgnoreProperties(ignoreUnknown = true)
data class LoanRepaymentListModel(

    val uid: Int?,
    val cbo_id: Long?,
    val mem_id: Long?,
    val mtg_guid: String?,
    val mtg_no: Int?,
    val mtg_date: Long?,
    val group_m_code: Long?,
    val member_name: String?,
    val loan_no: Int?,
    val loan_op: Int?,
    val loan_op_int: Int?,
    val loan_paid: Int?,
    val loan_paid_int: Int?,
    val principal_demand: Int?,
    val principal_demand_ob: Int?,
    val principal_demand_cb: Int?,
    val int_accrued: Int?,
    val int_accrued_op: Int?


)
