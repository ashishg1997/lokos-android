package com.microware.cdfi.api.meetingmodel

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class ShareCapitalModel(
    val uid: Int?,
    val cbo_id: Long?,
    val mem_id: Long?,
    val auid: Int?,
    val member_aid: Long?,
    val mtg_guid: String?,
    val member_name: String?,
    val mtgno: Int?,
    val type: String?,
    val amount: Int?,
    val trans_date: Long?,
    val date_realisation: Long?,
    val mode_payment: Int?,
    val transaction_no: String?
)