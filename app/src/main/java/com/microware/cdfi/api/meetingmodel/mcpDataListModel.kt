package com.microware.cdfi.api.meetingmodel

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class mcpDataListModel (
    val mcpId:Long,
    val purpose:String,
    val amount:Int,
    val request_date:Long
)