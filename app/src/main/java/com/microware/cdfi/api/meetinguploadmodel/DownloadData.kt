package com.microware.cdfi.api.meetinguploadmodel

import com.google.gson.annotations.SerializedName

data class DownloadData(

    @SerializedName("transactionId")
    var transaction_id: String? = "",

    @SerializedName("shgMeeting")
    var shgMeeting: DownloadMeetingData? = null,

    @SerializedName("shgGroupLoan")
    var shgGroupLoan: List<ShgGroupLoanData>? = null,

    @SerializedName("shgMemberLoanList")
    var shgMemberLoanList: List<ShgMemberLoanListData>? = null,

    @SerializedName("shgLoanApplicationList")
    var shgLoanApplicationList: List<ShgLoanApplicationListData>? = null,


/*
    @SerializedName("shgMeberLoanSchedule")
    var shgMeberLoanSchedule: List<ShgMeberLoanScheduleData>? = null,

    @SerializedName("shgGroupLoanSchedule")
    var shgGroupLoanSchedule: List<ShgGroupLoanScheduleData>? = null,
*/

    @SerializedName("shgMcp")
    var shgMcp: List<ShgMcpData>? = null /*Need to be check columns not in json */


)