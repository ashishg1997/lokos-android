package com.microware.cdfi.api.meetinguploadmodel

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.SerializedName
import com.microware.cdfi.api.meetingdatamodel.ShgFinancialTxnVouchersDataModel

@JsonIgnoreProperties(ignoreUnknown = true)
data class DownloadMeetingData(

    @SerializedName("uid")
    var uid: Long? = 0,

    @SerializedName("cboId")
    var cbo_id: Long? = 0,

    @SerializedName("mtgGuid")
    var mtg_guid: String? = "",

    @SerializedName("mtgType")
    var mtg_type: Int? = 0,

    @SerializedName("mtgNo")
    var mtg_no: Int? = 0,

    @SerializedName("mtgDate")
    var mtg_date: Long? = 0,

    @SerializedName("flagOpen")
    var flag_open: String? = "",

    @SerializedName("mtgFrq")
    var mtg_frq: Int? = 0,

    @SerializedName("flagDow")
    var flag_dow: String? = "",

    @SerializedName("daysElapsed")
    var days_elapsed: Int? = 0,

    @SerializedName("openingBalance")
    var opening_balance: Int? = 0,

    @SerializedName("closingBalance")
    var closing_balance: Int? = 0,

    @SerializedName("totalMeetingHeld")
    var total_meeting_held: Int? = 0,

    @SerializedName("expectedDate")
    var expected_date: Long? = 0,

    @SerializedName("approvalStatus")
    var approval_status: Int? = 0,

    @SerializedName("actionBy")
    var action_by: String? = "",

    @SerializedName("actionOn")
    var action_on: Long? = 0,

    @SerializedName("mnthCompSav")
    var mnth_comp_sav: Int? = 0,

    @SerializedName("totalAttendance")
    var total_attendance: Int? = 0,

    @SerializedName("savCompOb")
    var sav_comp_ob: Int? = 0,

    @SerializedName("savComp")
    var sav_comp: Int? = 0,

    @SerializedName("savCompCb")
    var sav_comp_cb: Int? = 0,

    @SerializedName("savCompWithdrawal")
    var sav_comp_withdrawal: Int? = 0,

    @SerializedName("savVolOb")
    var sav_vol_ob: Int? = 0,

    @SerializedName("savVol")
    var sav_vol: Int? = 0,

    @SerializedName("savVolCb")
    var sav_vol_cb: Int? = 0,

    @SerializedName("savVolWithdrawal")
    var sav_vol_withdrawal: Int? = 0,

    @SerializedName("memOtherReceipts")
    var mem_other_receipts: Int? = 0,

    @SerializedName("memOtherPayments")
    var mem_other_payments: Int? = 0,

    @SerializedName("noLoansDisbursed")
    var no_loans_disbursed: Int? = 0,

    @SerializedName("amtLoansDisbursed")
    var amt_loans_disbursed: Int? = 0,

    @SerializedName("noLoansRepaid")
    var no_loans_repaid: Int? = 0,

    @SerializedName("amtLoansRepaid")
    var amt_loans_repaid: Int? = 0,

    @SerializedName("noLoanOverdue")
    var no_loan_overdue: Int? = 0,

    @SerializedName("amtLoansOverdue")
    var amt_loans_overdue: Int? = 0,

    @SerializedName("noLoansOs")
    var no_loans_os: Int? = 0,

    @SerializedName("amtLoansOs")
    var amt_loans_os: Int? = 0,

    @SerializedName("amtInterestReceived")
    var amt_interest_received: Int? = 0,

    @SerializedName("noLoansNpa")
    var no_loans_npa: Int? = 0,

    @SerializedName("amtLoansNpa")
    var amt_loans_npa: Int? = 0,

    @SerializedName("noLoansReceivedExt")
    var no_loans_received_ext: Int? = 0,

    @SerializedName("amtLoansReceivedExt")
    var amt_loans_received_ext: Int? = 0,

    @SerializedName("noLoansRepaidExt")
    var no_loans_repaid_ext: Int? = 0,

    @SerializedName("amtLoansRepaidExt")
    var amt_loans_repaid_ext: Int? = 0,

    @SerializedName("noLoanOverdueExt")
    var no_loan_overdue_ext: Int? = 0,

    @SerializedName("amtLoansOverdueExt")
    var amt_loans_overdue_ext: Int? = 0,

    @SerializedName("noLoansOsExt")
    var no_loans_os_ext: Int? = 0,

    @SerializedName("amtLoansOsExt")
    var amt_loans_os_ext: Int? = 0,

    @SerializedName("amtInterestPaidExt")
    var amt_interest_paid_ext: Int? = 0,

    @SerializedName("noLoansNpaExt")
    var no_loans_npa_ext: Int? = 0,

    @SerializedName("amtLoansNpaExt")
    var amt_loans_npa_ext: Int? = 0,

    @SerializedName("otherReceipts")
    var other_receipts: Int? = 0,

    @SerializedName("otherPayments")
    var other_payments: Int? = 0,

    @SerializedName("depositedCash")
    var deposited_cash: Double? = null,

    @SerializedName("withdrawnCash")
    var withdrawn_cash: Double? = null,

    @SerializedName("openingBalanceCash")
    var opening_balance_cash: Int? = 0,

    @SerializedName("closingBalanceCash")
    var closing_balance_cash: Int? = 0,

    @SerializedName("createdBy")
    var created_by: String? = "",

    @SerializedName("createdOn")
    var created_on: Long? = 0,

    @SerializedName("updatedBy")
    var updated_by: String? = null,

    @SerializedName("updatedOn")
    var updated_on: Long? = null,

    @SerializedName("zeroMtgCashInHand")
    var zero_mtg_cash_in_hand: Int? = 0,

    @SerializedName("zeroMtgCashInTransit")
    var zero_mtg_cash_in_transit: Int? = 0,

    @SerializedName("balanceDate")
    var balance_date: Long? = 0,

    @SerializedName("is_edited")
    var is_edited: Int? = 0,

    @SerializedName("checkerRemarks")
    var checker_remarks: String? = "",

    @SerializedName("transaction_no")
    var transaction_no: String? = "",

    /*for json */

    @SerializedName("shgMeetingDetailsList")
    var shgMeetingDetailsList: List<ShgMeetingDetailsListData>? = null,

    @SerializedName("shgFinanceTransactionList")
    var shgFinanceTransactionList: List<ShgFinanceTransactionListData>? = null,

    @SerializedName("shgFinanceTransactionDetailGroupList")
    var shgFinanceTransactionDetailGroupList: List<ShgFinanceTransactionDetailGroupListData>? = null,

    @SerializedName("shgGroupLoanTransactionList")
    var shgGroupLoanTransaction: List<ShgGroupLoanTransactionData>? = null,

    @SerializedName("shgFinanceTransactionVouchersList")
    var shgFinanceTransactionVouchersList: List<ShgFinancialTxnVouchersDataModel>? = null

)