package com.microware.cdfi.api.meetinguploadmodel

data class MeetingApprovalCountModel (
    var pending:Int,
    var approved:Int,
    var reject:Int,
    var total:Int
)