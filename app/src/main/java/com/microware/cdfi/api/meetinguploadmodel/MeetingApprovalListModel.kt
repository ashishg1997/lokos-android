package com.microware.cdfi.api.meetinguploadmodel

data class MeetingApprovalListModel (
    val shgName:String,
    val mtgNo: Int,
    val mtgDate: Long,
    val pendingMtgCount: Int,
    val cboId: Long
)