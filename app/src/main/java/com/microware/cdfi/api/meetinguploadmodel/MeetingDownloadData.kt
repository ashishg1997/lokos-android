package com.microware.cdfi.api.meetinguploadmodel

import com.google.gson.annotations.SerializedName

class MeetingDownloadData {

    @SerializedName("shgMeeting")
    var shgMeeting: DownloadMeetingData? = null

    @SerializedName("shgGroupLoanList")
    var shgGroupLoanList: List<ShgGroupLoanData>? = null

    @SerializedName("shgMemberLoanList")
    var shgMemberLoanList: List<ShgMemberLoanListData>? = null

    @SerializedName("shgLoanApplicationList")
    var shgLoanApplicationList: List<ShgLoanApplicationListData>? = null

    @SerializedName("shgMcpList")
    var shgMcpList: List<ShgMcpData>? = null

}