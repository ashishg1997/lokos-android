package com.microware.cdfi.api.meetinguploadmodel

import com.google.gson.annotations.SerializedName

data class ShgFinanceTransactionDetailGroupListData(
    @SerializedName("uid") var uid: Long? = 0,
    @SerializedName("shgMtgUid") var shg_mtg_uid: Long? = 0,/*not in entity*/
    @SerializedName("mtgGuid") var mtg_guid: String? = "",
    @SerializedName("cboId") var cbo_id: Long? = 0,
    @SerializedName("mtgNo") var mtg_no: Int? = 0,
    @SerializedName("auid") var auid: Int? = 0,
    @SerializedName("fundType") var fund_type: Int? = 0,
    @SerializedName("amountToFrom") var amount_to_from: Int? = 0,
    @SerializedName("type") var type: String? = "",
    @SerializedName("amount") var amount: Int? = 0,
    @SerializedName("transDate") var trans_date: Long? = 0,
    @SerializedName("dateRealisation") var date_realisation: Long? = 0,
    @SerializedName("modePayment") var mode_payment: Int? = 0,
    @SerializedName("bankCode") var bank_code: String? = "",
    @SerializedName("transactionNo") var transaction_no: String? = "",
    @SerializedName("createdBy") var created_by: String? = "",
    @SerializedName("createdOn") var created_on: Long? = 0,
    @SerializedName("updatedBy") var updated_by: String? = null,
    @SerializedName("updatedOn") var updated_on: Long? = null
//    @SerializedName("uploaded_by") var uploaded_by: String?,
//    @SerializedName("uploaded_on") var uploaded_on: Long?
)
