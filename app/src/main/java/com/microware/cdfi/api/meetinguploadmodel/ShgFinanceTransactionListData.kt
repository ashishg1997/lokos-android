package com.microware.cdfi.api.meetinguploadmodel

import com.google.gson.annotations.SerializedName

data class ShgFinanceTransactionListData(
    @SerializedName("uid") var uid: Long? = 0,
    @SerializedName("cboId") var cbo_id: Long? = 0,
    @SerializedName("shgMtgUid") var shg_mtg_uid: Long? = 0,/*not in entity*/
    @SerializedName("mtgGuid") var mtg_guid: String? = "",
    @SerializedName("mtgNo") var mtg_no: Int? = 0,
    @SerializedName("bankCode") var bank_code: String? = "",
    @SerializedName("openingBalance") var opening_balance: Int? = 0,
    @SerializedName("closingBalance") var closing_balance: Int? = 0,
    @SerializedName("otherDeposits") var other_deposits: Int? = 0,
    @SerializedName("otherWithdrawals") var other_withdrawals: Int? = 0,
    @SerializedName("depositedCash") var deposited_cash: Int? = 0,
    @SerializedName("withdrawnCash") var withdrawn_cash: Int? = 0,
    @SerializedName("openingBalanceCash") var opening_balance_cash: Int? = 0,
    @SerializedName("closingBalanceCash") var closing_balance_cash: Int? = 0,
    @SerializedName("createdBy") var created_by: String? = "",
    @SerializedName("createdOn") var created_on: Long? = 0,
    @SerializedName("updatedBy") var updated_by: String? = null,
    @SerializedName("updatedOn") var updated_on: Long? = null,
    @SerializedName("zeroMtgCashBank") var zero_mtg_cash_bank: Int? = 0,
    @SerializedName("chequeIssuedNotRealized") var cheque_issued_not_realized: Int? = 0,
    @SerializedName("chequeReceivedNotCredited") var cheque_received_not_credited: Int? = 0,
    @SerializedName("balanceDate") var balance_date: Long? = 0
//    @SerializedName("uploaded_by") var uploadedby: String?,
//    @SerializedName("uploaded_on") var uploadedon: Long?
)