package com.microware.cdfi.api.meetinguploadmodel

import com.google.gson.annotations.SerializedName

data class ShgGroupLoanData(
    @SerializedName("uid") var uid: Long? = 0,
    @SerializedName("loanApplicationId") var loan_application_id: Long? = 0,
    @SerializedName("cboId") var cbo_id: Long? = 0,
    @SerializedName("mtgGuid") var mtg_guid: String? = "",
    @SerializedName("loanNo") var loan_no: Int? = 0,
    @SerializedName("mtgNo") var mtg_no: Int? = 0,
    @SerializedName("mtgDate") var mtg_date: Long? = 0,
    @SerializedName("disbursementDate") var disbursement_date: Long? = 0,
    @SerializedName("installmentDate") var installment_date: Long? = 0,
    @SerializedName("amount") var amount: Int? = 0,
    @SerializedName("loanPurpose") var loan_purpose: Int? = 0,
    @SerializedName("loanProductId") var loan_product_id: Int? = 0,
    @SerializedName("interestRate") var interest_rate: Double? = 0.0,
    @SerializedName("period") var period: Int? = 0,
    @SerializedName("principalOverdue") var principal_overdue: Int? = 0,
    @SerializedName("interestOverdue") var interest_overdue: Int? = 0,
    @SerializedName("interestDue") var interest_due: Double? = 0.0,
    @SerializedName("completionFlag") var completion_flag: Boolean? = null,
    @SerializedName("loanType") var loan_type: Int? = 0,
    @SerializedName("loanSource") var loan_source: Int? = 0,
    @SerializedName("externalLoanId") var external_loan_id: Int? = 0,
    @SerializedName("modePayment") var mode_payment: Int? = 0,
    @SerializedName("bankCode") var bank_code: String? = "",
    @SerializedName("transactionNo") var transaction_no: String? = "",
    @SerializedName("installmentFreq") var installment_freq: Int? = 0,
    @SerializedName("moratoriumPeriod") var moratorium_period: Int? = 0,
    @SerializedName("loanAccountNo") var loan_account_no: String? = "",
    @SerializedName("repayTo") var repay_to: Int? = 0,
    @SerializedName("createdBy") var created_by: String? = "",
    @SerializedName("createdOn") var created_on: Long? = 0,
    @SerializedName("updatedBy") var updated_by: String? = null,
    @SerializedName("updatedOn") var updated_on: Long? = null,
    @SerializedName("institution")
    var institution: Int? = 0,
    @SerializedName("sanctionedAmount")
    var sanctioned_amount: Int? = 0,
    @SerializedName("drawingLimit")
    var drawing_limit: Int? = 0,
    @SerializedName("loanRefNo")
    var loan_ref_no: String? = "",
    @SerializedName("organizationName")
    var organization_name: String? = "",
    @SerializedName("principleRepaid")
    var principle_repaid: Int? = 0,
    @SerializedName("orignalLoanAmount")
    var orignal_loan_amount: Int? = 0,
    @SerializedName("shgGroupLoanScheduleList")
    var shgGroupLoanSchedule: List<ShgGroupLoanScheduleData>? = null,
    @SerializedName("overduePeriod") var overdue_period: Int? = 0,
    @SerializedName("actualTransactionDate") var actual_transaction_date: Long? = 0,
    @SerializedName("originalInterestRate") var original_interest_rate: Double? = 0.0,
    @SerializedName("originalPeriod") var original_period: Int? = 0,
    @SerializedName("rescheduleReason") var reschedule_reason: Int? = 0


//    @SerializedName("createdBy") var created_by: String? = "",
//    @SerializedName("createdOn") var created_on: Long? = 0,
//    @SerializedName("updatedBy") var updated_by: String? = "",
//    @SerializedName("updatedOn") var updated_on: Long? = 0
//    @SerializedName("uploaded_by") var uploaded_by: String? = "",
//    @SerializedName("uploaded_on") var uploaded_on: Long? = 0,

)