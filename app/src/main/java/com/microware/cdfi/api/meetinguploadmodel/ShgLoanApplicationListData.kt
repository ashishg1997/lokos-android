package com.microware.cdfi.api.meetinguploadmodel

import com.google.gson.annotations.SerializedName

data class ShgLoanApplicationListData(
    @SerializedName("uid") var uid: Long? = 0,
    @SerializedName("shgMtgDetUid") var shg_mtg_det_uid: Long? = 0 ,/*filed not in entity*/
    @SerializedName("loanApplicationId") var loan_application_id: Long? = 0,
    @SerializedName("cboId") var cbo_id: Long? = 0,
    @SerializedName("memId") var mem_id: Long? = 0,
    @SerializedName("requestDate") var request_date: Long? = 0,
    @SerializedName("loanFee") var loan_fee: Int?  = 0,
    @SerializedName("amtDemand") var amt_demand: Int?  = 0,
    @SerializedName("amtSanction") var amt_sanction: Int?  = 0,
    @SerializedName("amtDisbursed") var amt_disbursed: Int?  = 0,
    @SerializedName("approvalDate") var approval_date: Long? = 0,
    @SerializedName("tentativeDate") var tentative_date: Long? = 0,
    @SerializedName("loanProductId") var loan_product_id: Long? = 0,
    @SerializedName("loanRequestPriority") var loan_request_priority: Int?  = 0,
    @SerializedName("loanSource") var loan_source: Int?  = 0,
    @SerializedName("loanPurpose") var loan_purpose: Int?  = 0,
    @SerializedName("loanPeriod") var loan_period: Int?  = 0,
    @SerializedName("loanRequestedMtgGuid") var loan_requested_mtg_guid: String? = "",
    @SerializedName("loanRequestedMtgNo") var loan_requested_mtg_no: Int?  = 0,
    @SerializedName("loanSanctionedMtgNo") var loan_sanctioned_mtg_no: Int?  = null,
    @SerializedName("loanSanctionedMtgGuid") var loan_sanctioned_mtg_guid: String? = null,
    @SerializedName("createdBy") var created_by: String? = "",
    @SerializedName("createdOn") var created_on: Long? = 0,
    @SerializedName("updatedBy") var updated_by: String? = null,
    @SerializedName("updatedOn") var updated_on: Long? = null
//    @SerializedName("uploadedby") var uploadedby: String? == "",
//    @SerializedName("uploaded_on") var uploaded_on: Long? = 0
)