package com.microware.cdfi.api.meetinguploadmodel

import com.google.gson.annotations.SerializedName

data class ShgMcpData(
    @SerializedName("mcpId") var mcp_id: Long? = 0,
    @SerializedName("cboId") var cbo_id: Long? = 0,
    @SerializedName("memId") var mem_id: Long? = 0,
    @SerializedName("amtDemand") var amt_demand: Int? = 0,
    @SerializedName("tentativeDate") var tentative_date: Long? = 0,
    @SerializedName("loanProductId") var loan_product_id: Int? =null,
    @SerializedName("loanPurpose") var loan_purpose: Int? =0,
    @SerializedName("loanPeriod") var loan_period: Int? =0,
    @SerializedName("loanRequestedMtgNo") var loan_requested_mtg_no: Int? = 0,
    @SerializedName("loanRequestedMtgGuid") var loan_requested_mtg_guid: String? = "",
    @SerializedName("loanSanctionedMtgNo") var loan_sanctioned_mtg_no: Int? =null,
    @SerializedName("loanSanctionedMtgGuid") var loan_sanctioned_mtg_guid: String? =null,
    @SerializedName("loanRequestPriority") var loan_request_priority: Int? = 0,
    @SerializedName("proposedEmiAmount") var proposed_emi_amount: Int? = 0,
    @SerializedName("createdBy") var created_by: String? = "",
    @SerializedName("createdOn") var created_on: Long? = 0,
    @SerializedName("updatedBy") var updated_by1: String? = null,
    @SerializedName("updatedOn") var updated_on1: Long? = null,
    @SerializedName("requestDate") var request_date: Long? = null

//   @SerializedName("loanSource") var loan_source: Int? =0,
)
