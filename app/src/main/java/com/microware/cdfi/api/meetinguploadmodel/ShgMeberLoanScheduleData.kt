package com.microware.cdfi.api.meetinguploadmodel

import com.google.gson.annotations.SerializedName

data class ShgMeberLoanScheduleData(
    @SerializedName("uid") var uid: Long? =0,
    @SerializedName("cboId") var cbo_id: Long? = 0,
    @SerializedName("memId") var mem_id: Long? = 0,
    @SerializedName("mtgGuid") var mtg_guid: String? = "",
    @SerializedName("loanNo") var loan_no: Int? = 0,
    @SerializedName("principalDemand") var principal_demand: Int? = 0,
    @SerializedName("loanDemandOs") var loan_demand_os: Int? = 0,
    @SerializedName("loanOs") var loan_os: Int? = 0,
    @SerializedName("loanPaid") var loan_paid: Int? = 0,
    @SerializedName("installmentNo") var installment_no: Int? = 0,
    @SerializedName("subInstallmentNo") var sub_installment_no: Int? = 0,
    @SerializedName("installmentDate") var installment_date: Long? = 0,
    @SerializedName("repaid") var repaid: Boolean? = null,
    @SerializedName("lastPaidDate") var last_paid_date: Long? = 0,
    @SerializedName("mtgNo") var mtg_no: Int? = 0,
    @SerializedName("createdBy") var created_by: String? = "",
    @SerializedName("createdOn") var created_on: Long? = 0,
    @SerializedName("updatedBy") var updated_by: String? = null,
    @SerializedName("updatedOn") var updated_on: Long? = null
//    @SerializedName("uploaded_by") var uploaded_by: String?,
//    @SerializedName("uploaded_on") var uploaded_on: Long?
)