package com.microware.cdfi.api.meetinguploadmodel

import com.google.gson.annotations.SerializedName

data class UploadMeetingData(

    @SerializedName("uid")
    val uid: Long? = null,

    @SerializedName("cboId")
    val cbo_id: Long? = null,

    @SerializedName("mtgGuid")
    val mtg_guid: String? = null,

    @SerializedName("mtgType")
    val mtg_type: Int? = null,

    @SerializedName("mtgNo")
    val mtg_no: Int? = null,

    @SerializedName("mtgDate")
    val mtg_date: Long? = null,

    @SerializedName("flagOpen")
    val flag_open: String? = null,

    @SerializedName("mtgFrq")
    val mtg_frq: Int? = null,

    @SerializedName("flagDow")
    val flag_dow: String? = null,

    @SerializedName("daysElapsed")
    val days_elapsed: Int? = null,

    @SerializedName("openingBalance")
    val opening_balance: Int? = null,

    @SerializedName("closingBalance")
    val closing_balance: Int? = null,

    @SerializedName("totalMeetingHeld")
    val total_meeting_held: Int? = null,

    @SerializedName("expectedDate")
    val expected_date: Long? = null,

    @SerializedName("approvalStatus")
    val approval_status: Int? = null,

//    @SerializedName("action_by")
//    val action_by: String?= null,

    @SerializedName("actionOn")
    val action_on: Long? = null,

    @SerializedName("mnthCompSav")
    val mnth_comp_sav: Int? = null,

    @SerializedName("totalAttendance")
    val total_attendance: Int? = null,

    @SerializedName("savCompOb")
    val sav_comp_ob: Int? = null,

    @SerializedName("savComp")
    val sav_comp: Int? = null,

    @SerializedName("savCompCb")
    val sav_comp_cb: Int? = null,

    @SerializedName("savCompWithdrawal")
    val sav_comp_withdrawal: Int? = null,

    @SerializedName("savVolOb")
    val sav_vol_ob: Int? = null,

    @SerializedName("savVol")
    val sav_vol: Int? = null,

    @SerializedName("savVolCb")
    val sav_vol_cb: Int? = null,

    @SerializedName("savVolWithdrawal")
    val sav_vol_withdrawal: Int? = null,

    @SerializedName("memOtherReceipts")
    val mem_other_receipts: Int? = null,

    @SerializedName("memOtherPayments")
    val mem_other_payments: Int? = null,

    @SerializedName("noLoansDisbursed")
    val no_loans_disbursed: Int? = null,

    @SerializedName("amtLoansDisbursed")
    val amt_loans_disbursed: Int? = null,

    @SerializedName("noLoansRepaid")
    val no_loans_repaid: Int? = null,

    @SerializedName("amtLoansRepaid")
    val amt_loans_repaid: Int? = null,

    @SerializedName("noLoanOverdue")
    val no_loan_overdue: Int? = null,

    @SerializedName("amtLoansOverdue")
    val amt_loans_overdue: Int? = null,

    @SerializedName("noLoansOs")
    val no_loans_os: Int? = null,

    @SerializedName("amtLoansOs")
    val amt_loans_os: Int? = null,

    @SerializedName("amtInterestReceived")
    val amt_interest_received: Int? = null,

    @SerializedName("noLoansNpa")
    val no_loans_npa: Int? = null,

    @SerializedName("amtLoansNpa")
    val amt_loans_npa: Int? = null,

    @SerializedName("noLoansReceivedExt")
    val no_loans_received_ext: Int? = null,

    @SerializedName("amtLoansReceivedExt")
    val amt_loans_received_ext: Int? = null,

    @SerializedName("noLoansRepaidExt")
    val no_loans_repaid_ext: Int? = null,

    @SerializedName("amtLoansRepaidExt")
    val amt_loans_repaid_ext: Int? = null,

    @SerializedName("noLoanOverdueExt")
    val no_loan_overdue_ext: Int? = null,

    @SerializedName("amtLoansOverdueExt")
    val amt_loans_overdue_ext: Int? = null,

    @SerializedName("noLoansOsExt")
    val no_loans_os_ext: Int? = null,

    @SerializedName("amtLoansOsExt")
    val amt_loans_os_ext: Int? = null,

    @SerializedName("amtInterestPaidExt")
    val amt_interest_paid_ext: Int? = null,

    @SerializedName("noLoansNpaExt")
    val no_loans_npa_ext: Int? = null,

    @SerializedName("amtLoansNpaExt")
    val amt_loans_npa_ext: Int? = null,

    @SerializedName("otherReceipts")
    val other_receipts: Int? = null,

    @SerializedName("otherPayments")
    val other_payments: Int? = null,

    @SerializedName("depositedCash")
    val deposited_cash: Double? = null,

    @SerializedName("withdrawnCash")
    val withdrawn_cash: Double? = null,

    @SerializedName("openingBalanceCash")
    val opening_balance_cash: Int? = null,

    @SerializedName("closingBalanceCash")
    val closing_balance_cash: Int? = null

    /*@SerializedName("shgMeetingDetailsList")
    var shgMeetingDetailsList: List<ShgMeetingDetailsListData>? = null
*/
//    @SerializedName("createdBy")
//    val created_by: String? = null,
//
//    @SerializedName("createdOn")
//    val created_on: Long? = null,
//
//    @SerializedName("updatedBy")
//    val updated_by: String? = null,
//
//    @SerializedName("updatedOn")
//    val updated_on: Long? = null

/*@SerializedName("uploaded_by")
val uploaded_by: String? = null,

@SerializedName("uploaded_on")
val uploaded_on: Long? = null,*/

)