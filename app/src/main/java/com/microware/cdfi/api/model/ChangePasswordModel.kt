package com.microware.cdfi.api.model

import androidx.room.Ignore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonIgnoreType
import com.microware.cdfi.entity.*
import org.modelmapper.internal.bytebuddy.implementation.bind.annotation.IgnoreForBinding
@JsonIgnoreProperties(ignoreUnknown = true)
data class ChangePasswordModel(

    var oldPassword: String = "",
    var newPassword: String = "",
    var confirmPassword: String? = "",
    var userId:String=""

)
