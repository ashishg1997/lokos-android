package com.microware.cdfi.api.model

data class ClfMappingDataModel (
    var federation_id:Long?=0,
    var guid:String?="",
    var federation_name:String?="",
    var federation_formation_date:Long?=0,
    var federation_code:Long?=0,
    var parent_cbo_code:Long?=0,
    var cooption_date:Long?=0,
    var federation_revival_date:Long?=0,
    var is_active:Int?=0,
    var is_edited:Int?=0,
    var status:Int?=0,
    var leaving_reason:Int?=0,
    var settlement_status:Int?=0
)