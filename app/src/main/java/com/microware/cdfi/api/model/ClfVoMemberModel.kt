package com.microware.cdfi.api.model

import com.microware.cdfi.entity.VoShgMemberPhoneEntity

data class ClfVoMemberModel (
    var clf_guid:String?,
    var vo_guid:String?,
    var designation: Int?,
    var joining_date: Long?,
    var member_guid: String,
    var member_id: Long,
    var member_name: String,
    var member_name_local: String?,
    var phone_guid: String?,
    var phone_no: String?,
    var relation: Int?,
    var relation_name: String?,
    var relation_name_local: String?,
    var shg_formation_date: Long?,
    var shg_guid: String,
    var shg_id: Long,
    var shg_name: String,
    var shg_name_local: String?,
    var memberPhoneDetailsList: List<VoShgMemberPhoneEntity>? = null
)