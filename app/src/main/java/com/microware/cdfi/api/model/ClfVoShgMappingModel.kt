package com.microware.cdfi.api.model

data class ClfVoShgMappingModel(

    var cbo_child_formation_date: Long?,
    var cbo_child_guid: String?,
    var cbo_child_id: Long?,
    var cbo_child_name: String?,
    var cbo_child_name_local: String?,
    var cbo_code: Long?,
    var cbo_guid: String,
    var joining_date: Long?,
    var leaving_date: Long?,
    var ecMemberDetailsForMappingList: List<ClfVoMemberModel>? = null
)

