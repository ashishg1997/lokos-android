package com.microware.cdfi.api.model

data class EcMemberPhoneModel (
    val member_guid:String?,
    val phoneNo:String?
)