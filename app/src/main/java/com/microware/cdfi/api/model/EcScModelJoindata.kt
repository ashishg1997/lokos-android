package com.microware.cdfi.api.model

data class EcScModelJoindata (
    val member_cd:Long?,
    val member_name:String?,
    val member_guid:String?,
    val join_date:Long?,
    val shg_name:String?

)