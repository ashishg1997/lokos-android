package com.microware.cdfi.api.model

data class FedrationResponseModelItem(
    val activationStatus: Int,
    val approve_status: Int,
    val checker_remarks: String?,
    val code: String?,
    val guid: String
)