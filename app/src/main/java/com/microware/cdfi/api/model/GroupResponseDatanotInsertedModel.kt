package com.microware.cdfi.api.model

data class GroupResponseDatanotInsertedModel(
    val id: Int?,
     val guid: String,
    val remarks: String?,
    val user_id: String?

    )