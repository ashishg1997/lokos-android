package com.microware.cdfi.api.model

data class GroupResponseModelItem(
    val activationStatus: Int,
    val approve_status: Int,
    val checker_remarks: String?,
    val code: String?,
    val guid: String,
    val memberDownloadStatus: List<MemberDownloadStatus>? = null,
    val bankDownloadStatus: List<SHGBankDownloadStatus>? = null
)
data class SHGBankDownloadStatus(
    val activationStatus: Int,
    val guid: String
)