package com.microware.cdfi.api.model

data class GroupResponseTransactionModel(

    val transaction_id: String,
    val status: Int,
    val remarks: String

)