package com.microware.cdfi.api.model

class MappedShgUploadModel (
    
    var cbo_guid:String?="",
    var cbo_child_guid:String?="",
    var cbo_id:Long?=0,
    var cbo_child_id:Long?=0,
    var cbo_child_code:Long?=0,
    var cbo_child_name:String?="",
    var cbo_child_village_id:String?="",
    var cbo_level:Int?=0,
    var cbo_child_level:Int?=0,
    var joining_date:Long?=0,
    var leaving_date:Long?=0,
    var settlement_status:Int?=0,
    var leaving_reason:Int?=0,
    var status:Int?=0,
    var is_active:Int?=0,
    var entry_source:Int?=0,
    var is_edited:Int?=0,
    var created_by:String?="",
    var updated_by:String?="",
    var created_date:Long?=0


)