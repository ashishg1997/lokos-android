package com.microware.cdfi.api.model

data class MappingDataModel (
    var shg_id:Long?=0,
    var guid:String?="",
    var shg_name:String?="",
    var shg_formation_date:Long?=0,
    var shg_revival_date:Long?=0,
    var shg_code:Long?=0,
    var parent_cbo_code:Long?=0,
    var federation_joining_date:Long?=0,
    var is_active:Int?=0,
    var is_edited:Int?=0,
    var status:Int?=0,
    var leaving_reason:Int?=0,
    var settlement_status:Int?=0
  /*  var cbo_child_name:String?="",
    var cbo_child_village_id:Int?=0,
    var cbo_level:Int?=0,
    var cbo_child_level:Int?=0,
    var joining_date:Long?=0,
    var leaving_date:Long?=0,
    var settlement_status:Int?=0,
    var leaving_reason:String?="",
    var status:Int?=1,
    var is_active:Int?=1,
    var entry_source:Int?=1,
    var is_edited:Int?=1,
    var created_by:String?="",
    var updated_by:String?="",
    var created_date:Long?=0*/
)