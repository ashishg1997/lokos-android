package com.microware.cdfi.api.model

data class MemberDownloadStatus(
    val activationStatus: Int,
    val approve_status: Int,
    val checker_remarks: String?,
    val code: String?,
    val guid: String,
    val memberDownloadStatus: Any?,
    val bankDownloadStatus: List<BankDownloadStatus>? = null,
    val kycDownloadStatus: List<KycDownloadStatus>? = null
)


data class BankDownloadStatus(
    val activationStatus: Int?,
    val guid: String?
)

data class KycDownloadStatus(
    val activationStatus: Int?,
    val guid: String?
)