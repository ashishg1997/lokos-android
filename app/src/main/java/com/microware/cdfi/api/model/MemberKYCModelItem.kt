package com.microware.cdfi.api.model

data class MemberKYCModelItem(
    val created_by: String="",
    val created_date: Long=0,
    val device:  Int=0,
    val is_edited:  Int=0,
    val kyc_front_doc_encp_name:  String="",
    val kyc_front_doc_orig_name:  String="",
    val kyc_guid:  String="",
    val kyc_id:  String="",
    val kyc_rear_doc_encp_name:  String="",
    val kyc_rear_doc_orig_name:  String="",
    val kyc_type:  Int=0,
    val entry_source:  Int=0,
    val member_code:  Int=0,
    val member_guid:  String="",
    val updated_by:  String="",
    val updated_date:  Int=0,
    val uploaded_by:  String="",
    val uploaded_date:  Int=0
)