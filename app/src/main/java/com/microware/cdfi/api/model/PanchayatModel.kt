package com.microware.cdfi.api.model

data class PanchayatModel (
    var panchayat_id:Long,
    var panchayat_name_en:String
)