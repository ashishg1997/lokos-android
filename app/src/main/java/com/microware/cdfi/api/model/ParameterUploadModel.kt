package com.microware.cdfi.api.model

data class ParameterUploadModel (

    var state_id:Int?=0,
    var district_id:Int?=0,
    var block_id:Int?=0,
    var panchayat_id:Int?=0,
    var village_id:Int?=0,
    var cbo_id:Long?=0,
    var sort_column:String?="",
    var sort_direction:String?="",
    var flag:String?="",
    var size:Int?=0
)