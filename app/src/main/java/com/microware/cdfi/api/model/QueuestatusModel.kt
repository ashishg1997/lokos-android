package com.microware.cdfi.api.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class QueuestatusModel(
    val id: Int,
    val transaction_no:String,
    val shg_guid:String?,
    val member_guid:String?,
    val transaction_type:Int?,
    val status:Int,
    val remarks:String,
    val member_name:String
)