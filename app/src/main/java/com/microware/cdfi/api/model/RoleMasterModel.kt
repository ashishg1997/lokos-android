package com.microware.cdfi.api.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

data class RoleMasterModel(
    val roleId: String,
    val roleName: String,
    val status: String,
    val categoryId: Int,
    val levelId: Int,
    val typeId: Int
)