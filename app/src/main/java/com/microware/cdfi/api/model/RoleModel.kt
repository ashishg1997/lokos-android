package com.microware.cdfi.api.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

data class RoleModel(
    val userRoleRightsMapId: String,
    val userId: String,
    val categoryId: String,
    val levelId: Int,
    val typeId: Int,
    val stateId: Int,
    val districtId: Int,
    val blockId: Int,
    val panchayatId: String,
    val villageId: String?,
    val roleMaster: RoleMasterModel?
)