package com.microware.cdfi.api.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.microware.cdfi.entity.subcommitee_memberEntity

@JsonIgnoreProperties(ignoreUnknown = true)
data class SubcommitteeModel(
    var subcommitee_guid:String="",
    var subcommitee_code:Long?=0,
    var subcommitee_type_id:Int?=0,
    var cbo_id:Long?=0,
    var cbo_guid:String?="",
    var cbo_code:Long?=0,
    var fromdate:Long?=0,
    var todate:Long?=0,
    var is_active:Int?=0,
    var entry_source:Int?=0,
    var status:Int?=0,
    var is_edited:Int?=0,
    var last_uploaded_date:Long?=0,
    var uploaded_by:String?="",
    var created_date:Long?=0,
    var created_by:String?="",
    var updated_date:Long?=0,
    var updated_by:String?="",
    var is_upload:Int?=0,
    var is_complete:Int?=0,
    var subCommitteeMemberList: List<subcommitee_memberEntity>? = null
)
