package com.microware.cdfi.api.model

import androidx.room.ColumnInfo
import com.microware.cdfi.entity.VoShgMemberEntity
import com.microware.cdfi.entity.VoShgMemberPhoneEntity

data class VoShgMappingModel(
    val guid: String,
    val vo_guid: String,
    val vo_code: Long,
    val shg_formation_date: Long,
    val joining_date: Long,
    val leaving_date: Long,
    val shg_id: Int,
    val shg_name: String,
    val shg_name_local: String,
    var status: Int? = 0,
    var settlement_status: Int? = 0,
    var leaving_reason: Int? = 0,
    val memberDetailsList: List<VoShgMemberModel>? = null

)