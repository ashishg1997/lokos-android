package com.microware.cdfi.api.model

import androidx.room.ColumnInfo
import androidx.room.PrimaryKey
import com.microware.cdfi.entity.VoShgMemberPhoneEntity

data class VoShgMemberModel (
    var designation: Int,
    var joining_date: Int,
    var member_guid: String,
    var vo_guid: String?,
    var member_id: Int?,
    var member_name: String?,
    var member_name_local: String?,
    var relation: Int?,
    var relation_name_local: String?,
    var cbo_guid: String?,
    val memberPhoneDetailsList: List<VoShgMemberPhoneEntity>? = null
)