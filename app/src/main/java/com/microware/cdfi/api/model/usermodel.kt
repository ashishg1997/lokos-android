package com.microware.cdfi.api.model


data class usermodel(
//    val id: Int?,
    val userId: String,
    val userName: String?,
//    val role: String?,
//    val state_code: Int?,
//    val district_code: Int?,
//    val block_code: Int?,
//    val panchayat_code: Int?,
//    val village_code: Int?,
    val designation: String?,
    val mobileNo: String?,
    val emailId: String?,
    val userRoleRightsMap: RoleModel?
)