package com.microware.cdfi.api.response

import com.google.gson.annotations.SerializedName
import com.microware.cdfi.entity.FederationEntity

class FederationListResponse {

    @SerializedName("federation")
    var federationList: List<FederationEntity>? = null
}