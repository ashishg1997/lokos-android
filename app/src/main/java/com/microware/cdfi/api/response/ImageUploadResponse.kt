package com.microware.cdfi.api.response

import com.google.gson.annotations.SerializedName

class ImageUploadResponse {

    @SerializedName("code")
    var status: Int? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("image_name")
    var image_name: String? = null

}