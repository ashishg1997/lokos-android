package com.microware.cdfi.api.response

import com.google.gson.annotations.SerializedName
import com.microware.cdfi.api.model.ClfMappingDataModel
import com.microware.cdfi.api.model.MappingDataModel

class MappingResponse {

    var totalRecord:Int=0

    @SerializedName("recordsList")
    var mappingList: List<MappingDataModel>? = null

}