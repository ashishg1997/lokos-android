package com.microware.cdfi.api.response

import com.google.gson.annotations.SerializedName
import com.microware.cdfi.api.model.*
import com.microware.cdfi.entity.*
import com.microware.cdfi.entity.MstVOCOAEntity

class MastersResponse {

    @SerializedName("statesMasterList")
    var mst_State: List<StateEntity>? = null

    @SerializedName("districtMasterList")
    var mst_District: List<DistrictEntity>? = null

    @SerializedName("blockMasterList")
    var mst_Block: List<BlockEntity>? = null

    @SerializedName("panchayatMasterList")
    var mstPanchayat: List<PanchayatEntity>? = null

    @SerializedName("villageMasterList")
    var mstVillage: List<VillageEntity>? = null

    @SerializedName("labelMasterList")
    var mstlabelMasterList: List<LabelMasterEntity>? = null

    @SerializedName("cadreCategoryMasterList")
    var cadreCategoryMasterList: List<CardreCateogaryEntity>? = null

 @SerializedName("cadreRoleMasterlist")
    var cadreRoleMasterlist: List<cadreRoleEntity>? = null

    @SerializedName("subCommitteeMasterList")
    var subCommitteeMasterList: List<subcommitee_masterEntity>? = null

    @SerializedName("userMasterList")
    var mstuser: List<usermodel>? = null

    @SerializedName("responseCodeMasterList")
    var mstResponse: List<ResponseCodeEntity>? = null

    @SerializedName("lookUpTableList")
    var mstlookup: List<LookupEntity>? = null

    @SerializedName("bankBranchMasterList")
    var mstbankBranch: List<Bank_branchEntity>? = null

    @SerializedName("bankMasterList")
    var mstBankMaster: List<BankEntity>? = null

    @SerializedName("fundingAgencyMasterList")
    var mstFundingMaster: List<FundingEntity>? = null


    @SerializedName("fundTypeMasterList")
    var mstFundType: List<FundTypeEntity>? = null

    @SerializedName("coaSubMasterList")
    var mstCoa: List<MstCOAEntity>? = null

    @SerializedName("voCOASubMasterList")
    var mstVoCoa: List<MstVOCOAEntity>? = null

    @SerializedName("accessToken")
    var accessToken: String? = null

    @SerializedName("msg")
    var msg: String? = null

    @SerializedName("nonSyncData")
    var groupResponseStatus: List<GroupResponseModelItem>? = null

    @SerializedName("mappedShgList")
    var VO_Shg_mapped_response: List<VoShgMappingModel>? = null

    @SerializedName("nonSyncFederationData")
    var federationResponseStatus: List<FedrationResponseModelItem>? = null

    @SerializedName("mappedVOList")
    var CLF_VO_Shg_mapped_response: List<ClfVoShgMappingModel>? = null

    @SerializedName("responseCode")
    var responseCode:Int?=null

    @SerializedName("federationId")
    var federationId: String? = null

    @SerializedName("loanProductMasterList")
    var mstLoanProduct: List<MstproductEntity>? = null
}