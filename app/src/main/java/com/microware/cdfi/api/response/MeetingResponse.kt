package com.microware.cdfi.api.response

import com.google.gson.annotations.SerializedName
import com.microware.cdfi.api.meetinguploadmodel.MeetingDownloadData

class MeetingResponse {

    @SerializedName("response")
    var response : String? = null

    @SerializedName("result")
    var result : String? = null

    @SerializedName("downLoadMtgList")
    var downLoadMtgList: List<MeetingDownloadData>? = null

}