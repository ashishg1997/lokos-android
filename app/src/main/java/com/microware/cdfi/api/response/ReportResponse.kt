package com.microware.cdfi.api.response

import com.google.gson.annotations.SerializedName

class ReportResponse(

    @field:SerializedName("reports")
    val reports: List<ReportsItem?>? = null
)

data class ReportsItem(

    @field:SerializedName("reportType")
    val reportType: String? = null,

    @field:SerializedName("displayType")
    val displayType: String? = null,

    @field:SerializedName("reportId")
    val reportId: Int? = null,

    @field:SerializedName("reportName")
    val reportName: String? = null,

    @field:SerializedName("reportCategory")
    val reportCategory: String? = null,

    @field:SerializedName("reportDate")
    var reportDate: String? = null
)
