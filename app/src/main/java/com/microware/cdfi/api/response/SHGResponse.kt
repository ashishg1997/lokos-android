package com.microware.cdfi.api.response

import com.google.gson.annotations.SerializedName
import com.microware.cdfi.api.model.GroupResponseDatanotInsertedModel
import com.microware.cdfi.api.model.GroupResponseModel
import com.microware.cdfi.api.model.GroupResponseModelItem
import com.microware.cdfi.api.model.GroupResponseTransactionModel
import com.microware.cdfi.entity.*

class SHGResponse {

    /*@SerializedName("shgProfileMobileList")
    var shgProfile: List<SHGEntity>? = null

    @SerializedName("cboAddressDetailsList")
    var addressList: List<AddressEntity>? = null

    @SerializedName("cboPhoneNoMobileDetailsList")
    var phoneList: List<Cbo_phoneEntity>? = null

    @SerializedName("cboBankMobileDetailsList")
    var bankList: List<Cbo_bankEntity>? = null

    @SerializedName("memberProfileMobileList")
    var memberProfileList: List<MemberEntity>? = null

    @SerializedName("memberAddressesList")
    var memberaddressList: List<member_addressEntity>? = null

    @SerializedName("memberBankList")
    var memberbankList: List<MemberBankAccountEntity>? = null

    @SerializedName("memberKYCDetailsList")
    var member_kyc_List: List<Member_KYC_Entity>? = null

    @SerializedName("memberPhoneNoDetailsList")
    var member_phone_List: List<MemberPhoneDetailEntity>? = null


    @SerializedName("memberSystemTagsList")
    var member_tag_List: List<MemberSystemTagEntity>? = null

    @SerializedName("shgProfileMobileList")
    var memberResponse: List<GroupResponseModelItem>? = null

    @SerializedName("nonSyncData")
    var groupResponseDatanotInserted: List<GroupResponseDatanotInsertedModel>? = null*/

    @SerializedName("transationStatus")
    var grouptransactionResponse: List<GroupResponseTransactionModel>? = null
}