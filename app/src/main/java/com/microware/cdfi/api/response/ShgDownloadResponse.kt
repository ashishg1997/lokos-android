package com.microware.cdfi.api.response

import com.google.gson.annotations.SerializedName
import com.microware.cdfi.api.model.SHGUploadModel
import com.microware.cdfi.entity.SHGEntity

class ShgDownloadResponse {

    @SerializedName("shgProfile")
    var shgUploadModel: SHGUploadModel? = null
}