package com.microware.cdfi.api.response

import com.google.gson.annotations.SerializedName
import com.microware.cdfi.entity.SHGEntity

class ShgListResponse {

    @SerializedName("shg")
    var shgProfile: List<SHGEntity>? = null
}