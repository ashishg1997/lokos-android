package com.microware.cdfi.api.response

import com.google.gson.annotations.SerializedName
import com.microware.cdfi.api.vomeetingmodel.VoDownloadModel
import com.microware.cdfi.api.vomeetingmodel.VoMtgDataModel

class VoDownloadMeetingResponse {

    @SerializedName("response")
    var response : String? = null

    @SerializedName("result")
    var result : String? = null

    @SerializedName("downLoadVoMtgList")
    var downLoadVoMtgList: List<VoDownloadModel>? = null

}