package com.microware.cdfi.api.response

import com.google.gson.annotations.SerializedName
import com.microware.cdfi.api.model.SHGUploadModel
import com.microware.cdfi.api.model.VODownloadModel
import com.microware.cdfi.api.model.VOUploadModel
import com.microware.cdfi.entity.SHGEntity

class VoDownloadResponse {

    @SerializedName("singleFederation")
    var voDownloadModel: VODownloadModel? = null
}