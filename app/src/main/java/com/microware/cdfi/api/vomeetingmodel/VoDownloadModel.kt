package com.microware.cdfi.api.vomeetingmodel

import com.google.gson.annotations.SerializedName
import com.microware.cdfi.entity.voentity.VoGroupLoanEntity
import com.microware.cdfi.entity.voentity.VoLoanApplicationEntity
import com.microware.cdfi.entity.voentity.VoMemLoanEntity

data class VoDownloadModel(

    @SerializedName("voMeeting")
    var voMeeting: VoMtgDataModel? = null,

    @SerializedName("voLoanApplicationList")
    var voLoanApplicationList: List<VoLoanApplicationEntity>? = null,

    @SerializedName("voMemberLoanList")
    var voMemberLoanList: List<VoMemLoanDataModel>? = null,

    @SerializedName("voGroupLoanList")
    var voGroupLoanList: List<VoGroupLoanDataModel>? = null

)