package com.microware.cdfi.api.vomeetingmodel

import com.google.gson.annotations.SerializedName
import com.microware.cdfi.entity.voentity.VoLoanApplicationEntity

data class VoMeetingUploadData(

    @SerializedName("voMeeting")
    var voMeeting: VoMtgDataModel? = null,

    @SerializedName("voLoanApplicationList")
    var voLoanApplicationList: List<VoLoanApplicationEntity>? = null,

    @SerializedName("voMemberLoanList")
    var voMemberLoanList: List<VoMemLoanDataModel>? = null,

    @SerializedName("voGroupLoanList")
    var voGroupLoanList: List<VoGroupLoanDataModel>? = null

)