package com.microware.cdfi.api.vomeetingmodel

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName
import com.microware.cdfi.entity.voentity.VoMemLoanScheduleEntity

data class VoMemLoanDataModel(
    @SerializedName("uid") var uid:Long? = 0,
    @SerializedName("voMtgDetUid") var voMtgDetUid:Long? = 0,
    @SerializedName("loanApplicationId") var loanApplicationId:Long? = 0,
    @SerializedName("mtgGuid") var mtgGuid:String? ="",
    @SerializedName("cboId") var cboId:Long? = 0,
    @SerializedName("memId") var memId:Long? = 0,
    @SerializedName("loanNo") var loanNo:Int? = 0,
    @SerializedName("mtgNo") var mtgNo:Int? = 0,
    @SerializedName("mtgDate") var mtgDate:Long? = 0,
    @SerializedName("installmentDate") var installmentDate:Long? = 0,
    @SerializedName("originalLoanAmount") var originalLoanAmount:Int? = 0,
    @SerializedName("amount") var amount:Int? = 0,
    @SerializedName("loanPurpose") var loanPurpose:Int? = 0,
    @SerializedName("fundType") var fundType:Int? = 0,
    @SerializedName("interestRate") var interestRate:Double? = 0.0,
    @SerializedName("period") var period:Int? = 0,
    @SerializedName("principalOverdue") var principalOverdue:Int? = 0,
    @SerializedName("interestOverdue") var interestOverdue:Int? = 0,
    @SerializedName("completionFlag") var completionFlag:Boolean? = null,
    @SerializedName("loanType") var loanType:Int? = 0,
    @SerializedName("loanSource") var loanSource:Int? = 0,
    @SerializedName("modePayment") var modePayment:Int? = 0,
    @SerializedName("bankCode") var bankCode:String? = "",
    @SerializedName("transactionNo") var transactionNo:String? ="",
    @SerializedName("installmentFreq") var installmentFreq:Int? = 0,
    @SerializedName("moratoriumPeriod") var moratoriumPeriod:Int? = 0,
    @SerializedName("createdBy") var createdBy:String? ="",
    @SerializedName("createdOn") var createdOn:Long? = 0,
    @SerializedName("principalRepaid") var principalRepaid:Int? = 0,
    @SerializedName("interestRepaid") var interestRepaid:Int? = 0,
    @SerializedName("disbursementDate") var disbursementDate:Long? = 0,
    @SerializedName( "rescheduleReason") var rescheduleReason: Int? = 0,
    @SerializedName("originalInterestRate") var originalInterestRate: Double? = 0.0,
    @SerializedName( "originalPeriod") var originalPeriod: Int? = 0,
    @SerializedName("noOfLoans") var noOfLoans: Int? = 0,
    @SerializedName("voMemberLoanScheduleList") var voMemberLoanScheduleList:List<VoMemLoanScheduleEntity>? = null
)