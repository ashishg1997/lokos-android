package com.microware.cdfi.api.vomeetingmodel

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.microware.cdfi.entity.voentity.VoFinTxnDetGrpEntity
import com.microware.cdfi.entity.voentity.VoFinTxnEntity
import com.microware.cdfi.entity.voentity.VoGroupLoanTxnEntity
import com.microware.cdfi.entity.voentity.VoMtgDetEntity

@JsonIgnoreProperties(ignoreUnknown = true)
data class VoMtgDataModel(

    var uid: Long? = 0,
    var cboId: Long? = 0,
    var mtgGuid: String = "",
    var mtgType: Int? = 0,
    var mtgNo: Int? = 0,
    var mtgDate: Long? = 0,
    var flagOpen: String? = "",
    var mtgFrq: String? = "",
    var flagDow: String? = "",
    var daysElapsed: Int? = 0,
    var openingBalance: Int? = 0,
    var closingBalance: Int? = 0,
    var totalMeetingHeld: Int? = 0,
    var expectedDate: Long? = 0,
    var approvalStatus: Int? = 0,
    var actionBy: String? = "",
    var actionOn: Long? = 0,
    var mnthCompSav: Int? = 0,
    var totalAttendance: Int? = 0,
    var savCompOb: Int? = 0,
    var savComp: Int? = 0,
    var savCompCb: Int? = 0,
    var savCompWithdrawal: Int? = 0,
    var savVolOb: Int? = 0,
    var savVol: Int? = 0,
    var savVolCb: Int? = 0,
    var savVolWithdrawal: Int? = 0,
    var memOtherPayments: Int? = 0,
    var noLoansDisbursed: Int? = 0,
    var amtLoansDisbursed: Int? = 0,
    var amtLoansDemand: Int? = 0,
    var noLoansRepaid: Int? = 0,
    var amtLoansRepaid: Int? = 0,
    var noLoanOverdue: Int? = 0,
    var amtLoansOverdue: Int? = 0,
    var noLoansOs: Int? = 0,
    var amtLoansOs: Int? = 0,
    var amtInterestReceived: Int? = 0,
    var noLoansNpa: Int? = 0,
    var amtLoansNpa: Int? = 0,
    var noLoansReceivedExt: Int? = 0,
    var amtLoansReceivedExt: Int? = 0,
    var noLoansRepaidExt: Int? = 0,
    var amtLoansRepaidExt: Int? = 0,
    var noLoanOverdueExt: Int? = 0,
    var amtLoansOverdueExt: Int? = 0,
    var amtLoansDemandExt: Int? = 0,
    var noLoansOsExt: Int? = 0,
    var amtLoansOsExt: Int? = 0,
    var amtInterestPaidExt: Int? = 0,
    var noLoansNpaExt: Int? = 0,
    var amtLoansNpaExt: Int? = 0,
    var otherReceipts: Int? = 0,
    var otherPayments: Int? = 0,
    var depositedCash: Int? = 0,
    var withdrawnCash: Int? = 0,
    var openingBalanceCash: Int? = 0,
    var closingBalanceCash: Int? = 0,
    var createdBy: String? = "",
    var createdOn: Long? = 0,
    var updatedBy: String? = "",
    var updatedOn: Long? = 0,
    var zeroMtgCashInHand: Int? = 0,
    var zeroMtgCashInTransit: Int? = 0,
    var balanceDate: Long? = 0,
    var active: Int? = 0,
    var checkerRemarks: String? = "",
    var summaryFlag: String? = "",
    var voMeetingDetailsList: List<VoMtgDetailDataModel>? = null,
    var voFinanceTransactionList: List<VoFinTxnEntity>? = null,
    var voFinanceTransactionDetailGroupList: List<VoFinTxnDetGrpEntity>? = null,
    var voGroupLoanTransactionList: List<VoGroupLoanTxnEntity>? = null

    )