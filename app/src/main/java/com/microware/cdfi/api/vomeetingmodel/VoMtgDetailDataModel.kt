package com.microware.cdfi.api.vomeetingmodel

import androidx.room.ColumnInfo
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.microware.cdfi.entity.voentity.VoFinTxnDetMemEntity
import com.microware.cdfi.entity.voentity.VoMemLoanTxnEntity
import com.microware.cdfi.entity.voentity.VoMemSettlementEntity


@JsonIgnoreProperties(ignoreUnknown = true)
data class VoMtgDetailDataModel(
    var cboId: Long? = 0,
    var memId: Long? = 0,
    var mtgGuid: String = "",
    var mtgNo: Int? = 0,
    var mtgDate: Long? = 0,
    var sNo: Int? = 0,
    var childCboName: String? = "",
    var attendance: String? = "",
    var ec1: String? = "",
    var ec2: String? = "",
    var ec3: String? = "",
    var ec4: String? = "",
    var ec5: String? = "",
    var attendanceOther: Int? = 0,
    var attendanceExternal: Int? = 0,
    var status: String? = "",
    var zeroMtgAttn: Int? = 0,
    var createdBy: String? = "",
    var createdOn: Long? = 0,
    var savCompOb: Int? = 0,
    var savComp: Int? = 0,
    var savCompCb: Int? = 0,
    var savVolOb: Int? = 0,
    var savVol: Int? = 0,
    var savVolCb: Int? = 0,
    var savVolWithdrawal: Int? = 0,
    var settlementStatus: Int? = 0,
    var voMemberLoanTransactionList: List<VoMemLoanTxnEntity>? = null,
    var voFinanceTransactionDetailMemberList: List<VoFinTxnDetMemEntity>? = null,
    var voMemberSettlementList: List<VoMemSettlementEntity>? = null
)