package com.microware.cdfi.api.vomodel

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class FundLinkageDataModel(
    var uid: Int?,
    var description: String?,
    var receipt_payment: Int?,
    var type: String?,
    var short_description: String?,
    var language_id: String?,
    var bankcode: String?
)