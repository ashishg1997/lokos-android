package com.microware.cdfi.api.vomodel

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class VOBankTransactionModel (
    val cbo_id:Long,
    val mtg_no:Long,
    val bank_from:String,
    val bank_to:String,
    val amount:Int,
    val auid:Int)