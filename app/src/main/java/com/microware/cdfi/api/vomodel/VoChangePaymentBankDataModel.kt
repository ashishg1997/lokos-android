package com.microware.cdfi.api.vomodel

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class VoChangePaymentBankDataModel(
    var cbo_bank_id: Long?,
    var account_no: String?,
    var bank_guid: String?,
    var bank_code: String?,
    var bank_id: Int?,
    var is_default: Int?,
    var bank_branch: String?,
    var ifsc_code: String?,
    var modePayment: Int?
)