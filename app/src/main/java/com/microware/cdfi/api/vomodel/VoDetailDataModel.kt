package com.microware.cdfi.api.vomodel
import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class VoDetailDataModel(
    var cbo_id:Long? = 0,
    var cbo_guid:String? = "",
    var ec_member_code:Long? = 0,
    var designation:Int? = 0,
    var joining_date:Long? = 0,
    var status:Int? = 0,
    var is_active:Int? = 0,
    var member_name:String? = "",
    var shg_name:String? = "",
    var shg_id:Long? =0

)