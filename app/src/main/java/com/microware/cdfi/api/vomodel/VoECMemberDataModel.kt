package com.microware.cdfi.api.vomodel

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class VoECMemberDataModel(
    var cbo_id: Long? = 0,
    var cbo_guid: String? = "",
    var member_id: Long? = 0,
    var designation: Int? = 0,
    var member_guid: String? = "",
    var ec_member_code: Long? = 0,
    var member_name: String? = "",
    var ec_cbo_code: Long? = 0,
    var ec1: String? = "",
    var ec2: String? = "",
    var ec3: String? = "",
    var ec4: String? = "",
    var ec5: String? = "",
    var attendanceOther: Int? = 0
)