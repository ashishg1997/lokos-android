package com.microware.cdfi.api.vomodel

import androidx.room.ColumnInfo
import androidx.room.Ignore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonIgnoreType
import com.microware.cdfi.entity.*
import org.modelmapper.internal.bytebuddy.implementation.bind.annotation.IgnoreForBinding

@JsonIgnoreProperties(ignoreUnknown = true)
data class VoLoanListModel(

    var uid: Int?,
    var cboId: Long?,
    var memId: Long?,
    var mtgGuid: String?,
    var mtgNo: Int?,
    var mtgDate: Long?,
//    var group_m_code: Long?,
    var childCboName: String?,
    var loanApplicationId: Long?,
    var loanNo: Int?,
    var fundType: Int?,
    var requestDate: Long?,
    var loanFee: Int?,
    var amtDemand: Int?,
    var amtSanction: Int?,
    var amtDisbursed: Int?,
    var approvalDate: Long?,
    var tentativeDate: Long?,
    var original_loan_amount: Int?,
    var noOfLoans: Int?


)
