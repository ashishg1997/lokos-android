package com.microware.cdfi.api.vomodel

import androidx.room.ColumnInfo
import androidx.room.Ignore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonIgnoreType
import com.microware.cdfi.entity.*
import org.modelmapper.internal.bytebuddy.implementation.bind.annotation.IgnoreForBinding

@JsonIgnoreProperties(ignoreUnknown = true)
data class VoLoanRepaymentListModel(

    val uid: Int?,
    val cboId: Long?,
    val memId: Long?,
    val mtgGuid: String?,
    val mtgNo: Int?,
    val mtgDate: Long?,
    val childCboName: String?,
    val loanNo: Int?,
    val loanOp: Int?,
    val loanOpInt: Int?,
    val loan_paid: Int?,
    val loan_paid_int: Int?,
    val principal_demand: Int?,
    val principal_demand_ob: Int?,
    val principal_demand_cb: Int?,
    val int_accrued: Int?,
    val int_accrued_op: Int?
)
