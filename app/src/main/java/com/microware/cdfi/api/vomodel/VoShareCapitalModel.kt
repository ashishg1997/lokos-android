package com.microware.cdfi.api.vomodel

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class VoShareCapitalModel(
    val uid: Int?,
    val cboId: Long?,
    val memId: Long?,
    val auid: Int?,
    val member_aid: Long?,
    val mtgGuid: String?,
    val childCboName: String?,
    val mtgNo: Int?,
    val type: String?,
    val amount: Int?,
    val dateRealisation: Int?,
    val modePayment: Int?,
    val transactionNo: String?
)