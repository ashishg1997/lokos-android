package com.microware.cdfi.application

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.room.Room
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.microware.cdfi.database.AppDataBase
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.Validate

open class CDFIApplication : Application() {


    companion object {

        var database1: AppDataBase? = null
        var database2: AppDataBase? = null
        var database3: AppDataBase? = null
        var database4: AppDataBase? = null

        var database: AppDataBase? = null
        var context: Context? = null
        var validate: Validate? = null

        fun changedb(): AppDataBase? {

            database1!!.openHelper.readableDatabase.isOpen
            database2!!.openHelper.readableDatabase.isOpen
            database3!!.openHelper.readableDatabase.isOpen
            database4!!.openHelper.readableDatabase.isOpen
            when {
                validate!!.returnRoleValue(validate!!.RetriveSharepreferenceString(AppSP.Roleid)) == 310 -> {
                    database = database1
                }
                validate!!.returnRoleValue(validate!!.RetriveSharepreferenceString(AppSP.Roleid)) == 410 -> {
                    database = database2
                }
                validate!!.returnRoleValue(validate!!.RetriveSharepreferenceString(AppSP.Roleid)) == 450 -> {
                    database = database3
                }
                validate!!.returnRoleValue(validate!!.RetriveSharepreferenceString(AppSP.Roleid)) == 510 -> {
                    database = database4
                }
                else -> {

                    database = database1

                }
            }
            return database
        }

    }


    override fun onCreate() {
        super.onCreate()
        context = applicationContext
        validate = Validate(applicationContext)
        database1 = Room.databaseBuilder(applicationContext, AppDataBase::class.java, "CDFIDB")
            .addMigrations().allowMainThreadQueries().build()
        database2 = Room.databaseBuilder(applicationContext, AppDataBase::class.java, "CDFIVODB")
            .addMigrations().allowMainThreadQueries().build()
        database3 = Room.databaseBuilder(applicationContext, AppDataBase::class.java, "CDFIAPPDB")
            .addMigrations().allowMainThreadQueries().build()
        database4 = Room.databaseBuilder(applicationContext, AppDataBase::class.java, "CDFICLFDB")
            .addMigrations().allowMainThreadQueries().build()
        when {
            validate!!.returnRoleValue(validate!!.RetriveSharepreferenceString(AppSP.Roleid)) == 310 -> {
                database = database1
            }
            validate!!.returnRoleValue(validate!!.RetriveSharepreferenceString(AppSP.Roleid)) == 410 -> {
                database = database2
            }
            validate!!.returnRoleValue(validate!!.RetriveSharepreferenceString(AppSP.Roleid)) == 450 -> {
                database = database3
            }
            validate!!.returnRoleValue(validate!!.RetriveSharepreferenceString(AppSP.Roleid)) == 510 -> {
                database = database4
            }
            else -> {

                database = database1

            }
        }


    }

}
