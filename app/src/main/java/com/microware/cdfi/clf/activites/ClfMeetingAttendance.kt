package com.microware.cdfi.clf.activites

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.microware.cdfi.R
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.clf.Constants
import com.microware.cdfi.clf.adapters.ClfAttendanceTabAdapter
import com.microware.cdfi.clf.models.get_meeting_participants.GetMeetingParticipants
import com.microware.cdfi.clf.models.meetings.GetMeetingsResponse
import com.microware.cdfi.utility.*
import kotlinx.android.synthetic.main.activity_clf_meeting_attendance.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.ref.WeakReference

class ClfMeetingAttendance : AppCompatActivity() {

    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: ApiInterface? = null
    var validate: Validate? = null
    lateinit var tabLayout: TabLayout
    lateinit var viewPager: ViewPager

    private var txtDate: TextView? = null
    private var txtMeetingNum: TextView? = null

    var token: String = ""
    private var cboId: String? = null
    private var uid: String? = null
    private var meetingDate: String? = null
    private var meetingNum: String? = null
    private var meetingType: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_clf_meeting_attendance)
        validate = Validate(this)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()

        initViews()
    }

    private fun initViews() {
        tabLayout = findViewById(R.id.tabLayout)
        viewPager = findViewById(R.id.viewPager)
        txtDate = findViewById(R.id.txtDate)
        txtMeetingNum = findViewById(R.id.txtMeetingNum)

        ic_Back.setOnClickListener {
            onBackPressed()
        }

        radioSavings.setOnClickListener {
            startActivity(Intent(applicationContext, ClfMeetingSaving::class.java))
        }
        radioLoan.setOnClickListener {
            startActivity(Intent(applicationContext, LoanRequestActivity::class.java))
        }

        radioLoan.setOnClickListener {
            startActivity(Intent(applicationContext, LoanRequestActivity::class.java))
        }

        setupTabs()
        callApi()
    }

    private fun callApi() {
        val prefUtil = PrefUtil(WeakReference(this))
        val meetingData = prefUtil.get<GetMeetingsResponse>("meeting_response")

        txtDate!!.text = "Date: " + DateUtil().getDate(meetingData!!.mtgDate!!)
        txtMeetingNum!!.text = "Meeting Num: " + meetingData!!.mtgNo

        token = Constants.Token
        //        getMeetingParticipants(meetingData.uid!!)

    }

    private fun setupTabs() {
        tabLayout.addTab(tabLayout.newTab().setText("EC Member Attendance"))
        tabLayout.addTab(tabLayout.newTab().setText("VO Attendance"))
        tabLayout.tabGravity = TabLayout.GRAVITY_FILL
        val adapter = ClfAttendanceTabAdapter(
            this, supportFragmentManager,
            tabLayout.tabCount
        )
        viewPager.adapter = adapter
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }


    private fun getMeetingParticipants(uid: Int) {
        val progressBar = ProgressDialog(this)
        progressBar.setCancelable(false)
        progressBar.setMessage(this.getString(R.string.msgLoading))
        progressBar.show()

        val user = "IUP_BPMQA"
        val call = apiInterface?.getMeetingParticipants(
            "application/json",
            token,
            user,
            uid
        )

        call?.enqueue(object : Callback<GetMeetingParticipants> {
            override fun onFailure(callCount: Call<GetMeetingParticipants>, t: Throwable) {
                t.printStackTrace()
                Log.e("onFailure", "" + t)
                progressBar.dismiss()
            }

            override fun onResponse(
                call: Call<GetMeetingParticipants>,
                response: Response<GetMeetingParticipants>
            ) {

                Log.e("getMeetingPar --> ", "" + response.code())
                Log.e("getMeetingPar --> ", "" + response.body())
                progressBar.dismiss()
                if (response.isSuccessful) {

                    try {

                        if (response.body()!!.clfMtgEcParticipants!!.isNotEmpty()) {
                            val prefUtil = PrefUtil(WeakReference(this@ClfMeetingAttendance))
                            prefUtil.put(
                                response.body()!!.clfMtgEcParticipants!!.toMutableList(),
                                "clfMtgEcParticipants"
                            )
                        }
                        if (response.body()!!.clfMtgVoPartcipants!!.isNotEmpty()) {
                            val prefUtil = PrefUtil(WeakReference(this@ClfMeetingAttendance))
                            prefUtil.put(
                                response.body()!!.clfMtgVoPartcipants,
                                "clfMtgVoPartcipants"
                            )
                        }

                        setupTabs()

                    } catch (ex: Exception) {
                        ex.printStackTrace()

                    }

                } else {
                    progressBar.dismiss()
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()
        radioAttendance.isChecked = true
    }


}