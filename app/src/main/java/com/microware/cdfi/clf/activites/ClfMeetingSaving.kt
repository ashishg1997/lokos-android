package com.microware.cdfi.clf.activites

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.microware.cdfi.R
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.clf.Constants
import com.microware.cdfi.clf.models.get_meeting_participants.ClfMtgVoPartcipants
import com.microware.cdfi.clf.models.get_meeting_participants.GetMeetingParticipants
import com.microware.cdfi.clf.models.meetings.GetMeetingsResponse
import com.microware.cdfi.clf.models.saving.*
import com.microware.cdfi.utility.*
import kotlinx.android.synthetic.main.activity_clf_meeting_saving.*
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.fragment_cheque_receipt.*
import kotlinx.android.synthetic.main.meeting_detail_item.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.ref.WeakReference
import java.lang.reflect.Type
import java.security.Timestamp
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class ClfMeetingSaving : AppCompatActivity() {

    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: ApiInterface? = null
    var validate: Validate? = null

    private var txtDate: TextView? = null
    private var txtMeetingNum: TextView? = null

    private var cboId: String? = null
    private var uid: String? = null
    private var meetingDate: String? = null
    private var meetingNum: String? = null
    private var meetingType: String? = null
    var Savinglist: ArrayList<SavingRequestModelItem>? = arrayListOf()
    var adapter: ClfSavingAdapter? = null
    var token: String = ""
    var meetingData: GetMeetingsResponse? = null
    var clfMtgVoPartcipants: ArrayList<ClfMtgVoPartcipants>? = null
    var voSavingData: ArrayList<GetMeetingVoSavingListResponseItem>? = null
    var progressBar: ProgressDialog? = null
    var savingId = "0"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_clf_meeting_saving)

        validate = Validate(this)

        validate!!.RemoveSharepreference(AppSP.ClfSavingList)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        Savinglist = ArrayList<SavingRequestModelItem>()
        voSavingData = ArrayList<GetMeetingVoSavingListResponseItem>()

        var gson = Gson()
        var SavinglistString = gson.toJson(Savinglist)
        validate!!.SaveSharepreferenceString(AppSP.ClfSavingList, SavinglistString)

        initViews()

        val prefUtil = PrefUtil(WeakReference(this))
        meetingData = prefUtil.get<GetMeetingsResponse>("meeting_response")
        tv_nam.text = "Meeting Type - " + meetingData!!.mtgType.toString().toUpperCase()
        tv_code.text = meetingData!!.mtgNo.toString()
        tv_date.text = meetingData!!.mtgDate.toString()
        tv_mtgNum.visibility = View.GONE
        iv_stauscolor.visibility = View.GONE


        var formatter = SimpleDateFormat("dd/MM/yyyy");
        var dateString = formatter.format(Date((meetingData!!.mtgDate)!!.toLong()));
        tv_date!!.setText(dateString);

        ic_Back.setOnClickListener {
            onBackPressed()
        }

        btn_save.setOnClickListener {

            var savingListdata = validate!!.RetriveSharepreferenceString(AppSP.ClfSavingList)

            val listType: Type =
                object : TypeToken<ArrayList<SavingRequestModelItem>>() {}.getType()
            gson = Gson()

            val mainJsonDataResponse: ArrayList<SavingRequestModelItem> =
                gson!!.fromJson(savingListdata, listType)
            var Savinglist = mainJsonDataResponse


            if (Savinglist.size > 0) {
                var progressBar = ProgressDialog(this@ClfMeetingSaving)
                progressBar.setCancelable(false)
                progressBar.setMessage(this.getString(R.string.msgLoading))
                progressBar.show()

                if (savingId == "0") {
                    val user = "IUP_BPMQA"
                    val call = apiInterface?.createClfSaving(
                        "application/json",
                        token,
                        user,
                        Savinglist!!
                    )

                    call?.enqueue(object : Callback<ClfSavingResponse> {
                        override fun onFailure(callCount: Call<ClfSavingResponse>, t: Throwable) {
                            progressBar.dismiss()
                            t.printStackTrace()
                            Log.e("onFailure", "" + t)
                        }

                        override fun onResponse(
                            call: Call<ClfSavingResponse>,
                            response: Response<ClfSavingResponse>
                        ) {

                            Log.e("getMeetingParticipants --> ", "" + response.code())
                            Log.e("getMeetingParticipants --> ", "" + response.body())

                            if (response.isSuccessful) {
                                progressBar.dismiss()
                                try {
                                    savingId = response.body()!!.savingsId.toString()
                                    validate!!.CustomAlertClfSaving(
                                        "Data saved successfully",
                                        this@ClfMeetingSaving
                                    )
                                } catch (ex: Exception) {
                                    ex.printStackTrace()
                                    Log.e("Exception", "" + ex)
                                }

                            } else {
                                progressBar.dismiss()
                                Log.e("else", "else")
                            }
                        }
                    })

                } else if (savingId.toInt() > 0) {

                    for (i in 0 until Savinglist.size) {
                        for (j in 0 until voSavingData!!.size) {
                            if (Savinglist.get(i).voId == voSavingData!!.get(j).memId) {
                                Savinglist.get(i).clfSavingReceiptDetails!!.uid =
                                    voSavingData!!.get(j).uid
                            }
                        }
                    }

                    var is_new_entry = false
                    for (i in 0 until Savinglist.size) {
                        if (Savinglist.get(i).clfSavingReceiptDetails!!.uid == 0) {
                            is_new_entry = true
                            break
                        }
                    }

                    if (is_new_entry) {

                        val user = "IUP_BPMQA"
                        val call = apiInterface?.createClfSaving(
                            "application/json",
                            token,
                            user,
                            Savinglist!!
                        )

                        call?.enqueue(object : Callback<ClfSavingResponse> {
                            override fun onFailure(
                                callCount: Call<ClfSavingResponse>,
                                t: Throwable
                            ) {
                                progressBar.dismiss()
                                t.printStackTrace()
                                Log.e("onFailure", "" + t)
                            }

                            override fun onResponse(
                                call: Call<ClfSavingResponse>,
                                response: Response<ClfSavingResponse>
                            ) {

                                Log.e("getMeetingParticipants --> ", "" + response.code())
                                Log.e("getMeetingParticipants --> ", "" + response.body())

                                if (response.isSuccessful) {
                                    progressBar.dismiss()
                                    try {
                                        savingId = response.body()!!.savingsId.toString()
                                        validate!!.CustomAlertClfSaving(
                                            "Data saved successfully",
                                            this@ClfMeetingSaving
                                        )
                                    } catch (ex: Exception) {
                                        ex.printStackTrace()
                                        Log.e("Exception", "" + ex)
                                    }

                                } else {
                                    progressBar.dismiss()
                                    Log.e("else", "else")
                                }
                            }
                        })
                    } else {
                        val user = "IUP_BPMQA"
                        val call = apiInterface?.updateClfSaving(
                            "application/json",
                            token,
                            user,
                            Savinglist!!,
                            savingId.toInt()
                        )

                        call?.enqueue(object : Callback<ClfSavingResponse> {
                            override fun onFailure(
                                callCount: Call<ClfSavingResponse>,
                                t: Throwable
                            ) {
                                progressBar.dismiss()
                                t.printStackTrace()
                                Log.e("onFailure", "" + t)
                            }

                            override fun onResponse(
                                call: Call<ClfSavingResponse>,
                                response: Response<ClfSavingResponse>
                            ) {

                                Log.e("getMeetingParticipants --> ", "" + response.code())
                                Log.e("getMeetingParticipants --> ", "" + response.body())

                                if (response.isSuccessful) {
                                    progressBar.dismiss()
                                    try {
                                        savingId = response.body()!!.savingsId.toString()
                                        validate!!.CustomAlertClfSaving(
                                            "Data saved successfully",
                                            this@ClfMeetingSaving
                                        )
                                    } catch (ex: Exception) {
                                        ex.printStackTrace()
                                        Log.e("Exception", "" + ex)
                                    }

                                } else {
                                    progressBar.dismiss()
                                    Log.e("else", "else")
                                }
                            }
                        })
                    }
                }
            } else {
                validate!!.CustomAlertSavingBtn(
                    "Please Select payment mode for saving amounts ! ",
                    this@ClfMeetingSaving
                )
            }
        }


    }

    private fun initViews() {

        var mlayoutManager = LinearLayoutManager(applicationContext)
        rvMemberList!!.hasFixedSize()
        rvMemberList!!.layoutManager = mlayoutManager
        rvMemberList!!.itemAnimator = DefaultItemAnimator()
        callApi()
    }

    private fun callApi() {
        val prefUtil = PrefUtil(WeakReference(this))
        val meetingData = prefUtil.get<GetMeetingsResponse>("meeting_response")

//        txtDate!!.text = "Date: " + DateUtil().getDate(meetingData!!.mtgDate!!)
//        txtMeetingNum!!.text = "Meeting Num: " + meetingData!!.mtgNo

        token = Constants.Token

        progressBar = ProgressDialog(this@ClfMeetingSaving)
        progressBar!!.setCancelable(false)
        progressBar!!.setMessage(this.getString(R.string.msgLoading))
        progressBar!!.show()

        getMeetingParticipants(meetingData!!.uid!!)
    }

    private fun getMeetingParticipants(uid: Int) {

        val user = "IUP_BPMQA"
        val call = apiInterface?.getMeetingParticipants(
            "application/json",
            token,
            user,
            uid
        )

        call?.enqueue(object : Callback<GetMeetingParticipants> {
            override fun onFailure(callCount: Call<GetMeetingParticipants>, t: Throwable) {
                progressBar!!.dismiss()
                t.printStackTrace()
                Log.e("onFailure", "" + t)
            }

            override fun onResponse(
                call: Call<GetMeetingParticipants>,
                response: Response<GetMeetingParticipants>
            ) {

                Log.e("getMeetingParticipants --> ", "" + response.code())
                Log.e("getMeetingParticipants --> ", "" + response.body())

                if (response.isSuccessful) {
                    try {
                        clfMtgVoPartcipants =
                            response.body()!!.clfMtgVoPartcipants!! as ArrayList<ClfMtgVoPartcipants>
                        if (response.body()!!.clfMtgEcParticipants != null) {
                            adapter = ClfSavingAdapter(
                                applicationContext,
                                clfMtgVoPartcipants!!,
                                listener
                            )

                            rvMemberList.adapter = adapter
                            rvMemberList.layoutManager = LinearLayoutManager(applicationContext)
                            tv_count.text = response.body()!!.clfMtgVoPartcipants!!.size.toString()

                            getMeetingSaving(meetingData!!.uid!!)

                        }
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        Log.e("Exception", "" + ex)
                    }

                } else {
                    progressBar!!.dismiss()
                    Log.e("else", "else")
                }
            }
        })
    }

    private fun getMeetingSaving(uid: Int) {

        val user = "IUP_BPMQA"
        val call = apiInterface?.getMeetingSaving(
            "application/json",
            token,
            user,
            uid
        )

        call?.enqueue(object : Callback<ArrayList<GetSavingResponse>> {
            override fun onFailure(callCount: Call<ArrayList<GetSavingResponse>>, t: Throwable) {
                progressBar!!.dismiss()
                t.printStackTrace()
                Log.e("onFailure", "" + t)
            }

            override fun onResponse(
                call: Call<ArrayList<GetSavingResponse>>,
                response: Response<ArrayList<GetSavingResponse>>
            ) {

                Log.e("getMeetingSaving --> ", "" + response.code())
                Log.e("getMeetingSaving --> ", "" + response.body())

                if (response.isSuccessful) {
                    try {
                        if (response.body() != null) {
                            if (response.body()!!.size == 0) {
                                progressBar!!.dismiss()
                            } else {
                                savingId = response.body()!!.get(0).uid.toString()
                                getVoSaving(
                                    response.body()!!.get(0).clfMtgUid,
                                    response.body()!!.get(0).uid
                                )
                            }
                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        Log.e("Exception", "" + ex)
                    }

                } else {
                    progressBar!!.dismiss()
                    Log.e("else", "else")
                }
            }
        })
    }

    private fun getVoSaving(clfMtgUid: Int?, uid: Int?) {

        val user = "IUP_BPMQA"
        val call = apiInterface?.getMeetingVoSavingList(
            "application/json",
            token,
            user,
            clfMtgUid!!,
            uid!!
        )

        call?.enqueue(object : Callback<ArrayList<GetMeetingVoSavingListResponseItem>> {
            override fun onFailure(
                callCount: Call<ArrayList<GetMeetingVoSavingListResponseItem>>,
                t: Throwable
            ) {
                progressBar!!.dismiss()
                t.printStackTrace()
                Log.e("onFailure", "" + t)
            }

            override fun onResponse(
                call: Call<ArrayList<GetMeetingVoSavingListResponseItem>>,
                response: Response<ArrayList<GetMeetingVoSavingListResponseItem>>
            ) {

                progressBar!!.dismiss()
                Log.e("getMeetingSaving --> ", "" + response.code())
                Log.e("getMeetingSaving --> ", "" + response.body())

                if (response.isSuccessful) {
                    try {
                        if (response.body() != null) {
                            voSavingData = response.body()!!
                            if (response.body()!!.size > 0) {
                                bindingData(response.body()!!)
                            }
                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        Log.e("Exception", "" + ex)
                    }

                } else {
                    progressBar!!.dismiss()
                    Log.e("else", "else")
                }
            }
        })
    }

    private fun bindingData(body: ArrayList<GetMeetingVoSavingListResponseItem>) {
        for (i in 0 until body.size) {
            for (j in 0 until clfMtgVoPartcipants!!.size) {
                if (body.get(i).memId == clfMtgVoPartcipants!!.get(j).voId) {
                    clfMtgVoPartcipants!!.get(j).compulsorySavingsAmount =
                        body.get(i).compulsorySavingsAmount
                    clfMtgVoPartcipants!!.get(j).voluntarySavingsAmount =
                        body.get(i).voluntarySavingsAmount
                    adapter!!.notifyDataSetChanged()
                }
            }
        }
    }


    private var listener = object : ClfSavingAdapter.OnItemClickListener {
        override fun onItemClick(
            dataList: ClfMtgVoPartcipants, pos: Int, c_amount: String, vo_amount: String
        ) {

            var savingData: GetMeetingVoSavingListResponseItem? = null
            for (i in 0 until voSavingData!!.size) {
                if (voSavingData!!.get(i).memId == dataList!!.voId) {
                    savingData = voSavingData!!.get(i)
                }
            }

            var intent = Intent(
                applicationContext,
                ReceiptDetailActivity::class.java
            )

            intent.putExtra("void", dataList.voId)
            intent.putExtra("c_amount", c_amount)
            intent.putExtra("v_amount", vo_amount)
            intent.putExtra("v_code", dataList.voCode)
            intent.putExtra("v_name", dataList.voName)
            if (savingData != null) {
                intent.putExtra("savingData", savingData)
            }
            startActivity(intent)

        }

        override fun compulsoryValueChanged() {
            var amount = 0
            for (i in 0 until clfMtgVoPartcipants!!.size) {
                amount = amount + clfMtgVoPartcipants!!.get(i).compulsorySavingsAmount!!.toInt()
            }
            tv_TotalCompulsaryValue.text = amount.toString()
        }

        override fun volountoryValueChanged() {
            var amount = 0
            for (i in 0 until clfMtgVoPartcipants!!.size) {
                amount = amount + clfMtgVoPartcipants!!.get(i).voluntarySavingsAmount!!.toInt()
            }
            tv_TotalVoluntaryValue.text = amount.toString()
        }
    }

}

