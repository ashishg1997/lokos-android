package com.microware.cdfi.clf.activites

import android.app.DatePickerDialog
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatSpinner
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textfield.TextInputEditText
import com.google.gson.Gson
import com.microware.cdfi.R
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.api.response.MeetingResponse
import com.microware.cdfi.clf.Constants
import com.microware.cdfi.clf.adapters.MyMeetingAdapter
import com.microware.cdfi.clf.models.create_meeting.CreateMeeting
import com.microware.cdfi.clf.models.create_meeting.CreateMeetingResponse
import com.microware.cdfi.clf.models.meeting_config.MeetingConfigResponse
import com.microware.cdfi.clf.models.meeting_config.MtgType
import com.microware.cdfi.clf.models.meetings.GetMeetingsResponse
import com.microware.cdfi.interfaces.OnListItemClickListener
import com.microware.cdfi.utility.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.ref.WeakReference
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ClfMyMeeting : AppCompatActivity(), View.OnClickListener, OnListItemClickListener {

    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: ApiInterface? = null
    var validate: Validate? = null
    private var recyclerMeetings: RecyclerView? = null
    private var tv_new: TextView? = null
    lateinit var mlayoutManager: LinearLayoutManager
    private var spinnerMeeting: Spinner? = null
    private var spinnerType: Spinner? = null
    private var edtMeetingType: TextInputEditText? = null
    private var edtMeetingNum: TextInputEditText? = null
    private var edtMeetingDate: TextInputEditText? = null

    var adapter: MyMeetingAdapter? = null
    var meetingType: MutableList<MtgType>? = null
    var meetingNum: Int? = null
    var meetingTypeId: String = ""
    var token: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_clf_my_meeting)
        validate = Validate(this)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()

        initViews()

    }

    private fun initViews() {
        recyclerMeetings = findViewById(R.id.recyclerMeetings)
        tv_new = findViewById(R.id.tv_new)
        spinnerMeeting = findViewById(R.id.spinnerMeeting)
        spinnerType = findViewById(R.id.spinnerType)
        edtMeetingType = findViewById(R.id.edtMeetingType)
        edtMeetingNum = findViewById(R.id.edtMeetingNum)
        edtMeetingDate = findViewById(R.id.edtMeetingDate)

        tv_new!!.setOnClickListener(this)

        token = Constants.Token

        getMeetingConfig()
        getMeetings()

    }

    override fun onClick(v: View?) {
        when (v!!.id) {

            R.id.tv_new -> {
//                val startMain = Intent(this, ClfMeetingAttendance::class.java)
//                startActivity(startMain)
                showGenerateMeetingDialog()
            }

        }
    }


    private fun getMeetingConfig() {

        val progressBar = ProgressDialog(this)
        progressBar.setCancelable(false)
        progressBar.setMessage(this.getString(R.string.msgLoading))
        progressBar.show()

//        val user = validate!!.RetriveSharepreferenceString(AppSP.userid)!!
//        val token = validate!!.returnStringValue(
//            AESEncryption.decrypt(
//                validate!!.RetriveSharepreferenceString(AppSP.token),
//                validate!!.RetriveSharepreferenceString(AppSP.userid)
//            )
//        )
        val user = "IUP_BPMQA"
        val cboid = validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        val mtgNum = validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber)
        val call = apiInterface?.getMeetingConfig(
            "application/json",
            token,
            user
        )

        call?.enqueue(object : Callback<MeetingConfigResponse> {
            override fun onFailure(callCount: Call<MeetingConfigResponse>, t: Throwable) {
                t.printStackTrace()
                progressBar.dismiss()
            }

            override fun onResponse(
                call: Call<MeetingConfigResponse>,
                response: Response<MeetingConfigResponse>
            ) {

                Log.e("response --> ", Gson().toJson(response.body()))
                progressBar.dismiss()
                if (response.isSuccessful) {

                    try {
                        meetingType = response.body()!!.mtgTypes
                        meetingNum = response.body()!!.lastMtgNo!!

                        val date = DateUtil().getDate(response.body()!!.lastMtgDate!!)
                        edtMeetingType!!.setText(response.body()!!.lastMtgType)
                        edtMeetingDate!!.setText(date)
                        edtMeetingNum!!.setText("" + response.body()!!.lastMtgNo!!)

                    } catch (ex: Exception) {
                        ex.printStackTrace()

                    }

                } else {
                    progressBar.dismiss()
                    var resCode = 0
                    var resMsg = ""

                }


            }

        })
    }


    private fun getMeetings() {

        val progressBar = ProgressDialog(this)
        progressBar.setCancelable(false)
        progressBar.setMessage(this.getString(R.string.msgLoading))
        progressBar.show()

        val user = "IUP_BPMQA"
//        val call = apiInterface?.getMeetings(
//            "application/json",
//            token,
//            user,
//            "0",
//            "150",
//            "50"
//        )

        val call = apiInterface?.getMeetings(
            "application/json",
            token,
            user,
            "1",
            "100",
            "100",
            "0"
        )

        call?.enqueue(object : Callback<ArrayList<GetMeetingsResponse>> {
            override fun onFailure(callCount: Call<ArrayList<GetMeetingsResponse>>, t: Throwable) {
                t.printStackTrace()
                Log.e("onFailure", "" + t)
                progressBar.dismiss()
            }

            override fun onResponse(
                call: Call<ArrayList<GetMeetingsResponse>>,
                response: Response<ArrayList<GetMeetingsResponse>>
            ) {

                Log.e("getMeetings --> ", "" + response.code())
                Log.e("getMeetings --> ", "" + response.body())
                progressBar.dismiss()
                if (response.isSuccessful) {

                    try {

                        setMeetingAdapter(response.body()!!)

                    } catch (ex: Exception) {
                        ex.printStackTrace()

                    }

                } else {
                    progressBar.dismiss()

                }


            }

        })
    }

    private fun setMeetingAdapter(list: ArrayList<GetMeetingsResponse>) {
        mlayoutManager = LinearLayoutManager(this)
        recyclerMeetings!!.hasFixedSize()
        recyclerMeetings!!.layoutManager = mlayoutManager
        recyclerMeetings!!.itemAnimator = DefaultItemAnimator()

        adapter = MyMeetingAdapter(this, list)
        adapter!!.setListener(this)
        recyclerMeetings!!.adapter = adapter
        adapter!!.notifyDataSetChanged()

    }

    private fun showGenerateMeetingDialog() {
        val alertDialog = Dialog(this)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_create_clf_meeting)
        alertDialog.setCancelable(false)
        val window = alertDialog.window
        val wlp = Objects.requireNonNull(window)!!.attributes
        wlp.gravity = Gravity.CENTER
        wlp.flags = wlp.flags and WindowManager.LayoutParams.FLAG_BLUR_BEHIND.inv()
        window!!.attributes = wlp
        alertDialog.window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )


        val txtMeetingNum = alertDialog.findViewById<View>(R.id.txtMeetingNum) as AppCompatTextView
        val txtMeetingDate =
            alertDialog.findViewById<View>(R.id.txtMeetingDate) as AppCompatTextView
        val spinnerType = alertDialog.findViewById<View>(R.id.spinnerType) as AppCompatSpinner
        val btnGenerate = alertDialog.findViewById<View>(R.id.btnGenerate) as AppCompatButton
        val btnClose = alertDialog.findViewById<View>(R.id.btnClose) as AppCompatButton

        val meetingNumber = meetingNum?.plus(1)
        txtMeetingNum.text = "" + meetingNumber

        val names: MutableList<String> = ArrayList()
        val ids: MutableList<String> = ArrayList()
        for (i in 0 until meetingType!!.size) {
            names.add(meetingType!![i].mtgTypeName!!)
            ids.add(meetingType!![i].mtgTypeCode!!)
        }
        setSpinnerAdapter(spinnerType, names, ids)



        txtMeetingDate.setOnClickListener { datePicker(txtMeetingDate) }
        btnGenerate.setOnClickListener {

            if (txtMeetingDate.text.toString() == "") {
                toast(resources.getString(R.string.msgSelectDate))
            } else {
                val createMeeting = CreateMeeting()
                createMeeting.mtgDate = DateUtil().setDate(txtMeetingDate.text.toString())
                createMeeting.mtgGuid =
                    DateUtil().setDate(txtMeetingDate.text.toString()).toString()
                createMeeting.mtgNo = txtMeetingNum.text.toString().toInt()
                createMeeting.mtgType = meetingTypeId

                createClfMeeting(createMeeting, alertDialog)
            }

        }
        btnClose.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
    }

    private fun datePicker(textView: AppCompatTextView) {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(this, { view, year, monthOfYear, dayOfMonth ->
            Log.e("seleted-->", "" + dayOfMonth + " " + monthOfYear + " " + year)
            val month: Int = monthOfYear + 1
            val dateString = "$dayOfMonth-$month-$year"

            textView.setText(dateString)


        }, year, month, day)
        dpd.show()
    }


    private fun setSpinnerAdapter(
        spinner: AppCompatSpinner,
        list: MutableList<String>,
        ids: MutableList<String>
    ) {
        if (list.isNotEmpty()) {
            val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
                this,
                android.R.layout.simple_spinner_dropdown_item,
                list
            )
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

            spinner.adapter = adapter

//            if (from.equals("Ranking")) {
//                if (str_nameOfWork != null) {
//                    val spinnerPosition = adapter.getPosition(str_nameOfWork)
//                    spinner_nameOfWork!!.setSelection(spinnerPosition)
//                }
//            }


            spinner.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View?, position: Int, id: Long
                ) {
                    Log.e("selectedItem", "" + parent.selectedItem.toString())
                    Log.e("selectedIds", "" + ids[position])
                    meetingTypeId = ids[position]
//                    if (position > 0) {
//                        str_nameOfWork = parent.selectedItem.toString()
//                        nameOfWorkId = idList[position]
//                        Log.e("nameOfWorkId",""+nameOfWorkId)
//                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                }
            }
        }
    }

    private fun createClfMeeting(createMeeting: CreateMeeting, dialog: Dialog) {

        val progressBar = ProgressDialog(this)
        progressBar.setCancelable(false)
        progressBar.setMessage(this.getString(R.string.msgLoading))
        progressBar.show()

        val user = "IUP_BPMQA"
        val call = apiInterface?.createClfMeeting(
            "application/json",
            token,
            user,
            createMeeting
        )

        call?.enqueue(object : Callback<CreateMeetingResponse> {
            override fun onFailure(callCount: Call<CreateMeetingResponse>, t: Throwable) {
                t.printStackTrace()
                Log.e("onFailure", "" + t)
                progressBar.dismiss()
            }

            override fun onResponse(
                call: Call<CreateMeetingResponse>,
                response: Response<CreateMeetingResponse>
            ) {

                Log.e("createClfMeeting --> ", "" + response.code())
                Log.e("createClfMeeting --> ", "" + response.body())
                progressBar.dismiss()
                if (response.isSuccessful) {

                    try {
                        if (dialog.isShowing) {
                            dialog.dismiss()
                        }
                        getMeetingConfig()
                        getMeetings()
                    } catch (ex: Exception) {
                        ex.printStackTrace()

                    }

                } else {
                    progressBar.dismiss()

                }


            }

        })
    }

    override fun onListItemClicked(itemId: Int, data: Any) {
        val item = data as GetMeetingsResponse
        val prefUtil = PrefUtil(WeakReference(this))
        prefUtil.put(item, "meeting_response")
        val intent = Intent(this@ClfMyMeeting, ClfMeetingAttendance::class.java)
        startActivity(intent)
    }

    override fun onListItemLongClicked(itemId: Int, data: Any) {

    }


}