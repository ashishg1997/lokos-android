package com.microware.cdfi.clf.activites

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.clf.models.get_meeting_participants.ClfMtgEcParticipant
import com.microware.cdfi.clf.models.get_meeting_participants.ClfMtgVoPartcipants
import com.microware.cdfi.interfaces.OnListItemClickListener

class ClfSavingAdapter(
    var context: Context,
    var datalist: ArrayList<ClfMtgVoPartcipants>,
    var listener: OnItemClickListener
) : RecyclerView.Adapter<ClfSavingAdapter.SavingHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SavingHolder {
        var inflator = LayoutInflater.from(context)
        var view = inflator.inflate(R.layout.clf_saving_item, parent, false)
        var holder = SavingHolder(view)
        return holder
    }

    override fun onBindViewHolder(holder: SavingHolder, position: Int) {
        var pos = position + 1
        holder.txtSlNo!!.text = "" + pos
        holder.txtMemberName!!.text = datalist.get(position).voName
        holder.et_compulasory_amount!!.setText(datalist.get(position).compulsorySavingsAmount!!.toString())
        holder.et_voluntary_saving_amount!!.setText(datalist.get(position).voluntarySavingsAmount!!.toString())
        holder.itemView.setOnClickListener {
            listener!!.onItemClick(
                datalist.get(position),
                position,
                holder.et_compulasory_amount.text.toString(),
                holder.et_voluntary_saving_amount.text.toString()
            )
        }

        holder.et_compulasory_amount.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                if (s.toString().length > 0) {
                    datalist.get(position).compulsorySavingsAmount = (s.toString()).toInt()
                    listener.compulsoryValueChanged()
                } else {
//                    holder.et_compulasory_amount.setText("0")
                    datalist.get(position).compulsorySavingsAmount = 0
                    listener.compulsoryValueChanged()
                }
            }
        })

        holder.et_voluntary_saving_amount.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                if (s.toString().length > 0) {
                    datalist.get(position).voluntarySavingsAmount = (s.toString()).toInt()
                    listener.volountoryValueChanged()
                } else {
//                    holder.et_voluntary_saving_amount.setText("0")
                    datalist.get(position).voluntarySavingsAmount = 0
                    listener.volountoryValueChanged()
                }


            }
        })
    }

    override fun getItemCount(): Int {
        return datalist.size
    }

    class SavingHolder(view: View) : RecyclerView.ViewHolder(view) {
        var txtMemberName = itemView.findViewById<TextView>(R.id.txtMemberName)
        var txtSlNo = itemView.findViewById<TextView>(R.id.txtSlNo)
        var et_compulasory_amount = itemView.findViewById<EditText>(R.id.et_compulasory_amount)
        var et_voluntary_saving_amount =
            itemView.findViewById<EditText>(R.id.et_voluntary_saving_amount)
    }

    interface OnItemClickListener {
        fun onItemClick(
            dataList: ClfMtgVoPartcipants,
            pos: Int,
            c_amount: String,
            vo_amount: String
        )

        fun compulsoryValueChanged()
        fun volountoryValueChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

}