package com.microware.cdfi.clf.activites

import android.app.Dialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatSpinner
import androidx.appcompat.widget.AppCompatTextView
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.microware.cdfi.R
import com.microware.cdfi.clf.adapters.ClfLoanTabAdapter
import com.microware.cdfi.clf.models.create_meeting.CreateMeeting
import com.microware.cdfi.utility.DateUtil
import com.microware.cdfi.utility.toast
import java.util.*
import kotlin.collections.ArrayList

class LoanRequestActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var tabLayout: TabLayout
    lateinit var viewPager: ViewPager
    private var tv_addRow: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loan_request)

        initViews()

    }

    private fun initViews(){
        tabLayout = findViewById(R.id.tabLayout)
        viewPager = findViewById(R.id.viewPager)
        tv_addRow = findViewById(R.id.tv_addRow)
        tv_addRow!!.setOnClickListener(this)
        setupTabs()
    }

    private fun setupTabs(){
        tabLayout.addTab(tabLayout.newTab().setText("SHG"))
        tabLayout.addTab(tabLayout.newTab().setText("VO"))
        tabLayout.tabGravity = TabLayout.GRAVITY_FILL
        val adapter = ClfLoanTabAdapter(this, supportFragmentManager,
            tabLayout.tabCount)
        viewPager.adapter = adapter
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {

            R.id.tv_addRow -> {
                showAddRowDialog()
            }

        }
    }

    private fun showAddRowDialog() {
        val alertDialog = Dialog(this)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_clf_loan_add_row)
        alertDialog.setCancelable(false)
        val window = alertDialog.window
        val wlp = Objects.requireNonNull(window)!!.attributes
        wlp.gravity = Gravity.CENTER
        wlp.flags = wlp.flags and WindowManager.LayoutParams.FLAG_BLUR_BEHIND.inv()
        window!!.attributes = wlp
        alertDialog.window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )


//        val txtMeetingNum = alertDialog.findViewById<View>(R.id.txtMeetingNum) as AppCompatTextView
//        val btnGenerate = alertDialog.findViewById<View>(R.id.btnGenerate) as AppCompatButton
        val btnCancel = alertDialog.findViewById<View>(R.id.btnCancel) as AppCompatButton


        btnCancel.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
    }

}