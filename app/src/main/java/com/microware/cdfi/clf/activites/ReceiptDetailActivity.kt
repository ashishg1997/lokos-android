package com.microware.cdfi.clf.activites

import android.os.Bundle
import android.widget.RadioGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.microware.cdfi.R
import com.microware.cdfi.clf.fragments.CashReceiptFragment
import com.microware.cdfi.clf.fragments.ChequeReceiptFragment
import com.microware.cdfi.clf.fragments.OnlineReceiptFragment
import com.microware.cdfi.clf.models.saving.GetBankListResponseItem
import com.microware.cdfi.clf.models.saving.GetMeetingVoSavingListResponseItem
import com.microware.cdfi.clf.models.saving.SavingRequestModelItem
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.activity_receipt_detail.*
import java.lang.reflect.Type


class ReceiptDetailActivity : AppCompatActivity() {

    lateinit var tabLayout: TabLayout
    lateinit var viewPager: ViewPager
    var vo_id = 0
    var vo_amount = ""
    var c_amount = ""
    var validate: Validate? = null
    var savingData: GetMeetingVoSavingListResponseItem? = null
    var Savinglist: ArrayList<SavingRequestModelItem>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_receipt_detail)

        validate = Validate(applicationContext!!)
        var intentdata = intent
        vo_id = intentdata.getIntExtra("void", 0)
        c_amount = intentdata.getStringExtra("c_amount")!!
        vo_amount = intentdata.getStringExtra("v_amount")!!
        var voname = intentdata.getStringExtra("v_name")!!
        var vocode = intentdata.getStringExtra("v_code")!!

        tv_vonam.text = voname.toString()
        tv_vocode.text = vocode.toString()
        tv_void.text = vo_id.toString()
        var amount = c_amount.toInt() + vo_amount.toInt()
        tv_vosaving.text = "Saving : ₹ " + amount.toString()

        Savinglist = ArrayList<SavingRequestModelItem>()
        var savingListdata = validate!!.RetriveSharepreferenceString(AppSP.ClfSavingList)

        val listType: Type = object : TypeToken<ArrayList<SavingRequestModelItem>>() {}.getType()
        var gson = Gson()

        val mainJsonDataResponse: ArrayList<SavingRequestModelItem> =
            gson!!.fromJson(savingListdata, listType)
        Savinglist = mainJsonDataResponse


        if (Savinglist!!.size > 0) {
            var is_content_vo = false
            var pos = 0
            for (i in 0 until Savinglist!!.size) {
                if (Savinglist!!.get(i).voId == vo_id) {
                    is_content_vo = true
                    pos = i
                }
            }

            if (is_content_vo) {
                if (Savinglist!!.get(pos).clfSavingReceiptDetails!!.modePayment == 1) {
                    radioCheque.isChecked = true
                    changeFragment(ChequeReceiptFragment())
                } else if (Savinglist!!.get(pos).clfSavingReceiptDetails!!.modePayment == 2) {
                    radioOnline.isChecked = true
                    changeFragment(OnlineReceiptFragment())
                } else if (Savinglist!!.get(pos).clfSavingReceiptDetails!!.modePayment == 3) {
                    radioCash.isChecked = true
                    changeFragment(CashReceiptFragment())
                } else {
                    radioCheque.isChecked = true
                    changeFragment(ChequeReceiptFragment())
                }
            } else {
                if (intentdata.hasExtra("savingData")) {
                    savingData =
                        intentdata.getSerializableExtra("savingData") as GetMeetingVoSavingListResponseItem?
                    if (savingData!!.modePayment == 1) {
                        radioCheque.isChecked = true
                        changeFragment(ChequeReceiptFragment())
                    } else if (savingData!!.modePayment == 2) {
                        radioOnline.isChecked = true
                        changeFragment(OnlineReceiptFragment())
                    } else if (savingData!!.modePayment == 2) {
                        radioCash.isChecked = true
                        changeFragment(CashReceiptFragment())
                    } else {
                        radioCheque.isChecked = true
                        changeFragment(ChequeReceiptFragment())
                    }
                } else {
                    radioCheque.isChecked = true
                    changeFragment(ChequeReceiptFragment())
                }
            }

        } else if (intentdata.hasExtra("savingData")) {
            savingData =
                intentdata.getSerializableExtra("savingData") as GetMeetingVoSavingListResponseItem?
            if (savingData!!.modePayment == 1) {
                radioCheque.isChecked = true
                changeFragment(ChequeReceiptFragment())
            } else if (savingData!!.modePayment == 2) {
                radioOnline.isChecked = true
                changeFragment(OnlineReceiptFragment())
            } else if (savingData!!.modePayment == 3) {
                radioCash.isChecked = true
                changeFragment(CashReceiptFragment())
            } else {
                radioCheque.isChecked = true
                changeFragment(ChequeReceiptFragment())
            }
        } else {
            radioCheque.isChecked = true
            changeFragment(ChequeReceiptFragment())
        }

        ic_Back.setOnClickListener {
            onBackPressed()
        }

        rg_paymentType.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
                if (checkedId == R.id.radioCheque) {
                    var fragment = ChequeReceiptFragment()
                    var ft = supportFragmentManager!!.beginTransaction()
                    ft.replace(R.id.fragment_container, fragment, "ChequePayment")
                    ft.commit()
                } else if (checkedId == R.id.radioOnline) {
                    var fragment = OnlineReceiptFragment()
                    var ft = supportFragmentManager!!.beginTransaction()
                    ft.replace(R.id.fragment_container, fragment, "OnlinePayment")
                    ft.commit()
                } else if (checkedId == R.id.radioCash) {
                    var fragment = CashReceiptFragment()
                    var ft = supportFragmentManager!!.beginTransaction()
                    ft.replace(R.id.fragment_container, fragment, "CashPayment")
                    ft.commit()
                }
            }
        })
    }

    private fun changeFragment(fragment: Fragment) {
        var ft = supportFragmentManager!!.beginTransaction()
        ft.replace(R.id.fragment_container, fragment, "ChequePayment")
        ft.commit()
    }

}