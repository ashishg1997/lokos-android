package com.microware.cdfi.clf.adapters

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.clf.activites.ClfSavingAdapter
import com.microware.cdfi.clf.models.get_meeting_participants.ClfMtgVoPartcipants
import com.microware.cdfi.clf.models.meetings.GetMeetingsResponse
import com.microware.cdfi.clf.models.saving.GetBranchListResponseItem
import com.microware.cdfi.interfaces.OnListItemClickListener
import com.microware.cdfi.utility.DateUtil

class BranchAdapter(
    val context: Context,
    var meetingList: ArrayList<GetBranchListResponseItem>,
    var listener: OnItemClickListener
) :
    RecyclerView.Adapter<BranchAdapter.ViewHolder>() {

    var mListener: OnListItemClickListener? = null

    fun setListener(mListener: OnListItemClickListener?) {
        this.mListener = mListener
    }

    fun setData(meetingListdata: ArrayList<GetBranchListResponseItem>) {
        this.meetingList = meetingListdata
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemCount(): Int {
        return meetingList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val listItem: GetBranchListResponseItem = meetingList[position]

        holder.txtbranchName!!.text = listItem.bankBranchName.toString()
        holder.itemView!!.setOnClickListener {
            listener!!.onItemClick(listItem, position)
        }

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var txtbranchName: TextView? = null

        init {
            txtbranchName = itemView.findViewById(R.id.txtbranchName)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val row: View =
            LayoutInflater.from(context).inflate(R.layout.clf_branch_item, parent, false)
        return ViewHolder(row)
    }


    interface OnItemClickListener {
        fun onItemClick(
            dataList: GetBranchListResponseItem,
            pos: Int
        )
    }

}
