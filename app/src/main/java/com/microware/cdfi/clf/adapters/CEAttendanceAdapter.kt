package com.microware.cdfi.clf.adapters

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.clf.models.get_meeting_participants.ClfMtgEcParticipant
import com.microware.cdfi.clf.models.get_meeting_participants.GetMeetingParticipants
import com.microware.cdfi.clf.models.meetings.GetMeetingsResponse
import com.microware.cdfi.interfaces.OnListItemClickListener

class CEAttendanceAdapter(val context: Activity, val meetingList: MutableList<ClfMtgEcParticipant>) :
    RecyclerView.Adapter<CEAttendanceAdapter.ViewHolder>() {

    var mListener: OnListItemClickListener? = null

    fun setListener(mListener: OnListItemClickListener?) {
        this.mListener = mListener
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemCount(): Int {
        return meetingList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val listItem: ClfMtgEcParticipant = meetingList[position]

        val pos = position+1
        holder.txtSlNo!!.text = ""+pos
        holder.txtMemberName!!.text = listItem.memberName
        holder.txtPost!!.text = listItem.memberPost

        if (listItem.attendance == "p"){
            holder.imgPresent!!.setImageResource(R.drawable.ic_present_green)
            holder.imgAbsent!!.setImageResource(R.drawable.ic_absent_light_red)
        }
        if (listItem.attendance == "a"){
            holder.imgPresent!!.setImageResource(R.drawable.ic_present_light_green)
            holder.imgAbsent!!.setImageResource(R.drawable.ic_absent_red)
        }

        holder.imgPresent!!.setOnClickListener {
            listItem.attendance = "p"
            holder.imgPresent!!.setImageResource(R.drawable.ic_present_green)
            holder.imgAbsent!!.setImageResource(R.drawable.ic_absent_light_red)
            mListener!!.onListItemClicked(R.id.imgPresent, listItem)
        }
        holder.imgAbsent!!.setOnClickListener {
            listItem.attendance = "a"
            holder.imgPresent!!.setImageResource(R.drawable.ic_present_light_green)
            holder.imgAbsent!!.setImageResource(R.drawable.ic_absent_red)
            mListener!!.onListItemClicked(R.id.imgAbsent, listItem)
        }


    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var layout: LinearLayoutCompat? = null
        var txtMemberName: TextView? = null
        var txtSlNo: TextView? = null
        var txtPost: TextView? = null
        var imgPresent: ImageView? = null
        var imgAbsent: ImageView? = null

        init {
            layout = itemView.findViewById(R.id.layout)
            txtMemberName = itemView.findViewById(R.id.txtMemberName)
            txtSlNo = itemView.findViewById(R.id.txtSlNo)
            txtPost = itemView.findViewById(R.id.txtPost)
            imgPresent = itemView.findViewById(R.id.imgPresent)
            imgAbsent = itemView.findViewById(R.id.imgAbsent)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val row: View = LayoutInflater.from(context).inflate(R.layout.cell_ce_attendance, parent, false)
        return ViewHolder(row)
    }


}
