package com.microware.cdfi.clf.adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.microware.cdfi.clf.fragments.CEAttendanceFragment
import com.microware.cdfi.clf.fragments.SHGLoanFragment
import com.microware.cdfi.clf.fragments.VOAttendanceFragment
import com.microware.cdfi.clf.fragments.VOLoanFragment

internal class ClfLoanTabAdapter(
    var context: Context,
    fm: FragmentManager,
    var totalTabs: Int
) :
    FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                SHGLoanFragment()
            }
            1 -> {
                VOLoanFragment()
            }
            else -> getItem(position)
        }
    }
    override fun getCount(): Int {
        return totalTabs
    }
}