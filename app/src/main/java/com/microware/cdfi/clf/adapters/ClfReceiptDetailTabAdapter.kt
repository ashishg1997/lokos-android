package com.microware.cdfi.clf.adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.microware.cdfi.clf.fragments.*

internal class ClfReceiptDetailTabAdapter(
    var context: Context,
    fm: FragmentManager,
    var totalTabs: Int
) :
    FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                ChequeReceiptFragment()
            }
            1 -> {
                OnlineReceiptFragment()
            }
            2 -> {
                CashReceiptFragment()
            }
            else -> getItem(position)
        }
    }

    override fun getCount(): Int {
        return totalTabs
    }

}