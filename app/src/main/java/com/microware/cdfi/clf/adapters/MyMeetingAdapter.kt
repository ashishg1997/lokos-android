package com.microware.cdfi.clf.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.clf.models.meetings.GetMeetingsResponse
import com.microware.cdfi.interfaces.OnListItemClickListener
import com.microware.cdfi.utility.DateUtil

class MyMeetingAdapter(val context: Activity, val meetingList: ArrayList<GetMeetingsResponse>) :
    RecyclerView.Adapter<MyMeetingAdapter.ViewHolder>() {

    var mListener: OnListItemClickListener? = null

    fun setListener(mListener: OnListItemClickListener?) {
        this.mListener = mListener
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemCount(): Int {
        return meetingList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val listItem: GetMeetingsResponse = meetingList[position]

        holder.txtMeetingNum!!.text = listItem.mtgNo.toString()
        holder.txtMeetingDate!!.text = DateUtil().getDate(listItem.mtgDate!!)
        holder.txtMeetingType!!.text = listItem.mtgType

        holder.layoutClick!!.setOnClickListener {
            mListener!!.onListItemClicked(R.id.layoutClick, listItem)
        }

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var layoutClick: LinearLayoutCompat? = null
        var txtMeetingNum: TextView? = null
        var txtMeetingDate: TextView? = null
        var txtMeetingType: TextView? = null

        init {
            layoutClick = itemView.findViewById(R.id.layoutClick)
            txtMeetingNum = itemView.findViewById(R.id.txtMeetingNum)
            txtMeetingDate = itemView.findViewById(R.id.txtMeetingDate)
            txtMeetingType = itemView.findViewById(R.id.txtMeetingType)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val row: View = LayoutInflater.from(context).inflate(R.layout.cell_clf_meetings, parent, false)
        return ViewHolder(row)
    }


}
