package com.microware.cdfi.clf.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.clf.models.get_meeting_participants.ClfMtgEcParticipant
import com.microware.cdfi.clf.models.get_meeting_participants.ClfMtgVoPartcipants
import com.microware.cdfi.interfaces.OnListItemClickListener

class VOAttendanceAdapter(val context: Activity, val meetingList: MutableList<ClfMtgVoPartcipants>) :
    RecyclerView.Adapter<VOAttendanceAdapter.ViewHolder>() {

    var mListener: OnListItemClickListener? = null

    fun setListener(mListener: OnListItemClickListener?) {
        this.mListener = mListener
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemCount(): Int {
        return meetingList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val listItem: ClfMtgVoPartcipants = meetingList[position]

        holder.txtSlNo!!.text = ""+position + 1
        holder.txtVoName!!.text = listItem.voName

        if (listItem.attendance == "p"){
            holder.imgPresent!!.setImageResource(R.drawable.ic_present_green)
            holder.imgAbsent!!.setImageResource(R.drawable.ic_absent_light_red)
        }
        if (listItem.attendance == "a"){
            holder.imgPresent!!.setImageResource(R.drawable.ic_present_light_green)
            holder.imgAbsent!!.setImageResource(R.drawable.ic_absent_red)
        }

        holder.imgPresent!!.setOnClickListener {
            listItem.attendance = "p"
            holder.imgPresent!!.setImageResource(R.drawable.ic_present_green)
            holder.imgAbsent!!.setImageResource(R.drawable.ic_absent_light_red)
            mListener!!.onListItemClicked(R.id.imgPresent, listItem)
        }
        holder.imgAbsent!!.setOnClickListener {
            listItem.attendance = "a"
            holder.imgPresent!!.setImageResource(R.drawable.ic_present_light_green)
            holder.imgAbsent!!.setImageResource(R.drawable.ic_absent_red)
            mListener!!.onListItemClicked(R.id.imgAbsent, listItem)
        }

    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var layout: LinearLayoutCompat? = null
        var txtSlNo: TextView? = null
        var txtVoName: TextView? = null
        var imgPresent: ImageView? = null
        var imgAbsent: ImageView? = null

        init {
            layout = itemView.findViewById(R.id.layout)
            txtVoName = itemView.findViewById(R.id.txtVoName)
            txtSlNo = itemView.findViewById(R.id.txtSlNo)
            imgPresent = itemView.findViewById(R.id.imgPresent)
            imgAbsent = itemView.findViewById(R.id.imgAbsent)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val row: View = LayoutInflater.from(context).inflate(R.layout.cell_vo_attendance, parent, false)
        return ViewHolder(row)
    }


}
