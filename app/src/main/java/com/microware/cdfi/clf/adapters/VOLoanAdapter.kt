package com.microware.cdfi.clf.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.interfaces.OnListItemClickListener

class VOLoanAdapter(val context: Activity) :
    RecyclerView.Adapter<VOLoanAdapter.ViewHolder>() {

    var mListener: OnListItemClickListener? = null

    fun setListener(mListener: OnListItemClickListener?) {
        this.mListener = mListener
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemCount(): Int {
        return 10
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

//        val listItem: ClfMtgVoPartcipants = meetingList[position]

        holder.txtSlNo!!.text = ""+position + 1
//        holder.txtShgName!!.text = listItem.voName

    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var layout: LinearLayoutCompat? = null
        var txtSlNo: TextView? = null
        var txtVoName: TextView? = null


        init {
            layout = itemView.findViewById(R.id.layout)
            txtVoName = itemView.findViewById(R.id.txtVoName)
            txtSlNo = itemView.findViewById(R.id.txtSlNo)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val row: View = LayoutInflater.from(context).inflate(R.layout.cell_clf_vo_loan, parent, false)
        return ViewHolder(row)
    }


}