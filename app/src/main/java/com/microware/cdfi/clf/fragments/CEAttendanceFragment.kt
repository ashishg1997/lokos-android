package com.microware.cdfi.clf.fragments

import android.app.ProgressDialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.microware.cdfi.R
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.clf.Constants
import com.microware.cdfi.clf.adapters.CEAttendanceAdapter
import com.microware.cdfi.clf.models.get_meeting_participants.ClfMtgEcParticipant
import com.microware.cdfi.clf.models.get_meeting_participants.ClfMtgVoPartcipants
import com.microware.cdfi.clf.models.get_meeting_participants.GetMeetingParticipants
import com.microware.cdfi.clf.models.meetings.GetMeetingsResponse
import com.microware.cdfi.clf.models.save_attendance.SaveAttendance
import com.microware.cdfi.clf.models.save_attendance.SaveAttendanceResponse
import com.microware.cdfi.interfaces.OnListItemClickListener
import com.microware.cdfi.utility.AESEncryption
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.PrefUtil
import com.microware.cdfi.utility.Validate
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.ref.WeakReference

class CEAttendanceFragment : Fragment(), OnListItemClickListener, View.OnClickListener {

    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: ApiInterface? = null
    var validate: Validate? = null
    private var recyclerView: RecyclerView? = null
    private var txtPresentCount: TextView? = null
    private var txtAbsentCount: TextView? = null
    lateinit var mlayoutManager: LinearLayoutManager
    private var btnSave: AppCompatButton? = null

    var adapter: CEAttendanceAdapter? = null
    var meetingData: GetMeetingsResponse? = null
    var token: String = ""
    var presentCount = 0
    var absentCount = 0
    var ceAttendanceList: MutableList<ClfMtgEcParticipant>? = ArrayList()
    var voAttendanceList: MutableList<ClfMtgVoPartcipants>? = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_ce_attendance, container, false)
        validate = Validate(requireActivity())
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()

        initViews(view)

        return view
    }

    private fun initViews(view: View){

        recyclerView = view.findViewById(R.id.recyclerCeAttendance)
        txtPresentCount = view.findViewById(R.id.txtPresentCount)
        txtAbsentCount = view.findViewById(R.id.txtAbsentCount)
        btnSave = view.findViewById(R.id.btnSave)

        mlayoutManager = LinearLayoutManager(requireActivity())
        recyclerView!!.hasFixedSize()
        recyclerView!!.layoutManager = mlayoutManager
        recyclerView!!.itemAnimator = DefaultItemAnimator()

        btnSave!!.setOnClickListener(this)

        val prefUtil = PrefUtil(WeakReference(requireActivity()))
        meetingData = prefUtil.get<GetMeetingsResponse>("meeting_response")

        txtPresentCount!!.text = ""+presentCount
        txtAbsentCount!!.text = ""+absentCount
        getMeetingParticipants(meetingData!!.uid!!)


    }

    private fun getMeetingParticipants(uid: Int) {

        val progressBar = ProgressDialog(requireActivity())
        progressBar.setCancelable(false)
        progressBar.setMessage(this.getString(R.string.msgLoading))
        progressBar.show()

        val call = apiInterface?.getMeetingParticipants(
            "application/json",
            Constants.Token,
            Constants.userId,
            uid
        )

        call?.enqueue(object : Callback<GetMeetingParticipants> {

            override fun onFailure(callCount: Call<GetMeetingParticipants>, t: Throwable) {
                progressBar.dismiss()
                t.printStackTrace()
                Log.e("onFailure",""+t)

            }

            override fun onResponse(
                call: Call<GetMeetingParticipants>,
                response: Response<GetMeetingParticipants>
            ) {

                Log.e("getMeetingParticipants --> ", ""+response.code())
                Log.e("getMeetingParticipants --> ", ""+response.body())

                if (response.isSuccessful) {
                    progressBar.dismiss()
                    try {

                        if (response.body()!!.clfMtgEcParticipants != null){
                            ceAttendanceList = response.body()!!.clfMtgEcParticipants!!
//                            voAttendanceList = response.body()!!.clfMtgVoPartcipants!!
                            ceAttendanceList!!.forEach { it.attendance = "p" }
//                            voAttendanceList!!.forEach { it.attendance = "p" }
                            setAdapter(ceAttendanceList!!)
                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        Log.e("Exception",""+ex)
                    }

                } else {
                    progressBar.dismiss()
                    Log.e("else","else")
                }


            }

        })
    }

    private fun setAdapter(clfMtgEcParticipant: MutableList<ClfMtgEcParticipant>){
        adapter = CEAttendanceAdapter(requireActivity(),clfMtgEcParticipant)
        adapter!!.setListener(this)
        recyclerView!!.adapter = adapter
        adapter!!.notifyDataSetChanged()

        presentCount = clfMtgEcParticipant.filter { it.attendance == "p" }.size
        absentCount = clfMtgEcParticipant.filter { it.attendance == "a" }.size
        txtPresentCount!!.text = ""+presentCount
        txtAbsentCount!!.text = ""+absentCount

    }

    override fun onListItemClicked(itemId: Int, data: Any) {
        val item = data as ClfMtgEcParticipant
        if (item.attendance != null && item.attendance == "p"){
            ceAttendanceList?.find { it.cboId == item.cboId }?.attendance = "p"
        }else{
            ceAttendanceList?.find { it.cboId == item.cboId }?.attendance = "a"
        }

        presentCount = ceAttendanceList?.filter { it.attendance == "p" }!!.size
        absentCount = ceAttendanceList?.filter { it.attendance == "a" }!!.size
        txtPresentCount!!.text = ""+presentCount
        txtAbsentCount!!.text = ""+absentCount

    }

    override fun onListItemLongClicked(itemId: Int, data: Any) {
        TODO("Not yet implemented")
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnSave -> {
                val saveAttendance = SaveAttendance()
                saveAttendance.clfMtgEcParticipants = ceAttendanceList
//                saveAttendance.clfMtgVoPartcipants = voAttendanceList
                saveClfAttendance(saveAttendance)
            }
        }

    }


    private fun saveClfAttendance(saveAttendance: SaveAttendance) {

        Log.e("saveAttendance",""+ Gson().toJson(saveAttendance))
        Log.e("meetingData",""+meetingData!!.uid!!)

        val progressBar = ProgressDialog(requireActivity())
        progressBar.setCancelable(false)
        progressBar.setMessage(this.getString(R.string.msgLoading))
        progressBar.show()

        val call = apiInterface?.saveClfAttendance(
            "application/json",
            Constants.Token,
            Constants.userId,
            meetingData!!.uid!!,
            saveAttendance
        )

        call?.enqueue(object : Callback<SaveAttendanceResponse> {
            override fun onFailure(callCount: Call<SaveAttendanceResponse>, t: Throwable) {
                t.printStackTrace()
                Log.e("onFailure", "" + t)
                progressBar.dismiss()
            }

            override fun onResponse(
                call: Call<SaveAttendanceResponse>,
                response: Response<SaveAttendanceResponse>
            ) {

                Log.e("saveClfAttendance --> ", "" + response.code())
                Log.e("saveClfAttendance --> ", "" + response.body())
                progressBar.dismiss()
                if (response.isSuccessful) {
                    Toast.makeText(requireActivity(), "Saved successfully", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(requireActivity(), "Something went wrong please try again", Toast.LENGTH_SHORT).show()
                    progressBar.dismiss()

                }


            }

        })
    }

}