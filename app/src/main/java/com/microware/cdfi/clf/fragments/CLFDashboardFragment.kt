package com.microware.cdfi.clf.fragments

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.clf.activites.ClfMyMeeting
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.FedrationViewModel
import com.microware.cdfi.viewModel.ResponseViewModel
import kotlinx.android.synthetic.main.fragment_clf_dashboard.view.*

class CLFDashboardFragment : Fragment() {
    var validate: Validate? = null
    var fedrationViewModel: FedrationViewModel? = null
    var cboType = 0
    internal lateinit var progressDialog: ProgressDialog
    var responseViewModel: ResponseViewModel? = null

    var apiInterface: ApiInterface? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_clf_dashboard, container, false)
        validate = Validate(requireActivity())


        fedrationViewModel = ViewModelProviders.of(this).get(FedrationViewModel::class.java)
        responseViewModel = ViewModelProviders.of(this).get(ResponseViewModel::class.java)

        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()

        // change by noman
        view.tbl_vo.setOnClickListener {
//            val startMain = Intent(activity, VOListActivity::class.java)
//            startMain.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
//            startActivity(startMain)
        }



        view.tbl_profile.setOnClickListener {
//            val startMain = Intent(activity, VoMyTaskActivity::class.java)
//            startMain.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
//            startActivity(startMain)
        }

        view.tbl_sync.setOnClickListener {
//            val startMain = Intent(activity, VoSyncActivity::class.java)
//            startMain.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
//            startActivity(startMain)
        }

        view.tbl_meeting.setOnClickListener {
            val startMain = Intent(activity, ClfMyMeeting::class.java)
            startActivity(startMain)
        }

        //    importPendingMeetingCount()
        setLabelText(view)
        return view
    }

    private fun setLabelText(view: View) {
        view.tvMeeting.text = LabelSet.getText(
            "my_meeting1",
            R.string.my_meeting1
        )
        // change by noman
        view.tvProfile.text = LabelSet.getText(
            "profile",
            R.string.profile
        )
        if (validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType) == 2) {
            cboType = 2
            var count = fedrationViewModel!!.getVoCount(cboType)
            view.tv_vo.text = LabelSet.getText(
                "clf",
                R.string.clf
            ) + " (" + count + ")"
        } else {
            cboType = 1
            var count = fedrationViewModel!!.getVoCount(cboType)
            view.tv_vo.text = LabelSet.getText(
                "village_organization",
                R.string.village_organization
            ) + " (" + count + ")"

        }

        view.tvSync.text = LabelSet.getText(
            "synchronization",
            R.string.synchronization
        )
        view.tvReport.text = LabelSet.getText(
            "report",
            R.string.report
        )
        view.tvPending_count.text = LabelSet.getText(
            "pending_count",
            R.string.pending_count
        ) + "(" + validate!!.RetriveSharepreferenceString(MeetingSP.pendingMeetings) + ")"
    }

}