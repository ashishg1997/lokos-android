package com.microware.cdfi.clf.fragments

import android.app.ProgressDialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.microware.cdfi.R
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.clf.activites.ClfMeetingSaving
import com.microware.cdfi.clf.activites.ReceiptDetailActivity
import com.microware.cdfi.clf.models.meetings.GetMeetingsResponse
import com.microware.cdfi.clf.models.saving.ClfSavingReceiptDetails
import com.microware.cdfi.clf.models.saving.ClfSavingResponse
import com.microware.cdfi.clf.models.saving.GetMeetingVoSavingListResponseItem
import com.microware.cdfi.clf.models.saving.SavingRequestModelItem
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.PrefUtil
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.fragment_cash_receipt.*
import kotlinx.android.synthetic.main.fragment_cash_receipt.et_Narration
import kotlinx.android.synthetic.main.fragment_cash_receipt.et_amount
import kotlinx.android.synthetic.main.fragment_cash_receipt.et_voucher
import kotlinx.android.synthetic.main.fragment_cheque_receipt.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.ref.WeakReference
import java.lang.reflect.Type
import java.text.SimpleDateFormat
import java.util.*

class CashReceiptFragment : Fragment() {

    var validate: Validate? = null
    var vo_id = 0
    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: ApiInterface? = null
    var Savinglist: ArrayList<SavingRequestModelItem>? = null
    var gson: Gson? = null
    var savingData: GetMeetingVoSavingListResponseItem? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_cash_receipt, container, false)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        validate = Validate(context!!)

        vo_id = (activity as ReceiptDetailActivity).vo_id
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()

        Savinglist = ArrayList<SavingRequestModelItem>()
        var savingListdata = validate!!.RetriveSharepreferenceString(AppSP.ClfSavingList)

        val listType: Type = object : TypeToken<ArrayList<SavingRequestModelItem>>() {}.getType()
        gson = Gson()

        val mainJsonDataResponse: ArrayList<SavingRequestModelItem> =
            gson!!.fromJson(savingListdata, listType)
        Savinglist = mainJsonDataResponse

        savingData = (context as ReceiptDetailActivity).savingData

        val prefUtil = PrefUtil(WeakReference(requireActivity()))
        val meetingData = prefUtil.get<GetMeetingsResponse>("meeting_response")

        val sdf = SimpleDateFormat("dd-MM-yyy", Locale.getDefault())
        val currentDateandTime: String = sdf.format(Date())

        var amount = (context as ReceiptDetailActivity).c_amount.toInt() +  (context as ReceiptDetailActivity).vo_amount.toInt().toInt()
        et_amount.setText(amount.toString())

        setData()

        btn_cancel.setOnClickListener {
            activity!!.onBackPressed()
        }

        btn_save.setOnClickListener {

            var amount = et_amount.text.toString()
            var voucher = et_voucher.text.toString()
            var narration = et_Narration.text.toString()

            var voucherDate = null
            var txnDate = null
            var checkNo = null
            var issuedateCheck = null
            var ReceivedateCheck = null
            var payeebank = null
            var payeebankBranch = null
            var bankAccount = null
            var txnNumber = null
            var type = null


            if (amount.isNullOrEmpty()) {
                validate!!.CustomAlertEditText(
                    "Please Insert Amount",
                    requireActivity(),
                    et_amount
                )
            }
//            else if (voucher.isNullOrEmpty()){
//                validate!!.CustomAlert("insert Voucher Number", requireActivity())
//            }
            else if (narration.isNullOrEmpty()) {
                validate!!.CustomAlertEditText(
                    "Please Insert Narration",
                    requireActivity(),
                    et_Narration
                )
            } else {

                var voamount = 0
                var camount = 0
                if ((activity as ReceiptDetailActivity).vo_amount.isNullOrEmpty()) {
                    voamount = 0
                } else {
                    voamount = (activity as ReceiptDetailActivity).vo_amount.toInt()
                }
                if ((activity as ReceiptDetailActivity).c_amount.isNullOrEmpty()) {
                    camount = 0
                } else {
                    camount = (activity as ReceiptDetailActivity).c_amount.toInt()
                }

                var clfDetail = ClfSavingReceiptDetails(
                    voamount.toInt(),
                    checkNo,
                    issuedateCheck,
                    ReceivedateCheck,
                    payeebank,  //payeeBankId
                    payeebankBranch,  //payeeBankBranchId
                    camount.toInt(),
                    et_voucher.text.toString(),
                    et_Narration.text.toString(),
                    bankAccount,  //bankAccountId
                    voucherDate,
                    3,  //modeOfPayment
                    txnDate,
                    txnNumber,
                    type
                )
                var objectData = SavingRequestModelItem(vo_id, meetingData!!.uid!!, clfDetail)

                if (Savinglist!!.size > 0) {
                    var is_present = false
                    for (i in 0 until Savinglist!!.size) {
                        if (Savinglist!!.get(i).voId == vo_id) {
                            Savinglist!!.remove(Savinglist!!.get(i))
                            Savinglist!!.add(objectData)
                            var SavinglistString = gson!!.toJson(Savinglist)
                            validate!!.SaveSharepreferenceString(
                                AppSP.ClfSavingList,
                                SavinglistString
                            )
                            break
                        }
                    }

                    if (!is_present) {
                        Savinglist!!.add(objectData)
                        var SavinglistString = gson!!.toJson(Savinglist)
                        validate!!.SaveSharepreferenceString(AppSP.ClfSavingList, SavinglistString)
                    }
                    validate!!.CustomAlertClfSaving(
                        "Data saved successfully",
                        requireActivity()
                    )

                } else {
                    Savinglist!!.add(objectData)
                    var SavinglistString = gson!!.toJson(Savinglist)
                    validate!!.SaveSharepreferenceString(AppSP.ClfSavingList, SavinglistString)

                    validate!!.CustomAlertClfSaving(
                        "Data saved successfully",
                        requireActivity()
                    )
                }
            }

        }


    }

    private fun setData() {
        if (Savinglist!!.size > 0) {
            var is_content_vo = false
            var pos = 0
            for (i in 0 until Savinglist!!.size) {
                if (Savinglist!!.get(i).voId == vo_id) {
                    is_content_vo = true
                    pos = i
                }
            }

            if (is_content_vo && Savinglist!!.get(pos).clfSavingReceiptDetails!!.modePayment == 3) {
                var i = pos

                et_voucher.setText(Savinglist!!.get(i).clfSavingReceiptDetails!!.voucherNumber.toString())

                et_Narration.setText(Savinglist!!.get(i).clfSavingReceiptDetails!!.narration.toString())
            } else if (savingData != null) {
                if (savingData!!.modePayment == 3) {

                    et_voucher.setText(savingData!!.voucherNumber.toString())

                    et_Narration.setText(savingData!!.narration.toString())

                }
            }
        }
        else if (savingData != null) {
            if (savingData!!.modePayment == 3) {

                et_voucher.setText(savingData!!.voucherNumber.toString())

                et_Narration.setText(savingData!!.narration.toString())
            }
        }
    }


}