package com.microware.cdfi.clf.fragments

import android.app.Activity
import android.app.ProgressDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.widget.AppCompatSpinner
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.microware.cdfi.R
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.clf.Constants
import com.microware.cdfi.clf.activites.ReceiptDetailActivity
import com.microware.cdfi.clf.adapters.BranchAdapter
import com.microware.cdfi.clf.models.meetings.GetMeetingsResponse
import com.microware.cdfi.clf.models.saving.*
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.PrefUtil
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.activity_clf_meeting_saving.*
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.dialoge.view.*
import kotlinx.android.synthetic.main.fragment_address_detail.*
import kotlinx.android.synthetic.main.fragment_basicdetail.*
import kotlinx.android.synthetic.main.fragment_cheque_receipt.*
import kotlinx.android.synthetic.main.meeting_detail_item.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.ref.WeakReference
import java.lang.reflect.Type
import java.text.SimpleDateFormat
import java.util.*
import java.util.Locale
import kotlin.collections.ArrayList


class ChequeReceiptFragment : Fragment() {

    var vo_id = 0
    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: ApiInterface? = null
    var validate: Validate? = null
    private var uid: String? = null

    var voluntarySavingsAmount: Int? = null
    var chequeNo: String? = null
    var chequeIssuedDate: String? = null
    var chequeReceivedDate: String? = null
    var payeeBankId: Int? = null
    var payeeBankBranchId: Int? = null
    var compulsorySavingsAmount: Int? = null
    var voucherNumber: String? = null
    var narration: String? = null
    var bankAccountId: Int? = null
    var voucherDate: String? = null
    var modePayment: Int? = null
    var txnDate: String? = null
    var Savinglist: ArrayList<SavingRequestModelItem>? = null
    var gson: Gson? = null
    var token: String = ""
    var bankList: ArrayList<GetBankListResponseItem>? = null
    var bankAccountList: ArrayList<GetBankAccountListResponseItem>? = null
    var payeeBankType: AppCompatSpinner? = null
    var receiveBankType: AppCompatSpinner? = null
    var bankId: String = "0"
    var savingData: GetMeetingVoSavingListResponseItem? = null
    var call: Call<ArrayList<GetBranchListResponseItem>>? = null
    var is_recycle_scroll = false
    var selectedBranchId: Long = 0
    var adapter: BranchAdapter? = null
    var progressBar1: ProgressDialog ?= null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var view = inflater.inflate(R.layout.fragment_cheque_receipt, container, false)
        payeeBankType = view.findViewById<View>(R.id.spin_payeeBank) as AppCompatSpinner
        receiveBankType = view.findViewById<View>(R.id.spin_receivingBankBranch) as AppCompatSpinner

        return view
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        validate = Validate(context!!)

        vo_id = (activity as ReceiptDetailActivity).vo_id
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()

        Savinglist = ArrayList<SavingRequestModelItem>()
        bankList = ArrayList<GetBankListResponseItem>()

        var savingListdata = validate!!.RetriveSharepreferenceString(AppSP.ClfSavingList)
        savingData = (context as ReceiptDetailActivity).savingData

        val listType: Type = object : TypeToken<ArrayList<SavingRequestModelItem>>() {}.getType()
        gson = Gson()

        val mainJsonDataResponse: ArrayList<SavingRequestModelItem> =
            gson!!.fromJson(savingListdata, listType)
        Savinglist = mainJsonDataResponse

        val prefUtil = PrefUtil(WeakReference(requireActivity()))
        val meetingData = prefUtil.get<GetMeetingsResponse>("meeting_response")

        token = Constants.Token

        getBankList()
        getBankAccountList()

        val sdf = SimpleDateFormat("dd-MM-yyy", Locale.getDefault())
        val currentDateandTime: String = sdf.format(Date())

        et_chequeIssueDate.setText(currentDateandTime)
        et_chequeReceiveDate.setText(currentDateandTime)

        var amount =
            (context as ReceiptDetailActivity).c_amount.toInt() + (context as ReceiptDetailActivity).vo_amount.toInt()
                .toInt()
        et_amount.setText(amount.toString())

        setData()


//        spin_payeeBank.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
//            override fun onItemSelected(adapterView: AdapterView<*>?, view: View, i: Int, l: Long) {
//                selectedBranchId = 0
//                et_payeeBankAndBranch.setText("")
//            }
//
//            override fun onNothingSelected(adapterView: AdapterView<*>?) {
//                return
//            }
//        })

        et_chequeIssueDate.setOnClickListener {
            validate!!.datePicker(
//                validate!!.Daybetweentime("01-01-1990"),
                et_chequeIssueDate
            )
        }

        et_chequeReceiveDate.setOnClickListener {
            validate!!.datePicker(
//                validate!!.Daybetweentime("01-01-1990"),
                et_chequeReceiveDate
            )
        }

        btn_cancel.setOnClickListener {
            activity!!.onBackPressed()
        }

        et_payeeBankAndBranch.setOnFocusChangeListener(object : View.OnFocusChangeListener {
            override fun onFocusChange(v: View?, hasFocus: Boolean) {
                if (!hasFocus) {
                    cv_branch.visibility = View.GONE
                }
            }

        })

        et_payeeBankAndBranch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                var data = s.toString()
                selectedBranchId = 0
                if (spin_payeeBank.selectedItemPosition == 0) {
                    validate!!.CustomAlertSpinner(
                        requireActivity(), spin_payeeBank,
                        "Please select Payee Bank first !"
                    )
                } else {
                    var payeebankID =
                        bankList!!.get((spin_payeeBank.selectedItemPosition) - 1).bankId

                    if (data.length > 0) {

                        if (progressBar1 != null){
                            progressBar1!!.dismiss()
                        }
                        if (call != null) {
                            call!!.cancel()
                        }

                        if (data.length == 1) {
                            progressBar1 = ProgressDialog(context!!)
                            progressBar1!!.setCancelable(false)
                            progressBar1!!.setMessage(context!!.getString(R.string.msgLoading))
                            progressBar1!!.show()
                        }


                        val user = "IUP_BPMQA"
                        call = apiInterface?.getBranchListData(
                            "application/json",
                            token,
                            user,
                            payeebankID!!,
                            data
                        )

                        call?.enqueue(object : Callback<ArrayList<GetBranchListResponseItem>> {
                            override fun onFailure(
                                callCount: Call<ArrayList<GetBranchListResponseItem>>,
                                t: Throwable
                            ) {
                                t.printStackTrace()
                                progressBar1!!.dismiss()
                                Log.e("onFailure", "" + t)
                            }

                            override fun onResponse(
                                call: Call<ArrayList<GetBranchListResponseItem>>,
                                response: Response<ArrayList<GetBranchListResponseItem>>
                            ) {
                                Log.e("getMeetings --> ", "" + response.code())
                                Log.e("getMeetings --> ", "" + response.body())
                                if (response.isSuccessful) {

                                    try {
                                        var branchList = response.body()!!

                                        val names: MutableList<String> = ArrayList()
                                        val ids: MutableList<String> = ArrayList()
                                        for (i in 0 until branchList!!.size) {
                                            names.add(branchList!![i].bankBranchName!!)
                                            ids.add(branchList!![i].bankBranchId!!.toString())
                                        }


                                        if (!is_recycle_scroll) {
                                            scrollbar.scrollBy(0, 200);
                                            scrollbar.scrollBy(0, 200);
                                            scrollbar.scrollBy(0, 200);
                                            is_recycle_scroll = true
                                        }

                                        progressBar1!!.dismiss()

                                        if (branchList.size > 0) {
                                            adapter =
                                                BranchAdapter(context!!, branchList!!, listener)
                                            recycle_branch.adapter = adapter
                                            recycle_branch.layoutManager =
                                                LinearLayoutManager(context)
                                            cv_branch.visibility = View.VISIBLE

                                        } else {
                                            var branchList1 = ArrayList<GetBranchListResponseItem>()
                                            adapter =
                                                BranchAdapter(context!!, branchList1!!, listener)
                                            recycle_branch.adapter = adapter
                                            recycle_branch.layoutManager =
                                                LinearLayoutManager(context)
                                            cv_branch.visibility = View.GONE
                                        }

                                    } catch (ex: Exception) {
                                        ex.printStackTrace()
                                    }

                                } else {
                                    progressBar1!!.dismiss()
                                }
                            }
                        })
                    } else {

                        if (call != null) {
                            call!!.cancel()
                        }

                        var branchList1 = ArrayList<GetBranchListResponseItem>()
                        adapter = BranchAdapter(context!!, branchList1!!, listener)
                        recycle_branch.adapter = adapter
                        recycle_branch.layoutManager = LinearLayoutManager(context)

                        cv_branch.visibility = View.GONE
                    }
                }
            }

        })

        btn_save.setOnClickListener {

            var checkno = et_checkNo.text.toString()
            var issuedate = et_chequeIssueDate.toString()
            var ReceiveDate = et_chequeReceiveDate.text.toString()
            var amount = et_amount.text.toString()
            var voucher = et_voucher.text.toString()
            var narration = et_Narration.text.toString()

            if (checkno.isNullOrEmpty()) {
                validate!!.CustomAlertEditText(
                    "Please Insert Check Number",
                    requireActivity(),
                    et_checkNo
                )
            } else if (issuedate.isNullOrEmpty()) {
                validate!!.CustomAlertEditText(
                    "Please Select Cheque Issue Date",
                    requireActivity(),
                    et_chequeIssueDate
                )
            } else if (ReceiveDate.isNullOrEmpty()) {
                validate!!.CustomAlertEditText(
                    "Please Select Cheque Receive Date",
                    requireActivity(),
                    et_chequeReceiveDate
                )
            } else if (spin_payeeBank.selectedItemPosition == 0) {
                validate!!.CustomAlertSpinner(
                    requireActivity(), spin_payeeBank,
                    "Please select Payee Bank"
                )
            } else if (selectedBranchId.toInt() == 0) {
                validate!!.CustomAlertEditText(
                    "Please select Payee Branch",
                    requireActivity(), et_payeeBankAndBranch
                )
            } else if (amount.isNullOrEmpty()) {
                validate!!.CustomAlertEditText(
                    "Please Insert Amount",
                    requireActivity(),
                    et_amount
                )
            }
//            else if (voucher.isNullOrEmpty()){
//                validate!!.CustomAlert("insert Voucher Number", requireActivity())
//            }
            else if (narration.isNullOrEmpty()) {
                validate!!.CustomAlertEditText(
                    "Please Insert Narration",
                    requireActivity(),
                    et_Narration
                )
            } else if (spin_receivingBankBranch.selectedItemPosition == 0) {
                validate!!.CustomAlertSpinner(
                    requireActivity(), spin_receivingBankBranch,
                    "Please select Receiving Bank & Branch"
                )
            } else {
                val sDate1 = et_chequeIssueDate.text.toString()
                val date1 = SimpleDateFormat("dd-MM-yyyy").parse(sDate1)
                var Checkissuedate = date1.getTime() / 1000

                val sDate2 = et_chequeReceiveDate.text.toString()
                val date2 = SimpleDateFormat("dd-MM-yyyy").parse(sDate2)
                var CheckReceivedate = date2.getTime() / 1000

                var voucherDate = null
                var txnDate = null
                var txnNumber = null
                var type = null

                var payeebankID =
                    bankList!!.get((spin_payeeBank.selectedItemPosition) - 1).bankId

                var receivebankID =
                    bankAccountList!!.get((spin_receivingBankBranch.selectedItemPosition) - 1).accountNo!!.toString()

                var voamount = 0
                var camount = 0
                if ((activity as ReceiptDetailActivity).vo_amount.isNullOrEmpty()) {
                    voamount = 0
                } else {
                    voamount = (activity as ReceiptDetailActivity).vo_amount.toInt()
                }
                if ((activity as ReceiptDetailActivity).c_amount.isNullOrEmpty()) {
                    camount = 0
                } else {
                    camount = (activity as ReceiptDetailActivity).c_amount.toInt()
                }

                var clfDetail = ClfSavingReceiptDetails(

                    voamount.toInt(),
                    et_checkNo.text.toString(),
                    Checkissuedate.toString(),
                    CheckReceivedate.toString(),
                    payeebankID,  //payeeBankId
                    selectedBranchId,  //payeeBankBranchId
                    camount.toInt(),
                    et_voucher.text.toString(),
                    et_Narration.text.toString(),
                    receivebankID,  //bankAccountId
                    voucherDate,
                    1,  //modeOfPayment
                    txnDate,
                    txnNumber,
                    type
                )
                var objectData = SavingRequestModelItem(vo_id, meetingData!!.uid!!, clfDetail)

                if (Savinglist!!.size > 0) {
                    var is_present = false
                    for (i in 0 until Savinglist!!.size) {
                        if (Savinglist!!.get(i).voId == vo_id) {
                            Savinglist!!.remove(Savinglist!!.get(i))
                            Savinglist!!.add(objectData)
                            var SavinglistString = gson!!.toJson(Savinglist)
                            validate!!.SaveSharepreferenceString(
                                AppSP.ClfSavingList,
                                SavinglistString
                            )
                            break
                        }
                    }

                    if (!is_present) {
                        Savinglist!!.add(objectData)
                        var SavinglistString = gson!!.toJson(Savinglist)
                        validate!!.SaveSharepreferenceString(
                            AppSP.ClfSavingList,
                            SavinglistString
                        )
                    }
                    validate!!.CustomAlertClfSaving(
                        "Data saved successfully",
                        requireActivity()
                    )

                } else {
                    Savinglist!!.add(objectData)
                    var SavinglistString = gson!!.toJson(Savinglist)
                    validate!!.SaveSharepreferenceString(AppSP.ClfSavingList, SavinglistString)

                    validate!!.CustomAlertClfSaving(
                        "Data saved successfully",
                        requireActivity()
                    )
                }
            }
        }
    }

    private fun setData() {

        if (Savinglist!!.size > 0) {
            var is_content_vo = false
            var pos = 0
            for (i in 0 until Savinglist!!.size) {
                if (Savinglist!!.get(i).voId == vo_id) {
                    is_content_vo = true
                    pos = i
                }
            }

            if (is_content_vo && Savinglist!!.get(pos).clfSavingReceiptDetails!!.modePayment == 1) {
                var i = pos

                et_checkNo.setText(Savinglist!!.get(i).clfSavingReceiptDetails!!.chequeNo.toString())

//                et_payeeBankAndBranch.setText(Savinglist!!.get(i).clfSavingReceiptDetails!!.payeeBankBranchId.toString())
//                selectedBranchId = Savinglist!!.get(i).clfSavingReceiptDetails!!.payeeBankBranchId!!.toLong()

                var chequeIssued: Long =
                    java.lang.Long.valueOf(Savinglist!!.get(i).clfSavingReceiptDetails!!.chequeIssuedDate!!) * 1000 // its need to be in milisecond
                val df = Date(chequeIssued)
                val date1 = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(df)
                et_chequeIssueDate.setText(date1.toString())

                var chequeReceive: Long =
                    java.lang.Long.valueOf(Savinglist!!.get(i).clfSavingReceiptDetails!!.chequeReceivedDate!!) * 1000 // its need to be in milisecond
                val df1 = Date(chequeReceive)
                val date2 = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(df1)
                et_chequeReceiveDate.setText(date2.toString())


                et_voucher.setText(Savinglist!!.get(i).clfSavingReceiptDetails!!.voucherNumber.toString())

                et_Narration.setText(Savinglist!!.get(i).clfSavingReceiptDetails!!.narration.toString())
            } else {
                if (savingData != null) {
                    if (savingData!!.modePayment == 1) {

                        et_checkNo.setText(savingData!!.chequeNo.toString())

//                        et_payeeBankAndBranch.setText(savingData!!.payeeBankBranchId.toString())
//                        selectedBranchId = savingData!!.payeeBankBranchId!!.toLong()

                        var chequeIssued: Long =
                            java.lang.Long.valueOf(savingData!!.chequeIssuedDate!!) * 1000 // its need to be in milisecond
                        val df = Date(chequeIssued)
                        val date1 = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(df)
                        et_chequeIssueDate.setText(date1.toString())

                        var chequeReceive: Long =
                            java.lang.Long.valueOf(savingData!!.chequeReceivedDate!!) * 1000 // its need to be in milisecond
                        val df1 = Date(chequeReceive)
                        val date2 = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(df1)
                        et_chequeReceiveDate.setText(date2.toString())

                        et_voucher.setText(savingData!!.voucherNumber.toString())

                        et_Narration.setText(savingData!!.narration.toString())

                    }
                }
            }
        } else if (savingData != null) {
            if (savingData!!.modePayment == 1) {

                et_checkNo.setText(savingData!!.chequeNo.toString())

//                et_payeeBankAndBranch.setText(savingData!!.payeeBankBranchId.toString())
//                selectedBranchId = savingData!!.payeeBankBranchId!!.toLong()

                var chequeIssued: Long =
                    java.lang.Long.valueOf(savingData!!.chequeIssuedDate!!) * 1000 // its need to be in milisecond
                val df = Date(chequeIssued)
                val date1 = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(df)
                et_chequeIssueDate.setText(date1.toString())

                var chequeReceive: Long =
                    java.lang.Long.valueOf(savingData!!.chequeReceivedDate!!) * 1000 // its need to be in milisecond
                val df1 = Date(chequeReceive)
                val date2 = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(df1)
                et_chequeReceiveDate.setText(date2.toString())

                et_voucher.setText(savingData!!.voucherNumber.toString())

                et_Narration.setText(savingData!!.narration.toString())
            }
        }
    }

    private fun getBankList() {
        val progressBar = ProgressDialog(context!!)
        progressBar.setCancelable(false)
        progressBar.setMessage(this.getString(R.string.msgLoading))
        progressBar.show()


        val user = "IUP_BPMQA"

        val call = apiInterface?.getBankListData(
            "application/json",
            token,
            user
        )

        call?.enqueue(object : Callback<ArrayList<GetBankListResponseItem>> {
            override fun onFailure(
                callCount: Call<ArrayList<GetBankListResponseItem>>,
                t: Throwable
            ) {
                t.printStackTrace()
                Log.e("onFailure", "" + t)
                progressBar.dismiss()
            }

            override fun onResponse(
                call: Call<ArrayList<GetBankListResponseItem>>,
                response: Response<ArrayList<GetBankListResponseItem>>
            ) {

                Log.e("getMeetings --> ", "" + response.code())
                Log.e("getMeetings --> ", "" + response.body())
                progressBar.dismiss()
                if (response.isSuccessful) {

                    try {
                        bankList = response.body()!!

                        val names: MutableList<String> = ArrayList()
                        val ids: MutableList<String> = ArrayList()
                        for (i in 0 until bankList!!.size) {
                            names.add(bankList!![i].bankName!!)
                            ids.add(bankList!![i].bankId!!.toString())
                        }

//                        setSpinnerAdapter(spinnerType!!, names, ids)

                        fillBankSpinner(activity!!, spin_payeeBank, bankList)

                        if (Savinglist!!.size > 0 && bankList!!.size > 0) {
                            var is_content_vo = false
                            var pos = 0
                            for (i in 0 until Savinglist!!.size) {
                                if (Savinglist!!.get(i).voId == vo_id) {
                                    is_content_vo = true
                                    pos = i
                                }
                            }


                            if (is_content_vo) {
                                for (i in 0 until bankList!!.size) {
                                    if (Savinglist!!.get(pos).clfSavingReceiptDetails!!.payeeBankId == bankList!!.get(
                                            i
                                        ).bankId
                                    ) {
                                        spin_payeeBank.setSelection(i + 1)
                                    }
                                }
                            } else if (savingData!!.payeeBankId != null && bankList!!.size > 0) {
                                for (i in 0 until bankList!!.size) {
                                    if (savingData!!.payeeBankId == bankList!!.get(i).bankId) {
                                        spin_payeeBank.setSelection(i + 1)
                                    }
                                }
                            }

                        } else if (savingData!!.payeeBankId != null && bankList!!.size > 0) {
                            for (i in 0 until bankList!!.size) {
                                if (savingData!!.payeeBankId == bankList!!.get(i).bankId) {
                                    spin_payeeBank.setSelection(i + 1)
                                }
                            }
                        }


                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }

                } else {
                    progressBar.dismiss()
                }
            }

        })
    }

    private fun getBankAccountList() {

        val progressBar = ProgressDialog(context!!)
        progressBar.setCancelable(false)
        progressBar.setMessage(this.getString(R.string.msgLoading))
        progressBar.show()


        val user = "IUP_BPMQA"
        val call = apiInterface?.getBankAccountListData(
            "application/json",
            token,
            user
        )

        call?.enqueue(object : Callback<ArrayList<GetBankAccountListResponseItem>> {
            override fun onFailure(
                callCount: Call<ArrayList<GetBankAccountListResponseItem>>,
                t: Throwable
            ) {
                t.printStackTrace()
                Log.e("onFailure", "" + t)
                progressBar.dismiss()
            }

            override fun onResponse(
                call: Call<ArrayList<GetBankAccountListResponseItem>>,
                response: Response<ArrayList<GetBankAccountListResponseItem>>
            ) {

                Log.e("getMeetings --> ", "" + response.code())
                Log.e("getMeetings --> ", "" + response.body())
                progressBar.dismiss()
                if (response.isSuccessful) {

                    try {
                        bankAccountList = response.body()!!

                        val names: MutableList<String> = ArrayList()
                        val ids: MutableList<String> = ArrayList()
                        for (i in 0 until bankAccountList!!.size) {
                            names.add(bankAccountList!![i].bankBranchName!!)
                            ids.add(bankAccountList!![i].accountNo!!.toString())
                        }

//                        setSpinnerAdapter(spinnerType!!, names, ids)

                        fillBankAccountSpinner(
                            activity!!,
                            spin_receivingBankBranch,
                            bankAccountList
                        )

                        if (Savinglist!!.size > 0 && bankAccountList!!.size > 0) {
                            var is_content_vo = false
                            var pos = 0
                            for (i in 0 until Savinglist!!.size) {
                                if (Savinglist!!.get(i).voId == vo_id) {
                                    is_content_vo = true
                                    pos = i
                                }
                            }


                            if (is_content_vo) {
                                for (i in 0 until bankAccountList!!.size) {
                                    if (Savinglist!!.get(pos).clfSavingReceiptDetails!!.bankAccountId!!.equals(
                                            bankAccountList!!.get(i).accountNo!!.toString()
                                        )
                                    ) {
                                        spin_receivingBankBranch.setSelection(i + 1)
                                    }
                                }
                            } else if (savingData!!.bankAccountId != null && bankAccountList!!.size > 0) {
                                for (i in 0 until bankAccountList!!.size) {
                                    if (savingData!!.bankAccountId.toString()
                                            .equals(bankAccountList!!.get(i).accountNo!!.toString())
                                    ) {
                                        spin_receivingBankBranch.setSelection(i + 1)
                                    }
                                }
                            }
                        } else if (savingData!!.bankAccountId != null && bankAccountList!!.size > 0) {
                            for (i in 0 until bankAccountList!!.size) {
                                var a = savingData!!.bankAccountId!!.toLong()
                                var b = bankAccountList!!.get(i).accountNo!!.toLong()
                                if (a == b) {
                                    spin_receivingBankBranch.setSelection(i + 1)
                                }
                            }
                        }
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }

                } else {
                    progressBar.dismiss()
                }
            }
        })
    }

    private fun setSpinnerAdapter(
        spinner: AppCompatSpinner,
        list: MutableList<String>,
        ids: MutableList<String>
    ) {
        if (list.isNotEmpty()) {
            val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
                context!!,
                android.R.layout.simple_spinner_dropdown_item,
                list
            )
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

            spinner.adapter = adapter

            spinner.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View?, position: Int, id: Long
                ) {
                    Log.e("selectedItem", "" + parent.selectedItem.toString())
                    Log.e("selectedIds", "" + ids[position])
                    bankId = ids[position]
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                }
            }
        }
    }

    fun fillBankSpinner(
        activity: Activity,
        spin: Spinner,
        data: List<GetBankListResponseItem>?
    ) {
        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)

            for (i in data.indices) {
                sValue[i + 1] = data[i].bankName
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }
    }

    fun fillBankAccountSpinner(
        activity: Activity,
        spin: Spinner,
        data: List<GetBankAccountListResponseItem>?
    ) {
        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)

            for (i in data.indices) {
                sValue[i + 1] = data[i].bankBranchName
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }
    }

    fun fillBranchSpinner(
        activity: Activity,
        spin: Spinner,
        data: List<GetBranchListResponseItem>?
    ) {
        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize)
//            sValue[0] = LabelSet.getText("Select", R.string.Select)

            for (i in data.indices) {
                sValue[i] = data[i].bankBranchName
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size)
//            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }
    }


    private var listener = object : BranchAdapter.OnItemClickListener {
        override fun onItemClick(
            dataList: GetBranchListResponseItem, pos: Int
        ) {
            et_payeeBankAndBranch.setText("" + dataList!!.bankBranchName!!)
            if (call != null) {
                call!!.cancel()
            }
            selectedBranchId = dataList!!.bankBranchId!!

            var branchList1 = ArrayList<GetBranchListResponseItem>()
            adapter!!.setData(branchList1)
            adapter!!.notifyDataSetChanged()
            cv_branch.visibility = View.GONE
            et_voucher.requestFocus()
        }
    }


}