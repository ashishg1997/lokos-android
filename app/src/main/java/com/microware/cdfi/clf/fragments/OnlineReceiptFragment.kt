package com.microware.cdfi.clf.fragments

import android.app.Activity
import android.app.ProgressDialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.widget.AppCompatSpinner
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.microware.cdfi.R
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.clf.activites.ClfMeetingSaving
import com.microware.cdfi.clf.activites.ReceiptDetailActivity
import com.microware.cdfi.clf.models.meetings.GetMeetingsResponse
import com.microware.cdfi.clf.models.saving.*
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.PrefUtil
import com.microware.cdfi.utility.Validate
import kotlinx.android.synthetic.main.buttons.*
import kotlinx.android.synthetic.main.fragment_cash_receipt.*
import kotlinx.android.synthetic.main.fragment_cheque_receipt.*
import kotlinx.android.synthetic.main.fragment_online_receipt.*
import kotlinx.android.synthetic.main.fragment_online_receipt.et_Narration
import kotlinx.android.synthetic.main.fragment_online_receipt.et_amount
import kotlinx.android.synthetic.main.fragment_online_receipt.et_voucher
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.ref.WeakReference
import java.lang.reflect.Type
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class OnlineReceiptFragment : Fragment() {

    var validate: Validate? = null
    var vo_id = 0
    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: ApiInterface? = null
    var Savinglist: ArrayList<SavingRequestModelItem>? = null
    var gson: Gson? = null
    var selectType: AppCompatSpinner? = null
    var savingData: GetMeetingVoSavingListResponseItem? = null
    var typelist: ArrayList<String>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_online_receipt, container, false)
        selectType = view.findViewById<View>(R.id.spin_online_type) as AppCompatSpinner
        return view
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        validate = Validate(context!!)

        vo_id = (activity as ReceiptDetailActivity).vo_id
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()

        Savinglist = ArrayList<SavingRequestModelItem>()
        var savingListdata = validate!!.RetriveSharepreferenceString(AppSP.ClfSavingList)

        val listType: Type = object : TypeToken<ArrayList<SavingRequestModelItem>>() {}.getType()
        gson = Gson()

        val mainJsonDataResponse: ArrayList<SavingRequestModelItem> =
            gson!!.fromJson(savingListdata, listType)
        Savinglist = mainJsonDataResponse

        savingData = (context as ReceiptDetailActivity).savingData


        val prefUtil = PrefUtil(WeakReference(requireActivity()))
        val meetingData = prefUtil.get<GetMeetingsResponse>("meeting_response")

        val sdf = SimpleDateFormat("dd-MM-yyy", Locale.getDefault())
        val currentDateandTime: String = sdf.format(Date())

        typelist = ArrayList<String>()
        typelist!!.add("RTGS")
        typelist!!.add("NEFT")
        typelist!!.add("Online")
        typelist!!.add("UPI")
        fillTypeSpinner(activity!!, spin_online_type, typelist!!)

        var amount =
            (context as ReceiptDetailActivity).c_amount.toInt() + (context as ReceiptDetailActivity).vo_amount.toInt()
                .toInt()
        et_amount.setText(amount.toString())

        setData()

        et_ReceiveDate.setText(currentDateandTime)

        et_ReceiveDate.setOnClickListener {
            validate!!.datePicker(
//                validate!!.Daybetweentime("01-01-1990"),
                et_ReceiveDate
            )
        }

        btn_cancel.setOnClickListener {
            activity!!.onBackPressed()
        }

        btn_save.setOnClickListener {

            var transaction_no = et_transaction_no.text.toString()
            var ReceiveDate = et_ReceiveDate.text.toString()
            var amount = et_amount.text.toString()
            var voucher = et_voucher.text.toString()
            var narration = et_Narration.text.toString()

            val sDate2 = et_ReceiveDate.text.toString()
            val date2 = SimpleDateFormat("dd-MM-yyyy").parse(sDate2)
            var txnDate = date2.getTime() / 1000

            var voucherDate = null
            var checkNo = null
            var issuedateCheck = null
            var ReceivedateCheck = null
            var payeebank = null
            var payeebankBranch = null
            var bankAccount = null

            if (spin_online_type.selectedItemPosition == 0) {
                validate!!.CustomAlertSpinner(
                    requireActivity(), spin_online_type,
                    "Please select Online Type"
                )
            } else if (transaction_no.isNullOrEmpty()) {
                validate!!.CustomAlertEditText(
                    "Please Insert Transaction Number",
                    requireActivity(),
                    et_transaction_no
                )
            } else if (ReceiveDate.isNullOrEmpty()) {
                validate!!.CustomAlertEditText(
                    "Please Select Cheque Receive Date",
                    requireActivity(),
                    et_chequeReceiveDate
                )
            } else if (amount.isNullOrEmpty()) {
                validate!!.CustomAlertEditText(
                    "Please Insert Amount",
                    requireActivity(),
                    et_amount
                )
            }
//            else if (voucher.isNullOrEmpty()){
//                validate!!.CustomAlert("insert Voucher Number", requireActivity())
//            }
            else if (narration.isNullOrEmpty()) {
                validate!!.CustomAlertEditText(
                    "Please Insert Narration",
                    requireActivity(),
                    et_Narration
                )
            } else {

                var voamount = 0
                var camount = 0
                if ((activity as ReceiptDetailActivity).vo_amount.isNullOrEmpty()) {
                    voamount = 0
                } else {
                    voamount = (activity as ReceiptDetailActivity).vo_amount.toInt()
                }
                if ((activity as ReceiptDetailActivity).c_amount.isNullOrEmpty()) {
                    camount = 0
                } else {
                    camount = (activity as ReceiptDetailActivity).c_amount.toInt()
                }

                var type: String? = null
                if (spin_online_type.selectedItemPosition == 0) {
                    type = null
                } else {
                    type = typelist!!.get((spin_online_type.selectedItemPosition) - 1)
                }

                var clfDetail = ClfSavingReceiptDetails(
                    voamount.toInt(),
                    checkNo,
                    issuedateCheck,
                    ReceivedateCheck,
                    payeebank,  //payeeBankId
                    payeebankBranch,  //payeeBankBranchId
                    camount.toInt(),
                    et_voucher.text.toString(),
                    et_Narration.text.toString(),
                    bankAccount,  //bankAccountId
                    voucherDate,
                    2,  //modeOfPayment
                    txnDate.toString(),
                    transaction_no.toString(),
                    type
                )
                var objectData = SavingRequestModelItem(vo_id, meetingData!!.uid!!, clfDetail)

                if (Savinglist!!.size > 0) {
                    var is_present = false
                    for (i in 0 until Savinglist!!.size) {
                        if (Savinglist!!.get(i).voId == vo_id) {
                            Savinglist!!.remove(Savinglist!!.get(i))
                            Savinglist!!.add(objectData)
                            var SavinglistString = gson!!.toJson(Savinglist)
                            validate!!.SaveSharepreferenceString(
                                AppSP.ClfSavingList,
                                SavinglistString
                            )
                            break
                        }
                    }

                    if (!is_present) {
                        Savinglist!!.add(objectData)
                        var SavinglistString = gson!!.toJson(Savinglist)
                        validate!!.SaveSharepreferenceString(AppSP.ClfSavingList, SavinglistString)
                    }
                    validate!!.CustomAlertClfSaving(
                        "Data saved successfully",
                        requireActivity()
                    )

                } else {
                    Savinglist!!.add(objectData)
                    var SavinglistString = gson!!.toJson(Savinglist)
                    validate!!.SaveSharepreferenceString(AppSP.ClfSavingList, SavinglistString)

                    validate!!.CustomAlertClfSaving(
                        "Data saved successfully",
                        requireActivity()
                    )
                }
            }
        }
    }


    fun fillTypeSpinner(activity: Activity, spin: Spinner, data: List<String>?) {
        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)

            for (i in data.indices) {
                sValue[i + 1] = data[i]
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }
    }


    private fun setData() {
        if (Savinglist!!.size > 0) {
            var is_content_vo = false
            var pos = 0
            for (i in 0 until Savinglist!!.size) {
                if (Savinglist!!.get(i).voId == vo_id) {
                    is_content_vo = true
                    pos = i
                }
            }

            if (is_content_vo && Savinglist!!.get(pos).clfSavingReceiptDetails!!.modePayment == 2) {
                var i = pos

                var ReceiveDate: Long =
                    java.lang.Long.valueOf(Savinglist!!.get(i).clfSavingReceiptDetails!!.txnDate!!) * 1000 // its need to be in milisecond
                val df = Date(ReceiveDate)
                val date1 = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(df)

                for (j in 0 until typelist!!.size) {
                    if (typelist!!.get(j)
                            .equals(Savinglist!!.get(pos).clfSavingReceiptDetails!!.type)
                    ) {
                        spin_online_type.setSelection(j + 1)
                        break
                    }
                }

                et_ReceiveDate.setText(date1.toString())

                et_transaction_no.setText(Savinglist!!.get(i).clfSavingReceiptDetails!!.transactionNo.toString())

                et_voucher.setText(Savinglist!!.get(i).clfSavingReceiptDetails!!.voucherNumber.toString())

                et_Narration.setText(Savinglist!!.get(i).clfSavingReceiptDetails!!.narration.toString())


            } else if (savingData != null) {
                if (savingData!!.modePayment == 2) {

                    var ReceiveDate: Long =
                        java.lang.Long.valueOf(savingData!!.txnDate!!) * 1000 // its need to be in milisecond
                    val df = Date(ReceiveDate)
                    val date1 = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(df)

                    for (i in 0 until typelist!!.size) {
                        if (typelist!!.get(i).equals(savingData!!.type)) {
                            spin_online_type.setSelection(i + 1)
                        }
                    }

                    et_ReceiveDate.setText(date1.toString())

                    et_transaction_no.setText(savingData!!.transactionNo.toString())

                    et_voucher.setText(savingData!!.voucherNumber.toString())

                    et_Narration.setText(savingData!!.narration.toString())

                }
            }
        } else if (savingData != null) {
            if (savingData!!.modePayment == 2) {

                var ReceiveDate: Long =
                    java.lang.Long.valueOf(savingData!!.txnDate!!) * 1000 // its need to be in milisecond
                val df = Date(ReceiveDate)
                val date1 = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(df)

                for (i in 0 until typelist!!.size) {
                    if (typelist!!.get(i).equals(savingData!!.type)) {
                        spin_online_type.setSelection(i + 1)
                    }
                }

                et_ReceiveDate.setText(date1.toString())

                et_transaction_no.setText(savingData!!.transactionNo.toString())

                et_voucher.setText(savingData!!.voucherNumber.toString())

                et_Narration.setText(savingData!!.narration.toString())
            }
        }
    }


}