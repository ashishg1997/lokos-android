package com.microware.cdfi.clf.fragments

import android.app.ProgressDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.clf.Constants
import com.microware.cdfi.clf.adapters.CEAttendanceAdapter
import com.microware.cdfi.clf.adapters.SHGLoanAdapter
import com.microware.cdfi.clf.models.get_meeting_participants.ClfMtgEcParticipant
import com.microware.cdfi.clf.models.get_meeting_participants.GetMeetingParticipants
import com.microware.cdfi.clf.models.meetings.GetMeetingsResponse
import com.microware.cdfi.interfaces.OnListItemClickListener
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.PrefUtil
import com.microware.cdfi.utility.Validate
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.ref.WeakReference

class SHGLoanFragment : Fragment(), OnListItemClickListener {

    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: ApiInterface? = null
    var validate: Validate? = null
    private var recyclerView: RecyclerView? = null
    lateinit var mlayoutManager: LinearLayoutManager

    var adapter: SHGLoanAdapter? = null
    var token: String = ""
    var presentCount = 0
    var absentCount = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_clf_shg_loan, container, false)
        validate = Validate(requireActivity())
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()

        initViews(view)

        return view
    }

    private fun initViews(view: View){

        recyclerView = view.findViewById(R.id.recyclerShgLoan)

        mlayoutManager = LinearLayoutManager(requireActivity())
        recyclerView!!.hasFixedSize()
        recyclerView!!.layoutManager = mlayoutManager
        recyclerView!!.itemAnimator = DefaultItemAnimator()

        val prefUtil = PrefUtil(WeakReference(requireActivity()))
        val meetingData = prefUtil.get<GetMeetingsResponse>("meeting_response")

        setAdapter()
//        getMeetingParticipants(meetingData!!.uid!!)


    }

    private fun getMeetingParticipants(uid: Int) {

        val progressBar = ProgressDialog(requireActivity())
        progressBar.setCancelable(false)
        progressBar.setMessage(this.getString(R.string.msgLoading))
        progressBar.show()

        val user = "IUP_BPM"
        val call = apiInterface?.getMeetingParticipants(
            "application/json",
            Constants.Token,
            Constants.userId,
            uid
        )

        call?.enqueue(object : Callback<GetMeetingParticipants> {

            override fun onFailure(callCount: Call<GetMeetingParticipants>, t: Throwable) {
                progressBar.dismiss()
                t.printStackTrace()
                Log.e("onFailure",""+t)

            }

            override fun onResponse(
                call: Call<GetMeetingParticipants>,
                response: Response<GetMeetingParticipants>
            ) {

                Log.e("getMeetingParticipants --> ", ""+response.code())
                Log.e("getMeetingParticipants --> ", ""+response.body())

                if (response.isSuccessful) {
                    progressBar.dismiss()
                    try {

//                        if (response.body()!!.clfMtgEcParticipants != null){
//                            setAdapter(response.body()!!.clfMtgEcParticipants!!)
//                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        Log.e("Exception",""+ex)
                    }

                } else {
                    progressBar.dismiss()
                    Log.e("else","else")
                }


            }

        })
    }

    private fun setAdapter(){
        adapter = SHGLoanAdapter(requireActivity())
        adapter!!.setListener(this)
        recyclerView!!.adapter = adapter
        adapter!!.notifyDataSetChanged()
    }

    override fun onListItemClicked(itemId: Int, data: Any) {
//        val item = data as ClfMtgEcParticipant

    }

    override fun onListItemLongClicked(itemId: Int, data: Any) {
        TODO("Not yet implemented")
    }


}