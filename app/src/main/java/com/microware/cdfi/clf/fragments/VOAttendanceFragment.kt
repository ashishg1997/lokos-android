package com.microware.cdfi.clf.fragments

import android.app.ProgressDialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.microware.cdfi.R
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.clf.Constants
import com.microware.cdfi.clf.adapters.CEAttendanceAdapter
import com.microware.cdfi.clf.adapters.VOAttendanceAdapter
import com.microware.cdfi.clf.models.get_meeting_participants.ClfMtgEcParticipant
import com.microware.cdfi.clf.models.get_meeting_participants.ClfMtgVoPartcipants
import com.microware.cdfi.clf.models.get_meeting_participants.GetMeetingParticipants
import com.microware.cdfi.clf.models.meetings.GetMeetingsResponse
import com.microware.cdfi.clf.models.save_attendance.SaveAttendance
import com.microware.cdfi.clf.models.save_attendance.SaveAttendanceResponse
import com.microware.cdfi.interfaces.OnListItemClickListener
import com.microware.cdfi.utility.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.ref.WeakReference

class VOAttendanceFragment : Fragment(), OnListItemClickListener, View.OnClickListener {

    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: ApiInterface? = null
    var validate: Validate? = null
    private var recyclerView: RecyclerView? = null
    private var txtPresentCount: TextView? = null
    private var txtAbsentCount: TextView? = null
    lateinit var mlayoutManager: LinearLayoutManager
    private var btnSave: AppCompatButton? = null

    var adapter: VOAttendanceAdapter? = null
    var meetingData: GetMeetingsResponse? = null
    var presentCount = 0
    var absentCount = 0
    var ceAttendanceList: MutableList<ClfMtgEcParticipant>? = ArrayList()
    var voAttendanceList: MutableList<ClfMtgVoPartcipants>? = ArrayList()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_vo_attendance, container, false)
        validate = Validate(requireActivity())
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()

        initViews(view)

        return view
    }

    private fun initViews(view: View){
        recyclerView = view.findViewById(R.id.recyclerVoAttendance)
        txtPresentCount = view.findViewById(R.id.txtPresentCount)
        txtAbsentCount = view.findViewById(R.id.txtAbsentCount)
        btnSave = view.findViewById(R.id.btnSave)

        mlayoutManager = LinearLayoutManager(requireActivity())
        recyclerView!!.hasFixedSize()
        recyclerView!!.layoutManager = mlayoutManager
        recyclerView!!.itemAnimator = DefaultItemAnimator()

        btnSave!!.setOnClickListener(this)

        val prefUtil = PrefUtil(WeakReference(requireActivity()))
        meetingData = prefUtil.get<GetMeetingsResponse>("meeting_response")

        txtPresentCount!!.text = ""+presentCount
        txtAbsentCount!!.text = ""+absentCount
        getMeetingParticipants(meetingData!!.uid!!)
    }

    private fun getMeetingParticipants(uid: Int) {

        val progressBar = ProgressDialog(requireActivity())
        progressBar.setCancelable(false)
        progressBar.setMessage(this.getString(R.string.msgLoading))
        progressBar.show()

        val call = apiInterface?.getMeetingParticipants(
            "application/json",
            Constants.Token,
            Constants.userId,
            uid
        )

        call?.enqueue(object : Callback<GetMeetingParticipants> {

            override fun onFailure(callCount: Call<GetMeetingParticipants>, t: Throwable) {
                progressBar.dismiss()
                t.printStackTrace()
                Log.e("onFailure",""+t)

            }

            override fun onResponse(
                call: Call<GetMeetingParticipants>,
                response: Response<GetMeetingParticipants>
            ) {

                Log.e("getMeetingParticipants --> ", ""+response.code())
                Log.e("getMeetingParticipants --> ", ""+response.body())

                if (response.isSuccessful) {
                    progressBar.dismiss()
                    try {

                        if (response.body()!!.clfMtgEcParticipants != null){
//                            ceAttendanceList = response.body()!!.clfMtgEcParticipants!!
                            voAttendanceList = response.body()!!.clfMtgVoPartcipants!!
//                            ceAttendanceList!!.forEach { it.attendance = "p" }
                            voAttendanceList!!.forEach { it.attendance = "p" }
                            setAdapter(voAttendanceList!!)
                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        Log.e("Exception",""+ex)
                    }

                } else {
                    progressBar.dismiss()
                    Log.e("else","else")
                }


            }

        })
    }

    private fun setAdapter(clfMtgVoParticipant: MutableList<ClfMtgVoPartcipants>){
        adapter = VOAttendanceAdapter(requireActivity(),clfMtgVoParticipant)
        adapter!!.setListener(this)
        recyclerView!!.adapter = adapter
        adapter!!.notifyDataSetChanged()

        presentCount = clfMtgVoParticipant.filter { it.attendance == "p" }.size
        absentCount = clfMtgVoParticipant.filter { it.attendance == "a" }.size
        txtPresentCount!!.text = ""+presentCount
        txtAbsentCount!!.text = ""+absentCount

    }

    override fun onListItemClicked(itemId: Int, data: Any) {
        val item = data as ClfMtgVoPartcipants
        if (item.attendance != null && item.attendance == "p"){
            voAttendanceList?.find { it.voId == item.voId }?.attendance = "p"
        }else{
            voAttendanceList?.find { it.voId == item.voId }?.attendance = "a"
        }

        presentCount = voAttendanceList?.filter { it.attendance == "p" }!!.size
        absentCount = voAttendanceList?.filter { it.attendance == "a" }!!.size
        txtPresentCount!!.text = ""+presentCount
        txtAbsentCount!!.text = ""+absentCount

    }

    override fun onListItemLongClicked(itemId: Int, data: Any) {
        TODO("Not yet implemented")
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnSave -> {
                val saveAttendance = SaveAttendance()
                saveAttendance.clfMtgEcParticipants = ceAttendanceList
                saveAttendance.clfMtgVoPartcipants = voAttendanceList
                saveClfAttendance(saveAttendance)
            }
        }

    }


    private fun saveClfAttendance(saveAttendance: SaveAttendance) {

        Log.e("saveAttendance",""+ Gson().toJson(saveAttendance))
        Log.e("meetingData",""+meetingData!!.uid!!)

        val progressBar = ProgressDialog(requireActivity())
        progressBar.setCancelable(false)
        progressBar.setMessage(this.getString(R.string.msgLoading))
        progressBar.show()

        val call = apiInterface?.saveClfAttendance(
            "application/json",
            Constants.Token,
            Constants.userId,
            meetingData!!.uid!!,
            saveAttendance
        )

        call?.enqueue(object : Callback<SaveAttendanceResponse> {
            override fun onFailure(callCount: Call<SaveAttendanceResponse>, t: Throwable) {
                t.printStackTrace()
                Log.e("onFailure", "" + t)
                progressBar.dismiss()
            }

            override fun onResponse(
                call: Call<SaveAttendanceResponse>,
                response: Response<SaveAttendanceResponse>
            ) {

                Log.e("saveClfAttendance --> ", "" + response.code())
                Log.e("saveClfAttendance --> ", "" + response.body())
                progressBar.dismiss()
                if (response.isSuccessful) {
                    Toast.makeText(requireActivity(), "Saved successfully", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(requireActivity(), "Something went wrong please try again", Toast.LENGTH_SHORT).show()
                    progressBar.dismiss()
                }


            }

        })
    }

}