package com.microware.cdfi.clf.models.create_meeting

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CreateMeeting {
    @SerializedName("mtgDate")
    @Expose
    var mtgDate: Long? = null

    @SerializedName("mtgType")
    @Expose
    var mtgType: String? = null

    @SerializedName("mtgNo")
    @Expose
    var mtgNo: Int? = null

    @SerializedName("mtgGuid")
    @Expose
    var mtgGuid: String? = null
}