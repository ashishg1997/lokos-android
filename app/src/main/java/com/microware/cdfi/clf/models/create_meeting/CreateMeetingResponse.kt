package com.microware.cdfi.clf.models.create_meeting

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CreateMeetingResponse {
    @SerializedName("meetingId")
    @Expose
    var meetingId: String? = null
}