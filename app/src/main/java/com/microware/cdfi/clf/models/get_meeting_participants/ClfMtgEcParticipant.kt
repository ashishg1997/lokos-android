package com.microware.cdfi.clf.models.get_meeting_participants

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class ClfMtgEcParticipant {
    @SerializedName("ecMemberId")
    @Expose
    var ecMemberId: Int? = null

    @SerializedName("memberId")
    @Expose
    var memberId: Int? = null

    @SerializedName("memberName")
    @Expose
    var memberName: String? = null

    @SerializedName("memberPost")
    @Expose
    var memberPost: String? = null

    @SerializedName("cboId")
    @Expose
    var cboId: Int? = null

    @SerializedName("cboLevel")
    @Expose
    var cboLevel: Int? = null

    @SerializedName("attendance")
    @Expose
    var attendance: String? = null
}