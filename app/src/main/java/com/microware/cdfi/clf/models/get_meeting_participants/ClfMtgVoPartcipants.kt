package com.microware.cdfi.clf.models.get_meeting_participants

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class ClfMtgVoPartcipants {
    @SerializedName("voId")
    @Expose
    var voId: Int? = null

    @SerializedName("voName")
    @Expose
    var voName: String? = null

    @SerializedName("attendance")
    @Expose
    var attendance: String? = null

    @SerializedName("vocode")
    @Expose
    var voCode: String? = null

    @field:SerializedName("voluntarySavingsAmount")
    var voluntarySavingsAmount: Int? = 0

    @field:SerializedName("compulsorySavingsAmount")
    var compulsorySavingsAmount: Int? = 0
}