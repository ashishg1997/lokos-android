package com.microware.cdfi.clf.models.get_meeting_participants

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class GetMeetingParticipants {
    @SerializedName("clfMtgEcParticipants")
    @Expose
    var clfMtgEcParticipants: MutableList<ClfMtgEcParticipant>? = null

    @SerializedName("clfMtgVoPartcipants")
    @Expose
    var clfMtgVoPartcipants: MutableList<ClfMtgVoPartcipants>? = null

    @SerializedName("responseMessage")
    @Expose
    var responseMessage: Any? = null
}