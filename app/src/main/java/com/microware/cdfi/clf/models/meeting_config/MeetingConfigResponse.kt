package com.microware.cdfi.clf.models.meeting_config

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class MeetingConfigResponse {
    @SerializedName("lastMtgId")
    @Expose
    var lastMtgId: Int? = null

    @SerializedName("lastMtgDate")
    @Expose
    var lastMtgDate: Long? = null

    @SerializedName("lastMtgNo")
    @Expose
    var lastMtgNo: Int? = null

    @SerializedName("lastMtgType")
    @Expose
    var lastMtgType: String? = null

    @SerializedName("mtgTypes")
    @Expose
    var mtgTypes: MutableList<MtgType>? = null

    @SerializedName("responseMessage")
    @Expose
    var responseMessage: Any? = null
}