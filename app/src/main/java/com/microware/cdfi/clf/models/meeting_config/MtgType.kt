package com.microware.cdfi.clf.models.meeting_config

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class MtgType {
    @SerializedName("mtgTypeCode")
    @Expose
    var mtgTypeCode: String? = null

    @SerializedName("mtgTypeName")
    @Expose
    var mtgTypeName: String? = null
}