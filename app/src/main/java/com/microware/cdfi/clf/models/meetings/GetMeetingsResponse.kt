package com.microware.cdfi.clf.models.meetings

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class GetMeetingsResponse {
    @SerializedName("uid")
    @Expose
    var uid: Int? = null

    @SerializedName("cboId")
    @Expose
    var cboId: Int? = null

    @SerializedName("mtgDate")
    @Expose
    var mtgDate: Long? = null

    @SerializedName("mtgType")
    @Expose
    var mtgType: String? = null

    @SerializedName("mtgGuid")
    @Expose
    var mtgGuid: String? = null

    @SerializedName("mtgNo")
    @Expose
    var mtgNo: Int? = null

    @SerializedName("flagOpen")
    @Expose
    var flagOpen: Any? = null

    @SerializedName("createdBy")
    @Expose
    var createdBy: String? = null

    @SerializedName("createdOn")
    @Expose
    var createdOn: Any? = null

    @SerializedName("updatedBy")
    @Expose
    var updatedBy: String? = null

    @SerializedName("updatedOn")
    @Expose
    var updatedOn: Any? = null

    @SerializedName("uploadedBy")
    @Expose
    var uploadedBy: Any? = null

    @SerializedName("uploadedOn")
    @Expose
    var uploadedOn: Any? = null

    @SerializedName("ec1")
    @Expose
    var ec1: Int? = null

    @SerializedName("ec2")
    @Expose
    var ec2: Int? = null

    @SerializedName("ec3")
    @Expose
    var ec3: Int? = null

    @SerializedName("ec4")
    @Expose
    var ec4: Int? = null

    @SerializedName("ec5")
    @Expose
    var ec5: Int? = null

    @SerializedName("createdOn1")
    @Expose
    var createdOn1: Any? = null
}