package com.microware.cdfi.clf.models.save_attendance

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class ClfMtgVoPartcipant {
    @SerializedName("voId")
    @Expose
    var voId: Int? = null

    @SerializedName("voName")
    @Expose
    var voName: String? = null

    @SerializedName("attendance")
    @Expose
    var attendance: String? = null

    @SerializedName("voCode")
    @Expose
    var voCode: String? = null
}