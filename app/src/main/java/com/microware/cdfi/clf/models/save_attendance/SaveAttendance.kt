package com.microware.cdfi.clf.models.save_attendance

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import com.microware.cdfi.clf.models.get_meeting_participants.ClfMtgEcParticipant
import com.microware.cdfi.clf.models.get_meeting_participants.ClfMtgVoPartcipants


class SaveAttendance {
    @SerializedName("clfMtgEcParticipants")
    @Expose
    var clfMtgEcParticipants: MutableList<ClfMtgEcParticipant>? = null

    @SerializedName("clfMtgVoPartcipants")
    @Expose
    var clfMtgVoPartcipants: MutableList<ClfMtgVoPartcipants>? = null

    @SerializedName("responseMessage")
    @Expose
    var responseMessage: Any? = null
}