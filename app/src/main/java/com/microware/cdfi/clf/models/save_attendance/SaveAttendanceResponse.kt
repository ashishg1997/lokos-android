package com.microware.cdfi.clf.models.save_attendance

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class SaveAttendanceResponse {
    @SerializedName("meetingDetailIds")
    @Expose
    var meetingDetailIds: String? = null
}