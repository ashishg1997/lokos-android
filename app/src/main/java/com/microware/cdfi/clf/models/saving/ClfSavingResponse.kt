package com.microware.cdfi.clf.models.saving

import com.google.gson.annotations.SerializedName

data class ClfSavingResponse(

	@field:SerializedName("savingsId")
	val savingsId: String? = null
)
