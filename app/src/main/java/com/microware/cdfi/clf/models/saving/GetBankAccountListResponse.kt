package com.microware.cdfi.clf.models.saving

import com.google.gson.annotations.SerializedName

data class GetBankAccountListResponse(

	@field:SerializedName("GetBankAccountListResponse")
	val getBankAccountListResponse: List<GetBankAccountListResponseItem?>? = null
)

data class GetBankAccountListResponseItem(

	@field:SerializedName("bankBranchId")
	val bankBranchId: Int? = null,

	@field:SerializedName("bankId")
	val bankId: Int? = null,

	@field:SerializedName("bankBranchCode")
	val bankBranchCode: String? = null,

	@field:SerializedName("bankBranchName")
	val bankBranchName: String? = null,

	@field:SerializedName("accountNo")
	val accountNo: String? = null,

	@field:SerializedName("ifscCode")
	val ifscCode: String? = null,

	@field:SerializedName("cboBankId")
	val cboBankId: Int? = null
)
