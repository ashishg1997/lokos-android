package com.microware.cdfi.clf.models.saving

import com.google.gson.annotations.SerializedName

data class GetBankListResponse(

	@field:SerializedName("GetBankListResponse")
	val getBankListResponse: List<GetBankListResponseItem?>? = null
)

data class GetBankListResponseItem(

	@field:SerializedName("bankCode")
	val bankCode: String? = null,

	@field:SerializedName("bankId")
	val bankId: Int? = null,

	@field:SerializedName("bankName")
	val bankName: String? = null
)
