package com.microware.cdfi.clf.models.saving

import com.google.gson.annotations.SerializedName

data class GetBranchListResponse(

	@field:SerializedName("GetBranchListResponse")
	val getBranchListResponse: List<GetBranchListResponseItem?>? = null
)

data class GetBranchListResponseItem(

	@field:SerializedName("bankBranchId")
	val bankBranchId: Long? = null,

	@field:SerializedName("bankId")
	val bankId: Int? = null,

	@field:SerializedName("bankBranchCode")
	val bankBranchCode: String? = null,

	@field:SerializedName("bankBranchName")
	val bankBranchName: String? = null,

	@field:SerializedName("bankBranchAddress")
	val bankBranchAddress: String? = null,

	@field:SerializedName("ifscCode")
	val ifscCode: String? = null
)
