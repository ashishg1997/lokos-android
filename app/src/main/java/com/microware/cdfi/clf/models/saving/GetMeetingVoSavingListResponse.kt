package com.microware.cdfi.clf.models.saving

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class GetMeetingVoSavingListResponse(

	@field:SerializedName("GetMeetingVoSavingListResponse")
	val getMeetingVoSavingListResponse: List<GetMeetingVoSavingListResponseItem?>? = null
) : Serializable

data class GetMeetingVoSavingListResponseItem(

	@field:SerializedName("voluntarySavingsAmount")
	val voluntarySavingsAmount: Int? = null,

	@field:SerializedName("transactionNo")
	val transactionNo: Any? = null,

	@field:SerializedName("cboId")
	val cboId: Int? = null,

	@field:SerializedName("dateRealisation")
	val dateRealisation: Any? = null,

	@field:SerializedName("type")
	val type: Any? = null,

	@field:SerializedName("createdOn")
	val createdOn: Any? = null,

	@field:SerializedName("mtgGuid")
	val mtgGuid: String? = null,

	@field:SerializedName("payeeBankId")
	val payeeBankId: Int? = null,

	@field:SerializedName("payeeBankBranchId")
	val payeeBankBranchId: Long? = null,

	@field:SerializedName("uid")
	val uid: Int? = null,

	@field:SerializedName("compulsorySavingsAmount")
	val compulsorySavingsAmount: Int? = null,

	@field:SerializedName("uploadedOn")
	val uploadedOn: Any? = null,

	@field:SerializedName("voucherNumber")
	val voucherNumber: String? = null,

	@field:SerializedName("voucherDate")
	val voucherDate: Long? = null,

	@field:SerializedName("uploadedBy")
	val uploadedBy: Any? = null,

	@field:SerializedName("addlRefDate")
	val addlRefDate: Long? = null,

	@field:SerializedName("bankCode")
	val bankCode: Int? = null,

	@field:SerializedName("auid")
	val auid: Int? = null,

	@field:SerializedName("amount")
	val amount: Int? = null,

	@field:SerializedName("updatedBy")
	val updatedBy: String? = null,

	@field:SerializedName("chequeNo")
	val chequeNo: Int? = null,

	@field:SerializedName("memType")
	val memType: Any? = null,

	@field:SerializedName("updatedOn")
	val updatedOn: Any? = null,

	@field:SerializedName("chequeIssuedDate")
	val chequeIssuedDate: Long? = null,

	@field:SerializedName("chequeReceivedDate")
	val chequeReceivedDate: Long? = null,

	@field:SerializedName("referenceMtgNo")
	val referenceMtgNo: Int? = null,

	@field:SerializedName("createdBy")
	val createdBy: String? = null,

	@field:SerializedName("mtgNo")
	val mtgNo: Int? = null,

	@field:SerializedName("narration")
	val narration: String? = null,

	@field:SerializedName("bankAccountId")
	val bankAccountId: String? = null,

	@field:SerializedName("modePayment")
	val modePayment: Int? = null,

	@field:SerializedName("txnDate")
	val txnDate: Long? = null,

	@field:SerializedName("memId")
	val memId: Int? = null
) : Serializable
