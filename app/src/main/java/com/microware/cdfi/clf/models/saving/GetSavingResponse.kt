package com.microware.cdfi.clf.models.saving

import com.google.gson.annotations.SerializedName

data class GetSavingResponse(

	@field:SerializedName("bankCode")
	val bankCode: Any? = null,

	@field:SerializedName("depositedCash")
	val depositedCash: Any? = null,

	@field:SerializedName("otherWithdrawals")
	val otherWithdrawals: Any? = null,

	@field:SerializedName("updatedBy")
	val updatedBy: String? = null,

	@field:SerializedName("clfMtgUid")
	val clfMtgUid: Int? = null,

	@field:SerializedName("otherDeposits")
	val otherDeposits: Any? = null,

	@field:SerializedName("closingBalance")
	val closingBalance: Any? = null,

	@field:SerializedName("createdOn1")
	val createdOn1: Any? = null,

	@field:SerializedName("cboId")
	val cboId: Int? = null,

	@field:SerializedName("chequeReceivedNotCredited")
	val chequeReceivedNotCredited: Any? = null,

	@field:SerializedName("mtgGuid")
	val mtgGuid: String? = null,

	@field:SerializedName("withdrawnCash")
	val withdrawnCash: Any? = null,

	@field:SerializedName("chequeIssuedNotRealized")
	val chequeIssuedNotRealized: Any? = null,

	@field:SerializedName("uid")
	val uid: Int? = null,

	@field:SerializedName("closingBalanceCash")
	val closingBalanceCash: Any? = null,

	@field:SerializedName("updatedOn1")
	val updatedOn1: Any? = null,

	@field:SerializedName("balanceDate")
	val balanceDate: Any? = null,

	@field:SerializedName("createdBy")
	val createdBy: String? = null,

	@field:SerializedName("uploadedOn")
	val uploadedOn: Any? = null,

	@field:SerializedName("mtgNo")
	val mtgNo: Int? = null,

	@field:SerializedName("openingBalanceCash")
	val openingBalanceCash: Any? = null,

	@field:SerializedName("openingBalance")
	val openingBalance: Any? = null,

	@field:SerializedName("uploadedBy")
	val uploadedBy: Any? = null
)
