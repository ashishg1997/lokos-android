package com.microware.cdfi.clf.models.saving

import com.google.gson.annotations.SerializedName

data class SavingRequestModel(

	@field:SerializedName("SavingRequestModel")
	val savingRequestModel: List<SavingRequestModelItem?>? = null
)


data class ClfSavingReceiptDetails(

	@field:SerializedName("voluntarySavingsAmount")
	var voluntarySavingsAmount: Int? = null,

	@field:SerializedName("chequeNo")
	var chequeNo: String? = null,

	@field:SerializedName("chequeIssuedDate")
	var chequeIssuedDate: String? = null,

	@field:SerializedName("chequeReceivedDate")
	var chequeReceivedDate: String? = null,

	@field:SerializedName("payeeBankId")
	var payeeBankId: Int? = null,

	@field:SerializedName("payeeBankBranchId")
	var payeeBankBranchId: Long? = null,

	@field:SerializedName("compulsorySavingsAmount")
	var compulsorySavingsAmount: Int? = null,

	@field:SerializedName("voucherNumber")
	var voucherNumber: String? = null,

	@field:SerializedName("narration")
	var narration: String? = null,

	@field:SerializedName("bankAccountId")
	var bankAccountId: String? = null,

	@field:SerializedName("voucherDate")
	var voucherDate: String? = null,

	@field:SerializedName("modePayment")
	var modePayment: Int? = null,

	@field:SerializedName("txnDate")
	var txnDate: String? = null,

	@field:SerializedName("transactionNo")
	var transactionNo: String? = null,

	@field:SerializedName("type")
	var type: String? = null,

	@field:SerializedName("uid")
	var uid: Int? = 0
)

data class SavingRequestModelItem(
	@field:SerializedName("voId")
	val voId: Int? = null,

	@field:SerializedName("mtgUid")
	val mtgUid: Int? = null,

	@field:SerializedName("clfSavingReceiptDetails")
	val clfSavingReceiptDetails: ClfSavingReceiptDetails? = null
)
