package com.microware.miracle.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.microware.cdfi.entity.BlockEntity

@Dao
interface BlockDao {

    @Insert
    fun insertBlock(block: BlockEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllBlock(blockEntity: List<BlockEntity>?)

    @Update
    fun updateBlock(patient: BlockEntity)

    @Query("Delete from block_master")
    fun deleteAllBlocks()

    @Query("Select * from block_master where state_id == :stateCode and district_id ==:districtCode")
    fun getSubdistrict(stateCode :Int,districtCode:Int): LiveData<List<BlockEntity>>

    @Query("Select * from block_master where state_id == :stateCode and district_id ==:districtCode")
    fun getSubDistrictData(stateCode :Int,districtCode:Int): List<BlockEntity>

    @Query("Select * from block_master where district_id ==:districtCode")
    fun getSubDistrictByDistrict(districtCode:Int): LiveData<List<BlockEntity>>

    @Query("Select * from block_master where district_id ==:districtCode")
    fun getSubDistrictByDistrictCode(districtCode:Int): List<BlockEntity>

    @Query("Select * from block_master")
    fun getAllBlocks() :List<BlockEntity>

    @Query("Select block_id from block_master where block_name_en=:block_name_en")
    fun getBlockCode(block_name_en: String):Int


    @Query("Select count(*) from block_master where  district_id =:districtCode and block_code=:block_code and state_id=:stateCode")
        fun getBlock_count(stateCode:Int, districtCode:Int,block_code:Int):Int


    @Query("Select * from block_master where state_id == :stateCode and district_id ==:districtCode")
    fun getBlock_district_code(stateCode :Int,districtCode:Int): LiveData<List<BlockEntity>>

    @Query("Select * from block_master where state_id == :stateCode and district_id ==:districtCode")
    fun getBlock_data(stateCode :Int,districtCode:Int): List<BlockEntity>

}