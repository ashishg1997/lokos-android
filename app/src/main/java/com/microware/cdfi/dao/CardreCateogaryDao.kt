package com.microware.cdfi.dao

import androidx.room.*
import com.microware.cdfi.entity.CardreCateogaryEntity

@Dao
interface CardreCateogaryDao {

    @Insert
    fun insertCardreCateogary(data: CardreCateogaryEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllCateogary(data: List<CardreCateogaryEntity>?)

    @Update
    fun updateCateogary(data: CardreCateogaryEntity)

    @Query("Delete from cadre_category_master")
    fun deleteAllCateogary()

    @Query("Select * from cadre_category_master where language_id=:langId")
    fun getCateogary(langId:String?):List<CardreCateogaryEntity>?

    @Query("Select cadre_category from cadre_category_master where language_id=:langId and cadre_cat_code=:cadre_cat_code")
    fun getCateogaryName(langId:String?,cadre_cat_code:Int):String
}