package com.microware.cdfi.dao

import androidx.room.*
import com.microware.cdfi.entity.cadreRoleEntity

@Dao
interface CardreRoleDao {

    @Insert
    fun insertCardreRole(data: cadreRoleEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllCardreRole(data: List<cadreRoleEntity>?)

   /* @Update
    fun updateCardreRole(data: cadreRoleEntity)*/

    @Query("Delete from cadre_role_master")
    fun deleteAllCardreRole()

    @Query("Select * from cadre_role_master where language_id=:langId and cadre_cat_code=:cadre_cat_code")
    fun getCardreRole(langId:String?,cadre_cat_code:Int):List<cadreRoleEntity>

    @Query("Select cadre_role from cadre_role_master where language_id=:langId and cadre_role_code=:roleCode")
    fun getRoleName(langId:String?,roleCode:Int):String

    @Query("Select * from cadre_role_master where language_id=:langId")
    fun getAllCardreRole(langId:String?):List<cadreRoleEntity>
}