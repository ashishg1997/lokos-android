package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.AddressEntity
@Dao
interface CboAddressDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAddressDetail(addressEntity:  List<AddressEntity>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAddressDetailData(addressEntity : AddressEntity?)

    @Query("Select * from addresses_details Where address_guid=:address_guid")
    fun getAddressdetaildata(address_guid: String?): List<AddressEntity>?

     @Query("Select * from addresses_details Where cbo_guid=:cbo_guid and is_active=1")
    fun getAddressData(cbo_guid: String?): LiveData<List<AddressEntity>>?

    @Query("Update addresses_details set is_active=0,is_edited=1 Where address_guid=:address_guid")
    fun deleteData(address_guid: String?)

    @Query("DELETE From addresses_details where address_guid=:address_guid")
    fun deleteRecord(address_guid:String?)

  @Query("DELETE From addresses_details")
    fun deleteAll()

    @Query("Update addresses_details set address_line1=:address_line1,address_line2=:address_line2,village_id=:village,panchayat_id=:panchayat_id,city_town=:town, landmark=:landmark, postal_code=:postal_code,is_active =:is_Active,entry_source=:device,is_edited=1,updated_date=:updated_date,updated_by=:updated_by,is_complete=1 where address_guid=:address_guid")
    fun updateAddressDetail(
        address_guid: String,
        address_line1: String,
        address_line2: String,
        village: Int,
        panchayat_id: Int,
        town: Int,
        landmark: String,
        postal_code: Int,
        is_Active: Int,
        device: Int,
        updated_date: Long,
        updated_by: String
    )

    @Query("Update addresses_details set  is_edited=0,last_uploaded_date=:last_uploaded_date where cbo_guid=:shgguid")
    fun updateisedit( shgguid: String,last_uploaded_date:Long)

    @Query("Select * from addresses_details Where cbo_guid=:cbo_guid and is_edited=1")
    fun getAddressDatalist(cbo_guid: String?): List<AddressEntity>?

    @Query("Select * from addresses_details Where cbo_guid=:cbo_guid and is_active=1 and is_complete=1")
    fun getAddressDatalistcount(cbo_guid: String?): List<AddressEntity>?

    @Query("Select count(*) from addresses_details where is_complete=0 and cbo_guid=:cbo_guid and is_verified=1")
    fun getCompletionCount(cbo_guid: String?):Int

    @Query("Select count(*) from addresses_details where cbo_guid=:cbo_guid and is_verified=1")
    fun getVerificationCount(cbo_guid: String?):Int

    @Query("Update addresses_details set  is_verified=9,is_edited=1,updated_date=:updated_date,updated_by=:updated_by where cbo_guid=:shgguid and is_verified=1")
    fun updateIsVerifed( shgguid: String?,updated_date: Long,updated_by: String)
}