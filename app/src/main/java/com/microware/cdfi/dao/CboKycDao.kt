package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.entity.Cbo_kycEntity

@Dao
interface CboKycDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertKycDetail(cbokycEntity: List<Cbo_kycEntity>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertKycDetailData(cbokycEntity: Cbo_kycEntity?)

    @Query("Select * from cbo_kyc_details Where cbo_guid=:cbo_guid")
    fun getKycdetaildata(cbo_guid: String?): LiveData<List<Cbo_kycEntity>>?

    @Query("Select * from cbo_kyc_details Where kyc_guid=:kycGUID")
    fun getKycdetail(kycGUID: String?): List<Cbo_kycEntity>?

    @Query("Update cbo_kyc_details set kyc_valid_to=:kyc_valid_to,kyc_valid_from=:kyc_valid_from,is_edited =1,entry_source =1,kyc_type=:kyc_type,kyc_number=:kyc_id,kyc_front_doc_orig_name=:kyc_front_doc_orig_name,kyc_front_doc_encp_name=:kyc_front_doc_encp_name, kyc_rear_doc_name=:kyc_rear_doc_orig_name, kyc_rear_doc_encp_name=:kyc_rear_doc_encp_name,updated_date=:updated_date,updated_by=:updated_by,is_complete=:is_complete where kyc_guid=:kyc_guid")
    fun updateKycDetail(
        kyc_guid: String,
        kyc_type: Int,
        kyc_id: String,
        kyc_front_doc_orig_name: String,
        kyc_front_doc_encp_name: String,
        kyc_rear_doc_orig_name: String,
        kyc_rear_doc_encp_name: String,
        kyc_valid_from:Long,
        kyc_valid_to:Long,
        updated_date: Long,
        updated_by: String,
        is_complete:Int
    )

    @Query("Select * from cbo_kyc_details Where cbo_guid=:cbo_guid and is_edited=1")
    fun getkycdatalist(cbo_guid: String?): List<Cbo_kycEntity>?


    @Query("DELETE From cbo_kyc_details where kyc_guid =:kycGUID")
    fun deleteKycData(kycGUID: String?)

    @Query("update cbo_kyc_details set dedupl_status=1,is_edited =1 where is_edited =1 and cbo_guid=:guid")
    fun updatededup(guid: String)

    @Query("Delete from cbo_kyc_details Where kyc_guid=:guid")
    fun deleteData(guid: String?)

    @Query("update cbo_kyc_details set is_active=0,is_edited=1 Where kyc_guid=:guid")
    fun deleteRecord(guid: String?)

    @Query("Update cbo_kyc_details set  is_edited=0,last_uploaded_date=:last_uploaded_date where cbo_guid=:cboguid")
    fun updateisedit( cboguid: String,last_uploaded_date:Long)

    @Query("Select count(*) from cbo_kyc_details where kyc_number=:kyc_number and cbo_type=:cboType and is_active = 1")
    fun getKyc_count(kyc_number:String,cboType:Int):Int
}