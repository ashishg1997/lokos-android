package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.api.vomodel.VoChangePaymentBankDataModel
import com.microware.cdfi.entity.Cbo_bankEntity

@Dao
interface Cbo_BankDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertBankDetail(cboBankEntity:  List<Cbo_bankEntity>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertBankDetailData(cboBankEntity : Cbo_bankEntity?)

    @Query("Select * from cbo_bank_details Where bank_guid=:bank_guid")
    fun getBankdetaildata(bank_guid: String?): List<Cbo_bankEntity>?

    @Query("Update cbo_bank_details set is_active=0,is_edited=1 Where bank_guid=:bank_guid")
    fun deleteData(bank_guid: String?)

    @Query("Delete from cbo_bank_details where bank_guid=:bank_guid")
    fun deleteRecord(bank_guid: String?)

    @Query("DELETE From cbo_bank_details")
    fun deleteAll()

    @Query("Update cbo_bank_details set bankpassbook_name=:bankpassbook_name,bank_id=:bank_id,account_opening_date=:account_opening_date,account_Linkage_Date=:account_Linkage_Date,account_no=:account_no,bank_branch_id=:bank_branch,ifsc_code=:ifsc_code,is_default =:is_default,sequence_no=:sequence_no,account_type=:account_type,verification=:verification,  entry_source=:device,  is_edited=1,updated_date=:updated_date,updated_by=:updated_by,passbook_firstpage=:imageName,deduplication_status=1 ,is_complete=:is_complete where bank_guid=:bank_guid")
    fun updateBankDetail(
        bank_guid: String,
        bank_id: Int,
        account_opening_date: Long,
        account_Linkage_Date: Long,
        account_no: String,
        bank_branch: Int,
        ifsc_code: String,
        is_default: Int,
        sequence_no: Int,
        account_type: Int,
        verification: Int,
        device: Int,
        updated_date: Long,
        updated_by: String,
        imageName: String,
        is_complete: Int,
        bankpassbook_name: String
    )

    @Query("Update cbo_bank_details set  is_edited=0,last_uploaded_date=:last_uploaded_date where cbo_guid=:shgguid")
    fun updateBankDetailisedit( shgguid: String,last_uploaded_date:Long)


    @Query("Update cbo_bank_details set  is_default=0,is_edited = 1 where cbo_guid=:shgguid and is_active=1")
    fun updateisdefault( shgguid: String)

    @Query("Select * from cbo_bank_details Where cbo_guid=:cbo_guid and is_active=1")
    fun getBankdata(cbo_guid: String?): LiveData<List<Cbo_bankEntity>>?

    @Query("Select * from cbo_bank_details Where cbo_guid=:cbo_guid and is_edited=1")
    fun getBankdatalist(cbo_guid: String?): List<Cbo_bankEntity>?

    @Query("Select * from cbo_bank_details Where cbo_guid=:cbo_guid and is_complete=1 ")
    fun getBankdatalistcount(cbo_guid: String?): List<Cbo_bankEntity>?

    @Query("Select count(*) from cbo_bank_details Where cbo_guid=:cbo_guid  and is_default=1 and is_active=1")
    fun getBankdatalistdefualtcount(cbo_guid: String?): Int

    @Query("Select count(*) from cbo_bank_details where account_no=:account_no and cbo_type=:cboType")
    fun getbank_acc_count(account_no:String,cboType:Int):Int

    @Query("Select count(*) from cbo_bank_details where is_complete=0 and cbo_guid=:cbo_guid and is_verified=1")
    fun getCompletionCount(cbo_guid: String?):Int

    @Query("Select count(*) from cbo_bank_details where cbo_guid=:cbo_guid and is_verified=1")
    fun getVerificationCount(cbo_guid: String?):Int

    @Query("Update cbo_bank_details set  is_verified=9,is_edited=1,updated_date=:updated_date,updated_by=:updated_by where cbo_guid=:shgguid and is_verified=1")
    fun updateIsVerifed(shgguid: String?,updated_date: Long,updated_by: String)

    @Query("Update cbo_bank_details set  activation_status=:status where bank_guid=:bank_guid ")
    fun updateactivotaionstatus(bank_guid: String?,status: Int)

    @Query("Select * from cbo_bank_details Where cbo_guid=:cbo_guid and cbo_type=:cboType and is_active=1 order by cbo_bank_id ")
    fun getcboBankdata(cbo_guid: String?,cboType:Int): List<Cbo_bankEntity>?

    @Query("Select cbo_bank_id,account_no,bank_guid,bank_code,bank_id,bank_branch,ifsc_code,is_default,2 as modePayment from cbo_bank_details Where cbo_guid=:cbo_guid and cbo_type=:cboType and is_active=1 union select '752','','','','65','','',0,'1' from cbo_bank_details order by cbo_bank_id ")
    fun getcboBankdataModel(cbo_guid: String?, cboType: Int): List<VoChangePaymentBankDataModel>?

    @Query("Select * from cbo_bank_details Where cbo_guid=:fedrationGUID and is_active=1")
    fun getBankdata1(fedrationGUID: String?): List<Cbo_bankEntity>?



}