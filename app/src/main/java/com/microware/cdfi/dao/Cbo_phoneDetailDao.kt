package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.Cbo_phoneEntity

@Dao
interface Cbo_phoneDetailDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPhoneDetail(phoneEntity: List<Cbo_phoneEntity>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPhoneDetailData(phoneEntity : Cbo_phoneEntity?)

    @Query("Select * from cbo_phone_details Where phone_guid=:PhoneGUID")
    fun getphonedetaildata(PhoneGUID: String?): List<Cbo_phoneEntity>?

    @Query("Select * from cbo_phone_details Where cbo_guid=:cbo_guid and is_active=1")
    fun getphonedata(cbo_guid: String?): LiveData<List<Cbo_phoneEntity>>?

    @Query("Update cbo_phone_details set is_active=0,is_edited=1 Where phone_guid=:PhoneGUID")
    fun deleteData(PhoneGUID: String?)

    @Query("Delete from cbo_phone_details where phone_guid=:PhoneGUID")
    fun deleteRecord(PhoneGUID: String?)

    @Query("DELETE From cbo_phone_details")
    fun deleteAll()

    @Query("Update cbo_phone_details set member_guid=:member_guid,mobile_no=:phone_no,is_default=:isDefault,phone_ownership=:phone_ownership,phone_ownership_details=:phone_ownership_detail, valid_from=:valid_from, valid_till=:valid_till,entry_source=:device,   is_edited=1,updated_date=:updated_date,updated_by=:updated_by ,deduplication_status=1,is_complete=:is_complete where phone_guid=:phone_guid")
    fun updatePhoneDetail(
        phone_guid: String,
        member_guid:String,
        phone_no: String,
        isDefault: Int,
        phone_ownership: String,
        phone_ownership_detail: String,
        valid_from: Long,
        valid_till: Long,
        device: Int,
        updated_date: Long,
        updated_by: String,
        is_complete: Int
    )

    @Query("Update cbo_phone_details set  is_edited=0,last_uploaded_date=:last_uploaded_date where cbo_guid=:shgguid")
    fun updateisedit( shgguid: String,last_uploaded_date:Long)

    @Query("Select * from cbo_phone_details Where cbo_guid=:cbo_guid and is_edited=1")
    fun getuploadlist(cbo_guid: String?): List<Cbo_phoneEntity>?

    @Query("Select * from cbo_phone_details Where cbo_guid=:cbo_guid and is_active=1 and is_complete=1")
    fun getuploadlistcount(cbo_guid: String?): List<Cbo_phoneEntity>?

    @Query("Select count(*) from cbo_phone_details where is_complete=0 and cbo_guid=:cbo_guid and is_verified=1")
    fun getCompletionCount(cbo_guid: String?):Int

    @Query("Select count(*) from cbo_phone_details where cbo_guid=:cbo_guid and is_verified=1")
    fun getVerificationCount(cbo_guid: String?):Int

    @Query("Update cbo_phone_details set  is_verified=9,is_edited=1,updated_date=:updated_date,updated_by=:updated_by where cbo_guid=:shgguid and is_verified=1")
    fun updateIsVerifed( shgguid: String?,updated_date: Long,updated_by: String)

    @Query("Select count(*) from cbo_phone_details where mobile_no=:mobile_no and cbo_type=:cboType and is_active = 1")
    fun getphone_count(mobile_no:String,cboType:Int):Int
}