package com.microware.cdfi.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.ClfVoMemberEntity
import com.microware.cdfi.entity.MappedShgEntity
import com.microware.cdfi.entity.VoShgMemberEntity
import com.microware.cdfi.entity.member_addressEntity

@Dao
interface ClfVoMemberDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMapped_Memberdata(voShgMemberEntity:  List<ClfVoMemberEntity>)

    @Query("Select * from tbl_clf_vo_member where vo_guid=:vo_guid")
    fun getMemberList(vo_guid:String):List<ClfVoMemberEntity>?

    @Query("update tbl_clf_vo_member set vo_guid=:vo_guid,clf_guid=:clf_guid where member_guid=:memberguid")
    fun updateVO_shgMember(vo_guid:String,clf_guid:String,memberguid:String)
}