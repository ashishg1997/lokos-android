package com.microware.miracle.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.microware.cdfi.entity.DistrictEntity


@Dao
interface DistrictDao {

  /*  @Insert
    fun insertDistrict(districtEntity: List<DistrictEntity>?)*/

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDistrict(districtEntity: List<DistrictEntity>?)

    @Update
    fun updateDistrict(district: DistrictEntity)

    @Query("Delete from district_master")
    fun deleteAllDistrict()

    @Query("Select * from district_master where state_id==:stateCode")
    fun getDistrictData(stateCode :Int): List<DistrictEntity>

    @Query("Select * from district_master where state_id==:stateCode")
    fun getDistrictByStateCode(stateCode:Int) :LiveData<List<DistrictEntity>>

    @Query("Select * from district_master")
    fun getAllDistrict() :LiveData<List<DistrictEntity>>

    @Query("Select * from district_master")
    fun getAllDistrictdata() :List<DistrictEntity>

    @Query("Select district_id from district_master where district_name_en=:districtName")
    fun getDistrictCode(districtName: String):DistrictEntity


}