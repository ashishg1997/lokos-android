package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.api.meetingmodel.mcpDataListModel
import com.microware.cdfi.api.meetinguploadmodel.ShgLoanApplicationListData
import com.microware.cdfi.entity.DtLoanApplicationEntity
import com.microware.cdfi.entity.ShgMcpEntity

@Dao
interface DtLoanApplicationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLoanApplicationAllData(dtLoanApplicationEntity: List<DtLoanApplicationEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLoanApplicationData(dtLoanApplicationEntity: DtLoanApplicationEntity)

    @Query("Select * from shg_loan_application Where loan_application_id=:loanappid")
    fun getLoanApplicationdata(loanappid: Int): List<DtLoanApplicationEntity>?

    @Query("Select * from shg_loan_application Where loan_application_id=:loanapplication_id")
    fun getmemberLoanApplication(loanapplication_id: Long): List<DtLoanApplicationEntity>?

    @Query("Select * from shg_loan_application Where loan_requested_mtg_no=:mtgno and cbo_id=:cboid")
    fun getLoanApplicationlist(mtgno: Int,cboid:Long): List<DtLoanApplicationEntity>?

    @Query("Select sum(amt_demand) from shg_loan_application Where loan_requested_mtg_no=:mtgno and cbo_id=:cboid")
    fun gettotaldemand(mtgno: Int,cboid:Long): Int

    @Query("Select count(loan_application_id) from shg_loan_application Where  cbo_id=:cboid")
    fun getmaxloanno(cboid:Long): Int

    @Query("Delete from shg_loan_application where loan_application_id=:loanappid")
    fun deleteRecord(loanappid: Int)

    @Query("DELETE From shg_loan_application")
    fun deleteAll()


    @Query(
        "Update shg_loan_application set loan_sanctioned_mtg_guid=:loansanctioned_mtg_guid, loan_sanctioned_mtg_no=:mtgno, amt_disbursed=:amt_disbursed where mem_id=:mem_id and loan_application_id=:loanapplication_id "
    )
    fun updateMemberloanapplication(
        loansanctioned_mtg_guid: String,
        mem_id: Long,
        loanapplication_id: Long,
        amt_disbursed: Int,
        mtgno: Int    )




    @Query(
        "Update shg_loan_application set updated_by=:updatedBy,updated_on=:updatedOn,mcp_id=:mcp_link_id,loan_product_id=:loan_product_id,amt_demand=:amt_disbursed, amt_sanction=:amtsaction, loan_purpose=:loanpurpose, loan_source=:loansource, loan_request_priority=:priorty, tentative_date=:tentivedate where  loan_application_id=:loanapplication_id "
    )
    fun updateMemberloanapplicationdetail(
        loanapplication_id: Long,
        amt_disbursed: Int,
        amtsaction: Int,
        loanpurpose: Int,
        loansource: Int,
        priorty: Int,
        tentivedate: Long,
        loan_product_id: Int?,
        mcp_link_id:Long?,
        updatedBy:String?,
        updatedOn:Long?
    )
    /* @Query(
         "Update loanApplication set cbo_id=:cbo_id,mem_id=:mem_id, request_date=:request_date, loanfee=:loanfee, amt_demand=:amt_demand, amt_sanction=:amt_sanction, amt_disbursed=:amt_disbursed, approval_date=:approval_date, tentative_date=:tentative_date, loansource=:loansource, customerstatus=:customerstatus, loanstatus=:loanstatus, loancloserdate=:loancloserdate, totalmfi=:totalmfi, toatalactivemfi=:toatalactivemfi, disbursedamtmfi=:disbursedamtmfi, loanmfioutstandingself=:loanmfioutstandingself, loanmfioutstandingcreditenquiry=:loanmfioutstandingcreditenquiry, creditenquiryremarks=:creditenquiryremarks, povertystatus=:povertystatus, kycstatus=:kycstatus, cgtmarks=:cgtmarks, grtdate=:grtdate, grtplace=:grtplace, cutomerstatusid=:cutomerstatusid, registrationstatusid=:registrationstatusid, registrationdate=:registrationdate, loanappliedid=:loanappliedid, loansanctionedid=:loansanctionedid, loandisbursedid=:loandisbursedid, loandisburseddate=:loandisburseddate, loandisbremarks=:loandisbremarks, loanemidate=:loanemidate, loanapprovedid=:loanapprovedid, loanapprovaldate=:loanapprovaldate, loanchequebankid=:loanchequebankid, loanchequeno=:loanchequeno, loanapprovedamt=:loanapprovedamt, loanapprovedinterestrate=:loanapprovedinterestrate, loanapprovedinsuranceamt=:loanapprovedinsuranceamt, loanapprovedfeesamt=:loanapprovedfeesamt, loanapprovedinstallmentamt=:loanapprovedinstallmentamt, loanapprovedpaymentweeks=:loanapprovedpaymentweeks, loanapprovedcloserminweeks=:loanapprovedcloserminweeks, loanapprovedremarks=:loanapprovedremarks, isloanclosed=:isloanclosed, tabletid=:tabletid, fundsourceid=:fundsourceid, isdeleted=:isdeleted, actionby=:actionby, loandisbursaldoneby=:loandisbursaldoneby, grtdoneby=:grtdoneby, creditenquirydoneby=:creditenquirydoneby, cgtdoneby=:cgtdoneby, existingloanamount=:existingloanamount, existingloancurrentweek=:existingloancurrentweek, existingloancurrentoutstanding=:existingloancurrentoutstanding, grtlat=:grtlat, grtlong=:grtlong, regplace=:regplace, reglat=:reglat, reglong=:reglong, isoldcustomer=:isoldcustomer, customernickname=:customernickname, ismobilenovalidated=:ismobilenovalidated, gurantorage=:gurantorage, iswriteoff=:iswriteoff, loancycle=:loancycle, creditequirypdf=:creditequirypdf, customerbankifsccode=:customerbankifsccode, mandateprint=:mandateprint, mandatestartdate=:mandatestartdate, mandateenddate=:mandateenddate, mandateapproved=:mandateapproved, mandateremark=:mandateremark where loanappid=:loanappid"
     )
     fun updateLoanApplication(
         loanappid: Int,
         cbo_id: Int,
         mem_id: Int,
         request_date: String,
         loanfee: Int,
         amt_demand: Int,
         amt_sanction: Int,
         amt_disbursed: Int,
         approval_date: String,
         tentative_date: String,
         loansource: Int,
         customerstatus: String,
         loanstatus: String,
         loancloserdate: String,
         totalmfi: Int,
         toatalactivemfi: Int,
         disbursedamtmfi: Int,
         loanmfioutstandingself: Int,
         loanmfioutstandingcreditenquiry: Int,
         creditenquiryremarks: String,
         povertystatus: String,
         kycstatus: String,
         cgtmarks: Int,
         grtdate: String,
         grtplace: String,
         cutomerstatusid: Int,
         registrationstatusid: Int,
         registrationdate: String,
         loanappliedid: Int,
         loansanctionedid: Int,
         loandisbursedid: Int,
         loandisburseddate: String,
         loandisbremarks: String,
         loanemidate: String,
         loanapprovedid: Int,
         loanapprovaldate: String,
         loanchequebankid: Int,
         loanchequeno: String,
         loanapprovedamt: Int,
         loanapprovedinterestrate: Double,
         loanapprovedinsuranceamt: Int,
         loanapprovedfeesamt: Int,
         loanapprovedinstallmentamt: Int,
         loanapprovedpaymentweeks: Int,
         loanapprovedcloserminweeks: Int,
         loanapprovedremarks: String,
         isloanclosed: String,
         tabletid: String,
         fundsourceid: Int,
         isdeleted: String,
         actionby: String,
         loandisbursaldoneby: String,
         grtdoneby: String,
         creditenquirydoneby: String,
         cgtdoneby: String,
         existingloanamount: Int,
         existingloancurrentweek: Int,
         existingloancurrentoutstanding: Int,
         grtlat: String,
         grtlong: String,
         regplace: String,
         reglat: String,
         reglong: String,
         isoldcustomer: String,
         customernickname: String,
         ismobilenovalidated: String,
         gurantorage: Int,
         iswriteoff: String,
         loancycle: Int,
         creditequirypdf: String,
         customerbankifsccode: String,
         mandateprint: Int,
         mandatestartdate: String,
         mandateenddate: String,
         mandateapproved: Int,
         mandateremark: String
     )
 */

    @Query("select * from shg_loan_application where loan_requested_mtg_no=:mtg_no and cbo_id=:cbo_id")
    fun getUploadListData(mtg_no: Int,cbo_id: Long) : List<DtLoanApplicationEntity>

    @Query("Delete from shg_loan_application where loan_requested_mtg_no=:mtg_no and cbo_id=:cbo_id")
    fun deleteShgLoanApplication(mtg_no: Int,cbo_id: Long)

    @Query("Delete from shg_loan_application where cbo_id=:cbo_id")
    fun deleteShgLoanApplicationByCboId(cbo_id: Long)

    @Query("Select mcp_id as mcpId,l.key_value as purpose,amt_demand as amount,request_date as request_date from shg_mcp left join (Select * from LookupMaster where key1=67) l on shg_mcp.loan_purpose=l.lookup_code Where mcp_id not in (Select mcp_id from shg_loan_application where cbo_id=:cbo_id and mem_id=:mem_id and mcp_id!=:mcp_id) and cbo_id=:cbo_id and mem_id=:mem_id group by mcp_id")
    fun getMCP_Linkdata(cbo_id:Long,mem_id:Long,mcp_id:Long?): List<mcpDataListModel>?

    @Query("Select count(*) from shg_loan_application where loan_request_priority=:priority and loan_requested_mtg_no=:mtg_no and cbo_id=:cbo_id")
    fun getPriorityCount(priority: Int,mtg_no: Int,cbo_id: Long):Int
}