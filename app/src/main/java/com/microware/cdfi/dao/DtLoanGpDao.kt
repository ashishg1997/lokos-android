package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.api.meetinguploadmodel.ShgGroupLoanData
import com.microware.cdfi.entity.DtLoanGpEntity

@Dao
interface DtLoanGpDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLoanGpAllData(dtLoanGpEntity: List<DtLoanGpEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLoanGpData(dtLoanGpEntity: DtLoanGpEntity)

    @Query("Select * from shg_group_loan Where cbo_id=:cbo_id")
    fun getLoanGpdata(cbo_id: Long): List<DtLoanGpEntity>?

    @Query("Select * from shg_group_loan Where cbo_id=:cbo_id and loan_no=:loanno")
    fun getLoandata(cbo_id: Long,loanno: Int): List<DtLoanGpEntity>?

    @Query("Select interest_rate from shg_group_loan Where loan_no=:loanno")
    fun getinterestrate(loanno: Int): Double

    @Query("Select amount from shg_group_loan Where loan_no=:loanno")
    fun gettotamt(loanno: Int): Int

    @Query("Delete from shg_group_loan where cbo_id=:cbo_id")
    fun deleteRecord(cbo_id: Int)

    @Query("Select max(loan_no) from shg_group_loan Where cbo_id=:cbo_id")
    fun getmaxLoanno(cbo_id: Long): Int

    @Query("DELETE From shg_group_loan")
    fun deleteAll()

    @Query(
        "Update shg_group_loan set mtg_no=:mtgno, mtg_date=:mtgdate, loan_no=:loanno, loan_type=:loan_type, loan_product_id=:loan_product, loan_account_no=:loan_accountno,period=:loan_period, installment_date=:installmentdate,amount=:amount,interest_rate=:interest_rate,interest_due=:interest_due, completion_flag=:completion_flag, mode_payment=:mode_payment, bank_code=:bank_code,loan_purpose=:loanpurpose,created_by=:createdby, created_on=:createdon, updated_by=:updatedby, updated_on=:updatedon, uploaded_on=:uploadedon where cbo_id=:cbo_id"
    )
    fun updateLoanGp(
        cbo_id: Int,
        mtgno: Int,
        mtgdate: String,
        loanno: Int,
        loan_type: String,
        loan_product: Int,
        loan_accountno: String,
        loan_period: Int,
        installmentdate: String,
        amount: Int,
        interest_rate: Double,
        interest_due: Int,
        completion_flag: String,
        mode_payment: String,
        bank_code: String,
        loanpurpose: Int,
        createdby: String,
        createdon: String,
        updatedby: String,
        updatedon: String,
        uploadedon: String

    )

    @Query("select * from shg_group_loan where mtg_no=:mtgno and cbo_id=:cbo_aid")
    fun getListDataByMtgnum(mtgno:Int,cbo_aid:Long): List<DtLoanGpEntity>

    @Query("Select sum(amount) from shg_group_loan Where mode_payment=1 and mtg_no=:mtgno and cbo_id=:cbo_id")
    fun gettotloangroup(mtgno:Int,cbo_id:Long): Int

    @Query("Select sum(amount) from shg_group_loan Where (mode_payment=2 or mode_payment=3 ) and mtg_no=:mtgno and cbo_id=:cbo_id and bank_code=:code")
    fun getgrouptotloanamtinbank(mtgno:Int,cbo_id:Long, code: String): Int

    @Query("Select max(loan_no) from shg_group_loan Where cbo_id=:cbo_id and mtg_no=:mtgNo")
    fun getCutOffmaxLoanno(cbo_id: Long,mtgNo:Int): Int

    @Query("Select sum(amount) from shg_group_loan Where mtg_no=:mtgno and cbo_id=:cbo_id")
    fun gettotloangroupamount(mtgno:Int,cbo_id:Long): Int

    @Query("select sum(orignal_loan_amount)-sum(principle_repaid) from shg_group_loan where mtg_no=:mtg_no and cbo_id=:cbo_id and loan_source=:loan_source")
    fun getTotalBankLoanOutstanding(mtg_no:Int,cbo_id:Long,loan_source:Int):Int

    @Query("select sum(orignal_loan_amount)-sum(principle_repaid) from shg_group_loan where mtg_no=:mtg_no and cbo_id=:cbo_id and loan_product_id=:fund_type")
    fun getTotalLoanOutstandingByFundType(mtg_no:Int,cbo_id:Long,fund_type:Int):Int

    @Query("select sum(interest_overdue) from shg_group_loan where mtg_no=:mtg_no and cbo_id=:cbo_id")
    fun getTotalInterestOverdue(mtg_no:Int,cbo_id:Long):Int

    @Query("Select sum(orignal_loan_amount)-sum(principle_repaid) from shg_group_loan where mtg_no=:mtg_no and cbo_id=:cbo_id and loan_product_id not in(2,9) and loan_source in(3,4,6)")
    fun getOtherLoanOutstanding(mtg_no:Int,cbo_id:Long):Int

    @Query("select * from shg_group_loan where mtg_no=:mtg_no and cbo_id=:cbo_id")
    fun getUploadListData(mtg_no:Int,cbo_id: Long) : List<DtLoanGpEntity>

    @Query("Delete from shg_group_loan where mtg_no=:mtg_no and cbo_id=:cbo_id")
    fun deleteGroupLoanData(mtg_no:Int,cbo_id: Long)

    @Query("Delete from shg_group_loan where cbo_id=:cbo_id")
    fun deleteGroupLoanDataByCboId(cbo_id: Long)

    @Query("Select sum(orignal_loan_amount) from shg_group_loan Where mode_payment=1 and mtg_no=:mtgno and cbo_id=:cbo_id")
    fun getTotalGrouploan(mtgno:Int,cbo_id:Long): Int

    @Query("Select count(*) from shg_group_loan Where mtg_no=:mtgno and cbo_id=:cbo_id")
    fun getTotalLoans(mtgno:Int,cbo_id:Long): Int

    @Query("Select sum(orignal_loan_amount) from shg_group_loan where mtg_no=:mtg_no and cbo_id=:cbo_id")
    fun getTotalCutOffLoanReceived(mtg_no:Int,cbo_id:Long):Int

    @Query("Update shg_group_loan set is_edited=1, completion_flag=:completionflag where cbo_id=:cbo_id and loan_no=:loan_no")
    fun updateShgGroupLoan(cbo_id:Long,loan_no: Int,completionflag: Boolean)

    @Query("select * from shg_group_loan where is_edited=1 and cbo_id=:cbo_id")
    fun getUploadGroupLoanList(cbo_id: Long) : List<DtLoanGpEntity>



}