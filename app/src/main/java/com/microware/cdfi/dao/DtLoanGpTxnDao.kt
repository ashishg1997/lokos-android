package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.api.meetinguploadmodel.ShgGroupLoanTransactionData
import com.microware.cdfi.entity.DtLoanGpTxnEntity

@Dao
interface DtLoanGpTxnDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLoanGpTxnAllData(dtLoanGpTxnEntity: List<DtLoanGpTxnEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLoanGpTxnData(dtLoanGpTxnEntity: DtLoanGpTxnEntity)

    @Query("DELETE From shg_group_loan_txn")
    fun deleteAll()

    @Query("Update shg_group_loan_txn set loan_no=:LoanNo, cbo_id=:cbo_id, mtg_no=:MtgNo, mtg_date=:MtgDate, loan_no=:loan_no, loan_op=:loan_op, loan_op_int=:loan_op_int, loan_paid=:loan_paid, loan_paid_int=:loan_paid_int, loan_cl=:loan_cl, loan_cl_int=:loan_cl_int, completion_flag=:completion_flag, int_accrued_op=:intaccrued_op, int_accrued=:intaccrued, int_accrued_cl=:intaccrued_cl, principal_demand_ob=:principal_demand_ob, principal_demand=:principal_demand, principal_demand_cb=:principal_demand_cb, mode_payment=:mode_payment, bank_code=:bank_code, transaction_no=:transaction_no,created_by=:createdby, created_on=:createdon, updated_by=:updatedby, updated_on=:updatedon, uploaded_by=:uploaded_by where UID=:UID")
    fun updateLoanGpTxn(
        UID: Int,
        LoanNo: Int,
        cbo_id: Int,
        MtgNo: Int,
        MtgDate: Long,
        loan_no: Int,
        loan_op: Int,
        loan_op_int: Int,
        loan_paid: Int,
        loan_paid_int: Int,
        loan_cl: Int,
        loan_cl_int: Int,
        completion_flag: Int,
        intaccrued_op: Int,
        intaccrued: Int,
        intaccrued_cl: Int,
        principal_demand_ob: Int,
        principal_demand: Int,
        principal_demand_cb: Int,
        mode_payment: Int,
        bank_code: String,
        transaction_no: String,
        createdby: String,
        createdon: Long,
        updatedby: String,
        updatedon: Long,
        uploaded_by: String
    )


    @Query("select * from shg_group_loan_txn where mtg_no=:mtgno and cbo_id=:cbo_aid")
    fun getListDataByMtgnum(mtgno:Int,cbo_aid:Long): List<DtLoanGpTxnEntity>

    @Query("Select * from shg_group_loan_txn where loan_no=:loanno and mtg_no=:mtgno and cbo_id=:shgid ")
    fun getloandata(loanno: Int, mtgno: Int,shgid:Long): List<DtLoanGpTxnEntity>?

    @Query("Select (sum(principal_demand_ob)+sum(principal_demand)+sum(int_accrued)+sum(int_accrued_op))-(sum(loan_paid_int)+sum(loan_paid)) as loan_paid from shg_group_loan_txn Where cbo_id=:cboid and mtg_no=:mtgno and loan_no=:loanno")
    fun gettotalpaid(cboid: Long, mtgno: Int,loanno:Int): Int

    @Query("Select (sum(loan_paid)+sum(loan_paid_int))from shg_group_loan_txn Where cbo_id=:cboid and mtg_no=:mtgno")
    fun getLoanRepaymentAmount(cboid: Long, mtgno: Int):Int

    @Query("Select (sum(loan_paid)+sum(loan_paid_int))from shg_group_loan_txn Where cbo_id=:cboid and mtg_no=:mtgno and mode_payment=:paymentMode")
    fun getLoanRepaymentAmountByMode(cboid: Long, mtgno: Int,paymentMode:Int):Int

    @Query("Select (sum(loan_paid)+sum(loan_paid_int))from shg_group_loan_txn Where cbo_id=:cboid and mtg_no=:mtgno and (mode_payment=2 OR mode_payment=3)")
    fun getLoanRepaymentAmountByMode1(cboid: Long, mtgno: Int):Int



    @Query("Select sum(loan_paid_int)+sum(loan_paid) from shg_group_loan_txn Where mode_payment=1 and mtg_no=:mtgno and cbo_id=:cbo_id")
    fun gettotloanpaidgroup(mtgno:Int,cbo_id:Long): Int

    @Query("Select sum(loan_paid_int)+sum(loan_paid) from shg_group_loan_txn Where (mode_payment=2 or mode_payment=3) and mtg_no=:mtgno and cbo_id=:cbo_id and bank_code=:account")
    fun getgrptotloanpaidinbank(mtgno:Int,cbo_id:Long,account:String): Int


    @Query("Select (sum(loan_paid)+sum(loan_paid_int))from shg_group_loan_txn Where cbo_id=:cboid and mtg_no=:mtgno and bank_code=:bank_code")
    fun getShgGrpLoanTxnAnount(cboid: Long, mtgno: Int,bank_code:String):Int

    @Query("select * from shg_group_loan_txn where mtg_no=:mtg_no and cbo_id=:cbo_id")
    fun getUploadListData(mtg_no:Int,cbo_id: Long) : List<DtLoanGpTxnEntity>

    @Query("Delete from shg_group_loan_txn where mtg_no=:mtg_no and cbo_id=:cbo_id")
    fun deleteGroupLoanTxnData(mtg_no:Int,cbo_id: Long)

    @Query("Delete from shg_group_loan_txn where cbo_id=:cbo_id")
    fun deleteGroupLoanTxnDataByCboId(cbo_id: Long)

    @Query("Update shg_group_loan_txn set completion_flag=:completionflag,loan_paid=:paid,loan_paid_int=:loanint,int_accrued_cl=(int_accrued_op+int_accrued)-:loanint,loan_cl_int=loan_op_int+:loanint,loan_cl=loan_op-:paid,mode_payment=:mode,bank_code=:bankaccount,transaction_no=:transactionno ,principal_demand_cb=:principaldemandcl where cbo_id=:cboid and loan_no=:loanno and mtg_no=:mtgno ")
    fun updateloanpaid(
        loanno: Int, mtgno: Int, cboid: Long, paid: Int, loanint: Int,
        mode: Int,
        bankaccount: String,
        transactionno: String,
        principaldemandcl: Int,
        completionflag:Boolean
    )

    @Query("Select sum(principal_demand_ob)+sum(principal_demand)+sum(int_accrued)+sum(int_accrued_op) from shg_group_loan_txn Where cbo_id=:cboid and mtg_no=:mtgno")
    fun getTotalGrpRepaymentDemand(cboid: Long, mtgno: Int): Int

    @Query("Select count(*) from shg_group_loan_txn Where cbo_id=:cboid and mtg_no=:mtgno and (loan_paid>0 or loan_paid_int>0)")
    fun getTotalGrpLoansRepaid(cboid: Long, mtgno: Int):Int

    @Query("Select count(*) from shg_group_loan_txn Where cbo_id=:cboid and mtg_no=:mtgno")
    fun getTotalGrpLoans(cboid: Long, mtgno: Int):Int


}