package com.microware.cdfi.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.DtLoanMemberEntity

@Dao
interface DtLoanMemberDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLoanMemberAllData(dtLoanMemberEntity: List<DtLoanMemberEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLoanMemberData(dtLoanMemberEntity: DtLoanMemberEntity)

    @Query("Select * from shg_mem_loan Where uid=:uid")
    fun getLoanMemberdata(uid: Int): List<DtLoanMemberEntity>?

    @Query("Select loan_no from shg_mem_loan Where loan_application_id=:loanappid")
    fun getLoanno(loanappid: Long): Int


    @Query("Select * from shg_mem_loan Where loan_application_id=:loanappid")
    fun getLoancount(loanappid: Long): List<DtLoanMemberEntity>?

    @Query("Select max(loan_no) from shg_mem_loan Where cbo_id=:cbo_id")
    fun getmaxLoanno(cbo_id: Long): Int

    @Query("Select interest_rate from shg_mem_loan Where loan_no=:loanno")
    fun getinterestrate(loanno: Int): Double

    @Query("Select amount from shg_mem_loan Where loan_no=:loanno and mem_id=:member_id")
    fun gettotamt(loanno: Int, member_id: Long): Double

    @Query("Select sum(amount) from shg_mem_loan Where mode_payment=1 and mtg_no=:mtgno and cbo_id=:cbo_id")
    fun gettotloan(mtgno: Int, cbo_id: Long): Int

    @Query("Select sum(original_loan_amount) from shg_mem_loan Where mode_payment=1 and mtg_no=:mtgno and cbo_id=:cbo_id")
    fun gettotalMemberLoan(mtgno: Int, cbo_id: Long): Int

/*

  @Query("Select sum() from dtmemloan Where mem_id=:memid")
    fun gettotalLoanamt(memid: Long): Int
*/

    @Query("Delete from shg_mem_loan where uid=:uid")
    fun deleteRecord(uid: Int)

    @Query("DELETE From shg_mem_loan")
    fun deleteAll()

    /* @Query(
         "Update dtmemloan set cbo_id=:cbo_id, member_AID=:member_AID, mtg_GUID=:mtg_GUID, mem_id=:mem_id, loanno=:loanno, mtgno=:mtgno, mtgdate=:mtgdate, installmentdate=:installmentdate, installments=:installments, amount=:amount, loan_purpose=:loan_purpose, loan_prod_code=:loan_prod_code, interest_rate=:interest_rate, period=:period, interest_due=:interest_due, completionflag=:completionflag, loantype=:loantype, loansource=:loansource, externalloanid=:externalloanid, modepayment=:modepayment, bankcode=:bankcode, installmentfreq=:installmentfreq, moratoriumperiod=:moratoriumperiod, loanaccountno=:loanaccountno, createdby=:createdby, createdon=:createdon, updatedby=:updatedby, updatedon=:updatedon, uploadedon=:uploadedon, corpus_external_id=:corpus_external_id, flag=:flag where uid=:uid"
     )
     fun updateLoanMember(
         uid: Int,
         cbo_id: Int,
         member_AID: Long,
         mtg_GUID: String,
         mem_id: Long,
         loanno: Long,
         mtgno: Int,
         mtgdate: String,
         installmentdate: String,
         installments: Int,
         amount: Double,
         loan_purpose: Int,
         loan_prod_code: Int,
         interest_rate: Double,
         period: Int,
         interest_due: Double,
         completionflag: String,
         loantype: Int,
         loansource: Int,
         externalloanid: Int,
         modepayment: Int,
         bankcode: String,
         installmentfreq: Int,
         moratoriumperiod: Int,
         loanaccountno: String,
         createdby: String,
         createdon: String,
         updatedby: String,
         updatedon: String,
         uploadedon: String,
         corpus_external_id: Int,
         flag: String

     )*/
    @Query("Select * from shg_mem_loan Where loan_no=:loan_no and cbo_id=:cbo_id and mem_id=:mem_id and mtg_guid=:mtg_guid")
    fun getLoanDetailData(
        loan_no: Int,
        cbo_id: Long,
        mem_id: Long,
        mtg_guid: String
    ): List<DtLoanMemberEntity>?

    @Query("select  * from shg_mem_loan where  mtg_no=:mtgno and  cbo_id=:cbo_id and mem_id=:mem_id")
    fun getloanListSummeryData(mtgno: Int, cbo_id: Long, mem_id: Long): List<DtLoanMemberEntity>

    @Query("select sum(amount) from shg_mem_loan where  mtg_no=:mtg_no and  cbo_id=:cbo_id and mode_payment=:modePayment")
    fun getLoanDisbursedAmount(cbo_id: Long, mtg_no: Int, modePayment: Int): Int

    @Query("select sum(amount) from shg_mem_loan where  mtg_no=:mtg_no and  cbo_id=:cbo_id")
    fun getMemberLoanDisbursedAmount(cbo_id: Long, mtg_no: Int): Int

    @Query("select sum(amount) from shg_mem_loan where  mtg_no=:mtg_no and  cbo_id=:cbo_id and (mode_payment= 2 or mode_payment = 3)")
    fun getMemberLoanDisbursedBankAmount(cbo_id: Long, mtg_no: Int): Int

    @Query("select sum(amount) from shg_mem_loan where  mtg_no=:mtg_no and  cbo_id=:cbo_id and bank_code=:bank_code")
    fun getMemLoanDisbursedAmount(cbo_id: Long, mtg_no: Int, bank_code: String): Int

    @Query("Select sum(original_loan_amount) from shg_mem_loan where mtg_no=:mtgNum and mem_id=:mem_id and completion_flag=:completionFlag")
    fun getTotalClosedLoanAmount(mtgNum: Int, mem_id: Long, completionFlag: Boolean): Int

    @Query("Select count(*) from shg_mem_loan where mtg_no=:mtgNum and mem_id=:mem_id and completion_flag=:completionFlag")
    fun getTotalClosedLoanCount(mtgNum: Int, mem_id: Long, completionFlag: Boolean): Int

    @Query("select sum(original_loan_amount) from shg_mem_loan where  mtg_no=:mtg_no and  cbo_id=:cbo_id and completion_flag=:completionFlag")
    fun getCutOffMemberLoanAmount(cbo_id: Long, mtg_no: Int, completionFlag: Boolean): Int

    @Query("select sum(amount) from shg_mem_loan where  mtg_no=:mtg_no and  cbo_id=:cbo_id  and completion_flag=:completionFlag")
    fun getTotalOutstandingMemberLoan(cbo_id: Long, mtg_no: Int, completionFlag: Boolean): Int

    @Query("select sum(interest_overdue) from shg_mem_loan where  mtg_no=:mtg_no and  cbo_id=:cbo_id")
    fun getTotalMemberInterestOverdue(cbo_id: Long, mtg_no: Int): Int

    @Query("select * from shg_mem_loan where mtg_no=:mtg_no and cbo_id=:cbo_id")
    fun getUploadListData(mtg_no: Int, cbo_id: Long): List<DtLoanMemberEntity>

    @Query("Select max(loan_no) from shg_mem_loan Where cbo_id=:cbo_id and mtg_no=:mtg_no")
    fun getCutOffmaxLoanno(cbo_id: Long, mtg_no: Int): Int

    @Query("Delete from shg_mem_loan Where cbo_id=:cbo_id and mtg_no=:mtg_no")
    fun deleteMemberLoan(cbo_id: Long, mtg_no: Int)

    @Query("Delete from shg_mem_loan Where cbo_id=:cbo_id")
    fun deleteMemberLoanByCboId(cbo_id: Long)

    @Query("select sum(original_loan_amount) from shg_mem_loan where  mtg_no=:mtg_no and  cbo_id=:cbo_id")
    fun getCutOffTotalDisbursement(cbo_id: Long, mtg_no: Int): Int

    @Query("Select no_of_loans from shg_mem_loan where mtg_no=:mtgNum and mem_id=:mem_id and completion_flag=:completionFlag")
    fun getTotalClosedLoan(mtgNum: Int, mem_id: Long, completionFlag: Boolean): Int

    @Query("Select original_loan_amount from shg_mem_loan where mtg_no=:mtgNum and mem_id=:mem_id and completion_flag=:completionFlag")
    fun getClosedLoanAmount(mtgNum: Int, mem_id: Long, completionFlag: Boolean): Int


    @Query("Update shg_mem_loan set is_edited=1,completion_flag=:completionflag where cbo_id=:cbo_id and mem_id=:mem_id and loan_no=:loan_no")
    fun updateMemberLoanEditFlag(cbo_id: Long, mem_id: Long, loan_no: Int, completionflag: Boolean)

    @Query("select * from shg_mem_loan where is_edited=1 and cbo_id=:cbo_id")
    fun getUploadMemberLoanList(cbo_id: Long): List<DtLoanMemberEntity>


}