package com.microware.cdfi.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.api.meetinguploadmodel.ShgMeberLoanScheduleData
import com.microware.cdfi.entity.DtLoanMemberSheduleEntity

@Dao
interface DtLoanMemberScheduleDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLoanMemberScheduleAllData(dtLoanMemberScheduleEntity: List<DtLoanMemberSheduleEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLoanMemberScheduleData(dtLoanMemberScheduleEntity: DtLoanMemberSheduleEntity)

    @Query("Select * from shg_mem_loan_schedule Where cbo_id=:cbo_id")
    fun getLoanMemberScheduledata(cbo_id: Int): List<DtLoanMemberSheduleEntity>?

    @Query("Select * from shg_mem_loan_schedule Where mem_id=:mem_id and loan_no=:loanno")
    fun getMemberScheduledata(
        mem_id: Long,
        loanno: Int
    ): List<DtLoanMemberSheduleEntity>?

    @Query("Select * from shg_mem_loan_schedule Where mem_id=:mem_id  and loan_no=:loanno and (repaid=0 or mtg_no=:mtgno) order by installment_no")
    fun getMemberScheduleloanwise(
        mem_id: Long,
        loanno: Int,
        mtgno: Int
    ): List<DtLoanMemberSheduleEntity>?

    @Query("Select sum(principal_demand) from shg_mem_loan_schedule Where loan_no=:loanno and mem_id=:mem_id and mtg_guid=:mtgguid ")
    fun gettotalloanamt(loanno: Int, mem_id: Long, mtgguid: String): Int

    @Query("Select sum(principal_demand) from shg_mem_loan_schedule Where repaid=0 and loan_no=:loanno and mem_id=:mem_id  ")
    fun gettotaloutstanding(loanno: Int, mem_id: Long): Int

    @Query("Select count(installment_no) from shg_mem_loan_schedule Where repaid=0 and loan_no=:loanno and mem_id=:mem_id  ")
    fun getremaininginstallment(loanno: Int, mem_id: Long): Int

    @Query("Select sum(principal_demand) from shg_mem_loan_schedule Where loan_no=:loanno and mem_id=:mem_id and installment_date>:lastmtgdate and installment_date<=:currentmtgdate and repaid=0 ")
    fun getprincipaldemand(loanno: Int, mem_id: Long, currentmtgdate: Long, lastmtgdate: Long): Int


    @Query("Delete from shg_mem_loan_schedule where cbo_id=:cbo_id")
    fun deleteRecord(cbo_id: Int)

    @Query("DELETE From shg_mem_loan_schedule")
    fun deleteAll()

    @Query(
        "Update shg_mem_loan_schedule set principal_demand=:principaldemand, loan_os=:loanos, loan_demand_os=:loandemandos where mem_id=:mem_id and mtg_guid=:mtgguid and loan_no=:loanno and installment_no=:installmentno"
    )
    fun updateLoanMemberSchedule(
        mtgguid: String,
        mem_id: Long,
        loanno: Int,
        installmentno: Int,
        principaldemand: Int,
        loandemandos: Int,
        loanos: Int
    )

    @Query(
        "Update shg_mem_loan_schedule set repaid=1,mtg_no=:mtgno, loan_paid=:loanpaid, last_paid_date=:mtgdate, updated_by=:updatedby, updated_on=:updatedon where mem_id=:mem_id  and loan_no=:loanno and installment_no=:installmentno and sub_installment_no=:sub_installment_no"
    )
    fun updaterepaid(
        loanno: Int,
        mem_id: Long,
        installmentno: Int,
        sub_installment_no: Int,
        loanpaid: Int,
        mtgdate: Long,
        mtgno: Int,
        updatedby: String?,
        updatedon: Long
    )

    @Query(
        "Update shg_mem_loan_schedule set principal_demand=:principaldemand,loan_demand_os=:principaldemand where mem_id=:mem_id and mtg_guid=:mtgguid and loan_no=:loanno and installment_no=:installmentno"
    )
    fun updateCutOffLoanMemberSchedule(
        mtgguid: String,
        mem_id: Long,
        loanno: Int,
        installmentno: Int,
        principaldemand: Int
    )

    @Query("Select principal_demand from shg_mem_loan_schedule where mem_id=:mem_id and mtg_guid=:mtg_guid and loan_no=:loan_no and installment_no=:installment_no")
    fun getPrincipalDemandByInstallmentNum(mem_id: Long,mtg_guid: String,loan_no: Int,installment_no:Int):Int

    @Query("Select principal_demand from shg_mem_loan_schedule where  loan_no=:loanno and mem_id=:mem_id and repaid=0 limit 1")
    fun getnextdemand(mem_id: Long,loanno:Int): Int

    @Query(
        "Update shg_mem_loan_schedule set repaid=0, loan_paid=0 where mem_id=:mem_id  and loan_no=:loanno and mtg_no=:mtgno and  last_paid_date=:mtgdate"
    )
    fun deleterepaid(
        loanno: Int,
        mem_id: Long,
        mtgdate: Long,
        mtgno: Int
    )

    @Query(
        "Delete from shg_mem_loan_schedule  where mem_id=:mem_id  and loan_no=:loanno and mtg_no=:mtgno and  last_paid_date=:mtgdate and sub_installment_no>1"
    )
    fun deletesubinstallment(
        loanno: Int,
        mem_id: Long,
        mtgdate: Long,
        mtgno: Int
    )

    @Query("Update shg_mem_loan_schedule set repaid=0, loan_paid=0 where cbo_id=:cbo_id and mtg_no=:mtgno and last_paid_date=:mtgdate")
    fun deleteMemberRepaidData(mtgdate: Long,mtgno: Int,cbo_id: Long)

    @Query("Delete from shg_mem_loan_schedule  where cbo_id=:cbo_id and mtg_no=:mtgno and  last_paid_date=:mtgdate and sub_installment_no>1")
    fun deleteMemberSubinstallmentData(mtgdate: Long,mtgno: Int,cbo_id: Long)

  @Query("Delete from shg_mem_loan_schedule  where mtg_guid=:mtgGuid")
    fun deleteMemberScheduleByMtgGuid(mtgGuid: String)

    @Query("Delete from shg_mem_loan_schedule  where cbo_id=:cbo_id")
    fun deleteMemberSubinstallmentDataByCboId(cbo_id: Long)

    @Query("Select sum(principal_demand) from shg_mem_loan_schedule Where repaid=0  and mem_id=:mem_id  ")
    fun gettotalloanoutstanding( mem_id: Long): Int

    @Query("Select max(installment_no) from shg_mem_loan_schedule where repaid = 1 and cbo_id=:cboId and mem_id=:mem_id  and loan_no=:loanno ")
    fun getMaxRepaidInstallmentNum(cboId:Long,mem_id: Long,loanno: Int):Int

    @Query("Select installment_date from shg_mem_loan_schedule where installment_no=:installmentNum and cbo_id=:cboId and mem_id=:mem_id  and loan_no=:loanno")
    fun getInstallmentdateByInstallmentNum(installmentNum:Int,cboId:Long,mem_id: Long,loanno: Int):Long

    @Query("Delete from shg_mem_loan_schedule where repaid = 0 and cbo_id=:cbo_id and mem_id=:mem_id and loan_no=:loanno and sub_installment_no>1")
    fun deleteMemberSubinstallment(cbo_id: Long,mem_id: Long,loanno: Int)

    @Query("select * from shg_mem_loan_schedule where mem_id=:mem_id and cbo_id=:cbo_id and loan_no=:loanno")
    fun getUploadListData(mem_id:Long,cbo_id: Long,loanno:Int) : List<DtLoanMemberSheduleEntity>

}