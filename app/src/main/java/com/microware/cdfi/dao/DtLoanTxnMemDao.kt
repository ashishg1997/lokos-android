package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.api.meetingmodel.LoanRepaymentListModel
import com.microware.cdfi.api.meetinguploadmodel.ShgMemberLoanTransactionListData
import com.microware.cdfi.entity.DtLoanTxnMemEntity

@Dao
interface DtLoanTxnMemDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLoanTxnMemAllData(dtLoanTxnMemEntity: List<DtLoanTxnMemEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLoanTxnMemData(dtLoanTxnMemEntity: DtLoanTxnMemEntity)

    @Query("Select * from shg_mem_loan_txn Where cbo_id=:cboid and mem_id=:mem_id and mtg_no=:mtgno order by loan_no")
    fun getmeetingLoanTxnMemdata(cboid: Long,mem_id: Long, mtgno: Int): List<DtLoanTxnMemEntity>?

    @Query("Select * from shg_mem_loan_txn Where mem_id=:memid and mtg_no=:mtgno")
    fun getmemberloantxnlist(memid: Long, mtgno: Int): List<DtLoanTxnMemEntity>?

    @Query("Select count(loan_no) as loan_no,loan_op,loan_op_int,sum(loan_paid) as loan_paid,sum(loan_paid_int) as loan_paid_int,sum(principal_demand) as principal_demand,sum(principal_demand_ob) as principal_demand_ob,sum(principal_demand_cb)  as principal_demand_cb,sum(int_accrued)  as int_accrued ,sum(int_accrued_op)  as int_accrued_op from shg_mem_loan_txn Where mem_id=:memid and mtg_no=:mtgno")
    fun getmemberloan(memid: Long, mtgno: Int): List<LoanRepaymentListModel>?

    @Query("Select * from shg_mem_loan_txn where loan_no=:loanno and mtg_no=:mtgno and mem_id=:memid")
    fun getmemberloandata(loanno: Int, mtgno: Int, memid: Long): List<DtLoanTxnMemEntity>?

    @Query("Delete from shg_mem_loan_txn where uid=:uid")
    fun deleteRecord(uid: Int)

    @Query("DELETE From shg_mem_loan_txn")
    fun deleteAll()


    @Query("Select (sum(principal_demand_ob)+sum(principal_demand)+sum(int_accrued)+sum(int_accrued_op))-(sum(loan_paid_int)+sum(loan_paid)) as loan_paid from shg_mem_loan_txn Where mem_id=:mem_id and mtg_no=:mtgno")
    fun gettotalpaid(mem_id: Long, mtgno: Int): Int

    @Query("Select sum(loan_paid_int)+sum(loan_paid) from shg_mem_loan_txn Where mode_payment=1 and mtg_no=:mtgno and cbo_id=:cbo_id")
    fun gettotloanpaid(mtgno:Int,cbo_id:Long): Int

    @Query("Select sum(loan_paid_int)+sum(loan_paid) from shg_mem_loan_txn Where (mode_payment=2 or mode_payment=3) and mtg_no=:mtgno and cbo_id=:cbo_id and bank_code=:account")
    fun getmemtotloanpaidinbank(mtgno:Int,cbo_id:Long,account:String): Int

    @Query("Select (sum(loan_paid)+sum(loan_paid_int))from shg_mem_loan_txn Where cbo_id=:cboid and mtg_no=:mtgno")
    fun getLoanRepaymentAmount(cboid: Long, mtgno: Int):Int

    @Query("Select (sum(loan_paid)+sum(loan_paid_int))from shg_mem_loan_txn Where cbo_id=:cboid and mtg_no=:mtgno and mode_payment=:paymentMode")
    fun getLoanRepaymentAmountByMode(cboid: Long, mtgno: Int,paymentMode:Int):Int

    @Query("Select (sum(loan_paid)+sum(loan_paid_int))from shg_mem_loan_txn Where cbo_id=:cboid and mtg_no=:mtgno and (mode_payment=2 OR mode_payment=3)")
    fun getLoanRepaymentAmountByMode1(cboid: Long, mtgno: Int):Int

    @Query("Select (sum(loan_paid)) from shg_mem_loan_txn where cbo_id=:cboid and mtg_no=:mtgno and mem_id=:mem_id")
    fun getMemberLoanRepaymentAmount(cboid: Long, mtgno: Int,mem_id: Long):Int

    @Query("Select (sum(loan_paid_int)) from shg_mem_loan_txn where cbo_id=:cboid and mtg_no=:mtgno and mem_id=:mem_id")
    fun getMemberLoanRepaymentIntAmount(cboid: Long, mtgno: Int,mem_id: Long):Int

    @Query("Select (sum(loan_op)) from shg_mem_loan_txn where cbo_id=:cboid and mtg_no=:mtgno and mem_id=:mem_id")
    fun getMemberLoanRepaymentOPAmount(cboid: Long, mtgno: Int,mem_id: Long):Int

    @Query("Select (sum(loan_paid)+sum(loan_paid_int))from shg_mem_loan_txn Where cbo_id=:cboid and mtg_no=:mtgno and bank_code=:bank_code")
    fun getShgMemLoanTxnAnount(cboid: Long, mtgno: Int,bank_code:String):Int

    @Query("Update shg_mem_loan_txn set completion_flag=:completionFlag,loan_paid=:paid,loan_paid_int=:loanint,int_accrued_cl=(int_accrued_op+int_accrued)-:loanint,loan_cl_int=loan_op_int+:loanint,loan_cl=loan_op-:paid,principal_demand_cb=:principaldemandcl,mode_payment=:mode,bank_code=:bankaccount,transaction_no=:transactionno where loan_no=:loanno and mtg_no=:mtgno and mem_id=:memid")
    fun updateloanpaid(
        loanno: Int, mtgno: Int, memid: Long, paid: Int, loanint: Int,
        mode: Int,
        bankaccount: String,
        transactionno: String,
        principaldemandcl: Int,
        completionFlag:Boolean
    )
    @Query("select * from shg_mem_loan_txn where mtg_no=:mtg_no and cbo_id=:cbo_id and mem_id=:mem_id")
    fun getUploadListData(mtg_no: Int,cbo_id: Long,mem_id: Long) : List<DtLoanTxnMemEntity>

    @Query("Delete from shg_mem_loan_txn where mtg_no=:mtg_no and cbo_id=:cbo_id")
    fun deleteMemberLoanTxnData(mtg_no: Int,cbo_id: Long)

    @Query("Delete from shg_mem_loan_txn where cbo_id=:cbo_id")
    fun deleteMemberLoanTxnDataByCboId(cbo_id: Long)

    @Query("Select sum(principal_demand) from shg_mem_loan_txn Where cbo_id=:cbo_id and mtg_no=:mtg_no")
    fun getTotalPrincipalDemand(mtg_no: Int,cbo_id: Long) :Int

    @Query("Select sum(principal_demand_ob) from shg_mem_loan_txn Where cbo_id=:cbo_id and mtg_no=:mtg_no")
    fun getTotalPrincipalDemandOb(mtg_no: Int,cbo_id: Long) :Int

    @Query("Select sum(int_accrued) from shg_mem_loan_txn Where cbo_id=:cbo_id and mtg_no=:mtg_no")
    fun getTotalInterestAccured(mtg_no: Int,cbo_id: Long) :Int

    @Query("Select sum(int_accrued_op) from shg_mem_loan_txn Where cbo_id=:cbo_id and mtg_no=:mtg_no")
    fun getTotalInterestAccuredOb(mtg_no: Int,cbo_id: Long) :Int

    @Query("select * from shg_mem_loan_txn where mtg_no=:mtg_no and cbo_id=:cbo_id")
    fun getMemberLoanTxnData(mtg_no: Int,cbo_id: Long) : List<DtLoanTxnMemEntity>
}

