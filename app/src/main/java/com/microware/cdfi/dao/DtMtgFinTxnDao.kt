package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.api.meetinguploadmodel.ShgFinanceTransactionListData
import com.microware.cdfi.entity.DtMtgFinTxnEntity

@Dao
interface DtMtgFinTxnDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMtgFinTxnAllData(dtMtgFinTxnEntity: List<DtMtgFinTxnEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMtgFinTxnData(dtMtgFinTxnEntity: DtMtgFinTxnEntity)

    @Query("Select * from shg_fin_txn Where uid=:uid")
    fun getMtgFinTxndata(uid: Int): List<DtMtgFinTxnEntity>?

    @Query("Delete from shg_fin_txn where uid=:uid")
    fun deleteRecord(uid: Int)

    @Query("DELETE From shg_fin_txn")
    fun deleteAll()

    @Query(
            "Update shg_fin_txn set cbo_id=:cbo_id, mtg_no=:mtgno, bank_code=:bankcode, opening_balance=:openingbalance, closing_balance=:closingbalance,other_deposits=:otherdeposits, other_withdrawals=:otherwithdrawals,deposited_cash=:depositedcash, withdrawn_cash=:withdrawncash,created_by=:createdby, created_on=:createdon, updated_by=:updatedby, updated_on=:updatedon, uploaded_by=:uploaded_by Where uid=:uid"
    )
    fun updateMtgFinTxn(
            uid: Int?,
            cbo_id: Int?,
            mtgno: Int?,
            bankcode: String?,
            openingbalance: Int?,
            closingbalance: Int?,
            otherdeposits: Int?,
            otherwithdrawals: Int?,
            depositedcash: Int?,
            withdrawncash: Int?,
            createdby: String?,
            createdon: Long?,
            updatedby: String,
            updatedon: Long?,
            uploaded_by: String?

    )

    @Query("select * from shg_fin_txn where mtg_no=:mtgno and cbo_id=:cbo_id and bank_code=:accountNo")
    fun getListDataByMtgnum(mtgno: Int, cbo_id: Long, accountNo: String): List<DtMtgFinTxnEntity>


    @Query("Update shg_fin_txn set closing_balance=:closingbal where mtg_no=:mtgno and cbo_id=:cbo_id and bank_code=:accountno ")
    fun updateclosingbalbank(
            closingbal: Int,
            mtgno: Int,
            cbo_id: Long,
            accountno: String
    )

    @Query("Select sum(closing_balance) from shg_fin_txn Where  mtg_no=:mtgno and cbo_id=:cbo_id")
    fun gettotalinbank(mtgno: Int, cbo_id: Long): Int

    @Query("Select sum(opening_balance) from shg_fin_txn Where  mtg_no=:mtgno and cbo_id=:cbo_id and bank_code=:accountno ")
    fun getopenningbal(mtgno: Int, cbo_id: Long, accountno: String): Int

    @Query("Select sum(closing_balance) from shg_fin_txn Where  mtg_no=:mtgno and cbo_id=:cbo_id and bank_code=:accountno ")
    fun getclosingbal(mtgno: Int, cbo_id: Long, accountno: String): Int

    @Query("Update shg_fin_txn set closing_balance=:closingbalance,zero_mtg_cash_bank=:zero_mtg_cash_bank, cheque_issued_not_realized=:cheque_issued_not_realized,cheque_received_not_credited=:cheque_received_not_credited,balance_date=:balance_date,updated_by=:updatedby, updated_on=:updatedon, uploaded_by=:uploaded_by Where cbo_id=:cbo_id and mtg_no=:mtgno and bank_code=:bankcode")
    fun updateCutOffBankBalance(
        cbo_id: Long?,
        mtgno: Int?,
        bankcode: String?,
        zero_mtg_cash_bank: Int?,
        cheque_issued_not_realized: Int?,
        cheque_received_not_credited: Int?,
        closingbalance: Int?,
        balance_date: Long?,
        updatedby: String,
        updatedon: Long?,
        uploaded_by: String?
    )

    @Query("select * from shg_fin_txn where mtg_no=:mtgno and cbo_id=:cbo_id")
    fun getBankListDataByMtgnum(mtgno:Int,cbo_id:Long): List<DtMtgFinTxnEntity>

    @Query("select * from shg_fin_txn where mtg_no=:mtg_no and cbo_id=:cbo_id")
    fun getUploadListData(mtg_no: Int,cbo_id: Long) : List<DtMtgFinTxnEntity>

    @Query("Delete from shg_fin_txn where mtg_no=:mtg_no and cbo_id=:cbo_id")
    fun deleteShgFinancialTxnData(mtg_no: Int,cbo_id: Long)

    @Query("Delete from shg_fin_txn where cbo_id=:cbo_id")
    fun deleteShgFinancialTxnDataByCboId(cbo_id: Long)

    @Query("select count(*) from shg_fin_txn where mtg_no=:mtgno and cbo_id=:cbo_id")
    fun getBankCount(mtgno:Int,cbo_id:Long): Int

    @Query("Select withdrawn_cash from shg_fin_txn where  mtg_no=:mtgno and cbo_id=:cbo_id and bank_code=:bankAccount")
    fun getBank_CashInTransit(mtgno:Int,cbo_id:Long,bankAccount:String): Int

    @Query("update shg_fin_txn set withdrawn_cash=:amount where  mtg_no=:mtgno and cbo_id=:cbo_id and bank_code=:bankAccount")
    fun updateBank_CashInTransit(amount:Int,mtgno:Int,cbo_id:Long,bankAccount:String)
}