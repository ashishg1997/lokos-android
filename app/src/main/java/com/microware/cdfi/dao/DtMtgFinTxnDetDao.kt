package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.DtMtgFinTxnDetEntity

@Dao
interface DtMtgFinTxnDetDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMtgFinTxnDetAllData(dtMtgFinTxnDetEntity: List<DtMtgFinTxnDetEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMtgFinTxnDetData(dtMtgFinTxnDetEntity: DtMtgFinTxnDetEntity)

    @Query("Select * from financial_transactions_oth Where uid=:uid")
    fun getMtgFinTxnDetdata(uid: Int): List<DtMtgFinTxnDetEntity>?

    @Query("Delete from financial_transactions_oth where uid=:uid")
    fun deleteRecord(uid: Int)

    @Query("DELETE From financial_transactions_oth")
    fun deleteAll()

    @Query(
        "Update financial_transactions_oth set cbo_id=:cbo_id, group_m_code=:group_m_code, mtgno=:mtgno, bankcode=:bankcode, auid=:auid, type=:type, amount=:amount, transdate=:transdate, deposit_receipt=:deposit_receipt, withdrawal_payment=:withdrawal_payment, date_realisation=:date_realisation, loanno=:loanno, effectivedate=:effectivedate, narration=:narration Where uid=:uid"
    )
    fun updateMtgFinTxnDet(
        uid: Int,
        cbo_id: Int,
        group_m_code: Int,
        mtgno: Int,
        bankcode: String,
        auid: Int,
        type: String,
        amount: Double,
        transdate: String,
        deposit_receipt: Double,
        withdrawal_payment: Double,
        date_realisation: String,
        loanno: Int,
        effectivedate: String,
        narration: String

    )




}