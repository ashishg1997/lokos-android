package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.api.meetinguploadmodel.ShgGroupLoanScheduleData
import com.microware.cdfi.entity.DtMtgGrpLoanScheduleEntity

@Dao
interface DtMtgGrpLoanScheduleDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMtgGrpLoanScheduleAllData(dtMtgGrpLoanScheduleEntity: List<DtMtgGrpLoanScheduleEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMtgGrpLoanScheduleData(dtMtgGrpLoanScheduleEntity: DtMtgGrpLoanScheduleEntity)

    @Query("Select * from shg_group_loan_schedule Where cbo_id=:cbo_id")
    fun getMtgGrpLoanScheduledata(cbo_id: Int): List<DtMtgGrpLoanScheduleEntity>?


    @Query("Select * from shg_group_loan_schedule Where cbo_id=:cboid  and loan_no=:loanno and repaid=0 order by installment_no")
    fun getScheduleloanwise(
        cboid: Long,
        loanno: Int
    ): List<DtMtgGrpLoanScheduleEntity>?
    @Query("Select count(installment_no) from shg_group_loan_schedule Where repaid=0 and loan_no=:loanno and cbo_id=:cboid  ")
    fun getremaininginstallment(loanno: Int, cboid: Long): Int

    @Query("Select sum(principal_demand) from shg_group_loan_schedule Where loan_no=:loanno and cbo_id=:cboid and installment_date>:lastmtgdate and installment_date<=:currentmtgdate and repaid=0 ")
    fun getprincipaldemand(loanno: Int, cboid: Long, currentmtgdate: Long, lastmtgdate: Long): Int


    @Query("Select sum(principal_demand) from shg_group_loan_schedule Where repaid=0 and loan_no=:loanno and cbo_id=:cboid  ")
    fun gettotaloutstanding(loanno: Int, cboid: Long): Int

    @Query("Delete from shg_group_loan_schedule where cbo_id=:cbo_id")
    fun deleteRecord(cbo_id: Int)

    @Query("DELETE From shg_group_loan_schedule")
    fun deleteAll()

    @Query("Select * from shg_group_loan_schedule Where cbo_id=:cboid and mtg_guid=:mtgguid and loan_no=:loanno")
    fun getGroupScheduledata(
        cboid: Long,
        mtgguid: String,
        loanno: Int
    ): List<DtMtgGrpLoanScheduleEntity>?

    @Query(
        "Update shg_group_loan_schedule set principal_demand=:principaldemand, loan_os=:loanos, loan_demand_os=:loandemandos where cbo_id=:cboid and mtg_guid=:mtgguid and loan_no=:loanno and installment_no=:installmentno"
    )
    fun updateLoanGroupSchedule(
        mtgguid: String,
        cboid: Long,
        loanno: Int,
        installmentno: Int,
        principaldemand: Int,
        loandemandos: Int,
        loanos: Int
    )

    @Query(
        "Update shg_group_loan_schedule set loan_no=:loan_no, principal_demand=:principal_demand, loan_demand_os=:loan_demand_os, loan_os=:loan_os, loan_paid=:loan_paid, installment_no=:installment_no, sub_installment_no=:sub_installment_no, installment_date=:installment_date, loan_date=:loan_date,repaid=:repaid, last_paid_date=:lastpaid_date, mtg_no=:mtg_no,created_by=:createdby, created_on=:createdon, updated_by=:updatedby, updated_on=:updatedon, uploaded_by=:uploaded_by where cbo_id=:cbo_id"
    )
    fun updateMtgGrpLoanSchedule(
        cbo_id: Long,
        loan_no: Int,
        principal_demand: Int,
        loan_demand_os: Int,
        loan_os: Int,
        loan_paid: Int,
        installment_no: Int,
        sub_installment_no: Int,
        installment_date: Long,
        loan_date: Long,
        repaid: Int,
        lastpaid_date: Long,
        mtg_no: Int,
        createdby: String,
        createdon: Long,
        updatedby: String,
        updatedon: Long,
        uploaded_by: String

    )

    @Query("Update shg_group_loan_schedule set principal_demand=:principaldemand,loan_demand_os=:principaldemand where mtg_guid=:mtgguid and loan_no=:loanno and installment_no=:installmentno and cbo_id=:cbo_id")
    fun updateCutOffLoanSchedule(
        mtgguid: String,
        cbo_id: Long,
        loanno: Int,
        installmentno: Int,
        principaldemand: Int
    )

    @Query("Select principal_demand from shg_group_loan_schedule where cbo_id=:cbo_id and mtg_guid=:mtg_guid and loan_no=:loan_no and installment_no=:installment_no")
    fun getPrincipalDemandByInstallmentNum(cbo_id: Long,mtg_guid: String,loan_no: Int,installment_no:Int):Int

    @Query(
        "Update shg_group_loan_schedule set repaid=1, loan_paid=:loanpaid, last_paid_date=:mtgdate, mtg_no=:mtgno, updated_by=:updatedby, updated_on=:updatedon where cbo_id=:cboid  and loan_no=:loanno and installment_no=:installmentno and sub_installment_no=:sub_installment_no"
    )
    fun updaterepaid(
        loanno: Int,
        cboid: Long,
        installmentno: Int,
        sub_installment_no: Int,
        loanpaid: Int,
        mtgdate: Long,
        mtgno: Int,
        updatedby: String?,
        updatedon: Long
    )
    @Query("Update shg_group_loan_schedule set repaid=0, loan_paid=0 where cbo_id=:cbo_id  and loan_no=:loanno and mtg_no=:mtgno and  last_paid_date=:mtgdate")
    fun deleterepaid(loanno: Int,cbo_id: Long,mtgdate: Long,mtgno: Int)

    @Query("Delete from shg_group_loan_schedule  where cbo_id=:cbo_id  and loan_no=:loanno and mtg_no=:mtgno and  last_paid_date=:mtgdate and sub_installment_no>1")
    fun deletsubinstallment(loanno: Int,cbo_id: Long,mtgdate: Long,mtgno: Int)


    @Query("Update shg_group_loan_schedule set repaid=0, loan_paid=0 where cbo_id=:cbo_id and mtg_no=:mtgno and  last_paid_date=:mtgdate")
    fun deleteGroupRepaidData(mtgdate:Long,mtgno:Int,cbo_id:Long)

    @Query("Delete from shg_group_loan_schedule  where cbo_id=:cbo_id and mtg_no=:mtgno and  last_paid_date=:mtgdate and sub_installment_no>1")
    fun deleteGroupSubinstallmentData(mtgdate:Long,mtgno:Int,cbo_id:Long)

    @Query("Delete from shg_group_loan_schedule where mtg_guid=:mtgGuid")
    fun deleteGroupScheduleByMtgGuid(mtgGuid:String)

    @Query("Delete from shg_group_loan_schedule  where cbo_id=:cbo_id")
    fun deleteGroupSubinstallmentDataByCboId(cbo_id:Long)

    @Query("Select * from shg_group_loan_schedule Where cbo_id=:cboId and loan_no=:loanno and (repaid=0 or mtg_no=:mtgno) order by installment_no")
    fun getGrpScheduleloanwise(
        cboId: Long,
        loanno: Int,
        mtgno: Int
    ): List<DtMtgGrpLoanScheduleEntity>?
    @Query("select * from shg_group_loan_schedule where cbo_id=:cbo_id and loan_no=:loanno")
    fun getUploadListData(cbo_id: Long,loanno:Int) : List<DtMtgGrpLoanScheduleEntity>

    @Query("Select principal_demand from shg_group_loan_schedule where  loan_no=:loanno and cbo_id=:cboid and repaid=0 limit 1")
    fun getnextdemand(cboid: Long,loanno:Int): Int

}