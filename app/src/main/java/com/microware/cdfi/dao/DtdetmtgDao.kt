package com.microware.cdfi.dao

import androidx.room.*
import com.microware.cdfi.api.meetingmodel.LoanListModel
import com.microware.cdfi.api.meetingmodel.LoanRepaymentListModel
import com.microware.cdfi.api.meetingmodel.ShareCapitalModel
import com.microware.cdfi.api.meetinguploadmodel.ShgMeetingDetailsListData
import com.microware.cdfi.api.model.SHGMeetingModel
import com.microware.cdfi.entity.DtmtgDetEntity
import com.microware.cdfi.entity.DtmtgEntity

@Dao
interface DtdetmtgDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDtmtgDet(dtmtgDetEntity: DtmtgDetEntity?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAlldtmtgDet(dtmtgDetEntity: List<DtmtgDetEntity>?)

    @Query("select * from shg_mtg_det where cbo_id = :shg_code order by mtg_no limit 1")
    fun getMtg(shg_code: String?): List<DtmtgDetEntity>

    @Query("select * from shg_mtg_det where cbo_id = :shg_id and mtg_no=:mtgnum order by mtg_no")
    fun getMtgByMtgnum(shg_id: Long?, mtgnum: Int): List<DtmtgDetEntity>

    @Query("select * from shg_mtg_det")
    fun getListDataMtgByMtgnum(): List<DtmtgDetEntity>

    @Query("select * from shg_mtg_det where mtg_no=:mtgno and cbo_id=:cbo_id and status='1' order by member_name")
    fun getListDataMtgByMtgnum(mtgno: Int, cbo_id: Long): List<DtmtgDetEntity>

    @Query("select   dtmtg.uid,dtmtg.cbo_id,dtmtg.mem_id, dtmtg.mtg_guid,dtmtg.mtg_no,dtmtg.mtg_date,dtmtg.group_m_code, dtmtg.member_name,loan_application_id,request_date,loan_fee,amt_demand,amt_sanction, amt_disbursed,approval_date, tentative_date from shg_mtg_det dtmtg left join shg_loan_application dtloan on dtmtg.mem_id=dtloan.mem_id and dtmtg.mtg_no=dtloan.loan_requested_mtg_no where  dtmtg.mtg_no=:mtgno and  dtmtg.cbo_id=:cbo_id and dtmtg.status='1' order by dtmtg.member_name")
    fun getloanListDataMtgByMtgnum(mtgno: Int, cbo_id: Long): List<LoanListModel>

    @Query("select   dtmtg.uid,dtmtg.cbo_id,dtmtg.mem_id,dtmtg.mtg_guid,dtmtg.mtg_no,dtmtg.mtg_date,dtmtg.group_m_code, dtmtg.member_name,loan_no,loan_op,loan_op_int,sum(loan_paid) as loan_paid,sum(loan_paid_int) as loan_paid_int,sum(principal_demand) as principal_demand,sum(principal_demand_ob) as principal_demand_ob,sum(principal_demand_cb)  as principal_demand_cb from shg_mtg_det dtmtg left join shg_mem_loan_txn dtloantxn on dtmtg.mem_id=dtloantxn.mem_id  where  dtmtg.mtg_no=:mtgno and  dtmtg.cbo_id=:cbo_id  and dtmtg.status='1' group by dtmtg.mem_id order by dtmtg.member_name")
    fun getloanrepaymentList(mtgno: Int, cbo_id: Long): List<LoanRepaymentListModel>

    @Query("select   dtmtg.uid,dtmtg.cbo_id,dtmtg.mem_id,dtmtg.mtg_guid,dtmtg.mtg_no,dtmtg.mtg_date,dtmtg.group_m_code, dtmtg.member_name,count(loan_no) as loan_no,loan_op,loan_op_int,sum(loan_paid) as loan_paid,sum(loan_paid_int) as loan_paidint,sum(principal_demand) as principal_demand,sum(principal_demand_ob) as principal_demand_ob,sum(principal_demand_cb)  as principal_demand_cb from shg_mtg_det dtmtg left join shg_mem_loan_txn dtloantxn on dtmtg.mem_id=dtloantxn.mem_id  where  dtmtg.mtg_no=:mtgno and  dtmtg.mem_id=:memberid group by dtmtg.mem_id order by dtmtg.member_name")
    fun getloanrepaymentListbymember(mtgno: Int, memberid: Long): List<LoanRepaymentListModel>

    @Query("select count(*) from shg_mtg_det where mtg_no=:mtgno and cbo_id=:cbo_id and sav_comp=0")
    fun gettotcountnotsaved(mtgno: Int, cbo_id: Long): Int

    @Query("select (sum(sav_comp)+ +sum(sav_vol)) from shg_mtg_det where mtg_no=:mtgno and cbo_id=:cbo_id")
    fun getupdatedcash(mtgno: Int, cbo_id: Long): Int


    @Query("Update shg_mtg_det set updated_by=:updated_by,updated_on=:updated_on,attendance=:attendance where mtg_no=:mtgno and cbo_id=:cbo_id and group_m_code=:group_m_code")
    fun updateAttendance(
        attendance: String,
        mtgno: Int,
        cbo_id: Long,
        group_m_code: Long,
        updated_by:String,
        updated_on:Long
    )

    @Query("select count(*) from shg_mtg_det where attendance=:attendance and mtg_no=:mtgno and cbo_id=:cbo_id")
    fun getTotalPresentAndAbsent(attendance: String, mtgno: Int, cbo_id: Long): Int

    @Query("Update shg_mtg_det set updated_by=:updated_by,updated_on=:updated_on,sav_comp=:sav_comp,sav_comp_cb=:sav_comp_cb where mtg_no=:mtgno and cbo_id=:cbo_id and group_m_code=:group_m_code")
    fun updateCompulsorySaving(
        sav_comp: Int,
        sav_comp_cb: Int,
        mtgno: Int,
        cbo_id: Long,
        group_m_code: Long,
        updated_by:String,
        updated_on:Long
    )

    @Query("Update shg_mtg_det set updated_by=:updated_by,updated_on=:updated_on,sav_vol=:sav_vol,sav_vol_cb=:sav_vol_cb where mtg_no=:mtgno and cbo_id=:cbo_id and group_m_code=:group_m_code")
    fun updateVoluntorySaving(
        sav_vol: Int,
        sav_vol_cb: Int,
        mtgno: Int,
        cbo_id: Long,
        group_m_code: Long,
        updated_by:String,
        updated_on:Long
    )

    @Query("Update shg_mtg_det set updated_by=:updated_by,updated_on=:updated_on,penalty=:penalty where mtg_no=:mtgno and cbo_id=:cbo_id and group_m_code=:group_m_code")
    fun updatePenaltySaving(
        penalty: Int,
        mtgno: Int,
        cbo_id: Long,
        group_m_code: Long,
        updated_by:String,
        updated_on:Long
    )

    @Query("select (sum(sav_comp)) from shg_mtg_det where mtg_no=:mtgno and cbo_id=:cbo_id")
    fun getsumcomp(mtgno: Int, cbo_id: Long): Int

    @Query("select (sum(sav_vol)) from shg_mtg_det where mtg_no=:mtgno and cbo_id=:cbo_id")
    fun getsumvol(mtgno: Int, cbo_id: Long): Int

    @Query("select (sum(attendance)) from shg_mtg_det where mtg_no=:mtgno and cbo_id=:cbo_id and attendance=1")
    fun getsumattendance(mtgno: Int, cbo_id: Long): Int

    @Query("select dtmtg.uid,dtmtg.cbo_id,dtmtg.mem_id,dtmtg.mtg_guid,dtmtg.mtg_no,dtmtg.member_name, auid,type,amount,trans_date, date_realisation,mode_payment, transaction_no from shg_mtg_det dtmtg left join shg_fin_txn_det_mem dtFinTxn on dtmtg.mem_id=dtFinTxn.mem_id and dtFinTxn.mtg_no=:mtgno and type=:type where dtmtg.mtg_no=:mtgno and dtmtg.cbo_id=:cbo_id and dtmtg.status='1' order by dtmtg.member_name")
    fun getCapitalListDataMtgByMtgnum(mtgno: Int, cbo_id: Long,type:String): List<ShareCapitalModel>

    @Query("select sum(zero_mtg_attn) from shg_mtg_det where mtg_no=:mtgno and cbo_id=:cbo_id")
    fun getCutOffTotalPresent(mtgno: Int, cbo_id: Long): Int

    @Query("select dtmtg.uid,dtmtg.cbo_id,dtmtg.mem_id,dtmtg.mtg_guid,dtmtg.mtg_no,dtmtg.member_name, auid,type,amount,trans_date, date_realisation,mode_payment, transaction_no from shg_mtg_det dtmtg left join shg_fin_txn_det_mem dtFinTxn on dtmtg.mem_id=dtFinTxn.mem_id and dtFinTxn.auid =:receiptType and dtFinTxn.mtg_no=:mtgno where dtmtg.mtg_no=:mtgno and dtmtg.cbo_id=:cbo_id order by dtmtg.member_name")
    fun getShareCapitalListByReceiptType(
        mtgno: Int,
        cbo_id: Long,
        receiptType: Int
    ): List<ShareCapitalModel>

    @Query("select dtmtg.uid,dtmtg.cbo_id,dtmtg.mem_id, dtmtg.mtg_guid,dtmtg.mtg_no,dtmtg.mtg_date,dtmtg.group_m_code, dtmtg.member_name,loan_no as loan_no,amount as amt_disbursed,original_loan_amount as original_loan_amount from shg_mtg_det dtmtg left join shg_mem_loan dtloan on dtmtg.mem_id=dtloan.mem_id and dtloan.mtg_no=:mtgno  and completion_flag=:completionFlag where  dtmtg.mtg_no=:mtgno and  dtmtg.cbo_id=:cbo_id order by dtmtg.member_name")
    fun getCutOffloanListDataByMtgnum(mtgno: Int, cbo_id: Long,completionFlag:Boolean): List<LoanListModel>

    @Query("select dtmtg.uid,dtmtg.cbo_id,dtmtg.mem_id, dtmtg.mtg_guid,dtmtg.mtg_no,dtmtg.mtg_date,dtmtg.group_m_code, dtmtg.member_name,loan_no as loan_no,amount as amt_disbursed,original_loan_amount as original_loan_amount from shg_mtg_det dtmtg left join shg_mem_loan dtloan on dtmtg.mem_id=dtloan.mem_id and dtloan.mtg_no=:mtgno  and completion_flag=:completionFlag where  dtmtg.mtg_no=:mtgno and  dtmtg.cbo_id=:cbo_id group by dtmtg.mem_id order by dtmtg.member_name")
    fun getCutOffClosedMemberLoanDataByMtgNum(mtgno: Int, cbo_id: Long,completionFlag:Boolean): List<LoanListModel>

    @Query("select (sum(sav_comp_ob)+ +sum(sav_vol_ob)) from shg_mtg_det where mtg_no=:mtgno and cbo_id=:cbo_id")
    fun getopeningcash(mtgno: Int, cbo_id: Long): Int

    @Query("select count(*) from shg_mtg_det where mtg_no=:mtgno and cbo_id=:cbo_id")
    fun getTotalMember(mtgno: Int, cbo_id: Long): Int

    @Query("select * from shg_mtg_det where mtg_no=:mtgno and cbo_id=:cbo_id and mem_id=:mem_id")
    fun getCompulsoryData(mtgno: Int, cbo_id: Long, mem_id: Long): List<DtmtgDetEntity>


    @Query("Update shg_mtg_det set sav_comp_cb=:sav_comp_cb,sav_vol_cb=:sav_vol_cb where mtg_no=:mtgno  and mem_id=:mem_id")
    fun updateclosing(mem_id: Long, mtgno: Int, sav_comp_cb: Int, sav_vol_cb: Int)


    @Query("Update shg_mtg_det set loan1_no=:loanno,loan1_ob=:loanop,loan1_int_accrued=:loan_int_accrued where mtg_no=:mtgno  and mem_id=:mem_id")
    fun updateloandata1(mem_id: Long, mtgno: Int, loanno: Int, loanop: Int, loan_int_accrued: Int)

    @Query("Update shg_mtg_det set loan2_no=:loanno,loan2_ob=:loanop,loan1_int_accrued=:loan_int_accrued  where mtg_no=:mtgno  and mem_id=:mem_id")
    fun updateloandata2(mem_id: Long, mtgno: Int, loanno: Int, loanop: Int, loan_int_accrued: Int)

    @Query("Update shg_mtg_det set loan3_no=:loanno,loan3_ob=:loanop,loan1_int_accrued=:loan_int_accrued  where mtg_no=:mtgno  and mem_id=:mem_id")
    fun updateloandata3(mem_id: Long, mtgno: Int, loanno: Int, loanop: Int, loan_int_accrued: Int)

    @Query("Update shg_mtg_det set loan4_no=:loanno,loan4_ob=:loanop,loan1_int_accrued=:loan_int_accrued  where mtg_no=:mtgno  and mem_id=:mem_id")
    fun updateloandata4(mem_id: Long, mtgno: Int, loanno: Int, loanop: Int, loan_int_accrued: Int)

    @Query("Update shg_mtg_det set loan5_no=:loanno,loan5_ob=:loanop,loan1_int_accrued=:loan_int_accrued  where mtg_no=:mtgno  and mem_id=:mem_id")
    fun updateloandata5(mem_id: Long, mtgno: Int, loanno: Int, loanop: Int, loan_int_accrued: Int)

    @Query("Update shg_mtg_det set updated_by=:updated_by,updated_on=:updated_on,zero_mtg_attn=:attendance where mtg_no=:mtgno and cbo_id=:cbo_id and group_m_code=:group_m_code")
    fun updateCutOffAttendance(
        attendance: String,
        mtgno: Int,
        cbo_id: Long,
        group_m_code: Long,
        updated_by:String,
        updated_on:Long
    )

    @Query("Update shg_mtg_det set updated_by=:updated_by,updated_on=:updated_on,sav_comp=:compulsaryAmount,sav_comp_cb=:compulsaryAmount,sav_vol=:voluntaryAmount,sav_vol_cb=:voluntaryAmount where mtg_no=:mtgno and cbo_id=:cbo_id and group_m_code=:group_m_code")
    fun updateCutOffSaving(
        compulsaryAmount: Int,
        voluntaryAmount: Int,
        mtgno: Int,
        cbo_id: Long,
        group_m_code: Long,
        updated_by:String,
        updated_on:Long
    )


    @Query("select * from shg_mtg_det")
    fun getUploadListData() : List<DtmtgEntity>

    @Query("select * from shg_mtg_det where mtg_no=:mtg_no and cbo_id=:cbo_id")
    fun getUploadListData(mtg_no:Int,cbo_id: Long) : List<DtmtgDetEntity>

    @Query("select (sum(penalty)) from shg_mtg_det where mtg_no=:mtgno and cbo_id=:cbo_id")
    fun getTotalPenaltyByMtgNum(mtgno: Int, cbo_id: Long): Int

    @Query("Delete from shg_mtg_det where mtg_no=:mtg_no and cbo_id=:cbo_id")
    fun deleteShgDetailData(mtg_no: Int,cbo_id: Long)

  @Query("Delete from shg_mtg_det where cbo_id=:cbo_id")
    fun deleteShgDetailDataByCboId(cbo_id: Long)

    @Query("select dtmtg.uid,dtmtg.cbo_id,dtmtg.mem_id, dtmtg.mtg_guid,dtmtg.mtg_no,dtmtg.mtg_date,dtmtg.group_m_code, dtmtg.member_name,loan_no as loan_no,amount as amt_disbursed,original_loan_amount as original_loan_amount from shg_mtg_det dtmtg left join shg_mem_loan dtloan on dtmtg.mem_id=dtloan.mem_id and dtloan.mtg_no=:mtgno  and completion_flag=:completionFlag where  dtmtg.mtg_no=:mtgno and  dtmtg.cbo_id=:cbo_id and dtmtg.mem_id=:memberid order by dtmtg.member_name")
    fun getClosedMemberLoanByMemberId(mtgno: Int, cbo_id: Long,completionFlag:Boolean,memberid: Long): List<LoanListModel>

    @Query("select dtmtg.uid,dtmtg.cbo_id,dtmtg.mem_id, dtmtg.mtg_guid,dtmtg.mtg_no,dtmtg.mtg_date,dtmtg.group_m_code, dtmtg.member_name,loan_no as loan_no,amount as amt_disbursed,original_loan_amount as original_loan_amount from shg_mtg_det dtmtg left join shg_mem_loan dtloan on dtmtg.mem_id=dtloan.mem_id and dtloan.mtg_no=:mtgno  and completion_flag=:completionFlag where  dtmtg.mtg_no=:mtgno and  dtmtg.cbo_id=:cbo_id order by dtmtg.member_name")
    fun getCutOffClosedMemberLoanDataByCboId(mtgno: Int, cbo_id: Long,completionFlag:Boolean): List<LoanListModel>

    @Query("Update shg_mtg_det set updated_by=:updated_by,updated_on=:updated_on,sav_vol_withdrawal=sav_vol_withdrawal+:sav_vol_withdrawal,sav_vol_cb=sav_vol_cb-:sav_vol_withdrawal where mtg_no=:mtgno and cbo_id=:cbo_id and mem_id=:memid")
    fun updatewithdrawl(
        sav_vol_withdrawal: Int,
        mtgno: Int,
        cbo_id: Long,
        memid: Long,
        updated_by:String,
        updated_on:Long
    )

    @Query("Update shg_mtg_det set updated_by=:updated_by,updated_on=:updated_on,sav_comp_withdrawal=:sav_withdrawal where mtg_no=:mtgno and cbo_id=:cbo_id and mem_id=:memid")
    fun updatesavingwithdrawl(
        sav_withdrawal: Int,
        mtgno: Int,
        cbo_id: Long,
        memid: Long,
        updated_by:String,
        updated_on:Long
    )

    @Query("Update shg_mtg_det set updated_by=:updated_by,updated_on=:updated_on,reference_mtg_no=:refmtgno,sav_comp_cb=sav_comp_cb+:sav_withdrawal where mtg_no=:mtgno and cbo_id=:cbo_id and mem_id=:memid")
    fun updateadjustwithdrawl(
        sav_withdrawal: Int,
        mtgno: Int,
        cbo_id: Long,
        memid: Long,
        updated_by:String,
        updated_on:Long,
        refmtgno:Int
    )

    @Query("Update shg_mtg_det set updated_by=:updated_by,updated_on=:updated_on,reference_mtg_no=:refmtgno,sav_vol_cb=sav_vol_cb+:sav_vol where mtg_no=:mtgno and cbo_id=:cbo_id and mem_id=:memid")
    fun updateadjustVoluntory(
        sav_vol: Int,
        mtgno: Int,
        cbo_id: Long,
        memid: Long,
        updated_by:String,
        updated_on:Long,
        refmtgno:Int
    )

    @Query("select * from shg_mtg_det where mtg_no=:mtgno and cbo_id=:cbo_id and status='1' and mem_id not in(:memid)")
    fun getListDatawithoutmember(mtgno: Int, cbo_id: Long, memid: Long): List<DtmtgDetEntity>

    @Query("select * from shg_mtg_det where mtg_no=:mtgno and cbo_id=:cbo_id and status='2' ")
    fun getListinactivemember(mtgno: Int, cbo_id: Long): List<DtmtgDetEntity>

    @Query("select (sum(sav_vol_withdrawal)+sum(sav_comp_withdrawal)) from shg_mtg_det where mtg_no=:mtgno and cbo_id=:cbo_id")
    fun getsumwithdrwal(mtgno: Int, cbo_id: Long): Int

    @Query("Select dtmtg.uid,dtmtg.cbo_id,dtmtg.mem_id, dtmtg.mtg_guid,dtmtg.mtg_no,dtmtg.mtg_date,dtmtg.group_m_code, dtmtg.member_name,dtloan.amount as amt_disbursed,loan_application_id,installment_date as request_date from shg_mem_loan dtloan left join shg_mtg_det dtmtg on dtmtg.mem_id=dtloan.mem_id and dtloan.mtg_no= dtmtg.mtg_no where  dtloan.mtg_no<:mtgno and  dtmtg.cbo_id=:cbo_id and dtmtg.status='1' order by dtmtg.member_name")
    fun getRescheduleloanListMtgByMtgnum(mtgno: Int, cbo_id: Long): List<LoanListModel>

    @Query("Select amount from shg_fin_txn_det_mem where mtg_no=:mtgno and mem_id=:memid and auid=:auid")
    fun getMemberAdjustmentAmount(mtgno:Int,memid:Long,auid:Int):Int?
}