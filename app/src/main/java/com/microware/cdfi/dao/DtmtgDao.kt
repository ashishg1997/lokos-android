package com.microware.cdfi.dao

import androidx.room.*
import com.microware.cdfi.api.model.SHGMeetingModel
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.entity.DtmtgEntity

@Dao
interface DtmtgDao {

    @Insert
    fun insertDtmtg(dtmtg: DtmtgEntity?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDtmtg1(dtmtg: DtmtgEntity?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllDtmtg(dtmtg: List<DtmtgEntity>?)

    @Update
    fun updateDtmtg(dtmtg: DtmtgEntity)

    @Query("Delete from shg_mtg")
    fun deleteAllDtmtgs()

    @Query("SELECT distinct tblgroup.*,dtMtg.* FROM shg_profile tblgroup LEFT JOIN  shg_mtg dtMtg ON dtMtg.cbo_id = tblgroup.shg_id where tblgroup.shg_id = :shg_id and tblgroup.Status=1 Order by dtMtg.mtg_no desc limit 1")
    fun getgroupMeetingsdata(shg_id: String?): List<SHGMeetingModel>?

    @Query("SELECT distinct tblgroup.guid,tblgroup.is_voluntary_saving, tblgroup.shg_id,tblgroup.bookkeeper_name, tblgroup.shg_code, tblgroup.status,tblgroup.activation_status,tblgroup.shg_type_code,tblgroup.tags, tblgroup.shg_name, tblgroup.shg_name_short_en, tblgroup.shg_name_short_local, tblgroup.month_comp_saving, dtMtg.mtg_no, dtMtg.mtg_date, dtMtg.Flag_Open, tblgroup.month_comp_saving, tblgroup.shg_formation_date, tblgroup.meeting_frequency, tblgroup.meeting_frequency_value, tblgroup.meeting_on,tblgroup.month_comp_saving, dtMtg.opening_balance, dtMtg.closing_balance,dtMtg.mtg_guid ,dtMtg.approval_status,dtMtg.checker_remarks as checker_remarks,dtMtg.mtg_type as mtg_type FROM shg_profile tblgroup LEFT JOIN  shg_mtg dtMtg ON dtMtg.cbo_id = tblgroup.shg_id where tblgroup.village_id = :villageid and tblgroup.Status=1 and tblgroup.activation_status=2  Group By tblgroup.shg_id Order by dtMtg.mtg_no desc")
    fun getgroupMeetingsdatalistbyvillage(villageid: Int?): List<SHGMeetingModel>?

    @Query("SELECT distinct tblgroup.guid,tblgroup.is_voluntary_saving, tblgroup.shg_id,tblgroup.bookkeeper_name, tblgroup.shg_code, tblgroup.status,tblgroup.activation_status,tblgroup.shg_type_code,tblgroup.tags, tblgroup.shg_name, tblgroup.shg_name_short_en, tblgroup.shg_name_short_local, tblgroup.month_comp_saving, dtMtg.mtg_no, dtMtg.mtg_date, dtMtg.Flag_Open, tblgroup.month_comp_saving, tblgroup.shg_formation_date, tblgroup.meeting_frequency, tblgroup.meeting_frequency_value, tblgroup.meeting_on,tblgroup.month_comp_saving, dtMtg.opening_balance, dtMtg.closing_balance , dtMtg.mtg_guid,dtMtg.approval_status,dtMtg.checker_remarks as checker_remarks,dtMtg.mtg_type as mtg_type FROM shg_profile tblgroup LEFT JOIN  shg_mtg dtMtg ON dtMtg.cbo_id = tblgroup.shg_id where  tblgroup.Status=1 and tblgroup.activation_status=2 Group By tblgroup.shg_id Order by tblgroup.shg_id")
    fun getgroupMeetingsdatalist(): List<SHGMeetingModel>?

    @Query("select * from shg_mtg where cbo_id = :cbo_id order by mtg_no limit 1")
    fun getMtg(cbo_id: String?): List<DtmtgEntity>?

    @Query("select * from shg_mtg where cbo_id = :cbo_id  order by mtg_no")
    fun getMtglist(cbo_id: Long?): List<DtmtgEntity>?

    @Query("select * from shg_mtg where cbo_id = :cbo_id  order by mtg_no desc limit 1")
    fun getMtglistdata(cbo_id: Long?): List<DtmtgEntity>?

    @Query("select flag_open from shg_mtg where cbo_id = :shgid and mtg_no=:Mtgno ")
    fun returnmeetingstatus(shgid: Long,Mtgno:Int): String

    @Query("select count(*) from shg_mtg where cbo_id = :cbo_id")
    fun getmeetingCount(cbo_id: String?): Int

    @Query("select count(*) from shg_mtg group by cbo_id")
    fun gettotmeetingCount(): Int


    @Query("select  sum(opening_balance) from shg_mtg where mtg_no=:mtgno and cbo_id=:cbo_id")
    fun getupdatedpeningcash(mtgno:Int,cbo_id:Long):Int


    @Query("Update shg_mtg set sav_comp_cb=:compsave where mtg_no=:mtgno and cbo_id=:cbo_id ")
    fun updatecompsave(
        compsave: Int,
        mtgno:Int,
        cbo_id:Long
    )
    @Query("Update shg_mtg set sav_vol_cb=:sav_vol where mtg_no=:mtgno and cbo_id=:cbo_id ")
    fun updatevolsave(
        sav_vol: Int,
        mtgno:Int,
        cbo_id:Long
    )
    @Query("SELECT tblgroup.guid, tblgroup.shg_id,tblgroup.is_voluntary_saving,tblgroup.bookkeeper_name, tblgroup.shg_code, tblgroup.status,tblgroup.activation_status,tblgroup.shg_type_code,tblgroup.tags, tblgroup.shg_name, tblgroup.shg_name_short_en, tblgroup.shg_name_short_local, tblgroup.month_comp_saving, tblgroup.shg_formation_date, tblgroup.meeting_frequency, tblgroup.meeting_frequency_value, tblgroup.meeting_on,dtMtg.flag_open,dtMtg.mtg_date,dtMtg.mtg_no,dtMtg.cbo_id,dtMtg.opening_balance, dtMtg.closing_balance ,dtMtg.approval_status,dtMtg.checker_remarks as checker_remarks,dtMtg.mtg_type as mtg_type, dtMtg.mtg_guid from shg_profile tblgroup left join (select * from (SELECT cbo_id,max(mtg_no)as maxmtgno from shg_mtg group by cbo_id ) mtgresult inner JOIN  shg_mtg dtMtg ON dtMtg.cbo_id = mtgresult.cbo_id  and mtgresult.maxmtgno=dtMtg.mtg_no) as dtMtg on tblgroup.shg_id=dtMtg.cbo_id where (CASE WHEN tblgroup.meeting_frequency = 1 and mtg_date>0 then dtMtg.mtg_date+(7 * 24 * 60 * 60)=:c_date WHEN tblgroup.meeting_frequency = 2 and mtg_date>0 then dtMtg.mtg_date+(14 * 24 * 60 * 60) =:c_date WHEN tblgroup.meeting_frequency = 3 and  mtg_date>0 then dtMtg.mtg_date+(28 * 24 * 60 * 60)=:c_date WHEN dtMtg.mtg_no is null THEN tblgroup.shg_formation_date=:c_date END) and tblgroup.Status=1 and tblgroup.activation_status=2 Group By tblgroup.shg_id Order by tblgroup.shg_id")
    fun getCurrentMeetingslist(c_date:Long): List<SHGMeetingModel>

    @Query("SELECT tblgroup.guid, tblgroup.shg_id,tblgroup.is_voluntary_saving,tblgroup.bookkeeper_name, tblgroup.shg_code, tblgroup.status,tblgroup.activation_status,tblgroup.shg_type_code,tblgroup.tags, tblgroup.shg_name, tblgroup.shg_name_short_en, tblgroup.shg_name_short_local, tblgroup.month_comp_saving, tblgroup.shg_formation_date, tblgroup.meeting_frequency, tblgroup.meeting_frequency_value, tblgroup.meeting_on,dtMtg.flag_open,dtMtg.mtg_date,dtMtg.mtg_no,dtMtg.cbo_id,dtMtg.opening_balance, dtMtg.closing_balance ,dtMtg.approval_status,dtMtg.checker_remarks as checker_remarks,dtMtg.mtg_type as mtg_type, dtMtg.mtg_guid from shg_profile tblgroup left join (select * from (SELECT cbo_id,max(mtg_no)as maxmtgno from shg_mtg group by cbo_id ) mtgresult inner JOIN  shg_mtg dtMtg ON dtMtg.cbo_id = mtgresult.cbo_id  and mtgresult.maxmtgno=dtMtg.mtg_no) as dtMtg on tblgroup.shg_id=dtMtg.cbo_id where (CASE WHEN tblgroup.meeting_frequency = 1 and mtg_date>0 then dtMtg.mtg_date+(7 * 24 * 60 * 60)<:c_date WHEN tblgroup.meeting_frequency = 2 and mtg_date>0 then dtMtg.mtg_date+(14 * 24 * 60 * 60) <:c_date WHEN tblgroup.meeting_frequency = 3 and  mtg_date>0 then dtMtg.mtg_date+(28 * 24 * 60 * 60)<:c_date WHEN dtMtg.mtg_no is null THEN tblgroup.shg_formation_date<:c_date END) and tblgroup.Status=1 and tblgroup.activation_status=2 Group By tblgroup.shg_id Order by tblgroup.shg_id")
    fun getMissedMeetingslist(c_date:Long): List<SHGMeetingModel>?

    @Query("SELECT tblgroup.guid, tblgroup.shg_id,tblgroup.is_voluntary_saving,tblgroup.bookkeeper_name, tblgroup.shg_code, tblgroup.status,tblgroup.activation_status,tblgroup.shg_type_code,tblgroup.tags, tblgroup.shg_name, tblgroup.shg_name_short_en, tblgroup.shg_name_short_local, tblgroup.month_comp_saving, tblgroup.shg_formation_date, tblgroup.meeting_frequency, tblgroup.meeting_frequency_value, tblgroup.meeting_on,dtMtg.flag_open,dtMtg.mtg_date,dtMtg.mtg_no,dtMtg.cbo_id,dtMtg.opening_balance, dtMtg.closing_balance ,dtMtg.approval_status,dtMtg.checker_remarks as checker_remarks,dtMtg.mtg_type as mtg_type, dtMtg.mtg_guid from shg_profile tblgroup left join (select * from (SELECT cbo_id,max(mtg_no)as maxmtgno from shg_mtg group by cbo_id ) mtgresult inner JOIN  shg_mtg dtMtg ON dtMtg.cbo_id = mtgresult.cbo_id  and mtgresult.maxmtgno=dtMtg.mtg_no) as dtMtg on tblgroup.shg_id=dtMtg.cbo_id where (CASE WHEN tblgroup.meeting_frequency = 1 and mtg_date>0 then dtMtg.mtg_date+(7 * 24 * 60 * 60)>:c_date WHEN tblgroup.meeting_frequency = 2 and mtg_date>0 then dtMtg.mtg_date+(14 * 24 * 60 * 60) >:c_date WHEN tblgroup.meeting_frequency = 3 and  mtg_date>0 then dtMtg.mtg_date+(28 * 24 * 60 * 60)>:c_date WHEN dtMtg.mtg_no is null THEN tblgroup.shg_formation_date>:c_date END) and tblgroup.Status=1 and tblgroup.activation_status=2 Group By tblgroup.shg_id Order by tblgroup.shg_id")
    fun getUpcomingMeetingslist(c_date:Long): List<SHGMeetingModel>?

    @Query("Select * from cbo_bank_details Where cbo_guid=:cbo_guid and is_active=1")
    fun getBankdata(cbo_guid: String?): List<Cbo_bankEntity>

    @Query("Select max(mtg_no) from shg_mtg where cbo_id=:shg_id")
    fun getMaxMeetingNum(shg_id: Long?):Int

    @Query("Select count(*) from shg_mtg where cbo_id=:shg_id")
    fun getMeetingCount(shg_id: Long?):Int

    @Query("Select max(mtg_no) from shg_mtg where cbo_id=:shg_id and mtg_no<:mtg_num")
    fun getSecondLastMeetingNum(shg_id: Long,mtg_num: Int):Int

    @Query("Update shg_mtg set closing_balance=:closingcash where mtg_no=:mtgno and cbo_id=:cbo_id ")
    fun updateclosingcash(
        closingcash: Int,
        mtgno:Int,
        cbo_id:Long
    )

    @Query("Select * from shg_mtg where is_edited=1 and flag_open='C'")
    fun getShgMeetingList(): List<DtmtgEntity>

    @Query("Update shg_mtg set closing_balance=:closingcash,zero_mtg_cash_in_hand=:cashInHand, zero_mtg_cash_in_transit=:cashInTransit,balance_date=:balanceDate,updated_on=:updatedOn,updated_by=:updatedBy where mtg_no=:mtgno and cbo_id=:cbo_id ")
    fun updateCutOffCash(
        cashInHand: Int,
        cashInTransit: Int,
        balanceDate: Long,
        closingcash: Int,
        mtgno:Int,
        cbo_id:Long,
        updatedOn:Long,
        updatedBy:String
    )

    @Query("Select * from shg_mtg where mtg_no=:mtgno and cbo_id=:cbo_id ")
    fun getMeetingDetailData(mtgno:Int,cbo_id:Long):List<DtmtgEntity>

    @Query("Update shg_mtg set flag_open=:flag where mtg_no=:mtgno and cbo_id=:cbo_id ")
    fun openMeeting(
        flag: String,
        mtgno: Int,
        cbo_id: Long
    )

    @Query("Delete from shg_mtg where mtg_no=:mtg_no and cbo_id=:cbo_id")
    fun deleteShg_MtgData(mtg_no: Int,cbo_id: Long)

    @Query("Delete from shg_mtg where cbo_id=:cbo_id")
    fun deleteShg_MtgByCboId(cbo_id: Long)

    @Query("Update shg_mtg set is_edited=0 where mtg_no=:mtg_no and cbo_id=:cbo_id")
    fun updateShg_mtgIsEdited(mtg_no: Int,cbo_id: Long)

    @Query("Update shg_mtg set flag_open=:flag,is_edited=1,updated_on=:updatedOn,updated_by=:updatedBy where mtg_no=:mtgno and cbo_id=:cbo_id ")
    fun closingMeeting(
        flag: String,
        mtgno: Int,
        cbo_id: Long,
        updatedOn:Long,
        updatedBy:String

    )

    @Query("Update shg_mtg set checker_remarks=:checkerRemarks,updated_on=:updatedOn,updated_by=:updatedBy,approval_status=:approvalStatus where mtg_no=:mtgno and cbo_id=:cbo_id ")
    fun updateMtgApprova_Rejection_Status(
        mtgno: Int,
        cbo_id: Long,
        updatedOn:Long,
        updatedBy:String,
        approvalStatus:Int,
        checkerRemarks:String

    )

    @Query("Select month_comp_saving from shg_profile where shg_id=:shg_id")
    fun getMonthlyCompulsarySaving(shg_id: Long?):Int

    @Query("update shg_mtg set checker_remarks=:checker_remarks,is_edited =0,transaction_no=:transactionid,uploaded_on=:lastupdatedate where cbo_id =:shg_id and mtg_no=:mtg_no")
    fun updateMeetinguploadStatus(
        shg_id: Long?,
        mtg_no: Int,
        transactionid: String,
        lastupdatedate: Long,
        checker_remarks:String
    )

    @Query("Update shg_mtg set checker_remarks=:checker_remarks where transaction_no=:transaction_id")
    fun updatedTransactionRemarks(checker_remarks:String,transaction_id:String)

    @Query("Select * from shg_mtg where cbo_id=:cbo_id")
    fun getMeetingStatusRemarks(cbo_id: Long):List<DtmtgEntity>

    @Query("Select withdrawn_cash from shg_mtg where cbo_id=:cbo_id and mtg_no=:mtgno")
    fun getCashInTransit(cbo_id: Long,mtgno: Int):Int

    @Query("update shg_mtg set withdrawn_cash=:cashInTransit where cbo_id=:cbo_id and mtg_no=:mtgno")
    fun updateCashInTransit(cashInTransit:Int,cbo_id: Long,mtgno: Int)

    @Query("Select count(*) from shg_mtg where cbo_id=:shgId and approval_status in(1,3)")
    fun getNonApprovedShgsCount(shgId:Long):Int

    @Query("Select count(*) from shg_mtg where cbo_id=:shgId and is_edited=1")
    fun getNonSynchedShgsCount(shgId:Long):Int

    @Query("Select approval_status from shg_mtg where cbo_id=:cboid and mtg_no=:mtgno")
    fun getMeetingStatus(cboid: Long,mtgno: Int):Int

    @Query("Update shg_mtg set transaction_status=:transaction_status where transaction_no=:transaction_id")
    fun updatedTransactionstatus(transaction_status:String,transaction_id:String)
}