package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.api.model.EcScModelJoindata
import com.microware.cdfi.api.vomodel.VoECMemberDataModel
import com.microware.cdfi.entity.*

@Dao
interface  ExecutiveMemberDao{

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertExectiveDetail(executiveEntity: List<Executive_memberEntity>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertExectiveData(executiveEntity : Executive_memberEntity?)

    @Query("Select * from executive_member where guid=:guid")
    fun getExecutivedetaildata(guid: String?): List<Executive_memberEntity>?

    @Query("Select * from executive_member where cbo_guid=:cbo_guid  and is_active=1 and status=1 group by ec_cbo_code")
    fun getExecutiveList(cbo_guid: String?): LiveData<List<Executive_memberEntity>>?

    @Query("Select * from executive_member where cbo_guid=:cbo_guid and ec_cbo_code =:ec_cbo_code and is_active=1 and status=1")
    fun getExecutiveMemberbyCode(cbo_guid:String,ec_cbo_code:Long):List<Executive_memberEntity>?

    @Query("Select count(*) from executive_member where cbo_guid=:cbo_guid and ec_cbo_code =:ec_cbo_code and is_active=1 and status=1")
    fun getExecutive_count(cbo_guid:String,ec_cbo_code:Long):Int

    @Query("Update executive_member set is_signatory=:isSignatory,signatory_joining_date=:signatory_joining_date,signatory_leaving_date=:signatory_leaving_date,is_complete=:is_complete,cbo_id=:cbo_id,cbo_guid=:cbo_guid,ec_cbo_code=:ec_cbo_code,ec_cbo_id=:ec_cbo_id,ec_member_code=:ec_member_code,designation=:designation,joining_date=:joining_date,leaving_date=:leaving_date,status=:status,is_active=:is_active,entry_source=:entry_source,is_edited=:is_edited,updated_date=:updated_date,updated_by=:updated_by where guid=:guid")
    fun updateExecutiveMember(cbo_id:Long?,
                              cbo_guid:String?,
                              guid:String,
                              ec_cbo_code:Long?,
                              ec_cbo_id:Long?,
                              ec_member_code:Long?,
                              designation:Int?,
                              joining_date:Long?,
                              leaving_date:Long?,
                              status:Int?,
                              is_active:Int?,
                              entry_source:Int?,
                              is_edited:Int?,
                              updated_date:Long?,
                              updated_by:String?,
                              is_complete:Int,
                              isSignatory:Int?,
                              signatory_joining_date:Long?,
                              signatory_leaving_date:Long?)

    @Query("Select mem.member_id as member_cd,ec.joining_date as join_date,mem.member_name as member_name,mem.shg_name as shg_name,mem.member_guid as member_guid from executive_member ec inner join tbl_VO_Shg_Member mem on ec.ec_member_code = mem.member_id where ec.cbo_guid =:cbo_guid order by ec.designation")
    fun getExecutiveMemberList(cbo_guid: String?): List<EcScModelJoindata>?

    @Query("Select mem.member_id as member_cd,ec.joining_date as join_date,mem.member_name as member_name,mem.shg_name as shg_name,mem.member_guid as member_guid from executive_member ec inner join tbl_clf_vo_member mem on ec.ec_member_code = mem.member_id where ec.cbo_guid =:cbo_guid order by ec.designation")
    fun getCLFExecutiveMemberList(cbo_guid: String?): List<EcScModelJoindata>?

    @Query("Select mem.member_id as member_cd,ec.joining_date as join_date,mem.member_name as member_name,mem.shg_name as shg_name,mem.member_guid as member_guid from executive_member ec inner join tbl_clf_vo_member mem on ec.ec_member_code = mem.member_id where ec.cbo_guid =:cbo_guid order by ec.designation")
    fun getECMemberList(cbo_guid: String?): List<EcScModelJoindata>?

    @Query("Select *  from tbl_VoShgMemberPhone where member_id =:member_id")
    fun getMemberPhone(member_id:Long):List<VoShgMemberPhoneEntity>

    @Query("Select count(*) FROM executive_member where cbo_guid=:guid and is_active=1 and status=1")
    fun getcount(guid: String?): Int

    @Query("Select count(*) from executive_member where cbo_guid=:cbo_guid and ec_member_code =:ec_member_code and is_active=1 and status=1")
    fun getECMember_count(cbo_guid:String,ec_member_code:Long):Int

    @Query("Select * from executive_member where is_edited=1")
    fun getPendingExecutiveMember():List<Executive_memberEntity>?

    @Query("update executive_member set is_edited =0,last_uploaded_date=:lastupdatedate where is_edited=1")
    fun updateuploadStatus(
        lastupdatedate: Long
    )

    @Query("Delete from executive_member where guid=:ec_guid")
    fun deleteRecord(ec_guid:String?)

    @Query("Delete from tbl_subcommitee_member where ec_member_guid=:ec_guid")
    fun deleteSCRecord(ec_guid:String?)

    @Query("Update executive_member set is_active=0,is_edited=1,updated_date=:updated_date,updated_by=:updated_by where guid=:ec_guid")
    fun deleteEcMemeber(ec_guid:String?,updated_date: Long?,updated_by: String?)

    @Query("Update tbl_subcommitee_member set is_active=0,is_edited=1,updated_date=:updated_date where ec_member_guid=:ec_guid")
    fun deleteScMemeber(ec_guid:String?,updated_date: Long?)

    @Query("Select count(*) from executive_member where ec_cbo_code=:ec_cbo_code and is_active=1 and status=1")
    fun getCboMemberCount(ec_cbo_code:Long):Int

    @Query("Select count(*) from executive_member where cbo_guid =:fedrationGuid and designation=:designation and is_active=1 and status=1")
    fun getDesignationCount(designation:Int,fedrationGuid:String):Int

    @Query("Select count(*) from tbl_subcommitee_member where ec_member_guid=:memberGuid and ec_member_code =:ec_member_code and is_active=1 and status=1")
    fun getScMember_count(memberGuid:String,ec_member_code:Long):Int

    @Query("update executive_member set is_edited =0,last_uploaded_date=:lastupdatedate where guid=:ec_guid and is_edited=1")
    fun updateEcuploaddata(
        lastupdatedate: Long,
        ec_guid:String
    )

    @Query("Delete from tbl_mapped_vo_shg where vo_guid=:federationGuid")
    fun deleteVOMappingdata(federationGuid:String)

    @Query("Delete from tbl_VO_Shg_Member where vo_guid=:federationGuid")
    fun deleteVOShgMember(federationGuid:String)

    @Query("Delete from tbl_VoShgMemberPhone where member_guid in(Select member_guid from tbl_VO_Shg_Member where vo_guid=:federationGuid)")
    fun deleteVOSHGMemberPhone(federationGuid:String)

    @Query("Delete from tbl_mapped_clf_vo where cbo_guid=:federationGuid")
    fun deleteClfMappingdata(federationGuid:String)

    @Query("Delete from tbl_clf_vo_member where clf_guid=:federationGuid")
    fun deleteClfVOMember(federationGuid:String)

    @Query("Delete from tbl_VoShgMemberPhone where member_guid in (Select member_guid from tbl_clf_vo_member where clf_guid=:federationGuid)")
    fun deleteClfVOMemberPhone(federationGuid:String)

    @Query("SELECT COUNT ( DISTINCT ec_cbo_code ) from executive_member where cbo_id=:cbo_id and is_active=1 and status=1")
    fun getDistinctCboCount(cbo_id: Long?):Int

    @Query("Select count(*) from executive_member where cbo_guid =:fedrationGuid and designation in (:designation) and is_active=1 and status=1")
    fun getOBCount(designation:List<Int>,fedrationGuid:String):Int

    @Query("Select a.designation,a.ec_member_code,a.ec_cbo_code,a.cbo_id,b.member_name,b.member_guid,b.member_id,b.cbo_guid,c.EC1,c.EC2,c.EC3,c.EC4,c.EC5,c.attendanceOther from executive_member a inner join tbl_VO_Shg_Member b on a.ec_member_code=b.member_id inner join vo_mtg_det c on a.ec_cbo_code = c.memId where a.ec_cbo_code =:ec_cbo_code and c.mtgNo=:mtgNo and a.designation IN(:designation) order by a.designation")
    fun getAllExecutivedetaildata1(
        ec_cbo_code: Long,
        mtgNo:Int,
        designation: List<Int>
    ): MutableList<VoECMemberDataModel>?

}

