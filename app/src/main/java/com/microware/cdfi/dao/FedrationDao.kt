package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.FederationEntity
import com.microware.cdfi.entity.SHGEntity

@Dao
interface FedrationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFedrationDetail(fedrationEntity:  List<FederationEntity>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFedrationDetailData(fedrationEntity : FederationEntity?)

    @Query("Select * from federation_profile where cbo_type =:cboType and is_active=1 order by federation_name")
    fun getFedrationAlldata(cboType:Int): LiveData<List<FederationEntity>>?

    @Query("Select * FROM federation_profile where village_id=:village_id and cbo_type=:cboType and is_active=1 order by federation_name")
    fun getFederation_by_village(village_id:Int,cboType:Int): LiveData<List<FederationEntity>>?

    @Query("Select * from federation_profile Where guid=:federation_guid")
    fun getFedrationDetaildata(federation_guid: String?): LiveData<List<FederationEntity>>?

    @Query("Select * from federation_profile Where guid=:federation_guid")
    fun getFedrationdata(federation_guid: String?): List<FederationEntity>?

    @Query("Select count(*) from federation_profile where cbo_type=:cboType and is_active=1")
    fun getVoCount(cboType:Int):Int

    @Query("DELETE From member_address")
    fun deleteAll()

    @Query("Update federation_profile set activation_status=:activationStatus,promoter_code=:promoter_code,federation_resolution=:imageName,approve_status=:approve_status,promoter_name=:promoter_name,is_complete=:is_completed,user_id=:user_id,promoted_by=:promotedby,cooption_date=:cooption_date,meber_cbo_count=:meber_cbo_count,is_financial_intermediation=:is_financial_intermediation,status=:status,election_tenure=:election_tenure,is_voluntary_saving=:is_volutary_saving,savings_interest=:savings_interest,voluntary_savings_interest=:voluntary_savings_interest,primary_activity=:primary_activity,secondary_activity=:secondary_activity,tertiary_activity=:tertiary_activity,bookkeeper_identified=:bookkeeper_identified,bookkeeper_name=:bookkeeper_name,bookkeeper_mobile=:bookkeeper_mobile,saving_frequency=:saving_frequency,panchayat_id=:panchayat_id,village_id=:village_id,federation_name=:federation_name,federation_name_hindi=:federation_name_hindi,federation_name_local=:federation_name_local,federation_name_short=:federation_name_short, federation_formation_date=:federation_formation_date,  federation_revival_date=:federation_revival_date,   meeting_frequency=:meeting_frequency,meeting_frequency_value=:meeting_frequency_value, meeting_on=:meeting_on,month_comp_saving=:month_comp_saving,is_bankaccount=:is_bankaccount,parent_cbo_code=:parent_cbo_code,parent_cbo_type=:parent_cbo_type,is_active=:is_active,dedupl_status=:dedupl_status,entry_source=:device,updated_date=:updated_date,updated_by=:updated_by,is_edited =:is_edited where guid=:federation_guid")
    fun updateBasicDetail(
        federation_guid: String,
        panchayat_id: Int?,
        village_id: Int?,
        federation_name: String,
        federation_name_hindi: String,
        federation_name_local: String,
        federation_name_short: String,
        federation_formation_date: Long,
        federation_revival_date: Long,
        meeting_frequency: Int,
        meeting_frequency_value: Int,
        meeting_on: Int,
        month_comp_saving: Int,
        is_bankaccount: Int,
        parent_cbo_code: Int,
        parent_cbo_type: Int,
        is_active: Int,
        dedupl_status: Int,
        device:Int,
        activationStatus:Int,
        updated_date: Long,
        updated_by: String,
        saving_frequency:Int,
        is_volutary_saving: Int?,
        savings_interest: Double,
        voluntary_savings_interest: Double,
        primary_activity: Int,
        secondary_activity: Int,
        tertiary_activity: Int,
        bookkeeper_identified: Int,
        bookkeeper_name: String,
        bookkeeper_mobile: String,
        election_tenure: Int,
        status: Int,
        is_financial_intermediation: Int,
        meber_cbo_count: Int,
        cooption_date:Long,
        promotedby:Int,
        promoter_code:Int,
        promoter_name:String,
        user_id:String,
        is_completed:Int,
        is_edited:Int,
        approve_status:Int,
        imageName: String
    )

    @Query("update federation_profile set is_upload=1,is_edited =0,last_uploaded_date=:lastupdatedate where guid =:guid")
    fun updateuploadStatus(
        guid: String,
        lastupdatedate: Long
    )

    @Query("update federation_profile set approve_status=1,dedupl_status=1,is_edited =1 where guid =:guid")
    fun updatededup(
        guid: String )

    @Query("Select * FROM federation_profile where is_edited=1 and cbo_type=:cboType")
    fun getAllFederationdata(cboType:Int): List<FederationEntity>?

    @Query("Select count(*) from federation_profile")
    fun getallVoCount(): Int

    @Query("Select count(*) from federation_profile where activation_status in(0,1,4,5)")
    fun getallVoPendingCount(): Int

    @Query("Select count(*) from federation_profile where status=1 and activation_status=2")
    fun getallVoActiveCount(): Int

    @Query("update federation_profile set is_upload=0,is_edited =1 where guid =:guid")
    fun updateisedit(guid: String )

    @Query("Select is_complete from federation_profile where guid=:guid")
    fun getIsCompleteValue(guid:String?):Int

    @Query("Select count(*) from cbo_phone_details where cbo_guid=:guid  and is_active=1 and is_complete=1")
    fun getPhoneCount(guid:String?):Int

    @Query("Select  count(*) from executive_member where cbo_guid=:guid and is_active=1 and status=1")
    fun getEcMemberCount(guid:String?):Int

    @Query("Select  count(*) from cbo_bank_details where cbo_guid=:guid  and is_active=1 and status=1 and is_complete=1")
    fun getBankCount(guid:String?):Int

    @Query("Select  count(*) from addresses_details where cbo_guid=:guid  and is_active=1 and status=1 and is_complete=1")
    fun getAddressCount(guid:String?):Int

    @Query("Select  count(*) from tbl_subcommittee where cbo_guid=:guid  and is_active=1 and status=1  and is_complete=1")
    fun getScCount(guid:String?):Int

    @Query("Select  count(*) from cbo_kyc_details where cbo_guid=:guid  and is_active=1 and status=1 and is_complete=1")
    fun getKycCount(guid:String?):Int

    @Query("Select  count(*) from tbl_mapped_vo_shg where vo_guid=:guid")
    fun getMappedShgCount(guid:String?):Int

    @Query("Select  count(*) from tbl_mapped_clf_vo where cbo_guid=:guid")
    fun getMappedVoCount(guid:String?):Int

    @Query("update federation_profile set is_upload = 0 ,federation_code=:code,approve_status=:approve_status,activation_status=:activationStatus,checker_remark =:checker_remarks where guid =:guid")
    fun updateFedrationDedupStatus(
        guid: String,
        code: String,
        activationStatus: Int,
        approve_status: Int,
        checker_remarks: String)

    @Query("update federation_profile set is_edited=:is_edited where guid =:guid")
    fun updateFedrationEditFlag(is_edited: Int,guid: String?)

    @Query("Select max(last_uploaded_date) FROM federation_profile")
    fun getlastdate(): Long

    @Query("Select count(*) from federation_profile where cbo_type=:cboType and activation_status in(0,1,4,5) and is_active=1")
    fun getVoPendingCount(cboType:Int): Int

    @Query("Select count(*) from federation_profile where cbo_type=:cboType and status=1 and activation_status=2 and is_active=1")
    fun getVoActiveCount(cboType:Int): Int

    @Query("update federation_profile set federation_id=:cbo_id where guid =:guid")
    fun updateFedrationCode(cbo_id: Long,guid: String?)

    @Query("update federation_profile set approve_status=0 where transaction_id=:transactionid and approve_status=1 and is_edited =0")
    fun updatetransactionstatus(transactionid: String )

    @Query("DELETE FROM federation_profile where guid=:federationGUID")
    fun deleteFederationByGuid(federationGUID: String?)

    @Query("DELETE FROM cbo_bank_details where cbo_guid=:federationGUID")
    fun deleteBankByfederationGUID(federationGUID: String?)

    @Query("DELETE FROM cbo_phone_details where cbo_guid=:federationGUID")
    fun deletePhoneByfederationGUID(federationGUID: String?)

    @Query("DELETE FROM addresses_details where cbo_guid=:federationGUID")
    fun deleteAddressByfederationGUID(federationGUID: String?)

    @Query("DELETE FROM executive_member where cbo_guid=:federationGUID")
    fun deleteEcByfederationGUID(federationGUID: String?)


    @Query("DELETE FROM tbl_subcommittee where cbo_guid=:federationGUID")
    fun deleteScByfederationGUID(federationGUID: String?)

    @Query("DELETE FROM tbl_subcommitee_member where subcommitee_guid in (Select subcommitee_guid FROM tbl_subcommittee where cbo_guid=:federationGUID)")
    fun deleteScMemberByfederationGUID(federationGUID: String?)

    @Query("Update federation_profile set is_active=0,is_edited=1 Where guid=:federationGUID")
    fun deleteFederationDataByGuid(federationGUID: String?)

    @Query("Update cbo_bank_details set is_active=0,is_edited=1 Where cbo_guid=:federationGUID")
    fun deleteBankDataByfederationGUID(federationGUID: String?)

    @Query("Update cbo_phone_details set is_active=0,is_edited=1 Where cbo_guid=:federationGUID")
    fun deletePhoneDataByfederationGUID(federationGUID: String?)

    @Query("Update addresses_details set is_active=0,is_edited=1 Where cbo_guid=:federationGUID")
    fun deleteAddressDataByfederationGUID(federationGUID: String?)

    @Query("Update executive_member set is_active=0,is_edited=1 Where cbo_guid=:federationGUID")
    fun deleteEcDataByfederationGUID(federationGUID: String?)

    @Query("Update tbl_subcommittee set is_active=0,is_edited=1 Where cbo_guid=:federationGUID")
    fun deleteScDataByfederationGUID(federationGUID: String?)

    @Query("Update tbl_subcommittee set is_active=0,is_edited=1 where subcommitee_guid in (Select subcommitee_guid FROM tbl_subcommittee where cbo_guid=:federationGUID)")
    fun deleteScMemberDataByfederationGUID(federationGUID: String?)

    @Query("update federation_profile set federation_id=:federation_id,is_edited =1 where guid =:guid")
    fun updateFedrationId(guid: String?,federation_id:Long?)

    @Query("Select count(*) from executive_member where ec_cbo_code=:shg_code and cbo_guid=:cbo_guid and is_active=1 and status=1")
    fun getEcCount(shg_code:Long,cbo_guid:String):Int

    @Query("Select ifsc_code from cbo_bank_details where  ifsc_code not in(Select ifsc_code from bank_branch_master)")
    fun getIfscCode():List<String>

    @Query("Select is_financial_intermediation from federation_profile Where guid=:federationGUID")
    fun getFI(federationGUID: String?):Int

    @Query("select count(*) from tbl_subcommittee tblcommitee  left join (select count(*) as total,subcommitee_guid from tbl_subcommitee_member group by subcommitee_guid) as member on member.subcommitee_guid=tblcommitee.subcommitee_guid where (total<2 or total is null) and cbo_guid=:federationGuid  and status=1 and is_active=1")
    fun getSCMemberCount(federationGuid:String):Int

    @Query("Select count(*) from executive_member where is_signatory=1 and cbo_guid=:cbo_guid and is_active=1 and status=1")
    fun getSignatoryCount(cbo_guid:String):Int

    @Query("Select * FROM federation_profile where panchayat_id=:panchayat_id and cbo_type=:cboType and is_active=1 order by federation_name")
    fun getFederation_by_PanchayatCode(panchayat_id:Int,cboType:Int): LiveData<List<FederationEntity>>?

    @Query("Select * FROM federation_profile where block_id=:block_id and cbo_type=:cboType and is_active=1 order by federation_name")
    fun getClfDataByBlock(block_id:Int,cboType:Int): LiveData<List<FederationEntity>>?

    @Query("Select count(*) from federation_profile where cbo_type=:cboType and panchayat_id=:panchayat_id and activation_status in(0,1,4,5) and is_active=1")
    fun getVoPendingCount(panchayat_id:Int,cboType:Int): Int

    @Query("Select count(*) from federation_profile where cbo_type=:cboType and block_id=:block_id and activation_status in(0,1,4,5) and is_active=1")
    fun getCLFPendingCount(block_id:Int,cboType:Int): Int

    @Query("Select count(*) from federation_profile where panchayat_id=:panchayat_id and cbo_type=:cboType and is_active=1")
    fun getVoCount(panchayat_id:Int,cboType:Int): Int

    @Query("Select count(*) from federation_profile where block_id=:block_id and cbo_type=:cboType and is_active=1")
    fun getCLFCount(block_id:Int,cboType:Int): Int

    @Query("Select count(*) from federation_profile where cbo_type=:cboType and panchayat_id=:panchayat_id and status=1 and activation_status=2 and is_active=1")
    fun getVoActiveCount(panchayat_id:Int,cboType:Int): Int

    @Query("Select count(*) from federation_profile where cbo_type=:cboType and block_id=:block_id and status=1 and activation_status=2 and is_active=1")
    fun getCLFActiveCount(block_id:Int,cboType:Int): Int
}