package com.microware.cdfi.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.api.meetinguploadmodel.ShgFinanceTransactionDetailMemberData
import com.microware.cdfi.entity.FinancialTransactionsMemEntity

@Dao
interface FinancialTransactionsMemDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMtgFinTxnAllData(dtFinTxnEntity: List<FinancialTransactionsMemEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMtgFinTxnData(dtFinTxnEntity: FinancialTransactionsMemEntity)

    @Query("Select * from shg_fin_txn_det_mem Where mtg_guid=:mtg_guid and cbo_id=:cbo_id and mem_id=:mem_id and mtg_no=:mtg_no and auid=:auid")
    fun getMtgFinTxndata(
        mtg_guid: String, cbo_id: Long,
        mem_id: Long,
        mtg_no: Int,
        auid:Int
    ): List<FinancialTransactionsMemEntity>?

    @Query("Delete from shg_fin_txn_det_mem where mtg_guid=:mtg_guid")
    fun deleteRecord(mtg_guid: String)

    @Query("DELETE From shg_fin_txn_det_mem")
    fun deleteAll()

    @Query("Update shg_fin_txn_det_mem set mem_id=:mem_id,bank_code=:bank_code,type=:type,amount=:amount,trans_date=:trans_date,date_realisation=:date_realisation,mode_payment=:mode_payment,transaction_no=:transaction_no,updated_by=:updatedby,updated_on=:updatedon where mtg_guid=:mtg_guid and cbo_id=:cbo_id and mem_id=:mem_id and mtg_no=:mtg_no and auid=:auid")
    fun updateShareCapitalDetail(
        mtg_guid: String,
        cbo_id: Long,
        mem_id: Long,
        mtg_no: Int,
        auid: Int,
        bank_code: String?,
        type: String?,
        amount: Int?,
        trans_date: Long?,
        date_realisation: Long?,
        mode_payment: Int?,
        transaction_no: String?,
        updatedby: String?,
        updatedon: Long?
    )

    @Query("Select sum(amount) from shg_fin_txn_det_mem Where mtg_guid=:mtg_guid and cbo_id=:cbo_id and mtg_no=:mtg_no and auid=:auid")
    fun getTotalAmountByReceiptType(mtg_guid: String, cbo_id: Long,mtg_no: Int,auid:Int): Int

    @Query("Select sum(amount) from shg_fin_txn_det_mem Where cbo_id=:cbo_id and mtg_no=:mtg_no and mode_payment=:modePayment and type=:type")
    fun getIncomeAmount(cbo_id: Long,mtg_no: Int,modePayment:Int,type:String): Int


    @Query("Select sum(amount) from shg_fin_txn_det_mem Where cbo_id=:cbo_id and mtg_no=:mtg_no and type=:type")
    fun getMemberIncomeAmount(cbo_id: Long,mtg_no: Int,type:String): Int

    @Query("Select sum(amount) from shg_fin_txn_det_mem Where cbo_id=:cbo_id and mtg_no=:mtg_no and type=:type and (mode_payment= 2 or mode_payment = 3)")
    fun getBankAmount(cbo_id: Long,mtg_no: Int,type:String): Int

    @Query("Select sum(amount) from shg_fin_txn_det_mem Where cbo_id=:cbo_id and mtg_no=:mtg_no and type=:type and (mode_payment= 2 or mode_payment = 3) and bank_code=:bank_code")
    fun getBankCodeAmount(cbo_id: Long,mtg_no: Int,bank_code: String?,type:String): Int

    @Query("Select sum(amount) from shg_fin_txn_det_mem Where cbo_id=:cbo_id and mtg_no=:mtg_no and bank_code=:bank_code and type=:type")
    fun getFinancialTransactionsMemAmount(cbo_id: Long,mtg_no: Int,bank_code:String,type:String): Int

    @Query("select * from shg_fin_txn_det_mem where mtg_no=:mtg_no and cbo_id=:cbo_id and mem_id=:mem_id")
    fun getUploadData(mtg_no: Int,cbo_id: Long,mem_id: Long) : List<FinancialTransactionsMemEntity>

    @Query("Delete from shg_fin_txn_det_mem where mtg_no=:mtg_no and cbo_id=:cbo_id")
    fun deleteFinancialTransactionMemberDetailData(mtg_no: Int,cbo_id: Long)

    @Query("Delete from shg_fin_txn_det_mem where cbo_id=:cbo_id")
    fun deleteFinancialTransactionMemberDetailDataByCboId(cbo_id: Long)

    @Query("Select sum(amount) from shg_fin_txn_det_mem Where cbo_id=:cbo_id and mtg_no=:mtg_no and type=:type")
    fun getMemberReceiptAmount(cbo_id: Long,mtg_no: Int,type:String): Int

    @Query("select * from shg_fin_txn_det_mem where mtg_no=:mtg_no and cbo_id=:cbo_id")
    fun getMemberFinancialTxnData(mtg_no: Int,cbo_id: Long) : List<FinancialTransactionsMemEntity>
}