package com.microware.cdfi.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.FundTypeEntity

@Dao
interface FundTypeDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFundslist(dtLoanGpEntity: List<FundTypeEntity>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFundsData(dtLoanGpEntity: FundTypeEntity)

    @Query("Select * from mst_fund_type where language_id=:langID order by sequence")
    fun getFundslist(langID:String): List<FundTypeEntity>?

    @Query("DELETE From mst_fund_type")
    fun deleteFundType()

    @Query("Select fund_type_short_name from mst_fund_type where language_id=:langID and fund_type_id=:fundTypeID")
    fun getFundName(fundTypeID:Int,langID: String):String?



}