package com.microware.cdfi.dao

import androidx.room.*
import com.microware.cdfi.entity.FundingEntity

@Dao
interface FundingAgencyDao {

    @Insert
    fun insertAgency(fundingEntity: FundingEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllAgency(fundingEntity: List<FundingEntity>?)

    @Update
    fun updateAgency(fundingEntity: FundingEntity)

    @Query("Delete from tblfunding_agency_master")
    fun deleteAllAgency()

    @Query("Select * from tblfunding_agency_master where agency_name_language_id=:languageId")
    fun getfundingAgency(languageId:String):List<FundingEntity>
}