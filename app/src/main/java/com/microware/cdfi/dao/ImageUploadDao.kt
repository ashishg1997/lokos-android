package com.microware.miracle.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.microware.cdfi.entity.DistrictEntity
import com.microware.cdfi.entity.ImageuploadEntity


@Dao
interface ImageUploadDao {


    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertimage(imageEntity: ImageuploadEntity?)



    @Query("Select * from image_upload where isupload=0")
    fun getimage() :List<ImageuploadEntity>

    @Query("Select * from image_upload where isupload=0 and shg_guid =:shg_guid")
    fun getimage_by_guid(shg_guid:String) :List<ImageuploadEntity>

}