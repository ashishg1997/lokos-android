package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.api.meetingmodel.BankTransactionModel
import com.microware.cdfi.api.meetinguploadmodel.ShgFinanceTransactionDetailGroupListData
import com.microware.cdfi.entity.ShgFinancialTxnDetailEntity

@Dao
interface IncomeandExpenditureDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertIncomeAndExpenditureAllData(shgFinancialTxnDetailEntity: List<ShgFinancialTxnDetailEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertIncomeAndExpenditureData(shgFinancialTxnDetailEntity: ShgFinancialTxnDetailEntity)

    @Query("Select * from shg_fin_txn_det_grp Where mtg_guid=:mtg_guid and cbo_id=:cbo_id and mtg_no=:mtg_no and auid=:auid and amount_to_from=:amount_to_from")
    fun getIncomeAndExpendituredata(mtg_guid: String, cbo_id: Long, mtg_no: Int,auid:Int,amount_to_from:Int): List<ShgFinancialTxnDetailEntity>?

    @Query("Delete from shg_fin_txn_det_grp where mtg_guid=:mtg_guid")
    fun deleteRecord(mtg_guid: String)

    @Query("DELETE From shg_fin_txn_det_grp")
    fun deleteAll()

    @Query("update shg_fin_txn_det_grp set fund_type=:fund_type,amount_to_from=:amount_to_from,type=:type,amount=:amount,trans_date=:trans_date,date_realisation=:date_realisation,mode_payment=:mode_payment,bank_code=:bank_code,transaction_no=:transaction_no,updated_by=:updatedby,updated_on=:updatedon where mtg_guid=:mtg_guid and cbo_id=:cbo_id and mtg_no=:mtg_no and auid=:auid and amount_to_from=:amount_to_from")
    fun updateIncomeAndExpenditure(
        mtg_guid: String,
        cbo_id: Long,
        mtg_no: Int,
        auid: Int,
        fund_type: Int?,
        amount_to_from: Int?,
        type: String?,
        amount: Int?,
        trans_date: Long?,
        date_realisation: Long?,
        mode_payment: Int?,
        bank_code: String?,
        transaction_no: String?,
        updatedby: String?,
        updatedon: Long?
    )

    @Query("Select * from shg_fin_txn_det_grp Where mtg_guid=:mtg_guid and cbo_id=:cbo_id and mtg_no=:mtg_no and fund_type=:fund_type")
    fun getIncomeAndExpenditureAlldata(mtg_guid: String, cbo_id: Long, mtg_no: Int,fund_type:Int): LiveData<List<ShgFinancialTxnDetailEntity>?>

    @Query("Select * from shg_fin_txn_det_grp Where auid not IN(:auid) and mtg_guid=:mtg_guid and cbo_id=:cbo_id and mtg_no=:mtg_no and fund_type=:fund_type")
    fun getIncomeAndExpenditurenotinauid(auid:List<Int>,mtg_guid: String, cbo_id: Long, mtg_no: Int,fund_type:Int): LiveData<List<ShgFinancialTxnDetailEntity>?>

    @Query("Select sum(amount) from shg_fin_txn_det_grp Where mtg_guid=:mtg_guid and cbo_id=:cbo_id and mtg_no=:mtg_no and fund_type=:fund_type")
    fun getTotalIncomeAndExpenditureByFundType(mtg_guid: String, cbo_id: Long, mtg_no: Int,fund_type:Int): Int

    @Query("select sum(amount) from shg_fin_txn_det_grp where  mtg_no=:mtg_no and  cbo_id=:cbo_id and mode_payment=:modePayment and fund_type=:fund_type")
    fun getIncomeAndExpenditureAmount(cbo_id: Long,mtg_no: Int,modePayment:Int,fund_type: Int):Int

    @Query("select sum(amount) from shg_fin_txn_det_grp where auid not IN(:auid) and  mtg_no=:mtg_no and  cbo_id=:cbo_id and fund_type=:fund_type")
    fun getMemberIncomeAndExpenditureAmountnotinauid(auid:List<Int>,cbo_id: Long,mtg_no: Int,fund_type: Int):Int

    @Query("select sum(amount) from shg_fin_txn_det_grp where  mtg_no=:mtg_no and  cbo_id=:cbo_id and fund_type=:fund_type")
    fun getMemberIncomeAndExpenditureAmount(cbo_id: Long,mtg_no: Int,fund_type: Int):Int

    @Query("select sum(amount) from shg_fin_txn_det_grp where  mtg_no=:mtg_no and  cbo_id=:cbo_id and (mode_payment= 2 OR mode_payment = 3) and fund_type=:fund_type")
    fun getBankIncomeAndExpenditureAmount(cbo_id: Long,mtg_no: Int,fund_type: Int):Int

    @Query("select sum(amount) from shg_fin_txn_det_grp where  mtg_no=:mtg_no and  cbo_id=:cbo_id and (mode_payment= 2 OR mode_payment = 3) and fund_type=:fund_type and bank_code=:bank_code")
    fun getBankCodeIncomeAndExpenditureAmount(cbo_id: Long,mtg_no: Int,fund_type: Int,bank_code: String?):Int

    @Query("Select sum(amount) from shg_fin_txn_det_grp Where cbo_id=:cbo_id and mtg_no=:mtg_no and bank_code=:bank_code and fund_type=:fund_type")
    fun getShgTransactionsGrpAmount(cbo_id: Long,mtg_no: Int,bank_code:String,fund_type: Int): Int

    @Query("Select * from shg_fin_txn_det_grp Where mtg_guid=:mtg_guid and cbo_id=:cbo_id and mtg_no=:mtg_no and auid=:auid and type=:type and amount_to_from=:amount_to_from")
    fun getInvestmentdata(mtg_guid: String, cbo_id: Long, mtg_no: Int,auid:Int,type:String,amount_to_from: Int): List<ShgFinancialTxnDetailEntity>?

    @Query("Select sum(amount) from shg_fin_txn_det_grp Where cbo_id=:cbo_id and mtg_no=:mtg_no and auid in (:auid) and type=:type")
    fun getTotalInvestmentByMtgNum(cbo_id: Long, mtg_no: Int,auid:List<Int>,type:String): Int

    @Query("Select * from shg_fin_txn_det_grp Where cbo_id=:cbo_id and mtg_no=:mtg_no and auid in (:auid) and type=:type")
    fun getInvestmentListByMtgNum(cbo_id: Long, mtg_no: Int,auid:List<Int>,type:String): LiveData<List<ShgFinancialTxnDetailEntity>?>

    @Query("Select sum(amount) from shg_fin_txn_det_grp Where cbo_id=:cbo_id and mtg_no=:mtg_no and auid=:auid and type=:type and amount_to_from in(3,4)")
    fun getTotalInvestmentByAuid( mtg_no: Int,cbo_id: Long,auid:Int,type:String): Int

    @Query("Select * from shg_fin_txn_det_grp where mtg_no=:mtg_no and cbo_id=:cbo_id")
    fun getUploadListData(mtg_no : Int,cbo_id: Long) : List<ShgFinancialTxnDetailEntity>

    @Query("Delete from shg_fin_txn_det_grp where mtg_no=:mtg_no and cbo_id=:cbo_id")
    fun deleteGrpFinancialTxnDetailData(mtg_no:Int,cbo_id:Long)

    @Query("Delete from shg_fin_txn_det_grp where cbo_id=:cbo_id")
    fun deleteGrpFinancialTxnDetailDataByCboId(cbo_id:Long)

    @Query("Select count(*) from shg_fin_txn_det_grp where mtg_no=:mtg_no and cbo_id=:cbo_id and Auid in(47,48)")
    fun getAdjustmentCashCount(mtg_no:Int,cbo_id:Long):Int

    @Query("update shg_fin_txn_det_grp set auid=:auid,fund_type=:fund_type,amount_to_from=:amount_to_from,type=:type,amount=:amount,trans_date=:trans_date,date_realisation=:date_realisation,mode_payment=:mode_payment,bank_code=:bank_code,transaction_no=:transaction_no,updated_by=:updatedby,updated_on=:updatedon where cbo_id=:cbo_id and mtg_no=:mtg_no and auid in(47,48)")
    fun updateCashAdjustment(
        cbo_id: Long,
        mtg_no: Int,
        auid: Int,
        fund_type: Int?,
        amount_to_from: Int?,
        type: String?,
        amount: Int?,
        trans_date: Long?,
        date_realisation: Long?,
        mode_payment: Int?,
        bank_code: String?,
        transaction_no: String?,
        updatedby: String?,
        updatedon: Long?
    )


    @Query("Select * from shg_fin_txn_det_grp Where mtg_guid=:mtg_guid and cbo_id=:cbo_id and mtg_no=:mtg_no and auid=:auid")
    fun getSingleTransferedBankList(mtg_guid: String, cbo_id: Long, mtg_no: Int,auid:Int): List<ShgFinancialTxnDetailEntity>?

    @Query("Select * from shg_fin_txn_det_grp Where cbo_id=:cbo_id and mtg_no=:mtg_no and fund_type=:fund_type")
    fun getAllTransferedBankList(cbo_id: Long, mtg_no: Int, fund_type: Int): LiveData<List<ShgFinancialTxnDetailEntity>?>

    @Query("select sum(amount) from shg_fin_txn_det_grp Where cbo_id=:cbo_id and mtg_no=:mtg_no and auid in (:auid) and type=:type and bank_code=:bank_code")
    fun getBankCodeIncomeAndExpendituretransferAmount(cbo_id: Long,mtg_no: Int,bank_code: String?,auid:List<Int>,type:String):Int

    @Query("Select shg_fin_income.cbo_id as cbo_id,shg_fin_income.mtg_no as mtg_no,shg_fin_income.bank_code as bank_to,shg_fin_expenditure.bank_code as bank_from,shg_fin_income.amount as amount,shg_fin_income.trans_date as trans_date,shg_fin_income.auid as auid from (Select * from shg_fin_txn_det_grp where fund_type=:fund_type and type='OI' and cbo_id=:cbo_id and mtg_no=:mtg_no) as shg_fin_income  inner join (Select * from shg_fin_txn_det_grp where fund_type=:fund_type and type='OE' and cbo_id=:cbo_id and mtg_no=:mtg_no) as shg_fin_expenditure on shg_fin_income.cbo_id = shg_fin_expenditure.cbo_id and shg_fin_income.mtg_no = shg_fin_expenditure.mtg_no and shg_fin_income.amount = shg_fin_expenditure.amount and shg_fin_income.trans_date = shg_fin_expenditure.trans_date")
    fun getBankTransferList(cbo_id: Long, mtg_no: Int, fund_type: Int): LiveData<List<BankTransactionModel>?>

    @Query("select sum(amount) from shg_fin_txn_det_grp Where cbo_id=:cbo_id and mtg_no=:mtg_no and type=:type")
    fun getTotalIncoming_OutgoingAmount(cbo_id: Long,mtg_no: Int,type:String):Int
    @Query("Select sum(amount) from shg_fin_txn_det_grp Where cbo_id=:cbo_id and mtg_no=:mtg_no and type=:type and auid in (:auid)")
    fun getInvestmentAmount(cbo_id: Long,mtg_no: Int,type:String,auid:List<Int>): Int
    @Query("Select sum(amount) from shg_fin_txn_det_grp Where cbo_id=:cbo_id and mtg_no=:mtg_no and auid=:auid and type=:type and amount_to_from = 8")
    fun getTotalFDAmount( mtg_no: Int,cbo_id: Long,auid:Int,type:String): Int

}