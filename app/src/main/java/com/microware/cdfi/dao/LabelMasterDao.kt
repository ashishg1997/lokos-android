package com.microware.cdfi.dao

import androidx.room.*
import com.microware.cdfi.entity.LabelMasterEntity

@Dao
interface LabelMasterDao {

    @Insert
    fun insertLabelMaster(data: LabelMasterEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllLabel(data: List<LabelMasterEntity>?)

    @Update
    fun updateLabel(data: LabelMasterEntity)

    @Query("Delete from label_master")
    fun deleteAllLabel()

    @Query("Select text_value from label_master where keyValue=:keyval and language_id=:langId")
    fun getLabel(keyval:String,langId:String?):String


}