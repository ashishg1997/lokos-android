package com.microware.cdfi.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.api.meetingmodel.FundTypeModel
import com.microware.cdfi.entity.MstproductEntity

@Dao
interface LoanProductDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertproductlist(dtLoanGpEntity: List<MstproductEntity>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertData(dtLoanGpEntity: MstproductEntity)

    @Query("Select * from loanProductMasterList")
    fun getproductlist(): List<MstproductEntity>?

    @Query("DELETE From loanProductMasterList")
    fun deleteProducts()

    /*Select f.fund_type_id,f.fund_type from loanProductMasterList p inner join mst_fund_type f on p.fundType=f.fund_type_id where p.source=2 and p.recipent=1 order by f.fund_type*/
    @Query("Select f.fund_type_id as fundTypeId,f.fund_type_short_name as fundType,p.interestDefault,p.defaultPeriod,p.minPeriod,p.maxPeriod,p.minInterest,p.maxInterest,p.minAmount,p.maxAmount  from loanProductMasterList p inner join mst_fund_type f on p.fundType=f.fund_type_id where p.source=:source and p.recipent=:recipent order by f.fund_type")
    fun getFundTypeBySource_Receipt(source:Int?,recipent:Int?): List<FundTypeModel>?

    @Query("Select f.fund_type_id as fundTypeId,f.fund_type_short_name as fundType,p.interestDefault,p.defaultPeriod,p.minPeriod,p.maxPeriod,p.minInterest,p.maxInterest,p.minAmount,p.maxAmount from loanProductMasterList p inner join mst_fund_type f on p.fundType=f.fund_type_id where p.fundType=:typeId and  p.source=:source and p.recipent=:recipent order by f.fund_type")
    fun getFundTypeBySource_Receiptwithtypeid(source:Int?,recipent:Int?,typeId: Int?): List<FundTypeModel>?

    @Query("Select f.fund_type_short_name from loanProductMasterList p inner join mst_fund_type f on p.fundType=f.fund_type_id where p.source=:source and p.recipent=:recipent and f.fund_type_id=:typeId order by f.fund_type")
    fun getFundType(source:Int?,recipent:Int?,typeId:Int?): String



    @Query("Select f.fund_type_id as fundTypeId,f.fund_type_short_name as fundType,p.interestDefault,p.defaultPeriod from loanProductMasterList p inner join mst_fund_type f on p.fundType=f.fund_type_id where p.source=:source and p.recipent=:recipent and p.loanGrant = 1 order by f.fund_type")
    fun getFundTypeBySource(source:Int?,recipent:Int?): List<FundTypeModel>?
}