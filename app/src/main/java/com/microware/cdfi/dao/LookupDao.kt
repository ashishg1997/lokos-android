package com.microware.cdfi.dao

import androidx.room.*
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.entity.subcommitee_masterEntity

@Dao
interface LookupDao {


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertlookup(lookupEntity: List<LookupEntity>?)

    @Query("Select * FROM LookupMaster where key1=:key1 and language_id=:languageID order by sequence_no ")
    fun getlookup(key1:Int?,languageID:String?):List<LookupEntity>?

    @Query("Select * FROM LookupMaster where key1=:key1 and language_id=:languageID and lookup_code in(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15) order by sequence_no ")
    fun getlookupbycode_data(key1:Int?,languageID:String?):List<LookupEntity>?

    @Query("Select * FROM LookupMaster where key_code=:keycode and language_id=:languageID order by sequence_no ")
    fun getlookupfromkeycode(keycode: String?,languageID:String?):List<LookupEntity>?

    @Query("Delete from LookupMaster")
    fun delete()

    @Query("select  description from LookupMaster where key_code=:kyc_code and  lookup_code=:addressType order by sequence_no ")
    fun getAddressType(kyc_code: String, addressType: Int?): String?

    @Query("select  description from LookupMaster where key1=:flag and  language_id=:languageID and lookup_code=:keycode")
    fun getlookupValue(flag:Int?,languageID:String?,keycode:Int?): String?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLookupData(lookpupEntity : LookupEntity?)

    @Query("Select count(*) FROM LookupMaster where key1=:key1")
    fun getlookupCount(key1:Int?):Int

    @Query("Select * FROM LookupMaster where key1=:key1 and language_id=:languageID and lookup_code in(:code) order by sequence_no ")
    fun getlookupMasterdata(key1:Int?,languageID:String?,code:List<Int>):List<LookupEntity>?

    @Query("Select * FROM LookupMaster where key1=:key1 and language_id=:languageID  and lookup_code not in(:notincode)order by sequence_no ")
    fun getlookupnotin(key1:Int?,notincode:Int,languageID:String?):List<LookupEntity>?


    @Query("select  key_value from LookupMaster where key1=:key1 and language_id=:language_id and lookup_code=:lookup_code")
    fun getDestination(key1: Int, language_id: String?,lookup_code: Int?): String?



    @Query("Select * FROM LookupMaster where key1=:key1 and language_id=:languageID and lookup_code in(4,8) order by sequence_no ")
    fun getplaceOfInvestment(key1:Int?,languageID:String?):List<LookupEntity>?
}