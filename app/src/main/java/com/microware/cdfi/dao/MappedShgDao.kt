package com.microware.cdfi.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.MappedShgEntity
import com.microware.cdfi.entity.member_addressEntity

@Dao
interface MappedShgDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMapped_data(mappedEntity:  List<MappedShgEntity>?)

    @Query("Select * from tbl_mapped_vo_shg where vo_guid =:vo_guid")
    fun getShgNameList(vo_guid:String):List<MappedShgEntity>?

    @Query("Select * from tbl_mapped_vo_shg where vo_guid =:vo_guid and status=1 and settlement_status!=1")
    fun getmeetingShgList(vo_guid:String):List<MappedShgEntity>?

    @Query("Select shg_name from tbl_mapped_vo_shg where shg_id=:shg_id and vo_guid =:vo_guid")
    fun getShgName(shg_id:Long,vo_guid:String):String?


    @Query("update tbl_mapped_vo_shg set settlement_status=:settlement where shg_id=:memid")
    fun updateSettlementstatus(memid: Long,settlement: Int)


}