package com.microware.cdfi.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.MappedShgEntity
import com.microware.cdfi.entity.MappedVoEntity
import com.microware.cdfi.entity.member_addressEntity

@Dao
interface MappedVoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMapped_data(mappedEntity:  List<MappedVoEntity>?)

    @Query("Select * from tbl_mapped_clf_vo where cbo_guid =:clf_guid")
    fun getVoNameList(clf_guid:String):List<MappedVoEntity>?
}