package com.microware.cdfi.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.Bank_branchEntity

@Dao
interface MasterBankBranchDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertbranch(branchentity: List<Bank_branchEntity>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertbranch(branchentity: Bank_branchEntity?)

    @Query("Select * from bank_branch_master where bank_id=:bankCode order by bank_branch_name")
    fun getBankBranch(bankCode:Int?): List<Bank_branchEntity>

    @Query("Select * from bank_branch_master where ifsc_code=:ifsccode order by bank_branch_name")
    fun BankBranchlistifsc(ifsccode:String): List<Bank_branchEntity>
    @Query("Select bank_branch_name from bank_branch_master where bank_branch_id=:bank_branch_code ")
    fun getBranchname(bank_branch_code:Int?): String
}