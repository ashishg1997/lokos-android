package com.microware.cdfi.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.api.meetingmodel.BankListModel
import com.microware.cdfi.entity.BankEntity

@Dao
interface MasterBankDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertbank(bankentity: List<BankEntity>?)

    @Query("Select * from bank_master order by bank_name")
    fun getBank(): List<BankEntity>

    @Query("Select * from bank_master where bank_id=:bankcode")
    fun getBankwithcode(bankcode:String): List<BankEntity>

    @Query("Select bank_name from bank_master where bank_id =:bank_id ")
    fun getBankName(bank_id: Int): String?

    @Query("Select bank_account_len from bank_master  where bank_id =:bank_id ")
    fun getAccountLength(bank_id: Int): String?

    @Query("SELECT  a.bank_id,a.bank_name,a.bank_code,b.account_no from bank_master a INNER join cbo_bank_details b on a.bank_id=b.bank_id where b.cbo_guid=:cboGuid")
    fun getBankMaster1(cboGuid:String): List<BankListModel>?

}