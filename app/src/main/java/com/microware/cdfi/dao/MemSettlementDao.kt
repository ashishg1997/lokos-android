package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.entity.DtMemSettlementEntity

@Dao
interface MemSettlementDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSettlement(dtMemSettlementEntity:  List<DtMemSettlementEntity>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSettlementData(dtMemSettlementEntity : DtMemSettlementEntity?)

    @Query("Select * from shg_mem_settlement Where mem_id=:memid and mtg_no=:mtgno")
    fun getSettlementdata(memid: Long?,mtgno:Int): List<DtMemSettlementEntity>?


    @Query("Delete from shg_mem_settlement where mtg_no=:mtgno")
    fun deleteRecord(mtgno: Int)

    @Query("DELETE From shg_mem_settlement")
    fun deleteAll()

    @Query("Select sum(paid_amt) from shg_mem_settlement where mtg_no=:mtgno and cbo_id=:cbo_id")
    fun getTotalSettelMentAmt(mtgno:Int,cbo_id:Long):Int
}