package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.member_addressEntity

@Dao
interface MemberAddressDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAddressDetail(addressEntity:  List<member_addressEntity>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAddressDetailData(addressEntity : member_addressEntity?)

    @Query("Select * from member_address Where address_guid=:address_guid")
    fun getAddressdetaildata(address_guid: String?): List<member_addressEntity>?

    @Query("Select * from member_address Where member_guid=:member_guid and is_active=1")
    fun getAddressdata(member_guid: String?): LiveData<List<member_addressEntity>>?


  @Query("Select * from member_address Where member_guid=:member_guid and is_edited=1")
    fun getAddressdatalist(member_guid: String?): List<member_addressEntity>?


  @Query("Select * from member_address Where member_guid=:member_guid and is_active=1 and is_complete=1")
    fun getAddressdatalistcount(member_guid: String?): List<member_addressEntity>?


    @Query("DELETE From member_address")
    fun deleteAll()

    @Query("Update member_address set is_complete=:isCompleted,address_type=:address_type,address_line1=:address_line1,address_line2=:address_line2,village_id=:village,panchayat_id=:panchayat_id,landmark=:landmark, postal_code=:postal_code,is_Active =:is_Active,  state_id=:state,district_id=:District,is_edited=1,updated_date=:updated_date,updated_by=:updated_by,address_location=:addresslocation where address_guid=:address_guid")
    fun updateAddressDetail(
        address_guid: String,
        address_type: Int,
        address_line1: String,
        address_line2: String,
        village: Int,
        panchayat_id: Int,
        landmark: String,
        state: Int,
        District: Int,
        postal_code: String,
        is_Active: Int,
        updated_date: Long,
        updated_by: String,
        addresslocation: Int,
        isCompleted: Int
    )

    @Query("Update member_address set  is_edited=0,last_uploaded_date=:last_uploaded_date where member_guid=:memberguid")
    fun updateisedit( memberguid: String,last_uploaded_date:Long)

    @Query("Update member_address set is_active=0,is_edited=1 where address_guid=:address_guid")
    fun deleteData(address_guid: String?)

    @Query("Delete from member_address where address_guid=:address_guid")
    fun deleteRecord(address_guid: String?)

    @Query("Update member_address set address_line1=:address_line1,address_line2=:address_line2,village_id=:village,panchayat_id=:panchayat_id,block_id=:block_id,postal_code=:postal_code,is_Active =:is_Active,state_id=:state,district_id=:District,is_edited=1,updated_date=:updated_date,updated_by=:updated_by,address_location=:addresslocation where address_guid=:address_guid")
    fun updateAddressdata(
        address_guid: String,
        address_line1: String,
        address_line2: String,
        village: Int,
        panchayat_id: Int,
        block_id: Int,
        state: Int,
        District: Int,
        postal_code: String,
        is_Active: Int,
        updated_date: Long,
        updated_by: String,
        addresslocation: Int
    )

    @Query("Select address_guid from member_address Where member_guid=:member_guid and is_active=1 and entry_source=:entrySource")
    fun getAddressByEntrySource(member_guid: String?,entrySource:Int): String?

    @Query("Select count(*) from member_address where is_complete=0 and member_guid=:member_guid and is_verified=1")
    fun getCompletionCount(member_guid: String?):Int

    @Query("Select count(*) from member_address where member_guid=:member_guid and is_verified=1")
    fun getVerificationCount(member_guid: String?):Int

    @Query("Update member_address set  is_verified=9,is_edited=1,updated_date=:updated_date,updated_by=:updated_by where member_guid=:member_guid and is_verified=1")
    fun updateIsVerifed( member_guid: String?,updated_date: Long,updated_by: String)
}