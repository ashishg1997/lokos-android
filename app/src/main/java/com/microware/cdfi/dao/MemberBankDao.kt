package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.MemberBankAccountEntity

@Dao
interface MemberBankDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertBankDetail(bankEntity:  List<MemberBankAccountEntity>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertBankDetailData(bankEntity : MemberBankAccountEntity?)

    @Query("Select * from MemberBankAccount Where member_guid=:memberGUID and is_active=1")
    fun getBankdetaildata(memberGUID: String?): LiveData<List<MemberBankAccountEntity>>?

    @Query("Select * from MemberBankAccount Where member_guid=:memberGUID and is_edited=1")
    fun getBankdetaildatalist(memberGUID: String?): List<MemberBankAccountEntity>?

    @Query("Select * from MemberBankAccount Where member_guid=:memberGUID and is_active=1 and is_complete=1")
    fun getBankdetaildatalistcount(memberGUID: String?): List<MemberBankAccountEntity>?

     @Query("Select count(*) from MemberBankAccount Where member_guid=:memberGUID and is_default_account=1 and is_active=1")
    fun getBankdetaildefaultcount(memberGUID: String?): Int

    @Query("Select * from MemberBankAccount Where bank_guid=:bank_guid")
    fun getBankdata(bank_guid: String?): List<MemberBankAccountEntity>?

    @Query("Update MemberBankAccount set ifsc_code=:ifsc_code,is_complete=:isCompleted,account_no=:account_no,bank_id=:bank_id,bank_account=:bank_account,account_type=:account_type, mem_branch_code=:branchID, account_open_date=:account_open_date,is_default_account=:is_default_account,status=:status,closing_date=:closing_date,gl_code =:gl_code,same_as_group=:sameAsGroup,  bank_passbook_name=:nameinbankpassbook,   is_edited=:is_edited,updated_date=:updated_date,updated_by=:updated_by,passbook_firstpage=:imageName,dedupl_status=1 where bank_guid=:bank_guid")
    fun updateBankDetail(
        bank_guid: String,
        account_no: String,
        bank_id: String,
        bank_account: Int,
        account_type: String,
        branchID: String,
        account_open_date: String,
        is_default_account: Int,
        status: Int,
        closing_date: String,
        gl_code: String,
        sameAsGroup: Int,
        nameinbankpassbook: String,
        is_edited: Int,
        updated_date: Long,
        updated_by: String,
        imageName: String,
        isCompleted: Int,
        ifsc_code:String
    )


    @Query("DELETE From MemberBankAccount")
    fun deleteAll()

    @Query("Update MemberBankAccount set  is_edited=0,last_uploaded_date=:last_uploaded_date where member_guid=:memberguid")
    fun updateisedit( memberguid: String,last_uploaded_date:Long)


    @Query("Update MemberBankAccount set  is_default_account=0,is_edited=1 where member_guid=:memberguid and is_active =1")
    fun updateisdefault( memberguid: String)


    @Query("Select count(*) from MemberBankAccount where account_no=:account_no and member_guid=:memberguid and is_active=1")
    fun getbank_acc_count(account_no:String,memberguid: String):Int

    @Query("Update MemberBankAccount set is_active=0,is_edited=1 where bank_guid=:bank_guid")
    fun deleteData(bank_guid: String?)

    @Query("Delete from MemberBankAccount where bank_guid=:bank_guid")
    fun deleteRecord(bank_guid: String?)

    @Query("Select count(*) from MemberBankAccount where is_complete=0 and member_guid=:member_guid and is_verified=1")
    fun getCompletionCount(member_guid: String?):Int

    @Query("Select count(*) from MemberBankAccount where member_guid=:member_guid and is_verified=1")
    fun getVerificationCount(member_guid: String?):Int

    @Query("Update MemberBankAccount set  is_verified=9,is_edited=1,updated_date=:updated_date,updated_by=:updated_by where member_guid=:member_guid and is_verified=1")
    fun updateIsVerifed( member_guid: String?,updated_date: Long,updated_by: String)

    @Query("Select count(*) from MemberBankAccount where account_no=:account_no and is_active=1")
    fun getbank_acc_count2(account_no:String):Int

    @Query("Update MemberBankAccount set  activation_status=:status where bank_guid=:memberbank_guid ")
    fun updateactivotaionstatus(memberbank_guid: String?,status: Int)
}