package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.api.model.QueuestatusModel
import com.microware.cdfi.entity.MemberEntity

@Dao
interface MemberDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMember(memberEntity: List<MemberEntity>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMemberData(memberEntity: MemberEntity?)

    @Query("Select * FROM member_profile where partial_status=0")
    fun getAllMember(): LiveData<List<MemberEntity>>?

    @Query("Select * FROM member_profile where partial_status=0 and member_guid=:memberGUID and is_complete=1")
    fun getMember(memberGUID: String?): List<MemberEntity>?

    @Query("Select member_name FROM member_profile where partial_status=0 and member_guid=:memberGUID")
    fun getMemberName(memberGUID: String?): String

    @Query("Select * FROM member_profile where  member_guid=:memberGUID")
    fun getMemberdetail(memberGUID: String?): List<MemberEntity>?

    @Query("Select * FROM member_profile where partial_status=0 and status=1 and is_active =1 and cbo_guid=:shg_guid")
    fun getActiveMember(shg_guid: String?): LiveData<List<MemberEntity>>?

    @Query("Select * FROM member_profile where partial_status=0 and cbo_guid=:shg_guid and is_active=1")
    fun getAllMember(shg_guid: String?): LiveData<List<MemberEntity>>?

    @Query("Select * FROM member_profile where partial_status=0 and cbo_guid=:shg_guid and is_edited=1")
    fun getMemberuploadlist(shg_guid: String?): List<MemberEntity>?


    @Query("Select count(*) FROM member_profile where partial_status=0 and cbo_guid=:shg_guid and is_active=1 and status=1")
    fun getcount(shg_guid: String?): Int

    @Query("Select max(seq_no) FROM member_profile where cbo_guid=:shg_guid")
    fun getseqno(shg_guid: String?): Int

    @Query("DELETE FROM member_profile")
    fun deleteAll()


    @Query(
        "Update member_profile set is_complete=:isCompleted,settlement_status =:settlement_status,seq_no=:Seq_no,member_name=:member_name,member_name_local=:member_name_local,father_husband=:Father_Husband, relation_name=:Relation_Name, relation_name_local=:Relation_Name_Local,  gender=:gender,   marital_status=:marital_status,religion=:religion,social_category=:Social_Category,tribal_category=:TribalCategory, bpl=:BPL,bpl_number=:BPL_Number,pip_category=:PIP_Category,pip_date=:PIP_Date, highest_education_level=:HighestEducationLevel,dob_available=:DOB_Available,dob=:DOB,age=:Age,age_as_on=:AgeAsOn,minority=:Minority,is_disabled=:IsDisabled,disability_details=:DisabilityDetails,wellbeing_category=:WellbeingCategory,primary_occupation=:PrimaryOccupation,secondary_occupation=:SecondaryOccupation,tertiary_occupation=:TertiaryOccupation,joining_date=:joining_date,leaving_date=:leaving_date,reason_for_leaving=:ReasonForLeaving,if_minor_member_replaced=:IfMinor_MemberReplaced,guardian_name=:GuardianName,guardian_name_local=:GuardianName_Local,guardian_relation=:Guardian_Relation,designation=:designation,status=:status,house_hold_code=:house_hold_code,head_house_hold=:head_house_hold,insurance=:Insurance,marked_as_defaulter=:marked_as_defaulter,marked_as_defaulter_date=:marked_as_defaulter_date,record_modified=:RecordModified,updated_date=:updated_date,updated_by=:updated_by,is_edited = 1,member_image =:imageName,mem_dedup_status=1,approve_status=1  ,partial_status=0  where member_guid=:member_guid"
    )
    fun updateMemberDetail(
        member_guid: String,
        Seq_no: Int,
        member_name: String,
        member_name_local: String,
        Father_Husband: Int,
        Relation_Name: String,
        Relation_Name_Local: String,
        gender: Int,
        marital_status: Int,
        religion: Int,
        Social_Category: Int,
        TribalCategory: Int?,
        BPL: Int,
        BPL_Number: String,
        PIP_Category: Int,
        PIP_Date: Int,
        HighestEducationLevel: Int,
        DOB_Available: Int,
        DOB: Long,
        Age: Int,
        AgeAsOn: Long,
        Minority: Int,
        IsDisabled: Int,
        DisabilityDetails: String,
        WellbeingCategory: Int,
        PrimaryOccupation: Int,
        SecondaryOccupation: Int,
        TertiaryOccupation: Int,
        joining_date: Long,
        leaving_date: Long,
        ReasonForLeaving: Int,
        IfMinor_MemberReplaced: Int,
        GuardianName: String,
        GuardianName_Local: String,
        Guardian_Relation: Int,
        designation: Int,
        status: Int,
        house_hold_code: Int,
        head_house_hold: Int,
        Insurance: Int,
        marked_as_defaulter: Int,
        marked_as_defaulter_date: Int,
        RecordModified: String,
        updated_date: Long,
        updated_by: String,
        settlement_status: Int,
        imageName: String,
        isCompleted: Int
    )

    @Query("update member_profile set approve_status=:approve_status,member_code=:code,mem_activation_status=:activationStatus,checker_remark =:checker_remarks where member_guid =:guid")
    fun updateMemberDedupStatus(
        guid: String,
        code: String,
        activationStatus: Int,
        approve_status: Int,
        checker_remarks: String

    )

    @Query("update member_profile set is_upload=1,is_edited =0,transaction_id=:transactionid,last_uploaded_date=:lastupdatedate where is_edited =1 and cbo_guid=:guid")
    fun updateMemberuploadStatus(
        guid: String, transactionid: String,
        lastupdatedate: Long)

    @Query("update member_profile set approve_status=1,mem_dedup_status=1,is_edited =1 where is_edited =1 and member_guid=:guid")
    fun updateMemberdedup(guid: String)


    @Query("update member_profile set designation=:designation,is_edited =1 where  member_guid=:guid")
    fun updateMemberpost(guid: String,designation: Int)

    @Query("update member_profile set approve_status=0 where transaction_id=:transactionid and approve_status=1 and is_edited =0")
    fun updatemembertransactionstatus(transactionid: String )



    @Query("Select * FROM member_profile where member_guid=:memberGUID")
    fun getMemberlistdata(memberGUID: String?): LiveData<List<MemberEntity>>??

    @Query("update member_profile set approve_status=0,is_edited =0 where approve_status=0 and member_guid =:guid")
    fun updatepartialstatus(
        guid: String )

    @Query("Select member_profile.* FROM member_profile where partial_status=0 and (status=1 or settlement_status==2 or settlement_status==3) and cbo_guid=:shg_guid group by member_profile.member_guid order by seq_no")
    fun getAllMemberlist(shg_guid: String?): List<MemberEntity>?

    @Query("Select member_profile.* FROM member_profile inner join MemberPhoneDetail on member_profile.member_guid=MemberPhoneDetail.member_guid where partial_status=0 and cbo_guid=:shg_guid group by member_profile.member_guid order by member_profile.designation")
    fun getAllMemberDeslist(shg_guid: String?): List<MemberEntity>?

    @Query("DELETE FROM member_profile where member_guid=:member_guid")
    fun deleteMemberByMemberGuid(member_guid: String?)

    @Query("DELETE FROM member_address Where member_guid=:member_guid")
    fun deleteMemberAddressByMemberGuid(member_guid: String?)

    @Query("DELETE FROM MemberPhoneDetail Where member_guid=:member_guid")
    fun deleteMemberPhoneByMemberGuid(member_guid: String?)

    @Query("DELETE FROM MemberBankAccount Where member_guid=:member_guid")
    fun deleteMemberBankByMemberGuid(member_guid: String?)

    @Query("DELETE FROM MemberKYCdetails Where member_guid=:member_guid")
    fun deleteMemberKycByMemberGuid(member_guid: String?)

    @Query("DELETE FROM membersystemTags Where member_guid=:member_guid")
    fun deleteMemberSystemTagByMemberGuid(member_guid: String?)

    @Query("Update member_profile set is_active=0,is_edited=1 Where member_guid=:member_guid")
    fun deleteMemberDataByMemberGuid(member_guid: String?)

    @Query("Update member_address set is_active=0,is_edited=1 Where member_guid=:member_guid")
    fun deleteMemberAddressDataByMemberGuid(member_guid: String?)

    @Query("Update MemberPhoneDetail set is_active=0,is_edited=1 Where member_guid=:member_guid")
    fun deleteMemberPhoneDataByMemberGuid(member_guid: String?)

    @Query("Update MemberBankAccount set is_active=0,is_edited=1 Where member_guid=:member_guid")
    fun deleteMemberBankDataByMemberGuid(member_guid: String?)

    @Query("Update MemberKYCdetails set is_active=0,is_edited=1 Where member_guid=:member_guid")
    fun deleteMemberKycDataByMemberGuid(member_guid: String?)

    @Query("Update membersystemTags set is_active=0,is_edited=1 Where member_guid=:member_guid")
    fun deleteMemberSystemTagDataByMemberGuid(member_guid: String?)

    @Query("Select is_verified from member_profile Where member_guid=:member_guid")
    fun getIsVerified(member_guid:String?):Int

    @Query("update member_profile set is_verified=:is_verified,is_edited=1,updated_date=:updated_date,updated_by=:updated_by Where member_guid=:member_guid")
    fun updateIsVerified(member_guid: String,is_verified:Int,updated_date: Long,updated_by: String)

    @Query("Select count(*) from member_profile where is_complete=0 and cbo_guid=:cbo_guid and is_verified=1")
    fun getCompletionCount(cbo_guid: String?):Int

    @Query("Select count(*) from member_profile where cbo_guid=:cbo_guid and is_verified=1")
    fun getVerificationCount(cbo_guid: String?):Int

    @Query("Select count(*) from member_profile where member_guid=:member_guid and is_verified in (1,3)")
    fun getFinalVerifyCount(member_guid: String?):Int

    @Query("Update member_profile set  is_verified=3,is_edited=1,updated_date=:updated_date,updated_by=:updated_by where cbo_guid=:shgguid and is_verified=1")
    fun updateIsVerifed( shgguid: String?,updated_date: Long,updated_by: String)

    @Query("Select member_image FROM member_profile where member_id=:member_id ")
    fun getimage(member_id: Long?): String

    @Query("Select designation FROM member_profile where member_id=:member_id ")
    fun reurnmemberpost(member_id: Long?): Int

    @Query("update member_profile set settlement_status=:settlement,is_edited =1 where  member_id=:memid")
    fun updateSettlementstatus(memid: Long,settlement: Int)

    @Query("Select count(*) FROM member_profile where partial_status=0 and cbo_guid=:shg_guid and is_active=1 and status=1 and designation in(:designationId)")
    fun getOfficeBearerCount(shg_guid: String?,designationId:List<Int>): Int

    @Query("Select a.id,a.transaction_no,a.shg_guid,a.member_guid,a.transaction_type,a.status,a.remarks,b.member_name from transaction_details a inner join member_profile b on a.shg_guid=:shgGuid and a.member_guid=b.member_guid")
    fun getmemberList(shgGuid:String):List<QueuestatusModel>

    @Query("Select * FROM member_profile where  member_guid=:memberGUID")
    fun getMemberSummary(memberGUID: String?): LiveData<List<MemberEntity>>?
}