package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.MemberDesignationEntity

@Dao
interface MemberDesignationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMemberDesignationList(memberDesignationEntity: List<MemberDesignationEntity>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMemberDesignation(memberDesignationEntity: MemberDesignationEntity?)

    @Query("Select * FROM shg_member_designation where cbo_guid=:shgGUID and member_guid=:memberGUID and status=1")
    fun getMemberDesignationdetail(shgGUID: String, memberGUID: String): MemberDesignationEntity

    @Query("Select * FROM shg_member_designation where cbo_guid=:shgGUID and is_edited=1")
    fun getMemberDesignationdetaillist(shgGUID: String): List<MemberDesignationEntity>

    @Query("Select * FROM shg_member_designation where cbo_guid=:shgGUID and is_active=1")
    fun getMemberDesignationdetaillistcount(shgGUID: String?): List<MemberDesignationEntity>

    @Query("Select * FROM shg_member_designation where cbo_guid=:shgGUID ")
    fun getMemberDesignation(shgGUID: String): LiveData<List<MemberDesignationEntity>>?

    @Query("Select * FROM shg_member_designation where cbo_guid=:shgGUID and designation=:id and status=1")
    fun getMemberDesignation(shgGUID: String, id: Int?): MemberDesignationEntity

    @Query("Select * FROM shg_member_designation where cbo_guid=:shgGUID and designation=:id and status=2 order by created_date desc")
    fun getMemberDesignationdata(shgGUID: String, id: Int?): List<MemberDesignationEntity>

    @Query("Select count(*) FROM shg_member_designation where member_guid=:memguid and status=1")
    fun getMembercount(memguid: String): Int

    @Query("Select count(*) FROM shg_member_designation where member_guid=:memguid and status=1 and is_signatory=1")
    fun getsignatoryMembercount(memguid: String): Int

    @Query("Select count(*) FROM shg_member_designation where cbo_guid=:shgGUID and status=1 and is_signatory=1")
    fun getactivesignatorycount(shgGUID: String): Int

    @Query("Select * FROM shg_member_designation where member_guid=:memGUID ")
    fun getMemberDesignationdetail(memGUID: String?): List<MemberDesignationEntity>


    @Query("update shg_member_designation set status=2,to_date =:todate,is_edited=1 where  member_guid =:guid")
    fun updatestatus(guid: String, todate: Long)

    @Query("Update shg_member_designation set  is_edited=0,last_uploaded_date=:last_uploaded_date where cbo_guid=:shgguid")
    fun updateisedit(shgguid: String, last_uploaded_date: Long)

}