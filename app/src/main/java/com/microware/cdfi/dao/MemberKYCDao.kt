package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.Member_KYC_Entity

@Dao
interface MemberKYCDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertKycDetail(kycEntity: List<Member_KYC_Entity>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertKycDetailData(kycEntity: Member_KYC_Entity?)

    @Query("Select * from MemberKYCdetails Where member_guid=:memberGUID and is_active=1")
    fun getKycdetaildata(memberGUID: String?): LiveData<List<Member_KYC_Entity>>?

 @Query("Select * from MemberKYCdetails Where member_guid=:memberGUID and is_edited=1")
    fun getKycdetaildatalist(memberGUID: String?): List<Member_KYC_Entity>?

 @Query("Select * from MemberKYCdetails Where member_guid=:memberGUID and is_active=1 and is_complete=1")
    fun getKycdetaildatalistcount(memberGUID: String?): List<Member_KYC_Entity>?

    @Query("Select * from MemberKYCdetails Where kyc_guid=:kycGUID")
    fun getKycdetail(kycGUID: String?): List<Member_KYC_Entity>?

    @Query("Select * from MemberKYCdetails Where kyc_number=:kyc_number")
    fun getKycwithkycno(kyc_number: String?): List<Member_KYC_Entity>?

    @Query("Update MemberKYCdetails set  is_edited=0,last_uploaded_date=:last_uploaded_date where member_guid=:memberguid")
    fun updateisedit( memberguid: String,last_uploaded_date:Long)

    @Query("Update MemberKYCdetails set is_complete=:isCompleted,kyc_type=:kyc_type,kyc_number=:kyc_id,kyc_front_doc_orig_name=:kyc_front_doc_orig_name,kyc_front_doc_encp_name=:kyc_front_doc_encp_name, kyc_rear_doc_orig_name=:kyc_rear_doc_orig_name, kyc_rear_doc_encp_name=:kyc_rear_doc_encp_name, entry_source=:device,   is_edited=:is_edited,updated_date=:updated_date,updated_by=:updated_by,dedupl_status=1 where kyc_guid=:kyc_guid")
    fun updateKycDetail(
        kyc_guid: String,
        kyc_type: Int,
        kyc_id: String,
        kyc_front_doc_orig_name: String,
        kyc_front_doc_encp_name: String,
        kyc_rear_doc_orig_name: String,
        kyc_rear_doc_encp_name: String,
        device: Int,
        is_edited: Int,
        updated_date: Long,
        updated_by: String,
        isCompleted: Int
    )

    @Query("DELETE From MemberKYCdetails where kyc_guid =:kycGUID")
    fun deleteKycData(kycGUID: String?)

    @Query("Update MemberKYCdetails set is_active=0,is_edited=1 where kyc_guid=:kyc_guid")
    fun deleteData(kyc_guid: String?)

    @Query("Delete from MemberKYCdetails where kyc_guid=:kyc_guid")
    fun deleteRecord(kyc_guid: String?)

    @Query("Select entry_source from MemberKYCdetails Where member_guid=:memberGUID and is_active=1 and kyc_type=:kyc_type")
    fun getKycEntrySource(memberGUID: String?,kyc_type: Int): Int?

    @Query("Select kyc_guid from MemberKYCdetails Where member_guid=:memberGUID and is_active=1 and kyc_type=:kyc_type")
    fun getKycGuidbyKycType(memberGUID: String?,kyc_type: Int): String?

    @Query("Update MemberKYCdetails set entry_source=:entrySource,kyc_number=:kyc_id,is_edited=:is_edited,updated_date=:updated_date,updated_by=:updated_by,dedupl_status=1 where kyc_guid=:kyc_guid")
    fun updateKycNumber(
        kyc_guid: String,
        kyc_id: String,
        is_edited: Int,
        entrySource: Int,
        updated_date: Long,
        updated_by: String
    )

    @Query("Select count(*) from MemberKYCdetails where is_complete=0 and member_guid=:member_guid and is_verified=1")
    fun getCompletionCount(member_guid: String?):Int

    @Query("Select count(*) from MemberKYCdetails where member_guid=:member_guid and is_verified=1")
    fun getVerificationCount(member_guid: String?):Int

    @Query("Update MemberKYCdetails set  is_verified=9,is_edited=1 where member_guid=:member_guid and is_verified=1")
    fun updateIsVerifed( member_guid: String?)

    @Query("Select count(*) from MemberKYCdetails where kyc_type=:kycType and kyc_number=:kycNumber and member_guid=:member_guid and is_active = 1")
    fun getKycCount(kycType:Int,kycNumber:String,member_guid: String):Int

    @Query("Update MemberKYCdetails set  activation_status=:status where kyc_guid=:memberkyc_guid ")
    fun updateactivotaionstatus(memberkyc_guid: String?,status: Int)
}