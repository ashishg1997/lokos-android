package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.MemberPhoneDetailEntity

@Dao
interface MemberPhoneDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPhoneDetail(phoneEntity: List<MemberPhoneDetailEntity>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPhoneDetailData(phoneEntity : MemberPhoneDetailEntity?)

    @Query("Select * from MemberPhoneDetail Where phone_guid=:phone_guid")
    fun getphonedetaildata(phone_guid: String?): List<MemberPhoneDetailEntity>?

    @Query("Select * from MemberPhoneDetail Where member_guid=:phone_guid")
    fun getphonedetaillistdata(phone_guid: String?): List<MemberPhoneDetailEntity>?

    @Query("Select * from MemberPhoneDetail Where member_guid=:memberGUID and is_active=1")
    fun getphoneData(memberGUID: String?): LiveData<List<MemberPhoneDetailEntity>>?

   @Query("Select * from MemberPhoneDetail Where member_guid=:memberGUID and is_edited=1")
    fun getphoneDatalist(memberGUID: String?): List<MemberPhoneDetailEntity>?

   @Query("Select * from MemberPhoneDetail Where member_guid=:memberGUID and is_active=1 and is_complete=1")
    fun getphoneDatalistcount(memberGUID: String?): List<MemberPhoneDetailEntity>?

    @Query("DELETE From MemberPhoneDetail")
    fun deleteAll()

    @Query("Update MemberPhoneDetail set  is_edited=0,last_uploaded_date=:last_uploaded_date where member_guid=:memberguid")
    fun updateisedit( memberguid: String,last_uploaded_date:Long)

    @Query("Update MemberPhoneDetail set is_complete=:isCompleted,phone_no=:phone_no,is_default=:isDefault,phone_ownership=:phone_ownership,phone_ownership_details=:phone_ownership_detail,valid_from=:valid_from,valid_till=:valid_till,entry_source=:device,is_edited=:is_edited,updated_date=:updated_date,updated_by=:updated_by,dedupl_status=1 where phone_guid=:phone_guid")
    fun phoneupdatePhoneDetail(
        phone_guid: String,
        phone_no: String,
        isDefault: Int,
        phone_ownership: String,
        phone_ownership_detail: String,
        valid_from: Long,
        valid_till: Long,
        device: Int,
        is_edited: Int,
        updated_date: Long,
        updated_by: String,
        isCompleted: Int
    )

    @Query("Update MemberPhoneDetail set is_active=0,is_edited=1 where phone_guid=:phone_guid")
    fun deleteData(phone_guid: String?)

    @Query("Delete from MemberPhoneDetail where phone_guid=:phone_guid")
    fun deleteRecord(phone_guid: String?)

    @Query("Select count(*) from MemberPhoneDetail where is_complete=0 and member_guid=:member_guid and is_verified=1")
    fun getCompletionCount(member_guid: String?):Int

    @Query("Select count(*) from MemberPhoneDetail where member_guid=:member_guid and is_verified=1")
    fun getVerificationCount(member_guid: String?):Int

    @Query("Update MemberPhoneDetail set  is_verified=9,is_edited=1,updated_date=:updated_date,updated_by=:updated_by where member_guid=:member_guid and is_verified=1")
    fun updateIsVerifed( member_guid: String?,updated_date: Long,updated_by: String)

    @Query("Select count(*) from MemberPhoneDetail where phone_no=:phone_no and member_guid=:member_guid and is_active = 1")
    fun getPhoneCount(phone_no:String,member_guid: String?):Int
}