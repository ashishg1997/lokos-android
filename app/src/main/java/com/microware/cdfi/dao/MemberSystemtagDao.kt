package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.MemberSystemTagEntity

@Dao
interface MemberSystemtagDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSystemtagDetail(systemEntity: List<MemberSystemTagEntity>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSystemtagData(systemEntity: MemberSystemTagEntity?)

    @Query("Select * from membersystemTags Where member_guid=:member_guid and is_active=1")
    fun getSystemtagdata(member_guid: String?): LiveData<List<MemberSystemTagEntity>>?


    @Query("Select * from membersystemTags Where member_guid=:member_guid and is_edited=1")
    fun getSystemtagdatalist(member_guid: String?): List<MemberSystemTagEntity>?

    @Query("Select * from membersystemTags Where member_guid=:member_guid and is_active=1 and is_complete=1")
    fun getSystemtagdatalistcount(member_guid: String?): List<MemberSystemTagEntity>?

    @Query("Select * from membersystemTags Where system_tag_guid=:systemtag_guid")
    fun getSystemtag(systemtag_guid: String?): List<MemberSystemTagEntity>?

    @Query("DELETE From membersystemTags where system_tag_guid =:systemtag_guid")
    fun deleteSystemtagData(systemtag_guid: String?)

    @Query("Update membersystemTags set  is_edited=0,last_uploaded_date=:last_uploaded_date where member_guid=:memberguid")
    fun updateisedit( memberguid: String,last_uploaded_date:Long)

    @Query("update membersystemTags set is_complete=1,system_type=:system_type,system_id=:system_id,updated_date=:updated_date,updated_by=:updated_by,is_active=1,entry_source=1,is_edited=1 where system_tag_guid =:systemtag_guid")
    fun updateSystemTagdata(system_type:Int,system_id:String,systemtag_guid:String,updated_date:Long,updated_by:String)

    @Query("Update membersystemTags set is_active=0,is_edited=1 where system_tag_guid=:systemtag_guid")
    fun deleteData(systemtag_guid: String?)

    @Query("Delete from membersystemTags where system_tag_guid=:systemtag_guid")
    fun deleteRecord(systemtag_guid: String?)

    @Query("Select count(*) from membersystemTags Where system_id=:systemtagid")
    fun getSystemtagcount(systemtagid: String): Int
}