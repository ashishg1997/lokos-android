package com.microware.cdfi.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.MstCOAEntity

@Dao
interface MstCOADao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMstCOAAllData(MstCOAEntity: List<MstCOAEntity>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMstCOAData(MstCOAEntity: MstCOAEntity)

    @Query("Select * from mst_shg_coa_sub Where type=:type and language_id=:languageCode order by sequence")
    fun getMstCOAdata(type: String,languageCode:String): List<MstCOAEntity>?

    @Query("Select * from mst_shg_coa_sub Where uid not IN(:uid) and type=:type and language_id=:languageCode order by sequence")
    fun getMstCOAdatanotin(uid:List<Int>,type: String,languageCode:String): List<MstCOAEntity>?

    @Query("select  description from mst_shg_coa_sub where type=:type and  language_id=:languageID and uid=:keycode")
    fun getcoaValue(type:String?,languageID:String?,keycode:Int?): String?

    @Query("Delete from mst_shg_coa_sub where uid=:uid")
    fun deleteRecord(uid: Int)

    @Query("DELETE From mst_shg_coa_sub")
    fun deleteAll()

    @Query("Select * from mst_shg_coa_sub where uid IN(:uid) and type=:type and language_id=:languageCode order by sequence")
    fun getCoaSubHeadData(uid:List<Int>,type:String,languageCode:String): List<MstCOAEntity>?

    @Query("Select description from mst_shg_coa_sub where uid=:auid and language_id=:languageCode ")
    fun getCoaSubHeadname(auid:Int,languageCode:String): String

    @Query("Select * from mst_shg_coa_sub where type=:type or type =:type1 and language_id=:languageCode order by sequence limit 3")
    fun getReceiptCoaSubHeadData(type:String,type1:String,languageCode:String): List<MstCOAEntity>?

    @Query("Select * from mst_shg_coa_sub where uid IN(:uid) and  language_id=:languageCode order by sequence")
    fun getCoaSubHeadlist(uid:List<Int>,languageCode:String): List<MstCOAEntity>?

    @Query("SELECT dtMem.amount FROM mst_shg_coa_sub tblgroup LEFT JOIN vo_fin_txn_det_mem dtMem ON dtMem.auid = tblgroup.uid where dtMem.auid=:uid")
    fun getCoaSubAmount(uid:Int) : Int

    @Query("SELECT dtGrp.amount FROM mst_shg_coa_sub tblgroup LEFT JOIN vo_fin_txn_det_grp dtGrp ON dtGrp.auid = tblgroup.uid where dtGrp.auid=:uid and dtGrp.mtgNo=:mtgNo")
    fun getCoaSubAmountGroup(uid:Int,mtgNo:Int) : Int

    @Query("select short_description from mst_shg_coa_sub where language_id =:languageID and uid=:keycode")
    fun getCOADesValue(languageID:String?,keycode:Int?): String?

}