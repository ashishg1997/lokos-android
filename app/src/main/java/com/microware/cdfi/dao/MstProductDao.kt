package com.microware.cdfi.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.LoanProductEntity

@Dao
interface MstProductDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertproductlist(dtLoanGpEntity: List<LoanProductEntity>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertData(dtLoanGpEntity: LoanProductEntity)

    @Query("Select * from mst_loanproduct")
    fun getproductlist(): List<LoanProductEntity>?

    @Query("DELETE From mst_loanproduct")
    fun deleteProducts()



}