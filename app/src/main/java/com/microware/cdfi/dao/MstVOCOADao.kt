package com.microware.cdfi.dao.vodao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.MstVOCOAEntity

@Dao
interface MstVOCOADao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMstCOAAllData(MstCOAEntity: List<MstVOCOAEntity>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMstCOAData(MstVOCOAEntity: MstVOCOAEntity)

    @Query("Select * from mst_vo_coa_sub Where type=:type and language_id=:languageCode order by sequence")
    fun getMstCOAdata(type: String, languageCode: String): List<MstVOCOAEntity>?

    @Query("select  description from mst_vo_coa_sub where type=:type and  language_id=:languageID and uid=:keycode")
    fun getcoaValue(type: String?, languageID: String?, keycode: Int?): String?

    @Query("Delete from mst_vo_coa_sub where uid=:uid")
    fun deleteRecord(uid: Int)

    @Query("DELETE From mst_vo_coa_sub")
    fun deleteAll()


    @Query("Select * from mst_vo_coa_sub where type=:type or type =:type1 and language_id=:languageCode order by sequence limit 3")
    fun getReceiptCoaSubHeadData(
        type: String,
        type1: String,
        languageCode: String
    ): List<MstVOCOAEntity>?

    @Query("Select * from mst_vo_coa_sub where uid IN(:uid) and  language_id=:languageCode order by sequence")
    fun getCoaSubHeadlist(uid: List<Int>, languageCode: String): List<MstVOCOAEntity>?


    @Query("SELECT dtMem.amount FROM mst_vo_coa_sub tblgroup LEFT JOIN vo_fin_txn_det_mem dtMem ON dtMem.auid = tblgroup.uid where dtMem.auid=:uid")
    fun getCoaSubAmount(uid: Int): Int

    @Query("Select * from mst_vo_coa_sub where type IN(:type) and language_id=:languageCode order by sequence")
    fun getAllVoCoaSubHead(
        type: List<String>,
        languageCode: String
    ): LiveData<List<MstVOCOAEntity>?>

    @Query("Select * from mst_vo_coa_sub where type IN(:type) and language_id=:languageCode order by sequence")
    fun getAlllist(type: List<String>, languageCode: String): List<MstVOCOAEntity>?


    @Query("Select description from mst_vo_coa_sub where uid=:auid and language_id=:languageCode ")
    fun getCoaSubHeadname(auid: Int, languageCode: String): String

    @Query("Select * from mst_vo_coa_sub where type IN(:type) and language_id=:languageID and receipt_payment=:receipt order by sequence")
    fun getCoaSubHeadData(
        receipt: Int,
        type: List<String>,
        languageID: String?
    ): List<MstVOCOAEntity>?


    @Query("Select short_description from mst_vo_coa_sub where receipt_payment=:receiptPayment and type=:type and uid=:uid")
    fun getFundTypeId(receiptPayment: Int, type: String, uid: Int): String


}