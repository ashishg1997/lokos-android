package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.PanchayatEntity

@Dao
interface PanchayatDao {

    @Query("Select * from panchayat_master where state_id=:stateCode and district_id=:district_code and block_id=:block_code")
    fun getPanchayatByPanchayatCode(stateCode: Int,district_code: Int,block_code: Int): List<PanchayatEntity>

    @Query("Select * from panchayat_master where state_id=:stateCode and district_id=:district_code and block_id=:block_code")
    fun getPanchayatdataByPanchayatCode(stateCode: Int,district_code: Int,block_code: Int): LiveData<List<PanchayatEntity>>

    @Query("Select * from panchayat_master")
    fun getPanchayat(): LiveData<List<PanchayatEntity>>

    @Query("Select count(*) from panchayat_master")
    fun getPanchayatCount(): Int

    @Query("DELETE FROM panchayat_master")
    fun deleteAllPanchayat()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllPanchayat(PanchayatEntity: List<PanchayatEntity>?)

    @Query("Select panchayat_id from panchayat_master where panchayat_name_en=:panchayat_name_en")
    fun getPanchayatCode(panchayat_name_en: String):PanchayatEntity

    @Query("Select * from panchayat_master")
    fun getPanchayatData(): List<PanchayatEntity>

    @Query("select  panchayat_name_en from panchayat_master where panchayat_id=:panchayat_id")
    fun getPanchayatName(panchayat_id:Int): String?

}