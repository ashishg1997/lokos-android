package com.microware.cdfi.dao

import androidx.room.*
import com.microware.cdfi.entity.ResponseCodeEntity

@Dao
interface ResponseCodeDao {

    @Insert
    fun insertResponse(responseEntity: ResponseCodeEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllResponse(responseEntity: List<ResponseCodeEntity>?)

    @Update
    fun updateResponse(responseEntity: ResponseCodeEntity)

    @Query("Delete from mst_response")
    fun deleteResponse()

    @Query("Select response_msg from mst_response where response_code == :response_code and language_id ==:language_id")
    fun getResponseMsg(response_code :Int,language_id:String): String

}