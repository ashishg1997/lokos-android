package com.microware.cdfi.dao

import androidx.room.*
import com.microware.cdfi.entity.RoleEntity

@Dao
interface RoleDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertrole(roleEntity: RoleEntity?)

    @Query("Select * from mstRole")
    fun getUserole(): List<RoleEntity>?

    @Query("Delete from mstRole")
    fun delete()
}