package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.MemberDesignationEntity
import com.microware.cdfi.entity.SHGEntity

@Dao
interface SHGDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertShg(shgEntity: List<SHGEntity>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertShgData(shgEntity: SHGEntity?)

    @Query("Select * FROM shg_profile where partial_status=0 and village_id=:village_id and is_active=1 order by shg_name COLLATE NOCASE ASC")
    fun getAllSHG(village_id:Int): LiveData<List<SHGEntity>>?


    @Query("Select * FROM shg_profile where partial_status=0 and is_active=1 and status !=2 order by shg_name COLLATE NOCASE ASC")
    fun getAll(): LiveData<List<SHGEntity>>?

    @Query("Select * FROM shg_profile where guid=:shgGUID")
    fun getSHGdetail(shgGUID: String?): List<SHGEntity>?

    @Query("Select * FROM shg_profile where  partial_status=0 and guid=:shgGUID and is_complete=1")
    fun getSHG(shgGUID: String?): List<SHGEntity>?

    @Query("Select count(*) from shg_profile where partial_status=0  and village_id=:villageid and is_active=1")
    fun getShgCount(villageid:Int): Int

    @Query("Select count(*) from shg_profile where partial_status=0 and is_active=1")
    fun getallShgCount(): Int

    @Query("Select count(*) from shg_profile where partial_status=0 and status=1 and village_id=:villageid and activation_status in(0,1,4,5) and is_active=1")
    fun getShgpendingCount(villageid:Int): Int

    @Query("Select count(*) from shg_profile where partial_status=0 and status=1 and activation_status in(0,1,4,5) and is_active=1")
    fun getallShgpendingCount(): Int

    @Query("Select count(*) from shg_profile where partial_status=0 and village_id=:villageid and status=1 and activation_status=2 and is_active=1")
    fun getShgactiveCount(villageid:Int): Int

    @Query("Select count(*) from shg_profile where   partial_status=0 and status=1 and activation_status=2 and is_active=1")
    fun getallShgactiveCount(): Int

    @Query("DELETE FROM shg_profile")
    fun deleteAllShg()

    @Query(
        "Update shg_profile set inactive_reason=:inactiveReason,is_complete=1,promoter_code=:promoter_code,is_verified=:isVerified,shg_type_other=:shg_type_other,shg_resolution=:imageName,shg_cooption_date=:shg_cooption_date,promoter_name=:promoter_name,saving_frequency=:saving_frequency,tags=:tags,is_voluntary_saving=:is_volutary_saving,savings_interest=:ComsavingRoi,voluntary_savings_interest=:voluntary_savings_interest,election_tenure =:election_tenure,bookkeeper_mobile =:bookkeeper_mobile,bookkeeper_name =:bookkeeper_name,primary_activity=:primaryActivity,secondary_activity=:secondaryActivity,tertiary_activity=:tertiaryActivity,hamlet_id=:HamletID,shg_name=:shg_name,shg_name_short_EN=:shg_name_short_EN,shg_name_short_local=:shg_name_short_local,shg_type_code=:shg_type_code,shg_name_local=:shg_name_local, composition=:composition, shg_formation_date=:shg_formation_date,  shg_revival_date=:shg_revival_date,   shg_promoted_by=:SHG_PromotedBy,shg_revived_by=:SHG_RevivedBy, meeting_frequency=:meeting_frequency, meeting_frequency_value=:meeting_frequency_value, meeting_on=:meeting_on,mode=:mode, month_comp_saving=:month_comp_saving, is_bankaccount=:is_bankaccount, funding_agency_id=:funding_agency_id,parent_cbo_code=:parent_cbo_code,parent_cbo_type=:parent_cbo_type,status=:Status,is_active=:is_active,dedupl_status=:dedupl_status,account_books_maintained=:AccountBooksMaintained,cash_book_start_date=:CashBookSTartDate,bank_book_start_date=:BankBooKStartDate,members_ledger_start_date=:MembersLedgerStartDate,Book4=:Book4,Book5=:Book5,Grade=:Grade,grading_done_on=:GradingDoneOn,grade_confirmation_status=:GradeConfirmationStatus,bookkeeper_identified=:bookkeeper_identified,micro_plan_prepared=:micro_plan_number,web_default_checker=:WebDefaultChecker,entry_source=:entry_source,latitude=:latitude,longitude=:longitude,updated_date=:updated_date,updated_by=:updated_by,is_edited = 1,approve_status = 1 where guid=:GUID"
    )
    fun updateBasicDetail(
        GUID: String,
        HamletID: String,
        shg_name: String,
        shg_name_short_EN: String,
        shg_name_short_local: String,
        shg_type_code: Int,
        shg_name_local: String,
        composition: Int,
        shg_formation_date: Long,
        shg_revival_date: Long,
        shg_cooption_date: Long,
        SHG_PromotedBy: Int,
        SHG_RevivedBy: Int,
        meeting_frequency: Int,
        meeting_frequency_value: Int,
        meeting_on: Int,
        mode: Int,
        month_comp_saving: Int,
        is_bankaccount: Int,
        funding_agency_id: Int,
        parent_cbo_code: Int,
        parent_cbo_type: Int,
        Status: Int,
        is_active: Int,
        dedupl_status: Int,
        AccountBooksMaintained: Int,
        CashBookSTartDate: Int,
        BankBooKStartDate: Int,
        MembersLedgerStartDate: Int,
        Book4: Int,
        Book5: Int,
        Grade: String,
        GradingDoneOn: Int,
        GradeConfirmationStatus: String,
        bookkeeper_identified: Int,
        micro_plan_number: Int,
        WebDefaultChecker: Int,
        entry_source: Int,
        latitude: String,
        longitude: String,
        updated_date: Long,
        updated_by: String,
        primaryActivity: Int,
        secondaryActivity: Int,
        tertiaryActivity: Int,
        bookkeeper_name: String,
        bookkeeper_mobile: String,
        election_tenure: Int,
        ComsavingRoi: Double,
        voluntary_savings_interest: Double,
        is_volutary_saving: Int,
        saving_frequency: Int,
        tags: Int,
        promoter_name: String,
        promoter_code:Int?,
        imageName: String,
        shg_type_other: String,
        isVerified:Int,
        inactiveReason:Int
    )

    @Query("Select * FROM shg_profile")
    fun getAllSHGlist(): List<SHGEntity>?

    @Query("update shg_profile set checker_remark =:remarks,updated_by=:user_id,user_id=:user_id where guid =:guid")
    fun updateShguploadStatus(
        guid: String,
        remarks: String,
        user_id: String

    )

    @Query("update shg_profile set is_upload = 0 ,is_locked=0,shg_code=:code,approve_status=:approve_status,activation_status=:activationStatus,checker_remark =:checker_remarks where guid =:guid")
    fun updateShgDedupStatus(
        guid: String,
        code: String,
        activationStatus: Int,
        approve_status: Int,
        checker_remarks: String)

    @Query("update shg_profile set is_upload=1,is_member_edited =0,is_edited =0,transaction_id=:transactionid,last_uploaded_date=:lastupdatedate where guid =:guid")
    fun updateuploadStatus(
        guid: String, transactionid: String,
        lastupdatedate: Long
    )
    @Query("update shg_profile set is_upload=0,is_edited =1 where guid =:guid")
    fun updateisedit(
        guid: String )

    @Query("update shg_profile set approve_status=0 where transaction_id=:transactionid and approve_status=1 and is_edited =0")
    fun updatetransactionstatus(transactionid: String )

    @Query("update shg_profile set approve_status=1,dedupl_status=1,is_edited =1 where guid =:guid")
    fun updatededup(
        guid: String )

    @Query("update shg_profile set approve_status=0,is_edited =0 where approve_status=0 and guid =:guid")
    fun updatepartialstatus(
        guid: String )

    @Query("update shg_profile set partial_status=0 where  guid =:guid")
    fun updatecompletestatus(
        guid: String )

    @Query("Select * FROM shg_profile where is_edited=1 or is_member_edited=1")
    fun getAllSHGlistdata(): List<SHGEntity>?

    @Query("Select * from shg_profile where is_edited=1 or is_member_edited=1")
    fun getPendingSHGlistdata(): List<SHGEntity>?

    @Query("Select count(*) FROM shg_profile where village_id=:village_id")
    fun getSHG_count(village_id:Int): Int

    @Query("Select max(last_uploaded_date) FROM shg_profile")
    fun getlastdate(): Long

    @Query("Select * FROM shg_profile where guid=:shgGUID")
    fun getSHGlistdata(shgGUID: String?): LiveData<List<SHGEntity>>??

    @Query("update shg_profile set activation_status =3,checker_remark=:checker_remark where guid=:shgGUID")
    fun updateCheckerRemarks(checker_remark: String,shgGUID: String)

    @Query("DELETE FROM shg_profile where GUID=:shgGUID")
    fun deleteShgByGuid(shgGUID: String?)

    @Query("DELETE FROM cbo_bank_details where cbo_guid=:shgGUID")
    fun deleteBankByShgGuid(shgGUID: String?)

    @Query("DELETE FROM cbo_phone_details where cbo_guid=:shgGUID")
    fun deletePhoneByShgGuid(shgGUID: String?)

    @Query("DELETE FROM addresses_details where cbo_guid=:shgGUID")
    fun deleteAddressByShgGuid(shgGUID: String?)

    @Query("DELETE FROM systemTags where cbo_guid=:shgGUID")
    fun deleteSystemTagByShgGuid(shgGUID: String?)

    @Query("DELETE FROM shg_member_designation where cbo_guid=:shgGUID")
    fun deleteDesignationByShgGuid(shgGUID: String?)

    @Query("DELETE FROM member_profile where cbo_guid=:shgGUID")
    fun deleteMemberByShgGuid(shgGUID: String?)

    @Query("DELETE FROM member_address Where member_guid in (Select member_guid from member_profile where cbo_guid=:shgGUID)")
    fun deleteMemberAddressByShgGuid(shgGUID: String?)

    @Query("DELETE FROM MemberPhoneDetail Where member_guid in (Select member_guid from member_profile where cbo_guid=:shgGUID)")
    fun deleteMemberPhoneByShgGuid(shgGUID: String?)

    @Query("DELETE FROM MemberBankAccount Where member_guid in (Select member_guid from member_profile where cbo_guid=:shgGUID)")
    fun deleteMemberBankByShgGuid(shgGUID: String?)

    @Query("DELETE FROM MemberKYCdetails Where member_guid in (Select member_guid from member_profile where cbo_guid=:shgGUID)")
    fun deleteMemberKycByShgGuid(shgGUID: String?)

    @Query("DELETE FROM membersystemTags Where member_guid in (Select member_guid from member_profile where cbo_guid=:shgGUID)")
    fun deleteMemberSystemTagByShgGuid(shgGUID: String?)

    @Query("Update shg_profile set is_active=0,is_edited=1 Where GUID=:shgGUID")
    fun deleteShgDataByGuid(shgGUID: String?)

    @Query("Update cbo_bank_details set is_active=0,is_edited=1 Where cbo_guid=:shgGUID")
    fun deleteBankDataByShgGuid(shgGUID: String?)

    @Query("Update cbo_phone_details set is_active=0,is_edited=1 Where cbo_guid=:shgGUID")
    fun deletePhoneDataByShgGuid(shgGUID: String?)

    @Query("Update addresses_details set is_active=0,is_edited=1 Where cbo_guid=:shgGUID")
    fun deleteAddressDataByShgGuid(shgGUID: String?)

    @Query("Update systemTags set is_active=0,is_edited=1 Where cbo_guid=:shgGUID")
    fun deleteSystemTagDataByShgGuid(shgGUID: String?)

    @Query("Update shg_member_designation set is_active=0,is_edited=1 Where cbo_guid=:shgGUID")
    fun deleteDesignationDataByShgGuid(shgGUID: String?)

    @Query("Update member_profile set is_active=0,is_edited=1 Where cbo_guid=:shgGUID")
    fun deleteMemberDataByShgGuid(shgGUID: String?)

    @Query("Update member_address set is_active=0,is_edited=1 Where member_guid in (Select member_guid from member_profile where cbo_guid=:shgGUID)")
    fun deleteMemberAddressDataByShgGuid(shgGUID: String?)

    @Query("Update MemberPhoneDetail set is_active=0,is_edited=1 Where member_guid in (Select member_guid from member_profile where cbo_guid=:shgGUID)")
    fun deleteMemberPhoneDataByShgGuid(shgGUID: String?)

    @Query("Update MemberBankAccount set is_active=0,is_edited=1 Where member_guid in (Select member_guid from member_profile where cbo_guid=:shgGUID)")
    fun deleteMemberBankDataByShgGuid(shgGUID: String?)

    @Query("Update MemberKYCdetails set is_active=0,is_edited=1 Where member_guid in (Select member_guid from member_profile where cbo_guid=:shgGUID)")
    fun deleteMemberKycDataByShgGuid(shgGUID: String?)

    @Query("Update membersystemTags set is_active=0,is_edited=1 Where member_guid in (Select member_guid from member_profile where cbo_guid=:shgGUID)")
    fun deleteMemberSystemTagDataByShgGuid(shgGUID: String?)

    @Query("Select is_verified from shg_profile where guid=:shgGUID")
    fun getIsVerified(shgGUID:String?):Int

    @Query("Select count(*) from shg_profile where guid=:shgGUID and is_verified in (1,3)")
    fun getFinalVerification(shgGUID:String?):Int

    @Query("update shg_profile set is_verified=:is_verified,is_edited=1,updated_date=:updated_date,updated_by=:updated_by where  guid =:guid")
    fun updateIsVerified(guid: String,is_verified:Int,updated_date:Long,updated_by:String)

    @Query("Select ifsc_code from MemberBankAccount where ifsc_code not in(Select ifsc_code from bank_branch_master) union Select ifsc_code from cbo_bank_details where  ifsc_code not in(Select ifsc_code from bank_branch_master)")
    fun getIfscCode():List<String>

    @Query("Select  count(*) from cbo_bank_details where cbo_guid=:guid  and is_active=1 and status=1 and is_complete=1")
    fun getBankCount(guid:String?):Int

    @Query("Select count(*) FROM shg_member_designation where cbo_guid=:shgGUID and is_signatory=1 and is_active=1 and status=1")
    fun getSignatoryCount(shgGUID: String?): Int

    @Query("Update shg_profile set is_locked=:isLocked,is_edited=:isEdited where guid=:shgGUID")
    fun updateLockingFlag(isLocked:Int,isEdited:Int,shgGUID: String?)

    @Query("Update shg_profile set is_locked=:isLocked,is_member_edited=:isEdited where guid=:shgGUID")
    fun updateMemberLockingFlag(isLocked:Int,isEdited:Int,shgGUID: String?)

    @Query("Select is_locked from shg_profile where guid=:shgGUID")
    fun getLockingFlag(shgGUID:String?):Int

    @Query("Select shg_name from shg_profile where guid=:ShgGUID")
    fun getSHGname(ShgGUID: String?): String
}

