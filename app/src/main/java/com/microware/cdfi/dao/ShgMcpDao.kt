package com.microware.cdfi.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.ShgMcpEntity

@Dao
interface ShgMcpDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertShgMcpAllData(shgmcpEntity: List<ShgMcpEntity>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertShgMcpData(shgmcpEntity: ShgMcpEntity)

    @Query("Select * from shg_mcp where mcp_id=:mcp_id and cbo_id=:cbo_id and mem_id=:mem_id")
    fun getShgMcpDataAlllist(mcp_id: Long,cbo_id: Long,mem_id: Long): List<ShgMcpEntity>?

    @Query("Select * from shg_mcp where cbo_id=:cbo_id")
    fun getShgMcpDatalist(cbo_id: Long): List<ShgMcpEntity>?

    @Query("Select sum(amt_demand) from shg_mcp where cbo_id=:cboid")
    fun gettotaldemand(cboid: Long): Int

    @Query("Select count(*) from shg_mcp where cbo_id=:cboid")
    fun getCount(cboid: Long): Int

    @Query("DELETE From shg_mcp")
    fun deleteAll()

    @Query("update shg_mcp set amt_demand=:amt_demand,tentative_date=:tentative_date,loan_product_id=:loan_product_id,loan_source=:loan_source,loan_purpose=:loan_purpose,loan_period=:loan_period,loan_requested_mtg_guid=:loan_requested_mtg_guid,loan_sanctioned_mtg_no=:loan_sanctioned_mtg_no,loan_sanctioned_mtg_guid=:loan_sanctioned_mtg_guid,updated_by=:updatedby,updated_on=:updatedon,loan_request_priority=:loan_request_priority,proposed_emi_amount=:proposed_emi_amount where mcp_id=:mcp_id and cbo_id=:cbo_id and mem_id=:mem_id and loan_requested_mtg_no=:loan_requested_mtg_no")
    fun updateMcpPrepration(
        mcp_id: Long,
        cbo_id: Long,
        mem_id: Long,
        amt_demand: Int,
        tentative_date: Long,
        loan_product_id: Int?,
        loan_source: Int?,
        loan_purpose: Int?,
        loan_period: Int?,
        loan_requested_mtg_no: Int?,
        loan_requested_mtg_guid: String?,
        loan_sanctioned_mtg_no: Int?,
        loan_sanctioned_mtg_guid: String?,
        loan_request_priority: Int?,
        proposed_emi_amount: Int?,
        updatedby: String?,
        updatedon: Long?
    )

    @Query("Select * from shg_mcp where loan_requested_mtg_no=:mtg_no and cbo_id=:cbo_id")
    fun getUploadListData(mtg_no:Int,cbo_id:Long) : List<ShgMcpEntity>

    @Query("Delete from shg_mcp where loan_requested_mtg_no=:mtg_no and cbo_id=:cbo_id")
    fun deleteMCPData(mtg_no:Int,cbo_id:Long)

    @Query("Delete from shg_mcp where cbo_id=:cbo_id")
    fun deleteMCPDataByCboId(cbo_id:Long)
}