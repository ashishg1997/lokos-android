package com.microware.miracle.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.StateEntity

@Dao
interface StateDao {

    @Query("Select * from state_master where state_code=:stateCode")
    fun getStateByStateCode(stateCode: Int): List<StateEntity>

    @Query("Select * from state_master ")
    fun getStateByStateCode(): List<StateEntity>

    @Query("Select * from state_master where state_code=:stateCode")
    fun getStatedataByStateCode(stateCode: Int): LiveData<List<StateEntity>>

    @Query("Select * from state_master")
    fun getState(): LiveData<List<StateEntity>>

    @Query("Select count(*) from state_master")
    fun getStateCount(): Int

    @Query("DELETE FROM state_master")
    fun deleteAllState()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertState(stateEntity: List<StateEntity>?)

    @Query("Select state_id from state_master where state_name_en=:stateName")
    fun getStateCode(stateName: String):StateEntity

    @Query("Select * from state_master")
    fun getStateData(): List<StateEntity>


}