package com.microware.miracle.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.StateEntity

@Dao
interface StateSpinDao {

    @Query("Select * from state_spin where state_code=:stateCode")
    fun getStateByStateCode(stateCode: Int): List<StateEntity>

    @Query("Select * from state_spin ")
    fun getStateByStateCode(): List<StateEntity>




}