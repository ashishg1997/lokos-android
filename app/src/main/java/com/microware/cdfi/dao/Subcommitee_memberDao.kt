package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.subcommitee_memberEntity

@Dao
interface Subcommitee_memberDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCommitteeMember(memberEntity:  List<subcommitee_memberEntity>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCommitteeMemberData(memberEntity : subcommitee_memberEntity?)

    @Query("Select * from tbl_subcommitee_member Where subcommitee_guid=:subcommitee_guid and is_active=1 and status=1")
    fun getCommitteeMemberdetaildata(subcommitee_guid: String?): List<subcommitee_memberEntity>?

    @Query("Select * from tbl_subcommitee_member Where subcommitee_guid=:subcommitee_guid and ec_member_code =:ec_member_code and is_active=1")
    fun getCommitteeMemberdata(subcommitee_guid: String?,ec_member_code: Long?): List<subcommitee_memberEntity>?

    @Query("update tbl_subcommitee_member set is_complete=:is_complete,fromdate=:fromdate,todate=:todate,status=:status,is_active =:is_active,entry_source=:entry_source,is_edited=:is_edited,updated_date=:updated_date,updated_by=:updated_by Where subcommitee_guid=:subcommitee_guid and ec_member_code =:ec_member_code and is_active=1")
    fun updateScMemberdata(subcommitee_guid: String,
                           ec_member_code: Long,
                           fromdate:Long?,
                           todate:Long?,
                           status:Int,
                           is_active:Int,
                           entry_source:Int,
                           is_edited:Int,
                           updated_date:Long,
                           updated_by:String,
                           is_complete:Int)

    @Query("Select count(*) from tbl_subcommitee_member Where ec_member_code =:ec_member_code and is_active=1 and status=1")
    fun getScMembercount(ec_member_code: Long?):Int

    @Query("Select count(*) from tbl_subcommitee_member Where subcommitee_guid=:subcommitee_guid and is_active=1 and status=1")
    fun getCommitteeMemberCount(subcommitee_guid: String?):Int

    @Query("Select * from tbl_subcommitee_member Where subcommitee_guid=:subcommitee_guid and is_edited=1")
    fun getCommitteeMemberPendingdata(subcommitee_guid: String?): List<subcommitee_memberEntity>?

    @Query("update tbl_subcommitee_member set is_upload=1,is_edited =0,last_uploaded_date=:lastupdatedate where subcommitee_guid =:guid and is_edited=1")
    fun updateuploadStatus(
        guid: String,
        lastupdatedate: Long
    )

    @Query("update tbl_subcommitee_member set status=2,todate=:todate,entry_source=:entry_source,is_edited=:is_edited,updated_date=:updated_date,updated_by=:updated_by Where subcommitee_guid=:subcommitee_guid and is_active=1")
    fun updateScMemberStatus(subcommitee_guid: String,
                           todate:Long?,
                           entry_source:Int,
                           is_edited:Int,
                           updated_date:Long,
                           updated_by:String)

    @Query("Select count(*) from tbl_subcommitee_member Where ec_member_code =:ec_member_code and  subcommitee_guid=:subcommitee_guid and is_active=1 and status=1")
    fun getScMembercountbySCGUID(ec_member_code: Long?,subcommitee_guid: String?):Int
}