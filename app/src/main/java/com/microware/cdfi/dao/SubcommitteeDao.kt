package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.subcommitteeEntity

@Dao
interface SubcommitteeDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCommittee(subcommitteeEntity:  List<subcommitteeEntity>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCommitteeData(subcommitteeEntity : subcommitteeEntity?)

    @Query("Select * from tbl_subcommittee Where subcommitee_guid=:subcommitee_guid")
    fun getCommitteedetaildata(subcommitee_guid: String?): List<subcommitteeEntity>?

    @Query("Select * from tbl_subcommittee Where cbo_guid=:cbo_guid and is_active=1 and status=1")
    fun getCommitteedata(cbo_guid: String?): LiveData<List<subcommitteeEntity>>?

    @Query("update tbl_subcommittee set status=:status,subcommitee_type_id=:subcommitee_type_id,fromdate=:fromdate,todate=:todate,is_active=:is_active,entry_source=:entry_source,is_edited=:is_edited,updated_date=:updated_date,updated_by=:updated_by where subcommitee_guid=:subcommitee_guid")
    fun updateCommitteedata(subcommitee_guid:String,
                            subcommitee_type_id:Int?,
                            fromdate:Long?,
                            todate:Long?,
                            is_active:Int?,
                            entry_source:Int?,
                            is_edited:Int?,
                            updated_date:Long?,
                            updated_by:String?,
                            status:Int)

    @Query("Select * from tbl_subcommittee where is_edited=1")
    fun getCommitteePendingdata():List<subcommitteeEntity>?

    @Query("update tbl_subcommittee set is_upload=1,is_edited =0,last_uploaded_date=:lastupdatedate where subcommitee_guid =:guid and is_edited=1")
    fun updateScuploadStatus(
        guid: String,
        lastupdatedate: Long
    )

    @Query("update tbl_subcommittee set is_upload=0,is_edited =1,updated_date=:lastupdatedate,is_complete=1 where subcommitee_guid =:guid")
    fun updateScCompleteStatus(
        guid: String,
        lastupdatedate: Long
    )

    @Query("Select count(*) from tbl_subcommittee Where subcommitee_type_id=:scId and cbo_guid=:cboGuid and is_active=1 and status=1")
    fun getScCount(scId: Int?,cboGuid:String?):Int

}