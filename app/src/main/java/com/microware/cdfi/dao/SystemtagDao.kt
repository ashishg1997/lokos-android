package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.SystemTagEntity

@Dao
interface SystemtagDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSystemtagDetail(systemEntity: List<SystemTagEntity>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSystemtagData(systemEntity: SystemTagEntity?)

    @Query("Select * from systemTags Where cbo_guid=:cbo_guid and is_active=1")
    fun getSystemtagdata(cbo_guid: String?): LiveData<List<SystemTagEntity>>?

   @Query("Select * from systemTags Where cbo_guid=:cbo_guid and is_edited=1")
    fun getSystemtagdatalist(cbo_guid: String?): List<SystemTagEntity>?

    @Query("Select * from systemTags Where cbo_guid=:cbo_guid and is_active=1 and is_complete=1")
    fun getSystemtagdatalistcount(cbo_guid: String?): List<SystemTagEntity>?

    @Query("Select * from systemTags Where system_tag_guid=:systemtag_guid")
    fun getSystemtag(systemtag_guid: String?): List<SystemTagEntity>?

    @Query("Update systemTags set is_active = 0,is_edited=1 where system_tag_guid =:systemtag_guid")
    fun deleteSystemtagData(systemtag_guid: String?)

    @Query("Delete from systemTags where system_tag_guid =:systemtag_guid")
    fun deleteRecord(systemtag_guid: String?)

    @Query("Update systemTags set is_complete=1,updated_date=:updated_date,updated_by=:updated_by,system_type=:system_type,system_id=:system_id,is_active=1,entry_source=1,is_edited=1 where system_tag_guid =:systemtag_guid")
    fun updateSystemTagdata(system_type:Int,system_id:String,systemtag_guid:String,updated_date:Long,updated_by:String)

    @Query("Update systemTags set  is_edited=0,last_uploaded_date=:last_uploaded_date where cbo_guid=:shgguid")
    fun updateisedit( shgguid: String,last_uploaded_date:Long)


}