package com.microware.miracle.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.microware.cdfi.entity.TransactionEntity


@Dao
interface TransactionDao {


    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertransaction(imageEntity: TransactionEntity?)



    @Query("Select * from transaction_details")
    fun gettransaction() :List<TransactionEntity>

    @Query("Select * from transaction_details where transaction_no=:transaction_no")
    fun getdatabytransaction(transaction_no:String) :List<TransactionEntity>

    @Query("update transaction_details set status=:status,remarks=:remarks where transaction_no=:transactionid ")
    fun updatetransactionstatus(transactionid: String, status: Int, remarks: String)

    @Query("Delete from transaction_details where shg_guid=:shgGUID")
    fun deleteTransactionByGuid(shgGUID: String?)

    @Query("Select * from transaction_details where shg_guid=:shgGuid GROUP BY transaction_type")
    fun getUpdateQueueList(shgGuid:String) : LiveData<List<TransactionEntity>>
}