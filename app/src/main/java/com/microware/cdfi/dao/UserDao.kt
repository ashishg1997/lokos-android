package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.microware.cdfi.entity.BlockEntity
import com.microware.cdfi.entity.UserEntity

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertuser(userentity: List<UserEntity>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertuser(userentity: UserEntity?)

    @Query("Select * from mstuser")
    fun getUserData(): List<UserEntity>?

    @Query("Select count(*) from mstuser")
    fun getUserCount(): Int

    @Query("Select count(*) from mstuser where userId=:userId")
    fun getUserCountByUserId(userId:String): Int

    @Query("Delete from mstUser")
    fun delete()
}