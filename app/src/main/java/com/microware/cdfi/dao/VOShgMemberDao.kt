package com.microware.cdfi.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.MappedShgEntity
import com.microware.cdfi.entity.VoShgMemberEntity
import com.microware.cdfi.entity.member_addressEntity

@Dao
interface VOShgMemberDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMapped_Memberdata(voShgMemberEntity:  List<VoShgMemberEntity>)

    @Query("Select * from tbl_VO_Shg_Member where cbo_guid=:shg_guid")
    fun getMemberList(shg_guid:String):List<VoShgMemberEntity>?

    @Query("update tbl_VO_Shg_Member set vo_guid=:federationGuid,cbo_guid=:shg_guid,shg_name=:shg_name where member_guid=:memberguid")
    fun updateVO_shgMember(shg_guid:String,shg_name:String,memberguid:String,federationGuid:String)

    @Query("Select joining_date from tbl_VO_Shg_Member where member_id=:member_code")
    fun getMemberJoiningDate(member_code:Long):Long
}