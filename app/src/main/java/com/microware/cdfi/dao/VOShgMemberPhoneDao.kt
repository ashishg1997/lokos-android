package com.microware.cdfi.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.VoShgMemberPhoneEntity

@Dao
interface VOShgMemberPhoneDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMapped_MemberPhonedata(voShgMemberEntity:  List<VoShgMemberPhoneEntity>)

    @Query("Select * from tbl_VoShgMemberPhone where member_guid=:member_guid")
    fun getphonedetaillistdata(member_guid:String):List<VoShgMemberPhoneEntity>?

}