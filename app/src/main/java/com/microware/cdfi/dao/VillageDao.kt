package com.microware.miracle.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.VillageEntity

@Dao
interface VillageDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllVillage(villageEntity: List<VillageEntity>?)

    @Query("Select * from village_master where state_id=:state_code and district_id=:district_code")
    fun getDistrictdata(state_code: Int, district_code: Int): List<VillageEntity>

    @Query("Delete from village_master")
    fun deleteAllVillage()

    @Query("Select * from village_master where state_id=:state_code and district_id=:district_code and block_id=:block_code order by village_name_en")
    fun getVillage(state_code:Int,district_code:Int,block_code:Int): LiveData<List<VillageEntity>>

    @Query("Select * from village_master where state_id=:state_code and district_id=:district_code and block_id=:block_code order by village_name_en")
    fun getVillagedata(state_code:Int,district_code:Int,block_code:Int): List<VillageEntity>

    @Query("Select village_id from village_master where village_name_en=:village_name_en")
    fun getvillage_code(village_name_en: String): VillageEntity

    @Query("Select * from village_master")
    fun getVillageData(): LiveData<List<VillageEntity>>

    @Query("Select * from village_master")
    fun getVillageData1(): List<VillageEntity>

    @Query("Select count(*) from village_master where state_id=:state_code and district_id=:district_code and block_id=:block_code")
    fun getVillage_count(state_code:Int,district_code:Int,block_code:Int): Int


    @Query("Select * from village_master where state_id=:state_code and district_id=:district_code and block_id=:block_code and panchayat_id=:panchayat_code order by village_name_en")
    fun getVillage_by_panchayatcode(state_code:Int,district_code:Int,block_code:Int,panchayat_code:Int): LiveData<List<VillageEntity>>

    @Query("Select * from village_master where state_id=:state_code and district_id=:district_code and block_id=:block_code and panchayat_id=:panchayat_code order by village_name_en")
    fun getVillagedata_by_panchayatCode(state_code:Int,district_code:Int,block_code:Int,panchayat_code:Int): List<VillageEntity>

    @Query("select  village_name_en from village_master where village_id=:village_id")
    fun getVillageName(village_id: Int): String?

}