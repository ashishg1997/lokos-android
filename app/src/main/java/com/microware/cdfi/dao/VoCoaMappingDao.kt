package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.microware.cdfi.entity.VoCoaMappingEntity

@Dao
interface VoCoaMappingDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCoaMapping(voCoaMappingEntity: VoCoaMappingEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllCoaMapping(voCoaMappingEntity: List<VoCoaMappingEntity>?)

    @Query("Delete from vo_coa_mapping")
    fun deleteAllCoaMapping()

    @Query("Select * from vo_coa_mapping where auid=:uid and cboId=:cboId")
    fun getCoaMappinData(uid:Int,cboId:Long):List<VoCoaMappingEntity>


    @Query("Select bankcode from vo_coa_mapping where auid=:uid and cboId=:cboId")
    fun getaccountno(uid:Int,cboId:Long):String


    @Query("Select * from vo_coa_mapping where  cboId=:cboId")
    fun getcboData(cboId:Long):List<VoCoaMappingEntity>


    @Query("Select * from vo_coa_mapping where  cboId=:cboId")
    fun getcboDatalist(cboId:Long):LiveData<List<VoCoaMappingEntity>>

    @Query("Select bankcode from vo_coa_mapping where auid=:uid and cboId=:cboId")
    fun getBankCode(uid:Int,cboId:Long):String?

}
