package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.ShgFinancialTxnVouchersEntity

@Dao
interface VouchersDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertVoucherList(vouchersEntity: List<ShgFinancialTxnVouchersEntity>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertVoucher(vouchersEntity: ShgFinancialTxnVouchersEntity?)

    @Query("Select * from tbl_shg_fin_txn_vouchers")
    fun getVouchers():List<ShgFinancialTxnVouchersEntity>?

    @Query("Select max(cast(substr(voucher_no,15,4) as int) ) from tbl_shg_fin_txn_vouchers where voucher_date=:mtgDate")
    fun getMaxVoucherNum(mtgDate:Long): Int

    @Query("Select * from tbl_shg_fin_txn_vouchers where voucher_date <:cmtgdate  and (date_realisation =0 or date_realisation is null) and  (bank_code !='' or bank_code is not null) and (mode_payment = 2 or mode_payment = 3) and cbo_id=:cbo_id")
    fun getDataList(cmtgdate: Long, cbo_id: Long): LiveData<List<ShgFinancialTxnVouchersEntity>>?

    @Query("UPDATE tbl_shg_fin_txn_vouchers SET date_realisation = :date_realisation WHERE voucher_no =:voucher_no and cbo_id =:cboId")
    fun updateDateRelisation(date_realisation :Long,voucher_no: String, cboId: Long)


    @Query("select * from tbl_shg_fin_txn_vouchers where mtg_no=:mtgNo and cbo_id=:cboId")
    fun getVoucherUploadList(mtgNo:Int,cboId: Long):List<ShgFinancialTxnVouchersEntity>


  //  2021-10-31-OR-0001
}