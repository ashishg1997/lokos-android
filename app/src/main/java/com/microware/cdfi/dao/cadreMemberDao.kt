package com.microware.cdfi.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.cadreShgMemberEntity

@Dao
interface cadreMemberDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCadre(cadreMemberEntity: List<cadreShgMemberEntity>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCadreData(cadreMemberEntity: cadreShgMemberEntity?)

    @Query("Select * FROM cadreShgMembers where cadre_guid=:cadre_guid")
    fun getCadredetail(cadre_guid: String?): List<cadreShgMemberEntity>?

    @Query("Select * from cadreShgMembers Where member_guid=:member_guid and is_active=1")
    fun getCadreListdata(member_guid: String?): LiveData<List<cadreShgMemberEntity>>?

    @Query("Select * from cadreShgMembers Where member_guid=:member_guid and is_active=1 and is_complete=1")
    fun getCadreListdata1(member_guid: String): List<cadreShgMemberEntity>?

    @Query("update cadreShgMembers set cadre_role_code=:cadre_role_code,cadre_cat_code=:cadre_cat_code,date_joining=:date_joining,date_leaving=:date_leaving,updated_by=:updated_by,updated_on=:updated_on,is_complete=:is_complete,is_edited=:is_edited where cadre_guid=:cadre_guid")
    fun updateCadreData(cadre_guid:String,
                        cadre_role_code:Int?,
                        cadre_cat_code:Int?,
                        date_joining:Long?,
                        date_leaving:Long?,
                        updated_by:String?,
                        updated_on:Long,
                        is_edited:Int,
                        is_complete:Int)

    @Query("Select * from cadreShgMembers Where member_guid=:member_guid and is_edited=1")
    fun getCadrePendingdata(member_guid: String?): List<cadreShgMemberEntity>?
}