package com.microware.cdfi.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.entity.subcommitee_masterEntity

@Dao
interface subcommitteeMasterDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertScMasterdata(scMasterEntity: List<subcommitee_masterEntity>?)

    @Query("Select * FROM tbl_subcommitee_master where language_id=:languageID")
    fun getScMaster(languageID:String?):List<subcommitee_masterEntity>?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertBankDetailData(commiteeEntity : subcommitee_masterEntity?)

    @Query("Select count(*) from tbl_subcommitee_master")
    fun getScMasterCount():Int

    @Query("Delete from tbl_subcommitee_master")
    fun deleteScMasterdata()
}

