package com.microware.cdfi.dao.vodao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.microware.cdfi.entity.DtMtgFinTxnEntity
import com.microware.cdfi.entity.voentity.VoFinTxnEntity

@Dao
interface VoFinTxnDao{

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertVoFinTxn(VoFinTxn: VoFinTxnEntity?)

    @Query("Delete from vo_fin_txn")
    fun deleteVoFinTxnData()

    @Query("Select * from vo_fin_txn")
    fun getVoFinTxnData(): LiveData<List<VoFinTxnEntity>>?

    @Query("Select * from vo_fin_txn")
    fun getVoFinTxnList(): List<VoFinTxnEntity>?

    @Query("select * from vo_fin_txn where mtgNo=:mtgno and cboId=:cbo_id and bankCode=:accountNo")
    fun getListDataByMtgnum(mtgno: Int, cbo_id: Long, accountNo: String): List<VoFinTxnEntity>

    @Query("Delete from vo_fin_txn where mtgNo=:mtg_no and cboId=:cbo_id")
    fun deleteVOFinancialTxnData(mtg_no: Int,cbo_id: Long)

    @Query("select * from vo_fin_txn where mtgNo=:mtgno and cboId=:cbo_id")
    fun getBankListDataByMtgnum(mtgno:Int,cbo_id:Long): List<VoFinTxnEntity>

    @Query("Update vo_fin_txn set closingBalance=:closingbalance,zeroMtgCashBank=:zero_mtg_cash_bank, chequeIssuedNotRealized=:cheque_issued_not_realized,chequeReceivedNotCredited=:cheque_received_not_credited,balanceDate=:balance_date,updatedBy=:updatedby, updatedOn=:updatedon, uploadedBy=:uploaded_by Where cboId=:cbo_id and mtgNo=:mtgno and bankCode=:bankcode")
    fun updateCutOffBankBalance(
        cbo_id: Long?,
        mtgno: Int?,
        bankcode: String?,
        zero_mtg_cash_bank: Int?,
        cheque_issued_not_realized: Int?,
        cheque_received_not_credited: Int?,
        closingbalance: Int?,
        balance_date: Long?,
        updatedby: String,
        updatedon: Long?,
        uploaded_by: String?)

    @Query("Select sum(closingBalance) from vo_fin_txn Where  mtgNo=:mtgno and cboId=:cbo_id")
    fun gettotalinbank(mtgno: Int, cbo_id: Long): Int
    @Query("Delete from vo_fin_txn where cboId=:cbo_id")
    fun deleteVO_FinancialTxnData(cbo_id: Long)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllVoFinTxn(VoFinTxn: List<VoFinTxnEntity>?)

    @Query("select * from vo_fin_txn where mtgNo=:mtgno and cboId=:cbo_id")
    fun getListData(mtgno:Int,cbo_id:Long): List<VoFinTxnEntity>

    @Query("Select sum(closingBalance) from vo_fin_txn Where  mtgNo=:mtgno and cboId=:cbo_id and bankCode=:bankcode")
    fun gettotalclosinginbank(mtgno: Int, cbo_id: Long, bankcode: String?): Int

    @Query("Select sum(openingBalance) from vo_fin_txn Where  mtgNo=:mtgno and cboId=:cbo_id and bankCode=:bankcode")
    fun gettotalopeninginbank(mtgno: Int, cbo_id: Long, bankcode: String?): Int

    @Query("Update vo_fin_txn set closingBalance=:closingbal where mtgNo=:mtgno and cboId=:cbo_id and bankCode=:accountno ")
    fun updateclosingbalbank(
        closingbal: Int,
        mtgno: Int,
        cbo_id: Long,
        accountno: String
    )

    @Query("Select sum(closingBalance) from vo_fin_txn Where  mtgNo=:mtgno and cboId=:cbo_id and bankCode=:accountno ")
    fun getclosingbal(mtgno: Int, cbo_id: Long, accountno: String): Int

}