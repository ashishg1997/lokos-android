package com.microware.cdfi.dao.vodao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.microware.cdfi.api.vomodel.VOBankTransactionModel
import com.microware.cdfi.entity.MstVOCOAEntity
import com.microware.cdfi.entity.voentity.VoFinTxnDetGrpEntity

@Dao
interface VoFinTxnDetGrpDao{

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertVoGroupLoanSchedule(VoFinTxnDetGrp: VoFinTxnDetGrpEntity?)

    @Query("Delete from vo_fin_txn_det_grp")
    fun deleteVoFinTxnDetGrpData()

    @Query("Select * from vo_fin_txn_det_grp")
    fun getVoFinTxnDetGrpData(): LiveData<List<VoFinTxnDetGrpEntity>>?

    @Query("Select * from vo_fin_txn_det_grp where mtgGuid=:mtg_guid and cboId=:cboid")
    fun getVoFinTxnDetGrpList(mtg_guid:String,cboid:Long): List<VoFinTxnDetGrpEntity>?

    @Query("Delete from vo_fin_txn_det_grp where mtgNo=:mtg_no and cboId=:cbo_id")
    fun deleteGrpFinancialTxnDetailData(mtg_no:Int,cbo_id:Long)

    @Query("Select count(*) from vo_fin_txn_det_grp where mtgNo=:mtg_no and cboId=:cbo_id and Auid in(47,48)")
    fun getAdjustmentCashCount(mtg_no:Int,cbo_id:Long):Int

    @Query("update vo_fin_txn_det_grp set auid=:auid,fundType=:fund_type,amountToFrom=:amount_to_from,type=:type,amount=:amount,dateRealisation=:date_realisation,modePayment=:mode_payment,bankCode=:bank_code,transactionNo=:transaction_no,updatedBy=:updatedby,updatedOn=:updatedon where cboId=:cbo_id and mtgNo=:mtg_no and auid in(47,48)")
    fun updateVoCashAdjustment(
        cbo_id: Long,
        mtg_no: Int,
        auid: Int,
        fund_type: Int?,
        amount_to_from: Int?,
        type: String?,
        amount: Int?,
        date_realisation: Long?,
        mode_payment: Int?,
        bank_code: String?,
        transaction_no: String?,
        updatedby: String?,
        updatedon: Long?
    )

    @Query("select sum(amount) from vo_fin_txn_det_grp where  mtgNo=:mtg_no and  cboId=:cbo_id and modePayment=:modePayment and fundType=:fund_type")
    fun getIncomeAndExpenditureAmount(cbo_id: Long,mtg_no: Int,modePayment:Int,fund_type: Int):Int

    @Query("select sum(amount) from vo_fin_txn_det_grp where  mtgNo=:mtg_no and  cboId=:cbo_id and fundType=:fund_type")
    fun getMemberIncomeAndExpenditureAmount(cbo_id: Long,mtg_no: Int,fund_type: Int):Int

    @Query("Select * from vo_fin_txn_det_grp Where cboId=:cbo_id and mtgNo=:mtg_no and auid in (:auid) and type=:type")
    fun getInvestmentListByMtgNum(cbo_id: Long, mtg_no: Int,auid:List<Int>,type:String): LiveData<List<VoFinTxnDetGrpEntity>?>

    @Query("Select * from mst_vo_coa_sub where uid IN(:uid) and type=:type and language_id=:languageCode order by sequence")
    fun getCoaSubHeadData(uid:List<Int>,type:String,languageCode:String): List<MstVOCOAEntity>?

    @Query("update vo_fin_txn_det_grp set fundType=:fund_type,amountToFrom=:amount_to_from,type=:type,amount=:amount,dateRealisation=:date_realisation,modePayment=:mode_payment,bankCode=:bank_code,transactionNo=:transaction_no,updatedBy=:updatedby,updatedOn=:updatedon where mtgGuid=:mtg_guid and cboId=:cbo_id and mtgNo=:mtg_no and auid=:auid and amountToFrom=:amount_to_from")
    fun updateIncomeAndExpenditure(
        mtg_guid: String,
        cbo_id: Long,
        mtg_no: Int,
        auid: Int,
        fund_type: Int?,
        amount_to_from: Int?,
        type: String?,
        amount: Int?,
        date_realisation: Long?,
        mode_payment: Int?,
        bank_code: String?,
        transaction_no: String?,
        updatedby: String?,
        updatedon: Long?
    )

    @Query("Select * from vo_fin_txn_det_grp Where mtgGuid=:mtg_guid and cboId=:cbo_id and mtgNo=:mtg_no and auid=:auid and type=:type and amountToFrom=:amount_to_from")
    fun getInvestmentdata(mtg_guid: String, cbo_id: Long, mtg_no: Int,auid:Int,type:String,amount_to_from: Int): List<VoFinTxnDetGrpEntity>?

    @Query("Select * from vo_fin_txn_det_grp Where mtgGuid=:mtg_guid and cboId=:cbo_id and mtgNo=:mtg_no and auid=:auid and amountToFrom=:amount_to_from")
    fun getIncomeAndExpendituredata(mtg_guid: String, cbo_id: Long, mtg_no: Int,auid:Int,amount_to_from:Int): List<VoFinTxnDetGrpEntity>?

    @Query("Select * from vo_fin_txn_det_grp Where mtgGuid=:mtg_guid and cboId=:cbo_id and mtgNo=:mtg_no and fundType=:fund_type")
    fun getVoFinTxnDetGrpData(mtg_guid: String, cbo_id: Long, mtg_no: Int,fund_type:Int): LiveData<List<VoFinTxnDetGrpEntity>>?
    @Query("Select sum(amount) from vo_fin_txn_det_grp Where cboId=:cbo_id and mtgNo=:mtg_no and auid in (:auid) and type=:type")
    fun getTotalInvestmentByMtgNum(cbo_id: Long, mtg_no: Int,auid:List<Int>,type:String): Int

    @Query("Delete from vo_fin_txn_det_grp where cboId=:cbo_id")
    fun deleteVO_FinancialTxnDetGrpData(cbo_id: Long)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllVoGroupLoanSchedule(VoFinTxnDetGrp: List<VoFinTxnDetGrpEntity>?)

    @Query("Select * from vo_fin_txn_det_grp where cboId=:cboid and mtgNo=:ntgNo")
    fun getVoFinTxnDetGrpListData(ntgNo: Int, cboid: Long): List<VoFinTxnDetGrpEntity>?

    @Query("Select * from vo_fin_txn_det_grp where mtgNo=:mtgno and cboId=:cbo_id and bankCode=:bankaccount and orgType=:orgType and type IN(:type)")
    fun getVoFinTxnDetgrpdatabyBank(
        cbo_id: Long,
        mtgno: Int,
        bankaccount: String,
        orgType: Int,
        type: List<String>
    ): List<VoFinTxnDetGrpEntity>?

    @Query("update vo_fin_txn_det_grp set modePayment=:mode_payment,isEdited=0,transactionNo=:transaction_no, updatedBy=:updatedby,updatedOn=:updatedon where mtgNo=:mtg_no and cboId=:cbo_id and bankCode=:bank_code")
    fun updatefindetailgrp(
        cbo_id: Long,
        mtg_no: Int,
        bank_code: String?,
        mode_payment: Int?,
        transaction_no: String?,
        updatedby: String?,
        updatedon: Long?
    )

    @Query("Select sum(amount) from vo_fin_txn_det_grp Where type IN(:reciept) and cboId=:cbo_id and mtgNo=:mtg_no and modePayment in(:modePayment) and bankCode=:bankcode and isEdited=0")
    fun getIncomeAmount(
        cbo_id: Long,
        mtg_no: Int,
        modePayment: List<Int>,
        reciept: List<String>,
        bankcode: String
    ): Int
    @Query("Select sum(amount) from vo_fin_txn_det_grp Where type IN(:reciept) and cboId=:cbo_id and mtgNo=:mtg_no and modePayment=:modePayment and bankCode=:bankcode and orgType=:orgtype and isEdited=1")
    fun getpendingIncomeAmount(
        cbo_id: Long,
        mtg_no: Int,
        modePayment: Int,
        reciept: List<String>,
        bankcode: String,
        orgtype: Int
    ): Int
    @Query("Select * from vo_fin_txn_det_grp where mtgNo=:mtgNO and cboID=:cboID and auid=:auid")
    fun getSavingamount(mtgNO:Int,cboID:Long, auid: Int): List<VoFinTxnDetGrpEntity>?

    @Query("Select amount from vo_fin_txn_det_grp where auid=:uid and mtgNo=:mtgNo and cboId=:cboID")
    fun getAmount(uid:Int,mtgNo:Int,cboID:Long) : Int

    @Query("Select sum(amount) from vo_fin_txn_det_grp where mtgNo=:mtgNo and cboId=:cboId and orgType=:orgType  and type IN(:type) and isEdited=0")
    fun getTotalFinTxnGrpAmount(mtgNo: Int, cboId: Long, orgType: Int, type: List<String>): Int

    @Query("Select sum(amount) from vo_fin_txn_det_grp where cboId=:cboId and mtgNo=:mtgNo and type=:type and bankCode=:bankCode and (modePayment= 2 or modePayment = 3)  and isEdited=0")
    fun getDebitAmount(cboId: Long, mtgNo: Int, type: String, bankCode: String): Int

    @Query("SELECT * FROM vo_fin_txn_det_grp where cboId=:cboId and mtgNo=:mtgNo and  bankCode=:bankCode and transactionNo=:txnno and (modePayment= 2 or modePayment = 3)")
    fun gettxnkist(
        cboId: Long,
        mtgNo: Int,
        bankCode: String,
        txnno: String
    ): List<VoFinTxnDetGrpEntity>?

    @Query("Select sum(amount) from vo_fin_txn_det_grp Where type IN(:reciept) and cboId=:cbo_id and mtgNo=:mtg_no and modePayment IN(:modePayment) and isEdited=0")
    fun getIncomeAmount(
        cbo_id: Long,
        mtg_no: Int,
        modePayment: List<Int>,
        reciept: List<String>
    ): Int

    @Query("Select shg_fin_income.cboId as cbo_id,shg_fin_income.mtgNo as mtg_no,shg_fin_income.bankCode as bank_to,shg_fin_expenditure.bankCode as bank_from,shg_fin_income.amount as amount,shg_fin_income.auid as auid from (Select * from vo_fin_txn_det_grp where fundType=:fund_type and type='RO' and cboId=:cbo_id and mtgNo=:mtg_no) as shg_fin_income  inner join (Select * from vo_fin_txn_det_grp where fundType=:fund_type and type='PO' and cboId=:cbo_id and mtgNo=:mtg_no) as shg_fin_expenditure on shg_fin_income.cboId = shg_fin_expenditure.cboId and shg_fin_income.mtgNo = shg_fin_expenditure.mtgNo and shg_fin_income.amount = shg_fin_expenditure.amount")
    fun getBankTransferList(cbo_id: Long, mtg_no: Int, fund_type: Int): LiveData<List<VOBankTransactionModel>?>


    @Query("Select sum(amount) from vo_fin_txn_det_grp where cboId=:cbodID and mtgNo=:mtgNo and type IN(:type) and isEdited=0")
    fun getFinTxnDetailGrpAmount(
        cbodID: Long,
        mtgNo: Int,
        type: List<String>
    ):Int



    @Query("Select amount from vo_fin_txn_det_grp where orgType=2 and type in(:type) and mtgNo=:mtgNo and cboId=:cboID and isEdited=0")
    fun getSavingDepositedAmount(mtgNo:Int,cboID:Long,type:List<String>) : Int

    @Query("SELECT * FROM vo_fin_txn_det_grp where cboId=:cboId and mtgNo=:mtgNo and isEdited=0")
    fun gettxnvoucherlist(
        cboId: Long,
        mtgNo: Int
    ): List<VoFinTxnDetGrpEntity>?

}