package com.microware.cdfi.dao.vodao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.microware.cdfi.entity.MstCOAEntity
import com.microware.cdfi.entity.voentity.VoFinTxnDetMemEntity

@Dao
interface VoFinTxnDetMemDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertVoGroupLoanSchedule(VoFinTxnDetMem: VoFinTxnDetMemEntity?)

    @Query("Delete from vo_fin_txn_det_mem")
    fun deleteVoFinTxnDetMemData()

    @Query("Select * from vo_fin_txn_det_mem")
    fun getVoFinTxnDetMemData(): LiveData<List<VoFinTxnDetMemEntity>>?

    @Query("Delete from vo_fin_txn_det_mem where mtgNo=:mtg_no and cboId=:cbo_id")
    fun deleteFinancialTransactionMemberDetailData(mtg_no: Int, cbo_id: Long)

    @Query("Select sum(amount) from vo_fin_txn_det_mem Where type IN(:reciept) and cboId=:cbo_id and mtgNo=:mtg_no and modePayment in(:modePayment) and bankCode=:bankcode and isEdited=0")
    fun getIncomeAmount(
        cbo_id: Long,
        mtg_no: Int,
        modePayment: List<Int>,
        reciept: List<String>,
        bankcode: String
    ): Int

    @Query("Select sum(amount) from vo_fin_txn_det_mem Where type IN(:reciept) and cboId=:cbo_id and memId=:memid and mtgNo=:mtg_no and modePayment=:modePayment and bankCode=:bankcode and isEdited=1")
    fun getpendingIncomeAmount(
        cbo_id: Long,
        memid: Long,
        mtg_no: Int,
        modePayment: Int,
        reciept: List<String>,
        bankcode: String
    ): Int

    @Query("Select * from vo_fin_txn_det_mem where mtgNo=:mtgNO and cboID=:cboID and memId=:memId and auid=:auid")
    fun getSavingamount(
        mtgNO: Int,
        cboID: Long,
        memId: Long,
        auid: Int
    ): List<VoFinTxnDetMemEntity>?

    @Query("Select * from mst_shg_coa_sub where uid IN(:uid) and type=:type1 and language_id=:languageCode order by sequence")
    fun getCoaSubHeadData(uid: List<Int>, type1: String, languageCode: String): List<MstCOAEntity>?

    @Query("Select * from vo_fin_txn_det_mem where mtgGuid=:mtg_guid and cboId=:cbo_id")
    fun getVoFinTxnDetMemList(mtg_guid: String, cbo_id: Long): List<VoFinTxnDetMemEntity>?


    @Query("Select * from vo_fin_txn_det_mem where memId=:memid and mtgNo=:mtgno and cboId=:cbo_id and bankCode=:bankaccount  and type IN(:type)  ")
    fun getVoFinTxnDetMemdata(
        cbo_id: Long,
        memid: Long,
        mtgno: Int,
        bankaccount: String,
        type: List<String>
    ): List<VoFinTxnDetMemEntity>?

    @Query("update vo_fin_txn_det_mem set type=:type,amount=:amount,dateRealisation=:date_realisation,modePayment=:mode_payment,bankCode=:bank_code,transactionNo=:transaction_no,updatedBy=:updatedby,updatedOn=:updatedon where mtgGuid=:mtg_guid and cboId=:cbo_id and mtgNo=:mtg_no and auid=:auid")
    fun updateIncomeAndExpenditure(
        mtg_guid: String,
        cbo_id: Long,
        mtg_no: Int,
        auid: Int,
        type: String?,
        amount: Int?,
        date_realisation: Long?,
        mode_payment: Int?,
        bank_code: String?,
        transaction_no: String?,
        updatedby: String?,
        updatedon: Long?
    )

    @Query("update vo_fin_txn_det_mem set modePayment=:mode_payment,transactionNo=:transaction_no,isEdited=0, updatedBy=:updatedby,updatedOn=:updatedon where mtgNo=:mtg_no and cboId=:cbo_id and memId=:memid and bankCode=:bank_code and isEdited=1")
    fun updatefindetailmem(
        cbo_id: Long,
        memid: Long,
        mtg_no: Int,
        bank_code: String?,
        mode_payment: Int?,
        transaction_no: String?,
        updatedby: String?,
        updatedon: Long?
    )

    @Query("Select * from vo_fin_txn_det_mem Where mtgGuid=:mtg_guid and cboId=:cbo_id and mtgNo=:mtg_no and auid=:auid")
    fun getIncomeAndExpendituredata(
        mtg_guid: String,
        cbo_id: Long,
        mtg_no: Int,
        auid: Int
    ): List<VoFinTxnDetMemEntity>?

    @Query("Select * from vo_fin_txn_det_mem Where mtgGuid=:mtgGuid and cboId=:cboId and mtgNo=:mtgNo")
    fun getIncomeAndExpenditureAlldata(
        mtgGuid: String,
        cboId: Long,
        mtgNo: Int
    ): LiveData<List<VoFinTxnDetMemEntity>?>


    @Query("Select sum(amount) from vo_fin_txn_det_mem Where mtgGuid=:mtg_guid and cboId=:cbo_id and mtgNo=:mtg_no and auid=:auid")
    fun getTotalAmountByReceiptType(mtg_guid: String, cbo_id: Long, mtg_no: Int, auid: Int): Int

    @Query("Delete from vo_fin_txn_det_mem where cboId=:cboId")
    fun deleteVO_memLoanTxnData(cboId: Long)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllData(VoFinTxnDetMem: List<VoFinTxnDetMemEntity>?)

    @Query("Select * from vo_fin_txn_det_mem where cboId=:cboId and memId=:memId and mtgNo=:mtgNo ")
    fun getFinTxnMemData(cboId: Long, memId: Long, mtgNo: Int): List<VoFinTxnDetMemEntity>?


    @Query("Select * from vo_fin_txn_det_mem where auid=:uid and memId=:memId and mtgNo=:mtgNo")
    fun getauidlist(uid: Int, memId: Long, mtgNo: Int): List<VoFinTxnDetMemEntity>?

    @Query("SELECT amount FROM vo_fin_txn_det_mem where auid=:uid and memId=:memId and mtgNo=:mtgNo")
    fun getAmount(uid: Int, memId: Long, mtgNo: Int): Int

    @Query("SELECT sum(amount) FROM vo_fin_txn_det_mem where cboid=:cboid and memId=:memId and mtgNo=:mtgNo and type IN(:type)  and isEdited=1")
    fun getpendingAmount(cboid: Long, memId: Long, mtgNo: Int, type: List<String>): Int

    @Query("SELECT sum(amount) FROM vo_fin_txn_det_mem where cboid=:cboid and memId=:memId and mtgNo=:mtgNo and type IN(:type)  and isEdited=0")
    fun getrecivedAmount(cboid: Long, memId: Long, mtgNo: Int, type: List<String>): Int

    @Query("select sum(amount) from vo_fin_txn_det_mem where mtgNo=:mtgNo and cboId=:cboId and orgType=:orgType and type IN(:type) and isEdited=0")
    fun getTotalFinTxnMemAmount(mtgNo: Int, cboId: Long, orgType: Int, type: List<String>): Int

    @Query("SELECT sum(amount) FROM vo_fin_txn_det_mem where cboId=:cboId and mtgNo=:mtgNo and type=:type and bankCode=:bankCode and (modePayment= 2 or modePayment = 3)")
    fun getDebitAmount(cboId: Long, mtgNo: Int, type: String, bankCode: String): Int

    @Query("SELECT * FROM vo_fin_txn_det_mem where memId=:cboId and mtgNo=:mtgNo and  bankCode=:bankCode and transactionNo=:txnno and (modePayment= 2 or modePayment = 3)")
    fun gettxnkist(
        cboId: Long,
        mtgNo: Int,
        bankCode: String,
        txnno: String
    ): List<VoFinTxnDetMemEntity>?

    @Query("select (sum(amount)) from vo_fin_txn_det_mem where mtgNo=:mtgno and cboId=:cbo_id and auid=82")
    fun getsumcomp(mtgno: Int, cbo_id: Long): Int

    @Query("select (sum(amount)) from vo_fin_txn_det_mem where mtgNo=:mtgno and cboId=:cbo_id and auid=3")
    fun getsumvol(mtgno: Int, cbo_id: Long): Int


    @Query("SELECT sum(amount) FROM vo_fin_txn_det_mem where auid=:uid and memId=:memId and mtgNo=:mtgNo and cboId=:cboid")
    fun getAmountByAuid(uid: Int, memId: Long, mtgNo: Int, cboid: Long): Int

    @Query("Select sum(amount) from vo_fin_txn_det_mem Where type IN(:reciept) and cboId=:cbo_id and mtgNo=:mtg_no and modePayment IN(:modePayment) and isEdited=0")
    fun getIncomeAmount(
        cbo_id: Long,
        mtg_no: Int,
        modePayment: List<Int>,
        reciept: List<String>
    ): Int


    @Query("Select sum(amount) from vo_fin_txn_det_mem where cboId=:cbodID and mtgNo=:mtgNo and type IN(:type) and isEdited=0")
    fun getFinTxnDetailMemAmount(
        cbodID: Long,
        mtgNo: Int,
        type: List<String>
    ): Int

    @Query("SELECT * FROM vo_fin_txn_det_mem where cboId=:cboId and mtgNo=:mtgNo and isEdited=0")
    fun gettxnvoucherlist(cboId: Long, mtgNo: Int): List<VoFinTxnDetMemEntity>?
}