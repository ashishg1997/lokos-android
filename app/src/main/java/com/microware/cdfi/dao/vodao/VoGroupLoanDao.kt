package com.microware.cdfi.dao.vodao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.microware.cdfi.entity.voentity.VoGroupLoanEntity

@Dao
interface VoGroupLoanDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertVoGroupLoan(VoGroupLoan: VoGroupLoanEntity?)

    @Query("Delete from vo_group_loan")
    fun deleteVoGroupLoanData()

    @Query("Select * from vo_group_loan")
    fun getVoGroupLoanData(): LiveData<List<VoGroupLoanEntity>>?

    @Query("Select * from vo_group_loan")
    fun getVoGroupLoanList(): List<VoGroupLoanEntity>?

    @Query("Select interestRate from vo_group_loan Where loanNo=:loanno")
    fun getinterestrate(loanno: Int): Double

    @Query("Delete from vo_group_loan where mtgNo=:mtg_no and cboId=:cbo_id")
    fun deleteGroupLoanData(mtg_no: Int, cbo_id: Long)

    @Query("Select max(loanNo) from vo_group_loan Where cboId=:cboId")
    fun getmaxLoanno(cboId: Long): Int

    @Query("select * from vo_group_loan where mtgNo=:mtgno and cboId=:cbo_aid and loanSource=:LoanSource and loanProductId=:fundType")
    fun getListDataByMtgnum(
        mtgno: Int,
        cbo_aid: Long,
        LoanSource: Int,
        fundType: Int
    ): List<VoGroupLoanEntity>

    @Query("select * from vo_group_loan where mtgNo=:mtgno and cboId=:cbo_aid ")
    fun getListDatavoucher(mtgno: Int, cbo_aid: Long): List<VoGroupLoanEntity>


    @Query("Select * from vo_group_loan Where cboId=:cbo_id and loanNo=:loanno and mtgNo=:mtgno")
    fun getLoandata(cbo_id: Long, loanno: Int, mtgno: Int): List<VoGroupLoanEntity>?


    @Query("Select * from vo_group_loan Where cboId=:cbo_id and loanNo=:loanno ")
    fun getLoandatabyloanno(cbo_id: Long, loanno: Int): List<VoGroupLoanEntity>?


    @Query("Select sum(amount) from vo_group_loan Where modePayment=1 and mtgNo=:mtgno and cboId=:cbo_id")
    fun gettotloangroup(mtgno: Int, cbo_id: Long): Int

    @Query("Select max(loanNo) from vo_group_loan Where cboId=:cbo_id and mtgNo=:mtg_no")
    fun getCutOffmaxLoanno(cbo_id: Long, mtg_no: Int): Int

    @Query("Select * from vo_group_loan Where cboId=:cbo_id and mtgNo=:mtgNo")
    fun getLoanDetail(cbo_id: Long, mtgNo: Int): List<VoGroupLoanEntity>?

    @Query("Select * from vo_group_loan Where loanNo=:loan_no and cboId=:cbo_id and mtgGuid=:mtg_guid")
    fun getLoanDetailData(loan_no: Int, cbo_id: Long, mtg_guid: String): List<VoGroupLoanEntity>?

    @Query("Delete from vo_group_loan where cboId=:cbo_id")
    fun deleteVO_GroupLoanData(cbo_id: Long)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllVoGroupLoan(VoGroupLoan: List<VoGroupLoanEntity>?)


    @Query("Select amount from vo_group_loan Where loanNo=:loanno and cboId=:cboID")
    fun gettotamt(loanno: Int, cboID: Long): Double

    @Query("Select sum(amount) from vo_group_loan Where cboId=:cboid and mtgNo=:mtgNum and loanProductId=:fundType and loanSource=:LoanSource")
    fun getfundamt(cboid: Long, mtgNum: Int, fundType: Int, LoanSource: Int): Int

    @Query("Select sum(amount) from vo_group_loan Where cboId=:cboid and mtgNo=:mtgNum and  loanSource=:LoanSource")
    fun gettotamtbyloansource(cboid: Long, mtgNum: Int, LoanSource: Int): Int

    @Query("Select sum(amount) from vo_group_loan Where modePayment in (:modeofpayment) and mtgNo=:mtgno and cboId=:cbo_id and bankCode=:code")
    fun getgrouptotloanamtinbank(
        mtgno: Int,
        cbo_id: Long,
        code: String,
        modeofpayment: List<Int>
    ): Int

    @Query("Select sum(amount) from vo_group_loan Where modePayment IN(:modePayment) and mtgNo=:mtgno and cboId=:cbo_id")
    fun getgrouptotloanamtinbank(modePayment: List<Int>, mtgno: Int, cbo_id: Long): Int


    @Query("Select sum(amount) from vo_group_loan Where loanSource =:loansource and mtgNo=:mtgno and cboId=:cbo_id")
    fun getLoanRecAmount(loansource: Int, mtgno: Int, cbo_id: Long): Int

    @Query("Update vo_group_loan set isEdited=1, completionFlag=:completionflag where cboId=:cbo_id and loanNo=:loan_no")
    fun updateShgGroupLoan(cbo_id:Long,loan_no: Int, completionflag: Boolean)

    @Query("select * from vo_group_loan where isEdited=1 and cboId=:cbo_id")
    fun getUploadGroupLoanList(cbo_id: Long) : List<VoGroupLoanEntity>


}