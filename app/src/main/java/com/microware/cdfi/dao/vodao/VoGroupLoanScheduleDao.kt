package com.microware.cdfi.dao.vodao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.microware.cdfi.entity.voentity.VoGroupLoanScheduleEntity

@Dao
interface VoGroupLoanScheduleDao{

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertVoGroupLoanSchedule(VoGroupLoanSchedule: VoGroupLoanScheduleEntity?)

    @Query("Delete from vo_group_loan_schedule")
    fun deleteVoGroupLoanScheduleData()

    @Query("Select * from vo_group_loan_schedule")
    fun getVoGroupLoanScheduleData(): LiveData<List<VoGroupLoanScheduleEntity>>?

    @Query("Select * from vo_group_loan_schedule")
    fun getVoGroupLoanScheduleList(): List<VoGroupLoanScheduleEntity>?

//    @Query("Select sum(principalDemand) from vo_group_loan_schedule Where loan_no=:loanno and cbo_id=:cboid and installment_date>:lastmtgdate and installment_date<=:currentmtgdate and repaid=0 ")
    @Query("Select sum(principalDemand) from vo_group_loan_schedule Where loanNo =:loanno and cboId=:cboid and installmentDate>:lastmtgdate and installmentDate<=:currentmtgdate and repaid=0 ")
    fun getprincipaldemand(loanno:Int,cboid: Long, currentmtgdate: Long, lastmtgdate: Long): Int

//    @Query("Select sum(principal_demand) from shg_group_loan_schedule Where repaid=0 and loan_no=:loanno and cbo_id=:cboid  ")
    @Query("Select sum(principalDemand) from vo_group_loan_schedule Where repaid=0 and loanNo=:loanno and cboId=:cboid  ")
    fun gettotaloutstanding(loanno:Int, cboid: Long): Int

    @Query("Update vo_group_loan_schedule set repaid=0, loanPaid=0 where cboId=:cbo_id and mtgNo=:mtgno and  lastPaidDate=:mtgdate")
    fun deleteGroupRepaidData(mtgdate:Long,mtgno:Int,cbo_id:Long)

    @Query("Delete from vo_group_loan_schedule  where cboId=:cbo_id and mtgNo=:mtgno and  lastPaidDate=:mtgdate and subInstallmentNo>0")
    fun deleteGroupSubinstallmentData(mtgdate:Long,mtgno:Int,cbo_id:Long)

    @Query("Select principalDemand from vo_group_loan_schedule where cboid=:cboid and mtgGuid=:mtgGuid and loanno=:loanno and installmentNo=:installmentNo")
    fun getPrincipalDemandByInstallmentNum(cboid: Long,mtgGuid: String,loanno: Int,installmentNo:Int):Int

    @Query("Update vo_group_loan_schedule set principalDemand=:principalDemand,loanDemandOs=:principalDemand where mtgGuid=:mtgGuid and loanno=:loanno and installmentNo=:installmentNo and cboid=:cboid")
    fun updateCutOffLoanSchedule(
        mtgGuid: String,
        cboid: Long,
        loanno: Int,
        installmentNo: Int,
        principalDemand: Int
    )

    @Query("Select * from vo_group_loan_schedule Where cboId=:cboid and mtgGuid=:mtgguid and loanNo=:loanno")
    fun getGroupScheduledata(
        cboid: Long,
        mtgguid: String,
        loanno: Int
    ): List<VoGroupLoanScheduleEntity>?

    @Query(
        "Update vo_group_loan_schedule set principalDemand=:principaldemand, loanOs=:loanos, loanDemandOs=:loandemandos where cboId=:cboid and mtgGuid=:mtgguid and loanNo=:loanno and installmentNo=:installmentno"
    )
    fun updateLoanGroupSchedule(
        mtgguid: String,
        cboid: Long,
        loanno: Int,
        installmentno: Int,
        principaldemand: Int,
        loandemandos: Int,
        loanos: Int
    )

    @Query("Delete from vo_group_loan_schedule where cboId=:cbo_id")
    fun deleteVO_GroupScenduleLoanData(cbo_id: Long)


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllVoGroupLoanSchedule(VoGroupLoanSchedule: List<VoGroupLoanScheduleEntity>?)

    @Query("Select * from vo_group_loan_schedule where cboId=:cboId and loanNo=:loanNo")
    fun getLoanScheduleDetail(cboId: Long, loanNo: Int): List<VoGroupLoanScheduleEntity>?

    @Query("Select count(installmentNo) from vo_group_loan_schedule Where repaid=0 and loanNo=:loanno and cboId=:cboid  ")
    fun getremaininginstallment(loanno: Int, cboid: Long): Int

    @Query(
        "Update vo_group_loan_schedule set repaid=0, loanPaid=0 where cboId=:shgId  and loanNo=:loanno and mtgNo=:mtgno and  lastPaidDate=:mtgdate")
    fun deleterepaid(
        loanno: Int,
        shgId: Long,
        mtgdate: Long ,
        mtgno: Int  )

    @Query(
        "Delete from vo_group_loan_schedule  where cboId=:shgId  and loanNo=:loanno and mtgNo=:mtgno and  lastPaidDate=:mtgdate and subInstallmentNo>1"
    )
    fun deletesubinstallment(
        loanno: Int,
        shgId: Long,
        mtgdate: Long ,
        mtgno: Int  )

    @Query("Select * from vo_group_loan_schedule Where cboId=:shgId  and loanNo=:loanno and (repaid=0 or mtgNo=:mtgno) order by installmentNo")
    fun getMemberScheduleloanwise(
        shgId: Long,
        loanno: Int,
        mtgno: Int
    ): List<VoGroupLoanScheduleEntity>?

    @Query(
        "Update vo_group_loan_schedule set repaid=1,mtgNo=:mtgno, loanPaid=:loanpaid, lastPaidDate=:mtgdate, updatedBy=:updatedby, updatedOn=:updatedon where cboId=:shgId  and loanNo=:loanno and installmentNo=:installmentno and subInstallmentNo=:sub_installment_no"
    )
    fun updaterepaid(
        loanno: Int,
        shgId: Long,
        installmentno: Int,
        sub_installment_no: Int,
        loanpaid: Int,
        mtgdate: Long,
        mtgno: Int,
        updatedby: String?,
        updatedon: Long
    )

}