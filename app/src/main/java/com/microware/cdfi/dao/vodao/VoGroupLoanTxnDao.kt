package com.microware.cdfi.dao.vodao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.microware.cdfi.entity.voentity.VoGroupLoanTxnEntity

@Dao
interface VoGroupLoanTxnDao{

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertVoGroupLoanTxn(VoGroupLoanTxn: VoGroupLoanTxnEntity?)

    @Query("Delete from vo_group_loan_txn")
    fun deleteVoGroupLoanTxnData()

    @Query("Select * from vo_group_loan_txn")
    fun getVoGroupLoanTxnData(): LiveData<List<VoGroupLoanTxnEntity>>?

    @Query("Select * from vo_group_loan_txn")
    fun getVoGroupLoanTxnList(): List<VoGroupLoanTxnEntity>?

    @Query("select * from vo_group_loan_txn where mtgNo=:mtgno and cboId=:cbo_aid")
    fun getListDataByMtgnum(mtgno:Int,cbo_aid:Long): List<VoGroupLoanTxnEntity>

    @Query("select txn.* from vo_group_loan_txn txn inner join vo_group_loan loan on loan.cboId=txn.cboId where txn.mtgNo=:mtgno and txn.cboId=:cbo_aid and loan.loanSource=:LoanSorurce GROUP by loan.loanno")
    fun getListDataByLoansorce(mtgno:Int,cbo_aid:Long,LoanSorurce:Int): List<VoGroupLoanTxnEntity>

    @Query("Delete from vo_group_loan_txn where mtgNo=:mtg_no and cboId=:cbo_id")
    fun deleteGroupLoanTxnData(mtg_no:Int,cbo_id: Long)

    @Query("Select sum(loanPaidInt)+sum(loanPaid) from vo_group_loan_txn Where modePayment=1 and mtgNo=:mtgno and cboId=:cbo_id")
    fun gettotloanpaidgroup(mtgno:Int,cbo_id:Long): Int

    @Query("Select sum(loanPaidInt)+sum(loanPaid) from vo_group_loan_txn txn inner join vo_group_loan loan on txn.cboId=loan.cboId and txn.loanNo=loan.loanNo Where txn.mtgNo=:mtgno and txn.cboId=:cbo_id and loan.loanProductId=:auid and loan.loanSource=:LoanSource")
    fun getgrouploanpaidbyauid(mtgno: Int, cbo_id: Long, auid: Int, LoanSource: Int): Int


    @Query("Select sum(loanPaidInt)+sum(loanPaid) from vo_group_loan_txn txn inner join vo_group_loan loan on txn.cboId=loan.cboId and txn.loanNo=loan.loanNo Where txn.mtgNo=:mtgno and txn.cboId=:cbo_id  and loan.loanSource=:LoanSource")
    fun getgrouploanpaidbycbo(mtgno: Int, cbo_id: Long, LoanSource: Int): Int


    @Query("Delete from vo_group_loan_txn where cboId=:cbo_id")
    fun deleteVO_GroupLoanTxnData(cbo_id: Long)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllVoGroupLoanTxn(VoGroupLoanTxn: List<VoGroupLoanTxnEntity>?)


    @Query("Select (sum(principalDemandOb)+sum(principalDemand)+sum(intAccrued)+sum(intAccruedOp))-(sum(loanPaidInt)+sum(loanPaid)) as loan_paid from vo_group_loan_txn Where cboId=:shgId and mtgNo=:mtgno")
    fun gettotalpaid(shgId: Long,mtgno: Int): Int

    @Query("Select * from vo_group_loan_txn Where cboId=:shgId and mtgNo=:mtgno")
    fun getgrouploantxnlist(shgId: Long, mtgno: Int): List<VoGroupLoanTxnEntity>?

    @Query("Select * from vo_group_loan_txn Where cboId=:shgid and mtgNo=:mtgno and loanNo=:loanno")
    fun getgrouploandata(loanno: Int, mtgno: Int, shgid: Long): List<VoGroupLoanTxnEntity>?

    @Query("Update vo_group_loan_txn set completionFlag=:completionflag,loanPaid=:paid,loanPaidInt=:loanint,intAccruedCl=(intAccruedOp+intAccrued)-:loanint,loanClInt=loanOpInt+:loanint,loanCl=loanOp-:paid,principalDemandCb=:principaldemandcl,modePayment=:mode,bankCode=:bankaccount,transactionNo=:transactionno where loanNo=:loanno and mtgNo=:mtgno and cboId=:shgID")
    fun updateloanpaid(
        loanno: Int, mtgno: Int,
        shgID: Long,
        paid: Int,
        loanint: Int,
        mode: Int,
        bankaccount: String,
        transactionno: String,
        principaldemandcl: Int,
        completionflag: Boolean
    )

    @Query("select sum(amount) from vo_group_loan where  mtgNo=:mtg_no and  cboId=:cbo_id")
    fun getCutOffgroupLoanAmount(cbo_id: Long,mtg_no: Int):Int


    @Query("Select sum(loanPaid)+sum(loanPaidInt) from vo_group_loan_txn Where modePayment in (:modePayment) and mtgNo=:mtgno and cboId=:cbo_id and bankCode=:account")
    fun getgrptotloanpaidinbank(mtgno: Int, cbo_id: Long, account: String, modePayment: List<Int>): Int

    @Query("Select sum(loanPaid)+sum(loanPaidInt) from vo_group_loan_txn Where modePayment IN(:modePayment) and mtgNo=:mtgno and cboId=:cbo_id")
    fun getgrptotloanpaidinbank(modePayment: List<Int>, mtgno: Int, cbo_id: Long): Int


    @Query("Select sum(loanPaid)+sum(loanPaidInt) from vo_group_loan_txn where mtgNo=:mtgno and cboId=:cbo_id")
    fun getRepaymentClfAmount(mtgno: Int,cbo_id: Long):Int

    @Query("Select sum(principalDemand)+sum(principalDemandOb)+sum(intAccrued)+sum(intAccruedOp) from vo_group_loan_txn where mtgNo=:mtgno and cboId=:cbo_id")
    fun getTotalRepaymentClfAmount(mtgno: Int,cbo_id: Long):Int


}