package com.microware.cdfi.dao.vodao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.DtLoanApplicationEntity
import com.microware.cdfi.entity.voentity.VoLoanApplicationEntity

@Dao
interface VoLoanApplicationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFedLoanApplicationData(fedLoanApplicationEntity: VoLoanApplicationEntity?)

    @Query("Delete from vo_loan_application")
    fun deleteFedLoanApplicationData()

    @Query("Select * from vo_loan_application")
    fun getFedLoanApplicationData(): LiveData<List<VoLoanApplicationEntity>>?

    @Query("Select * from vo_loan_application")
    fun getFedLoanApplicationDataAll(): List<VoLoanApplicationEntity>?

    @Query("Delete from vo_loan_application where loanRequestedMtgNo=:mtg_no and cboId=:cbo_id")
    fun deleteVoLoanApplication(mtg_no: Int, cbo_id: Long)

    @Query("Select count(loanApplicationId) from vo_loan_application Where  cboId=:cboid")
    fun getmaxloanno(cboid: Long): Int

    @Query("Update vo_loan_application set amtDemand=:amt_disbursed, amtSanction=:amtsaction, loanRequestPriority=:priorty,approvalDate=:approvalDate, tentativeDate=:tentivedate where  loanApplicationId=:loanapplication_id ")
    fun updateMemberloanapplicationdetail(
        loanapplication_id: Long,
        amt_disbursed: Int,
        amtsaction: Int,
        priorty: Int,
        approvalDate: Long,
        tentivedate: Long
    )
    @Query("Update vo_loan_application set amtDisbursed=:amt, loanSanctionedMtgGuid=:mtgguid, loanSanctionedMtgNo=:mtgno where  loanApplicationId=:loanappid and cboId=:void and memId=:shgmemid ")
    fun updatevoshgloanapplication(
        mtgguid: String,
        shgmemid: Long,
        void: Long,
        mtgno: Int,
        loanappid: Long,
        amt: Int


    )
    @Query("select * from vo_loan_application where loanApplicationId=:loanapplication_id and loanRequestedMtgNo=:mtgNo and memId=:shgid")
    fun getmemberLoanApplication(loanapplication_id: Long,mtgNo:Int,shgid: Long): List<VoLoanApplicationEntity>?

    @Query("Select * from vo_loan_application Where loanRequestedMtgNo=:mtgno and cboId=:cboid")
    fun getLoanApplicationlist(mtgno: Int,cboid:Long): List<VoLoanApplicationEntity>?

    @Query("Select sum(amtDemand) from vo_loan_application Where loanRequestedMtgNo=:mtgno and cboId=:cboid")
    fun gettotaldemand(mtgno: Int,cboid:Long): Int

    @Query("Delete from vo_loan_application where cboId=:cbo_id")
    fun deleteVO_LoanAppData(cbo_id: Long)
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllFedLoanApplicationData(fedLoanApplicationEntity: List<VoLoanApplicationEntity>?)


    @Query("Select count(*) from vo_loan_application where loanRequestPriority=:priority and loanRequestedMtgNo=:mtg_no and cboId=:cbo_id")
    fun getPriorityCount(priority: Int,mtg_no: Int,cbo_id: Long):Int

    @Query("Select count(*) from vo_loan_application where memId=:mem_id and loanRequestedMtgNo=:mtgNum")
    fun getTotalLoanCount(mtgNum:Int,mem_id: Long):Int

    @Query("Select sum(amtSanction) from vo_loan_application Where loanRequestedMtgNo=:mtgno and cboId=:cboid and memId=:memId")
    fun getLoanOutstanding(mtgno: Int,cboid:Long,memId:Long): Int

//replace//

    @Query("Select * from vo_loan_application Where loanRequestedMtgNo=:mtgno and cboId=:cboid and memId=:memId and loanProductId=:Loanproductid")
    fun getLoanApplicationlist(mtgno: Int,cboid:Long, memId:Long,Loanproductid:Int): List<VoLoanApplicationEntity>?

    @Query("Select sum(amtDemand) from vo_loan_application Where loanRequestedMtgNo=:mtgno and cboId=:cboid and memId=:memId and loanProductId=:Loanproductid")
    fun gettotaldemand(mtgno: Int,cboid:Long,memId:Long,Loanproductid:Int): Int


    @Query("Select sum(amtDemand) from vo_loan_application Where loanRequestedMtgNo=:mtgno and cboId=:cboid")
    fun getdemand(mtgno: Int,cboid:Long): Int


}