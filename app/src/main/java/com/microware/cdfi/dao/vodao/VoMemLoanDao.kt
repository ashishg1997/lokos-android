package com.microware.cdfi.dao.vodao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.microware.cdfi.entity.voentity.VoMemLoanEntity

@Dao
interface VoMemLoanDao{

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertVoMemLoan(MemLoan: VoMemLoanEntity?)

    @Query("Delete from vo_mem_loan")
    fun deleteVoMemLoanData()

    @Query("Select * from vo_mem_loan")
    fun getVoMemLoanData(): LiveData<List<VoMemLoanEntity>>?

    @Query("Select * from vo_mem_loan")
    fun getVoMemLoanList(): List<VoMemLoanEntity>?

    @Query("Select interestRate from vo_mem_loan Where memId=:memId and loanNo=:loanNo")
    fun getinterestrate(loanNo: Int, memId: Long?): Double

    @Query("Delete from vo_mem_loan Where cboId=:cbo_id and mtgNo=:mtg_no")
    fun deleteMemberLoan(cbo_id: Long,mtg_no: Int)

    @Query("Select sum(amount) from vo_mem_loan Where modePayment=1 and mtgNo=:mtgno and cboId=:cbo_id")
    fun gettotloan(mtgno: Int, cbo_id: Long): Int

    @Query("Select * from vo_mem_loan Where loanNo=:loan_no and cboId=:cbo_id and memId=:mem_id and mtgGuid=:mtg_guid")
    fun getLoanDetailData(loan_no: Int,cbo_id: Long,mem_id: Long,mtg_guid:String): List<VoMemLoanEntity>?

    @Query("Select max(loanNo) from vo_mem_loan Where cboId=:cbo_id and mtgNo=:mtg_no")
    fun getCutOffmaxLoanno(cbo_id: Long,mtg_no: Int): Int

    @Query("Select loanNo from vo_mem_loan Where loanApplicationId=:loanappid")
    fun getLoanno(loanappid: Long): Int

    @Query("Select * from vo_mem_loan Where cboId=:cboId and mtgNo=:mtgNo")
    fun getLoanDetail(cboId: Long,mtgNo: Int): List<VoMemLoanEntity>?

    @Query("Select sum(originalLoanAmount) from vo_mem_loan where mtgNo=:mtgNum and memId=:mem_id and completionFlag=3")
    fun getTotalClosedLoanAmount(mtgNum:Int,mem_id: Long):Int

    @Query("Select count(*) from vo_mem_loan where mtgNo=:mtgNum and memId=:mem_id and completionFlag=3")
    fun getTotalClosedLoanCount(mtgNum:Int,mem_id: Long):Int

    @Query("Select max(loanNo) from vo_mem_loan Where cboId=:cbo_id")
    fun getmaxLoanno(cbo_id: Long): Int

    @Query("Select count(*) from vo_mem_loan where mtgNo=:mtgNum and memId=:mem_id")
    fun getTotalLoanCount(mtgNum:Int,mem_id: Long):Int

    @Query("select sum(originalLoanAmount) from vo_mem_loan where  mtgNo=:mtg_no and  cboId=:cbo_id and completionFlag=:completionFlag")
    fun getCutOffMemberLoanAmount(cbo_id: Long,mtg_no: Int,completionFlag:Boolean):Int


    @Query("Select sum(originalLoanAmount) from vo_mem_loan where mtgNo=:mtgNum and memId=:mem_id and completionFlag=:completionFlag")
    fun getTotalClosedLoanAmount(mtgNum: Int, mem_id: Long, completionFlag: Boolean): Int

    @Query("Select count(*) from vo_mem_loan where mtgNo=:mtgNum and memId=:mem_id and completionFlag=:completionFlag")
    fun getTotalClosedLoanCount(mtgNum: Int, mem_id: Long, completionFlag: Boolean): Int

    @Query("Select sum(originalLoanAmount) from vo_mem_loan where mtgNo=:mtgNum and memId=:mem_id and completionFlag=2")
    fun getTotalLoanAmount(mtgNum:Int,mem_id: Long):Int

    @Query("Select sum(amount) from vo_mem_loan where mtgNo=:mtgNum and memId=:mem_id and completionFlag=2")
    fun getTotaloutstandingprinciple(mtgNum:Int,mem_id: Long):Int

    @Query("Delete from vo_mem_loan where cboId=:cbo_id")
    fun deleteVO_MemLoanData(cbo_id: Long)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllVoMemLoan(MemLoan: List<VoMemLoanEntity>?)


    @Query("Select amount from vo_mem_loan where mtgNo=:mtgNum and memId=:mem_id")
    fun getAmountPaid(mtgNum:Int,mem_id: Long):Int

    @Query("Select amount from vo_mem_loan Where loanNo=:loanno and memId=:member_id")
    fun gettotamt(loanno: Int,member_id:Long): Int

    @Query("Select sum(amount) from vo_mem_loan Where memId=:shgId and mtgNo=:mtgNum and fundType=:fundType")
    fun getfundamt(shgId:Long,mtgNum: Int,fundType:Int): Int


    @Query("Select sum(amount) from vo_mem_loan Where cboId=:cboId and mtgNo=:mtgNum ")
    fun gettotcboamt(cboId:Long,mtgNum: Int): Int

    @Query("select sum(amount) from vo_mem_loan where  modePayment in (:modeofpayment) and mtgNo=:mtg_no and  cboId=:cbo_id and bankCode=:bank_code")
    fun getMemLoanDisbursedAmount(
        cbo_id: Long,
        mtg_no: Int,
        bank_code: String,
        modeofpayment: List<Int>
    ):Int

    @Query("select * from vo_mem_loan where mtgNo=:mtgno and cboId=:cbo_id and memId=:mem_id")
    fun getloanListSummeryData(mtgno: Int, cbo_id: Long,mem_id: Long): List<VoMemLoanEntity>

    @Query("select sum(amount) from vo_mem_loan where modePayment IN(:modePayment) and mtgNo=:mtg_no and cboId=:cbo_id")
    fun getMemLoanDisbursedAmount(
        modePayment: List<Int>,
        cbo_id: Long,
        mtg_no: Int
    ): Int

    @Query("Select noOfLoans from vo_mem_loan where mtgNo=:mtgNum and memId=:mem_id and completionFlag=:completionFlag")
    fun getTotalClosedLoan(mtgNum:Int,mem_id: Long,completionFlag:Boolean):Int

    @Query("Select originalLoanAmount from vo_mem_loan where mtgNo=:mtgNum and memId=:mem_id and completionFlag=:completionFlag")
    fun getClosedLoanAmount(mtgNum:Int,mem_id: Long,completionFlag: Boolean):Int

    @Query("Update vo_mem_loan set isEdited=1,completionFlag=:completionflag where cboId=:cbo_id and memId=:mem_id and loanNo=:loan_no")
    fun updateMemberLoanEditFlag(cbo_id: Long, mem_id: Long, loan_no: Int, completionflag: Boolean)


    @Query("Select * from vo_mem_loan Where isEdited=1 and cboId=:cboId ")
    fun getuploadlist(cboId: Long): List<VoMemLoanEntity>?

}