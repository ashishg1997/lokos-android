package com.microware.cdfi.dao.vodao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.microware.cdfi.entity.voentity.VoMemLoanScheduleEntity

@Dao
interface VoMemLoanScheduleDao{

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertVoMemLoanSchedule(MemLoanSchedule: VoMemLoanScheduleEntity?)

    @Query("Delete from vo_mem_loan_schedule")
    fun deleteVoMemLoanScheduleData()

    @Query("Select * from vo_mem_loan_schedule")
    fun getVoMemLoanScheduleData(): LiveData<List<VoMemLoanScheduleEntity>>?

    @Query("Select * from vo_mem_loan_schedule")
    fun getVoMemLoanScheduleList(): List<VoMemLoanScheduleEntity>?

//    @Query("Select sum(principalDemand) from vo_mem_loan_schedule Where loan_no=:loanno and memId=:mem_id and installmentDate>:lastmtgdate and installmentDate<=:currentmtgdate and repaid=0 ")
    @Query("Select sum(principalDemand) from vo_mem_loan_schedule Where  memId=:mem_id and loanNo=:loanNo and installmentDate>:lastmtgdate and installmentDate<=:currentmtgdate and repaid=0 ")
    fun getprincipaldemand(loanNo: Int, mem_id: Long, currentmtgdate: Long, lastmtgdate: Long): Int

//    @Query("Select sum(principalDemand) from vo_mem_loan_schedule Where repaid=0 and loan_no=:loanno and mem_id=:mem_id  ")
    @Query("Select sum(principalDemand) from vo_mem_loan_schedule Where repaid=0 and memId=:mem_id and loanNo=:loanNo  ")
    fun gettotaloutstanding(loanNo: Int, mem_id: Long): Int

    @Query("Update vo_mem_loan_schedule set repaid=0, loanPaid=0 where cboId=:cbo_id and mtgNo=:mtgno and lastPaidDate=:mtgdate")
    fun deleteMemberRepaidData(mtgdate: Long,mtgno: Int,cbo_id: Long)

    @Query("Delete from vo_mem_loan_schedule  where cboId=:cbo_id and mtgNo=:mtgno and  lastPaidDate=:mtgdate and subInstallmentNo>0")
    fun deleteMemberSubinstallmentData(mtgdate: Long,mtgno: Int,cbo_id: Long)

    @Query("Select principalDemand from vo_mem_loan_schedule where memId=:memId and mtgGuid=:mtgGuid and loanNo=:loanNo and installmentNo=:installmenNo")
    fun getPrincipalDemandByInstallmentNum(memId: Long, mtgGuid: String, loanNo: Int, installmenNo: Int): Int

    @Query(
        "Update vo_mem_loan_schedule set principalDemand=:principaldemand,loanDemandOs=:principaldemand where memId=:memId and mtgGuid=:mtgguid and loanNo=:loanno and installmentNo=:installmentno"
    )
    fun updateCutOffLoanMemberSchedule(
        mtgguid: String,
        memId: Long,
        loanno: Int,
        installmentno: Int,
        principaldemand: Int
    )

    @Query("Select * from vo_mem_loan_schedule Where memId=:mem_id and mtgGuid=:mtgguid and loanNo=:loanno")
    fun getMemberScheduledata(
        mem_id: Long,
        mtgguid: String,
        loanno: Int
    ): List<VoMemLoanScheduleEntity>?

    @Query(
        "Update vo_mem_loan_schedule set principalDemand=:principaldemand, loanOs=:loanos, loanDemandOs=:loandemandos where memId=:mem_id and mtgGuid=:mtgguid and loanNo=:loanno and installmentNo=:installmentno")

    fun updateLoanMemberSchedule(
        mtgguid: String,
        mem_id: Long,
        loanno: Int,
        installmentno: Int,
        principaldemand: Int,
        loandemandos: Int,
        loanos: Int
    )

    @Query("Select sum(principalDemand) from vo_mem_loan_schedule Where loanNo=:loanno and memId=:mem_id and mtgGuid=:mtgguid ")
    fun gettotalloanamt(loanno: Int, mem_id: Long, mtgguid: String): Int

    @Query("Delete from vo_mem_loan_schedule where cboId=:cbo_id")
    fun deleteVO_MemLoanSceduleData(cbo_id: Long)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllVoMemLoanSchedule(MemLoanSchedule: List<VoMemLoanScheduleEntity>?)

    @Query("Select * from vo_mem_loan_schedule where cboId=:cboId and loanNo=:loanNo")
    fun getLoanScheduleDetail(cboId: Long, loanNo: Int): List<VoMemLoanScheduleEntity>?

    @Query("Select principalDemand from vo_mem_loan_schedule where loanNo=:loanno and memId=:mem_id and repaid=0 limit 1")
    fun getnextdemand(mem_id: Long,loanno:Int): Int


    @Query("Select count(installmentNo) from vo_mem_loan_schedule Where repaid=0 and loanNo=:loanno and memId=:mem_id  ")
    fun getremaininginstallment(loanno: Int, mem_id: Long): Int

    @Query(
        "Update vo_mem_loan_schedule set repaid=0, loanPaid=0 where memId=:mem_id  and loanNo=:loanno and mtgNo=:mtgno and  lastPaidDate=:mtgdate")
    fun deleterepaid(
        loanno: Int,
        mem_id: Long,
        mtgdate: Long ,
        mtgno: Int  )

    @Query(
        "Delete from vo_mem_loan_schedule  where memId=:mem_id  and loanNo=:loanno and mtgNo=:mtgno and  lastPaidDate=:mtgdate and subInstallmentNo>1"
    )
    fun deletesubinstallment(
        loanno: Int,
        mem_id: Long,
        mtgdate: Long ,
        mtgno: Int  )

    @Query("Select * from vo_mem_loan_schedule Where memId=:mem_id  and loanNo=:loanno and (repaid=0 or mtgNo=:mtgno) order by installmentNo")
    fun getMemberScheduleloanwise(
        mem_id: Long,
        loanno: Int,
        mtgno: Int
    ): List<VoMemLoanScheduleEntity>?

    @Query(
        "Update vo_mem_loan_schedule set repaid=1,mtgNo=:mtgno, loanPaid=:loanpaid, lastPaidDate=:mtgdate, updatedBy=:updatedby, updatedOn=:updatedon where memId=:mem_id  and loanNo=:loanno and installmentNo=:installmentno and subInstallmentNo=:subInstallmentNo"
    )
    fun updaterepaid(
        loanno: Int,
        mem_id: Long,
        installmentno: Int,
        subInstallmentNo: Int,
        loanpaid: Int,
        mtgdate: Long,
        mtgno: Int,
        updatedby: String?,
        updatedon: Long
    )




    @Query("Select sum(principalDemand) from vo_mem_loan_schedule Where repaid=0  and memId=:mem_id  ")
    fun gettotalloanoutstanding(mem_id: Long): Int
}