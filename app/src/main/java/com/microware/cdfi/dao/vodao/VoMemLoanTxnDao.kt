package com.microware.cdfi.dao.vodao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.microware.cdfi.api.vomodel.VoLoanRepaymentListModel
import com.microware.cdfi.entity.voentity.VoMemLoanTxnEntity

@Dao
interface VoMemLoanTxnDao{

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertVoMemLoanTxn(MemLoanTxn: VoMemLoanTxnEntity?)

    @Query("Delete from vo_mem_loan_txn")
    fun deleteVoMemLoanTxnData()

    @Query("Select * from vo_mem_loan_txn")
    fun getVoMemLoanTxnData(): LiveData<List<VoMemLoanTxnEntity>>?

    @Query("Select * from vo_mem_loan_txn")
    fun getVoMemLoanTxnList(): List<VoMemLoanTxnEntity>?

    @Query("Select * from vo_mem_loan_txn Where cboId=:cboid and memId=:mem_id and mtgNo=:mtgno")
    fun getmeetingLoanTxnMemdata(cboid: Long,mem_id: Long, mtgno: Int): List<VoMemLoanTxnEntity>?

    @Query("Delete from vo_mem_loan_txn where mtgNo=:mtg_no and cboId=:cbo_id")
    fun deleteMemberLoanTxnData(mtg_no: Int,cbo_id: Long)

    @Query("Select sum(loanPaidInt)+sum(loanPaid) from vo_mem_loan_txn Where  mtgNo=:mtgno and cboId=:cbo_id")
    fun gettotloanpaid(mtgno:Int,cbo_id:Long): Int

  @Query("Select sum(loanPaidInt)+sum(loanPaid) from vo_mem_loan_txn txn inner join vo_mem_loan loan on txn.memid=loan.memid and txn.loanNo=loan.loanNo Where txn.mtgNo=:mtgno and txn.memId=:memid and loan.fundType=:auid")
    fun getloanpaidbyauid(mtgno:Int,memid:Long,auid:Int): Int

    @Query("Delete from vo_mem_loan_txn where cboId=:cboId")
    fun deleteVO_memLoanTxnData(cboId: Long)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllVoMemLoanTxn(MemLoanTxn: List<VoMemLoanTxnEntity>?)

    @Query("Select count(loanNo) as loanNo,loanOp,loanOpInt,sum(loanPaid) as loan_paid,sum(loanPaidInt) as loan_paid_int,sum(principalDemand) as principal_demand,sum(principalDemandOb) as principal_demand_ob,sum(principalDemandCb)  as principal_demand_cb,sum(intAccrued)  as int_accrued ,sum(intAccruedOp)  as int_accrued_op from vo_mem_loan_txn Where memId=:memid and mtgNo=:mtgno")
    fun getmemberloan(memid: Long, mtgno: Int): List<VoLoanRepaymentListModel>?

    @Query("Select txn.* from vo_mem_loan_txn txn inner join vo_mem_loan loan on txn.loanNo=loan.loanNo and txn.memId=loan.memId Where txn.memId=:memid and txn.mtgNo=:mtgno and loan.fundType=:fundtype")
    fun getmemberloantxnlist(memid: Long, mtgno: Int, fundtype: Int): List<VoMemLoanTxnEntity>?

    @Query("Select (sum(principalDemandOb)+sum(principalDemand)+sum(intAccrued)+sum(intAccruedOp))-(sum(loanPaidInt)+sum(loanPaid)) as loan_paid from vo_mem_loan_txn txn inner join vo_mem_loan loan on txn.loanNo=loan.loanNo and txn.memId=loan.memId Where txn.memId=:mem_id and txn.mtgNo=:mtgno and loan.fundType=:fundtype")
    fun gettotalpaid(mem_id: Long, mtgno: Int, fundtype: Int): Int

    @Query("Select * from vo_mem_loan_txn Where memId=:memid and mtgNo=:mtgno and loanNo=:loanno")
    fun getmemberloandata(loanno: Int, mtgno: Int, memid: Long): List<VoMemLoanTxnEntity>?

    @Query("Update vo_mem_loan_txn set loanPaid=:paid,completionFlag=:completionflag,loanPaidInt=:loanint,intAccruedCl=(intAccruedOp+intAccrued)-:loanint,loanClInt=loanOpInt+:loanint,loanCl=loanOp-:paid,principalDemandCb=:principaldemandcl,modePayment=:mode,bankCode=:bankaccount,transactionNo=:transactionno where loanNo=:loanno and mtgNo=:mtgno and memId=:memid")
    fun updateloanpaid(
        loanno: Int, mtgno: Int, memid: Long, paid: Int, loanint: Int,
        mode: Int,
        bankaccount: String,
        transactionno: String,
        principaldemandcl: Int,
        completionflag: Boolean
    )

    @Query("Select sum(loanPaid)+sum(loanPaidInt) from vo_mem_loan_txn Where modePayment in (:modePayment) and mtgNo=:mtgno and cboId=:cbo_id and bankCode=:account")
    fun getmemtotloanpaidinbank(mtgno: Int, cbo_id: Long, account: String, modePayment: List<Int>): Int


    @Query("Select (sum(loanPaid)) from vo_mem_loan_txn where cboId=:cboid and mtgNo=:mtgno and memId=:mem_id")
    fun getMemberLoanRepaymentAmount(cboid: Long, mtgno: Int,mem_id: Long):Int

    @Query("Select (sum(loanPaidInt)) from vo_mem_loan_txn where cboId=:cboid and mtgNo=:mtgno and memId=:mem_id")
    fun getMemberLoanRepaymentIntAmount(cboid: Long, mtgno: Int,mem_id: Long):Int

    @Query("Select (sum(loanOp)) from vo_mem_loan_txn where cboId=:cboid and mtgNo=:mtgno and memId=:mem_id")
    fun getMemberLoanRepaymentOPAmount(cboid: Long, mtgno: Int,mem_id: Long):Int

    @Query("Select sum(loanPaid)+sum(loanPaidInt) from vo_mem_loan_txn Where modePayment IN(:modePayment) and mtgNo=:mtgno and cboId=:cbo_id")
    fun getmemtotloanpaidinbank(modePayment: List<Int>, mtgno: Int, cbo_id: Long): Int

    @Query("Select sum(loanPaid)+sum(loanPaidInt) from vo_mem_loan_txn where cboId=:cboid and mtgNo=:mtgNo")
    fun getLoanRepayReceived(cboid: Long, mtgNo: Int):Int

    @Query("Select sum(principalDemandOb)+sum(principalDemand) from vo_mem_loan_txn where cboId=:cboid and mtgNo=:mtgNo")
    fun getTotalLoanRepayReceived(cboid: Long, mtgNo: Int):Int

    @Query("select * from vo_mem_loan_txn where mtgNo=:mtgno and cboId=:cbo_aid")
    fun getListDataByMtgnum(mtgno:Int,cbo_aid:Long): List<VoMemLoanTxnEntity>

    @Query("Select (sum(principalDemandOb)+sum(principalDemand)+sum(intAccrued)+sum(intAccruedOp))-(sum(loanPaidInt)+sum(loanPaid)) as loan_paid from vo_mem_loan_txn Where memId=:mem_id and mtgNo=:mtgno")
    fun gettotalpaid1(mem_id: Long,mtgno: Int): Int

    @Query("Select * from vo_mem_loan_txn Where memId=:memid and mtgNo=:mtgno")
    fun getmemberloantxnlist1(memid: Long, mtgno: Int): List<VoMemLoanTxnEntity>?

}