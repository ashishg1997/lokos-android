package com.microware.cdfi.dao.vodao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.microware.cdfi.entity.voentity.VoMemSettlementEntity

@Dao
interface VoMemSettlementDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSettlement(dtMemSettlementEntity:  List<VoMemSettlementEntity>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSettlementData(dtMemSettlementEntity : VoMemSettlementEntity?)

    @Query("Select * from vo_mem_settlement Where memId=:memid and mtgNo=:mtgno")
    fun getSettlementdata(memid: Long?,mtgno:Int): List<VoMemSettlementEntity>?

    @Query("Delete from vo_mem_settlement where mtgNo=:mtgno")
    fun deleteRecord(mtgno: Int)

    @Query("DELETE From vo_mem_settlement")
    fun deleteAll()

    @Query("Select * from vo_mem_settlement Where cboId=:cboId and memId=:memid and mtgNo=:mtgno")
    fun getSettlementuploaddata(cboId:Long?,memid: Long?,mtgno:Int): List<VoMemSettlementEntity>?


}