package com.microware.cdfi.dao.vodao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.microware.cdfi.api.vomodel.VoDetailDataModel
import com.microware.cdfi.api.vomodel.VoLoanListModel
import com.microware.cdfi.api.vomodel.VoLoanRepaymentListModel
import com.microware.cdfi.api.vomodel.VoShareCapitalModel
import com.microware.cdfi.entity.DtmtgDetEntity
import com.microware.cdfi.entity.voentity.VoMtgDetEntity

@Dao
interface VoMtgDetDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertVoMtgDet(MtgDet: VoMtgDetEntity?)

    @Query("Delete from vo_mtg_det")
    fun deleteVoMtgDetData()

    @Query("Select * from vo_mtg_det")
    fun getVoMtgDetData(): LiveData<List<VoMtgDetEntity>>?

    @Query("Select * from vo_mtg_det")
    fun getVoMtgDetList(): List<VoMtgDetEntity>?

    @Query("select * from vo_mtg_det where cboId = :shg_id and mtgNo=:mtgnum order by mtgNo")
    fun getMtgByMtgnum(shg_id: Long?, mtgnum: Int): List<VoMtgDetEntity>

    @Query("select * from vo_mtg_det where memId = :memid and mtgNo=:mtgnum ")
    fun getmemdata(memid: Long?, mtgnum: Int): List<VoMtgDetEntity>

    /*@Query("Update vo_mtg_det set loan1_no=:loanno,loan1_ob=:loanop,loan1_int_accrued=:loan_int_accrued where mtgNo=:mtgno  and memId=:mem_id")
    fun updateloandata1(mem_id: Long, mtgno: Int, loanno: Int, loanop: Int, loan_int_accrued: Int)

    @Query("Update vo_mtg_det set loan2_no=:loanno,loan2_ob=:loanop,loan1_int_accrued=:loan_int_accrued  where mtgNo=:mtgno  and memId=:mem_id")
    fun updateloandata2(mem_id: Long, mtgno: Int, loanno: Int, loanop: Int, loan_int_accrued: Int)

    @Query("Update vo_mtg_det set loan3_no=:loanno,loan3_ob=:loanop,loan1_int_accrued=:loan_int_accrued  where mtgNo=:mtgno  and memId=:mem_id")
    fun updateloandata3(mem_id: Long, mtgno: Int, loanno: Int, loanop: Int, loan_int_accrued: Int)

    @Query("Update vo_mtg_det set loan4_no=:loanno,loan4_ob=:loanop,loan1_int_accrued=:loan_int_accrued  where mtgNo=:mtgno  and memId=:mem_id")
    fun updateloandata4(mem_id: Long, mtgno: Int, loanno: Int, loanop: Int, loan_int_accrued: Int)

    @Query("Update vo_mtg_det set loan5_no=:loanno,loan5_ob=:loanop,loan1_int_accrued=:loan_int_accrued  where mtgNo=:mtgno  and memId=:mem_id")
    fun updateloandata5(mem_id: Long, mtgno: Int, loanno: Int, loanop: Int, loan_int_accrued: Int)*/


    @Query("Delete from vo_mtg_det where mtgNo=:mtg_no and cboId=:cbo_id")
    fun deleteVODetailData(mtg_no: Int, cbo_id: Long)


    @Query("Update vo_mtg_det set updatedBy=:updated_by,updatedOn=:updated_on,ec1=:EC1 where mtgNo=:mtgno and cboId=:cbo_id and memId=:memId")
    fun updateAttendanceEC1(
        EC1: String,
        mtgno: Int,
        cbo_id: Long,
        memId: Long,
        updated_by: String,
        updated_on: Long
    )

    @Query("Update vo_mtg_det set updatedBy=:updated_by,updatedOn=:updated_on,ec2=:EC2 where mtgNo=:mtgno and cboId=:cbo_id and memId=:memId")
    fun updateAttendanceEC2(
        EC2: String,
        mtgno: Int,
        cbo_id: Long,
        memId: Long,
        updated_by: String,
        updated_on: Long
    )

    @Query("Update vo_mtg_det set updatedBy=:updated_by,updatedOn=:updated_on,ec3=:EC3 where mtgNo=:mtgno and cboId=:cbo_id and memId=:memId")
    fun updateAttendanceEC3(
        EC3: String,
        mtgno: Int,
        cbo_id: Long,
        memId: Long,
        updated_by: String,
        updated_on: Long
    )

    @Query("Update vo_mtg_det set updatedBy=:updated_by,updatedOn=:updated_on,attendanceOther=:attendanceOther where mtgNo=:mtgno and cboId=:cbo_id and memId=:memId")
    fun updateAttendanceOther(
        attendanceOther: Int,
        mtgno: Int,
        cbo_id: Long,
        memId: Long,
        updated_by: String,
        updated_on: Long
    )

    @Query("Select * from vo_mtg_det where mtgNo=:mtgno and cboId=:cbo_id")
    fun getListDataMtgByMtgnum(mtgno: Int, cbo_id: Long): List<VoMtgDetEntity>

    @Query("Update vo_mtg_det set updatedBy=:updatedBy,updatedOn=:updatedOn,attendance=:attendance where mtgNo=:mtgNo and cboId=:cboId and memId=:memId")
    fun updateAttendance(
        attendance: String,
        mtgNo: Int,
        cboId: Long,
        memId: Long,
        updatedBy: String?,
        updatedOn: Long
    )

    @Query("select count(*) from vo_mtg_det where attendance=:attendance and mtgNo=:mtgNo and cboId=:cboId")
    fun getTotalPresentAndAbsent(attendance: String, mtgNo: Int, cboId: Long): Int

    @Query("select vodtmtg.uid,vodtmtg.cboId,vodtmtg.memId,vodtmtg.mtgGuid,vodtmtg.mtgNo,vodtmtg.childCboName, auid,type,amount, dateRealisation,modePayment, transactionNo from vo_mtg_det vodtmtg left join vo_fin_txn_det_mem vodtFinTxn on vodtmtg.memId=vodtFinTxn.memId and vodtFinTxn.auid =:receiptType and vodtFinTxn.mtgNo=:mtgNo where vodtmtg.mtgNo=:mtgNo and vodtmtg.cboId=:cboId")
    fun getShareCapitalListByReceiptType(
        mtgNo: Int,
        cboId: Long,
        receiptType: Int
    ): List<VoShareCapitalModel>

    @Query("Update vo_mtg_det set updatedBy=:updatedBy,updatedOn=:updatedOn,zeroMtgAttn=:attendance where mtgNo=:mtgNo and cboId=:cboId and memId=:memId")
    fun updateCutOffAttendance(
        attendance: String,
        mtgNo: Int,
        cboId: Long,
        memId: Long,
        updatedBy: String?,
        updatedOn: Long
    )

    @Query("select sum(zeroMtgAttn) from vo_mtg_det where mtgNo=:mtgno and cboId=:cbo_id")
    fun getCutOffTotalPresent(mtgno: Int, cbo_id: Long): Int


    @Query("select vomtg.uid,vomtg.cboId,vomtg.memId, vomtg.mtgGuid,vomtg.mtgNo,vomtg.mtgDate, vomtg.childCboName,loanApplicationId,requestDate,loanFee,amtDemand,amtSanction, amtDisbursed,approvalDate, tentativeDate from vo_mtg_det vomtg left join vo_loan_application voloan on vomtg.memId=voloan.memId  where  vomtg.mtgNo=:mtgno and  vomtg.cboId=:cbo_id and  vomtg.memId=:memId and  voloan.loanProductId=:Loanproductid")
    fun getloanListDataMtgByMtgnum(
        mtgno: Int,
        cbo_id: Long,
        memId: Long,
        Loanproductid: Int
    ): List<VoLoanListModel>

    @Query("select * from vo_mtg_det where mtgNo=:mtgno and cboId=:cbo_id and memId not in(:memid)")
    fun getListDatawithoutmember(mtgno: Int, cbo_id: Long, memid: Long): List<VoMtgDetEntity>

    @Query("Delete from vo_mtg_det where cboId=:cbo_id")
    fun deleteVo_MtgByVoId(cbo_id: Long)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllVoMtgDet(MtgDet: List<VoMtgDetEntity>?)

    @Query("Update vo_mtg_det set updatedBy=:updated_by,updatedOn=:updated_on,ec4=:EC4 where mtgNo=:mtgno and cboId=:cbo_id and memId=:memId")
    fun updateAttendanceEC4(
        EC4: String,
        mtgno: Int,
        cbo_id: Long,
        memId: Long,
        updated_by: String,
        updated_on: Long
    )

    @Query("Update vo_mtg_det set updatedBy=:updated_by,updatedOn=:updated_on,ec5=:EC5 where mtgNo=:mtgno and cboId=:cbo_id and memId=:memId")
    fun updateAttendanceEC5(
        EC5: String,
        mtgno: Int,
        cbo_id: Long,
        memId: Long,
        updated_by: String,
        updated_on: Long
    )

    @Query("select (sum(attendance)) from vo_mtg_det where mtgNo=:mtgno and cboId=:cbo_id and attendance=1")
    fun getshgsumattendance(mtgno: Int, cbo_id: Long): Int


    @Query("select dtmtg.uid,dtmtg.cboId,dtmtg.memId,dtmtg.mtgGuid,dtmtg.mtgNo,dtmtg.mtgDate, dtmtg.childCboName,loanNo,loanOp,loanOpInt,sum(loanPaid) as loan_paid,sum(loanPaidInt) as loan_paid_int,sum(principalDemand) as principal_demand,sum(principalDemandOb) as principal_demand_ob,sum(principalDemandCb) as principal_demand_cb from vo_mtg_det dtmtg left join vo_mem_loan_txn dtloantxn on dtmtg.memId=dtloantxn.memId where dtmtg.mtgNo=:mtgNo and dtmtg.memId=:memId group by dtmtg.memId")
    fun getloanrepaymentList(mtgNo: Int, memId: Long): List<VoLoanRepaymentListModel>


    @Query("select vomtg.uid,vomtg.cboId,vomtg.memId, vomtg.mtgGuid,vomtg.mtgNo,vomtg.mtgDate, vomtg.childCboName,voloan.loanNo,voloan.fundType,voloan.amount as amtDisbursed,voloan.originalLoanAmount as original_loan_amount,voloan.noOfLoans from vo_mtg_det vomtg left join vo_mem_loan voloan on vomtg.memId=voloan.memId and voloan.mtgNo=:mtgno  and completionFlag=:completionFlag where  vomtg.mtgNo=:mtgno and  vomtg.cboId=:cbo_id group by vomtg.memId")
    fun getCutOffClosedMemberLoanDataByMtgNum(
        mtgno: Int,
        cbo_id: Long,
        completionFlag: Boolean
    ): List<VoLoanListModel>


    @Query("select vomtg.uid,vomtg.cboId,vomtg.memId, vomtg.mtgGuid,vomtg.mtgNo,vomtg.mtgDate, vomtg.childCboName,loanNo as loanNo,amount as amt_disbursed,originalLoanAmount as original_loan_amount,fundType as fundType from vo_mtg_det vomtg left join vo_mem_loan voloan on vomtg.memId=voloan.memId and voloan.mtgNo=:mtgno  and completionFlag=:completionFlag where  vomtg.mtgNo=:mtgno and  vomtg.cboId=:cbo_id and vomtg.memId=:memberid")
    fun getClosedMemberLoanByMemberId(
        mtgno: Int,
        cbo_id: Long,
        completionFlag: Boolean,
        memberid: Long
    ): List<VoLoanListModel>

    @Query("select ec1 + ec2 + ec3+ ec4 +ec5 + attendanceOther as Total from vo_mtg_det where mtgNo=:mtgno and cboId=:cbo_id  and attendance=:attendance")
    fun getECPresent(mtgno: Int, cbo_id: Long, attendance: String): List<Int>


    @Query("select count(*) from vo_mtg_det where mtgNo=:mtgNo and cboId=:cboId")
    fun getTotalPresentAbsent(mtgNo: Int, cboId: Long): Int

    @Query("select count(*) from vo_mtg_det where mtgNo=:mtgno and cboId=:cbo_id and (ec1 =1 or ec2 =1 or ec3 =1 or ec4 =1 or ec5 =1 or attendanceOther=1 or attendanceOther=2 or attendanceOther=3)")
    fun getECPresent(mtgno: Int, cbo_id: Long): Int

    @Query("select sum(savVol+savVolOb) from vo_mtg_det where mtgNo=:mtgno  and memId=:mem_id")
    fun getVolsaving(mem_id: Long, mtgno: Int): Int


    @Query("select sum(savVolCb) from vo_mtg_det where mtgNo=:mtgno  and memId=:mem_id")
    fun getVolclosing(mem_id: Long, mtgno: Int): Int

    @Query("select sum(savCompCb) from vo_mtg_det where mtgNo=:mtgno  and memId=:mem_id")
    fun getCompclosing(mem_id: Long, mtgno: Int): Int


    @Query("Update vo_mtg_det set savVolWithdrawal=:Withdrawal ,savVolCb=(savVol+savVolOb)-:Withdrawal where mtgNo=:mtgno  and memId=:mem_id")
    fun updateWithdrawal(mem_id: Long, mtgno: Int, Withdrawal: Int)

    @Query("Update vo_mtg_det set savVol=:sav_vol_cb ,savVolCb=savVolOb+:sav_vol_cb where mtgNo=:mtgno  and memId=:mem_id")
    fun updatevolclosing(mem_id: Long, mtgno: Int, sav_vol_cb: Int)

    @Query("Update vo_mtg_det set savComp=:sav_comp_cb ,savCompCb=savCompOb+:sav_comp_cb where mtgNo=:mtgno  and memId=:mem_id")
    fun updateCompclosing(mem_id: Long, mtgno: Int, sav_comp_cb: Int)

    @Query("Update vo_mtg_det set savVolCb=(savVolOb+savVol)+:sav_vol_cb where mtgNo=:mtgno  and memId=:mem_id")
    fun adjustvolclosing(mem_id: Long, mtgno: Int, sav_vol_cb: Int)

    @Query("Update vo_mtg_det set savCompCb=(savCompOb+savComp)+:sav_comp_cb where mtgNo=:mtgno  and memId=:mem_id")
    fun  adjustCompclosing(mem_id: Long, mtgno: Int, sav_comp_cb: Int)

    @Query("select * from vo_mtg_det where mtgNo=:mtgno and cboId=:cbo_id and status='2' ")
    fun getListinactivemember(mtgno: Int, cbo_id: Long): List<VoMtgDetEntity>

    @Query("select * from vo_mtg_det where mtgNo=:mtgno and cboId=:cbo_id and memId=:mem_id")
    fun getCompulsoryData(mtgno: Int, cbo_id: Long,mem_id: Long): List<VoMtgDetEntity>

    @Query("Update vo_mtg_det set updatedBy=:updated_by,updatedOn=:updated_on,savVolWithdrawal=:withdrawl where mtgNo=:mtgno and cboId=:cbo_id and memId=:memid")
    fun updatesavingwithdrawl(
        withdrawl: Int,
        mtgno: Int,
        cbo_id: Long,
        memid: Long,
        updated_by:String,
        updated_on:Long
    )
}