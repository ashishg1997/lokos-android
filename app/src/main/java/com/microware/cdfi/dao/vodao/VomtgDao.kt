package com.microware.cdfi.dao.vodao

import androidx.room.*
import com.microware.cdfi.api.vomodel.VoMeetingModel
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.entity.voentity.VomtgEntity

@Dao
interface VomtgDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFedmtg(dtmtg: VomtgEntity?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
     fun insertFedmtg1(dtmtg: VomtgEntity?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllFedmtg(dtmtg: List<VomtgEntity>?)

    @Query("select count(*) from vo_mtg where cboId = :cbo_id")
    fun getmeetingCount(cbo_id: String?): Int

    @Query("Select max(mtgNo) from vo_mtg where cboId=:shg_id")
    fun getMaxMeetingNum(shg_id: Long?): Int

    @Query("Select count(*) from vo_mtg where cboId=:federationId")
    fun getMeetingCount(federationId: Long?): Int

    @Query("SELECT distinct tblvo.*,voMtg.* FROM federation_profile tblvo LEFT JOIN  vo_mtg voMtg ON voMtg.cboId = tblvo.federation_id where tblvo.federation_id =:shg_id and tblvo.Status=1 Order by voMtg.mtgNo desc limit 1")
    fun getgroupMeetingsdata(shg_id: String?): List<VoMeetingModel>?

    @Update
    fun updateFedmtg(dtmtg: VomtgEntity)

    @Query("Delete from vo_mtg")
    fun deleteAllFedmtgs()

    @Query("Select * from cbo_bank_details Where cbo_guid=:cbo_guid and is_active=1")
    fun getBankdata(cbo_guid: String?): List<Cbo_bankEntity>

//        @Query("SELECT distinct tblgroup.guid,tblgroup.is_voluntary_saving, tblgroup.federation_id,tblgroup.bookkeeper_name, tblgroup.federation_code, tblgroup.status,tblgroup.activation_status,tblgroup.federation_type_code,tblgroup.tags, tblgroup.federation_name, tblgroup.federation_name_short, tblgroup.federation_name_local, tblgroup.month_comp_saving, dtMtg.mtgNo, dtMtg.mtgDate, dtMtg.flagOpen, tblgroup.month_comp_saving, tblgroup.shg_formation_date, tblgroup.meeting_frequency, tblgroup.meeting_frequency_value, tblgroup.meeting_on,tblgroup.month_comp_saving, dtMtg.openingBalance, dtMtg.closingBalance , dtMtg.mtgGuid,dtMtg.mtgType as mtg_type FROM federation_profile tblgroup LEFT JOIN  vo_mtg dtMtg ON dtMtg.cboId = tblgroup.federation_id where  tblgroup.Status=1 and tblgroup.activation_status=2 Group By tblgroup.federation_id Order by tblgroup.federation_id")
    @Query("SELECT distinct tblgroup.guid,tblgroup.is_voluntary_saving, tblgroup.federation_id,tblgroup.bookkeeper_name, tblgroup.federation_code, tblgroup.status,tblgroup.activation_status,tblgroup.federation_type_code, tblgroup.federation_name, tblgroup.federation_name_short, tblgroup.federation_name_local, tblgroup.month_comp_saving, dtMtg.mtgNo, dtMtg.mtgDate, dtMtg.flagOpen, tblgroup.month_comp_saving, tblgroup.federation_formation_date, tblgroup.meeting_frequency, tblgroup.meeting_frequency_value, tblgroup.meeting_on,tblgroup.month_comp_saving, dtMtg.openingBalance, dtMtg.closingBalance , dtMtg.mtgGuid,dtMtg.mtgType as mtg_type FROM federation_profile tblgroup LEFT JOIN  vo_mtg dtMtg ON dtMtg.cboId = tblgroup.federation_id where  tblgroup.Status=1 and tblgroup.activation_status=2 and tblgroup.cbo_type=:cboType Group By tblgroup.federation_id Order by tblgroup.federation_id")
    fun getgroupMeetingsdatalist(cboType:Int): List<VoMeetingModel>

    @Query("select * from vo_mtg where cboId = :cbo_id  order by mtgNo desc limit 1")
    fun getMtglistdata(cbo_id: Long?): List<VomtgEntity>?

    @Query("select flagOpen from vo_mtg where cboId =:federationId and mtgNo=:Mtgno ")
    fun returnmeetingstatus(federationId: Long, Mtgno: Int): String

    @Query("Update vo_mtg set flagOpen=:flag where cboId=:federationId and mtgNo=:mtgno")
    fun openMeeting(flag: String, federationId: Long, mtgno: Int)

    @Query("select * from vo_mtg where cboId = :cbo_id  order by mtgNo")
    fun getMtglist(cbo_id: Long?): List<VomtgEntity>?

    @Query("Update vo_mtg set  isEdited=1, flagOpen=:flag,updatedOn=:updatedOn,updatedBy=:updatedBy where mtgNo=:mtgno and cboId=:cbo_id ")
    fun closingMeeting(flag: String, mtgno: Int, cbo_id: Long, updatedOn: Long, updatedBy: String)

    @Query("Delete from vo_mtg where mtgNo=:mtg_no and cboId=:cbo_id")
    fun deleteVo_MtgData(mtg_no: Int, cbo_id: Long)

    @Query("Update vo_mtg set closingBalanceCash=:closingcash,zeroMtgCashInHand=:cashInHand, zeroMtgCashInTransit=:cashInTransit,balanceDate=:balanceDate,updatedOn=:updatedOn,updatedBy=:updatedBy where mtgNo=:mtgno and cboId=:cbo_id ")
    fun updateVoCutOffCash(
        cashInHand: Int,
        cashInTransit: Int,
        balanceDate: Long,
        closingcash: Int,
        mtgno:Int,
        cbo_id:Long,
        updatedOn:Long,
        updatedBy:String
    )

    @Query("Select * from vo_mtg where mtgNo=:mtgno and cboId=:cbo_id ")
    fun getMeetingDetailData(mtgno:Int,cbo_id:Long):List<VomtgEntity>

    @Query("select (sum(savComp)+ +sum(savVol)) from vo_mtg where mtgNo=:mtgno and cboId=:cbo_id")
    fun getupdatedcash(mtgno: Int, cbo_id: Long): Int

    @Query("select  sum(openingBalanceCash) from vo_mtg where mtgNo=:mtgno and cboId=:cbo_id")
    fun getupdatedpeningcash(mtgno:Int,cbo_id:Long):Int

    @Query("Update vo_mtg set closingBalanceCash=:closingcash where mtgNo=:mtgno and cboId=:cbo_id ")
    fun updateclosingcash(
        closingcash: Int,
        mtgno:Int,
        cbo_id:Long
    )

    @Query("Update vo_mtg set savCompCb=:compsave where mtgNo=:mtgno and cboId=:cbo_id ")
    fun updatecompsave(
        compsave: Int,
        mtgno:Int,
        cbo_id:Long
    )

    @Query("Update vo_mtg set savVolCb=:sav_vol where mtgNo=:mtgno and cboId=:cbo_id ")
    fun updatevolsave(
        sav_vol: Int,
        mtgno:Int,
        cbo_id:Long
    )

    @Query("select savCompCb from vo_mtg where mtgNo=:mtgNo and cboId=:cboId")
    fun getTotalCompulsoryamount(mtgNo: Int,cboId: Long): Int?

    @Query("select savVolCb from vo_mtg where mtgNo=:mtgNo and cboId=:cboId")
    fun getTotalVoluntaryamount(mtgNo: Int,cboId: Long): Int?

    @Query("Select * from cbo_bank_details Where cbo_guid=:cbo_guid and is_active=1 and bank_guid not in(:bank_guid)")
    fun getBankdataWithAccountNo(cbo_guid: String?, bank_guid: String?): List<Cbo_bankEntity>

    @Query("Delete from vo_mtg where cboId=:cbo_id")
    fun deleteVo_MtgByVoId(cbo_id: Long)

    @Query("Select * from vo_mtg where isEdited=1 and flagOpen='C'")
    fun getVoMeetingList(): List<VomtgEntity>

    @Query("update vo_mtg set isEdited =0,uploadedOn=:lastupdatedate where cboId =:shg_id and mtgNo=:mtg_no")
    fun updateMeetinguploadStatus(
        shg_id: Long?,
        mtg_no: Int,
        lastupdatedate: Long
    )

    @Query("select sum(closingBalance) from vo_mtg where mtgNo=:mtgNo and cboId=:cboId")
    fun getClosingBalanceCash(mtgNo: Int, cboId: Long): Int

    @Query("Select withdrawnCash from vo_mtg where mtgNo=:mtgNo and cboId=:cboId")
    fun getCashInTransit(mtgNo: Int, cboId: Long): Int

}