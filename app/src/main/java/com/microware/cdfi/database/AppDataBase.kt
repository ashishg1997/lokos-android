package com.microware.cdfi.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.microware.cdfi.dao.*
import com.microware.cdfi.dao.vodao.*
import com.microware.cdfi.entity.*
import com.microware.cdfi.entity.voentity.*
import com.microware.miracle.dao.*

@Database(entities = [AddressEntity::class,Bank_branchEntity::class,BankEntity::class,BlockEntity::class,
    Cbo_bankEntity::class,Cbo_kycEntity::class,Cbo_phoneEntity::class,DistrictEntity::class,FederationEntity::class,
    LookupEntity::class,MemberEntity::class,PanchayatEntity::class,SHGEntity::class,
    StateEntity::class,TokensEntity::class,UserEntity::class,VillageEntity::class,member_addressEntity::class,MemberPhoneDetailEntity::class,
    MemberBankAccountEntity::class,Member_KYC_Entity::class,RoleEntity::class,SystemTagEntity::class,MemberSystemTagEntity::class,ImageuploadEntity::class,MappedShgEntity::class,VoShgMemberEntity::class,
    VoShgMemberPhoneEntity::class,Executive_memberEntity::class,subcommitee_masterEntity::class,
    subcommitee_memberEntity::class,subcommitteeEntity::class,LanguageMasterEntity::class,LabelMasterEntity::class,MemberDesignationEntity::class,
    MappedVoEntity::class,ClfVoMemberEntity::class,TransactionEntity::class,ResponseCodeEntity::class,FundingEntity::class,DtmtgEntity::class,DtmtgDetEntity::class,
    DtLoanMemberSheduleEntity::class,DtLoanGpEntity::class,DtLoanGpTxnEntity::class, FundTypeEntity::class, MstCOAEntity::class, LoanProductEntity::class,DtLoanTxnMemEntity::class,
    DtMtgFinTxnDetEntity::class,DtMtgGrpLoanScheduleEntity::class,DtLoanApplicationEntity::class,
    DtLoanMemberEntity::class,DtMtgFinTxnEntity::class,FinancialTransactionsMemEntity::class,
    ShgFinancialTxnDetailEntity::class,ShgMcpEntity::class,DtMemSettlementEntity::class,
    CardreCateogaryEntity::class,cadreRoleEntity::class,cadreShgMemberEntity::class
    , VoLoanApplicationEntity::class, VomtgEntity::class, VoFinTxnDetGrpEntity::class, VoFinTxnDetMemEntity::class, VoFinTxnEntity::class
    ,VoGroupLoanEntity::class,VoGroupLoanScheduleEntity::class,VoGroupLoanTxnEntity::class,VoMemLoanEntity::class,
    VoMemLoanScheduleEntity::class,VoMemLoanTxnEntity::class,VoMtgDetEntity::class,MstVOCOAEntity::class,VoCoaMappingEntity::class,MstproductEntity::class,ShgFinancialTxnVouchersEntity::class,VoMemSettlementEntity::class],version = 1)

// version currently in field = 14  1.0.49  dated: 25/09/2020

abstract class AppDataBase : RoomDatabase() {
    abstract fun shgDao(): SHGDao?
    abstract fun cboaddressDao(): CboAddressDao?
    abstract fun cboBankDao(): Cbo_BankDao?
    abstract fun cbokycDao(): CboKycDao?
    abstract fun cbophoneDao(): Cbo_phoneDetailDao?
    abstract fun memberDao(): MemberDao?
    abstract fun memberphoneDao(): MemberPhoneDao?
    abstract fun memberaddressDao(): MemberAddressDao?
    abstract fun memberbanckDao(): MemberBankDao?
    abstract fun memberkycDao(): MemberKYCDao?
    abstract fun fedrationDao(): FedrationDao?
    abstract fun stateDao(): StateDao?
    abstract fun districtDao(): DistrictDao?
    abstract fun blockDao(): BlockDao?
    abstract fun panchayatDao(): PanchayatDao?
    abstract fun villageDao(): VillageDao?
    abstract fun userDao(): UserDao?
    abstract fun lookupdao(): LookupDao?
    abstract fun masterbankbranchDao(): MasterBankBranchDao?
    abstract fun masterBankDao(): MasterBankDao?
    abstract fun roledao(): RoleDao?
    abstract fun systemtagDao(): SystemtagDao?
    abstract fun memberSystemtagDao(): MemberSystemtagDao?
    abstract fun imageUploadDao(): ImageUploadDao?
    abstract fun mappedShgDao(): MappedShgDao?
    abstract fun voShgMemberDao(): VOShgMemberDao?
    abstract fun vOShgMemberPhoneDao(): VOShgMemberPhoneDao?
    abstract fun executiveDao(): ExecutiveMemberDao?
    abstract fun subcommitteeMasterDao(): subcommitteeMasterDao?
    abstract fun subcommitteeDao(): SubcommitteeDao?
    abstract fun subcommitee_memberDao(): Subcommitee_memberDao?
    abstract fun labelMasterDao(): LabelMasterDao?
    abstract fun memberDesignationDao(): MemberDesignationDao?
    abstract fun mappedVoDao(): MappedVoDao?
    abstract fun clfVoMemberDao(): ClfVoMemberDao?
    abstract fun transactionDao(): TransactionDao?
    abstract fun responseCodeDao(): ResponseCodeDao?
    abstract fun fundingDao(): FundingAgencyDao?
    abstract fun cardreCateogaryDao():CardreCateogaryDao?
    abstract fun cardreRoleDao():CardreRoleDao?
    abstract fun caderMemberDao():cadreMemberDao?
    /*Meeting Dao*/
    abstract fun dtmtgDao(): DtmtgDao
    abstract fun dtdetmtgDao(): DtdetmtgDao
    abstract fun dtLoanMemberScheduleDao(): DtLoanMemberScheduleDao
    abstract fun dtLoanGpDao(): DtLoanGpDao
    abstract fun dtLoanGpTxnDao(): DtLoanGpTxnDao
    abstract fun mstCOADao(): MstCOADao
    abstract fun dtLoanTxnMemDao(): DtLoanTxnMemDao
    abstract fun dtMtgFinTxnDetDao(): DtMtgFinTxnDetDao
    abstract fun dtMtgGrpLoanScheduleDao(): DtMtgGrpLoanScheduleDao
    abstract fun dtLoanApplicationDao(): DtLoanApplicationDao
    abstract fun dtLoanMemberDao(): DtLoanMemberDao
    abstract fun dtMtgFinTxnDao(): DtMtgFinTxnDao
    abstract fun mstProductDao(): MstProductDao
    abstract fun dtFinTxnDetDao(): FinancialTransactionsMemDao?
    abstract fun incomeandExpendatureDao(): IncomeandExpenditureDao?
    abstract fun fundsDao(): FundTypeDao?
    abstract fun shgmcpDao(): ShgMcpDao?
    abstract fun memSettlementDao(): MemSettlementDao?
    abstract fun voFinTxnDetGrpDao(): VoFinTxnDetGrpDao?
    abstract fun voFinTxnDetMemDao(): VoFinTxnDetMemDao?
    abstract fun voGroupLoanDao(): VoGroupLoanDao?
    abstract fun voFinTxnDao(): VoFinTxnDao?
    abstract fun voGroupLoanScheduleDao(): VoGroupLoanScheduleDao?
    abstract fun voGroupLoanTxnDao(): VoGroupLoanTxnDao?
    abstract fun voMemLoanDao(): VoMemLoanDao?
    abstract fun voMemLoanScheduleDao(): VoMemLoanScheduleDao?
    abstract fun voMtgDetDao(): VoMtgDetDao?
    abstract fun voMemLoanTxnDao(): VoMemLoanTxnDao?
    abstract fun voFedLoanAppDao(): VoLoanApplicationDao?
    abstract fun fedmtgDao(): VomtgDao?
    abstract fun mstVOCOADao(): MstVOCOADao?
    abstract fun voCoaMappingDao(): VoCoaMappingDao?
    abstract fun laonProductDao(): LoanProductDao?
    abstract fun vouchersDao(): VouchersDao?
    abstract fun vomemSettlementDao(): VoMemSettlementDao?
}