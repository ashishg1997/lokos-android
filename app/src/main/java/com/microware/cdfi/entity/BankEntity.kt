package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "bank_master")
data class BankEntity (@PrimaryKey @ColumnInfo (name = "bank_id") val bank_id:Int,
                       @ColumnInfo (name = "language_id") val language_id:String?,
                       @ColumnInfo (name = "bank_code") val bank_code:String?,
                       @ColumnInfo (name = "bank_name") val bank_name:String?,
                       @ColumnInfo (name = "bank_shortname") val bank_shortname:String?,
                       @ColumnInfo(name ="bank_type") val bank_type:Int?,
                       @ColumnInfo(name ="ifsc_mask") val ifsc_mask:String?,
                       @ColumnInfo(name ="bank_merged_with") val bank_merged_with:String?,
                       @ColumnInfo(name ="bank_level") val bank_level:Int?,
                       @ColumnInfo(name ="bank_account_len") val bank_account_len:String?
               //        @ColumnInfo(name ="bank_branch_id") val bank_branch_id:Int?,

)

