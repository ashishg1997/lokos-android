package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(tableName = "bank_branch_master")
data class Bank_branchEntity (@PrimaryKey @ColumnInfo (name = "bank_branch_id") val bank_branch_id:Int=0,
                              @ColumnInfo(name = "bank_id") val bank_id:Int?=0,
                              @ColumnInfo(name = "bank_code") val bank_code:String?="",
                              @ColumnInfo(name = "bank_branch_code") val bank_branch_code:Int?=0,
                              @ColumnInfo(name = "bank_branch_name") val bank_branch_name:String?="",
                              @ColumnInfo(name = "ifsc_code") val ifsc_code:String?="",
                              @ColumnInfo(name = "bank_branch_address") val bank_branch_address:String?="",
                              @ColumnInfo(name = "rural_urban_branch") val rural_urban_branch:String?="",
                              @ColumnInfo(name = "village_code") val village_code:String?="",
                              @ColumnInfo(name = "block_code") val block_code:String?="",
                              @ColumnInfo(name = "district_code") val district_code:String?="",
                              @ColumnInfo(name = "state_code") val state_code:String?="",
                              @ColumnInfo(name = "pincode") val pincode:String?="",
                              @ColumnInfo(name = "branch_merged_with") val branch_merged_with:String?=""

)