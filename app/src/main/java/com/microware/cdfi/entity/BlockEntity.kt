package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName ="block_master")
data class BlockEntity (@PrimaryKey @ColumnInfo(name ="block_id") val block_id:Int,
                        @ColumnInfo(name ="state_id") val state_id:String?,
                        @ColumnInfo(name ="district_id") val district_id:String?,
                        @ColumnInfo(name ="block_code") val block_code:String?,
                        @ColumnInfo(name ="block_name_en") val block_name_en:String?,
                        @ColumnInfo(name ="block_short_name_en") val block_short_name_en:String?,
                        @ColumnInfo(name ="block_name_local") val block_name_local:String?,
                        @ColumnInfo(name ="block_short_name_local") val block_short_name_local:String?,
                        @ColumnInfo(name ="rural_urban_area") val rural_urban_area:String?,
                        @ColumnInfo(name ="lgd_code") val lgd_code:String?,
                        @ColumnInfo(name ="nmmu_code_original") val nmmu_code_original:String?,
                        @ColumnInfo(name ="gps_id") val gps_id:String?,
                        @ColumnInfo(name = "language_id") val language_id:String?

)

