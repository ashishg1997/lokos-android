package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cadre_category_master")
data class CardreCateogaryEntity(
    @PrimaryKey@ColumnInfo(name="uid") var uid:Int=0,
    @ColumnInfo(name="cadre_level") var cadre_level:Int?=0,
    @ColumnInfo(name="cadre_cat_code") var cadre_cat_code:Int?=0,
    @ColumnInfo(name="cadre_category") var cadre_category:String?="",
    @ColumnInfo(name="language_id") var language_id:String?=""
)