package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cbo_bank_details")
data class Cbo_bankEntity (@ColumnInfo (name="cbo_bank_id") val cbo_bank_id:Long?,
                           @ColumnInfo (name="cbo_code") val cbo_code:Long,
                           @ColumnInfo (name="account_no") val account_no:String,
                           @ColumnInfo (name ="cbo_guid") val cbo_guid:String,
                           @PrimaryKey @ColumnInfo (name ="bank_guid") val bank_guid:String,
                           @ColumnInfo (name="bank_id") val bank_id:Int?,
                           @ColumnInfo (name="account_opening_date") val account_opening_date:Long?,
                           @ColumnInfo (name="account_Linkage_Date") val account_Linkage_Date:Long?,
                           @ColumnInfo (name="bank_code") val bank_code:String?,
                           @ColumnInfo (name="bank_branch") val bank_branch:String?,
                           @ColumnInfo (name="ifsc_code") val ifsc_code:String?,
                           @ColumnInfo (name="is_default") val is_default:Int?,
                           @ColumnInfo (name="sequence_no") val sequence_no:Int?,
                           @ColumnInfo (name="account_type") val account_type:Int?,
                           @ColumnInfo (name="verification") val verification:Int?,
                           @ColumnInfo (name="cbo_type") val cbo_type:Int?,
                           @ColumnInfo (name ="is_active") val is_active:Int?,
                           @ColumnInfo (name ="is_edited") val is_edited:Int?,
                           @ColumnInfo (name ="entry_source") val entry_source:Int?,
                           @ColumnInfo(name = "created_date") val created_date:Long?,
                           @ColumnInfo(name = "created_by") val created_by:String?,
                           @ColumnInfo(name = "updated_date") val updated_date:Long?,
                           @ColumnInfo(name = "updated_by") val updated_by:String?,
                           @ColumnInfo (name ="last_uploaded_date") val last_uploaded_date:Long?,
                           @ColumnInfo (name ="uploaded_by") val uploaded_by:String?,
                           @ColumnInfo (name ="bankpassbook_name") val bankpassbook_name:String?,
                           @ColumnInfo (name ="passbook_firstpage") val passbook_firstpage:String?,
                           @ColumnInfo (name ="bank_branch_id") val bank_branch_id:Int?,
                           @ColumnInfo (name ="closuredate") val closuredate:Int?,
                           @ColumnInfo (name ="status") val status:Int?,
                           @ColumnInfo (name ="deduplication_status") val deduplication_status:Int?,
                           @ColumnInfo (name ="activation_status") val activation_status:Int?,
                           @ColumnInfo(name = "is_complete") var is_complete: Int?=1,
                           @ColumnInfo(name = "partial_status") var partial_status: Int?=0,
                           @ColumnInfo(name = "is_verified") var is_verified: Int?=0


)