package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName ="cbo_kyc_details")
data class  Cbo_kycEntity( @ColumnInfo (name ="cbo_guid") val cbo_guid:String,
                           @PrimaryKey @ColumnInfo (name ="kyc_guid") val kyc_guid:String,
                           @ColumnInfo (name ="cbo_id") val cbo_id:Long,
                           @ColumnInfo (name ="kyc_type") val kyc_type:Int?,
                           @ColumnInfo (name ="kyc_number") val kyc_number:String?,
                           @ColumnInfo (name ="kyc_front_doc_orig_name") val kyc_front_doc_orig_name:String?,
                           @ColumnInfo (name ="kyc_front_doc_encp_name") val kyc_front_doc_encp_name:String?,
                           @ColumnInfo (name ="kyc_rear_doc_name") val kyc_rear_doc_name:String?,
                           @ColumnInfo (name ="kyc_rear_doc_encp_name") val kyc_rear_doc_encp_name:String?,
                           @ColumnInfo (name ="kyc_valid_from") val kyc_valid_from:Long?,
                           @ColumnInfo (name ="kyc_valid_to") val kyc_valid_to:Long?,
                           @ColumnInfo (name ="dedupl_status") val dedupl_status:Int?,
                           @ColumnInfo (name ="activation_status") val activation_status:Int?,
                           @ColumnInfo (name ="checker_remark") val checker_remark:String?,
                           @ColumnInfo (name ="status") val status:Int?,
                           @ColumnInfo (name ="is_active") val is_active:Int?,
                           @ColumnInfo (name ="entry_source") val entry_source:Int?,
                           @ColumnInfo (name ="is_edited") val is_edited:Int?,
                           @ColumnInfo (name ="created_date") val created_date:Long?,
                           @ColumnInfo (name ="created_by") val created_by:String?,
                           @ColumnInfo (name ="updated_date") val updated_date:Long?,
                           @ColumnInfo (name ="updated_by") val updated_by:String?,
                           @ColumnInfo (name ="last_uploaded_date") val last_uploaded_date:Long?,
                           @ColumnInfo (name ="uploaded_by") val uploaded_by:String?,
                           @ColumnInfo(name = "is_complete") var is_complete: Int?=1,
                           @ColumnInfo(name = "cbo_type") var cbo_type: Int?,
                           @ColumnInfo(name = "is_verified") var is_verified: Int?=0
)

/*cbo_guid	varchar(50)
kyc_guid	varchar(50)
cbo_code	BIGINT
kyc_type	INT
kyc_number	VARCHAR(50)
document_id	INT
KYC_validFrom	Date
KYC_ValidTo	Date
dedupl_status	SMALLINT
activation_status	SMALLINT
checker_remark	varchar(250)
status	SMALLINT
is_active	SMALLINT
entry_source	SMALLINT
is_edited	INT
last_uploaded_date	TIMESTAMP
uploaded_by	Varchar(100)
created_date	TIMESTAMP
created_by	Varchar(100)
updated_date	TIMESTAMP
updated_by	Varchar(100)*/