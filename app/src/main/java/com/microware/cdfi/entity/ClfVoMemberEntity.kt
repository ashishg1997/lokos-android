package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tbl_clf_vo_member")
data class ClfVoMemberEntity (
    @ColumnInfo(name ="clf_guid") var clf_guid: String?="",
    @ColumnInfo(name ="vo_guid") var vo_guid: String?="",
    @ColumnInfo(name ="designation") var designation: Int?=0,
    @ColumnInfo(name ="joining_date") var joining_date: Long?=0,
    @PrimaryKey@ColumnInfo(name ="member_guid") var member_guid: String="",
    @ColumnInfo(name ="member_id") var member_id: Long?=0,
    @ColumnInfo(name ="member_name") var member_name: String?="",
    @ColumnInfo(name ="member_name_local") var member_name_local: String?="",
    @ColumnInfo(name ="phone_guid") var phone_guid: String?="",
    @ColumnInfo(name ="phone_no") var phone_no: String?="",
    @ColumnInfo(name ="relation") var relation: Int?=0,
    @ColumnInfo(name ="relation_name") var relation_name: String?="",
    @ColumnInfo(name ="relation_name_local") var relation_name_local: String?="",
    @ColumnInfo(name ="shg_formation_date") var shg_formation_date: Long?=0,
    @ColumnInfo(name ="shg_guid") var shg_guid: String?="",
    @ColumnInfo(name ="shg_id") var shg_id: Long?=0,
    @ColumnInfo(name ="shg_name") var shg_name: String?="",
    @ColumnInfo(name ="shg_name_local") var shg_name_local: String?=""
)