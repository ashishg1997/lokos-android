package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName ="district_master")
data class DistrictEntity (@PrimaryKey @ColumnInfo(name ="district_id")val district_id:Int,
                           @ColumnInfo(name ="district_code") val district_code:String?,
                           @ColumnInfo(name ="state_id") val state_id:String?,
                           @ColumnInfo(name ="district_name_en") val district_name_en:String?,
                           @ColumnInfo(name ="district_short_name_en") val district_short_name_en:String?,
                           @ColumnInfo(name ="district_name_local") val district_name_local:String?,
                           @ColumnInfo(name ="district_short_name_local") val district_short_name_local:String?,
                           @ColumnInfo(name ="fundrelease_flag") val fundrelease_flag:Int?,
                           @ColumnInfo(name ="lgd_code") val lgd_code:String?,
                           @ColumnInfo(name ="nmmu_code_original") val nmmu_code_original:String?,
                           @ColumnInfo(name ="gps_id") val gps_id:String?,
                           @ColumnInfo(name ="language_id") val language_id:String?

)

