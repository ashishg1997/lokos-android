package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "shg_loan_application",primaryKeys = ["loan_application_id","cbo_id","mem_id"])
data class DtLoanApplicationEntity(
    @ColumnInfo(name = "uid") var uid: Long=0,
    @ColumnInfo(name = "loan_application_id") var loan_application_id: Long=0,
    @ColumnInfo(name = "cbo_id") var cbo_id: Long=0,
    @ColumnInfo(name = "mem_id") var mem_id: Long=0,
    @ColumnInfo(name = "request_date") var request_date: Long?=0,
    @ColumnInfo(name = "loan_fee") var loan_fee: Int?=0,
    @ColumnInfo(name = "amt_demand") var amt_demand: Int?=0,
    @ColumnInfo(name = "amt_sanction") var amt_sanction: Int?=0,
    @ColumnInfo(name = "amt_disbursed") var amt_disbursed: Int?=0,
    @ColumnInfo(name = "approval_date") var approval_date: Long?=0,
    @ColumnInfo(name = "tentative_date") var tentative_date: Long?=0,
    @ColumnInfo(name = "loan_product_id") var loan_product_id: Int?=0,
    @ColumnInfo(name = "loan_request_priority") var loan_request_priority: Int?=0,
    @ColumnInfo(name = "loan_source") var loan_source: Int?=0,
    @ColumnInfo(name = "loan_purpose") var loan_purpose: Int?=0,
    @ColumnInfo(name = "loan_period") var loan_period: Int?=0,
    @ColumnInfo(name = "loan_requested_mtg_guid") var loan_requested_mtg_guid: String?="",
    @ColumnInfo(name = "loan_requested_mtg_no") var loan_requested_mtg_no: Int?=0,
    @ColumnInfo(name = "loan_sanctioned_mtg_no") var loan_sanctioned_mtg_no: Int?=null,
    @ColumnInfo(name = "loan_sanctioned_mtg_guid") var loan_sanctioned_mtg_guid: String?=null,
    @ColumnInfo(name = "created_by") var created_by: String?=null,
    @ColumnInfo(name = "created_on") var created_on: Long?=null,
    @ColumnInfo(name = "updated_by") var updated_by: String?=null,
    @ColumnInfo(name = "updated_on") var updated_on: Long?=null,
    @ColumnInfo(name = "uploadedby") var uploadedby: String?=null,
    @ColumnInfo(name = "uploaded_on") var uploaded_on: Long?=null,
    @ColumnInfo(name = "mcp_id") var mcp_id: Long?=null

)