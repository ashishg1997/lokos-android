package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "shg_group_loan_txn",primaryKeys = ["cbo_id","mtg_guid","mtg_no","loan_no"])
data class DtLoanGpTxnEntity(
    @ColumnInfo(name = "uid") var uid: Long?=0,
    @ColumnInfo(name = "cbo_id") var cbo_id: Long=0,
    @ColumnInfo(name = "mtg_guid") var mtg_guid: String="",
    @ColumnInfo(name = "mtg_no") var mtg_no: Int=0,
    @ColumnInfo(name = "mtg_date") var mtg_date: Long?=0,
    @ColumnInfo(name = "loan_no") var loan_no: Int=0,
    @ColumnInfo(name = "loan_op") var loan_op: Int?=0,
    @ColumnInfo(name = "loan_op_int") var loan_op_int: Int?=0,
    @ColumnInfo(name = "loan_paid") var loan_paid: Int?=0,
    @ColumnInfo(name = "loan_paid_int") var loan_paid_int: Int?=0,
    @ColumnInfo(name = "loan_cl") var loan_cl: Int?=0,
    @ColumnInfo(name = "loan_cl_int") var loan_cl_int: Int?=0,
    @ColumnInfo(name = "completion_flag") var completion_flag: Boolean?=null,
    @ColumnInfo(name = "int_accrued_op") var int_accrued_op: Int?=0,
    @ColumnInfo(name = "int_accrued") var int_accrued: Int?=0,
    @ColumnInfo(name = "int_accrued_cl") var int_accrued_cl: Int?=0,
    @ColumnInfo(name = "principal_demand_ob") var principal_demand_ob: Int?=0,
    @ColumnInfo(name = "principal_demand") var principal_demand: Int?=0,
    @ColumnInfo(name = "principal_demand_cb") var principal_demand_cb: Int?=0,
    @ColumnInfo(name = "mode_payment") var mode_payment: Int?=0,
    @ColumnInfo(name = "bank_code") var bank_code: String?="",
    @ColumnInfo(name = "transaction_no") var transaction_no: String?="",
    @ColumnInfo(name = "created_by") var created_by: String?=null,
    @ColumnInfo(name = "created_on") var created_on: Long?=null,
    @ColumnInfo(name = "updated_by") var updated_by: String?=null,
    @ColumnInfo(name = "updated_on") var updated_on: Long?=null,
    @ColumnInfo(name = "uploaded_by") var uploaded_by: String?=null,
    @ColumnInfo(name = "uploaded_on") var uploaded_on: Long?=null,
    @ColumnInfo(name = "interest_repaid") var interest_repaid: Int?=0,
    @ColumnInfo(name = "interest_rate") var interest_rate: Double?=0.0,
    @ColumnInfo(name = "period") var period: Int?=0
)