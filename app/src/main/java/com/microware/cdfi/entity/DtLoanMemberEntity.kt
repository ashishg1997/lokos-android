package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "shg_mem_loan",primaryKeys = ["cbo_id","mem_id","loan_no"])
data class DtLoanMemberEntity(
    @ColumnInfo(name = "uid") var uid: Int=0,
     @ColumnInfo(name = "loan_application_id") var loan_application_id: Long?=null,
    @ColumnInfo(name = "mtg_guid") var mtg_guid: String="",
    @ColumnInfo(name = "cbo_id") var cbo_id: Long=0,
    @ColumnInfo(name = "mem_id") var mem_id: Long=0,
    @ColumnInfo(name = "loan_no") var loan_no: Int=0,
    @ColumnInfo(name = "mtg_no") var mtg_no: Int?=0,
    @ColumnInfo(name = "mtg_date") var mtg_date: Long?=0,
    @ColumnInfo(name = "installment_date") var installment_date: Long?=0,
//    @ColumnInfo(name = "installments") var installments: Int?,
    @ColumnInfo(name = "original_loan_amount") var original_loan_amount: Int?=0,
    @ColumnInfo(name = "amount") var amount: Int?=0,
    @ColumnInfo(name = "loan_purpose") var loan_purpose: Int?=0,
    @ColumnInfo(name = "loan_product_id") var loan_product_id: Int?=null,
    @ColumnInfo(name = "interest_rate") var interest_rate: Double?=0.0,
    @ColumnInfo(name = "period") var period: Int?=0,
    @ColumnInfo(name = "principal_overdue") var principal_overdue: Int?=0,
    @ColumnInfo(name = "interest_overdue") var interest_overdue: Int?=0,
    @ColumnInfo(name = "completion_flag") var completion_flag: Boolean?=null,
    @ColumnInfo(name = "loan_type") var loan_type: Int?=0,
    @ColumnInfo(name = "loan_source") var loan_source: Int?=0,
 //   @ColumnInfo(name = "externalloanid") var externalloanid: Int?,
    @ColumnInfo(name = "mode_payment") var modepayment: Int?=0,
    @ColumnInfo(name = "bank_code") var bank_code: String?="",
    @ColumnInfo(name = "transaction_no") var transaction_no: String?="",
    @ColumnInfo(name = "installment_freq") var installment_freq: Int?=0,
    @ColumnInfo(name = "moratorium_period") var moratorium_period: Int?=0,
    @ColumnInfo(name = "loanaccountno") var loanaccountno: String?="",
    @ColumnInfo(name = "created_by") var created_by: String?=null,
    @ColumnInfo(name = "created_on") var created_on: Long?=null,
    @ColumnInfo(name = "updated_by") var updated_by: String?=null,
    @ColumnInfo(name = "updated_on") var updated_on: Long?=null,
    @ColumnInfo(name = "uploaded_by") var uploaded_by: String?=null,
    @ColumnInfo(name = "uploaded_on") var uploaded_on: Long?=0,
    @ColumnInfo(name = "principal_repaid") var principal_repaid: Int?=0,
    @ColumnInfo(name = "interest_repaid") var interest_repaid: Int?=0,
    @ColumnInfo(name = "disbursement_date") var disbursement_date: Long?=0,
    @ColumnInfo(name = "no_of_loans") var no_of_loans: Int?=0,
    @ColumnInfo(name = "original_interest_rate") var original_interest_rate: Double?=0.0,
    @ColumnInfo(name = "original_period") var original_period: Int?=0,
    @ColumnInfo(name = "reschedule_reason") var reschedule_reason: Int?=0,
    @ColumnInfo(name = "is_edited") var is_edited: Int?=0
//    @ColumnInfo(name = "corpus_external_id") var corpus_external_id: Int?,
//    @ColumnInfo(name = "flag") val flag: String?


)