package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "shg_mem_loan_schedule",primaryKeys = ["cbo_id","mem_id","loan_no","installment_no","sub_installment_no"])
data class DtLoanMemberSheduleEntity(
   @ColumnInfo(name = "uid") var uid: Long?=0,
     @ColumnInfo(name = "cbo_id") var cbo_id: Long=0,
    @ColumnInfo(name = "mem_id") var mem_id: Long=0,
    @ColumnInfo(name = "mtg_guid") var mtg_guid: String="",
    @ColumnInfo(name = "loan_no") var loan_no: Int=0,
    @ColumnInfo(name = "principal_demand") var principal_demand: Int?=0,
    @ColumnInfo(name = "loan_demand_os") var loan_demand_os: Int?=0,
    @ColumnInfo(name = "loan_os") var loan_os: Int?=0,
    @ColumnInfo(name = "loan_paid") var loan_paid: Int?=0,
    @ColumnInfo(name = "installment_no") var installment_no: Int=0,
    @ColumnInfo(name = "sub_installment_no") var sub_installment_no: Int=0,
    @ColumnInfo(name = "installment_date") var installment_date: Long?=0,
  //  @ColumnInfo(name = "loan_date") var loan_date: Long?,
    @ColumnInfo(name = "repaid") var repaid: Int?=0,
    @ColumnInfo(name = "last_paid_date") var last_paid_date: Long?=0,
    @ColumnInfo(name = "mtg_no") var mtg_no: Int?=0,
    @ColumnInfo(name = "created_by") var created_by: String?=null,
    @ColumnInfo(name = "created_on") var created_on: Long?=null,
    @ColumnInfo(name = "updated_by") var updated_by: String?=null,
    @ColumnInfo(name = "updated_on") var updated_on: Long?=null,
    @ColumnInfo(name = "uploaded_by") var uploaded_by: String?=null,
    @ColumnInfo(name = "uploaded_on") var uploaded_on: Long?=null


)