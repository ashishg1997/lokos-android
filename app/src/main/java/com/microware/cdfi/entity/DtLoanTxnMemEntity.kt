package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "shg_mem_loan_txn",primaryKeys = ["cbo_id","mem_id","mtg_guid","mtg_no","loan_no"])
data class DtLoanTxnMemEntity(
    @ColumnInfo(name = "uid") var uid: Int=0,
    @ColumnInfo(name = "cbo_id") var cbo_id: Long=0,
    @ColumnInfo(name = "mtg_guid") var mtg_guid: String="",
    @ColumnInfo(name = "mtg_no") var mtg_no: Int=0,
    @ColumnInfo(name = "mtg_date") var mtg_date: Long?=0,
    @ColumnInfo(name = "mem_id") var mem_id: Long=0,
//    @ColumnInfo(name = "serial") var serial: Int?,
//    @ColumnInfo(name = "loanflag") var loanflag: String?,
    @ColumnInfo(name = "loan_no") var loan_no: Int=0,
//    @ColumnInfo(name = "loan_org") var loan_org: Int?,
    @ColumnInfo(name = "loan_op") var loan_op: Int?=0,
    @ColumnInfo(name = "loan_op_int") var loan_op_int: Int?=0,
//    @ColumnInfo(name = "loan_due") var loan_due: Int?,
//    @ColumnInfo(name = "loan_dueint") var loan_dueint: Int?,
    @ColumnInfo(name = "loan_paid") var loan_paid: Int?=0,
    @ColumnInfo(name = "loan_paid_int") var loan_paid_int: Int?=0,
    @ColumnInfo(name = "loan_cl") var loan_cl: Int?=0,
    @ColumnInfo(name = "loan_cl_int") var loan_cl_int: Int?=0,
//    @ColumnInfo(name = "loan_period") var loan_period: Int?,
//    @ColumnInfo(name = "nointerestperiod") var nointerestperiod: Int?,
    @ColumnInfo(name = "completion_flag") var completion_flag: Boolean?=null,
//    @ColumnInfo(name = "remarks") var remarks: String?,
//    @ColumnInfo(name = "loanint_accrued") var loanint_accrued: Int?,
    @ColumnInfo(name = "int_accrued_op") var int_accrued_op: Int?=0,
    @ColumnInfo(name = "int_accrued") var int_accrued: Int?=0,
    @ColumnInfo(name = "int_accrued_cl") var int_accrued_cl: Int?=0,
//    @ColumnInfo(name = "intduration") var intduration: Int?,
//    @ColumnInfo(name = "intdurationcb") var intdurationcb: Int?,
    @ColumnInfo(name = "principal_demand_ob") var principal_demand_ob: Int?=0,
    @ColumnInfo(name = "principal_demand") var principal_demand: Int?=0,
    @ColumnInfo(name = "principal_demand_cb") var principal_demand_cb: Int?=0,
  //  @ColumnInfo(name = "intdemand") var intdemand: Int?,
    @ColumnInfo(name = "mode_payment") var mode_payment: Int?=0,
    @ColumnInfo(name = "bank_code") var bank_code: String?="",
    @ColumnInfo(name = "transaction_no") var transaction_no: String?="",
  /*  @ColumnInfo(name = "penaltydemand_ob") var penaltydemand_ob: Int?,
    @ColumnInfo(name = "penaltydemand") var penaltydemand: Int?,
    @ColumnInfo(name = "penaltydemand_cb") var penaltydemand_cb: Int?,
    @ColumnInfo(name = "penalty_paid") var penalty_paid: Int?,*/  /*24-06-2021*/
    @ColumnInfo(name = "created_by") var created_by: String?=null,
    @ColumnInfo(name = "created_on") var created_on: Long?=null,
    @ColumnInfo(name = "updated_by") var updated_by: String?=null,
    @ColumnInfo(name = "updated_on") var updated_on: Long?=null,
    @ColumnInfo(name = "uploaded_by") var uploaded_by: String?=null,
    @ColumnInfo(name = "uploaded_on") var uploaded_on: Long?=null,
    @ColumnInfo(name = "interest_rate") var interest_rate: Double?=0.0,
    @ColumnInfo(name = "period") var period: Int?=0
//    @ColumnInfo(name = "loansource") var loansource: String?,
//    @ColumnInfo(name = "demandcalculation") var demandcalculation: Int?,
//    @ColumnInfo(name = "mnthintdemand") var mnthintdemand: Int?,
//    @ColumnInfo(name = "loanaccountno") var loanaccountno: String?,
//    @ColumnInfo(name = "narration") var narration: String?

)