package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "shg_mem_settlement",primaryKeys = ["cbo_id","mem_id","mtg_guid","mtg_no"])
data class DtMemSettlementEntity(
    @ColumnInfo(name = "uid") var uid: Int=0,
    @ColumnInfo(name = "cbo_id") var cbo_id: Long=0,
    @ColumnInfo(name = "mtg_guid") var mtg_guid: String="",
    @ColumnInfo(name = "mtg_no") var mtg_no: Int=0,
    @ColumnInfo(name = "mtg_date") var mtg_date: Long?=0,
    @ColumnInfo(name = "mem_id") var mem_id: Long=0,
    @ColumnInfo(name = "sav_comp") var sav_comp: Int=0,
    @ColumnInfo(name = "sav_vol") var sav_vol: Int=0,
    @ColumnInfo(name = "sharecapital") var sharecapital: Int=0,
    @ColumnInfo(name = "member_surplus") var member_surplus: Int=0,
    @ColumnInfo(name = "loan_outstanding") var loan_outstanding: Int=0,
    @ColumnInfo(name = "member_deficit") var member_deficit: Int=0,
    @ColumnInfo(name = "available_amt") var available_amt: Int=0,
    @ColumnInfo(name = "paid_amt") var paid_amt: Int=0,
    @ColumnInfo(name = "receiver_name") var receiver_name: String="",
    @ColumnInfo(name = "receiver_relation") var receiver_relation: Int=0,
    @ColumnInfo(name = "reason") var reason: Int=0,
    @ColumnInfo(name = "payment_date") var payment_date: Long?=0,
    @ColumnInfo(name = "mode_payment") var mode_payment: Int?=0,
    @ColumnInfo(name = "bank_code") var bank_code: String?="",
    @ColumnInfo(name = "transaction_no") var transaction_no: String?="",
    @ColumnInfo(name = "narration") var narration: String?="",
    @ColumnInfo(name = "created_by") var created_by: String?=null,
    @ColumnInfo(name = "created_on") var created_on: Long?=null,
    @ColumnInfo(name = "updated_by") var updated_by: String?=null,
    @ColumnInfo(name = "updated_on") var updated_on: Long?=null,
    @ColumnInfo(name = "uploaded_by") var uploaded_by: String?=null,
    @ColumnInfo(name = "uploaded_on") var uploaded_on: Long?=null,
    @ColumnInfo(name = "other") var other: Int?=0,
    @ColumnInfo(name = "other_specify") var other_specify: String?="",
    @ColumnInfo(name = "settlement_status") var settlement_status: Int?=0
)