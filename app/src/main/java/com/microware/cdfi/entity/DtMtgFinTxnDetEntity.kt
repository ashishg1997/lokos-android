package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "financial_transactions_oth")
data class DtMtgFinTxnDetEntity (@PrimaryKey @ColumnInfo (name ="uid") val uid:Int,
                                 @ColumnInfo (name ="cbo_id") val cbo_id:Int?,
                                 @ColumnInfo (name ="group_m_code") val group_m_code:Int?,
                                 @ColumnInfo (name ="mtgno") val mtgno:Int,
                                 @ColumnInfo (name ="bankcode") val bankcode:String?,
                                 @ColumnInfo (name ="auid") val auid:Int?,
                                 @ColumnInfo (name ="type") val type:String?,
                                 @ColumnInfo (name ="amount") val amount:Double?,
                                 @ColumnInfo (name ="transdate") val transdate:String?,
                                 @ColumnInfo (name ="deposit_receipt") val deposit_receipt:Double?,
                                 @ColumnInfo (name ="withdrawal_payment") val withdrawal_payment:Double?,
                                 @ColumnInfo (name ="date_realisation") val date_realisation:String?,
                                 @ColumnInfo (name ="loanno") val loanno:Int?,
                                 @ColumnInfo (name ="effectivedate") val effectivedate:String?,
                                 @ColumnInfo (name ="narration") val narration:String?
)