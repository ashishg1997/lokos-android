package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "shg_fin_txn",primaryKeys = ["cbo_id","mtg_guid","mtg_no","bank_code"])
data class DtMtgFinTxnEntity ( @ColumnInfo(name = "uid") var uid: Long?=0,
                               @ColumnInfo(name = "cbo_id") var cbo_id: Long=0,
                               @ColumnInfo(name = "mtg_guid") var mtg_guid: String="",
                               @ColumnInfo(name = "mtg_no") var mtg_no: Int=0,
                               @ColumnInfo(name = "bank_code") var bank_code: String="",
                               @ColumnInfo(name = "opening_balance") var opening_balance: Int?=0,
                               @ColumnInfo(name = "closing_balance") var closing_balance: Int?=0,
                               @ColumnInfo(name = "other_deposits") var other_deposits: Int?=0,
                               @ColumnInfo(name = "other_withdrawals") var other_withdrawals: Int?=0,
                               @ColumnInfo(name = "deposited_cash") var deposited_cash: Int?=0,
                               @ColumnInfo(name = "withdrawn_cash") var withdrawn_cash: Int?=0,
                               @ColumnInfo(name = "opening_balance_cash") var opening_balance_cash: Int?=0,
                               @ColumnInfo(name = "closing_balance_cash") var closing_balance_cash: Int?=0,
                               @ColumnInfo(name = "created_by") var createdby: String?=null,
                               @ColumnInfo(name = "created_on") var createdon: Long?=null,
                               @ColumnInfo(name = "updated_by") var updatedby: String?=null,
                               @ColumnInfo(name = "updated_on") var updatedon: Long?=null,
                               @ColumnInfo(name = "uploaded_by") var uploadedby: String?=null,
                               @ColumnInfo(name = "uploaded_on") var uploadedon: Long?=null,
                               @ColumnInfo(name = "zero_mtg_cash_bank") var zero_mtg_cash_bank: Int?=0,
                               @ColumnInfo(name = "cheque_issued_not_realized") var cheque_issued_not_realized: Int?=0,
                               @ColumnInfo(name = "cheque_received_not_credited") var cheque_received_not_credited: Int?=0,
                               @ColumnInfo(name = "balance_date") var balance_date: Long?=0
)