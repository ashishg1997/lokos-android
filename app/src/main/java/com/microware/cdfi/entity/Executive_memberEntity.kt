package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "executive_member")
//primaryKeys = ["cbo_guid","ec_cbo_code","ec_member_code","designation","joining_date"]
data class Executive_memberEntity (
    @ColumnInfo(name = "cbo_id") val cbo_id:Long?,
    @ColumnInfo(name = "cbo_guid") val cbo_guid:String?,
    @PrimaryKey@ColumnInfo(name = "guid") val guid:String,
    @ColumnInfo(name = "cbo_level") val cbo_level:Int?,
    @ColumnInfo(name = "ec_cbo_level") val ec_cbo_level:Int?,
    @ColumnInfo(name = "ec_cbo_code") val ec_cbo_code:Long?,
    @ColumnInfo(name = "ec_cbo_id") val ec_cbo_id:Long?,
    @ColumnInfo(name = "ec_member_code") val ec_member_code:Long?,
    @ColumnInfo(name = "designation") val designation:Int?,
    @ColumnInfo(name = "is_signatory") val is_signatory:Int?,
    @ColumnInfo(name = "joining_date") val joining_date:Long?,
    @ColumnInfo(name = "leaving_date") val leaving_date:Long?,
    @ColumnInfo(name = "signatory_joining_date") val signatory_joining_date:Long?,
    @ColumnInfo(name = "signatory_leaving_date") val signatory_leaving_date:Long?,
    @ColumnInfo(name = "status") val status:Int?,
    @ColumnInfo(name = "is_active") val is_active:Int?,
    @ColumnInfo(name = "entry_source") val entry_source:Int?,
    @ColumnInfo(name = "is_edited") val is_edited:Int?,
    @ColumnInfo(name = "last_uploaded_date") val last_uploaded_date:Long?,
    @ColumnInfo(name = "uploaded_by") val uploaded_by:String?,
    @ColumnInfo(name = "created_date") val created_date:Long?,
    @ColumnInfo(name = "created_by") val created_by:String?,
    @ColumnInfo(name = "updated_date") val updated_date:Long?,
    @ColumnInfo(name = "updated_by") val updated_by:String?,
    @ColumnInfo(name = "is_complete") val is_complete:Int?=1
)