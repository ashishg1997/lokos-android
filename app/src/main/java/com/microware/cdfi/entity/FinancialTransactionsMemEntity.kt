package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "shg_fin_txn_det_mem",primaryKeys = ["mtg_guid","cbo_id","mem_id","mtg_no","auid"])
data class FinancialTransactionsMemEntity(
    @ColumnInfo(name = "uid") var uid: Long?=0,
    @ColumnInfo(name = "mtg_guid") var mtg_guid: String="",
    @ColumnInfo(name = "cbo_id") var cbo_id: Long=0,
    @ColumnInfo(name = "mem_id") var mem_id: Long=0,
    @ColumnInfo(name = "mtg_no") var mtg_no: Int=0,
    @ColumnInfo(name = "bank_code") var bank_code: String?="",
    @ColumnInfo(name = "auid") var auid: Int=0,
    @ColumnInfo(name = "type") var type: String?="",
    @ColumnInfo(name = "amount") var amount: Int?=0,
    @ColumnInfo(name = "trans_date") var trans_date: Long?=0,
   /* @ColumnInfo(name = "deposit_receipt") var deposit_receipt: Float?,
    @ColumnInfo(name = "withdrawal_payment") var withdrawal_payment: Float?,*/
    @ColumnInfo(name = "date_realisation") var date_realisation: Long?=0,
    @ColumnInfo(name = "mode_payment") var mode_payment: Int?=0,
    @ColumnInfo(name = "transaction_no") var transaction_no: String?="",
    @ColumnInfo(name = "created_by") var created_by: String?="",
    @ColumnInfo(name = "created_on") var created_on: Long?=0,
    @ColumnInfo(name = "updated_by") var updated_by: String?="",
    @ColumnInfo(name = "updated_on") var updated_on: Long?=0,
    @ColumnInfo(name = "uploaded_by") var uploaded_by: String?="",
    @ColumnInfo(name = "uploaded_on") var uploaded_on: Long?=0,
    @ColumnInfo(name = "reference_mtg_no") var reference_mtg_no: Int?=0
)