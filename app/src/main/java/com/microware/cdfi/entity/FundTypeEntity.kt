package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "mst_fund_type",primaryKeys = ["fund_type_id","language_id"])
data class FundTypeEntity(
    @ColumnInfo(name = "uid") val uid: Long,
    @ColumnInfo(name = "fund_type_id") val fund_type_id: Int,
    @ColumnInfo(name = "fund_type") val fund_type: String?,
    @ColumnInfo(name = "sequence") val sequence: Int?,
    @ColumnInfo(name = "fund_type_short_name") val fund_type_short_name: String?,
    @ColumnInfo(name = "language_id") val language_id: String
)