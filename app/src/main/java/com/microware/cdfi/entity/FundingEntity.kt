package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tblfunding_agency_master",primaryKeys = ["funding_agency_id","state_id","agency_name_language_id"])
data class FundingEntity ( @ColumnInfo(name="funding_agency_id") val funding_agency_id:Int,
                          @ColumnInfo(name="state_id") val state_id:Int,
                          @ColumnInfo(name="district_id") val district_id:String?,
                          @ColumnInfo(name="block_id") val block_id:String?,
                          @ColumnInfo(name="agency_code") val agency_code:String?,
                          @ColumnInfo(name="agency_name") val agency_name:String?,
                          @ColumnInfo(name="agency_name_language_id") val agency_name_language_id:String,
                          @ColumnInfo(name="agency_type") val agency_type:Int?
)