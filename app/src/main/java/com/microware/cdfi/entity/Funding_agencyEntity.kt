package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "funding_agency_master")
data class Funding_agencyEntity (@PrimaryKey @ColumnInfo (name ="funding_agency_id") val funding_agency_id:Int,
                                 @ColumnInfo (name = "agency_code") val agency_code:String?,
                                 @ColumnInfo (name = "agency_name_en") val agency_name_hi:String?,
                                 @ColumnInfo(name ="agency_name_Local") val agency_name_Local:String?,
                                 @ColumnInfo(name ="agency_type") val agency_type:Int?,
                                 @ColumnInfo(name = "created_date") val created_date:String?,
                                 @ColumnInfo(name = "created_by") val created_by:String?,
                                 @ColumnInfo(name = "updated_date") val updated_date:String?,
                                 @ColumnInfo(name = "updated_by") val updated_by:String?,
                                 @ColumnInfo(name ="is_active") val is_active:Int?
                                 )