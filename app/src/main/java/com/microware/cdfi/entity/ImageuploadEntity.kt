package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "image_upload")
data class ImageuploadEntity (@PrimaryKey @ColumnInfo (name = "image_name") val image_name:String,
                              @ColumnInfo (name = "shg_guid") val shg_guid:String?,
                              @ColumnInfo (name = "member_guid") val member_guid:String?,
                              @ColumnInfo (name = "image_type") val image_type:Int?,
                              @ColumnInfo(name ="isupload") val isupload:Int

)

