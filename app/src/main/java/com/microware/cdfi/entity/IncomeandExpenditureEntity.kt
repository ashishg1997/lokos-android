package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = "shg_fin_txn_det_grp", primaryKeys = ["mtg_guid","cbo_id","mtg_no","auid"])
data class IncomeandExpenditureEntity(
    @ColumnInfo(name = "uid") val uid: Long?,
    @ColumnInfo(name = "mtg_guid") val mtg_guid: String,
    @ColumnInfo(name = "cbo_id") val cbo_id: Long,
    @ColumnInfo(name = "mtg_no") val mtg_no: Int,
    @ColumnInfo(name = "auid") val auid: Int,
    @ColumnInfo(name = "fund_type") val fund_type: Int?,
    @ColumnInfo(name = "amount_to_from") val amount_to_from: Int?,
    @ColumnInfo(name = "type") val type: String?,
    @ColumnInfo(name = "amount") val amount: Float?,
    @ColumnInfo(name = "trans_date") val trans_date: Long?,
   /* @ColumnInfo(name = "deposit_receipt") val deposit_receipt: Float?,
    @ColumnInfo(name = "withdrawal_payment") val withdrawal_payment: Float?,*/
    @ColumnInfo(name = "date_realisation") val date_realisation: String?,
    @ColumnInfo(name = "mode_payment") val mode_payment: Int?,
    @ColumnInfo(name = "bank_code") val bank_code: String?,
    @ColumnInfo(name = "transaction_no") val transaction_no: String?,
    @ColumnInfo(name = "created_by") val created_by: String?,
    @ColumnInfo(name = "created_on") val created_on: Long?,
    @ColumnInfo(name = "updated_by") val updated_by: String?,
    @ColumnInfo(name = "updated_on") val updated_on: Long?,
    @ColumnInfo(name = "uploaded_by") val uploaded_by: String?,
    @ColumnInfo(name = "uploaded_on") val uploaded_on: Long?
)
