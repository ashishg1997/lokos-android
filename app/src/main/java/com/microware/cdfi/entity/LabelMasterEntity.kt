package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "label_master")
class LabelMasterEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id") val id: Int,
    @ColumnInfo(name = "keyValue") val key: String,
    @ColumnInfo(name = "text_value") val text_value: String,
    @ColumnInfo(name = "language_id") val language_id: String,
    @ColumnInfo(name = "is_active") val is_active: Int?,
    @ColumnInfo(name = "created_date") val created_date: String?,
    @ColumnInfo(name = "created_by") val created_by: String?,
    @ColumnInfo(name = "updated_date") val updated_date: String?,
    @ColumnInfo(name = "updated_by") val updated_by: String?
)
