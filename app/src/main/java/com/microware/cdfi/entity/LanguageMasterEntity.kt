package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "language_master")
data class LanguageMasterEntity ( @PrimaryKey(autoGenerate = true)
@ColumnInfo(name ="id") val id:Int,@ColumnInfo(name ="language_name") val language_name:String,
    @ColumnInfo(name ="language_short") val language_short:String,
                                  @ColumnInfo(name ="is_active") val is_active:Int,@ColumnInfo(name ="created_date") val created_date:String,
                                  @ColumnInfo(name ="created_by") val created_by:String,@ColumnInfo(name ="updated_date") val updated_date:String,
                                  @ColumnInfo(name ="updated_by") val updated_by:String

)