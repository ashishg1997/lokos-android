package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "mst_loanproduct")
data class LoanProductEntity(
    @PrimaryKey @ColumnInfo(name = "product_id") val product_id: Long,
    @ColumnInfo(name = "level") val level: Int?,
    @ColumnInfo(name = "cbo_id") val cbo_id: Long?,
    @ColumnInfo(name = "loan_purpose") val loan_purpose: Int?,
    @ColumnInfo(name = "amount_min") val amount_min: Int?,
    @ColumnInfo(name = "amount_max") val amount_max: Int?,
    @ColumnInfo(name = "period_min") val period_min: Int?,
    @ColumnInfo(name = "period_max") val period_max: Int?,
    @ColumnInfo(name = "interest_min") val interest_min: Float?,
    @ColumnInfo(name = "interest_max") val interest_max: Float?,
    @ColumnInfo(name = "interest_def") val interest_def: Float?,
    @ColumnInfo(name = "moratorium_period_min") val moratorium_period_min: Int?,
    @ColumnInfo(name = "moratorium_period_max") val moratorium_period_max: Int?,
    @ColumnInfo(name = "moratorium_period_def") val moratorium_period_def: Int?,
    @ColumnInfo(name = "penalty_type") val penalty_type: Float?,
    @ColumnInfo(name = "penalty_value") val penalty_value: Float?,
    @ColumnInfo(name = "loan_processing_fee") val loan_processing_fee: Float?
)