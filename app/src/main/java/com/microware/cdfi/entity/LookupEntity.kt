package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "LookupMaster")
data class LookupEntity(
    @PrimaryKey@ColumnInfo(name = "lookup_id") val lookup_id: Int,
    @ColumnInfo(name = "key_code") val key_code: String?,
    @ColumnInfo(name = "lookup_code") val lookup_code: Int?,
    @ColumnInfo(name = "key_value") val key_value: String?,
    @ColumnInfo(name = "key1") val key1: Int?,
    @ColumnInfo(name = "description") val description: String?,
    @ColumnInfo(name = "sequence_no") val sequence_no: Int?,
    @ColumnInfo(name = "language_id") val language_id: String?
)

