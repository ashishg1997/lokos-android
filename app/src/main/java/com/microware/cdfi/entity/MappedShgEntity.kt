package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tbl_mapped_vo_shg")
data class MappedShgEntity (
    @PrimaryKey @ColumnInfo(name ="guid") var guid: String="",
    @ColumnInfo(name ="vo_guid") var vo_guid: String?="",
    @ColumnInfo(name ="vo_code") var vo_code: Long?=0,
    @ColumnInfo(name ="shg_formation_date") var shg_formation_date: Long?=0,
    @ColumnInfo(name ="joining_date") var joining_date: Long?=0,
    @ColumnInfo(name ="leaving_date") var leaving_date: Long?=0,
    @ColumnInfo(name ="shg_id") var shg_id: Long?=0,
    @ColumnInfo(name ="shg_name") var shg_name: String?="",
    @ColumnInfo(name ="shg_name_local") var shg_name_local: String?="",
    @ColumnInfo(name = "status") var status: Int?=0,
    @ColumnInfo(name = "settlement_status") var settlement_status: Int?=0,
    @ColumnInfo(name = "leaving_reason") var leaving_reason: Int?=0
)