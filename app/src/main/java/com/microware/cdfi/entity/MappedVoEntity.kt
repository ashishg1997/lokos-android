package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = "tbl_mapped_clf_vo",primaryKeys = ["cbo_child_guid","cbo_guid"])
data class MappedVoEntity (
    @ColumnInfo(name ="cbo_child_formation_date") var cbo_child_formation_date: Long?=0,
    @ColumnInfo(name ="cbo_child_guid") var cbo_child_guid: String="",
    @ColumnInfo(name ="cbo_child_id") var cbo_child_id: Long?=0,
    @ColumnInfo(name ="cbo_child_name") var cbo_child_name: String?="",
    @ColumnInfo(name ="cbo_child_name_local") var cbo_child_name_local: String?="",
    @ColumnInfo(name ="cbo_code") var cbo_code: Long?=0,
    @ColumnInfo(name ="cbo_guid") var cbo_guid: String="",
    @ColumnInfo(name ="joining_date") var joining_date: Long?=0,
    @ColumnInfo(name ="leaving_date") var leaving_date: Long?=0
)