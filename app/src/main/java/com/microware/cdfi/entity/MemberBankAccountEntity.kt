package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "MemberBankAccount")
data class MemberBankAccountEntity(
    @ColumnInfo(name = "member_bank_details_id") val member_bank_details_id: Long?,
    @ColumnInfo(name = "member_code") val member_code: Long?,
    @ColumnInfo(name = "cbo_id") val cbo_id: Long?,
    @ColumnInfo(name = "member_guid") val member_guid: String?,
    @PrimaryKey @ColumnInfo(name = "bank_guid") val bank_guid: String,
    @ColumnInfo(name = "account_no") val account_no: String?,
    @ColumnInfo(name = "bank_id") val bank_id: String?,
    @ColumnInfo(name = "bank_account") val bank_account: Int?,
    @ColumnInfo(name = "account_type") val account_type: String?,
   // @ColumnInfo(name = "lgd_reference") val lgd_reference: String?,
    @ColumnInfo(name = "account_open_date") val account_open_date: Long?,
    @ColumnInfo(name = "is_default_account") val is_default_account: Int?,
    @ColumnInfo(name = "status") val status: Int?,
    @ColumnInfo(name = "closing_date") val closing_date: Long?,
    @ColumnInfo(name = "gl_code") val gl_code: String?,
    @ColumnInfo(name = "same_as_group") val sameasgroup: Int?,
    @ColumnInfo(name = "entry_source") val entry_source: Int?,
    @ColumnInfo(name = "is_edited") val is_edited: Int?,
    @ColumnInfo(name = "created_date") val created_date: Long?,
    @ColumnInfo(name = "created_by") val created_by: String?,
    @ColumnInfo(name = "updated_date") val updated_date: Long?,
    @ColumnInfo(name = "updated_by") val updated_by: String?,
    @ColumnInfo(name = "last_uploaded_date") val last_uploaded_date: Long?,
    @ColumnInfo(name = "uploaded_by") val uploaded_by: String?,
    @ColumnInfo(name = "bank_passbook_name") val bank_passbook_name: String?,
    @ColumnInfo(name = "passbook_firstpage") val passbook_firstpage: String?,
    @ColumnInfo (name="ifsc_code") val ifsc_code:String?,
    @ColumnInfo (name="mem_branch_code") val mem_branch_code:String?,
    @ColumnInfo (name="dedupl_status") val dedupl_status:Int?,
    @ColumnInfo (name="activation_status") val activation_status:Int?,
    @ColumnInfo (name="is_active") val is_active:Int?,
    @ColumnInfo (name="checker_remark") val checker_remark:String?,
    @ColumnInfo(name = "is_complete") var is_complete: Int?,
    @ColumnInfo(name = "partial_status") var partial_status: Int?=0,
    @ColumnInfo(name = "is_verified") var is_verified: Int?=0
)