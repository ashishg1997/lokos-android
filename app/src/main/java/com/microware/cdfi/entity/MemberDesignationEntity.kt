package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "shg_member_designation")
data class MemberDesignationEntity(
    @ColumnInfo(name = "member_designation_id") var  member_designation_id: Long?=0,
    @ColumnInfo(name = "member_code") var  member_code: String?="",
    @ColumnInfo(name = "cbo_code") var  cbo_code: String?="",
    @PrimaryKey @ColumnInfo(name = "member_designation_guid") var  member_designation_guid: String="",
    @ColumnInfo(name = "member_guid") var  member_guid: String="",
    @ColumnInfo(name = "cbo_guid") var  cbo_guid: String="",
    @ColumnInfo(name = "date_election") var  date_election: Long?=0,
    @ColumnInfo(name = "designation") var  designation: Int?=0,
    @ColumnInfo(name = "from_date") var  from_date: Long?=0,
    @ColumnInfo(name = "to_date") var  to_date: Long?=0,
    @ColumnInfo(name = "status") var  status: Int?=0,
    @ColumnInfo(name = "is_active") var  is_active: Int?=0,
    @ColumnInfo(name = "entry_source") var  entry_source: Int?=0,
    @ColumnInfo(name = "is_edited") var  is_edited: Int?=0,
    @ColumnInfo(name = "last_uploaded_date") var  last_uploaded_date: Long?=0,
    @ColumnInfo(name = "uploaded_by") var  uploaded_by: String?="",
    @ColumnInfo(name = "created_date") var  created_date: Long?=0,
    @ColumnInfo(name = "created_by") var  created_by: String?="",
    @ColumnInfo(name = "updated_date") var  updated_date: Long?=0,
    @ColumnInfo(name = "updated_by") var  updated_by: String?="",
    @ColumnInfo(name = "rural_urban_area") var  rural_urban_area: String?="",
    @ColumnInfo(name = "lgd_code") var  lgd_code: String?="",
    @ColumnInfo(name = "nmmu_code_original") var  nmmu_code_original: String?="",
    @ColumnInfo(name = "gps_id") var  gps_id: String?="",
    @ColumnInfo(name = "language_id") var  language_id: String?="",
    @ColumnInfo(name = "is_complete") var  is_complete: Int?=0,
    @ColumnInfo(name = "is_signatory") var  is_signatory: Int?=0

)

