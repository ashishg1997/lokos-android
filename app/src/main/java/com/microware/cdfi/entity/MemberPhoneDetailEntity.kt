package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName="MemberPhoneDetail")
data class MemberPhoneDetailEntity (@ColumnInfo(name ="member_phone_details_id") val member_phone_details_id:Long?,
                                    @ColumnInfo(name ="cbo_id") val cbo_id:Long?,
                                    @ColumnInfo(name ="member_code") val member_code:Long?,
                                    @ColumnInfo(name = "member_guid") val member_guid: String?,
                                    @PrimaryKey @ColumnInfo(name = "phone_guid") val phone_guid: String,
                                    @ColumnInfo(name ="phone_no") val phone_no:String?,
                                    @ColumnInfo(name ="is_default") val is_default:Int?,
                                    @ColumnInfo(name ="phone_ownership") val phone_ownership:String?,
                                    @ColumnInfo(name ="phone_ownership_details") val phone_ownership_details:String?,
                                    @ColumnInfo(name ="valid_from") val valid_from:Long?,
                                    @ColumnInfo(name ="valid_till") val valid_till:Long?,
                                    @ColumnInfo (name ="entry_source") val entry_source:Int?,
                                    @ColumnInfo (name ="is_edited") val is_edited:Int?,
                                    @ColumnInfo(name ="created_date") val created_date:Long?,
                                    @ColumnInfo(name ="created_by") val created_by:String?,
                                    @ColumnInfo(name ="updated_date") val updated_date:Long?,
                                    @ColumnInfo(name ="updated_by") val updated_by:String?,
                                    @ColumnInfo (name ="last_uploaded_date") val last_uploaded_date:Long?,
                                    @ColumnInfo (name ="uploaded_by") val uploaded_by:String?,
                                    @ColumnInfo (name ="is_active") val is_active:Int?,
                                    @ColumnInfo (name ="cbo_type") val cbo_type:Int?,
                                    @ColumnInfo (name ="dedupl_status") val dedupl_status:Int?,
                                    @ColumnInfo (name ="activation_status") val activation_status:Int?,
                                    @ColumnInfo (name ="status") val status:Int?,
                                    @ColumnInfo (name ="checker_remark") val checker_remark:String?,
                                    @ColumnInfo(name = "is_complete") var is_complete: Int?,
                                    @ColumnInfo(name = "is_verified") var is_verified: Int?=0
)