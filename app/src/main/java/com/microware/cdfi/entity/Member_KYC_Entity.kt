package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "MemberKYCdetails")
data class Member_KYC_Entity (@ColumnInfo(name ="member_kyc_details_id") val member_kyc_details_id:Long?,
                              @ColumnInfo(name ="member_code") val member_code:Long?,
                              @ColumnInfo(name ="member_guid") val member_guid:String?,
                              @PrimaryKey @ColumnInfo(name="kyc_guid") val kyc_guid:String,
                              @ColumnInfo(name ="kyc_type") val kyc_type:Int?,
                              @ColumnInfo(name ="kyc_number") val kyc_number:String?,
                              @ColumnInfo(name ="cbo_id") val cbo_id:String?,
                              @ColumnInfo (name ="kyc_front_doc_orig_name") val kyc_front_doc_orig_name:String?,
                              @ColumnInfo (name ="kyc_front_doc_encp_name") val kyc_front_doc_encp_name:String?,
                              @ColumnInfo (name ="kyc_rear_doc_orig_name") val kyc_rear_doc_orig_name:String?,
                              @ColumnInfo (name ="kyc_rear_doc_encp_name") val kyc_rear_doc_encp_name:String?,
                              @ColumnInfo (name ="created_date") val created_date:Long?,
                              @ColumnInfo (name ="created_by") val created_by:String?,
                              @ColumnInfo (name ="updated_date") val updated_date:Long?,
                              @ColumnInfo (name ="updated_by") val updated_by:String?,
                              @ColumnInfo (name ="last_uploaded_date") val last_uploaded_date:Long?,
                              @ColumnInfo (name ="uploaded_by") val uploaded_by:String?,
                              @ColumnInfo (name ="is_edited") val is_edited:Int?,
                              @ColumnInfo (name ="is_active") val is_active:Int?,
                              @ColumnInfo (name ="entry_source") val entry_source:Int?,
                              @ColumnInfo (name ="dedupl_status") val dedupl_status:Int?,
                              @ColumnInfo (name ="activation_status") val activation_status:Int?,
                              @ColumnInfo (name ="checker_remark") val checker_remark:String?,
                              @ColumnInfo (name ="status") val status:Int?,
                              @ColumnInfo(name = "is_complete") var is_complete: Int?,
                              @ColumnInfo(name = "is_verified") var is_verified: Int?=0
)