package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "mst_vo_coa_sub",primaryKeys = ["uid","language_id"])
data class MstVOCOAEntity(
    @ColumnInfo(name = "uid") val uid: Int,
    @ColumnInfo(name = "ledger_code") val ledger_code: Int?,
    @ColumnInfo(name = "description") val description: String?,
    @ColumnInfo(name = "sequence") val sequence: Int?,
    @ColumnInfo(name = "receipt_payment") val receipt_payment: Int?,
    @ColumnInfo(name = "type") val type: String?,
    @ColumnInfo(name = "short_description") val short_description: String?,
    @ColumnInfo(name = "language_id") val language_id: String,
    @ColumnInfo(name = "is_active") val is_active: Int?
)