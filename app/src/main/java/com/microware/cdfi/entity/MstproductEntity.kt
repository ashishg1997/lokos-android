package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "loanProductMasterList",primaryKeys = ["fundType","source","recipent"])
data class MstproductEntity(
    @ColumnInfo(name = "fundTypeId") val fundTypeId: Int?=0,
    @ColumnInfo(name = "fundType") val fundType: Int=0,
    @ColumnInfo(name = "source") val source: Int=0,
    @ColumnInfo(name = "recipent") val recipent: Int=0,
    @ColumnInfo(name = "loanGrant") val loanGrant: Int?=0,
    @ColumnInfo(name = "minInterest") val minInterest: Double?=0.0,
    @ColumnInfo(name = "maxInterest") val maxInterest: Double?=0.0,
    @ColumnInfo(name = "interestDefault") val interestDefault: Double?=0.0,
    @ColumnInfo(name = "moratoriumUpto") val moratoriumUpto: Int?=0,
    @ColumnInfo(name = "minAmount") val minAmount: Int?=0,
    @ColumnInfo(name = "maxAmount") val maxAmount: Int?=0,
    @ColumnInfo(name = "installmentType") val installmentType: Int?=0,
    @ColumnInfo(name = "minPeriod") val minPeriod: Int?=0,
    @ColumnInfo(name = "maxPeriod") val maxPeriod: Int?=0,
    @ColumnInfo(name = "defaultPeriod") val defaultPeriod: Int?=0,
    @ColumnInfo(name = "processingFee") val processingFee: Int?=0,
    @ColumnInfo(name = "effectiveFrom") val effectiveFrom: Long?=0,
    @ColumnInfo(name = "level") val level: Int?=0

)