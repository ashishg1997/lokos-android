package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "panchayat_master")
data class PanchayatEntity (@PrimaryKey @ColumnInfo(name ="panchayat_id")val panchayat_id:Int,
                            @ColumnInfo(name ="state_id") val state_id:String?,
                            @ColumnInfo(name ="district_id") val district_id:String?,
                            @ColumnInfo(name ="block_id") val block_id:String?,
                            @ColumnInfo(name ="panchayat_code") val panchayat_code:String?,
                            @ColumnInfo(name ="panchayat_name_en") val panchayat_name_en:String?,
                            @ColumnInfo(name ="panchayat_name_Local") val panchayat_name_Local:String?,
                            @ColumnInfo(name ="rural_urban_area") val rural_urban_area:String?,
                            @ColumnInfo(name ="nmmu_code_original") val nmmu_code_original:String?,
                            @ColumnInfo(name ="gps_id") val gps_id:String?,
                            @ColumnInfo(name ="language_id") val language_id:String?
)
