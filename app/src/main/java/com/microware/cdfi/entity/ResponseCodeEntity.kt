package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "mst_response")
data class ResponseCodeEntity (
    @PrimaryKey @ColumnInfo(name ="id") val id:Int,
    @ColumnInfo(name ="response_code") val response_code:Int?,
    @ColumnInfo(name ="response_msg") val response_msg:String?,
    @ColumnInfo(name ="language_id") val language_id:String?
)