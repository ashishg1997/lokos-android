package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "mstRole")
data class  RoleEntity(
    @PrimaryKey @ColumnInfo(name = "roleId") val roleId: String,
    @ColumnInfo(name = "roleName") val roleName: String,
    @ColumnInfo(name = "status") val status: String,
    @ColumnInfo(name = "categoryId") val categoryId: Int,
    @ColumnInfo(name = "levelId") val levelId: Int,
    @ColumnInfo(name = "typeId") val typeId: Int
)