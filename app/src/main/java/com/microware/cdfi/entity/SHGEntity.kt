package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "shg_profile")
data class SHGEntity(
    @ColumnInfo(name = "shg_id")  var shg_id: Long=0,
    @PrimaryKey @ColumnInfo(name = "guid") var guid: String="",
    @ColumnInfo(name = "shg_code") var shg_code: String?="",
    @ColumnInfo(name = "state_id") var state_id: Int?=0,
    @ColumnInfo(name = "district_id") var district_id: Int?=0,
    @ColumnInfo(name = "block_id") var block_id: Int?=0,
    @ColumnInfo(name = "village_id") var village_id: Int?=0,
    @ColumnInfo(name = "panchayat_id") var panchayat_id: Int?=0,
    @ColumnInfo(name = "hamlet_id") var hamlet_id: String?="",
    @ColumnInfo(name = "shg_name") var shg_name: String?="",
    @ColumnInfo(name = "shg_name_short_en") var shg_name_short_en: String?="",
    @ColumnInfo(name = "shg_name_short_local") var shg_name_short_local: String?="",
    @ColumnInfo(name = "shg_type_code") var shg_type_code: Int?=0,
    @ColumnInfo(name = "language_id") var language_id: String?="",
    @ColumnInfo(name = "shg_name_local") var shg_name_local: String?="",
    @ColumnInfo(name = "composition") var composition: Int?=0,
    @ColumnInfo(name = "shg_formation_date") var shg_formation_date: Long?=0,
    @ColumnInfo(name = "shg_revival_date") var shg_revival_date: Long?=0,
    @ColumnInfo(name = "shg_promoted_by") var shg_promoted_by: Int?=0,
    @ColumnInfo(name = "promoter_code") var promoter_code: Int?=0,
    @ColumnInfo(name = "promoter_name") var promoter_name: String?="",
    @ColumnInfo(name = "shg_revived_by") var shg_revived_by: Int?=0,
    @ColumnInfo(name = "meeting_frequency") var meeting_frequency: Int?=0,
    @ColumnInfo(name = "meeting_frequency_value") var meeting_frequency_value: Int?=0,
    @ColumnInfo(name = "meeting_on") var meeting_on: Int?=0,
    @ColumnInfo(name = "mode") var mode: Int?=0,
    @ColumnInfo(name = "month_comp_saving") var month_comp_saving: Int?=0,
    @ColumnInfo(name = "is_bankaccount") var is_bankaccount: Int?=0,
    @ColumnInfo(name = "funding_agency_id") var funding_agency_id: Int?=0,
    @ColumnInfo(name = "parent_cbo_code") var parent_cbo_code: Int?=0,
    @ColumnInfo(name = "parent_cbo_type") var parent_cbo_type: Int?=0,
    @ColumnInfo(name = "status") var status: Int?=0,
    @ColumnInfo(name = "is_active") var is_active: Int?=0,
    @ColumnInfo(name = "dedupl_status") var dedupl_status: Int?=0,
    @ColumnInfo(name = "activation_status") var activation_status: Int?=0,
    @ColumnInfo(name = "account_books_maintained") var account_books_maintained: Int?=0,
    @ColumnInfo(name = "cash_book_start_date") var cash_book_start_date: Long?=0,
    @ColumnInfo(name = "bank_book_start_date") var bank_book_start_date: Long?=0,
    @ColumnInfo(name = "members_ledger_start_date") var members_ledger_start_date: Long?=0,
    @ColumnInfo(name = "book4") var book4: Int?=0,
    @ColumnInfo(name = "book5") var book5: Int?=0,
    @ColumnInfo(name = "grade") var grade: String?="",
    @ColumnInfo(name = "grading_done_on") var grading_done_on: Int?=0,
    @ColumnInfo(name = "grade_confirmation_status") var grade_confirmation_status: String?="",
    @ColumnInfo(name = "bookkeeper_identified") var bookkeeper_identified: Int?=0,
    @ColumnInfo (name ="micro_plan_prepared") var micro_plan_prepared:Int?=0,
    @ColumnInfo(name = "mobile_default_user") var mobile_default_user: Int?=0,
    @ColumnInfo(name = "web_default_checker") var web_default_checker: Int?=0,
    @ColumnInfo(name = "entry_source") var entry_source: Int?=0,
    @ColumnInfo(name = "is_edited") var is_edited: Int?=0,
    @ColumnInfo(name = "latitude") var latitude: String?="",
    @ColumnInfo(name = "longitude") var longitude: String?="",
    @ColumnInfo(name = "last_uploaded_date") var last_uploaded_date: Long?=0,
    @ColumnInfo(name = "uploaded_by") var uploaded_by: String?="",
    @ColumnInfo(name = "created_date") var created_date: Long?=0,
    @ColumnInfo(name = "created_by") var created_by: String?="",
    @ColumnInfo(name = "updated_date") var updated_date: Long?=0,
    @ColumnInfo(name = "updated_by") var updated_by: String?="",
    @ColumnInfo(name = "primary_activity") var primary_activity: Int?=0,
    @ColumnInfo(name = "secondary_activity") var secondary_activity: Int?=0,
    @ColumnInfo(name = "tertiary_activity") var tertiary_activity: Int?=0,
    @ColumnInfo(name = "panelty_non_saving") var panelty_non_saving: Double?=0.0,
    @ColumnInfo(name = "interloaning_rate") var interloaning_rate:  Double?=0.0,
    @ColumnInfo(name = "savings_interest") var savings_interest:  Double?=0.0,
    @ColumnInfo(name = "voluntary_savings_interest") var voluntary_savings_interest:  Double?=0.0,
    @ColumnInfo(name = "user_id") var user_id: String?="",
    @ColumnInfo(name = "social_category") var social_category: Int?=0,
    @ColumnInfo(name = "religion") var religion: Int?=0,
    @ColumnInfo(name = "basic_shg_training") var basic_shg_training: Int?=0,
    @ColumnInfo(name = "bookkeeper_name") var bookkeeper_name: String?="",
    @ColumnInfo(name = "bookkeeper_mobile") var bookkeeper_mobile: String?="",
    @ColumnInfo(name = "election_tenure") var election_tenure: Int?=0,
    @ColumnInfo(name = "is_voluntary_saving") var is_voluntary_saving: Int?=0,
    @ColumnInfo(name = "saving_frequency") var saving_frequency: Int?=0,
    @ColumnInfo(name = "tags") var tags: Int?=0,
    @ColumnInfo(name = "checker_remark") var checker_remark: String?="",
    @ColumnInfo(name = "settlement_status") var settlement_status: Int?=0,
    @ColumnInfo(name = "micro_plan_number") var micro_plan_number: Int?=0,
    @ColumnInfo(name = "is_upload") var is_upload: Int?=0,
    @ColumnInfo(name = "shg_cooption_date") var shg_cooption_date: Long?=0,
    @ColumnInfo(name = "approve_status") var approve_status: Int?=0,
    @ColumnInfo(name = "partial_status") var partial_status: Int?=0,
    @ColumnInfo(name = "shg_resolution") var shg_resolution: String?="",
    @ColumnInfo(name = "transaction_id") var transaction_id: String?="",
    @ColumnInfo(name = "shg_type_other") var shg_type_other: String?="",
    @ColumnInfo(name = "inactive_reason") var inactive_reason: Int?=0,
    @ColumnInfo(name = "is_complete") var is_complete: Int?=0,
    @ColumnInfo(name = "is_verified") var is_verified: Int?=0,
    @ColumnInfo(name = "is_locked") var is_locked: Int?=0,
    @ColumnInfo(name = "is_member_edited") var is_member_edited: Int?=0
)
