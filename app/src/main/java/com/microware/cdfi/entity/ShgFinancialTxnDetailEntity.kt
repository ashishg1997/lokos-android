package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = "shg_fin_txn_det_grp", primaryKeys = ["mtg_guid","cbo_id","mtg_no","auid","amount_to_from","trans_date"])
data class ShgFinancialTxnDetailEntity(
    @ColumnInfo(name = "uid") var uid: Long?=0,
    @ColumnInfo(name = "mtg_guid") var mtg_guid: String="",
    @ColumnInfo(name = "cbo_id") var cbo_id: Long=0,
    @ColumnInfo(name = "mtg_no") var mtg_no: Int=0,
    @ColumnInfo(name = "auid") var auid: Int=0,
    @ColumnInfo(name = "fund_type") var fund_type: Int?=0,
    @ColumnInfo(name = "amount_to_from") var amount_to_from: Int=0,
    @ColumnInfo(name = "type") var type: String?="",
    @ColumnInfo(name = "amount") var amount: Int?=0,
    @ColumnInfo(name = "trans_date") var trans_date: Long=0,
   /* @ColumnInfo(name = "deposit_receipt") var deposit_receipt: Float?,
    @ColumnInfo(name = "withdrawal_payment") var withdrawal_payment: Float?,*/
    @ColumnInfo(name = "date_realisation") var date_realisation: Long?=0,
    @ColumnInfo(name = "mode_payment") var mode_payment: Int?=0,
    @ColumnInfo(name = "bank_code") var bank_code: String?="",
    @ColumnInfo(name = "transaction_no") var transaction_no: String?="",
    @ColumnInfo(name = "created_by") var created_by: String?="",
    @ColumnInfo(name = "created_on") var created_on: Long?=0,
    @ColumnInfo(name = "updated_by") var updated_by: String?="",
    @ColumnInfo(name = "updated_on") var updated_on: Long?=0,
    @ColumnInfo(name = "uploaded_by") var uploaded_by: String?="",
    @ColumnInfo(name = "uploaded_on") var uploaded_on: Long?=0
)
