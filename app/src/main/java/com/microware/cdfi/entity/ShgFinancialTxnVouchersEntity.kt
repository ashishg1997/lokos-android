package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tbl_shg_fin_txn_vouchers")
data class ShgFinancialTxnVouchersEntity (@ColumnInfo(name = "cbo_id") var cbo_id:Long=0,
                                          @ColumnInfo (name="member_id") var member_id:Long?=0,
                                          @ColumnInfo(name = "mtg_no") var mtg_no:Int?=0,
                                          @ColumnInfo(name = "mode_payment") var mode_payment:Int?=0,
                                          @ColumnInfo(name="bank_code") var bank_code:String?="",
                                          @ColumnInfo(name = "ref_mtg_no") var ref_mtg_no:Int?=0,
                                          @ColumnInfo(name = "transaction_no") var transaction_no:String?="",
                                          @ColumnInfo (name = "cheque_no") var cheque_no:String?="",
                                          @ColumnInfo (name="amount") var amount:Int?=0,
                                          @ColumnInfo (name = "voucher_date") var voucher_date:Long=0,
                                          @PrimaryKey @ColumnInfo (name ="voucher_no") var voucher_no:String="",
                                          @ColumnInfo (name = "voucher_type") var voucher_type:Int?=0,
                                          @ColumnInfo (name = "auid") var auid:Int?=0,
                                          @ColumnInfo (name = "date_realisation") var date_realisation:Long?=0,
                                          @ColumnInfo (name = "created_by") var created_by:String?="",
                                          @ColumnInfo (name = "created_on") var created_on:Long?=0,
                                          @ColumnInfo (name = "updated_by") var updated_by:String?="",
                                          @ColumnInfo (name = "updated_on") var updated_on:Long?=0,
                                          @ColumnInfo (name = "uploaded_by") var uploaded_by:String?="",
                                          @ColumnInfo (name = "uploaded_on") var uploaded_on:Long?=0)