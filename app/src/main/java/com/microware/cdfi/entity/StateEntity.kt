package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName ="state_master")
data class StateEntity (@PrimaryKey @ColumnInfo(name ="state_id")val state_id:Int,
                        @ColumnInfo(name = "state_name_en")val state_name_en:String?,
                        @ColumnInfo(name = "state_name_hi")val state_name_hi:String?,
                        @ColumnInfo(name = "StateName_Local")val StateName_Local:String?,
                        @ColumnInfo(name = "state_short_local_name")val state_short_local_name:String?,
                        @ColumnInfo(name = "state_short_name_en")val state_short_name_en:String?,
                        @ColumnInfo(name = "category")val category:Int?,
                        @ColumnInfo(name = "lgd_code")val lgd_code:String?,
                        @ColumnInfo(name = "nmmu_code_original") val nmmu_code_original:String?,
                        @ColumnInfo(name = "gps_id") val gps_id:String?,
                        @ColumnInfo(name = "state_code") val state_code:String?

)



