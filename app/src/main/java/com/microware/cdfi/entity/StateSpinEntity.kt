package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName ="state_spin")
data class StateSpinEntity (@PrimaryKey(autoGenerate = true) @ColumnInfo(name ="state_id")val state_id:Int,
                            @ColumnInfo(name = "state_name_en")val state_name_en:String?,
                            @ColumnInfo(name = "state_code") val state_code:String?

)



