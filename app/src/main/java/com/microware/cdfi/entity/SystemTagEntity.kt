package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "systemTags")
data class SystemTagEntity (@ColumnInfo(name ="systemtags_id")val systemtags_id:Long?,
                            @ColumnInfo(name ="cbo_code")val cbo_code:Long?,
                            @ColumnInfo (name ="cbo_guid") val cbo_guid:String?,
                            @PrimaryKey @ColumnInfo(name="system_tag_guid") val system_tag_guid:String,
                            @ColumnInfo (name ="system_type") val system_type:Int?,
                            @ColumnInfo (name ="system_id") val system_id:String?,
                            @ColumnInfo (name ="status") val status:Int?,
                            @ColumnInfo (name ="is_active") val is_active:Int?,
                            @ColumnInfo (name ="entry_source") val entry_source:Int?,
                            @ColumnInfo (name ="is_edited") val is_edited:Int?,
                            @ColumnInfo (name ="last_uploaded_date") val last_uploaded_date:Long?,
                            @ColumnInfo (name ="uploaded_by") val uploaded_by:String?,
                            @ColumnInfo(name ="created_date") val created_date:Long?,
                            @ColumnInfo(name ="created_by") val created_by:String?,
                            @ColumnInfo(name ="updated_date") val updated_date:Long?,
                            @ColumnInfo(name ="updated_by") val updated_by:String?,
                            @ColumnInfo(name = "is_complete") var is_complete: Int?


)