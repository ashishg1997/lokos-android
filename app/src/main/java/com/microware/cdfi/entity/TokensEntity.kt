package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName ="generate_tokens")
data class TokensEntity (@PrimaryKey @ColumnInfo (name ="token_code") val token_code:Int,
                         @ColumnInfo (name ="token_id") val token_id:Int?,
                         @ColumnInfo (name ="created_date") val created_date:Int?,
                         @ColumnInfo (name ="created_by") val created_by:Int?,
                         @ColumnInfo (name ="token_status") val token_status:Int?,
                         @ColumnInfo (name ="device") val device:Int?
                         )