package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "transaction_details")
data class TransactionEntity (@PrimaryKey(autoGenerate = true)
                              @ColumnInfo(name = "id") val id: Int,
                              @ColumnInfo (name = "transaction_no") val transaction_no:String,
                              @ColumnInfo (name = "shg_guid") val shg_guid:String?,
                              @ColumnInfo (name = "member_guid") val member_guid:String?,
                              @ColumnInfo (name = "transaction_type") val transaction_type:Int?,
                              @ColumnInfo(name ="isupload") val isupload:Int?,
                              @ColumnInfo(name ="status") val status:Int?,
                              @ColumnInfo(name ="remarks") val remarks:String?


)

