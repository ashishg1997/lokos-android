package com.microware.cdfi.entity

import androidx.room.*

@Entity(tableName = "mstUser")
data class  UserEntity(
//    @PrimaryKey @ColumnInfo(name = "id") val id: Int,
    @PrimaryKey @ColumnInfo(name = "userId") val userId: String,
    @ColumnInfo(name = "userName") val userName: String?,
//    @ColumnInfo(name = "role") val role: String?,
//    @ColumnInfo(name = "state_code") val state_code: Int,
//    @ColumnInfo(name = "district_code") val district_code: Int,
//    @ColumnInfo(name = "block_code") val block_code: Int,
//    @ColumnInfo(name = "panchayat_code") val panchayat_code: Int,
//    @ColumnInfo(name = "village_code") val village_code: Int,
    @ColumnInfo(name = "designation") val designation: String?,
    @ColumnInfo(name = "emailId") val emailId: String?,
    @ColumnInfo(name = "mobileNo") val mobileNo: String?


)