package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "UserRoleRights")
data class UserRoleRightsEntity (@PrimaryKey@ColumnInfo(name ="userRoleRightsMapId") val userRoleRightsMapId:String,
                                 @ColumnInfo(name = "userId") val userId:String,
                                 @ColumnInfo(name = "categoryId") val categoryId:Int,
                                 @ColumnInfo(name = "levelId") val levelId:String,
                                 @ColumnInfo(name = "typeId") val typeId:String

)