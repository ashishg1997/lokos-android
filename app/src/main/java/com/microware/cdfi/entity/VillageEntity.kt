package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "village_master")
data class VillageEntity (@PrimaryKey @ColumnInfo(name ="village_id") val village_id:Int,
                          @ColumnInfo(name ="state_id") val state_id:String?,
                          @ColumnInfo(name ="district_id") val district_id:String?,
                          @ColumnInfo(name ="block_id") val block_id:String?,
                          @ColumnInfo(name ="panchayat_id") val panchayat_id:String?,
                          @ColumnInfo(name ="village_code") val village_code:String?,
                          @ColumnInfo(name = "village_name_en") val village_name_en:String?,
                          @ColumnInfo(name ="village_name_Local") val village_name_Local:String?,
                          @ColumnInfo(name ="rural_urban_area") val rural_urban_area:String?,
                          @ColumnInfo(name ="gps_id") val gps_id:String?,
                          @ColumnInfo(name = "census2011") val census2011:Int?
)
