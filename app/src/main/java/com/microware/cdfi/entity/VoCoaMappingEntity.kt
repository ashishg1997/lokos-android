package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "vo_coa_mapping",primaryKeys = ["cboId","auid"])
data class VoCoaMappingEntity(
    @ColumnInfo(name = "cboId") val cboId: Long,
    @ColumnInfo(name = "auid") val auid: Int,
    @ColumnInfo(name = "bankcode") val bankcode: String?
)

