package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tbl_VO_Shg_Member")
data class VoShgMemberEntity (
    @ColumnInfo(name = "designation") var designation: Int?=0,
    @ColumnInfo(name = "joining_date") var joining_date: Long?=0,
    @PrimaryKey @ColumnInfo(name = "member_guid")var member_guid: String="",
    @ColumnInfo(name = "vo_guid") var vo_guid: String?="",
    @ColumnInfo(name = "cbo_guid")var cbo_guid: String?="",
    @ColumnInfo(name = "member_id") var member_id: Long?=0,
    @ColumnInfo(name = "member_name") var member_name: String?="",
    @ColumnInfo(name = "member_name_local") var member_name_local: String?="",
    @ColumnInfo(name = "relation") var relation: Int?=0,
    @ColumnInfo(name = "relation_name") var relation_name: String?="",
    @ColumnInfo(name = "relation_name_local") var relation_name_local: String?="",
    @ColumnInfo(name = "shg_name") var shg_name: String?=""

)