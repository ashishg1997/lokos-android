package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName ="tbl_VoShgMemberPhone")
data class VoShgMemberPhoneEntity (@PrimaryKey @ColumnInfo(name = "phone_guid") val phone_guid:String,
                                   @ColumnInfo(name = "member_id") val member_id:Long?,
                                   @ColumnInfo(name = "member_guid") val member_guid:String?,
                                   @ColumnInfo(name = "phone_no") val phone_no:String?
)