package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cadre_role_master")
data class cadreRoleEntity (
    @PrimaryKey @ColumnInfo(name="uid") var uid:Int=0,
    @ColumnInfo(name = "cadre_level") var cadre_level:Int?=0,
    @ColumnInfo(name = "cadre_cat_code") var cadre_cat_code:Int?=0,
    @ColumnInfo(name = "cadre_role_code") var cadre_role_code:Int?=0,
    @ColumnInfo(name = "cadre_role") var cadre_role:String?="",
    @ColumnInfo(name = "language_id") var language_id:String?=""

    )