package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cadreShgMembers")
data class cadreShgMemberEntity (
    @ColumnInfo(name ="uid") val uid:Long?=0,
    @PrimaryKey@ColumnInfo(name ="cadre_guid") var cadre_guid:String="",
    @ColumnInfo(name ="cbo_id") var cbo_id:Long?=null,
    @ColumnInfo(name ="member_guid") var member_guid:String?="",
    @ColumnInfo(name ="member_id") var member_id:Long?=null,
    @ColumnInfo(name ="cadre_role_code") var cadre_role_code:Int?=0,
    @ColumnInfo(name ="cadre_cat_code") var cadre_cat_code:Int?=0,
    @ColumnInfo(name ="date_joining") var date_joining:Long?=0,
    @ColumnInfo(name ="date_leaving") var date_leaving:Long?=0,
    @ColumnInfo(name ="created_by") var created_by:String?="",
    @ColumnInfo(name ="created_on") var created_on:Long?=0,
    @ColumnInfo(name ="updated_by") var updated_by:String?="",
    @ColumnInfo(name ="updated_on") var updated_on:Long?=0,
    @ColumnInfo(name ="uploaded_by") var uploaded_by:String?="",
    @ColumnInfo(name ="uploaded_on") var uploaded_on:Long?=0,
    @ColumnInfo(name = "is_active") var is_active:Int?=0,
    @ColumnInfo(name = "is_edited") var is_edited:Int?=0,
    @ColumnInfo(name = "is_complete") var is_complete:Int?=0
    )