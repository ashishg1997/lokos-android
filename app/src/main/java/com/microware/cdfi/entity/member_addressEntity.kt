package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName="member_address")
data class member_addressEntity (@ColumnInfo(name ="member_address_id") val member_address_id:Long?,
                                 @ColumnInfo(name ="member_code") val member_code:Long?,
                                 @ColumnInfo(name="member_guid") val member_guid: String?,
                                 @PrimaryKey @ColumnInfo(name="address_guid") val address_guid: String,
                                 @ColumnInfo(name ="address_type") val address_type:Int?,
                                 @ColumnInfo(name ="address_line1") val address_line1:String?,
                                 @ColumnInfo(name ="address_line2") val address_line2:String?,
                                 @ColumnInfo(name ="village_id") val village_id:Int?,
                                 @ColumnInfo(name ="panchayat_id") val panchayat_id:Int?,
                                 @ColumnInfo(name ="block_id") val block_id:Int?,
                                 @ColumnInfo(name ="landmark") val landmark:String?,
                                 @ColumnInfo(name ="state_id") val state_id:Int?,
                                 @ColumnInfo(name ="district_id") val district_id:Int?,
    //     @ColumnInfo(name ="City") val City:String?,
                                 @ColumnInfo(name ="postal_code") val postal_code:String?,
                                 @ColumnInfo(name ="is_active") val is_active:Int?,
                                 @ColumnInfo(name ="entry_source") val entry_source:Int?,
                                 @ColumnInfo(name ="is_edited") val is_edited:Int?,
                                 @ColumnInfo(name ="created_date") val created_date:Long?,
                                 @ColumnInfo(name ="created_by") val created_by:String?,
                                 @ColumnInfo(name ="updated_date") val updated_date:Long?,
                                 @ColumnInfo(name ="updated_by") val updated_by:String?,
                                 @ColumnInfo(name ="last_uploaded_date") val last_uploaded_date:Long?,
                                 @ColumnInfo(name ="uploaded_by") val uploaded_by:String?,
                                 @ColumnInfo(name ="status") val status:Int?,
                                 @ColumnInfo(name ="cbo_id") val cbo_id:Int?,
                                 @ColumnInfo(name = "is_complete") var is_complete: Int?,
                                 @ColumnInfo(name = "address_location") var address_location: Int?,
                                 @ColumnInfo(name = "is_verified") var is_verified: Int?=0
)