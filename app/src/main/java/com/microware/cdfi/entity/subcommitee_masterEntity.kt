package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tbl_subcommitee_master",primaryKeys = ["subcommitee_type_id","language_id"])
data class subcommitee_masterEntity (@ColumnInfo (name ="subcommitee_type_id") val subcommitee_type_id:Int,
                                     @ColumnInfo (name ="subcommitee_name") val subcommitee_name:String?,
                                     @ColumnInfo(name = "language_id") val language_id: String,
                                     @ColumnInfo (name ="is_active") val is_active:Int?=1
)
