package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName ="tbl_subcommitee_member",primaryKeys = ["subcommitee_guid","ec_member_code","is_active"])
data class subcommitee_memberEntity (@ColumnInfo (name ="subcommitee_guid") val subcommitee_guid:String,
                                     @ColumnInfo(name ="subcommitee_code") val subcommitee_code:Long?,
                                     @ColumnInfo(name = "ec_member_id") val ec_member_id:Long?,
                                     @ColumnInfo (name ="ec_member_guid") val ec_member_guid:String?,
                                     @ColumnInfo(name = "ec_member_code") val ec_member_code:Long,
                                     @ColumnInfo(name = "fromdate") val fromdate:Long?,
                                     @ColumnInfo(name = "todate") val todate:Long?,
                                     @ColumnInfo(name = "is_active") val is_active:Int,
                                     @ColumnInfo(name = "entry_source") val entry_source:Int?,
                                     @ColumnInfo(name = "status") val status:Int?,
                                     @ColumnInfo(name = "is_edited") val is_edited:Int?,
                                     @ColumnInfo(name = "last_uploaded_date") val last_uploaded_date:Long?,
                                     @ColumnInfo(name = "uploaded_by") val uploaded_by:String?,
                                     @ColumnInfo(name = "created_date") val created_date:Long?,
                                     @ColumnInfo(name = "created_by") val created_by:String?,
                                     @ColumnInfo(name = "updated_date") val updated_date:Long?,
                                     @ColumnInfo(name = "updated_by") val updated_by:String?,
                                     @ColumnInfo(name = "is_upload") val is_upload:Int?=0,
                                     @ColumnInfo(name = "is_complete") val is_complete:Int?=1
)