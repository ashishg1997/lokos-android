package com.microware.cdfi.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tbl_subcommittee")
data class subcommitteeEntity (@PrimaryKey @ColumnInfo(name ="subcommitee_guid") var subcommitee_guid:String="",
                               @ColumnInfo(name ="subcommitee_code") var subcommitee_code:Long?=0,
                               @ColumnInfo(name ="subcommitee_type_id") var subcommitee_type_id:Int?=0,
                               @ColumnInfo(name = "cbo_id") var cbo_id:Long?=0,
                               @ColumnInfo(name = "cbo_guid") var cbo_guid:String?="",
                               @ColumnInfo(name = "cbo_code") var cbo_code:Long?=0,
                               @ColumnInfo(name = "fromdate") var fromdate:Long?=0,
                               @ColumnInfo(name = "todate") var todate:Long?=0,
                               @ColumnInfo(name = "is_active") var is_active:Int?=0,
                               @ColumnInfo(name = "entry_source") var entry_source:Int?=0,
                               @ColumnInfo(name = "status") var status:Int?=0,
                               @ColumnInfo(name = "is_edited") var is_edited:Int?=0,
                               @ColumnInfo(name = "last_uploaded_date") var last_uploaded_date:Long?=0,
                               @ColumnInfo(name = "uploaded_by") var uploaded_by:String?="",
                               @ColumnInfo(name = "created_date") var created_date:Long?=0,
                               @ColumnInfo(name = "created_by") var created_by:String?="",
                               @ColumnInfo(name = "updated_date") var updated_date:Long?=0,
                               @ColumnInfo(name = "updated_by") var updated_by:String?="",
                               @ColumnInfo(name = "is_upload") var is_upload:Int?=0,
                               @ColumnInfo(name = "is_complete") var is_complete:Int?=0

)