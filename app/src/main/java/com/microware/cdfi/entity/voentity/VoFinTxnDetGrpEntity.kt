package com.microware.cdfi.entity.voentity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "vo_fin_txn_det_grp", primaryKeys = ["mtgGuid","cboId","mtgNo","auid","amountToFrom"])
data class VoFinTxnDetGrpEntity(
    @ColumnInfo(name = "uid") var uid: Long?,
    @ColumnInfo(name = "voMtgUid") var voMtgUid: Long?,
    @ColumnInfo(name = "mtgGuid") var mtgGuid: String,
    @ColumnInfo(name = "cboId") var cboId: Long,
    @ColumnInfo(name = "mtgNo") var mtgNo: Int,
    @ColumnInfo(name = "mtgDate") var mtgDate: Long?,
    @ColumnInfo(name = "auid") var auid: Int,
    @ColumnInfo(name = "fundType") var fundType: Int?,
    @ColumnInfo(name = "fundSource") var fundSource: Int?,
    @ColumnInfo(name = "amountToFrom") var amountToFrom: Int,
    @ColumnInfo(name = "type") var type: String?,
    @ColumnInfo(name = "amount") var amount: Int?,
    @ColumnInfo(name = "transactionNo") var transactionNo: String?,
    @ColumnInfo(name = "dateRealisation") var dateRealisation: Long?,
    @ColumnInfo(name = "modePayment") var modePayment: Int?,
    @ColumnInfo(name = "bankCode") var bankCode: String?,
//    @ColumnInfo(name = "voucherNumber") var voucherNumber: String?,
//    @ColumnInfo(name = "voucherDate") var voucherDate: Long?,
    @ColumnInfo(name = "narration") var narration: String?,
    @ColumnInfo(name = "AddlRefDate") var AddlRefDate: Long?,
    @ColumnInfo(name = "createdBy") var createdBy: String?,
    @ColumnInfo(name = "createdOn") var createdOn: Long?,
    @ColumnInfo(name = "updatedBy") var updatedBy: String?,
    @ColumnInfo(name = "updatedOn") var updatedOn: Long?,
    @ColumnInfo(name = "uploadedBy") var uploadedBy: String?,
    @ColumnInfo(name = "uploadedOn") var uploadedOn: Long?,
    @ColumnInfo(name = "isEdited") var isEdited: Int?=0,
    @ColumnInfo(name = "orgType") var orgType: Int?=0

)

