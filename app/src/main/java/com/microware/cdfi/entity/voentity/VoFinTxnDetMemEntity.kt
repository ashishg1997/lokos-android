package com.microware.cdfi.entity.voentity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "vo_fin_txn_det_mem",primaryKeys = ["mtgGuid","cboId","memId","mtgNo","auid"])
data class VoFinTxnDetMemEntity(
    @ColumnInfo(name = "uid") var uid: Long?,
    @ColumnInfo(name = "mtgGuid") var mtgGuid: String,
    @ColumnInfo(name = "cboId") var cboId: Long,
    @ColumnInfo(name = "memId") var memId: Long,
    @ColumnInfo(name = "mtgNo") var mtgNo: Int,
    @ColumnInfo(name = "mtgDate") var mtgDate: Long?,
    @ColumnInfo(name = "auid") var auid: Int,
    @ColumnInfo(name = "type") var type: String?,
    @ColumnInfo(name = "amount") var amount: Int?,
    @ColumnInfo(name = "modePayment") var modePayment: Int?,
    @ColumnInfo(name = "dateRealisation") var dateRealisation: Long?,
    @ColumnInfo(name = "bankCode") var bankCode: String?,
    @ColumnInfo(name = "transactionNo") var transactionNo: String?,
//    @ColumnInfo(name = "voucherNumber") var voucherNumber: String?,
//    @ColumnInfo(name = "voucherDate") var voucherDate: Long?,
    @ColumnInfo(name = "narration") var narration: String?,
    @ColumnInfo(name = "AddlRefDate") var AddlRefDate: Long?,
    @ColumnInfo(name = "createdBy") var createdBy: String?,
    @ColumnInfo(name = "createdOn") var createdOn: Long?,
    @ColumnInfo(name = "updatedBy") var updatedBy: String?,
    @ColumnInfo(name = "updatedOn") var updatedOn: Long?,
    @ColumnInfo(name = "uploadedBy") var uploadedBy: String?,
    @ColumnInfo(name = "uploadedOn") var uploadedOn: Long?,
    @ColumnInfo(name = "referenceMtgNo") var referenceMtgNo: Int?,
    @ColumnInfo(name = "isEdited") var isEdited: Int?=0,
    @ColumnInfo(name = "orgType") var orgType: Int?=0

)

