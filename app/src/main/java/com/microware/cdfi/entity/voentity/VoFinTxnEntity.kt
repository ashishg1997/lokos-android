package com.microware.cdfi.entity.voentity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "vo_fin_txn", primaryKeys = ["cboId", "mtgGuid", "mtgNo", "bankCode"])
data class VoFinTxnEntity(
    @ColumnInfo(name = "uid") var uid: Long?,
    @ColumnInfo(name = "voMtgUid") var voMtgUid: Long?,
    @ColumnInfo(name = "cboId") var cboId: Long,
    @ColumnInfo(name = "mtgGuid") var mtgGuid: String,
    @ColumnInfo(name = "mtgNo") var mtgNo: Int,
    @ColumnInfo(name = "bankCode") var bankCode: String,
    @ColumnInfo(name = "openingBalance") var openingBalance: Int?,
    @ColumnInfo(name = "closingBalance") var closingBalance: Int?,
    @ColumnInfo(name = "otherDeposits") var otherDeposits: Int?,
    @ColumnInfo(name = "otherWithdrawals") var otherWithdrawals: Int?,
    @ColumnInfo(name = "depositedCash") var depositedCash: Int?,
    @ColumnInfo(name = "withdrawnCash") var withdrawnCash: Int?,
    @ColumnInfo(name = "openingBalanceCash") var openingBalanceCash: Int?,
    @ColumnInfo(name = "closingBalanceCash") var closingBalanceCash: Int?,
    @ColumnInfo(name = "createdBy") var createdBy: String?,
    @ColumnInfo(name = "createdOn") var createdOn: Long?,
    @ColumnInfo(name = "updatedBy") var updatedBy: String?,
    @ColumnInfo(name = "updatedOn") var updatedOn: Long?,
    @ColumnInfo(name = "uploadedBy") var uploadedBy: String?,
    @ColumnInfo(name = "uploadedOn") var uploadedOn: Long?,
    @ColumnInfo(name = "zeroMtgCashBank") var zeroMtgCashBank: Int?,
    @ColumnInfo(name = "chequeIssuedNotRealized") var chequeIssuedNotRealized: Int?,
    @ColumnInfo(name = "chequeReceivedNotCredited") var chequeReceivedNotCredited: Int?,
    @ColumnInfo(name = "balanceDate") var balanceDate: Long?

)

