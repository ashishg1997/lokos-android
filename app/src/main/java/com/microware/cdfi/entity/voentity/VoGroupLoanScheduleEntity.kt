package com.microware.cdfi.entity.voentity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "vo_group_loan_schedule",primaryKeys = ["cboId","loanNo","installmentNo","subInstallmentNo"])
data class VoGroupLoanScheduleEntity(
    @ColumnInfo(name = "uid") var uid: Long?,
    @ColumnInfo(name = "cboId") var cboId: Long,
    @ColumnInfo(name = "mtgGuid") var mtgGuid: String?,
    @ColumnInfo(name = "loanNo") var loanNo: Int,
    @ColumnInfo(name = "principalDemand") var principalDemand: Int?,
    @ColumnInfo(name = "loanDemandOs") var loanDemandOs: Int?,
    @ColumnInfo(name = "loanOs") var loanOs: Int?,
    @ColumnInfo(name = "loanPaid") var loanPaid: Int?,
    @ColumnInfo(name = "installmentNo") var installmentNo: Int,
    @ColumnInfo(name = "subInstallmentNo") var subInstallmentNo: Int,
    @ColumnInfo(name = "installmentDate") var installmentDate: Long?,
    @ColumnInfo(name = "loanDate") var loanDate: Long?,
    @ColumnInfo(name = "repaid") var repaid: Boolean?,
    @ColumnInfo(name = "lastPaidDate") var lastPaidDate: Long?,
    @ColumnInfo(name = "mtgNo") var mtgNo: Int?,
    @ColumnInfo(name = "createdBy") var createdBy: String?,
    @ColumnInfo(name = "createdOn") var createdOn: Long?,
    @ColumnInfo(name = "updatedBy") var updatedBy: String?,
    @ColumnInfo(name = "updatedOn") var updatedOn: Long?,
    @ColumnInfo(name = "uploadedBy") var uploadedBy: String?,
    @ColumnInfo(name = "uploadedOn") var uploadedOn: Long?
)

