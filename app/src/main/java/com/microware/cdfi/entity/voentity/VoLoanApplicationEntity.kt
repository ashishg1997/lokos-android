package com.microware.cdfi.entity.voentity

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = "vo_loan_application",primaryKeys = ["loanApplicationId","cboId","memId"])
data class VoLoanApplicationEntity(
    @ColumnInfo(name = "uid") var uid: Long?=0,
    @ColumnInfo(name = "voMtgDetUid") var voMtgDetUid: Long?=0,
    @ColumnInfo(name = "loanApplicationId") var loanApplicationId: Long=0,
    @ColumnInfo(name = "cboId") var cboId: Long=0,
    @ColumnInfo(name = "memId") var memId: Long=0,
    @ColumnInfo(name = "requestDate") var requestDate: Long?=0,
    @ColumnInfo(name = "loanFee") var loanFee: Int?=0,
    @ColumnInfo(name = "amtDemand") var amtDemand: Int?=0,
    @ColumnInfo(name = "amtSanction") var amtSanction: Int?=0,
    @ColumnInfo(name = "amtDisbursed") var amtDisbursed: Int?=0,
    @ColumnInfo(name = "approvalDate") var approvalDate: Long?=0,
    @ColumnInfo(name = "tentativeDate") var tentativeDate: Long?=0,
    @ColumnInfo(name = "loanProductId") var loanProductId: Int?=0,
    @ColumnInfo(name = "loanSource") var loanSource: Int?=0,
    @ColumnInfo(name = "loanPurpose") var loanPurpose: Int?=0,
    @ColumnInfo(name = "loanPeriod") var loanPeriod: Int?=0,
    @ColumnInfo(name = "loanRequestedMtgNo") var loanRequestedMtgNo: Int?=0,
    @ColumnInfo(name = "loanRequestedMtgGuid") var loanRequestedMtgGuid: String?="",
    @ColumnInfo(name = "loanSanctionedMtgNo") var loanSanctionedMtgNo: Long?=0,
    @ColumnInfo(name = "loanSanctionedMtgGuid") var loanSanctionedMtgGuid: String?="",
    @ColumnInfo(name = "createdBy") var createdBy: String?="",
    @ColumnInfo(name = "createdOn") var createdOn: Long?=0,
    @ColumnInfo(name = "updatedBy") var updatedBy: String?="",
    @ColumnInfo(name = "updatedOn") val updatedOn: Long?,
    @ColumnInfo(name = "uploadedBy") var uploadedBy: String?="",
    @ColumnInfo(name = "uploadedOn") var uploadedOn: Long?=0,
    @ColumnInfo(name = "loanRequestPriority") var loanRequestPriority: Int?=0
)