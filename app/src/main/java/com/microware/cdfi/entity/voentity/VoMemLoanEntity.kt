package com.microware.cdfi.entity.voentity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "vo_mem_loan", primaryKeys = ["cboId", "memId", "loanNo"])
data class VoMemLoanEntity(
    @ColumnInfo(name = "uid") var uid: Long? = 0,
    @ColumnInfo(name = "voMtgDetUid") var voMtgDetUid: Long? = 0,
    @ColumnInfo(name = "loanApplicationId") var loanApplicationId: Long? = 0,
    @ColumnInfo(name = "mtgGuid") var mtgGuid: String? = "",
    @ColumnInfo(name = "cboId") var cboId: Long = 0,
    @ColumnInfo(name = "memId") var memId: Long = 0,
    @ColumnInfo(name = "loanNo") var loanNo: Int = 0,
    @ColumnInfo(name = "mtgNo") var mtgNo: Int? = 0,
    @ColumnInfo(name = "mtgDate") var mtgDate: Long? = 0,
    @ColumnInfo(name = "installmentDate") var installmentDate: Long? = 0,
    @ColumnInfo(name = "originalLoanAmount") var originalLoanAmount: Int? = 0,
    @ColumnInfo(name = "amount") var amount: Int? = 0,
    @ColumnInfo(name = "loanPurpose") var loanPurpose: Int? = 0,
    @ColumnInfo(name = "fundType") var fundType: Int? = 0,
    @ColumnInfo(name = "interestRate") var interestRate: Double? = 0.0,
    @ColumnInfo(name = "period") var period: Int? = 0,
    @ColumnInfo(name = "principalOverdue") var principalOverdue: Int? = 0,
    @ColumnInfo(name = "interestOverdue") var interestOverdue: Int? = 0,
    @ColumnInfo(name = "completionFlag") var completionFlag: Boolean? = null,
    @ColumnInfo(name = "loanType") var loanType: Int? = 0,
    @ColumnInfo(name = "loanSource") var loanSource: Int? = 0,
    @ColumnInfo(name = "modePayment") var modePayment: Int? = 0,
    @ColumnInfo(name = "bankCode") var bankCode: String? = "",
    @ColumnInfo(name = "transactionNo") var transactionNo: String? = "",
    @ColumnInfo(name = "installmentFreq") var installmentFreq: Int? = 0,
    @ColumnInfo(name = "moratoriumPeriod") var moratoriumPeriod: Int? = 0,
    @ColumnInfo(name = "createdBy") var createdBy: String? = "",
    @ColumnInfo(name = "createdOn") var createdOn: Long? = 0,
    @ColumnInfo(name = "updatedBy") var updatedBy: String? = "",
    @ColumnInfo(name = "updatedOn") var updatedOn: Long? = 0,
    @ColumnInfo(name = "uploadedBy") var uploadedBy: String? = "",
    @ColumnInfo(name = "uploadedOn") var uploadedOn: Long? = 0,
    @ColumnInfo(name = "principalRepaid") var principalRepaid: Int? = 0,
    @ColumnInfo(name = "interestRepaid") var interestRepaid: Int? = 0,
    @ColumnInfo(name = "disbursementDate") var disbursementDate: Long? = 0,
    @ColumnInfo(name = "sanctionDate") var sanctionDate: Long? = 0,
    @ColumnInfo(name = "rescheduleReason") var rescheduleReason: Int? = 0,
    @ColumnInfo(name = "originalInterestRate") var originalInterestRate: Double? = 0.0,
    @ColumnInfo(name = "originalPeriod") var originalPeriod: Int? = 0,
    @ColumnInfo(name = "noOfLoans") var noOfLoans: Int? = 0,
    @ColumnInfo(name = "isEdited") var isEdited: Int?=0
)

