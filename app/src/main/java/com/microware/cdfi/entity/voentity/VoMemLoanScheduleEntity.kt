package com.microware.cdfi.entity.voentity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "vo_mem_loan_schedule",primaryKeys = ["cboId","loanNo","memId","installmentNo","subInstallmentNo"])
data class VoMemLoanScheduleEntity(
   @ColumnInfo(name = "uid") var uid: Long? = 0,
    @ColumnInfo(name = "cboId") var cboId: Long = 0,
    @ColumnInfo(name = "memId") var memId: Long = 0,
    @ColumnInfo(name = "mtgGuid") var mtgGuid: String? = "",
    @ColumnInfo (name = "loanNo") var loanNo:Int = 0,
    @ColumnInfo(name = "principalDemand") var principalDemand: Int? = 0,
    @ColumnInfo(name = "loanDemandOs") var loanDemandOs: Int? = 0,
    @ColumnInfo(name = "loanOs") var loanOs: Int? = 0,
    @ColumnInfo(name = "loanPaid") var loanPaid: Int? = 0,
    @ColumnInfo(name = "installmentNo") var installmentNo: Int = 0,
    @ColumnInfo(name = "subInstallmentNo") var subInstallmentNo: Int = 0,
    @ColumnInfo(name = "installmentDate") var installmentDate: Long? = 0,
    @ColumnInfo(name = "repaid") var repaid: Boolean? = null,
    @ColumnInfo(name = "lastPaidDate") var lastPaidDate: Long? = 0,
    @ColumnInfo(name = "mtgNo") var mtgNo: Int? = 0,
    @ColumnInfo(name = "createdBy") var createdBy: String? = "",
    @ColumnInfo(name = "createdOn") var createdOn: Long? = 0,
    @ColumnInfo(name = "updatedBy") var updatedBy: String? = "",
    @ColumnInfo(name = "updatedOn") var updatedOn: Long? = 0,
    @ColumnInfo(name = "uploadedBy") var uploadedBy: String? = "",
    @ColumnInfo(name = "uploadedOn") var uploadedOn: Long? = 0

)

