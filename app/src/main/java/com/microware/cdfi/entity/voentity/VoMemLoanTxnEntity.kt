package com.microware.cdfi.entity.voentity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "vo_mem_loan_txn",primaryKeys = ["cboId","memId","mtgGuid","mtgNo","loanNo"])
data class VoMemLoanTxnEntity(
    @ColumnInfo(name = "uid") var uid: Long,
    @ColumnInfo(name = "voMtgDetUid") var voMtgDetUid: Long?,
    @ColumnInfo(name = "voMemLoanUid") var voMemLoanUid: Long?,
    @ColumnInfo(name = "cboId") var cboId: Long,
    @ColumnInfo(name = "memId") var memId: Long,
    @ColumnInfo(name = "mtgGuid") var mtgGuid: String,
    @ColumnInfo(name = "mtgNo") var mtgNo: Int,
    @ColumnInfo(name = "mtgDate") var mtgDate: Long?,
    @ColumnInfo(name = "loanOp") var loanOp: Int?,
    @ColumnInfo(name = "loanOpInt") var loanOpInt: Int?,
    @ColumnInfo(name = "loanPaid") var loanPaid: Int?,
    @ColumnInfo(name = "loanPaidInt") var loanPaidInt: Int?,
    @ColumnInfo(name = "loanCl") var loanCl: Int?,
    @ColumnInfo(name = "loanClInt") var loanClInt: Int?,
    @ColumnInfo(name = "completionFlag") var completionFlag: Boolean?,
    @ColumnInfo(name = "intAccruedOp") var intAccruedOp: Int?,
    @ColumnInfo(name = "intAccrued") var intAccrued: Int?,
    @ColumnInfo(name = "intAccruedCl") var intAccruedCl: Int?,
    @ColumnInfo(name = "principalDemandOb") var principalDemandOb: Int?,
    @ColumnInfo(name = "principalDemand") var principalDemand: Int?,
    @ColumnInfo(name = "principalDemandCb") var principalDemandCb: Int?,
    @ColumnInfo(name = "modePayment") var modePayment: Int?,
    @ColumnInfo(name = "bankCode") var bankCode: String?,
    @ColumnInfo(name = "transactionNo") var transactionNo: String?,
    @ColumnInfo(name = "createdBy") var createdBy: String?,
    @ColumnInfo(name = "createdOn") var createdOn: Long?,
    @ColumnInfo(name = "updatedBy") var updatedBy: String?,
    @ColumnInfo(name = "updatedOn") var updatedOn: Long?,
    @ColumnInfo(name = "uploadedBy") var uploadedBy: String?,
    @ColumnInfo(name = "uploadedOn") var uploadedOn: Long?,
    @ColumnInfo(name = "loanNo") var loanNo: Int,
    @ColumnInfo(name = "interestRate") var interestRate: Double?,
    @ColumnInfo(name = "period") var period: Int?
)

