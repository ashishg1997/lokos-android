package com.microware.cdfi.entity.voentity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "vo_mem_settlement",primaryKeys = ["cboId","memId","mtgGuid","mtgNo"])
data class VoMemSettlementEntity(
    @ColumnInfo(name = "uid") var uid: Int=0,
    @ColumnInfo(name = "cboId") var cboId: Long=0,
    @ColumnInfo(name = "mtgGuid") var mtgGuid: String="",
    @ColumnInfo(name = "mtgNo") var mtgNo: Int=0,
    @ColumnInfo(name = "mtgDate") var mtgDate: Long?=0,
    @ColumnInfo(name = "memId") var memId: Long=0,
    @ColumnInfo(name = "savComp") var savComp: Int=0,
    @ColumnInfo(name = "savVol") var savVol: Int=0,
    @ColumnInfo(name = "shareCapital") var shareCapital: Int=0,
    @ColumnInfo(name = "memberSurplus") var memberSurplus: Int=0,
    @ColumnInfo(name = "loanOutstanding") var loanOutstanding: Int=0,
    @ColumnInfo(name = "memberDeficit") var memberDeficit: Int=0,
    @ColumnInfo(name = "availableAmt") var availableAmt: Int=0,
    @ColumnInfo(name = "paidAmt") var paidAmt: Int=0,
    @ColumnInfo(name = "receiverName") var receiverName: String="",
    @ColumnInfo(name = "receiverRelation") var receiverRelation: Int=0,
    @ColumnInfo(name = "reason") var reason: Int=0,
    @ColumnInfo(name = "paymentDate") var paymentDate: Long?=0,
    @ColumnInfo(name = "modePayment") var modePayment: Int?=0,
    @ColumnInfo(name = "bankCode") var bankCode: String?="",
    @ColumnInfo(name = "transactionNo") var transactionNo: String?="",
    @ColumnInfo(name = "narration") var narration: String?="",
    @ColumnInfo(name = "createdBy") var createdBy: String?=null,
    @ColumnInfo(name = "createdOn") var createdOn: Long?=null,
    @ColumnInfo(name = "updatedBy") var updatedBy: String?=null,
    @ColumnInfo(name = "updatedOn") var updatedOn: Long?=null,
    @ColumnInfo(name = "uploadedBy") var uploadedBy: String?=null,
    @ColumnInfo(name = "uploadedOn") var uploadedOn: Long?=null,
    @ColumnInfo(name = "other") var other: Int?=0,
    @ColumnInfo(name = "otherSpecify") var otherSpecify: String?="",
    @ColumnInfo(name = "settlementStatus") var settlementStatus: Int?=0
)