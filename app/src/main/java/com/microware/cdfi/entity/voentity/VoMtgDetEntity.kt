package com.microware.cdfi.entity.voentity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "vo_mtg_det",primaryKeys = ["cboId","memId","mtgNo"])
data class VoMtgDetEntity(
    @ColumnInfo(name = "uid") var uid: Long? = 0,
    @ColumnInfo(name = "voMtgUid") var voMtgUid: Long? = 0,
    @ColumnInfo(name = "cboId") var cboId: Long = 0,
    @ColumnInfo(name = "memId") var memId: Long = 0,
    @ColumnInfo(name = "mtgGuid") var mtgGuid: String? ="",
    @ColumnInfo(name = "mtgNo") var mtgNo: Int = 0,
    @ColumnInfo(name = "mtgDate") var mtgDate: Long? = 0,
    @ColumnInfo(name = "sNo") var sNo: Int? = 0,
    @ColumnInfo(name = "childCboName") var childCboName: String? = "",
    @ColumnInfo(name = "attendance") var attendance: String? = "",
    @ColumnInfo(name = "ec1") var ec1: String? = "",
    @ColumnInfo(name = "ec2") var ec2: String? = "",
    @ColumnInfo(name = "ec3") var ec3: String? ="",
    @ColumnInfo(name = "ec4") var ec4: String? ="",
    @ColumnInfo(name = "ec5") var ec5: String? ="",
    @ColumnInfo(name = "attendanceOther") var attendanceOther: Int? = 0,
    @ColumnInfo(name = "attendanceExternal") var attendanceExternal: Int? = 0,
    @ColumnInfo(name = "status") var status: String? ="",
    @ColumnInfo(name = "zeroMtgAttn") var zeroMtgAttn: Int? = 0,
    @ColumnInfo(name = "savCompOb") var savCompOb: Int?=0,
    @ColumnInfo(name = "savComp") var savComp: Int?=0,
    @ColumnInfo(name = "savCompCb") var savCompCb: Int?=0,
    @ColumnInfo(name = "savVolOb") var savVolOb: Int?=0,
    @ColumnInfo(name = "savVol") var savVol: Int?=0,
    @ColumnInfo(name = "savVolCb") var savVolCb: Int?=0,
    @ColumnInfo(name = "savVolWithdrawal") var savVolWithdrawal: Int?=0,
    @ColumnInfo(name = "createdBy") var createdBy: String? ="",
    @ColumnInfo(name = "createdOn") var createdOn: Long? = 0,
    @ColumnInfo(name = "updatedBy") var updatedBy: String? ="",
    @ColumnInfo(name = "updatedOn") var updatedOn: Long? = 0,
    @ColumnInfo(name = "uploadedBy") var uploadedBy: String? ="",
    @ColumnInfo(name = "uploadedOn") var uploadedOn: Long? = 0,
    @ColumnInfo(name = "settlementStatus") var settlementStatus: Int?=0

)

