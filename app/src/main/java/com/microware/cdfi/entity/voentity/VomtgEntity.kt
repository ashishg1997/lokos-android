package com.microware.cdfi.entity.voentity

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = "vo_mtg", primaryKeys = ["cboId", "mtgGuid", "mtgNo"])
data class VomtgEntity(
    @ColumnInfo(name = "uid") var uid: Long? = 0,
    @ColumnInfo(name = "cboId") var cboId: Long = 0,
    @ColumnInfo(name = "mtgGuid") var mtgGuid: String = "",
    @ColumnInfo(name = "mtgType") var mtgType: Int? = 0,
    @ColumnInfo(name = "mtgNo") var mtgNo: Int = 0,
    @ColumnInfo(name = "mtgDate") var mtgDate: Long? = 0,
    @ColumnInfo(name = "flagOpen") var flagOpen: String? = "",
    @ColumnInfo(name = "mtgFrq") var mtgFrq: String? = "",
    @ColumnInfo(name = "flagDow") var flagDow: String? = "",
    @ColumnInfo(name = "daysElapsed") var daysElapsed: Int? = 0,
    @ColumnInfo(name = "openingBalance") var openingBalance: Int? = 0,
    @ColumnInfo(name = "closingBalance") var closingBalance: Int? = 0,
    @ColumnInfo(name = "totalMeetingHeld") var totalMeetingHeld: Int? = 0,
    @ColumnInfo(name = "expectedDate") var expectedDate: Long? = 0,
    @ColumnInfo(name = "approvalStatus") var approvalStatus: Int? = 0,
    @ColumnInfo(name = "actionBy") var actionBy: String? = "",
    @ColumnInfo(name = "actionOn") var actionOn: Long? = 0,
    @ColumnInfo(name = "mnthCompSav") var mnthCompSav: Int? = 0,
    @ColumnInfo(name = "totalAttendance") var totalAttendance: Int? = 0,
    @ColumnInfo(name = "savCompOb") var savCompOb: Int? = 0,
    @ColumnInfo(name = "savComp") var savComp: Int? = 0,
    @ColumnInfo(name = "savCompCb") var savCompCb: Int? = 0,
    @ColumnInfo(name = "savCompWithdrawal") var savCompWithdrawal: Int? = 0,
    @ColumnInfo(name = "savVolOb") var savVolOb: Int? = 0,
    @ColumnInfo(name = "savVol") var savVol: Int? = 0,
    @ColumnInfo(name = "savVolCb") var savVolCb: Int? = 0,
    @ColumnInfo(name = "savVolWithdrawal") var savVolWithdrawal: Int? = 0,
    @ColumnInfo(name = "memOtherReceipts") var memOtherReceipts: Int? = 0,
    @ColumnInfo(name = "memOtherPayments") var memOtherPayments: Int? = 0,
    @ColumnInfo(name = "noLoansDisbursed") var noLoansDisbursed: Int? = 0,
    @ColumnInfo(name = "amtLoansDisbursed") var amtLoansDisbursed: Int? = 0,
    @ColumnInfo(name = "amtLoansDemand") var amtLoansDemand: Int? = 0,/*0*/
    @ColumnInfo(name = "noLoansRepaid") var noLoansRepaid: Int? = 0,
    @ColumnInfo(name = "amtLoansRepaid") var amtLoansRepaid: Int? = 0,
    @ColumnInfo(name = "noLoanOverdue") var noLoanOverdue: Int? = 0,
    @ColumnInfo(name = "amtLoansOverdue") var amtLoansOverdue: Int? = 0,
    @ColumnInfo(name = "noLoansOs") var noLoansOs: Int? = 0,
    @ColumnInfo(name = "amtLoansOs") var amtLoansOs: Int? = 0,
    @ColumnInfo(name = "amtInterestReceived") var amtInterestReceived: Int? = 0,
    @ColumnInfo(name = "noLoansNpa") var noLoansNpa: Int? = 0,
    @ColumnInfo(name = "amtLoansNpa") var amtLoansNpa: Int? = 0,
    @ColumnInfo(name = "noLoansReceivedExt") var noLoansReceivedExt: Int? = 0,
    @ColumnInfo(name = "amtLoansReceivedExt") var amtLoansReceivedExt: Int? = 0,
    @ColumnInfo(name = "noLoansRepaidExt") var noLoansRepaidExt: Int? = 0,
    @ColumnInfo(name = "amtLoansRepaidExt") var amtLoansRepaidExt: Int? = 0,
    @ColumnInfo(name = "noLoanOverdueExt") var noLoanOverdueExt: Int? = 0,
    @ColumnInfo(name = "amtLoansOverdueExt") var amtLoansOverdueExt: Int? = 0,
    @ColumnInfo(name = "amtLoansDemandExt") var amtLoansDemandExt: Int? = 0,/*1*/
    @ColumnInfo(name = "noLoansOsExt") var noLoansOsExt: Int? = 0,
    @ColumnInfo(name = "amtLoansOsExt") var amtLoansOsExt: Int? = 0,
    @ColumnInfo(name = "amtInterestPaidExt") var amtInterestPaidExt: Int? = 0,
    @ColumnInfo(name = "noLoansNpaExt") var noLoansNpaExt: Int? = 0,
    @ColumnInfo(name = "amtLoansNpaExt") var amtLoansNpaExt: Int? = 0,
    @ColumnInfo(name = "otherReceipts") var otherReceipts: Int? = 0,
    @ColumnInfo(name = "otherPayments") var otherPayments: Int? = 0,
    @ColumnInfo(name = "depositedCash") var depositedCash: Int? = 0,
    @ColumnInfo(name = "withdrawnCash") var withdrawnCash: Int? = 0,
    @ColumnInfo(name = "openingBalanceCash") var openingBalanceCash: Int? = 0,
    @ColumnInfo(name = "closingBalanceCash") var closingBalanceCash: Int? = 0,
    @ColumnInfo(name = "createdBy") var createdBy: String? = "",
    @ColumnInfo(name = "createdOn") var createdOn: Long? = 0,
    @ColumnInfo(name = "updatedBy") var updatedBy: String? = "",
    @ColumnInfo(name = "updatedOn") var updatedOn: Long? = 0,
    @ColumnInfo(name = "uploadedBy") var uploadedBy: String? = "",
    @ColumnInfo(name = "uploadedOn") var uploadedOn: Long? = 0,
    @ColumnInfo(name = "zeroMtgCashInHand") var zeroMtgCashInHand: Int? = 0,
    @ColumnInfo(name = "zeroMtgCashInTransit") var zeroMtgCashInTransit: Int? = 0,
    @ColumnInfo(name = "balanceDate") var balanceDate: Long? = 0,
    @ColumnInfo(name = "isEdited") var isEdited: Int? = 0
)