package com.microware.cdfi.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.activity.MasterSyncActivity
import com.microware.cdfi.activity.ShgListActivity
import com.microware.cdfi.activity.meeting.MeetingSynchronizationActivity
import com.microware.cdfi.activity.meeting.MyTaskActivity
import com.microware.cdfi.activity.meeting.SHGMeetingListActivity
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.SHGViewmodel
import kotlinx.android.synthetic.main.fragment_dashboard.view.*

class DashboardFragment : Fragment() {
    var shgViewModel: SHGViewmodel? = null
    var validate: Validate? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_dashboard, container, false)
        shgViewModel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        validate = Validate(activity!!)
        setLabelText(view)
        var shg_count = shgViewModel!!.getallShgCount()
        view.tv_shg.text = LabelSet.getText("shg", R.string.shg)+" ("+shg_count+")"
        view.tbl_shg.setOnClickListener {
            val startMain = Intent(activity, ShgListActivity::class.java)
            startMain.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(startMain)
            activity!!.finish()
        }

        view.tbl_sync.setOnClickListener {
            val startMain = Intent(activity, MasterSyncActivity::class.java)
            startMain.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(startMain)
            activity!!.finish()
        }

        view.tbl_mymeeting.setOnClickListener {
            val startMain = Intent(activity, SHGMeetingListActivity::class.java)
            startMain.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(startMain)
            activity!!.finish()
        }
        view.tbl_mytask.setOnClickListener {
            val startMain = Intent(activity, MyTaskActivity::class.java)
            startMain.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(startMain)
            activity!!.finish()
        }
        view.tbl_Metingsync.setOnClickListener {
            val startMain = Intent(activity, MeetingSynchronizationActivity::class.java)
            startMain.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(startMain)
            activity!!.finish()
        }
       /* view.tbl_language.setOnClickListener {
            val startMain = Intent(activity, LanguageActivity::class.java)
            startMain.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(startMain)
            activity!!.finish()
        }*/

        return view
    }

    fun setLabelText(view:View)
    {
        view. tv_mymeeting.text = LabelSet.getText(
            "my_meeting",
            R.string.my_meeting
        )
        view. tv_myTask.text = LabelSet.getText(
            "my_task",
            R.string.my_task
        )
        view. tv_shg.text = LabelSet.getText("shg", R.string.shg)
        view. tv_synchronization.text = LabelSet.getText(
            "profile_sync",
            R.string.profile_sync
        )
        view. tv_mtg_synchronization.text = LabelSet.getText(
            "meeting_sync",
            R.string.meeting_sync
        )
        view. tv_report.text = LabelSet.getText(
            "report",
            R.string.report
        )


    }

}