package com.microware.cdfi.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import com.microware.cdfi.viewModel.SHGViewmodel


class JournalVoucherFragment : Fragment() {
    var shgViewModel: SHGViewmodel? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    var validate: Validate? = null
   // var flag = flag
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_journal_voucher, container, false)
        shgViewModel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        validate = Validate(activity!!)


        return view
    }


}


