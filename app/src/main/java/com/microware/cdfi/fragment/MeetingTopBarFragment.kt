package com.microware.cdfi.fragment

import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.*
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.cash_in_hand_detail_item.view.*
import kotlinx.android.synthetic.main.fragment_dashboard.view.*
import kotlinx.android.synthetic.main.meeting_detail_item.view.*
import kotlinx.android.synthetic.main.repay_toolbar.view.*
import kotlinx.android.synthetic.main.repaytablayout.view.*

class MeetingTopBarFragment(flag: Int) : Fragment() {
    var shgViewModel: SHGViewmodel? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    var validate: Validate? = null
    var flag = flag
    var count = 0
    lateinit var financialTransactionsMemViewmodel: FinancialTransactionsMemViewmodel
    var incomeandExpenditureViewmodel: IncomeandExpenditureViewmodel? = null
    var dtLoanMemberViewmodel: DtLoanMemberViewmodel? = null
    var dtLoanTxnMemViewmodel: DtLoanTxnMemViewmodel? = null
    var dtLoanGpViewmodel: DtLoanGpViewmodel? = null
    var dtLoanGPTxnViewmodel: DtLoanGpTxnViewmodel? = null
    var dtLoanViewmodel: DtLoanViewmodel? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_meetingtopbar, container, false)
        dtLoanViewmodel = ViewModelProviders.of(this).get(DtLoanViewmodel::class.java)
        dtLoanTxnMemViewmodel = ViewModelProviders.of(this).get(DtLoanTxnMemViewmodel::class.java)
        dtLoanGpViewmodel = ViewModelProviders.of(this).get(DtLoanGpViewmodel::class.java)
        dtLoanGPTxnViewmodel = ViewModelProviders.of(this).get(DtLoanGpTxnViewmodel::class.java)

        shgViewModel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        dtLoanMemberViewmodel = ViewModelProviders.of(this).get(DtLoanMemberViewmodel::class.java)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        incomeandExpenditureViewmodel =
            ViewModelProviders.of(this).get(IncomeandExpenditureViewmodel::class.java)
        financialTransactionsMemViewmodel =
            ViewModelProviders.of(this).get(FinancialTransactionsMemViewmodel::class.java)
        validate = Validate(activity!!)
        // setLabelText(view)

        count = validate!!.RetriveSharepreferenceInt(AppSP.ScrollCount)
        if (validate!!.RetriveSharepreferenceInt(MeetingSP.isVoluntarySaving) == 1) {
            view.lay_saving.visibility = View.VISIBLE
        } else {
            view.lay_saving.visibility = View.GONE
        }

        if (flag == 1) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(0).left, 0)
            }
            view.IvAttendance.setImageDrawable(resources.getDrawable(R.drawable.ic_selected_attendence))
            view.tvAttendance.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabcolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "attendance",
                R.string.attendance
            )
        } else {
            view.IvAttendance.setImageDrawable(resources.getDrawable(R.drawable.ic_grey_attendence))
            view.tvAttendance.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }

        if (flag == 2) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(1).left, 0)
            }
            view.IvCompulsorySaving.setImageDrawable(resources.getDrawable(R.drawable.ic_compulsory_saving))
            view.tvCompulsorySaving.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabcolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "compulsory_saving",
                R.string.compulsory_saving
            )
        } else {
            view.IvCompulsorySaving.setImageDrawable(resources.getDrawable(R.drawable.ic_greycompulsory_saving))
            view.tvCompulsorySaving.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }

        if (flag == 3) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(2).left, 0)
            }
            view.IvSaving.setImageDrawable(resources.getDrawable(R.drawable.ic_selected_voluntry_saving))
            view.tvSaving.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabcolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "voluntary_saving",
                R.string.voluntary_saving
            )
        } else {
            view.IvSaving.setImageDrawable(resources.getDrawable(R.drawable.ic_voluntry_saving))
            view.tvSaving.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }

        if (flag == 4) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(3).left, 0)
            }
            view.Ivloan_disbursment.setImageDrawable(resources.getDrawable(R.drawable.ic_loan_disbursment))
            view.tvloan_disbursment.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabloancolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "loan_disbursement",
                R.string.loan_disbursement
            )
        } else {
            view.Ivloan_disbursment.setImageDrawable(resources.getDrawable(R.drawable.ic_greyloan_disbursment))
            view.tvloan_disbursment.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }

        if (flag == 5) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(6).left, 0)
            }
            view.IvWithdrawal.setImageDrawable(resources.getDrawable(R.drawable.ic_withdrawl2))
            view.tvWithdrawal.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabcolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "withdrawal",
                R.string.withdrawal
            )
        } else {
            view.IvWithdrawal.setImageDrawable(resources.getDrawable(R.drawable.ic_withdrawal1))
            view.tvWithdrawal.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }

        if (flag == 6) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(7).left, 0)
            }
            view.IvPenalty.setImageDrawable(resources.getDrawable(R.drawable.ic_selected_penalty))
            view.tvPenalty.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabcolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "penalty",
                R.string.penalty
            )
        } else {
            view.IvPenalty.setImageDrawable(resources.getDrawable(R.drawable.ic_penalty))
            view.tvPenalty.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }

        if (flag == 7) {
            view.Ivloan_demand.setImageDrawable(resources.getDrawable(R.drawable.ic_loan_request))
            view.tvloan_demand.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabloancolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "loanrequest",
                R.string.loanrequest
            )
        } else {
            view.Ivloan_demand.setImageDrawable(resources.getDrawable(R.drawable.ic_loan_requestgrey))
            view.tvloan_demand.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }

        if (flag == 8) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(8).left, 0)
            }
            view.IvRepayment.setImageDrawable(resources.getDrawable(R.drawable.ic_loan_repayment))
            view.tvRepayment.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabloancolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "loan_repayment",
                R.string.loan_repayment
            )
        } else {
            view.IvRepayment.setImageDrawable(resources.getDrawable(R.drawable.ic_greyloan_repayment))
            view.tvRepayment.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }

        if (flag == 9) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(18).left, 0)
            }
            view.IvShareCapital.setImageDrawable(resources.getDrawable(R.drawable.ic_capital))
            view.tvShareCapital.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabcolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "share_capital_other",
                R.string.share_capital_other
            )
        } else {
            view.IvShareCapital.setImageDrawable(resources.getDrawable(R.drawable.ic_capital1))
            view.tvShareCapital.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }

        if (flag == 10) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(13).left, 0)
            }
            view.IvBankTransaction.setImageDrawable(resources.getDrawable(R.drawable.ic_bank1))
            view.tvBankTransaction.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabcolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "bank_transaction",
                R.string.bank_summary
            )
        } else {
            view.IvBankTransaction.setImageDrawable(resources.getDrawable(R.drawable.ic_grey_bank))
            view.tvBankTransaction.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }

        if (flag == 11) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(15).left, 0)
            }
            view.IvGroupMeeting.setImageDrawable(resources.getDrawable(R.drawable.ic_member_summary))
            view.tvGroupMeeting.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabcolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "group_meeting",
                R.string.group_meeting
            )
        } else {
            view.IvGroupMeeting.setImageDrawable(resources.getDrawable(R.drawable.ic_memgrey_summery))
            view.tvGroupMeeting.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }

        if (flag == 12) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(14).left, 0)
            }
            view.IvCashBox.setImageDrawable(resources.getDrawable(R.drawable.ic_cashbox1))
            view.tvCashBox.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabcolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "cashBox",
                R.string.cashBox
            )
        } else {
            view.IvCashBox.setImageDrawable(resources.getDrawable(R.drawable.ic_grey_cashbox))
            view.tvCashBox.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }

        if (flag == 13) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(17).left, 0)
            }
            view.IvRecipientIncome.setImageDrawable(resources.getDrawable(R.drawable.ic_loan_receipt))
            view.tvRecipientIncome.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabcolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "recepient_and_income1",
                R.string.recepient_and_income1
            )
        } else {
            view.IvRecipientIncome.setImageDrawable(resources.getDrawable(R.drawable.ic_loan_receipt1))
            view.tvRecipientIncome.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }

        if (flag == 14) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(16).left, 0)
            }
            view.IvExpenditurePayment.setImageDrawable(resources.getDrawable(R.drawable.ic_expenditure_and_payment1))
            view.tvExpenditurePayment.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabcolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "expenditure_and_payment",
                R.string.expenditure_and_payment
            )
        } else {
            view.IvExpenditurePayment.setImageDrawable(resources.getDrawable(R.drawable.ic_expenditure_grey_payment))
            view.tvExpenditurePayment.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }

        if (flag == 15) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(12).left, 0)
            }
            view.IvgroupLoanReceived.setImageDrawable(resources.getDrawable(R.drawable.ic_selectgroup_loan_received))
            view.tvgroupLoanReceived.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabloancolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "group_new_borrowings",
                R.string.group_new_borrowings
            )
        } else {
            view.IvgroupLoanReceived.setImageDrawable(resources.getDrawable(R.drawable.ic_group_loan_received))
            view.tvgroupLoanReceived.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }

        if (flag == 16) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(9).left, 0)
            }
            view.IvgroupLoanRepaid.setImageDrawable(resources.getDrawable(R.drawable.ic_selectgroup_loan_repaid))
            view.tvgroupLoanRepaid.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabloancolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "group_loan_repaid",
                R.string.group_loan_repaid
            )
        } else {
            view.IvgroupLoanRepaid.setImageDrawable(resources.getDrawable(R.drawable.ic_group_loan_repaid))
            view.tvgroupLoanRepaid.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }

        if (flag == 17) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(4).left, 0)
            }
            view.Ivloan_demandsection.setImageDrawable(resources.getDrawable(R.drawable.ic_loan_request))
            view.tvloan_demandsection.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabloancolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "demandsanctionlist",
                R.string.demandsanctionlist
            )
        } else {
            view.Ivloan_demandsection.setImageDrawable(resources.getDrawable(R.drawable.ic_loan_requestgrey))
            view.tvloan_demandsection.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }

        if (flag == 99) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(4).left, 0)
            }
            view.Ivloan_demandsection.setImageDrawable(resources.getDrawable(R.drawable.ic_loan_request))
            view.tvloan_demandsection.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabloancolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "mcp_sanction_priority_list",
                R.string.mcp_sanction_priority_list
            )
        } else {
            view.Ivloan_demandsection.setImageDrawable(resources.getDrawable(R.drawable.ic_loan_requestgrey))
            view.tvloan_demandsection.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }

        if (flag == 100) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(4).left, 0)
            }
            view.Ivloan_demandsection.setImageDrawable(resources.getDrawable(R.drawable.ic_loan_request))
            view.tvloan_demandsection.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabloancolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "mcp_preparation",
                R.string.mcp_preparation
            )
        } else {
            view.Ivloan_demandsection.setImageDrawable(resources.getDrawable(R.drawable.ic_loan_requestgrey))
            view.tvloan_demandsection.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }

        if (flag == 18) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(5).left, 0)
            }
            view.Ivloan_demand.setImageDrawable(resources.getDrawable(R.drawable.ic_loan_disbursment))
            view.tvloan_demand.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabloancolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "loanrequestsummary",
                R.string.loanrequestsummary
            )
        } else {
            view.Ivloan_demand.setImageDrawable(resources.getDrawable(R.drawable.ic_greyloan_disbursment))
            view.tvloan_demand.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }

        if (flag == 98) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(20).left, 0)
            }
            view.IvGroupInvestment.setImageDrawable(resources.getDrawable(R.drawable.ic_group_investment))
            view.tvGroupInvestment.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabloancolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "group_investment",
                R.string.group_investment
            )
        } else {
            view.IvGroupInvestment.setImageDrawable(resources.getDrawable(R.drawable.ic_greygroup_investment))
            view.tvGroupInvestment.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }

        if (flag == 97) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(19).left, 0)
            }
            view.IvShareCapitalpayment.setImageDrawable(resources.getDrawable(R.drawable.ic_payment_member))
            view.IvShareCapitalpayment.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.transperent
                )
            )
            view.tv_title.text = LabelSet.getText(
                "share_capital_other_payment",
                R.string.share_capital_other_payment
            )
        } else {
            view.IvShareCapitalpayment.setImageDrawable(resources.getDrawable(R.drawable.ic_payment_member_grey))
            view.IvShareCapitalpayment.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }

        if (flag == 96) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(10).left, 0)
            }
            view.ivMemberMeetingSummery.setImageDrawable(resources.getDrawable(R.drawable.ic_membersummery_green))
            view.ivMemberMeetingSummery.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.transperent
                )
            )
            view.tv_title.text = LabelSet.getText(
                "member_meeting_summary",
                R.string.member_meeting_summary
            )
        } else {
            view.ivMemberMeetingSummery.setImageDrawable(resources.getDrawable(R.drawable.ic_memgrey_summery))
            view.ivMemberMeetingSummery.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }

        if (flag == 95) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(11).left, 0)
            }
            view.ivAdjustment.setImageDrawable(resources.getDrawable(R.drawable.ic_loan_receipt))
            view.ivAdjustment.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.transperent
                )
            )
            view.tv_title.text = LabelSet.getText(
                "adjustment",
                R.string.adjustment
            )
        } else {
            view.ivAdjustment.setImageDrawable(resources.getDrawable(R.drawable.ic_loan_receipt1))
            view.ivAdjustment.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }

        view.tv_code.text = validate!!.RetriveSharepreferenceLong(MeetingSP.Shgcode).toString()
        view.tv_nam.text = validate!!.RetriveSharepreferenceString(MeetingSP.ShgName)
        view.tv_date.text =
            validate!!.convertDatetime(validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate))
        view.tv_mtgNum.text = validate!!.returnStringValue(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber).toString()
        )
        view.tv_count.text = "" + validate!!.RetriveSharepreferenceInt(MeetingSP.MemberCount)
        view.ic_Back.setOnClickListener {
            var intent = Intent(activity, MeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }
        view.lay_attendance.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_attendance).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.ScrollCount, count)
            var intent = Intent(activity, AttendnceDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }
        view.lay_compulsorySaving.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_compulsorySaving).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.ScrollCount, count)
            var intent = Intent(activity!!, CompulsorySavingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }
        view.lay_saving.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_saving).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.ScrollCount, count)
            var intent = Intent(activity!!, VoluntorySavingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }
        view.lay_loan_disbursment.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_loan_disbursment).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.ScrollCount, count)
            var intent = Intent(activity!!, LoanDisbursement1Activity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }

        view.lay_withdrawal.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_withdrawal).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.ScrollCount, count - 1)
            var intent = Intent(activity!!, WidthdrawalDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }

        view.lay_penalty.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_penalty).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.ScrollCount, count - 1)
            var intent = Intent(activity!!, PeneltyDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }

        view.lay_repayment.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_repayment).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.ScrollCount, count - 1)
            var intent = Intent(activity!!, RepaymentDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }

        view.lay_groupLoanRepaid.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_groupLoanRepaid).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.ScrollCount, count - 1)
            var intent = Intent(activity!!, GroupRepaymentDetailActivity::class.java)
            //  var intent = Intent(activity!!, GroupLoanRepaidActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }

        view.lay_groupLoanReceived.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_groupLoanReceived).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.ScrollCount, count - 1)
            var intent = Intent(activity, GroupLoanlist::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }

        view.lay_BankTransaction.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_BankTransaction).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.ScrollCount, count - 1)
            var intent = Intent(activity, BankSummeryActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }

        view.lay_CashBox.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_CashBox).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.ScrollCount, count - 1)
            var intent = Intent(activity, CashBoxActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }

        view.lay_GroupMeeting.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_GroupMeeting).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.ScrollCount, count - 1)
            validate!!.SaveSharepreferenceInt(MeetingSP.buttonHide,1)
            var intent = Intent(activity, GroupMeetingSummery::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }
        /*  view.lay_ExpenditurePayment.setOnClickListener {
              var intent = Intent(activity, ExpenditurePaymentActivity::class.java)
              intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
              startActivity(intent)
              activity!!.overridePendingTransition(0, 0)
          }*/
        view.lay_ExpenditurePayment.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_ExpenditurePayment).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.ScrollCount, count - 1)
            var intent = Intent(activity, ExpenditurePaymentSummeryListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }

        view.lay_RecipientIncome.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_RecipientIncome).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.ScrollCount, count - 1)
            var intent = Intent(activity, GroupReceiptAndIncomeListActiity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }
        view.lay_ShareCapital.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_ShareCapital).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.ScrollCount, count - 1)
            var intent = Intent(activity, ShareCapitalOtherReceiptListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }
        /* view.lay_RecipientIncome.setOnClickListener {
             var intent = Intent(activity, RecipentIncomeActivity::class.java)
             intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
             startActivity(intent)
             activity!!.overridePendingTransition(0, 0)
         }
         view.lay_ShareCapital.setOnClickListener {
             var intent = Intent(activity, ShareCapitalOtherReceiptActivity::class.java)
             intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
             startActivity(intent)
             activity!!.overridePendingTransition(0, 0)
         }*/
        if (validate!!.RetriveSharepreferenceInt(MeetingSP.MeetingType) == 0) {
            view.lay_loan_demandsection.visibility = View.VISIBLE
        } else {
            view.lay_loan_demandsection.visibility = View.GONE
        }
        view.lay_loan_demandsection.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_loan_demandsection).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.ScrollCount, count - 1)
            var intent = Intent(activity, LoanDemandSectionActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }
        view.lay_loan_demand.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_loan_demand).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.ScrollCount, count - 1)
            var intent = Intent(activity, LoanDemandSummaryActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }

        view.lay_GroupInvestment.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_GroupInvestment).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.ScrollCount, count - 1)
            var intent = Intent(activity, RegularMeetingInvestmentListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }

        view.lay_ShareCapitalPayment.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_ShareCapitalPayment).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.ScrollCount, count - 1)
            var intent = Intent(activity, ShareCapitalOtherPaymentListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }

        view.lay_memberMeetingSummery.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_memberMeetingSummery).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.ScrollCount, count - 1)
            var intent = Intent(activity, MemberMeetingSummaryActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }

        view.lay_Adjustment.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_Adjustment).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.ScrollCount, count - 1)
//            var intent = Intent(activity, ShareCapitalOtherPaymentListActivity::class.java)
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
//            startActivity(intent)
//            activity!!.overridePendingTransition(0, 0)
        }

        view.Ivright.setOnClickListener {
            count++
            if (count >= 19) {
                count = 19
            }
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(count).left, 0)
                moveTo(count)
            }
        }

        view.Ivleft.setOnClickListener {
            count--
            if (count <= 0) {
                count = 0
            }
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(count).left, 0)
                moveTo(count)
            }
        }
        setfilledcolor(
            view.IvAttendance,
            resources.getDrawable(R.drawable.ic_selected_attendence),
            generateMeetingViewmodel.getsumattendance(
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
            )
        )
        setfilledcolor(
            view.IvCompulsorySaving,
            resources.getDrawable(R.drawable.ic_compulsory_saving),
            generateMeetingViewmodel.getsumcomp(
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
            )
        )
        setfilledcolor(
            view.IvSaving,
            resources.getDrawable(R.drawable.ic_selected_voluntry_saving),
            generateMeetingViewmodel.getsumvol(
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
            )
        )
        //  setfilledcolor(tv_Penality_amount, imgtick_Penalty, 0)
        setfilledcolor(
            view.IvRepayment,
            resources.getDrawable(R.drawable.ic_loan_repayment),
            dtLoanTxnMemViewmodel!!.getLoanRepaymentAmount(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber)
            )
        )
        setfilledcolor(
            view.IvShareCapital,
            resources.getDrawable(R.drawable.ic_capital),
            financialTransactionsMemViewmodel.getMemberIncomeAmount(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),"OR"))

        setfilledcolor(
            view.IvShareCapitalpayment,
            resources.getDrawable(R.drawable.ic_payment_member),
            financialTransactionsMemViewmodel.getMemberIncomeAmount(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),"OP"))

        setfilledcolor(
            view.Ivloan_demand,
            resources.getDrawable(R.drawable.ic_loan_disbursment),
            dtLoanMemberViewmodel!!.getMemberLoanDisbursedAmount(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber)
            )
        )
         setfilledcolor(view.IvWithdrawal, resources.getDrawable(R.drawable.ic_withdrawl2), generateMeetingViewmodel.getsumwithdrwal(
             validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
             validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)))
//        setfilledcolor(tv_member_summary_amount, 0)
        // setfilledcolor(tv_group_investment_amount, imgtick_group_investment, 0)
        setfilledcolor(
            view.IvRecipientIncome,
            resources.getDrawable(R.drawable.ic_loan_receipt),
            incomeandExpenditureViewmodel!!.getMemberIncomeAndExpenditureAmount(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                1
            )
        )
        setfilledcolor(
            view.IvgroupLoanRepaid,
            resources.getDrawable(R.drawable.ic_selectgroup_loan_repaid),
            dtLoanGPTxnViewmodel!!.getLoanRepaymentAmount(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber)
            )
        )
        setfilledcolor(
            view.IvExpenditurePayment,
            resources.getDrawable(R.drawable.ic_expenditure_and_payment1),
            incomeandExpenditureViewmodel!!.getMemberIncomeAndExpenditureAmount(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                2
            )
        )
//        setfilledcolor(tv_group_summary_amount, 0)
        setfilledcolor(
            view.IvBankTransaction,
            resources.getDrawable(R.drawable.ic_bank1),
            generateMeetingViewmodel.gettotalinbank(
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
            )
        )
        // setfilledcolor(tv_cashBox_amount, imgtick_cashbox, validate!!.RetriveSharepreferenceInt(MeetingSP.Cashinhand))
        setfilledcolor(
            view.Ivloan_demandsection,
            resources.getDrawable(R.drawable.ic_loan_request),
            dtLoanViewmodel!!.gettotaldemand(
                validate!!.RetriveSharepreferenceInt(
                    MeetingSP.currentmeetingnumber
                ),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
            )
        )
        setfilledcolor(
            view.IvgroupLoanReceived,
            resources.getDrawable(R.drawable.ic_selectgroup_loan_received),
            dtLoanGpViewmodel!!.gettotloangroupamount(

                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
            )
        )
        updatecashinhand(view)
        updateShgType(view)
        updatecashinBank()
        return view
    }

    fun setLabelText(view: View) {
        view.tv_mymeeting.text = LabelSet.getText(
            "my_meeting",
            R.string.my_meeting
        )
        view.tv_myTask.text = LabelSet.getText(
            "my_task",
            R.string.my_task
        )
        view.tv_shg.text = LabelSet.getText("shg", R.string.shg)
        view.tv_synchronization.text = LabelSet.getText(
            "synchronization",
            R.string.synchronization
        )
        view.tv_report.text = LabelSet.getText(
            "report",
            R.string.report
        )


    }

    fun updatecashinhand(view: View) {
        var totcurrentcash = generateMeetingViewmodel.getupdatedcash(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        var totcash = generateMeetingViewmodel.getupdatedpeningcash(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        var totloan = generateMeetingViewmodel.gettotloan(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        var totloanpaid = generateMeetingViewmodel.gettotloanpaid(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        var totloangroup = generateMeetingViewmodel.gettotloangroup(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        var totloanpaidgroup = generateMeetingViewmodel.gettotloanpaidgroup(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        val incomeAmount = incomeandExpenditureViewmodel!!.getIncomeAndExpenditureAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            1, 1
        )
        val capitailIncome = financialTransactionsMemViewmodel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            1,"OR")
        val capitalPayments = financialTransactionsMemViewmodel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            1,"OP")

        val expenditureAmount = incomeandExpenditureViewmodel!!.getIncomeAndExpenditureAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            1, 2
        )

        val totalPenalty = generateMeetingViewmodel.getTotalPenaltyByMtgNum(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )

        var totcashwithdrawl = generateMeetingViewmodel.getsumwithdrwal(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))

        var totalclosingbal =
            totcash + totcurrentcash + totloanpaid + totloangroup + incomeAmount + totalPenalty + capitailIncome - totcashwithdrawl - capitalPayments - totloanpaidgroup - totloan.toInt() - expenditureAmount
        validate!!.SaveSharepreferenceInt(MeetingSP.Cashinhand, totalclosingbal)
        view.tv_amount_cash_in_hand.text = totalclosingbal.toString()
        generateMeetingViewmodel.updateclosingcash(
            totalclosingbal,
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
    }

    fun updatecashinBank() {
        var shgBankList =
            generateMeetingViewmodel.getBankdata(validate!!.RetriveSharepreferenceString(MeetingSP.SHGGUID))
        for (i in shgBankList.indices) {
            var accountno =
                shgBankList.get(i).ifsc_code!!.dropLast(7) + shgBankList.get(i).account_no

            val openningbal = generateMeetingViewmodel.getopenningbal(
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), accountno
            )
            val grploanDisbursedAmt = generateMeetingViewmodel.getgrouptotloanamtinbank(
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), accountno
            )

            val capitailIncome = financialTransactionsMemViewmodel.getBankCodeAmount(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                accountno,"OR"
            )

            val capitailPayments = financialTransactionsMemViewmodel.getBankCodeAmount(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                accountno,"OP"
            )

            val incomeAmount =
                incomeandExpenditureViewmodel!!.getBankCodeIncomeAndExpenditureAmount(
                    validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                    validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                    1, accountno
                )
            val memloanDisbursedAmt = dtLoanMemberViewmodel!!.getMemLoanDisbursedAmount(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber), accountno
            )
            var grptotloanpaid = generateMeetingViewmodel.getgrptotloanpaidinbank(
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), accountno
            )
            var memtotloanpaid = generateMeetingViewmodel.getmemtotloanpaidinbank(
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), accountno
            )
            val expenditureAmount =
                incomeandExpenditureViewmodel!!.getBankCodeIncomeAndExpenditureAmount(
                    validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                    validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                    2, accountno
                )

            var tot =
                openningbal + grploanDisbursedAmt + capitailIncome + incomeAmount + memtotloanpaid - memloanDisbursedAmt - capitailPayments - expenditureAmount - grptotloanpaid
            //  validate!!.SaveSharepreferenceInt(MeetingSP.CashinBank, tot)
            generateMeetingViewmodel.updateclosingbalbank(
                tot,
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                accountno
            )
        }
    }

    fun updateShgType(view: View) {
        view.tv_count.setBackgroundResource(R.drawable.item_countactive)
        view.iv_stauscolor.setBackgroundResource(R.drawable.item_countactive)
        if (validate!!.RetriveSharepreferenceInt(MeetingSP.SHGType) == 1) {
            view.iv_stauscolor.setImageResource(R.drawable.ic_womangroup)
            view.iv_stauscolor.visibility = View.INVISIBLE
        } else {
            view.iv_stauscolor.visibility = View.VISIBLE
            if (validate!!.RetriveSharepreferenceInt(MeetingSP.Tag) == 1) {
                view.iv_stauscolor.setImageResource(R.drawable.ic_pvtg)
            } else if (validate!!.RetriveSharepreferenceInt(MeetingSP.Tag) == 2) {
                view.iv_stauscolor.setImageResource(R.drawable.ic_elderly)
            } else if (validate!!.RetriveSharepreferenceInt(MeetingSP.Tag) == 3) {
                view.iv_stauscolor.setImageResource(R.drawable.ic_disable)
            } else if (validate!!.RetriveSharepreferenceInt(MeetingSP.Tag) == 99) {
                view.iv_stauscolor.setImageResource(R.drawable.ic_other)
            }
        }
    }

    private fun moveTo(value: Int) {
        validate!!.SaveSharepreferenceInt(AppSP.ScrollCount, value)
        if (value == 0) {
            Destination(AttendnceDetailActivity::class.java)
        } else if (value == 1) {
            Destination(CompulsorySavingActivity::class.java)
        } else if (value == 2) {
            Destination(VoluntorySavingActivity::class.java)
        } else if (value == 3) {
            Destination(LoanDemandSectionActivity::class.java)
        } else if (value == 4) {
            Destination(LoanDemandSummaryActivity::class.java)
        } else if (value == 5) {
            Destination(WidthdrawalDetailActivity::class.java)
        } else if (value == 6) {
            Destination(PeneltyDetailActivity::class.java)
        } else if (value == 7) {
            Destination(GroupRepaymentDetailActivity::class.java)
        } else if (value == 8) {
            Destination(GroupLoanRepaidActivity::class.java)
        } else if (value == 9) {
            Destination(GroupLoanReceivedActivity::class.java)
        } else if (value == 10) {
            Destination(MemberMeetingSummaryActivity::class.java)
        } else if (value == 11) {
//            Destination(Adjust::class.java)
        } else if (value == 12) {
            Destination(BankSummeryActivity::class.java)
        } else if (value == 13) {
            Destination(CashBoxActivity::class.java)
        } else if (value == 14) {
            Destination(MeetingSummeryActivity::class.java)
        } else if (value == 15) {
            Destination(ExpenditurePaymentSummeryListActivity::class.java)
        } else if (value == 16) {
            Destination(GroupReceiptAndIncomeListActiity::class.java)
        } else if (value == 17) {
            Destination(ShareCapitalOtherReceiptListActivity::class.java)
        } else if (value == 18) {
            Destination(ShareCapitalOtherPaymentListActivity::class.java)
        }else if (value == 19) {
            Destination(RegularMeetingInvestmentListActivity::class.java)
        } else {
            Destination(AttendnceDetailActivity::class.java)
        }
    }

    fun setfilledcolor(
        imageView: ImageView,
        drawable: Drawable,
        value: Int
    ) {
        if (value != null && value > 0) {
            imageView.setImageDrawable(drawable)
        }
    }

    private fun Destination(java: Class<*>) {
        var intent = Intent(activity!!, java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        activity!!.overridePendingTransition(0, 0)
    }

    override fun onStop() {
        super.onStop()
        validate!!.SaveSharepreferenceInt(AppSP.ScrollCount, 0)
    }

}