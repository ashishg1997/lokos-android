package com.microware.cdfi.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.*
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.*
import kotlinx.android.synthetic.main.cash_in_hand_detail_item.view.*
import kotlinx.android.synthetic.main.fragment_dashboard.view.*
import kotlinx.android.synthetic.main.repay_toolbar.view.*
import kotlinx.android.synthetic.main.cutrepaytablayout.view.*
import kotlinx.android.synthetic.main.meeting_detail_item_zero.view.*

class MeetingTopBarZeroFragment(flag: Int) : Fragment() {
    var shgViewModel: SHGViewmodel? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    var validate: Validate? = null
    var flag = flag
    var count = 0
    lateinit var financialTransactionsMemViewmodel: FinancialTransactionsMemViewmodel
    var incomeandExpenditureViewmodel: IncomeandExpenditureViewmodel? = null
    var dtLoanMemberViewmodel: DtLoanMemberViewmodel? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_meetingtopbar_zero, container, false)
        shgViewModel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        dtLoanMemberViewmodel = ViewModelProviders.of(this).get(DtLoanMemberViewmodel::class.java)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        incomeandExpenditureViewmodel =
            ViewModelProviders.of(this).get(IncomeandExpenditureViewmodel::class.java)
        financialTransactionsMemViewmodel =
            ViewModelProviders.of(this).get(FinancialTransactionsMemViewmodel::class.java)
        validate = Validate(activity!!)
        // setLabelText(view)
        count = validate!!.RetriveSharepreferenceInt(AppSP.CutOffScrollCount)
        if (flag == 1) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(0).left, 0)
            }
            view.IvAttendance.setImageDrawable(resources.getDrawable(R.drawable.ic_selected_attendence))
            view.tvAttendance.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabcolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "cut_off_meeting_attendance",
                R.string.cut_off_meeting_attendance
            )
        } else {

            view.IvAttendance.setImageDrawable(resources.getDrawable(R.drawable.ic_grey_attendence))
            view.tvAttendance.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }
        if (flag == 2) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(1).left, 0)
            }
            view.IvCompulsorySaving.setImageDrawable(resources.getDrawable(R.drawable.ic_compulsory_saving))
            view.tvCompulsorySaving.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabcolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "cut_off_member_saving",
                R.string.cut_off_member_saving
            )
        } else {
            view.IvCompulsorySaving.setImageDrawable(resources.getDrawable(R.drawable.ic_greycompulsory_saving))

            view.tvCompulsorySaving.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }
        if (flag == 3) {
            view.IvSaving.setColorFilter(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabcolor
                )
            )
            view.tvSaving.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabcolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "cut_off_member_saving",
                R.string.cut_off_member_saving
            )
        } else {
            view.IvSaving.setColorFilter(
                ContextCompat.getColor(
                    activity!!,
                    R.color.unselectedclr
                )
            )
            view.tvSaving.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }
        if (flag == 4) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(3).left, 0)
            }
            view.Ivloan_disbursment.setImageDrawable(resources.getDrawable(R.drawable.ic_member_closed_loan))
            view.tvloan_disbursment.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabloancolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "cutoffmember_closed_loan",
                R.string.cutoffmember_closed_loan
            )
        } else {
            view.Ivloan_disbursment.setImageDrawable(resources.getDrawable(R.drawable.ic_greymember_closed_loan))
            view.tvloan_disbursment.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }
        if (flag == 5) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(4).left, 0)
            }
            view.Ivloan_demandsection.setImageDrawable(resources.getDrawable(R.drawable.ic_active_memberloan1))
            view.tvloan_demandsection.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabloancolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "cutoffmember_active_loan",
                R.string.cutoffmember_active_loan
            )
        } else {
            view.Ivloan_demandsection.setImageDrawable(resources.getDrawable(R.drawable.ic_greymember_active_loan))
            view.tvloan_demandsection.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }
        if (flag == 6) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(5).left, 0)
            }
            view.IvShareCapital.setImageDrawable(resources.getDrawable(R.drawable.ic_member_share_capital))
            view.tvShareCapital.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabcolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "cut_off_share_capital",
                R.string.cut_off_share_capital
            )
        } else {
            view.IvShareCapital.setImageDrawable(resources.getDrawable(R.drawable.ic_greymember_share_capital))
            view.tvShareCapital.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }
        if (flag == 7) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(6).left, 0)
            }
            view.IvMemberFee.setImageDrawable(resources.getDrawable(R.drawable.ic_green_memberfees1))
            view.tvMemberFee.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabcolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "cut_off_membership_fee",
                R.string.cut_off_membership_fee
            )
        } else {
            view.IvMemberFee.setImageDrawable(resources.getDrawable(R.drawable.ic_grey_membershipfees1))
            view.tvMemberFee.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }
        if (flag == 8) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(7).left, 0)
            }
            view.IvGroupInvestment.setImageDrawable(resources.getDrawable(R.drawable.ic_group_investment))
            view.tvGroupInvestment.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabloancolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "cut_off_group_investment",
                R.string.cut_off_group_investment
            )
        } else {
            view.IvGroupInvestment.setImageDrawable(resources.getDrawable(R.drawable.ic_greygroup_investment))
            view.tvGroupInvestment.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }
        if (flag == 9) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(8).left, 0)
            }
            view.IvGroupSummery.setImageDrawable(resources.getDrawable(R.drawable.ic_group_loan_summary))
            view.tvGroupSummery.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabcolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "cut_off_group_loan_summary",
                R.string.cut_off_group_loan_summary
            )
        } else {
            view.IvGroupSummery.setImageDrawable(resources.getDrawable(R.drawable.ic_greygroup_loan_summery))
            view.tvGroupSummery.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }
        if (flag == 10) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(10).left, 0)
            }
            view.IvBankTransaction.setImageDrawable(resources.getDrawable(R.drawable.ic_group_bank_balance))

            view.tvBankTransaction.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabcolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "cut_off_group_bank_balance",
                R.string.cut_off_group_bank_balance
            )
        } else {
            view.IvBankTransaction.setImageDrawable(resources.getDrawable(R.drawable.ic_group_bank_balance1))
            view.tvBankTransaction.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }
        if (flag == 11) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(11).left, 0)
            }
            view.IvGroupSurplus.setImageDrawable(resources.getDrawable(R.drawable.ic_surplus))

            view.tvGroupSurplus.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabcolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "cut_off_surplus_or_loss",
                R.string.cut_off_surplus_or_loss
            )
        } else {
            view.IvGroupSurplus.setImageDrawable(resources.getDrawable(R.drawable.ic_surplus1))
            view.tvGroupSurplus.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }
        if (flag == 12) {
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(9).left, 0)
            }
            view.IvCashBox.setImageDrawable(resources.getDrawable(R.drawable.ic_group_cash_balance))

            view.tvCashBox.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.meetingtabcolor
                )
            )
            view.tv_title.text = LabelSet.getText(
                "cut_off_group_cash_balance",
                R.string.cut_off_group_cash_balance
            )
        } else {
            view.IvCashBox.setImageDrawable(resources.getDrawable(R.drawable.ic_greygroup_cash_balance))

            view.tvCashBox.setBackgroundColor(
                ContextCompat.getColor(
                    activity!!,
                    R.color.white
                )
            )
        }

        view.tv_code.text = validate!!.RetriveSharepreferenceLong(MeetingSP.Shgcode).toString()
        view.tv_nam.text = validate!!.RetriveSharepreferenceString(MeetingSP.ShgName)
        view.tv_date.text =
            validate!!.convertDatetime(validate!!.RetriveSharepreferenceLong(MeetingSP.CurrentMtgDate))
        view.tv_mtgNum.text = validate!!.returnStringValue(validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber).toString())
        view.tv_count.text = "" + validate!!.RetriveSharepreferenceInt(MeetingSP.MemberCount)

        view.ic_Back.setOnClickListener {
            var intent = Intent(activity, CutOffMeetingMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }

        view.lay_attendance.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_attendance).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.CutOffScrollCount, count)
            var intent = Intent(activity, AttendnceZeroDetailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }

        view.lay_compulsorySaving.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_compulsorySaving).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.CutOffScrollCount, count)
            var intent = Intent(activity!!, MemberSavingZeroActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }

        view.lay_saving.setOnClickListener {
            var intent = Intent(activity!!, VoluntorySavingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }

        view.lay_loan_disbursment.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_loan_disbursment).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.CutOffScrollCount, count-1)
            var intent = Intent(activity!!, CutOffLoanDisbursementListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }

        view.lay_closedLoan.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_closedLoan).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.CutOffScrollCount, count-1)
            var intent = Intent(activity!!, CutOffShgMemberCloseLoan::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }

        view.lay_BankTransaction.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_BankTransaction).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.CutOffScrollCount, count-1)
            var intent = Intent(activity, CutOffBankBalanceList::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }

        view.lay_CashBox.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_CashBox).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.CutOffScrollCount, count-1)
            var intent = Intent(activity, CutOffGroupCashBalanceActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }

        view.lay_GroupSurplus.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_GroupSurplus).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.CutOffScrollCount, count-1)
            var intent = Intent(activity, CutOffGroupActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }

        view.lay_ShareCapital.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_ShareCapital).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.CutOffScrollCount, count-1)
            validate!!.SaveSharepreferenceInt(MeetingSP.ReceiptType, 6)
            var intent = Intent(activity, CutOffShareCapitalOtherReceiptListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }

        view.lay_MemberFee.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_MemberFee).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.CutOffScrollCount, count-1)
            validate!!.SaveSharepreferenceInt(MeetingSP.ReceiptType, 4)
            var intent = Intent(activity, CutOffShareCapitalOtherReceiptListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }

        view.lay_GroupInvestment.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_GroupInvestment).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.CutOffScrollCount, count-1)
            var intent = Intent(activity, GroupInvestmentListActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }

        view.lay_GroupSummery.setOnClickListener {
            count = validate!!.returnIntegerValue(
                view.layoutView.indexOfChild(view.lay_GroupSummery).toString()
            )
            validate!!.SaveSharepreferenceInt(AppSP.CutOffScrollCount, count-1)
            var intent = Intent(activity, CutOffGroupLoanlist::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }

        view.Ivright.setOnClickListener {
            count++
            if (count >= 11) {
                count = 10
            }
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(count).left, 0)
                moveTo(count)
            }
        }

        view.Ivleft.setOnClickListener {
            count--
            if (count <= 0) {
                count = 0
            }
            view.horizontalScroll.post {
                view.horizontalScroll.smoothScrollTo(view.layoutView.getChildAt(count).left, 0)
                moveTo(count)
            }
        }

        /* view.lay_loan_demand.setOnClickListener {
            var intent = Intent(activity, LoanDemandSummaryActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            activity!!.overridePendingTransition(0, 0)
        }*/

        /* view.lay_ExpenditurePayment.setOnClickListener {
             var intent = Intent(activity, ExpenditurePaymentActivity::class.java)
             intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
             startActivity(intent)
             activity!!.overridePendingTransition(0, 0)
         }

         view.lay_RecipientIncome.setOnClickListener {
             var intent = Intent(activity, RecipentIncomeActivity::class.java)
             intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
             startActivity(intent)
             activity!!.overridePendingTransition(0, 0)
         }*/

        /* view.lay_withdrawal.setOnClickListener {
           var intent = Intent(activity!!, WidthdrawalDetailActivity::class.java)
           intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
           startActivity(intent)
           activity!!.overridePendingTransition(0, 0)
       }

       view.lay_penalty.setOnClickListener {
           var intent = Intent(activity!!, PeneltyDetailActivity::class.java)
           intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
           startActivity(intent)
           activity!!.overridePendingTransition(0, 0)
       }

       view.lay_repayment.setOnClickListener {
           var intent = Intent(activity!!, RepaymentDetailActivity::class.java)
           intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
           startActivity(intent)
           activity!!.overridePendingTransition(0, 0)
       }

       view.lay_groupLoanRepaid.setOnClickListener {
           var intent = Intent(activity!!, GroupLoanRepaidActivity::class.java)
           intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
           startActivity(intent)
           activity!!.overridePendingTransition(0, 0)
       }*/

        /* view.lay_groupLoanReceived.setOnClickListener {
             var intent = Intent(activity, CutOffGroupLoanlist::class.java)
             intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
             startActivity(intent)
             activity!!.overridePendingTransition(0, 0)
         }*/

        updatecashinhand(view)
        //     updatecashinBank()
        updateShgType(view)
        return view
    }

    fun setLabelText(view: View) {
        view.tv_mymeeting.text = LabelSet.getText(
            "my_meeting",
            R.string.my_meeting
        )
        view.tv_myTask.text = LabelSet.getText(
            "my_task",
            R.string.my_task
        )
        view.tv_shg.text = LabelSet.getText("shg", R.string.shg)
        view.tv_synchronization.text = LabelSet.getText(
            "synchronization",
            R.string.synchronization
        )
        view.tv_report.text = LabelSet.getText(
            "report",
            R.string.report
        )


    }

    fun updatecashinhand(view: View) {
        var totcurrentcash = generateMeetingViewmodel.getupdatedcash(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        var totcash = generateMeetingViewmodel.getupdatedpeningcash(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        var totloan = generateMeetingViewmodel.gettotalMemberLoan(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        var totloanpaid = generateMeetingViewmodel.gettotloanpaid(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        var totloangroup = generateMeetingViewmodel.getTotalGrouploan(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        var totloanpaidgroup = generateMeetingViewmodel.gettotloanpaidgroup(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
        val incomeAmount = incomeandExpenditureViewmodel!!.getIncomeAndExpenditureAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            1, 1
        )
        val capitailIncome = financialTransactionsMemViewmodel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            1,"OR")

        val capitailPayments = financialTransactionsMemViewmodel.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            1,"OP")


        val expenditureAmount = incomeandExpenditureViewmodel!!.getIncomeAndExpenditureAmount(
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            1, 2
        )

        /* var totloanpaidgroup = generateMeetingViewmodel!!.gettotloanpaidgroup(
             validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
             validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
         )*/
        var totalclosingbal =
            totcash + totcurrentcash + totloanpaid + totloangroup + incomeAmount + capitailIncome - totloanpaidgroup - capitailPayments - totloan.toInt() - expenditureAmount
        validate!!.SaveSharepreferenceInt(MeetingSP.Cashinhand, totalclosingbal)
        view.tv_amount_cash_in_hand.text = totalclosingbal.toString()
        generateMeetingViewmodel.updateclosingcash(
            totalclosingbal,
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
        )
    }

    fun updatecashinBank() {
        var shgBankList =
            generateMeetingViewmodel.getBankdata(validate!!.RetriveSharepreferenceString(MeetingSP.SHGGUID))
        for (i in shgBankList.indices) {
            var accountno =
                shgBankList.get(i).ifsc_code!!.dropLast(7) + shgBankList.get(i).account_no

            val openningbal = generateMeetingViewmodel.getopenningbal(
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), accountno
            )
            val grploanDisbursedAmt = generateMeetingViewmodel.getgrouptotloanamtinbank(
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), accountno
            )

            val capitailIncome = financialTransactionsMemViewmodel.getBankCodeAmount(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                accountno,"OR")

            val capitailPayments = financialTransactionsMemViewmodel.getBankCodeAmount(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                accountno,"OP")

            val incomeAmount =
                incomeandExpenditureViewmodel!!.getBankCodeIncomeAndExpenditureAmount(
                    validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                    validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                    1, accountno
                )
            val memloanDisbursedAmt = dtLoanMemberViewmodel!!.getMemLoanDisbursedAmount(
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber), accountno
            )
            var grptotloanpaid = generateMeetingViewmodel.getgrptotloanpaidinbank(
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), accountno
            )
            var memtotloanpaid = generateMeetingViewmodel.getmemtotloanpaidinbank(
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid), accountno
            )
            val expenditureAmount =
                incomeandExpenditureViewmodel!!.getBankCodeIncomeAndExpenditureAmount(
                    validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                    validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                    2, accountno
                )

            var tot =
                openningbal + grploanDisbursedAmt + capitailIncome + incomeAmount + memtotloanpaid - memloanDisbursedAmt - capitailPayments - expenditureAmount - grptotloanpaid
            //  validate!!.SaveSharepreferenceInt(MeetingSP.CashinBank, tot)
            generateMeetingViewmodel.updateclosingbalbank(
                tot,
                validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(MeetingSP.shgid),
                accountno
            )
        }
    }

    fun updateShgType(view: View) {
        view.tv_count.setBackgroundResource(R.drawable.item_countactive)
        view.iv_stauscolor.setBackgroundResource(R.drawable.item_countactive)
        if (validate!!.RetriveSharepreferenceInt(MeetingSP.SHGType) == 1) {
            view.iv_stauscolor.setImageResource(R.drawable.ic_womangroup)
            view.iv_stauscolor.visibility = View.INVISIBLE
        } else {
            view.iv_stauscolor.visibility = View.VISIBLE
            if (validate!!.RetriveSharepreferenceInt(MeetingSP.Tag) == 1) {
                view.iv_stauscolor.setImageResource(R.drawable.ic_pvtg)
            } else if (validate!!.RetriveSharepreferenceInt(MeetingSP.Tag) == 2) {
                view.iv_stauscolor.setImageResource(R.drawable.ic_elderly)
            } else if (validate!!.RetriveSharepreferenceInt(MeetingSP.Tag) == 3) {
                view.iv_stauscolor.setImageResource(R.drawable.ic_disable)
            } else if (validate!!.RetriveSharepreferenceInt(MeetingSP.Tag) == 99) {
                view.iv_stauscolor.setImageResource(R.drawable.ic_other)
            }
        }
    }

    private fun moveTo(value: Int) {
        validate!!.SaveSharepreferenceInt(AppSP.CutOffScrollCount, value)
        if (value == 0) {
            Destination(AttendnceZeroDetailActivity::class.java)
        } else if (value == 1) {
            Destination(MemberSavingZeroActivity::class.java)
        } else if (value == 2) {
            Destination(CutOffShgMemberCloseLoan::class.java)
        } else if (value == 3) {
            Destination(CutOffLoanDisbursementListActivity::class.java)
        } else if (value == 4) {
            validate!!.SaveSharepreferenceInt(MeetingSP.ReceiptType, 1)
            Destination(CutOffShareCapitalOtherReceiptListActivity::class.java)
        } else if (value == 5) {
            validate!!.SaveSharepreferenceInt(MeetingSP.ReceiptType, 3)
            Destination(CutOffShareCapitalOtherReceiptListActivity::class.java)
        } else if (value == 6) {
            Destination(GroupInvestmentListActivity::class.java)
        } else if (value == 7) {
            Destination(CutOffGroupLoanlist::class.java)
        } else if (value == 8) {
            Destination(CutOffGroupCashBalanceActivity::class.java)
        } else if (value == 9) {
            Destination(CutOffBankBalanceList::class.java)
        } else if (value == 10) {
            Destination(CutOffGroupActivity::class.java)
        } else {
            Destination(AttendnceDetailActivity::class.java)
        }

    }

    private fun Destination(java: Class<*>) {
        var intent = Intent(activity!!, java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        activity!!.overridePendingTransition(0, 0)
    }

    override fun onStop() {
        super.onStop()
        validate!!.SaveSharepreferenceInt(AppSP.CutOffScrollCount, 0)
    }


}