package com.microware.cdfi.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.ExpenditureAndPaymentSummary
import com.microware.cdfi.activity.vomeeting.ReceiptsAndIncomeSummary
import com.microware.cdfi.activity.vomeeting.SHGVoSHGTransactionSummary
import com.microware.cdfi.activity.vomeeting.VoMeetingMenuActivity
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import com.microware.cdfi.viewModel.CboBankViewmodel
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import com.microware.cdfi.viewModel.MasterBankViewmodel
import com.microware.cdfi.viewModel.SHGViewmodel
import com.microware.cdfi.viewModel.voviewmodel.*
import kotlinx.android.synthetic.main.bank_detail_notification.view.*
import kotlinx.android.synthetic.main.bank_summary_toolbar.view.*
import kotlinx.android.synthetic.main.fragment_others_screen_top_bar.view.*
import kotlinx.android.synthetic.main.user_detail_status.view.*

class OthersScreenTopBarFragment(flag: Int) : Fragment() {
    var shgViewModel: SHGViewmodel? = null
    lateinit var generateMeetingViewmodel: VoGenerateMeetingViewmodel
    var cboBankViewModel: CboBankViewmodel? = null
    var bankMasterViewmodel: MasterBankViewmodel? = null
    var voFinTxnDetMemViewModel: VoFinTxnDetMemViewModel? = null
    var voFinTxnDetGrpViewModel: VoFinTxnDetGrpViewModel? = null
    var voFinTxnViewModel: VoFinTxnViewModel? = null
    var voMemLoanViewModel: VoMemLoanViewModel? = null
    var voMemLoanTxnViewModel: VoMemLoanTxnViewModel? = null
    var voGroupLoanViewModel: VoGroupLoanViewModel? = null
    var voGroupLoanTxnViewModel: VoGroupLoanTxnViewModel? = null
    var validate: Validate? = null
    var flag = flag
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_others_screen_top_bar, container, false)
        shgViewModel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        voMemLoanViewModel = ViewModelProviders.of(this).get(VoMemLoanViewModel::class.java)
        voMemLoanTxnViewModel = ViewModelProviders.of(this).get(VoMemLoanTxnViewModel::class.java)
        voGroupLoanViewModel = ViewModelProviders.of(this).get(VoGroupLoanViewModel::class.java)
        voGroupLoanTxnViewModel =
            ViewModelProviders.of(this).get(VoGroupLoanTxnViewModel::class.java)
        cboBankViewModel = ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        voFinTxnViewModel = ViewModelProviders.of(this).get(VoFinTxnViewModel::class.java)
        voFinTxnDetGrpViewModel =
            ViewModelProviders.of(this).get(VoFinTxnDetGrpViewModel::class.java)
        bankMasterViewmodel = ViewModelProviders.of(this).get(MasterBankViewmodel::class.java)
        voFinTxnDetMemViewModel =
            ViewModelProviders.of(this).get(VoFinTxnDetMemViewModel::class.java)

        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        validate = Validate(requireContext())
        // setLabelText(view)

        view.tv_code.text = validate!!.RetriveSharepreferenceLong(VoSpData.voShgcode).toString()
        view.tv_name.text = validate!!.RetriveSharepreferenceString(VoSpData.voShgName)
        view.tv_date.text = validate!!.convertDatetime(validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate))
        view.tv_count.text = "" + validate!!.RetriveSharepreferenceInt(VoSpData.voMemberCount)

        view.ic_Back.setOnClickListener {
            if (flag == 23) {
                requireActivity().startActivity<ExpenditureAndPaymentSummary>()
            } else if (flag == 25) {
                requireActivity().startActivity<ReceiptsAndIncomeSummary>()
            } else if (flag == 27) {
                requireActivity().startActivity<SHGVoSHGTransactionSummary>()
            } else if (flag == 28) {
                requireActivity().startActivity<VoMeetingMenuActivity>()
            } else if (flag == 29) {
                requireActivity().startActivity<VoMeetingMenuActivity>()
            } else {
                requireActivity().startActivity<VoMeetingMenuActivity>()
            }
        }

        if (flag == 19) {
            view.tv_title.text = LabelSet.getText(
                "transaction_summary_n_shg_vo_shg",
                R.string.transaction_summary_n_shg_vo_shg
            )
            view.sub_heading1.visibility = View.VISIBLE
            view.sub_heading1.setText(R.string.shg_vo_shg)
        } else if (flag == 20) {
            view.tv_title.text = LabelSet.getText(
                "transaction_summary_n_all",
                R.string.transaction_summary_n_all
            )
            view.sub_heading.visibility = View.GONE
        } else if (flag == 21) {
            view.sub_heading.visibility = View.GONE
            view.tv_title.text = LabelSet.getText("bank_summary", R.string.bank_summary)
        } else if (flag == 22) {
            view.sub_heading.visibility = View.GONE
            view.tv_title.text = LabelSet.getText("cashBox", R.string.cashBox)
        } else if (flag == 23) {
            view.sub_heading.visibility = View.GONE
            view.bank_detail.visibility = View.VISIBLE
            view.tv_title.text = LabelSet.getText(
                "expenditure_and_payment",
                R.string.expenditure_and_payment
            )
        } else if (flag == 24) {
            view.sub_heading.visibility = View.GONE
            view.bank_detail.visibility = View.VISIBLE
            view.tv_title.text = LabelSet.getText(
                "expenditure_amp_payment_summary",
                R.string.expenditure_amp_payment_summary
            )
        } else if (flag == 25) {
            view.sub_heading.visibility = View.GONE
            view.bank_detail.visibility = View.VISIBLE
            view.tv_title.text = LabelSet.getText(
                "receipt_and_income",
                R.string.receipt_and_income
            )
        } else if (flag == 26) {
            view.sub_heading.visibility = View.GONE
            view.bank_detail.visibility = View.VISIBLE
            view.tv_title.text = LabelSet.getText(
                "receipts_and_income_summary",
                R.string.receipts_and_income_summary
            )
        } else if (flag == 27) {
            view.sub_heading.visibility = View.GONE
            view.bank_detail.visibility = View.VISIBLE
            view.tv_title.text = LabelSet.getText("adjustment", R.string.adjustment)
        } else if (flag == 28) {
            view.sub_heading.visibility = View.GONE
            view.tv_title.text = LabelSet.getText("loan_scheduler", R.string.loan_scheduler)
            view.user_meeting_status.visibility = View.GONE
        } else if (flag == 29) {
            view.sub_heading.visibility = View.GONE
            view.tv_title.text = LabelSet.getText(
                "loan_re_scheduler",
                R.string.loan_re_scheduler
            )
            view.user_meeting_status.visibility = View.GONE
            view.sub_heading.visibility = View.VISIBLE
        }
        updatecashinhand(view)
        updatecashinbank(view)
        returndefaaultaccount(view.tv_bankname, view.tv_bank_amount)
        return view
    }
    fun updatecashinhand(view: View) {

        var openingcash = generateMeetingViewmodel.getupdatedpeningcash(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        var totshgcash = voFinTxnDetMemViewModel!!.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(1),
            listOf("RG", "RL", "RO"),
            ""
        )
        var totvocash = voFinTxnDetGrpViewModel!!.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(1),
            listOf("RG", "RL", "RO"),
            ""
        )

        var totshgcashdebit = voFinTxnDetMemViewModel!!.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(1),
            listOf("PG", "PL", "PO"),
            ""
        )
        var totvocashdebit = voFinTxnDetGrpViewModel!!.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(1),
            listOf("PG", "PL", "PO"),
            ""
        )
        var totmemloan = voMemLoanViewModel!!.getMemLoanDisbursedAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            "", listOf<Int>(1)
        )

        var totvoloan = voGroupLoanViewModel!!.getgrouptotloanamtinbank(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            "", listOf<Int>(1)
        )

        var totmemloantxn = voMemLoanTxnViewModel!!.getmemtotloanpaidinbank(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            "", listOf<Int>(1)
        )

        var totvoloantxn = voGroupLoanTxnViewModel!!.getgrptotloanpaidinbank(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            "", listOf<Int>(1)
        )


        /* var totcashwithdrawl = generateMeetingViewmodel.getsumwithdrwal(
             validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
             validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
 */
        var totalclosingbal =
            openingcash + totshgcash + totvocash + totmemloantxn + totvoloan - totshgcashdebit - totvocashdebit - totmemloan - totvoloantxn
        validate!!.SaveSharepreferenceInt(VoSpData.voCashinhand, totalclosingbal)
        view.tv_cash_in_hand.text = totalclosingbal.toString()
        generateMeetingViewmodel.updateclosingcash(
            totalclosingbal,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
    }

    fun updatecashinbank(view: View) {
        var voBankList =
            cboBankViewModel!!.getBankdata1(validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID))
        for (i in voBankList!!.indices) {
            var accountno =
                voBankList.get(i).ifsc_code!!.dropLast(7) + voBankList.get(i).account_no


            var totshgcash = voFinTxnDetMemViewModel!!.getIncomeAmount(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                listOf<Int>(2, 3),
                listOf("RG", "RL", "RO"),
                accountno
            )

            var totvocash = voFinTxnDetGrpViewModel!!.getIncomeAmount(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                listOf<Int>(2, 3),
                listOf("RG", "RL", "RO"),
                accountno
            )
            var totshgcashdebit = voFinTxnDetMemViewModel!!.getIncomeAmount(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                listOf<Int>(2, 3),
                listOf("PG", "PL", "PO"),
                accountno
            )
            var totvocashdebit = voFinTxnDetGrpViewModel!!.getIncomeAmount(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                listOf<Int>(2, 3),
                listOf("PG", "PL", "PO"),
                accountno
            )
            var totopeningcash = voFinTxnViewModel!!.gettotalopeninginbank(
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                accountno
            )

            var totmemloan = voMemLoanViewModel!!.getMemLoanDisbursedAmount(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                accountno,
                listOf<Int>(2, 3)
            )

            var totvoloan = voGroupLoanViewModel!!.getgrouptotloanamtinbank(
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                accountno,
                listOf<Int>(2, 3)
            )

            var totmemloantxn = voMemLoanTxnViewModel!!.getmemtotloanpaidinbank(
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                accountno,
                listOf<Int>(2, 3)
            )

            var totvoloantxn = voGroupLoanTxnViewModel!!.getgrptotloanpaidinbank(
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                accountno,
                listOf<Int>(2, 3)
            )
            var totalclosingbal =
                totopeningcash + totshgcash + totvocash + totmemloantxn + totvoloan - totshgcashdebit - totvocashdebit - totmemloan - totvoloantxn

            voFinTxnViewModel!!.updateclosingbalbank(
                totalclosingbal,
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                accountno
            )
            //view.tv_bank_amount.text = totalclosingbal.toString()

        }


        /* var totcashwithdrawl = generateMeetingViewmodel.getsumwithdrwal(
             validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
             validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
 */

        /* generateMeetingViewmodel.updateclosingcash(
             totalclosingbal,
             validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
             validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
         )*/
    }


    fun returndefaaultaccount(tvBankname: TextView, tvBankAmount: TextView): String {
        var accountNo = ""
        val bankList = cboBankViewModel!!.getcboBankdata(
            validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID),
            1
        )
        if (!bankList.isNullOrEmpty()) {
            for (i in bankList.indices) {
                if (bankList.get(i).is_default == 1) {
                    accountNo =
                        bankList.get(i).ifsc_code!!.dropLast(7) + bankList.get(i).account_no
                    tvBankname.text = accountNo
                    break
                }
            }
        }

        var totclosingcash = voFinTxnViewModel!!.gettotalclosinginbank(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            accountNo
        )
        tvBankAmount.text = totclosingcash.toString()
        return accountNo
    }
    inline fun <reified T : Context> Context.startActivity() {
        startActivity(Intent(this, T::class.java))
    }
}