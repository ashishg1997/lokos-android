package com.microware.cdfi.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.viewModel.GenerateMeetingViewmodel
import com.microware.cdfi.viewModel.SHGViewmodel
import kotlinx.android.synthetic.main.fragment_shg_meeting_approval_top_bar.view.*
import kotlinx.android.synthetic.main.shg_meeting_toolbar.view.*


class SHGmeetingApprovalTopBarFragment(flag:Int) : Fragment() {
    var shgViewModel: SHGViewmodel? = null
    lateinit var generateMeetingViewmodel: GenerateMeetingViewmodel
    var validate: Validate? = null
    var flag = flag

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.fragment_shg_meeting_approval_top_bar, container, false)
        shgViewModel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)
        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(GenerateMeetingViewmodel::class.java)
        validate = Validate(activity!!)
        // setLabelText(view)

        if (flag == 1) {
            view.user_detail_status.visibility = View.GONE
            view.user_meeting_status.visibility = View.GONE
            view.tv_title.text = LabelSet.getText(
                "meeting_pending_for_approval",
                R.string.meeting_pending_for_approval
            )
        } else if (flag == 2) {
            view.user_detail_status.visibility = View.VISIBLE
            view.user_meeting_status.visibility = View.VISIBLE
            view.tv_title.text = LabelSet.getText(
                "shg_meeting_approval",
                R.string.shg_meeting_approval
            )
        }
        return view
    }


}