package com.microware.cdfi.fragment

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.speech.tts.UtteranceProgressListener
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.MeetingDetailTTSActivity
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.getDownloadLanguageISO
import kotlinx.android.synthetic.main.fragment_shg_meeting_summary.*
import java.util.*

class ShgMeetingSummaryFragment : Fragment(), TextToSpeech.OnInitListener {

    var translatedData = ""
    private val REQ_TTS_STATUS_CHECK = 0
    private val TAG = "TTS Demo"
    private var mTts: TextToSpeech? = null
    private var uttCount = 0
    private var lastUtterance = -1
    private val params = HashMap<String, String>()
    var languageCode: String = ""
    var finalParagraphString: String? = null
    var activitydata: Activity? = null

    var attendanceData = ""
    var savingData = ""
    var distributeData = ""
    var repaymentData = ""
    var bankData = ""
    var cashData = ""
    var groupData = ""
    var sentence21 = ""
    var sentence22 = ""
    var sentence23 = ""


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_shg_meeting_summary, container, false)
        activitydata = (activity as MeetingDetailTTSActivity)

        attendanceData = (activitydata as MeetingDetailTTSActivity).attendanceData
        savingData = (activitydata as MeetingDetailTTSActivity).savingData
        distributeData = (activitydata as MeetingDetailTTSActivity).distributeData
        repaymentData = (activitydata as MeetingDetailTTSActivity).repaymentData
        bankData = (activitydata as MeetingDetailTTSActivity).bankData
        cashData = (activitydata as MeetingDetailTTSActivity).cashData
        groupData = (activitydata as MeetingDetailTTSActivity).groupData
        sentence21 = (activitydata as MeetingDetailTTSActivity).sentence21
        sentence22 = (activitydata as MeetingDetailTTSActivity).sentence22
        sentence23 = (activitydata as MeetingDetailTTSActivity).sentence23

        val checkIntent = Intent()
        checkIntent.action = TextToSpeech.Engine.ACTION_CHECK_TTS_DATA
        startActivityForResult(checkIntent, REQ_TTS_STATUS_CHECK)
        languageCode = CDFIApplication.validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!

        var iv_attendance1 = view.findViewById<ImageView>(R.id.iv_attendance1)
        var iv_savings1 = view.findViewById<ImageView>(R.id.iv_savings1)
        var iv_loanDistributed1 = view.findViewById<ImageView>(R.id.iv_loanDistributed1)
        var iv_loanRepayment1 = view.findViewById<ImageView>(R.id.iv_loanRepayment1)
        var iv_bankCollection1 = view.findViewById<ImageView>(R.id.iv_bankCollection1)
        var iv_cashCollection1 = view.findViewById<ImageView>(R.id.iv_cashCollection1)
        var iv_grpTransaction1 = view.findViewById<ImageView>(R.id.iv_grpTransaction1)

        iv_attendance1.setOnClickListener {
            if (mTts != null) {
                mTts!!.stop()

                var testdata = attendanceData.replace("\n", "")
                if (testdata.isNullOrEmpty()) {
                    doSpeak(sentence21)
                    alertDialog("Attendance", "\n\n" + sentence21)
                } else {
                    doSpeak(attendanceData)
                    attendanceData = attendanceData.replace("=", "")
                    attendanceData = attendanceData.replace("'", "")
                    alertDialog("Attendance", attendanceData)
                }
            }
        }

        iv_savings1.setOnClickListener {
            if (mTts != null) {
                mTts!!.stop()

                var testdata = savingData.replace("\n", "")
                if (testdata.isNullOrEmpty()) {
                    doSpeak(sentence21)
                    alertDialog("Saving", "\n\n" + sentence21)
                } else {
                    doSpeak(savingData)
                    savingData = savingData.replace("=", "")
                    savingData = savingData.replace("'", "")
                    alertDialog("Saving", savingData)
                }
            }
        }

        iv_loanDistributed1.setOnClickListener {
            if (mTts != null) {
                mTts!!.stop()
                var testdata = distributeData.replace("\n", "")
                if (testdata.isNullOrEmpty()) {
                    doSpeak(sentence22)
                    alertDialog("Loan Distribution", "\n\n" + sentence22)
                } else {
                    doSpeak(distributeData)
                    distributeData = distributeData.replace("=", "")
                    distributeData = distributeData.replace("'", "")
                    alertDialog("Loan Distribution", distributeData)
                }
            }
        }
        iv_loanRepayment1.setOnClickListener {
            if (mTts != null) {
                mTts!!.stop()

                var testdata = repaymentData.replace("\n", "")
                if (testdata.isNullOrEmpty()) {
                    doSpeak(sentence23)
                    alertDialog("Load Repayment", "\n\n" + sentence23)
                } else {
                    doSpeak(repaymentData)
                    repaymentData = repaymentData.replace("=", "")
                    repaymentData = repaymentData.replace("'", "")
                    alertDialog("Load Repayment", repaymentData)
                }
            }
        }
        iv_bankCollection1.setOnClickListener {
            if (mTts != null) {
                mTts!!.stop()

                var testdata = bankData.replace("\n", "")
                if (testdata.isNullOrEmpty()) {
                    doSpeak(sentence21)
                    alertDialog("Bank Collection", "\n\n" + sentence21)
                } else {
                    doSpeak(bankData)
                    bankData = bankData.replace("=", "")
                    bankData = bankData.replace("'", "")
                    alertDialog("Bank Collection", bankData)
                }
            }
        }
        iv_cashCollection1.setOnClickListener {
            if (mTts != null) {
                mTts!!.stop()

                var testdata = cashData.replace("\n", "")
                if (testdata.isNullOrEmpty()) {
                    doSpeak(sentence21)
                    alertDialog("Cash Collection", "\n\n" + sentence21)
                } else {
                    doSpeak(cashData)
                    cashData = cashData.replace("=", "")
                    cashData = cashData.replace("'", "")
                    alertDialog("Cash Collection", cashData)
                }
            }
        }

        iv_grpTransaction1.setOnClickListener {
            if (mTts != null) {
                mTts!!.stop()

                var testdata = groupData.replace("\n", "")
                if (testdata.isNullOrEmpty()) {
                    doSpeak(sentence21)
                    alertDialog("Group Transaction", "\n\n" + sentence21)
                } else {
                    doSpeak(groupData)
                    groupData = groupData.replace("=", "")
                    groupData = groupData.replace("'", "")
                    alertDialog("Group Transaction", groupData)
                }
            }
        }

        return view
    }


    fun doSpeak(word: String) {
        val st = StringTokenizer(word.toString(), ",.=")
        while (st.hasMoreTokens()) {
            params[TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID] = uttCount++.toString()
            mTts!!.setPitch(1.toFloat())
            mTts!!.setSpeechRate(0.94.toFloat())
            mTts!!.speak(st.nextToken(), TextToSpeech.QUEUE_ADD, params)
//            mTts!!.setOnUtteranceCompletedListener(object :
//                TextToSpeech.OnUtteranceCompletedListener {
//                override fun onUtteranceCompleted(utteranceId: String?) {
//                    Log.v(TAG, "Got completed message for uttId: $utteranceId")
//                }
//            })

            mTts!!.setOnUtteranceProgressListener(object : UtteranceProgressListener() {
                override fun onStart(utteranceId: String) {

                    if (mTts != null) {
//                        tv_summary_play.text = "STOP"
                    }
                }

                override fun onDone(utteranceId: String) {

                    if (mTts != null && utteranceId.equals((uttCount - 1).toString())) {
                        mTts!!.stop()
//                        tv_summary_play1.text = "PLAY"

//                        if (mTts == null){
//
//                        }
                    }
                }

                override fun onError(utteranceId: String) {
                    Log.i("TextToSpeech", "On Error")
                }
            })
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQ_TTS_STATUS_CHECK) {
            when (resultCode) {
                TextToSpeech.Engine.CHECK_VOICE_DATA_PASS -> {
                    mTts = TextToSpeech(requireContext(), this)
                }
                TextToSpeech.Engine.CHECK_VOICE_DATA_BAD_DATA, TextToSpeech.Engine.CHECK_VOICE_DATA_MISSING_DATA, TextToSpeech.Engine.CHECK_VOICE_DATA_MISSING_VOLUME -> {
                    val installIntent = Intent()
                    installIntent.action = TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA
                    startActivity(installIntent)
                }
                TextToSpeech.Engine.CHECK_VOICE_DATA_FAIL -> Log.e(TAG, "Got a failure.")
                else -> Log.e(TAG, "Got a failure.")
            }

            val available = data!!.getStringArrayListExtra("availableVoices")
            Log.v("languages count", available!!.size.toString())
            val iter: Iterator<String> = available.iterator()
            while (iter.hasNext()) {
                val lang = iter.next()
                val locale = Locale(lang)
            }
            val locales = Locale.getAvailableLocales()
            Log.v(TAG, "available locales:")
            for (i in locales.indices) Log.v(TAG, "locale: " + locales[i].displayName)
            val defloc = Locale.getDefault()
            Log.v(TAG, "current locale: " + defloc.displayName)
        }
    }

    override fun onInit(status: Int) {
        if (status == TextToSpeech.SUCCESS) {
            mTts!!.language = Locale(getDownloadLanguageISO(languageCode), "IND", "variant")
        }

    }


    override fun onPause() {
        super.onPause()
        if (mTts != null) mTts!!.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mTts != null) mTts!!.shutdown()
    }

    override fun onResume() {
        super.onResume()
        if (mTts != null) {
            mTts!!.stop()
//            tv_summary_play1.text = "PLAY"
        }
    }


    private fun alertDialog(titleData: String, paragraphData: String) {
        val mDialogView: View = LayoutInflater.from(context)
            .inflate(R.layout.paragraph_text_ui, null)
        val mBuilder = android.app.AlertDialog.Builder(context)
            .setView(mDialogView)
        var mAlertDialog = mBuilder.show()
//        mAlertDialog!!.setCancelable(false)
        val title = mDialogView.findViewById<TextView>(R.id.title)
        val tv_paragraph = mDialogView.findViewById<TextView>(R.id.tv_paragraph)
        val iv_cancel = mDialogView.findViewById<ImageView>(R.id.iv_cancel)

        iv_cancel.setOnClickListener {
            if (mTts != null) {
                mTts!!.stop()
//                doSpeak(activitydata!!)
            }
            mAlertDialog!!.dismiss()
        }

        title.text = titleData
        tv_paragraph.text = paragraphData

        mAlertDialog!!.setOnDismissListener(object : DialogInterface.OnDismissListener {
            override fun onDismiss(dialog: DialogInterface?) {
                if (mTts != null) {
                    mTts!!.stop()
                }
                mAlertDialog.dismiss()
            }
        })
    }


}