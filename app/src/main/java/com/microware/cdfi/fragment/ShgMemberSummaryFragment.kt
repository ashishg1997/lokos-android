package com.microware.cdfi.fragment

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.speech.tts.UtteranceProgressListener
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.MeetingDetailTTSActivity
import com.microware.cdfi.adapter.SHGMeetingDetailTTSAdapter
import com.microware.cdfi.api.meetingmodel.LoanListModel
import com.microware.cdfi.api.meetingmodel.LoanRepaymentListModel
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.DtmtgDetEntity
import com.microware.cdfi.utility.AppSP
import com.microware.cdfi.utility.MeetingSP
import com.microware.cdfi.utility.MeetingTTS
import com.microware.cdfi.utility.getDownloadLanguageISO
import kotlinx.android.synthetic.main.fragment_shg_member_summary.*
import java.util.*

class ShgMemberSummaryFragment : Fragment(), TextToSpeech.OnInitListener {

    var activitydata: Activity? = null
    var Userlist: List<DtmtgDetEntity>? = null
    var LoanDistributelist: List<LoanListModel>? = null
    var LoanRepaymentlist: List<LoanRepaymentListModel>? = null

    private val REQ_TTS_STATUS_CHECK = 0
    private val TAG = "TTS Demo"
    private var mTts: TextToSpeech? = null
    private var uttCount = 0
    private var lastUtterance = -1
    private val params = HashMap<String, String>()
    var languageCode: String = ""


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_shg_member_summary, container, false)
        activitydata = (activity as MeetingDetailTTSActivity)

        Userlist = (activitydata as MeetingDetailTTSActivity).Userlist
        LoanDistributelist = (activitydata as MeetingDetailTTSActivity).LoanDistributelist
        LoanRepaymentlist = (activitydata as MeetingDetailTTSActivity).LoanRepaymentlist
        var adapter = SHGMeetingDetailTTSAdapter(requireContext(), Userlist, listener)
        var recycle_shgMeeting = view.findViewById<RecyclerView>(R.id.recycle_shgMeeting)
        recycle_shgMeeting.adapter = adapter
        recycle_shgMeeting.layoutManager = LinearLayoutManager(context)


        val checkIntent = Intent()
        checkIntent.action = TextToSpeech.Engine.ACTION_CHECK_TTS_DATA
        startActivityForResult(checkIntent, REQ_TTS_STATUS_CHECK)
        languageCode = CDFIApplication.validate!!.RetriveSharepreferenceString(AppSP.Langaugecode)!!

        return view
    }


    private var listener = object : SHGMeetingDetailTTSAdapter.OnItemClickListener {
        override fun onItemClick(
            dataList: DtmtgDetEntity, pos: Int
        ) {

            var meetno =  (activitydata as MeetingDetailTTSActivity).meetno
            var meetDate = (activitydata as MeetingDetailTTSActivity).MeetingDate
            var data = MeetingTTS.GetMemberData(
                meetno,
                meetDate,
                languageCode,
                context!!,
                dataList,
                LoanDistributelist!!,
                LoanRepaymentlist!!
                )

            if (mTts != null) {
                mTts!!.stop()

                var testdata = data.replace("\n", "")
                if (testdata.isNullOrEmpty()) {
                    doSpeak("Member data not available")
                    alertDialog("Member Detail", "\n\n" + "Member data not available" + "\n\n")
                } else {
                    doSpeak(data)
                    data = data.replace("=", "")
                    data = data.replace("'", "")
                    alertDialog("Member Detail", data)
                }
            }
        }
    }



    fun doSpeak(word: String) {
        val st = StringTokenizer(word.toString(), ",.=")
        while (st.hasMoreTokens()) {
            params[TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID] = uttCount++.toString()
            mTts!!.setPitch(1.toFloat())
            mTts!!.setSpeechRate(0.94.toFloat())
            mTts!!.speak(st.nextToken(), TextToSpeech.QUEUE_ADD, params)

            mTts!!.setOnUtteranceProgressListener(object : UtteranceProgressListener() {
                override fun onStart(utteranceId: String) {

                    if (mTts != null) {
                    }
                }

                override fun onDone(utteranceId: String) {

                    if (mTts != null && utteranceId.equals((uttCount - 1).toString())) {
                        mTts!!.stop()
                    }
                }

                override fun onError(utteranceId: String) {
                    Log.i("TextToSpeech", "On Error")
                }
            })
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQ_TTS_STATUS_CHECK) {
            when (resultCode) {
                TextToSpeech.Engine.CHECK_VOICE_DATA_PASS -> {
                    mTts = TextToSpeech(requireContext(), this)
                }
                TextToSpeech.Engine.CHECK_VOICE_DATA_BAD_DATA, TextToSpeech.Engine.CHECK_VOICE_DATA_MISSING_DATA, TextToSpeech.Engine.CHECK_VOICE_DATA_MISSING_VOLUME -> {
                    val installIntent = Intent()
                    installIntent.action = TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA
                    startActivity(installIntent)
                }
                TextToSpeech.Engine.CHECK_VOICE_DATA_FAIL -> Log.e(TAG, "Got a failure.")
                else -> Log.e(TAG, "Got a failure.")
            }

            val available = data!!.getStringArrayListExtra("availableVoices")
            Log.v("languages count", available!!.size.toString())
            val iter: Iterator<String> = available.iterator()
            while (iter.hasNext()) {
                val lang = iter.next()
                val locale = Locale(lang)
            }
            val locales = Locale.getAvailableLocales()
            Log.v(TAG, "available locales:")
            for (i in locales.indices) Log.v(TAG, "locale: " + locales[i].displayName)
            val defloc = Locale.getDefault()
            Log.v(TAG, "current locale: " + defloc.displayName)
        }
    }

    override fun onInit(status: Int) {
        if (status == TextToSpeech.SUCCESS) {
            mTts!!.language = Locale(getDownloadLanguageISO(languageCode), "IND", "variant")
        }

    }


    override fun onPause() {
        super.onPause()
        if (mTts != null) mTts!!.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mTts != null) mTts!!.shutdown()
    }

    override fun onResume() {
        super.onResume()
        if (mTts != null) {
            mTts!!.stop()
//            tv_summary_play1.text = "PLAY"
        }
    }


    private fun alertDialog(titleData: String, paragraphData: String) {
        val mDialogView: View = LayoutInflater.from(context)
            .inflate(R.layout.paragraph_text_ui, null)
        val mBuilder = android.app.AlertDialog.Builder(context)
            .setView(mDialogView)
        var mAlertDialog = mBuilder.show()
//        mAlertDialog!!.setCancelable(false)
        val title = mDialogView.findViewById<TextView>(R.id.title)
        val tv_paragraph = mDialogView.findViewById<TextView>(R.id.tv_paragraph)
        val iv_cancel = mDialogView.findViewById<ImageView>(R.id.iv_cancel)

        iv_cancel.setOnClickListener {
            if (mTts != null) {
                mTts!!.stop()
//                doSpeak(activitydata!!)
            }
            mAlertDialog!!.dismiss()
        }

        title.text = titleData
        tv_paragraph.text = paragraphData

        mAlertDialog!!.setOnDismissListener(object : DialogInterface.OnDismissListener {
            override fun onDismiss(dialog: DialogInterface?) {
                if (mTts != null) {
                    mTts!!.stop()
                }
                mAlertDialog.dismiss()
            }
        })
    }




}