package com.microware.cdfi.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.*
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import com.microware.cdfi.viewModel.CboBankViewmodel
import com.microware.cdfi.viewModel.MasterBankViewmodel
import com.microware.cdfi.viewModel.SHGViewmodel
import com.microware.cdfi.viewModel.voviewmodel.*
import kotlinx.android.synthetic.main.bank_detail_notification.view.*
import kotlinx.android.synthetic.main.user_detail_status.view.*
import kotlinx.android.synthetic.main.vo_cut_off_toolbar.view.*


class VoCutOffTopBarFragment(flag: Int) : Fragment() {
    var generateMeetingViewmodel: VoGenerateMeetingViewmodel? = null
    var voFinTxnDetMemViewModel: VoFinTxnDetMemViewModel? = null
    var voFinTxnDetGrpViewModel: VoFinTxnDetGrpViewModel? = null
    var voFinTxnViewModel: VoFinTxnViewModel? = null
    var voMemLoanViewModel: VoMemLoanViewModel? = null
    var voMemLoanTxnViewModel: VoMemLoanTxnViewModel? = null
    var voGroupLoanViewModel: VoGroupLoanViewModel? = null
    var voGroupLoanTxnViewModel: VoGroupLoanTxnViewModel? = null
    var validate: Validate? = null
    var flag = flag
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_vo_cut_of_top_bar, container, false)
        voMemLoanViewModel = ViewModelProviders.of(this).get(VoMemLoanViewModel::class.java)
        voMemLoanTxnViewModel = ViewModelProviders.of(this).get(VoMemLoanTxnViewModel::class.java)
        voGroupLoanViewModel = ViewModelProviders.of(this).get(VoGroupLoanViewModel::class.java)
        voGroupLoanTxnViewModel =
            ViewModelProviders.of(this).get(VoGroupLoanTxnViewModel::class.java)
        voFinTxnViewModel = ViewModelProviders.of(this).get(VoFinTxnViewModel::class.java)
        voFinTxnDetGrpViewModel =
            ViewModelProviders.of(this).get(VoFinTxnDetGrpViewModel::class.java)
        voFinTxnDetMemViewModel =
            ViewModelProviders.of(this).get(VoFinTxnDetMemViewModel::class.java)

        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        validate = Validate(requireContext())

        view.tv_code.text = validate!!.RetriveSharepreferenceLong(VoSpData.voShgcode).toString()
        view.tv_name.text = validate!!.RetriveSharepreferenceString(VoSpData.voShgName)
        view.tv_date.text =
            validate!!.convertDatetime(validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate))
        view.tv_count.text = "" + validate!!.RetriveSharepreferenceInt(VoSpData.voMemberCount)


        view.img_Back.setOnClickListener {
            if (flag == 7) {
                requireContext().startActivity<VoCutOffClosedLoanVotoShgSummary>()
            } else if (flag == 8) {
                requireContext().startActivity<VoCutOffSummaryOfActiveLoan>()
            }else {
                requireContext().startActivity<VoCutOffMenuActivity>()
            }
        }
        // setLabelText(view)

        /*if (flag == 1) {
            view.tv_title.setText(LabelSet.getText("generate_meeting", R.string.generate_meeting))

            view.img_Back.setOnClickListener(View.OnClickListener {
                val i = Intent(context, VoCutOffListActivity::class.java)
                startActivity(i)
            })

        } else if (flag == 2) {
            view.tv_title.setText(
                LabelSet.getText(
                    "cut_off_meeting",
                    R.string.cut_off_meeting
                )
            )
            view.img_Back.setOnClickListener(View.OnClickListener {
                val i = Intent(context, VoCutOffGenerateMeeting::class.java)
                startActivity(i)
            })

        } else*/
        if (flag == 3) {
            view.tv_title.text = LabelSet.getText(
                "cut_off_menu",
                R.string.cut_off_menu
            )
        } else if (flag == 4) {
            view.tv_title.text = LabelSet.getText(
                "cut_off_meeting_attendance",
                R.string.cut_off_meeting_attendance
            )
        } else if (flag == 5) {
            view.tv_title.text = LabelSet.getText(
                "cut_off_members_saving",
                R.string.cut_off_members_saving
            )

        } else if (flag == 6) {
            view.tv_title.text = LabelSet.getText(
                "cut_off_closed_Loan",
                R.string.cut_off_summary_closed_Loan_vo_to_shg
            )
            view.tr_summary.visibility = View.VISIBLE
        } else if (flag == 7) {
            view.tv_title.text = LabelSet.getText(
                "cut_off_close_load",
                R.string.cut_off_close_Loan_vo_to_shg
            )

        } else if (flag == 8) {
            view.tv_title.text = LabelSet.getText(
                "cut_off_active_Loan_vo_to_shg",
                R.string.cut_off_active_Loan_vo_to_shg
            )

        } else if (flag == 9) {
            view.tv_title.text = LabelSet.getText(
                "cut_off_active_Loan_vo_to_shg",
                R.string.cut_off_active_Loan_vo_to_shg
            )
            view.tr_summary.visibility = View.VISIBLE

        } else if (flag == 10) {
            view.tv_title.text = LabelSet.getText(
                "cut_off_members_share_capital",
                R.string.cut_off_members_share_capital
            )
        } else if (flag == 11) {
            view.tv_title.text = LabelSet.getText(
                "cut_off_members_fee",
                R.string.cut_off_members_fee
            )
        } else if (flag == 12) {
            view.tv_title.text = LabelSet.getText(
                "cut_off_vo_investment",
                R.string.cut_off_vo_investment
            )
        } else if (flag == 13) {
            view.tv_title.text = LabelSet.getText(
                "cut_off_vo_loan_clf_to_vo",
                R.string.cut_off_vo_loan_clf_to_vo
            )
        } else if (flag == 14) {
            view.tv_title.text = LabelSet.getText(
                "cut_off_closed_loan_clf_to_vo",
                R.string.cut_off_closed_loan_clf_to_vo
            )

        } else if (flag == 15) {
            view.tv_title.text = LabelSet.getText(
                "cut_off_vo_cash_balance",
                R.string.cut_off_vo_cash_balance
            )

        } else if (flag == 16) {
            view.tv_title.text = LabelSet.getText(
                "cut_off_vo_bank_balance",
                R.string.cut_off_vo_bank_balance
            )

        } else if (flag == 17) {
            view.tv_title.text = LabelSet.getText(
                "cut_off_vo_balance_sheet",
                R.string.cut_off_vo_balance_sheet
            )

        } else if (flag == 18) {
            view.tv_title.text = LabelSet.getText(
                "cut_off_vo_loan_bank",
                R.string.cut_off_vo_loan_bank
            )

        }

        updatecashinhand()

        return view
    }

    fun updatecashinhand() {

        var openingcash = generateMeetingViewmodel!!.getupdatedpeningcash(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        var totshgcash = voFinTxnDetMemViewModel!!.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(1),
            listOf("RG", "RL", "RO"),
            ""
        )

        var totvocash = voFinTxnDetGrpViewModel!!.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(1),
            listOf("RG", "RL", "RO"),
            ""
        )

        var totshgcashdebit = voFinTxnDetMemViewModel!!.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(1),
            listOf("PG", "PL", "PO"),
            ""
        )

        var totvocashdebit = voFinTxnDetGrpViewModel!!.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(1),
            listOf("PG", "PL", "PO"),
            ""
        )

        var totmemloan = voMemLoanViewModel!!.getMemLoanDisbursedAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            "", listOf<Int>(1)
        )

        var totvoloan = voGroupLoanViewModel!!.getgrouptotloanamtinbank(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            "", listOf<Int>(1)
        )

        var totmemloantxn = voMemLoanTxnViewModel!!.getmemtotloanpaidinbank(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            "", listOf<Int>(1)
        )

        var totvoloantxn = voGroupLoanTxnViewModel!!.getgrptotloanpaidinbank(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            "", listOf<Int>(1)
        )

        /*var totcashwithdrawl = generateMeetingViewmodel.getsumwithdrwal(
            validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))*/

        var totalclosingbal =
            openingcash + totshgcash + totvocash + totmemloantxn + totvoloan - totshgcashdebit - totvocashdebit - totmemloan - totvoloantxn
        validate!!.SaveSharepreferenceInt(VoSpData.voCashinhand, totalclosingbal)
//        view.tv_cash_in_hand.text = totalclosingbal.toString()

        generateMeetingViewmodel!!.updateclosingcash(
            totalclosingbal,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

    }

    inline fun <reified T : Context> Context.startActivity() {
        startActivity(Intent(this, T::class.java))
    }

}