package com.microware.cdfi.fragment

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProviders
import com.google.gson.JsonObject
import com.microware.cdfi.R
import com.microware.cdfi.activity.vo.VoSyncActivity
import com.microware.cdfi.activity.vo.VOListActivity
import com.microware.cdfi.activity.vo.VoMeetingListActivity
import com.microware.cdfi.activity.vo.VoMyTaskActivity
import com.microware.cdfi.activity.vomeeting.VoMeetingListActivity1
import com.microware.cdfi.activity.vomeeting.VoMeetingSynchronizationActivity
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.api.meetinguploadmodel.MeetingApprovalCountModel
import com.microware.cdfi.utility.*
import com.microware.cdfi.viewModel.FedrationViewModel
import com.microware.cdfi.viewModel.ResponseViewModel
import kotlinx.android.synthetic.main.customealertdialogelogin.*
import kotlinx.android.synthetic.main.customealertdialogelogin.view.*
import kotlinx.android.synthetic.main.fragment_vo_dashboard.view.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class VoDashboardFragment : Fragment() {
    var validate: Validate? = null
    var fedrationViewModel: FedrationViewModel? = null
    var cboType = 0
    internal lateinit var progressDialog: ProgressDialog
    var responseViewModel: ResponseViewModel? = null

    var apiInterface: ApiInterface? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_vo_dashboard, container, false)
        validate = Validate(activity!!)


        fedrationViewModel = ViewModelProviders.of(this).get(FedrationViewModel::class.java)
        responseViewModel = ViewModelProviders.of(this).get(ResponseViewModel::class.java)

        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()

        view.tbl_vo.setOnClickListener {
            val startMain = Intent(activity, VOListActivity::class.java)
            startMain.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(startMain)
        }


        view.tbl_task.setOnClickListener {
            val startMain = Intent(activity, VoMyTaskActivity::class.java)
            startMain.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(startMain)
        }

        view.tbl_sync.setOnClickListener {
            val startMain = Intent(activity, VoSyncActivity::class.java)
            startMain.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(startMain)
        }
        view.tbl_meeting.setOnClickListener {
            if (validate!!.returnRoleValue(validate!!.RetriveSharepreferenceString(AppSP.Roleid)) == 410) {
                val startMain = Intent(activity, VoMeetingListActivity1::class.java)
                startMain.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(startMain)
            }
        }
        view.tbl_meetingupload.setOnClickListener {
            if (validate!!.returnRoleValue(validate!!.RetriveSharepreferenceString(AppSP.Roleid)) == 410) {
                val startMain = Intent(activity, VoMeetingSynchronizationActivity::class.java)
                startMain.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(startMain)
            }
        }

        //    importPendingMeetingCount()
        setLabelText(view)
        return view
    }

    private fun setLabelText(view: View) {
        view.tvMeeting.text = LabelSet.getText(
            "my_meeting1",
            R.string.my_meeting1
        )
        view.tvTasks.text = LabelSet.getText(
            "my_task",
            R.string.my_task
        )
        if (validate!!.RetriveSharepreferenceInt(AppSP.FormRoleType) == 2) {
            cboType = 2
            var count = fedrationViewModel!!.getVoCount(cboType)
            view.tv_vo.text = LabelSet.getText(
                "clf",
                R.string.clf
            ) + " (" + count + ")"
        } else {
            cboType = 1
            var count = fedrationViewModel!!.getVoCount(cboType)
            view.tv_vo.text = LabelSet.getText(
                "village_organization",
                R.string.village_organization
            ) + " (" + count + ")"

        }

        view.tvSync.text = LabelSet.getText(
            "synchronization",
            R.string.synchronization
        )
        view.tvReport.text = LabelSet.getText(
            "report",
            R.string.report
        )
        view.tvPending_count.text = LabelSet.getText(
            "pending_count",
            R.string.pending_count
        ) + "(" + validate!!.RetriveSharepreferenceString(MeetingSP.pendingMeetings) + ")"
    }

}