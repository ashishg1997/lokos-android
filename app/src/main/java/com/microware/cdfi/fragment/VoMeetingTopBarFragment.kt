package com.microware.cdfi.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.*
import com.microware.cdfi.entity.voentity.VomtgEntity
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import com.microware.cdfi.viewModel.SHGViewmodel
import com.microware.cdfi.viewModel.voviewmodel.VoGenerateMeetingViewmodel
import kotlinx.android.synthetic.main.repay_toolbar_vomeeting.view.*
import kotlinx.android.synthetic.main.repay_toolbar_vomeeting.view.ic_Back
import kotlinx.android.synthetic.main.repay_toolbar_vomeeting.view.tv_title
import kotlinx.android.synthetic.main.shg_vo_receipts_top_toolbar.view.*
import kotlinx.android.synthetic.main.user_detail_status_new.view.*

class VoMeetingTopBarFragment(flag: Int) : Fragment() {
    var shgViewModel: SHGViewmodel? = null
    lateinit var generateMeetingViewmodel: VoGenerateMeetingViewmodel
    var validate: Validate? = null
    var flag = flag
    var mtglist: List<VomtgEntity>? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_vo_meeting_top_bar, container, false)
        shgViewModel = ViewModelProviders.of(this).get(SHGViewmodel::class.java)

        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        validate = Validate(requireActivity())
        // setLabelText(view)

        /*view.ic_Back.setOnClickListener {
            var intent = Intent(requireContext(), VoMeetingListActivity1::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
        }*/
        view.ic_Back.setOnClickListener {
            if (flag == 1) {
                requireActivity().startActivity<VoMeetingListActivity1>()
            } else if (flag == 3) {
                requireActivity().startActivity<VoMeetingMenuActivity>()
            } else if (flag == 4) {
                requireActivity().startActivity<VoMeetingMenuActivity>()
            }
        }
        /*common in all*/

        view.tv_code.text = validate!!.RetriveSharepreferenceLong(VoSpData.voShgcode).toString()
        view.tv_name.text = validate!!.RetriveSharepreferenceString(VoSpData.voShgName)
        view.tv_date.text =
            validate!!.convertDatetime(validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate))
        view.tv_count.text = "" + validate!!.RetriveSharepreferenceInt(VoSpData.voMemberCount)

        if (flag == 1) {
            view.tv_title.text = LabelSet.getText("generate_meeting", R.string.generate_meeting)
        } else if (flag == 3) {
            view.tv_title.text = LabelSet.getText("shg_attendance", R.string.shg_attendance)
        } else if (flag == 4) {
            view.tv_title.text = LabelSet.getText(
                "ec_member_attendance",
                R.string.ec_member_attendance
            )
        }

        return view
    }

    /* fun setLabelText(view: View) {
         view.tv_mymeeting.setText(LabelSet.getText("my_meeting", R.string.my_meeting))
         view.tv_myTask.setText(LabelSet.getText("my_task", R.string.my_task))
         view.tv_shg.setText(LabelSet.getText("shg", R.string.shg))
         view.tv_synchronization.setText(
             LabelSet.getText(
                 "synchronization",
                 R.string.synchronization
             )
         )
         view.tv_report.setText(LabelSet.getText("report", R.string.report))


     }
 */

    /* fun updateShgType(view: View) {
         view.tv_count.setBackgroundResource(R.drawable.item_countactive)
         view.iv_stauscolor.setBackgroundResource(R.drawable.item_countactive)
         if (validate!!.RetriveSharepreferenceInt(MeetingSP.SHGType) == 1) {
             view.iv_stauscolor.setImageResource(R.drawable.ic_womangroup)
             view.iv_stauscolor.visibility = View.INVISIBLE
         } else {
             view.iv_stauscolor.visibility = View.VISIBLE
             if (validate!!.RetriveSharepreferenceInt(MeetingSP.Tag) == 1) {
                 view.iv_stauscolor.setImageResource(R.drawable.ic_pvtg)
             } else if (validate!!.RetriveSharepreferenceInt(MeetingSP.Tag) == 2) {
                 view.iv_stauscolor.setImageResource(R.drawable.ic_elderly)
             } else if (validate!!.RetriveSharepreferenceInt(MeetingSP.Tag) == 3) {
                 view.iv_stauscolor.setImageResource(R.drawable.ic_disable)
             } else if (validate!!.RetriveSharepreferenceInt(MeetingSP.Tag) == 99) {
                 view.iv_stauscolor.setImageResource(R.drawable.ic_other)
             }
         }}*/
    inline fun <reified T : Context> Context.startActivity() {
        startActivity(Intent(this, T::class.java))
    }
}