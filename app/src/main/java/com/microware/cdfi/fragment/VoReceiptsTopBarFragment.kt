package com.microware.cdfi.fragment

import android.content.Context
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.DisplayMetrics
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.PopupWindow
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.microware.cdfi.R
import com.microware.cdfi.activity.vomeeting.*
import com.microware.cdfi.adapter.vomeetingadapter.ChangeBankAdapter
import com.microware.cdfi.utility.LabelSet
import com.microware.cdfi.utility.Validate
import com.microware.cdfi.utility.VoSpData
import com.microware.cdfi.viewModel.CboBankViewmodel
import com.microware.cdfi.viewModel.MasterBankViewmodel
import com.microware.cdfi.viewModel.SHGViewmodel
import com.microware.cdfi.viewModel.voviewmodel.*
import kotlinx.android.synthetic.main.bank_detail_notification.view.*
import kotlinx.android.synthetic.main.fragment_shg_vo_top_bar.view.*
import kotlinx.android.synthetic.main.shg_vo_receipts_top_toolbar.view.*
import kotlinx.android.synthetic.main.user_detail_status.view.*
import kotlinx.android.synthetic.main.vomeeting_bank_item_dialog.view.*

class VoReceiptsTopBarFragment(flag: Int) : Fragment() {
    var shgViewModel: SHGViewmodel? = null
    var cboBankViewModel: CboBankViewmodel? = null
    var bankMasterViewmodel: MasterBankViewmodel? = null
    var generateMeetingViewmodel: VoGenerateMeetingViewmodel? = null
    var voFinTxnDetMemViewModel: VoFinTxnDetMemViewModel? = null
    var voFinTxnDetGrpViewModel: VoFinTxnDetGrpViewModel? = null
    var voFinTxnViewModel: VoFinTxnViewModel? = null
    var voMemLoanViewModel: VoMemLoanViewModel? = null
    var voMemLoanTxnViewModel: VoMemLoanTxnViewModel? = null
    var voGroupLoanViewModel: VoGroupLoanViewModel? = null
    var voGroupLoanTxnViewModel: VoGroupLoanTxnViewModel? = null
    var validate: Validate? = null
    var flag = flag
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_shg_vo_top_bar, container, false)

        voMemLoanViewModel = ViewModelProviders.of(this).get(VoMemLoanViewModel::class.java)
        voMemLoanTxnViewModel = ViewModelProviders.of(this).get(VoMemLoanTxnViewModel::class.java)
        voGroupLoanViewModel = ViewModelProviders.of(this).get(VoGroupLoanViewModel::class.java)
        voGroupLoanTxnViewModel =
            ViewModelProviders.of(this).get(VoGroupLoanTxnViewModel::class.java)
        cboBankViewModel = ViewModelProviders.of(this).get(CboBankViewmodel::class.java)
        voFinTxnViewModel = ViewModelProviders.of(this).get(VoFinTxnViewModel::class.java)
        voFinTxnDetGrpViewModel =
            ViewModelProviders.of(this).get(VoFinTxnDetGrpViewModel::class.java)
        bankMasterViewmodel = ViewModelProviders.of(this).get(MasterBankViewmodel::class.java)
        voFinTxnDetMemViewModel =
            ViewModelProviders.of(this).get(VoFinTxnDetMemViewModel::class.java)

        generateMeetingViewmodel =
            ViewModelProviders.of(this).get(VoGenerateMeetingViewmodel::class.java)
        validate = Validate(requireActivity())
        /*common in all*/

        view.tv_code.text = validate!!.RetriveSharepreferenceLong(VoSpData.voShgcode).toString()
        view.tv_name.text = validate!!.RetriveSharepreferenceString(VoSpData.voShgName)
        view.tv_date.text =
            validate!!.convertDatetime(validate!!.RetriveSharepreferenceLong(VoSpData.voCurrentMtgDate))
        view.tv_count.text = "" + validate!!.RetriveSharepreferenceInt(VoSpData.voMemberCount)

        view.ic_Back.setOnClickListener {
            if (flag == 1) {
                requireActivity().startActivity<VoMeetingMenuActivity>()
            } else if (flag == 2) {
                requireActivity().startActivity<LoanDueStatusSHGtoVOActivity>()
            } else if (flag == 3) {
                requireActivity().startActivity<SHGtoVOReceipts>()
            } else if (flag == 4) {
                requireActivity().startActivity<VoMeetingMenuActivity>()
            } else if (flag == 5) {
                requireActivity().startActivity<CLFtoVOReceipts>()
            } else if (flag == 6) {
                requireActivity().startActivity<CLFtoVoLoanReceipts>()
            } else if (flag == 7) {
                requireActivity().startActivity<CLFtoVORfVrfGrantReceipts>()
            } else if (flag == 8) {
                requireActivity().startActivity<CLFtoVoOtherReceipts>()
            } else if (flag == 9) {
                requireActivity().startActivity<VoMeetingMenuActivity>()
            } else if (flag == 10) {
                requireActivity().startActivity<CLFtoVOReceipts>()
            } else if (flag == 11) {
                requireActivity().startActivity<SRLMtoVOLoanReceipts>()
            } else if (flag == 12) {
                requireActivity().startActivity<SRLMtoVOGrantReceipts>()
            } else if (flag == 13) {
                requireActivity().startActivity<VoMeetingMenuActivity>()
            } else if (flag == 14) {
                requireActivity().startActivity<CLFtoVOReceipts>()
            } else if (flag == 15) {
                requireActivity().startActivity<OthertoVoLoanReceipt>()
            } else if (flag == 16) {
                requireActivity().startActivity<BanktoVoLoanReceipt>()
            } else if (flag == 17) {
                requireActivity().startActivity<VoMeetingMenuActivity>()
            } else if (flag == 18) {
                requireActivity().startActivity<SHGtoVOReceipts>()
            } else if (flag == 19) {
                requireActivity().startActivity<VOtoSHGLoanRequestSummary>()
            } else if (flag == 20) {
                requireActivity().startActivity<VOtoSHGLoanSummary>()
            } else if (flag == 21) {
                requireActivity().startActivity<VOtoSHGLoanSummary>()
            } else if (flag == 22) {
                requireActivity().startActivity<VOtoSHGLoanPayment>()
            } else if (flag == 23) {
                requireActivity().startActivity<VOtoOthersHrCaderPayments>()
            } else if (flag == 24) {
                requireActivity().startActivity<VOtoSHGOtherPayments>()
            } else if (flag == 25) {
                requireActivity().startActivity<VoMeetingMenuActivity>()
            } else if (flag == 26) {
                requireActivity().startActivity<CLFtoVOReceipts>()
            } else if (flag == 27) {
                requireActivity().startActivity<VOLoanRepaymentSummary>()
            } else if (flag == 28) {
                requireActivity().startActivity<VOLoanwisedustatus>()
            } else if (flag == 29) {
                requireActivity().startActivity<VoMeetingMenuActivity>()
            } else if (flag == 30) {
                requireActivity().startActivity<VOtoOtherPayments>()
            } else if (flag == 31) {
                requireActivity().startActivity<VOtoOthersLoanRepaymentSumamry>()
            } else if (flag == 32) {
                requireActivity().startActivity<VOtoOthersLoanWiseDueStatus>()
            } else if (flag == 33) {
                requireActivity().startActivity<VOtoOthersLoanRepayment>()
            } else if (flag == 34) {
                requireActivity().startActivity<VOtoOthersHrCaderPayments>()
            }
        }

        if (flag == 1) {
            view.tv_title.text = LabelSet.getText(
                "shg_to_vo_receipts",
                R.string.shg_to_vo_receipts
            )
            view.tblSubheading.visibility = View.GONE
            view.lay_bank.visibility = View.VISIBLE
        } else if (flag == 2) {
            view.tv_title.text = LabelSet.getText(
                "loan_wise_repayment_by_shg",
                R.string.loan_wise_repayment_by_shg
            )
            view.sub_heading.text = LabelSet.getText(
                "shg_to_vo_head",
                R.string.shg_to_vo_head
            )
            view.tblSubheading.visibility = View.VISIBLE
            view.lay_bank.visibility = View.VISIBLE

        } else if (flag == 3) {
            view.tv_title.text = LabelSet.getText(
                "loan_wise_due_status1",
                R.string.loan_wise_due_status1
            )
            view.sub_heading.text = LabelSet.getText(
                "shg_to_vo_head",
                R.string.shg_to_vo_head
            )
            view.tblSubheading.visibility = View.VISIBLE
            view.lay_bank.visibility = View.VISIBLE

        } else if (flag == 4) {
            view.tv_title.text = LabelSet.getText(
                "clf_to_vo_receipts",
                R.string.clf_to_vo_receipts
            )
            view.tblSubheading.visibility = View.GONE
            view.lay_bank.visibility = View.VISIBLE

        } else if (flag == 5) {
            view.tv_title.text = LabelSet.getText(
                "loan_receipts",
                R.string.loan_receipts
            )
            view.sub_heading.text = LabelSet.getText(
                "clf_to_vo_head",
                R.string.clf_to_vo_head
            )
            view.tblSubheading.visibility = View.VISIBLE
            view.lay_bank.visibility = View.GONE

        } else if (flag == 6) {
            view.tv_title.text = LabelSet.getText(
                "rf_vrf_grant_receipts",
                R.string.rf_vrf_grant_receipts
            )
            view.sub_heading.text = LabelSet.getText(
                "clf_to_vo_head",
                R.string.clf_to_vo_head
            )
            view.tblSubheading.visibility = View.VISIBLE
            view.lay_bank.visibility = View.GONE

        } else if (flag == 7) {
            view.tv_title.text = LabelSet.getText(
                "other_receipts1",
                R.string.other_receipts1
            )
            view.sub_heading.text = LabelSet.getText(
                "clf_to_vo_head",
                R.string.clf_to_vo_head
            )
            view.tblSubheading.visibility = View.VISIBLE
            view.lay_bank.visibility = View.GONE

        } else if (flag == 8) {
            view.tv_title.text = LabelSet.getText(
                "withdrawal",
                R.string.withdrawal
            )
            view.sub_heading.text = LabelSet.getText(
                "clf_to_vo_head",
                R.string.clf_to_vo_head
            )
            view.tblSubheading.visibility = View.VISIBLE
            view.lay_bank.visibility = View.GONE

        } else if (flag == 9) {
            view.tv_title.text = LabelSet.getText(
                "srlm_to_vo_receipts",
                R.string.srlm_to_vo_receipts
            )
            view.tblSubheading.visibility = View.GONE
            view.lay_bank.visibility = View.VISIBLE

        } else if (flag == 10) {
            view.tv_title.text = LabelSet.getText(
                "loan_receipts",
                R.string.loan_receipts
            )
            view.sub_heading.text = LabelSet.getText(
                "srlm_to_vo_head",
                R.string.srlm_to_vo_head
            )
            view.tblSubheading.visibility = View.VISIBLE
            view.lay_bank.visibility = View.GONE

        } else if (flag == 11) {
            view.tv_title.text = LabelSet.getText(
                "rf_vrf_grant_receipts",
                R.string.rf_vrf_grant_receipts
            )
            view.sub_heading.text = LabelSet.getText(
                "srlm_to_vo_head",
                R.string.srlm_to_vo_head
            )
            view.tblSubheading.visibility = View.VISIBLE
            view.lay_bank.visibility = View.GONE

        } else if (flag == 12) {
            view.tv_title.text = LabelSet.getText(
                "other_receipts1",
                R.string.other_receipts1
            )
            view.sub_heading.text = LabelSet.getText(
                "srlm_to_vo_head",
                R.string.srlm_to_vo_head
            )
            view.tblSubheading.visibility = View.VISIBLE
            view.lay_bank.visibility = View.GONE

        } else if (flag == 13) {
            view.tv_title.text = LabelSet.getText(
                "others_to_vo_receipt",
                R.string.others_to_vo_receipt
            )
            view.tblSubheading.visibility = View.GONE
            view.lay_bank.visibility = View.VISIBLE

        } else if (flag == 14) {
            view.tv_title.text = LabelSet.getText(
                "loan_receipts",
                R.string.loan_receipts
            )
            view.sub_heading.text = LabelSet.getText(
                "other_to_vo_head",
                R.string.other_to_vo_head
            )
            view.tblSubheading.visibility = View.VISIBLE
            view.lay_bank.visibility = View.GONE

        } else if (flag == 15) {
            view.tv_title.text = LabelSet.getText(
                "loan_receipts",
                R.string.loan_receipts
            )
            view.sub_heading.text = LabelSet.getText(
                "bank_to_vo_head",
                R.string.bank_to_vo_head
            )
            view.tblSubheading.visibility = View.VISIBLE
            view.lay_bank.visibility = View.GONE

        } else if (flag == 16) {
            view.tv_title.text = LabelSet.getText(
                "other_receipts1",
                R.string.other_receipts1
            )
            view.sub_heading.text = LabelSet.getText(
                "other_to_vo_head",
                R.string.other_to_vo_head
            )
            view.tblSubheading.visibility = View.VISIBLE
            view.lay_bank.visibility = View.GONE

        } else if (flag == 17) {
            view.tv_title.text = LabelSet.getText(
                "vo_to_shg_payments",
                R.string.vo_to_shg_payments
            )
            view.tblSubheading.visibility = View.GONE
            view.lay_bank.visibility = View.VISIBLE

        } else if (flag == 18) {
            view.tv_title.text = LabelSet.getText(
                "loan_summary",
                R.string.loan_summary
            )
            view.sub_heading.text = LabelSet.getText(
                "vo_to_shg_head",
                R.string.vo_to_shg_head
            )
            view.tblSubheading.visibility = View.VISIBLE
            view.lay_bank.visibility = View.VISIBLE

        } else if (flag == 19) {
            view.tv_title.text = LabelSet.getText(
                "loan_request",
                R.string.loan_request
            )
            view.sub_heading.text = LabelSet.getText(
                "vo_to_shg_head",
                R.string.vo_to_shg_head
            )
            view.tblSubheading.visibility = View.VISIBLE
            view.lay_bank.visibility = View.VISIBLE

        } else if (flag == 20) {
            view.tv_title.text = LabelSet.getText(
                "loan_request_summary",
                R.string.loan_request_summary
            )
            view.sub_heading.text = LabelSet.getText(
                "vo_to_shg_head",
                R.string.vo_to_shg_head
            )
            view.tblSubheading.visibility = View.VISIBLE
            view.lay_bank.visibility = View.VISIBLE

        } else if (flag == 21) {
            view.tv_title.text = LabelSet.getText(
                "loan_disbursement",
                R.string.loan_disbursement
            )
            view.sub_heading.text = LabelSet.getText(
                "vo_to_shg_head",
                R.string.vo_to_shg_head
            )
            view.tblSubheading.visibility = View.VISIBLE
            view.lay_bank.visibility = View.GONE

        } else if (flag == 22) {
            view.tv_title.text = LabelSet.getText(
                "rf_vrf_grant_payments",
                R.string.rf_vrf_grant_payments
            )
            view.sub_heading.text = LabelSet.getText(
                "vo_to_shg_head",
                R.string.vo_to_shg_head
            )
            view.tblSubheading.visibility = View.VISIBLE
            view.lay_bank.visibility = View.GONE

        } else if (flag == 23) {
            view.tv_title.text = LabelSet.getText(
                "other_payments",
                R.string.other_payments
            )
            view.sub_heading.text = LabelSet.getText(
                "vo_to_shg_head",
                R.string.vo_to_shg_head
            )
            view.tblSubheading.visibility = View.VISIBLE
            view.lay_bank.visibility = View.GONE

        } else if (flag == 24) {
            view.tv_title.text = LabelSet.getText(
                "withdrawal",
                R.string.withdrawal
            )
            view.sub_heading.text = LabelSet.getText(
                "vo_to_shg_head",
                R.string.vo_to_shg_head
            )
            view.tblSubheading.visibility = View.VISIBLE
            view.lay_bank.visibility = View.GONE

        } else if (flag == 25) {
            view.tv_title.text = LabelSet.getText(
                "vo_to_clf_payment",
                R.string.vo_to_clf_payment
            )

            view.tblSubheading.visibility = View.GONE
            view.lay_bank.visibility = View.VISIBLE

        } else if (flag == 26) {
            view.tv_title.text = LabelSet.getText(
                "loan_repayment_summary",
                R.string.loan_repayment_summary
            )
            view.sub_heading.text = LabelSet.getText(
                "vo_to_clf_head",
                R.string.vo_to_clf_head
            )
            view.tblSubheading.visibility = View.VISIBLE
            view.lay_bank.visibility = View.VISIBLE

        } else if (flag == 27) {
            view.tv_title.text = LabelSet.getText(
                "loan_wise_due_status",
                R.string.loan_wise_due_status
            )
            view.sub_heading.text = LabelSet.getText(
                "vo_to_clf_head",
                R.string.vo_to_clf_head
            )
            view.tblSubheading.visibility = View.VISIBLE
            view.lay_bank.visibility = View.VISIBLE

        } else if (flag == 28) {
            view.tv_title.text = LabelSet.getText(
                "vo_loan_repayment",
                R.string.vo_loan_repayment
            )
            view.sub_heading.text = LabelSet.getText(
                "vo_to_clf_head",
                R.string.vo_to_clf_head
            )
            view.tblSubheading.visibility = View.VISIBLE
            view.lay_bank.visibility = View.VISIBLE

        } else if (flag == 29) {
            view.tv_title.text = LabelSet.getText(
                "vo_to_others_payments",
                R.string.vo_to_others_payments
            )
            view.tblSubheading.visibility = View.GONE
            view.lay_bank.visibility = View.VISIBLE

        } else if (flag == 30) {
            view.tv_title.text = LabelSet.getText(
                "loan_repayment_summary",
                R.string.loan_repayment_summary
            )
            view.sub_heading.text = LabelSet.getText(
                "vo_to_others_head",
                R.string.vo_to_others_head
            )
            view.tblSubheading.visibility = View.VISIBLE
            view.lay_bank.visibility = View.VISIBLE

        } else if (flag == 31) {
            view.tv_title.text = LabelSet.getText(
                "loan_wise_due_status",
                R.string.loan_wise_due_status
            )
            view.sub_heading.text = LabelSet.getText(
                "vo_to_others_head",
                R.string.vo_to_others_head
            )
            view.tblSubheading.visibility = View.VISIBLE
            view.lay_bank.visibility = View.VISIBLE

        } else if (flag == 32) {
            view.tv_title.text = LabelSet.getText(
                "vo_loan_repayment",
                R.string.vo_loan_repayment
            )
            view.sub_heading.text = LabelSet.getText(
                "vo_to_others_head",
                R.string.vo_to_others_head
            )
            view.tblSubheading.visibility = View.VISIBLE
            view.lay_bank.visibility = View.VISIBLE

        } else if (flag == 33) {
            view.tv_title.text = LabelSet.getText(
                "hr_cader_payments",
                R.string.hr_cader_payments
            )
            view.sub_heading.text = LabelSet.getText(
                "vo_to_others_head",
                R.string.vo_to_others_head
            )
            view.tblSubheading.visibility = View.VISIBLE
            view.lay_bank.visibility = View.GONE

        } else if (flag == 34) {
            view.tv_title.text = LabelSet.getText(
                "other_payments",
                R.string.other_payments
            )
            view.sub_heading.text = LabelSet.getText(
                "vo_to_others_head",
                R.string.vo_to_others_head
            )
            view.tblSubheading.visibility = View.VISIBLE
            view.lay_bank.visibility = View.GONE

        }else if (flag == 35) {
            view.tv_title.text = LabelSet.getText(
                "loan_repayment_schedule",
                R.string.loan_repayment_schedule
            )
            view.sub_heading.text = LabelSet.getText(
                "vo_to_shg_head",
                R.string.vo_to_shg_head
            )
            view.tblSubheading.visibility = View.VISIBLE
            view.lay_bank.visibility = View.GONE

        }
        view.lay_banklist.setOnClickListener {
            var popupwindow: PopupWindow = popupDisplay(view.tv_bankname, view.tv_bank_amount)
            val values = IntArray(2)
            view.lay_banklist.getLocationInWindow(values)
            val positionOfIcon = values[1]
            val displayMetrics: DisplayMetrics = requireActivity().resources.displayMetrics
            val height: Int = displayMetrics.heightPixels * 2 / 3
            if (positionOfIcon > height) {
                popupwindow.showAsDropDown(view.lay_banklist, -20, -200)
            } else {
                popupwindow.showAsDropDown(view.lay_banklist, -20, 0)
            }
        }

        updatecashinhand(view)
        updatecashinbank(view)
        returndefaaultaccount(view.tv_bankname, view.tv_bank_amount)
        return view
    }

    fun popupDisplay(tvbankcode: TextView, tvamount: TextView): PopupWindow {
        val popupWindow = PopupWindow(activity)
        // inflate your layout or dynamically add view
        val inflater =
            requireActivity().getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.vomeeting_bank_item_dialog, null, false)
        popupWindow.isFocusable = true
        popupWindow.width = WindowManager.LayoutParams.WRAP_CONTENT
        popupWindow.height = WindowManager.LayoutParams.WRAP_CONTENT
        popupWindow.setBackgroundDrawable(ColorDrawable(resources.getColor(R.color.white)))
        view.tvchangepayment.text =
            LabelSet.getText("change_payment_mode", R.string.change_payment_mode)
        fillRecyclerView2(view, tvbankcode, popupWindow, tvamount)

        popupWindow.contentView = view

        return popupWindow
    }

    private fun fillRecyclerView2(
        view: View,
        tvbankcode: TextView,
        popupWindow: PopupWindow,
        tvamount: TextView
    ) {
        val bankList = cboBankViewModel!!.getcboBankdata(
            validate!!.RetriveSharepreferenceString(
                VoSpData.voSHGGUID
            ), 1
        )
        if (!bankList.isNullOrEmpty()) {
            view.rvList.layoutManager = LinearLayoutManager(context)
            view.rvList.adapter = ChangeBankAdapter(
                requireActivity(),
                bankList,
                bankMasterViewmodel!!,
                tvbankcode,
                popupWindow, tvamount
            )
        }
    }

    fun updatecashinhand(view: View) {

        var openingcash = generateMeetingViewmodel!!.getupdatedpeningcash(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )

        var totshgcash = voFinTxnDetMemViewModel!!.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(1),
            listOf("RG", "RL", "RO"),
            ""
        )
        var totvocash = voFinTxnDetGrpViewModel!!.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(1),
            listOf("RG", "RL", "RO"),
            ""
        )

        var totshgcashdebit = voFinTxnDetMemViewModel!!.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(1),
            listOf("PG", "PL", "PO"),
            ""
        )
        var totvocashdebit = voFinTxnDetGrpViewModel!!.getIncomeAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            listOf<Int>(1),
            listOf("PG", "PL", "PO"),
            ""
        )
        var totmemloan = voMemLoanViewModel!!.getMemLoanDisbursedAmount(
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            "", listOf<Int>(1)
        )

        var totvoloan = voGroupLoanViewModel!!.getgrouptotloanamtinbank(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            "", listOf<Int>(1)
        )

        var totmemloantxn = voMemLoanTxnViewModel!!.getmemtotloanpaidinbank(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            "", listOf<Int>(1)
        )

        var totvoloantxn = voGroupLoanTxnViewModel!!.getgrptotloanpaidinbank(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            "", listOf<Int>(1)
        )


        /* var totcashwithdrawl = generateMeetingViewmodel.getsumwithdrwal(
             validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
             validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
 */
        var totalclosingbal =
            openingcash + totshgcash + totvocash + totmemloantxn + totvoloan - totshgcashdebit - totvocashdebit - totmemloan - totvoloantxn
        validate!!.SaveSharepreferenceInt(VoSpData.voCashinhand, totalclosingbal)
        view.tv_cash_in_hand.text = totalclosingbal.toString()
        generateMeetingViewmodel!!.updateclosingcash(
            totalclosingbal,
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid)
        )
    }

    fun updatecashinbank(view: View) {
        var voBankList =
            cboBankViewModel!!.getBankdata1(validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID))
        for (i in voBankList!!.indices) {
            var accountno =
                voBankList.get(i).ifsc_code!!.dropLast(7) + voBankList.get(i).account_no


            var totshgcash = voFinTxnDetMemViewModel!!.getIncomeAmount(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                listOf<Int>(2, 3),
                listOf("RG", "RL", "RO"),
                accountno
            )

            var totvocash = voFinTxnDetGrpViewModel!!.getIncomeAmount(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                listOf<Int>(2, 3),
                listOf("RG", "RL", "RO"),
                accountno
            )
            var totshgcashdebit = voFinTxnDetMemViewModel!!.getIncomeAmount(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                listOf<Int>(2, 3),
                listOf("PG", "PL", "PO"),
                accountno
            )
            var totvocashdebit = voFinTxnDetGrpViewModel!!.getIncomeAmount(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                listOf<Int>(2, 3),
                listOf("PG", "PL", "PO"),
                accountno
            )
            var totopeningcash = voFinTxnViewModel!!.gettotalopeninginbank(
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                accountno
            )

            var totmemloan = voMemLoanViewModel!!.getMemLoanDisbursedAmount(
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                accountno,
                listOf<Int>(2, 3)
            )

            var totvoloan = voGroupLoanViewModel!!.getgrouptotloanamtinbank(
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                accountno,
                listOf<Int>(2, 3)
            )

            var totmemloantxn = voMemLoanTxnViewModel!!.getmemtotloanpaidinbank(
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                accountno,
                listOf<Int>(2, 3)
            )

            var totvoloantxn = voGroupLoanTxnViewModel!!.getgrptotloanpaidinbank(
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                accountno,
                listOf<Int>(2, 3)
            )
            var totalclosingbal =
                totopeningcash + totshgcash + totvocash + totmemloantxn + totvoloan - totshgcashdebit - totvocashdebit - totmemloan - totvoloantxn

            voFinTxnViewModel!!.updateclosingbalbank(
                totalclosingbal,
                validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
                validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
                accountno
            )
            //view.tv_bank_amount.text = totalclosingbal.toString()

        }


        /* var totcashwithdrawl = generateMeetingViewmodel.getsumwithdrwal(
             validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
             validate!!.RetriveSharepreferenceLong(MeetingSP.shgid))
 */

        /* generateMeetingViewmodel.updateclosingcash(
             totalclosingbal,
             validate!!.RetriveSharepreferenceInt(MeetingSP.currentmeetingnumber),
             validate!!.RetriveSharepreferenceLong(MeetingSP.shgid)
         )*/
    }


    fun returndefaaultaccount(tvBankname: TextView, tvBankAmount: TextView): String {
        var accountNo = ""
        val bankList = cboBankViewModel!!.getcboBankdata(
            validate!!.RetriveSharepreferenceString(VoSpData.voSHGGUID),
            1
        )
        if (!bankList.isNullOrEmpty()) {
            for (i in bankList.indices) {
                if (bankList.get(i).is_default == 1) {
                    accountNo =
                        bankList.get(i).ifsc_code!!.dropLast(7) + bankList.get(i).account_no
                    tvBankname.text = accountNo
                    break
                }
            }
        }

        var totclosingcash = voFinTxnViewModel!!.gettotalclosinginbank(
            validate!!.RetriveSharepreferenceInt(VoSpData.vocurrentmeetingnumber),
            validate!!.RetriveSharepreferenceLong(VoSpData.voshgid),
            accountNo
        )
        tvBankAmount.text = totclosingcash.toString()
        return accountNo
    }

    inline fun <reified T : Context> Context.startActivity() {
        startActivity(Intent(this, T::class.java))
    }
}


