package com.microware.cdfi.interfaces

interface OnListItemClickListener {
    fun onListItemClicked(itemId:Int,data: Any)
    fun onListItemLongClicked(itemId:Int,data: Any)
}