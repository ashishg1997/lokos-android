package com.microware.cdfi.interfaces

import androidx.lifecycle.LiveData

interface ResultCallBacks {

    fun onSuccess(loginResponse: LiveData<String>)
    fun onError(message:Int)
    fun onError(message:String)
    fun onStarted()
    fun onnetworkfailed()

}