package com.microware.cdfi.repository

import android.app.Application
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.CardreCateogaryDao
import com.microware.cdfi.entity.CardreCateogaryEntity

class CardreCateogaryRepository {

    var cadrecategoryDao: CardreCateogaryDao? = null

    constructor(application: Application){
        cadrecategoryDao = CDFIApplication.database?.cardreCateogaryDao()
    }

    fun getCateogary(langId:String?):List<CardreCateogaryEntity>?{
        return cadrecategoryDao!!.getCateogary(langId)
    }

    fun getCateogaryName(langId:String?,cadre_cat_code:Int):String{
        return cadrecategoryDao!!.getCateogaryName(langId,cadre_cat_code)
    }

}