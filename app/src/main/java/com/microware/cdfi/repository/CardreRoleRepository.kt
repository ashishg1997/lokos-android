package com.microware.cdfi.repository

import android.app.Application
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.CardreRoleDao
import com.microware.cdfi.entity.cadreRoleEntity

class CardreRoleRepository {

    var carderRoleDao: CardreRoleDao? = null

    constructor(application: Application){
        carderRoleDao = CDFIApplication.database?.cardreRoleDao()
    }

    fun getCardreRole(langId:String?,cadre_cat_code:Int):List<cadreRoleEntity>{
        return carderRoleDao!!.getCardreRole(langId, cadre_cat_code)
    }

    fun getAllCardreRole(langId:String?):List<cadreRoleEntity>{
        return carderRoleDao!!.getAllCardreRole(langId)
    }

    fun getRoleName(langId:String?,roleCode:Int):String{
        return carderRoleDao!!.getRoleName(langId,roleCode)
    }

}