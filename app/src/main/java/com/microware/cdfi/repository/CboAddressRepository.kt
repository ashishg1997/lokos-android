package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.CboAddressDao
import com.microware.cdfi.entity.AddressEntity

class CboAddressRepository {
    var cboAddressDao: CboAddressDao? = null

    constructor(application: Application){
        cboAddressDao = CDFIApplication.database?.cboaddressDao()
    }

    fun getAddressdetaildata(address_guid: String?): List<AddressEntity>?{
        return cboAddressDao!!.getAddressdetaildata(address_guid)
    }

    fun getAddressdata(shg_guid: String?): LiveData<List<AddressEntity>>?{
        return cboAddressDao!!.getAddressData(shg_guid)
    }

    fun updateAddressDetail(
        address_guid: String,
        address_line1: String,
        address_line2: String,
        village: Int,
        panchayat_id: Int,
        town: Int,
        landmark: String,
        postal_code: Int,
        is_Active: Int,
        device: Int,
        updated_date: Long,
        updated_by: String
    ) {
        cboAddressDao!!.updateAddressDetail(
            address_guid,
            address_line1,
            address_line2,
            village,panchayat_id,
            town,landmark,postal_code,is_Active,device,updated_date,updated_by
        )
    }


    fun insert(addressEntity : AddressEntity){
        insertAsyncTask(addressEntity,cboAddressDao!!).execute()
    }

    class insertAsyncTask(): AsyncTask<Void, Void, String>(){

        private var addressEntity: AddressEntity? = null
        var cboAddressDao: CboAddressDao? = null

        constructor (addressEntity: AddressEntity?, cboAddressDao: CboAddressDao) : this() {
            this.addressEntity = addressEntity
            this.cboAddressDao = cboAddressDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            cboAddressDao!!.insertAddressDetailData(addressEntity)
            return null
        }

    }

    fun getAddressDatalist(shg_guid: String?): List<AddressEntity>?{
        return cboAddressDao!!.getAddressDatalist(shg_guid)
    }
    fun getAddressDatalistcount(shg_guid: String?): List<AddressEntity>?{
        return cboAddressDao!!.getAddressDatalistcount(shg_guid)
    }

    fun deleteData(address_guid: String?){
        cboAddressDao!!.deleteData(address_guid)
    }

    fun deleteRecord(address_guid: String?){
        cboAddressDao!!.deleteRecord(address_guid)
    }

    fun getCompletionCount(cbo_guid: String?):Int{
        return cboAddressDao!!.getCompletionCount(cbo_guid)
    }

    fun getVerificationCount(cbo_guid: String?):Int{
        return cboAddressDao!!.getVerificationCount(cbo_guid)
    }

    fun updateIsVerifed(shgguid: String?,updated_date: Long,updated_by: String){
        cboAddressDao!!.updateIsVerifed(shgguid,updated_date,updated_by)
    }
}