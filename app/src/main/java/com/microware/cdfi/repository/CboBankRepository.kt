package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.api.vomodel.VoChangePaymentBankDataModel
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.Cbo_BankDao
import com.microware.cdfi.dao.Cbo_phoneDetailDao
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.entity.Cbo_phoneEntity

class CboBankRepository {
    var cboBankdao: Cbo_BankDao? = null

    constructor(application: Application){
        cboBankdao = CDFIApplication.database?.cboBankDao()
    }

    fun getBankdetaildata(bankguid: String?): List<Cbo_bankEntity>?{
        return cboBankdao!!.getBankdetaildata(bankguid)
    }

    fun getBankdata(bankguid: String?): LiveData<List<Cbo_bankEntity>>?{
        return cboBankdao!!.getBankdata(bankguid)
    }

    fun updateBankDetail(
        bank_guid: String,
        bank_id: Int,
        account_opening_date: Long,
        account_Linkage_Date: Long,
        account_no: String,
        bank_branch: Int,
        ifsc_code: String,
        is_default: Int,
        sequence_no: Int,
        account_type: Int,
        verification: Int,
        device: Int,
        updated_date: Long,
        updated_by: String,
        imageName: String,
        is_complete: Int,
        bankpassbook_name: String
    ) {
        cboBankdao!!.updateBankDetail(
            bank_guid,
            bank_id,
            account_opening_date,
            account_Linkage_Date,
            account_no,bank_branch,ifsc_code,is_default,
            sequence_no,account_type,verification,device,updated_date,updated_by,imageName,is_complete,bankpassbook_name
        )
    }


    fun insert(cbobankEntity : Cbo_bankEntity){
        insertAsyncTask(cbobankEntity,cboBankdao!!).execute()
    }

    class insertAsyncTask(): AsyncTask<Void, Void, String>(){

        private var cbobankEntity: Cbo_bankEntity? = null
        var cboBankdao: Cbo_BankDao? = null

        constructor (cbobankEntity: Cbo_bankEntity?, cboBankdao: Cbo_BankDao) : this() {
            this.cbobankEntity = cbobankEntity
            this.cboBankdao = cboBankdao
        }

        override fun doInBackground(vararg params: Void?): String? {
            cboBankdao!!.insertBankDetailData(cbobankEntity)
            return null
        }

    }

    fun getBankdatalist(bankguid: String?): List<Cbo_bankEntity>?{
        return cboBankdao!!.getBankdatalist(bankguid)
    }
    fun getBankdatalistcount(bankguid: String?): List<Cbo_bankEntity>?{
        return cboBankdao!!.getBankdatalistcount(bankguid)
    }

    fun getBankdatalistdefualtcount(cbo_guid: String?): Int{
        return cboBankdao!!.getBankdatalistdefualtcount(cbo_guid)
    }

    fun getbank_acc_count(account_no:String,cboType:Int):Int{
        return cboBankdao!!.getbank_acc_count(account_no,cboType)
    }

    fun deleteData(bank_guid: String?){
        cboBankdao!!.deleteData(bank_guid)
    }

    fun deleteRecord(bank_guid: String?){
        cboBankdao!!.deleteRecord(bank_guid)
    }

    fun getCompletionCount(cbo_guid: String?):Int{
        return cboBankdao!!.getCompletionCount(cbo_guid)
    }

    fun getVerificationCount(cbo_guid: String?):Int{
        return cboBankdao!!.getVerificationCount(cbo_guid)
    }

    fun updateIsVerifed(shgguid: String?,updated_date: Long,updated_by: String){
        cboBankdao!!.updateIsVerifed(shgguid,updated_date,updated_by)
    }

    fun updateactivotaionstatus(bank_guid: String?, status: Int) {
        cboBankdao!!.updateactivotaionstatus(bank_guid, status)
    }
    fun getcboBankdata(cbo_guid: String?,cboType:Int): List<Cbo_bankEntity>?{
        return cboBankdao!!.getcboBankdata(cbo_guid,cboType)
    }

    fun getcboBankdataModel(cbo_guid: String?,cboType:Int): List<VoChangePaymentBankDataModel>?{
        return cboBankdao!!.getcboBankdataModel(cbo_guid,cboType)
    }
    fun getBankdata1(fedrationGUID: String?): List<Cbo_bankEntity>?{
        return cboBankdao!!.getBankdata1(fedrationGUID)
    }

}