package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.CboKycDao
import com.microware.cdfi.dao.Cbo_BankDao
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.entity.Cbo_kycEntity

class CboKycRepository {
    var cboKycDao : CboKycDao? = null

    constructor(application: Application){
        cboKycDao = CDFIApplication.database?.cbokycDao()
    }

    fun getKycdetaildata(shgGUID: String?): LiveData<List<Cbo_kycEntity>>?{
        return cboKycDao!!.getKycdetaildata(shgGUID)
    }
    fun getKycdetail(kycGUID: String?): List<Cbo_kycEntity>?{
        return cboKycDao!!.getKycdetail(kycGUID)
    }

    fun insert(cbokycEntity : Cbo_kycEntity){
        insertAsyncTask(cbokycEntity,cboKycDao!!).execute()
    }

    class insertAsyncTask(): AsyncTask<Void, Void, String>(){

        private var cbokycEntity: Cbo_kycEntity? = null
        var cboKycDao: CboKycDao? = null

        constructor (cbokycEntity: Cbo_kycEntity?, cboKycDao: CboKycDao) : this() {
            this.cbokycEntity = cbokycEntity
            this.cboKycDao = cboKycDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            cboKycDao!!.insertKycDetailData(cbokycEntity)
            return null
        }

    }

    fun updateKycDetail(
        kyc_guid: String,
        kyc_type: Int,
        kyc_id: String,
        kyc_front_doc_orig_name: String,
        kyc_front_doc_encp_name: String,
        kyc_rear_doc_orig_name: String,
        kyc_rear_doc_encp_name: String,
        kyc_valid_from:Long,
        kyc_valid_to:Long,
        updated_date: Long,
        updated_by: String,
        is_complete:Int
    ) {
        cboKycDao!!.updateKycDetail(
            kyc_guid,
            kyc_type,
            kyc_id,
            kyc_front_doc_orig_name,
            kyc_front_doc_encp_name,
            kyc_rear_doc_orig_name,
            kyc_rear_doc_encp_name,
            kyc_valid_from,
            kyc_valid_to,
            updated_date,
            updated_by,is_complete
        )
    }

    internal fun getkycdatalist(cbo_guid: String?): List<Cbo_kycEntity>?{
        return cboKycDao!!.getkycdatalist(cbo_guid)
    }

    internal fun updatededup(guid: String){
        cboKycDao!!.updatededup(guid)
    }

    internal fun deleteData(guid: String?){
        cboKycDao!!.deleteData(guid)
    }
    internal fun deleteRecord(guid: String?){
        cboKycDao!!.deleteRecord(guid)
    }

    internal  fun getKyc_count(kyc_number:String,cboType:Int):Int{
        return cboKycDao!!.getKyc_count(kyc_number,cboType)
    }
}