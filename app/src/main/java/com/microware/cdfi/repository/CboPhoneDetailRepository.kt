package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.Cbo_phoneDetailDao
import com.microware.cdfi.entity.Cbo_phoneEntity

class CboPhoneDetailRepository {
    var cboPhonedetaildao: Cbo_phoneDetailDao? = null

    constructor(application: Application){
        cboPhonedetaildao = CDFIApplication.database?.cbophoneDao()
    }

    fun getphonedetaildata(PhoneGUID: String?): List<Cbo_phoneEntity>?{
        return cboPhonedetaildao!!.getphonedetaildata(PhoneGUID)
    }

    fun getphonedata(shgGUID: String?): LiveData<List<Cbo_phoneEntity>>?{
        return cboPhonedetaildao!!.getphonedata(shgGUID)
    }

    fun insert(cboPhoneEntity : Cbo_phoneEntity){
        insertAsyncTask(cboPhoneEntity,cboPhonedetaildao!!).execute()
    }

    class insertAsyncTask(): AsyncTask<Void,Void,String>(){

        private var cboPhoneEntity: Cbo_phoneEntity? = null
        var cboPhonedetaildao: Cbo_phoneDetailDao? = null

        constructor (cboPhoneEntity: Cbo_phoneEntity?, cboPhonedetaildao: Cbo_phoneDetailDao) : this() {
            this.cboPhoneEntity = cboPhoneEntity
            this.cboPhonedetaildao = cboPhonedetaildao
        }

        override fun doInBackground(vararg params: Void?): String? {
            cboPhonedetaildao!!.insertPhoneDetailData(cboPhoneEntity)
            return null
        }

    }

    fun updatePhoneDetail(
        phone_guid: String,
        member_guid: String,
        phone_no: String,
        isDefault: Int,
        phone_ownership: String,
        phone_ownership_detail: String,
        valid_from: Long,
        valid_till: Long,
        device: Int,
        updated_date: Long,
        updated_by: String,
        is_complete:Int

    ) {
        cboPhonedetaildao!!.updatePhoneDetail(
            phone_guid,
            member_guid,
            phone_no,
            isDefault,
            phone_ownership,
            phone_ownership_detail,valid_from,valid_till,device,updated_date,updated_by,is_complete
        )
    }

    fun getuploadlist(cbo_guid: String?): List<Cbo_phoneEntity>??{
        return cboPhonedetaildao!!.getuploadlist(cbo_guid)
    }
    fun getuploadlistcount(cbo_guid: String?): List<Cbo_phoneEntity>??{
        return cboPhonedetaildao!!.getuploadlistcount(cbo_guid)
    }

    fun deleteData(PhoneGUID: String?){
        cboPhonedetaildao!!.deleteData(PhoneGUID)
    }
    fun deleteRecord(PhoneGUID: String?){
        cboPhonedetaildao!!.deleteRecord(PhoneGUID)
    }

    fun getCompletionCount(cbo_guid: String?):Int{
        return cboPhonedetaildao!!.getCompletionCount(cbo_guid)
    }

    fun getVerificationCount(cbo_guid: String?):Int{
        return cboPhonedetaildao!!.getVerificationCount(cbo_guid)
    }

    fun updateIsVerifed(shgguid: String?,updated_date: Long,updated_by: String){
        cboPhonedetaildao!!.updateIsVerifed(shgguid,updated_date,updated_by)
    }

    fun getphone_count(mobile_no:String,cboType:Int):Int{
        return cboPhonedetaildao!!.getphone_count(mobile_no, cboType)
    }
}