package com.microware.cdfi.repository

import android.app.Application
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.ClfVoMemberDao
import com.microware.cdfi.dao.VOShgMemberDao
import com.microware.cdfi.entity.ClfVoMemberEntity
import com.microware.cdfi.entity.VoShgMemberEntity

class ClfVOMemberRepository {

    var clfVoMemberDao: ClfVoMemberDao? = null

    constructor(application: Application){
        clfVoMemberDao = CDFIApplication.database?.clfVoMemberDao()
    }

    fun getMemberList(vo_guid:String):List<ClfVoMemberEntity>?{
        return clfVoMemberDao!!.getMemberList(vo_guid)
    }
}