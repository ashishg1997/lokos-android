package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.api.meetingmodel.mcpDataListModel
import com.microware.cdfi.api.meetinguploadmodel.ShgLoanApplicationListData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.DtLoanApplicationDao
import com.microware.cdfi.entity.DtLoanApplicationEntity
import com.microware.cdfi.entity.ShgMcpEntity

class DtLoanApplicationRepository {
    var dtLoanApplicationDao: DtLoanApplicationDao? = null

    constructor(application: Application) {
        dtLoanApplicationDao = CDFIApplication.database?.dtLoanApplicationDao()
    }

    fun getLoanApplicationdata(loanappid: Int): List<DtLoanApplicationEntity>? {
        return dtLoanApplicationDao!!.getLoanApplicationdata(loanappid)
    }

    fun getmemberLoanApplication(loanapplication_id: Long): List<DtLoanApplicationEntity>? {
        return dtLoanApplicationDao!!.getmemberLoanApplication(loanapplication_id)
    }

    fun getLoanApplicationlist(mtgno: Int, cboid: Long): List<DtLoanApplicationEntity>? {
        return dtLoanApplicationDao!!.getLoanApplicationlist(mtgno, cboid)
    }

    fun gettotaldemand(mtgno: Int, cboid: Long): Int {
        return dtLoanApplicationDao!!.gettotaldemand(mtgno, cboid)
    }

    fun getmaxloanno(cboid: Long): Int {
        return dtLoanApplicationDao!!.getmaxloanno(cboid)
    }

    fun insert(dtLoanApplicationEntity: DtLoanApplicationEntity) {
        insertAsyncTask(dtLoanApplicationEntity, dtLoanApplicationDao!!).execute()
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var dtLoanApplicationEntity: DtLoanApplicationEntity? = null
        var dtLoanApplicationDao: DtLoanApplicationDao? = null

        constructor (
            dtLoanApplicationEntity: DtLoanApplicationEntity?,
            dtLoanApplicationDao: DtLoanApplicationDao
        ) : this() {
            this.dtLoanApplicationEntity = dtLoanApplicationEntity
            this.dtLoanApplicationDao = dtLoanApplicationDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            dtLoanApplicationDao!!.insertLoanApplicationData(dtLoanApplicationEntity!!)
            return null
        }
    }

    fun deleteRecord(loanappid: Int) {
        dtLoanApplicationDao!!.deleteRecord(loanappid)
    }

    fun updateMemberloanapplication(
        loansanctioned_mtg_guid: String,
        mem_id: Long,
        loanapplication_id: Long,
        amt_disbursed: Int,
        mtgno: Int


    ) {
        dtLoanApplicationDao!!.updateMemberloanapplication(
            loansanctioned_mtg_guid,
            mem_id,
            loanapplication_id,
            amt_disbursed,
            mtgno
        )
    }


    fun updateMemberloanapplicationdetail(
        loanapplication_id: Long,
        amt_disbursed: Int,
        amtsaction: Int,
        loanpurpose: Int,
        loansource: Int,
        priorty: Int,
        tentivedate: Long,
        loan_product_id: Int?,
        mcp_link_id:Long?,
        updatedBy:String?,
        updatedOn:Long?
    ) {
        dtLoanApplicationDao!!.updateMemberloanapplicationdetail(
            loanapplication_id,
            amt_disbursed,
            amtsaction,
            loanpurpose,
            loansource,
            priorty,tentivedate,loan_product_id,
            mcp_link_id,
            updatedBy,
            updatedOn
        )
    }

    fun getMCP_Linkdata(cbo_id:Long,mem_id:Long,mcp_id:Long?): List<mcpDataListModel>?{
        return dtLoanApplicationDao!!.getMCP_Linkdata(cbo_id,mem_id,mcp_id)
    }

    fun getPriorityCount(priority: Int,mtg_no: Int,cbo_id: Long):Int{
        return dtLoanApplicationDao!!.getPriorityCount(priority,mtg_no,cbo_id)
    }
    /* fun updateLoanApplication(
         loanappid: Int,
         cbo_id: Int,
         mem_id: Int,
         request_date: String,
         loanfee: Int,
         amt_demand: Int,
         amt_sanction: Int,
         amt_disbursed: Int,
         approval_date: String,
         tentative_date: String,
         loansource: Int,
         customerstatus: String,
         loanstatus: String,
         loancloserdate: String,
         totalmfi: Int,
         toatalactivemfi: Int,
         disbursedamtmfi: Int,
         loanmfioutstandingself: Int,
         loanmfioutstandingcreditenquiry: Int,
         creditenquiryremarks: String,
         povertystatus: String,
         kycstatus: String,
         cgtmarks: Int,
         grtdate: String,
         grtplace: String,
         cutomerstatusid: Int,
         registrationstatusid: Int,
         registrationdate: String,
         loanappliedid: Int,
         loansanctionedid: Int,
         loandisbursedid: Int,
         loandisburseddate: String,
         loandisbremarks: String,
         loanemidate: String,
         loanapprovedid: Int,
         loanapprovaldate: String,
         loanchequebankid: Int,
         loanchequeno: String,
         loanapprovedamt: Int,
         loanapprovedinterestrate: Double,
         loanapprovedinsuranceamt: Int,
         loanapprovedfeesamt: Int,
         loanapprovedinstallmentamt: Int,
         loanapprovedpaymentweeks: Int,
         loanapprovedcloserminweeks: Int,
         loanapprovedremarks: String,
         isloanclosed: String,
         tabletid: String,
         fundsourceid: Int,
         isdeleted: String,
         actionby: String,
         loandisbursaldoneby: String,
         grtdoneby: String,
         creditenquirydoneby: String,
         cgtdoneby: String,
         existingloanamount: Int,
         existingloancurrentweek: Int,
         existingloancurrentoutstanding: Int,
         grtlat: String,
         grtlong: String,
         regplace: String,
         reglat: String,
         reglong: String,
         isoldcustomer: String,
         customernickname: String,
         ismobilenovalidated: String,
         gurantorage: Int,
         iswriteoff: String,
         loancycle: Int,
         creditequirypdf: String,
         customerbankifsccode: String,
         mandateprint: Int,
         mandatestartdate: String,
         mandateenddate: String,
         mandateapproved: Int,
         mandateremark: String
     ) {
         dtLoanApplicationDao!!.updateLoanApplication(
             loanappid,
             cbo_id,
             mem_id,
             request_date,
             loanfee,
             amt_demand,
             amt_sanction,
             amt_disbursed,
             approval_date,
             tentative_date,
             loansource,
             customerstatus,
             loanstatus,
             loancloserdate,
             totalmfi,
             toatalactivemfi,
             disbursedamtmfi,
             loanmfioutstandingself,
             loanmfioutstandingcreditenquiry,
             creditenquiryremarks,
             povertystatus,
             kycstatus,
             cgtmarks,
             grtdate,
             grtplace,
             cutomerstatusid,
             registrationstatusid,
             registrationdate,
             loanappliedid,
             loansanctionedid,
             loandisbursedid,
             loandisburseddate,
             loandisbremarks,
             loanemidate,
             loanapprovedid,
             loanapprovaldate,
             loanchequebankid,
             loanchequeno,
             loanapprovedamt,
             loanapprovedinterestrate,
             loanapprovedinsuranceamt,
             loanapprovedfeesamt,
             loanapprovedinstallmentamt,
             loanapprovedpaymentweeks,
             loanapprovedcloserminweeks,
             loanapprovedremarks,
             isloanclosed,
             tabletid,
             fundsourceid,
             isdeleted,
             actionby,
             loandisbursaldoneby,
             grtdoneby,
             creditenquirydoneby,
             cgtdoneby,
             existingloanamount,
             existingloancurrentweek,
             existingloancurrentoutstanding,
             grtlat,
             grtlong,
             regplace,
             reglat,
             reglong,
             isoldcustomer,
             customernickname,
             ismobilenovalidated,
             gurantorage,
             iswriteoff,
             loancycle,
             creditequirypdf,
             customerbankifsccode,
             mandateprint,
             mandatestartdate,
             mandateenddate,
             mandateapproved,
             mandateremark
         )
     }*/

    fun getUploadListData(mtg_no: Int,cbo_id: Long) : List<DtLoanApplicationEntity>{
        return dtLoanApplicationDao!!.getUploadListData(mtg_no,cbo_id)
    }

}