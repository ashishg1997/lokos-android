package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.api.meetinguploadmodel.ShgGroupLoanData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.DtLoanGpDao
import com.microware.cdfi.entity.DtLoanGpEntity

class DtLoanGpRepository {
    var dtLoanGpDao: DtLoanGpDao? = null

    constructor(application: Application) {
        dtLoanGpDao = CDFIApplication.database?.dtLoanGpDao()
    }

    fun getLoanGpdata(cbo_id: Long): List<DtLoanGpEntity>? {
        return dtLoanGpDao!!.getLoanGpdata(cbo_id)
    }

    fun getLoandata(cbo_id: Long,loanno: Int): List<DtLoanGpEntity>? {
        return dtLoanGpDao!!.getLoandata(cbo_id,loanno)
    }

    fun getinterestrate(loanno: Int): Double {
        return dtLoanGpDao!!.getinterestrate(loanno)
    }
    fun gettotamt(loanno: Int): Int {
        return dtLoanGpDao!!.gettotamt(loanno)
    }

    fun getmaxLoanno(cbo_id: Long): Int {
        return dtLoanGpDao!!.getmaxLoanno(cbo_id)
    }

    fun insert(dtLoanGpEntity: DtLoanGpEntity) {
        insertAsyncTask(dtLoanGpEntity, dtLoanGpDao!!).execute()
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var dtLoanGpEntity: DtLoanGpEntity? = null
        var dtLoanGpDao: DtLoanGpDao? = null

        constructor (
            dtLoanGpEntity: DtLoanGpEntity?,
            dtLoanGpDao: DtLoanGpDao
        ) : this() {
            this.dtLoanGpEntity = dtLoanGpEntity
            this.dtLoanGpDao = dtLoanGpDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            dtLoanGpDao!!.insertLoanGpData(dtLoanGpEntity!!)
            return null
        }
    }

    fun deleteRecord(cbo_id: Int) {
        dtLoanGpDao!!.deleteRecord(cbo_id)
    }


    fun updateLoanGp(
        cbo_id: Int,
        mtgno: Int,
        mtgdate: String,
        loanno: Int,
        loan_type: String,
        loan_product: Int,
        loan_accountno: String,
        loan_period: Int,
        installmentdate: String,
        amount: Int,
        interest_rate: Double,
        interest_due: Int,
        completion_flag: String,
        mode_payment: String,
        bank_code: String,
        loanpurpose: Int,
        createdby: String,
        createdon: String,
        updatedby: String,
        updatedon: String,
        uploadedon: String

    ) {
        dtLoanGpDao!!.updateLoanGp(
            cbo_id,
            mtgno,
            mtgdate,
            loanno,
            loan_type,
            loan_product,
            loan_accountno,
            loan_period,
            installmentdate,
            amount,
            interest_rate,
            interest_due,
            completion_flag,
            mode_payment,
            bank_code,
            loanpurpose,
            createdby,
            createdon,
            updatedby,
            updatedon,
            uploadedon

        )
    }

    internal fun getListDataByMtgnum(mtgno:Int,cbo_aid:Long): List<DtLoanGpEntity> {
        return dtLoanGpDao!!.getListDataByMtgnum(mtgno,cbo_aid)
    }

    internal fun gettotloangroupamount(mtgno:Int,cbo_id:Long): Int {
        return dtLoanGpDao!!.gettotloangroupamount(mtgno,cbo_id)
    }

    fun getUploadListData(mtg_no:Int,cbo_id: Long) : List<DtLoanGpEntity>{
        return dtLoanGpDao!!.getUploadListData(mtg_no,cbo_id)
    }
    internal fun updateShgGroupLoan(cbo_id:Long,loan_no: Int,completionflag: Boolean){
        dtLoanGpDao!!.updateShgGroupLoan(cbo_id,loan_no,completionflag)
    }

    internal fun getUploadGroupLoanList(cbo_id: Long) : List<DtLoanGpEntity>{
        return dtLoanGpDao!!.getUploadGroupLoanList(cbo_id)
    }
}