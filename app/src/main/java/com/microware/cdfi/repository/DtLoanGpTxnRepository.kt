package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.api.meetinguploadmodel.ShgGroupLoanTransactionData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.DtLoanGpTxnDao
import com.microware.cdfi.entity.DtLoanGpTxnEntity

class DtLoanGpTxnRepository {
    var dtLoanGpTxnDao: DtLoanGpTxnDao? = null

    constructor(application: Application) {
        dtLoanGpTxnDao = CDFIApplication.database?.dtLoanGpTxnDao()
    }

    internal fun getListDataByMtgnum(mtgno:Int,cbo_aid:Long): List<DtLoanGpTxnEntity> {
        return dtLoanGpTxnDao!!.getListDataByMtgnum(mtgno,cbo_aid)
    }
    fun insert(dtLoanGpTxnEntity: DtLoanGpTxnEntity) {
        insertAsyncTask(dtLoanGpTxnEntity, dtLoanGpTxnDao!!).execute()
    }
    fun getloandata(loanno: Int, mtgno: Int,shgid:Long): List<DtLoanGpTxnEntity>? {
        return dtLoanGpTxnDao!!.getloandata(loanno,mtgno,shgid)
    }
    fun gettotalpaid(cboid: Long, mtgno: Int,loanno:Int): Int {
        return dtLoanGpTxnDao!!.gettotalpaid(cboid, mtgno,loanno)
    }


    fun updateloanpaid(
        loanno: Int,mtgno: Int, cboid: Long, paid: Int, loanint: Int,
        mode: Int,
        bankaccount: String,
        transactionno: String,
        principaldemandcl: Int,
        completionflag:Boolean
    ) {
        dtLoanGpTxnDao!!.updateloanpaid(loanno, mtgno, cboid, paid,loanint,mode,bankaccount,transactionno,principaldemandcl,completionflag
        )
    }
    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var dtLoanGpTxnEntity: DtLoanGpTxnEntity? = null
        var dtLoanGpTxnDao: DtLoanGpTxnDao? = null

        constructor (
            dtLoanGpTxnEntity: DtLoanGpTxnEntity?,
            dtLoanGpTxnDao: DtLoanGpTxnDao
        ) : this() {
            this.dtLoanGpTxnEntity = dtLoanGpTxnEntity
            this.dtLoanGpTxnDao = dtLoanGpTxnDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            dtLoanGpTxnDao!!.insertLoanGpTxnData(dtLoanGpTxnEntity!!)
            return null
        }
    }

    fun updateLoanGpTxn(
        UID: Int,
        LoanNo: Int,
        cbo_id: Int,
        MtgNo: Int,
        MtgDate: Long,
        loan_no: Int,
        loan_op: Int,
        loan_op_int: Int,
        loan_paid: Int,
        loan_paid_int: Int,
        loan_cl: Int,
        loan_cl_int: Int,
        completion_flag: Int,
        intaccrued_op: Int,
        intaccrued: Int,
        intaccrued_cl: Int,
        principal_demand_ob: Int,
        principal_demand: Int,
        principal_demand_cb: Int,
        mode_payment: Int,
        bank_code: String,
        transaction_no: String,
        createdby: String,
        createdon: Long,
        updatedby: String,
        updatedon: Long,
        uploaded_by: String
    ) {
        dtLoanGpTxnDao!!.updateLoanGpTxn(
            UID,
            LoanNo,
            cbo_id,
            MtgNo,
            MtgDate,
            loan_no,
            loan_op,
            loan_op_int,
            loan_paid,
            loan_paid_int,
            loan_cl,
            loan_cl_int,
            completion_flag,
            intaccrued_op,
            intaccrued,
            intaccrued_cl,
            principal_demand_ob,
            principal_demand,
            principal_demand_cb,
            mode_payment,
            bank_code,
            transaction_no,
            createdby,
            createdon,
            updatedby,
            updatedon,
            uploaded_by

        )
    }

    fun getLoanRepaymentAmount(cboid: Long, mtgno: Int):Int{
        return dtLoanGpTxnDao!!.getLoanRepaymentAmount(cboid, mtgno)
    }

    fun getLoanRepaymentByModeAmount(cboid: Long, mtgno: Int,modePayment:Int):Int{
        return dtLoanGpTxnDao!!.getLoanRepaymentAmountByMode(cboid, mtgno,modePayment)
    }

    fun getLoanRepaymentAmountByMode1(cboid: Long, mtgno: Int):Int{
        return dtLoanGpTxnDao!!.getLoanRepaymentAmountByMode1(cboid, mtgno)
    }

    fun getShgGrpLoanTxnAnount(cboid: Long, mtgno: Int,bank_code:String):Int{
        return dtLoanGpTxnDao!!.getShgGrpLoanTxnAnount(cboid,mtgno, bank_code)
    }

    fun getUploadListData(mtg_no:Int,cbo_id: Long) : List<DtLoanGpTxnEntity>{
        return dtLoanGpTxnDao!!.getUploadListData(mtg_no,cbo_id)
    }

}