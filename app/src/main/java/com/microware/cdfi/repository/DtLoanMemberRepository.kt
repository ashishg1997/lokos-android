package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.DtLoanMemberDao
import com.microware.cdfi.entity.DtLoanMemberEntity

class DtLoanMemberRepository {
    var dtLoanMemberDao: DtLoanMemberDao? = null

    constructor(application: Application) {
        dtLoanMemberDao = CDFIApplication.database?.dtLoanMemberDao()
    }

    fun getLoanMemberdata(uid: Int): List<DtLoanMemberEntity>? {
        return dtLoanMemberDao!!.getLoanMemberdata(uid)
    }

   fun getLoanno(loanappid: Long): Int {
        return dtLoanMemberDao!!.getLoanno(loanappid)
    }
 fun getLoancount(loanappid: Long): List<DtLoanMemberEntity>? {
        return dtLoanMemberDao!!.getLoancount(loanappid)
    }

   fun getmaxLoanno(cbo_id: Long): Int {
        return dtLoanMemberDao!!.getmaxLoanno(cbo_id)
    }

   fun getinterestrate(loanno: Int): Double {
        return dtLoanMemberDao!!.getinterestrate(loanno)
    }
 fun gettotamt(loanno: Int,member_id:Long): Double {
        return dtLoanMemberDao!!.gettotamt(loanno,member_id)
    }

    fun insert(dtLoanMemberEntity: DtLoanMemberEntity) {
        insertAsyncTask(dtLoanMemberEntity, dtLoanMemberDao!!).execute()
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var dtLoanMemberEntity: DtLoanMemberEntity? = null
        var dtLoanMemberDao: DtLoanMemberDao? = null

        constructor (
            dtLoanMemberEntity: DtLoanMemberEntity,
            dtLoanMemberDao: DtLoanMemberDao
        ) : this() {
            this.dtLoanMemberEntity = dtLoanMemberEntity
            this.dtLoanMemberDao = dtLoanMemberDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            dtLoanMemberDao!!.insertLoanMemberData(dtLoanMemberEntity!!)
            return null
        }
    }

    fun deleteRecord(uid: Int) {
        dtLoanMemberDao!!.deleteRecord(uid)
    }


    /*fun updateLoanMember(
        uid: Int,
        cbo_id: Int,
        member_AID: Long,
        mtg_GUID: String,
        mem_id: Long,
        loanno: Long,
        mtgno: Int,
        mtgdate: String,
        installmentdate: String,
        installments: Int,
        amount: Double,
        loan_purpose: Int,
        loan_prod_code: Int,
        interest_rate: Double,
        period: Int,
        interest_due: Double,
        completionflag: String,
        loantype: Int,
        loansource: Int,
        externalloanid: Int,
        modepayment: Int,
        bankcode: String,
        installmentfreq: Int,
        moratoriumperiod: Int,
        loanaccountno: String,
        createdby: String,
        createdon: String,
        updatedby: String,
        updatedon: String,
        uploadedon: String,
        corpus_external_id: Int,
        flag: String
    ) {
        dtLoanMemberDao!!.updateLoanMember(
            uid,
            cbo_id,
            member_AID,
            mtg_GUID,
            mem_id,
            loanno,
            mtgno,
            mtgdate,
            installmentdate,
            installments,
            amount,
            loan_purpose,
            loan_prod_code,
            interest_rate,
            period,
            interest_due,
            completionflag,
            loantype,
            loansource,
            externalloanid,
            modepayment,
            bankcode,
            installmentfreq,
            moratoriumperiod,
            loanaccountno,
            createdby,
            createdon,
            updatedby,
            updatedon,
            uploadedon,
            corpus_external_id,
            flag
        )
    }*/

    fun getLoanDetailData(loan_no: Int,cbo_id: Long,mem_id: Long,mtg_guid:String): List<DtLoanMemberEntity>?{
        return dtLoanMemberDao!!.getLoanDetailData(loan_no,cbo_id,mem_id,mtg_guid)
    }

    fun getloanListSummeryData(mtgno: Int, cbo_id: Long,mem_id: Long): List<DtLoanMemberEntity>{
        return dtLoanMemberDao!!.getloanListSummeryData(mtgno, cbo_id, mem_id)
    }
    fun getLoanDisbursedAmount(cbo_id: Long,mtg_no: Int,modePayment:Int):Int{
        return dtLoanMemberDao!!.getLoanDisbursedAmount(cbo_id,mtg_no,modePayment)
    }

    fun getMemberLoanDisbursedAmount(cbo_id: Long,mtg_no: Int):Int{
        return dtLoanMemberDao!!.getMemberLoanDisbursedAmount(cbo_id,mtg_no)
    }

    fun getMemberLoanDisbursedBankAmount(cbo_id: Long,mtg_no: Int):Int{
        return dtLoanMemberDao!!.getMemberLoanDisbursedBankAmount(cbo_id,mtg_no)
    }

    fun getMemLoanDisbursedAmount(cbo_id: Long,mtg_no: Int,bank_code:String):Int{
        return dtLoanMemberDao!!.getMemLoanDisbursedAmount(cbo_id, mtg_no, bank_code)
    }
    fun getTotalClosedLoanAmount(mtgNum:Int,mem_id: Long,completionFlag: Boolean):Int{
        return dtLoanMemberDao!!.getTotalClosedLoanAmount(mtgNum,mem_id,completionFlag)
    }

    fun getTotalClosedLoanCount(mtgNum:Int,mem_id: Long,completionFlag:Boolean):Int{
        return dtLoanMemberDao!!.getTotalClosedLoanCount(mtgNum,mem_id,completionFlag)
    }

    fun getUploadListData(mtg_no:Int,cbo_id: Long) : List<DtLoanMemberEntity>{
        return dtLoanMemberDao!!.getUploadListData(mtg_no,cbo_id)
    }

    fun getCutOffmaxLoanno(cbo_id: Long,mtg_no: Int): Int{
        return dtLoanMemberDao!!.getCutOffmaxLoanno(cbo_id,mtg_no)
    }

    fun getTotalClosedLoan(mtgNum:Int,mem_id: Long,completionFlag:Boolean):Int{
        return dtLoanMemberDao!!.getTotalClosedLoan(mtgNum,mem_id,completionFlag)
    }

    fun getClosedLoanAmount(mtgNum:Int,mem_id: Long,completionFlag: Boolean):Int{
        return dtLoanMemberDao!!.getClosedLoanAmount(mtgNum,mem_id,completionFlag)
    }
    internal  fun updateMemberLoanEditFlag(
        cbo_id: Long,
        mem_id: Long,
        loan_no: Int,
        completionflag: Boolean
    ){
        dtLoanMemberDao!!.updateMemberLoanEditFlag(cbo_id,mem_id,loan_no,completionflag)
    }

    internal fun getUploadMemberLoanList(cbo_id: Long) : List<DtLoanMemberEntity>{
        return dtLoanMemberDao!!.getUploadMemberLoanList(cbo_id)
    }
}
