package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import com.microware.cdfi.api.meetinguploadmodel.ShgMeberLoanScheduleData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.DtLoanMemberScheduleDao
import com.microware.cdfi.entity.DtLoanMemberSheduleEntity

class DtLoanMemberScheduleRepository {
    var dtLoanMemberScheduleDao: DtLoanMemberScheduleDao? = null

    constructor(application: Application) {
        dtLoanMemberScheduleDao = CDFIApplication.database?.dtLoanMemberScheduleDao()
    }

    fun getLoanMemberScheduledata(cbo_id: Int): List<DtLoanMemberSheduleEntity>? {
        return dtLoanMemberScheduleDao!!.getLoanMemberScheduledata(cbo_id)
    }

    fun getMemberScheduledata(mem_id: Long,loanno:Int): List<DtLoanMemberSheduleEntity>? {
        return dtLoanMemberScheduleDao!!.getMemberScheduledata(mem_id,loanno)
    }
 fun getMemberScheduleloanwise(mem_id: Long,loanno:Int,mtgno: Int): List<DtLoanMemberSheduleEntity>? {
        return dtLoanMemberScheduleDao!!.getMemberScheduleloanwise(mem_id,loanno,mtgno)
    }
fun gettotalloanamt(loanno: Int,mem_id: Long,mtgguid:String): Int {
        return dtLoanMemberScheduleDao!!.gettotalloanamt(loanno,mem_id,mtgguid)
    }
fun gettotaloutstanding(loanno: Int,mem_id: Long): Int {
        return dtLoanMemberScheduleDao!!.gettotaloutstanding(loanno,mem_id)
    }

fun getremaininginstallment(loanno: Int,mem_id: Long): Int {
        return dtLoanMemberScheduleDao!!.getremaininginstallment(loanno,mem_id)
    }


fun getprincipaldemand(loanno: Int,mem_id: Long,currentmtgdate:Long,lastmtgdate:Long): Int {
        return dtLoanMemberScheduleDao!!.getprincipaldemand(loanno,mem_id,currentmtgdate,lastmtgdate)
    }


    fun insert(dtLoanMemberScheduleEntity: DtLoanMemberSheduleEntity) {
        insertAsyncTask(dtLoanMemberScheduleEntity, dtLoanMemberScheduleDao!!).execute()
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var dtLoanMemberScheduleEntity: DtLoanMemberSheduleEntity? = null
        var dtLoanMemberScheduleDao: DtLoanMemberScheduleDao? = null

        constructor (
            dtLoanMemberScheduleEntity: DtLoanMemberSheduleEntity?,
            dtLoanMemberScheduleDao: DtLoanMemberScheduleDao
        ) : this() {
            this.dtLoanMemberScheduleEntity = dtLoanMemberScheduleEntity
            this.dtLoanMemberScheduleDao = dtLoanMemberScheduleDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            dtLoanMemberScheduleDao!!.insertLoanMemberScheduleData(dtLoanMemberScheduleEntity!!)
            return null
        }
    }
    fun updateLoanMemberSchedule(
        mtgguid: String,
        mem_id: Long,
        loanno: Int,
        installmentno: Int,
        principaldemand: Int,
       loandemandos: Int,
        loanos: Int


        ) {
        dtLoanMemberScheduleDao!!.updateLoanMemberSchedule(
            mtgguid,
            mem_id,
            loanno,
            installmentno,
            principaldemand,
            loandemandos,
            loanos

        )
    }


    fun updaterepaid(
        loanno: Int,
        mem_id: Long,
        installmentno: Int,
        sub_installment_no: Int,
        loanpaid: Int,
        mtgdate: Long ,
        mtgno: Int ,
        updatedby: String?,
        updatedon: Long )
    {
        dtLoanMemberScheduleDao!!. updaterepaid(
            loanno,
            mem_id,
            installmentno,
            sub_installment_no,
            loanpaid,
            mtgdate,mtgno ,updatedby,updatedon  )

    }
    fun deleteRecord(cbo_id: Int) {
        dtLoanMemberScheduleDao!!.deleteRecord(cbo_id)
    }

    /*fun updateLoanMemberSchedule(
        cbo_id: Int,
        mem_id: Int,
        loanno: Int,
        principaldemand: Int,
        loanos: Int,
        loanpaid: Int,
        installmentno: Int,
        subinstallmentno: Int,
        installmentdate: String,
        loandate: String,
        repaid: String,
        lastpaiddate: String,
        prepayment: Int,
        totloanos: Int,
        sequence: String,
        currentDue: Int,
        overdue: Int,
        mtgno: Int,
        flag: String,
        txnmtgno: Int,
        sflag: String,
        loanAccountNo: String,
        createdby: String,
        createdon: String,
        updatedby: String,
        updatedon: String,
        uploadedon: String
    ) {
        dtLoanMemberScheduleDao!!.updateLoanMemberSchedule(
            cbo_id,
            mem_id,
            loanno,
            principaldemand,
            loanos,
            loanpaid,
            installmentno,
            subinstallmentno,
            installmentdate,
            loandate,
            repaid,
            lastpaiddate,
            prepayment,
            totloanos,
            sequence,
            currentDue,
            overdue,
            mtgno,
            flag,
            txnmtgno,
            sflag,
            loanAccountNo,
            createdby,
            createdon,
            updatedby,
            updatedon,
            uploadedon
        )
    }*/

    fun updateCutOffLoanMemberSchedule(
        mtgguid: String,
        mem_id: Long,
        loanno: Int,
        installmentno: Int,
        principaldemand: Int
    ) {
        dtLoanMemberScheduleDao!!.updateCutOffLoanMemberSchedule(
            mtgguid,
            mem_id,
            loanno,
            installmentno,
            principaldemand

        )
    }

    fun getPrincipalDemandByInstallmentNum(mem_id: Long,mtg_guid: String,loan_no: Int,installment_no:Int):Int{
      return  dtLoanMemberScheduleDao!!.getPrincipalDemandByInstallmentNum(mem_id,mtg_guid,loan_no,installment_no)
    }
    fun getnextdemand(mem_id: Long,loanno:Int): Int {
        return dtLoanMemberScheduleDao!!.getnextdemand(mem_id,loanno)
    }

    fun deleterepaid(
        loanno: Int,
        mem_id: Long,
        mtgdate: Long ,
        mtgno: Int  )
    {
        dtLoanMemberScheduleDao!!. deleterepaid(
            loanno,
            mem_id,
            mtgdate,mtgno   )

    }
    fun deletesubinstallment(
        loanno: Int,
        mem_id: Long,
        mtgdate: Long ,
        mtgno: Int  )
    {
        dtLoanMemberScheduleDao!!. deletesubinstallment(
            loanno,
            mem_id,
            mtgdate,mtgno   )

    }
    fun getUploadListData(mem_id:Long,cbo_id: Long,loanno:Int) : List<DtLoanMemberSheduleEntity>{
        return dtLoanMemberScheduleDao!!.getUploadListData(mem_id,cbo_id,loanno)
    }
    fun gettotalloanoutstanding(mem_id: Long): Int {
        return dtLoanMemberScheduleDao!!.gettotalloanoutstanding(mem_id)
    }

    fun getMaxRepaidInstallmentNum(cboId:Long,mem_id: Long,loanno: Int):Int{
        return dtLoanMemberScheduleDao!!.getMaxRepaidInstallmentNum(cboId,mem_id,loanno)
    }

    fun getInstallmentdateByInstallmentNum(installmentNum:Int,cboId:Long,mem_id: Long,loanno: Int):Long{
        return dtLoanMemberScheduleDao!!.getInstallmentdateByInstallmentNum(installmentNum,cboId,mem_id,loanno)
    }

    fun deleteMemberSubinstallment(cboId:Long,mem_id: Long,loanno: Int){
        return dtLoanMemberScheduleDao!!.deleteMemberSubinstallment(cboId,mem_id,loanno)
    }
}