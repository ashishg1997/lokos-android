package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.api.meetingmodel.LoanRepaymentListModel
import com.microware.cdfi.api.meetinguploadmodel.ShgMemberLoanTransactionListData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.DtLoanTxnMemDao
import com.microware.cdfi.entity.DtLoanTxnMemEntity

class DtLoanTxnMemRepository {
    var dtLoanTxnMemDao: DtLoanTxnMemDao? = null

    constructor(application: Application) {
        dtLoanTxnMemDao = CDFIApplication.database?.dtLoanTxnMemDao()
    }

    fun getmeetingLoanTxnMemdata(cboid: Long,mem_id: Long,mtgno: Int): List<DtLoanTxnMemEntity>? {
        return dtLoanTxnMemDao!!.getmeetingLoanTxnMemdata(cboid,mem_id,mtgno)
    }
 fun gettotalpaid(mem_id: Long,mtgno: Int): Int {
        return dtLoanTxnMemDao!!.gettotalpaid(mem_id,mtgno)
    }

    fun getmemberloantxnlist(memid: Long, mtgno: Int): List<DtLoanTxnMemEntity>? {
        return dtLoanTxnMemDao!!.getmemberloantxnlist(memid,mtgno)
    }
 fun getmemberloan(memid: Long, mtgno: Int): List<LoanRepaymentListModel>? {
        return dtLoanTxnMemDao!!.getmemberloan(memid,mtgno)
    }

   fun getmemberloandata(loanno: Int, mtgno: Int, memid: Long): List<DtLoanTxnMemEntity>? {
        return dtLoanTxnMemDao!!.getmemberloandata(loanno,mtgno,memid)
    }

    fun insert(dtLoanTxnMemEntity: DtLoanTxnMemEntity) {
        insertAsyncTask(dtLoanTxnMemEntity, dtLoanTxnMemDao!!).execute()
    }
    fun updateloanpaid(
        loanno: Int,mtgno: Int, memid: Long, paid: Int, loanint: Int,
        mode: Int,
        bankaccount: String,
        transactionno: String,
        principaldemandcl: Int,
        completionFlag:Boolean
    ) {
        dtLoanTxnMemDao!!.updateloanpaid(loanno, mtgno, memid, paid,loanint,
            mode,
            bankaccount,
            transactionno,principaldemandcl,completionFlag)
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var dtLoanTxnMemEntity: DtLoanTxnMemEntity? = null
        var dtLoanTxnMemDao: DtLoanTxnMemDao? = null

        constructor (
            dtLoanTxnMemEntity: DtLoanTxnMemEntity?,
            dtLoanTxnMemDao: DtLoanTxnMemDao
        ) : this() {
            this.dtLoanTxnMemEntity = dtLoanTxnMemEntity
            this.dtLoanTxnMemDao = dtLoanTxnMemDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            dtLoanTxnMemDao!!.insertLoanTxnMemData(dtLoanTxnMemEntity!!)
            return null
        }
    }

    fun deleteRecord(uid: Int) {
        dtLoanTxnMemDao!!.deleteRecord(uid)
    }


   /* fun updateLoanTxnMem(
        uid: Int,
        cbo_id: Int,
        mtgno: Int,
        mtgdate: String,
        mem_id: Int,
        serial: Int,
        loanflag: String,
        loan_no: Int,
        loan_org: Int,
        loan_op: Int,
        loan_opint: Int,
        loan_due: Int,
        loan_dueint: Int,
        loan_paid: Int,
        loan_paidint: Int,
        loan_cl: Int,
        loan_clint: Int,
        loan_period: Int,
        nointerestperiod: Int,
        completionflag: String,
        remarks: String,
        loanint_accrued: Int,
        intaccruedop: Int,
        intaccrued: Int,
        intaccruedcl: Int,
        intduration: Int,
        intdurationcb: Int,
        principaldemand_ob: Int,
        principaldemand: Int,
        principaldemand_cb: Int,
        intdemand: Int,
        modepayment: Int,
        bankcode: String,
        penaltydemand_ob: Int,
        penaltydemand: Int,
        penaltydemand_cb: Int,
        penalty_paid: Int,
        createdby: String,
        createdon: String,
        updatedby: String,
        updatedon: String,
        uploadedon: String,
        loansource: String,
        demandcalculation: Int,
        mnthintdemand: Int,
        loanaccountno: String,
        narration: String
    ) {
        dtLoanTxnMemDao!!.updateLoanTxnMem(
            uid,
            cbo_id,
            mtgno,
            mtgdate,
            mem_id,
            serial,
            loanflag,
            loan_no,
            loan_org,
            loan_op,
            loan_opint,
            loan_due,
            loan_dueint,
            loan_paid,
            loan_paidint,
            loan_cl,
            loan_clint,
            loan_period,
            nointerestperiod,
            completionflag,
            remarks,
            loanint_accrued,
            intaccruedop,
            intaccrued,
            intaccruedcl,
            intduration,
            intdurationcb,
            principaldemand_ob,
            principaldemand,
            principaldemand_cb,
            intdemand,
            modepayment,
            bankcode,
            penaltydemand_ob,
            penaltydemand,
            penaltydemand_cb,
            penalty_paid,
            createdby,
            createdon,
            updatedby,
            updatedon,
            uploadedon,
            loansource,
            demandcalculation,
            mnthintdemand,
            loanaccountno,
            narration

        )
    }*/

    fun getLoanRepaymentAmount(cboid: Long, mtgno: Int):Int{
        return dtLoanTxnMemDao!!.getLoanRepaymentAmount(cboid, mtgno)
    }

    fun getLoanRepaymentByModeAmount(cboid: Long, mtgno: Int,modePayment:Int):Int{
        return dtLoanTxnMemDao!!.getLoanRepaymentAmountByMode(cboid, mtgno,modePayment)
    }

    fun getLoanRepaymentAmountByMode1(cboid: Long, mtgno: Int):Int{
        return dtLoanTxnMemDao!!.getLoanRepaymentAmountByMode1(cboid, mtgno)
    }


    fun getMemberLoanRepaymentAmount(cboid: Long, mtgno: Int,mem_id: Long):Int{
        return dtLoanTxnMemDao!!.getMemberLoanRepaymentAmount(cboid, mtgno,mem_id)
    }

    fun getMemberLoanRepaymentIntAmount(cboid: Long, mtgno: Int,mem_id: Long):Int{
        return dtLoanTxnMemDao!!.getMemberLoanRepaymentIntAmount(cboid, mtgno, mem_id)
    }

    fun getMemberLoanRepaymentOPAmount(cboid: Long, mtgno: Int,mem_id: Long):Int{
        return dtLoanTxnMemDao!!.getMemberLoanRepaymentOPAmount(cboid, mtgno, mem_id)
    }

    fun getShgMemLoanTxnAnount(cboid: Long, mtgno: Int,bank_code:String):Int{
        return dtLoanTxnMemDao!!.getShgMemLoanTxnAnount(cboid, mtgno, bank_code)
    }

    fun getUploadListData(mtg_no: Int,cbo_id: Long,mem_id: Long) : List<DtLoanTxnMemEntity>{
        return dtLoanTxnMemDao!!.getUploadListData(mtg_no,cbo_id,mem_id)
    }

    fun getMemberLoanTxnData(mtg_no: Int,cbo_id: Long) : List<DtLoanTxnMemEntity>{
        return dtLoanTxnMemDao!!.getMemberLoanTxnData(mtg_no,cbo_id)
    }
}