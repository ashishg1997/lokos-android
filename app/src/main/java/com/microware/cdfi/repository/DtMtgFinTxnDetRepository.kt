package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.DtMtgFinTxnDetDao
import com.microware.cdfi.entity.DtMtgFinTxnDetEntity

class DtMtgFinTxnDetRepository {
    var dtMtgFinTxnDetDao: DtMtgFinTxnDetDao? = null

    constructor(application: Application) {
        dtMtgFinTxnDetDao = CDFIApplication.database?.dtMtgFinTxnDetDao()
    }

    fun getMtgFinTxnDetdata(uid: Int): List<DtMtgFinTxnDetEntity>? {
        return dtMtgFinTxnDetDao!!.getMtgFinTxnDetdata(uid)
    }

    fun insert(dtMtgFinTxnDetEntity: DtMtgFinTxnDetEntity) {
        insertAsyncTask(dtMtgFinTxnDetEntity, dtMtgFinTxnDetDao!!).execute()
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var dtMtgFinTxnDetEntity: DtMtgFinTxnDetEntity? = null
        var dtMtgFinTxnDetDao: DtMtgFinTxnDetDao? = null

        constructor (
            dtMtgFinTxnDetEntity: DtMtgFinTxnDetEntity?,
            dtMtgFinTxnDetDao: DtMtgFinTxnDetDao
        ) : this() {
            this.dtMtgFinTxnDetEntity = dtMtgFinTxnDetEntity
            this.dtMtgFinTxnDetDao = dtMtgFinTxnDetDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            dtMtgFinTxnDetDao!!.insertMtgFinTxnDetData(dtMtgFinTxnDetEntity!!)
            return null
        }
    }

    fun deleteRecord(uid: Int) {
        dtMtgFinTxnDetDao!!.deleteRecord(uid)
    }


    fun updateMtgFinTxnDet(
        uid: Int,
        cbo_id: Int,
        group_m_code: Int,
        mtgno: Int,
        bankcode: String,
        auid: Int,
        type: String,
        amount: Double,
        transdate: String,
        deposit_receipt: Double,
        withdrawal_payment: Double,
        date_realisation: String,
        loanno: Int,
        effectivedate: String,
        narration: String
    ) {
        dtMtgFinTxnDetDao!!.updateMtgFinTxnDet(
            uid,
            cbo_id,
            group_m_code,
            mtgno,
            bankcode,
            auid,
            type,
            amount,
            transdate,
            deposit_receipt,
            withdrawal_payment,
            date_realisation,
            loanno,
            effectivedate,
            narration
        )
    }
}