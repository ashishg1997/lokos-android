package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.DtMtgFinTxnDao
import com.microware.cdfi.entity.DtMtgFinTxnEntity

class DtMtgFinTxnRepository {
    var dtMtgFinTxnDao: DtMtgFinTxnDao? = null

    constructor(application: Application) {
        dtMtgFinTxnDao = CDFIApplication.database?.dtMtgFinTxnDao()
    }

    fun getMtgFinTxndata(uid: Int): List<DtMtgFinTxnEntity>? {
        return dtMtgFinTxnDao!!.getMtgFinTxndata(uid)
    }

    fun insert(dtMtgFinTxnEntity: DtMtgFinTxnEntity) {
        insertAsyncTask(dtMtgFinTxnEntity, dtMtgFinTxnDao!!).execute()
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var dtMtgFinTxnEntity: DtMtgFinTxnEntity? = null
        var dtMtgFinTxnDao: DtMtgFinTxnDao? = null

        constructor (
            dtMtgFinTxnEntity: DtMtgFinTxnEntity?,
            dtMtgFinTxnDao: DtMtgFinTxnDao
        ) : this() {
            this.dtMtgFinTxnEntity = dtMtgFinTxnEntity
            this.dtMtgFinTxnDao = dtMtgFinTxnDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            dtMtgFinTxnDao!!.insertMtgFinTxnData(dtMtgFinTxnEntity!!)
            return null
        }
    }

    fun deleteRecord(uid: Int) {
        dtMtgFinTxnDao!!.deleteRecord(uid)
    }


    fun updateMtgFinTxn(
        uid: Int?,
        cbo_id: Int?,
        mtgno: Int?,
        bankcode: String?,
        openingbalance: Int?,
        closingbalance: Int?,
        otherdeposits: Int?,
        otherwithdrawals: Int?,
        depositedcash: Int?,
        withdrawncash: Int?,
        createdby: String?,
        createdon: Long?,
        updatedby: String,
        updatedon: Long?,
        uploaded_by: String?
    ) {
        dtMtgFinTxnDao!!.updateMtgFinTxn(
            uid,
            cbo_id,
            mtgno,
            bankcode,
            openingbalance,
            closingbalance,
            otherdeposits,
            otherwithdrawals,
            depositedcash,
            withdrawncash,
            createdby,
            createdon,
            updatedby,
            updatedon,
            uploaded_by)
    }
    internal fun getListDataByMtgnum(mtgno:Int,cbo_id:Long,
                                     accountNo: String): List<DtMtgFinTxnEntity> {
        return dtMtgFinTxnDao!!.getListDataByMtgnum(mtgno,cbo_id,accountNo)
    }

}