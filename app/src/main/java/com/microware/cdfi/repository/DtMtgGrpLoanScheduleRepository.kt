package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.api.meetinguploadmodel.ShgGroupLoanScheduleData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.DtMtgGrpLoanScheduleDao
import com.microware.cdfi.entity.DtMtgGrpLoanScheduleEntity

class DtMtgGrpLoanScheduleRepository {
    var dtMtgGrpLoanScheduleDao: DtMtgGrpLoanScheduleDao? = null

    constructor(application: Application) {
        dtMtgGrpLoanScheduleDao = CDFIApplication.database?.dtMtgGrpLoanScheduleDao()
    }

    fun getMtgGrpLoanScheduledata(cbo_id: Int): List<DtMtgGrpLoanScheduleEntity>? {
        return dtMtgGrpLoanScheduleDao!!.getMtgGrpLoanScheduledata(cbo_id)
    }
    fun getScheduleloanwise(mem_id: Long,loanno:Int): List<DtMtgGrpLoanScheduleEntity>? {
        return dtMtgGrpLoanScheduleDao!!.getScheduleloanwise(mem_id,loanno)
    }

    fun getGroupScheduledata(cboid: Long,mtgguid:String,loanno:Int): List<DtMtgGrpLoanScheduleEntity>? {
        return dtMtgGrpLoanScheduleDao!!.getGroupScheduledata(cboid,mtgguid,loanno)
    }
    fun getremaininginstallment(loanno: Int,cbo_id: Long): Int {
        return dtMtgGrpLoanScheduleDao!!.getremaininginstallment(loanno,cbo_id)
    }


    fun getprincipaldemand(loanno: Int,cbo_id: Long,currentmtgdate:Long,lastmtgdate:Long): Int {
        return dtMtgGrpLoanScheduleDao!!.getprincipaldemand(loanno,cbo_id,currentmtgdate,lastmtgdate)
    }

    internal fun gettotaloutstanding(loanno: Int, cboid: Long): Int {
        return dtMtgGrpLoanScheduleDao!!.gettotaloutstanding(loanno, cboid)
    }


    fun insert(dtMtgGrpLoanScheduleEntity: DtMtgGrpLoanScheduleEntity) {
        insertAsyncTask(dtMtgGrpLoanScheduleEntity, dtMtgGrpLoanScheduleDao!!).execute()
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var dtMtgGrpLoanScheduleEntity: DtMtgGrpLoanScheduleEntity? = null
        var dtMtgGrpLoanScheduleDao: DtMtgGrpLoanScheduleDao? = null

        constructor (
            dtMtgGrpLoanScheduleEntity: DtMtgGrpLoanScheduleEntity?,
            dtMtgGrpLoanScheduleDao: DtMtgGrpLoanScheduleDao
        ) : this() {
            this.dtMtgGrpLoanScheduleEntity = dtMtgGrpLoanScheduleEntity
            this.dtMtgGrpLoanScheduleDao = dtMtgGrpLoanScheduleDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            dtMtgGrpLoanScheduleDao!!.insertMtgGrpLoanScheduleData(dtMtgGrpLoanScheduleEntity!!)
            return null
        }
    }

    fun deleteRecord(cbo_id: Int) {
        dtMtgGrpLoanScheduleDao!!.deleteRecord(cbo_id)
    }


    fun updateLoanGroupSchedule(
        mtgguid: String,
        cboid: Long,
        loanno: Int,
        installmentno: Int,
        principaldemand: Int,
        loandemandos: Int,
        loanos: Int


    ) {
        dtMtgGrpLoanScheduleDao!!.updateLoanGroupSchedule(
            mtgguid,
            cboid,
            loanno,
            installmentno,
            principaldemand,
            loandemandos,
            loanos

        )
    }
    fun updateMtgGrpLoanSchedule(
        cbo_id: Long,
        loan_no: Int,
        principal_demand: Int,
        loan_demand_os: Int,
        loan_os: Int,
        loan_paid: Int,
        installment_no: Int,
        sub_installment_no: Int,
        installment_date: Long,
        loan_date: Long,
        repaid: Int,
        lastpaid_date: Long,
        mtg_no: Int,
        createdby: String,
        createdon: Long,
        updatedby: String,
        updatedon: Long,
        uploaded_by: String
    ) {
        dtMtgGrpLoanScheduleDao!!.updateMtgGrpLoanSchedule(
            cbo_id,
            loan_no,
            principal_demand,
            loan_demand_os,
            loan_os,
            loan_paid,
            installment_no,
            sub_installment_no,
            installment_date,
            loan_date,
            repaid,
            lastpaid_date,
            mtg_no,
            createdby,
            createdon,
            updatedby,
            updatedon,
            uploaded_by
        )
    }
    fun updateCutOffLoanSchedule(
        mtgguid: String,
        cbo_id: Long,
        loanno: Int,
        installmentno: Int,
        principaldemand: Int
    ) {
        dtMtgGrpLoanScheduleDao!!.updateCutOffLoanSchedule(
            mtgguid,
            cbo_id,
            loanno,
            installmentno,
            principaldemand

        )
    }

    fun getPrincipalDemandByInstallmentNum(cbo_id: Long,mtg_guid: String,loan_no: Int,installment_no:Int):Int{
        return  dtMtgGrpLoanScheduleDao!!.getPrincipalDemandByInstallmentNum(cbo_id,mtg_guid,loan_no,installment_no)
    }

    fun updaterepaid(
        loanno: Int,
        cbo_id: Long,
        installmentno: Int,
        sub_installment_no: Int,
        loanpaid: Int,
        mtgdate: Long ,
        mtgno:Int,
        updatedby: String?,
        updatedon: Long)
    {
        dtMtgGrpLoanScheduleDao!!. updaterepaid(
            loanno,
            cbo_id,
            installmentno,
            sub_installment_no,
            loanpaid,
            mtgdate ,mtgno,updatedby,updatedon  )

    }
    fun deleterepaid(
        loanno: Int,
        cbo_id: Long,
        mtgdate: Long,
        mtgno:Int)
    {
        dtMtgGrpLoanScheduleDao!!. deleterepaid(
            loanno,
            cbo_id,
            mtgdate,mtgno   )

    }
    fun deletsubinstallment(
        loanno: Int,
        cbo_id: Long,
        mtgdate: Long,
        mtgno:Int)
    {
        dtMtgGrpLoanScheduleDao!!. deletsubinstallment(
            loanno,
            cbo_id,
            mtgdate,mtgno   )

    }

    fun getUploadListData(cbo_id: Long,loanno:Int) : List<DtMtgGrpLoanScheduleEntity>{
        return dtMtgGrpLoanScheduleDao!!.getUploadListData(cbo_id,loanno)
    }
    fun getGrpScheduleloanwise(
        cboId: Long,
        loanno: Int,
        mtgno: Int
    ): List<DtMtgGrpLoanScheduleEntity>?{
        return dtMtgGrpLoanScheduleDao!!.getGrpScheduleloanwise(cboId, loanno, mtgno)
    }

    fun getnextdemand(cboid: Long,loanno:Int): Int {
        return dtMtgGrpLoanScheduleDao!!.getnextdemand(cboid,loanno)
    }
}