package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.api.vomodel.VoECMemberDataModel
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.ExecutiveMemberDao
import com.microware.cdfi.dao.VOShgMemberDao
import com.microware.cdfi.entity.Executive_memberEntity

class ExecutiveMemberRepository {

    var executiveDao: ExecutiveMemberDao? = null
    var memberDao: VOShgMemberDao? = null

    constructor(application: Application) {
        executiveDao = CDFIApplication.database?.executiveDao()
        memberDao = CDFIApplication.database?.voShgMemberDao()
    }

    fun insert(executiveEntity: Executive_memberEntity) {
        insertAsyncTask(executiveEntity, executiveDao!!).execute()
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var executiveEntity: Executive_memberEntity? = null
        var executiveDao: ExecutiveMemberDao? = null

        constructor (
            executiveEntity: Executive_memberEntity?,
            executiveDao: ExecutiveMemberDao
        ) : this() {
            this.executiveEntity = executiveEntity
            this.executiveDao = executiveDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            executiveDao!!.insertExectiveData(executiveEntity)
            return null
        }

    }

    fun updateExecutiveMember(
        cbo_id: Long?,
        cbo_guid: String?,
        guid: String,
        ec_cbo_code: Long?,
        ec_cbo_id: Long?,
        ec_member_code: Long?,
        designation: Int?,
        joining_date: Long?,
        leaving_date: Long?,
        status: Int?,
        is_active: Int?,
        entry_source: Int?,
        is_edited: Int?,
        updated_date: Long?,
        updated_by: String?,
        is_complete: Int,
        isSignatory:Int?,
        signatory_joining_date:Long?,
        signatory_leaving_date:Long?
    ) {
        executiveDao!!.updateExecutiveMember(
            cbo_id,
            cbo_guid,
            guid,
            ec_cbo_code,
            ec_cbo_id,
            ec_member_code,
            designation,
            joining_date,
            leaving_date,
            status,
            is_active,
            entry_source,
            is_edited,
            updated_date,
            updated_by,
            is_complete,
            isSignatory,
            signatory_joining_date,
            signatory_leaving_date
        )
    }

    fun getExecutivedetaildata(guid: String?): List<Executive_memberEntity>? {
        return executiveDao!!.getExecutivedetaildata(guid)
    }

    fun getExecutiveList(cbo_guid: String?): LiveData<List<Executive_memberEntity>>? {
        return executiveDao!!.getExecutiveList(cbo_guid)
    }

    fun getExecutive_count(cbo_guid: String, ec_cbo_code: Long): Int {
        return executiveDao!!.getExecutive_count(cbo_guid, ec_cbo_code)
    }

    fun getExecutiveMemberbyCode(
        cbo_guid: String,
        ec_cbo_code: Long
    ): List<Executive_memberEntity>? {
        return executiveDao!!.getExecutiveMemberbyCode(cbo_guid, ec_cbo_code)
    }

    fun getECMember_count(cbo_guid: String, ec_member_code: Long): Int {
        return executiveDao!!.getECMember_count(cbo_guid, ec_member_code)
    }

    fun getPendingExecutiveMember(): List<Executive_memberEntity>? {
        return executiveDao!!.getPendingExecutiveMember()
    }

    fun deleteRecord(ec_guid: String?) {
        executiveDao!!.deleteRecord(ec_guid)
    }

    fun deleteSCRecord(ec_guid: String?) {
        executiveDao!!.deleteSCRecord(ec_guid)
    }

    fun deleteEcMemeber(ec_guid: String?, updated_date: Long?,updated_by: String?) {
        executiveDao!!.deleteEcMemeber(ec_guid, updated_date,updated_by)
    }

    fun deleteScMemeber(ec_guid: String?, updated_date: Long?) {
        executiveDao!!.deleteScMemeber(ec_guid, updated_date)
    }

    fun getCboMemberCount(ec_cbo_code: Long): Int {
        return executiveDao!!.getCboMemberCount(ec_cbo_code)
    }

    fun getDesignationCount(designation: Int, fedrationGuid: String): Int {
        return executiveDao!!.getDesignationCount(designation, fedrationGuid)
    }

    fun getMemberJoiningDate(member_code: Long): Long {
        return memberDao!!.getMemberJoiningDate(member_code)
    }

    fun getScMember_count(memberGuid: String, ec_member_code: Long): Int {
        return executiveDao!!.getScMember_count(memberGuid, ec_member_code)
    }

    fun updateEcuploaddata(
        lastupdatedate: Long,
        ec_guid: String
    ) {
        executiveDao!!.updateEcuploaddata(lastupdatedate, ec_guid)
    }

    fun deleteVOMappingdata(federationGuid:String) {
        executiveDao!!.deleteVOMappingdata(federationGuid)
    }

    fun deleteVOShgMember(federationGuid:String) {
        executiveDao!!.deleteVOShgMember(federationGuid)
    }

    fun deleteVOSHGMemberPhone(federationGuid:String) {
        executiveDao!!.deleteVOSHGMemberPhone(federationGuid)
    }

    fun deleteClfMappingdata(federationGuid:String) {
        executiveDao!!.deleteClfMappingdata(federationGuid)
    }

    fun deleteClfVOMember(federationGuid:String) {
        executiveDao!!.deleteClfVOMember(federationGuid)
    }

    fun deleteClfVOMemberPhone(federationGuid:String) {
        executiveDao!!.deleteClfVOMemberPhone(federationGuid)
    }

    internal fun getDistinctCboCount(cbo_id: Long?):Int{
        return executiveDao!!.getDistinctCboCount(cbo_id)
    }

    internal fun getOBCount(designation:List<Int>,fedrationGuid:String):Int{
        return executiveDao!!.getOBCount(designation,fedrationGuid)
    }
    fun getAllExecutivedetaildata1(
        ec_cbo_code: Long,
        mtgNo:Int,
        designation: List<Int>
    ): MutableList<VoECMemberDataModel>? {
        return executiveDao!!.getAllExecutivedetaildata1(ec_cbo_code,mtgNo, designation)
    }
}