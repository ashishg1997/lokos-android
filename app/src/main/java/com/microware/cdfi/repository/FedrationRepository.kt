package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.api.model.ParameterUploadModel
import com.microware.cdfi.api.response.MappingResponse
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.ExecutiveMemberDao
import com.microware.cdfi.dao.FedrationDao
import com.microware.cdfi.entity.FederationEntity
import com.microware.cdfi.entity.SHGEntity
import okhttp3.MediaType
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FedrationRepository {

    var fedrationDao : FedrationDao? = null
    var executiveDao: ExecutiveMemberDao? = null

    constructor(application: Application){
        fedrationDao = CDFIApplication.database?.fedrationDao()
        executiveDao = CDFIApplication.database?.executiveDao()
    }

    fun getFedrationDetaildata(federation_guid: String?): LiveData<List<FederationEntity>>?{
        return fedrationDao!!.getFedrationDetaildata(federation_guid)
    }

    internal fun getFedrationdata(fedrationGUID: String?): List<FederationEntity>?{
        return fedrationDao!!.getFedrationdata(fedrationGUID)
    }

    fun insert(federationEntity : FederationEntity){
        insertAsyncTask(federationEntity,fedrationDao!!).execute()
    }

    internal fun getVoCount(cboType:Int):Int{
        return fedrationDao!!.getVoCount(cboType)
    }

    class insertAsyncTask(): AsyncTask<Void, Void, String>(){

        private var federationEntity: FederationEntity? = null
        var fedrationDao: FedrationDao? = null

        constructor (federationEntity: FederationEntity?, fedrationDao: FedrationDao) : this() {
            this.federationEntity = federationEntity
            this.fedrationDao = fedrationDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            fedrationDao!!.insertFedrationDetailData(federationEntity)
            return null
        }

    }


    fun updateBasicDetail(
        federation_guid: String,
        panchayat_id: Int?,
        village_id: Int?,
        federation_name: String,
        federation_name_hindi: String,
        federation_name_local: String,
        federation_name_short: String,
        federation_formation_date: Long,
        federation_revival_date: Long,
        meeting_frequency: Int,
        meeting_frequency_value: Int,
        meeting_on: Int,
        month_comp_saving: Int,
        is_bankaccount: Int,
        parent_cbo_code: Int,
        parent_cbo_type: Int,
        is_active: Int,
        dedupl_status: Int,
        device:Int,
        activationStatus:Int,
        updated_date: Long,
        updated_by: String,
        saving_frequency:Int,
        is_volutary_saving: Int?,
        savings_interest: Double,
        voluntary_savings_interest: Double,
        primary_activity: Int,
        secondary_activity: Int,
        tertiary_activity: Int,
        bookkeeper_identified: Int,
        bookkeeper_name: String,
        bookkeeper_mobile: String,
        election_tenure: Int,
        status: Int,
        is_financial_intermediation: Int,
        meber_cbo_count: Int,
        cooption_date:Long,
        promotedby:Int,
        promoter_code:Int,
        promoter_name:String,
        user_id:String,
        is_completed:Int,
        is_edited:Int,
        approve_status:Int,
        imageName: String
    ) {
        fedrationDao!!.updateBasicDetail(
            federation_guid,
            panchayat_id,
            village_id,
            federation_name,
            federation_name_hindi,
            federation_name_local,
            federation_name_short, federation_formation_date, federation_revival_date, meeting_frequency,
            meeting_frequency_value,meeting_on,month_comp_saving,is_bankaccount,parent_cbo_code,
            parent_cbo_type,is_active,dedupl_status,device,activationStatus,
            updated_date,updated_by,saving_frequency,
            is_volutary_saving,
            savings_interest,
            voluntary_savings_interest,
            primary_activity,
            secondary_activity,
            tertiary_activity,
            bookkeeper_identified,
            bookkeeper_name,
            bookkeeper_mobile,
            election_tenure,status,
            is_financial_intermediation,
            meber_cbo_count,cooption_date,promotedby,promoter_code,promoter_name,user_id,is_completed,is_edited,approve_status,imageName
        )
    }

    internal fun getAllFederationdata(cboType:Int): List<FederationEntity>? {
        return fedrationDao!!.getAllFederationdata(cboType)
    }

    internal fun getFederation_by_village(village_id:Int,cboType: Int): LiveData<List<FederationEntity>>? {
        return fedrationDao!!.getFederation_by_village(village_id,cboType)
    }
    internal fun getFedrationAlldata(cboType:Int): LiveData<List<FederationEntity>>? {
        return fedrationDao!!.getFedrationAlldata(cboType)
    }

    internal fun getallVoCount():Int{
        return fedrationDao!!.getallVoCount()
    }
    internal fun getallVoPendingCount():Int{
        return fedrationDao!!.getallVoPendingCount()
    }
    internal fun getallVoActiveCount():Int{
        return fedrationDao!!.getallVoActiveCount()
    }

    internal fun getcount(federation_guid: String?): Int {
        return executiveDao?.getcount(federation_guid)!!
    }

    internal fun updateisedit(guid: String ) {
        return fedrationDao?.updateisedit(guid)!!
    }

    internal   fun updatededup(
        guid: String ){
        fedrationDao!!.updatededup(guid)
    }

    internal fun getIsCompleteValue(guid:String?):Int{
        return fedrationDao!!.getIsCompleteValue(guid)
    }

    internal fun getPhoneCount(guid:String?):Int{
        return fedrationDao!!.getPhoneCount(guid)
    }

    internal fun getEcMemberCount(guid:String?):Int{
        return fedrationDao!!.getEcMemberCount(guid)
    }

    internal fun getBankCount(guid:String?):Int{
        return fedrationDao!!.getBankCount(guid)
    }

    internal fun getAddressCount(guid:String?):Int{
        return fedrationDao!!.getAddressCount(guid)
    }

    internal fun getScCount(guid:String?):Int{
        return fedrationDao!!.getScCount(guid)
    }

    internal fun getKycCount(guid:String?):Int{
        return fedrationDao!!.getKycCount(guid)
    }

    internal fun getMappedShgCount(guid:String?):Int{
        return fedrationDao!!.getMappedShgCount(guid)
    }

    internal fun getMappedVoCount(guid:String?):Int{
        return fedrationDao!!.getMappedVoCount(guid)
    }
    internal fun updateFedrationDedupStatus(
        guid: String,
        code: String,
        activationStatus: Int,
        approve_status: Int,
        checker_remarks: String



    ){
        fedrationDao!!.updateFedrationDedupStatus(
            guid,
            code,
            activationStatus,
            approve_status,
            checker_remarks

        )
    }
    fun updateFedrationEditFlag(is_edited: Int,guid: String?){
        fedrationDao!!.updateFedrationEditFlag(is_edited,guid)
    }
    fun getlastdate(): Long{
        return fedrationDao!!.getlastdate()
    }

    internal fun getVoPendingCount(cboType:Int):Int{
        return fedrationDao!!.getVoPendingCount(cboType)
    }
    internal fun getVoActiveCount(cboType:Int):Int{
        return fedrationDao!!.getVoActiveCount(cboType)
    }

    fun updateFedrationCode(cbo_id: Long,guid: String?){
        return fedrationDao!!.updateFedrationCode(cbo_id,guid)
    }

    internal fun deleteFederationByGuid(federationGUID: String?){
        return fedrationDao!!.deleteFederationByGuid(federationGUID)
    }

    internal fun deleteBankByfederationGUID(federationGUID: String?){
        return fedrationDao!!.deleteBankByfederationGUID(federationGUID)
    }

    internal fun deletePhoneByfederationGUID(federationGUID: String?){
        return fedrationDao!!.deletePhoneByfederationGUID(federationGUID)
    }

    internal fun deleteAddressByfederationGUID(federationGUID: String?){
        return fedrationDao!!.deleteAddressByfederationGUID(federationGUID)
    }

    internal fun deleteEcByfederationGUID(federationGUID: String?){
        return fedrationDao!!.deleteEcByfederationGUID(federationGUID)
    }

    internal fun deleteScByfederationGUID(federationGUID: String?){
        return fedrationDao!!.deleteScByfederationGUID(federationGUID)
    }

    internal fun deleteScMemberByfederationGUID(federationGUID: String?){
        return fedrationDao!!.deleteScMemberByfederationGUID(federationGUID)
    }

    internal fun deleteFederationDataByGuid(federationGUID: String?){
        return fedrationDao!!.deleteFederationDataByGuid(federationGUID)
    }

    internal fun deleteBankDataByfederationGUID(federationGUID: String?){
        return fedrationDao!!.deleteBankDataByfederationGUID(federationGUID)
    }

    internal fun deletePhoneDataByfederationGUID(federationGUID: String?){
        return fedrationDao!!.deletePhoneDataByfederationGUID(federationGUID)
    }

    internal fun deleteAddressDataByfederationGUID(federationGUID: String?){
        return fedrationDao!!.deleteAddressDataByfederationGUID(federationGUID)
    }

    internal fun deleteEcDataByfederationGUID(federationGUID: String?){
        return fedrationDao!!.deleteEcDataByfederationGUID(federationGUID)
    }

    internal fun deleteScDataByfederationGUID(federationGUID: String?){
        return fedrationDao!!.deleteScDataByfederationGUID(federationGUID)
    }

    internal fun deleteScMemberDataByfederationGUID(federationGUID: String?){
        return fedrationDao!!.deleteScMemberDataByfederationGUID(federationGUID)
    }

    internal fun updateFedrationId(guid: String?,federation_id:Long?){
        fedrationDao!!.updateFedrationId(guid,federation_id)
    }

    internal fun getEcCount(shg_code:Long,cbo_guid:String):Int{
        return fedrationDao!!.getEcCount(shg_code,cbo_guid)
    }

    internal  fun getIfscCode():List<String>{
        return fedrationDao!!.getIfscCode()
    }

    internal  fun getFI(federationGUID: String?):Int{
        return fedrationDao!!.getFI(federationGUID)
    }

    internal fun getSCMemberCount(federationGuid:String):Int{
        return fedrationDao!!.getSCMemberCount(federationGuid)
    }

    internal fun getSignatoryCount(cbo_guid:String):Int{
        return fedrationDao!!.getSignatoryCount(cbo_guid)
    }

    internal fun getFederation_by_PanchayatCode(panchayat_id:Int,cboType:Int): LiveData<List<FederationEntity>>?{
        return fedrationDao!!.getFederation_by_PanchayatCode(panchayat_id,cboType)
    }

    internal fun getClfDataByBlock(block_id:Int,cboType:Int): LiveData<List<FederationEntity>>?{
        return fedrationDao!!.getClfDataByBlock(block_id,cboType)
    }

    internal fun getVoPendingCount(panchayat_id:Int,cboType:Int):Int{
        return fedrationDao!!.getVoPendingCount(panchayat_id,cboType)
    }

    internal fun getCLFPendingCount(block_id:Int,cboType:Int):Int{
        return fedrationDao!!.getCLFPendingCount(block_id,cboType)
    }

    internal fun getVoCount(panchayat_id:Int,cboType:Int):Int{
        return fedrationDao!!.getVoCount(panchayat_id,cboType)
    }

    internal fun getCLFCount(block_id:Int,cboType:Int):Int{
        return fedrationDao!!.getCLFCount(block_id,cboType)
    }

    internal fun getVoActiveCount(panchayat_id:Int,cboType:Int):Int{
        return fedrationDao!!.getVoActiveCount(panchayat_id,cboType)
    }

    internal fun getCLFActiveCount(block_id:Int,cboType:Int):Int{
        return fedrationDao!!.getCLFActiveCount(block_id,cboType)
    }

}