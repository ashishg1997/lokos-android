package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.room.Query
import com.microware.cdfi.api.meetinguploadmodel.ShgFinanceTransactionDetailMemberData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.FinancialTransactionsMemDao
import com.microware.cdfi.entity.FinancialTransactionsMemEntity

class FinancialTransactionsMemRepository {

    var dtFinTxnDetDao: FinancialTransactionsMemDao? = null

    constructor(application: Application) {
        dtFinTxnDetDao = CDFIApplication.database?.dtFinTxnDetDao()
    }

    fun getMtgFinTxnDetdata(mtg_guid: String,cbo_id: Long,mem_id: Long,mtg_no: Int,auid:Int): List<FinancialTransactionsMemEntity>? {
        return dtFinTxnDetDao!!.getMtgFinTxndata(mtg_guid,cbo_id, mem_id, mtg_no,auid)
    }

    fun insert(dtFinTxnDetEntity: FinancialTransactionsMemEntity) {
        insertAsyncTask(dtFinTxnDetEntity, dtFinTxnDetDao!!).execute()
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var dtFinTxnDetEntity: FinancialTransactionsMemEntity? = null
        var dtFinTxnDetDao: FinancialTransactionsMemDao? = null

        constructor (
            dtFinTxnDetEntity: FinancialTransactionsMemEntity?,
            dtFinTxnDetDao: FinancialTransactionsMemDao
        ) : this() {
            this.dtFinTxnDetEntity = dtFinTxnDetEntity
            this.dtFinTxnDetDao = dtFinTxnDetDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            dtFinTxnDetDao!!.insertMtgFinTxnData(dtFinTxnDetEntity!!)
            return null
        }
    }

    fun deleteRecord(mtg_guid: String) {
        dtFinTxnDetDao!!.deleteRecord(mtg_guid)
    }

    fun updateShareCapitalDetail(
        mtg_guid: String,
        cbo_id: Long,
        mem_id: Long,
        mtg_no: Int,
        auid: Int,
        bank_code: String?,
        type: String?,
        amount: Int?,
        trans_date: Long?,
        date_realisation: Long?,
        mode_payment: Int?,
        transaction_no: String?,
        updatedby: String?,
        updatedon: Long?
    ) {
        dtFinTxnDetDao!!.updateShareCapitalDetail(
            mtg_guid,
            cbo_id,
            mem_id,
            mtg_no,
            auid,
            bank_code,
            type,
            amount,
            trans_date,
            date_realisation,
            mode_payment,
            transaction_no,
            updatedby,
            updatedon
        )
    }
    fun getIncomeAmount(cbo_id: Long,mtg_no: Int,modePayment:Int,type:String): Int {
        return dtFinTxnDetDao!!.getIncomeAmount(cbo_id, mtg_no, modePayment,type)

    }

    fun getMemberIncomeAmount(cbo_id: Long,mtg_no: Int,type:String): Int {
        return dtFinTxnDetDao!!.getMemberIncomeAmount(cbo_id, mtg_no,type)

    }

    fun getBankAmount(cbo_id: Long,mtg_no: Int,type:String): Int {
        return dtFinTxnDetDao!!.getBankAmount(cbo_id, mtg_no,type)

    }

    fun getBankCodeAmount(cbo_id: Long,mtg_no: Int,bank_code: String?,type:String): Int {
        return dtFinTxnDetDao!!.getBankCodeAmount(cbo_id, mtg_no,bank_code,type)

    }

    fun getFinancialTransactionsMemAmount(cbo_id: Long,mtg_no: Int,bank_code:String,type:String): Int{
        return dtFinTxnDetDao!!.getFinancialTransactionsMemAmount(cbo_id, mtg_no, bank_code,type)
    }

    fun getUploadData(mtg_no: Int,cbo_id: Long,mem_id: Long) : List<FinancialTransactionsMemEntity>{
        return dtFinTxnDetDao!!.getUploadData(mtg_no,cbo_id,mem_id)
    }


}