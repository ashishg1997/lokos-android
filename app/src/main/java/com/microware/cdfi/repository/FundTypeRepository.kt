package com.microware.cdfi.repository

import android.app.Application
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.FundTypeDao
import com.microware.cdfi.entity.FundTypeEntity

class FundTypeRepository {

    var fundtypeDao: FundTypeDao? = null

    constructor(application: Application) {
        fundtypeDao = CDFIApplication.database?.fundsDao()
    }

    internal fun getFundslist(langID : String):List<FundTypeEntity>?{
        return fundtypeDao!!.getFundslist(langID)
    }

    fun getFundName(fubdTypeID:Int,langID: String):String?{
        return fundtypeDao!!.getFundName(fubdTypeID,langID)
    }
}