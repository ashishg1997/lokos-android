package com.microware.cdfi.repository

import android.app.Application
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.FundingAgencyDao
import com.microware.cdfi.entity.FundingEntity

class FundingAgencyRepository {

    var fundingDao: FundingAgencyDao? = null

    constructor(application: Application) {
        fundingDao = CDFIApplication.database?.fundingDao()
    }

    internal fun getfundingAgency(languageId:String):List<FundingEntity>{
       return fundingDao!!.getfundingAgency(languageId)
    }
}