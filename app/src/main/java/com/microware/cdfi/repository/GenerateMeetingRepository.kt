package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import com.microware.cdfi.api.meetingmodel.LoanListModel
import com.microware.cdfi.api.meetingmodel.LoanRepaymentListModel
import com.microware.cdfi.api.meetingmodel.ShareCapitalModel
import com.microware.cdfi.api.model.SHGMeetingModel
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.*
import com.microware.cdfi.entity.*

class GenerateMeetingRepository {

    var lookupDao: LookupDao? = null
    var dtmtgDao: DtmtgDao? = null
    var dtdetmtgDao: DtdetmtgDao? = null
    var dtLoanMemberDao: DtLoanMemberDao? = null
    var dtLoanTxnMemDao: DtLoanTxnMemDao? = null
    var dtLoanGpTxnDao: DtLoanGpTxnDao? = null
    var dtLoanGpDao: DtLoanGpDao? = null
    var incomeandExpenditureDao: IncomeandExpenditureDao? = null
    var dtFinTxnDetDao: FinancialTransactionsMemDao? = null
    var dtMtgFinTxnDao: DtMtgFinTxnDao? = null
    var shgMcpDao: ShgMcpDao? = null
    var dtLoanApplicationDao: DtLoanApplicationDao? = null
    var dtLoanMemberScheduleDao: DtLoanMemberScheduleDao? = null
    var dtMtgGrpLoanScheduleDao: DtMtgGrpLoanScheduleDao? = null
    var memberDao: MemberDao? = null
    constructor(application: Application) {
        lookupDao = CDFIApplication.database?.lookupdao()
        dtmtgDao = CDFIApplication.database?.dtmtgDao()
        dtdetmtgDao = CDFIApplication.database?.dtdetmtgDao()
        dtLoanMemberDao = CDFIApplication.database?.dtLoanMemberDao()
        dtLoanTxnMemDao = CDFIApplication.database?.dtLoanTxnMemDao()
        dtFinTxnDetDao = CDFIApplication.database?.dtFinTxnDetDao()
        incomeandExpenditureDao = CDFIApplication.database?.incomeandExpendatureDao()
        dtLoanGpTxnDao = CDFIApplication.database?.dtLoanGpTxnDao()
        dtLoanGpDao = CDFIApplication.database?.dtLoanGpDao()
        dtMtgFinTxnDao = CDFIApplication.database?.dtMtgFinTxnDao()
        shgMcpDao = CDFIApplication.database?.shgmcpDao()
        dtLoanApplicationDao = CDFIApplication.database?.dtLoanApplicationDao()
        dtLoanMemberScheduleDao = CDFIApplication.database?.dtLoanMemberScheduleDao()
        dtMtgGrpLoanScheduleDao = CDFIApplication.database?.dtMtgGrpLoanScheduleDao()
        memberDao = CDFIApplication.database?.memberDao()

    }

    fun insertlookup(lookup: LookupEntity) {
        return lookupDao!!.insertLookupData(lookup)
    }

    fun getlookupCount(id: Int): Int {
        return lookupDao!!.getlookupCount(id)
    }

    fun getmeetingCount(cbo_id: String): Int {
        return dtmtgDao!!.getmeetingCount(cbo_id)
    }

    fun gettotmeetingCount(): Int {
        return dtmtgDao!!.gettotmeetingCount()
    }

    fun getupdatedpeningcash(mtgno: Int, cbo_id: Long): Int {
        return dtmtgDao!!.getupdatedpeningcash(mtgno, cbo_id)
    }

    fun gettotloan(mtgno: Int, cbo_id: Long): Int {
        return dtLoanMemberDao!!.gettotloan(mtgno, cbo_id)
    }

    fun gettotalMemberLoan(mtgno: Int, cbo_id: Long): Int {
        return dtLoanMemberDao!!.gettotalMemberLoan(mtgno, cbo_id)
    }

    fun gettotloanpaid(mtgno: Int, cbo_id: Long): Int {
        return dtLoanTxnMemDao!!.gettotloanpaid(mtgno, cbo_id)
    }
    fun getmemtotloanpaidinbank(mtgno:Int,cbo_id:Long,account:String): Int {
        return dtLoanTxnMemDao!!.getmemtotloanpaidinbank(mtgno, cbo_id,account)
    }
    fun gettotloangroup(mtgno:Int,cbo_id:Long):Int  {
        return dtLoanGpDao!!.gettotloangroup(mtgno,cbo_id)
    }
    fun getTotalGrouploan(mtgno:Int,cbo_id:Long):Int  {
        return dtLoanGpDao!!.getTotalGrouploan(mtgno,cbo_id)
    }

    fun getTotalLoans(mtgno:Int,cbo_id:Long): Int{
        return dtLoanGpDao!!.getTotalLoans(mtgno,cbo_id)
    }

    fun getTotalCutOffLoanReceived(mtgno:Int,cbo_id:Long): Int{
        return dtLoanGpDao!!.getTotalCutOffLoanReceived(mtgno,cbo_id)
    }
    fun getgrptotloanpaidinbank(mtgno:Int,cbo_id:Long,account:String): Int {
        return dtLoanGpTxnDao!!.getgrptotloanpaidinbank(mtgno, cbo_id,account)
    }

    fun getgrouptotloanamtinbank(mtgno:Int,cbo_id:Long, code: String):Int  {
        return dtLoanGpDao!!.getgrouptotloanamtinbank(mtgno,cbo_id,code)
    }
    fun gettotloanpaidgroup(mtgno: Int, cbo_id: Long): Int {
        return dtLoanGpTxnDao!!.gettotloanpaidgroup(mtgno, cbo_id)
    }
    fun gettotalinbank(mtgno: Int, cbo_id: Long): Int {
        return dtMtgFinTxnDao!!.gettotalinbank(mtgno, cbo_id)
    }
    fun getopenningbal(mtgno: Int, cbo_id: Long,accountno: String): Int {
        return dtMtgFinTxnDao!!.getopenningbal(mtgno, cbo_id,accountno)
    }
    fun getclosingbal(mtgno: Int, cbo_id: Long,accountno: String): Int {
        return dtMtgFinTxnDao!!.getclosingbal(mtgno, cbo_id,accountno)
    }
    fun getMtg(cbo_id: String?): List<DtmtgEntity>? {
        return dtmtgDao!!.getMtg(cbo_id)
    }

    fun getMtglist(cbo_id: Long?): List<DtmtgEntity>? {
        return dtmtgDao!!.getMtglist(cbo_id)
    }

    fun getMtglistdata(cbo_id: Long?): List<DtmtgEntity>? {
        return dtmtgDao!!.getMtglistdata(cbo_id)
    }
    fun returnmeetingstatus(shgid: Long,Mtgno:Int): String {
        return dtmtgDao!!.returnmeetingstatus(shgid,Mtgno)
    }

    fun getgroupMeetingsdata(shg_code: String?): List<SHGMeetingModel>? {
        return dtmtgDao!!.getgroupMeetingsdata(shg_code)
    }

    fun getgroupMeetingsdatalist(): List<SHGMeetingModel>? {
        return dtmtgDao!!.getgroupMeetingsdatalist()
    }

    fun getgroupMeetingsdatalistbyvillage(villageid: Int?): List<SHGMeetingModel>? {
        return dtmtgDao!!.getgroupMeetingsdatalistbyvillage(villageid)
    }

    fun insert(dtmtgEntity: DtmtgEntity) {
        insertAsyncTask(dtmtgEntity, dtmtgDao!!).execute()
    }

    fun insertdtmtgDet(dtmtgDetEntity: DtmtgDetEntity) {
        insertAsyncTaskdtmtgDet(dtmtgDetEntity, dtdetmtgDao!!).execute()
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var dtmtgEntity: DtmtgEntity? = null
        var dtmtgDao: DtmtgDao? = null

        constructor (dtmtgEntity: DtmtgEntity?, dtmtgDao: DtmtgDao?) : this() {
            this.dtmtgEntity = dtmtgEntity
            this.dtmtgDao = dtmtgDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            dtmtgDao!!.insertDtmtg(dtmtgEntity)
            return null
        }

    }

    class insertAsyncTaskdtmtgDet() : AsyncTask<Void, Void, String>() {

        private var dtmtgDetEntity: DtmtgDetEntity? = null
        var dtdetmtgDao: DtdetmtgDao? = null

        constructor (dtmtgDetEntity: DtmtgDetEntity?, dtdetmtgDao: DtdetmtgDao?) : this() {
            this.dtmtgDetEntity = dtmtgDetEntity
            this.dtdetmtgDao = dtdetmtgDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            dtdetmtgDao!!.insertDtmtgDet(dtmtgDetEntity)
            return null
        }

    }

    internal fun getMtgByMtgnum(shg_id: Long?, mtgnum: Int): List<DtmtgDetEntity> {
        return dtdetmtgDao!!.getMtgByMtgnum(shg_id, mtgnum)
    }

    internal fun getListDataMtgByMtgnum(): List<DtmtgDetEntity> {
        return dtdetmtgDao!!.getListDataMtgByMtgnum()
    }

    internal fun getListDataMtgByMtgnum(mtgno: Int, cbo_id: Long): List<DtmtgDetEntity> {
        return dtdetmtgDao!!.getListDataMtgByMtgnum(mtgno, cbo_id)
    }

    internal fun getloanListDataMtgByMtgnum(mtgno: Int, cbo_id: Long): List<LoanListModel> {
        return dtdetmtgDao!!.getloanListDataMtgByMtgnum(mtgno, cbo_id)
    }

    internal fun getloanrepaymentList(mtgno: Int, cbo_id: Long): List<LoanRepaymentListModel> {
        return dtdetmtgDao!!.getloanrepaymentList(mtgno, cbo_id)
    }

    internal fun gettotcountnotsaved(mtgno: Int, cbo_id: Long): Int {
        return dtdetmtgDao!!.gettotcountnotsaved(mtgno, cbo_id)
    }

    internal fun getupdatedcash(mtgno: Int, cbo_id: Long): Int {
        return dtdetmtgDao!!.getupdatedcash(mtgno, cbo_id)
    }

    fun updateAttendance(
        attendance: String,
        mtgno: Int,
        cbo_id: Long,
        group_m_code: Long,
        updated_by:String,
        updated_on:Long
    ) {
        dtdetmtgDao!!.updateAttendance(
            attendance,
            mtgno,
            cbo_id,
            group_m_code,
            updated_by,
            updated_on
        )
    }


    internal fun getTotalPresentAndAbsent(attendance: String, mtgno: Int, cbo_id: Long): Int {
        return dtdetmtgDao!!.getTotalPresentAndAbsent(attendance, mtgno, cbo_id)
    }

    fun updateCompulsorySaving(
        sav_comp: Int,
        sav_comp_cb: Int,
        mtgno: Int,
        cbo_id: Long,
        group_m_code: Long,
        updated_by:String,
        updated_on:Long
    ) {
        dtdetmtgDao!!.updateCompulsorySaving(
            sav_comp,
            sav_comp_cb,
            mtgno,
            cbo_id,
            group_m_code,
            updated_by,
            updated_on
        )
    }

    fun updateVoluntorySaving(
        sav_vol: Int,
        sav_vol_cb: Int,
        mtgno: Int,
        cbo_id: Long,
        group_m_code: Long,
        updated_by:String,
        updated_on:Long
    ) {
        dtdetmtgDao!!.updateVoluntorySaving(
            sav_vol,
            sav_vol_cb,
            mtgno,
            cbo_id,
            group_m_code,
            updated_by,
            updated_on
        )
    }

    fun updatePenaltySaving(
        penalty: Int,
        mtgno: Int,
        cbo_id: Long,
        group_m_code: Long,
        updated_by:String,
        updated_on:Long
    ) {
        dtdetmtgDao!!.updatePenaltySaving(
            penalty,
            mtgno,
            cbo_id,
            group_m_code,
            updated_by,
            updated_on
        )
    }

    fun updateclosing(mem_id: Long, mtgno: Int, sav_comp_cb: Int, sav_vol_cb: Int) {
        dtdetmtgDao!!.updateclosing(mem_id, mtgno, sav_comp_cb, sav_vol_cb)
    }

    fun updateloandata1(mem_id: Long, mtgno: Int, loanno: Int, loanop: Int, loan_int_accrued: Int) {
        dtdetmtgDao!!.updateloandata1(mem_id, mtgno, loanno, loanop,loan_int_accrued)
    }

    fun updateloandata2(mem_id: Long, mtgno: Int, loanno: Int, loanop: Int, loan_int_accrued: Int) {
        dtdetmtgDao!!.updateloandata2(mem_id, mtgno, loanno, loanop,loan_int_accrued)
    }

    fun updateloandata3(mem_id: Long, mtgno: Int, loanno: Int, loanop: Int, loan_int_accrued: Int) {
        dtdetmtgDao!!.updateloandata3(mem_id, mtgno, loanno, loanop,loan_int_accrued)
    }

    fun updateloandata4(mem_id: Long, mtgno: Int, loanno: Int, loanop: Int, loan_int_accrued: Int) {
        dtdetmtgDao!!.updateloandata4(mem_id, mtgno, loanno, loanop,loan_int_accrued)
    }

    fun updateloandata5(mem_id: Long, mtgno: Int, loanno: Int, loanop: Int, loan_int_accrued: Int) {
        dtdetmtgDao!!.updateloandata5(mem_id, mtgno, loanno, loanop,loan_int_accrued)
    }




    internal fun getsumcomp(mtgno: Int, cbo_id: Long): Int {
        return dtdetmtgDao!!.getsumcomp(mtgno, cbo_id)
    }

    internal fun getsumvol(mtgno: Int, cbo_id: Long): Int {
        return dtdetmtgDao!!.getsumvol(mtgno, cbo_id)
    }
    internal fun getsumwithdrwal(mtgno: Int, cbo_id: Long): Int {
        return dtdetmtgDao!!.getsumwithdrwal(mtgno, cbo_id)
    }

    internal fun getsumattendance(mtgno: Int, cbo_id: Long): Int {
        return dtdetmtgDao!!.getsumattendance(mtgno, cbo_id)
    }

    internal fun getCurrentMeetingslist(c_date: Long): List<SHGMeetingModel>? {
        return dtmtgDao!!.getCurrentMeetingslist(c_date)
    }

    internal fun getMissedMeetingslist(c_date: Long): List<SHGMeetingModel>? {
        return dtmtgDao!!.getMissedMeetingslist(c_date)
    }

    internal fun getUpcomingMeetingslist(c_date: Long): List<SHGMeetingModel>? {
        return dtmtgDao!!.getUpcomingMeetingslist(c_date)
    }

    internal fun getBankdata(cbo_guid: String?): List<Cbo_bankEntity> {
        return dtmtgDao!!.getBankdata(cbo_guid)
    }

    internal fun getCapitalListDataMtgByMtgnum(mtgno: Int, cbo_id: Long,type:String): List<ShareCapitalModel> {
        return dtdetmtgDao!!.getCapitalListDataMtgByMtgnum(mtgno, cbo_id,type)
    }

    fun getCutOffTotalPresent(mtgno: Int, cbo_id: Long): Int {
        return dtdetmtgDao!!.getCutOffTotalPresent(mtgno, cbo_id)
    }

    fun getMaxMeetingNum(shg_id: Long?): Int {
        return dtmtgDao!!.getMaxMeetingNum(shg_id)
    }

    fun getMeetingCount(shg_id: Long?): Int {
        return dtmtgDao!!.getMeetingCount(shg_id)
    }

    fun getShareCapitalListByReceiptType(
        mtgno: Int,
        cbo_id: Long,
        receiptType: Int
    ): List<ShareCapitalModel> {
        return dtdetmtgDao!!.getShareCapitalListByReceiptType(mtgno, cbo_id, receiptType)
    }

    fun getTotalAmountByReceiptType(mtg_guid: String, cbo_id: Long, mtg_no: Int, auid: Int): Int {
        return dtFinTxnDetDao!!.getTotalAmountByReceiptType(mtg_guid, cbo_id, mtg_no, auid)
    }

    fun getTotalIncomeAndExpenditureByFundType(
        mtg_guid: String,
        cbo_id: Long,
        mtg_no: Int,
        fund_type: Int
    ): Int {
        return incomeandExpenditureDao!!.getTotalIncomeAndExpenditureByFundType(
            mtg_guid,
            cbo_id,
            mtg_no,
            fund_type
        )
    }

    fun getSecondLastMeetingNum(shg_id: Long, mtg_num: Int): Int {
        return dtmtgDao!!.getSecondLastMeetingNum(shg_id, mtg_num)
    }


    fun getopeningcash(mtgno: Int, cbo_id: Long): Int {
        return dtdetmtgDao!!.getopeningcash(mtgno, cbo_id)
    }

    internal fun getTotalMember(mtgno: Int, cbo_id: Long): Int {
        return dtdetmtgDao!!.getTotalMember(mtgno, cbo_id)
    }

    fun getCompulsoryData(mtgno: Int, cbo_id: Long, mem_id: Long): List<DtmtgDetEntity> {
        return dtdetmtgDao!!.getCompulsoryData(mtgno, cbo_id, mem_id)
    }

    fun updateCutOffAttendance(
        attendance: String,
        mtgno: Int,
        cbo_id: Long,
        group_m_code: Long,
        updated_by:String,
        updated_on:Long
    ) {
        dtdetmtgDao!!.updateCutOffAttendance(
            attendance,
            mtgno,
            cbo_id,
            group_m_code,
            updated_by,
            updated_on
        )
    }

    fun updateCutOffSaving(
        compulsaryAmount: Int,
        voluntaryAmount: Int,
        mtgno: Int,
        cbo_id: Long,
        group_m_code: Long,
        updated_by:String,
        updated_on:Long
    ){
        dtdetmtgDao!!.updateCutOffSaving(
            compulsaryAmount,
            voluntaryAmount,
            mtgno,
            cbo_id,
            group_m_code,
            updated_by,
            updated_on
        )
    }
    fun updateclosingcash(closingcash: Int,mtgno:Int,cbo_id:Long) {
        dtmtgDao!!.updateclosingcash(closingcash, mtgno, cbo_id)
    }

    fun getCutOffmaxLoanno(cbo_id: Long,mtgNo:Int): Int{
        return dtLoanGpDao!!.getCutOffmaxLoanno(cbo_id,mtgNo)
    }
    fun updateclosingbalbank(closingbal: Int,mtgno:Int,cbo_id:Long,accountno: String) {
        dtMtgFinTxnDao!!.updateclosingbalbank(closingbal, mtgno, cbo_id,accountno)
    }
    internal fun getCutOffloanListDataByMtgnum(mtgno: Int, cbo_id: Long,completionFlag:Boolean): List<LoanListModel> {
        return dtdetmtgDao!!.getCutOffloanListDataByMtgnum(mtgno, cbo_id,completionFlag)
    }
    internal fun getCutOffClosedMemberLoanDataByMtgNum(mtgno: Int, cbo_id: Long,completionFlag:Boolean): List<LoanListModel> {
        return dtdetmtgDao!!.getCutOffClosedMemberLoanDataByMtgNum(mtgno, cbo_id,completionFlag)
    }
    fun getShgMeetingList(): List<DtmtgEntity>{
        return dtmtgDao!!.getShgMeetingList()
    }

    fun getUploadListData(): List<DtmtgEntity>{
        return dtdetmtgDao!!.getUploadListData()
    }


    internal fun updateCutOffCash(
        cashInHand: Int,
        cashInTransit: Int,
        balanceDate: Long,
        closingcash: Int,
        mtgno:Int,
        cbo_id:Long,
        updatedOn:Long,
        updatedBy:String
    ){
        dtmtgDao!!.updateCutOffCash(
            cashInHand,
            cashInTransit,
            balanceDate,
            closingcash,
            mtgno,
            cbo_id,
            updatedOn,
            updatedBy
        )
    }

    internal fun getMeetingDetailData(mtgno:Int,cbo_id:Long):List<DtmtgEntity>{
        return  dtmtgDao!!.getMeetingDetailData(mtgno,cbo_id)
    }

    internal  fun updateCutOffBankBalance(
        cbo_id: Long?,
        mtgno: Int?,
        bankcode: String?,
        zero_mtg_cash_bank: Int?,
        cheque_issued_not_realized: Int?,
        cheque_received_not_credited: Int?,
        closingbalance: Int?,
        balance_date: Long?,
        updatedby: String,
        updatedon: Long?,
        uploaded_by: String?
    ){
        dtMtgFinTxnDao!!.updateCutOffBankBalance(
            cbo_id,
            mtgno,
            bankcode,
            zero_mtg_cash_bank,
            cheque_issued_not_realized,
            cheque_received_not_credited,
            closingbalance,
            balance_date,
            updatedby,
            updatedon,
            uploaded_by
        )
    }

    internal fun getBankListDataByMtgnum(mtgno:Int,cbo_id:Long): List<DtMtgFinTxnEntity>{
        return dtMtgFinTxnDao!!.getBankListDataByMtgnum(mtgno,cbo_id)
    }

    internal fun getCutOffMemberLoanAmount(cbo_id: Long,mtg_no: Int,completionFlag:Boolean):Int{
        return dtLoanMemberDao!!.getCutOffMemberLoanAmount(cbo_id,mtg_no,completionFlag)
    }

    fun closingMeeting(flag: String,mtgno:Int,cbo_id:Long,updatedOn:Long,updatedBy:String) {
        dtmtgDao!!.closingMeeting(flag, mtgno, cbo_id,updatedOn,updatedBy)
    }

    fun openMeeting(flag: String,mtgno:Int,cbo_id:Long) {
        dtmtgDao!!.openMeeting(flag, mtgno, cbo_id)
    }

    fun getTotalInvestmentByMtgNum(cbo_id: Long, mtg_no: Int,auid:List<Int>,type:String): Int{
        return incomeandExpenditureDao!!.getTotalInvestmentByMtgNum(cbo_id,mtg_no,auid,type)
    }

    internal fun getTotalBankLoanOutstanding(mtg_no:Int,cbo_id:Long,loan_source:Int):Int{
        return dtLoanGpDao!!.getTotalBankLoanOutstanding(mtg_no,cbo_id,loan_source)
    }

    internal fun getTotalLoanOutstandingByFundType(mtg_no:Int,cbo_id:Long,fund_type:Int):Int{
        return dtLoanGpDao!!.getTotalLoanOutstandingByFundType(mtg_no,cbo_id,fund_type)
    }

    internal fun getTotalInterestOverdue(mtg_no:Int,cbo_id:Long):Int{
        return dtLoanGpDao!!.getTotalInterestOverdue(mtg_no,cbo_id)
    }

    internal fun getTotalInvestmentByAuid( mtg_no: Int,cbo_id: Long,auid:Int,type:String): Int{
        return incomeandExpenditureDao!!.getTotalInvestmentByAuid( mtg_no,cbo_id,auid,type)
    }

    internal  fun getOtherLoanOutstanding(mtg_no:Int,cbo_id:Long):Int{
        return dtLoanGpDao!!.getOtherLoanOutstanding(mtg_no,cbo_id)
    }

    internal fun getTotalOutstandingMemberLoan(cbo_id: Long,mtg_no: Int, completionFlag: Boolean):Int{
        return dtLoanMemberDao!!.getTotalOutstandingMemberLoan(cbo_id,mtg_no,completionFlag)
    }

    internal fun getTotalMemberInterestOverdue(cbo_id: Long,mtg_no: Int):Int{
        return dtLoanMemberDao!!.getTotalMemberInterestOverdue(cbo_id,mtg_no)
    }

    fun getUploadListData1(mtg_no: Int,cbo_id: Long) : List<DtMtgFinTxnEntity>{
        return dtMtgFinTxnDao!!.getUploadListData(mtg_no,cbo_id)
    }

    fun getUploadListData(mtg_no:Int,cbo_id: Long): List<DtmtgDetEntity>{
        return dtdetmtgDao!!.getUploadListData(mtg_no,cbo_id)
    }

    fun getTotalPenaltyByMtgNum(mtgno: Int, cbo_id: Long): Int{
        return dtdetmtgDao!!.getTotalPenaltyByMtgNum(mtgno, cbo_id)
    }

    fun deleteShg_MtgData(mtg_no: Int,cbo_id: Long){
        dtmtgDao!!.deleteShg_MtgData(mtg_no,cbo_id)
    }
    fun deleteShg_MtgByCboId(cbo_id: Long){
        dtmtgDao!!.deleteShg_MtgByCboId(cbo_id)
    }

    fun deleteShgDetailData(mtg_no: Int,cbo_id: Long){
        dtdetmtgDao!!.deleteShgDetailData(mtg_no,cbo_id)
    }

    fun deleteShgDetailDataByCboId(cbo_id: Long){
        dtdetmtgDao!!.deleteShgDetailDataByCboId(cbo_id)
    }

    fun deleteFinancialTransactionMemberDetailData(mtg_no: Int,cbo_id: Long){
        dtFinTxnDetDao!!.deleteFinancialTransactionMemberDetailData(mtg_no,cbo_id)
    }

    fun deleteFinancialTransactionMemberDetailDataByCboId(cbo_id: Long){
        dtFinTxnDetDao!!.deleteFinancialTransactionMemberDetailDataByCboId(cbo_id)
    }

    fun deleteGrpFinancialTxnDetailData(mtg_no: Int,cbo_id: Long){
        incomeandExpenditureDao!!.deleteGrpFinancialTxnDetailData(mtg_no,cbo_id)
    }

    fun deleteGrpFinancialTxnDetailDataByCboId(cbo_id: Long){
        incomeandExpenditureDao!!.deleteGrpFinancialTxnDetailDataByCboId(cbo_id)
    }

    fun deleteShgFinancialTxnData(mtg_no: Int,cbo_id: Long){
        dtMtgFinTxnDao!!.deleteShgFinancialTxnData(mtg_no,cbo_id)
    }

    fun deleteShgFinancialTxnDataByCboId(cbo_id: Long){
        dtMtgFinTxnDao!!.deleteShgFinancialTxnDataByCboId(cbo_id)
    }

    fun deleteMemberLoanTxnData(mtg_no: Int,cbo_id: Long){
        dtLoanTxnMemDao!!.deleteMemberLoanTxnData(mtg_no,cbo_id)
    }

    fun deleteMemberLoanTxnDataByCboId(cbo_id: Long){
        dtLoanTxnMemDao!!.deleteMemberLoanTxnDataByCboId(cbo_id)
    }

    fun deleteGroupLoanData(mtg_no:Int,cbo_id: Long){
        dtLoanGpDao!!.deleteGroupLoanData(mtg_no,cbo_id)
    }

    fun deleteGroupLoanDataByCboId(cbo_id: Long){
        dtLoanGpDao!!.deleteGroupLoanDataByCboId(cbo_id)
    }

    fun deleteGroupLoanTxnData(mtg_no:Int,cbo_id: Long){
        dtLoanGpTxnDao!!.deleteGroupLoanTxnData(mtg_no,cbo_id)
    }

    fun deleteGroupLoanTxnDataByCboId(cbo_id: Long){
        dtLoanGpTxnDao!!.deleteGroupLoanTxnDataByCboId(cbo_id)
    }

    fun deleteMCPData(mtg_no:Int,cbo_id:Long){
        shgMcpDao!!.deleteMCPData(mtg_no,cbo_id)
    }

    fun deleteMCPDataByCboId(cbo_id:Long){
        shgMcpDao!!.deleteMCPDataByCboId(cbo_id)
    }

    fun deleteMemberLoan(cbo_id: Long,mtg_no: Int){
        dtLoanMemberDao!!.deleteMemberLoan(cbo_id,mtg_no)
    }

    fun deleteMemberLoanByCboId(cbo_id: Long){
        dtLoanMemberDao!!.deleteMemberLoanByCboId(cbo_id)
    }

    fun deleteShgLoanApplication(mtg_no: Int,cbo_id: Long){
        dtLoanApplicationDao!!.deleteShgLoanApplication(mtg_no,cbo_id)
    }

    fun deleteShgLoanApplicationByCboId(cbo_id: Long){
        dtLoanApplicationDao!!.deleteShgLoanApplicationByCboId(cbo_id)
    }

    fun deleteMemberRepaidData(mtgdate:Long,mtgno:Int,cbo_id:Long){
        dtLoanMemberScheduleDao!!.deleteMemberRepaidData(mtgdate,mtgno,cbo_id)
    }

    fun deleteMemberSubinstallmentData(mtgdate:Long,mtgno:Int,cbo_id:Long){
        dtLoanMemberScheduleDao!!.deleteMemberSubinstallmentData(mtgdate,mtgno,cbo_id)
    }

    fun deleteMemberScheduleByMtgGuid(mtgGuid: String){
        dtLoanMemberScheduleDao!!.deleteMemberScheduleByMtgGuid(mtgGuid)
    }

    fun deleteMemberSubinstallmentDataByCboId(cbo_id:Long){
        dtLoanMemberScheduleDao!!.deleteMemberSubinstallmentDataByCboId(cbo_id)
    }

    fun deleteGroupRepaidData(mtgdate:Long,mtgno:Int,cbo_id:Long){
        dtMtgGrpLoanScheduleDao!!.deleteGroupRepaidData(mtgdate,mtgno,cbo_id)
    }

    fun deleteGroupSubinstallmentData(mtgdate:Long,mtgno:Int,cbo_id:Long){
        dtMtgGrpLoanScheduleDao!!.deleteGroupSubinstallmentData(mtgdate,mtgno,cbo_id)
    }

    fun deleteGroupSubinstallmentDataByCboId(cbo_id:Long){
        dtMtgGrpLoanScheduleDao!!.deleteGroupSubinstallmentDataByCboId(cbo_id)
    }

    fun deleteGroupScheduleByMtgGuid(mtgGuid:String){
        dtMtgGrpLoanScheduleDao!!.deleteGroupScheduleByMtgGuid(mtgGuid)
    }

    fun updateShg_mtgIsEdited(mtg_no: Int,cbo_id: Long){
        dtmtgDao!!.updateShg_mtgIsEdited(mtg_no,cbo_id)
    }

    fun updateMtgApprova_Rejection_Status(
        mtgno: Int,
        cbo_id: Long,
        updatedOn:Long,
        updatedBy:String,
        approvalStatus:Int,
        checkerRemarks:String

    ){
        dtmtgDao!!.updateMtgApprova_Rejection_Status(
            mtgno,
            cbo_id,
            updatedOn,
            updatedBy,
            approvalStatus,
            checkerRemarks)
    }

    fun reurnmemberpost(memid: Long?):Int{
        return memberDao!!.reurnmemberpost(memid)
    }

    fun getClosedMemberLoanByMemberId(mtgno: Int, cbo_id: Long,completionFlag:Boolean,memberid: Long): List<LoanListModel>{
        return dtdetmtgDao!!.getClosedMemberLoanByMemberId(mtgno, cbo_id,completionFlag,memberid)
    }

    fun getCutOffClosedMemberLoanDataByCboId(mtgno: Int, cbo_id: Long,completionFlag:Boolean): List<LoanListModel>{
        return dtdetmtgDao!!.getCutOffClosedMemberLoanDataByCboId(mtgno, cbo_id,completionFlag)
    }

    fun getMonthlyCompulsarySaving(shg_id: Long?):Int{
        return dtmtgDao!!.getMonthlyCompulsarySaving(shg_id)
    }

    fun getTotalIncoming_OutgoingAmount(cbo_id: Long,mtg_no: Int,type:String):Int{
        return incomeandExpenditureDao!!.getTotalIncoming_OutgoingAmount(cbo_id,mtg_no,type)
    }

    fun getMemberReceiptAmount(cbo_id: Long,mtg_no: Int,type:String): Int{
        return dtFinTxnDetDao!!.getMemberReceiptAmount(cbo_id,mtg_no,type)
    }

    fun getTotalPrincipalDemand(mtg_no: Int,cbo_id: Long):Int{
        return dtLoanTxnMemDao!!.getTotalPrincipalDemand(mtg_no,cbo_id)
    }

    fun getTotalPrincipalDemandOb(mtg_no: Int,cbo_id: Long):Int{
        return dtLoanTxnMemDao!!.getTotalPrincipalDemandOb(mtg_no,cbo_id)
    }

    fun getTotalInterestAccured(mtg_no: Int,cbo_id: Long):Int{
        return dtLoanTxnMemDao!!.getTotalInterestAccured(mtg_no,cbo_id)
    }

    fun getTotalInterestAccuredOb(mtg_no: Int,cbo_id: Long):Int{
        return dtLoanTxnMemDao!!.getTotalInterestAccuredOb(mtg_no,cbo_id)
    }

    internal fun getBankCount(mtgno:Int,cbo_id:Long): Int{
        return dtMtgFinTxnDao!!.getBankCount(mtgno,cbo_id)
    }

    fun getTotalGrpRepaymentDemand(cboid: Long, mtgno: Int): Int{
        return dtLoanGpTxnDao!!.getTotalGrpRepaymentDemand(cboid, mtgno)
    }

    fun getTotalGrpLoansRepaid(cboid: Long, mtgno: Int):Int{
        return dtLoanGpTxnDao!!.getTotalGrpLoansRepaid(cboid, mtgno)
    }

    fun getTotalGrpLoans(cboid: Long, mtgno: Int):Int{
        return dtLoanGpTxnDao!!.getTotalGrpLoans(cboid, mtgno)
    }

    fun updatewithdrawl(
        withdrawl: Int,
        mtgno: Int,
        cbo_id: Long,
        memid: Long,
        updated_by:String,
        updated_on:Long
    ) {
        dtdetmtgDao!!.updatewithdrawl(
            withdrawl,
            mtgno,
            cbo_id,
            memid,
            updated_by,
            updated_on
        )
    }
    fun updatesavingwithdrawl(
        withdrawl: Int,
        mtgno: Int,
        cbo_id: Long,
        memid: Long,
        updated_by:String,
        updated_on:Long
    ) {
        dtdetmtgDao!!.updatesavingwithdrawl(
            withdrawl,
            mtgno,
            cbo_id,
            memid,
            updated_by,
            updated_on
        )
    }
    fun updateadjustwithdrawl(
        withdrawl: Int,
        mtgno: Int,
        cbo_id: Long,
        memid: Long,
        updated_by:String,
        updated_on:Long,
        refmtgno:Int
    ) {
        dtdetmtgDao!!.updateadjustwithdrawl(
            withdrawl,
            mtgno,
            cbo_id,
            memid,
            updated_by,
            updated_on,refmtgno
        )
    }
    fun updateadjustVoluntory(
        withdrawl: Int,
        mtgno: Int,
        cbo_id: Long,
        memid: Long,
        updated_by:String,
        updated_on:Long,
        refmtgno:Int
    ) {
        dtdetmtgDao!!.updateadjustVoluntory(
            withdrawl,
            mtgno,
            cbo_id,
            memid,
            updated_by,
            updated_on,refmtgno
        )
    }
    fun updateSettlementstatus(memid: Long,settlement: Int)
    {
        memberDao!!.updateSettlementstatus(
            memid,settlement)
    }

    internal fun getListDatawithoutmember(mtgno: Int, cbo_id: Long, memid: Long): List<DtmtgDetEntity> {
        return dtdetmtgDao!!.getListDatawithoutmember(mtgno, cbo_id,memid)
    }
    internal fun getListinactivemember(mtgno: Int, cbo_id: Long): List<DtmtgDetEntity> {
        return dtdetmtgDao!!.getListinactivemember(mtgno, cbo_id)
    }

    internal  fun updateMeetinguploadStatus(
        shg_id: Long?,
        mtg_no: Int,
        transactionid: String,
        lastupdatedate: Long,
        checker_remarks:String
    ){
        dtmtgDao!!.updateMeetinguploadStatus(
            shg_id,
            mtg_no,
            transactionid,
            lastupdatedate,
            checker_remarks
        )
    }

    internal fun updatedTransactionRemarks(checker_remarks:String,transaction_id:String){
        dtmtgDao!!.updatedTransactionRemarks(checker_remarks,transaction_id)
    }

    internal  fun getMeetingStatusRemarks(cbo_id: Long):List<DtmtgEntity>{
        return  dtmtgDao!!.getMeetingStatusRemarks(cbo_id)
    }

    internal fun getCashInTransit(cbo_id: Long,mtgno: Int):Int{
        return dtmtgDao!!.getCashInTransit(cbo_id,mtgno)
    }

    internal fun updateCashInTransit(cashInTransit:Int,cbo_id: Long,mtgno: Int){
        dtmtgDao!!.updateCashInTransit(cashInTransit,cbo_id,mtgno)
    }

    internal fun getBank_CashInTransit(mtgno:Int,cbo_id:Long,bank_code:String):Int{
        return dtMtgFinTxnDao!!.getBank_CashInTransit(mtgno,cbo_id,bank_code)
    }

    internal fun updateBank_CashInTransit(amount:Int,mtgno:Int,cbo_id:Long,bank_code:String){
        dtMtgFinTxnDao!!.updateBank_CashInTransit(amount,mtgno,cbo_id,bank_code)
    }

    internal fun getMemberFinancialTxnData(mtg_no: Int,cbo_id: Long) : List<FinancialTransactionsMemEntity>{
        return dtFinTxnDetDao!!.getMemberFinancialTxnData(mtg_no,cbo_id)
    }

    internal fun getRescheduleloanListMtgByMtgnum(mtgno: Int, cbo_id: Long): List<LoanListModel>{
        return dtdetmtgDao!!.getRescheduleloanListMtgByMtgnum(mtgno, cbo_id)
    }
    internal fun getMemberAdjustmentAmount(mtgno:Int,memid:Long,auid:Int):Int?{
        return dtdetmtgDao!!.getMemberAdjustmentAmount(mtgno,memid,auid)
    }
    internal fun getTotalFDAmount( mtg_no: Int,cbo_id: Long,auid:Int,type:String): Int{
        return incomeandExpenditureDao!!.getTotalFDAmount( mtg_no,cbo_id,auid,type)
    }

    internal  fun getNonApprovedShgsCount(shgId:Long):Int{
        return  dtmtgDao!!.getNonApprovedShgsCount(shgId)
    }

    internal  fun getNonSynchedShgsCount(shgId:Long):Int{
        return  dtmtgDao!!.getNonSynchedShgsCount(shgId)
    }

    fun getMeetingStatus(cboid: Long,mtgno: Int):Int{
        return dtmtgDao!!.getMeetingStatus(cboid, mtgno)
    }

    fun updatedTransactionstatus(transaction_status:String,transaction_id:String){
        return dtmtgDao!!.updatedTransactionstatus(transaction_status, transaction_id)
    }
}