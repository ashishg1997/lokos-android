package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.ImageuploadEntity
import com.microware.miracle.dao.ImageUploadDao

class ImageUploadRepository {
    var imageUploadDao: ImageUploadDao? = null

    constructor(application: Application){
        imageUploadDao = CDFIApplication.database?.imageUploadDao()
    }

    fun getimage(): List<ImageuploadEntity>?{
        return imageUploadDao!!.getimage()
    }

  fun getimage_by_guid(shg_guid:String): List<ImageuploadEntity>?{
        return imageUploadDao!!.getimage_by_guid(shg_guid)
    }

    fun insert(imageuploadEntity : ImageuploadEntity){
        insertAsyncTask(imageuploadEntity,imageUploadDao!!).execute()
    }

    class insertAsyncTask(): AsyncTask<Void, Void, String>(){

        private var imageuploadEntity: ImageuploadEntity? = null
        var imageUploadDao: ImageUploadDao? = null

        constructor (imageuploadEntity: ImageuploadEntity?, imageUploadDao: ImageUploadDao) : this() {
            this.imageuploadEntity = imageuploadEntity
            this.imageUploadDao = imageUploadDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            imageUploadDao!!.insertimage(imageuploadEntity)
            return null
        }

    }


}