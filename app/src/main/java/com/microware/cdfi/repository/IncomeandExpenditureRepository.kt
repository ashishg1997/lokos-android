package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import androidx.room.Query
import com.microware.cdfi.api.meetingmodel.BankTransactionModel
import com.microware.cdfi.api.meetinguploadmodel.ShgFinanceTransactionDetailGroupListData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.IncomeandExpenditureDao
import com.microware.cdfi.dao.MstCOADao
import com.microware.cdfi.entity.MstCOAEntity
import com.microware.cdfi.entity.ShgFinancialTxnDetailEntity

class IncomeandExpenditureRepository {

    var incomeandExpenditureDao: IncomeandExpenditureDao? = null
    var mstCoaDao: MstCOADao? = null

    constructor(application: Application) {
        incomeandExpenditureDao = CDFIApplication.database?.incomeandExpendatureDao()
        mstCoaDao = CDFIApplication.database?.mstCOADao()
    }

    fun getIncomeAndExpendituredata(mtg_guid: String, cbo_id: Long, mtg_no: Int,auid:Int,amount_to_from:Int): List<ShgFinancialTxnDetailEntity>? {
        return incomeandExpenditureDao!!.getIncomeAndExpendituredata(mtg_guid, cbo_id, mtg_no, auid,amount_to_from)
    }

    fun insert(shgFinancialTxnDetailEntity: ShgFinancialTxnDetailEntity) {
        insertAsyncTask(shgFinancialTxnDetailEntity, incomeandExpenditureDao!!).execute()
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var shgFinancialTxnDetailEntity: ShgFinancialTxnDetailEntity? = null
        var incomeandExpenditureDao: IncomeandExpenditureDao? = null

        constructor (
            shgFinancialTxnDetailEntity: ShgFinancialTxnDetailEntity?,
            incomeandExpenditureDao: IncomeandExpenditureDao
        ) : this() {
            this.shgFinancialTxnDetailEntity = shgFinancialTxnDetailEntity
            this.incomeandExpenditureDao = incomeandExpenditureDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            incomeandExpenditureDao!!.insertIncomeAndExpenditureData(shgFinancialTxnDetailEntity!!)
            return null
        }
    }

    fun deleteRecord(mtg_guid: String) {
        incomeandExpenditureDao!!.deleteRecord(mtg_guid)
    }

    fun updateIncomeAndExpenditure(
        mtg_guid: String,
        cbo_id: Long,
        mtg_no:Int,
        auid: Int,
        fund_type: Int?,
        amount_to_from: Int?,
        type: String?,
        amount: Int?,
        trans_date: Long?,
        date_realisation: Long?,
        mode_payment: Int?,
        bank_code: String?,
        transaction_no: String?,
        updatedby: String?,
        updatedon: Long?
    ) {
        incomeandExpenditureDao!!.updateIncomeAndExpenditure(
            mtg_guid,
            cbo_id,
            mtg_no,
            auid,
            fund_type,
            amount_to_from,
            type,
            amount,
            trans_date,
            date_realisation,
            mode_payment,
            bank_code,
            transaction_no,
            updatedby,
            updatedon
        )
    }

    fun getIncomeAndExpenditureAlldata(mtg_guid: String, cbo_id: Long, mtg_no: Int,fund_type:Int): LiveData<List<ShgFinancialTxnDetailEntity>?>{
        return incomeandExpenditureDao!!.getIncomeAndExpenditureAlldata(mtg_guid, cbo_id, mtg_no, fund_type)
    }
 fun getIncomeAndExpenditurenotinauid(auid:List<Int>,mtg_guid: String, cbo_id: Long, mtg_no: Int,fund_type:Int): LiveData<List<ShgFinancialTxnDetailEntity>?>{
        return incomeandExpenditureDao!!.getIncomeAndExpenditurenotinauid(auid,mtg_guid, cbo_id, mtg_no, fund_type)
    }

    fun getIncomeAndExpenditureAmount(cbo_id: Long,mtg_no: Int,modePayment:Int,fund_type: Int):Int{
        return incomeandExpenditureDao!!.getIncomeAndExpenditureAmount(cbo_id, mtg_no, modePayment, fund_type)
    }
 fun getMemberIncomeAndExpenditureAmountnotinauid(auid:List<Int>,cbo_id: Long,mtg_no: Int,fund_type: Int):Int{
        return incomeandExpenditureDao!!.getMemberIncomeAndExpenditureAmountnotinauid(auid,cbo_id, mtg_no, fund_type)
    }

    fun getMemberIncomeAndExpenditureAmount(cbo_id: Long,mtg_no: Int,fund_type: Int):Int{
        return incomeandExpenditureDao!!.getMemberIncomeAndExpenditureAmount(cbo_id, mtg_no, fund_type)
    }

    fun getBankIncomeAndExpenditureAmount(cbo_id: Long,mtg_no: Int,fund_type: Int):Int{
        return incomeandExpenditureDao!!.getBankIncomeAndExpenditureAmount(cbo_id, mtg_no, fund_type)
    }

    fun getBankCodeIncomeAndExpenditureAmount(cbo_id: Long,mtg_no: Int,fund_type: Int,bank_code: String?):Int{
        return incomeandExpenditureDao!!.getBankCodeIncomeAndExpenditureAmount(cbo_id, mtg_no, fund_type,bank_code)
    }

    fun getShgTransactionsGrpAmount(cbo_id: Long,mtg_no: Int,bank_code:String,fund_type: Int): Int{
        return incomeandExpenditureDao!!.getShgTransactionsGrpAmount(cbo_id, mtg_no, bank_code,fund_type)
    }


    internal fun getCoaSubHeadData(uid:List<Int>,type:String,languageCode:String): List<MstCOAEntity>?{
        return mstCoaDao!!.getCoaSubHeadData(uid,type,languageCode)
    }

    fun getInvestmentdata(mtg_guid: String, cbo_id: Long, mtg_no: Int,auid:Int,type:String,amount_to_from: Int): List<ShgFinancialTxnDetailEntity>?{
        return incomeandExpenditureDao!!.getInvestmentdata(mtg_guid, cbo_id, mtg_no,auid,type,amount_to_from)
    }

    fun getInvestmentListByMtgNum(cbo_id: Long, mtg_no: Int,auid:List<Int>,type:String): LiveData<List<ShgFinancialTxnDetailEntity>?>{
        return incomeandExpenditureDao!!.getInvestmentListByMtgNum(cbo_id, mtg_no,auid,type)
    }

    fun getUploadListData(mtg_no:Int,cbo_id: Long) : List<ShgFinancialTxnDetailEntity>{
        return incomeandExpenditureDao!!.getUploadListData(mtg_no,cbo_id)
    }

    fun getAdjustmentCashCount(mtg_no:Int,cbo_id:Long):Int{
        return incomeandExpenditureDao!!.getAdjustmentCashCount(mtg_no,cbo_id)
    }

    fun updateCashAdjustment(
        cbo_id: Long,
        mtg_no:Int,
        auid: Int,
        fund_type: Int?,
        amount_to_from: Int?,
        type: String?,
        amount: Int?,
        trans_date: Long?,
        date_realisation: Long?,
        mode_payment: Int?,
        bank_code: String?,
        transaction_no: String?,
        updatedby: String?,
        updatedon: Long?
    ) {
        incomeandExpenditureDao!!.updateCashAdjustment(
            cbo_id,
            mtg_no,
            auid,
            fund_type,
            amount_to_from,
            type,
            amount,
            trans_date,
            date_realisation,
            mode_payment,
            bank_code,
            transaction_no,
            updatedby,
            updatedon
        )
    }
    fun getSingleTransferedBankList(
        mtg_guid: String,
        cbo_id: Long,
        mtg_no: Int,
        auid: Int
    ): List<ShgFinancialTxnDetailEntity>? {
        return incomeandExpenditureDao!!.getSingleTransferedBankList(
            mtg_guid, cbo_id, mtg_no, auid
        )
    }
    fun getAllTransferedBankList(
        cbo_id: Long,
        mtg_no: Int,
        fund_type: Int
    ): LiveData<List<ShgFinancialTxnDetailEntity>?> {
        return incomeandExpenditureDao!!.getAllTransferedBankList(
            cbo_id,
            mtg_no,
            fund_type
        )
    }

    fun getBankCodeIncomeAndExpendituretransferAmount(cbo_id: Long,mtg_no: Int,bank_code: String?,auid:List<Int>,type:String):Int{
        return incomeandExpenditureDao!!.getBankCodeIncomeAndExpendituretransferAmount(cbo_id,mtg_no,bank_code,auid,type)
    }

    fun getBankTransferList(cbo_id: Long, mtg_no: Int, fund_type: Int): LiveData<List<BankTransactionModel>?>{
        return incomeandExpenditureDao!!.getBankTransferList(cbo_id,mtg_no,fund_type)
    }
    fun getInvestmentAmount(cbo_id: Long,mtg_no: Int,type:String,auid:List<Int>): Int{
        return incomeandExpenditureDao!!.getInvestmentAmount(cbo_id, mtg_no, type, auid)
    }



}