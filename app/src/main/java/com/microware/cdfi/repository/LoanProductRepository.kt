package com.microware.cdfi.repository

import android.app.Application
import com.microware.cdfi.api.meetingmodel.FundTypeModel
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.LoanProductDao
import com.microware.cdfi.entity.MstproductEntity

class LoanProductRepository {

    var loanProductDao:LoanProductDao? = null

    constructor(application: Application){
        loanProductDao = CDFIApplication.database?.laonProductDao()
    }

    fun getFundTypeBySource_Receipt(source:Int?,recipent:Int?): List<FundTypeModel>? {
        return loanProductDao!!.getFundTypeBySource_Receipt(source,recipent)
    }
   fun getFundTypeBySource_Receiptwithtypeid(source:Int?,recipent:Int?,typeId: Int?): List<FundTypeModel>? {
        return loanProductDao!!.getFundTypeBySource_Receiptwithtypeid(source,recipent,typeId)
    }
    fun getFundType(source:Int?,recipent:Int?,typeId:Int?): String{
        return loanProductDao!!.getFundType(source, recipent, typeId)
    }
    fun getFundTypeBySource(source:Int?,recipent:Int?): List<FundTypeModel>?{
        return loanProductDao!!.getFundTypeBySource(source, recipent)
    }
}