package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.CboAddressDao
import com.microware.cdfi.dao.PanchayatDao
import com.microware.cdfi.entity.*
import com.microware.miracle.dao.BlockDao
import com.microware.miracle.dao.DistrictDao
import com.microware.miracle.dao.StateDao
import com.microware.miracle.dao.VillageDao

class LocationRepository{
    var stateDao: StateDao? = null
    var districtDao: DistrictDao? = null
    var blockDao: BlockDao? = null
    var panchayatDao: PanchayatDao? = null
    var villageDao: VillageDao? = null

    constructor(application: Application){
        stateDao = CDFIApplication.database?.stateDao()
        districtDao = CDFIApplication.database?.districtDao()
        blockDao = CDFIApplication.database?.blockDao()
        panchayatDao = CDFIApplication.database?.panchayatDao()
        villageDao = CDFIApplication.database?.villageDao()
    }

    fun getState(): LiveData<List<StateEntity>> {
        return stateDao!!.getState()
    }

    fun getStateByStateCode(): List<StateEntity> {
        return stateDao!!.getStateByStateCode()
    }
fun getStateByStateCode(stateCode: Int): List<StateEntity> {
        return stateDao!!.getStateByStateCode(stateCode)
    }

    fun getDistrictByStateCode(stateCode: Int): LiveData<List<DistrictEntity>> {
        return districtDao!!.getDistrictByStateCode(stateCode)
    }

    fun getDistrictData(stateCode: Int): List<DistrictEntity> {
        return districtDao!!.getDistrictData(stateCode)
    }

    fun getBlock_district_code(stateCode:Int,districtCode:Int): LiveData<List<BlockEntity>> {
        return blockDao!!.getBlock_district_code(stateCode,districtCode)
    }

    fun getBlock_data(stateCode:Int,districtCode:Int): List<BlockEntity> {
        return blockDao!!.getBlock_data(stateCode,districtCode)
    }

    fun getPanchayatdataByPanchayatCode(stateCode: Int,district_code: Int,block_code: Int): LiveData<List<PanchayatEntity>>{
        return panchayatDao!!.getPanchayatdataByPanchayatCode(stateCode,district_code,block_code)
    }

    fun getPanchayatByPanchayatCode(stateCode: Int,district_code: Int,block_code: Int): List<PanchayatEntity>{
        return panchayatDao!!.getPanchayatByPanchayatCode(stateCode,district_code,block_code)
    }
    fun getVillage_by_panchayatcode(stateCode: Int,district_code: Int,block_code: Int,panchayat_code:Int): LiveData<List<VillageEntity>>{
        return villageDao!!.getVillage_by_panchayatcode(stateCode,district_code,block_code,panchayat_code)
    }

    fun getVillagedata_by_panchayatCode(stateCode: Int,district_code: Int,block_code: Int,panchayat_code:Int): List<VillageEntity>{
        return villageDao!!.getVillagedata_by_panchayatCode(stateCode,district_code,block_code,panchayat_code)
    }

    internal fun getVillageName(village_id: Int): String? {
        return villageDao?.getVillageName(village_id)
    }

    internal fun getPanchayatName(panchayat_id: Int): String? {
        return panchayatDao?.getPanchayatName(panchayat_id)
    }

}