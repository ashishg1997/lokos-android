package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.dao.LookupDao
import com.microware.cdfi.dao.subcommitteeMasterDao
import com.microware.cdfi.entity.subcommitee_masterEntity

class LookupRepository {
    var lookupDao: LookupDao? = null

    constructor(application: Application) {
        lookupDao = CDFIApplication.database?.lookupdao()
    }

    internal fun getlookup(flag:Int?,languageID:String?): List<LookupEntity>? {
        return lookupDao!!.getlookup(flag,languageID)
    }

    internal fun getlookupbycode_data(key1:Int?,languageID:String?):List<LookupEntity>? {
        return lookupDao!!.getlookupbycode_data(key1,languageID)
    }

    internal fun getlookupfromkeycode(flag: String?,languageID:String?): List<LookupEntity>? {
        return lookupDao!!.getlookupfromkeycode(flag,languageID)
    }

    internal fun getAddressType(kyc_code: String, addressType: Int?): String? {
        return lookupDao?.getAddressType(kyc_code,addressType)
    }

    internal fun getlookupValue(flag:Int?,languageID:String?,keycode:Int?): String? {
        return lookupDao!!.getlookupValue(flag,languageID,keycode)
    }

    fun insert(lookupEntity: LookupEntity){
        insertAsyncTask(lookupEntity,lookupDao!!).execute()
    }

    class insertAsyncTask(): AsyncTask<Void, Void, String>(){

        private var lookupEntity: LookupEntity? = null
        var lookupDao: LookupDao? = null

        constructor (lookupEntity: LookupEntity?, lookupDao: LookupDao) : this() {
            this.lookupEntity = lookupEntity
            this.lookupDao = lookupDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            lookupDao!!.insertLookupData(lookupEntity)
            return null
        }

    }

    internal  fun getlookupMasterdata(key1:Int?,languageID:String?,code:List<Int>):List<LookupEntity>?{
        return lookupDao!!.getlookupMasterdata(key1,languageID,code)
    }

    internal fun getlookupnotin(flag:Int?,notincode:Int,languageID:String?): List<LookupEntity>? {
        return lookupDao!!.getlookupnotin(flag,notincode,languageID)
    }
    internal fun getDestination(key1: Int, language_id: String?, lookup_code: Int?): String? {
        return lookupDao!!.getDestination(key1, language_id, lookup_code)
    }
    fun getplaceOfInvestment(key1:Int?,languageID:String?):List<LookupEntity>?{
        return lookupDao!!.getplaceOfInvestment(key1, languageID)
    }
}