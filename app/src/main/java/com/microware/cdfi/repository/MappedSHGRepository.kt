package com.microware.cdfi.repository

import android.app.Application
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.MappedShgDao
import com.microware.cdfi.dao.SHGDao
import com.microware.cdfi.entity.MappedShgEntity

class MappedSHGRepository {

    var mappedShgDao: MappedShgDao? = null

    constructor(application: Application) {
        mappedShgDao = CDFIApplication.database?.mappedShgDao()
    }

    internal fun getShgNameList(vo_guid:String):List<MappedShgEntity>?{
        return mappedShgDao!!.getShgNameList(vo_guid)
    }
    internal fun getmeetingShgList(vo_guid:String):List<MappedShgEntity>?{
        return mappedShgDao!!.getmeetingShgList(vo_guid)
    }
    internal fun getShgName(shg_id: Long, vo_guid: String): String? {
        return mappedShgDao!!.getShgName(shg_id, vo_guid)
    }
    fun updateSettlementstatus(memid: Long,settlement: Int) {
        return mappedShgDao!!.updateSettlementstatus(
            memid,settlement)
    }
}