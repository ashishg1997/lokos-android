package com.microware.cdfi.repository

import android.app.Application
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.MappedShgDao
import com.microware.cdfi.dao.MappedVoDao
import com.microware.cdfi.dao.SHGDao
import com.microware.cdfi.entity.MappedShgEntity
import com.microware.cdfi.entity.MappedVoEntity

class MappedVoRepository {

    var mappedVoDao: MappedVoDao? = null

    constructor(application: Application) {
        mappedVoDao = CDFIApplication.database?.mappedVoDao()
    }

    internal fun getVoNameList(clf_guid:String):List<MappedVoEntity>?{
        return mappedVoDao!!.getVoNameList(clf_guid)
    }
}