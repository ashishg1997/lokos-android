package com.microware.cdfi.repository

import android.app.Application
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.MasterBankBranchDao
import com.microware.cdfi.entity.Bank_branchEntity

class MasterBankBranchRepository {
    var masterBankBranchDao: MasterBankBranchDao? = null

    constructor(application: Application) {
        masterBankBranchDao = CDFIApplication.database?.masterbankbranchDao()!!
    }

    fun getBankBranch(bankCode: Int?): List<Bank_branchEntity>? {
        return masterBankBranchDao!!.getBankBranch(bankCode)
    }

    fun BankBranchlistifsc(ifsccode:String): List<Bank_branchEntity>? {
        return masterBankBranchDao!!.BankBranchlistifsc(ifsccode)
    }

    fun getBranchname(bank_branch_code: Int?): String {
        return masterBankBranchDao!!.getBranchname(bank_branch_code)
    }
}