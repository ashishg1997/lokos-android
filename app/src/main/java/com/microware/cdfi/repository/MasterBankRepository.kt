package com.microware.cdfi.repository

import android.app.Application
import com.microware.cdfi.api.meetingmodel.BankListModel
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.MasterBankDao
import com.microware.cdfi.entity.BankEntity
import com.microware.cdfi.entity.Cbo_bankEntity

class MasterBankRepository {

    var masterBankDao: MasterBankDao? = null

    constructor(application: Application){
        masterBankDao = CDFIApplication.database?.masterBankDao()
    }

    fun getBankMaster(): List<BankEntity>?{
        return masterBankDao!!.getBank()
    }


    fun getBankwithcode(bankcode:String): List<BankEntity>?{
        return masterBankDao!!.getBankwithcode(bankcode)
    }

    internal fun getBankName(bank_id: Int): String? {
        return masterBankDao?.getBankName(bank_id)
    }

    internal fun getAccountLength(bank_id: Int): String?{
        return masterBankDao?.getAccountLength(bank_id)
    }
    fun getBankMaster1(cboGuid:String): List<BankListModel>? {
        return masterBankDao!!.getBankMaster1(cboGuid)
    }



}