package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.Cbo_BankDao
import com.microware.cdfi.dao.Cbo_phoneDetailDao
import com.microware.cdfi.dao.MemSettlementDao
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.entity.Cbo_phoneEntity
import com.microware.cdfi.entity.DtMemSettlementEntity

class MemSettlementRepository {
    var memSettlementDao: MemSettlementDao? = null

    constructor(application: Application){
        memSettlementDao = CDFIApplication.database?.memSettlementDao()
    }

    fun getSettlementdata(memid: Long?,mtgno:Int): List<DtMemSettlementEntity>?{
        return memSettlementDao!!.getSettlementdata(memid,mtgno)
    }

    fun insertSettlementData(dtMemSettlementEntity: DtMemSettlementEntity){
         memSettlementDao!!.insertSettlementData(dtMemSettlementEntity)
    }

    fun getTotalSettelMentAmt(mtgno:Int,cbo_id:Long):Int{
        return memSettlementDao!!.getTotalSettelMentAmt(mtgno, cbo_id)
    }
}