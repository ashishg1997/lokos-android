package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.MemberAddressDao
import com.microware.cdfi.entity.member_addressEntity

class MemberAddressRepository {

    var addressDao: MemberAddressDao? = null

    constructor(application: Application){
        addressDao = CDFIApplication.database?.memberaddressDao()
    }

    fun getAddressdetaildata(member_guid: String?): List<member_addressEntity>?{
        return addressDao!!.getAddressdetaildata(member_guid)
    }

    fun getAddressdata(member_guid: String?): LiveData<List<member_addressEntity>>?{
        return addressDao!!.getAddressdata(member_guid)
    }
 fun getAddressdatalist(member_guid: String?): List<member_addressEntity>?
 {
        return addressDao!!.getAddressdatalist(member_guid)
    }

   fun getAddressdatalistcount(member_guid: String?): List<member_addressEntity>?
 {
        return addressDao!!.getAddressdatalistcount(member_guid)
    }

    fun insert(addressEntity : member_addressEntity){
        insertAsyncTask(addressEntity,addressDao!!).execute()
    }

    class insertAsyncTask(): AsyncTask<Void, Void, String>(){

        private var addressEntity: member_addressEntity? = null
        var addressDao: MemberAddressDao? = null

        constructor (addressEntity: member_addressEntity?, addressDao: MemberAddressDao) : this() {
            this.addressEntity = addressEntity
            this.addressDao = addressDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            addressDao!!.insertAddressDetailData(addressEntity)
            return null
        }

    }

    fun updateAddressDetail(
        address_guid: String,
        address_type: Int,
        address_line1: String,
        address_line2: String,
        village: Int,
        panchayat_id: Int,
        landmark: String,
        state: Int,
        District: Int,
        postal_code: String,
        is_Active: Int,
        updated_date: Long,
        updated_by: String,
        addresslocation: Int,
        isCompleted: Int
    ) {
        addressDao!!.updateAddressDetail(
            address_guid,
            address_type,
            address_line1,
            address_line2,
            village,
            panchayat_id,landmark,state,District,postal_code,is_Active,updated_date,updated_by,addresslocation,isCompleted
        )
    }
    fun deleteData(address_guid: String?){
        addressDao!!.deleteData(address_guid)
    }
    fun deleteRecord(address_guid: String?){
        addressDao!!.deleteRecord(address_guid)
    }
    fun updateAddressdata(
        address_guid: String,
        address_line1: String,
        address_line2: String,
        village: Int,
        panchayat_id: Int,
        block_id: Int,
        state: Int,
        District: Int,
        postal_code: String,
        is_Active: Int,
        updated_date: Long,
        updated_by: String,
        addresslocation: Int
    ){
        addressDao!!.updateAddressdata(
            address_guid,
            address_line1,
            address_line2,
            village,
            panchayat_id,
            block_id,
            state,
            District,
            postal_code,
            is_Active,
            updated_date,
            updated_by,
            addresslocation
        )
    }

    fun getAddressByEntrySource(member_guid: String?,entrySource:Int): String?{
        return addressDao!!.getAddressByEntrySource(member_guid,entrySource)
    }

    fun getCompletionCount(member_guid: String?):Int{
        return addressDao!!.getCompletionCount(member_guid)
    }

    fun getVerificationCount(member_guid: String?):Int{
        return addressDao!!.getVerificationCount(member_guid)
    }

    fun updateIsVerifed(member_guid: String?,updated_date: Long,updated_by: String){
        addressDao!!.updateIsVerifed(member_guid,updated_date,updated_by)
    }
}