package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.MemberBankDao
import com.microware.cdfi.entity.MemberBankAccountEntity

class MemberBankRepository {

    var bankdao: MemberBankDao? = null

    constructor(application: Application) {
        bankdao = CDFIApplication.database?.memberbanckDao()
    }

    fun getBankdetaildata(memberGUID: String?): LiveData<List<MemberBankAccountEntity>>? {
        return bankdao!!.getBankdetaildata(memberGUID)
    }

     fun getBankdetaildatalist(memberGUID: String?): List<MemberBankAccountEntity>? {
        return bankdao!!.getBankdetaildatalist(memberGUID)
    }

     fun getBankdetaildatalistcount(memberGUID: String?): List<MemberBankAccountEntity>? {
        return bankdao!!.getBankdetaildatalistcount(memberGUID)
    }
fun getBankdetaildefaultcount(memberGUID: String?): Int {
        return bankdao!!.getBankdetaildefaultcount(memberGUID)
    }

    fun getBankdata(memberGUID: String?): List<MemberBankAccountEntity>? {
        return bankdao!!.getBankdata(memberGUID)
    }

    fun insert(bankEntity: MemberBankAccountEntity) {
        insertAsyncTask(bankEntity, bankdao!!).execute()
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var bankEntity: MemberBankAccountEntity? = null
        var bankdao: MemberBankDao? = null

        constructor (bankEntity: MemberBankAccountEntity?, bankdao: MemberBankDao) : this() {
            this.bankEntity = bankEntity
            this.bankdao = bankdao
        }

        override fun doInBackground(vararg params: Void?): String? {
            bankdao!!.insertBankDetailData(bankEntity)
            return null
        }

    }


    fun updateBankDetail(
        bank_guid: String,
        account_no: String,
        bank_id: String,
        bank_account: Int,
        account_type: String,
        branchID: String,
        account_open_date: String,
        is_default_account: Int,
        status: Int,
        closing_date: String,
        gl_code: String,
        sameAsGroup: Int,
        nameinbankpassbook: String,
        is_edited: Int,
        updated_date: Long,
        updated_by: String,
        imageName: String,
        isCompleted: Int,
        ifsc_code:String
    ) {
        bankdao!!.updateBankDetail(
            bank_guid,
            account_no,
            bank_id,
            bank_account,
            account_type,branchID,account_open_date,is_default_account,
            status,closing_date,gl_code,sameAsGroup,nameinbankpassbook,is_edited,updated_date,updated_by,imageName,isCompleted,ifsc_code
        )
    }

    fun getbank_acc_count(account_no:String,memberguid: String):Int{
        return bankdao!!.getbank_acc_count(account_no,memberguid)
    }

    fun deleteData(bank_guid: String?){
        bankdao!!.deleteData(bank_guid)
    }

    fun deleteRecord(bank_guid: String?){
        bankdao!!.deleteRecord(bank_guid)
    }

    fun updateisedit( memberguid: String,last_uploaded_date:Long){
        bankdao!!.updateisedit( memberguid,last_uploaded_date)
    }

    fun getCompletionCount(member_guid: String?):Int{
        return bankdao!!.getCompletionCount(member_guid)
    }

    fun getVerificationCount(member_guid: String?):Int{
        return bankdao!!.getVerificationCount(member_guid)
    }

    fun updateIsVerifed(member_guid: String?,updated_date: Long,updated_by: String){
        bankdao!!.updateIsVerifed(member_guid,updated_date,updated_by)
    }

    fun getbank_acc_count2(account_no:String):Int{
        return bankdao!!.getbank_acc_count2(account_no)
    }

    fun updateactivotaionstatus(memberbank_guid: String?, status: Int){
        bankdao!!.updateactivotaionstatus(memberbank_guid,status)
    }
}