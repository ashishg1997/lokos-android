package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.MemberDesignationDao
import com.microware.cdfi.entity.MemberDesignationEntity

class MemberDesignationRepository {
    var memberDesignationDao: MemberDesignationDao? = null

    constructor(application: Application) {
        memberDesignationDao = CDFIApplication.database?.memberDesignationDao()
    }

    fun getMemberDesignationdata(shgGuid: String, memberGUID: String): MemberDesignationEntity {
        return memberDesignationDao!!.getMemberDesignationdetail(shgGuid, memberGUID)
    }

    fun getMemberDesignationdetaillist(shgGuid: String): List<MemberDesignationEntity> {
        return memberDesignationDao!!.getMemberDesignationdetaillist(shgGuid)
    }

    fun getMemberDesignationdetaillistcount(shgGuid: String?): List<MemberDesignationEntity> {
        return memberDesignationDao!!.getMemberDesignationdetaillistcount(shgGuid)
    }

    fun insert(memberDesignationEntity: MemberDesignationEntity) {
        insertAsyncTask(memberDesignationEntity, memberDesignationDao!!).execute()
    }

    fun getMemberDesignation(shgGUID: String): LiveData<List<MemberDesignationEntity>>? {
        return memberDesignationDao!!.getMemberDesignation(shgGUID)
    }

    fun getMembercount(memguid: String): Int {
        return memberDesignationDao!!.getMembercount(memguid)
    }

    fun getsignatoryMembercount(memguid: String): Int {
        return memberDesignationDao!!.getsignatoryMembercount(memguid)
    }

    fun getactivesignatorycount(shgGUID: String): Int {
        return memberDesignationDao!!.getactivesignatorycount(shgGUID)
    }

     fun getMemberDesignationdata(shgGUID: String, id: Int?): List<MemberDesignationEntity> {
        return memberDesignationDao!!.getMemberDesignationdata(shgGUID,id)
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var memberDesignationEntity: MemberDesignationEntity? = null
        var memberDesignationDao: MemberDesignationDao? = null

        constructor (
            memberDesignationEntity: MemberDesignationEntity?,
            memberDesignationDao: MemberDesignationDao
        ) : this() {
            this.memberDesignationEntity = memberDesignationEntity
            this.memberDesignationDao = memberDesignationDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            memberDesignationDao!!.insertMemberDesignation(memberDesignationEntity)
            return null
        }

    }
}