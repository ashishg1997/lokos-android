package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.MemberKYCDao
import com.microware.cdfi.entity.Member_KYC_Entity

class MemberKYCRepository {

    var kycDao : MemberKYCDao? = null

    constructor(application: Application){
        kycDao = CDFIApplication.database?.memberkycDao()
    }

    fun getKycdetaildata(memberGUID: String?): LiveData<List<Member_KYC_Entity>>?{
        return kycDao!!.getKycdetaildata(memberGUID)
    }
   fun getKycdetaildatalist(memberGUID: String?): List<Member_KYC_Entity>?{
        return kycDao!!.getKycdetaildatalist(memberGUID)
    }
   fun getKycdetaildatalistcount(memberGUID: String?): List<Member_KYC_Entity>?{
        return kycDao!!.getKycdetaildatalistcount(memberGUID)
    }
    fun getKycdetail(kycGUID: String?): List<Member_KYC_Entity>?{
        return kycDao!!.getKycdetail(kycGUID)
    }

   fun getKycwithkycno(kyc_number: String?): List<Member_KYC_Entity>?{
        return kycDao!!.getKycwithkycno(kyc_number)
    }

    fun insert(kycEntity : Member_KYC_Entity){
        insertAsyncTask(kycEntity,kycDao!!).execute()
    }

    class insertAsyncTask(): AsyncTask<Void, Void, String>(){

        private var cbokycEntity: Member_KYC_Entity? = null
        var kycDao: MemberKYCDao? = null

        constructor (cbokycEntity: Member_KYC_Entity?, kycDao: MemberKYCDao) : this() {
            this.cbokycEntity = cbokycEntity
            this.kycDao = kycDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            kycDao!!.insertKycDetailData(cbokycEntity)
            return null
        }

    }

    fun updateKycDetail(
        kyc_guid: String,
        kyc_type: Int,
        kyc_id: String,
        kyc_front_doc_orig_name: String,
        kyc_front_doc_encp_name: String,
        kyc_rear_doc_orig_name: String,
        kyc_rear_doc_encp_name: String,
        device: Int,
        is_edited: Int,
        updated_date: Long,
        updated_by: String,
        isCompleted: Int
    ) {
        kycDao!!.updateKycDetail(
            kyc_guid,
            kyc_type,
            kyc_id,
            kyc_front_doc_orig_name,
            kyc_front_doc_encp_name,kyc_rear_doc_orig_name,kyc_rear_doc_encp_name,device,is_edited,updated_date,updated_by,isCompleted
        )
    }

    fun deleteData(kyc_guid: String?){
        kycDao!!.deleteData(kyc_guid)
    }
    fun deleteRecord(kyc_guid: String?){
        kycDao!!.deleteRecord(kyc_guid)
    }

    fun getKycEntrySource(memberGUID: String?,kyc_type: Int): Int?{
        return kycDao!!.getKycEntrySource(memberGUID,kyc_type)
    }

    fun getKycGuidbyKycType(memberGUID: String?,kyc_type: Int):String?{
        return kycDao!!.getKycGuidbyKycType(memberGUID,kyc_type)
    }

    fun updateKycNumber(
        kyc_guid: String,
        kyc_id: String,
        is_edited: Int,
        entrySource: Int,
        updated_date: Long,
        updated_by: String
    ){
        kycDao!!.updateKycNumber(
            kyc_guid,
            kyc_id,
            is_edited,
            entrySource,
            updated_date,
            updated_by
        )
    }

    fun getCompletionCount(member_guid: String?):Int{
        return kycDao!!.getCompletionCount(member_guid)
    }

    fun getVerificationCount(member_guid: String?):Int{
        return kycDao!!.getVerificationCount(member_guid)
    }

    fun updateIsVerifed(member_guid: String?){
        kycDao!!.updateIsVerifed(member_guid)
    }

    fun getKycCount(kycType:Int,kycNumber:String,member_guid: String):Int{
        return kycDao!!.getKycCount(kycType, kycNumber, member_guid)
    }

    fun updateactivotaionstatus(memberkyc_guid: String?, status: Int){
        kycDao!!.updateactivotaionstatus(memberkyc_guid,status)
    }

}