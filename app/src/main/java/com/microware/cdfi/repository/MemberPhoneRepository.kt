package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.MemberPhoneDao
import com.microware.cdfi.entity.MemberPhoneDetailEntity

class MemberPhoneRepository {
    var Phonedetaildao: MemberPhoneDao? = null

    constructor(application: Application) {
        Phonedetaildao = CDFIApplication.database?.memberphoneDao()
    }

    fun getphonedetaildata(memberphoneGuid: String?): List<MemberPhoneDetailEntity>? {
        return Phonedetaildao!!.getphonedetaildata(memberphoneGuid)
    }

    fun getphonedetaillistdata(memberphoneGuid: String?): List<MemberPhoneDetailEntity>? {
        return Phonedetaildao!!.getphonedetaillistdata(memberphoneGuid)
    }

    fun getphoneData(memeberGUID: String?): LiveData<List<MemberPhoneDetailEntity>>? {
        return Phonedetaildao!!.getphoneData(memeberGUID)
    }
 fun  getphoneDatalist(memberGUID: String?): List<MemberPhoneDetailEntity>?{
        return Phonedetaildao!!.getphoneDatalist(memberGUID)
    }

 fun  getphoneDatalistcount(memberGUID: String?): List<MemberPhoneDetailEntity>?{
        return Phonedetaildao!!.getphoneDatalistcount(memberGUID)
    }

    fun insert(PhoneEntity: MemberPhoneDetailEntity) {
        insertAsyncTask(PhoneEntity, Phonedetaildao!!).execute()
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var PhoneEntity: MemberPhoneDetailEntity? = null
        var Phonedetaildao: MemberPhoneDao? = null

        constructor (
            PhoneEntity: MemberPhoneDetailEntity?,
            Phonedetaildao: MemberPhoneDao
        ) : this() {
            this.PhoneEntity = PhoneEntity
            this.Phonedetaildao = Phonedetaildao
        }

        override fun doInBackground(vararg params: Void?): String? {
            Phonedetaildao!!.insertPhoneDetailData(PhoneEntity)
            return null
        }

    }

    fun updatePhoneDetail(
        phone_guid: String,
        phone_no: String,
        isDefault: Int,
        phone_ownership: String,
        phone_ownership_detail: String,
        valid_from: Long,
        valid_till: Long,
        device: Int,
        is_edited: Int,
        updated_date: Long,
        updated_by: String,
        isCompleted: Int
    ) {
        Phonedetaildao!!.phoneupdatePhoneDetail(
            phone_guid,
            phone_no,
            isDefault,
            phone_ownership,
            phone_ownership_detail,valid_from,valid_till,device,is_edited,updated_date,updated_by,isCompleted
        )
    }

    fun deleteData(phone_guid: String?){
        Phonedetaildao!!.deleteData(phone_guid)
    }
    fun deleteRecord(phone_guid: String?){
        Phonedetaildao!!.deleteRecord(phone_guid)
    }

    fun getCompletionCount(member_guid: String?):Int{
        return Phonedetaildao!!.getCompletionCount(member_guid)
    }

    fun getVerificationCount(member_guid: String?):Int{
        return Phonedetaildao!!.getVerificationCount(member_guid)
    }

    fun updateIsVerifed(member_guid: String?,updated_date: Long,updated_by: String){
        Phonedetaildao!!.updateIsVerifed(member_guid,updated_date,updated_by)
    }

    fun getPhoneCount(phone_no:String,member_guid: String?):Int{
        return Phonedetaildao!!.getPhoneCount(phone_no,member_guid)
    }
}