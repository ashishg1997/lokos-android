package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.api.model.QueuestatusModel
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.MemberDao
import com.microware.cdfi.entity.MemberEntity

class MemberRepository {

    var memberDao: MemberDao? = null

    constructor(application: Application) {
        memberDao = CDFIApplication.database?.memberDao()
    }

    internal fun getAllMember(): LiveData<List<MemberEntity>>? {
        return memberDao!!.getAllMember()
    }

    internal fun getmember(memberGUID: String?): List<MemberEntity>? {
        return memberDao!!.getMember(memberGUID)
    }

   internal fun getMemberdetail(memberGUID: String?): List<MemberEntity>? {
        return memberDao!!.getMemberdetail(memberGUID)
    }

    internal fun getAllMember(shg_guid: String?): LiveData<List<MemberEntity>>? {
        return memberDao!!.getAllMember(shg_guid)
    }
     internal fun getActiveMember(shg_guid: String?): LiveData<List<MemberEntity>>? {
        return memberDao!!.getActiveMember(shg_guid)
    }
    internal fun getMemberuploadlist(shg_guid: String?): List<MemberEntity>? {
        return memberDao!!.getMemberuploadlist(shg_guid)
    }
    internal fun getAllMemberlist(shg_guid: String?): List<MemberEntity>? {
        return memberDao!!.getAllMemberlist(shg_guid)
    }

    internal fun getAllMemberDeslist(shg_guid: String?): List<MemberEntity>? {
        return memberDao!!.getAllMemberDeslist(shg_guid)
    }
    internal fun getcount(shg_guid: String?): Int {
        return memberDao!!.getcount(shg_guid)
    }

    internal fun getseqno(shg_guid: String?): Int {
        return memberDao!!.getseqno(shg_guid)
    }

    fun insert(memberEntity: MemberEntity) {
        insertAsyncTask(memberEntity, memberDao!!).execute()
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var memberEntity: MemberEntity? = null
        var memberDao: MemberDao? = null

        constructor (memberEntity: MemberEntity?, memberDao: MemberDao) : this() {
            this.memberEntity = memberEntity
            this.memberDao = memberDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            memberDao!!.insertMemberData(memberEntity)
            return null
        }

    }

    fun updateMemberDetail(
        member_guid: String,
        Seq_no: Int,
        member_name: String,
        member_name_local: String,
        Father_Husband: Int,
        Relation_Name: String,
        Relation_Name_Local: String,
        gender: Int,
        marital_status: Int,
        religion: Int,
        Social_Category: Int,
        TribalCategory: Int?,
        BPL: Int,
        BPL_Number: String,
        PIP_Category: Int,
        PIP_Date: Int,
        HighestEducationLevel: Int,
        DOB_Available: Int,
        DOB: Long,
        Age: Int,
        AgeAsOn: Long,
        Minority: Int,
        IsDisabled: Int,
        DisabilityDetails: String,
        WellbeingCategory: Int,
        PrimaryOccupation: Int,
        SecondaryOccupation:Int,
        TertiaryOccupation:Int,
        joining_date: Long,
        leaving_date: Long,
        ReasonForLeaving: Int,
        IfMinor_MemberReplaced: Int,
        GuardianName: String,
        GuardianName_Local: String,
        Guardian_Relation: Int,
        designation: Int,
        status: Int,
        house_hold_code: Int,
        head_house_hold: Int,
        Insurance: Int,
        marked_as_defaulter: Int,
        marked_as_defaulter_date: Int,
        RecordModified: String,
        updated_date: Long,
        updated_by: String,
        settlement_status: Int,
        imageName: String,
        isCompleted: Int
    ) {
        memberDao!!.updateMemberDetail(
            member_guid,
            Seq_no,
            member_name,
            member_name_local,
            Father_Husband,
            Relation_Name,
            Relation_Name_Local,
            gender,
            marital_status,
            religion,
            Social_Category,
            TribalCategory, BPL, BPL_Number, PIP_Category,PIP_Date,HighestEducationLevel,DOB_Available,DOB,Age,
            AgeAsOn,Minority,IsDisabled,DisabilityDetails,WellbeingCategory,PrimaryOccupation,
            SecondaryOccupation,TertiaryOccupation,joining_date,
            leaving_date,ReasonForLeaving,IfMinor_MemberReplaced,GuardianName,GuardianName_Local,Guardian_Relation
            ,designation,status,house_hold_code,head_house_hold,Insurance,marked_as_defaulter,
            marked_as_defaulter_date,RecordModified,updated_date,updated_by,settlement_status,imageName,isCompleted
        )
    }

    fun updateMemberDedupStatus(
        guid: String,
        code: String,
        activationStatus: Int,
        approve_status: Int,
        checker_remarks: String

    ){
        memberDao!!.updateMemberDedupStatus(
            guid,
            code,
            activationStatus,
            approve_status,
            checker_remarks

        )
    }

    fun updateMemberuploadStatus(
        guid: String, transactionid: String,
        lastupdatedate: Long)
    {
        memberDao!!.updateMemberuploadStatus(
            guid,transactionid,
            lastupdatedate)
    }

    fun getMemberlistdata(memberGUID: String?): LiveData<List<MemberEntity>>??{
        return memberDao!!.getMemberlistdata(memberGUID)
    }

    internal fun deleteMemberByMemberGuid(memberGUID: String?){
        memberDao!!.deleteMemberByMemberGuid(memberGUID)
    }

    internal fun deleteMemberAddressByMemberGuid(memberGUID: String?){
        memberDao!!.deleteMemberAddressByMemberGuid(memberGUID)
    }

    internal fun deleteMemberPhoneByMemberGuid(memberGUID: String?){
        memberDao!!.deleteMemberPhoneByMemberGuid(memberGUID)
    }

   internal fun deleteMemberBankByMemberGuid(memberGUID: String?){
        memberDao!!.deleteMemberBankByMemberGuid(memberGUID)
    }

    internal fun deleteMemberKycByMemberGuid(memberGUID: String?){
        memberDao!!.deleteMemberKycByMemberGuid(memberGUID)
    }

    internal fun deleteMemberSystemTagByMemberGuid(memberGUID: String?){
        memberDao!!.deleteMemberSystemTagByMemberGuid(memberGUID)
    }
         internal fun deleteMemberDataByMemberGuid(memberGUID: String?){
        memberDao!!.deleteMemberDataByMemberGuid(memberGUID)
    }

    internal fun deleteMemberAddressDataByMemberGuid(memberGUID: String?){
        memberDao!!.deleteMemberAddressDataByMemberGuid(memberGUID)
    }

    internal fun deleteMemberPhoneDataByMemberGuid(memberGUID: String?){
        memberDao!!.deleteMemberPhoneDataByMemberGuid(memberGUID)
    }

   internal fun deleteMemberBankDataByMemberGuid(memberGUID: String?){
        memberDao!!.deleteMemberBankDataByMemberGuid(memberGUID)
    }

    internal fun deleteMemberKycDataByMemberGuid(memberGUID: String?){
        memberDao!!.deleteMemberKycDataByMemberGuid(memberGUID)
    }

    internal fun deleteMemberSystemTagDataByMemberGuid(memberGUID: String?){
        memberDao!!.deleteMemberSystemTagDataByMemberGuid(memberGUID)
    }

    internal  fun getIsVerified(GUID:String?):Int{
        return  memberDao!!.getIsVerified(GUID)
    }

    internal  fun updateIsVerified(guid: String,is_verified:Int,updated_date: Long,updated_by: String){
        return   memberDao!!.updateIsVerified(guid,is_verified,updated_date,updated_by)
    }

    fun getCompletionCount(cbo_guid: String?):Int{
        return memberDao!!.getCompletionCount(cbo_guid)
    }

    fun getVerificationCount(cbo_guid: String?):Int{
        return memberDao!!.getVerificationCount(cbo_guid)
    }

    fun getFinalVerifyCount(member_guid: String?):Int{
        return memberDao!!.getFinalVerifyCount(member_guid)
    }

    fun updateIsVerifed(shgguid: String?,updated_date: Long,updated_by: String){
        memberDao!!.updateIsVerifed(shgguid,updated_date,updated_by)
    }

    internal fun getimage(member_id: Long?): String? {
        return memberDao?.getimage(member_id)
    }

    internal fun getOfficeBearerCount(shg_guid: String?,designationId:List<Int>): Int{
        return memberDao!!.getOfficeBearerCount(shg_guid,designationId)
    }


    fun getmemberList(shgGuid:String):List<QueuestatusModel>{
        return memberDao!!.getmemberList(shgGuid)
    }

    fun getMemberSummary(memberGUID: String?): LiveData<List<MemberEntity>>?{
        return memberDao!!.getMemberSummary(memberGUID)
    }
}