package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.MemberSystemtagDao
import com.microware.cdfi.entity.MemberSystemTagEntity

class MemberSystemtagRepository {

    var memberSystemtagDao: MemberSystemtagDao? = null

    constructor(application: Application) {
        memberSystemtagDao = CDFIApplication.database?.memberSystemtagDao()
    }



    fun getSystemtagdata(cbuguid: String?): LiveData<List<MemberSystemTagEntity>>? {
        return memberSystemtagDao!!.getSystemtagdata(cbuguid)
    }
    fun getSystemtagdatalist(member_guid: String?): List<MemberSystemTagEntity>? {
        return memberSystemtagDao!!.getSystemtagdatalist(member_guid)
    }
      fun getSystemtagdatalistcount(member_guid: String?): List<MemberSystemTagEntity>? {
        return memberSystemtagDao!!.getSystemtagdatalistcount(member_guid)
    }
    fun getSystemtag(systemguid: String?): List<MemberSystemTagEntity>? {
        return memberSystemtagDao!!.getSystemtag(systemguid)
    }

    fun insert(systemTagEntity: MemberSystemTagEntity) {
        insertAsyncTask(systemTagEntity, memberSystemtagDao!!).execute()
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var systemTagEntity: MemberSystemTagEntity? = null
        var memberSystemtagDao: MemberSystemtagDao? = null

        constructor (systemTagEntity: MemberSystemTagEntity?, memberSystemtagDao: MemberSystemtagDao) : this() {
            this.systemTagEntity = systemTagEntity
            this.memberSystemtagDao = memberSystemtagDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            memberSystemtagDao!!.insertSystemtagData(systemTagEntity)
            return null
        }

    }

    fun updateSystemTagdata(system_type:Int,system_id:String,systemtag_guid:String,updated_date:Long,updated_by:String){
        memberSystemtagDao!!.updateSystemTagdata(system_type,system_id,systemtag_guid,updated_date,updated_by)
    }
    fun deleteData(systemtag_guid: String?){
        memberSystemtagDao!!.deleteData(systemtag_guid)
    }

    fun deleteRecord(systemtag_guid: String?){
        memberSystemtagDao!!.deleteRecord(systemtag_guid)
    }

    internal fun getSystemtagcount(systemtagid: String): Int{
        return memberSystemtagDao!!.getSystemtagcount(systemtagid)
    }
}