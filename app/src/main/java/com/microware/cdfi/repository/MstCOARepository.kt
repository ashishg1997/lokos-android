package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.MstCOADao
import com.microware.cdfi.entity.MstCOAEntity

class MstCOARepository {
    var mstCOADao: MstCOADao? = null

    constructor(application: Application) {
        mstCOADao = CDFIApplication.database?.mstCOADao()
    }

    internal fun getMstCOAdata(type: String,languageCode:String): List<MstCOAEntity>?{
        return mstCOADao!!.getMstCOAdata(type,languageCode)
    }

    fun getMstCOAdatanotin(uid:List<Int>,type: String,languageCode:String): List<MstCOAEntity>?{
        return mstCOADao!!.getMstCOAdatanotin(uid,type,languageCode)
    }

    internal fun getcoaValue(type:String?,languageID:String?,keycode:Int?): String?{
        return mstCOADao!!.getcoaValue(type, languageID, keycode)
    }

    fun insert(mstCOAEntity: MstCOAEntity) {
        insertAsyncTask(mstCOAEntity, mstCOADao!!).execute()
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var mstCOAEntity: MstCOAEntity? = null
        var mstCOADao: MstCOADao? = null

        constructor (
            mstCOAEntity: MstCOAEntity?,
            mstCOADao: MstCOADao
        ) : this() {
            this.mstCOAEntity = mstCOAEntity
            this.mstCOADao = mstCOADao
        }

        override fun doInBackground(vararg params: Void?): String? {
            mstCOADao!!.insertMstCOAData(mstCOAEntity!!)
            return null
        }
    }

    fun deleteRecord(uid: Int) {
        mstCOADao!!.deleteRecord(uid)
    }
    internal fun getCoaSubHeadlist(uid:List<Int>,languageCode:String): List<MstCOAEntity>?{
        return mstCOADao!!.getCoaSubHeadlist(uid,languageCode)
    }
    internal fun getCoaSubHeadData(
        uid: List<Int>,
        type: String,
        languageCode: String
    ): List<MstCOAEntity>? {
        return mstCOADao!!.getCoaSubHeadData(uid, type, languageCode)
    }
    fun getReceiptCoaSubHeadData(
        type: String,
        type1: String,
        languageCode: String
    ): List<MstCOAEntity>? {
        return mstCOADao!!.getReceiptCoaSubHeadData(type, type1, languageCode)
    }
    fun getCoaSubAmount(uid:Int) : Int{
        return mstCOADao!!.getCoaSubAmount(uid)
    }
    fun getCoaSubHeadname(auid:Int,languageCode:String): String{
        return mstCOADao!!.getCoaSubHeadname(auid,languageCode)
    }


    fun getCoaSubAmountGroup(uid:Int,mtgNo:Int) : Int{
        return mstCOADao!!.getCoaSubAmountGroup(uid, mtgNo)
    }

    internal fun getCOADesValue(languageID:String?,keycode:Int?): String?{
        return mstCOADao!!.getCOADesValue( languageID, keycode)
    }
}