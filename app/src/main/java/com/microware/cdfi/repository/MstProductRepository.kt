package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.CboAddressDao
import com.microware.cdfi.dao.MstProductDao
import com.microware.cdfi.dao.PanchayatDao
import com.microware.cdfi.entity.*
import com.microware.miracle.dao.BlockDao
import com.microware.miracle.dao.DistrictDao
import com.microware.miracle.dao.StateDao
import com.microware.miracle.dao.VillageDao

class MstProductRepository{
    var mstProductDao: MstProductDao? = null

    constructor(application: Application){
        mstProductDao = CDFIApplication.database?.mstProductDao()
    }

    fun getproductlist(): List<LoanProductEntity>? {
        return mstProductDao!!.getproductlist()
    }



}