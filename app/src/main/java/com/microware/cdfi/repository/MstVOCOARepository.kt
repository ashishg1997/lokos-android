package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.vodao.MstVOCOADao
import com.microware.cdfi.entity.MstVOCOAEntity

class MstVOCOARepository {
    var mstVOCOADao: MstVOCOADao? = null

    constructor(application: Application) {
        mstVOCOADao = CDFIApplication.database?.mstVOCOADao()
    }

    internal fun getMstCOAdata(type: String,languageCode:String): List<MstVOCOAEntity>?{
        return mstVOCOADao!!.getMstCOAdata(type,languageCode)
    }

    internal fun getcoaValue(type:String?,languageID:String?,keycode:Int?): String?{
        return mstVOCOADao!!.getcoaValue(type, languageID, keycode)
    }

    fun insert(mstCOAEntity: MstVOCOAEntity) {
        insertAsyncTask(mstCOAEntity, mstVOCOADao!!).execute()
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var mstCOAEntity: MstVOCOAEntity? = null
        var mstVOCOADao: MstVOCOADao? = null

        constructor (
            mstCOAEntity: MstVOCOAEntity?,
            mstVOCOADao: MstVOCOADao
        ) : this() {
            this.mstCOAEntity = mstCOAEntity
            this.mstVOCOADao = mstVOCOADao
        }

        override fun doInBackground(vararg params: Void?): String? {
            mstVOCOADao!!.insertMstCOAData(mstCOAEntity!!)
            return null
        }
    }

    fun deleteRecord(uid: Int) {
        mstVOCOADao!!.deleteRecord(uid)
    }
    internal fun getCoaSubHeadlist(uid:List<Int>,languageCode:String): List<MstVOCOAEntity>?{
        return mstVOCOADao!!.getCoaSubHeadlist(uid,languageCode)
    }
    internal fun getCoaSubHeadData(
        receipt:Int,
        type: List<String>,
        languageID: String?
    ): List<MstVOCOAEntity>? {
        return mstVOCOADao!!.getCoaSubHeadData(receipt, type, languageID)
    }
    fun getReceiptCoaSubHeadData(
        type: String,
        type1: String,
        languageCode: String
    ): List<MstVOCOAEntity>? {
        return mstVOCOADao!!.getReceiptCoaSubHeadData(type, type1, languageCode)
    }
    fun getCoaSubAmount(uid:Int) : Int{
        return mstVOCOADao!!.getCoaSubAmount(uid)
    }

    fun getAllVoCoaSubHead(
        type: List<String>,
        languageCode: String
    ): LiveData<List<MstVOCOAEntity>?> {
        return mstVOCOADao!!.getAllVoCoaSubHead(type, languageCode)
    }
    fun getAlllist(
        type: List<String>,
        languageCode: String
    ): List<MstVOCOAEntity>? {
        return mstVOCOADao!!.getAlllist(type, languageCode)
    }
    fun getCoaSubHeadname(auid:Int,languageCode:String): String{
        return mstVOCOADao!!.getCoaSubHeadname(auid,languageCode)
    }

    fun getFundTypeId(receiptPayment:Int,type:String,uid:Int):String{
        return mstVOCOADao!!.getFundTypeId(receiptPayment, type, uid)
    }
}