package com.microware.cdfi.repository

import android.app.Application
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.ResponseCodeDao

class ResponseRepository {

    var responseDao:ResponseCodeDao? = null

    constructor(application: Application){
        responseDao = CDFIApplication.database?.responseCodeDao()
    }

    internal fun getResponseMsg(response_code :Int,language_id:String): String{
        return responseDao!!.getResponseMsg(response_code,language_id)
    }
}