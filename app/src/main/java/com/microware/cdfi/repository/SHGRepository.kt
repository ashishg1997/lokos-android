package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import androidx.room.Query
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.SHGDao
import com.microware.cdfi.entity.SHGEntity

class SHGRepository {

    var shgDao: SHGDao? = null

    constructor(application: Application) {
        shgDao = CDFIApplication.database?.shgDao()
    }

    internal fun getAllSHG(village_id:Int): LiveData<List<SHGEntity>>? {
        return shgDao!!.getAllSHG(village_id)
    }
    internal fun getAll(): LiveData<List<SHGEntity>>? {
        return shgDao!!.getAll()
    }

    internal fun getSHG(shgGUID:String?): List<SHGEntity>? {
        return shgDao!!.getSHG(shgGUID)
    }
    internal fun getSHGdetail(shgGUID:String?): List<SHGEntity>? {
        return shgDao!!.getSHGdetail(shgGUID)
    }

    internal fun getShgCount(villageid:Int):Int{
        return shgDao!!.getShgCount(villageid)
    }
    internal fun getShgpendingCount(villageid:Int):Int{
        return shgDao!!.getShgpendingCount(villageid)
    }
    internal fun getShgactiveCount(villageid:Int):Int{
        return shgDao!!.getShgactiveCount(villageid)
    }

    internal fun getallShgCount():Int{
        return shgDao!!.getallShgCount()
    }
    internal fun getallShgpendingCount():Int{
        return shgDao!!.getallShgpendingCount()
    }
    internal fun getallShgactiveCount():Int{
        return shgDao!!.getallShgactiveCount()
    }


    fun insert(shgEntity: SHGEntity) {
        insertAsyncTask(shgEntity, shgDao!!).execute()
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var shgEntity: SHGEntity? = null
        var shgDao: SHGDao? = null

        constructor (shgEntity: SHGEntity?, shgDao: SHGDao) : this() {
            this.shgEntity = shgEntity
            this.shgDao = shgDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            shgDao!!.insertShgData(shgEntity)
            return null
        }

    }

    fun updateBasicDetail(
        GUID: String,
        HamletID: String,
        shg_name: String,
        shg_name_short_EN: String,
        shg_name_short_local: String,
        shg_type_code: Int,
        shg_name_local: String,
        composition: Int,
        shg_formation_date: Long,
        shg_revival_date: Long,
        shg_cooption_date: Long,
        SHG_PromotedBy: Int,
        SHG_RevivedBy: Int,
        meeting_frequency: Int,
        meeting_frequency_value: Int,
        meeting_on: Int,
        mode: Int,
        month_comp_saving: Int,
        is_bankaccount: Int,
        funding_agency_id: Int,
        parent_cbo_code: Int,
        parent_cbo_type: Int,
        Status: Int,
        is_active: Int,
        dedupl_status: Int,
        AccountBooksMaintained: Int,
        CashBookSTartDate: Int,
        BankBooKStartDate: Int,
        MembersLedgerStartDate: Int,
        Book4: Int,
        Book5: Int,
        Grade: String,
        GradingDoneOn: Int,
        GradeConfirmationStatus: String,
        bookkeeper_identified: Int,
        micro_plan_number: Int,
        WebDefaultChecker: Int,
        entry_source: Int,
        latitude: String,
        longitude: String,
        updated_date: Long,
        updated_by: String,
        primaryActivity:Int,
        secondaryActivity:Int,
        tertiaryActivity:Int,
        bookkeeper_name:String,
        bookkeeper_mobile:String,
        election_tenure:Int,
        ComsavingRoi:Double,
        voluntary_savings_interest:Double,
        is_volutary_saving:Int,
        saving_frequency:Int,
        tags:Int,
        promoter_name:String,
        promoter_code:Int?,
        imageName:String,
        shg_type_other:String,
        isVerified:Int,
        inactiveReason:Int
    ) {
        shgDao!!.updateBasicDetail(
            GUID,
            HamletID,
            shg_name,
            shg_name_short_EN,
            shg_name_short_local,
            shg_type_code,
            shg_name_local,
            composition,
            shg_formation_date,
            shg_revival_date,shg_cooption_date,
            SHG_PromotedBy, SHG_RevivedBy, meeting_frequency, meeting_frequency_value,meeting_on,mode,month_comp_saving,is_bankaccount,funding_agency_id,
            parent_cbo_code,parent_cbo_type,Status,is_active,dedupl_status,AccountBooksMaintained,
            CashBookSTartDate,BankBooKStartDate,MembersLedgerStartDate,Book4,Book5,Grade
            ,GradingDoneOn,GradeConfirmationStatus,bookkeeper_identified,micro_plan_number,WebDefaultChecker,entry_source,
            latitude,longitude,updated_date,updated_by,primaryActivity,secondaryActivity,tertiaryActivity,
            bookkeeper_name,
            bookkeeper_mobile,
            election_tenure,
            ComsavingRoi,
            voluntary_savings_interest,
            is_volutary_saving,
            saving_frequency,
            tags,promoter_name,promoter_code,imageName,shg_type_other,isVerified,
            inactiveReason
        )
    }

    internal fun getAllSHGlist(): List<SHGEntity>? {
        return shgDao!!.getAllSHGlist()
    }

    internal fun getAllSHGlistdata(): List<SHGEntity>? {
        return shgDao!!.getAllSHGlistdata()
    }

    fun getPendingSHGlistdata(): List<SHGEntity>?{
        return shgDao!!.getPendingSHGlistdata()
    }

    fun updateShguploadStatus(
        guid: String,
        remarks: String,
        user_id: String

    ){
        shgDao!!.updateShguploadStatus(
            guid,
            remarks,
            user_id

        )
    }

    fun updateShgDedupStatus(
        guid: String,
        code: String,
        activationStatus: Int,
        approve_status: Int,
        checker_remarks: String


    ){
        shgDao!!.updateShgDedupStatus(
            guid,
            code,
            activationStatus,
            approve_status,
            checker_remarks)
    }
    fun getSHG_count(village_id:Int): Int{
        return shgDao!!.getSHG_count(village_id)
    }

    fun getlastdate(): Long{
        return shgDao!!.getlastdate()
    }

    fun getSHGlistdata(shgGUID: String?): LiveData<List<SHGEntity>>??{
        return shgDao!!.getSHGlistdata(shgGUID)
    }
    fun updateCheckerRemarks(checker_remark: String,shgGUID: String){
        shgDao!!.updateCheckerRemarks(checker_remark,shgGUID)
    }

    internal fun deleteShgByGuid(shgGUID: String?){
        return shgDao!!.deleteShgByGuid(shgGUID)
    }

    internal fun deleteBankByShgGuid(shgGUID: String?){
        return shgDao!!.deleteBankByShgGuid(shgGUID)
    }

    internal fun deletePhoneByShgGuid(shgGUID: String?){
        return shgDao!!.deletePhoneByShgGuid(shgGUID)
    }

    internal fun deleteAddressByShgGuid(shgGUID: String?){
        return shgDao!!.deleteAddressByShgGuid(shgGUID)
    }

    internal fun deleteSystemTagByShgGuid(shgGUID: String?){
        return shgDao!!.deleteSystemTagByShgGuid(shgGUID)
    }

    internal fun deleteDesignationByShgGuid(shgGUID: String?){
        return shgDao!!.deleteDesignationByShgGuid(shgGUID)
    }

    internal fun deleteMemberByShgGuid(shgGUID: String?){
        return shgDao!!.deleteMemberByShgGuid(shgGUID)
    }

    internal fun deleteMemberAddressByShgGuid(shgGUID: String?){
        return shgDao!!.deleteMemberAddressByShgGuid(shgGUID)
    }

    internal fun deleteMemberPhoneByShgGuid(shgGUID: String?){
        return shgDao!!.deleteMemberPhoneByShgGuid(shgGUID)
    }

    internal fun deleteMemberBankByShgGuid(shgGUID: String?){
        return shgDao!!.deleteMemberBankByShgGuid(shgGUID)
    }

    internal fun deleteMemberKycByShgGuid(shgGUID: String?){
        return shgDao!!.deleteMemberKycByShgGuid(shgGUID)
    }

    internal fun deleteMemberSystemTagByShgGuid(shgGUID: String?){
        return shgDao!!.deleteMemberSystemTagByShgGuid(shgGUID)
    }

    internal fun deleteShgDataByGuid(shgGUID: String?){
        return shgDao!!.deleteShgDataByGuid(shgGUID)
    }

    internal fun deleteBankDataByShgGuid(shgGUID: String?){
        return shgDao!!.deleteBankDataByShgGuid(shgGUID)
    }

    internal fun deletePhoneDataByShgGuid(shgGUID: String?){
        return shgDao!!.deletePhoneDataByShgGuid(shgGUID)
    }

    internal fun deleteAddressDataByShgGuid(shgGUID: String?){
        return shgDao!!.deleteAddressDataByShgGuid(shgGUID)
    }

    internal fun deleteSystemTagDataByShgGuid(shgGUID: String?){
        return shgDao!!.deleteSystemTagDataByShgGuid(shgGUID)
    }

    internal fun deleteDesignationDataByShgGuid(shgGUID: String?){
        return shgDao!!.deleteDesignationDataByShgGuid(shgGUID)
    }

    internal fun deleteMemberDataByShgGuid(shgGUID: String?){
        return shgDao!!.deleteMemberDataByShgGuid(shgGUID)
    }

    internal fun deleteMemberAddressDataByShgGuid(shgGUID: String?){
        return shgDao!!.deleteMemberAddressDataByShgGuid(shgGUID)
    }

    internal fun deleteMemberPhoneDataByShgGuid(shgGUID: String?){
        return shgDao!!.deleteMemberPhoneDataByShgGuid(shgGUID)
    }

    internal fun deleteMemberBankDataByShgGuid(shgGUID: String?){
        return shgDao!!.deleteMemberBankDataByShgGuid(shgGUID)
    }

    internal fun deleteMemberKycDataByShgGuid(shgGUID: String?){
        return shgDao!!.deleteMemberKycDataByShgGuid(shgGUID)
    }

    internal fun deleteMemberSystemTagDataByShgGuid(shgGUID: String?){
        return shgDao!!.deleteMemberSystemTagDataByShgGuid(shgGUID)
    }

    internal  fun getIsVerified(shgGUID:String?):Int{
        return   shgDao!!.getIsVerified(shgGUID)
    }

    fun getFinalVerification(shgGUID:String?):Int{
        return shgDao!!.getFinalVerification(shgGUID)
    }
    internal  fun updateIsVerified(guid: String,is_verified:Int,updated_date:Long,updated_by:String){
        return   shgDao!!.updateIsVerified(guid,is_verified,updated_date,updated_by)
    }

    internal  fun getIfscCode():List<String>{
        return shgDao!!.getIfscCode()
    }

    internal fun getBankCount(guid:String?):Int{
        return shgDao!!.getBankCount(guid)
    }

    internal fun getSignatoryCount(shgGUID: String?):Int{
        return shgDao!!.getSignatoryCount(shgGUID)
    }

    fun updateLockingFlag(isLocked:Int,isEdited:Int,shgGUID: String?){
        shgDao!!.updateLockingFlag(isLocked,isEdited,shgGUID)
    }

   fun updateMemberLockingFlag(isLocked:Int,isEdited:Int,shgGUID: String?){
        shgDao!!.updateMemberLockingFlag(isLocked,isEdited,shgGUID)
    }

    fun getLockingFlag(shgGUID:String?):Int{
       return shgDao!!.getLockingFlag(shgGUID)
    }

    fun getSHGname(ShgGUID: String?): String{
        return  shgDao!!.getSHGname(ShgGUID)
    }
}