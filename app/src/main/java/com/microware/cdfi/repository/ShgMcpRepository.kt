package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.IncomeandExpenditureDao
import com.microware.cdfi.dao.ShgMcpDao
import com.microware.cdfi.entity.ShgFinancialTxnDetailEntity
import com.microware.cdfi.entity.ShgMcpEntity

class ShgMcpRepository {

    var shgMcpDao: ShgMcpDao? = null

    constructor(application: Application){
        shgMcpDao = CDFIApplication.database?.shgmcpDao()
    }

    fun insert(shgMcpEntity : ShgMcpEntity) {
        insertAsyncTask(shgMcpEntity, shgMcpDao!!).execute()
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var shgMcpEntity: ShgMcpEntity? = null
        var shgMcpDao: ShgMcpDao? = null

        constructor (shgMcpEntity: ShgMcpEntity?, shgMcpDao: ShgMcpDao) : this() {
            this.shgMcpEntity = shgMcpEntity
            this.shgMcpDao = shgMcpDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            shgMcpDao!!.insertShgMcpData(shgMcpEntity!!)
            return null
        }
    }



    fun getShgMcpDataAlllist(mcp_id: Long,cbo_id: Long,mem_id: Long): List<ShgMcpEntity>? {
        return shgMcpDao!!.getShgMcpDataAlllist(mcp_id, cbo_id, mem_id)
    }

    fun getShgMcpDatalist(cbo_id: Long): List<ShgMcpEntity>? {
        return shgMcpDao!!.getShgMcpDatalist(cbo_id)
    }

    internal fun gettotaldemand(cboid: Long): Int {
        return shgMcpDao!!.gettotaldemand(cboid)
    }

    internal fun getCount(cboid: Long): Int {
        return shgMcpDao!!.getCount(cboid)
    }

    fun deleteAll(){
        return shgMcpDao!!.deleteAll()
    }

    fun updateMcpPrepration(
        mcp_id: Long,
        cbo_id: Long,
        mem_id: Long,
        amt_demand: Int,
        tentative_date: Long,
        loan_product_id: Int?,
        loan_source: Int?,
        loan_purpose: Int?,
        loan_period: Int?,
        loan_requested_mtg_no: Int?,
        loan_requested_mtg_guid: String?,
        loan_sanctioned_mtg_no: Int?,
        loan_sanctioned_mtg_guid: String?,
        loan_request_priority: Int?,
        proposed_emi_amount: Int?,
        updatedby: String?,
        updatedon: Long?
    ) {
        shgMcpDao!!.updateMcpPrepration(
            mcp_id,
            cbo_id,
            mem_id,
            amt_demand,
            tentative_date,
            loan_product_id,
            loan_source,
            loan_purpose,
            loan_period,
            loan_requested_mtg_no,
            loan_requested_mtg_guid,
            loan_sanctioned_mtg_no,
            loan_sanctioned_mtg_guid,
            loan_request_priority,
            proposed_emi_amount,
            updatedby,
            updatedon
        )
    }

    fun getUploadListData(mtg_no:Int,cbo_id:Long) : List<ShgMcpEntity>{
        return shgMcpDao!!.getUploadListData(mtg_no,cbo_id)
    }

}