package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.api.model.EcScModelJoindata
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.ExecutiveMemberDao
import com.microware.cdfi.dao.Subcommitee_memberDao
import com.microware.cdfi.entity.VoShgMemberPhoneEntity
import com.microware.cdfi.entity.subcommitee_memberEntity

class Subcommitee_memberRepository {

    var memberDao: Subcommitee_memberDao? = null
    var executiveDao: ExecutiveMemberDao? = null

    constructor(application: Application){
        memberDao = CDFIApplication.database?.subcommitee_memberDao()
        executiveDao = CDFIApplication.database?.executiveDao()
    }

    fun insert(memberEntity: subcommitee_memberEntity){
        insertAsyncTask(memberEntity, memberDao!!).execute()
    }


    class insertAsyncTask(): AsyncTask<Void, Void, String>(){

        private var memberEntity: subcommitee_memberEntity? = null
        var memberDao: Subcommitee_memberDao? = null

        constructor (memberEntity: subcommitee_memberEntity?, memberDao: Subcommitee_memberDao) : this() {
            this.memberEntity = memberEntity
            this.memberDao = memberDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            memberDao!!.insertCommitteeMemberData(memberEntity)
            return null
        }

    }

    fun getCommitteeMemberdetaildata(subcommitee_guid: String?): List<subcommitee_memberEntity>?{
        return memberDao!!.getCommitteeMemberdetaildata(subcommitee_guid)
    }

    fun getCommitteeMemberdata(subcommitee_guid: String?,ec_member_code: Long?): List<subcommitee_memberEntity>?{
        return memberDao!!.getCommitteeMemberdata(subcommitee_guid,ec_member_code)
    }

    fun getExecutiveMemberList(cbo_guid: String?): List<EcScModelJoindata>?{
        return executiveDao!!.getExecutiveMemberList(cbo_guid)
    }

    fun getECMemberList(cbo_guid: String?): List<EcScModelJoindata>?{
        return executiveDao!!.getECMemberList(cbo_guid)
    }

    fun updateScMemberdata(subcommitee_guid: String,
                           ec_member_code: Long,
                           fromdate:Long?,
                           todate:Long?,
                           status:Int,
                           is_active:Int,
                           entry_source:Int,
                           is_edited:Int,
                           updated_date:Long,
                           updated_by:String,
                           is_complete:Int){
        memberDao!!.updateScMemberdata(subcommitee_guid,
            ec_member_code,
            fromdate,
            todate,
            status,
            is_active,
            entry_source,
            is_edited,
            updated_date,
            updated_by,
            is_complete)
    }

    fun getScMembercount(ec_member_code: Long?):Int{
        return memberDao!!.getScMembercount(ec_member_code)
    }

    fun getCommitteeMemberCount(subcommitee_guid: String?):Int{
        return memberDao!!.getCommitteeMemberCount(subcommitee_guid)
    }
    fun getMemberPhone(member_id:Long):List<VoShgMemberPhoneEntity>{
        return executiveDao!!.getMemberPhone(member_id)
    }
    fun getCommitteeMemberPendingdata(subcommitee_guid: String?): List<subcommitee_memberEntity>?{
        return memberDao!!.getCommitteeMemberPendingdata(subcommitee_guid)
    }
    fun getCLFExecutiveMemberList(cbo_guid: String?): List<EcScModelJoindata>?{
        return executiveDao!!.getCLFExecutiveMemberList(cbo_guid)
    }

    fun getScMembercountbySCGUID(ec_member_code: Long?,subcommitee_guid: String?):Int{
        return memberDao!!.getScMembercountbySCGUID(ec_member_code,subcommitee_guid)
    }
}