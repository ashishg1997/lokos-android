package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.Subcommitee_memberDao
import com.microware.cdfi.dao.SubcommitteeDao
import com.microware.cdfi.entity.subcommitee_memberEntity
import com.microware.cdfi.entity.subcommitteeEntity

class SubcommitteeRepository {

    var subcommitteeDao: SubcommitteeDao? = null
    var memberDao: Subcommitee_memberDao? = null

    constructor(application: Application){
        subcommitteeDao = CDFIApplication.database?.subcommitteeDao()
        memberDao = CDFIApplication.database?.subcommitee_memberDao()
    }

    fun insert(subcomitteeEntity: subcommitteeEntity){
        insertAsyncTask(subcomitteeEntity, subcommitteeDao!!).execute()
    }


    class insertAsyncTask(): AsyncTask<Void, Void, String>(){

        private var subcomitteeEntity: subcommitteeEntity? = null
        var subcommitteeDao: SubcommitteeDao? = null

        constructor (subcomitteeEntity: subcommitteeEntity?, subcommitteeDao: SubcommitteeDao) : this() {
            this.subcomitteeEntity = subcomitteeEntity
            this.subcommitteeDao = subcommitteeDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            subcommitteeDao!!.insertCommitteeData(subcomitteeEntity)
            return null
        }

    }

    fun getCommitteedetaildata(subcommitee_guid: String?): List<subcommitteeEntity>?{
        return subcommitteeDao!!.getCommitteedetaildata(subcommitee_guid)
    }

    fun getCommitteedata(cbo_guid: String?): LiveData<List<subcommitteeEntity>>?{
        return subcommitteeDao!!.getCommitteedata(cbo_guid)
    }

    fun updateCommitteedata(subcommitee_guid:String,
                            subcommitee_type_id:Int?,
                            fromdate:Long?,
                            todate:Long?,
                            is_active:Int?,
                            entry_source:Int?,
                            is_edited:Int?,
                            updated_date:Long?,
                            updated_by:String?,
                            status:Int){
        return subcommitteeDao!!.updateCommitteedata(subcommitee_guid,
            subcommitee_type_id,
            fromdate,
            todate,
            is_active,
            entry_source,
            is_edited,
            updated_date,
            updated_by,
            status)
    }

    fun getCommitteePendingdata():List<subcommitteeEntity>?{
        return subcommitteeDao!!.getCommitteePendingdata()
    }

    fun getCommitteeMemberPendingdata(subcommitee_guid: String?): List<subcommitee_memberEntity>?{
        return memberDao!!.getCommitteeMemberPendingdata(subcommitee_guid)
    }

    fun updateuploadStatus(
        guid: String,
        lastupdatedate: Long
    ){
        memberDao!!.updateuploadStatus(
            guid,lastupdatedate)
    }

    fun updateScuploadStatus(
        guid: String,
        lastupdatedate: Long
    ){
        subcommitteeDao!!.updateScuploadStatus(
            guid,lastupdatedate)
    }

    fun updateScCompleteStatus(
        guid: String,
        lastupdatedate: Long
    ){
        subcommitteeDao!!.updateScCompleteStatus(
            guid,lastupdatedate)
    }

    fun getScCount(scId: Int?,cboGuid:String?):Int{
        return subcommitteeDao!!.getScCount(scId,cboGuid)
    }

    fun updateScMemberStatus(subcommitee_guid: String,
                             todate:Long?,
                             entry_source:Int,
                             is_edited:Int,
                             updated_date:Long,
                             updated_by:String){
        memberDao!!.updateScMemberStatus(subcommitee_guid,
            todate,
            entry_source,
            is_edited,
            updated_date,
            updated_by)
    }
}