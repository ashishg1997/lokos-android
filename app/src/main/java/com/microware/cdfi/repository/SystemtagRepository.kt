package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.SystemtagDao
import com.microware.cdfi.entity.SystemTagEntity

class SystemtagRepository {

    var systemtagDao: SystemtagDao? = null

    constructor(application: Application) {
        systemtagDao = CDFIApplication.database?.systemtagDao()
    }



    fun getSystemtagdata(cbuguid: String?): LiveData<List<SystemTagEntity>>? {
        return systemtagDao!!.getSystemtagdata(cbuguid)
    }

    fun getSystemtagdatalist(cbuguid: String?): List<SystemTagEntity>? {
        return systemtagDao!!.getSystemtagdatalist(cbuguid)
    }
    fun getSystemtagdatalistcount(cbuguid: String?): List<SystemTagEntity>? {
        return systemtagDao!!.getSystemtagdatalistcount(cbuguid)
    }
    fun getSystemtag(systemguid: String?): List<SystemTagEntity>? {
        return systemtagDao!!.getSystemtag(systemguid)
    }

    fun insert(systemTagEntity: SystemTagEntity) {
        insertAsyncTask(systemTagEntity, systemtagDao!!).execute()
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var systemTagEntity: SystemTagEntity? = null
        var systemtagDao: SystemtagDao? = null

        constructor (systemTagEntity: SystemTagEntity?, systemtagDao: SystemtagDao) : this() {
            this.systemTagEntity = systemTagEntity
            this.systemtagDao = systemtagDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            systemtagDao!!.insertSystemtagData(systemTagEntity)
            return null
        }

    }

    fun updateSystemTagdata(system_type:Int,system_id:String,systemtag_guid:String,updated_date:Long,updated_by:String){
        systemtagDao!!.updateSystemTagdata(system_type,system_id,systemtag_guid,updated_date,updated_by)
    }
    fun deleteSystemtagData(systemtag_guid: String?){
        systemtagDao!!.deleteSystemtagData(systemtag_guid)
    }
    fun deleteRecord(systemtag_guid: String?){
        systemtagDao!!.deleteRecord(systemtag_guid)
    }
}