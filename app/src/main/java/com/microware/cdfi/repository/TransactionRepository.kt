package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.TransactionEntity
import com.microware.miracle.dao.TransactionDao

class TransactionRepository {
    var transactionDao: TransactionDao? = null

    constructor(application: Application){
        transactionDao = CDFIApplication.database?.transactionDao()
    }

    fun gettransaction(): List<TransactionEntity>?{
        return transactionDao!!.gettransaction()
    }

  fun getdatabytransaction(transactionno:String): List<TransactionEntity>?{
        return transactionDao!!.getdatabytransaction(transactionno)
    }

    fun insert(transactionEntity: TransactionEntity ){
        insertAsyncTask(transactionEntity,transactionDao!!).execute()
    }

    class insertAsyncTask(): AsyncTask<Void, Void, String>(){

        private var transactionEntity: TransactionEntity? = null
        var transactionDao: TransactionDao? = null

        constructor (transactionEntity: TransactionEntity?, transactionDao: TransactionDao) : this() {
            this.transactionEntity = transactionEntity
            this.transactionDao = transactionDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            transactionDao!!.insertransaction(transactionEntity)
            return null
        }

    }

    fun deleteTransactionByGuid(shgGUID: String?){
        transactionDao!!.deleteTransactionByGuid(shgGUID)
    }

    fun getUpdateQueueList(shgGuid:String) : LiveData<List<TransactionEntity>> {
        return transactionDao!!.getUpdateQueueList(shgGuid)
    }

}