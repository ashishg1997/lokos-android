package com.microware.cdfi.repository

import android.app.Application
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.UserEntity
import com.microware.cdfi.dao.UserDao

class UserRepository {
    var userDao: UserDao? = null

    constructor(application: Application){
        userDao = CDFIApplication.database?.userDao()
    }

    internal fun getUserData(): List<UserEntity>? {
        return userDao!!.getUserData()
    }

    internal fun getUserCount(): Int {
        return userDao!!.getUserCount()
    }

    internal fun getUserCountByUserId(userId:String): Int {
        return userDao!!.getUserCountByUserId(userId)
    }
}