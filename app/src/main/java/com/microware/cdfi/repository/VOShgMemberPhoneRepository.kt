package com.microware.cdfi.repository

import android.app.Application
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.VOShgMemberDao
import com.microware.cdfi.dao.VOShgMemberPhoneDao
import com.microware.cdfi.entity.VoShgMemberEntity
import com.microware.cdfi.entity.VoShgMemberPhoneEntity

class VOShgMemberPhoneRepository {

    var voShgMemberPhoneDao: VOShgMemberPhoneDao? = null

    constructor(application: Application){
        voShgMemberPhoneDao = CDFIApplication.database?.vOShgMemberPhoneDao()
    }

    fun getphonedetaillistdata(memberGuid:String):List<VoShgMemberPhoneEntity>?{
        return voShgMemberPhoneDao!!.getphonedetaillistdata(memberGuid)
    }
}