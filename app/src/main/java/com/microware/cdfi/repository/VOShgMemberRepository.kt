package com.microware.cdfi.repository

import android.app.Application
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.VOShgMemberDao
import com.microware.cdfi.entity.VoShgMemberEntity

class VOShgMemberRepository {

    var voShgMemberDao: VOShgMemberDao? = null

    constructor(application: Application){
        voShgMemberDao = CDFIApplication.database?.voShgMemberDao()
    }

    fun getMemberList(shg_guid:String):List<VoShgMemberEntity>?{
        return voShgMemberDao!!.getMemberList(shg_guid)
    }
}