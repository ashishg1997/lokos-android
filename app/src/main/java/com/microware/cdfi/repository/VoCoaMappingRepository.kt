package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.VoCoaMappingDao
import com.microware.cdfi.entity.VoCoaMappingEntity

class VoCoaMappingRepository {

    var voCoaMappingDao: VoCoaMappingDao? = null

    constructor(application: Application) {
        voCoaMappingDao = CDFIApplication.database?.voCoaMappingDao()
    }

    fun insert(voCoaMappingEntity: VoCoaMappingEntity) {
        insertAsyncTask(voCoaMappingEntity, voCoaMappingDao!!).execute()
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var voCoaMappingEntity: VoCoaMappingEntity? = null
        var voCoaMappingDao: VoCoaMappingDao? = null

        constructor (
            voCoaMappingEntity: VoCoaMappingEntity?,
            voCoaMappingDao: VoCoaMappingDao
        ) : this() {
            this.voCoaMappingEntity = voCoaMappingEntity
            this.voCoaMappingDao = voCoaMappingDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            voCoaMappingDao!!.insertCoaMapping(voCoaMappingEntity!!)
            return null
        }

    }

    fun getCoaMappinData(uid:Int,cboId:Long):List<VoCoaMappingEntity>{
        return voCoaMappingDao!!.getCoaMappinData(uid,cboId)
    }
    fun getaccountno(uid:Int,cboId:Long):String{
        return voCoaMappingDao!!.getaccountno(uid,cboId)
    }
    fun getcboData(cboId: Long): List<VoCoaMappingEntity> {
        return voCoaMappingDao!!.getcboData(cboId)
    }
 fun getcboDatalist(cboId: Long): LiveData<List<VoCoaMappingEntity>> {
        return voCoaMappingDao!!.getcboDatalist(cboId)
    }

    fun getBankCode(uid:Int,cboId:Long):String?{
        return voCoaMappingDao!!.getBankCode(uid,cboId)
    }

}