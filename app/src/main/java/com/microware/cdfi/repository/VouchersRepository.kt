package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.MemberDao
import com.microware.cdfi.dao.VouchersDao
import com.microware.cdfi.entity.ShgFinancialTxnVouchersEntity

class VouchersRepository {

    var vouchersDao:VouchersDao?=null

    constructor(application: Application) {
        vouchersDao = CDFIApplication.database?.vouchersDao()
    }

    fun insert(vouchersEntity: ShgFinancialTxnVouchersEntity) {
        //insertAsyncTask(vouchersEntity, vouchersDao!!).execute()
        vouchersDao!!.insertVoucher(vouchersEntity)
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var vouchersEntity: ShgFinancialTxnVouchersEntity? = null
        var vouchersDao: VouchersDao? = null

        constructor (vouchersEntity: ShgFinancialTxnVouchersEntity?, vouchersDao: VouchersDao) : this() {
            this.vouchersEntity = vouchersEntity
            this.vouchersDao = vouchersDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            vouchersDao!!.insertVoucher(vouchersEntity)
            return null
        }

    }

    fun getVouchers():List<ShgFinancialTxnVouchersEntity>?{
        return vouchersDao!!.getVouchers()
    }

    fun getMaxVoucherNum(mtgDate:Long): Int{
        return vouchersDao!!.getMaxVoucherNum(mtgDate)
    }

    fun getDataList(mtgdate:Long,cbo_id: Long): LiveData<List<ShgFinancialTxnVouchersEntity>>?{
        return vouchersDao!!.getDataList(mtgdate,cbo_id)
    }
    fun updateDateRelisation(date_realisation :Long,voucher_no: String, cboId: Long){
        vouchersDao!!.updateDateRelisation(date_realisation ,voucher_no, cboId)
    }

    fun getVoucherUploadList(mtgNo:Int,cboId: Long):List<ShgFinancialTxnVouchersEntity>{
        return vouchersDao!!.getVoucherUploadList(mtgNo,cboId)
    }
}