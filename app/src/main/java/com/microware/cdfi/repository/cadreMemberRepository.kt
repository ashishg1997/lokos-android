package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.cadreMemberDao
import com.microware.cdfi.entity.cadreShgMemberEntity

class cadreMemberRepository {

    var cadreDao: cadreMemberDao? = null

    constructor(application: Application) {
        cadreDao = CDFIApplication.database?.caderMemberDao()
    }

    internal fun insert(cadreEntity: cadreShgMemberEntity) {
        insertAsyncTask(cadreEntity, cadreDao!!).execute()
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var cadreEntity: cadreShgMemberEntity? = null
        var cadreDao: cadreMemberDao? = null

        constructor (cadreEntity: cadreShgMemberEntity?, cadreDao: cadreMemberDao) : this() {
            this.cadreEntity = cadreEntity
            this.cadreDao = cadreDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            cadreDao!!.insertCadreData(cadreEntity)
            return null
        }

    }

    internal fun getCadredetail(cadre_guid: String?): List<cadreShgMemberEntity>?{
        return cadreDao!!.getCadredetail(cadre_guid)
    }

    internal fun getCadreListdata(member_guid:String): LiveData<List<cadreShgMemberEntity>>?{
        return cadreDao!!.getCadreListdata(member_guid)
    }

    internal fun getCadreListdata1(member_guid: String): List<cadreShgMemberEntity>? {
        return cadreDao!!.getCadreListdata1(member_guid)
    }

    internal fun getCadrePendingdata(member_guid: String?): List<cadreShgMemberEntity>?{
        return cadreDao!!.getCadrePendingdata(member_guid)
    }

    internal fun updateCadreData(cadre_guid:String,
                                 cadre_role_code:Int?,
                                 cadre_cat_code:Int?,
                                 date_joining:Long?,
                                 date_leaving:Long?,
                                 updated_by:String?,
                                 updated_on:Long,
                                 is_edited:Int,
                                 is_complete:Int){
        cadreDao!!.updateCadreData(cadre_guid,
            cadre_role_code,
            cadre_cat_code,
            date_joining,
            date_leaving,
            updated_by,
            updated_on,
            is_edited,
            is_complete)
    }

}