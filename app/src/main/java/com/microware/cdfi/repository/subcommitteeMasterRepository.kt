package com.microware.cdfi.repository

import android.app.Application
import android.os.AsyncTask
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.Cbo_BankDao
import com.microware.cdfi.dao.subcommitteeMasterDao
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.entity.subcommitee_masterEntity

class subcommitteeMasterRepository {

    var subcommitteeMasterDao: subcommitteeMasterDao? = null

    constructor(application: Application) {
        subcommitteeMasterDao = CDFIApplication.database?.subcommitteeMasterDao()
    }

    fun getScMaster(languageID:String?):List<subcommitee_masterEntity>?{
        return  subcommitteeMasterDao!!.getScMaster(languageID)
    }
    fun getScMasterCount():Int{
        return  subcommitteeMasterDao!!.getScMasterCount()
    }

    fun insert(subcommitee_masterEntity : subcommitee_masterEntity){
        insertAsyncTask(subcommitee_masterEntity,subcommitteeMasterDao!!).execute()
    }

    class insertAsyncTask(): AsyncTask<Void, Void, String>(){

        private var subcommitee_masterEntity: subcommitee_masterEntity? = null
        var subcommitteeMasterDao: subcommitteeMasterDao? = null

        constructor (subcommitee_masterEntity: subcommitee_masterEntity?, subcommitteeMasterDao: subcommitteeMasterDao) : this() {
            this.subcommitee_masterEntity = subcommitee_masterEntity
            this.subcommitteeMasterDao = subcommitteeMasterDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            subcommitteeMasterDao!!.insertBankDetailData(subcommitee_masterEntity)
            return null
        }

    }

}