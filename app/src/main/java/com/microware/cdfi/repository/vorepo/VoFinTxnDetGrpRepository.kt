package com.microware.cdfi.repository.vorepo

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.api.vomodel.VOBankTransactionModel
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.vodao.VoFinTxnDetGrpDao
import com.microware.cdfi.entity.MstVOCOAEntity
import com.microware.cdfi.entity.voentity.VoFinTxnDetGrpEntity


class VoFinTxnDetGrpRepository {


    var voFinTxnDetGrpDao: VoFinTxnDetGrpDao? = null

    constructor(application: Application) {
        voFinTxnDetGrpDao = CDFIApplication.database?.voFinTxnDetGrpDao()
    }

    fun insertVoGroupLoanSchedule(VoFinTxnDetGrp: VoFinTxnDetGrpEntity?) {
        insertAsyncTask(VoFinTxnDetGrp, voFinTxnDetGrpDao!!).execute()
    }

    fun deleteVoFinTxnDetGrpData() {
        voFinTxnDetGrpDao!!.deleteVoFinTxnDetGrpData()
    }

    fun getVoFinTxnDetGrpData(): LiveData<List<VoFinTxnDetGrpEntity>>? {
        return voFinTxnDetGrpDao!!.getVoFinTxnDetGrpData()
    }

    fun getVoFinTxnDetGrpList(mtg_guid: String, cboid: Long): List<VoFinTxnDetGrpEntity>? {
        return voFinTxnDetGrpDao!!.getVoFinTxnDetGrpList(mtg_guid, cboid)
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var voFixTxnDetGrpEntity: VoFinTxnDetGrpEntity? = null
        var voFinTxnDetGrpDao: VoFinTxnDetGrpDao? = null

        constructor (
            voFixTxnDetGrpEntity: VoFinTxnDetGrpEntity?,
            voFixTxnDetGrpDao: VoFinTxnDetGrpDao
        ) : this() {
            this.voFixTxnDetGrpEntity = voFixTxnDetGrpEntity
            this.voFinTxnDetGrpDao = voFixTxnDetGrpDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            voFinTxnDetGrpDao!!.insertVoGroupLoanSchedule(voFixTxnDetGrpEntity)
            return null
        }

    }

    fun deleteGrpFinancialTxnDetailData(mtg_no: Int, cbo_id: Long) {
        voFinTxnDetGrpDao!!.deleteGrpFinancialTxnDetailData(mtg_no, cbo_id)
    }

    fun getAdjustmentCashCount(mtg_no: Int, cbo_id: Long): Int {
        return voFinTxnDetGrpDao!!.getAdjustmentCashCount(mtg_no, cbo_id)
    }

    fun updateVoCashAdjustment(
        cbo_id: Long,
        mtg_no: Int,
        auid: Int,
        fund_type: Int?,
        amount_to_from: Int?,
        type: String?,
        amount: Int?,
        date_realisation: Long?,
        mode_payment: Int?,
        bank_code: String?,
        transaction_no: String?,
        updatedby: String?,
        updatedon: Long?
    ) {
        voFinTxnDetGrpDao!!.updateVoCashAdjustment(
            cbo_id,
            mtg_no,
            auid,
            fund_type,
            amount_to_from,
            type,
            amount,
            date_realisation,
            mode_payment,
            bank_code,
            transaction_no,
            updatedby,
            updatedon
        )
    }

    fun getIncomeAndExpenditureAmount(
        cbo_id: Long,
        mtg_no: Int,
        modePayment: Int,
        fund_type: Int
    ): Int {
        return voFinTxnDetGrpDao!!.getIncomeAndExpenditureAmount(
            cbo_id,
            mtg_no,
            modePayment,
            fund_type
        )
    }

    fun getMemberIncomeAndExpenditureAmount(cbo_id: Long, mtg_no: Int, fund_type: Int): Int {
        return voFinTxnDetGrpDao!!.getMemberIncomeAndExpenditureAmount(cbo_id, mtg_no, fund_type)
    }

    fun getInvestmentListByMtgNum(
        cbo_id: Long,
        mtg_no: Int,
        auid: List<Int>,
        type: String
    ): LiveData<List<VoFinTxnDetGrpEntity>?> {
        return voFinTxnDetGrpDao!!.getInvestmentListByMtgNum(cbo_id, mtg_no, auid, type)
    }

    fun getCoaSubHeadData(
        uid: List<Int>,
        type: String,
        languageCode: String
    ): List<MstVOCOAEntity>? {
        return voFinTxnDetGrpDao!!.getCoaSubHeadData(uid, type, languageCode)
    }

    fun updateIncomeAndExpenditure(
        mtg_guid: String,
        cbo_id: Long,
        mtg_no: Int,
        auid: Int,
        fund_type: Int?,
        amount_to_from: Int?,
        type: String?,
        amount: Int?,
        date_realisation: Long?,
        mode_payment: Int?,
        bank_code: String?,
        transaction_no: String?,
        updatedby: String?,
        updatedon: Long?
    ) {
        voFinTxnDetGrpDao!!.updateIncomeAndExpenditure(
            mtg_guid,
            cbo_id,
            mtg_no,
            auid,
            fund_type,
            amount_to_from,
            type,
            amount,
            date_realisation,
            mode_payment,
            bank_code,
            transaction_no,
            updatedby,
            updatedon
        )
    }

    internal fun getInvestmentdata(
        mtg_guid: String,
        cbo_id: Long,
        mtg_no: Int,
        auid: Int,
        type: String,
        amount_to_from: Int
    ): List<VoFinTxnDetGrpEntity>? {
        return voFinTxnDetGrpDao!!.getInvestmentdata(
            mtg_guid,
            cbo_id,
            mtg_no,
            auid,
            type,
            amount_to_from
        )
    }

    internal fun getIncomeAndExpendituredata(
        mtg_guid: String,
        cbo_id: Long,
        mtg_no: Int,
        auid: Int,
        amount_to_from: Int
    ): List<VoFinTxnDetGrpEntity>? {
        return voFinTxnDetGrpDao!!.getIncomeAndExpendituredata(
            mtg_guid,
            cbo_id,
            mtg_no,
            auid,
            amount_to_from
        )
    }

    fun getVoFinTxnDetGrpData(
        mtg_guid: String,
        cbo_id: Long,
        mtg_no: Int,
        fund_type: Int
    ): LiveData<List<VoFinTxnDetGrpEntity>>? {
        return voFinTxnDetGrpDao!!.getVoFinTxnDetGrpData(mtg_guid, cbo_id, mtg_no, fund_type)
    }

    fun getVoFinTxnDetGrpListData(ntgNo: Int, cboid: Long): List<VoFinTxnDetGrpEntity>? {
        return voFinTxnDetGrpDao!!.getVoFinTxnDetGrpListData(ntgNo, cboid)
    }

    fun getVoFinTxnDetgrpdatabyBank(
        cbo_id: Long,
        mtgno: Int,
        bankaccount: String,
        orgtype: Int,
        type: List<String>
    ): List<VoFinTxnDetGrpEntity>? {
        return voFinTxnDetGrpDao!!.getVoFinTxnDetgrpdatabyBank(
            cbo_id,
            mtgno,
            bankaccount,
            orgtype,
            type
        )
    }

    fun updatefindetailgrp(
        cbo_id: Long,
        mtg_no: Int,
        bank_code: String?,
        mode_payment: Int?,
        transaction_no: String?,
        updatedby: String?,
        updatedon: Long?
    ) {
        voFinTxnDetGrpDao!!.updatefindetailgrp(
            cbo_id,
            mtg_no,
            bank_code,
            mode_payment,
            transaction_no,
            updatedby,
            updatedon
        )
    }

    fun getIncomeAmount(
        cbo_id: Long,
        mtg_no: Int,
        modePayment: List<Int>,
        reciept: List<String>,
        bankcode: String
    ): Int {
        return voFinTxnDetGrpDao!!.getIncomeAmount(cbo_id, mtg_no, modePayment, reciept, bankcode)

    }

    fun getpendingIncomeAmount(
        cbo_id: Long,
        mtg_no: Int,
        modePayment: Int,
        reciept: List<String>,
        bankcode: String,
        orgtype: Int
    ): Int {
        return voFinTxnDetGrpDao!!.getpendingIncomeAmount(
            cbo_id,
            mtg_no,
            modePayment,
            reciept,
            bankcode,
            orgtype
        )

    }

    fun getSavingamount(mtgNO: Int, cboID: Long, auid: Int): List<VoFinTxnDetGrpEntity>? {
        return voFinTxnDetGrpDao!!.getSavingamount(mtgNO, cboID, auid)
    }

    fun getAmount(uid: Int, mtgNo: Int, cboID: Long): Int {
        return voFinTxnDetGrpDao!!.getAmount(uid, mtgNo, cboID)
    }

    fun getTotalFinTxnGrpAmount(mtgNo: Int, cboId: Long, orgType: Int, type: List<String>): Int {
        return voFinTxnDetGrpDao!!.getTotalFinTxnGrpAmount(mtgNo, cboId, orgType, type)
    }

    fun getDebitAmount(cboId: Long, mtgNo: Int, type: String, bankCode: String): Int {
        return voFinTxnDetGrpDao!!.getDebitAmount(cboId, mtgNo, type, bankCode)
    }

    fun gettxnkist(
        cboId: Long,
        mtgNo: Int,
        bankCode: String,
        txnno: String
    ): List<VoFinTxnDetGrpEntity>? {
        return voFinTxnDetGrpDao!!.gettxnkist(cboId, mtgNo, bankCode, txnno)
    }

    fun getIncomeAmount(
        cbo_id: Long,
        mtg_no: Int,
        modePayment: List<Int>,
        reciept: List<String>
    ): Int {
        return voFinTxnDetGrpDao!!.getIncomeAmount(cbo_id, mtg_no, modePayment, reciept)

    }

    fun getBankTransferList(
        cbo_id: Long,
        mtg_no: Int,
        fund_type: Int
    ): LiveData<List<VOBankTransactionModel>?> {
        return voFinTxnDetGrpDao!!.getBankTransferList(cbo_id, mtg_no, fund_type)
    }

    fun getFinTxnDetailGrpAmount(
        cbodID: Long,
        mtgNo: Int,
        type: List<String>
    ): Int {
        return voFinTxnDetGrpDao!!.getFinTxnDetailGrpAmount(cbodID, mtgNo, type)
    }

    fun getSavingDepositedAmount(mtgNo: Int, cboID: Long, type: List<String>): Int {
        return voFinTxnDetGrpDao!!.getSavingDepositedAmount(mtgNo, cboID, type)
    }

    fun gettxnvoucherlist(cboId: Long, mtgNo: Int): List<VoFinTxnDetGrpEntity>? {
        return voFinTxnDetGrpDao!!.gettxnvoucherlist(cboId, mtgNo)
    }
}