package com.microware.cdfi.repository.vorepo

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.vodao.VoFinTxnDetMemDao
import com.microware.cdfi.entity.MstCOAEntity
import com.microware.cdfi.entity.voentity.VoFinTxnDetMemEntity

class VoFinTxnDetMemRepository {


    var voFinTxnDetMemDao: VoFinTxnDetMemDao? = null

    constructor(application: Application) {
        voFinTxnDetMemDao = CDFIApplication.database?.voFinTxnDetMemDao()
    }

    fun insertVoGroupLoanSchedule(voFinTxnDetMem: VoFinTxnDetMemEntity?) {
        insertAsyncTask(voFinTxnDetMem, voFinTxnDetMemDao!!).execute()
    }

    fun deleteVoFinTxnDetMemData() {
        voFinTxnDetMemDao!!.deleteVoFinTxnDetMemData()
    }

    fun getVoFinTxnDetMemData(): LiveData<List<VoFinTxnDetMemEntity>>? {
        return voFinTxnDetMemDao!!.getVoFinTxnDetMemData()
    }

    fun getVoFinTxnDetMemList(mtg_guid: String, cbo_id: Long): List<VoFinTxnDetMemEntity>? {
        return voFinTxnDetMemDao!!.getVoFinTxnDetMemList(mtg_guid, cbo_id)
    }

    fun getVoFinTxnDetMemdata(
        cbo_id: Long,
        memid: Long,
        mtgno: Int,
        bankaccount: String,
        type: List<String>
    ): List<VoFinTxnDetMemEntity>? {
        return voFinTxnDetMemDao!!.getVoFinTxnDetMemdata(cbo_id, memid, mtgno, bankaccount,type)
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var voFinTxnDetMemEntity: VoFinTxnDetMemEntity? = null
        var voFinTxnDetMemDao: VoFinTxnDetMemDao? = null

        constructor (
            voFinTxn: VoFinTxnDetMemEntity?,
            voFinTxnDetMemDao: VoFinTxnDetMemDao
        ) : this() {
            this.voFinTxnDetMemEntity = voFinTxn
            this.voFinTxnDetMemDao = voFinTxnDetMemDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            voFinTxnDetMemDao!!.insertVoGroupLoanSchedule(voFinTxnDetMemEntity)
            return null
        }

    }

    fun deleteFinancialTransactionMemberDetailData(mtg_no: Int, cbo_id: Long) {
        voFinTxnDetMemDao!!.deleteFinancialTransactionMemberDetailData(mtg_no, cbo_id)
    }

    fun getIncomeAmount(
        cbo_id: Long,
        mtg_no: Int,
        modePayment: List<Int>,
        reciept: List<String>,
        bankcode: String
    ): Int {
        return voFinTxnDetMemDao!!.getIncomeAmount(cbo_id, mtg_no, modePayment,reciept,bankcode)

    }
 fun getpendingIncomeAmount(
        cbo_id: Long,
        memid: Long,
        mtg_no: Int,
        modePayment: Int,
        reciept: List<String>,
        bankcode: String
    ): Int {
        return voFinTxnDetMemDao!!.getpendingIncomeAmount(cbo_id,memid, mtg_no, modePayment,reciept,bankcode)

    }

    fun getSavingamount(
        mtgNO: Int,
        cboID: Long,
        memId: Long,
        auid: Int
    ): List<VoFinTxnDetMemEntity>? {
        return voFinTxnDetMemDao!!.getSavingamount(mtgNO, cboID, memId, auid)
    }

    fun getCoaSubHeadData(
        uid: List<Int>,
        type1: String,
        languageCode: String
    ): List<MstCOAEntity>? {
        return voFinTxnDetMemDao!!.getCoaSubHeadData(uid, type1, languageCode)
    }

    fun updateIncomeAndExpenditure(
        mtg_guid: String,
        cbo_id: Long,
        mtg_no: Int,
        auid: Int,
        type: String?,
        amount: Int?,
        date_realisation: Long?,
        mode_payment: Int?,
        bank_code: String?,
        transaction_no: String?,
        updatedby: String?,
        updatedon: Long?
    ) {
        voFinTxnDetMemDao!!.updateIncomeAndExpenditure(
            mtg_guid,
            cbo_id,
            mtg_no,
            auid,
            type,
            amount,
            date_realisation,
            mode_payment,
            bank_code,
            transaction_no,
            updatedby,
            updatedon
        )
    }

    fun updatefindetailmem(
        cbo_id: Long,
        memid: Long,
        mtg_no: Int,
        bank_code: String?,
        mode_payment: Int?,
        transaction_no: String?,
        updatedby: String?,
        updatedon: Long?
    ) {
        voFinTxnDetMemDao!!.updatefindetailmem(
            cbo_id,
            memid,
            mtg_no,
            bank_code,
            mode_payment,
            transaction_no,
            updatedby,
            updatedon
        )
    }

    fun getIncomeAndExpendituredata(
        mtg_guid: String,
        cbo_id: Long,
        mtg_no: Int,
        auid: Int
    ): List<VoFinTxnDetMemEntity>? {
        return voFinTxnDetMemDao!!.getIncomeAndExpendituredata(mtg_guid, cbo_id, mtg_no, auid)
    }

    fun getIncomeAndExpenditureAlldata(
        mtg_guid: String,
        cbo_id: Long,
        mtg_no: Int
    ): LiveData<List<VoFinTxnDetMemEntity>?> {
        return voFinTxnDetMemDao!!.getIncomeAndExpenditureAlldata(
            mtg_guid,
            cbo_id,
            mtg_no
        )
    }

    fun getsumvol(mtgno: Int, cbo_id: Long): Int {
        return voFinTxnDetMemDao!!.getsumvol(mtgno, cbo_id)
    }

    fun getsumcomp(mtgno: Int, cbo_id: Long): Int {
        return voFinTxnDetMemDao!!.getsumcomp(mtgno, cbo_id)
    }


    fun getFinTxnMemData(cboId: Long, memId: Long, mtgNo: Int): List<VoFinTxnDetMemEntity>? {
        return voFinTxnDetMemDao!!.getFinTxnMemData(cboId, memId, mtgNo)
    }

    fun getauidlist(uid: Int, memId: Long, mtgNo: Int): List<VoFinTxnDetMemEntity>? {
        return voFinTxnDetMemDao!!.getauidlist(uid, memId, mtgNo)
    }

    fun getAmount(uid: Int, memId: Long, mtgNo: Int): Int {
        return voFinTxnDetMemDao!!.getAmount(uid, memId, mtgNo)
    }

    fun getpendingAmount(cboid: Long, memId: Long, mtgNo: Int, type: List<String>): Int {
        return voFinTxnDetMemDao!!.getpendingAmount(cboid, memId, mtgNo, type)
    }

    fun getrecivedAmount(cboid: Long, memId: Long, mtgNo: Int, type: List<String>): Int {
        return voFinTxnDetMemDao!!.getrecivedAmount(cboid, memId, mtgNo, type)
    }

    fun getTotalFinTxnMemAmount(mtgNo: Int, cboId: Long, orgType: Int, type: List<String>): Int {
        return voFinTxnDetMemDao!!.getTotalFinTxnMemAmount(mtgNo, cboId, orgType,type)
    }

    fun getDebitAmount(cboId: Long, mtgNo: Int, type: String, bankCode: String): Int {
        return voFinTxnDetMemDao!!.getDebitAmount(cboId, mtgNo, type, bankCode)
    }
    fun gettxnkist(cboId: Long, mtgNo: Int,  bankCode: String,  txnno: String): List<VoFinTxnDetMemEntity>? {
        return voFinTxnDetMemDao!!.gettxnkist(cboId, mtgNo,  bankCode,txnno)
    }

    fun getAmountByAuid(uid: Int, memId: Long, mtgNo: Int, cboid: Long): Int {
        return voFinTxnDetMemDao!!.getAmountByAuid(uid, memId, mtgNo, cboid)
    }

    fun getIncomeAmount(
        cbo_id: Long,
        mtg_no: Int,
        modePayment: List<Int>,
        reciept: List<String>
    ): Int {
        return voFinTxnDetMemDao!!.getIncomeAmount(cbo_id, mtg_no, modePayment, reciept)

    }

    fun getFinTxnDetailMemAmount(
        cbodID: Long,
        mtgNo: Int,
        type: List<String>
    ):Int{
        return voFinTxnDetMemDao!!.getFinTxnDetailMemAmount(cbodID, mtgNo, type)
    }
    fun gettxnvoucherlist(cboId: Long, mtgNo: Int): List<VoFinTxnDetMemEntity>? {
        return voFinTxnDetMemDao!!.gettxnvoucherlist(cboId, mtgNo)
    }
}