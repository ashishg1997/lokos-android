package com.microware.cdfi.repository.vorepo

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.vodao.VoFinTxnDao
import com.microware.cdfi.entity.DtMtgFinTxnEntity
import com.microware.cdfi.entity.voentity.VoFinTxnEntity

class VoFinTxnRepository {


    var voFinTxnDao: VoFinTxnDao? = null

    constructor(application: Application) {
        voFinTxnDao = CDFIApplication.database?.voFinTxnDao()
    }

    fun insertVoFinTxn(voFinTxn: VoFinTxnEntity?) {
        insertAsyncTask(voFinTxn, voFinTxnDao!!).execute()
    }

    fun deleteVoFinTxnData() {
        voFinTxnDao!!.deleteVoFinTxnData()
    }

    fun getVoFinTxnData(): LiveData<List<VoFinTxnEntity>>? {
        return voFinTxnDao!!.getVoFinTxnData()
    }

    fun getVoFinTxnList(): List<VoFinTxnEntity>? {
        return voFinTxnDao!!.getVoFinTxnList()
    }

    internal fun getListDataByMtgnum(
        mtgno: Int, cbo_id: Long,
        accountNo: String
    ): List<VoFinTxnEntity> {
        return voFinTxnDao!!.getListDataByMtgnum(mtgno, cbo_id, accountNo)
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var voFinTxnEntity: VoFinTxnEntity? = null
        var voFixTxnDao: VoFinTxnDao? = null

        constructor (voFinTxn: VoFinTxnEntity?, voFixTxnDao: VoFinTxnDao) : this() {
            this.voFinTxnEntity = voFinTxn
            this.voFixTxnDao = voFixTxnDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            voFixTxnDao!!.insertVoFinTxn(voFinTxnEntity)
            return null
        }

    }

    fun deleteVOFinancialTxnData(mtg_no: Int, cbo_id: Long) {
        return voFinTxnDao!!.deleteVOFinancialTxnData(mtg_no, cbo_id)
    }

    fun getListData(mtgno: Int, cbo_id: Long): List<VoFinTxnEntity> {
        return voFinTxnDao!!.getListData(mtgno, cbo_id)
    }

    fun gettotalclosinginbank(mtgno: Int, cbo_id: Long, bankcode: String?): Int {
        return voFinTxnDao!!.gettotalclosinginbank(mtgno, cbo_id, bankcode)
    }

    fun gettotalopeninginbank(mtgno: Int, cbo_id: Long, bankcode: String?): Int {
        return voFinTxnDao!!.gettotalopeninginbank(mtgno, cbo_id, bankcode)
    }

    fun updateclosingbalbank(closingbal: Int, mtgno: Int, cbo_id: Long, accountno: String) {
        voFinTxnDao!!.updateclosingbalbank(closingbal, mtgno, cbo_id, accountno)
    }

}