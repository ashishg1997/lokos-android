package com.microware.cdfi.repository.vorepo

import android.app.Application
import android.os.AsyncTask
import com.microware.cdfi.api.vomodel.VoLoanListModel
import com.microware.cdfi.api.vomodel.VoLoanRepaymentListModel
import com.microware.cdfi.api.vomodel.VoMeetingModel
import com.microware.cdfi.api.vomodel.VoShareCapitalModel
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.LookupDao
import com.microware.cdfi.dao.vodao.*
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.entity.voentity.VoFinTxnEntity
import com.microware.cdfi.entity.voentity.VoMtgDetEntity
import com.microware.cdfi.entity.voentity.VomtgEntity

class VoGenerateMeetingRepository {

    var lookupDao: LookupDao? = null
    var fedmtgDao: VomtgDao? = null
    var voMtgDetDao: VoMtgDetDao? = null
    var voMemLoanDao: VoMemLoanDao? = null
    var voMemLoanTxnDao: VoMemLoanTxnDao? = null
    var voGroupLoanDao: VoGroupLoanDao? = null
    var voGroupLoanTxnDao: VoGroupLoanTxnDao? = null
    var voFinTxnDao: VoFinTxnDao? = null
    var voFinTxndetDao: VoFinTxnDetMemDao? = null
    var voFinTxndetgrpDao: VoFinTxnDetGrpDao? = null

    constructor(application: Application) {
        lookupDao = CDFIApplication.database?.lookupdao()
        fedmtgDao = CDFIApplication.database?.fedmtgDao()
        voMtgDetDao = CDFIApplication.database?.voMtgDetDao()
        voMemLoanDao = CDFIApplication.database?.voMemLoanDao()
        voMemLoanTxnDao = CDFIApplication.database?.voMemLoanTxnDao()
        voGroupLoanDao = CDFIApplication.database?.voGroupLoanDao()
        voGroupLoanTxnDao = CDFIApplication.database?.voGroupLoanTxnDao()
        voFinTxnDao = CDFIApplication.database?.voFinTxnDao()
        voFinTxndetDao = CDFIApplication.database?.voFinTxnDetMemDao()
        voFinTxndetgrpDao = CDFIApplication.database?.voFinTxnDetGrpDao()

    }

    fun getmeetingCount(cbo_id: String): Int {
        return fedmtgDao!!.getmeetingCount(cbo_id)
    }

    fun getMaxMeetingNum(shg_id: Long?): Int {
        return fedmtgDao!!.getMaxMeetingNum(shg_id)
    }

    fun getMeetingCount(federationId: Long?): Int {
        return fedmtgDao!!.getMeetingCount(federationId)
    }

    fun insert(vomtgEntity: VomtgEntity) {
        insertAsyncTask(vomtgEntity, fedmtgDao!!).execute()
    }

    fun openMeeting(flag: String, federationId: Long, mtgno: Int) {
        fedmtgDao!!.openMeeting(flag, federationId, mtgno)
    }


    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var vomtgEntity: VomtgEntity? = null
        var fedmtgDao: VomtgDao? = null

        constructor (vomtgEntity: VomtgEntity?, fedmtgDao: VomtgDao?) : this() {
            this.vomtgEntity = vomtgEntity
            this.fedmtgDao = fedmtgDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            fedmtgDao!!.insertFedmtg(vomtgEntity)
            return null
        }

    }


    fun getgroupMeetingsdata(shg_code: String?): List<VoMeetingModel>? {
        return fedmtgDao!!.getgroupMeetingsdata(shg_code)
    }

    internal fun getMtgByMtgnum(shg_id: Long?, mtgnum: Int): List<VoMtgDetEntity> {
        return voMtgDetDao!!.getMtgByMtgnum(shg_id, mtgnum)
    }

    fun insertvomtgDet(vomtgDetEntity: VoMtgDetEntity) {
        insertAsyncTaskvomtgDet(vomtgDetEntity, voMtgDetDao!!).execute()
    }

    /* fun updateloandata1(mem_id: Long, mtgno: Int, loanno: Int, loanop: Int, loan_int_accrued: Int) {
         voMtgDetDao!!.updateloandata1(mem_id, mtgno, loanno, loanop,loan_int_accrued)
     }

     fun updateloandata2(mem_id: Long, mtgno: Int, loanno: Int, loanop: Int, loan_int_accrued: Int) {
         voMtgDetDao!!.updateloandata2(mem_id, mtgno, loanno, loanop,loan_int_accrued)
     }

     fun updateloandata3(mem_id: Long, mtgno: Int, loanno: Int, loanop: Int, loan_int_accrued: Int) {
         voMtgDetDao!!.updateloandata3(mem_id, mtgno, loanno, loanop,loan_int_accrued)
     }

     fun updateloandata4(mem_id: Long, mtgno: Int, loanno: Int, loanop: Int, loan_int_accrued: Int) {
         voMtgDetDao!!.updateloandata4(mem_id, mtgno, loanno, loanop,loan_int_accrued)
     }

     fun updateloandata5(mem_id: Long, mtgno: Int, loanno: Int, loanop: Int, loan_int_accrued: Int) {
         voMtgDetDao!!.updateloandata5(mem_id, mtgno, loanno, loanop,loan_int_accrued)
     }*/

    internal fun getBankdata(cbo_guid: String?): List<Cbo_bankEntity> {
        return fedmtgDao!!.getBankdata(cbo_guid)
    }

    fun getMtglistdata(cbo_id: Long?): List<VomtgEntity>? {
        return fedmtgDao!!.getMtglistdata(cbo_id)
    }


    class insertAsyncTaskvomtgDet() : AsyncTask<Void, Void, String>() {

        private var vomtgDetEntity: VoMtgDetEntity? = null
        var voMtgDetDao: VoMtgDetDao? = null

        constructor (vomtgDetEntity: VoMtgDetEntity?, voMtgDetDao: VoMtgDetDao?) : this() {
            this.vomtgDetEntity = vomtgDetEntity
            this.voMtgDetDao = voMtgDetDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            voMtgDetDao!!.insertVoMtgDet(vomtgDetEntity)
            return null
        }

    }

    fun getgroupMeetingsdatalist(cboType: Int): List<VoMeetingModel> {
        return fedmtgDao!!.getgroupMeetingsdatalist(cboType)
    }

    fun returnmeetingstatus(federationId: Long, Mtgno: Int): String {
        return fedmtgDao!!.returnmeetingstatus(federationId, Mtgno)
    }

    fun getMtglist(cbo_id: Long?): List<VomtgEntity>? {
        return fedmtgDao!!.getMtglist(cbo_id)
    }

    fun closingMeeting(flag: String, mtgno: Int, cbo_id: Long, updatedOn: Long, updatedBy: String) {
        fedmtgDao!!.closingMeeting(flag, mtgno, cbo_id, updatedOn, updatedBy)
    }

    fun deleteVo_MtgData(mtg_no: Int, cbo_id: Long) {
        fedmtgDao!!.deleteVo_MtgData(mtg_no, cbo_id)
    }

    fun deleteVODetailData(mtg_no: Int, cbo_id: Long) {
        voMtgDetDao!!.deleteVODetailData(mtg_no, cbo_id)
    }

    fun getShareCapitalListByReceiptType(
        mtgNo: Int,
        cboId: Long,
        receiptType: Int
    ): List<VoShareCapitalModel> {
        return voMtgDetDao!!.getShareCapitalListByReceiptType(mtgNo, cboId, receiptType)
    }

    internal fun updateVoCutOffCash(
        cashInHand: Int,
        cashInTransit: Int,
        balanceDate: Long,
        closingcash: Int,
        mtgno: Int,
        cbo_id: Long,
        updatedOn: Long,
        updatedBy: String
    ) {
        fedmtgDao!!.updateVoCutOffCash(
            cashInHand,
            cashInTransit,
            balanceDate,
            closingcash,
            mtgno,
            cbo_id,
            updatedOn,
            updatedBy
        )
    }

    internal fun getMeetingDetailData(mtgno: Int, cbo_id: Long): List<VomtgEntity> {
        return fedmtgDao!!.getMeetingDetailData(mtgno, cbo_id)
    }

    internal fun getupdatedcash(mtgno: Int, cbo_id: Long): Int {
        return fedmtgDao!!.getupdatedcash(mtgno, cbo_id)
    }

    fun getupdatedpeningcash(mtgno: Int, cbo_id: Long): Int {
        return fedmtgDao!!.getupdatedpeningcash(mtgno, cbo_id)
    }

    fun gettotloan(mtgno: Int, cbo_id: Long): Int {
        return voMemLoanDao!!.gettotloan(mtgno, cbo_id)
    }

    fun gettotloanpaid(mtgno: Int, cbo_id: Long): Int {
        return voMemLoanTxnDao!!.gettotloanpaid(mtgno, cbo_id)
    }

    fun getloanpaidbyauid(mtgno: Int, memid: Long, auid: Int): Int {
        return voMemLoanTxnDao!!.getloanpaidbyauid(mtgno, memid, auid)
    }
 fun getgrouploanpaidbyauid(mtgno: Int, cbo_id: Long, auid: Int, LoanSource: Int): Int {
        return voGroupLoanTxnDao!!.getgrouploanpaidbyauid(mtgno, cbo_id, auid,LoanSource)
    }

   fun getgrouploanpaidbycbo(mtgno: Int, cbo_id: Long, LoanSource: Int): Int {
        return voGroupLoanTxnDao!!.getgrouploanpaidbycbo(mtgno, cbo_id, LoanSource)
    }

    fun gettotloangroup(mtgno: Int, cbo_id: Long): Int {
        return voGroupLoanDao!!.gettotloangroup(mtgno, cbo_id)
    }

    fun gettotloanpaidgroup(mtgno: Int, cbo_id: Long): Int {
        return voGroupLoanTxnDao!!.gettotloanpaidgroup(mtgno, cbo_id)
    }

    fun updateclosingcash(closingcash: Int, mtgno: Int, cbo_id: Long) {
        fedmtgDao!!.updateclosingcash(closingcash, mtgno, cbo_id)
    }

    internal fun getBankListDataByMtgnum(mtgno: Int, cbo_id: Long): List<VoFinTxnEntity> {
        return voFinTxnDao!!.getBankListDataByMtgnum(mtgno, cbo_id)
    }

    internal fun updateCutOffBankBalance(
        cbo_id: Long?,
        mtgno: Int?,
        bankcode: String?,
        zero_mtg_cash_bank: Int?,
        cheque_issued_not_realized: Int?,
        cheque_received_not_credited: Int?,
        closingbalance: Int?,
        balance_date: Long?,
        updatedby: String,
        updatedon: Long?,
        uploaded_by: String?
    ) {
        voFinTxnDao!!.updateCutOffBankBalance(
            cbo_id,
            mtgno,
            bankcode,
            zero_mtg_cash_bank,
            cheque_issued_not_realized,
            cheque_received_not_credited,
            closingbalance,
            balance_date,
            updatedby,
            updatedon,
            uploaded_by
        )
    }

    fun getTotalCompulsoryamount(mtgNo: Int, cboId: Long): Int? {
        return fedmtgDao!!.getTotalCompulsoryamount(mtgNo, cboId)
    }

    fun getTotalVoluntaryamount(mtgNo: Int, cboId: Long): Int? {
        return fedmtgDao!!.getTotalVoluntaryamount(mtgNo, cboId)
    }


    internal fun getloanListDataMtgByMtgnum(
        mtgno: Int,
        cbo_id: Long,
        memId: Long,Loanproductid:Int
    ): List<VoLoanListModel> {
        return voMtgDetDao!!.getloanListDataMtgByMtgnum(mtgno, cbo_id, memId,Loanproductid)
    }

    fun getCutOffTotalPresent(mtgno: Int, cbo_id: Long): Int {
        return voMtgDetDao!!.getCutOffTotalPresent(mtgno, cbo_id)
    }


    fun getTotalAmountByReceiptType(mtg_guid: String, cbo_id: Long, mtg_no: Int, auid: Int): Int {
        return voFinTxndetDao!!.getTotalAmountByReceiptType(mtg_guid, cbo_id, mtg_no, auid)
    }

    fun getTotalInvestmentByMtgNum(cbo_id: Long, mtg_no: Int, auid: List<Int>, type: String): Int {
        return voFinTxndetgrpDao!!.getTotalInvestmentByMtgNum(cbo_id, mtg_no, auid, type)
    }

    fun gettotalinbank(mtgno: Int, cbo_id: Long): Int {
        return voFinTxnDao!!.gettotalinbank(mtgno, cbo_id)
    }

    internal fun getBankdataWithAccountNo(
        cbo_guid: String?,
        bank_guid: String?
    ): List<Cbo_bankEntity> {
        return fedmtgDao!!.getBankdataWithAccountNo(cbo_guid, bank_guid)
    }

    internal fun getListDatawithoutmember(
        mtgno: Int,
        cbo_id: Long,
        memid: Long
    ): List<VoMtgDetEntity> {
        return voMtgDetDao!!.getListDatawithoutmember(mtgno, cbo_id, memid)
    }

    fun getClosedMemberLoanByMemberId(
        mtgno: Int,
        cbo_id: Long,
        completionFlag: Boolean,
        memberid: Long
    ): List<VoLoanListModel> {
        return voMtgDetDao!!.getClosedMemberLoanByMemberId(mtgno, cbo_id, completionFlag, memberid)
    }

    fun getVoMeetingList(): List<VomtgEntity> {
        return fedmtgDao!!.getVoMeetingList()
    }

    internal fun updateMeetinguploadStatus(
        shg_id: Long?,
        mtg_no: Int,
        lastupdatedate: Long
    ) {
        fedmtgDao!!.updateMeetinguploadStatus(
            shg_id,
            mtg_no,
            lastupdatedate
        )
    }

    internal fun getloanrepaymentList(mtgno: Int, memId: Long): List<VoLoanRepaymentListModel> {
        return voMtgDetDao!!.getloanrepaymentList(mtgno, memId)
    }


    internal fun getCutOffMemberLoanAmount(
        cbo_id: Long,
        mtg_no: Int,
        completionFlag: Boolean
    ): Int {
        return voMemLoanDao!!.getCutOffMemberLoanAmount(cbo_id, mtg_no, completionFlag)
    }

    internal fun getCutOffClosedMemberLoanDataByMtgNum(
        mtgno: Int,
        cbo_id: Long,
        completionFlag: Boolean
    ): List<VoLoanListModel> {
        return voMtgDetDao!!.getCutOffClosedMemberLoanDataByMtgNum(mtgno, cbo_id, completionFlag)
    }


    fun getClosingBalanceCash(mtgNo: Int, cboId: Long): Int {
        return fedmtgDao!!.getClosingBalanceCash(mtgNo, cboId)
    }

    fun getCashInTransit(mtgNo: Int, cboId: Long): Int {
        return fedmtgDao!!.getCashInTransit(mtgNo, cboId)
    }


    fun getclosingbal(mtgno: Int, cbo_id: Long,accountno: String): Int {
        return voFinTxnDao!!.getclosingbal(mtgno, cbo_id,accountno)
    }
    internal fun getListinactivemember(mtgno: Int, cbo_id: Long): List<VoMtgDetEntity> {
        return voMtgDetDao!!.getListinactivemember(mtgno, cbo_id)
    }
    fun updatesavingwithdrawl(
        withdrawl: Int,
        mtgno: Int,
        cbo_id: Long,
        memid: Long,
        updated_by:String,
        updated_on:Long
    ) {
        voMtgDetDao!!.updatesavingwithdrawl(
            withdrawl,
            mtgno,
            cbo_id,
            memid,
            updated_by,
            updated_on
        )
    }
    fun getCompulsoryData(mtgno: Int, cbo_id: Long,mem_id: Long): List<VoMtgDetEntity>{
        return voMtgDetDao!!.getCompulsoryData(mtgno,cbo_id,mem_id)
    }
}