package com.microware.cdfi.repository.vorepo

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.vodao.VoGroupLoanDao
import com.microware.cdfi.entity.voentity.VoGroupLoanEntity

class VoGroupLoanRepository {
    var voGroupLoanDao: VoGroupLoanDao? = null

    constructor(application: Application) {
        voGroupLoanDao = CDFIApplication.database?.voGroupLoanDao()
    }

    fun insertVoGroupLoan(voGroupLoanEntity: VoGroupLoanEntity) {
        insertAsyncTask(voGroupLoanEntity, voGroupLoanDao!!).execute()
    }

    fun deleteVoGroupLoanData() {
        voGroupLoanDao!!.deleteVoGroupLoanData()
    }

    fun getVoGroupLoanData(): LiveData<List<VoGroupLoanEntity>>? {
        return voGroupLoanDao!!.getVoGroupLoanData()
    }

    fun getVoGroupLoanList(): List<VoGroupLoanEntity>? {
        return voGroupLoanDao!!.getVoGroupLoanList()
    }

    fun getinterestrate(loanno: Int): Double {
        return voGroupLoanDao!!.getinterestrate(loanno)
    }


    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var voGroupLoanEntity: VoGroupLoanEntity? = null
        var voGroupLoanDao: VoGroupLoanDao? = null

        constructor (
            voGroupLoanEntity: VoGroupLoanEntity?,
            voGroupLoanDao: VoGroupLoanDao
        ) : this() {
            this.voGroupLoanEntity = voGroupLoanEntity
            this.voGroupLoanDao = voGroupLoanDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            voGroupLoanDao!!.insertVoGroupLoan(voGroupLoanEntity)
            return null
        }

    }

    fun deleteGroupLoanData(mtg_no: Int, cbo_id: Long) {
        return voGroupLoanDao!!.deleteGroupLoanData(mtg_no, cbo_id)
    }

    fun getmaxLoanno(cboId: Long): Int {
        return voGroupLoanDao!!.getmaxLoanno(cboId)
    }

    fun getListDataByMtgnum(mtgno: Int, cbo_aid: Long, LoanSource: Int, fundType: Int): List<VoGroupLoanEntity> {
        return voGroupLoanDao!!.getListDataByMtgnum(mtgno, cbo_aid, LoanSource,fundType)
    }
fun getListDatavoucher(mtgno: Int, cbo_aid: Long): List<VoGroupLoanEntity> {
        return voGroupLoanDao!!.getListDatavoucher(mtgno, cbo_aid)
    }

    fun getLoandata(cbo_id: Long, loanno: Int, mtgno: Int): List<VoGroupLoanEntity>? {
        return voGroupLoanDao!!.getLoandata(cbo_id, loanno, mtgno)
    }

    fun getLoandatabyloanno(cbo_id: Long, loanno: Int): List<VoGroupLoanEntity>? {
        return voGroupLoanDao!!.getLoandatabyloanno(cbo_id, loanno)
    }


    fun getCutOffmaxLoanno(cbo_id: Long, mtg_no: Int): Int {
        return voGroupLoanDao!!.getCutOffmaxLoanno(cbo_id, mtg_no)
    }

    fun getLoanDetail(cbo_id: Long, mtgNo: Int): List<VoGroupLoanEntity>? {
        return voGroupLoanDao!!.getLoanDetail(cbo_id, mtgNo)
    }

    fun getLoanDetailData(loan_no: Int, cbo_id: Long, mtg_guid: String): List<VoGroupLoanEntity>? {
        return voGroupLoanDao!!.getLoanDetailData(loan_no, cbo_id, mtg_guid)
    }



    fun gettotamt(loanno: Int, cboID: Long): Double {
        return voGroupLoanDao!!.gettotamt(loanno, cboID)
    }

    fun getfundamt(cboid: Long, mtgNum: Int, fundType: Int, LoanSource: Int): Int {
        return voGroupLoanDao!!.getfundamt(cboid, mtgNum, fundType, LoanSource)
    }

    fun gettotamtbyloansource(cboid: Long, mtgNum: Int, LoanSource: Int): Int {
        return voGroupLoanDao!!.gettotamtbyloansource(cboid, mtgNum, LoanSource)
    }


    fun getgrouptotloanamtinbank(
        mtgno: Int,
        cbo_id: Long,
        code: String,
        modeofpayment: List<Int>
    ): Int {
        return voGroupLoanDao!!.getgrouptotloanamtinbank(mtgno, cbo_id, code, modeofpayment)
    }

    fun getgrouptotloanamtinbank(modePayment: List<Int>, mtgno: Int, cbo_id: Long): Int {
        return voGroupLoanDao!!.getgrouptotloanamtinbank(modePayment, mtgno, cbo_id)
    }
    fun getLoanRecAmount(loansource: Int, mtgno: Int, cbo_id: Long): Int{
        return voGroupLoanDao!!.getLoanRecAmount(loansource, mtgno, cbo_id)
    }

    internal fun updateShgGroupLoan(cbo_id:Long,loan_no: Int, completionflag: Boolean){
        voGroupLoanDao!!.updateShgGroupLoan(cbo_id,loan_no,completionflag)
    }
    fun getUploadGroupLoanList(cbo_id: Long) : List<VoGroupLoanEntity>{
        return voGroupLoanDao!!.getUploadGroupLoanList( cbo_id)
    }
}