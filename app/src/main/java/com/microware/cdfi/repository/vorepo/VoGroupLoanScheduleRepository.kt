package com.microware.cdfi.repository.vorepo

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.vodao.VoGroupLoanScheduleDao
import com.microware.cdfi.entity.voentity.VoGroupLoanScheduleEntity


class VoGroupLoanScheduleRepository {

    var voGroupLoanScheduleDao: VoGroupLoanScheduleDao? = null

    constructor(application: Application) {
        voGroupLoanScheduleDao = CDFIApplication.database?.voGroupLoanScheduleDao()
    }

    fun deleteVoGroupLoanScheduleData() {
        voGroupLoanScheduleDao!!.deleteVoGroupLoanScheduleData()
    }

    fun getVoGroupLoanScheduleData(): LiveData<List<VoGroupLoanScheduleEntity>>? {
        return voGroupLoanScheduleDao!!.getVoGroupLoanScheduleData()
    }

    fun getVoGroupLoanScheduleList(): List<VoGroupLoanScheduleEntity>? {
        return voGroupLoanScheduleDao!!.getVoGroupLoanScheduleList()
    }

    fun getprincipaldemand(loanno:Int,cbo_id: Long,currentmtgdate:Long,lastmtgdate:Long): Int {
        return voGroupLoanScheduleDao!!.getprincipaldemand(loanno,cbo_id,currentmtgdate,lastmtgdate)
    }

    fun insertVoGroupLoanSchedule(voGroupLoanSchduleEntity: VoGroupLoanScheduleEntity) {
        insertAsyncTask(voGroupLoanSchduleEntity, voGroupLoanScheduleDao!!).execute()
    }

    internal fun gettotaloutstanding(loanno:Int,cboid: Long): Int {
        return voGroupLoanScheduleDao!!.gettotaloutstanding(loanno,cboid)
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var voGroupLoanScheduleEntity: VoGroupLoanScheduleEntity? = null
        var voGroupLoanScheduleDao: VoGroupLoanScheduleDao? = null

        constructor (
            voGroupLoanScheduleEntity: VoGroupLoanScheduleEntity?,
            voGroupLoanScheduleDao: VoGroupLoanScheduleDao
        ) : this() {
            this.voGroupLoanScheduleEntity = voGroupLoanScheduleEntity
            this.voGroupLoanScheduleDao = voGroupLoanScheduleDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            voGroupLoanScheduleDao!!.insertVoGroupLoanSchedule(voGroupLoanScheduleEntity)
            return null
        }

    }

    fun deleteGroupRepaidData(mtgdate: Long, mtgno: Int, cbo_id: Long) {
        voGroupLoanScheduleDao!!.deleteGroupRepaidData(mtgdate, mtgno, cbo_id)
    }

    fun deleteGroupSubinstallmentData(mtgdate: Long, mtgno: Int, cbo_id: Long) {
        voGroupLoanScheduleDao!!.deleteGroupSubinstallmentData(mtgdate, mtgno, cbo_id)
    }

    fun getPrincipalDemandByInstallmentNum(cboid: Long,mtgGuid: String,loanno: Int,installmentNo:Int):Int{
        return  voGroupLoanScheduleDao!!.getPrincipalDemandByInstallmentNum(cboid,mtgGuid,loanno,installmentNo)
    }

    fun updateCutOffLoanSchedule(
        mtgGuid: String,
        cbo_id: Long,
        loanno: Int,
        installmentNo: Int,
        principalDemand: Int
    ) {
        voGroupLoanScheduleDao!!.updateCutOffLoanSchedule(
            mtgGuid,
            cbo_id,
            loanno,
            installmentNo,
            principalDemand

        )
    }

    fun getGroupScheduledata(cboid: Long,mtgguid:String,loanno:Int): List<VoGroupLoanScheduleEntity>? {
        return voGroupLoanScheduleDao!!.getGroupScheduledata(cboid,mtgguid,loanno)
    }

    fun updateLoanGroupSchedule(
        mtgguid: String,
        cboid: Long,
        loanno: Int,
        installmentno: Int,
        principaldemand: Int,
        loandemandos: Int,
        loanos: Int) {
        voGroupLoanScheduleDao!!.updateLoanGroupSchedule(
            mtgguid,
            cboid,
            loanno,
            installmentno,
            principaldemand,
            loandemandos,
            loanos

        )
    }

    fun getLoanScheduleDetail(cboId: Long, loanNo: Int): List<VoGroupLoanScheduleEntity>? {
        return voGroupLoanScheduleDao!!.getLoanScheduleDetail(cboId,loanNo)
    }

    internal fun getremaininginstallment(loanno: Int, cboid: Long): Int {
        return voGroupLoanScheduleDao!!.getremaininginstallment(loanno, cboid)
    }

    fun deleterepaid(
        loanno: Int,
        shgId: Long,
        mtgdate: Long ,
        mtgno: Int  )
    {
        voGroupLoanScheduleDao!!. deleterepaid(
            loanno,
            shgId,
            mtgdate,
            mtgno   )

    }

    fun deletesubinstallment(
        loanno: Int,
        shgId: Long,
        mtgdate: Long ,
        mtgno: Int  )
    {
        voGroupLoanScheduleDao!!. deletesubinstallment(
            loanno,
            shgId,
            mtgdate,mtgno   )

    }

    fun getMemberScheduleloanwise(
        shgId: Long,
        loanno: Int,
        mtgno: Int
    ): List<VoGroupLoanScheduleEntity>? {
        return voGroupLoanScheduleDao!!.getMemberScheduleloanwise(shgId,loanno,mtgno)
    }

    fun updaterepaid(
        loanno: Int,
        shgId: Long,
        installmentno: Int,
        sub_installment_no: Int,
        loanpaid: Int,
        mtgdate: Long,
        mtgno: Int,
        updatedby: String?,
        updatedon: Long
    ) {
        voGroupLoanScheduleDao!!.updaterepaid(
            loanno,
            shgId,
            installmentno,
            sub_installment_no,
            loanpaid,
            mtgdate,mtgno,updatedby,updatedon
        )

    }
}