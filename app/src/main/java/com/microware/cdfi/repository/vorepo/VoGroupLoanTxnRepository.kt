package com.microware.cdfi.repository.vorepo

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.vodao.VoGroupLoanTxnDao
import com.microware.cdfi.entity.voentity.VoGroupLoanTxnEntity


class VoGroupLoanTxnRepository {


    var voGroupLoanTxnDao: VoGroupLoanTxnDao? = null

    constructor(application: Application){
        voGroupLoanTxnDao = CDFIApplication.database?.voGroupLoanTxnDao()
    }

    fun insertVoGroupLoanTxn(voGroupLoanTxnEntity: VoGroupLoanTxnEntity) {
        insertAsyncTask(voGroupLoanTxnEntity,voGroupLoanTxnDao!!).execute()
    }

    fun deleteVoGroupLoanTxnData() {
        voGroupLoanTxnDao!!.deleteVoGroupLoanTxnData()
    }

    fun getVoGroupLoanTxnData(): LiveData<List<VoGroupLoanTxnEntity>>? {
       return voGroupLoanTxnDao!!.getVoGroupLoanTxnData()
    }

    fun getVoGroupLoanTxnList(): List<VoGroupLoanTxnEntity>? {
      return voGroupLoanTxnDao!!.getVoGroupLoanTxnList()
    }

    internal fun getListDataByMtgnum(mtgno:Int,cbo_aid:Long): List<VoGroupLoanTxnEntity> {
        return voGroupLoanTxnDao!!.getListDataByMtgnum(mtgno,cbo_aid)
    }

     internal fun getListDataByLoansorce(mtgno:Int,cbo_aid:Long,LoanSorurce:Int): List<VoGroupLoanTxnEntity> {
        return voGroupLoanTxnDao!!.getListDataByLoansorce(mtgno,cbo_aid,LoanSorurce)
    }

    class insertAsyncTask(): AsyncTask<Void, Void, String>(){

        private var voGroupLoanTxnEntity: VoGroupLoanTxnEntity? = null
        var voGroupLoanTxnDao: VoGroupLoanTxnDao? = null

        constructor (voGroupLoanTxnEntity: VoGroupLoanTxnEntity?, voGroupLoanTxnDao: VoGroupLoanTxnDao) : this() {
            this.voGroupLoanTxnEntity = voGroupLoanTxnEntity
            this.voGroupLoanTxnDao = voGroupLoanTxnDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            voGroupLoanTxnDao!!.insertVoGroupLoanTxn(voGroupLoanTxnEntity)
            return null
        }

    }

    fun deleteGroupLoanTxnData(mtg_no: Int, cbo_id: Long) {
        return voGroupLoanTxnDao!!.deleteGroupLoanTxnData(mtg_no, cbo_id)
    }

    internal fun gettotalpaid(shgId: Long,mtgno: Int): Int {
        return voGroupLoanTxnDao!!.gettotalpaid(shgId,mtgno)
    }

    fun getgrouploantxnlist(shgId: Long, mtgno: Int): List<VoGroupLoanTxnEntity>? {
        return voGroupLoanTxnDao!!.getgrouploantxnlist(shgId,mtgno)
    }

    fun getgrouploandata(loanno: Int, mtgno: Int, shgid: Long): List<VoGroupLoanTxnEntity>? {
        return voGroupLoanTxnDao!!.getgrouploandata(loanno,mtgno,shgid)
    }

    fun updateloanpaid(
        loanno: Int, mtgno: Int,
        shgID: Long,
        paid: Int,
        loanint: Int,
        mode: Int,
        bankaccount: String,
        transactionno: String,
        principaldemandcl: Int,
        completionflag: Boolean
    ) {
        voGroupLoanTxnDao!!.updateloanpaid(loanno, mtgno, shgID, paid,loanint,
            mode,
            bankaccount,
            transactionno,principaldemandcl,completionflag
        )
    }

    internal fun getCutOffgroupLoanAmount(cbo_id: Long,mtg_no: Int):Int{
        return voGroupLoanTxnDao!!.getCutOffgroupLoanAmount(cbo_id,mtg_no)
    }

    fun getgrptotloanpaidinbank(mtgno: Int, cbo_id: Long, account: String, modePayment: List<Int>): Int {
        return voGroupLoanTxnDao!!.getgrptotloanpaidinbank(mtgno, cbo_id,account,modePayment)
    }

    fun getgrptotloanpaidinbank(modePayment: List<Int>,mtgno:Int,cbo_id:Long): Int {
        return voGroupLoanTxnDao!!.getgrptotloanpaidinbank(modePayment,mtgno, cbo_id)
    }
    fun getRepaymentClfAmount(mtgno: Int,cbo_id: Long):Int{
        return voGroupLoanTxnDao!!.getRepaymentClfAmount(mtgno, cbo_id)
    }

    fun getTotalRepaymentClfAmount(mtgno: Int,cbo_id: Long):Int{
        return voGroupLoanTxnDao!!.getTotalRepaymentClfAmount(mtgno, cbo_id)
    }
}