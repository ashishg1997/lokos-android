package com.microware.cdfi.repository.vorepo

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.vodao.VoLoanApplicationDao
import com.microware.cdfi.entity.DtLoanApplicationEntity
import com.microware.cdfi.entity.voentity.VoLoanApplicationEntity

class VoLoanApplicationRepo {

    var voFedLoanAppDao: VoLoanApplicationDao? = null

    constructor(application: Application) {
        voFedLoanAppDao = CDFIApplication.database?.voFedLoanAppDao()
    }

    fun deleteFedLoanApplicationData() {
        voFedLoanAppDao!!.deleteFedLoanApplicationData()
    }

    fun getFedLoanApplicationData(): LiveData<List<VoLoanApplicationEntity>>? {
        return voFedLoanAppDao!!.getFedLoanApplicationData()
    }

    fun getFedLoanApplicationDataAll(): List<VoLoanApplicationEntity>? {
        return voFedLoanAppDao!!.getFedLoanApplicationDataAll()
    }

    fun insertFedLoanApplicationData(voFedLoanAppEntity: VoLoanApplicationEntity?) {
        insertAsyncTask(voFedLoanAppEntity, voFedLoanAppDao!!).execute()
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var voFedLoanAppEntity: VoLoanApplicationEntity? = null
        var voFedLoanAppDao: VoLoanApplicationDao? = null

        constructor (
            voFedLoanAppEntity: VoLoanApplicationEntity?,
            voFedLoanAppDao: VoLoanApplicationDao
        ) : this() {
            this.voFedLoanAppEntity = voFedLoanAppEntity
            this.voFedLoanAppDao = voFedLoanAppDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            voFedLoanAppDao!!.insertFedLoanApplicationData(voFedLoanAppEntity)
            return null
        }

    }

    fun deleteVoLoanApplication(mtg_no: Int, cbo_id: Long) {
        voFedLoanAppDao!!.deleteVoLoanApplication(mtg_no, cbo_id)
    }

    internal fun getmaxloanno(cboid: Long): Int {
        return voFedLoanAppDao!!.getmaxloanno(cboid)
    }

    fun updateMemberloanapplicationdetail(
        loanapplication_id: Long,
        amt_disbursed: Int,
        amtsaction: Int,
        priorty: Int,
        approvalDate: Long,
        tentivedate: Long
    ) {
        voFedLoanAppDao!!.updateMemberloanapplicationdetail(
            loanapplication_id,
            amt_disbursed,
            amtsaction,
            priorty,
            approvalDate,
            tentivedate
        )
    }

    internal fun getmemberLoanApplication(loanapplication_id: Long,mtgNo:Int,shgid: Long): List<VoLoanApplicationEntity>? {
        return voFedLoanAppDao!!.getmemberLoanApplication(loanapplication_id,mtgNo,shgid)
    }


    fun getTotalLoanCount(mtgNum:Int,mem_id: Long):Int{
        return voFedLoanAppDao!!.getTotalLoanCount(mtgNum,mem_id)
    }

    fun getLoanOutstanding(mtgNum:Int,shgid:Long,mem_id: Long):Int{
        return voFedLoanAppDao!!.getLoanOutstanding(mtgNum,shgid,mem_id)
    }

    fun getPriorityCount(priority: Int,mtg_no: Int,cbo_id: Long):Int{
        return voFedLoanAppDao!!.getPriorityCount(priority,mtg_no,cbo_id)
    }
//replace//

    internal fun getLoanApplicationlist(mtgno: Int, cboid: Long,memId:Long,Loanproductid:Int): List<VoLoanApplicationEntity>? {
        return voFedLoanAppDao!!.getLoanApplicationlist(mtgno, cboid, memId,Loanproductid)
    }

    internal fun gettotaldemand(mtgno: Int, cboid: Long,memId:Long,Loanproductid:Int): Int {
        return voFedLoanAppDao!!.gettotaldemand(mtgno, cboid,memId,Loanproductid)
    }
    internal fun getLoanApplicationlist(mtgno: Int, cboid: Long): List<VoLoanApplicationEntity>? {
        return voFedLoanAppDao!!.getLoanApplicationlist(mtgno, cboid)
    }

    fun updatevoshgloanapplication(
        mtgguid: String,
        shgmemid: Long,
        void: Long,
        mtgno: Int,
        loanappid: Long,
        amt: Int


    ) {
        voFedLoanAppDao!!.updatevoshgloanapplication(
            mtgguid,
            shgmemid,
            void,
            mtgno,
            loanappid,
            amt
        )
    }

    fun getdemand(mtgno: Int,cboid:Long): Int{
        return voFedLoanAppDao!!.getdemand(mtgno, cboid)
    }
}