package com.microware.cdfi.repository.vorepo

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.vodao.VoMemLoanDao
import com.microware.cdfi.entity.voentity.VoMemLoanEntity


class VoMemLoanRepository {
    var voMemLoanDao: VoMemLoanDao? = null

    constructor(application: Application) {
        voMemLoanDao = CDFIApplication.database?.voMemLoanDao()
    }

    fun insertVoMemLoan(voMemLoanEntity: VoMemLoanEntity) {
        insertAsyncTask(voMemLoanEntity, voMemLoanDao!!).execute()
    }

    fun deleteVoMemLoanData() {
        voMemLoanDao!!.deleteVoMemLoanData()
    }

    fun getVoMemLoanData(): LiveData<List<VoMemLoanEntity>>? {
        return voMemLoanDao!!.getVoMemLoanData()
    }

    fun getVoMemLoanList(): List<VoMemLoanEntity>? {
        return voMemLoanDao!!.getVoMemLoanList()
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var voMemLoanEntity: VoMemLoanEntity? = null
        var voMemLoanDao: VoMemLoanDao? = null

        constructor (voMemLoanEntity: VoMemLoanEntity?, voMemLoanDao: VoMemLoanDao) : this() {
            this.voMemLoanEntity = voMemLoanEntity
            this.voMemLoanDao = voMemLoanDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            voMemLoanDao!!.insertVoMemLoan(voMemLoanEntity)
            return null
        }

    }

    fun getinterestrate(loanNo: Int, memId: Long?): Double {
        return voMemLoanDao!!.getinterestrate(loanNo, memId)
    }

    fun deleteMemberLoan(cbo_id: Long, mtg_no: Int) {
        return voMemLoanDao!!.deleteMemberLoan(cbo_id, mtg_no)
    }

    fun getLoanDetailData(
        loan_no: Int,
        cbo_id: Long,
        mem_id: Long,
        mtg_guid: String
    ): List<VoMemLoanEntity>? {
        return voMemLoanDao!!.getLoanDetailData(loan_no, cbo_id, mem_id, mtg_guid)
    }

    fun getCutOffmaxLoanno(cbo_id: Long, mtg_no: Int): Int {
        return voMemLoanDao!!.getCutOffmaxLoanno(cbo_id, mtg_no)
    }

    fun getLoanno(appid: Long): Int {
        return voMemLoanDao!!.getLoanno(appid)
    }

    fun getLoanDetail(cbo_id: Long, mtgNo: Int): List<VoMemLoanEntity>? {
        return voMemLoanDao!!.getLoanDetail(cbo_id, mtgNo)
    }

    fun getTotalClosedLoanAmount(mtgNum: Int, mem_id: Long): Int {
        return voMemLoanDao!!.getTotalClosedLoanAmount(mtgNum, mem_id)
    }

    fun getTotalClosedLoanCount(mtgNum: Int, mem_id: Long): Int {
        return voMemLoanDao!!.getTotalClosedLoanCount(mtgNum, mem_id)
    }

    fun getmaxLoanno(cbo_id: Long): Int {
        return voMemLoanDao!!.getmaxLoanno(cbo_id)
    }

    fun getTotalLoanCount(mtgNum: Int, mem_id: Long): Int {
        return voMemLoanDao!!.getTotalLoanCount(mtgNum, mem_id)


    }

    fun getTotalClosedLoanAmount(mtgNum: Int, mem_id: Long, completionFlag: Boolean): Int {
        return voMemLoanDao!!.getTotalClosedLoanAmount(mtgNum, mem_id, completionFlag)
    }

    fun getTotalClosedLoanCount(mtgNum: Int, mem_id: Long, completionFlag: Boolean): Int {
        return voMemLoanDao!!.getTotalClosedLoanCount(mtgNum, mem_id, completionFlag)
    }

    fun getTotalLoanAmount(mtgNum: Int, mem_id: Long): Int {
        return voMemLoanDao!!.getTotalLoanAmount(mtgNum, mem_id)
    }

    fun getTotaloutstandingprinciple(mtgNum: Int, mem_id: Long): Int {
        return voMemLoanDao!!.getTotaloutstandingprinciple(mtgNum, mem_id)
    }

    fun getAmountPaid(mtgNum: Int, mem_id: Long): Int {
        return voMemLoanDao!!.getAmountPaid(mtgNum, mem_id)
    }

    fun gettotamt(loanno: Int, member_id: Long): Int {
        return voMemLoanDao!!.gettotamt(loanno, member_id)
    }

    fun getfundamt(shgId: Long, mtgNum: Int, fundType: Int): Int {
        return voMemLoanDao!!.getfundamt(shgId, mtgNum, fundType)
    }

    fun gettotcboamt(shgId: Long, mtgNum: Int): Int {
        return voMemLoanDao!!.gettotcboamt(shgId, mtgNum)
    }

    fun getMemLoanDisbursedAmount(
        cbo_id: Long,
        mtg_no: Int,
        bank_code: String,
        modeofpayment: List<Int>
    ): Int {
        return voMemLoanDao!!.getMemLoanDisbursedAmount(cbo_id, mtg_no, bank_code, modeofpayment)
    }

    fun getloanListSummeryData(mtgno: Int, cbo_id: Long, mem_id: Long): List<VoMemLoanEntity> {
        return voMemLoanDao!!.getloanListSummeryData(mtgno, cbo_id, mem_id)
    }

    fun getMemLoanDisbursedAmount(
        modePayment: List<Int>,
        cbo_id: Long,
        mtg_no: Int
    ): Int {
        return voMemLoanDao!!.getMemLoanDisbursedAmount(modePayment, cbo_id, mtg_no)
    }

    fun getTotalClosedLoan(mtgNum:Int,mem_id: Long,completionFlag:Boolean):Int{
        return voMemLoanDao!!.getTotalClosedLoan(mtgNum,mem_id,completionFlag)
    }

    fun getClosedLoanAmount(mtgNum:Int,mem_id: Long,completionFlag: Boolean):Int{
        return voMemLoanDao!!.getClosedLoanAmount(mtgNum,mem_id,completionFlag)
    }
    internal  fun updateMemberLoanEditFlag(cbo_id:Long,mem_id:Long,loan_no:Int, completionflag: Boolean){
        voMemLoanDao!!.updateMemberLoanEditFlag(cbo_id,mem_id,loan_no,completionflag)
    }

    fun getuploadlist(cbo_id: Long): List<VoMemLoanEntity>? {
        return voMemLoanDao!!.getuploadlist(cbo_id)
    }

}