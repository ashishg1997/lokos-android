package com.microware.cdfi.repository.vorepo

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.vodao.VoMemLoanScheduleDao
import com.microware.cdfi.entity.voentity.VoMemLoanScheduleEntity

class VoMemLoanScheduleRepository {
    var voMemLoanScheduleDao: VoMemLoanScheduleDao? = null

    constructor(application: Application){
        voMemLoanScheduleDao = CDFIApplication.database?.voMemLoanScheduleDao()
    }

    fun insertVoMemLoanSchedule(voMemLoanScheduleEntity: VoMemLoanScheduleEntity) {
        insertAsyncTask(voMemLoanScheduleEntity,voMemLoanScheduleDao!!).execute()
    }

    fun deleteVoMemLoanScheduleData() {
        voMemLoanScheduleDao!!.deleteVoMemLoanScheduleData()
    }

    fun getVoMemLoanScheduleData(): LiveData<List<VoMemLoanScheduleEntity>>? {
        return voMemLoanScheduleDao!!.getVoMemLoanScheduleData()
    }

    fun getVoMemLoanScheduleList(): List<VoMemLoanScheduleEntity>? {
        return voMemLoanScheduleDao!!.getVoMemLoanScheduleList()
    }

    class insertAsyncTask(): AsyncTask<Void, Void, String>(){

        private var voMemLoanScheduleEntity: VoMemLoanScheduleEntity? = null
        var voMemLoanScheduleDao: VoMemLoanScheduleDao? = null

        constructor (voMemLoanScheduleEntity: VoMemLoanScheduleEntity?, voMemLoanScheduleDao: VoMemLoanScheduleDao) : this() {
            this.voMemLoanScheduleEntity = voMemLoanScheduleEntity
            this.voMemLoanScheduleDao = voMemLoanScheduleDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            voMemLoanScheduleDao!!.insertVoMemLoanSchedule(voMemLoanScheduleEntity)
            return null
        }

    }

    fun getprincipaldemand(
        loanNo: Int,
        mem_id: Long,
        currentmtgdate: Long,
        lastmtgdate: Long
    ): Int {
        return voMemLoanScheduleDao!!.getprincipaldemand(
            loanNo,mem_id,
            currentmtgdate,
            lastmtgdate
        )
    }

    internal fun gettotaloutstanding(loanNo: Int, mem_id: Long): Int {
        return voMemLoanScheduleDao!!.gettotaloutstanding( loanNo,mem_id)
    }

    fun deleteMemberRepaidData(mtgdate: Long, mtgno: Int, cbo_id: Long) {
        voMemLoanScheduleDao!!.deleteMemberRepaidData(mtgdate, mtgno, cbo_id)
    }

    fun deleteMemberSubinstallmentData(mtgdate: Long, mtgno: Int, cbo_id: Long) {
        voMemLoanScheduleDao!!.deleteMemberSubinstallmentData(mtgdate, mtgno, cbo_id)
    }

    fun getPrincipalDemandByInstallmentNum(memId: Long,mtgGuid: String,loanNo: Int,installmenNo:Int):Int{
        return  voMemLoanScheduleDao!!.getPrincipalDemandByInstallmentNum(memId,mtgGuid,loanNo,installmenNo)
    }

    fun updateCutOffLoanMemberSchedule(
        mtgguid: String,
        memId: Long,
        loanno: Int,
        installmentno: Int,
        principaldemand: Int
    ) {
        voMemLoanScheduleDao!!.updateCutOffLoanMemberSchedule(
            mtgguid,
            memId,
            loanno,
            installmentno,
            principaldemand
        )
    }

    internal fun getMemberScheduledata(
        mem_id: Long,
        mtgguid: String,
        loanno: Int
    ): List<VoMemLoanScheduleEntity>? {
        return voMemLoanScheduleDao!!.getMemberScheduledata(mem_id, mtgguid, loanno)
    }

    fun updateLoanMemberSchedule(
        mtgguid: String,
        mem_id: Long,
        loanno: Int,
        installmentno: Int,
        principaldemand: Int,
        loandemandos: Int,
        loanos: Int

    ) {
        voMemLoanScheduleDao!!.updateLoanMemberSchedule(
            mtgguid,
            mem_id,
            loanno,
            installmentno,
            principaldemand,
            loandemandos,
            loanos

        )
    }

    internal fun gettotalloanamt(loanno: Int, mem_id: Long, mtgguid: String): Int {
        return voMemLoanScheduleDao!!.gettotalloanamt(loanno, mem_id, mtgguid)
    }

    fun getLoanScheduleDetail(cboId: Long, loanNo: Int): List<VoMemLoanScheduleEntity>? {
        return voMemLoanScheduleDao!!.getLoanScheduleDetail(cboId,loanNo)
    }

    fun getnextdemand(mem_id: Long,loanno:Int): Int {
        return voMemLoanScheduleDao!!.getnextdemand(mem_id,loanno)
    }

    internal fun getremaininginstallment(loanno: Int, mem_id: Long): Int {
        return voMemLoanScheduleDao!!.getremaininginstallment(loanno, mem_id)
    }

    fun deleterepaid(
        loanno: Int,
        mem_id: Long,
        mtgdate: Long ,
        mtgno: Int  )
    {
        voMemLoanScheduleDao!!. deleterepaid(
            loanno,
            mem_id,
            mtgdate,mtgno   )

    }
    fun deletesubinstallment(
        loanno: Int,
        mem_id: Long,
        mtgdate: Long ,
        mtgno: Int  )
    {
        voMemLoanScheduleDao!!. deletesubinstallment(
            loanno,
            mem_id,
            mtgdate,mtgno   )

    }

    fun getMemberScheduleloanwise(
        mem_id: Long,
        loanno: Int,
        mtgno: Int
    ): List<VoMemLoanScheduleEntity>? {
        return voMemLoanScheduleDao!!.getMemberScheduleloanwise(mem_id,loanno,mtgno)
    }

    fun updaterepaid(
        loanno: Int,
        mem_id: Long,
        installmentno: Int,
        subInstallmentNo: Int,
        loanpaid: Int,
        mtgdate: Long,
        mtgno: Int,
        updatedby: String?,
        updatedon: Long
    ) {
        voMemLoanScheduleDao!!.updaterepaid(
            loanno,
            mem_id,
            installmentno,
            subInstallmentNo,
            loanpaid,
            mtgdate,mtgno,updatedby,updatedon
        )

    }
    fun gettotalloanoutstanding(mem_id: Long): Int {
        return voMemLoanScheduleDao!!.gettotalloanoutstanding(mem_id)
    }
}