package com.microware.cdfi.repository.vorepo

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.api.vomodel.VoLoanRepaymentListModel
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.vodao.VoMemLoanTxnDao
import com.microware.cdfi.entity.voentity.VoMemLoanTxnEntity

class VoMemLoanTxnRepository {
    var voMemLoanTxnDao: VoMemLoanTxnDao? = null

    constructor(application: Application){
        voMemLoanTxnDao = CDFIApplication.database?.voMemLoanTxnDao()
    }

    fun insertVoMemLoanTxn(voMemLoanTxnEntity: VoMemLoanTxnEntity) {
        insertAsyncTask(voMemLoanTxnEntity,voMemLoanTxnDao!!).execute()
    }

    fun deleteVoMemLoanTxnData() {
        voMemLoanTxnDao!!.deleteVoMemLoanTxnData()
    }

    fun getVoMemLoanTxnData(): LiveData<List<VoMemLoanTxnEntity>>? {
        return voMemLoanTxnDao!!.getVoMemLoanTxnData()
    }

    fun getVoMemLoanTxnList(): List<VoMemLoanTxnEntity>? {
        return voMemLoanTxnDao!!.getVoMemLoanTxnList()
    }

    internal fun getmeetingLoanTxnMemdata(
        cboid: Long,
        mem_id: Long,
        mtgno: Int
    ): List<VoMemLoanTxnEntity>? {
        return voMemLoanTxnDao!!.getmeetingLoanTxnMemdata(cboid, mem_id, mtgno)
    }

    class insertAsyncTask(): AsyncTask<Void, Void, String>(){

        private var voMemLoanTxnEntity: VoMemLoanTxnEntity? = null
        var voMemLoanTxnDao: VoMemLoanTxnDao? = null

        constructor (voMemLoanTxnEntity: VoMemLoanTxnEntity?, voMemLoanTxnDao: VoMemLoanTxnDao) : this() {
            this.voMemLoanTxnEntity = voMemLoanTxnEntity
            this.voMemLoanTxnDao = voMemLoanTxnDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            voMemLoanTxnDao!!.insertVoMemLoanTxn(voMemLoanTxnEntity)
            return null
        }

    }

    fun deleteMemberLoanTxnData(mtg_no: Int, cbo_id: Long) {
        voMemLoanTxnDao!!.deleteMemberLoanTxnData(mtg_no, cbo_id)
    }

    fun getmemberloan(memid: Long, mtgno: Int): List<VoLoanRepaymentListModel>? {
        return voMemLoanTxnDao!!.getmemberloan(memid,mtgno)
    }

    fun getmemberloantxnlist(memid: Long, mtgno: Int, fundtype: Int): List<VoMemLoanTxnEntity>? {
        return voMemLoanTxnDao!!.getmemberloantxnlist(memid,mtgno,fundtype)
    }

    internal fun gettotalpaid(mem_id: Long,mtgno: Int, fundtype: Int): Int {
        return voMemLoanTxnDao!!.gettotalpaid(mem_id,mtgno,fundtype)
    }

    fun getmemberloandata(loanno: Int, mtgno: Int, memid: Long): List<VoMemLoanTxnEntity>? {
        return voMemLoanTxnDao!!.getmemberloandata(loanno,mtgno,memid)
    }

    fun updateloanpaid(
        loanno: Int, mtgno: Int, memid: Long, paid: Int, loanint: Int,
        mode: Int,
        bankaccount: String,
        transactionno: String,
        principaldemandcl: Int,
        completionflag: Boolean
    ) {
        voMemLoanTxnDao!!.updateloanpaid(loanno, mtgno, memid, paid,loanint,
            mode,
            bankaccount,
            transactionno,principaldemandcl,completionflag
        )
    }


    fun getmemtotloanpaidinbank(mtgno: Int, cbo_id: Long, account: String, modePayment: List<Int>): Int {
        return voMemLoanTxnDao!!.getmemtotloanpaidinbank(mtgno, cbo_id,account,modePayment)
    }

    fun getMemberLoanRepaymentAmount(cboid: Long, mtgno: Int,mem_id: Long):Int{
        return voMemLoanTxnDao!!.getMemberLoanRepaymentAmount(cboid, mtgno,mem_id)
    }

    fun getMemberLoanRepaymentIntAmount(cboid: Long, mtgno: Int,mem_id: Long):Int{
        return voMemLoanTxnDao!!.getMemberLoanRepaymentIntAmount(cboid, mtgno, mem_id)
    }

    fun getMemberLoanRepaymentOPAmount(cboid: Long, mtgno: Int,mem_id: Long):Int{
        return voMemLoanTxnDao!!.getMemberLoanRepaymentOPAmount(cboid, mtgno, mem_id)
    }

    fun getmemtotloanpaidinbank(modePayment: List<Int>, mtgno: Int, cbo_id: Long): Int {
        return voMemLoanTxnDao!!.getmemtotloanpaidinbank(modePayment, mtgno, cbo_id)
    }

    fun getTotalLoanRepayReceived(cboid: Long, mtgNo: Int):Int{
        return voMemLoanTxnDao!!.getTotalLoanRepayReceived(cboid, mtgNo)
    }

    fun getLoanRepayReceived(cboid: Long, mtgNo: Int):Int{
        return voMemLoanTxnDao!!.getLoanRepayReceived(cboid, mtgNo)
    }

    fun getListDataByMtgnum(mtgno:Int,cbo_aid:Long): List<VoMemLoanTxnEntity> {
        return voMemLoanTxnDao!!.getListDataByMtgnum(mtgno,cbo_aid)
    }
    fun gettotalpaid1(mem_id: Long,mtgno: Int): Int {
        return voMemLoanTxnDao!!.gettotalpaid1(mem_id,mtgno)
    }

    fun getmemberloantxnlist1(memid: Long, mtgno: Int): List<VoMemLoanTxnEntity>?{
        return voMemLoanTxnDao!!.getmemberloantxnlist1(memid,mtgno)
    }
}