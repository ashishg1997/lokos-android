package com.microware.cdfi.repository.vorepo

import android.app.Application
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.vodao.VoMemSettlementDao
import com.microware.cdfi.entity.voentity.VoMemSettlementEntity

class VoMemSettlementRepository {
    var memSettlementDao: VoMemSettlementDao? = null

    constructor(application: Application){
        memSettlementDao = CDFIApplication.database?.vomemSettlementDao()
    }

    fun getSettlementdata(memid: Long?,mtgno:Int): List<VoMemSettlementEntity>?{
        return memSettlementDao!!.getSettlementdata(memid,mtgno)
    }

    fun insertSettlementData(dtMemSettlementEntity: VoMemSettlementEntity){
         memSettlementDao!!.insertSettlementData(dtMemSettlementEntity)
    }

    fun getSettlementuploaddata(cboId:Long?,memid: Long?,mtgno:Int): List<VoMemSettlementEntity>?{
        return memSettlementDao!!.getSettlementuploaddata(cboId,memid,mtgno)
    }


}