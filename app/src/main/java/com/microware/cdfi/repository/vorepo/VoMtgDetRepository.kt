package com.microware.cdfi.repository.vorepo

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.microware.cdfi.api.vomodel.VoDetailDataModel
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.dao.vodao.VoMtgDetDao
import com.microware.cdfi.entity.voentity.VoMtgDetEntity


class VoMtgDetRepository {
    var voMtgDetDao: VoMtgDetDao? = null

    constructor(application: Application) {
        voMtgDetDao = CDFIApplication.database?.voMtgDetDao()
    }

    fun insertVoMtgDet(voMtgDetEntity: VoMtgDetEntity) {
        insertAsyncTask(voMtgDetEntity, voMtgDetDao!!).execute()
    }

    fun deleteVoMtgDetData() {
        voMtgDetDao!!.deleteVoMtgDetData()
    }

    fun getVoMtgDetData(): LiveData<List<VoMtgDetEntity>>? {
        return voMtgDetDao!!.getVoMtgDetData()
    }

    fun getVoMtgDetList(): List<VoMtgDetEntity>? {
        return voMtgDetDao!!.getVoMtgDetList()
    }

    class insertAsyncTask() : AsyncTask<Void, Void, String>() {

        private var voMtgDetEntity: VoMtgDetEntity? = null
        var voMtgDetDao: VoMtgDetDao? = null

        constructor (voMtgDetEntity: VoMtgDetEntity?, voMtgDetDao: VoMtgDetDao) : this() {
            this.voMtgDetEntity = voMtgDetEntity
            this.voMtgDetDao = voMtgDetDao
        }

        override fun doInBackground(vararg params: Void?): String? {
            voMtgDetDao!!.insertVoMtgDet(voMtgDetEntity)
            return null
        }

    }

    internal fun getListDataMtgByMtgnum(mtgno: Int, cbo_id: Long): List<VoMtgDetEntity> {
        return voMtgDetDao!!.getListDataMtgByMtgnum(mtgno, cbo_id)
    }

    fun updateAttendanceEC1(
        EC1: String,
        mtgno: Int,
        cbo_id: Long,
        memId: Long,
        updated_by: String,
        updated_on: Long
    ) {
        voMtgDetDao!!.updateAttendanceEC1(
            EC1,
            mtgno,
            cbo_id,
            memId,
            updated_by,
            updated_on
        )
    }

    fun updateAttendanceEC2(
        EC2: String,
        mtgno: Int,
        cbo_id: Long,
        memId: Long,
        updated_by: String,
        updated_on: Long
    ) {
        voMtgDetDao!!.updateAttendanceEC2(
            EC2,
            mtgno,
            cbo_id,
            memId,
            updated_by,
            updated_on
        )
    }

    fun updateAttendanceEC3(
        EC3: String,
        mtgno: Int,
        cbo_id: Long,
        memId: Long,
        updated_by: String,
        updated_on: Long
    ) {
        voMtgDetDao!!.updateAttendanceEC3(
            EC3,
            mtgno,
            cbo_id,
            memId,
            updated_by,
            updated_on
        )
    }

    fun updateAttendanceOther(
        attendanceOther: Int,
        mtgno: Int,
        cbo_id: Long,
        memId: Long,
        updated_by: String,
        updated_on: Long
    ) {
        voMtgDetDao!!.updateAttendanceOther(
            attendanceOther,
            mtgno,
            cbo_id,
            memId,
            updated_by,
            updated_on
        )
    }

    fun updateAttendance(
        attendance: String,
        mtgNo: Int,
        cboId: Long,
        memId: Long,
        updatedBy: String?,
        updatedOn: Long
    ) {
        voMtgDetDao!!.updateAttendance(
            attendance,
            mtgNo,
            cboId,
            memId,
            updatedBy,
            updatedOn
        )
    }

    internal fun getTotalPresentAndAbsent(attendance: String, mtgNo: Int, cboId: Long): Int {
        return voMtgDetDao!!.getTotalPresentAndAbsent(attendance, mtgNo, cboId)
    }

    fun updateCutOffAttendance(
        attendance: String,
        mtgNo: Int,
        cboId: Long,
        memId: Long,
        updatedBy: String?,
        updatedOn: Long
    ) {
        voMtgDetDao!!.updateCutOffAttendance(
            attendance,
            mtgNo,
            cboId,
            memId,
            updatedBy,
            updatedOn
        )
    }

    fun getCutOffTotalPresent(mtgno: Int, cbo_id: Long): Int {
        return voMtgDetDao!!.getCutOffTotalPresent(mtgno, cbo_id)
    }

    fun updateAttendanceEC4(
        EC4: String,
        mtgno: Int,
        cbo_id: Long,
        memId: Long,
        updated_by: String,
        updated_on: Long
    ) {
        voMtgDetDao!!.updateAttendanceEC4(
            EC4,
            mtgno,
            cbo_id,
            memId,
            updated_by,
            updated_on
        )
    }

    fun updateAttendanceEC5(
        EC5: String,
        mtgno: Int,
        cbo_id: Long,
        memId: Long,
        updated_by: String,
        updated_on: Long
    ) {
        voMtgDetDao!!.updateAttendanceEC5(
            EC5,
            mtgno,
            cbo_id,
            memId,
            updated_by,
            updated_on
        )
    }

    internal fun getshgsumattendance(mtgno: Int, cbo_id: Long): Int {
        return voMtgDetDao!!.getshgsumattendance(mtgno, cbo_id)
    }

    fun getECPresent(mtgno: Int, cbo_id: Long, attendance: String): List<Int> {
        return voMtgDetDao!!.getECPresent(mtgno, cbo_id, attendance)
    }

    internal fun getTotalPresentAbsent(mtgNo: Int, cboId: Long): Int {
        return voMtgDetDao!!.getTotalPresentAbsent(mtgNo, cboId)
    }

    fun getECPresent(mtgno: Int, cbo_id: Long): Int {
        return voMtgDetDao!!.getECPresent(mtgno, cbo_id)
    }

    fun getVolsaving(mem_id: Long, mtgno: Int): Int {
        return voMtgDetDao!!.getVolsaving(mem_id, mtgno)
    }

    fun updateWithdrawal(mem_id: Long, mtgno: Int, Withdrawal: Int) {
        voMtgDetDao!!.updateWithdrawal(mem_id, mtgno, Withdrawal)
    }

    fun updatevolclosing(mem_id: Long, mtgno: Int, sav_vol_cb: Int) {
        voMtgDetDao!!.updatevolclosing(mem_id, mtgno, sav_vol_cb)
    }

    fun updateCompclosing(mem_id: Long, mtgno: Int, sav_comp_cb: Int) {
        voMtgDetDao!!.updateCompclosing(mem_id, mtgno, sav_comp_cb)
    }
     fun adjustCompclosing(mem_id: Long, mtgno: Int, sav_vol_cb: Int) {
        voMtgDetDao!!.adjustCompclosing(mem_id, mtgno, sav_vol_cb)
    }

    fun adjustvolclosing(mem_id: Long, mtgno: Int, sav_comp_cb: Int) {
        voMtgDetDao!!.adjustvolclosing(mem_id, mtgno, sav_comp_cb)
    }
     fun getmemdata(memid: Long?, mtgnum: Int): List<VoMtgDetEntity> {
        return voMtgDetDao!!.getmemdata(memid, mtgnum)
    }

    fun getVolclosing(mem_id: Long, mtgno: Int): Int {
        return voMtgDetDao!!.getVolclosing(mem_id, mtgno)
    }
    fun getCompclosing(mem_id: Long, mtgno: Int): Int {
        return voMtgDetDao!!.getCompclosing(mem_id, mtgno)
    }
    internal fun getListinactivemember(mtgno: Int, cbo_id: Long): List<VoMtgDetEntity> {
        return voMtgDetDao!!.getListinactivemember(mtgno, cbo_id)
    }

    fun getCompulsoryData(mtgno: Int, cbo_id: Long,mem_id: Long): List<VoMtgDetEntity>{
        return voMtgDetDao!!.getCompulsoryData(mtgno,cbo_id,mem_id)
    }

    fun updatesavingwithdrawl(
        withdrawl: Int,
        mtgno: Int,
        cbo_id: Long,
        memid: Long,
        updated_by:String,
        updated_on:Long
    ) {
        voMtgDetDao!!.updatesavingwithdrawl(
            withdrawl,
            mtgno,
            cbo_id,
            memid,
            updated_by,
            updated_on
        )
    }
}