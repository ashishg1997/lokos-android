package com.microware.cdfi.utility

import java.lang.Exception
import java.lang.StringBuilder
import java.nio.charset.StandardCharsets
import java.security.*
import javax.crypto.spec.SecretKeySpec
import java.security.spec.KeySpec
import java.util.*
import javax.crypto.*
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.PBEKeySpec

object AESEncryption {


    fun setKey(userId: String): SecretKey? {
        var userId = userId
        userId = userId.toUpperCase()
        val sb = StringBuilder(userId)
        val midString = userId + sb.reverse()
        val finalCharArr = CharArray(32)
        for (i in 0..31) {
            if (i == 0) {
                finalCharArr[0] = midString[0]
            } else {
                finalCharArr[i] = midString[i % midString.length]
            }
        }
        val finalString = String(finalCharArr)
        return SecretKeySpec(finalString.toByteArray(StandardCharsets.UTF_8), "AES")
    }

  /*  fun setKey(myKey: String): SecretKey {
        val saltBytes = byteArrayOf(1, 2, 3, 4, 5, 6, 7, 8)
        val factory =
            SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256")
        val spec: KeySpec =
            PBEKeySpec(myKey.toCharArray(), saltBytes, 65536, 256)
        return SecretKeySpec(
            factory.generateSecret(spec).encoded, "AES"
        )
    }*/

  /*  fun generateIv(): IvParameterSpec {
        val iv = byteArrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16)
        return IvParameterSpec(iv)
    }*/

    fun generateIv(): IvParameterSpec? {
        val iv = "zsefvbhukmhtfcvg"
        return IvParameterSpec(iv.toByteArray(StandardCharsets.UTF_8))
    }

    @Throws(
        NoSuchPaddingException::class,
        NoSuchAlgorithmException::class,
        InvalidAlgorithmParameterException::class,
        InvalidKeyException::class,
        BadPaddingException::class,
        IllegalBlockSizeException::class
    )
    fun aesencrypt(
        algorithm: String?, input: String?, key: SecretKey?,
        iv: IvParameterSpec?
    ): String {
        val cipher = Cipher.getInstance(algorithm)
        cipher.init(Cipher.ENCRYPT_MODE, key, iv)
        val cipherText = cipher.doFinal(input!!.toByteArray())
        return Base64.getEncoder()
            .encodeToString(cipherText)
    }

    @Throws(
        NoSuchPaddingException::class,
        NoSuchAlgorithmException::class,
        InvalidAlgorithmParameterException::class,
        InvalidKeyException::class,
        BadPaddingException::class,
        IllegalBlockSizeException::class
    )
    fun decrypt(
        algorithm: String?, cipherText: String?, key: SecretKey?,
        iv: IvParameterSpec?
    ): String {
        val cipher = Cipher.getInstance(algorithm)
        cipher.init(Cipher.DECRYPT_MODE, key, iv)
        val plainText = cipher.doFinal(Base64.getDecoder().decode(cipherText))
        return String(plainText)
    }

    val randomSalt: String
        get() {
            val random = Random()
            return String.format("%04d", random.nextInt(10000))
        }

    fun removeSaltFromPlainText(plainText: String): String {
        return plainText.substring(4)
    }


    fun encrypt(strToEncrypt: String?, secret: String?): String? {
        try {
            val secretValue = setKey(secret!!)
            val iv = generateIv()

            var plainTextWithSalt = randomSalt + strToEncrypt
            var encryptedpass=aesencrypt("AES/CBC/PKCS5Padding", plainTextWithSalt, secretValue, iv)
            var encryptedpassreplace=encryptedpass.replace("+","Microware@78534")
            return encryptedpassreplace

        } catch (e: Exception) {
            println("Error while encrypting: $e")
        }
        return null
    }

    fun decrypt(strToDecrypt: String?, secret: String?): String? {
        try {
            val secretValue = setKey(secret!!)
            val iv = generateIv()
            var decryptedpassreplace=strToDecrypt!!.replace("Microware@78534","+")

            val plainTextWithSaltDecrpt = decrypt("AES/CBC/PKCS5Padding", decryptedpassreplace, secretValue, iv)
            return removeSaltFromPlainText(plainTextWithSaltDecrpt)

        } catch (e: Exception) {
            println("Error while encrypting: $e")
        }
        return null
    }

}