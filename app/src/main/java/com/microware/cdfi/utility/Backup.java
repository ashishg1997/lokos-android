package com.microware.cdfi.utility;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import com.microware.cdfi.application.CDFIApplication;

import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.PeriodType;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okio.BufferedSource;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class Backup {
    Context context;
    int isUpload = 0;

    public Backup(Context context) {
        this.context = context;
    }

    String Imagedir = "CDFI";

    public void backup(String dbname) {
        int permission = ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission == PackageManager.PERMISSION_GRANTED) {
            //   AppDatabase appDatabase = DatabaseClient.getInstance(context).getAppDatabase();
            CDFIApplication.Companion.getDatabase().close();

            File db = context.getDatabasePath(dbname);
            File dbShm = new File(db.getParent(), dbname+"-shm");
            File dbWal = new File(db.getParent(), dbname+"-wal");

            File db2 = new File("/sdcard/", dbname);
            File dbShm2 = new File(db2.getParent(), dbname+"-shm");
            File dbWal2 = new File(db2.getParent(), dbname+"-wal");

            try {
                copyFile(db, db2);
                copyFile(dbShm, dbShm2);
                copyFile(dbWal, dbWal2);
            } catch (Exception e) {
                e.printStackTrace();
//                Log.e("SAVEDB", e.toString());
            }
        } else {
            Toast.makeText(context, "Please allow access to your storage", Toast.LENGTH_SHORT).show();
        }
    }

    public void copyFile(File inputFile, File outputFile) {
        try {
            InputStream input = new FileInputStream(inputFile);
            OutputStream output = new FileOutputStream(outputFile);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = input.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }
            output.flush();
            output.close();
            input.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public String changeDateFormat(String Dt) {
        if (Dt != null && Dt.length() > 0) {
            String[] Cdt = Dt.split("-");
            String DD = Cdt[0];
            String MM = Cdt[1];
            String YYYY = Cdt[2];
            String date = YYYY + "-" + MM + "-" + DD;
            return date;
        } else {
            return "";
        }
    }

    public String calculateAge(Date birthDate) {

        int years = 0;
        int months = 0;
        int days = 0;

        try {
         /*   long timeInMillis = Long.parseLong(strDate);
            Date birthDate = new Date(timeInMillis);*/


            //create calendar object for birth day
            Calendar birthDay = Calendar.getInstance();
            birthDay.setTimeInMillis(birthDate.getTime());
            //create calendar object for current day
            long currentTime = System.currentTimeMillis();
            Calendar now = Calendar.getInstance();
            now.setTimeInMillis(currentTime);
            //Get difference between years
            years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
            int currMonth = now.get(Calendar.MONTH) + 1;
            int birthMonth = birthDay.get(Calendar.MONTH) + 1;

            //Get difference between months
            months = currMonth - birthMonth;


            //if month difference is in negative then reduce years by one and calculate the number of months.
            if (months < 0) {
                years--;
                months = 12 - birthMonth + currMonth;
                if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
                    months--;
            } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
                years--;
                months = 11;
            }
            //Calculate the days
            if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
                days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
            else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
                int today = now.get(Calendar.DAY_OF_MONTH);
                now.add(Calendar.MONTH, -1);
                days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;
            } else {
                days = 0;
                if (months == 12) {
                    years++;
                    months = 0;
                }
            }
            //adarsh
            if (currMonth > birthMonth) {
                if (birthDay.get(Calendar.DATE) > now.get(Calendar.DATE)) {
                    months = months - 1;
                }
            }//---------------------------------

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Create new Age object
        return years + " - " + months + " - " + days;
    }


    public String AgeCalculator(String DOB, String C_dt) {

        String sAge = "";

        int DOB_yr = Integer.valueOf(DOB.split("-")[2]);
        int DOB_month = Integer.valueOf(DOB.split("-")[1]);
        int DOB_day = Integer.valueOf(DOB.split("-")[0]);

        int C_dt_yr = Integer.valueOf(C_dt.split("-")[2]);
        int C_dt_month = Integer.valueOf(C_dt.split("-")[1]);
        int C_dt_day = Integer.valueOf(C_dt.split("-")[0]);

        //   LocalDate birthdate = new LocalDate (1970, 1, 20);

        LocalDate birthdate = new LocalDate(DOB_yr, DOB_month, DOB_day);      //Birth date
        LocalDate compare_dt = new LocalDate(C_dt_yr, C_dt_month, C_dt_day);                        //Today's date

        Period period = new Period(birthdate, compare_dt, PeriodType.yearMonthDay());

//Now access the values as below
        System.out.println(period.getDays());
        System.out.println(period.getMonths());
        System.out.println(period.getYears());
        sAge = period.getYears() + "-" + period.getMonths() + "-" + period.getDays();
        return sAge;
    }

    public String sDOBCalculator(int DObyear, int Dobmonth, int Dobday, String C_dt) {
        String sDOB = "";

        int C_dt_yr = Integer.valueOf(C_dt.split("-")[2]);
        int C_dt_month = Integer.valueOf(C_dt.split("-")[1]);
        int C_dt_day = Integer.valueOf(C_dt.split("-")[0]);

        LocalDate compare_dt = new LocalDate(C_dt_yr, C_dt_month, C_dt_day);
        LocalDate dob = compare_dt.minusYears(DObyear)
                .minusMonths(Dobmonth)
                .minusDays(Dobday);

        /*DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");*/

        /*DateTimeFormatter dateTimeFormatter;
        dateTimeFormatter = DateTimeFormatter.ISO_LOCAL_DATE;*/

        sDOB = dob.toString();

        return sDOB;
    }

    public void sms(final Activity activity, String phone) {

        String phoneNum = phone;
        if (phoneNum.length() > 0) {
            String dial = "tel:" + phoneNum;
            try {

                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.putExtra("sms_body", "default content");
                sendIntent.putExtra("address", phoneNum);
                sendIntent.setType("vnd.android-dir/mms-sms");
                activity.startActivity(sendIntent);

            } catch (Exception e) {

                e.printStackTrace();
            }


        } else {
            Toast.makeText(activity, "Please enter a valid telephone number", Toast.LENGTH_SHORT).show();
        }

    }



    public String[] returnarray(final String str) {

        String[] sstrarr = str.split(",");
        return sstrarr;
    }
public MultipartBody.Part[] returnmultipart( int n) {

        return new MultipartBody.Part[n];


    }

    public String getpath(Context context, Uri uri) {
        String path = "";
        try {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = context.getContentResolver().query(uri, filePathColumn, null, null, null);
            if (cursor.moveToFirst()) {
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                path = cursor.getString(columnIndex);
            } else {
                //boooo, cursor doesn't have rows ...
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return path;
    }

}
