package com.microware.cdfi.utility


import android.text.format.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class DateUtil {

    fun getDate(date: Long): String? {
        val calendar: Calendar = Calendar.getInstance(Locale.ENGLISH)
        val time = System.currentTimeMillis()
        calendar.setTimeInMillis(date * 1000L)

        //dd=day, MM=month, yyyy=year, hh=hour, mm=minute, ss=second.
        return DateFormat.format("dd-MM-yyyy", calendar).toString()
    }

    fun setDate(dateString: String): Long?{
        val date = SimpleDateFormat("dd-MM-yyyy").parse(dateString)
        println(date.time)
        return date.time
    }
}