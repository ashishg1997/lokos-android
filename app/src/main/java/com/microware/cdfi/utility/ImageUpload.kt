package com.microware.cdfi.utility
import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.os.Environment
import com.microware.cdfi.api.ApiClientConnection
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.api.response.ImageUploadResponse
import com.microware.cdfi.viewModel.Memberviewmodel
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class ImageUpload : IntentService("service") {
    var validate: Validate? = null
    var apiInterface: ApiInterface? = null
    var memberviewmodel: Memberviewmodel? = null

    override fun onCreate() {
        super.onCreate()
        // If a Context object is needed, call getApplicationContext() here.
        validate = Validate(applicationContext)
        memberviewmodel = Memberviewmodel(application)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
    }

    override fun onHandleIntent(intent: Intent?) {

        var listdata = memberviewmodel!!.getAllMemberlist("")
        if (!listdata.isNullOrEmpty()) {
            for (i in listdata.indices) {
                var iName = listdata.get(i).member_image
                var iType = 1

                uploadImages(iName, iType, applicationContext)
            }
        }

    }

    fun uploadImages(imageName: String?, iType: Int?, context: Context) {
        val storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!.toString()
        var sFullImagePath = ""
        val mediaStorageDir = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
            Config().IMAGE_DIRECTORY_NAME
        )
        sFullImagePath = mediaStorageDir.path + File.separator + imageName
        val imagefile = File(sFullImagePath)
        uploadImage(imagefile, imageName, iType, apiInterface)

    }

    fun uploadImage(file: File, imageName: String?, iType: Int?, apiCallback: ApiInterface?) {

        if (file.exists()) {

            var call: Call<ImageUploadResponse>? = null
            var userData = HashMap<String, RequestBody>()
            userData.put(
                "username",
                RequestBody.create(
                    MediaType.parse("text/plain"),
                    validate!!.RetriveSharepreferenceString(AppSP.userid)!!
                )
            )

            val file1 = RequestBody.create(MediaType.parse("multipart/form-data"), file)
            val imagePart = MultipartBody.Part.createFormData("image_name", file.name, file1)

            if (iType == 1) {
                //call = apiCallback!!.postUploadImage(userData, imagePart)
            }
           /* else if (iType == 2) {
                call = apiCallback!!.postUploadShgInspectionImage(userData, imagePart)
            } else if (iType == 3) {
                call = apiCallback!!.postUploadThrMixingImage(userData, imagePart)
            } else if (iType == 4) {
                call = apiCallback!!.postUploadAwcthrreceivedImage(userData, imagePart)
            } else if (iType == 5) {
                call = apiCallback!!.postUploadAwcthrdistributionImage(userData, imagePart)
            } else if (iType == 6) {
                call = apiCallback!!.postUploadVisitorImage(userData, imagePart)
            } else if (iType == 7) {
                call = apiCallback!!.postUploadGrievanceImage(userData, imagePart)
            }*/

            call!!.enqueue(object : Callback<ImageUploadResponse> {
                override fun onResponse(
                    call: Call<ImageUploadResponse>,
                    response: Response<ImageUploadResponse>) {
                    if (response.isSuccessful) {
                        if (response.body()!!.status == 200) {
                            //imageViewModel!!.deleteImage(response.body()?.image_name)
                        }
                    }
                }

                override fun onFailure(call: Call<ImageUploadResponse>, t: Throwable) {
                    call.cancel()
                    t.message
                }
            })

        }
    }


}