package com.microware.cdfi.utility;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.microware.cdfi.api.meetingdatamodel.ShgFinanceTransactionDetailGroupUploadListData;
import com.microware.cdfi.api.meetingdatamodel.ShgFinanceTransactionDetailMemberUploadData;
import com.microware.cdfi.api.meetingdatamodel.ShgFinanceTransactionUploadListData;
import com.microware.cdfi.api.meetingdatamodel.ShgFinancialTxnVouchersDataModel;
import com.microware.cdfi.api.meetingdatamodel.ShgGroupLoanScheduleUploadData;
import com.microware.cdfi.api.meetingdatamodel.ShgGroupLoanTransactionUploadData;
import com.microware.cdfi.api.meetingdatamodel.ShgGroupLoanUploadData;
import com.microware.cdfi.api.meetingdatamodel.ShgLoanApplicationListUploadData;
import com.microware.cdfi.api.meetingdatamodel.ShgMeberLoanScheduleUploadData;
import com.microware.cdfi.api.meetingdatamodel.ShgMeetingDetailsUploadListData;
import com.microware.cdfi.api.meetingdatamodel.ShgMemberLoanListUploadData;
import com.microware.cdfi.api.meetingdatamodel.ShgMemberLoanTransactionUploadListData;
import com.microware.cdfi.api.meetingdatamodel.ShgMemberSettlementListDataModel;
import com.microware.cdfi.api.meetingdatamodel.UploadData;
import com.microware.cdfi.api.meetingdatamodel.UploadMeetingData;
import com.microware.cdfi.api.meetinguploadmodel.DownloadMeetingData;
import com.microware.cdfi.api.meetinguploadmodel.ShgFinanceTransactionDetailGroupListData;
import com.microware.cdfi.api.meetinguploadmodel.ShgFinanceTransactionDetailMemberData;
import com.microware.cdfi.api.meetinguploadmodel.ShgFinanceTransactionListData;
import com.microware.cdfi.api.meetinguploadmodel.ShgGroupLoanData;
import com.microware.cdfi.api.meetinguploadmodel.ShgGroupLoanScheduleData;
import com.microware.cdfi.api.meetinguploadmodel.ShgGroupLoanTransactionData;
import com.microware.cdfi.api.meetinguploadmodel.ShgLoanApplicationListData;
import com.microware.cdfi.api.meetinguploadmodel.ShgMcpData;
import com.microware.cdfi.api.meetinguploadmodel.ShgMeberLoanScheduleData;
import com.microware.cdfi.api.meetinguploadmodel.ShgMeetingDetailsListData;
import com.microware.cdfi.api.meetinguploadmodel.ShgMemberLoanListData;
import com.microware.cdfi.api.meetinguploadmodel.ShgMemberLoanTransactionListData;
import com.microware.cdfi.api.model.ClfVoMemberModel;
import com.microware.cdfi.api.model.ClfVoShgMappingModel;
import com.microware.cdfi.api.model.SHGMemberUploadModel;
import com.microware.cdfi.api.model.SHGUploadModel;
import com.microware.cdfi.api.model.SubcommitteeModel;
import com.microware.cdfi.api.model.VODownloadModel;
import com.microware.cdfi.api.model.VOUploadModel;
import com.microware.cdfi.api.model.VoShgMappingModel;
import com.microware.cdfi.api.model.VoShgMemberModel;
import com.microware.cdfi.api.vomeetingmodel.VoGroupLoanDataModel;
import com.microware.cdfi.api.vomeetingmodel.VoMeetingUploadData;
import com.microware.cdfi.api.vomeetingmodel.VoMemLoanDataModel;
import com.microware.cdfi.api.vomeetingmodel.VoMtgDataModel;
import com.microware.cdfi.api.vomeetingmodel.VoMtgDetailDataModel;
import com.microware.cdfi.entity.ClfVoMemberEntity;
import com.microware.cdfi.entity.DtLoanApplicationEntity;
import com.microware.cdfi.entity.DtLoanGpEntity;
import com.microware.cdfi.entity.DtLoanGpTxnEntity;
import com.microware.cdfi.entity.DtLoanMemberEntity;
import com.microware.cdfi.entity.DtLoanMemberSheduleEntity;
import com.microware.cdfi.entity.DtLoanTxnMemEntity;
import com.microware.cdfi.entity.DtMemSettlementEntity;
import com.microware.cdfi.entity.DtMtgFinTxnEntity;
import com.microware.cdfi.entity.DtMtgGrpLoanScheduleEntity;
import com.microware.cdfi.entity.DtmtgDetEntity;
import com.microware.cdfi.entity.DtmtgEntity;
import com.microware.cdfi.entity.FederationEntity;
import com.microware.cdfi.entity.FinancialTransactionsMemEntity;
import com.microware.cdfi.entity.MappedShgEntity;
import com.microware.cdfi.entity.MappedVoEntity;
import com.microware.cdfi.entity.MemberEntity;
import com.microware.cdfi.entity.SHGEntity;
import com.microware.cdfi.entity.ShgFinancialTxnDetailEntity;
import com.microware.cdfi.entity.ShgFinancialTxnVouchersEntity;
import com.microware.cdfi.entity.ShgMcpEntity;
import com.microware.cdfi.entity.VoShgMemberEntity;
import com.microware.cdfi.entity.subcommitteeEntity;
import com.microware.cdfi.entity.voentity.VoGroupLoanEntity;
import com.microware.cdfi.entity.voentity.VoMemLoanEntity;
import com.microware.cdfi.entity.voentity.VoMtgDetEntity;
import com.microware.cdfi.entity.voentity.VomtgEntity;

import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MappingData {

    public static List<Object> returnADDListObj(List<Object> mod)
    {
        ObjectMapper mapper = new ObjectMapper();
        Gson gson = new Gson();
        List<Object> myObjects = null;
        try {
            String json=gson.toJson(mod);
            //  myObjects = mapper.readValue(json, new TypeReference<List<UserEntity>>(){});
            myObjects=mapper.readValue(json, mapper.getTypeFactory().constructCollectionType(List.class, Object.class));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return myObjects;
    }

  /*  public static SHGUploadModel returnObj(Object mod)
    {
        ObjectMapper mapper = new ObjectMapper();
        Gson gson = new Gson();
        SHGUploadModel myObjects = null;
        try {
            String json=gson.toJson(mod);
              myObjects = mapper.readValue(json, new TypeReference<Object>(){});
          //  myObjects=mapper.readValue(json, mapper.getTypeFactory().constructCollectionType(List.class, Object.class));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return myObjects;
    }
*/

    public static SHGUploadModel returnObj(Object mod)
    {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        return mapper.map(mod, SHGUploadModel.class);
    }

    /*Change 1*/
    public static UploadMeetingData returnmeetingObj(Object mod) {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        return mapper.map(mod, UploadMeetingData.class);
    }

    public static SHGEntity returnObjentity(SHGUploadModel mod) {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(mod, SHGEntity.class);
    }


    public static List<MemberEntity> returnmemberentity(List<SHGMemberUploadModel> mod)
    {
        List<MemberEntity> meberlist=new ArrayList<>();
        ModelMapper mapper = new ModelMapper();
        for (int i=0;i<mod.size();i++){
            meberlist.add(mapper.map(mod.get(i), MemberEntity.class));
        }

        return meberlist;
    }
    public static SHGMemberUploadModel returnObjmember(Object mod)
    {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        return mapper.map(mod, SHGMemberUploadModel.class);
    }
    /*public static List<SHGMemberUploadModel> returnSHGMemberUploadModelistObj(List<MemberEntity> mod)
    {
        ObjectMapper mapper = new ObjectMapper();
        Gson gson = new Gson();
        List<SHGMemberUploadModel> myObjects = null;
        try {
            String json=gson.toJson(mod);
            //  myObjects = mapper.readValue(json, new TypeReference<List<UserEntity>>(){});
            myObjects=mapper.readValue(json, mapper.getTypeFactory().constructCollectionType(List.class, SHGMemberUploadModel.class));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return myObjects;
    }*/
    public static List<SHGMemberUploadModel> returnSHGMemberUploadModelistObj(List<MemberEntity> mod) {
        List<SHGMemberUploadModel> shgMemberUploadModels=new ArrayList<SHGMemberUploadModel>();
        for (int i=0;i<mod.size();i++){
            shgMemberUploadModels.add(returnObjmember(mod.get(i)));
        }
        return shgMemberUploadModels;
    }


    public static VOUploadModel returnObject(Object mod)
    {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(mod, VOUploadModel.class);
    }
    public static FederationEntity returnObjentity(VOUploadModel mod)
    {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(mod, FederationEntity.class);
    }

    public static List<MappedShgEntity> returnVO_Shg_listObj(List<VoShgMappingModel> mod) {
        List<MappedShgEntity> VO_SHG_Model=new ArrayList<MappedShgEntity>();
        for (int i=0;i<mod.size();i++){
            VO_SHG_Model.add(returnObjvo_shg(mod.get(i)));
        }
        return VO_SHG_Model;
    }

    public static MappedShgEntity returnObjvo_shg(Object mod)
    {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(mod, MappedShgEntity.class);
    }


    public static List<VoShgMemberEntity> returnVO_Shgmemberentity(List<VoShgMemberModel> mod)
    {
        List<VoShgMemberEntity> meberlist=new ArrayList<>();
        ModelMapper mapper = new ModelMapper();
        for (int i=0;i<mod.size();i++){
            meberlist.add(mapper.map(mod.get(i), VoShgMemberEntity.class));
        }

        return meberlist;
    }
    public static FederationEntity returnObjentity(VODownloadModel mod)
    {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(mod, FederationEntity.class);
    }


    public static subcommitteeEntity returnSubCommitteeEntity(SubcommitteeModel mod)
    {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(mod, subcommitteeEntity.class);
    }
    public static SubcommitteeModel returnScObject(Object mod)
    {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(mod, SubcommitteeModel.class);
    }

    public static List<subcommitteeEntity> returnScEntity(List<SubcommitteeModel> mod)
    {
        List<subcommitteeEntity> scList=new ArrayList<>();
        ModelMapper mapper = new ModelMapper();
        for (int i=0;i<mod.size();i++){
            scList.add(mapper.map(mod.get(i), subcommitteeEntity.class));
        }

        return scList;
    }
    public static List<MappedVoEntity> returnCLF_VO_listObj(List<ClfVoShgMappingModel> mod) {
        List<MappedVoEntity> clf_vo_Model=new ArrayList<MappedVoEntity>();
        for (int i=0;i<mod.size();i++){
            clf_vo_Model.add(returnObjclf_vo(mod.get(i)));
        }
        return clf_vo_Model;
    }

    public static MappedVoEntity returnObjclf_vo(Object mod)
    {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(mod, MappedVoEntity.class);
    }


    public static List<ClfVoMemberEntity> returnClf_VO_memberentity(List<ClfVoMemberModel> mod)
    {
        List<ClfVoMemberEntity> meberlist=new ArrayList<>();
        ModelMapper mapper = new ModelMapper();
        for (int i=0;i<mod.size();i++){
            meberlist.add(mapper.map(mod.get(i), ClfVoMemberEntity.class));
        }

        return meberlist;
    }

    //2
    public static List<ShgMeetingDetailsUploadListData> returnmeetingDetailObj(List<DtmtgDetEntity> mod) {
        List<ShgMeetingDetailsUploadListData> shgMeetingDetaillist =new ArrayList<ShgMeetingDetailsUploadListData>();
        for (int i=0;i<mod.size();i++){
            shgMeetingDetaillist.add(returnObjmeetingDetail(mod.get(i)));
        }

        return shgMeetingDetaillist;
    }

    //2
    public static ShgMeetingDetailsUploadListData returnObjmeetingDetail(Object mod) {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        return mapper.map(mod, ShgMeetingDetailsUploadListData.class);
    }

    /*mcp new*/
    public static List<ShgMcpData> returnmcpObj(List<ShgMcpEntity> mod) {
        List<ShgMcpData> listData=new ArrayList<ShgMcpData>();
        for (int i=0;i<mod.size();i++){
            listData.add(returnmcpList(mod.get(i)));
        }
        return listData;
    }

    public static ShgMcpData returnmcpList(Object mod) {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        return mapper.map(mod, ShgMcpData.class);
    }
    //3
    public static List<ShgFinanceTransactionUploadListData> returnShgFinanceTransactionListObj(List<DtMtgFinTxnEntity> mod) {
        List<ShgFinanceTransactionUploadListData> listData=new ArrayList<ShgFinanceTransactionUploadListData>();
        for (int i=0;i<mod.size();i++){
            listData.add(returnShgFinanceTransactionList(mod.get(i)));
        }
        return listData;
    }

    //3
    public static ShgFinanceTransactionUploadListData returnShgFinanceTransactionList(Object mod) {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        return mapper.map(mod, ShgFinanceTransactionUploadListData.class);
    }

    //4
    public static List<ShgFinanceTransactionDetailGroupUploadListData> returnShgFinanceTransactionGrpListObj(List<ShgFinancialTxnDetailEntity> mod) {
        List<ShgFinanceTransactionDetailGroupUploadListData> listData=new ArrayList<ShgFinanceTransactionDetailGroupUploadListData>();
        for (int i=0;i<mod.size();i++){
            listData.add(returnShgFinanceTransactionGrpList(mod.get(i)));
        }
        return listData;
    }
//4
    public static ShgFinanceTransactionDetailGroupUploadListData returnShgFinanceTransactionGrpList(Object mod) {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        return mapper.map(mod, ShgFinanceTransactionDetailGroupUploadListData.class);
    }

    //8
    public static List<ShgGroupLoanUploadData> returnGrpLoanObj(List<DtLoanGpEntity> mod) {
        List<ShgGroupLoanUploadData> listData=new ArrayList<ShgGroupLoanUploadData>();
        for (int i=0;i<mod.size();i++){
            listData.add(returnShgGrpLoan(mod.get(i)));
        }
        return listData;
    }
//8
    public static ShgGroupLoanUploadData returnShgGrpLoan(Object mod) {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        return mapper.map(mod, ShgGroupLoanUploadData.class);
    }

    //5
    public static List<ShgGroupLoanTransactionUploadData> returnGrpLoanTxnObj(List<DtLoanGpTxnEntity> mod) {
        List<ShgGroupLoanTransactionUploadData> listData=new ArrayList<ShgGroupLoanTransactionUploadData>();
        for (int i=0;i<mod.size();i++){
            listData.add(returnShgGrpLoanTxn(mod.get(i)));
        }
        return listData;
    }

    //5
    public static ShgGroupLoanTransactionUploadData returnShgGrpLoanTxn(Object mod) {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        return mapper.map(mod, ShgGroupLoanTransactionUploadData.class);
    }
//10
    public static List<ShgMeberLoanScheduleUploadData> returnmemberLoanScheduleObj(List<DtLoanMemberSheduleEntity> mod) {
        List<ShgMeberLoanScheduleUploadData> listData=new ArrayList<ShgMeberLoanScheduleUploadData>();
        for (int i=0;i<mod.size();i++){
            listData.add(returnmemberLoanSchedule(mod.get(i)));
        }
        return listData;
    }
//10
    public static ShgMeberLoanScheduleUploadData returnmemberLoanSchedule(Object mod) {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        return mapper.map(mod, ShgMeberLoanScheduleUploadData.class);
    }
//9
    public static List<ShgGroupLoanScheduleUploadData> returnGrpLoanScheduleObj(List<DtMtgGrpLoanScheduleEntity> mod) {
        List<ShgGroupLoanScheduleUploadData> listData=new ArrayList<ShgGroupLoanScheduleUploadData>();
        for (int i=0;i<mod.size();i++){
            listData.add(returnGrpLoanSchedule(mod.get(i)));
        }
        return listData;
    }
//9
    public static ShgGroupLoanScheduleUploadData returnGrpLoanSchedule(Object mod) {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        return mapper.map(mod, ShgGroupLoanScheduleUploadData.class);
    }

    public static ShgMeetingDetailsListData returnShgMeetingDetailObj(Object mod) {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        return mapper.map(mod, ShgMeetingDetailsListData.class);
    }
//12
    public static List<ShgLoanApplicationListUploadData> returnloanApplicationObj(List<DtLoanApplicationEntity> mod) {
        List<ShgLoanApplicationListUploadData> listData=new ArrayList<ShgLoanApplicationListUploadData>();
        for (int i=0;i<mod.size();i++){
            listData.add(returnloanApplication(mod.get(i)));
        }
        return listData;
    }
//12
    public static ShgLoanApplicationListUploadData returnloanApplication(Object mod) {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        return mapper.map(mod, ShgLoanApplicationListUploadData.class);
    }

    //9
    public static List<ShgMemberLoanListUploadData> returnmemLoanListObj(List<DtLoanMemberEntity> mod) {
        List<ShgMemberLoanListUploadData> listData=new ArrayList<ShgMemberLoanListUploadData>();
        for (int i=0;i<mod.size();i++){
            listData.add(returnmemLoanList(mod.get(i)));
        }
        return listData;
    }
//9
    public static ShgMemberLoanListUploadData returnmemLoanList(Object mod) {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        return mapper.map(mod, ShgMemberLoanListUploadData.class);
    }

    //6
    public static List<ShgFinanceTransactionDetailMemberUploadData> returnmemfintxndetailObj(List<FinancialTransactionsMemEntity> mod) {
        List<ShgFinanceTransactionDetailMemberUploadData> listData=new ArrayList<ShgFinanceTransactionDetailMemberUploadData>();
        for (int i=0;i<mod.size();i++){
            listData.add(returnmemfintxndetail(mod.get(i)));
        }
        return listData;
    }

    //6
    public static ShgFinanceTransactionDetailMemberUploadData returnmemfintxndetail(Object mod) {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        return mapper.map(mod, ShgFinanceTransactionDetailMemberUploadData.class);
    }

    //7
    public static List<ShgMemberLoanTransactionUploadListData> returnmemloanTxnObj(List<DtLoanTxnMemEntity> mod) {
        List<ShgMemberLoanTransactionUploadListData> listData=new ArrayList<ShgMemberLoanTransactionUploadListData>();
        for (int i=0;i<mod.size();i++){
            listData.add(returnmemloanTxn(mod.get(i)));
        }
        return listData;
    }
//7
    public static ShgMemberLoanTransactionUploadListData returnmemloanTxn(Object mod) {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        return mapper.map(mod, ShgMemberLoanTransactionUploadListData.class);
    }
//13
    public static UploadData returnUploadDataObj(Object mod) {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        return mapper.map(mod, UploadData.class);
    }



    /*meeting download*/

    public static DtmtgEntity returnmtgObjentity(DownloadMeetingData mod) {
        List<DtmtgEntity> listData=new ArrayList<>();
        ModelMapper mapper = new ModelMapper();
        return mapper.map(mod, DtmtgEntity.class);
    }

    public static List<DtLoanGpEntity> returnGpmtgObjentity(List<ShgGroupLoanData> mod) {
        List<DtLoanGpEntity> listData=new ArrayList<>();
        ModelMapper mapper = new ModelMapper();
        for (int i=0;i<mod.size();i++){
            listData.add(mapper.map(mod.get(i), DtLoanGpEntity.class));
        }

        return listData;
    }

    public static List<DtLoanMemberEntity> returnmemmtgObjentity(List<ShgMemberLoanListData> mod) {
        List<DtLoanMemberEntity> listData=new ArrayList<>();
        ModelMapper mapper = new ModelMapper();
        for (int i=0;i<mod.size();i++){
            listData.add(mapper.map(mod.get(i), DtLoanMemberEntity.class));
        }

        return listData;
    }

    public static List<DtLoanApplicationEntity> returnloanAppliObjentity(List<ShgLoanApplicationListData> mod) {
        List<DtLoanApplicationEntity> listData=new ArrayList<>();
        ModelMapper mapper = new ModelMapper();
        for (int i=0;i<mod.size();i++){
            listData.add(mapper.map(mod.get(i), DtLoanApplicationEntity.class));
        }

        return listData;
    }

    public static List<DtMtgGrpLoanScheduleEntity> returngploanScheduleObjentity(List<ShgGroupLoanScheduleData> mod) {
        List<DtMtgGrpLoanScheduleEntity> listData=new ArrayList<>();
        ModelMapper mapper = new ModelMapper();
        for (int i=0;i<mod.size();i++){
            listData.add(mapper.map(mod.get(i), DtMtgGrpLoanScheduleEntity.class));
        }

        return listData;
    }

    public static List<DtLoanMemberSheduleEntity> returnmembloanScheduleObjentity(List<ShgMeberLoanScheduleData> mod) {
        List<DtLoanMemberSheduleEntity> listData=new ArrayList<>();
        ModelMapper mapper = new ModelMapper();
        for (int i=0;i<mod.size();i++){
            listData.add(mapper.map(mod.get(i), DtLoanMemberSheduleEntity.class));
        }

        return listData;
    }

    public static List<ShgMcpEntity> returnmcpObjentity(List<ShgMcpData> mod) {
        List<ShgMcpEntity> listData=new ArrayList<>();
        ModelMapper mapper = new ModelMapper();
        for (int i=0;i<mod.size();i++){
            listData.add(mapper.map(mod.get(i), ShgMcpEntity.class));
        }

        return listData;
    }

    public static List<DtmtgDetEntity> returnshgMeetingDetailObjentity(List<ShgMeetingDetailsListData> mod) {
        List<DtmtgDetEntity> listData=new ArrayList<>();
        ModelMapper mapper = new ModelMapper();
        for (int i=0;i<mod.size();i++){
            listData.add(mapper.map(mod.get(i), DtmtgDetEntity.class));
        }

        return listData;
    }

    public static List<DtMtgFinTxnEntity> returnshgshgFinanceTxnObjentity(List<ShgFinanceTransactionListData> mod) {
        List<DtMtgFinTxnEntity> listData=new ArrayList<>();
        ModelMapper mapper = new ModelMapper();
        for (int i=0;i<mod.size();i++){
            listData.add(mapper.map(mod.get(i), DtMtgFinTxnEntity.class));
        }

        return listData;
    }

    public static List<ShgFinancialTxnDetailEntity> returnshgFinanceTxnDtGroupObjentity(List<ShgFinanceTransactionDetailGroupListData> mod) {
        List<ShgFinancialTxnDetailEntity> listData=new ArrayList<>();
        ModelMapper mapper = new ModelMapper();
        for (int i=0;i<mod.size();i++){
            listData.add(mapper.map(mod.get(i), ShgFinancialTxnDetailEntity.class));
        }

        return listData;
    }

    public static List<DtLoanGpTxnEntity> returnshgGrpLoanTxnObjentity(List<ShgGroupLoanTransactionData> mod) {
        List<DtLoanGpTxnEntity> listData=new ArrayList<>();
        ModelMapper mapper = new ModelMapper();
        for (int i=0;i<mod.size();i++){
            listData.add(mapper.map(mod.get(i), DtLoanGpTxnEntity.class));
        }

        return listData;
    }

    public static List<DtLoanTxnMemEntity> returnshgmemLoanTxnObjentity(List<ShgMemberLoanTransactionListData> mod) {
        List<DtLoanTxnMemEntity> listData=new ArrayList<>();
        ModelMapper mapper = new ModelMapper();
        for (int i=0;i<mod.size();i++){
            listData.add(mapper.map(mod.get(i), DtLoanTxnMemEntity.class));
        }

        return listData;
    }

    public static List<FinancialTransactionsMemEntity> returnFinanceTxnDtMemberObjentity(List<ShgFinanceTransactionDetailMemberData> mod) {
        List<FinancialTransactionsMemEntity> listData=new ArrayList<>();
        ModelMapper mapper = new ModelMapper();
        for (int i=0;i<mod.size();i++){
            listData.add(mapper.map(mod.get(i), FinancialTransactionsMemEntity.class));
        }

        return listData;
    }

    //VoMeetingData
    public static VomtgEntity returnvomtgObjentity(VoMtgDataModel mod) {
        List<VomtgEntity> listData=new ArrayList<>();
        ModelMapper mapper = new ModelMapper();
        return mapper.map(mod, VomtgEntity.class);
    }

    public static List<VoMemLoanEntity> returnvomemLoanObjentity(List<VoMemLoanDataModel> mod) {
        List<VoMemLoanEntity> listData=new ArrayList<>();
        ModelMapper mapper = new ModelMapper();
        for (int i=0;i<mod.size();i++){
            listData.add(mapper.map(mod.get(i), VoMemLoanEntity.class));
        }

        return listData;
    }

    public static List<VoGroupLoanEntity> returnvogroupLoanmtgObjentity(List<VoGroupLoanDataModel> mod) {
        List<VoGroupLoanEntity> listData=new ArrayList<>();
        ModelMapper mapper = new ModelMapper();
        for (int i=0;i<mod.size();i++){
            listData.add(mapper.map(mod.get(i), VoGroupLoanEntity.class));
        }

        return listData;
    }

    public static List<VoMtgDetEntity> returnvomtgDetailObjentity(List<VoMtgDetailDataModel> mod) {
        List<VoMtgDetEntity> listData=new ArrayList<>();
        ModelMapper mapper = new ModelMapper();
        for (int i=0;i<mod.size();i++){
            listData.add(mapper.map(mod.get(i), VoMtgDetEntity.class));
        }

        return listData;
    }

    public static VoMeetingUploadData returnvomeetingObj(Object mod) {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        return mapper.map(mod, VoMeetingUploadData.class);
    }

    public static VoMtgDataModel returnmeetingdataObj(Object mod) {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        return mapper.map(mod, VoMtgDataModel.class);
    }

    public static List<VoMemLoanDataModel> returnmemberLoandataObj(List<VoMemLoanEntity> mod) {
        List<VoMemLoanDataModel> vomemLoanData =new ArrayList<VoMemLoanDataModel>();
        for (int i=0;i<mod.size();i++){
            vomemLoanData.add(returnObjmemLoanData(mod.get(i)));
        }

        return vomemLoanData;
    }

    public static VoMemLoanDataModel returnObjmemLoanData(Object mod) {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        return mapper.map(mod, VoMemLoanDataModel.class);
    }

    public static List<VoGroupLoanDataModel> returnGroupLoandataObj(List<VoGroupLoanEntity> mod) {
        List<VoGroupLoanDataModel> vomemLoanData =new ArrayList<VoGroupLoanDataModel>();
        for (int i=0;i<mod.size();i++){
            vomemLoanData.add(returnObjgrpLoanData(mod.get(i)));
        }

        return vomemLoanData;
    }

    public static VoGroupLoanDataModel returnObjgrpLoanData(Object mod) {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        return mapper.map(mod, VoGroupLoanDataModel.class);
    }

    public static List<VoMtgDetailDataModel> returnlvomtgDetaidataObj(List<VoMtgDetEntity> mod) {
        List<VoMtgDetailDataModel> vomemLoanData =new ArrayList<VoMtgDetailDataModel>();
        for (int i=0;i<mod.size();i++){
            vomemLoanData.add(returnObjgrpLoanvomtgDetaiData(mod.get(i)));
        }

        return vomemLoanData;
    }

    public static VoMtgDetailDataModel returnObjgrpLoanvomtgDetaiData(Object mod) {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        return mapper.map(mod, VoMtgDetailDataModel.class);
    }
    public static List<ShgMemberSettlementListDataModel> returnmemSettelmemtObj(List<DtMemSettlementEntity> mod) {
        List<ShgMemberSettlementListDataModel> listData=new ArrayList<ShgMemberSettlementListDataModel>();
        for (int i=0;i<mod.size();i++){
            listData.add(returnshgmemSettelment(mod.get(i)));
        }
        return listData;
    }

    public static ShgMemberSettlementListDataModel returnshgmemSettelment(Object mod) {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        return mapper.map(mod, ShgMemberSettlementListDataModel.class);
    }

    public static List<DtMemSettlementEntity> returnShgMemSettelmentObjentity(List<ShgMemberSettlementListDataModel> mod) {
        List<DtMemSettlementEntity> listData=new ArrayList<>();
        ModelMapper mapper = new ModelMapper();
        for (int i=0;i<mod.size();i++){
            listData.add(mapper.map(mod.get(i), DtMemSettlementEntity.class));
        }

        return listData;
    }
    public static List<ShgFinancialTxnVouchersDataModel> returnShgFinTxnVoucherTxnObj(List<ShgFinancialTxnVouchersEntity> mod) {
        List<ShgFinancialTxnVouchersDataModel> listData=new ArrayList<ShgFinancialTxnVouchersDataModel>();
        for (int i=0;i<mod.size();i++){
            listData.add(returnShgFinTxnVoucher(mod.get(i)));
        }
        return listData;
    }

    public static ShgFinancialTxnVouchersDataModel returnShgFinTxnVoucher(Object mod) {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        return mapper.map(mod, ShgFinancialTxnVouchersDataModel.class);
    }

    public static List<ShgFinancialTxnVouchersEntity> returnshgfINTxnvoucherObjentity(List<ShgFinancialTxnVouchersDataModel> mod) {
        List<ShgFinancialTxnVouchersEntity> listData=new ArrayList<>();
        ModelMapper mapper = new ModelMapper();
        for (int i=0;i<mod.size();i++){
            listData.add(mapper.map(mod.get(i), ShgFinancialTxnVouchersEntity.class));
        }

        return listData;
    }
}
