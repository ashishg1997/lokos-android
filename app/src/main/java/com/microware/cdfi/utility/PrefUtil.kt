package com.microware.cdfi.utility

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import java.lang.ref.WeakReference
import java.lang.reflect.Type
import org.json.JSONObject
import java.lang.Exception


class PrefUtil(context: WeakReference<Context>?) {

    var instance: PrefUtil? = null
    var context: WeakReference<Context>? = null
    var editor: SharedPreferences.Editor? = null
    var sharedPref: SharedPreferences? = null

    init {
        this.context = context
        sharedPref = context!!.get()!!.getSharedPreferences("FORM_SAVE_APP_DATA", Context.MODE_PRIVATE)
    }


    @SuppressLint("CommitPrefEdits")
    fun downloadData(isSaved: Boolean){
        editor = sharedPref!!.edit()
        editor!!.putBoolean("OFFLINE_DATA", isSaved)
        editor!!.apply()
    }

    fun isDataDownloaded(): Boolean{
        return sharedPref!!.getBoolean("OFFLINE_DATA", false)
    }

    @SuppressLint("CommitPrefEdits")
    fun saveLang(lang: String){
        editor = sharedPref!!.edit()
        editor!!.putString("APP_LANG", lang)
        editor!!.apply()
    }

    fun getLang(): String{
        return sharedPref!!.getString("APP_LANG", "en")!!
    }

    @SuppressLint("CommitPrefEdits")
    fun saveSecondaryLang(lang: String, loginId: String){
        editor = sharedPref!!.edit()
        editor!!.putString("SECONDARY_LANG" + loginId, lang)
        editor!!.apply()
    }

    fun getSecondaryLang(loginId: String): String{
        return sharedPref!!.getString("SECONDARY_LANG" + loginId, "null")!!
    }

    @SuppressLint("CommitPrefEdits")
    fun saveSecondaryLangCode(lang: String, loginId: String){
        editor = sharedPref!!.edit()
        editor!!.putString("SECONDARY_LANG_CODE" + loginId, lang)
        editor!!.apply()
    }

    fun getSecondaryLangCode(loginId: String): String{
        return sharedPref!!.getString("SECONDARY_LANG_CODE" + loginId, "null")!!
    }

    @SuppressLint("CommitPrefEdits")
    fun saveLangFromId(loginId: String,lang: String){
        editor = sharedPref!!.edit()
        editor!!.putString("APP_LANG" + loginId, lang)
        editor!!.apply()
    }

    fun getLangFromId(loginId: String?): String?{
        return if (loginId == null){
            null
        }else{
            sharedPref!!.getString("APP_LANG" + loginId, "")!!
        }
    }

    @SuppressLint("CommitPrefEdits")
    fun savePostLoginSetup(isDone: Boolean, loginId: String){
        editor = sharedPref!!.edit()
        editor!!.putBoolean("POST_LOGIN_SETUP_" + loginId, isDone)
        editor!!.apply()
    }

    fun isPostLoginSetupComplete(loginId: String): Boolean{
        return sharedPref!!.getBoolean("POST_LOGIN_SETUP_" + loginId, false)
    }

    @SuppressLint("CommitPrefEdits")
    fun removePostLogin(loginId: String){
        editor = sharedPref!!.edit()
        editor!!.remove("POST_LOGIN_SETUP_" + loginId)
        editor!!.apply()
    }


    @SuppressLint("CommitPrefEdits")
    fun saveOfflineDataImport(isSaved: Boolean, loginId: String){
        editor = sharedPref!!.edit()
        editor!!.putBoolean("OFFLINE_DATA_IMPORT_" + loginId, isSaved)
        editor!!.apply()
    }

    fun isOfflineDataImported(loginId: String): Boolean{
        return sharedPref!!.getBoolean("OFFLINE_DATA_IMPORT_" + loginId, false)
    }

    @SuppressLint("CommitPrefEdits")
    fun saveUserShgsDataImport(isSaved: Boolean, loginId: String){
        editor = sharedPref!!.edit()
        editor!!.putBoolean("USER_SHG_DATA_IMPORT_" + loginId, isSaved)
        editor!!.apply()
    }

    fun isUserShgsDataImported(loginId: String): Boolean{
        return sharedPref!!.getBoolean("USER_SHG_DATA_IMPORT_" + loginId, false)
    }

    @SuppressLint("CommitPrefEdits")
    fun removeOfflineData(loginId: String){
        editor = sharedPref!!.edit()
        editor!!.remove("OFFLINE_DATA_IMPORT_" + loginId)
        editor!!.apply()
    }

    @SuppressLint("CommitPrefEdits")
    fun saveUserData(isSaved: Boolean){
        editor = sharedPref!!.edit()
        editor!!.putBoolean("USER_DATA", isSaved)
        editor!!.apply()
    }

    fun isUserDataSaved(): Boolean{
        return sharedPref!!.getBoolean("USER_DATA", false)
    }


    @SuppressLint("CommitPrefEdits")
    fun saveLangCodes(list: ArrayList<String>, loginId: String) {
//        val prefs = PreferenceManager.getDefaultSharedPreferences(activity)
        editor = sharedPref!!.edit()
        val gson = Gson()
        val json = gson.toJson(list)
        editor!!.putString("LANG_CODES" + loginId, json)
        editor!!.apply()
    }

    fun getLangCodes(loginId: String): ArrayList<String> {
        val gson = Gson()
        val json = sharedPref!!.getString("LANG_CODES" + loginId, null)
        val type = object : TypeToken<ArrayList<String>>() {}.type
        return gson.fromJson(json, type)
    }

    fun <T> put(`object`: T, key: String) {
        //Convert object to JSON String.
        val jsonString = GsonBuilder().create().toJson(`object`)
        //Save that String in SharedPreferences
        sharedPref!!.edit().putString(key, jsonString).apply()
    }

    inline fun <reified T> get(key: String): T? {
        //We read JSON String which was saved.
        val value = sharedPref!!.getString(key, null)
        //JSON String was found which means object can be read.
        //We convert this JSON String to model object. Parameter "c" (of
        //type Class < T >" is used to cast.
        return GsonBuilder().create().fromJson(value, T::class.java)
    }


    @SuppressLint("CommitPrefEdits")
    fun saveMap(inputMap: HashMap<String?, Int?>) {
        if (sharedPref != null) {
            val jsonObject = JSONObject(inputMap as Map<*, *>)
            val jsonString = jsonObject.toString()
            editor = sharedPref!!.edit()
            editor!!.remove("SHG_SURVEY_MAP").commit()
            editor!!.putString("SHG_SURVEY_MAP", jsonString)
            editor!!.commit()
        }
    }

    fun loadMap(): Map<String, Int> {
        val outputMap: MutableMap<String, Int> = HashMap()
            try {
            if (sharedPref != null) {
                val jsonString = sharedPref!!.getString("SHG_SURVEY_MAP", JSONObject().toString())
                val jsonObject = JSONObject(jsonString!!)
                val keysItr = jsonObject.keys()
                while (keysItr.hasNext()) {
                    val k = keysItr.next()
                    val v = jsonObject[k] as Int
                    outputMap[k] = v
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return outputMap
    }

    @SuppressLint("CommitPrefEdits")
    fun saveListOfID(id: String) {

        val gsonC = Gson()
        val type: Type = object : TypeToken<List<String?>?>() {}.type

        val listString = sharedPref!!.getString("ID_LIST", "")!!
        var list = ArrayList<String>()
        if(listString.isNotEmpty()){
            list = gsonC.fromJson(
                listString
                , type
            )
        }

        list.add(id)
       /* list = gsonC.fromJson(
            sharedPref!!.getString("ID_LIST", "")!!, type
        )*/


        val stringConvertedList = gsonC.toJson(list)

        editor = sharedPref!!.edit()
        editor!!.putString("ID_LIST", stringConvertedList)
        editor!!.apply()
    }

    fun getListOfID(): List<String> {
        val gsonC = Gson()
        val type: Type = object : TypeToken<List<String?>?>() {}.type

        val listString = sharedPref!!.getString("ID_LIST", "")!!
        var list = ArrayList<String>()
        if(listString.isNotEmpty()){
            list = gsonC.fromJson(
                listString
                , type
            )
        }

        return list
    }

    @SuppressLint("CommitPrefEdits")
    fun removeSavedID(){
        editor = sharedPref!!.edit()
        editor!!.clear()
        editor!!.apply()
    }

    @SuppressLint("CommitPrefEdits")
    fun clearPref(){
        editor = sharedPref!!.edit()
        editor!!.clear()
        editor!!.apply()
    }

}