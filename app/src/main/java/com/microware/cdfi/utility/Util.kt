package com.microware.cdfi.utility

import android.content.Context
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.microware.cdfi.R
import com.microware.cdfi.activity.meeting.CutOffClosedMemberLoanList
import com.microware.cdfi.activity.vomeeting.SHGtoVOReceipts
import com.microware.cdfi.activity.vomeeting.VOCutOffMeetingTTSActivity
import com.microware.cdfi.activity.vomeeting.VoRegularMeetingTTSActivity
import com.microware.cdfi.activity.meeting.CutOffMeetingDetailTTSMainActivity
import com.microware.cdfi.activity.vomeeting.CutOffVoMemberCloseLoan
import com.microware.cdfi.api.meetingmodel.LoanListModel
import com.microware.cdfi.api.meetingmodel.LoanRepaymentListModel
import com.microware.cdfi.api.meetingmodel.ShareCapitalModel
import com.microware.cdfi.api.vomodel.VoLoanListModel
import com.microware.cdfi.api.vomodel.VoShareCapitalModel
import com.microware.cdfi.entity.*
import com.microware.cdfi.entity.voentity.VoMtgDetEntity
import com.microware.cdfi.viewModel.DtLoanMemberViewmodel
import kotlinx.android.synthetic.main.activity_s_h_gto_v_o_receipts.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

fun AppCompatActivity.replaceFragmenty(
    fragment: Fragment,
    allowStateLoss: Boolean = false,
    @IdRes containerViewId: Int
) {
    val ft = supportFragmentManager.beginTransaction().replace(containerViewId, fragment)
    if (!supportFragmentManager.isStateSaved) {
        ft.commit()
    } else if (allowStateLoss) {
        ft.commitAllowingStateLoss()
    }

}

fun Fragment.replaceFragments(
    fragment: Fragment,
    allowStateLoss: Boolean = false,
    @IdRes containerViewId: Int
) {
    val ft = activity!!.supportFragmentManager
        .beginTransaction()
        .replace(containerViewId, fragment)
    if (!activity!!.supportFragmentManager.isStateSaved) {
        ft.commit()
    } else if (allowStateLoss) {
        ft.commitAllowingStateLoss()
    }
}

//fun getDownloadLanguageCode(code: String): Int {
//    var Languagearray = GetLanguageData()
//    for (i in 0 until Languagearray.size) {
//        if (code.equals(Languagearray.get(i).appLnaguageCode)) {
//            return Languagearray.get(i).firebaseCode
//        }
//    }
//    return 0
//}
//

fun getDownloadLanguageISO(code: String): String {
    var Languagearray = GetLanguageData()
    for (i in 0 until Languagearray.size) {
        if (code.equals(Languagearray.get(i).appLnaguageCode)) {
            return Languagearray.get(i).translatorLnaguageCode
        }
    }
    return ""
}

fun GetLanguageData(): ArrayList<TranslatorLanguageCodeEntity> {
    var list = ArrayList<TranslatorLanguageCodeEntity>()
    list.add(TranslatorLanguageCodeEntity("as", "asm", 0))
    list.add(TranslatorLanguageCodeEntity("bn", "ben", 4))
    list.add(TranslatorLanguageCodeEntity("gu", "guj", 20))
    list.add(TranslatorLanguageCodeEntity("hi", "hin", 22))
    list.add(TranslatorLanguageCodeEntity("kn", "kan", 31))
    list.add(TranslatorLanguageCodeEntity("ml", "mal", 0))
    list.add(TranslatorLanguageCodeEntity("mr", "mar", 36))
    list.add(TranslatorLanguageCodeEntity("or", "ori", 0))
    list.add(TranslatorLanguageCodeEntity("pa", "pan", 0))
    list.add(TranslatorLanguageCodeEntity("ta", "tam", 50))
    list.add(TranslatorLanguageCodeEntity("te", "tel", 51))
    list.add(TranslatorLanguageCodeEntity("ur", "urd", 56))
    list.add(TranslatorLanguageCodeEntity("en", "eng", 56))
    return list
}

object MeetingTTS {

    fun GetSHGRegularParagraph(
        meetNo: Int,
        lang: String,
        context: Context,
        list: List<DtmtgDetEntity>,
        loan_distribute_list: List<LoanListModel>,
        loan_repayment_list: List<LoanRepaymentListModel>,
        SHG_name: String,
        MeetingDate: String,
        totalMember: String,
        presentMember: String,
        savingAmount: String,
        cashBalance: String,
        bankBalance: String,
        cashReceipt: String,
        cashPayment: String,
        bankReceipt: String,
        bankPayment: String,
        grp_investdata: String,
        grp_borrowdata: String,
        grp_receiptdata: String,
        grp_repaymentdata: String,
        grp_expendituredata: String

    ): String {
        var sentence1 = ""
        var sentence2 = ""
        var sentence3 = ""
        var sentence4 = ""
        var sentence5 = ""
        var sentence6 = ""
        var sentence7 = ""
        var sentence8 = ""
        var sentence9 = ""
        var sentence10 = ""
        var sentence11 = ""
        var sentence12 = ""
        var sentence13 = ""
        var sentence14 = ""
        var sentence15 = ""
        var sentence16 = ""
        var sentence17 = ""
        var sentence18 = ""
        var sentence19 = ""
        var sentence20 = ""
        var sentence21 = ""
        var sentence22 = ""
        var sentence23 = ""
        var grp_invest = ""
        var grp_borrow = ""
        var grp_receipt = ""
        var grp_repayment = ""
        var grp_expenditure = ""
        var grp_transaction = ""
        var finalParagraph = ""

        var absentList = ""
        var NeverSaveList = ""
        var LoanDistributeStringList = ""
        var LoanRepaymentStringList = ""
        var Absentcount = 1
        var NeverSavecount = 1
        var LoanDistributecount = 1
        var LoanRepaymentcount = 1

//    <string name="sentence1">Meeting Summary.</string>
//    <string name="sentence2">SHG_NAME was conducted a meeting on SHG_DATE on SHG_DAY.</string>
//    <string name="sentence3">Where SHG_PRESENT out of SHG_TOTAL members were present.</string>
//    <string name="sentence4">Member were absent for this meeting, those are.</string>
//    <string name="sentence5">were absent.</string>
//    <string name="sentence6">Total savings was SHG_AMOUNT rupees.</string>
//    <string name="sentence7">Members who were not saved the amount for this meeting, those are.</string>
//    <string name="sentence8">SHG_MEM did not save the amount for this meeting.</string>
//    <string name="sentence9">New Loan.</string>
//    <string name="sentence10">SHG_LOAN_COUNT new loans disbursed to members, those are.</string>
//    <string name="sentence11">loan distributed to SHG_MEM of SHG_LOAN_AMOUNT rupees.</string>
//    <string name="sentence12">Loan Repayments.</string>
//    <string name="sentence13">SHG_LOAN_AMOUNT rupee loan repayment received from SHG_MEM.</string>
//    <string name="sentence14">Total Collection.</string>
//    <string name="sentence15">Cash Receipt : SHG_AMOUNT rupees.</string>
//    <string name="sentence16">Cash Payments : SHG_AMOUNT rupees.</string>
//    <string name="sentence17">Bank Receipt : SHG_AMOUNT rupees</string>
//    <string name="sentence18">Bank Payment : SHG_AMOUNT rupees.</string>
//    <string name="sentence19">Bank Balance : SHG_AMOUNT rupees.</string>
//    <string name="sentence20">Cash Balance : SHG_AMOUNT rupees.</string>

        var days = arrayOf("रविवार", "सोमवार", "मंगलवार", "बुधवार", "गुरुवार", "शुक्रवार", "शनिवार")

        if (lang.equals("hi")) {
            days = arrayOf("रविवार", "सोमवार", "मंगलवार", "बुधवार", "गुरुवार", "शुक्रवार", "शनिवार")

            sentence1 = context.resources.getString(R.string.sentence1_hin) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_hin) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence3_hin) + "\n\n"
            sentence4 = context.resources.getString(R.string.sentence4_hin) + "\n\n"
            sentence5 = context.resources.getString(R.string.sentence5_hin) + "\n\n"
            sentence6 = context.resources.getString(R.string.sentence6_hin) + "\n\n"
            sentence7 = context.resources.getString(R.string.sentence7_hin) + "\n\n"
            sentence8 = context.resources.getString(R.string.sentence8_hin) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence9_hin) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence10_hin) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence11_hin) + "\n\n"
            sentence12 = context.resources.getString(R.string.sentence12_hin) + "\n\n"
            sentence13 = context.resources.getString(R.string.sentence13_hin) + "\n\n"
            sentence14 = context.resources.getString(R.string.sentence14_hin) + "\n\n"
            sentence15 = context.resources.getString(R.string.sentence15_hin) + "\n\n"
            sentence16 = context.resources.getString(R.string.sentence16_hin) + "\n\n"
            sentence17 = context.resources.getString(R.string.sentence17_hin) + "\n\n"
            sentence18 = context.resources.getString(R.string.sentence18_hin) + "\n\n"
            sentence19 = context.resources.getString(R.string.sentence19_hin) + "\n\n"
            sentence20 = context.resources.getString(R.string.sentence20_hin) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_hin) + "\n\n"
            sentence22 = context.resources.getString(R.string.sentence22_hin) + "\n\n"
            sentence23 = context.resources.getString(R.string.sentence23_hin) + "\n\n"
            grp_invest = context.resources.getString(R.string.group_investment_hin) + "\n\n"
            grp_borrow = context.resources.getString(R.string.group_borrow_hin) + "\n\n"
            grp_receipt = context.resources.getString(R.string.group_receipt_income_hin) + "\n\n"
            grp_repayment = context.resources.getString(R.string.group_repayment_hin) + "\n\n"
            grp_expenditure = context.resources.getString(R.string.group_expanditure_hin) + "\n\n"
            grp_transaction = context.resources.getString(R.string.group_transcation_hin) + "\n\n"

            for (i in 0 until list.size) {
                if (list.get(i).attendance!!.equals("2")) {
                    absentList =
                        absentList + Absentcount.toString() + ". " + list.get(i).member_name!!.toLowerCase() + " " + context.resources.getString(
                            R.string.sentence5_hin
                        ) + "\n"
                    Absentcount++
                }

                if (list.get(i).sav_comp!!.toString().equals("0")) {
                    var a =
                        list.get(i).member_name!!.toLowerCase() + " " + context.resources.getString(
                            R.string.sentence8_hin
                        )
                    a = a.replace("SHG_MEM", "")
                    NeverSaveList =
                        NeverSaveList + NeverSavecount.toString() + ". " + a + "\n"

                    NeverSavecount++
                }


            }

            for (i in 0 until loan_distribute_list.size) {
                if (loan_distribute_list.get(i).amt_demand != null) {
                    if (loan_distribute_list.get(i).amt_demand!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.sentence11_hin)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_distribute_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_LOAN_AMOUNT",
                            "'" + loan_distribute_list.get(i).amt_disbursed!!.toString() + "'"
                        )
                        LoanDistributeStringList =
                            LoanDistributeStringList + LoanDistributecount.toString() + ". " + a + "\n"

                        LoanDistributecount++
                    }
                }
            }

            for (i in 0 until loan_repayment_list.size) {
                if (loan_repayment_list.get(i).loan_paid != null) {
                    if (loan_repayment_list.get(i).loan_paid!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.sentence13_hin)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_repayment_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_LOAN_AMOUNT",
                            "'" + loan_repayment_list.get(i).loan_paid!!.toString() + "'"
                        )
                        LoanRepaymentStringList =
                            LoanRepaymentStringList + LoanRepaymentcount.toString() + ". " + a + "\n"
                        LoanRepaymentcount++
                    }
                }
            }
        } else if (lang.equals("bn")) {
            days =
                arrayOf(
                    "রবিবার",
                    "সোমবার",
                    "মঙ্গলবার",
                    "বুধবার",
                    "বৃহস্পতিবার",
                    "শুক্রবার",
                    "শনিবার"
                )

            sentence1 = context.resources.getString(R.string.sentence1_bangla) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_bangla) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence3_bangla) + "\n\n"
            sentence4 = context.resources.getString(R.string.sentence4_bangla) + "\n\n"
            sentence5 = context.resources.getString(R.string.sentence5_bangla) + "\n\n"
            sentence6 = context.resources.getString(R.string.sentence6_bangla) + "\n\n"
            sentence7 = context.resources.getString(R.string.sentence7_bangla) + "\n\n"
            sentence8 = context.resources.getString(R.string.sentence8_bangla) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence9_bangla) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence10_bangla) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence11_bangla) + "\n\n"
            sentence12 = context.resources.getString(R.string.sentence12_bangla) + "\n\n"
            sentence13 = context.resources.getString(R.string.sentence13_bangla) + "\n\n"
            sentence14 = context.resources.getString(R.string.sentence14_bangla) + "\n\n"
            sentence15 = context.resources.getString(R.string.sentence15_bangla) + "\n\n"
            sentence16 = context.resources.getString(R.string.sentence16_bangla) + "\n\n"
            sentence17 = context.resources.getString(R.string.sentence17_bangla) + "\n\n"
            sentence18 = context.resources.getString(R.string.sentence18_bangla) + "\n\n"
            sentence19 = context.resources.getString(R.string.sentence19_bangla) + "\n\n"
            sentence20 = context.resources.getString(R.string.sentence20_bangla) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_bangla) + "\n\n"
            sentence22 = context.resources.getString(R.string.sentence22_bangla) + "\n\n"
            sentence23 = context.resources.getString(R.string.sentence23_bangla) + "\n\n"
            grp_invest = context.resources.getString(R.string.group_investment_bangla) + "\n\n"
            grp_borrow = context.resources.getString(R.string.group_borrow_bangla) + "\n\n"
            grp_receipt = context.resources.getString(R.string.group_receipt_income_bangla) + "\n\n"
            grp_repayment = context.resources.getString(R.string.group_repayment_bangla) + "\n\n"
            grp_expenditure =
                context.resources.getString(R.string.group_expanditure_bangla) + "\n\n"
            grp_transaction =
                context.resources.getString(R.string.group_transcation_bangla) + "\n\n"


            for (i in 0 until list.size) {
                if (list.get(i).attendance!!.equals("2")) {
                    absentList =
                        absentList + Absentcount.toString() + ". " + list.get(i).member_name!!.toLowerCase() + " " + context.resources.getString(
                            R.string.sentence5_bangla
                        ) + "\n"
                    Absentcount++
                }

                if (list.get(i).sav_comp!!.toString().equals("0")) {
                    var a =
                        list.get(i).member_name!!.toLowerCase() + " " + context.resources.getString(
                            R.string.sentence8_bangla
                        )
                    a = a.replace("SHG_MEM", "")
                    NeverSaveList =
                        NeverSaveList + NeverSavecount.toString() + ". " + a + "\n"

                    NeverSavecount++
                }
            }


            for (i in 0 until loan_distribute_list.size) {
                if (loan_distribute_list.get(i).amt_demand != null) {
                    if (loan_distribute_list.get(i).amt_demand!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.sentence11_bangla)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_distribute_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_LOAN_AMOUNT",
                            "'" + loan_distribute_list.get(i).amt_disbursed!!.toString() + "'"
                        )
                        LoanDistributeStringList =
                            LoanDistributeStringList + LoanDistributecount.toString() + ". " + a + "\n"

                        LoanDistributecount++
                    }
                }
            }

            for (i in 0 until loan_repayment_list.size) {
                if (loan_repayment_list.get(i).loan_paid != null) {
                    if (loan_repayment_list.get(i).loan_paid!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.sentence13_bangla)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_repayment_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_LOAN_AMOUNT",
                            "'" + "'" + loan_repayment_list.get(i).loan_paid!!.toString() + "'" + "'"
                        )
                        LoanRepaymentStringList =
                            LoanRepaymentStringList + LoanRepaymentcount.toString() + ". " + a + "\n"
                        LoanRepaymentcount++
                    }
                }
            }
        } else if (lang.equals("gu")) {
            days = arrayOf("રવિવાર", "સોમવાર", "મંગળવાર", "બુધવાર", "ગુરુવાર", "શુક્રવાર", "શનિવાર")

            sentence1 = context.resources.getString(R.string.sentence1_gujarati) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_gujarati) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence3_gujarati) + "\n\n"
            sentence4 = context.resources.getString(R.string.sentence4_gujarati) + "\n\n"
            sentence5 = context.resources.getString(R.string.sentence5_gujarati) + "\n\n"
            sentence6 = context.resources.getString(R.string.sentence6_gujarati) + "\n\n"
            sentence7 = context.resources.getString(R.string.sentence7_gujarati) + "\n\n"
            sentence8 = context.resources.getString(R.string.sentence8_gujarati) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence9_gujarati) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence10_gujarati) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence11_gujarati) + "\n\n"
            sentence12 = context.resources.getString(R.string.sentence12_gujarati) + "\n\n"
            sentence13 = context.resources.getString(R.string.sentence13_gujarati) + "\n\n"
            sentence14 = context.resources.getString(R.string.sentence14_gujarati) + "\n\n"
            sentence15 = context.resources.getString(R.string.sentence15_gujarati) + "\n\n"
            sentence16 = context.resources.getString(R.string.sentence16_gujarati) + "\n\n"
            sentence17 = context.resources.getString(R.string.sentence17_gujarati) + "\n\n"
            sentence18 = context.resources.getString(R.string.sentence18_gujarati) + "\n\n"
            sentence19 = context.resources.getString(R.string.sentence19_gujarati) + "\n\n"
            sentence20 = context.resources.getString(R.string.sentence20_gujarati) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_gujarati) + "\n\n"
            sentence22 = context.resources.getString(R.string.sentence22_gujarati) + "\n\n"
            sentence23 = context.resources.getString(R.string.sentence23_gujarati) + "\n\n"
            grp_invest = context.resources.getString(R.string.group_investment_gujarati) + "\n\n"
            grp_borrow = context.resources.getString(R.string.group_borrow_gujarati) + "\n\n"
            grp_receipt =
                context.resources.getString(R.string.group_receipt_income_gujarati) + "\n\n"
            grp_repayment = context.resources.getString(R.string.group_repayment_gujarati) + "\n\n"
            grp_expenditure =
                context.resources.getString(R.string.group_expanditure_gujarati) + "\n\n"
            grp_transaction =
                context.resources.getString(R.string.group_transcation_gujarati) + "\n\n"

            for (i in 0 until list.size) {
                if (list.get(i).attendance!!.equals("2")) {
                    absentList =
                        absentList + Absentcount.toString() + ". " + list.get(i).member_name!!.toLowerCase() + " " + context.resources.getString(
                            R.string.sentence5_gujarati
                        ) + "\n"
                    Absentcount++
                }

                if (list.get(i).sav_comp!!.toString().equals("0")) {
                    var a =
                        list.get(i).member_name!!.toLowerCase() + " " + context.resources.getString(
                            R.string.sentence8_gujarati
                        )
                    a = a.replace("SHG_MEM", "")
                    NeverSaveList =
                        NeverSaveList + NeverSavecount.toString() + ". " + a + "\n"
                    NeverSavecount++
                }
            }


            for (i in 0 until loan_distribute_list.size) {
                if (loan_distribute_list.get(i).amt_demand != null) {
                    if (loan_distribute_list.get(i).amt_demand!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.sentence11_gujarati)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_distribute_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_LOAN_AMOUNT",
                            "'" + loan_distribute_list.get(i).amt_disbursed!!.toString() + "'"
                        )
                        LoanDistributeStringList =
                            LoanDistributeStringList + LoanDistributecount.toString() + ". " + a + "\n"

                        LoanDistributecount++
                    }
                }
            }

            for (i in 0 until loan_repayment_list.size) {
                if (loan_repayment_list.get(i).loan_paid != null) {
                    if (loan_repayment_list.get(i).loan_paid!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.sentence13_gujarati)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_repayment_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_LOAN_AMOUNT",
                            "'" + loan_repayment_list.get(i).loan_paid!!.toString() + "'"
                        )
                        LoanRepaymentStringList =
                            LoanRepaymentStringList + LoanRepaymentcount.toString() + ". " + a + "\n"
                        LoanRepaymentcount++
                    }
                }
            }

        } else if (lang.equals("kn")) {
            days =
                arrayOf("ಭಾನುವಾರ", "ಸೋಮವಾರ", "ಮಂಗಳವಾರ", "ಬುಧವಾರ", "ಗುರುವಾರ", "ಶುಕ್ರವಾರ", "ಶನಿವಾರ")

            sentence1 = context.resources.getString(R.string.sentence1_Kannad) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_Kannad) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence3_Kannad) + "\n\n"
            sentence4 = context.resources.getString(R.string.sentence4_Kannad) + "\n\n"
            sentence5 = context.resources.getString(R.string.sentence5_Kannad) + "\n\n"
            sentence6 = context.resources.getString(R.string.sentence6_Kannad) + "\n\n"
            sentence7 = context.resources.getString(R.string.sentence7_Kannad) + "\n\n"
            sentence8 = context.resources.getString(R.string.sentence8_Kannad) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence9_Kannad) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence10_Kannad) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence11_Kannad) + "\n\n"
            sentence12 = context.resources.getString(R.string.sentence12_Kannad) + "\n\n"
            sentence13 = context.resources.getString(R.string.sentence13_Kannad) + "\n\n"
            sentence14 = context.resources.getString(R.string.sentence14_Kannad) + "\n\n"
            sentence15 = context.resources.getString(R.string.sentence15_Kannad) + "\n\n"
            sentence16 = context.resources.getString(R.string.sentence16_Kannad) + "\n\n"
            sentence17 = context.resources.getString(R.string.sentence17_Kannad) + "\n\n"
            sentence18 = context.resources.getString(R.string.sentence18_Kannad) + "\n\n"
            sentence19 = context.resources.getString(R.string.sentence19_Kannad) + "\n\n"
            sentence20 = context.resources.getString(R.string.sentence20_Kannad) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_Kannad) + "\n\n"
            sentence22 = context.resources.getString(R.string.sentence22_Kannad) + "\n\n"
            sentence23 = context.resources.getString(R.string.sentence23_Kannad) + "\n\n"
            grp_invest = context.resources.getString(R.string.group_investment_Kannad) + "\n\n"
            grp_borrow = context.resources.getString(R.string.group_borrow_Kannad) + "\n\n"
            grp_receipt = context.resources.getString(R.string.group_receipt_income_Kannad) + "\n\n"
            grp_repayment = context.resources.getString(R.string.group_repayment_Kannad) + "\n\n"
            grp_expenditure =
                context.resources.getString(R.string.group_expanditure_Kannad) + "\n\n"
            grp_transaction =
                context.resources.getString(R.string.group_transcation_Kannad) + "\n\n"

            for (i in 0 until list.size) {
                if (list.get(i).attendance!!.equals("2")) {
                    absentList =
                        absentList + Absentcount.toString() + ". " + list.get(i).member_name!!.toLowerCase() + " " + context.resources.getString(
                            R.string.sentence5_Kannad
                        ) + "\n"
                    Absentcount++
                }

                if (list.get(i).sav_comp!!.toString().equals("0")) {
                    var a =
                        list.get(i).member_name!!.toLowerCase() + " " + context.resources.getString(
                            R.string.sentence8_Kannad
                        )
                    a = a.replace("SHG_MEM", "")
                    NeverSaveList =
                        NeverSaveList + NeverSavecount.toString() + ". " + a + "\n"
                    NeverSavecount++
                }
            }


            for (i in 0 until loan_distribute_list.size) {
                if (loan_distribute_list.get(i).amt_demand != null) {
                    if (loan_distribute_list.get(i).amt_demand!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.sentence11_Kannad)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_distribute_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_LOAN_AMOUNT",
                            "'" + loan_distribute_list.get(i).amt_disbursed!!.toString() + "'"
                        )
                        LoanDistributeStringList =
                            LoanDistributeStringList + LoanDistributecount.toString() + ". " + a + "\n"

                        LoanDistributecount++
                    }
                }
            }

            for (i in 0 until loan_repayment_list.size) {
                if (loan_repayment_list.get(i).loan_paid != null) {
                    if (loan_repayment_list.get(i).loan_paid!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.sentence13_Kannad)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_repayment_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_LOAN_AMOUNT",
                            "'" + loan_repayment_list.get(i).loan_paid!!.toString() + "'"
                        )
                        LoanRepaymentStringList =
                            LoanRepaymentStringList + LoanRepaymentcount.toString() + ". " + a + "\n"
                        LoanRepaymentcount++
                    }
                }
            }


        } else if (lang.equals("ml")) {
            days = arrayOf("ഞായർ", "തിങ്കൾ", "ചൊവ്വ", "ബുധൻ", "വ്യാഴം", "വെള്ളി", "ശനി")

            sentence1 = context.resources.getString(R.string.sentence1_malayalam) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_malayalam) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence3_malayalam) + "\n\n"
            sentence4 = context.resources.getString(R.string.sentence4_malayalam) + "\n\n"
            sentence5 = context.resources.getString(R.string.sentence5_malayalam) + "\n\n"
            sentence6 = context.resources.getString(R.string.sentence6_malayalam) + "\n\n"
            sentence7 = context.resources.getString(R.string.sentence7_malayalam) + "\n\n"
            sentence8 = context.resources.getString(R.string.sentence8_malayalam) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence9_malayalam) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence10_malayalam) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence11_malayalam) + "\n\n"
            sentence12 = context.resources.getString(R.string.sentence12_malayalam) + "\n\n"
            sentence13 = context.resources.getString(R.string.sentence13_malayalam) + "\n\n"
            sentence14 = context.resources.getString(R.string.sentence14_malayalam) + "\n\n"
            sentence15 = context.resources.getString(R.string.sentence15_malayalam) + "\n\n"
            sentence16 = context.resources.getString(R.string.sentence16_malayalam) + "\n\n"
            sentence17 = context.resources.getString(R.string.sentence17_malayalam) + "\n\n"
            sentence18 = context.resources.getString(R.string.sentence18_malayalam) + "\n\n"
            sentence19 = context.resources.getString(R.string.sentence19_malayalam) + "\n\n"
            sentence20 = context.resources.getString(R.string.sentence20_malayalam) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_malayalam) + "\n\n"
            sentence22 = context.resources.getString(R.string.sentence22_malayalam) + "\n\n"
            sentence23 = context.resources.getString(R.string.sentence23_malayalam) + "\n\n"
            grp_invest = context.resources.getString(R.string.group_investment_malayalam) + "\n\n"
            grp_borrow = context.resources.getString(R.string.group_borrow_malayalam) + "\n\n"
            grp_receipt =
                context.resources.getString(R.string.group_receipt_income_malayalam) + "\n\n"
            grp_repayment = context.resources.getString(R.string.group_repayment_malayalam) + "\n\n"
            grp_expenditure =
                context.resources.getString(R.string.group_expanditure_malayalam) + "\n\n"
            grp_transaction =
                context.resources.getString(R.string.group_transcation_malayalam) + "\n\n"

            for (i in 0 until list.size) {
                if (list.get(i).attendance!!.equals("2")) {
                    absentList =
                        absentList + Absentcount.toString() + ". " + list.get(i).member_name!!.toLowerCase() + " " + context.resources.getString(
                            R.string.sentence5_malayalam
                        ) + "\n"
                    Absentcount++
                }

                if (list.get(i).sav_comp!!.toString().equals("0")) {
                    var a =
                        list.get(i).member_name!!.toLowerCase() + " " + context.resources.getString(
                            R.string.sentence8_malayalam
                        )
                    a = a.replace("SHG_MEM", "")
                    NeverSaveList =
                        NeverSaveList + NeverSavecount.toString() + ". " + a + "\n"
                    NeverSavecount++
                }
            }


            for (i in 0 until loan_distribute_list.size) {
                if (loan_distribute_list.get(i).amt_demand != null) {
                    if (loan_distribute_list.get(i).amt_demand!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.sentence11_malayalam)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_distribute_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_LOAN_AMOUNT",
                            "'" + loan_distribute_list.get(i).amt_disbursed!!.toString() + "'"
                        )
                        LoanDistributeStringList =
                            LoanDistributeStringList + LoanDistributecount.toString() + ". " + a + "\n"

                        LoanDistributecount++
                    }
                }
            }

            for (i in 0 until loan_repayment_list.size) {
                if (loan_repayment_list.get(i).loan_paid != null) {
                    if (loan_repayment_list.get(i).loan_paid!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.sentence13_malayalam)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_repayment_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_LOAN_AMOUNT",
                            "'" + loan_repayment_list.get(i).loan_paid!!.toString() + "'"
                        )
                        LoanRepaymentStringList =
                            LoanRepaymentStringList + LoanRepaymentcount.toString() + ". " + a + "\n"
                        LoanRepaymentcount++
                    }
                }
            }

        } else if (lang.equals("mr")) {
            days = arrayOf("रविवार", "सोमवार", "मंगळवार", "बुधवार", "गुरुवार", "शुक्रवार", "शनिवार")

            sentence1 = context.resources.getString(R.string.sentence1_marathi) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_marathi) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence3_marathi) + "\n\n"
            sentence4 = context.resources.getString(R.string.sentence4_marathi) + "\n\n"
            sentence5 = context.resources.getString(R.string.sentence5_marathi) + "\n\n"
            sentence6 = context.resources.getString(R.string.sentence6_marathi) + "\n\n"
            sentence7 = context.resources.getString(R.string.sentence7_marathi) + "\n\n"
            sentence8 = context.resources.getString(R.string.sentence8_marathi) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence9_marathi) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence10_marathi) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence11_marathi) + "\n\n"
            sentence12 = context.resources.getString(R.string.sentence12_marathi) + "\n\n"
            sentence13 = context.resources.getString(R.string.sentence13_marathi) + "\n\n"
            sentence14 = context.resources.getString(R.string.sentence14_marathi) + "\n\n"
            sentence15 = context.resources.getString(R.string.sentence15_marathi) + "\n\n"
            sentence16 = context.resources.getString(R.string.sentence16_marathi) + "\n\n"
            sentence17 = context.resources.getString(R.string.sentence17_marathi) + "\n\n"
            sentence18 = context.resources.getString(R.string.sentence18_marathi) + "\n\n"
            sentence19 = context.resources.getString(R.string.sentence19_marathi) + "\n\n"
            sentence20 = context.resources.getString(R.string.sentence20_marathi) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_marathi) + "\n\n"
            sentence22 = context.resources.getString(R.string.sentence22_marathi) + "\n\n"
            sentence23 = context.resources.getString(R.string.sentence23_marathi) + "\n\n"
            grp_invest = context.resources.getString(R.string.group_investment_marathi) + "\n\n"
            grp_borrow = context.resources.getString(R.string.group_borrow_marathi) + "\n\n"
            grp_receipt =
                context.resources.getString(R.string.group_receipt_income_marathi) + "\n\n"
            grp_repayment = context.resources.getString(R.string.group_repayment_marathi) + "\n\n"
            grp_expenditure =
                context.resources.getString(R.string.group_expanditure_marathi) + "\n\n"
            grp_transaction =
                context.resources.getString(R.string.group_transcation_marathi) + "\n\n"

            for (i in 0 until list.size) {
                if (list.get(i).attendance!!.equals("2")) {
                    absentList =
                        absentList + Absentcount.toString() + ". " + list.get(i).member_name!!.toLowerCase() + " " + context.resources.getString(
                            R.string.sentence5_marathi
                        ) + "\n"
                    Absentcount++
                }

                if (list.get(i).sav_comp!!.toString().equals("0")) {
                    var a =
                        list.get(i).member_name!!.toLowerCase() + " " + context.resources.getString(
                            R.string.sentence8_marathi
                        )
                    a = a.replace("SHG_MEM", "")
                    NeverSaveList =
                        NeverSaveList + NeverSavecount.toString() + ". " + a + "\n"
                    NeverSavecount++
                }
            }

            for (i in 0 until loan_distribute_list.size) {
                if (loan_distribute_list.get(i).amt_demand != null) {
                    if (loan_distribute_list.get(i).amt_demand!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.sentence11_marathi)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_distribute_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_LOAN_AMOUNT",
                            "'" + loan_distribute_list.get(i).amt_disbursed!!.toString() + "'"
                        )
                        LoanDistributeStringList =
                            LoanDistributeStringList + LoanDistributecount.toString() + ". " + a + "\n"

                        LoanDistributecount++
                    }
                }
            }

            for (i in 0 until loan_repayment_list.size) {
                if (loan_repayment_list.get(i).loan_paid != null) {
                    if (loan_repayment_list.get(i).loan_paid!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.sentence13_marathi)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_repayment_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_LOAN_AMOUNT",
                            "'" + loan_repayment_list.get(i).loan_paid!!.toString() + "'"
                        )
                        LoanRepaymentStringList =
                            LoanRepaymentStringList + LoanRepaymentcount.toString() + ". " + a + "\n"
                        LoanRepaymentcount++
                    }
                }
            }

        } else if (lang.equals("or")) {
            days =
                arrayOf("ରବିବାର", "ସୋମବାର", "ମଙ୍ଗଳବାର", "ବୁଧବାର", "ଗୁରୁବାର", "ଶୁକ୍ରବାର", "ଶନିବାର")

            sentence1 = context.resources.getString(R.string.sentence1_oriya) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_oriya) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence3_oriya) + "\n\n"
            sentence4 = context.resources.getString(R.string.sentence4_oriya) + "\n\n"
            sentence5 = context.resources.getString(R.string.sentence5_oriya) + "\n\n"
            sentence6 = context.resources.getString(R.string.sentence6_oriya) + "\n\n"
            sentence7 = context.resources.getString(R.string.sentence7_oriya) + "\n\n"
            sentence8 = context.resources.getString(R.string.sentence8_oriya) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence9_oriya) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence10_oriya) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence11_oriya) + "\n\n"
            sentence12 = context.resources.getString(R.string.sentence12_oriya) + "\n\n"
            sentence13 = context.resources.getString(R.string.sentence13_oriya) + "\n\n"
            sentence14 = context.resources.getString(R.string.sentence14_oriya) + "\n\n"
            sentence15 = context.resources.getString(R.string.sentence15_oriya) + "\n\n"
            sentence16 = context.resources.getString(R.string.sentence16_oriya) + "\n\n"
            sentence17 = context.resources.getString(R.string.sentence17_oriya) + "\n\n"
            sentence18 = context.resources.getString(R.string.sentence18_oriya) + "\n\n"
            sentence19 = context.resources.getString(R.string.sentence19_oriya) + "\n\n"
            sentence20 = context.resources.getString(R.string.sentence20_oriya) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_oriya) + "\n\n"
            sentence22 = context.resources.getString(R.string.sentence22_oriya) + "\n\n"
            sentence23 = context.resources.getString(R.string.sentence23_oriya) + "\n\n"
            grp_invest = context.resources.getString(R.string.group_investment_oriya) + "\n\n"
            grp_borrow = context.resources.getString(R.string.group_borrow_oriya) + "\n\n"
            grp_receipt = context.resources.getString(R.string.group_receipt_income_oriya) + "\n\n"
            grp_repayment = context.resources.getString(R.string.group_repayment_oriya) + "\n\n"
            grp_expenditure = context.resources.getString(R.string.group_expanditure_oriya) + "\n\n"
            grp_transaction = context.resources.getString(R.string.group_transcation_oriya) + "\n\n"

            for (i in 0 until list.size) {
                if (list.get(i).attendance!!.equals("2")) {
                    absentList =
                        absentList + Absentcount.toString() + ". " + list.get(i).member_name!!.toLowerCase() + " " + context.resources.getString(
                            R.string.sentence5_oriya
                        ) + "\n"
                    Absentcount++
                }

                if (list.get(i).sav_comp!!.toString().equals("0")) {
                    var a =
                        list.get(i).member_name!!.toLowerCase() + " " + context.resources.getString(
                            R.string.sentence8_oriya
                        )
                    a = a.replace("SHG_MEM", "")
                    NeverSaveList =
                        NeverSaveList + NeverSavecount.toString() + ". " + a + "\n"
                    NeverSavecount++
                }
            }

            for (i in 0 until loan_distribute_list.size) {
                if (loan_distribute_list.get(i).amt_demand != null) {
                    if (loan_distribute_list.get(i).amt_demand!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.sentence11_oriya)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_distribute_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_LOAN_AMOUNT",
                            "'" + loan_distribute_list.get(i).amt_disbursed!!.toString() + "'"
                        )
                        LoanDistributeStringList =
                            LoanDistributeStringList + LoanDistributecount.toString() + ". " + a + "\n"

                        LoanDistributecount++
                    }
                }
            }

            for (i in 0 until loan_repayment_list.size) {
                if (loan_repayment_list.get(i).loan_paid != null) {
                    if (loan_repayment_list.get(i).loan_paid!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.sentence13_oriya)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_repayment_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_LOAN_AMOUNT",
                            "'" + loan_repayment_list.get(i).loan_paid!!.toString() + "'"
                        )
                        LoanRepaymentStringList =
                            LoanRepaymentStringList + LoanRepaymentcount.toString() + ". " + a + "\n"
                        LoanRepaymentcount++
                    }
                }
            }

        } else if (lang.equals("pa")) {
            days =
                arrayOf("ਐਤਵਾਰ", "ਸੋਮਵਾਰ", "ਮੰਗਲਵਾਰ", "ਬੁੱਧਵਾਰ", "ਵੀਰਵਾਰ", "ਸ਼ੁੱਕਰਵਾਰ", "ਸ਼ਨੀਵਾਰ")

            sentence1 = context.resources.getString(R.string.sentence1_punjab) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_punjab) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence3_punjab) + "\n\n"
            sentence4 = context.resources.getString(R.string.sentence4_punjab) + "\n\n"
            sentence5 = context.resources.getString(R.string.sentence5_punjab) + "\n\n"
            sentence6 = context.resources.getString(R.string.sentence6_punjab) + "\n\n"
            sentence7 = context.resources.getString(R.string.sentence7_punjab) + "\n\n"
            sentence8 = context.resources.getString(R.string.sentence8_punjab) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence9_punjab) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence10_punjab) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence11_punjab) + "\n\n"
            sentence12 = context.resources.getString(R.string.sentence12_punjab) + "\n\n"
            sentence13 = context.resources.getString(R.string.sentence13_punjab) + "\n\n"
            sentence14 = context.resources.getString(R.string.sentence14_punjab) + "\n\n"
            sentence15 = context.resources.getString(R.string.sentence15_punjab) + "\n\n"
            sentence16 = context.resources.getString(R.string.sentence16_punjab) + "\n\n"
            sentence17 = context.resources.getString(R.string.sentence17_punjab) + "\n\n"
            sentence18 = context.resources.getString(R.string.sentence18_punjab) + "\n\n"
            sentence19 = context.resources.getString(R.string.sentence19_punjab) + "\n\n"
            sentence20 = context.resources.getString(R.string.sentence20_punjab) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_punjab) + "\n\n"
            sentence22 = context.resources.getString(R.string.sentence22_punjab) + "\n\n"
            sentence23 = context.resources.getString(R.string.sentence23_punjab) + "\n\n"
            grp_invest = context.resources.getString(R.string.group_investment_punjab) + "\n\n"
            grp_borrow = context.resources.getString(R.string.group_borrow_punjab) + "\n\n"
            grp_receipt = context.resources.getString(R.string.group_receipt_income_punjab) + "\n\n"
            grp_repayment = context.resources.getString(R.string.group_repayment_punjab) + "\n\n"
            grp_expenditure =
                context.resources.getString(R.string.group_expanditure_punjab) + "\n\n"
            grp_transaction =
                context.resources.getString(R.string.group_transcation_punjab) + "\n\n"

            for (i in 0 until list.size) {
                if (list.get(i).attendance!!.equals("2")) {
                    absentList =
                        absentList + Absentcount.toString() + ". " + list.get(i).member_name!!.toLowerCase() + " " + context.resources.getString(
                            R.string.sentence5_punjab
                        ) + "\n"
                    Absentcount++
                }

                if (list.get(i).sav_comp!!.toString().equals("0")) {
                    var a =
                        list.get(i).member_name!!.toLowerCase() + " " + context.resources.getString(
                            R.string.sentence8_punjab
                        )
                    a = a.replace("SHG_MEM", "")
                    NeverSaveList =
                        NeverSaveList + NeverSavecount.toString() + ". " + a + "\n"
                    NeverSavecount++
                }
            }


            for (i in 0 until loan_distribute_list.size) {
                if (loan_distribute_list.get(i).amt_demand != null) {
                    if (loan_distribute_list.get(i).amt_demand!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.sentence11_punjab)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_distribute_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_LOAN_AMOUNT",
                            "'" + loan_distribute_list.get(i).amt_disbursed!!.toString() + "'"
                        )
                        LoanDistributeStringList =
                            LoanDistributeStringList + LoanDistributecount.toString() + ". " + a + "\n"

                        LoanDistributecount++
                    }
                }
            }

            for (i in 0 until loan_repayment_list.size) {
                if (loan_repayment_list.get(i).loan_paid != null) {
                    if (loan_repayment_list.get(i).loan_paid!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.sentence13_punjab)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_repayment_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_LOAN_AMOUNT",
                            "'" + loan_repayment_list.get(i).loan_paid!!.toString() + "'"
                        )
                        LoanRepaymentStringList =
                            LoanRepaymentStringList + LoanRepaymentcount.toString() + ". " + a + "\n"
                        LoanRepaymentcount++
                    }
                }
            }


        } else if (lang.equals("ta")) {
            days =
                arrayOf("ஞாயிறு", "திங்கள்", "செவ்வாய்", "புதன்", "வியாழன்", "வெள்ளி", "சனிக்கிழமை")

            sentence1 = context.resources.getString(R.string.sentence1_tamil) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_tamil) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence3_tamil) + "\n\n"
            sentence4 = context.resources.getString(R.string.sentence4_tamil) + "\n\n"
            sentence5 = context.resources.getString(R.string.sentence5_tamil) + "\n\n"
            sentence6 = context.resources.getString(R.string.sentence6_tamil) + "\n\n"
            sentence7 = context.resources.getString(R.string.sentence7_tamil) + "\n\n"
            sentence8 = context.resources.getString(R.string.sentence8_tamil) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence9_tamil) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence10_tamil) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence11_tamil) + "\n\n"
            sentence12 = context.resources.getString(R.string.sentence12_tamil) + "\n\n"
            sentence13 = context.resources.getString(R.string.sentence13_tamil) + "\n\n"
            sentence14 = context.resources.getString(R.string.sentence14_tamil) + "\n\n"
            sentence15 = context.resources.getString(R.string.sentence15_tamil) + "\n\n"
            sentence16 = context.resources.getString(R.string.sentence16_tamil) + "\n\n"
            sentence17 = context.resources.getString(R.string.sentence17_tamil) + "\n\n"
            sentence18 = context.resources.getString(R.string.sentence18_tamil) + "\n\n"
            sentence19 = context.resources.getString(R.string.sentence19_tamil) + "\n\n"
            sentence20 = context.resources.getString(R.string.sentence20_tamil) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_tamil) + "\n\n"
            sentence22 = context.resources.getString(R.string.sentence22_tamil) + "\n\n"
            sentence23 = context.resources.getString(R.string.sentence23_tamil) + "\n\n"
            grp_invest = context.resources.getString(R.string.group_investment_tamil) + "\n\n"
            grp_borrow = context.resources.getString(R.string.group_borrow_tamil) + "\n\n"
            grp_receipt = context.resources.getString(R.string.group_receipt_income_tamil) + "\n\n"
            grp_repayment = context.resources.getString(R.string.group_repayment_tamil) + "\n\n"
            grp_expenditure = context.resources.getString(R.string.group_expanditure_tamil) + "\n\n"
            grp_transaction = context.resources.getString(R.string.group_transcation_tamil) + "\n\n"

            for (i in 0 until list.size) {
                if (list.get(i).attendance!!.equals("2")) {
                    absentList =
                        absentList + Absentcount.toString() + ". " + list.get(i).member_name!!.toLowerCase() + " " + context.resources.getString(
                            R.string.sentence5_tamil
                        ) + "\n"
                    Absentcount++
                }

                if (list.get(i).sav_comp!!.toString().equals("0")) {
                    var a =
                        list.get(i).member_name!!.toLowerCase() + " " + context.resources.getString(
                            R.string.sentence8_tamil
                        )
                    a = a.replace("SHG_MEM", "")
                    NeverSaveList =
                        NeverSaveList + NeverSavecount.toString() + ". " + a + "\n"
                    NeverSavecount++
                }
            }


            for (i in 0 until loan_distribute_list.size) {
                if (loan_distribute_list.get(i).amt_demand != null) {
                    if (loan_distribute_list.get(i).amt_demand!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.sentence11_tamil)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_distribute_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_LOAN_AMOUNT",
                            "'" + loan_distribute_list.get(i).amt_disbursed!!.toString() + "'"
                        )
                        LoanDistributeStringList =
                            LoanDistributeStringList + LoanDistributecount.toString() + ". " + a + "\n"

                        LoanDistributecount++
                    }
                }
            }

            for (i in 0 until loan_repayment_list.size) {
                if (loan_repayment_list.get(i).loan_paid != null) {
                    if (loan_repayment_list.get(i).loan_paid!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.sentence13_tamil)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_repayment_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_LOAN_AMOUNT",
                            "'" + loan_repayment_list.get(i).loan_paid!!.toString() + "'"
                        )
                        LoanRepaymentStringList =
                            LoanRepaymentStringList + LoanRepaymentcount.toString() + ". " + a + "\n"
                        LoanRepaymentcount++
                    }
                }
            }


        } else if (lang.equals("te")) {
            days =
                arrayOf(
                    "ఆదివారం",
                    "సోమవారం",
                    "మంగళవారం",
                    "బుధవారం",
                    "గురువారం",
                    "శుక్రవారం",
                    "శనివారం"
                )

            sentence1 = context.resources.getString(R.string.sentence1_telgu) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_telgu) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence3_telgu) + "\n\n"
            sentence4 = context.resources.getString(R.string.sentence4_telgu) + "\n\n"
            sentence5 = context.resources.getString(R.string.sentence5_telgu) + "\n\n"
            sentence6 = context.resources.getString(R.string.sentence6_telgu) + "\n\n"
            sentence7 = context.resources.getString(R.string.sentence7_telgu) + "\n\n"
            sentence8 = context.resources.getString(R.string.sentence8_telgu) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence9_telgu) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence10_telgu) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence11_telgu) + "\n\n"
            sentence12 = context.resources.getString(R.string.sentence12_telgu) + "\n\n"
            sentence13 = context.resources.getString(R.string.sentence13_telgu) + "\n\n"
            sentence14 = context.resources.getString(R.string.sentence14_telgu) + "\n\n"
            sentence15 = context.resources.getString(R.string.sentence15_telgu) + "\n\n"
            sentence16 = context.resources.getString(R.string.sentence16_telgu) + "\n\n"
            sentence17 = context.resources.getString(R.string.sentence17_telgu) + "\n\n"
            sentence18 = context.resources.getString(R.string.sentence18_telgu) + "\n\n"
            sentence19 = context.resources.getString(R.string.sentence19_telgu) + "\n\n"
            sentence20 = context.resources.getString(R.string.sentence20_telgu) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_telgu) + "\n\n"
            sentence22 = context.resources.getString(R.string.sentence22_telgu) + "\n\n"
            sentence23 = context.resources.getString(R.string.sentence23_telgu) + "\n\n"
            grp_invest = context.resources.getString(R.string.group_investment_telgu) + "\n\n"
            grp_borrow = context.resources.getString(R.string.group_borrow_telgu) + "\n\n"
            grp_receipt = context.resources.getString(R.string.group_receipt_income_telgu) + "\n\n"
            grp_repayment = context.resources.getString(R.string.group_repayment_telgu) + "\n\n"
            grp_expenditure = context.resources.getString(R.string.group_expanditure_telgu) + "\n\n"
            grp_transaction = context.resources.getString(R.string.group_transcation_telgu) + "\n\n"

            for (i in 0 until list.size) {
                if (list.get(i).attendance!!.equals("2")) {
                    absentList =
                        absentList + Absentcount.toString() + ". " + list.get(i).member_name!!.toLowerCase() + " " + context.resources.getString(
                            R.string.sentence5_telgu
                        ) + "\n"
                    Absentcount++
                }

                if (list.get(i).sav_comp!!.toString().equals("0")) {
                    var a =
                        list.get(i).member_name!!.toLowerCase() + " " + context.resources.getString(
                            R.string.sentence8_telgu
                        )
                    a = a.replace("SHG_MEM", "")
                    NeverSaveList =
                        NeverSaveList + NeverSavecount.toString() + ". " + a + "\n"
                    NeverSavecount++
                }
            }


            for (i in 0 until loan_distribute_list.size) {
                if (loan_distribute_list.get(i).amt_demand != null) {
                    if (loan_distribute_list.get(i).amt_demand!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.sentence11_telgu)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_distribute_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_LOAN_AMOUNT",
                            "'" + loan_distribute_list.get(i).amt_disbursed!!.toString() + "'"
                        )
                        LoanDistributeStringList =
                            LoanDistributeStringList + LoanDistributecount.toString() + ". " + a + "\n"

                        LoanDistributecount++
                    }
                }
            }

            for (i in 0 until loan_repayment_list.size) {
                if (loan_repayment_list.get(i).loan_paid != null) {
                    if (loan_repayment_list.get(i).loan_paid!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.sentence13_telgu)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_repayment_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_LOAN_AMOUNT",
                            "'" + loan_repayment_list.get(i).loan_paid!!.toString() + "'"
                        )
                        LoanRepaymentStringList =
                            LoanRepaymentStringList + LoanRepaymentcount.toString() + ". " + a + "\n"
                        LoanRepaymentcount++
                    }
                }
            }


        } else if (lang.equals("ur")) {
            days = arrayOf("اتوار", "پیر", "منگل", "بدھ", "جمعرات", "جمعہ", "ہفتہ")

            sentence1 = context.resources.getString(R.string.sentence1_urdu) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_urdu) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence3_urdu) + "\n\n"
            sentence4 = context.resources.getString(R.string.sentence4_urdu) + "\n\n"
            sentence5 = context.resources.getString(R.string.sentence5_urdu) + "\n\n"
            sentence6 = context.resources.getString(R.string.sentence6_urdu) + "\n\n"
            sentence7 = context.resources.getString(R.string.sentence7_urdu) + "\n\n"
            sentence8 = context.resources.getString(R.string.sentence8_urdu) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence9_urdu) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence10_urdu) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence11_urdu) + "\n\n"
            sentence12 = context.resources.getString(R.string.sentence12_urdu) + "\n\n"
            sentence13 = context.resources.getString(R.string.sentence13_urdu) + "\n\n"
            sentence14 = context.resources.getString(R.string.sentence14_urdu) + "\n\n"
            sentence15 = context.resources.getString(R.string.sentence15_urdu) + "\n\n"
            sentence16 = context.resources.getString(R.string.sentence16_urdu) + "\n\n"
            sentence17 = context.resources.getString(R.string.sentence17_urdu) + "\n\n"
            sentence18 = context.resources.getString(R.string.sentence18_urdu) + "\n\n"
            sentence19 = context.resources.getString(R.string.sentence19_urdu) + "\n\n"
            sentence20 = context.resources.getString(R.string.sentence20_urdu) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_urdu) + "\n\n"
            sentence22 = context.resources.getString(R.string.sentence22_urdu) + "\n\n"
            sentence23 = context.resources.getString(R.string.sentence23_urdu) + "\n\n"
            grp_invest = context.resources.getString(R.string.group_investment_urdu) + "\n\n"
            grp_borrow = context.resources.getString(R.string.group_borrow_urdu) + "\n\n"
            grp_receipt = context.resources.getString(R.string.group_receipt_income_urdu) + "\n\n"
            grp_repayment = context.resources.getString(R.string.group_repayment_urdu) + "\n\n"
            grp_expenditure = context.resources.getString(R.string.group_expanditure_urdu) + "\n\n"
            grp_transaction = context.resources.getString(R.string.group_transcation_urdu) + "\n\n"

            for (i in 0 until list.size) {
                if (list.get(i).attendance!!.equals("2")) {
                    absentList =
                        absentList + Absentcount.toString() + ". " + list.get(i).member_name!!.toLowerCase() + " " + context.resources.getString(
                            R.string.sentence5_urdu
                        ) + "\n"
                    Absentcount++
                }

                if (list.get(i).sav_comp!!.toString().equals("0")) {
                    var a =
                        list.get(i).member_name!!.toLowerCase() + " " + context.resources.getString(
                            R.string.sentence8_urdu
                        )
                    a = a.replace("SHG_MEM", "")
                    NeverSaveList =
                        NeverSaveList + NeverSavecount.toString() + ". " + a + "\n"
                    NeverSavecount++
                }
            }

            for (i in 0 until loan_distribute_list.size) {
                if (loan_distribute_list.get(i).amt_demand != null) {
                    if (loan_distribute_list.get(i).amt_demand!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.sentence11_urdu)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_distribute_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_LOAN_AMOUNT",
                            "'" + loan_distribute_list.get(i).amt_disbursed!!.toString() + "'"
                        )
                        LoanDistributeStringList =
                            LoanDistributeStringList + LoanDistributecount.toString() + ". " + a + "\n"

                        LoanDistributecount++
                    }
                }
            }

            for (i in 0 until loan_repayment_list.size) {
                if (loan_repayment_list.get(i).loan_paid != null) {
                    if (loan_repayment_list.get(i).loan_paid!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.sentence13_urdu)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_repayment_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_LOAN_AMOUNT",
                            "'" + loan_repayment_list.get(i).loan_paid!!.toString() + "'"
                        )
                        LoanRepaymentStringList =
                            LoanRepaymentStringList + LoanRepaymentcount.toString() + ". " + a + "\n"
                        LoanRepaymentcount++
                    }
                }
            }

        } else if (lang.equals("en")) {
            days = arrayOf(
                "Sunday",
                "Monday",
                "Tuesday",
                "Wednesday",
                "Thursday",
                "Friday",
                "Saturday"
            )

            sentence1 = context.resources.getString(R.string.sentence1) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence3) + "\n\n"
            sentence4 = context.resources.getString(R.string.sentence4) + "\n\n"
            sentence5 = context.resources.getString(R.string.sentence5) + "\n\n"
            sentence6 = context.resources.getString(R.string.sentence6) + "\n\n"
            sentence7 = context.resources.getString(R.string.sentence7) + "\n\n"
            sentence8 = context.resources.getString(R.string.sentence8) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence9) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence10) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence11) + "\n\n"
            sentence12 = context.resources.getString(R.string.sentence12) + "\n\n"
            sentence13 = context.resources.getString(R.string.sentence13) + "\n\n"
            sentence14 = context.resources.getString(R.string.sentence14) + "\n\n"
            sentence15 = context.resources.getString(R.string.sentence15) + "\n\n"
            sentence16 = context.resources.getString(R.string.sentence16) + "\n\n"
            sentence17 = context.resources.getString(R.string.sentence17) + "\n\n"
            sentence18 = context.resources.getString(R.string.sentence18) + "\n\n"
            sentence19 = context.resources.getString(R.string.sentence19) + "\n\n"
            sentence20 = context.resources.getString(R.string.sentence20) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21) + "\n\n"
            sentence22 = context.resources.getString(R.string.sentence22) + "\n\n"
            sentence23 = context.resources.getString(R.string.sentence23) + "\n\n"
            grp_invest = context.resources.getString(R.string.group_investment1) + "\n\n"
            grp_borrow = context.resources.getString(R.string.group_borrow) + "\n\n"
            grp_receipt = context.resources.getString(R.string.group_receipt_income) + "\n\n"
            grp_repayment = context.resources.getString(R.string.group_repayment) + "\n\n"
            grp_expenditure = context.resources.getString(R.string.group_expanditure) + "\n\n"
            grp_transaction = context.resources.getString(R.string.group_transcation) + "\n\n"

            for (i in 0 until list.size) {
                if (list.get(i).attendance!!.equals("2")) {
                    absentList =
                        absentList + Absentcount.toString() + ". " + list.get(i).member_name!!.toLowerCase() + " " + context.resources.getString(
                            R.string.sentence5
                        ) + "\n"
                    Absentcount++
                }

                if (list.get(i).sav_comp!!.toString().equals("0")) {
                    var a =
                        list.get(i).member_name!!.toLowerCase() + " " + context.resources.getString(
                            R.string.sentence8
                        )
                    a = a.replace("SHG_MEM", "")
                    NeverSaveList =
                        NeverSaveList + NeverSavecount.toString() + ". " + a + "\n"
                    NeverSavecount++
                }
            }

            for (i in 0 until loan_distribute_list.size) {
                if (loan_distribute_list.get(i).amt_demand != null) {
                    if (loan_distribute_list.get(i).amt_demand!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.sentence11)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_distribute_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_LOAN_AMOUNT",
                            "'" + loan_distribute_list.get(i).amt_disbursed!!.toString() + "'"
                        )
                        LoanDistributeStringList =
                            LoanDistributeStringList + LoanDistributecount.toString() + ". " + a + "\n"

                        LoanDistributecount++
                    }
                }
            }

            for (i in 0 until loan_repayment_list.size) {
                if (loan_repayment_list.get(i).loan_paid != null) {
                    if (loan_repayment_list.get(i).loan_paid!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.sentence13)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_repayment_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_LOAN_AMOUNT",
                            "'" + loan_repayment_list.get(i).loan_paid!!.toString() + "'"
                        )
                        LoanRepaymentStringList =
                            LoanRepaymentStringList + LoanRepaymentcount.toString() + ". " + a + "\n"
                        LoanRepaymentcount++
                    }
                }
            }

        } else if (lang.equals("as")) {
            days = arrayOf(
                "দেওবাৰ",
                "সোমবাৰে",
                "মঙলবাৰে",
                "বুধবাৰে",
                "বৃহস্পতিবাৰে",
                "শুক্ৰবাৰে",
                "শনিবাৰে"
            )

            sentence1 = context.resources.getString(R.string.sentence1_ass) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_ass) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence3_ass) + "\n\n"
            sentence4 = context.resources.getString(R.string.sentence4_ass) + "\n\n"
            sentence5 = context.resources.getString(R.string.sentence5_ass) + "\n\n"
            sentence6 = context.resources.getString(R.string.sentence6_ass) + "\n\n"
            sentence7 = context.resources.getString(R.string.sentence7_ass) + "\n\n"
            sentence8 = context.resources.getString(R.string.sentence8_ass) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence9_ass) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence10_ass) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence11_ass) + "\n\n"
            sentence12 = context.resources.getString(R.string.sentence12_ass) + "\n\n"
            sentence13 = context.resources.getString(R.string.sentence13_ass) + "\n\n"
            sentence14 = context.resources.getString(R.string.sentence14_ass) + "\n\n"
            sentence15 = context.resources.getString(R.string.sentence15_ass) + "\n\n"
            sentence16 = context.resources.getString(R.string.sentence16_ass) + "\n\n"
            sentence17 = context.resources.getString(R.string.sentence17_ass) + "\n\n"
            sentence18 = context.resources.getString(R.string.sentence18_ass) + "\n\n"
            sentence19 = context.resources.getString(R.string.sentence19_ass) + "\n\n"
            sentence20 = context.resources.getString(R.string.sentence20_ass) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_ass) + "\n\n"
            sentence22 = context.resources.getString(R.string.sentence22_ass) + "\n\n"
            sentence23 = context.resources.getString(R.string.sentence23_ass) + "\n\n"
            grp_invest = context.resources.getString(R.string.group_investment_ass) + "\n\n"
            grp_borrow = context.resources.getString(R.string.group_borrow_ass) + "\n\n"
            grp_receipt = context.resources.getString(R.string.group_receipt_income_ass) + "\n\n"
            grp_repayment = context.resources.getString(R.string.group_repayment_ass) + "\n\n"
            grp_expenditure = context.resources.getString(R.string.group_expanditure_ass) + "\n\n"
            grp_transaction = context.resources.getString(R.string.group_transcation_ass) + "\n\n"

            for (i in 0 until list.size) {
                if (list.get(i).attendance!!.equals("2")) {
                    absentList =
                        absentList + Absentcount.toString() + ". " + list.get(i).member_name!!.toLowerCase() + " " + context.resources.getString(
                            R.string.sentence5_ass
                        ) + "\n"
                    Absentcount++
                }

                if (list.get(i).sav_comp!!.toString().equals("0")) {
                    var a =
                        list.get(i).member_name!!.toLowerCase() + " " + context.resources.getString(
                            R.string.sentence8_ass
                        )
                    a = a.replace("SHG_MEM", "")
                    NeverSaveList =
                        NeverSaveList + NeverSavecount.toString() + ". " + a + "\n"
                    NeverSavecount++
                }
            }

            for (i in 0 until loan_distribute_list.size) {
                if (loan_distribute_list.get(i).amt_demand != null) {
                    if (loan_distribute_list.get(i).amt_demand!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.sentence11_ass)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_distribute_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_LOAN_AMOUNT",
                            "'" + loan_distribute_list.get(i).amt_disbursed!!.toString() + "'"
                        )
                        LoanDistributeStringList =
                            LoanDistributeStringList + LoanDistributecount.toString() + ". " + a + "\n"

                        LoanDistributecount++
                    }
                }
            }

            for (i in 0 until loan_repayment_list.size) {
                if (loan_repayment_list.get(i).loan_paid != null) {
                    if (loan_repayment_list.get(i).loan_paid!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.sentence13_ass)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_repayment_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_LOAN_AMOUNT",
                            "'" + loan_repayment_list.get(i).loan_paid!!.toString() + "'"
                        )
                        LoanRepaymentStringList =
                            LoanRepaymentStringList + LoanRepaymentcount.toString() + ". " + a + "\n"
                        LoanRepaymentcount++
                    }
                }
            }

        }


        var date = Date()
        val date_format = SimpleDateFormat("dd-MM-yyyy")
        try {
            date = date_format.parse(MeetingDate)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        val c = Calendar.getInstance()
        c.time = date
        var dayIndex: Int = c.get(Calendar.DAY_OF_WEEK)


        var dayData = days[dayIndex - 1]

        sentence2 = sentence2.replace("SHG_Name", "'" + SHG_name + "'")
        sentence2 = sentence2.replace("SHG_DATE", MeetingDate)
        sentence2 = sentence2.replace("SHG_DAY", dayData)
        sentence2 = sentence2.replace("MEET_NO", meetNo.toString())

        sentence3 = sentence3.replace("SHG_TOTAL", totalMember + "'")
        sentence3 = sentence3.replace("SHG_PRESENT", presentMember + "'")

        sentence6 = sentence6.replace("SHG_AMOUNT", "'" + savingAmount.toString() + "'")

        sentence10 = sentence10.replace("SHG_LOAN_COUNT", (LoanDistributecount - 1).toString())

        sentence6 = sentence6.replace("SHG_AMOUNT", "" + savingAmount.toString() + "")

        sentence15 = sentence15.replace("SHG_AMOUNT", "= '" + cashReceipt.toString() + "= '")
        sentence16 = sentence16.replace("SHG_AMOUNT", "= '" + cashPayment.toString() + "= '")

        sentence17 = sentence17.replace("SHG_AMOUNT", "= '" + bankReceipt.toString() + "= '")
        sentence18 = sentence18.replace("SHG_AMOUNT", "= '" + bankPayment.toString() + "= '")

        sentence19 = sentence19.replace("SHG_AMOUNT", "= '" + bankBalance.toString() + "= '")
        sentence20 = sentence20.replace("SHG_AMOUNT", "= '" + cashBalance.toString() + "= '")

        grp_invest = grp_invest.replace("SHG_AMOUNT", "= '" + grp_investdata.toString() + "= '")
        grp_borrow = grp_borrow.replace("SHG_AMOUNT", "= '" + grp_borrowdata.toString() + "= '")
        grp_receipt = grp_receipt.replace("SHG_AMOUNT", "= '" + grp_receiptdata.toString() + "= '")
        grp_repayment =
            grp_repayment.replace("SHG_AMOUNT", "= '" + grp_repaymentdata.toString() + "= '")
        grp_expenditure =
            grp_expenditure.replace("SHG_AMOUNT", "= '" + grp_expendituredata.toString() + "= '")

//    sentence15 = sentence15.replace("SHG_AMOUNT", "" + cashReceipt.toString() + "")
//    sentence16 = sentence16.replace("SHG_AMOUNT", "" + cashPayment.toString() + "")
//
//    sentence17 = sentence17.replace("SHG_AMOUNT", " '" + bankReceipt.toString() + "")
//    sentence18 = sentence18.replace("SHG_AMOUNT", "" + bankPayment.toString() + "")
//
//    sentence19 = sentence19.replace("SHG_AMOUNT", "" + bankBalance.toString() + "")
//    sentence20 = sentence20.replace("SHG_AMOUNT", "" + cashBalance.toString() + "")

        if (absentList.isNullOrEmpty()) {
            sentence4 = ""
        } else {
            absentList = absentList + "\n"
        }
        if (NeverSaveList.isNullOrEmpty()) {
            sentence7 = ""
        } else {
            NeverSaveList = NeverSaveList + "\n"
        }
        if (LoanDistributeStringList.isNullOrEmpty()) {
            sentence9 = ""
            sentence10 = ""
        } else {
            LoanDistributeStringList = LoanDistributeStringList + "\n"
        }
        if (LoanRepaymentStringList.isNullOrEmpty()) {
            sentence12 = ""
        } else {
            LoanRepaymentStringList = LoanRepaymentStringList + "\n"
        }

        finalParagraph =
            sentence1 + "@#@" + sentence2 + "@#@" + sentence3 + "@#@" + sentence4 + "@#@" + absentList + "@#@" + sentence6 + "@#@" + sentence7 + "@#@" + NeverSaveList + "@#@" + sentence9 + "@#@" + sentence10 + "@#@" + LoanDistributeStringList + "@#@" + sentence12 + "@#@" + LoanRepaymentStringList + "@#@" + sentence14 + "@#@" + sentence15 + "@#@" + sentence16 + "@#@" + sentence17 + "@#@" + sentence18 + "@#@" + sentence19 + "@#@" + sentence20 + "@#@" + sentence21 + "@#@" + sentence22 + "@#@" + sentence23 + "@#@" + grp_transaction + "@#@" + grp_invest + "@#@" + grp_borrow + "@#@" + grp_receipt + "@#@" + grp_repayment + "@#@" + grp_expenditure

        return finalParagraph
    }


    fun GetSHGCutOffParagraph(
        meetNo: Int,
        lang: String,
        context: Context,
        list: List<DtmtgDetEntity>,
        loan_close_list: List<LoanListModel>,
        loan_active_list: List<LoanListModel>,
        shareCapital_list: List<ShareCapitalModel>,
        SHG_name: String,
        MeetingDate: String,
        savingAmount: String,
        closedLoan_amount: String,
        activeLoan_amount: String,
        share_capital_amount: String,
        investAmount: String,
        borrowAmount: String,
        cashBalance: String,
        bankBalance: String
    ): String {
        var sentence1 = ""
        var sentence2 = ""
        var sentence3 = ""
        var sentence4 = ""
        var sentence5 = ""
        var sentence6 = ""
        var sentence7 = ""
        var sentence8 = ""
        var sentence9 = ""
        var sentence10 = ""
        var sentence11 = ""
        var sentence19 = ""
        var sentence20 = ""
        var sentence21 = ""
        var finalParagraph = ""

        var AmountSaveListString = ""
        var loan_close_listString = ""
        var loan_active_listString = ""
        var shareCapital_listString = ""
        var AmountSavecount = 1
        var loan_closecount = 1
        var loan_activecount = 1
        var loan_shareCapitalcount = 1

        var days = arrayOf("रविवार", "सोमवार", "मंगलवार", "बुधवार", "गुरुवार", "शुक्रवार", "शनिवार")

        if (lang.equals("hi")) {
            days = arrayOf("रविवार", "सोमवार", "मंगलवार", "बुधवार", "गुरुवार", "शुक्रवार", "शनिवार")

            sentence1 = context.resources.getString(R.string.sentence1_hin) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_hin) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence6_hin) + "\n\n"
            sentence4 = context.resources.getString(R.string.shg_cutoff_sentence2_hin) + "\n\n"
            sentence5 = context.resources.getString(R.string.shg_cutoff_sentence4_hin) + "\n\n"
            sentence6 = context.resources.getString(R.string.shg_cutoff_sentence7_hin) + "\n\n"
            sentence7 = context.resources.getString(R.string.shg_cutoff_sentence9_hin) + "\n\n"
            sentence8 = context.resources.getString(R.string.shg_cutoff_sentence10_hin) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence14_hin) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence19_hin) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence20_hin) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_hin) + "\n\n"

            for (i in 0 until list.size) {

                if (!list.get(i).sav_comp!!.toString()
                        .equals("0") || !list.get(i).sav_vol!!.toString().equals("0")
                ) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence1_hin)
                    a = a.replace("SHG_MEM", list.get(i).member_name!!)
                    var amount = list.get(i).sav_comp!! + list.get(i).sav_vol!!
                    a = a.replace("SHG_AMOUNT", amount.toString())
                    AmountSaveListString =
                        AmountSaveListString + AmountSavecount.toString() + ". " + a + "\n\n"

                    AmountSavecount++
                }
            }

            for (i in 0 until loan_close_list.size) {
                var amount =
                    (context as CutOffMeetingDetailTTSMainActivity).getTotalClosedLoanAmount(
                        list.get(i).mem_id,
                        list.get(i).mtg_no
                    )
                if (amount != null) {
                    if (amount.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence3_hin)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_close_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace("SHG_AMOUNT", "'" + amount.toString() + "'")

                        loan_close_listString =
                            loan_close_listString + loan_closecount.toString() + ". " + a + "\n\n"

                        loan_closecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amt_disbursed != null) {
                    if (loan_active_list.get(i).amt_disbursed!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence5_hin)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_active_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + loan_active_list.get(i).amt_disbursed!!.toString() + "'"
                        )
                        loan_active_listString =
                            loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                        loan_activecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amt_disbursed == null || loan_active_list.get(i).amt_disbursed!!.toInt() == 0) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence6_hin)
                    a = a.replace(
                        "SHG_MEM",
                        "'" + loan_active_list.get(i).member_name!!.toLowerCase() + "'"
                    )

                    loan_active_listString =
                        loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                    loan_activecount++
                }
            }

            for (i in 0 until shareCapital_list.size) {
                if (shareCapital_list.get(i).amount != null) {
                    if (shareCapital_list.get(i).amount!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence8_hin)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + shareCapital_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + shareCapital_list.get(i).amount!!.toString() + "'"
                        )
                        shareCapital_listString =
                            shareCapital_listString + loan_shareCapitalcount.toString() + ". " + a + "\n\n"
                        loan_shareCapitalcount++
                    }
                }
            }
        } else if (lang.equals("en")) {
            days = arrayOf(
                "Sunday",
                "Monday",
                "Tuesday",
                "Wednesday",
                "Thursday",
                "Friday",
                "Saturday"
            )

            sentence1 = context.resources.getString(R.string.sentence1) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence6) + "\n\n"
            sentence4 = context.resources.getString(R.string.shg_cutoff_sentence2) + "\n\n"
            sentence5 = context.resources.getString(R.string.shg_cutoff_sentence4) + "\n\n"
            sentence6 = context.resources.getString(R.string.shg_cutoff_sentence7) + "\n\n"
            sentence7 = context.resources.getString(R.string.shg_cutoff_sentence9) + "\n\n"
            sentence8 = context.resources.getString(R.string.shg_cutoff_sentence10) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence14) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence19) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence20) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21) + "\n\n"

            for (i in 0 until list.size) {

                if (!list.get(i).sav_comp!!.toString()
                        .equals("0") || !list.get(i).sav_vol!!.toString().equals("0")
                ) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence1)
                    a = a.replace("SHG_MEM", list.get(i).member_name!!)
                    var amount = list.get(i).sav_comp!! + list.get(i).sav_vol!!
                    a = a.replace("SHG_AMOUNT", amount.toString())
                    AmountSaveListString =
                        AmountSaveListString + AmountSavecount.toString() + ". " + a + "\n\n"

                    AmountSavecount++
                }
            }

            for (i in 0 until loan_close_list.size) {
                var amount =
                    (context as CutOffMeetingDetailTTSMainActivity).getTotalClosedLoanAmount(
                        list.get(i).mem_id,
                        list.get(i).mtg_no
                    )

                if (amount != null) {
                    if (amount.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence3)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_close_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace("SHG_AMOUNT", "'" + amount.toString() + "'")

                        loan_close_listString =
                            loan_close_listString + loan_closecount.toString() + ". " + a + "\n\n"

                        loan_closecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amt_disbursed != null) {
                    if (loan_active_list.get(i).amt_disbursed!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence5)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_active_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + loan_active_list.get(i).amt_disbursed!!.toString() + "'"
                        )
                        loan_active_listString =
                            loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                        loan_activecount++
                    }

                    if (loan_active_list.get(i).amt_disbursed == null || loan_active_list.get(i).amt_disbursed!!.toInt() == 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence6)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_active_list.get(i).member_name!!.toLowerCase() + "'"
                        )

                        loan_active_listString =
                            loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                        loan_activecount++
                    }

                }


            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amt_disbursed == null || loan_active_list.get(i).amt_disbursed!!.toInt() == 0) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence6)
                    a = a.replace(
                        "SHG_MEM",
                        "'" + loan_active_list.get(i).member_name!!.toLowerCase() + "'"
                    )

                    loan_active_listString =
                        loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                    loan_activecount++
                }
            }

            for (i in 0 until shareCapital_list.size) {
                if (shareCapital_list.get(i).amount != null) {
                    if (shareCapital_list.get(i).amount!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence8)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + shareCapital_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + shareCapital_list.get(i).amount!!.toString() + "'"
                        )
                        shareCapital_listString =
                            shareCapital_listString + loan_shareCapitalcount.toString() + ". " + a + "\n\n"
                        loan_shareCapitalcount++
                    }
                }
            }
        } else if (lang.equals("as")) {
            days = arrayOf(
                "দেওবাৰ",
                "সোমবাৰে",
                "মঙলবাৰে",
                "বুধবাৰে",
                "বৃহস্পতিবাৰে",
                "শুক্ৰবাৰে",
                "শনিবাৰে"
            )
            sentence1 = context.resources.getString(R.string.sentence1_ass) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_ass) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence6_ass) + "\n\n"
            sentence4 = context.resources.getString(R.string.shg_cutoff_sentence2_ass) + "\n\n"
            sentence5 = context.resources.getString(R.string.shg_cutoff_sentence4_ass) + "\n\n"
            sentence6 = context.resources.getString(R.string.shg_cutoff_sentence7_ass) + "\n\n"
            sentence7 = context.resources.getString(R.string.shg_cutoff_sentence9_ass) + "\n\n"
            sentence8 = context.resources.getString(R.string.shg_cutoff_sentence10_ass) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence14_ass) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence19_ass) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence20_ass) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_ass) + "\n\n"

            for (i in 0 until list.size) {

                if (!list.get(i).sav_comp!!.toString()
                        .equals("0") || !list.get(i).sav_vol!!.toString().equals("0")
                ) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence1_ass)
                    a = a.replace("SHG_MEM", list.get(i).member_name!!)
                    var amount = list.get(i).sav_comp!! + list.get(i).sav_vol!!
                    a = a.replace("SHG_AMOUNT", amount.toString())
                    AmountSaveListString =
                        AmountSaveListString + AmountSavecount.toString() + ". " + a + "\n\n"

                    AmountSavecount++
                }
            }

            for (i in 0 until loan_close_list.size) {
                var amount =
                    (context as CutOffMeetingDetailTTSMainActivity).getTotalClosedLoanAmount(
                        list.get(i).mem_id,
                        list.get(i).mtg_no
                    )
                if (amount != null) {
                    if (amount.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence3_ass)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_close_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace("SHG_AMOUNT", "'" + amount.toString() + "'")

                        loan_close_listString =
                            loan_close_listString + loan_closecount.toString() + ". " + a + "\n\n"

                        loan_closecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amt_disbursed != null) {
                    if (loan_active_list.get(i).amt_disbursed!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence5_ass)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_active_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + loan_active_list.get(i).amt_disbursed!!.toString() + "'"
                        )
                        loan_active_listString =
                            loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                        loan_activecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amt_disbursed == null || loan_active_list.get(i).amt_disbursed!!.toInt() == 0) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence6_ass)
                    a = a.replace(
                        "SHG_MEM",
                        "'" + loan_active_list.get(i).member_name!!.toLowerCase() + "'"
                    )

                    loan_active_listString =
                        loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                    loan_activecount++
                }
            }

            for (i in 0 until shareCapital_list.size) {
                if (shareCapital_list.get(i).amount != null) {
                    if (shareCapital_list.get(i).amount!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence8_ass)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + shareCapital_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + shareCapital_list.get(i).amount!!.toString() + "'"
                        )
                        shareCapital_listString =
                            shareCapital_listString + loan_shareCapitalcount.toString() + ". " + a + "\n\n"
                        loan_shareCapitalcount++
                    }
                }
            }
        } else if (lang.equals("bn")) {
            days =
                arrayOf(
                    "রবিবার",
                    "সোমবার",
                    "মঙ্গলবার",
                    "বুধবার",
                    "বৃহস্পতিবার",
                    "শুক্রবার",
                    "শনিবার"
                )
            sentence1 = context.resources.getString(R.string.sentence1_bangla) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_bangla) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence6_bangla) + "\n\n"
            sentence4 = context.resources.getString(R.string.shg_cutoff_sentence2_bangla) + "\n\n"
            sentence5 = context.resources.getString(R.string.shg_cutoff_sentence4_bangla) + "\n\n"
            sentence6 = context.resources.getString(R.string.shg_cutoff_sentence7_bangla) + "\n\n"
            sentence7 = context.resources.getString(R.string.shg_cutoff_sentence9_bangla) + "\n\n"
            sentence8 = context.resources.getString(R.string.shg_cutoff_sentence10_bangla) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence14_bangla) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence19_bangla) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence20_bangla) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_bangla) + "\n\n"

            for (i in 0 until list.size) {

                if (!list.get(i).sav_comp!!.toString()
                        .equals("0") || !list.get(i).sav_vol!!.toString().equals("0")
                ) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence1_bangla)
                    a = a.replace("SHG_MEM", list.get(i).member_name!!)
                    var amount = list.get(i).sav_comp!! + list.get(i).sav_vol!!
                    a = a.replace("SHG_AMOUNT", amount.toString())
                    AmountSaveListString =
                        AmountSaveListString + AmountSavecount.toString() + ". " + a + "\n\n"

                    AmountSavecount++
                }
            }

            for (i in 0 until loan_close_list.size) {
                var amount =
                    (context as CutOffMeetingDetailTTSMainActivity).getTotalClosedLoanAmount(
                        list.get(i).mem_id,
                        list.get(i).mtg_no
                    )
                if (amount != null) {
                    if (amount.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence3_bangla)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_close_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace("SHG_AMOUNT", "'" + amount.toString() + "'")

                        loan_close_listString =
                            loan_close_listString + loan_closecount.toString() + ". " + a + "\n\n"

                        loan_closecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amt_disbursed != null) {
                    if (loan_active_list.get(i).amt_disbursed!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence5_bangla)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_active_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + loan_active_list.get(i).amt_disbursed!!.toString() + "'"
                        )
                        loan_active_listString =
                            loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                        loan_activecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amt_disbursed == null || loan_active_list.get(i).amt_disbursed!!.toInt() == 0) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence6_bangla)
                    a = a.replace(
                        "SHG_MEM",
                        "'" + loan_active_list.get(i).member_name!!.toLowerCase() + "'"
                    )

                    loan_active_listString =
                        loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                    loan_activecount++
                }
            }

            for (i in 0 until shareCapital_list.size) {
                if (shareCapital_list.get(i).amount != null) {
                    if (shareCapital_list.get(i).amount!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence8_bangla)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + shareCapital_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + shareCapital_list.get(i).amount!!.toString() + "'"
                        )
                        shareCapital_listString =
                            shareCapital_listString + loan_shareCapitalcount.toString() + ". " + a + "\n\n"
                        loan_shareCapitalcount++
                    }
                }
            }
        } else if (lang.equals("gu")) {
            days = arrayOf("રવિવાર", "સોમવાર", "મંગળવાર", "બુધવાર", "ગુરુવાર", "શુક્રવાર", "શનિવાર")

            sentence1 = context.resources.getString(R.string.sentence1_gujarati) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_gujarati) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence6_gujarati) + "\n\n"
            sentence4 = context.resources.getString(R.string.shg_cutoff_sentence2_gujarati) + "\n\n"
            sentence5 = context.resources.getString(R.string.shg_cutoff_sentence4_gujarati) + "\n\n"
            sentence6 = context.resources.getString(R.string.shg_cutoff_sentence7_gujarati) + "\n\n"
            sentence7 = context.resources.getString(R.string.shg_cutoff_sentence9_gujarati) + "\n\n"
            sentence8 =
                context.resources.getString(R.string.shg_cutoff_sentence10_gujarati) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence14_gujarati) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence19_gujarati) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence20_gujarati) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_gujarati) + "\n\n"

            for (i in 0 until list.size) {

                if (!list.get(i).sav_comp!!.toString()
                        .equals("0") || !list.get(i).sav_vol!!.toString().equals("0")
                ) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence1_gujarati)
                    a = a.replace("SHG_MEM", list.get(i).member_name!!)
                    var amount = list.get(i).sav_comp!! + list.get(i).sav_vol!!
                    a = a.replace("SHG_AMOUNT", amount.toString())
                    AmountSaveListString =
                        AmountSaveListString + AmountSavecount.toString() + ". " + a + "\n\n"

                    AmountSavecount++
                }
            }

            for (i in 0 until loan_close_list.size) {
                var amount =
                    (context as CutOffMeetingDetailTTSMainActivity).getTotalClosedLoanAmount(
                        list.get(i).mem_id,
                        list.get(i).mtg_no
                    )
                if (amount != null) {
                    if (amount.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence3_gujarati)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_close_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace("SHG_AMOUNT", "'" + amount.toString() + "'")

                        loan_close_listString =
                            loan_close_listString + loan_closecount.toString() + ". " + a + "\n\n"

                        loan_closecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amt_disbursed != null) {
                    if (loan_active_list.get(i).amt_disbursed!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence5_gujarati)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_active_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + loan_active_list.get(i).amt_disbursed!!.toString() + "'"
                        )
                        loan_active_listString =
                            loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                        loan_activecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amt_disbursed == null || loan_active_list.get(i).amt_disbursed!!.toInt() == 0) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence6_gujarati)
                    a = a.replace(
                        "SHG_MEM",
                        "'" + loan_active_list.get(i).member_name!!.toLowerCase() + "'"
                    )

                    loan_active_listString =
                        loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                    loan_activecount++
                }
            }

            for (i in 0 until shareCapital_list.size) {
                if (shareCapital_list.get(i).amount != null) {
                    if (shareCapital_list.get(i).amount!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence8_gujarati)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + shareCapital_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + shareCapital_list.get(i).amount!!.toString() + "'"
                        )
                        shareCapital_listString =
                            shareCapital_listString + loan_shareCapitalcount.toString() + ". " + a + "\n\n"
                        loan_shareCapitalcount++
                    }
                }
            }
        } else if (lang.equals("kn")) {
            days =
                arrayOf("ಭಾನುವಾರ", "ಸೋಮವಾರ", "ಮಂಗಳವಾರ", "ಬುಧವಾರ", "ಗುರುವಾರ", "ಶುಕ್ರವಾರ", "ಶನಿವಾರ")

            sentence1 = context.resources.getString(R.string.sentence1_Kannad) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_Kannad) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence6_Kannad) + "\n\n"
            sentence4 = context.resources.getString(R.string.shg_cutoff_sentence2_Kannad) + "\n\n"
            sentence5 = context.resources.getString(R.string.shg_cutoff_sentence4_Kannad) + "\n\n"
            sentence6 = context.resources.getString(R.string.shg_cutoff_sentence7_Kannad) + "\n\n"
            sentence7 = context.resources.getString(R.string.shg_cutoff_sentence9_Kannad) + "\n\n"
            sentence8 = context.resources.getString(R.string.shg_cutoff_sentence10_Kannad) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence14_Kannad) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence19_Kannad) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence20_Kannad) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_Kannad) + "\n\n"

            for (i in 0 until list.size) {

                if (!list.get(i).sav_comp!!.toString()
                        .equals("0") || !list.get(i).sav_vol!!.toString().equals("0")
                ) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence1_Kannad)
                    a = a.replace("SHG_MEM", list.get(i).member_name!!)
                    var amount = list.get(i).sav_comp!! + list.get(i).sav_vol!!
                    a = a.replace("SHG_AMOUNT", amount.toString())
                    AmountSaveListString =
                        AmountSaveListString + AmountSavecount.toString() + ". " + a + "\n\n"

                    AmountSavecount++
                }
            }

            for (i in 0 until loan_close_list.size) {
                var amount =
                    (context as CutOffMeetingDetailTTSMainActivity).getTotalClosedLoanAmount(
                        list.get(i).mem_id,
                        list.get(i).mtg_no
                    )
                if (amount != null) {
                    if (amount.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence3_Kannad)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_close_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace("SHG_AMOUNT", "'" + amount.toString() + "'")

                        loan_close_listString =
                            loan_close_listString + loan_closecount.toString() + ". " + a + "\n\n"

                        loan_closecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amt_disbursed != null) {
                    if (loan_active_list.get(i).amt_disbursed!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence5_Kannad)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_active_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + loan_active_list.get(i).amt_disbursed!!.toString() + "'"
                        )
                        loan_active_listString =
                            loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                        loan_activecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amt_disbursed == null || loan_active_list.get(i).amt_disbursed!!.toInt() == 0) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence6_Kannad)
                    a = a.replace(
                        "SHG_MEM",
                        "'" + loan_active_list.get(i).member_name!!.toLowerCase() + "'"
                    )

                    loan_active_listString =
                        loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                    loan_activecount++
                }
            }

            for (i in 0 until shareCapital_list.size) {
                if (shareCapital_list.get(i).amount != null) {
                    if (shareCapital_list.get(i).amount!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence8_Kannad)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + shareCapital_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + shareCapital_list.get(i).amount!!.toString() + "'"
                        )
                        shareCapital_listString =
                            shareCapital_listString + loan_shareCapitalcount.toString() + ". " + a + "\n\n"
                        loan_shareCapitalcount++
                    }
                }
            }
        } else if (lang.equals("ml")) {
            days = arrayOf("ഞായർ", "തിങ്കൾ", "ചൊവ്വ", "ബുധൻ", "വ്യാഴം", "വെള്ളി", "ശനി")

            sentence1 = context.resources.getString(R.string.sentence1_malayalam) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_malayalam) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence6_malayalam) + "\n\n"
            sentence4 =
                context.resources.getString(R.string.shg_cutoff_sentence2_malayalam) + "\n\n"
            sentence5 =
                context.resources.getString(R.string.shg_cutoff_sentence4_malayalam) + "\n\n"
            sentence6 =
                context.resources.getString(R.string.shg_cutoff_sentence7_malayalam) + "\n\n"
            sentence7 =
                context.resources.getString(R.string.shg_cutoff_sentence9_malayalam) + "\n\n"
            sentence8 =
                context.resources.getString(R.string.shg_cutoff_sentence10_malayalam) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence14_malayalam) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence19_malayalam) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence20_malayalam) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_malayalam) + "\n\n"

            for (i in 0 until list.size) {

                if (!list.get(i).sav_comp!!.toString()
                        .equals("0") || !list.get(i).sav_vol!!.toString().equals("0")
                ) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence1_malayalam)
                    a = a.replace("SHG_MEM", list.get(i).member_name!!)
                    var amount = list.get(i).sav_comp!! + list.get(i).sav_vol!!
                    a = a.replace("SHG_AMOUNT", amount.toString())
                    AmountSaveListString =
                        AmountSaveListString + AmountSavecount.toString() + ". " + a + "\n\n"

                    AmountSavecount++
                }
            }

            for (i in 0 until loan_close_list.size) {
                var amount =
                    (context as CutOffMeetingDetailTTSMainActivity).getTotalClosedLoanAmount(
                        list.get(i).mem_id,
                        list.get(i).mtg_no
                    )
                if (amount != null) {
                    if (amount.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence3_malayalam)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_close_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace("SHG_AMOUNT", "'" + amount.toString() + "'")

                        loan_close_listString =
                            loan_close_listString + loan_closecount.toString() + ". " + a + "\n\n"

                        loan_closecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amt_disbursed != null) {
                    if (loan_active_list.get(i).amt_disbursed!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence5_malayalam)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_active_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + loan_active_list.get(i).amt_disbursed!!.toString() + "'"
                        )
                        loan_active_listString =
                            loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                        loan_activecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amt_disbursed == null || loan_active_list.get(i).amt_disbursed!!.toInt() == 0) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence6_malayalam)
                    a = a.replace(
                        "SHG_MEM",
                        "'" + loan_active_list.get(i).member_name!!.toLowerCase() + "'"
                    )

                    loan_active_listString =
                        loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                    loan_activecount++
                }
            }

            for (i in 0 until shareCapital_list.size) {
                if (shareCapital_list.get(i).amount != null) {
                    if (shareCapital_list.get(i).amount!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence8_malayalam)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + shareCapital_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + shareCapital_list.get(i).amount!!.toString() + "'"
                        )
                        shareCapital_listString =
                            shareCapital_listString + loan_shareCapitalcount.toString() + ". " + a + "\n\n"
                        loan_shareCapitalcount++
                    }
                }
            }
        } else if (lang.equals("mr")) {
            days = arrayOf("रविवार", "सोमवार", "मंगलवार", "बुधवार", "गुरुवार", "शुक्रवार", "शनिवार")

            sentence1 = context.resources.getString(R.string.sentence1_marathi) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_marathi) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence6_marathi) + "\n\n"
            sentence4 = context.resources.getString(R.string.shg_cutoff_sentence2_marathi) + "\n\n"
            sentence5 = context.resources.getString(R.string.shg_cutoff_sentence4_marathi) + "\n\n"
            sentence6 = context.resources.getString(R.string.shg_cutoff_sentence7_marathi) + "\n\n"
            sentence7 = context.resources.getString(R.string.shg_cutoff_sentence9_marathi) + "\n\n"
            sentence8 = context.resources.getString(R.string.shg_cutoff_sentence10_marathi) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence14_marathi) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence19_marathi) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence20_marathi) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_marathi) + "\n\n"

            for (i in 0 until list.size) {

                if (!list.get(i).sav_comp!!.toString()
                        .equals("0") || !list.get(i).sav_vol!!.toString().equals("0")
                ) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence1_marathi)
                    a = a.replace("SHG_MEM", list.get(i).member_name!!)
                    var amount = list.get(i).sav_comp!! + list.get(i).sav_vol!!
                    a = a.replace("SHG_AMOUNT", amount.toString())
                    AmountSaveListString =
                        AmountSaveListString + AmountSavecount.toString() + ". " + a + "\n\n"

                    AmountSavecount++
                }
            }

            for (i in 0 until loan_close_list.size) {
                var amount =
                    (context as CutOffMeetingDetailTTSMainActivity).getTotalClosedLoanAmount(
                        list.get(i).mem_id,
                        list.get(i).mtg_no
                    )
                if (amount != null) {
                    if (amount.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence3_marathi)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_close_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace("SHG_AMOUNT", "'" + amount.toString() + "'")

                        loan_close_listString =
                            loan_close_listString + loan_closecount.toString() + ". " + a + "\n\n"

                        loan_closecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amt_disbursed != null) {
                    if (loan_active_list.get(i).amt_disbursed!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence5_marathi)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_active_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + loan_active_list.get(i).amt_disbursed!!.toString() + "'"
                        )
                        loan_active_listString =
                            loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                        loan_activecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amt_disbursed == null || loan_active_list.get(i).amt_disbursed!!.toInt() == 0) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence6_marathi)
                    a = a.replace(
                        "SHG_MEM",
                        "'" + loan_active_list.get(i).member_name!!.toLowerCase() + "'"
                    )

                    loan_active_listString =
                        loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                    loan_activecount++
                }
            }

            for (i in 0 until shareCapital_list.size) {
                if (shareCapital_list.get(i).amount != null) {
                    if (shareCapital_list.get(i).amount!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence8_marathi)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + shareCapital_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + shareCapital_list.get(i).amount!!.toString() + "'"
                        )
                        shareCapital_listString =
                            shareCapital_listString + loan_shareCapitalcount.toString() + ". " + a + "\n\n"
                        loan_shareCapitalcount++
                    }
                }
            }
        } else if (lang.equals("or")) {
            days =
                arrayOf("ରବିବାର", "ସୋମବାର", "ମଙ୍ଗଳବାର", "ବୁଧବାର", "ଗୁରୁବାର", "ଶୁକ୍ରବାର", "ଶନିବାର")

            sentence1 = context.resources.getString(R.string.sentence1_oriya) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_oriya) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence6_oriya) + "\n\n"
            sentence4 = context.resources.getString(R.string.shg_cutoff_sentence2_oriya) + "\n\n"
            sentence5 = context.resources.getString(R.string.shg_cutoff_sentence4_oriya) + "\n\n"
            sentence6 = context.resources.getString(R.string.shg_cutoff_sentence7_oriya) + "\n\n"
            sentence7 = context.resources.getString(R.string.shg_cutoff_sentence9_oriya) + "\n\n"
            sentence8 = context.resources.getString(R.string.shg_cutoff_sentence10_oriya) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence14_oriya) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence19_oriya) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence20_oriya) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_oriya) + "\n\n"

            for (i in 0 until list.size) {

                if (!list.get(i).sav_comp!!.toString()
                        .equals("0") || !list.get(i).sav_vol!!.toString().equals("0")
                ) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence1_oriya)
                    a = a.replace("SHG_MEM", list.get(i).member_name!!)
                    var amount = list.get(i).sav_comp!! + list.get(i).sav_vol!!
                    a = a.replace("SHG_AMOUNT", amount.toString())
                    AmountSaveListString =
                        AmountSaveListString + AmountSavecount.toString() + ". " + a + "\n\n"

                    AmountSavecount++
                }
            }

            for (i in 0 until loan_close_list.size) {
                var amount =
                    (context as CutOffMeetingDetailTTSMainActivity).getTotalClosedLoanAmount(
                        list.get(i).mem_id,
                        list.get(i).mtg_no
                    )
                if (amount != null) {
                    if (amount.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence3_oriya)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_close_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace("SHG_AMOUNT", "'" + amount.toString() + "'")

                        loan_close_listString =
                            loan_close_listString + loan_closecount.toString() + ". " + a + "\n\n"

                        loan_closecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amt_disbursed != null) {
                    if (loan_active_list.get(i).amt_disbursed!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence5_oriya)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_active_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + loan_active_list.get(i).amt_disbursed!!.toString() + "'"
                        )
                        loan_active_listString =
                            loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                        loan_activecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amt_disbursed == null || loan_active_list.get(i).amt_disbursed!!.toInt() == 0) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence6_oriya)
                    a = a.replace(
                        "SHG_MEM",
                        "'" + loan_active_list.get(i).member_name!!.toLowerCase() + "'"
                    )

                    loan_active_listString =
                        loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                    loan_activecount++
                }
            }

            for (i in 0 until shareCapital_list.size) {
                if (shareCapital_list.get(i).amount != null) {
                    if (shareCapital_list.get(i).amount!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence8_oriya)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + shareCapital_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + shareCapital_list.get(i).amount!!.toString() + "'"
                        )
                        shareCapital_listString =
                            shareCapital_listString + loan_shareCapitalcount.toString() + ". " + a + "\n\n"
                        loan_shareCapitalcount++
                    }
                }
            }
        } else if (lang.equals("pa")) {
            days =
                arrayOf("ਐਤਵਾਰ", "ਸੋਮਵਾਰ", "ਮੰਗਲਵਾਰ", "ਬੁੱਧਵਾਰ", "ਵੀਰਵਾਰ", "ਸ਼ੁੱਕਰਵਾਰ", "ਸ਼ਨੀਵਾਰ")

            sentence1 = context.resources.getString(R.string.sentence1_punjab) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_punjab) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence6_punjab) + "\n\n"
            sentence4 = context.resources.getString(R.string.shg_cutoff_sentence2_punjab) + "\n\n"
            sentence5 = context.resources.getString(R.string.shg_cutoff_sentence4_punjab) + "\n\n"
            sentence6 = context.resources.getString(R.string.shg_cutoff_sentence7_punjab) + "\n\n"
            sentence7 = context.resources.getString(R.string.shg_cutoff_sentence9_punjab) + "\n\n"
            sentence8 = context.resources.getString(R.string.shg_cutoff_sentence10_punjab) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence14_punjab) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence19_punjab) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence20_punjab) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_punjab) + "\n\n"

            for (i in 0 until list.size) {

                if (!list.get(i).sav_comp!!.toString()
                        .equals("0") || !list.get(i).sav_vol!!.toString().equals("0")
                ) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence1_punjab)
                    a = a.replace("SHG_MEM", list.get(i).member_name!!)
                    var amount = list.get(i).sav_comp!! + list.get(i).sav_vol!!
                    a = a.replace("SHG_AMOUNT", amount.toString())
                    AmountSaveListString =
                        AmountSaveListString + AmountSavecount.toString() + ". " + a + "\n\n"

                    AmountSavecount++
                }
            }

            for (i in 0 until loan_close_list.size) {
                var amount =
                    (context as CutOffMeetingDetailTTSMainActivity).getTotalClosedLoanAmount(
                        list.get(i).mem_id,
                        list.get(i).mtg_no
                    )
                if (amount != null) {
                    if (amount.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence3_punjab)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_close_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace("SHG_AMOUNT", "'" + amount.toString() + "'")

                        loan_close_listString =
                            loan_close_listString + loan_closecount.toString() + ". " + a + "\n\n"

                        loan_closecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amt_disbursed != null) {
                    if (loan_active_list.get(i).amt_disbursed!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence5_punjab)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_active_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + loan_active_list.get(i).amt_disbursed!!.toString() + "'"
                        )
                        loan_active_listString =
                            loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                        loan_activecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amt_disbursed == null || loan_active_list.get(i).amt_disbursed!!.toInt() == 0) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence6_punjab)
                    a = a.replace(
                        "SHG_MEM",
                        "'" + loan_active_list.get(i).member_name!!.toLowerCase() + "'"
                    )

                    loan_active_listString =
                        loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                    loan_activecount++
                }
            }

            for (i in 0 until shareCapital_list.size) {
                if (shareCapital_list.get(i).amount != null) {
                    if (shareCapital_list.get(i).amount!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence8_punjab)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + shareCapital_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + shareCapital_list.get(i).amount!!.toString() + "'"
                        )
                        shareCapital_listString =
                            shareCapital_listString + loan_shareCapitalcount.toString() + ". " + a + "\n\n"
                        loan_shareCapitalcount++
                    }
                }
            }
        } else if (lang.equals("ta")) {
            days =
                arrayOf("ஞாயிறு", "திங்கள்", "செவ்வாய்", "புதன்", "வியாழன்", "வெள்ளி", "சனிக்கிழமை")

            sentence1 = context.resources.getString(R.string.sentence1_tamil) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_tamil) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence6_tamil) + "\n\n"
            sentence4 = context.resources.getString(R.string.shg_cutoff_sentence2_tamil) + "\n\n"
            sentence5 = context.resources.getString(R.string.shg_cutoff_sentence4_tamil) + "\n\n"
            sentence6 = context.resources.getString(R.string.shg_cutoff_sentence7_tamil) + "\n\n"
            sentence7 = context.resources.getString(R.string.shg_cutoff_sentence9_tamil) + "\n\n"
            sentence8 = context.resources.getString(R.string.shg_cutoff_sentence10_tamil) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence14_tamil) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence19_tamil) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence20_tamil) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_tamil) + "\n\n"

            for (i in 0 until list.size) {

                if (!list.get(i).sav_comp!!.toString()
                        .equals("0") || !list.get(i).sav_vol!!.toString().equals("0")
                ) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence1_tamil)
                    a = a.replace("SHG_MEM", list.get(i).member_name!!)
                    var amount = list.get(i).sav_comp!! + list.get(i).sav_vol!!
                    a = a.replace("SHG_AMOUNT", amount.toString())
                    AmountSaveListString =
                        AmountSaveListString + AmountSavecount.toString() + ". " + a + "\n\n"

                    AmountSavecount++
                }
            }

            for (i in 0 until loan_close_list.size) {
                var amount =
                    (context as CutOffMeetingDetailTTSMainActivity).getTotalClosedLoanAmount(
                        list.get(i).mem_id,
                        list.get(i).mtg_no
                    )
                if (amount != null) {
                    if (amount.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence3_tamil)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_close_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace("SHG_AMOUNT", "'" + amount.toString() + "'")

                        loan_close_listString =
                            loan_close_listString + loan_closecount.toString() + ". " + a + "\n\n"

                        loan_closecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amt_disbursed != null) {
                    if (loan_active_list.get(i).amt_disbursed!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence5_tamil)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_active_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + loan_active_list.get(i).amt_disbursed!!.toString() + "'"
                        )
                        loan_active_listString =
                            loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                        loan_activecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amt_disbursed == null || loan_active_list.get(i).amt_disbursed!!.toInt() == 0) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence6_tamil)
                    a = a.replace(
                        "SHG_MEM",
                        "'" + loan_active_list.get(i).member_name!!.toLowerCase() + "'"
                    )

                    loan_active_listString =
                        loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                    loan_activecount++
                }
            }

            for (i in 0 until shareCapital_list.size) {
                if (shareCapital_list.get(i).amount != null) {
                    if (shareCapital_list.get(i).amount!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence8_tamil)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + shareCapital_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + shareCapital_list.get(i).amount!!.toString() + "'"
                        )
                        shareCapital_listString =
                            shareCapital_listString + loan_shareCapitalcount.toString() + ". " + a + "\n\n"
                        loan_shareCapitalcount++
                    }
                }
            }
        } else if (lang.equals("te")) {
            days =
                arrayOf(
                    "ఆదివారం",
                    "సోమవారం",
                    "మంగళవారం",
                    "బుధవారం",
                    "గురువారం",
                    "శుక్రవారం",
                    "శనివారం"
                )
            sentence1 = context.resources.getString(R.string.sentence1_telgu) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_telgu) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence6_telgu) + "\n\n"
            sentence4 = context.resources.getString(R.string.shg_cutoff_sentence2_telgu) + "\n\n"
            sentence5 = context.resources.getString(R.string.shg_cutoff_sentence4_telgu) + "\n\n"
            sentence6 = context.resources.getString(R.string.shg_cutoff_sentence7_telgu) + "\n\n"
            sentence7 = context.resources.getString(R.string.shg_cutoff_sentence9_telgu) + "\n\n"
            sentence8 = context.resources.getString(R.string.shg_cutoff_sentence10_telgu) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence14_telgu) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence19_telgu) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence20_telgu) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_telgu) + "\n\n"

            for (i in 0 until list.size) {

                if (!list.get(i).sav_comp!!.toString()
                        .equals("0") || !list.get(i).sav_vol!!.toString().equals("0")
                ) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence1_telgu)
                    a = a.replace("SHG_MEM", list.get(i).member_name!!)
                    var amount = list.get(i).sav_comp!! + list.get(i).sav_vol!!
                    a = a.replace("SHG_AMOUNT", amount.toString())
                    AmountSaveListString =
                        AmountSaveListString + AmountSavecount.toString() + ". " + a + "\n\n"

                    AmountSavecount++
                }
            }

            for (i in 0 until loan_close_list.size) {
                var amount =
                    (context as CutOffMeetingDetailTTSMainActivity).getTotalClosedLoanAmount(
                        list.get(i).mem_id,
                        list.get(i).mtg_no
                    )
                if (amount != null) {
                    if (amount.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence3_telgu)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_close_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace("SHG_AMOUNT", "'" + amount.toString() + "'")

                        loan_close_listString =
                            loan_close_listString + loan_closecount.toString() + ". " + a + "\n\n"

                        loan_closecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amt_disbursed != null) {
                    if (loan_active_list.get(i).amt_disbursed!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence5_telgu)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_active_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + loan_active_list.get(i).amt_disbursed!!.toString() + "'"
                        )
                        loan_active_listString =
                            loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                        loan_activecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amt_disbursed == null || loan_active_list.get(i).amt_disbursed!!.toInt() == 0) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence6_telgu)
                    a = a.replace(
                        "SHG_MEM",
                        "'" + loan_active_list.get(i).member_name!!.toLowerCase() + "'"
                    )

                    loan_active_listString =
                        loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                    loan_activecount++
                }
            }

            for (i in 0 until shareCapital_list.size) {
                if (shareCapital_list.get(i).amount != null) {
                    if (shareCapital_list.get(i).amount!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence8_telgu)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + shareCapital_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + shareCapital_list.get(i).amount!!.toString() + "'"
                        )
                        shareCapital_listString =
                            shareCapital_listString + loan_shareCapitalcount.toString() + ". " + a + "\n\n"
                        loan_shareCapitalcount++
                    }
                }
            }
        } else if (lang.equals("ur")) {
            days = arrayOf("اتوار", "پیر", "منگل", "بدھ", "جمعرات", "جمعہ", "ہفتہ")

            sentence1 = context.resources.getString(R.string.sentence1_urdu) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_urdu) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence6_urdu) + "\n\n"
            sentence4 = context.resources.getString(R.string.shg_cutoff_sentence2_urdu) + "\n\n"
            sentence5 = context.resources.getString(R.string.shg_cutoff_sentence4_urdu) + "\n\n"
            sentence6 = context.resources.getString(R.string.shg_cutoff_sentence7_urdu) + "\n\n"
            sentence7 = context.resources.getString(R.string.shg_cutoff_sentence9_urdu) + "\n\n"
            sentence8 = context.resources.getString(R.string.shg_cutoff_sentence10_urdu) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence14_urdu) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence19_urdu) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence20_urdu) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_urdu) + "\n\n"

            for (i in 0 until list.size) {

                if (!list.get(i).sav_comp!!.toString()
                        .equals("0") || !list.get(i).sav_vol!!.toString().equals("0")
                ) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence1_urdu)
                    a = a.replace("SHG_MEM", list.get(i).member_name!!)
                    var amount = list.get(i).sav_comp!! + list.get(i).sav_vol!!
                    a = a.replace("SHG_AMOUNT", amount.toString())
                    AmountSaveListString =
                        AmountSaveListString + AmountSavecount.toString() + ". " + a + "\n\n"

                    AmountSavecount++
                }
            }

            for (i in 0 until loan_close_list.size) {
                var amount =
                    (context as CutOffMeetingDetailTTSMainActivity).getTotalClosedLoanAmount(
                        list.get(i).mem_id,
                        list.get(i).mtg_no
                    )
                if (amount != null) {
                    if (amount.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence3_urdu)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_close_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace("SHG_AMOUNT", "'" + amount.toString() + "'")

                        loan_close_listString =
                            loan_close_listString + loan_closecount.toString() + ". " + a + "\n\n"

                        loan_closecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amt_disbursed != null) {
                    if (loan_active_list.get(i).amt_disbursed!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence5_urdu)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_active_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + loan_active_list.get(i).amt_disbursed!!.toString() + "'"
                        )
                        loan_active_listString =
                            loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                        loan_activecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amt_disbursed == null || loan_active_list.get(i).amt_disbursed!!.toInt() == 0) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence6_urdu)
                    a = a.replace(
                        "SHG_MEM",
                        "'" + loan_active_list.get(i).member_name!!.toLowerCase() + "'"
                    )

                    loan_active_listString =
                        loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                    loan_activecount++
                }
            }

            for (i in 0 until shareCapital_list.size) {
                if (shareCapital_list.get(i).amount != null) {
                    if (shareCapital_list.get(i).amount!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence8_urdu)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + shareCapital_list.get(i).member_name!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + shareCapital_list.get(i).amount!!.toString() + "'"
                        )
                        shareCapital_listString =
                            shareCapital_listString + loan_shareCapitalcount.toString() + ". " + a + "\n\n"
                        loan_shareCapitalcount++
                    }
                }
            }
        }

        var date = Date()
        val date_format = SimpleDateFormat("dd-MM-yyyy")
        try {
            date = date_format.parse(MeetingDate)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        val c = Calendar.getInstance()
        c.time = date
        var dayIndex: Int = c.get(Calendar.DAY_OF_WEEK)


        var dayData = days[dayIndex - 1]

        sentence2 = sentence2.replace("SHG_Name", "'" + SHG_name + "'")
        sentence2 = sentence2.replace("SHG_DATE", MeetingDate)
        sentence2 = sentence2.replace("SHG_DAY", dayData)
        sentence2 = sentence2.replace("MEET_NO", meetNo.toString())

        sentence3 = sentence3.replace("SHG_AMOUNT", "'" + savingAmount.toString() + "'")
        sentence4 = sentence4.replace("SHG_AMOUNT", "'" + closedLoan_amount.toString() + "'")
        sentence5 = sentence5.replace("SHG_AMOUNT", "'" + activeLoan_amount.toString() + "'")
        sentence6 = sentence6.replace("SHG_AMOUNT", "'" + share_capital_amount.toString() + "'")
        sentence7 = sentence7.replace("SHG_AMOUNT", "'" + investAmount.toString() + "'")
        sentence8 = sentence8.replace("SHG_AMOUNT", "'" + borrowAmount.toString() + "'")

        sentence10 = sentence10.replace("SHG_AMOUNT", "= '" + bankBalance.toString() + "= '")
        sentence11 = sentence11.replace("SHG_AMOUNT", "= '" + cashBalance.toString() + "= '")

        sentence3 = sentence3.replace("₹", "")
        sentence4 = sentence4.replace("₹", "")
        sentence5 = sentence5.replace("₹", "")
        sentence6 = sentence6.replace("₹", "")
        sentence7 = sentence7.replace("₹", "")
        sentence8 = sentence8.replace("₹", "")
        sentence10 = sentence10.replace("₹", "")
        sentence11 = sentence11.replace("₹", "")



        if (AmountSaveListString.isNullOrEmpty()) {
        } else {
            AmountSaveListString = AmountSaveListString + "\n"
        }
        if (loan_close_listString.isNullOrEmpty()) {
        } else {
            loan_close_listString = loan_close_listString + "\n"
        }
        if (loan_active_listString.isNullOrEmpty()) {
        } else {
            loan_active_listString = loan_active_listString + "\n"
        }
        if (shareCapital_listString.isNullOrEmpty()) {
        } else {
            shareCapital_listString = shareCapital_listString + "\n"
        }

        finalParagraph =
            sentence1 + "@#@" + sentence2 + "@#@" + sentence3 + "@#@" + AmountSaveListString + "@#@" + sentence4 + "@#@" + loan_close_listString + "@#@" +
                    sentence5 + "@#@" + loan_active_listString + "@#@" + sentence6 + "@#@" + shareCapital_listString + "@#@" + sentence7 + "@#@" + sentence8 + "@#@" +
                    sentence9 + "@#@" + sentence10 + "@#@" + sentence11 + "@#@" + sentence21


        return finalParagraph
    }


    fun GetVOCutOffParagraph(
        meetNo: Int,
        lang: String,
        context: Context,
        list: List<VoMtgDetEntity>,
        loan_close_list: List<VoMtgDetEntity>,
        loan_active_list: List<VoLoanListModel>,
        shareCapital_list: List<VoShareCapitalModel>,
        SHG_name: String,
        MeetingDate: String,
        savingAmount: String,
        closedLoan_amount: String,
        activeLoan_amount: String,
        share_capital_amount: String,
        investAmount: String,
        borrowAmount: String,
        cashBalance: String,
        bankBalance: String
    ): String {
        var sentence1 = ""
        var sentence2 = ""
        var sentence3 = ""
        var sentence4 = ""
        var sentence5 = ""
        var sentence6 = ""
        var sentence7 = ""
        var sentence8 = ""
        var sentence9 = ""
        var sentence10 = ""
        var sentence11 = ""
        var sentence19 = ""
        var sentence20 = ""
        var sentence21 = ""
        var finalParagraph = ""

        var AmountSaveListString = ""
        var loan_close_listString = ""
        var loan_active_listString = ""
        var shareCapital_listString = ""
        var AmountSavecount = 1
        var loan_closecount = 1
        var loan_activecount = 1
        var loan_shareCapitalcount = 1

        var days = arrayOf("रविवार", "सोमवार", "मंगलवार", "बुधवार", "गुरुवार", "शुक्रवार", "शनिवार")

        if (lang.equals("hi")) {
            days = arrayOf("रविवार", "सोमवार", "मंगलवार", "बुधवार", "गुरुवार", "शुक्रवार", "शनिवार")

            sentence1 = context.resources.getString(R.string.sentence1_hin) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_hin) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence6_hin) + "\n\n"
            sentence4 = context.resources.getString(R.string.shg_cutoff_sentence2_hin) + "\n\n"
            sentence5 = context.resources.getString(R.string.shg_cutoff_sentence4_hin) + "\n\n"
            sentence6 = context.resources.getString(R.string.shg_cutoff_sentence7_hin) + "\n\n"
            sentence7 = context.resources.getString(R.string.shg_cutoff_sentence9_hin) + "\n\n"
            sentence8 = context.resources.getString(R.string.shg_cutoff_sentence10_hin) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence14_hin) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence19_hin) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence20_hin) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_hin) + "\n\n"

            for (i in 0 until list.size) {

                if (!list.get(i).savComp!!.toString()
                        .equals("0") || !list.get(i).savVol!!.toString().equals("0")
                ) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence1_hin)
                    a = a.replace("SHG_MEM", list.get(i).childCboName!!)
                    var amount = list.get(i).savComp!! + list.get(i).savVol!!
                    a = a.replace("SHG_AMOUNT", amount.toString())
                    AmountSaveListString =
                        AmountSaveListString + AmountSavecount.toString() + ". " + a + "\n\n"

                    AmountSavecount++
                }
            }

            for (i in 0 until loan_close_list.size) {
                var amount =
                    (context as VOCutOffMeetingTTSActivity).getTotalClosedLoanAmount(
                        list.get(i).memId,
                        list.get(i).mtgNo
                    )
                if (amount != null) {
                    if (amount.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence3_hin)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_close_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace("SHG_AMOUNT", "'" + amount.toString() + "'")

                        loan_close_listString =
                            loan_close_listString + loan_closecount.toString() + ". " + a + "\n\n"

                        loan_closecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amtDisbursed != null) {
                    if (loan_active_list.get(i).amtDisbursed!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence5_hin)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_active_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + loan_active_list.get(i).amtDisbursed!!.toString() + "'"
                        )
                        loan_active_listString =
                            loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                        loan_activecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amtDisbursed == null || loan_active_list.get(i).amtDisbursed!!.toInt() == 0) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence6_hin)
                    a = a.replace(
                        "SHG_MEM",
                        "'" + loan_active_list.get(i).childCboName!!.toLowerCase() + "'"
                    )

                    loan_active_listString =
                        loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                    loan_activecount++
                }
            }

            for (i in 0 until shareCapital_list.size) {
                if (shareCapital_list.get(i).amount != null) {
                    if (shareCapital_list.get(i).amount!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence8_hin)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + shareCapital_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + shareCapital_list.get(i).amount!!.toString() + "'"
                        )
                        shareCapital_listString =
                            shareCapital_listString + loan_shareCapitalcount.toString() + ". " + a + "\n\n"
                        loan_shareCapitalcount++
                    }
                }
            }
        } else if (lang.equals("en")) {
            days = arrayOf(
                "Sunday",
                "Monday",
                "Tuesday",
                "Wednesday",
                "Thursday",
                "Friday",
                "Saturday"
            )

            sentence1 = context.resources.getString(R.string.sentence1) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence6) + "\n\n"
            sentence4 = context.resources.getString(R.string.shg_cutoff_sentence2) + "\n\n"
            sentence5 = context.resources.getString(R.string.shg_cutoff_sentence4) + "\n\n"
            sentence6 = context.resources.getString(R.string.shg_cutoff_sentence7) + "\n\n"
            sentence7 = context.resources.getString(R.string.shg_cutoff_sentence9) + "\n\n"
            sentence8 = context.resources.getString(R.string.shg_cutoff_sentence10) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence14) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence19) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence20) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21) + "\n\n"

            for (i in 0 until list.size) {

                if (!list.get(i).savComp!!.toString()
                        .equals("0") || !list.get(i).savVol!!.toString().equals("0")
                ) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence1)
                    a = a.replace("SHG_MEM", list.get(i).childCboName!!)
                    var amount = list.get(i).savComp!! + list.get(i).savVol!!
                    a = a.replace("SHG_AMOUNT", amount.toString())
                    AmountSaveListString =
                        AmountSaveListString + AmountSavecount.toString() + ". " + a + "\n\n"

                    AmountSavecount++
                }
            }

            for (i in 0 until loan_close_list.size) {

                var amount =
                    (context as VOCutOffMeetingTTSActivity).getTotalClosedLoanAmount(
                        list.get(i).memId,
                        list.get(i).mtgNo
                    )

                if (amount != null) {
                    if (amount.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence3)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_close_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace("SHG_AMOUNT", "'" + amount.toString() + "'")

                        loan_close_listString =
                            loan_close_listString + loan_closecount.toString() + ". " + a + "\n\n"

                        loan_closecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amtDisbursed != null) {
                    if (loan_active_list.get(i).amtDisbursed!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence5)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_active_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + loan_active_list.get(i).amtDisbursed!!.toString() + "'"
                        )
                        loan_active_listString =
                            loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                        loan_activecount++
                    }

                    if (loan_active_list.get(i).amtDisbursed == null || loan_active_list.get(i).amtDisbursed!!.toInt() == 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence6)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_active_list.get(i).childCboName!!.toLowerCase() + "'"
                        )

                        loan_active_listString =
                            loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                        loan_activecount++
                    }

                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amtDisbursed == null || loan_active_list.get(i).amtDisbursed!!.toInt() == 0) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence6)
                    a = a.replace(
                        "SHG_MEM",
                        "'" + loan_active_list.get(i).childCboName!!.toLowerCase() + "'"
                    )

                    loan_active_listString =
                        loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                    loan_activecount++
                }
            }

            for (i in 0 until shareCapital_list.size) {
                if (shareCapital_list.get(i).amount != null) {
                    if (shareCapital_list.get(i).amount!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence8)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + shareCapital_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + shareCapital_list.get(i).amount!!.toString() + "'"
                        )
                        shareCapital_listString =
                            shareCapital_listString + loan_shareCapitalcount.toString() + ". " + a + "\n\n"
                        loan_shareCapitalcount++
                    }
                }
            }
        } else if (lang.equals("as")) {
            days = arrayOf(
                "দেওবাৰ",
                "সোমবাৰে",
                "মঙলবাৰে",
                "বুধবাৰে",
                "বৃহস্পতিবাৰে",
                "শুক্ৰবাৰে",
                "শনিবাৰে"
            )
            sentence1 = context.resources.getString(R.string.sentence1_ass) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_ass) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence6_ass) + "\n\n"
            sentence4 = context.resources.getString(R.string.shg_cutoff_sentence2_ass) + "\n\n"
            sentence5 = context.resources.getString(R.string.shg_cutoff_sentence4_ass) + "\n\n"
            sentence6 = context.resources.getString(R.string.shg_cutoff_sentence7_ass) + "\n\n"
            sentence7 = context.resources.getString(R.string.shg_cutoff_sentence9_ass) + "\n\n"
            sentence8 = context.resources.getString(R.string.shg_cutoff_sentence10_ass) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence14_ass) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence19_ass) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence20_ass) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_ass) + "\n\n"

            for (i in 0 until list.size) {

                if (!list.get(i).savComp!!.toString()
                        .equals("0") || !list.get(i).savVol!!.toString().equals("0")
                ) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence1_ass)
                    a = a.replace("SHG_MEM", list.get(i).childCboName!!)
                    var amount = list.get(i).savComp!! + list.get(i).savVol!!
                    a = a.replace("SHG_AMOUNT", amount.toString())
                    AmountSaveListString =
                        AmountSaveListString + AmountSavecount.toString() + ". " + a + "\n\n"

                    AmountSavecount++
                }
            }

            for (i in 0 until loan_close_list.size) {
                var amount =
                    (context as VOCutOffMeetingTTSActivity).getTotalClosedLoanAmount(
                        list.get(i).memId,
                        list.get(i).mtgNo
                    )
                if (amount != null) {
                    if (amount.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence3_ass)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_close_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace("SHG_AMOUNT", "'" + amount.toString() + "'")

                        loan_close_listString =
                            loan_close_listString + loan_closecount.toString() + ". " + a + "\n\n"

                        loan_closecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amtDisbursed != null) {
                    if (loan_active_list.get(i).amtDisbursed!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence5_ass)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_active_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + loan_active_list.get(i).amtDisbursed!!.toString() + "'"
                        )
                        loan_active_listString =
                            loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                        loan_activecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amtDisbursed == null || loan_active_list.get(i).amtDisbursed!!.toInt() == 0) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence6_ass)
                    a = a.replace(
                        "SHG_MEM",
                        "'" + loan_active_list.get(i).childCboName!!.toLowerCase() + "'"
                    )

                    loan_active_listString =
                        loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                    loan_activecount++
                }
            }

            for (i in 0 until shareCapital_list.size) {
                if (shareCapital_list.get(i).amount != null) {
                    if (shareCapital_list.get(i).amount!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence8_ass)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + shareCapital_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + shareCapital_list.get(i).amount!!.toString() + "'"
                        )
                        shareCapital_listString =
                            shareCapital_listString + loan_shareCapitalcount.toString() + ". " + a + "\n\n"
                        loan_shareCapitalcount++
                    }
                }
            }
        } else if (lang.equals("bn")) {
            days =
                arrayOf(
                    "রবিবার",
                    "সোমবার",
                    "মঙ্গলবার",
                    "বুধবার",
                    "বৃহস্পতিবার",
                    "শুক্রবার",
                    "শনিবার"
                )
            sentence1 = context.resources.getString(R.string.sentence1_bangla) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_bangla) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence6_bangla) + "\n\n"
            sentence4 = context.resources.getString(R.string.shg_cutoff_sentence2_bangla) + "\n\n"
            sentence5 = context.resources.getString(R.string.shg_cutoff_sentence4_bangla) + "\n\n"
            sentence6 = context.resources.getString(R.string.shg_cutoff_sentence7_bangla) + "\n\n"
            sentence7 = context.resources.getString(R.string.shg_cutoff_sentence9_bangla) + "\n\n"
            sentence8 = context.resources.getString(R.string.shg_cutoff_sentence10_bangla) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence14_bangla) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence19_bangla) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence20_bangla) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_bangla) + "\n\n"

            for (i in 0 until list.size) {

                if (!list.get(i).savComp!!.toString()
                        .equals("0") || !list.get(i).savVol!!.toString().equals("0")
                ) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence1_bangla)
                    a = a.replace("SHG_MEM", list.get(i).childCboName!!)
                    var amount = list.get(i).savComp!! + list.get(i).savVol!!
                    a = a.replace("SHG_AMOUNT", amount.toString())
                    AmountSaveListString =
                        AmountSaveListString + AmountSavecount.toString() + ". " + a + "\n\n"

                    AmountSavecount++
                }
            }

            for (i in 0 until loan_close_list.size) {
                var amount =
                    (context as VOCutOffMeetingTTSActivity).getTotalClosedLoanAmount(
                        list.get(i).memId,
                        list.get(i).mtgNo
                    )
                if (amount != null) {
                    if (amount.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence3_bangla)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_close_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace("SHG_AMOUNT", "'" + amount.toString() + "'")

                        loan_close_listString =
                            loan_close_listString + loan_closecount.toString() + ". " + a + "\n\n"

                        loan_closecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amtDisbursed != null) {
                    if (loan_active_list.get(i).amtDisbursed!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence5_bangla)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_active_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + loan_active_list.get(i).amtDisbursed!!.toString() + "'"
                        )
                        loan_active_listString =
                            loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                        loan_activecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amtDisbursed == null || loan_active_list.get(i).amtDisbursed!!.toInt() == 0) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence6_bangla)
                    a = a.replace(
                        "SHG_MEM",
                        "'" + loan_active_list.get(i).childCboName!!.toLowerCase() + "'"
                    )

                    loan_active_listString =
                        loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                    loan_activecount++
                }
            }

            for (i in 0 until shareCapital_list.size) {
                if (shareCapital_list.get(i).amount != null) {
                    if (shareCapital_list.get(i).amount!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence8_bangla)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + shareCapital_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + shareCapital_list.get(i).amount!!.toString() + "'"
                        )
                        shareCapital_listString =
                            shareCapital_listString + loan_shareCapitalcount.toString() + ". " + a + "\n\n"
                        loan_shareCapitalcount++
                    }
                }
            }
        } else if (lang.equals("gu")) {
            days = arrayOf("રવિવાર", "સોમવાર", "મંગળવાર", "બુધવાર", "ગુરુવાર", "શુક્રવાર", "શનિવાર")

            sentence1 = context.resources.getString(R.string.sentence1_gujarati) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_gujarati) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence6_gujarati) + "\n\n"
            sentence4 = context.resources.getString(R.string.shg_cutoff_sentence2_gujarati) + "\n\n"
            sentence5 = context.resources.getString(R.string.shg_cutoff_sentence4_gujarati) + "\n\n"
            sentence6 = context.resources.getString(R.string.shg_cutoff_sentence7_gujarati) + "\n\n"
            sentence7 = context.resources.getString(R.string.shg_cutoff_sentence9_gujarati) + "\n\n"
            sentence8 =
                context.resources.getString(R.string.shg_cutoff_sentence10_gujarati) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence14_gujarati) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence19_gujarati) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence20_gujarati) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_gujarati) + "\n\n"

            for (i in 0 until list.size) {

                if (!list.get(i).savComp!!.toString()
                        .equals("0") || !list.get(i).savVol!!.toString().equals("0")
                ) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence1_gujarati)
                    a = a.replace("SHG_MEM", list.get(i).childCboName!!)
                    var amount = list.get(i).savComp!! + list.get(i).savVol!!
                    a = a.replace("SHG_AMOUNT", amount.toString())
                    AmountSaveListString =
                        AmountSaveListString + AmountSavecount.toString() + ". " + a + "\n\n"

                    AmountSavecount++
                }
            }

            for (i in 0 until loan_close_list.size) {
                var amount =
                    (context as VOCutOffMeetingTTSActivity).getTotalClosedLoanAmount(
                        list.get(i).memId,
                        list.get(i).mtgNo
                    )
                if (amount != null) {
                    if (amount.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence3_gujarati)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_close_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace("SHG_AMOUNT", "'" + amount.toString() + "'")

                        loan_close_listString =
                            loan_close_listString + loan_closecount.toString() + ". " + a + "\n\n"

                        loan_closecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amtDisbursed != null) {
                    if (loan_active_list.get(i).amtDisbursed!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence5_gujarati)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_active_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + loan_active_list.get(i).amtDisbursed!!.toString() + "'"
                        )
                        loan_active_listString =
                            loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                        loan_activecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amtDisbursed == null || loan_active_list.get(i).amtDisbursed!!.toInt() == 0) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence6_gujarati)
                    a = a.replace(
                        "SHG_MEM",
                        "'" + loan_active_list.get(i).childCboName!!.toLowerCase() + "'"
                    )

                    loan_active_listString =
                        loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                    loan_activecount++
                }
            }

            for (i in 0 until shareCapital_list.size) {
                if (shareCapital_list.get(i).amount != null) {
                    if (shareCapital_list.get(i).amount!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence8_gujarati)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + shareCapital_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + shareCapital_list.get(i).amount!!.toString() + "'"
                        )
                        shareCapital_listString =
                            shareCapital_listString + loan_shareCapitalcount.toString() + ". " + a + "\n\n"
                        loan_shareCapitalcount++
                    }
                }
            }
        } else if (lang.equals("kn")) {
            days =
                arrayOf("ಭಾನುವಾರ", "ಸೋಮವಾರ", "ಮಂಗಳವಾರ", "ಬುಧವಾರ", "ಗುರುವಾರ", "ಶುಕ್ರವಾರ", "ಶನಿವಾರ")

            sentence1 = context.resources.getString(R.string.sentence1_Kannad) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_Kannad) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence6_Kannad) + "\n\n"
            sentence4 = context.resources.getString(R.string.shg_cutoff_sentence2_Kannad) + "\n\n"
            sentence5 = context.resources.getString(R.string.shg_cutoff_sentence4_Kannad) + "\n\n"
            sentence6 = context.resources.getString(R.string.shg_cutoff_sentence7_Kannad) + "\n\n"
            sentence7 = context.resources.getString(R.string.shg_cutoff_sentence9_Kannad) + "\n\n"
            sentence8 = context.resources.getString(R.string.shg_cutoff_sentence10_Kannad) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence14_Kannad) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence19_Kannad) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence20_Kannad) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_Kannad) + "\n\n"

            for (i in 0 until list.size) {

                if (!list.get(i).savComp!!.toString()
                        .equals("0") || !list.get(i).savVol!!.toString().equals("0")
                ) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence1_Kannad)
                    a = a.replace("SHG_MEM", list.get(i).childCboName!!)
                    var amount = list.get(i).savComp!! + list.get(i).savVol!!
                    a = a.replace("SHG_AMOUNT", amount.toString())
                    AmountSaveListString =
                        AmountSaveListString + AmountSavecount.toString() + ". " + a + "\n\n"

                    AmountSavecount++
                }
            }

            for (i in 0 until loan_close_list.size) {
                var amount =
                    (context as VOCutOffMeetingTTSActivity).getTotalClosedLoanAmount(
                        list.get(i).memId,
                        list.get(i).mtgNo
                    )
                if (amount != null) {
                    if (amount.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence3_Kannad)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_close_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace("SHG_AMOUNT", "'" + amount.toString() + "'")

                        loan_close_listString =
                            loan_close_listString + loan_closecount.toString() + ". " + a + "\n\n"

                        loan_closecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amtDisbursed != null) {
                    if (loan_active_list.get(i).amtDisbursed!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence5_Kannad)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_active_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + loan_active_list.get(i).amtDisbursed!!.toString() + "'"
                        )
                        loan_active_listString =
                            loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                        loan_activecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amtDisbursed == null || loan_active_list.get(i).amtDisbursed!!.toInt() == 0) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence6_Kannad)
                    a = a.replace(
                        "SHG_MEM",
                        "'" + loan_active_list.get(i).childCboName!!.toLowerCase() + "'"
                    )

                    loan_active_listString =
                        loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                    loan_activecount++
                }
            }

            for (i in 0 until shareCapital_list.size) {
                if (shareCapital_list.get(i).amount != null) {
                    if (shareCapital_list.get(i).amount!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence8_Kannad)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + shareCapital_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + shareCapital_list.get(i).amount!!.toString() + "'"
                        )
                        shareCapital_listString =
                            shareCapital_listString + loan_shareCapitalcount.toString() + ". " + a + "\n\n"
                        loan_shareCapitalcount++
                    }
                }
            }
        } else if (lang.equals("ml")) {
            days = arrayOf("ഞായർ", "തിങ്കൾ", "ചൊവ്വ", "ബുധൻ", "വ്യാഴം", "വെള്ളി", "ശനി")

            sentence1 = context.resources.getString(R.string.sentence1_malayalam) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_malayalam) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence6_malayalam) + "\n\n"
            sentence4 =
                context.resources.getString(R.string.shg_cutoff_sentence2_malayalam) + "\n\n"
            sentence5 =
                context.resources.getString(R.string.shg_cutoff_sentence4_malayalam) + "\n\n"
            sentence6 =
                context.resources.getString(R.string.shg_cutoff_sentence7_malayalam) + "\n\n"
            sentence7 =
                context.resources.getString(R.string.shg_cutoff_sentence9_malayalam) + "\n\n"
            sentence8 =
                context.resources.getString(R.string.shg_cutoff_sentence10_malayalam) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence14_malayalam) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence19_malayalam) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence20_malayalam) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_malayalam) + "\n\n"

            for (i in 0 until list.size) {

                if (!list.get(i).savComp!!.toString()
                        .equals("0") || !list.get(i).savVol!!.toString().equals("0")
                ) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence1_malayalam)
                    a = a.replace("SHG_MEM", list.get(i).childCboName!!)
                    var amount = list.get(i).savComp!! + list.get(i).savVol!!
                    a = a.replace("SHG_AMOUNT", amount.toString())
                    AmountSaveListString =
                        AmountSaveListString + AmountSavecount.toString() + ". " + a + "\n\n"

                    AmountSavecount++
                }
            }

            for (i in 0 until loan_close_list.size) {
                var amount =
                    (context as VOCutOffMeetingTTSActivity).getTotalClosedLoanAmount(
                        list.get(i).memId,
                        list.get(i).mtgNo
                    )
                if (amount != null) {
                    if (amount.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence3_malayalam)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_close_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace("SHG_AMOUNT", "'" + amount.toString() + "'")

                        loan_close_listString =
                            loan_close_listString + loan_closecount.toString() + ". " + a + "\n\n"

                        loan_closecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amtDisbursed != null) {
                    if (loan_active_list.get(i).amtDisbursed!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence5_malayalam)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_active_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + loan_active_list.get(i).amtDisbursed!!.toString() + "'"
                        )
                        loan_active_listString =
                            loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                        loan_activecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amtDisbursed == null || loan_active_list.get(i).amtDisbursed!!.toInt() == 0) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence6_malayalam)
                    a = a.replace(
                        "SHG_MEM",
                        "'" + loan_active_list.get(i).childCboName!!.toLowerCase() + "'"
                    )

                    loan_active_listString =
                        loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                    loan_activecount++
                }
            }

            for (i in 0 until shareCapital_list.size) {
                if (shareCapital_list.get(i).amount != null) {
                    if (shareCapital_list.get(i).amount!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence8_malayalam)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + shareCapital_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + shareCapital_list.get(i).amount!!.toString() + "'"
                        )
                        shareCapital_listString =
                            shareCapital_listString + loan_shareCapitalcount.toString() + ". " + a + "\n\n"
                        loan_shareCapitalcount++
                    }
                }
            }
        } else if (lang.equals("mr")) {
            days = arrayOf("रविवार", "सोमवार", "मंगलवार", "बुधवार", "गुरुवार", "शुक्रवार", "शनिवार")

            sentence1 = context.resources.getString(R.string.sentence1_marathi) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_marathi) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence6_marathi) + "\n\n"
            sentence4 = context.resources.getString(R.string.shg_cutoff_sentence2_marathi) + "\n\n"
            sentence5 = context.resources.getString(R.string.shg_cutoff_sentence4_marathi) + "\n\n"
            sentence6 = context.resources.getString(R.string.shg_cutoff_sentence7_marathi) + "\n\n"
            sentence7 = context.resources.getString(R.string.shg_cutoff_sentence9_marathi) + "\n\n"
            sentence8 = context.resources.getString(R.string.shg_cutoff_sentence10_marathi) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence14_marathi) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence19_marathi) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence20_marathi) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_marathi) + "\n\n"

            for (i in 0 until list.size) {

                if (!list.get(i).savComp!!.toString()
                        .equals("0") || !list.get(i).savVol!!.toString().equals("0")
                ) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence1_marathi)
                    a = a.replace("SHG_MEM", list.get(i).childCboName!!)
                    var amount = list.get(i).savComp!! + list.get(i).savVol!!
                    a = a.replace("SHG_AMOUNT", amount.toString())
                    AmountSaveListString =
                        AmountSaveListString + AmountSavecount.toString() + ". " + a + "\n\n"

                    AmountSavecount++
                }
            }

            for (i in 0 until loan_close_list.size) {
                var amount =
                    (context as VOCutOffMeetingTTSActivity).getTotalClosedLoanAmount(
                        list.get(i).memId,
                        list.get(i).mtgNo
                    )
                if (amount != null) {
                    if (amount.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence3_marathi)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_close_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace("SHG_AMOUNT", "'" + amount.toString() + "'")

                        loan_close_listString =
                            loan_close_listString + loan_closecount.toString() + ". " + a + "\n\n"

                        loan_closecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amtDisbursed != null) {
                    if (loan_active_list.get(i).amtDisbursed!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence5_marathi)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_active_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + loan_active_list.get(i).amtDisbursed!!.toString() + "'"
                        )
                        loan_active_listString =
                            loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                        loan_activecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amtDisbursed == null || loan_active_list.get(i).amtDisbursed!!.toInt() == 0) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence6_marathi)
                    a = a.replace(
                        "SHG_MEM",
                        "'" + loan_active_list.get(i).childCboName!!.toLowerCase() + "'"
                    )

                    loan_active_listString =
                        loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                    loan_activecount++
                }
            }

            for (i in 0 until shareCapital_list.size) {
                if (shareCapital_list.get(i).amount != null) {
                    if (shareCapital_list.get(i).amount!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence8_marathi)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + shareCapital_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + shareCapital_list.get(i).amount!!.toString() + "'"
                        )
                        shareCapital_listString =
                            shareCapital_listString + loan_shareCapitalcount.toString() + ". " + a + "\n\n"
                        loan_shareCapitalcount++
                    }
                }
            }
        } else if (lang.equals("or")) {
            days =
                arrayOf("ରବିବାର", "ସୋମବାର", "ମଙ୍ଗଳବାର", "ବୁଧବାର", "ଗୁରୁବାର", "ଶୁକ୍ରବାର", "ଶନିବାର")

            sentence1 = context.resources.getString(R.string.sentence1_oriya) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_oriya) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence6_oriya) + "\n\n"
            sentence4 = context.resources.getString(R.string.shg_cutoff_sentence2_oriya) + "\n\n"
            sentence5 = context.resources.getString(R.string.shg_cutoff_sentence4_oriya) + "\n\n"
            sentence6 = context.resources.getString(R.string.shg_cutoff_sentence7_oriya) + "\n\n"
            sentence7 = context.resources.getString(R.string.shg_cutoff_sentence9_oriya) + "\n\n"
            sentence8 = context.resources.getString(R.string.shg_cutoff_sentence10_oriya) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence14_oriya) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence19_oriya) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence20_oriya) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_oriya) + "\n\n"

            for (i in 0 until list.size) {

                if (!list.get(i).savComp!!.toString()
                        .equals("0") || !list.get(i).savVol!!.toString().equals("0")
                ) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence1_oriya)
                    a = a.replace("SHG_MEM", list.get(i).childCboName!!)
                    var amount = list.get(i).savComp!! + list.get(i).savVol!!
                    a = a.replace("SHG_AMOUNT", amount.toString())
                    AmountSaveListString =
                        AmountSaveListString + AmountSavecount.toString() + ". " + a + "\n\n"

                    AmountSavecount++
                }
            }

            for (i in 0 until loan_close_list.size) {
                var amount =
                    (context as VOCutOffMeetingTTSActivity).getTotalClosedLoanAmount(
                        list.get(i).memId,
                        list.get(i).mtgNo
                    )
                if (amount != null) {
                    if (amount.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence3_oriya)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_close_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace("SHG_AMOUNT", "'" + amount.toString() + "'")

                        loan_close_listString =
                            loan_close_listString + loan_closecount.toString() + ". " + a + "\n\n"

                        loan_closecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amtDisbursed != null) {
                    if (loan_active_list.get(i).amtDisbursed!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence5_oriya)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_active_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + loan_active_list.get(i).amtDisbursed!!.toString() + "'"
                        )
                        loan_active_listString =
                            loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                        loan_activecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amtDisbursed == null || loan_active_list.get(i).amtDisbursed!!.toInt() == 0) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence6_oriya)
                    a = a.replace(
                        "SHG_MEM",
                        "'" + loan_active_list.get(i).childCboName!!.toLowerCase() + "'"
                    )

                    loan_active_listString =
                        loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                    loan_activecount++
                }
            }

            for (i in 0 until shareCapital_list.size) {
                if (shareCapital_list.get(i).amount != null) {
                    if (shareCapital_list.get(i).amount!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence8_oriya)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + shareCapital_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + shareCapital_list.get(i).amount!!.toString() + "'"
                        )
                        shareCapital_listString =
                            shareCapital_listString + loan_shareCapitalcount.toString() + ". " + a + "\n\n"
                        loan_shareCapitalcount++
                    }
                }
            }
        } else if (lang.equals("pa")) {
            days =
                arrayOf("ਐਤਵਾਰ", "ਸੋਮਵਾਰ", "ਮੰਗਲਵਾਰ", "ਬੁੱਧਵਾਰ", "ਵੀਰਵਾਰ", "ਸ਼ੁੱਕਰਵਾਰ", "ਸ਼ਨੀਵਾਰ")

            sentence1 = context.resources.getString(R.string.sentence1_punjab) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_punjab) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence6_punjab) + "\n\n"
            sentence4 = context.resources.getString(R.string.shg_cutoff_sentence2_punjab) + "\n\n"
            sentence5 = context.resources.getString(R.string.shg_cutoff_sentence4_punjab) + "\n\n"
            sentence6 = context.resources.getString(R.string.shg_cutoff_sentence7_punjab) + "\n\n"
            sentence7 = context.resources.getString(R.string.shg_cutoff_sentence9_punjab) + "\n\n"
            sentence8 = context.resources.getString(R.string.shg_cutoff_sentence10_punjab) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence14_punjab) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence19_punjab) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence20_punjab) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_punjab) + "\n\n"

            for (i in 0 until list.size) {

                if (!list.get(i).savComp!!.toString()
                        .equals("0") || !list.get(i).savVol!!.toString().equals("0")
                ) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence1_punjab)
                    a = a.replace("SHG_MEM", list.get(i).childCboName!!)
                    var amount = list.get(i).savComp!! + list.get(i).savVol!!
                    a = a.replace("SHG_AMOUNT", amount.toString())
                    AmountSaveListString =
                        AmountSaveListString + AmountSavecount.toString() + ". " + a + "\n\n"

                    AmountSavecount++
                }
            }

            for (i in 0 until loan_close_list.size) {
                var amount =
                    (context as VOCutOffMeetingTTSActivity).getTotalClosedLoanAmount(
                        list.get(i).memId,
                        list.get(i).mtgNo
                    )
                if (amount != null) {
                    if (amount.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence3_punjab)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_close_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace("SHG_AMOUNT", "'" + amount.toString() + "'")

                        loan_close_listString =
                            loan_close_listString + loan_closecount.toString() + ". " + a + "\n\n"

                        loan_closecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amtDisbursed != null) {
                    if (loan_active_list.get(i).amtDisbursed!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence5_punjab)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_active_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + loan_active_list.get(i).amtDisbursed!!.toString() + "'"
                        )
                        loan_active_listString =
                            loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                        loan_activecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amtDisbursed == null || loan_active_list.get(i).amtDisbursed!!.toInt() == 0) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence6_punjab)
                    a = a.replace(
                        "SHG_MEM",
                        "'" + loan_active_list.get(i).childCboName!!.toLowerCase() + "'"
                    )

                    loan_active_listString =
                        loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                    loan_activecount++
                }
            }

            for (i in 0 until shareCapital_list.size) {
                if (shareCapital_list.get(i).amount != null) {
                    if (shareCapital_list.get(i).amount!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence8_punjab)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + shareCapital_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + shareCapital_list.get(i).amount!!.toString() + "'"
                        )
                        shareCapital_listString =
                            shareCapital_listString + loan_shareCapitalcount.toString() + ". " + a + "\n\n"
                        loan_shareCapitalcount++
                    }
                }
            }
        } else if (lang.equals("ta")) {
            days =
                arrayOf("ஞாயிறு", "திங்கள்", "செவ்வாய்", "புதன்", "வியாழன்", "வெள்ளி", "சனிக்கிழமை")

            sentence1 = context.resources.getString(R.string.sentence1_tamil) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_tamil) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence6_tamil) + "\n\n"
            sentence4 = context.resources.getString(R.string.shg_cutoff_sentence2_tamil) + "\n\n"
            sentence5 = context.resources.getString(R.string.shg_cutoff_sentence4_tamil) + "\n\n"
            sentence6 = context.resources.getString(R.string.shg_cutoff_sentence7_tamil) + "\n\n"
            sentence7 = context.resources.getString(R.string.shg_cutoff_sentence9_tamil) + "\n\n"
            sentence8 = context.resources.getString(R.string.shg_cutoff_sentence10_tamil) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence14_tamil) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence19_tamil) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence20_tamil) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_tamil) + "\n\n"

            for (i in 0 until list.size) {

                if (!list.get(i).savComp!!.toString()
                        .equals("0") || !list.get(i).savVol!!.toString().equals("0")
                ) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence1_tamil)
                    a = a.replace("SHG_MEM", list.get(i).childCboName!!)
                    var amount = list.get(i).savComp!! + list.get(i).savVol!!
                    a = a.replace("SHG_AMOUNT", amount.toString())
                    AmountSaveListString =
                        AmountSaveListString + AmountSavecount.toString() + ". " + a + "\n\n"

                    AmountSavecount++
                }
            }

            for (i in 0 until loan_close_list.size) {
                var amount =
                    (context as VOCutOffMeetingTTSActivity).getTotalClosedLoanAmount(
                        list.get(i).memId,
                        list.get(i).mtgNo
                    )
                if (amount != null) {
                    if (amount.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence3_tamil)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_close_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace("SHG_AMOUNT", "'" + amount.toString() + "'")

                        loan_close_listString =
                            loan_close_listString + loan_closecount.toString() + ". " + a + "\n\n"

                        loan_closecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amtDisbursed != null) {
                    if (loan_active_list.get(i).amtDisbursed!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence5_tamil)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_active_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + loan_active_list.get(i).childCboName!!.toString() + "'"
                        )
                        loan_active_listString =
                            loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                        loan_activecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amtDisbursed == null || loan_active_list.get(i).amtDisbursed!!.toInt() == 0) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence6_tamil)
                    a = a.replace(
                        "SHG_MEM",
                        "'" + loan_active_list.get(i).childCboName!!.toLowerCase() + "'"
                    )

                    loan_active_listString =
                        loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                    loan_activecount++
                }
            }

            for (i in 0 until shareCapital_list.size) {
                if (shareCapital_list.get(i).amount != null) {
                    if (shareCapital_list.get(i).amount!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence8_tamil)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + shareCapital_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + shareCapital_list.get(i).amount!!.toString() + "'"
                        )
                        shareCapital_listString =
                            shareCapital_listString + loan_shareCapitalcount.toString() + ". " + a + "\n\n"
                        loan_shareCapitalcount++
                    }
                }
            }
        } else if (lang.equals("te")) {
            days =
                arrayOf(
                    "ఆదివారం",
                    "సోమవారం",
                    "మంగళవారం",
                    "బుధవారం",
                    "గురువారం",
                    "శుక్రవారం",
                    "శనివారం"
                )
            sentence1 = context.resources.getString(R.string.sentence1_telgu) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_telgu) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence6_telgu) + "\n\n"
            sentence4 = context.resources.getString(R.string.shg_cutoff_sentence2_telgu) + "\n\n"
            sentence5 = context.resources.getString(R.string.shg_cutoff_sentence4_telgu) + "\n\n"
            sentence6 = context.resources.getString(R.string.shg_cutoff_sentence7_telgu) + "\n\n"
            sentence7 = context.resources.getString(R.string.shg_cutoff_sentence9_telgu) + "\n\n"
            sentence8 = context.resources.getString(R.string.shg_cutoff_sentence10_telgu) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence14_telgu) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence19_telgu) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence20_telgu) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_telgu) + "\n\n"

            for (i in 0 until list.size) {

                if (!list.get(i).savComp!!.toString()
                        .equals("0") || !list.get(i).savVol!!.toString().equals("0")
                ) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence1_telgu)
                    a = a.replace("SHG_MEM", list.get(i).childCboName!!)
                    var amount = list.get(i).savComp!! + list.get(i).savVol!!
                    a = a.replace("SHG_AMOUNT", amount.toString())
                    AmountSaveListString =
                        AmountSaveListString + AmountSavecount.toString() + ". " + a + "\n\n"

                    AmountSavecount++
                }
            }

            for (i in 0 until loan_close_list.size) {
                var amount =
                    (context as VOCutOffMeetingTTSActivity).getTotalClosedLoanAmount(
                        list.get(i).memId,
                        list.get(i).mtgNo
                    )
                if (amount != null) {
                    if (amount.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence3_telgu)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_close_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace("SHG_AMOUNT", "'" + amount.toString() + "'")

                        loan_close_listString =
                            loan_close_listString + loan_closecount.toString() + ". " + a + "\n\n"

                        loan_closecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amtDisbursed != null) {
                    if (loan_active_list.get(i).amtDisbursed!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence5_telgu)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_active_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + loan_active_list.get(i).amtDisbursed!!.toString() + "'"
                        )
                        loan_active_listString =
                            loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                        loan_activecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amtDisbursed == null || loan_active_list.get(i).amtDisbursed!!.toInt() == 0) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence6_telgu)
                    a = a.replace(
                        "SHG_MEM",
                        "'" + loan_active_list.get(i).childCboName!!.toLowerCase() + "'"
                    )

                    loan_active_listString =
                        loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                    loan_activecount++
                }
            }

            for (i in 0 until shareCapital_list.size) {
                if (shareCapital_list.get(i).amount != null) {
                    if (shareCapital_list.get(i).amount!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence8_telgu)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + shareCapital_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + shareCapital_list.get(i).amount!!.toString() + "'"
                        )
                        shareCapital_listString =
                            shareCapital_listString + loan_shareCapitalcount.toString() + ". " + a + "\n\n"
                        loan_shareCapitalcount++
                    }
                }
            }
        } else if (lang.equals("ur")) {
            days = arrayOf("اتوار", "پیر", "منگل", "بدھ", "جمعرات", "جمعہ", "ہفتہ")

            sentence1 = context.resources.getString(R.string.sentence1_urdu) + "\n\n"
            sentence2 = context.resources.getString(R.string.sentence2_urdu) + "\n\n"
            sentence3 = context.resources.getString(R.string.sentence6_urdu) + "\n\n"
            sentence4 = context.resources.getString(R.string.shg_cutoff_sentence2_urdu) + "\n\n"
            sentence5 = context.resources.getString(R.string.shg_cutoff_sentence4_urdu) + "\n\n"
            sentence6 = context.resources.getString(R.string.shg_cutoff_sentence7_urdu) + "\n\n"
            sentence7 = context.resources.getString(R.string.shg_cutoff_sentence9_urdu) + "\n\n"
            sentence8 = context.resources.getString(R.string.shg_cutoff_sentence10_urdu) + "\n\n"
            sentence9 = context.resources.getString(R.string.sentence14_urdu) + "\n\n"
            sentence10 = context.resources.getString(R.string.sentence19_urdu) + "\n\n"
            sentence11 = context.resources.getString(R.string.sentence20_urdu) + "\n\n"
            sentence21 = context.resources.getString(R.string.sentence21_urdu) + "\n\n"

            for (i in 0 until list.size) {

                if (!list.get(i).savComp!!.toString()
                        .equals("0") || !list.get(i).savVol!!.toString().equals("0")
                ) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence1_urdu)
                    a = a.replace("SHG_MEM", list.get(i).childCboName!!)
                    var amount = list.get(i).savComp!! + list.get(i).savVol!!
                    a = a.replace("SHG_AMOUNT", amount.toString())
                    AmountSaveListString =
                        AmountSaveListString + AmountSavecount.toString() + ". " + a + "\n\n"

                    AmountSavecount++
                }
            }

            for (i in 0 until loan_close_list.size) {
                var amount =
                    (context as VOCutOffMeetingTTSActivity).getTotalClosedLoanAmount(
                        list.get(i).memId,
                        list.get(i).mtgNo
                    )
                if (amount != null) {
                    if (amount.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence3_urdu)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_close_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace("SHG_AMOUNT", "'" + amount.toString() + "'")

                        loan_close_listString =
                            loan_close_listString + loan_closecount.toString() + ". " + a + "\n\n"

                        loan_closecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amtDisbursed != null) {
                    if (loan_active_list.get(i).amtDisbursed!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence5_urdu)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + loan_active_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + loan_active_list.get(i).amtDisbursed!!.toString() + "'"
                        )
                        loan_active_listString =
                            loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                        loan_activecount++
                    }
                }
            }

            for (i in 0 until loan_active_list.size) {
                if (loan_active_list.get(i).amtDisbursed == null || loan_active_list.get(i).amtDisbursed!!.toInt() == 0) {
                    var a = context.resources.getString(R.string.shg_cutoff_sentence6_urdu)
                    a = a.replace(
                        "SHG_MEM",
                        "'" + loan_active_list.get(i).childCboName!!.toLowerCase() + "'"
                    )

                    loan_active_listString =
                        loan_active_listString + loan_activecount.toString() + ". " + a + "\n\n"
                    loan_activecount++
                }
            }

            for (i in 0 until shareCapital_list.size) {
                if (shareCapital_list.get(i).amount != null) {
                    if (shareCapital_list.get(i).amount!!.toInt() > 0) {
                        var a = context.resources.getString(R.string.shg_cutoff_sentence8_urdu)
                        a = a.replace(
                            "SHG_MEM",
                            "'" + shareCapital_list.get(i).childCboName!!.toLowerCase() + "'"
                        )
                        a = a.replace(
                            "SHG_AMOUNT",
                            "'" + shareCapital_list.get(i).amount!!.toString() + "'"
                        )
                        shareCapital_listString =
                            shareCapital_listString + loan_shareCapitalcount.toString() + ". " + a + "\n\n"
                        loan_shareCapitalcount++
                    }
                }
            }
        }


        var date = Date()
        val date_format = SimpleDateFormat("dd-MM-yyyy")
        try {
            date = date_format.parse(MeetingDate)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        val c = Calendar.getInstance()
        c.time = date
        var dayIndex: Int = c.get(Calendar.DAY_OF_WEEK)


        var dayData = days[dayIndex - 1]

        sentence2 = sentence2.replace("SHG_Name", "'" + SHG_name + "'")
        sentence2 = sentence2.replace("SHG_DATE", MeetingDate)
        sentence2 = sentence2.replace("SHG_DAY", dayData)
        sentence2 = sentence2.replace("MEET_NO", meetNo.toString())

        sentence3 = sentence3.replace("SHG_AMOUNT", "'" + savingAmount.toString() + "'")
        sentence4 = sentence4.replace("SHG_AMOUNT", "'" + closedLoan_amount.toString() + "'")
        sentence5 = sentence5.replace("SHG_AMOUNT", "'" + activeLoan_amount.toString() + "'")
        sentence6 = sentence6.replace("SHG_AMOUNT", "'" + share_capital_amount.toString() + "'")
        sentence7 = sentence7.replace("SHG_AMOUNT", "'" + investAmount.toString() + "'")
        sentence8 = sentence8.replace("SHG_AMOUNT", "'" + borrowAmount.toString() + "'")

        sentence10 = sentence10.replace("SHG_AMOUNT", "= '" + bankBalance.toString() + "= '")
        sentence11 = sentence11.replace("SHG_AMOUNT", "= '" + cashBalance.toString() + "= '")

        sentence3 = sentence3.replace("₹", "")
        sentence4 = sentence4.replace("₹", "")
        sentence5 = sentence5.replace("₹", "")
        sentence6 = sentence6.replace("₹", "")
        sentence7 = sentence7.replace("₹", "")
        sentence8 = sentence8.replace("₹", "")
        sentence10 = sentence10.replace("₹", "")
        sentence11 = sentence11.replace("₹", "")



        if (AmountSaveListString.isNullOrEmpty()) {
        } else {
            AmountSaveListString = AmountSaveListString + "\n"
        }
        if (loan_close_listString.isNullOrEmpty()) {
        } else {
            loan_close_listString = loan_close_listString + "\n"
        }
        if (loan_active_listString.isNullOrEmpty()) {
        } else {
            loan_active_listString = loan_active_listString + "\n"
        }
        if (shareCapital_listString.isNullOrEmpty()) {
        } else {
            shareCapital_listString = shareCapital_listString + "\n"
        }

        finalParagraph =
            sentence1 + "@#@" + sentence2 + "@#@" + sentence3 + "@#@" + AmountSaveListString + "@#@" + sentence4 + "@#@" + loan_close_listString + "@#@" +
                    sentence5 + "@#@" + loan_active_listString + "@#@" + sentence6 + "@#@" + shareCapital_listString + "@#@" + sentence7 + "@#@" + sentence8 + "@#@" +
                    sentence9 + "@#@" + sentence10 + "@#@" + sentence11 + "@#@" + sentence21


        return finalParagraph
    }


    fun GetMemberData(
        meetNo: Int,
        MeetingDate: String,
        lang: String,
        context: Context,
        memberData: DtmtgDetEntity,
        loan_distribute_list: List<LoanListModel>,
        loan_repayment_list: List<LoanRepaymentListModel>
    ): String {
        var finaldata = ""
        var savingAmount = ""
        var distributedAmount = ""
        var repaymentAmount = ""
        var attendance_sentence = ""
        var saving_sentence = ""
        var distributedAmount_sentence = ""
        var repaymentAmount_sentence = ""


        savingAmount = memberData.sav_comp!!.toString()

        for (i in 0 until loan_distribute_list.size) {
            if (memberData.mem_id.toString()
                    .equals(loan_distribute_list.get(i).mem_id.toString())
            ) {
                distributedAmount = loan_distribute_list.get(i).amt_disbursed.toString()
            }
        }

        for (i in 0 until loan_repayment_list.size) {
            if (memberData.mem_id.toString().equals(loan_repayment_list.get(i).mem_id.toString())) {
                repaymentAmount = loan_repayment_list.get(i).loan_paid.toString()
            }
        }


        if (lang == "hi") {
            if (memberData.attendance == null || memberData.attendance.equals("2") || memberData.attendance.equals(
                    ""
                )
            ) {
                attendance_sentence =
                    context.resources.getString(R.string.was_absent_for_meeting_no_on_date_hin) + "\n\n"

            } else if (memberData.attendance.equals("1")) {
                attendance_sentence =
                    context.resources.getString(R.string.was_present_for_meeting_no_on_date_hin) + "\n\n"
            }

            if (savingAmount.equals("null") || savingAmount.equals("0")) {
                var a = context.resources.getString(R.string.she_didnot_save_amount_hin)
                a = a.replace("SHG_MEM", "")
                saving_sentence = a + "\n\n"
            } else {
                var a = context.resources.getString(R.string.she_was_save_amount_hin)
                a = a.replace("SHG_MEM", memberData.member_name!!.toLowerCase())
                a = a.replace("SHG_AMOUNT", savingAmount)
                saving_sentence = a + "\n\n"
            }

            if (distributedAmount.equals("null") || distributedAmount.toString().equals("0")) {
                var a = context.resources.getString(R.string.loan_not_distributed_hin)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                distributedAmount_sentence = a + "\n\n"
            } else if (distributedAmount.toLong() > 0) {
                var a = context.resources.getString(R.string.loan_distributed_to_her_hin)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                a = a.replace("SHG_LOAN_AMOUNT", "'" + distributedAmount.toInt() + "'")
                distributedAmount_sentence = a + "\n\n"
            }

            if (repaymentAmount.equals("null") || repaymentAmount.toString().equals("0")) {
                var a = context.resources.getString(R.string.didnt_have_repayment_hin)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                repaymentAmount_sentence = a + "\n\n"
            } else if (repaymentAmount.toLong() > 0) {
                var a = context.resources.getString(R.string.loan_repayment_was_hin)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                a = a.replace("SHG_LOAN_AMOUNT", "'" + repaymentAmount.toInt() + "'")
                repaymentAmount_sentence = a + "\n\n"
            }
        } else if (lang == "en") {
            if (memberData.attendance == null || memberData.attendance.equals("2") || memberData.attendance.equals(
                    ""
                )
            ) {
                attendance_sentence =
                    context.resources.getString(R.string.was_absent_for_meeting_no_on_date) + "\n\n"

            } else if (memberData.attendance.equals("1")) {
                attendance_sentence =
                    context.resources.getString(R.string.was_present_for_meeting_no_on_date) + "\n\n"
            }

            if (savingAmount.equals("null") || savingAmount.equals("0")) {
                var a = context.resources.getString(R.string.she_didnot_save_amount)
                a = a.replace("SHG_MEM", "")
                saving_sentence = a + "\n\n"
            } else {
                var a = context.resources.getString(R.string.she_was_save_amount)
                a = a.replace("SHG_MEM", memberData.member_name!!.toLowerCase())
                a = a.replace("SHG_AMOUNT", savingAmount)
                saving_sentence = a + "\n\n"
            }

            if (distributedAmount.equals("null") || distributedAmount.toString().equals("0")) {
                var a = context.resources.getString(R.string.loan_not_distributed)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                distributedAmount_sentence = a + "\n\n"
            } else if (distributedAmount.toLong() > 0) {
                var a = context.resources.getString(R.string.loan_distributed_to_her)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                a = a.replace("SHG_LOAN_AMOUNT", "'" + distributedAmount.toInt() + "'")
                distributedAmount_sentence = a + "\n\n"
            }

            if (repaymentAmount.equals("null") || repaymentAmount.toString().equals("0")) {
                var a = context.resources.getString(R.string.didnt_have_repayment)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                repaymentAmount_sentence = a + "\n\n"
            } else if (repaymentAmount.toLong() > 0) {
                var a = context.resources.getString(R.string.loan_repayment_was)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                a = a.replace("SHG_LOAN_AMOUNT", "'" + repaymentAmount.toInt() + "'")
                repaymentAmount_sentence = a + "\n\n"
            }
        } else if (lang == "as") {
            if (memberData.attendance == null || memberData.attendance.equals("2") || memberData.attendance.equals(
                    ""
                )
            ) {
                attendance_sentence =
                    context.resources.getString(R.string.was_absent_for_meeting_no_on_date_ass) + "\n\n"

            } else if (memberData.attendance.equals("1")) {
                attendance_sentence =
                    context.resources.getString(R.string.was_present_for_meeting_no_on_date_ass) + "\n\n"
            }

            if (savingAmount.equals("null") || savingAmount.equals("0")) {
                var a = context.resources.getString(R.string.she_didnot_save_amount_ass)
                a = a.replace("SHG_MEM", "")
                saving_sentence = a + "\n\n"
            } else {
                var a = context.resources.getString(R.string.she_was_save_amount_ass)
                a = a.replace("SHG_MEM", memberData.member_name!!.toLowerCase())
                a = a.replace("SHG_AMOUNT", savingAmount)
                saving_sentence = a + "\n\n"
            }

            if (distributedAmount.equals("null") || distributedAmount.toString().equals("0")) {
                var a = context.resources.getString(R.string.loan_not_distributed_ass)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                distributedAmount_sentence = a + "\n\n"
            } else if (distributedAmount.toLong() > 0) {
                var a = context.resources.getString(R.string.loan_distributed_to_her_ass)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                a = a.replace("SHG_LOAN_AMOUNT", "'" + distributedAmount.toInt() + "'")
                distributedAmount_sentence = a + "\n\n"
            }

            if (repaymentAmount.equals("null") || repaymentAmount.toString().equals("0")) {
                var a = context.resources.getString(R.string.didnt_have_repayment_ass)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                repaymentAmount_sentence = a + "\n\n"
            } else if (repaymentAmount.toLong() > 0) {
                var a = context.resources.getString(R.string.loan_repayment_was_ass)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                a = a.replace("SHG_LOAN_AMOUNT", "'" + repaymentAmount.toInt() + "'")
                repaymentAmount_sentence = a + "\n\n"
            }
        } else if (lang == "bn") {
            if (memberData.attendance == null || memberData.attendance.equals("2") || memberData.attendance.equals(
                    ""
                )
            ) {
                attendance_sentence =
                    context.resources.getString(R.string.was_absent_for_meeting_no_on_date_bangla) + "\n\n"

            } else if (memberData.attendance.equals("1")) {
                attendance_sentence =
                    context.resources.getString(R.string.was_present_for_meeting_no_on_date_bangla) + "\n\n"
            }

            if (savingAmount.equals("null") || savingAmount.equals("0")) {
                var a = context.resources.getString(R.string.she_didnot_save_amount_bangla)
                a = a.replace("SHG_MEM", "")
                saving_sentence = a + "\n\n"
            } else {
                var a = context.resources.getString(R.string.she_was_save_amount_bangla)
                a = a.replace("SHG_MEM", memberData.member_name!!.toLowerCase())
                a = a.replace("SHG_AMOUNT", savingAmount)
                saving_sentence = a + "\n\n"
            }

            if (distributedAmount.equals("null") || distributedAmount.toString().equals("0")) {
                var a = context.resources.getString(R.string.loan_not_distributed_bangla)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                distributedAmount_sentence = a + "\n\n"
            } else if (distributedAmount.toLong() > 0) {
                var a = context.resources.getString(R.string.loan_distributed_to_her_bangla)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                a = a.replace("SHG_LOAN_AMOUNT", "'" + distributedAmount.toInt() + "'")
                distributedAmount_sentence = a + "\n\n"
            }

            if (repaymentAmount.equals("null") || repaymentAmount.toString().equals("0")) {
                var a = context.resources.getString(R.string.didnt_have_repayment_bangla)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                repaymentAmount_sentence = a + "\n\n"
            } else if (repaymentAmount.toLong() > 0) {
                var a = context.resources.getString(R.string.loan_repayment_was_bangla)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                a = a.replace("SHG_LOAN_AMOUNT", "'" + repaymentAmount.toInt() + "'")
                repaymentAmount_sentence = a + "\n\n"
            }
        } else if (lang == "gu") {
            if (memberData.attendance == null || memberData.attendance.equals("2") || memberData.attendance.equals(
                    ""
                )
            ) {
                attendance_sentence =
                    context.resources.getString(R.string.was_absent_for_meeting_no_on_date_gujarati) + "\n\n"

            } else if (memberData.attendance.equals("1")) {
                attendance_sentence =
                    context.resources.getString(R.string.was_present_for_meeting_no_on_date_gujarati) + "\n\n"
            }

            if (savingAmount.equals("null") || savingAmount.equals("0")) {
                var a = context.resources.getString(R.string.she_didnot_save_amount_gujarati)
                a = a.replace("SHG_MEM", "")
                saving_sentence = a + "\n\n"
            } else {
                var a = context.resources.getString(R.string.she_was_save_amount_gujarati)
                a = a.replace("SHG_MEM", memberData.member_name!!.toLowerCase())
                a = a.replace("SHG_AMOUNT", savingAmount)
                saving_sentence = a + "\n\n"
            }

            if (distributedAmount.equals("null") || distributedAmount.toString().equals("0")) {
                var a = context.resources.getString(R.string.loan_not_distributed_gujarati)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                distributedAmount_sentence = a + "\n\n"
            } else if (distributedAmount.toLong() > 0) {
                var a = context.resources.getString(R.string.loan_distributed_to_her_gujarati)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                a = a.replace("SHG_LOAN_AMOUNT", "'" + distributedAmount.toInt() + "'")
                distributedAmount_sentence = a + "\n\n"
            }

            if (repaymentAmount.equals("null") || repaymentAmount.toString().equals("0")) {
                var a = context.resources.getString(R.string.didnt_have_repayment_gujarati)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                repaymentAmount_sentence = a + "\n\n"
            } else if (repaymentAmount.toLong() > 0) {
                var a = context.resources.getString(R.string.loan_repayment_was_gujarati)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                a = a.replace("SHG_LOAN_AMOUNT", "'" + repaymentAmount.toInt() + "'")
                repaymentAmount_sentence = a + "\n\n"
            }
        } else if (lang == "kn") {
            if (memberData.attendance == null || memberData.attendance.equals("2") || memberData.attendance.equals(
                    ""
                )
            ) {
                attendance_sentence =
                    context.resources.getString(R.string.was_absent_for_meeting_no_on_date_Kannad) + "\n\n"

            } else if (memberData.attendance.equals("1")) {
                attendance_sentence =
                    context.resources.getString(R.string.was_present_for_meeting_no_on_date_Kannad) + "\n\n"
            }

            if (savingAmount.equals("null") || savingAmount.equals("0")) {
                var a = context.resources.getString(R.string.she_didnot_save_amount_Kannad)
                a = a.replace("SHG_MEM", "")
                saving_sentence = a + "\n\n"
            } else {
                var a = context.resources.getString(R.string.she_was_save_amount_Kannad)
                a = a.replace("SHG_MEM", memberData.member_name!!.toLowerCase())
                a = a.replace("SHG_AMOUNT", savingAmount)
                saving_sentence = a + "\n\n"
            }

            if (distributedAmount.equals("null") || distributedAmount.toString().equals("0")) {
                var a = context.resources.getString(R.string.loan_not_distributed_Kannad)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                distributedAmount_sentence = a + "\n\n"
            } else if (distributedAmount.toLong() > 0) {
                var a = context.resources.getString(R.string.loan_distributed_to_her_Kannad)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                a = a.replace("SHG_LOAN_AMOUNT", "'" + distributedAmount.toInt() + "'")
                distributedAmount_sentence = a + "\n\n"
            }

            if (repaymentAmount.equals("null") || repaymentAmount.toString().equals("0")) {
                var a = context.resources.getString(R.string.didnt_have_repayment_Kannad)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                repaymentAmount_sentence = a + "\n\n"
            } else if (repaymentAmount.toLong() > 0) {
                var a = context.resources.getString(R.string.loan_repayment_was_Kannad)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                a = a.replace("SHG_LOAN_AMOUNT", "'" + repaymentAmount.toInt() + "'")
                repaymentAmount_sentence = a + "\n\n"
            }
        } else if (lang == "ml") {
            if (memberData.attendance == null || memberData.attendance.equals("2") || memberData.attendance.equals(
                    ""
                )
            ) {
                attendance_sentence =
                    context.resources.getString(R.string.was_absent_for_meeting_no_on_date_malayalam) + "\n\n"

            } else if (memberData.attendance.equals("1")) {
                attendance_sentence =
                    context.resources.getString(R.string.was_present_for_meeting_no_on_date_malayalam) + "\n\n"
            }

            if (savingAmount.equals("null") || savingAmount.equals("0")) {
                var a = context.resources.getString(R.string.she_didnot_save_amount_malayalam)
                a = a.replace("SHG_MEM", "")
                saving_sentence = a + "\n\n"
            } else {
                var a = context.resources.getString(R.string.she_was_save_amount_malayalam)
                a = a.replace("SHG_MEM", memberData.member_name!!.toLowerCase())
                a = a.replace("SHG_AMOUNT", savingAmount)
                saving_sentence = a + "\n\n"
            }

            if (distributedAmount.equals("null") || distributedAmount.toString().equals("0")) {
                var a = context.resources.getString(R.string.loan_not_distributed_malayalam)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                distributedAmount_sentence = a + "\n\n"
            } else if (distributedAmount.toLong() > 0) {
                var a = context.resources.getString(R.string.loan_distributed_to_her_malayalam)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                a = a.replace("SHG_LOAN_AMOUNT", "'" + distributedAmount.toInt() + "'")
                distributedAmount_sentence = a + "\n\n"
            }

            if (repaymentAmount.equals("null") || repaymentAmount.toString().equals("0")) {
                var a = context.resources.getString(R.string.didnt_have_repayment_malayalam)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                repaymentAmount_sentence = a + "\n\n"
            } else if (repaymentAmount.toLong() > 0) {
                var a = context.resources.getString(R.string.loan_repayment_was_malayalam)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                a = a.replace("SHG_LOAN_AMOUNT", "'" + repaymentAmount.toInt() + "'")
                repaymentAmount_sentence = a + "\n\n"
            }
        } else if (lang == "mr") {
            if (memberData.attendance == null || memberData.attendance.equals("2") || memberData.attendance.equals(
                    ""
                )
            ) {
                attendance_sentence =
                    context.resources.getString(R.string.was_absent_for_meeting_no_on_date_marathi) + "\n\n"

            } else if (memberData.attendance.equals("1")) {
                attendance_sentence =
                    context.resources.getString(R.string.was_present_for_meeting_no_on_date_marathi) + "\n\n"
            }

            if (savingAmount.equals("null") || savingAmount.equals("0")) {
                var a = context.resources.getString(R.string.she_didnot_save_amount_marathi)
                a = a.replace("SHG_MEM", "")
                saving_sentence = a + "\n\n"
            } else {
                var a = context.resources.getString(R.string.she_was_save_amount_marathi)
                a = a.replace("SHG_MEM", memberData.member_name!!.toLowerCase())
                a = a.replace("SHG_AMOUNT", savingAmount)
                saving_sentence = a + "\n\n"
            }

            if (distributedAmount.equals("null") || distributedAmount.toString().equals("0")) {
                var a = context.resources.getString(R.string.loan_not_distributed_marathi)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                distributedAmount_sentence = a + "\n\n"
            } else if (distributedAmount.toLong() > 0) {
                var a = context.resources.getString(R.string.loan_distributed_to_her_marathi)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                a = a.replace("SHG_LOAN_AMOUNT", "'" + distributedAmount.toInt() + "'")
                distributedAmount_sentence = a + "\n\n"
            }

            if (repaymentAmount.equals("null") || repaymentAmount.toString().equals("0")) {
                var a = context.resources.getString(R.string.didnt_have_repayment_marathi)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                repaymentAmount_sentence = a + "\n\n"
            } else if (repaymentAmount.toLong() > 0) {
                var a = context.resources.getString(R.string.loan_repayment_was_marathi)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                a = a.replace("SHG_LOAN_AMOUNT", "'" + repaymentAmount.toInt() + "'")
                repaymentAmount_sentence = a + "\n\n"
            }
        } else if (lang == "or") {
            if (memberData.attendance == null || memberData.attendance.equals("2") || memberData.attendance.equals(
                    ""
                )
            ) {
                attendance_sentence =
                    context.resources.getString(R.string.was_absent_for_meeting_no_on_date_oriya) + "\n\n"

            } else if (memberData.attendance.equals("1")) {
                attendance_sentence =
                    context.resources.getString(R.string.was_present_for_meeting_no_on_date_oriya) + "\n\n"
            }

            if (savingAmount.equals("null") || savingAmount.equals("0")) {
                var a = context.resources.getString(R.string.she_didnot_save_amount_oriya)
                a = a.replace("SHG_MEM", "")
                saving_sentence = a + "\n\n"
            } else {
                var a = context.resources.getString(R.string.she_was_save_amount_oriya)
                a = a.replace("SHG_MEM", memberData.member_name!!.toLowerCase())
                a = a.replace("SHG_AMOUNT", savingAmount)
                saving_sentence = a + "\n\n"
            }

            if (distributedAmount.equals("null") || distributedAmount.toString().equals("0")) {
                var a = context.resources.getString(R.string.loan_not_distributed_oriya)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                distributedAmount_sentence = a + "\n\n"
            } else if (distributedAmount.toLong() > 0) {
                var a = context.resources.getString(R.string.loan_distributed_to_her_oriya)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                a = a.replace("SHG_LOAN_AMOUNT", "'" + distributedAmount.toInt() + "'")
                distributedAmount_sentence = a + "\n\n"
            }

            if (repaymentAmount.equals("null") || repaymentAmount.toString().equals("0")) {
                var a = context.resources.getString(R.string.didnt_have_repayment_oriya)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                repaymentAmount_sentence = a + "\n\n"
            } else if (repaymentAmount.toLong() > 0) {
                var a = context.resources.getString(R.string.loan_repayment_was_oriya)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                a = a.replace("SHG_LOAN_AMOUNT", "'" + repaymentAmount.toInt() + "'")
                repaymentAmount_sentence = a + "\n\n"
            }
        } else if (lang == "pa") {
            if (memberData.attendance == null || memberData.attendance.equals("2") || memberData.attendance.equals(
                    ""
                )
            ) {
                attendance_sentence =
                    context.resources.getString(R.string.was_absent_for_meeting_no_on_date_punjab) + "\n\n"

            } else if (memberData.attendance.equals("1")) {
                attendance_sentence =
                    context.resources.getString(R.string.was_present_for_meeting_no_on_date_punjab) + "\n\n"
            }

            if (savingAmount.equals("null") || savingAmount.equals("0")) {
                var a = context.resources.getString(R.string.she_didnot_save_amount_punjab)
                a = a.replace("SHG_MEM", "")
                saving_sentence = a + "\n\n"
            } else {
                var a = context.resources.getString(R.string.she_was_save_amount_punjab)
                a = a.replace("SHG_MEM", memberData.member_name!!.toLowerCase())
                a = a.replace("SHG_AMOUNT", savingAmount)
                saving_sentence = a + "\n\n"
            }

            if (distributedAmount.equals("null") || distributedAmount.toString().equals("0")) {
                var a = context.resources.getString(R.string.loan_not_distributed_punjab)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                distributedAmount_sentence = a + "\n\n"
            } else if (distributedAmount.toLong() > 0) {
                var a = context.resources.getString(R.string.loan_distributed_to_her_punjab)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                a = a.replace("SHG_LOAN_AMOUNT", "'" + distributedAmount.toInt() + "'")
                distributedAmount_sentence = a + "\n\n"
            }

            if (repaymentAmount.equals("null") || repaymentAmount.toString().equals("0")) {
                var a = context.resources.getString(R.string.didnt_have_repayment_punjab)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                repaymentAmount_sentence = a + "\n\n"
            } else if (repaymentAmount.toLong() > 0) {
                var a = context.resources.getString(R.string.loan_repayment_was_punjab)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                a = a.replace("SHG_LOAN_AMOUNT", "'" + repaymentAmount.toInt() + "'")
                repaymentAmount_sentence = a + "\n\n"
            }
        } else if (lang == "ta") {
            if (memberData.attendance == null || memberData.attendance.equals("2") || memberData.attendance.equals(
                    ""
                )
            ) {
                attendance_sentence =
                    context.resources.getString(R.string.was_absent_for_meeting_no_on_date_tamil) + "\n\n"

            } else if (memberData.attendance.equals("1")) {
                attendance_sentence =
                    context.resources.getString(R.string.was_present_for_meeting_no_on_date_tamil) + "\n\n"
            }

            if (savingAmount.equals("null") || savingAmount.equals("0")) {
                var a = context.resources.getString(R.string.she_didnot_save_amount_tamil)
                a = a.replace("SHG_MEM", "")
                saving_sentence = a + "\n\n"
            } else {
                var a = context.resources.getString(R.string.she_was_save_amount_tamil)
                a = a.replace("SHG_MEM", memberData.member_name!!.toLowerCase())
                a = a.replace("SHG_AMOUNT", savingAmount)
                saving_sentence = a + "\n\n"
            }

            if (distributedAmount.equals("null") || distributedAmount.toString().equals("0")) {
                var a = context.resources.getString(R.string.loan_not_distributed_tamil)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                distributedAmount_sentence = a + "\n\n"
            } else if (distributedAmount.toLong() > 0) {
                var a = context.resources.getString(R.string.loan_distributed_to_her_tamil)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                a = a.replace("SHG_LOAN_AMOUNT", "'" + distributedAmount.toInt() + "'")
                distributedAmount_sentence = a + "\n\n"
            }

            if (repaymentAmount.equals("null") || repaymentAmount.toString().equals("0")) {
                var a = context.resources.getString(R.string.didnt_have_repayment_tamil)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                repaymentAmount_sentence = a + "\n\n"
            } else if (repaymentAmount.toLong() > 0) {
                var a = context.resources.getString(R.string.loan_repayment_was_tamil)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                a = a.replace("SHG_LOAN_AMOUNT", "'" + repaymentAmount.toInt() + "'")
                repaymentAmount_sentence = a + "\n\n"
            }
        } else if (lang == "te") {
            if (memberData.attendance == null || memberData.attendance.equals("2") || memberData.attendance.equals(
                    ""
                )
            ) {
                attendance_sentence =
                    context.resources.getString(R.string.was_absent_for_meeting_no_on_date_telgu) + "\n\n"

            } else if (memberData.attendance.equals("1")) {
                attendance_sentence =
                    context.resources.getString(R.string.was_present_for_meeting_no_on_date_telgu) + "\n\n"
            }

            if (savingAmount.equals("null") || savingAmount.equals("0")) {
                var a = context.resources.getString(R.string.she_didnot_save_amount_telgu)
                a = a.replace("SHG_MEM", "")
                saving_sentence = a + "\n\n"
            } else {
                var a = context.resources.getString(R.string.she_was_save_amount_telgu)
                a = a.replace("SHG_MEM", memberData.member_name!!.toLowerCase())
                a = a.replace("SHG_AMOUNT", savingAmount)
                saving_sentence = a + "\n\n"
            }

            if (distributedAmount.equals("null") || distributedAmount.toString().equals("0")) {
                var a = context.resources.getString(R.string.loan_not_distributed_telgu)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                distributedAmount_sentence = a + "\n\n"
            } else if (distributedAmount.toLong() > 0) {
                var a = context.resources.getString(R.string.loan_distributed_to_her_telgu)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                a = a.replace("SHG_LOAN_AMOUNT", "'" + distributedAmount.toInt() + "'")
                distributedAmount_sentence = a + "\n\n"
            }

            if (repaymentAmount.equals("null") || repaymentAmount.toString().equals("0")) {
                var a = context.resources.getString(R.string.didnt_have_repayment_telgu)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                repaymentAmount_sentence = a + "\n\n"
            } else if (repaymentAmount.toLong() > 0) {
                var a = context.resources.getString(R.string.loan_repayment_was_telgu)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                a = a.replace("SHG_LOAN_AMOUNT", "'" + repaymentAmount.toInt() + "'")
                repaymentAmount_sentence = a + "\n\n"
            }
        } else if (lang == "ur") {
            if (memberData.attendance == null || memberData.attendance.equals("2") || memberData.attendance.equals(
                    ""
                )
            ) {
                attendance_sentence =
                    context.resources.getString(R.string.was_absent_for_meeting_no_on_date_urdu) + "\n\n"

            } else if (memberData.attendance.equals("1")) {
                attendance_sentence =
                    context.resources.getString(R.string.was_present_for_meeting_no_on_date_urdu) + "\n\n"
            }

            if (savingAmount.equals("null") || savingAmount.equals("0")) {
                var a = context.resources.getString(R.string.she_didnot_save_amount_urdu)
                a = a.replace("SHG_MEM", "")
                saving_sentence = a + "\n\n"
            } else {
                var a = context.resources.getString(R.string.she_was_save_amount_urdu)
                a = a.replace("SHG_MEM", memberData.member_name!!.toLowerCase())
                a = a.replace("SHG_AMOUNT", savingAmount)
                saving_sentence = a + "\n\n"
            }

            if (distributedAmount.equals("null") || distributedAmount.toString().equals("0")) {
                var a = context.resources.getString(R.string.loan_not_distributed_urdu)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                distributedAmount_sentence = a + "\n\n"
            } else if (distributedAmount.toLong() > 0) {
                var a = context.resources.getString(R.string.loan_distributed_to_her_urdu)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                a = a.replace("SHG_LOAN_AMOUNT", "'" + distributedAmount.toInt() + "'")
                distributedAmount_sentence = a + "\n\n"
            }

            if (repaymentAmount.equals("null") || repaymentAmount.toString().equals("0")) {
                var a = context.resources.getString(R.string.didnt_have_repayment_urdu)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                repaymentAmount_sentence = a + "\n\n"
            } else if (repaymentAmount.toLong() > 0) {
                var a = context.resources.getString(R.string.loan_repayment_was_urdu)
                a = a.replace("SHG_MEM", "'" + memberData.member_name!!.toLowerCase() + "'")
                a = a.replace("SHG_LOAN_AMOUNT", "'" + repaymentAmount.toInt() + "'")
                repaymentAmount_sentence = a + "\n\n"
            }
        }

        attendance_sentence = attendance_sentence.replace("DATE", MeetingDate)
        attendance_sentence =
            attendance_sentence.replace("SHG_MEM", memberData.member_name!!.toLowerCase())
        attendance_sentence = attendance_sentence.replace("MEET_NO", meetNo.toString())

        finaldata =
            "\n" + attendance_sentence + saving_sentence + distributedAmount_sentence + repaymentAmount_sentence
        return finaldata

    }


    fun VORegularMeeting(
        meetNo: Int,
        lang: String,
        context: Context,
        SHG_name: String,
        MeetingDate: String,
        totalMember: String,
        presentMember: String,
        bank_balance: Int,
        bank_credit_balance: Int,
        bank_debit_balance: Int,
        cash_balance: Int,
        cash_credit_balance: Int,
        cash_debit_balance: Int,
        receipt_shg: String,
        receipt_clf: String,
        receipt_srlm: String,
        receipt_other: String,
        payment_shg: String,
        payment_clf: String,
        payment_other: String,
        shg_receiptList: List<MstVOCOAEntity>,
        shg_paymentList: List<MstVOCOAEntity>,
        shgList: List<MappedShgEntity>,
        voList: List<MappedVoEntity>,
        MemberList: List<VoMtgDetEntity>

    ): String {

        var sentence1 = ""
        var summarySentence = ""
        var attendanceSentence = ""
        var AbsentattendanceSentence = ""
        var totalreceiptSentence = ""
        var totalpaymentSentence = ""
        var receiptFromShgSentence = ""
        var receiptFromClfSentence = ""
        var receiptFromSrlmSentence = ""
        var receiptFromOtherSentence = ""
        var receiptFrom = ""
        var notreceiptFrom = ""
        var paymentFrom = ""
        var notpaymentFrom = ""
        var paymentFromShgSentence = ""
        var paymentFromClfSentence = ""
        var paymentFromOtherSentence = ""

        var cash_balanceSentence = ""
        var cash_debitSentence = ""
        var cash_creditSentence = ""
        var bank_balanceSentence = ""
        var bank_debitSentence = ""
        var bank_creditSentence = ""
        var totalCollectionSentence = ""
        var was_absent = ""

        var receipt_memList = ""
        var payment_memList = ""
        var receipt_count = 1
        var payment_count = 1
        var absent_count = 1

        var totalrecepit =
            receipt_shg.toInt() + receipt_clf.toInt() + receipt_srlm.toInt() + receipt_other.toInt()
        var totalpayment = payment_shg.toInt() + payment_clf.toInt() + payment_other.toInt()

        var days = arrayOf("रविवार", "सोमवार", "मंगलवार", "बुधवार", "गुरुवार", "शुक्रवार", "शनिवार")


        if (lang.equals("hi")) {
            var days =
                arrayOf("रविवार", "सोमवार", "मंगलवार", "बुधवार", "गुरुवार", "शुक्रवार", "शनिवार")

            sentence1 = context.resources.getString(R.string.sentence1_hin) + "\n\n"
            summarySentence = context.resources.getString(R.string.sentence2_hin) + "\n\n"
            attendanceSentence =
                context.resources.getString(R.string.where_vo_present_out_of_vo_total_hin) + "\n\n"
            totalreceiptSentence =
                context.resources.getString(R.string.total_receipt_collect_are_hin) + "\n\n"
            receiptFrom =
                context.resources.getString(R.string.total_receipt_collect_from_shg_are_hin) + "\n\n"
            paymentFrom =
                context.resources.getString(R.string.total_payment_to_shg_are_rupee_hin) + "\n\n"
            totalpaymentSentence =
                context.resources.getString(R.string.total_payment_are_hin) + "\n\n"
            totalCollectionSentence = context.resources.getString(R.string.sentence14_hin) + "\n\n"
            notreceiptFrom =
                context.resources.getString(R.string.receipt_not_receive_from_shg_hin) + "\n\n"
            notpaymentFrom =
                context.resources.getString(R.string.total_payment_not_receive_hin) + "\n\n"

            cash_balanceSentence = context.resources.getString(R.string.sentence20_hin) + "\n\n"
            cash_debitSentence = context.resources.getString(R.string.sentence16_hin) + "\n\n"
            cash_creditSentence = context.resources.getString(R.string.sentence15_hin) + "\n\n"
            bank_balanceSentence = context.resources.getString(R.string.sentence19_hin) + "\n\n"
            bank_debitSentence = context.resources.getString(R.string.sentence18_hin) + "\n\n"
            bank_creditSentence = context.resources.getString(R.string.sentence17_hin) + "\n\n"

            for (i in 0 until MemberList.size) {

                if (MemberList != null || MemberList.size > 0) {
                    if (MemberList.get(i).attendanceOther!!.toInt() == 0) {
                        var a =
                            "'" + MemberList.get(i).childCboName!!.toLowerCase() + "' " + context.resources.getString(
                                R.string.sentence5_hin
                            )

                        AbsentattendanceSentence =
                            AbsentattendanceSentence + absent_count.toString() + ". " + a + "\n"
                        absent_count++
                    }
                }
            }

            if (!AbsentattendanceSentence.isNullOrEmpty()) {
                AbsentattendanceSentence = context.resources.getString(
                    R.string.shg_absent_for_meeting_those_are_hin
                ) + "\n\n" + AbsentattendanceSentence
            }


            for (i in 0 until shgList.size) {
                var amount = (context as VoRegularMeetingTTSActivity).getTotalValue(
                    shg_receiptList,
                    shgList.get(i).shg_id!!
                )
                if (amount.toInt() > 0) {
                    var a =
                        context.resources.getString(R.string.total_receipt_collect_from_shg_are_hin)
                    a = a.replace(
                        "SHG",
                        "'" + shgList.get(i).shg_name!!.toLowerCase() + "'"
                    )
                    a = a.replace(
                        "VO_AMOUNT",
                        "'" + amount.toString() + "'"
                    )
                    receipt_memList = receipt_memList + receipt_count.toString() + ". " + a + "\n\n"
                    receipt_count++
                }
            }

            for (i in 0 until shgList.size) {
                var amount = (context as VoRegularMeetingTTSActivity).getTotalValue(
                    shg_paymentList,
                    shgList.get(i).shg_id!!
                )
                if (amount.toInt() > 0) {
                    var a = context.resources.getString(R.string.total_payment_to_shg_are_rupee_hin)
                    a = a.replace(
                        "SHG",
                        "'" + shgList.get(i).shg_name!!.toLowerCase() + "'"
                    )
                    a = a.replace(
                        "VO_AMOUNT",
                        "'" + amount.toString() + "'"
                    )
                    payment_memList = payment_memList + payment_count.toString() + ". " + a + "\n\n"
                    payment_count++
                }
            }

        }
        if (lang.equals("en")) {
            days = arrayOf(
                "Sunday",
                "Monday",
                "Tuesday",
                "Wednesday",
                "Thursday",
                "Friday",
                "Saturday"
            )
            sentence1 = context.resources.getString(R.string.sentence1) + "\n\n"
            summarySentence = context.resources.getString(R.string.sentence2) + "\n\n"
            attendanceSentence =
                context.resources.getString(R.string.where_vo_present_out_of_vo_total) + "\n\n"
            totalreceiptSentence =
                context.resources.getString(R.string.total_receipt_collect_are) + "\n\n"
            receiptFrom =
                context.resources.getString(R.string.total_receipt_collect_from_shg_are) + "\n\n"
            paymentFrom =
                context.resources.getString(R.string.total_payment_to_shg_are_rupee) + "\n\n"
            totalpaymentSentence = context.resources.getString(R.string.total_payment_are) + "\n\n"
            totalCollectionSentence = context.resources.getString(R.string.sentence14) + "\n\n"
            notreceiptFrom =
                context.resources.getString(R.string.receipt_not_receive_from_shg) + "\n\n"
            notpaymentFrom =
                context.resources.getString(R.string.total_payment_not_receive) + "\n\n"

            cash_balanceSentence = context.resources.getString(R.string.sentence20) + "\n\n"
            cash_debitSentence = context.resources.getString(R.string.sentence16) + "\n\n"
            cash_creditSentence = context.resources.getString(R.string.sentence15) + "\n\n"
            bank_balanceSentence = context.resources.getString(R.string.sentence19) + "\n\n"
            bank_debitSentence = context.resources.getString(R.string.sentence18) + "\n\n"
            bank_creditSentence = context.resources.getString(R.string.sentence17) + "\n\n"

            for (i in 0 until MemberList.size) {

                if (MemberList != null || MemberList.size > 0) {
                    if (MemberList.get(i).attendanceOther!!.toInt() == 0) {
                        var a =
                            "'" + MemberList.get(i).childCboName!!.toLowerCase() + "' " + context.resources.getString(
                                R.string.sentence5
                            )

                        AbsentattendanceSentence =
                            AbsentattendanceSentence + absent_count.toString() + ". " + a + "\n"
                        absent_count++
                    }
                }
            }

            if (!AbsentattendanceSentence.isNullOrEmpty()) {
                AbsentattendanceSentence = context.resources.getString(
                    R.string.shg_absent_for_meeting_those_are
                ) + "\n\n" + AbsentattendanceSentence
            }


            for (i in 0 until shgList.size) {
                var amount = (context as VoRegularMeetingTTSActivity).getTotalValue(
                    shg_receiptList,
                    shgList.get(i).shg_id!!
                )
                if (amount.toInt() > 0) {
                    var a = context.resources.getString(R.string.total_receipt_collect_from_shg_are)
                    a = a.replace(
                        "SHG",
                        "'" + shgList.get(i).shg_name!!.toLowerCase() + "'"
                    )
                    a = a.replace(
                        "VO_AMOUNT",
                        "'" + amount.toString() + "'"
                    )
                    receipt_memList = receipt_memList + receipt_count.toString() + ". " + a + "\n\n"
                    receipt_count++
                }
            }

            for (i in 0 until shgList.size) {
                var amount = (context as VoRegularMeetingTTSActivity).getTotalValue(
                    shg_paymentList,
                    shgList.get(i).shg_id!!
                )
                if (amount.toInt() > 0) {
                    var a = context.resources.getString(R.string.total_payment_to_shg_are_rupee)
                    a = a.replace(
                        "SHG",
                        "'" + shgList.get(i).shg_name!!.toLowerCase() + "'"
                    )
                    a = a.replace(
                        "VO_AMOUNT",
                        "'" + amount.toString() + "'"
                    )
                    payment_memList = payment_memList + payment_count.toString() + ". " + a + "\n\n"
                    payment_count++
                }
            }


        }
        if (lang.equals("as")) {
            days = arrayOf(
                "দেওবাৰ",
                "সোমবাৰে",
                "মঙলবাৰে",
                "বুধবাৰে",
                "বৃহস্পতিবাৰে",
                "শুক্ৰবাৰে",
                "শনিবাৰে"
            )
            sentence1 = context.resources.getString(R.string.sentence1_ass) + "\n\n"
            summarySentence = context.resources.getString(R.string.sentence2_ass) + "\n\n"
            attendanceSentence =
                context.resources.getString(R.string.where_vo_present_out_of_vo_total_ass) + "\n\n"
            totalreceiptSentence =
                context.resources.getString(R.string.total_receipt_collect_are_ass) + "\n\n"
            receiptFrom =
                context.resources.getString(R.string.total_receipt_collect_from_shg_are_ass) + "\n\n"
            paymentFrom =
                context.resources.getString(R.string.total_payment_to_shg_are_rupee_ass) + "\n\n"
            totalpaymentSentence =
                context.resources.getString(R.string.total_payment_are_ass) + "\n\n"
            totalCollectionSentence = context.resources.getString(R.string.sentence14_ass) + "\n\n"
            notreceiptFrom =
                context.resources.getString(R.string.receipt_not_receive_from_shg_ass) + "\n\n"
            notpaymentFrom =
                context.resources.getString(R.string.total_payment_not_receive_ass) + "\n\n"

            cash_balanceSentence = context.resources.getString(R.string.sentence20_ass) + "\n\n"
            cash_debitSentence = context.resources.getString(R.string.sentence16_ass) + "\n\n"
            cash_creditSentence = context.resources.getString(R.string.sentence15_ass) + "\n\n"
            bank_balanceSentence = context.resources.getString(R.string.sentence19_ass) + "\n\n"
            bank_debitSentence = context.resources.getString(R.string.sentence18_ass) + "\n\n"
            bank_creditSentence = context.resources.getString(R.string.sentence17_ass) + "\n\n"

            for (i in 0 until MemberList.size) {

                if (MemberList != null || MemberList.size > 0) {
                    if (MemberList.get(i).attendanceOther!!.toInt() == 0) {
                        var a =
                            "'" + MemberList.get(i).childCboName!!.toLowerCase() + "' " + context.resources.getString(
                                R.string.sentence5_ass
                            )

                        AbsentattendanceSentence =
                            AbsentattendanceSentence + absent_count.toString() + ". " + a + "\n"
                        absent_count++
                    }
                }
            }

            if (!AbsentattendanceSentence.isNullOrEmpty()) {
                AbsentattendanceSentence = context.resources.getString(
                    R.string.shg_absent_for_meeting_those_are_ass
                ) + "\n\n" + AbsentattendanceSentence
            }

        }
        if (lang.equals("bn")) {
            days =
                arrayOf(
                    "রবিবার",
                    "সোমবার",
                    "মঙ্গলবার",
                    "বুধবার",
                    "বৃহস্পতিবার",
                    "শুক্রবার",
                    "শনিবার"
                )
            sentence1 = context.resources.getString(R.string.sentence1_bangla) + "\n\n"
            summarySentence = context.resources.getString(R.string.sentence2_bangla) + "\n\n"
            attendanceSentence =
                context.resources.getString(R.string.where_vo_present_out_of_vo_total_bangla) + "\n\n"
            totalreceiptSentence =
                context.resources.getString(R.string.total_receipt_collect_are_bangla) + "\n\n"
            receiptFrom =
                context.resources.getString(R.string.total_receipt_collect_from_shg_are_bangla) + "\n\n"
            paymentFrom =
                context.resources.getString(R.string.total_payment_to_shg_are_rupee_bangla) + "\n\n"
            totalpaymentSentence =
                context.resources.getString(R.string.total_payment_are_bangla) + "\n\n"
            totalCollectionSentence =
                context.resources.getString(R.string.sentence14_bangla) + "\n\n"
            notreceiptFrom =
                context.resources.getString(R.string.receipt_not_receive_from_shg_bangla) + "\n\n"
            notpaymentFrom =
                context.resources.getString(R.string.total_payment_not_receive_bangla) + "\n\n"

            cash_balanceSentence = context.resources.getString(R.string.sentence20_bangla) + "\n\n"
            cash_debitSentence = context.resources.getString(R.string.sentence16_bangla) + "\n\n"
            cash_creditSentence = context.resources.getString(R.string.sentence15_bangla) + "\n\n"
            bank_balanceSentence = context.resources.getString(R.string.sentence19_bangla) + "\n\n"
            bank_debitSentence = context.resources.getString(R.string.sentence18_bangla) + "\n\n"
            bank_creditSentence = context.resources.getString(R.string.sentence17_bangla) + "\n\n"

            for (i in 0 until MemberList.size) {

                if (MemberList != null || MemberList.size > 0) {
                    if (MemberList.get(i).attendanceOther!!.toInt() == 0) {
                        var a =
                            "'" + MemberList.get(i).childCboName!!.toLowerCase() + "' " + context.resources.getString(
                                R.string.sentence5_bangla
                            )

                        AbsentattendanceSentence =
                            AbsentattendanceSentence + absent_count.toString() + ". " + a + "\n"
                        absent_count++
                    }
                }
            }

            if (!AbsentattendanceSentence.isNullOrEmpty()) {
                AbsentattendanceSentence = context.resources.getString(
                    R.string.shg_absent_for_meeting_those_are_bangla
                ) + "\n\n" + AbsentattendanceSentence
            }

        }
        if (lang.equals("gu")) {
            days = arrayOf("રવિવાર", "સોમવાર", "મંગળવાર", "બુધવાર", "ગુરુવાર", "શુક્રવાર", "શનિવાર")

            sentence1 = context.resources.getString(R.string.sentence1_gujarati) + "\n\n"
            summarySentence = context.resources.getString(R.string.sentence2_gujarati) + "\n\n"
            attendanceSentence =
                context.resources.getString(R.string.where_vo_present_out_of_vo_total_gujarati) + "\n\n"
            totalreceiptSentence =
                context.resources.getString(R.string.total_receipt_collect_are_gujarati) + "\n\n"
            receiptFrom =
                context.resources.getString(R.string.total_receipt_collect_from_shg_are_gujarati) + "\n\n"
            paymentFrom =
                context.resources.getString(R.string.total_payment_to_shg_are_rupee_gujarati) + "\n\n"
            totalpaymentSentence =
                context.resources.getString(R.string.total_payment_are_gujarati) + "\n\n"
            totalCollectionSentence =
                context.resources.getString(R.string.sentence14_gujarati) + "\n\n"
            notreceiptFrom =
                context.resources.getString(R.string.receipt_not_receive_from_shg_gujarati) + "\n\n"
            notpaymentFrom =
                context.resources.getString(R.string.total_payment_not_receive_gujarati) + "\n\n"

            cash_balanceSentence =
                context.resources.getString(R.string.sentence20_gujarati) + "\n\n"
            cash_debitSentence = context.resources.getString(R.string.sentence16_gujarati) + "\n\n"
            cash_creditSentence = context.resources.getString(R.string.sentence15_gujarati) + "\n\n"
            bank_balanceSentence =
                context.resources.getString(R.string.sentence19_gujarati) + "\n\n"
            bank_debitSentence = context.resources.getString(R.string.sentence18_gujarati) + "\n\n"
            bank_creditSentence = context.resources.getString(R.string.sentence17_gujarati) + "\n\n"

            for (i in 0 until MemberList.size) {

                if (MemberList != null || MemberList.size > 0) {
                    if (MemberList.get(i).attendanceOther!!.toInt() == 0) {
                        var a =
                            "'" + MemberList.get(i).childCboName!!.toLowerCase() + "' " + context.resources.getString(
                                R.string.sentence5_gujarati
                            )

                        AbsentattendanceSentence =
                            AbsentattendanceSentence + absent_count.toString() + ". " + a + "\n"
                        absent_count++
                    }
                }
            }

            if (!AbsentattendanceSentence.isNullOrEmpty()) {
                AbsentattendanceSentence = context.resources.getString(
                    R.string.shg_absent_for_meeting_those_are_gujarati
                ) + "\n\n" + AbsentattendanceSentence
            }

        }
        if (lang.equals("kn")) {
            days =
                arrayOf("ಭಾನುವಾರ", "ಸೋಮವಾರ", "ಮಂಗಳವಾರ", "ಬುಧವಾರ", "ಗುರುವಾರ", "ಶುಕ್ರವಾರ", "ಶನಿವಾರ")

            sentence1 = context.resources.getString(R.string.sentence1_Kannad) + "\n\n"
            summarySentence = context.resources.getString(R.string.sentence2_Kannad) + "\n\n"
            attendanceSentence =
                context.resources.getString(R.string.where_vo_present_out_of_vo_total_Kannad) + "\n\n"
            totalreceiptSentence =
                context.resources.getString(R.string.total_receipt_collect_are_Kannad) + "\n\n"
            receiptFrom =
                context.resources.getString(R.string.total_receipt_collect_from_shg_are_Kannad) + "\n\n"
            paymentFrom =
                context.resources.getString(R.string.total_payment_to_shg_are_rupee_Kannad) + "\n\n"
            totalpaymentSentence =
                context.resources.getString(R.string.total_payment_are_Kannad) + "\n\n"
            totalCollectionSentence =
                context.resources.getString(R.string.sentence14_Kannad) + "\n\n"
            notreceiptFrom =
                context.resources.getString(R.string.receipt_not_receive_from_shg_Kannad) + "\n\n"
            notpaymentFrom =
                context.resources.getString(R.string.total_payment_not_receive_Kannad) + "\n\n"

            cash_balanceSentence = context.resources.getString(R.string.sentence20_Kannad) + "\n\n"
            cash_debitSentence = context.resources.getString(R.string.sentence16_Kannad) + "\n\n"
            cash_creditSentence = context.resources.getString(R.string.sentence15_Kannad) + "\n\n"
            bank_balanceSentence = context.resources.getString(R.string.sentence19_Kannad) + "\n\n"
            bank_debitSentence = context.resources.getString(R.string.sentence18_Kannad) + "\n\n"
            bank_creditSentence = context.resources.getString(R.string.sentence17_Kannad) + "\n\n"

            for (i in 0 until MemberList.size) {

                if (MemberList != null || MemberList.size > 0) {
                    if (MemberList.get(i).attendanceOther!!.toInt() == 0) {
                        var a =
                            "'" + MemberList.get(i).childCboName!!.toLowerCase() + "' " + context.resources.getString(
                                R.string.sentence5_Kannad
                            )

                        AbsentattendanceSentence =
                            AbsentattendanceSentence + absent_count.toString() + ". " + a + "\n"
                        absent_count++
                    }
                }
            }

            if (!AbsentattendanceSentence.isNullOrEmpty()) {
                AbsentattendanceSentence = context.resources.getString(
                    R.string.shg_absent_for_meeting_those_are_Kannad
                ) + "\n\n" + AbsentattendanceSentence
            }

        }
        if (lang.equals("ml")) {
            days = arrayOf("ഞായർ", "തിങ്കൾ", "ചൊവ്വ", "ബുധൻ", "വ്യാഴം", "വെള്ളി", "ശനി")

            sentence1 = context.resources.getString(R.string.sentence1_malayalam) + "\n\n"
            summarySentence = context.resources.getString(R.string.sentence2_malayalam) + "\n\n"
            attendanceSentence =
                context.resources.getString(R.string.where_vo_present_out_of_vo_total_malayalam) + "\n\n"
            totalreceiptSentence =
                context.resources.getString(R.string.total_receipt_collect_are_malayalam) + "\n\n"
            receiptFrom =
                context.resources.getString(R.string.total_receipt_collect_from_shg_are_malayalam) + "\n\n"
            paymentFrom =
                context.resources.getString(R.string.total_payment_to_shg_are_rupee_malayalam) + "\n\n"
            totalpaymentSentence =
                context.resources.getString(R.string.total_payment_are_malayalam) + "\n\n"
            totalCollectionSentence =
                context.resources.getString(R.string.sentence14_malayalam) + "\n\n"
            notreceiptFrom =
                context.resources.getString(R.string.receipt_not_receive_from_shg_malayalam) + "\n\n"
            notpaymentFrom =
                context.resources.getString(R.string.total_payment_not_receive_malayalam) + "\n\n"

            cash_balanceSentence =
                context.resources.getString(R.string.sentence20_malayalam) + "\n\n"
            cash_debitSentence = context.resources.getString(R.string.sentence16_malayalam) + "\n\n"
            cash_creditSentence =
                context.resources.getString(R.string.sentence15_malayalam) + "\n\n"
            bank_balanceSentence =
                context.resources.getString(R.string.sentence19_malayalam) + "\n\n"
            bank_debitSentence = context.resources.getString(R.string.sentence18_malayalam) + "\n\n"
            bank_creditSentence =
                context.resources.getString(R.string.sentence17_malayalam) + "\n\n"

            for (i in 0 until MemberList.size) {

                if (MemberList != null || MemberList.size > 0) {
                    if (MemberList.get(i).attendanceOther!!.toInt() == 0) {
                        var a =
                            "'" + MemberList.get(i).childCboName!!.toLowerCase() + "' " + context.resources.getString(
                                R.string.sentence5_malayalam
                            )

                        AbsentattendanceSentence =
                            AbsentattendanceSentence + absent_count.toString() + ". " + a + "\n"
                        absent_count++
                    }
                }
            }

            if (!AbsentattendanceSentence.isNullOrEmpty()) {
                AbsentattendanceSentence = context.resources.getString(
                    R.string.shg_absent_for_meeting_those_are_malayalam
                ) + "\n\n" + AbsentattendanceSentence
            }

        }
        if (lang.equals("mr")) {
            days = arrayOf("रविवार", "सोमवार", "मंगलवार", "बुधवार", "गुरुवार", "शुक्रवार", "शनिवार")

            sentence1 = context.resources.getString(R.string.sentence1_marathi) + "\n\n"
            summarySentence = context.resources.getString(R.string.sentence2_marathi) + "\n\n"
            attendanceSentence =
                context.resources.getString(R.string.where_vo_present_out_of_vo_total_marathi) + "\n\n"
            totalreceiptSentence =
                context.resources.getString(R.string.total_receipt_collect_are_marathi) + "\n\n"
            receiptFrom =
                context.resources.getString(R.string.total_receipt_collect_from_shg_are_marathi) + "\n\n"
            paymentFrom =
                context.resources.getString(R.string.total_payment_to_shg_are_rupee_marathi) + "\n\n"
            totalpaymentSentence =
                context.resources.getString(R.string.total_payment_are_marathi) + "\n\n"
            totalCollectionSentence =
                context.resources.getString(R.string.sentence14_marathi) + "\n\n"
            notreceiptFrom =
                context.resources.getString(R.string.receipt_not_receive_from_shg_marathi) + "\n\n"
            notpaymentFrom =
                context.resources.getString(R.string.total_payment_not_receive_marathi) + "\n\n"

            cash_balanceSentence = context.resources.getString(R.string.sentence20_marathi) + "\n\n"
            cash_debitSentence = context.resources.getString(R.string.sentence16_marathi) + "\n\n"
            cash_creditSentence = context.resources.getString(R.string.sentence15_marathi) + "\n\n"
            bank_balanceSentence = context.resources.getString(R.string.sentence19_marathi) + "\n\n"
            bank_debitSentence = context.resources.getString(R.string.sentence18_marathi) + "\n\n"
            bank_creditSentence = context.resources.getString(R.string.sentence17_marathi) + "\n\n"

            for (i in 0 until MemberList.size) {

                if (MemberList != null || MemberList.size > 0) {
                    if (MemberList.get(i).attendanceOther!!.toInt() == 0) {
                        var a =
                            "'" + MemberList.get(i).childCboName!!.toLowerCase() + "' " + context.resources.getString(
                                R.string.sentence5_marathi
                            )

                        AbsentattendanceSentence =
                            AbsentattendanceSentence + absent_count.toString() + ". " + a + "\n"
                        absent_count++
                    }
                }
            }

            if (!AbsentattendanceSentence.isNullOrEmpty()) {
                AbsentattendanceSentence = context.resources.getString(
                    R.string.shg_absent_for_meeting_those_are_marathi
                ) + "\n\n" + AbsentattendanceSentence
            }

        }
        if (lang.equals("or")) {
            days =
                arrayOf("ରବିବାର", "ସୋମବାର", "ମଙ୍ଗଳବାର", "ବୁଧବାର", "ଗୁରୁବାର", "ଶୁକ୍ରବାର", "ଶନିବାର")

            sentence1 = context.resources.getString(R.string.sentence1_oriya) + "\n\n"
            summarySentence = context.resources.getString(R.string.sentence2_oriya) + "\n\n"
            attendanceSentence =
                context.resources.getString(R.string.where_vo_present_out_of_vo_total_oriya) + "\n\n"
            totalreceiptSentence =
                context.resources.getString(R.string.total_receipt_collect_are_oriya) + "\n\n"
            receiptFrom =
                context.resources.getString(R.string.total_receipt_collect_from_shg_are_oriya) + "\n\n"
            paymentFrom =
                context.resources.getString(R.string.total_payment_to_shg_are_rupee_oriya) + "\n\n"
            totalpaymentSentence =
                context.resources.getString(R.string.total_payment_are_oriya) + "\n\n"
            totalCollectionSentence =
                context.resources.getString(R.string.sentence14_oriya) + "\n\n"
            notreceiptFrom =
                context.resources.getString(R.string.receipt_not_receive_from_shg_oriya) + "\n\n"
            notpaymentFrom =
                context.resources.getString(R.string.total_payment_not_receive_oriya) + "\n\n"

            cash_balanceSentence = context.resources.getString(R.string.sentence20_oriya) + "\n\n"
            cash_debitSentence = context.resources.getString(R.string.sentence16_oriya) + "\n\n"
            cash_creditSentence = context.resources.getString(R.string.sentence15_oriya) + "\n\n"
            bank_balanceSentence = context.resources.getString(R.string.sentence19_oriya) + "\n\n"
            bank_debitSentence = context.resources.getString(R.string.sentence18_oriya) + "\n\n"
            bank_creditSentence = context.resources.getString(R.string.sentence17_oriya) + "\n\n"

            for (i in 0 until MemberList.size) {

                if (MemberList != null || MemberList.size > 0) {
                    if (MemberList.get(i).attendanceOther!!.toInt() == 0) {
                        var a =
                            "'" + MemberList.get(i).childCboName!!.toLowerCase() + "' " + context.resources.getString(
                                R.string.sentence5_oriya
                            )

                        AbsentattendanceSentence =
                            AbsentattendanceSentence + absent_count.toString() + ". " + a + "\n"
                        absent_count++
                    }
                }
            }

            if (!AbsentattendanceSentence.isNullOrEmpty()) {
                AbsentattendanceSentence = context.resources.getString(
                    R.string.shg_absent_for_meeting_those_are_oriya
                ) + "\n\n" + AbsentattendanceSentence
            }

        }
        if (lang.equals("pa")) {
            days =
                arrayOf("ਐਤਵਾਰ", "ਸੋਮਵਾਰ", "ਮੰਗਲਵਾਰ", "ਬੁੱਧਵਾਰ", "ਵੀਰਵਾਰ", "ਸ਼ੁੱਕਰਵਾਰ", "ਸ਼ਨੀਵਾਰ")

            sentence1 = context.resources.getString(R.string.sentence1_punjab) + "\n\n"
            summarySentence = context.resources.getString(R.string.sentence2_punjab) + "\n\n"
            attendanceSentence =
                context.resources.getString(R.string.where_vo_present_out_of_vo_total_punjab) + "\n\n"
            totalreceiptSentence =
                context.resources.getString(R.string.total_receipt_collect_are_punjab) + "\n\n"
            receiptFrom =
                context.resources.getString(R.string.total_receipt_collect_from_shg_are_punjab) + "\n\n"
            paymentFrom =
                context.resources.getString(R.string.total_payment_to_shg_are_rupee_punjab) + "\n\n"
            totalpaymentSentence =
                context.resources.getString(R.string.total_payment_are_punjab) + "\n\n"
            totalCollectionSentence =
                context.resources.getString(R.string.sentence14_punjab) + "\n\n"
            notreceiptFrom =
                context.resources.getString(R.string.receipt_not_receive_from_shg_punjab) + "\n\n"
            notpaymentFrom =
                context.resources.getString(R.string.total_payment_not_receive_punjab) + "\n\n"

            cash_balanceSentence = context.resources.getString(R.string.sentence20_punjab) + "\n\n"
            cash_debitSentence = context.resources.getString(R.string.sentence16_punjab) + "\n\n"
            cash_creditSentence = context.resources.getString(R.string.sentence15_punjab) + "\n\n"
            bank_balanceSentence = context.resources.getString(R.string.sentence19_punjab) + "\n\n"
            bank_debitSentence = context.resources.getString(R.string.sentence18_punjab) + "\n\n"
            bank_creditSentence = context.resources.getString(R.string.sentence17_punjab) + "\n\n"

            for (i in 0 until MemberList.size) {

                if (MemberList != null || MemberList.size > 0) {
                    if (MemberList.get(i).attendanceOther!!.toInt() == 0) {
                        var a =
                            "'" + MemberList.get(i).childCboName!!.toLowerCase() + "' " + context.resources.getString(
                                R.string.sentence5_punjab
                            )

                        AbsentattendanceSentence =
                            AbsentattendanceSentence + absent_count.toString() + ". " + a + "\n"
                        absent_count++
                    }
                }
            }

            if (!AbsentattendanceSentence.isNullOrEmpty()) {
                AbsentattendanceSentence = context.resources.getString(
                    R.string.shg_absent_for_meeting_those_are_punjab
                ) + "\n\n" + AbsentattendanceSentence
            }

        }
        if (lang.equals("ta")) {
            days =
                arrayOf("ஞாயிறு", "திங்கள்", "செவ்வாய்", "புதன்", "வியாழன்", "வெள்ளி", "சனிக்கிழமை")

            sentence1 = context.resources.getString(R.string.sentence1_tamil) + "\n\n"
            summarySentence = context.resources.getString(R.string.sentence2_tamil) + "\n\n"
            attendanceSentence =
                context.resources.getString(R.string.where_vo_present_out_of_vo_total_tamil) + "\n\n"
            totalreceiptSentence =
                context.resources.getString(R.string.total_receipt_collect_are_tamil) + "\n\n"
            receiptFrom =
                context.resources.getString(R.string.total_receipt_collect_from_shg_are_tamil) + "\n\n"
            paymentFrom =
                context.resources.getString(R.string.total_payment_to_shg_are_rupee_tamil) + "\n\n"
            totalpaymentSentence =
                context.resources.getString(R.string.total_payment_are_tamil) + "\n\n"
            totalCollectionSentence =
                context.resources.getString(R.string.sentence14_tamil) + "\n\n"
            notreceiptFrom =
                context.resources.getString(R.string.receipt_not_receive_from_shg_tamil) + "\n\n"
            notpaymentFrom =
                context.resources.getString(R.string.total_payment_not_receive_tamil) + "\n\n"

            cash_balanceSentence = context.resources.getString(R.string.sentence20_tamil) + "\n\n"
            cash_debitSentence = context.resources.getString(R.string.sentence16_tamil) + "\n\n"
            cash_creditSentence = context.resources.getString(R.string.sentence15_tamil) + "\n\n"
            bank_balanceSentence = context.resources.getString(R.string.sentence19_tamil) + "\n\n"
            bank_debitSentence = context.resources.getString(R.string.sentence18_tamil) + "\n\n"
            bank_creditSentence = context.resources.getString(R.string.sentence17_tamil) + "\n\n"

            for (i in 0 until MemberList.size) {

                if (MemberList != null || MemberList.size > 0) {
                    if (MemberList.get(i).attendanceOther!!.toInt() == 0) {
                        var a =
                            "'" + MemberList.get(i).childCboName!!.toLowerCase() + "' " + context.resources.getString(
                                R.string.sentence5_tamil
                            )

                        AbsentattendanceSentence =
                            AbsentattendanceSentence + absent_count.toString() + ". " + a + "\n"
                        absent_count++
                    }
                }
            }

            if (!AbsentattendanceSentence.isNullOrEmpty()) {
                AbsentattendanceSentence = context.resources.getString(
                    R.string.shg_absent_for_meeting_those_are_tamil
                ) + "\n\n" + AbsentattendanceSentence
            }

        }
        if (lang.equals("te")) {
            days =
                arrayOf(
                    "ఆదివారం",
                    "సోమవారం",
                    "మంగళవారం",
                    "బుధవారం",
                    "గురువారం",
                    "శుక్రవారం",
                    "శనివారం"
                )
            sentence1 = context.resources.getString(R.string.sentence1_telgu) + "\n\n"
            summarySentence = context.resources.getString(R.string.sentence2_telgu) + "\n\n"
            attendanceSentence =
                context.resources.getString(R.string.where_vo_present_out_of_vo_total_telgu) + "\n\n"
            totalreceiptSentence =
                context.resources.getString(R.string.total_receipt_collect_are_telgu) + "\n\n"
            receiptFrom =
                context.resources.getString(R.string.total_receipt_collect_from_shg_are_telgu) + "\n\n"
            paymentFrom =
                context.resources.getString(R.string.total_payment_to_shg_are_rupee_telgu) + "\n\n"
            totalpaymentSentence =
                context.resources.getString(R.string.total_payment_are_telgu) + "\n\n"
            totalCollectionSentence =
                context.resources.getString(R.string.sentence14_telgu) + "\n\n"
            notreceiptFrom =
                context.resources.getString(R.string.receipt_not_receive_from_shg_telgu) + "\n\n"
            notpaymentFrom =
                context.resources.getString(R.string.total_payment_not_receive_telgu) + "\n\n"

            cash_balanceSentence = context.resources.getString(R.string.sentence20_telgu) + "\n\n"
            cash_debitSentence = context.resources.getString(R.string.sentence16_telgu) + "\n\n"
            cash_creditSentence = context.resources.getString(R.string.sentence15_telgu) + "\n\n"
            bank_balanceSentence = context.resources.getString(R.string.sentence19_telgu) + "\n\n"
            bank_debitSentence = context.resources.getString(R.string.sentence18_telgu) + "\n\n"
            bank_creditSentence = context.resources.getString(R.string.sentence17_telgu) + "\n\n"

            for (i in 0 until MemberList.size) {

                if (MemberList != null || MemberList.size > 0) {
                    if (MemberList.get(i).attendanceOther!!.toInt() == 0) {
                        var a =
                            "'" + MemberList.get(i).childCboName!!.toLowerCase() + "' " + context.resources.getString(
                                R.string.sentence5_telgu
                            )

                        AbsentattendanceSentence =
                            AbsentattendanceSentence + absent_count.toString() + ". " + a + "\n"
                        absent_count++
                    }
                }
            }

            if (!AbsentattendanceSentence.isNullOrEmpty()) {
                AbsentattendanceSentence = context.resources.getString(
                    R.string.shg_absent_for_meeting_those_are_telgu
                ) + "\n\n" + AbsentattendanceSentence
            }

        }
        if (lang.equals("ur")) {
            days = arrayOf("اتوار", "پیر", "منگل", "بدھ", "جمعرات", "جمعہ", "ہفتہ")

            sentence1 = context.resources.getString(R.string.sentence1_urdu) + "\n\n"
            summarySentence = context.resources.getString(R.string.sentence2_urdu) + "\n\n"
            attendanceSentence =
                context.resources.getString(R.string.where_vo_present_out_of_vo_total_urdu) + "\n\n"
            totalreceiptSentence =
                context.resources.getString(R.string.total_receipt_collect_are_urdu) + "\n\n"
            receiptFrom =
                context.resources.getString(R.string.total_receipt_collect_from_shg_are_urdu) + "\n\n"
            notreceiptFrom =
                context.resources.getString(R.string.receipt_not_receive_from_shg_urdu) + "\n\n"
            paymentFrom =
                context.resources.getString(R.string.total_payment_to_shg_are_rupee_urdu) + "\n\n"
            totalpaymentSentence =
                context.resources.getString(R.string.total_payment_are_urdu) + "\n\n"
            totalCollectionSentence = context.resources.getString(R.string.sentence14_urdu) + "\n\n"

            notpaymentFrom =
                context.resources.getString(R.string.total_payment_not_receive_urdu) + "\n\n"

            cash_balanceSentence = context.resources.getString(R.string.sentence20_urdu) + "\n\n"
            cash_debitSentence = context.resources.getString(R.string.sentence16_urdu) + "\n\n"
            cash_creditSentence = context.resources.getString(R.string.sentence15_urdu) + "\n\n"
            bank_balanceSentence = context.resources.getString(R.string.sentence19_urdu) + "\n\n"
            bank_debitSentence = context.resources.getString(R.string.sentence18_urdu) + "\n\n"
            bank_creditSentence = context.resources.getString(R.string.sentence17_urdu) + "\n\n"

            for (i in 0 until MemberList.size) {

                if (MemberList != null || MemberList.size > 0) {
                    if (MemberList.get(i).attendanceOther!!.toInt() == 0) {
                        var a =
                            "'" + MemberList.get(i).childCboName!!.toLowerCase() + "' " + context.resources.getString(
                                R.string.sentence5_urdu
                            )

                        AbsentattendanceSentence =
                            AbsentattendanceSentence + absent_count.toString() + ". " + a + "\n"
                        absent_count++
                    }
                }
            }

            if (!AbsentattendanceSentence.isNullOrEmpty()) {
                AbsentattendanceSentence = context.resources.getString(
                    R.string.shg_absent_for_meeting_those_are_urdu
                ) + "\n\n" + AbsentattendanceSentence
            }

        }


        var date = Date()
        val date_format = SimpleDateFormat("dd-MM-yyyy")
        try {
            date = date_format.parse(MeetingDate)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val c = Calendar.getInstance()
        c.time = date
        var dayIndex: Int = c.get(Calendar.DAY_OF_WEEK)

        var dayData = days[dayIndex - 1]

        summarySentence = summarySentence.replace("SHG_Name", "'" + SHG_name + "'")
        summarySentence = summarySentence.replace("SHG_DATE", MeetingDate)
        summarySentence = summarySentence.replace("SHG_DAY", dayData)
        summarySentence = summarySentence.replace("MEET_NO", meetNo.toString())

        attendanceSentence = attendanceSentence.replace("VO_TOTAL", "'" + totalMember + "'")
        attendanceSentence = attendanceSentence.replace("VO_PRESENT", "'" + presentMember + "'")
        totalreceiptSentence = totalreceiptSentence.replace("VO_AMOUNT", "'" + totalrecepit + "'")
        totalpaymentSentence = totalpaymentSentence.replace("VO_AMOUNT", "'" + totalpayment + "'")

        if (receipt_shg.toInt() > 0) {
            receiptFromShgSentence = receiptFrom
            receiptFromShgSentence = receiptFromShgSentence.replace("SHG", "'" + "SHG" + "'")
            receiptFromShgSentence =
                receiptFromShgSentence.replace("VO_AMOUNT", "'" + receipt_shg + "'")
        } else {
            receiptFromShgSentence = notreceiptFrom
            receiptFromShgSentence = receiptFromShgSentence.replace("SHG", "'" + "SHG" + "'")
        }

        if (receipt_clf.toInt() > 0) {
            receiptFromClfSentence = receiptFrom
            receiptFromClfSentence = receiptFromClfSentence.replace("SHG", "'" + "CLF" + "'")
            receiptFromClfSentence =
                receiptFromClfSentence.replace("VO_AMOUNT", "'" + receipt_clf + "'")
        } else {
            receiptFromClfSentence = notreceiptFrom
            receiptFromClfSentence = receiptFromClfSentence.replace("SHG", "'" + "CLF" + "'")
        }

        if (receipt_srlm.toInt() > 0) {
            receiptFromSrlmSentence = receiptFrom
            receiptFromSrlmSentence = receiptFromSrlmSentence.replace("SHG", "'" + "SRLM" + "'")
            receiptFromSrlmSentence =
                receiptFromSrlmSentence.replace("VO_AMOUNT", "'" + receipt_srlm + "'")
        } else {
            receiptFromSrlmSentence = notreceiptFrom
            receiptFromSrlmSentence = receiptFromSrlmSentence.replace("SHG", "'" + "SRLM" + "'")
        }

        if (receipt_other.toInt() > 0) {
            receiptFromOtherSentence = receiptFrom
            receiptFromOtherSentence = receiptFromOtherSentence.replace("SHG", "'" + "Other" + "'")
            receiptFromOtherSentence =
                receiptFromOtherSentence.replace("VO_AMOUNT", "'" + receipt_other + "'")
        } else {
            receiptFromOtherSentence = notreceiptFrom
            receiptFromOtherSentence = receiptFromOtherSentence.replace("SHG", "'" + "Other" + "'")
        }
//-------------

        if (payment_shg.toInt() > 0) {
            paymentFromShgSentence = paymentFrom
            paymentFromShgSentence = paymentFromShgSentence.replace("SHG", "'" + "SHG" + "'")
            paymentFromShgSentence =
                paymentFromShgSentence.replace("VO_AMOUNT", "'" + payment_shg + "'")
        } else {
            paymentFromShgSentence = notpaymentFrom
            paymentFromShgSentence = paymentFromShgSentence.replace("SHG", "'" + "SHG" + "'")
        }

        if (payment_clf.toInt() > 0) {
            paymentFromClfSentence = paymentFrom
            paymentFromClfSentence = paymentFromClfSentence.replace("SHG", "'" + "CLF" + "'")
            paymentFromClfSentence =
                paymentFromClfSentence.replace("VO_AMOUNT", "'" + payment_clf + "'")
        } else {
            paymentFromClfSentence = notpaymentFrom
            paymentFromClfSentence = paymentFromClfSentence.replace("SHG", "'" + "CLF" + "'")
        }

        if (payment_other.toInt() > 0) {
            paymentFromOtherSentence = paymentFrom
            paymentFromOtherSentence = paymentFromOtherSentence.replace("SHG", "'" + "Other" + "'")
            paymentFromOtherSentence =
                paymentFromOtherSentence.replace("VO_AMOUNT", "'" + payment_other + "'")
        } else {
            paymentFromOtherSentence = notpaymentFrom
            paymentFromOtherSentence = paymentFromOtherSentence.replace("SHG", "'" + "Other" + "'")
        }

        cash_balanceSentence =
            cash_balanceSentence.replace("SHG_AMOUNT", "= '" + cash_balance.toString() + "= '")
        cash_debitSentence =
            cash_debitSentence.replace("SHG_AMOUNT", "= '" + cash_debit_balance.toString() + "= '")

        cash_creditSentence = cash_creditSentence.replace(
            "SHG_AMOUNT",
            "= '" + cash_credit_balance.toString() + "= '"
        )
        bank_balanceSentence =
            bank_balanceSentence.replace("SHG_AMOUNT", "= '" + bank_balance.toString() + "= '")

        bank_debitSentence =
            bank_debitSentence.replace("SHG_AMOUNT", "= '" + bank_debit_balance.toString() + "= '")
        bank_creditSentence =
            bank_creditSentence.replace(
                "SHG_AMOUNT",
                "= '" + bank_credit_balance.toString() + "= '"
            )


        var finalData =
            sentence1 + "@#@" + summarySentence + "@#@" + attendanceSentence + "@#@" + AbsentattendanceSentence + "@#@" + totalreceiptSentence + "@#@" + receiptFromShgSentence + "@#@" + receipt_memList + "@#@" + receiptFromClfSentence + "@#@" + receiptFromSrlmSentence + "@#@" + receiptFromOtherSentence + "@#@" + totalpaymentSentence + "@#@" + paymentFromShgSentence + "@#@" + payment_memList + "@#@" +
                    paymentFromClfSentence + "@#@" + paymentFromOtherSentence + "@#@" + totalCollectionSentence + "@#@" +
                    cash_balanceSentence + "@#@" + cash_debitSentence + "@#@" + cash_creditSentence + "@#@" +
                    bank_balanceSentence + "@#@" + bank_debitSentence + "@#@" + bank_creditSentence
        return finalData
    }



}


