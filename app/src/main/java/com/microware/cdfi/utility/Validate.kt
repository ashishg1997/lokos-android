package com.microware.cdfi.utility

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.os.Build
import android.provider.Settings
import android.text.InputFilter
import android.text.TextUtils
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import com.google.gson.Gson
import com.microware.cdfi.R
import com.microware.cdfi.api.NetworkConnectionInterceptor
import com.microware.cdfi.api.meetingmodel.FundTypeModel
import com.microware.cdfi.api.meetingmodel.mcpDataListModel
import com.microware.cdfi.api.model.EcScModelJoindata
import com.microware.cdfi.entity.*
import com.microware.cdfi.entity.voentity.VomtgEntity
import com.microware.cdfi.viewModel.*
import com.mukesh.OtpView
import kotlinx.android.synthetic.main.dialoge.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern
import kotlin.system.exitProcess


class Validate(context: Context) {

    val MyPREFERENCES = "CDFISP"
    lateinit var sharedpreferences: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    lateinit var context: Context
    lateinit var myLocale: Locale
    internal var datepic: Dialog? = null

    init {
        this.context = context
        sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE)
        editor = sharedpreferences.edit()
    }

    fun SaveSharepreferenceString(key: String, Value: String) {
        editor.putString(key, Value)
        editor.commit()
    }

    fun RetriveSharepreferenceString(key: String): String? {
        return sharedpreferences.getString(key, "")
    }

    fun SaveSharepreferenceInt(key: String, iValue: Int) {
        editor.putInt(key, iValue)
        editor.commit()
    }
    fun SaveSharepreferenceLong(key: String, iValue: Long) {
        editor.putLong(key, iValue)
        editor.commit()
    }

    fun RetriveSharepreferenceInt(key: String): Int {
        return sharedpreferences.getInt(key, 0)
    }
    fun RetriveSharepreferenceLong(key: String): Long {
        return sharedpreferences.getLong(key, 0)
    }


    fun RemoveSharepreference(key: String) {
        editor.remove(key)
    }

    fun ClearSharedPrefrence() {
        editor.clear()
        editor.commit()
    }

    fun ReturnVersionCode(): Int {
        var pinfo: PackageInfo? = null
        try {
            pinfo = context.packageManager.getPackageInfo(context.packageName, 0)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        val iVersionCode = pinfo!!.versionCode
        val sVersionName = pinfo.versionName
        return iVersionCode
    }

    fun random(): String {
        val chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".toCharArray()
        val sb = StringBuilder()
        val random = Random()
        for (i in 0..29) {
            val c = chars[random.nextInt(chars.size)]
            sb.append(c)
        }
        val dateFormat = SimpleDateFormat("yyyyMMddHHmmssSS", Locale.US)
        val date = Date()
        val SDateString = dateFormat.format(date)
        return sb.toString() + SDateString
    }
    fun randomtransactionno(): String {
        val dateFormat = SimpleDateFormat("yyyyMMddHHmmssSS", Locale.US)
        val SDateString = dateFormat.format(Date())
        return  SDateString
    }

    @SuppressLint("ResourceType")
    fun dynamicRadio(context: Context, radioGroup: RadioGroup, values: Array<String?>) {

        for (i in values) {
            val radioButton1 = RadioButton(context)
            radioButton1.layoutParams =
                LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
            radioButton1.text = i
            radioButton1.id = values.indexOf(i)
            val params = RadioGroup.LayoutParams(context, null)
            params.setMargins(0, 0, 100, 0)
            radioButton1.layoutParams = params
            if (radioGroup != null) {
                radioGroup.addView(radioButton1)
                radioGroup.setOnCheckedChangeListener { group, checkedId ->

                }
            }
        }

    }


    fun CheckNumbers(input: String?): Boolean {
        for (ctr in 0 until input!!.length) {
            return if ("1234567890".contains(Character.valueOf(input[ctr]).toString())) {
                continue
            } else {
                false
            }
        }
        return true
    }

    fun returnRoleValue(myString: String?): Int {
        var iValue = 0
        if (myString != null && !myString.equals("null", ignoreCase = true) && myString.length > 0) {
            if (CheckNumbers(myString)==true) {
                iValue = Integer.valueOf(myString)
            }
        }
        return iValue

    } fun returnIntegerValue(myString: String?): Int {
        var iValue = 0
        if (myString != null && !myString.equals("null", ignoreCase = true) && myString.length > 0) {
            //   if (CheckNumbers(myString)==true) {
            iValue = Integer.valueOf(myString)

        }
        return iValue

    }
    fun returnLongValue(myString: String?): Long {
        var iValue:Long = 0
        if (myString != null && !myString.equals(
                "null",
                ignoreCase = true
            ) && myString.length > 0
        ) {
            //      if (CheckNumbers(myString)==true) {
            iValue = myString.toLong()
        }
        return iValue

    }

    fun returnDoubleValue(myString: String?): Double {
        var iValue = 0.0
        if (myString != null && !myString.equals("null",ignoreCase = true) && myString.length > 0 && !myString.equals(".")) {
            iValue = myString.toDouble()
        }
        return iValue

    }

    fun returnMasterBankpos(id: Int?, data: List<BankEntity>?): Int {

        var pos = 0
        if (!data.isNullOrEmpty()) {
            if (id!! > 0) {
                for (i in data.indices) {
                    if (id == returnIntegerValue(data.get(i).bank_id.toString()))
                        pos = i + 1
                }
            }
        }
        return pos
    }

    fun returnStringValue(myString: String?): String {
        var iValue = ""
        if (myString != null && !myString.equals(
                "null",
                ignoreCase = true
            ) && myString.length > 0
        ) {
            iValue = myString
        }
        return iValue

    }
    fun returnStringcapitalize(myString: String?): String {
        var iValue = ""
        if (myString != null && !myString.equals(
                "null",
                ignoreCase = true
            ) && myString.length > 0
        ) {
            iValue = myString.substring(0, 1).toUpperCase(Locale.US) + myString.substring(1)
        }
        return iValue

    }

    fun returndoubleValue(myString: String?): Double {
        var iValue = 0.0
        if (myString != null && !myString.equals(
                "null",
                ignoreCase = true
            ) && myString.length > 0
        ) {
            iValue = myString.toDouble()
        }
        return iValue

    }

    /* val currentdate: String
         get() {
             val sdf = SimpleDateFormat("dd-MM-yyyy")
             return sdf.format(Date())
         }*/
    val currentdatetime: String
        get() {
            val sdf = SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.US)

            return sdf.format(Date())
        }

    val currentdate: String
        get() {
            val sdf = SimpleDateFormat("dd-MM-yyyy", Locale.US)

            return sdf.format(Date())
        }

    fun ReturnJson(
        abc: Array<ArrayList<*>>,
        TableName: Array<String>,
        jsonObjectcombined: JSONObject
    ): String {

        for (i in TableName.indices) {
            try {
                if (abc[i].size > 0) {
                    val json1 = Gson().toJson(abc[i])
                    val JSONCrosscheck = JSONArray(json1)
                    jsonObjectcombined.put(
                        TableName[i],
                        JSONCrosscheck
                    )
                } else {
                    val otherJsonArray = JSONArray()
                    jsonObjectcombined.put(TableName[i], otherJsonArray)

                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        return jsonObjectcombined.toString()
    }

    fun CustomAlertEditText(str: String, activity: Activity, text: TextView) {
        val mDialogView = LayoutInflater.from(activity).inflate(R.layout.dialoge, null)
        val mBuilder = AlertDialog.Builder(activity)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str
        mDialogView.btn_ok.text = LabelSet.getText("ok",R.string.ok)
        mDialogView.btn_ok.setOnClickListener {

            mAlertDialog.dismiss()
            text.text = ""
            text.requestFocus()
            text.performClick()


        }
    }

    fun CustomAlert(str: String, activity: Activity, java: Class<*>) {
        val mDialogView =
            LayoutInflater.from(activity).inflate(R.layout.customealertdialoge, null)
        val mBuilder = AlertDialog.Builder(activity)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str
        mDialogView.btn_ok.text = LabelSet.getText("ok",R.string.ok)
        mDialogView.btn_ok.setOnClickListener {

            val intent = Intent(context, java)
            activity.startActivity(intent)
            activity.finish()
            mAlertDialog.dismiss()


        }
    }

    fun CustomAlert(str: String, activity: Activity) {
        val mDialogView =
            LayoutInflater.from(activity).inflate(R.layout.customealertdialoge, null)
        val mBuilder = AlertDialog.Builder(activity)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str
        mDialogView.btn_ok.text = LabelSet.getText("ok",R.string.ok)
        mDialogView.btn_ok.setOnClickListener {


            mAlertDialog.dismiss()


        }
    }
    fun CustomAlertVO(str: String, activity: Activity, java: Class<*>) {
        val mDialogView =
            LayoutInflater.from(activity).inflate(R.layout.customealertdialoge, null)
        val mBuilder = AlertDialog.Builder(activity)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.btn_ok.setBackgroundColor(activity.resources.getColor(R.color.colorPrimary1))
        mDialogView.txt_msg.text = str
        mDialogView.btn_ok.text = LabelSet.getText("ok",R.string.ok)
        mDialogView.btn_ok.setOnClickListener {

            val intent = Intent(context, java)
            activity.startActivity(intent)
            activity.finish()
            mAlertDialog.dismiss()


        }
    }

    fun CustomAlertVO(str: String, activity: Activity) {
        val mDialogView =
            LayoutInflater.from(activity).inflate(R.layout.customealertdialoge, null)
        val mBuilder = AlertDialog.Builder(activity)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.btn_ok.setBackgroundColor(activity.resources.getColor(R.color.colorPrimary1))
        mDialogView.txt_msg.text = str
        mDialogView.btn_ok.text = LabelSet.getText("ok",R.string.ok)
        mDialogView.btn_ok.setOnClickListener {


            mAlertDialog.dismiss()


        }
    }

    fun CustomAlertOtp(str: String, activity: Activity, otpView: OtpView) {
        val mDialogView =
            LayoutInflater.from(activity).inflate(R.layout.customealertdialoge, null)
        val mBuilder = AlertDialog.Builder(activity)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mAlertDialog.setCanceledOnTouchOutside(false)
        mDialogView.txt_msg.text = str
        mDialogView.btn_ok.text = LabelSet.getText("ok",R.string.ok)
        mDialogView.btn_ok.setOnClickListener {

            otpView.setText("")
            otpView.requestFocus()
            mAlertDialog.dismiss()


        }
    }

    fun datePicker(date1: EditText) {
        val myCalendar = Calendar.getInstance()
        // myCalendar.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"))
        val date =
            OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // TODO Auto-generated method stub
                myCalendar[Calendar.YEAR] = year
                myCalendar[Calendar.MONTH] = monthOfYear
                myCalendar[Calendar.DAY_OF_MONTH] = dayOfMonth
                val myFormat = "dd-MM-yyyy" // your format
                val sdf =
                    SimpleDateFormat(myFormat, Locale.getDefault())
                //sdf.setTimeZone(TimeZone.getTimeZone("IST"))
                date1.setText(sdf.format(myCalendar.time))
            }
        if(date1.text.toString().length==0){
            val dpDialog = DatePickerDialog(
                context,
                date,
                myCalendar[Calendar.YEAR],
                myCalendar[Calendar.MONTH],
                myCalendar[Calendar.DAY_OF_MONTH]
            )
            dpDialog.show()

        }else{
            var datelist=date1.text.toString().split("-")
            if (datelist.size==3){
                val dpDialog = DatePickerDialog(
                    context,
                    date,
                    datelist[2].toInt(),
                    datelist[1].toInt() - 1,
                    datelist[0].toInt()
                )
                dpDialog.show()
                dpDialog.datePicker.maxDate = myCalendar.timeInMillis
            }else{
                val dpDialog = DatePickerDialog(
                    context,
                    date,
                    myCalendar[Calendar.YEAR],
                    myCalendar[Calendar.MONTH],
                    myCalendar[Calendar.DAY_OF_MONTH]
                )
                dpDialog.show()
                dpDialog.datePicker.maxDate = myCalendar.timeInMillis
            }

        }

    }
    fun datePickerwithmindate(sec: Long, date1: EditText) {
        val myCalendar = Calendar.getInstance()
        val date =
            OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // TODO Auto-generated method stub
                myCalendar[Calendar.YEAR] = year
                myCalendar[Calendar.MONTH] = monthOfYear
                myCalendar[Calendar.DAY_OF_MONTH] = dayOfMonth
                val myFormat = "dd-MM-yyyy" // your format
                val sdf =
                    SimpleDateFormat(myFormat, Locale.getDefault())
                date1.setText(sdf.format(myCalendar.time))
            }

        if(date1.text.toString().length==0){
            val dpDialog = DatePickerDialog(
                context,
                date,
                myCalendar[Calendar.YEAR],
                myCalendar[Calendar.MONTH],
                myCalendar[Calendar.DAY_OF_MONTH]
            )
            dpDialog.show()
            dpDialog.datePicker.minDate = sec * 1000
            dpDialog.datePicker.maxDate = myCalendar.timeInMillis
        }else {
            var datelist = date1.text.toString().split("-")
            if (datelist.size == 3) {
                val dpDialog = DatePickerDialog(
                    context,
                    date,
                    datelist[2].toInt(),
                    datelist[1].toInt() - 1,
                    datelist[0].toInt()
                )
                dpDialog.show()
                dpDialog.datePicker.minDate = sec * 1000
                dpDialog.datePicker.maxDate = myCalendar.timeInMillis
            } else {
                val dpDialog = DatePickerDialog(
                    context,
                    date,
                    myCalendar[Calendar.YEAR],
                    myCalendar[Calendar.MONTH],
                    myCalendar[Calendar.DAY_OF_MONTH]
                )
                dpDialog.show()
                dpDialog.datePicker.minDate = sec * 1000
                dpDialog.datePicker.maxDate = myCalendar.timeInMillis
            }

        }
    }
    fun datePickerwithminmaxdate(minsec: Long,maxsec: Long, date1: EditText) {
        val myCalendar = Calendar.getInstance()
        val date =
            OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // TODO Auto-generated method stub
                myCalendar[Calendar.YEAR] = year
                myCalendar[Calendar.MONTH] = monthOfYear
                myCalendar[Calendar.DAY_OF_MONTH] = dayOfMonth
                val myFormat = "dd-MM-yyyy" // your format
                val sdf =
                    SimpleDateFormat(myFormat, Locale.getDefault())
                date1.setText(sdf.format(myCalendar.time))
            }

        if(date1.text.toString().length==0){
            val dpDialog = DatePickerDialog(
                context,
                date,
                myCalendar[Calendar.YEAR],
                myCalendar[Calendar.MONTH],
                myCalendar[Calendar.DAY_OF_MONTH]
            )
            dpDialog.show()
            dpDialog.datePicker.minDate = minsec * 1000
            dpDialog.datePicker.maxDate = maxsec * 1000
        }else {
            var datelist = date1.text.toString().split("-")
            if (datelist.size == 3) {
                val dpDialog = DatePickerDialog(
                    context,
                    date,
                    datelist[2].toInt(),
                    datelist[1].toInt() - 1,
                    datelist[0].toInt()
                )
                dpDialog.show()
                dpDialog.datePicker.minDate = minsec * 1000
                dpDialog.datePicker.maxDate = maxsec * 1000
            } else {
                val dpDialog = DatePickerDialog(
                    context,
                    date,
                    myCalendar[Calendar.YEAR],
                    myCalendar[Calendar.MONTH],
                    myCalendar[Calendar.DAY_OF_MONTH]
                )
                dpDialog.show()
                dpDialog.datePicker.minDate = minsec * 1000
                dpDialog.datePicker.maxDate = maxsec * 1000
            }

        }
    }

    fun datePickerwithmindate1(sec: Long, date1: EditText) {
        val myCalendar = Calendar.getInstance()
        val date =
            OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // TODO Auto-generated method stub
                myCalendar[Calendar.YEAR] = year
                myCalendar[Calendar.MONTH] = monthOfYear
                myCalendar[Calendar.DAY_OF_MONTH] = dayOfMonth
                val myFormat = "dd-MM-yyyy" // your format
                val sdf =
                    SimpleDateFormat(myFormat, Locale.getDefault())
                date1.setText(sdf.format(myCalendar.time))
            }

        if(date1.text.toString().length==0){
            val dpDialog = DatePickerDialog(
                context,
                date,
                myCalendar[Calendar.YEAR],
                myCalendar[Calendar.MONTH],
                myCalendar[Calendar.DAY_OF_MONTH]
            )
            dpDialog.show()
            dpDialog.datePicker.minDate = sec * 1000
            //   dpDialog.getDatePicker().setMaxDate(myCalendar.getTimeInMillis())
        }else {
            var datelist = date1.text.toString().split("-")
            if (datelist.size == 3) {
                val dpDialog = DatePickerDialog(
                    context,
                    date,
                    datelist[2].toInt(),
                    datelist[1].toInt() - 1,
                    datelist[0].toInt()
                )
                dpDialog.show()
                dpDialog.datePicker.minDate = sec * 1000
                //   dpDialog.datePicker.maxDate = myCalendar.timeInMillis
            } else {
                val dpDialog = DatePickerDialog(
                    context,
                    date,
                    myCalendar[Calendar.YEAR],
                    myCalendar[Calendar.MONTH],
                    myCalendar[Calendar.DAY_OF_MONTH]
                )
                dpDialog.show()
                dpDialog.datePicker.minDate = sec * 1000
                //    dpDialog.datePicker.maxDate = myCalendar.timeInMillis
            }

        }
    }

    fun futureDatePicker(date1: EditText) {
        val myCalendar = Calendar.getInstance()
        val date =
            OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // TODO Auto-generated method stub
                myCalendar[Calendar.YEAR] = year
                myCalendar[Calendar.MONTH] = monthOfYear
                myCalendar[Calendar.DAY_OF_MONTH] = dayOfMonth
                val myFormat = "dd-MM-yyyy" // your format
                val sdf =
                    SimpleDateFormat(myFormat, Locale.getDefault())
                date1.setText(sdf.format(myCalendar.time))
            }

        if(date1.text.toString().length==0){
            val dpDialog = DatePickerDialog(
                context,
                date,
                myCalendar[Calendar.YEAR],
                myCalendar[Calendar.MONTH],
                myCalendar[Calendar.DAY_OF_MONTH]
            )
            dpDialog.show()
         //   dpDialog.datePicker.minDate = sec * 1000
            //   dpDialog.getDatePicker().setMaxDate(myCalendar.getTimeInMillis())
        }else {
            var datelist = date1.text.toString().split("-")
            if (datelist.size == 3) {
                val dpDialog = DatePickerDialog(
                    context,
                    date,
                    datelist[2].toInt(),
                    datelist[1].toInt() - 1,
                    datelist[0].toInt()
                )
                dpDialog.show()
            //    dpDialog.datePicker.minDate = sec * 1000
                //   dpDialog.datePicker.maxDate = myCalendar.timeInMillis
            } else {
                val dpDialog = DatePickerDialog(
                    context,
                    date,
                    myCalendar[Calendar.YEAR],
                    myCalendar[Calendar.MONTH],
                    myCalendar[Calendar.DAY_OF_MONTH]
                )
                dpDialog.show()
             //   dpDialog.datePicker.minDate = sec * 1000
                //    dpDialog.datePicker.maxDate = myCalendar.timeInMillis
            }

        }
    }
    fun CustomAlertSpinner(activity: Activity, spin: Spinner, msg: String) {
        val mDialogView = LayoutInflater.from(activity).inflate(R.layout.dialoge, null)
        val mBuilder = AlertDialog.Builder(activity)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = msg
        mDialogView.btn_ok.text = LabelSet.getText("ok",R.string.ok)
        mDialogView.btn_ok.setOnClickListener {

            mAlertDialog.dismiss()
            spin.setSelection(0)
            spin.performClick()
            spin.requestFocus()

        }
    }

    fun CustomAlertRadio(activity: Activity, msg: String, rg: RadioGroup) {
        val mDialogView = LayoutInflater.from(activity).inflate(R.layout.dialoge, null)
        val mBuilder = AlertDialog.Builder(activity)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = msg
        mDialogView.btn_ok.text = LabelSet.getText("ok",R.string.ok)
        mDialogView.btn_ok.setOnClickListener {

            mAlertDialog.dismiss()
            rg.requestFocus()

        }
    }

    fun checkmobileno(mobileno: String): Int {

        if (mobileno != null && !mobileno.equals("null") && mobileno.length > 0) {
            var pattern = Pattern.compile("[6-9]{1}[0-9]{9}")
            var matcher = pattern.matcher(mobileno)
            // Check if pattern matches
            if (matcher.matches()) {
                return 1
            } else {
                return 0
            }
        }
        return 0

    }

    fun isLocationEnabled(context: Context): Int {
        var locationMode = 0
        val locationProviders: String

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(
                    context.contentResolver,
                    Settings.Secure.LOCATION_MODE
                )

            } catch (e: Settings.SettingNotFoundException) {
                e.printStackTrace()
                return 0
            }

            return locationMode

        } else {
            locationProviders =
                Settings.Secure.getString(
                    context.contentResolver,
                    Settings.Secure.LOCATION_PROVIDERS_ALLOWED
                )

            return if (!TextUtils.isEmpty(locationProviders)) {
                1
            } else {
                0
            }
        }


    }

    fun changeDateFormat(Dt: String?): String {
        var Value = ""
        if (Dt != null && !Dt.equals("null", ignoreCase = true) && Dt.length > 0) {
            val Cdt = Dt.split("-".toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()
            var DD = ""
            var MM = ""
            var YYYY = ""
            if(Cdt[0].length==4){
                YYYY= Cdt[0]
                MM = Cdt[1]
                DD = Cdt[2]
            }else{
                DD= Cdt[0]
                MM = Cdt[1]
                YYYY = Cdt[2]
            }

            val date =  DD+ "-" + MM + "-" + YYYY
            Value = date
        }
        return Value
    }

    /*fun Daybetween(date2: String?): Long {
        var Value: String? = ""
        if (date2 != null && !date2.equals("null", ignoreCase = true) && date2.length > 0) { *//*String[] Cdt = date2.split("-");
            String DD = Cdt[2];
            String MM = Cdt[1];
            String YYYY = Cdt[0];
            String date = DD + "-" + MM + "-" + YYYY;*//*
            Value = date2
        }
        val date = Value
        val olddate = "01-01-1900"
        var days: Long = 0
        return if (olddate != null && olddate.length > 0 && date != null && date.length > 0) {
            val sdf = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
            var Date1: Date? = null
            var Date2: Date? = null
            try {
                Date1 = sdf.parse(olddate)
                Date2 = sdf.parse(date)
                days = (Date2.getTime() - Date1.getTime()) / (24 * 60 * 60 * 1000)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            days
        } else {
            days
        }
    }*/

    fun Daybetweentime(date2: String?): Long {
        var Value: String? = ""
        if (date2 != null && !date2.equals("null", ignoreCase = true) && date2.length > 0) {

            if(date2.length<16){
                Value = date2+" 00:00:00"
            }else{
                Value = date2
            }
        }
        val date = Value
        val olddate = "01-01-1970 00:00:00"
        var Seconds: Long = 0

        return if (olddate != null && olddate.length > 0 && date != null && date.length > 0) {
            val sdf = SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.US)
            var Date1: Date? = null
            var Date2: Date? = null
            try {
                Date1 = sdf.parse(olddate)
                Date2 = sdf.parse(date)
                Seconds = (Date2.getTime() - Date1.getTime()) / 1000
                // Seconds = (int) Math. round (value);
            } catch (e: Exception) {
                e.printStackTrace()
            }
            Seconds
        } else {
            Seconds
        }
    }

    fun getage(date1: String?, date2: String?): Int {
        var Value1: String? = ""
        var Value2: String? = ""
        if (!date2.isNullOrEmpty()) {
            Value2 = date2+" 00:00:00"
        }
        if (!date1.isNullOrEmpty()) {
            Value1 = date1
        }
        val date = Value1
        val olddate = Value2
        var yr:Long = 0

        if (olddate != null && olddate.length > 0 && date != null && date.length > 0) {
            val sdf = SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.US)
            var Date1: Date? = null
            var Date2: Date? = null
            try {
                Date1 = sdf.parse(olddate)
                Date2 = sdf.parse(date)
                yr = (Date2.getTime() - Date1.getTime()) / ( 60 * 60 * 24 * 365)
                yr=yr/1000
                // Seconds = (int) Math. round (value);
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return yr.toInt()
        } else {
            return  yr.toInt()
        }
    }
    fun getday(date1: String?, date2: String?): Int {
        var Value1: String? = ""
        var Value2: String? = ""
        if (!date2.isNullOrEmpty()) {
            if(date2.length<16){
                Value2 = date2+" 00:00:00"
            }else{
                Value2 = date2
            }
        }
        if (!date1.isNullOrEmpty()) {
            if(date1.length<16){
                Value1 = date1+" 00:00:00"
            }else{
                Value1 = date1
            }
        }
        val date = Value1
        val olddate = Value2
        var yr:Long = 0

        if (olddate != null && olddate.length > 0 && date != null && date.length > 0) {
            val sdf = SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.US)
            var Date1: Date? = null
            var Date2: Date? = null
            try {
                Date1 = sdf.parse(olddate)
                Date2 = sdf.parse(date)
                yr = (Date2.getTime() - Date1.getTime()) / ( 60 * 60 * 24 )
                yr=yr/1000
                // Seconds = (int) Math. round (value);
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return yr.toInt()
        } else {
            return  yr.toInt()
        }
    }

    fun getdob(date1: String, yr: Int): String {
        var dob: String = ""
        if (!date1.isNullOrEmpty()) {
            try {
                var dobarr = date1.split("-")
                if (dobarr.size > 2) {
                    var updatedyr = dobarr[2].toInt() - yr
                    dob = dobarr[0] +"-"+ dobarr[1] +"-"+  updatedyr
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        return dob
    }


/*fun convertDate(days: Long): String? {
    val oldDate = "01-01-1900"
    if (days > 0) {
        val sdf = SimpleDateFormat("dd-MM-yyyy")
        val c: Calendar = Calendar.getInstance()
        try {
            c.setTime(sdf.parse(oldDate))
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        c.add(Calendar.DAY_OF_MONTH, days.toInt())
        return sdf.format(c.getTime())
    }
    return ""
}*/

    /* fun convertDate(days: Int): String? {
         val oldDate = "01-01-1900"
         if (days > 0) {
             val sdf = SimpleDateFormat("dd-MM-yyyy")
             val c: Calendar = Calendar.getInstance()
             try {
                 c.setTime(sdf.parse(oldDate))
             } catch (e: ParseException) {
                 e.printStackTrace()
             }
             c.add(Calendar.DAY_OF_MONTH, days)
             return sdf.format(c.getTime())
         }
         return ""
     }*/
    fun convertDate(yr: Int, date: String): String? {
        val oldDate = date
        if (yr > 0) {
            val sdf = SimpleDateFormat("dd-MM-yyyy", Locale.US)
            val c: Calendar = Calendar.getInstance()
            try {
                c.time = sdf.parse(oldDate)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            c.add(Calendar.YEAR, yr)
            return sdf.format(c.time)
        }
        return ""
    }

    fun convertDatetime(days1: Long?): String? {
        val oldDate = "01-01-1970 00:00:00"
        var days= returnLongValue(days1.toString())
        if (days > 0||days < 0) {
            val sdf = SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.US)
            val c: Calendar = Calendar.getInstance()
            try {
                c.time = sdf.parse(oldDate)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            c.timeInMillis = days * 1000
            var date1 = sdf.format(c.time)
            var removetime = date1.split(" ")
            return removetime.get(0)
        }
        return ""
    }

    fun getDate(date1: String, date2: String): Int {
        val sdf = SimpleDateFormat("dd-MM-yyyy", Locale.US)
        try {
            val d2 = sdf.parse(date2)
            val d1 = sdf.parse(date1)
            val cal1 = Calendar.getInstance()
            val cal2 = Calendar.getInstance()
            cal1.time = d1
            cal2.time = d2
            if (cal1.after(cal2)) {
                return 3
            } else if (cal1.before(cal2)) {
                return 1
            }
            if (cal1.equals(cal2)) {
                return 2
            }
        } catch (e: Exception) {
            println(e)
        }

        return 0
    }

    fun returnString_DateValue(myString: String?): String {
        var iValue = ""
        if (myString != null && !myString.equals(
                "null",
                ignoreCase = true
            ) && myString.length > 0 && !myString.equals("1900-01-01", ignoreCase = true)
        ) {
            iValue = myString
        }
        return iValue

    }

    fun fillspinner(activity: Activity, spin: Spinner, data: List<LookupEntity>?) {

        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize)

            for (i in data.indices) {
                sValue[i] = data[i].key_value
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }

    }

    fun fillmemberspinner(activity: Activity, spin: Spinner, data: List<MemberEntity>?) {

        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in data.indices) {
                sValue[i + 1] = data[i].member_name
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText(
                "nomemberavailable",
                R.string.nomemberavailable
            )
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }

    }fun fillmemberDesspinner(activity: Activity, spin: Spinner, data: List<MemberEntity>?) {

        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in data.indices) {
                sValue[i + 1] = data[i].member_name
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText(
                "nomemberAva",
                R.string.nomemberAva
            )
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }

    }

    fun returnlookupcode(spin: Spinner, data: List<LookupEntity>?): Int {

        var pos = spin.selectedItemPosition
        var id = 0

        if (!data.isNullOrEmpty()) {
            if (pos > 0) id = data.get(pos).lookup_code!!
        }
        return id
    }

    fun returnlookupcodepos(id: Int?, data: List<LookupEntity>?): Int {

        var pos = 0
        if (!data.isNullOrEmpty()) {
            if (id!! > 0) {
                for (i in data.indices) {
                    if (id == data.get(i).lookup_code)
                        pos = i
                }
            }
        }
        return pos
    }

    fun returnlookupcodevalue(id: Int?, data: List<LookupEntity>?): String {

        var pos = ""
        if (!data.isNullOrEmpty()) {
            if (id!! > 0) {
                for (i in data.indices) {
                    if (id == data.get(i).lookup_code)
                        pos = data.get(i).key_value!!
                }
            }
        }
        return pos
    }

    fun fillMasterBankspinner(activity: Activity, spin: Spinner, data: List<BankEntity>?) {

        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText(
                "selectbankname",
                R.string.selectbankname
            )
            for (i in data.indices) {
                sValue[i + 1] = data[i].bank_name
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText(
                "selectbankname",
                R.string.selectbankname
            )
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }

    }

    fun returnMasterBankID(spin: Spinner, data: List<BankEntity>?): Int {

        var pos = spin.selectedItemPosition
        var id = 0

        if (!data.isNullOrEmpty()) {
            if (pos > 0) id = returnIntegerValue(data.get(pos - 1).bank_id.toString())
        }
        return id
    }

    fun returnAccountLength(spin: Spinner, data: List<BankEntity>?): String {

        var pos = spin.selectedItemPosition
        var length = ""

        if (!data.isNullOrEmpty()) {
            if (pos > 0) length = returnStringValue(data.get(pos - 1).bank_account_len)
        }
        return length
    }

    fun fillMasterBankBranchspinner(
        activity: Activity,
        spin: Spinner,
        data: List<Bank_branchEntity>?,
        branch_id: Int
    ) {

        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText(
                "selectbranchname",
                R.string.selectbranchname
            )
            for (i in data.indices) {
                sValue[i + 1] = data[i].bank_branch_name +" "+ "-" + " " + data[i].bank_branch_address
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText(
                "selectbranchname",
                R.string.selectbranchname
            )
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }
        /*  if(branch_id>0) {
              spin.setSelection(returnMasterBankBranchpos(branch_id,data))
          }*/
    }

    fun returnMasterBankBranchID(spin: Spinner, data: List<Bank_branchEntity>?): Int {

        var pos = spin.selectedItemPosition
        var id = 0

        if (!data.isNullOrEmpty()) {
            if (pos > 0) id = data.get(pos - 1).bank_branch_id.toInt()
        }
        return id
    }

    fun returnMasterBankBranchpos(id: Int, data: List<Bank_branchEntity>?): Int {

        var pos = 0

        if (!data.isNullOrEmpty()) {
            for (i in data.indices) {
                if (id == data.get(i).bank_branch_id)
                    pos = i + 1
            }
        }
        return pos
    }


    fun returnMasterBankBranchifsc(spin: Spinner, data: List<Bank_branchEntity>?): String {

        var pos = spin.selectedItemPosition
        var id = ""

        if (!data.isNullOrEmpty()) {
            if (pos > 0) id = data.get(pos - 1).ifsc_code!!
        }
        return id
    }

    fun returnMasterBankcode(spin: Spinner, data: List<Bank_branchEntity>?): Int {

        var pos = spin.selectedItemPosition
        var id = 0

        if (!data.isNullOrEmpty()) {
            if (pos > 0) id = returnIntegerValue(data.get(pos - 1).bank_id.toString())
        }
        return id
    }

    fun fillStateSpinner(activity: Activity, spin: Spinner, data: List<StateEntity>?) {
        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)

            for (i in data.indices) {
                sValue[i + 1] = data[i].state_name_en
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }
    }

    fun returnStateID(spin: Spinner, data: List<StateEntity>?): Int {

        var pos = spin.selectedItemPosition
        var id = 0

        if (!data.isNullOrEmpty()) {
            if (pos > 0)
                id = data.get(pos - 1).state_id
        }
        return id
    }

    fun returnStatepos(id: Int?, data: List<StateEntity>?): Int {

        var pos = 0
        if (!data.isNullOrEmpty()) {
            if (id!! > 0) {
                for (i in data.indices) {
                    if (id == data.get(i).state_id)
                        pos = i + 1
                }
            }
        }
        return pos
    }

    fun fillDistrictSpinner(activity: Activity, spin: Spinner, data: List<DistrictEntity>?) {
        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)

            for (i in data.indices) {
                sValue[i + 1] = data[i].district_name_en
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }
    }

    fun returnDistrictID(spin: Spinner, data: List<DistrictEntity>?): Int {

        var pos = spin.selectedItemPosition
        var id = 0

        if (!data.isNullOrEmpty()) {
            if (pos > 0)
                id = data.get(pos - 1).district_id
        }
        return id
    }

    fun returnDistrictpos(id: Int?, data: List<DistrictEntity>?): Int {

        var pos = 0
        if (!data.isNullOrEmpty()) {
            if (id!! > 0) {
                for (i in data.indices) {
                    if (id == data.get(i).district_id)
                        pos = i + 1
                }
            }
        }
        return pos
    }

    fun fillBlockSpinner(activity: Activity, spin: Spinner, data: List<BlockEntity>?) {
        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)

            for (i in data.indices) {
                sValue[i + 1] = data[i].block_name_en
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }
    }

    fun fillCLFBlockSpinner(activity: Activity, spin: Spinner, data: List<BlockEntity>?) {
        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("all", R.string.all)

            for (i in data.indices) {
                sValue[i + 1] = data[i].block_name_en
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }
    }

    fun returnBlockID(spin: Spinner, data: List<BlockEntity>?): Int {

        var pos = spin.selectedItemPosition
        var id = 0

        if (!data.isNullOrEmpty()) {
            if (pos > 0)
                id = data.get(pos - 1).block_id
        }
        return id
    }

    fun returnBlockpos(id: Int?, data: List<BlockEntity>?): Int {

        var pos = 0
        if (!data.isNullOrEmpty()) {
            if (id!! > 0) {
                for (i in data.indices) {
                    if (id == data.get(i).block_id)
                        pos = i + 1
                }
            }
        }
        return pos
    }

    fun fillPanchayatSpinner(activity: Activity, spin: Spinner, data: List<PanchayatEntity>?) {
        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)

            for (i in data.indices) {
                sValue[i + 1] = data[i].panchayat_name_en
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }
    }

    fun fillVOPanchayatSpinner(activity: Activity, spin: Spinner, data: List<PanchayatEntity>?) {
        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("all", R.string.all)

            for (i in data.indices) {
                sValue[i + 1] = data[i].panchayat_name_en
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }
    }

    fun returnPanchayatID(spin: Spinner, data: List<PanchayatEntity>?): Int {

        var pos = spin.selectedItemPosition
        var id = 0

        if (!data.isNullOrEmpty()) {
            if (pos > 0)
                id = data.get(pos - 1).panchayat_id
        }
        return id
    }

    fun returnPanchayatpos(id: Int?, data: List<PanchayatEntity>?): Int {

        var pos = 0
        if (!data.isNullOrEmpty()) {
            if (id!! > 0) {
                for (i in data.indices) {
                    if (id == data.get(i).panchayat_id)
                        pos = i + 1
                }
            }
        }
        return pos
    }


    fun fillVillageSpinner(activity: Activity, spin: Spinner, data: List<VillageEntity>?) {
        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)

            for (i in data.indices) {
                sValue[i + 1] = data[i].village_name_en
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }
    }

    fun returnVillageID(spin: Spinner, data: List<VillageEntity>?): Int {

        var pos = spin.selectedItemPosition
        var id = 0

        if (!data.isNullOrEmpty()) {
            if (pos > 0)
                id = data.get(pos - 1).village_id
        }
        return id
    }

    fun returnVillagepos(id: Int?, data: List<VillageEntity>?): Int {

        var pos = 0
        if (!data.isNullOrEmpty()) {
            if (id!! > 0) {
                for (i in data.indices) {
                    if (id == data.get(i).village_id)
                        pos = i + 1
                }
            }
        }
        return pos
    }

    fun fillradio(Radio: RadioGroup, value: Int, data: List<LookupEntity>?, activity: Activity) {

        Radio.removeAllViews()
        Radio.clearCheck()
        if (!data.isNullOrEmpty()) {
            val rb = arrayOfNulls<RadioButton>(data.size)
            for (i in data.indices) {
                rb[i] = RadioButton(activity)
                rb[i]!!.layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    1f
                )
                // rb[i]!!.setLayoutParams(params)
                Radio.addView(rb[i])
                //                rb[i].setButtonDrawable(R.drawable.radio_check);
                rb[i]!!.text = data.get(i).key_value
                rb[i]!!.id = data.get(i).lookup_code!!
                rb[i]!!.setTextColor(activity.resources.getColor(R.color.black))
                rb[i]!!.setTypeface(Typeface.SANS_SERIF, Typeface.NORMAL)
                rb[i]!!.setTextSize(
                    TypedValue.COMPLEX_UNIT_PX,
                    activity.resources.getDimension(R.dimen.radio)
                )
                Radio.setPadding(0, 5, 30, 5)
                //                }
                if (value == data.get(i).lookup_code) {
                    rb[i]!!.isChecked = true
                } else if (value == 0) {
                    rb[0]?.isChecked = true
                }
            }
        }
    }


  fun fillradioDisabilitiy(Radio: RadioGroup, value: Int, data: List<LookupEntity>?, activity: Activity) {

        Radio.removeAllViews()
        Radio.clearCheck()
        if (!data.isNullOrEmpty()) {
            val rb = arrayOfNulls<RadioButton>(data.size)
            for (i in data.indices) {
                rb[i] = RadioButton(activity)
                rb[i]!!.layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    1f
                )
                // rb[i]!!.setLayoutParams(params)
                Radio.addView(rb[i])
                //                rb[i].setButtonDrawable(R.drawable.radio_check);
                rb[i]!!.text = data.get(i).description
                rb[i]!!.id = data.get(i).lookup_code!!
                rb[i]!!.setTextColor(activity.resources.getColor(R.color.black))
                rb[i]!!.setTypeface(Typeface.SANS_SERIF, Typeface.NORMAL)
                rb[i]!!.setTextSize(
                    TypedValue.COMPLEX_UNIT_PX,
                    activity.resources.getDimension(R.dimen.radio)
                )
                Radio.setPadding(0, 5, 30, 5)
                //                }
                if (value == data.get(i).lookup_code) {
                    rb[i]!!.isChecked = true
                } else if (value == 0) {
                    rb[0]?.isChecked = true
                }
            }
        }
    }

    fun fillradioFinancial(
        Radio: RadioGroup,
        value: Int,
        data: List<LookupEntity>?,
        activity: Activity,
        cboType: Int
    ) {

        Radio.removeAllViews()
        Radio.clearCheck()
        if (!data.isNullOrEmpty()) {
            val rb = arrayOfNulls<RadioButton>(data.size)
            for (i in data.indices) {
                rb[i] = RadioButton(activity)
                rb[i]!!.layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    1f
                )
                // rb[i]!!.setLayoutParams(params)
                Radio.addView(rb[i])
                //                rb[i].setButtonDrawable(R.drawable.radio_check);
                rb[i]!!.text = data.get(i).key_value
                rb[i]!!.id = data.get(i).lookup_code!!
                rb[i]!!.setTextColor(activity.resources.getColor(R.color.black))
                rb[i]!!.setTypeface(Typeface.SANS_SERIF, Typeface.NORMAL)
                rb[i]!!.setTextSize(
                    TypedValue.COMPLEX_UNIT_PX,
                    activity.resources.getDimension(R.dimen.radio)
                )
                Radio.setPadding(0, 5, 30, 5)
                //                }
                if (value == data.get(i).lookup_code) {
                    rb[i]!!.isChecked = true
                } else if (value == 0) {
                    rb[0]?.isChecked = true
                }
                if(cboType ==2) {
                    rb[i]!!.isEnabled = false
                }else if(cboType ==1) {
                    rb[i]!!.isEnabled = true
                }
            }
        }
    }

    fun dynamicRadio(context: Context, data: List<LookupEntity>?, radioGroup: RadioGroup) {

        if (data != null) {

            val iGen = data.size
            val name = arrayOfNulls<String>(iGen)
            for (i in 0 until data.size) {
                val radioButton1 = RadioButton(context)
                radioButton1.layoutParams =
                    LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1f
                    )
                radioButton1.text = data.get(i).key_value
                radioButton1.id = data.get(i).lookup_code!!
                radioButton1.setTextColor(context.resources.getColor(R.color.black))
                radioButton1.setTypeface(Typeface.SANS_SERIF, Typeface.NORMAL)
                radioButton1.setTextSize(
                    TypedValue.COMPLEX_UNIT_PX,
                    context.resources.getDimension(R.dimen.radio)
                )

                if (radioGroup != null) {
                    radioGroup.addView(radioButton1)
                }


            }

        }


    }

    fun GetAnswerTypeRadioButtonID(radioGroup: RadioGroup): Int {
        var ID = 0

        for (i in 0 until radioGroup.childCount) {

            val radioButton1 = radioGroup.getChildAt(i) as RadioButton
            if (radioButton1.isChecked) {
                ID = radioButton1.id

            }
        }
        return ID
    }


    fun SetAnswerTypeRadioButton(radioGroup: RadioGroup, data: Int) {

        for (i in 0 until radioGroup.childCount) {

            val radioButton1 = radioGroup.getChildAt(i) as RadioButton
            if (radioButton1.id == data) {
                radioButton1.isChecked = true
            }
        }
    }

    fun fill_Ec_memberspinner(activity: Activity, spin: Spinner, data: List<VoShgMemberEntity>?) {

        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in data.indices) {
                sValue[i + 1] = data[i].member_name + " "+ "("+" "+ data[i].shg_name+" "+")"
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText(
                "nomemberAva",
                R.string.nomemberAva
            )
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }
    }

    fun fill_ClfEc_memberspinner(activity: Activity, spin: Spinner, data: List<ClfVoMemberEntity>?) {

        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in data.indices) {
                sValue[i + 1] = data[i].member_name + " "+ "("+" "+ data[i].shg_name+" "+")"
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText(
                "nomemberAvail",
                R.string.nomemberAvail
            )
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }
    }

    fun fillShgspinner(activity: Activity, spin: Spinner, data: List<MappedShgEntity>?) {

        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in data.indices) {
                sValue[i + 1] = data[i].shg_name
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText(
                "noshgavailable",
                R.string.noshgavailable
            )
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }

    }

    fun fillVospinner(activity: Activity, spin: Spinner, data: List<MappedVoEntity>?) {

        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in data.indices) {
                sValue[i + 1] = data[i].cbo_child_name
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText(
                "novo_available",
                R.string.novo_available
            )
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }

    }


    fun fillSubcommitteeSpinner(
        activity: Activity,
        spin: Spinner,
        data: List<subcommitee_masterEntity>?
    ) {
        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)

            for (i in data.indices) {
                sValue[i + 1] = data[i].subcommitee_name
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }
    }

    fun returnSubcommitteeID(spin: Spinner, data: List<subcommitee_masterEntity>?): Int {

        var pos = spin.selectedItemPosition
        var id = 0

        if (!data.isNullOrEmpty()) {
            if (pos > 0)
                id = data.get(pos - 1).subcommitee_type_id
        }
        return id
    }

    fun returnSubcommitteeName(id: Int?, data: List<subcommitee_masterEntity>?): String {

        var sName = ""
        if (!data.isNullOrEmpty()) {
            if (id!! > 0) {
                for (i in data.indices) {
                    if (id == data.get(i).subcommitee_type_id)
                        sName = returnStringValue(data.get(i).subcommitee_name)
                }
            }
        }
        return sName
    }

    fun returnSubcommitteePos(id: Int?, data: List<subcommitee_masterEntity>?): Int {

        var pos = 0
        if (!data.isNullOrEmpty()) {
            if (id!! > 0) {
                for (i in data.indices) {
                    if (id == data.get(i).subcommitee_type_id)
                        pos = i + 1
                }
            }
        }
        return pos
    }
    fun sSplitValue(sValue: String?, i: Int): String {
        var Value = ""
        if (sValue != null && !sValue.equals("null", ignoreCase = true) && sValue.length > 0) {
            val Cdt = sValue.split(",".toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()

            Value = Cdt[i]
        }
        return Value
    }
    fun changeLang(lang: String, activity: Activity) {
        if (lang.equals("", ignoreCase = true)) return
        myLocale = Locale(lang)
        saveLocale(lang, activity)
        Locale.setDefault(myLocale)
        val config = Configuration()
        config.locale = myLocale
        activity.baseContext.resources.updateConfiguration(
            config,
            activity.baseContext.resources.displayMetrics
        )
        // updateTexts();
    }

    fun saveLocale(lang: String?, activity: Activity) {
        val langPref = "Language"
        val prefs = activity.getSharedPreferences(
            "CommonPrefs",
            Context.MODE_PRIVATE
        )

        val editor = prefs.edit()
        editor.putString(langPref, lang)
        editor.commit()
    }

    fun fillScMemberSpinner(activity: Activity, spin: Spinner, data: List<EcScModelJoindata>?) {

        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in data.indices) {
                sValue[i + 1] = data[i].member_name
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText(
                "nomemberavailable",
                R.string.nomemberavailable
            )
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }
    }

    fun isNetworkConnected(activity: Activity): Boolean {
        val connectivityManager =
            activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager //1
        val networkInfo = connectivityManager.activeNetworkInfo //2
        return networkInfo != null && networkInfo.isConnected //3
    }
    fun fillScMemberSpinner(
        activity: Activity,
        spin: Spinner,
        data: List<EcScModelJoindata>?,
        defaultString: String
    ) {

        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in data.indices) {
                sValue[i + 1] = data[i].member_name + " "+ "("+" "+ data[i].shg_name+" "+")"
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = defaultString
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }
    }

    fun disableEmojiInTitle(editText: EditText,maxLength:Int) {
        val emojiFilter = InputFilter { source, start, end, dest, dstart, dend ->
            for (index in start until end) {
                val type = Character.getType(source[index])

                when (type) {
                    '*'.toInt(),
                    Character.OTHER_SYMBOL.toInt(),
                    Character.SURROGATE.toInt() -> {
                        return@InputFilter ""
                    }
                    Character.LOWERCASE_LETTER.toInt() -> {
                        val index2 = index + 1
                        if (index2 < end && Character.getType(source[index + 1]) == Character.NON_SPACING_MARK.toInt())
                            return@InputFilter ""
                    }
                    Character.DECIMAL_DIGIT_NUMBER.toInt() -> {
                        val index2 = index + 1
                        val index3 = index + 2
                        if (index2 < end && index3 < end &&
                            Character.getType(source[index2]) == Character.NON_SPACING_MARK.toInt() &&
                            Character.getType(source[index3]) == Character.ENCLOSING_MARK.toInt()
                        )
                            return@InputFilter ""
                    }
                    Character.OTHER_PUNCTUATION.toInt() -> {
                        val index2 = index + 1

                        if (index2 < end && Character.getType(source[index2]) == Character.NON_SPACING_MARK.toInt()) {
                            return@InputFilter ""
                        }
                    }
                    Character.MATH_SYMBOL.toInt() -> {
                        val index2 = index + 1
                        if (index2 < end && Character.getType(source[index2]) == Character.NON_SPACING_MARK.toInt())
                            return@InputFilter ""
                    }
                }

            }
            return@InputFilter null
        }
        editText.filters = arrayOf(emojiFilter,InputFilter.LengthFilter(maxLength))
    }

    fun CustomAlertMsg(
        activity: Activity,
        viewModel: ResponseViewModel?,
        responseCode: Int,
        languageId: String,
        responseMsg: String
    ) {
        var str = ""
        val mDialogView =
            LayoutInflater.from(activity).inflate(R.layout.customealertdialoge, null)
        val mBuilder = AlertDialog.Builder(activity)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        var strMsg = returnStringValue(viewModel!!.getResponseMsg(responseCode, languageId))
        if(!strMsg.isNullOrEmpty()){
            str = viewModel.getResponseMsg(responseCode, languageId)
        }else {
            str = responseMsg
        }
        mDialogView.txt_msg.text = str
        mDialogView.btn_ok.text = LabelSet.getText("ok",R.string.ok)
        mDialogView.btn_ok.setOnClickListener {

            mAlertDialog.dismiss()


        }
    }
    fun CustomAlertMsgVO(
        activity: Activity,
        viewModel: ResponseViewModel?,
        responseCode: Int,
        languageId: String,
        responseMsg: String
    ) {
        var str = ""
        val mDialogView =
            LayoutInflater.from(activity).inflate(R.layout.customealertdialoge, null)
        val mBuilder = AlertDialog.Builder(activity)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.btn_ok.setBackgroundColor(activity.resources.getColor(R.color.colorPrimary1))
        var strMsg = returnStringValue(viewModel!!.getResponseMsg(responseCode, languageId))
        if(!strMsg.isNullOrEmpty()){
            str = viewModel.getResponseMsg(responseCode, languageId)
        }else {
            str = responseMsg
        }
        mDialogView.txt_msg.text = str
        mDialogView.btn_ok.text = LabelSet.getText("ok",R.string.ok)
        mDialogView.btn_ok.setOnClickListener {

            mAlertDialog.dismiss()


        }
    }

    fun alertMsg(
        activity: Activity,
        viewModel: ResponseViewModel?,
        responseCode: Int,
        languageId: String,
        responseMsg: String
    ):String {
        var strMsg = ""

        var str = returnStringValue(viewModel!!.getResponseMsg(responseCode, languageId))
        if(!str.isNullOrEmpty()){
            strMsg = str
        }else {
            strMsg = responseMsg
        }
        return strMsg
    }

    fun checkname(name: String): Boolean {

        if (name != null && !name.equals("null") && name.length > 0) {
            var pattern = Pattern.compile("[A-Za-z ]+[A-Za-z0-9\\u00C0-\\u017F-' ]*\$")
            var matcher = pattern.matcher(name)
            // Check if pattern matches
            return matcher.matches()
        }
        return false

    }

    fun validateTAN(name: String): Boolean {

        if (name != null && !name.equals("null") && name.length > 0) {
            var pattern = Pattern.compile("[A-Za-z]{4}[0-9]{5}[A-Za-z]{1}")
            var matcher = pattern.matcher(name)
            // Check if pattern matches
            return matcher.matches()
        }
        return false

    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun networkCheck(): Boolean {
        var check: Boolean = false
        val networkconnection: NetworkConnectionInterceptor =
            NetworkConnectionInterceptor(context)
        if (networkconnection.isInternetAvailable() == true) {
            check = true
        }
        return check
    }

    fun fillFundingspinner(activity: Activity, spin: Spinner, data: List<FundingEntity>?) {

        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize+1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in 0 until data.size) {
                sValue[i+1] = data.get(i).agency_name
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }

    }

    fun returnFundingcode(spin: Spinner, data: List<FundingEntity>?): Int {

        var pos = spin.selectedItemPosition
        var id = 0

        if (!data.isNullOrEmpty()) {
            if (pos > 0) id = data.get(pos-1).funding_agency_id
        }
        return id
    }

    fun returnFundingpos(id: Int?, data: List<FundingEntity>?): Int {

        var pos = 0
        if (!data.isNullOrEmpty()) {
            if (id!! > 0) {
                for (i in data.indices) {
                    if (id == data.get(i).funding_agency_id)
                        pos = i+1
                }
            }
        }
        return pos
    }
    fun setday(days1: Long?, day: Int): String? {
        val oldDate = "01-01-1970 00:00:00"
        var days = returnLongValue(days1.toString())
        if (days > 0 || days < 0) {
            val sdf = SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.US)
            val c: Calendar = Calendar.getInstance()
            try {
                c.time = sdf.parse(oldDate)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            c.timeInMillis = days * 1000
            c.set(Calendar.DAY_OF_WEEK, day + 1)
            var date1 = sdf.format(c.time)
            var removetime = date1.split(" ")
            return removetime.get(0)
        }
        return ""
    }

    fun addmonth(days1: Long?, month: Int, frequency: Int): Long? {
        val oldDate = "01-01-1970 00:00:00"
        var days = returnLongValue(days1.toString())
        if (days > 0 || days < 0) {
            val sdf = SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.US)
            val c: Calendar = Calendar.getInstance()
            try {
                c.time = sdf.parse(oldDate)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            c.timeInMillis = days * 1000
            //  Log.i("time1",sdf.format(c.getTime()))
            if(frequency==1){
                c.add(Calendar.DATE, month*7)
            }else if(frequency==2){
                c.add(Calendar.DATE, month*14)
            }else{
                c.add(Calendar.MONTH, month)
            }

            // c.set(Calendar.HOUR_OF_DAY, 0)
            //  c.set(Calendar.MI, 0)
            // Log.i("time2",sdf.format(c.getTime()))
            var date1 = sdf.format(c.time)
            var removetime = date1.split(" ")
            return Daybetweentime(removetime.get(0))
        }
        return 0
    }


    fun setdate(days1: Long?, day: Int): String? {
        val oldDate = "01-01-1970 00:00:00"
        var days = returnLongValue(days1.toString())
        if (days > 0 || days < 0) {
            val sdf = SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.US)
            val c: Calendar = Calendar.getInstance()
            try {
                c.time = sdf.parse(oldDate)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            c.timeInMillis = days * 1000
            c.set(Calendar.DAY_OF_MONTH, day)
            var date1 = sdf.format(c.time)
            var removetime = date1.split(" ")
            return removetime.get(0)
        }
        return ""
    }

    fun datePickerwithfuturedate(date1: EditText) {
        val myCalendar = Calendar.getInstance()
        // myCalendar.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"))
        val date =
            OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // TODO Auto-generated method stub
                myCalendar[Calendar.YEAR] = year
                myCalendar[Calendar.MONTH] = monthOfYear
                myCalendar[Calendar.DAY_OF_MONTH] = dayOfMonth
                val myFormat = "dd-MM-yyyy" // your format
                val sdf =
                    SimpleDateFormat(myFormat, Locale.getDefault())
                //sdf.setTimeZone(TimeZone.getTimeZone("IST"))
                date1.setText(sdf.format(myCalendar.time))
            }
        if (date1.text.toString().length == 0) {
            val dpDialog = DatePickerDialog(
                context,
                date,
                myCalendar[Calendar.YEAR],
                myCalendar[Calendar.MONTH],
                myCalendar[Calendar.DAY_OF_MONTH]
            )
            dpDialog.show()
            dpDialog.datePicker.minDate = myCalendar.timeInMillis
        } else {
            var datelist = date1.text.toString().split("-")
            if (datelist.size == 3) {
                val dpDialog = DatePickerDialog(
                    context,
                    date,
                    datelist[2].toInt(),
                    datelist[1].toInt() - 1,
                    datelist[0].toInt()
                )
                dpDialog.show()
                dpDialog.datePicker.maxDate = myCalendar.timeInMillis
            } else {
                val dpDialog = DatePickerDialog(
                    context,
                    date,
                    myCalendar[Calendar.YEAR],
                    myCalendar[Calendar.MONTH],
                    myCalendar[Calendar.DAY_OF_MONTH]
                )
                dpDialog.show()
                dpDialog.datePicker.maxDate = myCalendar.timeInMillis
            }

        }

    }

    fun returnMeetingdate(spin: Spinner, data: List<DtmtgEntity>?): Long {

        var pos = spin.selectedItemPosition
        var id = 0L

        if (!data.isNullOrEmpty()) {
            if (pos > 0)
                id = data.get(pos - 1).mtg_date!!
        }
        return id
    }

    fun returnMeetingNum(spin: Spinner, data: List<DtmtgEntity>?): Int {

        var pos = spin.selectedItemPosition
        var id = 0

        if (!data.isNullOrEmpty()) {
            if (pos > 0)
                id = data.get(pos - 1).mtg_no
        }
        return id
    }

    fun returnMeetingGuid(spin: Spinner, data: List<DtmtgEntity>?): String {

        var pos = spin.selectedItemPosition
        var mtgGuid = ""

        if (!data.isNullOrEmpty()) {
            if (pos > 0)
                mtgGuid = data.get(pos - 1).mtg_guid
        }
        return mtgGuid
    }

    fun fillmeetingspinner(activity: Activity, spin: Spinner, data: List<DtmtgEntity>?) {

        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in data.indices) {
                sValue[i + 1] = data[i].mtg_no.toString()
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }

    }

    fun returnmeetingpos(id: Int?, data: List<DtmtgEntity>?): Int {

        var pos = 0
        if (!data.isNullOrEmpty()) {
            if (id!! > 0) {
                for (i in data.indices) {
                    if (id == data.get(i).mtg_no)
                        pos = i + 1
                }
            }
        }
        return pos
    }


    fun fillCoaSubHeadspinner(activity: Activity, spin: Spinner, data: List<MstCOAEntity>?) {

        val adapter: ArrayAdapter<String?>
        if (data != null) {
            val iGen = data.size
            val sValue = arrayOfNulls<String>(iGen + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)

            for (i in 0 until data.size) {
                sValue[i + 1] = data[i].description
            }
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter

        }else {
            val sValue = arrayOfNulls<String>(data?.size!! + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }

    }

    fun returnSubHeadcode(spin: Spinner, data: List<MstCOAEntity>?): Int {

        var pos = spin.selectedItemPosition
        var id = 0

        if (!data.isNullOrEmpty()) {
            if (pos > 0) id = data.get(pos - 1).uid
        }
        return id
    }



    fun returnSubHeadpos(id: Int?, data: List<MstCOAEntity>?): Int {

        var pos = 0
        if (!data.isNullOrEmpty()) {
            if (id!! > 0) {
                for (i in data.indices) {
                    if (id == data.get(i).uid)
                        pos = i + 1
                }
            }
        }
        return pos
    }

    fun fillfundTypespinner(activity: Activity, spin: Spinner, data: List<FundTypeEntity>?) {

        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in data.indices) {
                sValue[i + 1] = data[i].fund_type
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }

    }

    fun returnfundTypepos(id: Int?, data: List<FundTypeEntity>?): Int {

        var pos = 0
        if (!data.isNullOrEmpty()) {
            if (id!! > 0) {
                for (i in data.indices) {
                    if (id == data.get(i).fund_type_id)
                        pos = i + 1
                }
            }
        }
        return pos
    }

    fun returnfundTypeId(spin: Spinner, data: List<FundTypeEntity>?): Int {

        var pos = spin.selectedItemPosition
        var id = 0

        if (!data.isNullOrEmpty()) {
            if (pos > 0) id = data.get(pos-1).fund_type_id
        }
        return id
    }

    fun fillcoaspinner(activity: Activity, spin: Spinner, data: List<MstCOAEntity>?) {

        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in data.indices) {
                sValue[i + 1] = data[i].description
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }

    }

    fun returncoauidpos(id: Int?, data: List<MstCOAEntity>?): Int {

        var pos = 0
        if (!data.isNullOrEmpty()) {
            if (id!! > 0) {
                for (i in data.indices) {
                    if (id == data.get(i).uid.toInt())
                        pos = i + 1
                }
            }
        }
        return pos
    }

    fun returncoauid(spin: Spinner, data: List<MstCOAEntity>?): Int {

        var pos = spin.selectedItemPosition
        var id = 0

        if (!data.isNullOrEmpty()) {
            if (pos > 0) id = data.get(pos-1).uid.toInt()
        }
        return id
    }

    fun fillMCPspinner(activity: Activity, spin: Spinner, data: List<mcpDataListModel>?,linked_mcp_pos:Int) {

        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("none", R.string.none)
            for (i in data.indices) {
                sValue[i + 1] = data[i].purpose + "-" + data[i].amount.toString()
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText("none", R.string.none)
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }

        spin.setSelection(linked_mcp_pos)
    }

    fun returnMCPpos(id: Long?, data: List<mcpDataListModel>?): Int {

        var pos = 0
        if (!data.isNullOrEmpty()) {
            if (id!! > 0) {
                for (i in data.indices) {
                    if (id == data.get(i).mcpId)
                        pos = i + 1
                }
            }
        }
        return pos
    }

    fun returnMCPid(spin: Spinner, data: List<mcpDataListModel>?): Long {

        var pos = spin.selectedItemPosition
        var id:Long = 0

        if (!data.isNullOrEmpty()) {
            if (pos > 0) id = data.get(pos-1).mcpId
        }
        return id
    }

    fun updateLockingFlag(shgGuid:String?,shgViewModel:SHGViewmodel?,memberviewmodel:Memberviewmodel?) {
        var bankCount = shgViewModel!!.getBankCount(shgGuid)
        var memberCount = memberviewmodel!!.getcount(shgGuid)
        var officeBearerCount = memberviewmodel.getOfficeBearerCount(shgGuid,listOf(1, 2, 3))
        var signatoryCount = shgViewModel.getSignatoryCount(shgGuid)

        if (bankCount == 0 && memberCount >= 5 && officeBearerCount>=2) {
            shgViewModel.updateLockingFlag(0,1,shgGuid)
        }else if(bankCount>0 && memberCount>=5 && officeBearerCount>=2 && signatoryCount>=2){
            shgViewModel.updateLockingFlag(1,1,shgGuid)
        }else {
            shgViewModel.updateLockingFlag(0,0,shgGuid)
        }

    }

    fun updateMemberEditFlag(shgGuid:String?,shgViewModel:SHGViewmodel?,memberviewmodel:Memberviewmodel?) {

        var lockingFlag = shgViewModel!!.getLockingFlag(shgGuid)
        var bankCount = shgViewModel.getBankCount(shgGuid)
        var memberCount = memberviewmodel!!.getcount(shgGuid)
        var officeBearerCount = memberviewmodel.getOfficeBearerCount(shgGuid,listOf(1, 2, 3))
        var signatoryCount = shgViewModel.getSignatoryCount(shgGuid)

        if (bankCount == 0 && memberCount >= 5 && officeBearerCount>=2) {
            shgViewModel.updateMemberLockingFlag(lockingFlag,1,shgGuid)
        }else if(bankCount>0 && memberCount>=5 && officeBearerCount>=2 && signatoryCount>=2){
            shgViewModel.updateMemberLockingFlag(lockingFlag,1,shgGuid)
        }else {
            shgViewModel.updateMemberLockingFlag(lockingFlag,0,shgGuid)
        }

    }

    fun restartApplication(activity: Activity) {
        val pm = activity.packageManager
        val packageName = "com.microware.cdfi"
        val intent = pm.getLaunchIntentForPackage(packageName)
        activity.finishAffinity()
        activity.startActivity(intent)
        System.exit(0)
    }
    fun openApp(activity: Activity) {
        val packageName = "com.microware.cdfi"
        activity.startActivity(activity.packageManager.getLaunchIntentForPackage(packageName))
        exitProcess(0)

    }

    fun updateFederationEditFlag(cboType:Int,federationGuid:String,federation_id:Long,fedrationViewModel: FedrationViewModel?,executiveviewmodel: ExecutiveMemberViewmodel?){
        var is_Edited = 0
        var bank_Fi = 0
        var financialIntermidiation = fedrationViewModel!!.getFI(federationGuid)
        var bankCount = fedrationViewModel.getBankCount(federationGuid)
        var signatoryCount = fedrationViewModel.getSignatoryCount(federationGuid)

        if((financialIntermidiation == 1 && bankCount>0 && signatoryCount>=2) || financialIntermidiation == 0){
            bank_Fi = 1
        }else{
            bank_Fi = 0
        }
        var scMemberCount = fedrationViewModel.getSCMemberCount(federationGuid)
        var officeBearerCount = executiveviewmodel!!.getOBCount(listOf(1,3,5),federationGuid)
        var cboCount = executiveviewmodel.getDistinctCboCount(federation_id)
        var ecCount = fedrationViewModel.getEcMemberCount(federationGuid)
        var mapped_shg_count =
            fedrationViewModel.getMappedShgCount(federationGuid)
        var mapped_vo_count =
            fedrationViewModel.getMappedVoCount(federationGuid)
        if(cboType == 1 && ecCount > 4 && mapped_shg_count > 4 && mapped_shg_count == cboCount && officeBearerCount==3 && bank_Fi == 1 && scMemberCount==0) {
            is_Edited = 1
        }else if(cboType == 2 && ecCount > 4 && mapped_vo_count > 4 && mapped_vo_count == cboCount && officeBearerCount==3 && bank_Fi == 1 && scMemberCount==0) {
            is_Edited = 1
        } else {
            is_Edited = -1
        }
        fedrationViewModel.updateFedrationEditFlag(is_Edited,federationGuid)

    }

    fun fillcardreCatspinner(activity: Activity, spin: Spinner, data: List<CardreCateogaryEntity>?) {

        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in data.indices) {
                sValue[i + 1] = data[i].cadre_category
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }

    }

    fun returncardreCatpos(id: Int?, data: List<CardreCateogaryEntity>?): Int {

        var pos = 0
        if (!data.isNullOrEmpty()) {
            if (id!! > 0) {
                for (i in data.indices) {
                    if (id == data.get(i).cadre_cat_code)
                        pos = i + 1
                }
            }
        }
        return pos
    }

    fun returncardreCatcode(spin: Spinner, data: List<CardreCateogaryEntity>?): Int {

        var pos = spin.selectedItemPosition
        var id = 0

        if (!data.isNullOrEmpty()) {
            if (pos > 0) id = data.get(pos-1).cadre_cat_code!!
        }
        return id
    }
    fun returnVoMeetingNum(spin: Spinner, data: List<VomtgEntity>?): Int {

        var pos = spin.selectedItemPosition
        var id = 0

        if (!data.isNullOrEmpty()) {
            if (pos > 0)
                id = data.get(pos - 1).mtgNo
        }
        return id
    }

    fun fillVomeetingspinner(activity: Activity, spin: Spinner, data: List<VomtgEntity>?) {

        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in data.indices) {
                sValue[i + 1] = data[i].mtgNo.toString()
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }

    }

    fun returnvomeetingpos(id: Int?, data: List<VomtgEntity>?): Int {

        var pos = 0
        if (!data.isNullOrEmpty()) {
            if (id!! > 0) {
                for (i in data.indices) {
                    if (id == data.get(i).mtgNo)
                        pos = i + 1
                }
            }
        }
        return pos
    }

    fun returnvoMeetingdate(spin: Spinner, data: List<VomtgEntity>?): Long {

        var pos = spin.selectedItemPosition
        var id = 0L

        if (!data.isNullOrEmpty()) {
            if (pos > 0)
                id = data.get(pos - 1).mtgDate!!
        }
        return id
    }

    fun returnvoMeetingNum(spin: Spinner, data: List<VomtgEntity>?): Int {

        var pos = spin.selectedItemPosition
        var id = 0

        if (!data.isNullOrEmpty()) {
            if (pos > 0)
                id = data.get(pos - 1).mtgNo
        }
        return id
    }fun returnShgspinnerID(spin: Spinner, data: List<MappedShgEntity>?): Long {

        var pos = spin.selectedItemPosition
        var id: Long = 0

        if (!data.isNullOrEmpty()) {
            if (pos > 0)
                id = data.get(pos - 1).shg_id!!
        }
        return id
    }

    fun returnShgspinnerpos(id: Long?, data: List<MappedShgEntity>?): Int {

        var pos = 0
        if (!data.isNullOrEmpty()) {
            if (id!! > 0) {
                for (i in data.indices) {
                    if (id == data.get(i).shg_id)
                        pos = i + 1
                }
            }
        }
        return pos
    }


    fun returnVospinnerID(spin: Spinner, data: List<MappedVoEntity>?): Long {

        var pos = spin.selectedItemPosition
        var id: Long  = 0

        if (!data.isNullOrEmpty()) {
            if (pos > 0)
                id = data.get(pos - 1).cbo_child_id!!
        }
        return id
    }

    fun returnVospinnerpos(id: Long?, data: List<MappedVoEntity>?): Int {

        var pos = 0
        if (!data.isNullOrEmpty()) {
            if (id!! > 0) {
                for (i in data.indices) {
                    if (id == data.get(i).cbo_child_id)
                        pos = i + 1
                }
            }
        }
        return pos
    }
    fun returnShgspinnerName(spin: Spinner, data: List<MappedShgEntity>?): String {

        var pos = spin.selectedItemPosition
        var name: String = ""

        if (!data.isNullOrEmpty()) {
            if (pos > 0)
                name = data.get(pos - 1).shg_name!!
        }
        return name
    }
    fun returnVospinnerName(spin: Spinner, data: List<MappedVoEntity>?): String {

        var pos = spin.selectedItemPosition
        var name: String = ""

        if (!data.isNullOrEmpty()) {
            if (pos > 0)
                name = data.get(pos - 1).cbo_child_name!!
        }
        return name
    }
    fun fillFundType(activity: Activity, spin: Spinner,dataspin_fund: List<FundTypeModel>?) {

        val adapter: ArrayAdapter<String?>
        if (!dataspin_fund.isNullOrEmpty()) {
            val isize = dataspin_fund.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in dataspin_fund.indices) {
                sValue[i + 1] =
                    dataspin_fund[i].fundType
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }
        //  spin.setSelection(setFundType(ifundTypeId,dataspin_fund))
    }

    fun returnFundTypeId(spin: Spinner, dataspin_fund: List<FundTypeModel>?): Int {

        var pos = spin.selectedItemPosition
        var id = 0

        if (!dataspin_fund.isNullOrEmpty()) {
            if (pos > 0) id = dataspin_fund.get(pos - 1).fundTypeId
        }
        return id
    }

    fun setFundType(id: Int?, dataspin_fund: List<FundTypeModel>?): Int {

        var pos = 0

        if (!dataspin_fund.isNullOrEmpty()) {
            for (i in dataspin_fund.indices) {
                if (dataspin_fund.get(i).fundTypeId == id)
                    pos = i + 1
            }
        }
        return pos
    }
    fun fillLookupspinner(activity: Activity, spin: Spinner, data: List<LookupEntity>?) {

        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize+1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in 0 until data.size) {
                sValue[i+1] = data.get(i).key_value
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(data!!.size + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }

    }

    fun returnLookupcode(spin: Spinner, data: List<LookupEntity>?): Int {

        var pos = spin.selectedItemPosition
        var id = 0

        if (!data.isNullOrEmpty()) {
            if (pos > 0) id = data.get(pos-1).lookup_code!!
        }
        return id
    }

    fun returnLookuppos(id: Int?, data: List<LookupEntity>?): Int {

        var pos = 0
        if (!data.isNullOrEmpty()) {
            if (id!! > 0) {
                for (i in data.indices) {
                    if (id == data.get(i).lookup_code)
                        pos = i+1
                }
            }
        }
        return pos
    }

    fun fillVOCoaSubHeadspinner(activity: Activity, spin: Spinner, data: List<MstVOCOAEntity>?) {

        val adapter: ArrayAdapter<String?>
        if (data != null) {
            val iGen = data.size
            val sValue = arrayOfNulls<String>(iGen + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)

            for (i in 0 until data.size) {
                sValue[i + 1] = data[i].description
            }
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter

        }else {
            val sValue = arrayOfNulls<String>(data?.size!! + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }

    }

    fun returnVOSubHeadcode(spin: Spinner, data: List<MstVOCOAEntity>?): Int {

        var pos = spin.selectedItemPosition
        var id = 0

        if (!data.isNullOrEmpty()) {
            if (pos > 0) id = data.get(pos - 1).uid
        }
        return id
    }

    fun returnVOSubHeadpos(id: Int?, data: List<MstVOCOAEntity>?): Int {

        var pos = 0
        if (!data.isNullOrEmpty()) {
            if (id!! > 0) {
                for (i in data.indices) {
                    if (id == data.get(i).uid)
                        pos = i + 1
                }
            }
        }
        return pos
    }

    fun fillFundSource(activity: Activity, spin: Spinner, data: List<LookupEntity>?) {

        val adapter: ArrayAdapter<String?>
        if (!data.isNullOrEmpty()) {
            val isize = data.size
            val sValue = arrayOfNulls<String>(isize + 1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            for (i in data.indices) {
                sValue[i + 1] =
                    data[i].key_value
            }

            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        } else {
            val sValue = arrayOfNulls<String>(1)
            sValue[0] = LabelSet.getText("Select", R.string.Select)
            adapter = ArrayAdapter(
                activity,
                R.layout.my_spinner_space, sValue
            )
            adapter.setDropDownViewResource(R.layout.my_spinner)
            spin.adapter = adapter
        }
    }

    fun returnFundSourceId(spin: Spinner, data: List<LookupEntity>?): Int {

        var pos = spin.selectedItemPosition
        var id = 0

        if (!data.isNullOrEmpty()) {
            if (pos > 0) id = data.get(pos - 1).lookup_code!!
        }
        return id
    }

    fun setFundSource(id: Int?, data: List<LookupEntity>?): Int {

        var pos = 0

        if (!data.isNullOrEmpty()) {
            for (i in data.indices) {
                if (data.get(i).lookup_code == id)
                    pos = i + 1
            }
        }
        return pos
    }

    fun setFundName(id: Int?, dataspin_fund: List<FundTypeModel>?): String {

        var sFundName = ""

        if (!dataspin_fund.isNullOrEmpty()) {
            for (i in dataspin_fund.indices) {
                if (dataspin_fund.get(i).fundTypeId == id)
                    sFundName = dataspin_fund.get(i).fundType
            }
        }
        return sFundName
    }

    fun addDays(date: String, Days: Int): String {
        val sdf = SimpleDateFormat("dd-MM-yyyy", Locale.US)
        val c = Calendar.getInstance()
        try {
            c.time = sdf.parse(date)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        c.add(
            Calendar.DATE,
            Days
        ) // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
        val sdf1 = SimpleDateFormat("dd-MM-yyyy", Locale.US)

        return sdf1.format(c.time)
    }


    fun CustomAlertClfSaving(str: String, activity: Activity) {
        val mDialogView =
            LayoutInflater.from(activity).inflate(R.layout.customealertdialoge, null)
        val mBuilder = AlertDialog.Builder(activity)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str
        mDialogView.btn_ok.text = LabelSet.getText("ok",R.string.ok)
        mDialogView.btn_ok.setOnClickListener {
            mAlertDialog.dismiss()
            activity.onBackPressed()
        }
    }


    fun CustomAlertSavingBtn(str: String, activity: Activity) {
        val mDialogView = LayoutInflater.from(activity).inflate(R.layout.dialoge, null)
        val mBuilder = AlertDialog.Builder(activity)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialogView.txt_msg.text = str
        mDialogView.btn_ok.text = LabelSet.getText("ok",R.string.ok)
        mDialogView.btn_ok.setOnClickListener {

            mAlertDialog.dismiss()
        }
    }

}