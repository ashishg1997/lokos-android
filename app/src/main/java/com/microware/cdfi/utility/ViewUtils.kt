package com.microware.cdfi.utility

import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast


fun Context.toast(message: String) {
//    Toast.makeText(this, message, Toast.LENGTH_LONG)
    val toast = Toast.makeText(this, message, Toast.LENGTH_LONG)
    toast.setGravity(Gravity.CENTER, 0, 0)
    toast.show()
}

fun show(progressbar: ProgressBar) {
    progressbar.visibility = View.VISIBLE
}

fun hide(progressbar: ProgressBar) {
    progressbar.visibility = View.INVISIBLE
}



