package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.entity.cadreShgMemberEntity
import com.microware.cdfi.repository.cadreMemberRepository

class CadreMemberViewModel : AndroidViewModel {

    var cadreRepository: cadreMemberRepository? = null

    constructor(application: Application) : super(application) {
        cadreRepository = cadreMemberRepository(application)
    }

    fun insert(cadreEntity: cadreShgMemberEntity) {
        cadreRepository!!.insert(cadreEntity)
    }

    internal fun getCadredetail(cadre_guid: String?): List<cadreShgMemberEntity>? {
        return cadreRepository!!.getCadredetail(cadre_guid)
    }

    internal fun getCadreListdata(member_guid: String): LiveData<List<cadreShgMemberEntity>>? {
        return cadreRepository!!.getCadreListdata(member_guid)
    }

    internal fun getCadreListdata1(member_guid: String): List<cadreShgMemberEntity>? {
        return cadreRepository!!.getCadreListdata1(member_guid)
    }

    internal fun getCadrePendingdata(member_guid: String?): List<cadreShgMemberEntity>?{
        return cadreRepository!!.getCadrePendingdata(member_guid)
    }

    internal fun updateCadreData(
        cadre_guid: String,
        cadre_role_code: Int?,
        cadre_cat_code: Int?,
        date_joining: Long?,
        date_leaving: Long?,
        updated_by: String?,
        updated_on: Long,
        is_edited: Int,
        is_complete: Int
    ) {
        cadreRepository!!.updateCadreData(
            cadre_guid,
            cadre_role_code,
            cadre_cat_code,
            date_joining,
            date_leaving,
            updated_by,
            updated_on,
            is_edited,
            is_complete
        )
    }
}