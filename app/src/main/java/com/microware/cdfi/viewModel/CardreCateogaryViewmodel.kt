package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.microware.cdfi.entity.CardreCateogaryEntity
import com.microware.cdfi.repository.CardreCateogaryRepository

class CardreCateogaryViewmodel : AndroidViewModel {

    var cardrecatRepository:  CardreCateogaryRepository? = null

    constructor(application: Application):super(application){
        cardrecatRepository = CardreCateogaryRepository(application)
    }

    fun getCateogary(langId:String?):List<CardreCateogaryEntity>?{
        return cardrecatRepository!!.getCateogary(langId)
    }

    fun getCateogaryName(langId:String?,cadre_cat_code:Int):String{
        return cardrecatRepository!!.getCateogaryName(langId,cadre_cat_code)
    }

}