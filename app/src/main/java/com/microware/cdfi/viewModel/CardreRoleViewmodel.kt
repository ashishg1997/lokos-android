package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.microware.cdfi.entity.cadreRoleEntity
import com.microware.cdfi.repository.CardreRoleRepository

class CardreRoleViewmodel : AndroidViewModel {

    var cardreRoleRepository:  CardreRoleRepository? = null

    constructor(application: Application):super(application){
        cardreRoleRepository = CardreRoleRepository(application)
    }

    fun getCardreRole(langId:String?,cadre_cat_code:Int):List<cadreRoleEntity>{
        return cardreRoleRepository!!.getCardreRole(langId, cadre_cat_code)
    }

    fun getAllCardreRole(langId:String?):List<cadreRoleEntity>{
        return cardreRoleRepository!!.getAllCardreRole(langId)
    }

    fun getRoleName(langId:String?,roleCode:Int):String{
        return cardreRoleRepository!!.getRoleName(langId,roleCode)
    }

}