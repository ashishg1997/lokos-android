package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.entity.AddressEntity
import com.microware.cdfi.repository.CboAddressRepository

class CboAddressViewmodel : AndroidViewModel {

    var cboAddressRepository:  CboAddressRepository? = null

    constructor(application: Application):super(application){
        cboAddressRepository = CboAddressRepository(application)
    }

    internal fun getAddressdetaildata(address_guid: String?): List<AddressEntity>?{
        return cboAddressRepository!!.getAddressdetaildata(address_guid)
    }

    fun getAddressdata(shg_guid: String?): LiveData<List<AddressEntity>>?{
        return cboAddressRepository!!.getAddressdata(shg_guid)
    }

    fun insert(addressEntity: AddressEntity){
        return cboAddressRepository!!.insert(addressEntity)
    }

    fun updateAddressDetail(
        address_guid: String,
        address_line1: String,
        address_line2: String,
        village: Int,
        panchayat_id: Int,
        town: Int,
        landmark: String,
        postal_code: Int,
        is_Active: Int,
        device: Int,
        updated_date: Long,
        updated_by: String
    ) {
        cboAddressRepository!!.updateAddressDetail(
            address_guid,
            address_line1,
            address_line2,
            village,panchayat_id,
            town,landmark,postal_code,is_Active,device,updated_date,updated_by
        )
    }

    fun getAddressDatalist(shg_guid: String?): List<AddressEntity>?{
        return cboAddressRepository!!.getAddressDatalist(shg_guid)
    }
    fun getAddressDatalistcount(shg_guid: String?): List<AddressEntity>?{
        return cboAddressRepository!!.getAddressDatalistcount(shg_guid)
    }
    fun deleteData(address_guid: String?){
        cboAddressRepository!!.deleteData(address_guid)
    }

    fun deleteRecord(address_guid: String?){
        cboAddressRepository!!.deleteRecord(address_guid)
    }

    fun getCompletionCount(cbo_guid: String?):Int{
        return cboAddressRepository!!.getCompletionCount(cbo_guid)
    }

    fun getVerificationCount(cbo_guid: String?):Int{
        return cboAddressRepository!!.getVerificationCount(cbo_guid)
    }
    fun updateIsVerifed(shgguid: String?,updated_date: Long,updated_by: String){
        cboAddressRepository!!.updateIsVerifed(shgguid,updated_date,updated_by)
    }
}