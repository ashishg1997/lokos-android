package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.api.vomodel.VoChangePaymentBankDataModel
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.repository.CboBankRepository

class CboBankViewmodel: AndroidViewModel {

    var cboBankRepository: CboBankRepository? = null

    constructor(application: Application):super(application){
        cboBankRepository = CboBankRepository(application)
    }

    internal fun getBankdetaildata(bankGuid: String?): List<Cbo_bankEntity>?{
        return cboBankRepository!!.getBankdetaildata(bankGuid)
    }

    fun getBankdata(bankguid: String?): LiveData<List<Cbo_bankEntity>>?{
        return cboBankRepository!!.getBankdata(bankguid)
    }

    fun insert(cboBankEntity: Cbo_bankEntity){
        return cboBankRepository!!.insert(cboBankEntity)
    }

    fun updateBankDetail(
        bank_guid: String,
        bank_id: Int,
        account_opening_date: Long,
        account_Linkage_Date: Long,
        account_no: String,
        bank_branch: Int,
        ifsc_code: String,
        is_default: Int,
        sequence_no: Int,
        account_type: Int,
        verification: Int,
        device: Int,
        updated_date: Long,
        updated_by: String,
        imageName: String,
        is_complete: Int,
        bankpassbook_name: String
    ) {
        cboBankRepository!!.updateBankDetail(
            bank_guid,
            bank_id,
            account_opening_date,
            account_Linkage_Date,
            account_no,bank_branch,ifsc_code,is_default,
            sequence_no,account_type,verification,device,updated_date,updated_by,imageName,is_complete,bankpassbook_name
        )
    }

    fun getBankdatalist(bankguid: String?): List<Cbo_bankEntity>?{
        return cboBankRepository!!.getBankdatalist(bankguid)
    }

    fun getBankdatalistcount(bankguid: String?): List<Cbo_bankEntity>?{
        return cboBankRepository!!.getBankdatalistcount(bankguid)
    }


    fun getBankdatalistdefualtcount(cbo_guid: String?): Int{
        return cboBankRepository!!.getBankdatalistdefualtcount(cbo_guid)
    }

    fun getbank_acc_count(account_no:String,cboType:Int):Int{
        return cboBankRepository!!.getbank_acc_count(account_no,cboType)
    }

    fun deleteData(bank_guid: String?){
        cboBankRepository!!.deleteData(bank_guid)
    }

    fun deleteRecord(bank_guid: String?){
        cboBankRepository!!.deleteRecord(bank_guid)
    }

    fun getCompletionCount(cbo_guid: String?):Int{
        return cboBankRepository!!.getCompletionCount(cbo_guid)
    }

    fun getVerificationCount(cbo_guid: String?):Int{
        return cboBankRepository!!.getVerificationCount(cbo_guid)
    }

    fun updateIsVerifed(shgguid: String?,updated_date: Long,updated_by: String){
        cboBankRepository!!.updateIsVerifed(shgguid,updated_date,updated_by)
    }

    fun updateactivotaionstatus(bank_guid: String?, status: Int) {
        cboBankRepository!!.updateactivotaionstatus(bank_guid, status)
    }
    fun getcboBankdata(cbo_guid: String?,cboType:Int): List<Cbo_bankEntity>?{
        return cboBankRepository!!.getcboBankdata(cbo_guid,cboType)
    }
    fun getcboBankdataModel(cbo_guid: String?,cboType:Int): List<VoChangePaymentBankDataModel>?{
        return cboBankRepository!!.getcboBankdataModel(cbo_guid,cboType)
    }
    fun getBankdata1(fedrationGUID: String?): List<Cbo_bankEntity>?{
        return cboBankRepository!!.getBankdata1(fedrationGUID)
    }
}