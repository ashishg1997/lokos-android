package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.entity.Cbo_kycEntity
import com.microware.cdfi.repository.CboKycRepository

class CboKycViewmodel : AndroidViewModel {

    var cboKycRepository: CboKycRepository? = null

    constructor(application: Application):super(application){
        cboKycRepository = CboKycRepository(application)
    }

    internal fun getKycdetaildata(shgGUID: String?): LiveData<List<Cbo_kycEntity>>?{
        return cboKycRepository!!.getKycdetaildata(shgGUID)
    }

    internal fun getKycdetail(kycGUID: String?): List<Cbo_kycEntity>?{
        return cboKycRepository!!.getKycdetail(kycGUID)
    }

    fun insert(cboKycentity: Cbo_kycEntity){
        return cboKycRepository!!.insert(cboKycentity)
    }

    fun updateKycDetail(
        kyc_guid: String,
        kyc_type: Int,
        kyc_id: String,
        kyc_front_doc_orig_name: String,
        kyc_front_doc_encp_name: String,
        kyc_rear_doc_orig_name: String,
        kyc_rear_doc_encp_name: String,
        kyc_valid_from:Long,
        kyc_valid_to:Long,
        updated_date: Long,
        updated_by: String,is_complete:Int
    ) {
        cboKycRepository!!.updateKycDetail(
            kyc_guid,
            kyc_type,
            kyc_id,
            kyc_front_doc_orig_name,
            kyc_front_doc_encp_name,
            kyc_rear_doc_orig_name,
            kyc_rear_doc_encp_name,
            kyc_valid_from,
            kyc_valid_to,
            updated_date,
            updated_by,is_complete
        )
    }

    internal fun getkycdatalist(cbo_guid: String?): List<Cbo_kycEntity>?{
        return cboKycRepository!!.getkycdatalist(cbo_guid)
    }

    internal fun updatededup(guid: String){
        cboKycRepository!!.updatededup(guid)
    }

    internal fun deleteData(guid: String?){
        cboKycRepository!!.deleteData(guid)
    }
    internal fun deleteRecord(guid: String?){
        cboKycRepository!!.deleteRecord(guid)
    }

    internal  fun getKyc_count(kyc_number:String,cboType:Int):Int{
        return cboKycRepository!!.getKyc_count(kyc_number,cboType)
    }
}