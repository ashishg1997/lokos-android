package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.entity.Cbo_phoneEntity
import com.microware.cdfi.repository.CboPhoneDetailRepository

class CboPhoneViewmodel : AndroidViewModel {
    var cboPhoneDetailRepository: CboPhoneDetailRepository? = null

    constructor(application: Application):super(application){
        cboPhoneDetailRepository = CboPhoneDetailRepository(application)
    }

    internal fun getphonedetaildata(PhoneGUID: String?): List<Cbo_phoneEntity>?{
        return cboPhoneDetailRepository!!.getphonedetaildata(PhoneGUID)
    }

    fun getphonedata(shgGUID: String?): LiveData<List<Cbo_phoneEntity>>?{
        return cboPhoneDetailRepository!!.getphonedata(shgGUID)
    }

    fun insert(cboPhoneentity: Cbo_phoneEntity){
        return cboPhoneDetailRepository!!.insert(cboPhoneentity)
    }

    fun updatePhoneDetail(
        phone_guid: String,
        member_guid:String,
        phone_no: String,
        isDefault: Int,
        phone_ownership: String,
        phone_ownership_detail: String,
        valid_from: Long,
        valid_till: Long,
        device: Int,
        updated_date: Long,
        updated_by: String,
        is_complete:Int
    ) {
        cboPhoneDetailRepository!!.updatePhoneDetail(
            phone_guid,
            member_guid,
            phone_no,
            isDefault,
            phone_ownership,
            phone_ownership_detail,valid_from,valid_till,device,updated_date,updated_by,is_complete
        )
    }

    fun getuploadlist(cbo_guid: String?): List<Cbo_phoneEntity>?{
        return cboPhoneDetailRepository!!.getuploadlist(cbo_guid)
    }
    fun getuploadlistcount(cbo_guid: String?): List<Cbo_phoneEntity>?{
        return cboPhoneDetailRepository!!.getuploadlistcount(cbo_guid)
    }

    fun deleteData(PhoneGUID: String?){
        cboPhoneDetailRepository!!.deleteData(PhoneGUID)
    }
    fun deleteRecord(PhoneGUID: String?){
        cboPhoneDetailRepository!!.deleteRecord(PhoneGUID)
    }

    fun getCompletionCount(cbo_guid: String?):Int{
        return cboPhoneDetailRepository!!.getCompletionCount(cbo_guid)
    }

    fun getVerificationCount(cbo_guid: String?):Int{
        return cboPhoneDetailRepository!!.getVerificationCount(cbo_guid)
    }

    fun updateIsVerifed(shgguid: String?,updated_date: Long,updated_by: String){
        cboPhoneDetailRepository!!.updateIsVerifed(shgguid,updated_date,updated_by)
    }

    fun getphone_count(mobile_no:String,cboType:Int):Int{
        return cboPhoneDetailRepository!!.getphone_count(mobile_no, cboType)
    }
}