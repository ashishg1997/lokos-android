package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.microware.cdfi.entity.ClfVoMemberEntity
import com.microware.cdfi.entity.VoShgMemberEntity
import com.microware.cdfi.repository.ClfVOMemberRepository
import com.microware.cdfi.repository.VOShgMemberRepository

class ClfVoMemberViewmodel : AndroidViewModel {

    var clfvoMemberRepository:  ClfVOMemberRepository? = null

    constructor(application: Application):super(application){
        clfvoMemberRepository = ClfVOMemberRepository(application)
    }

    fun getMemberList(vo_guid:String):List<ClfVoMemberEntity>?{
        return clfvoMemberRepository!!.getMemberList(vo_guid)
    }
}