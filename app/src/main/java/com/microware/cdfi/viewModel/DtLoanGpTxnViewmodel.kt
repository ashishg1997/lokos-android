package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.api.meetinguploadmodel.ShgGroupLoanTransactionData
import com.microware.cdfi.entity.DtLoanGpTxnEntity
import com.microware.cdfi.repository.DtLoanGpTxnRepository

class DtLoanGpTxnViewmodel: AndroidViewModel {

    var dtLoanGpTxnRepository: DtLoanGpTxnRepository? = null

    constructor(application: Application):super(application){
        dtLoanGpTxnRepository = DtLoanGpTxnRepository(application)
    }


    internal fun getListDataByMtgnum(mtgno:Int,cbo_aid:Long): List<DtLoanGpTxnEntity> {
        return dtLoanGpTxnRepository!!.getListDataByMtgnum(mtgno,cbo_aid)
    }

    fun insert(dtLoanGpTxnEntity: DtLoanGpTxnEntity){
        return dtLoanGpTxnRepository!!.insert(dtLoanGpTxnEntity)
    }
    fun getloandata(loanno: Int, mtgno: Int,shgid:Long): List<DtLoanGpTxnEntity>? {
        return dtLoanGpTxnRepository!!.getloandata(loanno,mtgno,shgid)
    }
    fun gettotalpaid(cboid: Long, mtgno: Int,loanno:Int): Int {
        return dtLoanGpTxnRepository!!.gettotalpaid(cboid, mtgno,loanno)
    }
    fun updateloanpaid(
        loanno: Int,mtgno: Int, cboid: Long, paid: Int, loanint: Int,
        mode: Int,
        bankaccount: String,
        transactionno: String,
        principaldemandcl: Int,
        completionflag:Boolean
    ) {
        dtLoanGpTxnRepository!!.updateloanpaid(loanno, mtgno, cboid, paid,loanint,mode,bankaccount,transactionno,principaldemandcl,completionflag
        )
    }
    fun updateLoanGpTxn(
        UID: Int,
        LoanNo: Int,
        cbo_id: Int,
        MtgNo: Int,
        MtgDate: Long,
        loan_no: Int,
        loan_op: Int,
        loan_op_int: Int,
        loan_paid: Int,
        loan_paid_int: Int,
        loan_cl: Int,
        loan_cl_int: Int,
        completion_flag: Int,
        intaccrued_op: Int,
        intaccrued: Int,
        intaccrued_cl: Int,
        principal_demand_ob: Int,
        principal_demand: Int,
        principal_demand_cb: Int,
        mode_payment: Int,
        bank_code: String,
        transaction_no: String,
        createdby: String,
        createdon: Long,
        updatedby: String,
        updatedon: Long,
        uploaded_by: String
    ) {
        dtLoanGpTxnRepository!!.updateLoanGpTxn(
            UID,
            LoanNo,
            cbo_id,
            MtgNo,
            MtgDate,
            loan_no,
            loan_op,
            loan_op_int,
            loan_paid,
            loan_paid_int,
            loan_cl,
            loan_cl_int,
            completion_flag,
            intaccrued_op,
            intaccrued,
            intaccrued_cl,
            principal_demand_ob,
            principal_demand,
            principal_demand_cb,
            mode_payment,
            bank_code,
            transaction_no,
            createdby,
            createdon,
            updatedby,
            updatedon,
            uploaded_by

        )
    }

    fun getLoanRepaymentAmount(cboid: Long, mtgno: Int):Int{
        return dtLoanGpTxnRepository!!.getLoanRepaymentAmount(cboid, mtgno)
    }

    fun getLoanRepaymentByModeAmount(cboid: Long, mtgno: Int,modePayment:Int):Int{
        return dtLoanGpTxnRepository!!.getLoanRepaymentByModeAmount(cboid, mtgno,modePayment)
    }

    fun getLoanRepaymentAmountByMode1(cboid: Long, mtgno: Int):Int{
        return dtLoanGpTxnRepository!!.getLoanRepaymentAmountByMode1(cboid, mtgno)
    }

    fun getShgGrpLoanTxnAnount(cboid: Long, mtgno: Int,bank_code:String):Int{
        return dtLoanGpTxnRepository!!.getShgGrpLoanTxnAnount(cboid,mtgno, bank_code)
    }


    fun getUploadListData(mtg_no:Int,cbo_id: Long) : List<DtLoanGpTxnEntity>{
        return dtLoanGpTxnRepository!!.getUploadListData(mtg_no,cbo_id)
    }
}