package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.api.meetinguploadmodel.ShgGroupLoanData
import com.microware.cdfi.entity.DtLoanGpEntity
import com.microware.cdfi.repository.DtLoanGpRepository

class DtLoanGpViewmodel: AndroidViewModel {

    var dtLoanGpRepository: DtLoanGpRepository? = null

    constructor(application: Application):super(application){
        dtLoanGpRepository = DtLoanGpRepository(application)
    }

    internal fun getLoanGpdata(cbo_id: Long): List<DtLoanGpEntity>?{
        return dtLoanGpRepository!!.getLoanGpdata(cbo_id)
    }
    fun getLoandata(cbo_id: Long,loanno: Int): List<DtLoanGpEntity>? {
        return dtLoanGpRepository!!.getLoandata(cbo_id,loanno)
    }
    fun getinterestrate(loanno: Int): Double {
        return dtLoanGpRepository!!.getinterestrate(loanno)
    }
    fun gettotamt(loanno: Int): Int {
        return dtLoanGpRepository!!.gettotamt(loanno)
    }
    fun getmaxLoanno(cbo_id: Long): Int {
        return dtLoanGpRepository!!.getmaxLoanno(cbo_id)
    }

    fun insert(dtLoanGpEntity: DtLoanGpEntity){
        return dtLoanGpRepository!!.insert(dtLoanGpEntity)
    }

    fun updateLoanGp(
        cbo_id: Int,
        mtgno: Int,
        mtgdate: String,
        loanno: Int,
        loan_type: String,
        loan_product: Int,
        loan_accountno: String,
        loan_period: Int,
        installmentdate: String,
        amount: Int,
        interest_rate: Double,
        interest_due: Int,
        completion_flag: String,
        mode_payment: String,
        bank_code: String,
        loanpurpose: Int,
        createdby: String,
        createdon: String,
        updatedby: String,
        updatedon: String,
        uploadedon: String

    ) {
        dtLoanGpRepository!!.updateLoanGp(
            cbo_id,
            mtgno,
            mtgdate,
            loanno,
            loan_type,
            loan_product,
            loan_accountno,
            loan_period,
            installmentdate,
            amount,
            interest_rate,
            interest_due,
            completion_flag,
            mode_payment,
            bank_code,
            loanpurpose,
            createdby,
            createdon,
            updatedby,
            updatedon,
            uploadedon
        )
    }


    fun deleteRecord(cbo_id: Int){
        dtLoanGpRepository!!.deleteRecord(cbo_id)
    }

    internal fun getListDataByMtgnum(mtgno:Int,cbo_aid:Long): List<DtLoanGpEntity> {
        return dtLoanGpRepository!!.getListDataByMtgnum(mtgno,cbo_aid)
    }

    internal fun gettotloangroupamount(mtgno:Int,cbo_id:Long): Int {
        return dtLoanGpRepository!!.gettotloangroupamount(mtgno,cbo_id)
    }

    fun getUploadListData(mtg_no:Int,cbo_id: Long) : List<DtLoanGpEntity>{
        return dtLoanGpRepository!!.getUploadListData(mtg_no,cbo_id)
    }
    internal fun updateShgGroupLoan(cbo_id:Long,loan_no: Int,completionflag: Boolean){
        dtLoanGpRepository!!.updateShgGroupLoan(cbo_id,loan_no,completionflag)
    }

    internal fun getUploadGroupLoanList(cbo_id: Long) : List<DtLoanGpEntity>{
        return dtLoanGpRepository!!.getUploadGroupLoanList(cbo_id)
    }
}