package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.microware.cdfi.entity.DtLoanMemberSheduleEntity
import com.microware.cdfi.repository.DtLoanMemberScheduleRepository

class DtLoanMemberScheduleViewmodel : AndroidViewModel {

    var dtLoanMemberScheduleRepository: DtLoanMemberScheduleRepository? = null

    constructor(application: Application) : super(application) {
        dtLoanMemberScheduleRepository = DtLoanMemberScheduleRepository(application)
    }

    internal fun getLoanMemberScheduledata(cbo_id: Int): List<DtLoanMemberSheduleEntity>? {
        return dtLoanMemberScheduleRepository!!.getLoanMemberScheduledata(cbo_id)
    }

    internal fun getMemberScheduledata(
        mem_id: Long,
        loanno: Int
    ): List<DtLoanMemberSheduleEntity>? {
        return dtLoanMemberScheduleRepository!!.getMemberScheduledata(mem_id, loanno)
    }

    fun getMemberScheduleloanwise(
        mem_id: Long,
        loanno: Int,
        mtgno: Int
    ): List<DtLoanMemberSheduleEntity>? {
        return dtLoanMemberScheduleRepository!!.getMemberScheduleloanwise(mem_id,loanno,mtgno)
    }
    fun getprincipaldemand(
        loanno: Int,
        mem_id: Long,
        currentmtgdate: Long,
        lastmtgdate: Long
    ): Int {
        return dtLoanMemberScheduleRepository!!.getprincipaldemand(
            loanno,
            mem_id,
            currentmtgdate,
            lastmtgdate
        )
    }


    internal fun gettotalloanamt(loanno: Int, mem_id: Long, mtgguid: String): Int {
        return dtLoanMemberScheduleRepository!!.gettotalloanamt(loanno, mem_id, mtgguid)
    }

    internal fun gettotaloutstanding(loanno: Int, mem_id: Long): Int {
        return dtLoanMemberScheduleRepository!!.gettotaloutstanding(loanno, mem_id)
    }
internal fun getremaininginstallment(loanno: Int, mem_id: Long): Int {
        return dtLoanMemberScheduleRepository!!.getremaininginstallment(loanno, mem_id)
    }


    fun insert(dtLoanMemberScheduleEntity: DtLoanMemberSheduleEntity) {
        return dtLoanMemberScheduleRepository!!.insert(dtLoanMemberScheduleEntity)
    }

    fun updateLoanMemberSchedule(
        mtgguid: String,
        mem_id: Long,
        loanno: Int,
        installmentno: Int,
        principaldemand: Int,
        loandemandos: Int,
        loanos: Int

    ) {
        dtLoanMemberScheduleRepository!!.updateLoanMemberSchedule(
            mtgguid,
            mem_id,
            loanno,
            installmentno,
            principaldemand,
            loandemandos,
            loanos

        )
    }

    fun updaterepaid(
        loanno: Int,
        mem_id: Long,
        installmentno: Int,
        sub_installment_no: Int,
        loanpaid: Int,
        mtgdate: Long,
        mtgno: Int,
        updatedby: String?,
        updatedon: Long
    ) {
        dtLoanMemberScheduleRepository!!.updaterepaid(
            loanno,
            mem_id,
            installmentno,
            sub_installment_no,
            loanpaid,
            mtgdate,mtgno,updatedby,updatedon
        )

    }

    fun deleteRecord(cbo_id: Int) {
        dtLoanMemberScheduleRepository!!.deleteRecord(cbo_id)
    }

    fun updateCutOffLoanMemberSchedule(
        mtgguid: String,
        mem_id: Long,
        loanno: Int,
        installmentno: Int,
        principaldemand: Int
    ) {
        dtLoanMemberScheduleRepository!!.updateCutOffLoanMemberSchedule(
            mtgguid,
            mem_id,
            loanno,
            installmentno,
            principaldemand

        )
    }
    fun getPrincipalDemandByInstallmentNum(mem_id: Long,mtg_guid: String,loan_no: Int,installment_no:Int):Int{
        return  dtLoanMemberScheduleRepository!!.getPrincipalDemandByInstallmentNum(mem_id,mtg_guid,loan_no,installment_no)
    }
    fun getnextdemand(mem_id: Long,loanno:Int): Int {
        return dtLoanMemberScheduleRepository!!.getnextdemand(mem_id,loanno)
    }

    fun deleterepaid(
        loanno: Int,
        mem_id: Long,
        mtgdate: Long ,
        mtgno: Int  )
    {
        dtLoanMemberScheduleRepository!!. deleterepaid(
            loanno,
            mem_id,
            mtgdate,mtgno   )

    }
    fun deletesubinstallment(
        loanno: Int,
        mem_id: Long,
        mtgdate: Long ,
        mtgno: Int  )
    {
        dtLoanMemberScheduleRepository!!. deletesubinstallment(
            loanno,
            mem_id,
            mtgdate,mtgno   )

    }
    fun getUploadListData(mem_id:Long,cbo_id: Long,loanno:Int) : List<DtLoanMemberSheduleEntity>{
        return dtLoanMemberScheduleRepository!!.getUploadListData(mem_id,cbo_id,loanno)
    }
    fun gettotalloanoutstanding(mem_id: Long): Int {
        return dtLoanMemberScheduleRepository!!.gettotalloanoutstanding(mem_id)
    }

    fun getMaxRepaidInstallmentNum(cboId:Long,mem_id: Long,loanno: Int):Int{
        return dtLoanMemberScheduleRepository!!.getMaxRepaidInstallmentNum(cboId,mem_id,loanno)
    }

    fun getInstallmentdateByInstallmentNum(installmentNum:Int,cboId:Long,mem_id: Long,loanno: Int):Long{
        return dtLoanMemberScheduleRepository!!.getInstallmentdateByInstallmentNum(installmentNum,cboId,mem_id,loanno)
    }

    fun deleteMemberSubinstallment(cboId:Long,mem_id: Long,loanno: Int){
        return dtLoanMemberScheduleRepository!!.deleteMemberSubinstallment(cboId,mem_id,loanno)
    }
}