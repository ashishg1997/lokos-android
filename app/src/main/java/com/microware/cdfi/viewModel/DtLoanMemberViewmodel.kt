package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.microware.cdfi.entity.DtLoanMemberEntity
import com.microware.cdfi.repository.DtLoanMemberRepository

class DtLoanMemberViewmodel : AndroidViewModel {

    var dtLoanMemberRepository: DtLoanMemberRepository? = null

    constructor(application: Application) : super(application) {
        dtLoanMemberRepository = DtLoanMemberRepository(application)
    }

    internal fun getLoanMemberdata(uid: Int): List<DtLoanMemberEntity>? {
        return dtLoanMemberRepository!!.getLoanMemberdata(uid)
    }

    internal fun getLoanno(loanappid: Long): Int {
        return dtLoanMemberRepository!!.getLoanno(loanappid)
    }
    internal fun getLoancount(loanappid: Long): List<DtLoanMemberEntity>? {
        return dtLoanMemberRepository!!.getLoancount(loanappid)
    }

    internal fun getmaxLoanno(cbo_id: Long): Int {
        return dtLoanMemberRepository!!.getmaxLoanno(cbo_id)
    }

    fun getinterestrate(loanno: Int): Double {
        return dtLoanMemberRepository!!.getinterestrate(loanno)
    }

    fun gettotamt(loanno: Int,member_id:Long): Double {
        return dtLoanMemberRepository!!.gettotamt(loanno,member_id)
    }


    fun insert(dtLoanMemberEntity: DtLoanMemberEntity) {
        return dtLoanMemberRepository!!.insert(dtLoanMemberEntity)
    }

    /* fun updateLoanMember(
         uid: Int,
         cbo_id: Int,
         member_AID: Long,
         mtg_GUID: String,
         mem_id: Long,
         loanno: Long,
         mtgno: Int,
         mtgdate: String,
         installmentdate: String,
         installments: Int,
         amount: Double,
         loan_purpose: Int,
         loan_prod_code: Int,
         interest_rate: Double,
         period: Int,
         interest_due: Double,
         completionflag: String,
         loantype: Int,
         loansource: Int,
         externalloanid: Int,
         modepayment: Int,
         bankcode: String,
         installmentfreq: Int,
         moratoriumperiod: Int,
         loanaccountno: String,
         createdby: String,
         createdon: String,
         updatedby: String,
         updatedon: String,
         uploadedon: String,
         corpus_external_id: Int,
         flag: String
     ) {
         dtLoanMemberRepository!!.updateLoanMember(
             uid,
             cbo_id,
             member_AID,
             mtg_GUID,
             mem_id,
             loanno,
             mtgno,
             mtgdate,
             installmentdate,
             installments,
             amount,
             loan_purpose,
             loan_prod_code,
             interest_rate,
             period,
             interest_due,
             completionflag,
             loantype,
             loansource,
             externalloanid,
             modepayment,
             bankcode,
             installmentfreq,
             moratoriumperiod,
             loanaccountno,
             createdby,
             createdon,
             updatedby,
             updatedon,
             uploadedon,
             corpus_external_id,
             flag
         )
     }*/


    fun deleteRecord(uid: Int) {
        dtLoanMemberRepository!!.deleteRecord(uid)
    }

    fun getLoanDetailData(loan_no: Int,cbo_id: Long,mem_id: Long,mtg_guid:String): List<DtLoanMemberEntity>?{
        return dtLoanMemberRepository!!.getLoanDetailData(loan_no,cbo_id,mem_id,mtg_guid)
    }

    fun getloanListSummeryData(mtgno: Int, cbo_id: Long,mem_id: Long): List<DtLoanMemberEntity>{
        return dtLoanMemberRepository!!.getloanListSummeryData(mtgno, cbo_id, mem_id)
    }

    fun getLoanDisbursedAmount(cbo_id: Long,mtg_no: Int,modePayment:Int):Int{
        return dtLoanMemberRepository!!.getLoanDisbursedAmount(cbo_id,mtg_no,modePayment)
    }

    fun getMemberLoanDisbursedAmount(cbo_id: Long,mtg_no: Int):Int{
        return dtLoanMemberRepository!!.getMemberLoanDisbursedAmount(cbo_id,mtg_no)
    }

    fun getMemberLoanDisbursedBankAmount(cbo_id: Long,mtg_no: Int):Int{
        return dtLoanMemberRepository!!.getMemberLoanDisbursedBankAmount(cbo_id,mtg_no)
    }
    fun getMemLoanDisbursedAmount(cbo_id: Long,mtg_no: Int,bank_code:String):Int{
        return dtLoanMemberRepository!!.getMemLoanDisbursedAmount(cbo_id, mtg_no, bank_code)
    }
    fun getTotalClosedLoanAmount(mtgNum:Int,mem_id: Long,completionFlag: Boolean):Int{
        return dtLoanMemberRepository!!.getTotalClosedLoanAmount(mtgNum,mem_id,completionFlag)
    }

    fun getTotalClosedLoanCount(mtgNum:Int,mem_id: Long,completionFlag:Boolean):Int{
        return dtLoanMemberRepository!!.getTotalClosedLoanCount(mtgNum,mem_id,completionFlag)
    }


    fun getUploadListData(mtg_no:Int,cbo_id: Long) : List<DtLoanMemberEntity>{
        return dtLoanMemberRepository!!.getUploadListData(mtg_no,cbo_id)
    }

    fun getCutOffmaxLoanno(cbo_id: Long,mtg_no: Int): Int{
        return dtLoanMemberRepository!!.getCutOffmaxLoanno(cbo_id,mtg_no)
    }

    fun getTotalClosedLoan(mtgNum:Int,mem_id: Long,completionFlag:Boolean):Int{
        return dtLoanMemberRepository!!.getTotalClosedLoan(mtgNum,mem_id,completionFlag)
    }

    fun getClosedLoanAmount(mtgNum:Int,mem_id: Long,completionFlag: Boolean):Int{
        return dtLoanMemberRepository!!.getClosedLoanAmount(mtgNum,mem_id,completionFlag)
    }
    internal  fun updateMemberLoanEditFlag(
        cbo_id: Long,
        mem_id: Long,
        loan_no: Int,
        completionflag: Boolean
    ){
        dtLoanMemberRepository!!.updateMemberLoanEditFlag(cbo_id,mem_id,loan_no,completionflag)
    }

    internal fun getUploadMemberLoanList(cbo_id: Long) : List<DtLoanMemberEntity>{
        return dtLoanMemberRepository!!.getUploadMemberLoanList(cbo_id)
    }
}