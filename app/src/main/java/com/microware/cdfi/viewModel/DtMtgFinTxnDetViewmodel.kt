package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.entity.DtMtgFinTxnDetEntity
import com.microware.cdfi.repository.DtMtgFinTxnDetRepository

class DtMtgFinTxnDetViewmodel: AndroidViewModel {

    var dtMtgFinTxnDetRepository: DtMtgFinTxnDetRepository? = null

    constructor(application: Application):super(application){
        dtMtgFinTxnDetRepository = DtMtgFinTxnDetRepository(application)
    }

    internal fun getMtgFinTxnDetdata(uid: Int): List<DtMtgFinTxnDetEntity>?{
        return dtMtgFinTxnDetRepository!!.getMtgFinTxnDetdata(uid)
    }


    fun insert(dtMtgFinTxnDetEntity: DtMtgFinTxnDetEntity){
        return dtMtgFinTxnDetRepository!!.insert(dtMtgFinTxnDetEntity)
    }

    fun updateMtgFinTxnDet(
        uid: Int,
        cbo_id: Int,
        group_m_code: Int,
        mtgno: Int,
        bankcode: String,
        auid: Int,
        type: String,
        amount: Double,
        transdate: String,
        deposit_receipt: Double,
        withdrawal_payment: Double,
        date_realisation: String,
        loanno: Int,
        effectivedate: String,
        narration: String
    ) {
        dtMtgFinTxnDetRepository!!.updateMtgFinTxnDet(
            uid,
            cbo_id,
            group_m_code,
            mtgno,
            bankcode,
            auid,
            type,
            amount,
            transdate,
            deposit_receipt,
            withdrawal_payment,
            date_realisation,
            loanno,
            effectivedate,
            narration
        )
    }


    fun deleteRecord(uid: Int){
        dtMtgFinTxnDetRepository!!.deleteRecord(uid)
    }
}