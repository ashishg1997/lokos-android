package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.microware.cdfi.entity.DtMtgFinTxnEntity
import com.microware.cdfi.repository.DtMtgFinTxnRepository

class DtMtgFinTxnViewmodel: AndroidViewModel {

    var dtMtgFinTxnRepository: DtMtgFinTxnRepository? = null

    constructor(application: Application):super(application){
        dtMtgFinTxnRepository = DtMtgFinTxnRepository(application)
    }

    internal fun getMtgFinTxndata(uid: Int): List<DtMtgFinTxnEntity>?{
        return dtMtgFinTxnRepository!!.getMtgFinTxndata(uid)
    }


    fun insert(dtMtgFinTxnEntity: DtMtgFinTxnEntity){
        return dtMtgFinTxnRepository!!.insert(dtMtgFinTxnEntity)
    }

    fun updateMtgFinTxn(
        uid: Int?,
        cbo_id: Int?,
        mtgno: Int?,
        bankcode: String?,
        openingbalance: Int?,
        closingbalance: Int?,
        otherdeposits: Int?,
        otherwithdrawals: Int?,
        depositedcash: Int?,
        withdrawncash: Int?,
        createdby: String?,
        createdon: Long?,
        updatedby: String,
        updatedon: Long?,
        uploaded_by: String?
    ) {
        dtMtgFinTxnRepository!!.updateMtgFinTxn(
            uid,
            cbo_id,
            mtgno,
            bankcode,
            openingbalance,
            closingbalance,
            otherdeposits,
            otherwithdrawals,
            depositedcash,
            withdrawncash,
            createdby,
            createdon,
            updatedby,
            updatedon,
            uploaded_by
        )
    }


    fun deleteRecord(uid: Int){
        dtMtgFinTxnRepository!!.deleteRecord(uid)
    }
    internal fun getListDataByMtgnum(
        mtgno: Int,
        cbo_id: Long,
        accountNo: String
    ): List<DtMtgFinTxnEntity> {
        return dtMtgFinTxnRepository!!.getListDataByMtgnum(mtgno,cbo_id,accountNo)
    }

}