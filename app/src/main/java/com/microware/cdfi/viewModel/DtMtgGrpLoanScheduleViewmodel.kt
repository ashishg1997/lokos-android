package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.microware.cdfi.entity.DtMtgGrpLoanScheduleEntity
import com.microware.cdfi.repository.DtMtgGrpLoanScheduleRepository

class DtMtgGrpLoanScheduleViewmodel : AndroidViewModel {

    var dtMtgGrpLoanScheduleRepository: DtMtgGrpLoanScheduleRepository? = null

    constructor(application: Application) : super(application) {
        dtMtgGrpLoanScheduleRepository = DtMtgGrpLoanScheduleRepository(application)
    }

    internal fun getMtgGrpLoanScheduledata(cbo_id: Int): List<DtMtgGrpLoanScheduleEntity>? {
        return dtMtgGrpLoanScheduleRepository!!.getMtgGrpLoanScheduledata(cbo_id)
    }

    fun getScheduleloanwise(mem_id: Long,loanno:Int): List<DtMtgGrpLoanScheduleEntity>? {
        return dtMtgGrpLoanScheduleRepository!!.getScheduleloanwise(mem_id,loanno)
    }
    fun getremaininginstallment(loanno: Int,cbo_id: Long): Int {
        return dtMtgGrpLoanScheduleRepository!!.getremaininginstallment(loanno,cbo_id)
    }
    fun getGroupScheduledata(cboid: Long,mtgguid:String,loanno:Int): List<DtMtgGrpLoanScheduleEntity>? {
        return dtMtgGrpLoanScheduleRepository!!.getGroupScheduledata(cboid,mtgguid,loanno)
    }

    fun getprincipaldemand(loanno: Int,cbo_id: Long,currentmtgdate:Long,lastmtgdate:Long): Int {
        return dtMtgGrpLoanScheduleRepository!!.getprincipaldemand(loanno,cbo_id,currentmtgdate,lastmtgdate)
    }
    internal fun gettotaloutstanding(loanno: Int, cboid: Long): Int {
        return dtMtgGrpLoanScheduleRepository!!.gettotaloutstanding(loanno, cboid)
    }

    fun insert(dtMtgGrpLoanScheduleEntity: DtMtgGrpLoanScheduleEntity) {
        return dtMtgGrpLoanScheduleRepository!!.insert(dtMtgGrpLoanScheduleEntity)
    }

    fun updateLoanGroupSchedule(
        mtgguid: String,
        cboid: Long,
        loanno: Int,
        installmentno: Int,
        principaldemand: Int,
        loandemandos: Int,
        loanos: Int


    ) {
        dtMtgGrpLoanScheduleRepository!!.updateLoanGroupSchedule(
            mtgguid,
            cboid,
            loanno,
            installmentno,
            principaldemand,
            loandemandos,
            loanos

        )
    }
    fun updateMtgGrpLoanSchedule(
        cbo_id: Long,
        loan_no: Int,
        principal_demand: Int,
        loan_demand_os: Int,
        loan_os: Int,
        loan_paid: Int,
        installment_no: Int,
        sub_installment_no: Int,
        installment_date: Long,
        loan_date: Long,
        repaid: Int,
        lastpaid_date: Long,
        mtg_no: Int,
        createdby: String,
        createdon: Long,
        updatedby: String,
        updatedon: Long,
        uploaded_by: String
    ) {
        dtMtgGrpLoanScheduleRepository!!.updateMtgGrpLoanSchedule(
            cbo_id,
            loan_no,
            principal_demand,
            loan_demand_os,
            loan_os,
            loan_paid,
            installment_no,
            sub_installment_no,
            installment_date,
            loan_date,
            repaid,
            lastpaid_date,
            mtg_no,
            createdby,
            createdon,
            updatedby,
            updatedon,
            uploaded_by
        )
    }


    fun deleteRecord(cbo_id: Int) {
        dtMtgGrpLoanScheduleRepository!!.deleteRecord(cbo_id)
    }

    fun updateCutOffLoanSchedule(
        mtgguid: String,
        cbo_id: Long,
        loanno: Int,
        installmentno: Int,
        principaldemand: Int
    ) {
        dtMtgGrpLoanScheduleRepository!!.updateCutOffLoanSchedule(
            mtgguid,
            cbo_id,
            loanno,
            installmentno,
            principaldemand

        )
    }

    fun getPrincipalDemandByInstallmentNum(cbo_id: Long,mtg_guid: String,loan_no: Int,installment_no:Int):Int{
        return  dtMtgGrpLoanScheduleRepository!!.getPrincipalDemandByInstallmentNum(cbo_id,mtg_guid,loan_no,installment_no)
    }

    fun updaterepaid(
        loanno: Int,
        cbo_id: Long,
        installmentno: Int,
        sub_installment_no: Int,
        loanpaid: Int,
        mtgdate: Long,
        mtgno: Int,
        updatedby: String?,
        updatedon: Long
    )
    {
        dtMtgGrpLoanScheduleRepository!!. updaterepaid(
            loanno,
            cbo_id,
            installmentno,
            sub_installment_no,
            loanpaid,
            mtgdate ,mtgno ,updatedby,updatedon )

    }
    fun deleterepaid(
        loanno: Int,
        cbo_id: Long,
        mtgdate: Long,
        mtgno:Int)
    {
        dtMtgGrpLoanScheduleRepository!!. deleterepaid(
            loanno,
            cbo_id,
            mtgdate,mtgno   )

    }
    fun deletsubinstallment(
        loanno: Int,
        cbo_id: Long,
        mtgdate: Long,
        mtgno:Int)
    {
        dtMtgGrpLoanScheduleRepository!!. deletsubinstallment(
            loanno,
            cbo_id,
            mtgdate,mtgno   )

    }

    fun getUploadListData(cbo_id: Long,loanno:Int) : List<DtMtgGrpLoanScheduleEntity>{
        return dtMtgGrpLoanScheduleRepository!!.getUploadListData(cbo_id,loanno)
    }
    fun getGrpScheduleloanwise(
        cboId: Long,
        loanno: Int,
        mtgno: Int
    ): List<DtMtgGrpLoanScheduleEntity>?{
        return dtMtgGrpLoanScheduleRepository!!.getGrpScheduleloanwise(cboId, loanno, mtgno)
    }
    fun getnextdemand(cboid: Long,loanno:Int): Int {
        return dtMtgGrpLoanScheduleRepository!!.getnextdemand(cboid,loanno)
    }
}