package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.api.vomodel.VoECMemberDataModel
import com.microware.cdfi.entity.AddressEntity
import com.microware.cdfi.entity.Executive_memberEntity
import com.microware.cdfi.repository.ExecutiveMemberRepository

class ExecutiveMemberViewmodel : AndroidViewModel {

    var executiveRepository:  ExecutiveMemberRepository? = null

    constructor(application: Application):super(application){
        executiveRepository = ExecutiveMemberRepository(application)
    }

    fun insert(executiveEntity: Executive_memberEntity){
        return executiveRepository!!.insert(executiveEntity)
    }

    fun updateExecutiveMember(cbo_id:Long?,
                              cbo_guid:String?,
                              guid:String,
                              ec_cbo_code:Long?,
                              ec_cbo_id:Long?,
                              ec_member_code:Long?,
                              designation:Int?,
                              joining_date:Long?,
                              leaving_date:Long?,
                              status:Int?,
                              is_active:Int?,
                              entry_source:Int?,
                              is_edited:Int?,
                              updated_date:Long?,
                              updated_by:String?,
                              is_complete:Int,
                              isSignatory:Int?,
                              signatory_joining_date:Long?,
                              signatory_leaving_date:Long?){
        executiveRepository!!.updateExecutiveMember(cbo_id,
            cbo_guid,
            guid,
            ec_cbo_code,
            ec_cbo_id,
            ec_member_code,
            designation,
            joining_date,
            leaving_date,
            status,
            is_active,
            entry_source,
            is_edited,
            updated_date,
            updated_by,
            is_complete,
            isSignatory,
            signatory_joining_date,
            signatory_leaving_date)
    }

    fun getExecutivedetaildata(guid: String?): List<Executive_memberEntity>? {
        return executiveRepository!!.getExecutivedetaildata(guid)
    }
    fun getExecutiveList(cbo_guid: String?): LiveData<List<Executive_memberEntity>>?{
        return executiveRepository!!.getExecutiveList(cbo_guid)
    }

    fun getExecutiveMemberbyCode(cbo_guid:String,ec_cbo_code:Long): List<Executive_memberEntity>?{
        return executiveRepository!!.getExecutiveMemberbyCode(cbo_guid,ec_cbo_code)
    }

    fun getExecutive_count(cbo_guid:String,ec_cbo_code:Long):Int{
        return executiveRepository!!.getExecutive_count(cbo_guid,ec_cbo_code)
    }

    fun getECMember_count(cbo_guid:String,ec_member_code:Long):Int{
        return executiveRepository!!.getECMember_count(cbo_guid,ec_member_code)
    }

    fun getPendingExecutiveMember():List<Executive_memberEntity>?{
        return executiveRepository!!.getPendingExecutiveMember()
    }

    fun deleteRecord(ec_guid:String?){
        executiveRepository!!.deleteRecord(ec_guid)
    }

    fun deleteSCRecord(ec_guid:String?){
        executiveRepository!!.deleteSCRecord(ec_guid)
    }

    fun deleteEcMemeber(ec_guid:String?,updated_date: Long?,updated_by: String?){
        executiveRepository!!.deleteEcMemeber(ec_guid,updated_date,updated_by)
    }

    fun deleteScMemeber(ec_guid:String?,updated_date: Long?){
        executiveRepository!!.deleteScMemeber(ec_guid,updated_date)
    }

    fun getCboMemberCount(ec_cbo_code:Long):Int{
        return executiveRepository!!.getCboMemberCount(ec_cbo_code)
    }

    fun getDesignationCount(designation:Int,fedrationGuid:String):Int{
        return executiveRepository!!.getDesignationCount(designation,fedrationGuid)
    }

    fun getMemberJoiningDate(member_code:Long):Long{
        return executiveRepository!!.getMemberJoiningDate(member_code)
    }

    fun getScMember_count(memberGuid:String,ec_member_code:Long):Int{
        return executiveRepository!!.getScMember_count(memberGuid,ec_member_code)
    }

    fun updateEcuploaddata(
        lastupdatedate: Long,
        ec_guid:String
    ){
        executiveRepository!!.updateEcuploaddata(lastupdatedate,ec_guid)
    }

    fun deleteVOMappingdata(federationGuid:String) {
        executiveRepository!!.deleteVOMappingdata(federationGuid)
    }

    fun deleteVOShgMember(federationGuid:String) {
        executiveRepository!!.deleteVOShgMember(federationGuid)
    }

    fun deleteVOSHGMemberPhone(federationGuid:String) {
        executiveRepository!!.deleteVOSHGMemberPhone(federationGuid)
    }

    fun deleteClfMappingdata(federationGuid:String) {
        executiveRepository!!.deleteClfMappingdata(federationGuid)
    }

    fun deleteClfVOMember(federationGuid:String) {
        executiveRepository!!.deleteClfVOMember(federationGuid)
    }

    fun deleteClfVOMemberPhone(federationGuid:String) {
        executiveRepository!!.deleteClfVOMemberPhone(federationGuid)
    }

    internal fun getDistinctCboCount(cbo_id: Long?):Int{
        return executiveRepository!!.getDistinctCboCount(cbo_id)
    }

    internal fun getOBCount(designation:List<Int>,fedrationGuid:String):Int{
        return executiveRepository!!.getOBCount(designation,fedrationGuid)
    }
    fun getAllExecutivedetaildata1(
        ec_cbo_code: Long,
        mtgNo:Int,
        designation: List<Int>
    ): MutableList<VoECMemberDataModel>? {
        return executiveRepository!!.getAllExecutivedetaildata1(ec_cbo_code,mtgNo, designation)
    }
}