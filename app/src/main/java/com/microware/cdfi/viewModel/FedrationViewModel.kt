package com.microware.cdfi.viewModel

import android.app.Application
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.microware.cdfi.api.ApiInterface
import com.microware.cdfi.api.model.ParameterUploadModel
import com.microware.cdfi.entity.FederationEntity
import com.microware.cdfi.interfaces.ResultCallBacks
import com.microware.cdfi.repository.FedrationRepository
import com.microware.cdfi.utility.Validate
import com.wrms.wwfap.models.networks.ApiException
import com.wrms.wwfap.models.networks.NoInternetException

class FedrationViewModel : AndroidViewModel {

    var fedrationRepository: FedrationRepository? = null
    var listener: ResultCallBacks? = null
    var liveDataMaster: LiveData<Any>? = null

    constructor(application: Application):super(application){
        fedrationRepository = FedrationRepository(application)
    }

    internal fun getFedrationDetaildata(fedrationGUID: String?): LiveData<List<FederationEntity>>?{
        return fedrationRepository!!.getFedrationDetaildata(fedrationGUID)
    }

    internal fun getFedrationdata(fedrationGUID: String?): List<FederationEntity>?{
        return fedrationRepository!!.getFedrationdata(fedrationGUID)
    }

    fun insert(fedrationentity: FederationEntity){
        return fedrationRepository!!.insert(fedrationentity)
    }
    internal fun getVoCount(cboType:Int):Int{
        return fedrationRepository!!.getVoCount(cboType)
    }

    fun updateBasicDetail(
        federation_guid: String,
        panchayat_id: Int?,
        village_id: Int?,
        federation_name: String,
        federation_name_hindi: String,
        federation_name_local: String,
        federation_name_short: String,
        federation_formation_date: Long,
        federation_revival_date: Long,
        meeting_frequency: Int,
        meeting_frequency_value: Int,
        meeting_on: Int,
        month_comp_saving: Int,
        is_bankaccount: Int,
        parent_cbo_code: Int,
        parent_cbo_type: Int,
        is_active: Int,
        dedupl_status: Int,
        device:Int,
        activationStatus:Int,
        updated_date: Long,
        updated_by: String,
        saving_frequency:Int,
        is_volutary_saving: Int?,
        savings_interest: Double,
        voluntary_savings_interest: Double,
        primary_activity: Int,
        secondary_activity: Int,
        tertiary_activity: Int,
        bookkeeper_identified: Int,
        bookkeeper_name: String,
        bookkeeper_mobile: String,
        election_tenure: Int,
        status: Int,
        is_financial_intermediation: Int,
        meber_cbo_count: Int,
        cooption_date:Long,
        promotedby:Int,
        promoter_code:Int,
        promoter_name:String,
        user_id:String,
        is_completed:Int,
        is_edited:Int,
        approve_status: Int,
        imageName: String
    ) {
        fedrationRepository!!.updateBasicDetail(
            federation_guid,
            panchayat_id,
            village_id,
            federation_name,
            federation_name_hindi,
            federation_name_local,
            federation_name_short, federation_formation_date, federation_revival_date, meeting_frequency,
            meeting_frequency_value,meeting_on,month_comp_saving,is_bankaccount,parent_cbo_code,
            parent_cbo_type,is_active,dedupl_status,device,activationStatus,
            updated_date,updated_by,saving_frequency,
            is_volutary_saving,
            savings_interest,
            voluntary_savings_interest,
            primary_activity,
            secondary_activity,
            tertiary_activity,
            bookkeeper_identified,
            bookkeeper_name,
            bookkeeper_mobile,
            election_tenure,status,
            is_financial_intermediation,
            meber_cbo_count,cooption_date,promotedby,promoter_code,promoter_name,user_id,is_completed,is_edited,approve_status,imageName
        )
    }



    internal fun getAllFederationdata(cboType:Int): List<FederationEntity>? {
        return fedrationRepository!!.getAllFederationdata(cboType)
    }

    internal fun getFederation_by_village(village_id:Int,cboType: Int): LiveData<List<FederationEntity>>? {
        return fedrationRepository!!.getFederation_by_village(village_id,cboType)
    }
    internal fun getFedrationAlldata(cboType:Int): LiveData<List<FederationEntity>>? {
        return fedrationRepository!!.getFedrationAlldata(cboType)
    }

    internal fun getallVoCount():Int{
        return fedrationRepository!!.getallVoCount()
    }
    internal fun getallVoPendingCount():Int{
        return fedrationRepository!!.getallVoPendingCount()
    }
    internal fun getallVoActiveCount():Int{
        return fedrationRepository!!.getallVoActiveCount()
    }

    internal fun getcount(federation_guid: String?): Int {
        return fedrationRepository?.getcount(federation_guid)!!
    }

    internal fun updateisedit(guid: String ) {
        return fedrationRepository?.updateisedit(guid)!!
    }

    internal   fun updatededup(
        guid: String ){
        fedrationRepository!!.updatededup(guid)
    }

    internal fun getIsCompleteValue(guid:String??):Int{
        return fedrationRepository!!.getIsCompleteValue(guid)
    }

    internal fun getEcMemberCount(guid:String?):Int{
        return fedrationRepository!!.getEcMemberCount(guid)
    }

    internal fun getPhoneCount(guid:String?):Int{
        return fedrationRepository!!.getPhoneCount(guid)
    }

    internal fun getBankCount(guid:String?):Int{
        return fedrationRepository!!.getBankCount(guid)
    }

    internal fun getAddressCount(guid:String?):Int{
        return fedrationRepository!!.getAddressCount(guid)
    }

    internal fun getScCount(guid:String?):Int{
        return fedrationRepository!!.getScCount(guid)
    }

    internal fun getKycCount(guid:String?):Int{
        return fedrationRepository!!.getKycCount(guid)
    }

    internal fun getMappedShgCount(guid:String?):Int{
        return fedrationRepository!!.getMappedShgCount(guid)
    }

    internal fun getMappedVoCount(guid:String?):Int{
        return fedrationRepository!!.getMappedVoCount(guid)
    }
    internal fun updateFedrationDedupStatus(
        guid: String,
        code: String,
        activationStatus: Int,
        approve_status: Int,
        checker_remarks: String



    ){
        fedrationRepository!!.updateFedrationDedupStatus(
            guid,
            code,
            activationStatus,
            approve_status,
            checker_remarks

        )
    }
    fun updateFedrationEditFlag(is_edited: Int,guid: String?){
        fedrationRepository!!.updateFedrationEditFlag(is_edited,guid)
    }
    fun getlastdate(): Long{
        return fedrationRepository!!.getlastdate()
    }

    internal fun getVoPendingCount(cboType:Int):Int{
        return fedrationRepository!!.getVoPendingCount(cboType)
    }
    internal fun getVoActiveCount(cboType:Int):Int{
        return fedrationRepository!!.getVoActiveCount(cboType)
    }

    fun updateFedrationCode(cbo_id: Long,guid: String?){
        return fedrationRepository!!.updateFedrationCode(cbo_id,guid)
    }

    internal fun deleteFederationByGuid(federationGUID: String?){
        return fedrationRepository!!.deleteFederationByGuid(federationGUID)
    }

    internal fun deleteBankByfederationGUID(federationGUID: String?){
        return fedrationRepository!!.deleteBankByfederationGUID(federationGUID)
    }

    internal fun deletePhoneByfederationGUID(federationGUID: String?){
        return fedrationRepository!!.deletePhoneByfederationGUID(federationGUID)
    }

    internal fun deleteAddressByfederationGUID(federationGUID: String?){
        return fedrationRepository!!.deleteAddressByfederationGUID(federationGUID)
    }

    internal fun deleteEcByfederationGUID(federationGUID: String?){
        return fedrationRepository!!.deleteEcByfederationGUID(federationGUID)
    }

    internal fun deleteScByfederationGUID(federationGUID: String?){
        return fedrationRepository!!.deleteScByfederationGUID(federationGUID)
    }

    internal fun deleteScMemberByfederationGUID(federationGUID: String?){
        return fedrationRepository!!.deleteScMemberByfederationGUID(federationGUID)
    }

    internal fun deleteFederationDataByGuid(federationGUID: String?){
        return fedrationRepository!!.deleteFederationDataByGuid(federationGUID)
    }

    internal fun deleteBankDataByfederationGUID(federationGUID: String?){
        return fedrationRepository!!.deleteBankDataByfederationGUID(federationGUID)
    }

    internal fun deletePhoneDataByfederationGUID(federationGUID: String?){
        return fedrationRepository!!.deletePhoneDataByfederationGUID(federationGUID)
    }

    internal fun deleteAddressDataByfederationGUID(federationGUID: String?){
        return fedrationRepository!!.deleteAddressDataByfederationGUID(federationGUID)
    }

    internal fun deleteEcDataByfederationGUID(federationGUID: String?){
        return fedrationRepository!!.deleteEcDataByfederationGUID(federationGUID)
    }

    internal fun deleteScDataByfederationGUID(federationGUID: String?){
        return fedrationRepository!!.deleteScDataByfederationGUID(federationGUID)
    }

    internal fun deleteScMemberDataByfederationGUID(federationGUID: String?){
        return fedrationRepository!!.deleteScMemberDataByfederationGUID(federationGUID)
    }

    internal fun updateFedrationId(guid: String?,federation_id:Long?){
        fedrationRepository!!.updateFedrationId(guid,federation_id)
    }

    internal fun getEcCount(shg_code:Long,cbo_guid:String):Int{
        return fedrationRepository!!.getEcCount(shg_code,cbo_guid)
    }

    internal  fun getIfscCode():List<String>{
        return fedrationRepository!!.getIfscCode()
    }

    internal  fun getFI(federationGUID: String?):Int{
        return fedrationRepository!!.getFI(federationGUID)
    }

    internal fun getSCMemberCount(federationGuid:String):Int{
        return fedrationRepository!!.getSCMemberCount(federationGuid)
    }

    internal fun getSignatoryCount(cbo_guid:String):Int{
        return fedrationRepository!!.getSignatoryCount(cbo_guid)
    }

    internal fun getFederation_by_PanchayatCode(panchayat_id:Int,cboType:Int): LiveData<List<FederationEntity>>?{
        return fedrationRepository!!.getFederation_by_PanchayatCode(panchayat_id,cboType)
    }

    internal fun getClfDataByBlock(block_id:Int,cboType:Int): LiveData<List<FederationEntity>>?{
        return fedrationRepository!!.getClfDataByBlock(block_id,cboType)
    }

    internal fun getVoPendingCount(panchayat_id:Int,cboType:Int):Int{
        return fedrationRepository!!.getVoPendingCount(panchayat_id,cboType)
    }

    internal fun getCLFPendingCount(block_id:Int,cboType:Int):Int{
        return fedrationRepository!!.getCLFPendingCount(block_id,cboType)
    }

    internal fun getVoCount(panchayat_id:Int,cboType: Int):Int{
        return fedrationRepository!!.getVoCount(panchayat_id,cboType)
    }

    internal fun getCLFCount(block_id:Int,cboType: Int):Int{
        return fedrationRepository!!.getCLFCount(block_id,cboType)
    }

    internal fun getVoActiveCount(panchayat_id:Int,cboType:Int):Int{
        return fedrationRepository!!.getVoActiveCount(panchayat_id,cboType)
    }

    internal fun getCLFActiveCount(block_id:Int,cboType:Int):Int{
        return fedrationRepository!!.getCLFActiveCount(block_id,cboType)
    }
}