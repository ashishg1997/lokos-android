package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.microware.cdfi.api.meetinguploadmodel.ShgFinanceTransactionDetailMemberData
import com.microware.cdfi.entity.FinancialTransactionsMemEntity
import com.microware.cdfi.repository.FinancialTransactionsMemRepository

class FinancialTransactionsMemViewmodel : AndroidViewModel {

    var dtFinTxnDetRepository: FinancialTransactionsMemRepository? = null

    constructor(application: Application) : super(application) {
        dtFinTxnDetRepository = FinancialTransactionsMemRepository(application)
    }

    fun getMtgFinTxnDetdata(mtg_guid: String,cbo_id: Long,mem_id: Long,mtg_no: Int,auid:Int): List<FinancialTransactionsMemEntity>? {
        return dtFinTxnDetRepository!!.getMtgFinTxnDetdata(mtg_guid,cbo_id, mem_id, mtg_no,auid)
    }

    fun insert(dtFinTxnDetEntity: FinancialTransactionsMemEntity) {
        return dtFinTxnDetRepository!!.insert(dtFinTxnDetEntity)
    }

    fun deleteRecord(mtg_guid: String) {
        dtFinTxnDetRepository!!.deleteRecord(mtg_guid)
    }

    fun updateShareCapitalDetail(
        mtg_guid: String,
        cbo_id: Long,
        mem_id: Long,
        mtg_no: Int,
        auid: Int,
        bank_code: String?,
        type: String?,
        amount: Int?,
        trans_date: Long?,
        date_realisation: Long?,
        mode_payment: Int?,
        transaction_no: String?,
        updatedby: String?,
        updatedon: Long?
    ) {
        dtFinTxnDetRepository!!.updateShareCapitalDetail(
            mtg_guid,
            cbo_id,
            mem_id,
            mtg_no,
            auid,
            bank_code,
            type,
            amount,
            trans_date,
            date_realisation,
            mode_payment,
            transaction_no,
            updatedby,
            updatedon
        )
    }

    fun getIncomeAmount(cbo_id: Long,mtg_no: Int,modePayment:Int,type:String): Int {
        return dtFinTxnDetRepository!!.getIncomeAmount(cbo_id, mtg_no, modePayment,type)

    }

    fun getMemberIncomeAmount(cbo_id: Long,mtg_no: Int,type:String): Int {
        return dtFinTxnDetRepository!!.getMemberIncomeAmount(cbo_id, mtg_no,type)

    }

    fun getBankAmount(cbo_id: Long,mtg_no: Int,type:String): Int {
        return dtFinTxnDetRepository!!.getBankAmount(cbo_id, mtg_no,type)

    }

    fun getBankCodeAmount(cbo_id: Long,mtg_no: Int,bank_code: String?,type:String): Int {
        return dtFinTxnDetRepository!!.getBankCodeAmount(cbo_id, mtg_no,bank_code,type)

    }

    fun getFinancialTransactionsMemAmount(cbo_id: Long,mtg_no: Int,bank_code:String,type:String): Int{
        return dtFinTxnDetRepository!!.getFinancialTransactionsMemAmount(cbo_id, mtg_no, bank_code,type)
    }


    fun getUploadData(mtg_no: Int,cbo_id: Long,mem_id:Long) : List<FinancialTransactionsMemEntity>{
        return dtFinTxnDetRepository!!.getUploadData(mtg_no,cbo_id,mem_id)
    }
}