package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.microware.cdfi.entity.FundTypeEntity
import com.microware.cdfi.repository.FundTypeRepository

class FundTypeViewmodel : AndroidViewModel {

    var mRepository: FundTypeRepository? = null

    constructor(application: Application) : super(application) {
        mRepository = FundTypeRepository(application)
    }

    internal fun getFundslist(langID: String):List<FundTypeEntity>?{
        return mRepository!!.getFundslist(langID)
    }

    fun getFundName(fubdTypeID:Int,langID: String):String?{
        return mRepository!!.getFundName(fubdTypeID,langID)
    }

}