package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.microware.cdfi.entity.FundingEntity
import com.microware.cdfi.repository.FundingAgencyRepository
import com.microware.cdfi.repository.LookupRepository

class FundingAgencyViewModel:AndroidViewModel {

    var mRepository: FundingAgencyRepository? = null

    constructor(application: Application) : super(application) {
        mRepository = FundingAgencyRepository(application)
    }

    internal fun getfundingAgency(languageId:String):List<FundingEntity>{
        return mRepository!!.getfundingAgency(languageId)
    }
}