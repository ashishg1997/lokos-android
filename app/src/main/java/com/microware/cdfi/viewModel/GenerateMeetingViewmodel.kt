package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.microware.cdfi.api.meetingmodel.LoanListModel
import com.microware.cdfi.api.meetingmodel.LoanRepaymentListModel
import com.microware.cdfi.api.meetingmodel.ShareCapitalModel
import com.microware.cdfi.api.model.SHGMeetingModel
import com.microware.cdfi.entity.*
import com.microware.cdfi.repository.GenerateMeetingRepository

class GenerateMeetingViewmodel: AndroidViewModel {

    var generateMeetingRepository:  GenerateMeetingRepository? = null

    constructor(application: Application):super(application){
        generateMeetingRepository = GenerateMeetingRepository(application)
    }

    fun insertlookup(lookup: LookupEntity){
        return generateMeetingRepository!!.insertlookup(lookup)
    }

    fun getlookupCount(id: Int): Int {
        return generateMeetingRepository!!.getlookupCount(id)
    }

    fun getmeetingCount(cbo_id: String): Int {
        return generateMeetingRepository!!.getmeetingCount(cbo_id)
    }

    fun gettotmeetingCount(): Int {
        return generateMeetingRepository!!.gettotmeetingCount()
    }
    fun getupdatedpeningcash(mtgno:Int,cbo_id:Long):Int  {
        return generateMeetingRepository!!.getupdatedpeningcash(mtgno,cbo_id)
    }

    fun gettotloan(mtgno:Int,cbo_id:Long):Int  {
        return generateMeetingRepository!!.gettotloan(mtgno,cbo_id)
    }

    fun gettotalMemberLoan(mtgno:Int,cbo_id:Long):Int  {
        return generateMeetingRepository!!.gettotalMemberLoan(mtgno,cbo_id)
    }
    fun gettotloanpaid(mtgno: Int, cbo_id: Long): Int {
        return generateMeetingRepository!!.gettotloanpaid(mtgno, cbo_id)
    }
    fun getmemtotloanpaidinbank(mtgno:Int,cbo_id:Long,account:String): Int {
        return generateMeetingRepository!!.getmemtotloanpaidinbank(mtgno, cbo_id,account)
    }

    fun gettotloangroup(mtgno:Int,cbo_id:Long):Int  {
        return generateMeetingRepository!!.gettotloangroup(mtgno,cbo_id)
    }

    fun getTotalGrouploan(mtgno:Int,cbo_id:Long):Int  {
        return generateMeetingRepository!!.getTotalGrouploan(mtgno,cbo_id)
    }

    fun getTotalLoans(mtgno:Int,cbo_id:Long): Int{
        return generateMeetingRepository!!.getTotalLoans(mtgno,cbo_id)
    }

    fun getTotalCutOffLoanReceived(mtgno:Int,cbo_id:Long): Int{
        return generateMeetingRepository!!.getTotalCutOffLoanReceived(mtgno,cbo_id)
    }

    fun getgrptotloanpaidinbank(mtgno:Int,cbo_id:Long,account:String): Int {
        return generateMeetingRepository!!.getgrptotloanpaidinbank(mtgno, cbo_id,account)
    }

    fun getgrouptotloanamtinbank(mtgno: Int, cbo_id: Long, code: String):Int  {
        return generateMeetingRepository!!.getgrouptotloanamtinbank(mtgno,cbo_id,code)
    }
    fun gettotloanpaidgroup(mtgno: Int, cbo_id: Long): Int {
        return generateMeetingRepository!!.gettotloanpaidgroup(mtgno, cbo_id)
    }

    fun getgroupMeetingsdata(shg_code: String?): List<SHGMeetingModel>? {
        return generateMeetingRepository!!.getgroupMeetingsdata(shg_code)
    }
    fun getgroupMeetingsdatalist(): List<SHGMeetingModel>? {
        return generateMeetingRepository!!.getgroupMeetingsdatalist()
    }
    fun getgroupMeetingsdatalistbyvillage(villageid: Int?): List<SHGMeetingModel>? {
        return generateMeetingRepository!!.getgroupMeetingsdatalistbyvillage(villageid)
    }
    fun getMtg(cbo_id: String?): List<DtmtgEntity>? {
        return generateMeetingRepository!!.getMtg(cbo_id)
    }
    fun getMtglist(cbo_id: Long?): List<DtmtgEntity>? {
        return generateMeetingRepository!!.getMtglist(cbo_id)
    }
    fun getMtglistdata(cbo_id: Long?): List<DtmtgEntity>? {
        return generateMeetingRepository!!.getMtglistdata(cbo_id)
    }
    fun returnmeetingstatus(shgid: Long,Mtgno:Int): String {
        return generateMeetingRepository!!.returnmeetingstatus(shgid,Mtgno)
    }
    fun insert(dtmtgEntity: DtmtgEntity){
        return generateMeetingRepository!!.insert(dtmtgEntity)
    }
    fun insertdtmtgDet(dtmtgDetEntity: DtmtgDetEntity){
        return generateMeetingRepository!!.insertdtmtgDet(dtmtgDetEntity)
    }

    internal fun getMtgByMtgnum(shg_id: Long?,mtgnum:Int): List<DtmtgDetEntity>{
        return generateMeetingRepository!!.getMtgByMtgnum(shg_id,mtgnum)
    }
    internal fun getListDataMtgByMtgnum(): List<DtmtgDetEntity>{
        return generateMeetingRepository!!.getListDataMtgByMtgnum()
    }

    internal fun getListDataMtgByMtgnum(mtgno:Int,cbo_id:Long): List<DtmtgDetEntity> {
        return generateMeetingRepository!!.getListDataMtgByMtgnum(mtgno,cbo_id)
    }

    internal fun getloanListDataMtgByMtgnum(mtgno:Int,cbo_id:Long): List<LoanListModel> {
        return generateMeetingRepository!!.getloanListDataMtgByMtgnum(mtgno,cbo_id)
    }

    internal fun getloanrepaymentList(mtgno:Int,cbo_id:Long): List<LoanRepaymentListModel> {
        return generateMeetingRepository!!.getloanrepaymentList(mtgno,cbo_id)
    }

    internal fun gettotcountnotsaved(mtgno:Int,cbo_id:Long): Int {
        return generateMeetingRepository!!.gettotcountnotsaved(mtgno,cbo_id)
    }

    internal fun getupdatedcash(mtgno:Int,cbo_id:Long): Int {
        return generateMeetingRepository!!.getupdatedcash(mtgno,cbo_id)
    }

    fun updateAttendance(
        attendance: String,
        mtgno:Int,
        cbo_id:Long,
        group_m_code:Long,
        updated_by:String,
        updated_on:Long
    ) {
        generateMeetingRepository!!.updateAttendance(
            attendance,
            mtgno,
            cbo_id,
            group_m_code,
            updated_by,
            updated_on
        )
    }
    internal fun getTotalPresentAndAbsent(attendance:String,mtgno:Int,cbo_id:Long): Int {
        return generateMeetingRepository!!.getTotalPresentAndAbsent(attendance,mtgno,cbo_id)
    }
    fun updateCompulsorySaving(
        sav_comp: Int,
        sav_comp_cb: Int,
        mtgno:Int,
        cbo_id:Long,
        group_m_code:Long,
        updated_by:String,
        updated_on:Long
    ) {
        generateMeetingRepository!!.updateCompulsorySaving(
            sav_comp,
            sav_comp_cb,
            mtgno,
            cbo_id,
            group_m_code,
            updated_by,
            updated_on
        )
    }
    fun updateVoluntorySaving(
        sav_vol: Int,
        sav_vol_cb: Int,
        mtgno:Int,
        cbo_id:Long,
        group_m_code:Long,
        updated_by:String,
        updated_on:Long
    ) {
        generateMeetingRepository!!.updateVoluntorySaving(
            sav_vol,
            sav_vol_cb,
            mtgno,
            cbo_id,
            group_m_code,
            updated_by,
            updated_on
        )
    }
    fun updatePenaltySaving(
        penalty: Int,
        mtgno:Int,
        cbo_id:Long,
        group_m_code:Long,
        updated_by:String,
        updated_on:Long
    ) {
        generateMeetingRepository!!.updatePenaltySaving(
            penalty,
            mtgno,
            cbo_id,
            group_m_code,
            updated_by,
            updated_on
        )
    }
    fun updateclosing(mem_id: Long,mtgno: Int,sav_comp_cb: Int,sav_vol_cb: Int) {
        generateMeetingRepository!!.updateclosing(mem_id,mtgno,sav_comp_cb,sav_vol_cb)
    }
    fun updateloandata1(mem_id: Long, mtgno: Int, loanno: Int, loanop: Int, loan_int_accrued: Int) {
        generateMeetingRepository!!.updateloandata1(mem_id, mtgno, loanno, loanop,loan_int_accrued)
    }

    fun updateloandata2(mem_id: Long, mtgno: Int, loanno: Int, loanop: Int, loan_int_accrued: Int) {
        generateMeetingRepository!!.updateloandata2(mem_id, mtgno, loanno, loanop,loan_int_accrued)
    }

    fun updateloandata3(mem_id: Long, mtgno: Int, loanno: Int, loanop: Int, loan_int_accrued: Int) {
        generateMeetingRepository!!.updateloandata3(mem_id, mtgno, loanno, loanop,loan_int_accrued)
    }

    fun updateloandata4(mem_id: Long, mtgno: Int, loanno: Int, loanop: Int, loan_int_accrued: Int) {
        generateMeetingRepository!!.updateloandata4(mem_id, mtgno, loanno, loanop,loan_int_accrued)
    }

    fun updateloandata5(mem_id: Long, mtgno: Int, loanno: Int, loanop: Int, loan_int_accrued: Int) {
        generateMeetingRepository!!.updateloandata5(mem_id, mtgno, loanno, loanop,loan_int_accrued)
    }
    internal fun getsumcomp(mtgno:Int,cbo_id:Long): Int {
        return generateMeetingRepository!!.getsumcomp(mtgno,cbo_id)
    }

    internal fun getsumvol(mtgno:Int,cbo_id:Long): Int {
        return generateMeetingRepository!!.getsumvol(mtgno,cbo_id)
    }
internal fun getsumwithdrwal(mtgno:Int,cbo_id:Long): Int {
        return generateMeetingRepository!!.getsumwithdrwal(mtgno,cbo_id)
    }

    internal fun getsumattendance(mtgno:Int,cbo_id:Long): Int {
        return generateMeetingRepository!!.getsumattendance(mtgno,cbo_id)
    }


    internal fun getCurrentMeetingslist(c_date:Long): List<SHGMeetingModel>?{
        return generateMeetingRepository!!.getCurrentMeetingslist(c_date)
    }

    internal fun getMissedMeetingslist(c_date:Long): List<SHGMeetingModel>?{
        return generateMeetingRepository!!.getMissedMeetingslist(c_date)
    }
    internal fun getUpcomingMeetingslist(c_date:Long): List<SHGMeetingModel>?{
        return generateMeetingRepository!!.getUpcomingMeetingslist(c_date)
    }
    internal fun getBankdata(cbo_guid: String?): List<Cbo_bankEntity>{
        return generateMeetingRepository!!.getBankdata(cbo_guid)
    }

    internal fun getCapitalListDataMtgByMtgnum(mtgno:Int,cbo_id:Long,type:String): List<ShareCapitalModel>{
        return generateMeetingRepository!!.getCapitalListDataMtgByMtgnum(mtgno,cbo_id,type)
    }

    fun getCutOffTotalPresent(mtgno:Int,cbo_id:Long): Int{
        return generateMeetingRepository!!.getCutOffTotalPresent(mtgno,cbo_id)
    }

    fun getMaxMeetingNum(shg_id: Long?): Int{
        return generateMeetingRepository!!.getMaxMeetingNum(shg_id)
    }

    fun getMeetingCount(shg_id: Long?): Int{
        return generateMeetingRepository!!.getMeetingCount(shg_id)
    }

    fun getShareCapitalListByReceiptType(
        mtgno: Int, cbo_id: Long,
        receiptType:Int
    ): List<ShareCapitalModel>{
        return generateMeetingRepository!!.getShareCapitalListByReceiptType(mtgno, cbo_id,receiptType)
    }

    fun getTotalAmountByReceiptType(mtg_guid: String, cbo_id: Long,mtg_no: Int,auid:Int): Int{
        return generateMeetingRepository!!.getTotalAmountByReceiptType(mtg_guid,cbo_id,mtg_no,auid)
    }
    fun getTotalIncomeAndExpenditureByFundType(mtg_guid: String, cbo_id: Long, mtg_no: Int,fund_type:Int): Int{
        return generateMeetingRepository!!.getTotalIncomeAndExpenditureByFundType(mtg_guid,cbo_id,mtg_no,fund_type)
    }
    fun getSecondLastMeetingNum(shg_id: Long,mtg_num: Int):Int{
        return generateMeetingRepository!!.getSecondLastMeetingNum(shg_id,mtg_num)
    }

    fun getopeningcash(mtgno: Int, cbo_id: Long): Int{
        return generateMeetingRepository!!.getopeningcash(mtgno, cbo_id)
    }

    internal fun getTotalMember(mtgno: Int, cbo_id: Long): Int {
        return generateMeetingRepository!!.getTotalMember(mtgno, cbo_id)
    }

    fun getCompulsoryData(mtgno: Int, cbo_id: Long,mem_id: Long): List<DtmtgDetEntity>{
        return generateMeetingRepository!!.getCompulsoryData(mtgno,cbo_id,mem_id)
    }
    fun updateCutOffAttendance(
        attendance: String,
        mtgno: Int,
        cbo_id: Long,
        group_m_code: Long,
        updated_by:String,
        updated_on:Long
    ) {
        generateMeetingRepository!!.updateCutOffAttendance(
            attendance,
            mtgno,
            cbo_id,
            group_m_code,
            updated_by,
            updated_on
        )
    }

    fun updateCutOffSaving(
        compulsaryAmount: Int,
        voluntaryAmount: Int,
        mtgno: Int,
        cbo_id: Long,
        group_m_code: Long,
        updated_by:String,
        updated_on:Long
    ){
        generateMeetingRepository!!.updateCutOffSaving(
            compulsaryAmount,
            voluntaryAmount,
            mtgno,
            cbo_id,
            group_m_code,
            updated_by,
            updated_on
        )
    }

    fun updateclosingcash(closingcash: Int,mtgno:Int,cbo_id:Long) {
        generateMeetingRepository!!.updateclosingcash(closingcash, mtgno, cbo_id)
    }

    fun getCutOffmaxLoanno(cbo_id: Long,mtgNo:Int): Int{
        return generateMeetingRepository!!.getCutOffmaxLoanno(cbo_id,mtgNo)
    }

    fun updateclosingbalbank(
        closingbal: Int,
        mtgno: Int,
        cbo_id: Long,
        accountno: String
    ) {
        generateMeetingRepository!!.updateclosingbalbank(closingbal, mtgno, cbo_id,accountno)
    }

    fun gettotalinbank(mtgno: Int, cbo_id: Long): Int {
        return generateMeetingRepository!!.gettotalinbank(mtgno, cbo_id)
    }
    fun getopenningbal(mtgno: Int, cbo_id: Long,accountno: String): Int {
        return generateMeetingRepository!!.getopenningbal(mtgno, cbo_id,accountno)
    }
    fun getclosingbal(mtgno: Int, cbo_id: Long,accountno: String): Int {
        return generateMeetingRepository!!.getclosingbal(mtgno, cbo_id,accountno)
    }
    internal fun getCutOffloanListDataByMtgnum(mtgno: Int, cbo_id: Long,completionFlag:Boolean): List<LoanListModel> {
        return generateMeetingRepository!!.getCutOffloanListDataByMtgnum(mtgno, cbo_id,completionFlag)
    }
    internal fun getCutOffClosedMemberLoanDataByMtgNum(mtgno: Int, cbo_id: Long,completionFlag:Boolean): List<LoanListModel> {
        return generateMeetingRepository!!.getCutOffClosedMemberLoanDataByMtgNum(mtgno, cbo_id,completionFlag)
    }
    fun getShgMeetingList(): List<DtmtgEntity>{
        return generateMeetingRepository!!.getShgMeetingList()
    }

    fun getUploadListData(): List<DtmtgEntity>{
        return generateMeetingRepository!!.getUploadListData()
    }

    internal fun updateCutOffCash(
        cashInHand: Int,
        cashInTransit: Int,
        balanceDate: Long,
        closingcash: Int,
        mtgno:Int,
        cbo_id:Long,
        updatedOn:Long,
        updatedBy:String
    ){
        generateMeetingRepository!!.updateCutOffCash(
            cashInHand,
            cashInTransit,
            balanceDate,
            closingcash,
            mtgno,
            cbo_id,
            updatedOn,
            updatedBy
        )
    }

    internal fun getMeetingDetailData(mtgno:Int,cbo_id:Long):List<DtmtgEntity>{
        return  generateMeetingRepository!!.getMeetingDetailData(mtgno,cbo_id)
    }

    internal  fun updateCutOffBankBalance(
        cbo_id: Long?,
        mtgno: Int?,
        bankcode: String?,
        zero_mtg_cash_bank: Int?,
        cheque_issued_not_realized: Int?,
        cheque_received_not_credited: Int?,
        closingbalance: Int?,
        balance_date: Long?,
        updatedby: String,
        updatedon: Long?,
        uploaded_by: String?
    ){
        generateMeetingRepository!!.updateCutOffBankBalance(
            cbo_id,
            mtgno,
            bankcode,
            zero_mtg_cash_bank,
            cheque_issued_not_realized,
            cheque_received_not_credited,
            closingbalance,
            balance_date,
            updatedby,
            updatedon,
            uploaded_by
        )
    }

    internal fun getBankListDataByMtgnum(mtgno:Int,cbo_id:Long): List<DtMtgFinTxnEntity>{
        return generateMeetingRepository!!.getBankListDataByMtgnum(mtgno,cbo_id)
    }

    internal fun getCutOffMemberLoanAmount(cbo_id: Long,mtg_no: Int,completionFlag:Boolean):Int{
        return generateMeetingRepository!!.getCutOffMemberLoanAmount(cbo_id,mtg_no,completionFlag)
    }

    fun closingMeeting(flag: String,mtgno:Int,cbo_id:Long,updatedOn:Long,updatedBy:String) {
        generateMeetingRepository!!.closingMeeting(flag, mtgno, cbo_id,updatedOn,updatedBy)
    }

    fun openMeeting(flag: String,mtgno:Int,cbo_id:Long) {
        generateMeetingRepository!!.openMeeting(flag, mtgno, cbo_id)
    }

    fun getTotalInvestmentByMtgNum(cbo_id: Long, mtg_no: Int,auid:List<Int>,type:String): Int{
        return generateMeetingRepository!!.getTotalInvestmentByMtgNum(cbo_id,mtg_no,auid,type)
    }

    internal fun getTotalBankLoanOutstanding(mtg_no:Int,cbo_id:Long,loan_source:Int):Int{
        return generateMeetingRepository!!.getTotalBankLoanOutstanding(mtg_no,cbo_id,loan_source)
    }

    internal fun getTotalLoanOutstandingByFundType(mtg_no:Int,cbo_id:Long,fund_type:Int):Int{
        return generateMeetingRepository!!.getTotalLoanOutstandingByFundType(mtg_no,cbo_id,fund_type)
    }

    internal fun getTotalInterestOverdue(mtg_no:Int,cbo_id:Long):Int{
        return generateMeetingRepository!!.getTotalInterestOverdue(mtg_no,cbo_id)
    }

    internal fun getTotalInvestmentByAuid( mtg_no: Int,cbo_id: Long,auid:Int,type:String): Int{
        return generateMeetingRepository!!.getTotalInvestmentByAuid( mtg_no,cbo_id,auid,type)
    }

    internal  fun getOtherLoanOutstanding(mtg_no:Int,cbo_id:Long):Int{
        return generateMeetingRepository!!.getOtherLoanOutstanding(mtg_no,cbo_id)
    }

    internal fun getTotalOutstandingMemberLoan(cbo_id: Long,mtg_no: Int, completionFlag: Boolean):Int{
        return generateMeetingRepository!!.getTotalOutstandingMemberLoan(cbo_id,mtg_no,completionFlag)
    }

    internal fun getTotalMemberInterestOverdue(cbo_id: Long,mtg_no: Int):Int{
        return generateMeetingRepository!!.getTotalMemberInterestOverdue(cbo_id,mtg_no)
    }

    fun getUploadListData1(mtg_no : Int,cbo_id: Long) : List<DtMtgFinTxnEntity>{
        return generateMeetingRepository!!.getUploadListData1(mtg_no,cbo_id)
    }

    fun getUploadListData(mtg_no:Int,cbo_id: Long): List<DtmtgDetEntity>{
        return generateMeetingRepository!!.getUploadListData(mtg_no,cbo_id)
    }

    fun getTotalPenaltyByMtgNum(mtgno: Int, cbo_id: Long): Int{
        return generateMeetingRepository!!.getTotalPenaltyByMtgNum(mtgno, cbo_id)
    }

    fun deleteShg_MtgData(mtg_no: Int,cbo_id: Long){
        generateMeetingRepository!!.deleteShg_MtgData(mtg_no,cbo_id)
    }

    fun deleteShg_MtgByCboId(cbo_id: Long){
        generateMeetingRepository!!.deleteShg_MtgByCboId(cbo_id)
    }

    fun deleteShgDetailData(mtg_no: Int,cbo_id: Long){
        generateMeetingRepository!!.deleteShgDetailData(mtg_no,cbo_id)
    }

    fun deleteShgDetailDataByCboId(cbo_id: Long){
        generateMeetingRepository!!.deleteShgDetailDataByCboId(cbo_id)
    }

    fun deleteFinancialTransactionMemberDetailData(mtg_no: Int,cbo_id: Long){
        generateMeetingRepository!!.deleteFinancialTransactionMemberDetailData(mtg_no,cbo_id)
    }

    fun deleteFinancialTransactionMemberDetailDataByCboId(cbo_id: Long){
        generateMeetingRepository!!.deleteFinancialTransactionMemberDetailDataByCboId(cbo_id)
    }


    fun deleteGrpFinancialTxnDetailData(mtg_no: Int,cbo_id: Long){
        generateMeetingRepository!!.deleteGrpFinancialTxnDetailData(mtg_no,cbo_id)
    }

    fun deleteGrpFinancialTxnDetailDataByCboId(cbo_id: Long){
        generateMeetingRepository!!.deleteGrpFinancialTxnDetailDataByCboId(cbo_id)
    }

    fun deleteShgFinancialTxnData(mtg_no: Int,cbo_id: Long){
        generateMeetingRepository!!.deleteShgFinancialTxnData(mtg_no,cbo_id)
    }

    fun deleteShgFinancialTxnDataByCboId(cbo_id: Long){
        generateMeetingRepository!!.deleteShgFinancialTxnDataByCboId(cbo_id)
    }

    fun deleteMemberLoanTxnData(mtg_no: Int,cbo_id: Long){
        generateMeetingRepository!!.deleteMemberLoanTxnData(mtg_no,cbo_id)
    }

    fun deleteMemberLoanTxnDataByCboId(cbo_id: Long){
        generateMeetingRepository!!.deleteMemberLoanTxnDataByCboId(cbo_id)
    }

    fun deleteGroupLoanData(mtg_no:Int,cbo_id: Long){
        return generateMeetingRepository!!.deleteGroupLoanData(mtg_no,cbo_id)
    }

    fun deleteGroupLoanDataByCboId(cbo_id: Long){
        return generateMeetingRepository!!.deleteGroupLoanDataByCboId(cbo_id)
    }

    fun deleteGroupLoanTxnData(mtg_no:Int,cbo_id: Long){
        return generateMeetingRepository!!.deleteGroupLoanTxnData(mtg_no,cbo_id)
    }
    fun deleteGroupLoanTxnDataByCboId(cbo_id: Long){
        return generateMeetingRepository!!.deleteGroupLoanTxnDataByCboId(cbo_id)
    }

    fun deleteMCPData(mtg_no:Int,cbo_id:Long){
        return generateMeetingRepository!!.deleteMCPData(mtg_no,cbo_id)
    }

    fun deleteMCPDataByCboId(cbo_id:Long){
        return generateMeetingRepository!!.deleteMCPDataByCboId(cbo_id)
    }

    fun deleteMemberLoan(cbo_id: Long,mtg_no: Int){
        return generateMeetingRepository!!.deleteMemberLoan(cbo_id,mtg_no)
    }

    fun deleteMemberLoanByCboId(cbo_id: Long){
        return generateMeetingRepository!!.deleteMemberLoanByCboId(cbo_id)
    }

    fun deleteShgLoanApplication(mtg_no: Int,cbo_id: Long){
        generateMeetingRepository!!.deleteShgLoanApplication(mtg_no,cbo_id)
    }

    fun deleteShgLoanApplicationByCboId(cbo_id: Long){
        generateMeetingRepository!!.deleteShgLoanApplicationByCboId(cbo_id)
    }

    fun deleteMemberRepaidData(mtgdate:Long,mtgno:Int,cbo_id:Long){
        generateMeetingRepository!!.deleteMemberRepaidData(mtgdate,mtgno,cbo_id)
    }

    fun deleteMemberSubinstallmentData(mtgdate:Long,mtgno:Int,cbo_id:Long){
        generateMeetingRepository!!.deleteMemberSubinstallmentData(mtgdate,mtgno,cbo_id)
    }

    fun deleteMemberScheduleByMtgGuid(mtgGuid: String){
        generateMeetingRepository!!.deleteMemberScheduleByMtgGuid(mtgGuid)
    }

    fun deleteMemberSubinstallmentDataByCboId(cbo_id:Long){
        generateMeetingRepository!!.deleteMemberSubinstallmentDataByCboId(cbo_id)
    }

    fun deleteGroupRepaidData(mtgdate:Long,mtgno:Int,cbo_id:Long){
        generateMeetingRepository!!.deleteGroupRepaidData(mtgdate,mtgno,cbo_id)
    }

    fun deleteGroupSubinstallmentData(mtgdate:Long,mtgno:Int,cbo_id:Long){
        generateMeetingRepository!!.deleteGroupSubinstallmentData(mtgdate,mtgno,cbo_id)
    }

    fun deleteGroupSubinstallmentDataByCboId(cbo_id:Long){
        generateMeetingRepository!!.deleteGroupSubinstallmentDataByCboId(cbo_id)
    }

    fun deleteGroupScheduleByMtgGuid(mtgGuid:String){
        generateMeetingRepository!!.deleteGroupScheduleByMtgGuid(mtgGuid)
    }

    fun updateShg_mtgIsEdited(mtg_no: Int,cbo_id: Long){
        generateMeetingRepository!!.updateShg_mtgIsEdited(mtg_no,cbo_id)
    }

    fun updateMtgApprova_Rejection_Status(
        mtgno: Int,
        cbo_id: Long,
        updatedOn:Long,
        updatedBy:String,
        approvalStatus:Int,
        checkerRemarks:String

    ){
        generateMeetingRepository!!.updateMtgApprova_Rejection_Status(
            mtgno,
            cbo_id,
            updatedOn,
            updatedBy,
            approvalStatus,
            checkerRemarks)
    }


    fun reurnmemberpost(memid: Long?):Int{
        return generateMeetingRepository!!.reurnmemberpost(memid)
    }

    fun getClosedMemberLoanByMemberId(mtgno: Int, cbo_id: Long,completionFlag:Boolean,memberid: Long): List<LoanListModel>{
        return generateMeetingRepository!!.getClosedMemberLoanByMemberId(mtgno, cbo_id,completionFlag,memberid)
    }

    fun getCutOffClosedMemberLoanDataByCboId(mtgno: Int, cbo_id: Long,completionFlag:Boolean): List<LoanListModel>{
        return generateMeetingRepository!!.getCutOffClosedMemberLoanDataByCboId(mtgno, cbo_id,completionFlag)
    }

    fun getMonthlyCompulsarySaving(shg_id: Long?):Int{
        return generateMeetingRepository!!.getMonthlyCompulsarySaving(shg_id)
    }

    fun getTotalIncoming_OutgoingAmount(cbo_id: Long,mtg_no: Int,type:String):Int{
        return generateMeetingRepository!!.getTotalIncoming_OutgoingAmount(cbo_id,mtg_no,type)
    }

    fun getMemberReceiptAmount(cbo_id: Long,mtg_no: Int,type:String): Int{
        return generateMeetingRepository!!.getMemberReceiptAmount(cbo_id,mtg_no,type)
    }

    fun getTotalPrincipalDemand(mtg_no: Int,cbo_id: Long):Int{
        return generateMeetingRepository!!.getTotalPrincipalDemand(mtg_no,cbo_id)
    }

    fun getTotalPrincipalDemandOb(mtg_no: Int,cbo_id: Long):Int{
        return generateMeetingRepository!!.getTotalPrincipalDemandOb(mtg_no,cbo_id)
    }

    fun getTotalInterestAccured(mtg_no: Int,cbo_id: Long):Int{
        return generateMeetingRepository!!.getTotalInterestAccured(mtg_no,cbo_id)
    }

    fun getTotalInterestAccuredOb(mtg_no: Int,cbo_id: Long):Int{
        return generateMeetingRepository!!.getTotalInterestAccuredOb(mtg_no,cbo_id)
    }

    internal fun getBankCount(mtgno:Int,cbo_id:Long): Int{
        return generateMeetingRepository!!.getBankCount(mtgno,cbo_id)
    }

    fun getTotalGrpRepaymentDemand(cboid: Long, mtgno: Int): Int{
        return generateMeetingRepository!!.getTotalGrpRepaymentDemand(cboid, mtgno)
    }

    fun getTotalGrpLoansRepaid(cboid: Long, mtgno: Int):Int{
        return generateMeetingRepository!!.getTotalGrpLoansRepaid(cboid, mtgno)
    }

    fun getTotalGrpLoans(cboid: Long, mtgno: Int):Int{
        return generateMeetingRepository!!.getTotalGrpLoans(cboid, mtgno)
    }

    fun updatewithdrawl(
        withdrawl: Int,
        mtgno: Int,
        cbo_id: Long,
        memid: Long,
        updated_by:String,
        updated_on:Long
    ) {
        generateMeetingRepository!!.updatewithdrawl(
            withdrawl,
            mtgno,
            cbo_id,
            memid,
            updated_by,
            updated_on
        )
    }
    fun updatesavingwithdrawl(
        withdrawl: Int,
        mtgno: Int,
        cbo_id: Long,
        memid: Long,
        updated_by:String,
        updated_on:Long
    ) {
        generateMeetingRepository!!.updatesavingwithdrawl(
            withdrawl,
            mtgno,
            cbo_id,
            memid,
            updated_by,
            updated_on
        )
    }
    fun updateadjustwithdrawl(
        withdrawl: Int,
        mtgno: Int,
        cbo_id: Long,
        memid: Long,
        updated_by:String,
        updated_on:Long,
        refmtgno:Int
    ) {
        generateMeetingRepository!!.updateadjustwithdrawl(
            withdrawl,
            mtgno,
            cbo_id,
            memid,
            updated_by,
            updated_on,refmtgno
        )
    }
    fun updateadjustVoluntory(
        withdrawl: Int,
        mtgno: Int,
        cbo_id: Long,
        memid: Long,
        updated_by:String,
        updated_on:Long,
        refmtgno:Int
    ) {
        generateMeetingRepository!!.updateadjustVoluntory(
            withdrawl,
            mtgno,
            cbo_id,
            memid,
            updated_by,
            updated_on,refmtgno
        )
    }
    fun updateSettlementstatus(memid: Long,settlement: Int)
    {
        generateMeetingRepository!!.updateSettlementstatus(
            memid,settlement)
    }

    internal fun getListDatawithoutmember(mtgno: Int, cbo_id: Long, memid: Long): List<DtmtgDetEntity> {
        return generateMeetingRepository!!.getListDatawithoutmember(mtgno, cbo_id,memid)
    }
    internal fun getListinactivemember(mtgno: Int, cbo_id: Long): List<DtmtgDetEntity> {
        return generateMeetingRepository!!.getListinactivemember(mtgno, cbo_id)
    }

    internal  fun updateMeetinguploadStatus(
        shg_id: Long?,
        mtg_no: Int,
        transactionid: String,
        lastupdatedate: Long,
        checker_remarks:String
    ){
        generateMeetingRepository!!.updateMeetinguploadStatus(
            shg_id,
            mtg_no,
            transactionid,
            lastupdatedate,
            checker_remarks
        )
    }

    internal fun updatedTransactionRemarks(checker_remarks:String,transaction_id:String){
        generateMeetingRepository!!.updatedTransactionRemarks(checker_remarks,transaction_id)
    }

    internal  fun getMeetingStatusRemarks(cbo_id: Long):List<DtmtgEntity>{
        return  generateMeetingRepository!!.getMeetingStatusRemarks(cbo_id)
    }

    internal fun getCashInTransit(cbo_id: Long,mtgno: Int):Int{
        return generateMeetingRepository!!.getCashInTransit(cbo_id,mtgno)
    }

    internal fun updateCashInTransit(cashInTransit:Int,cbo_id: Long,mtgno: Int){
        generateMeetingRepository!!.updateCashInTransit(cashInTransit,cbo_id,mtgno)
    }

    internal fun getBank_CashInTransit(mtgno:Int,cbo_id:Long,bank_code:String):Int{
        return generateMeetingRepository!!.getBank_CashInTransit(mtgno,cbo_id,bank_code)
    }

    internal fun updateBank_CashInTransit(amount:Int,mtgno:Int,cbo_id:Long,bank_code:String){
        generateMeetingRepository!!.updateBank_CashInTransit(amount,mtgno,cbo_id,bank_code)
    }

    internal fun getMemberFinancialTxnData(mtg_no: Int,cbo_id: Long) : List<FinancialTransactionsMemEntity>{
        return generateMeetingRepository!!.getMemberFinancialTxnData(mtg_no,cbo_id)
    }

    internal fun getRescheduleloanListMtgByMtgnum(mtgno: Int, cbo_id: Long): List<LoanListModel>{
        return generateMeetingRepository!!.getRescheduleloanListMtgByMtgnum(mtgno, cbo_id)
    }

    internal fun getMemberAdjustmentAmount(mtgno:Int,memid:Long,auid:Int):Int?{
        return generateMeetingRepository!!.getMemberAdjustmentAmount(mtgno,memid,auid)
    }
    internal fun getTotalFDAmount( mtg_no: Int,cbo_id: Long,auid:Int,type:String): Int{
        return generateMeetingRepository!!.getTotalFDAmount( mtg_no,cbo_id,auid,type)
    }

    internal  fun getNonApprovedShgsCount(shgId:Long):Int{
        return  generateMeetingRepository!!.getNonApprovedShgsCount(shgId)
    }

    internal  fun getNonSynchedShgsCount(shgId:Long):Int{
        return  generateMeetingRepository!!.getNonSynchedShgsCount(shgId)
    }

    fun getMeetingStatus(cboid: Long,mtgno: Int):Int{
        return generateMeetingRepository!!.getMeetingStatus(cboid, mtgno)
    }

    fun updatedTransactionstatus(transaction_status:String,transaction_id:String){
        return generateMeetingRepository!!.updatedTransactionstatus(transaction_status, transaction_id)
    }
}