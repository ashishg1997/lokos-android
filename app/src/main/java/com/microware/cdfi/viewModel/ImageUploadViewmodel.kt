package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.entity.ImageuploadEntity
import com.microware.cdfi.repository.CboBankRepository
import com.microware.cdfi.repository.ImageUploadRepository

class ImageUploadViewmodel: AndroidViewModel {

    var imageUploadRepository: ImageUploadRepository? = null

    constructor(application: Application):super(application){
        imageUploadRepository = ImageUploadRepository(application)
    }

    internal fun getimage(): List<ImageuploadEntity>? {
        return imageUploadRepository!!.getimage()

    }

    internal fun getimage_by_guid(shg_guid:String): List<ImageuploadEntity>?{
        return imageUploadRepository!!.getimage_by_guid(shg_guid)
    }

    fun insert(imageuploadEntity: ImageuploadEntity){
        return imageUploadRepository!!.insert(imageuploadEntity)
    }




}