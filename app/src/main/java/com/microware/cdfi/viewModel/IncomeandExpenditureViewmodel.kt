package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.api.meetingmodel.BankTransactionModel
import com.microware.cdfi.api.meetinguploadmodel.ShgFinanceTransactionDetailGroupListData
import com.microware.cdfi.entity.MstCOAEntity
import com.microware.cdfi.entity.ShgFinancialTxnDetailEntity
import com.microware.cdfi.repository.IncomeandExpenditureRepository

class IncomeandExpenditureViewmodel : AndroidViewModel {

    var incomeandExpenditureRepository: IncomeandExpenditureRepository? = null

    constructor(application: Application) : super(application) {
        incomeandExpenditureRepository = IncomeandExpenditureRepository(application)
    }

    fun getIncomeAndExpendituredata(mtg_guid: String, cbo_id: Long, mtg_no: Int,auid:Int,amount_to_from:Int): List<ShgFinancialTxnDetailEntity>? {
        return incomeandExpenditureRepository!!.getIncomeAndExpendituredata(mtg_guid, cbo_id, mtg_no, auid,amount_to_from)
    }


    fun insert(shgFinancialTxnDetailEntity: ShgFinancialTxnDetailEntity) {
        return incomeandExpenditureRepository!!.insert(shgFinancialTxnDetailEntity)
    }

    fun deleteRecord(mtg_guid: String) {
        incomeandExpenditureRepository!!.deleteRecord(mtg_guid)
    }

    fun updateIncomeAndExpenditure(
        mtg_guid: String,
        cbo_id: Long,
        mtg_no: Int,
        auid: Int,
        fund_type: Int?,
        amount_to_from: Int?,
        type: String?,
        amount: Int?,
        trans_date: Long?,
        date_realisation: Long?,
        mode_payment: Int?,
        bank_code: String?,
        transaction_no: String?,
        updatedby: String?,
        updatedon: Long?
    ) {
        incomeandExpenditureRepository!!.updateIncomeAndExpenditure(
            mtg_guid,
            cbo_id,
            mtg_no,
            auid,
            fund_type,
            amount_to_from,
            type,
            amount,
            trans_date,
            date_realisation,
            mode_payment,
            bank_code,
            transaction_no,
            updatedby,
            updatedon
        )
    }

    fun getIncomeAndExpenditureAlldata(
        mtg_guid: String,
        cbo_id: Long,
        mtg_no: Int,
        fund_type: Int
    ): LiveData<List<ShgFinancialTxnDetailEntity>?> {
        return incomeandExpenditureRepository!!.getIncomeAndExpenditureAlldata(
            mtg_guid,
            cbo_id,
            mtg_no,
            fund_type
        )
    }
    fun getIncomeAndExpenditurenotinauid(auid:List<Int>,mtg_guid: String, cbo_id: Long, mtg_no: Int,fund_type:Int): LiveData<List<ShgFinancialTxnDetailEntity>?>{
        return incomeandExpenditureRepository!!.getIncomeAndExpenditurenotinauid(auid,mtg_guid, cbo_id, mtg_no, fund_type)
    }


    fun getIncomeAndExpenditureAmount(cbo_id: Long,mtg_no: Int,modePayment:Int,fund_type: Int):Int{
        return incomeandExpenditureRepository!!.getIncomeAndExpenditureAmount(cbo_id, mtg_no, modePayment, fund_type)
    }
    fun getMemberIncomeAndExpenditureAmountnotinauid(auid:List<Int>,cbo_id: Long,mtg_no: Int,fund_type: Int):Int{
        return incomeandExpenditureRepository!!.getMemberIncomeAndExpenditureAmountnotinauid(auid,cbo_id, mtg_no,  fund_type)
    }


    fun getMemberIncomeAndExpenditureAmount(cbo_id: Long,mtg_no: Int,fund_type: Int):Int{
        return incomeandExpenditureRepository!!.getMemberIncomeAndExpenditureAmount(cbo_id, mtg_no, fund_type)
    }

    fun getBankIncomeAndExpenditureAmount(cbo_id: Long,mtg_no: Int,fund_type: Int):Int{
        return incomeandExpenditureRepository!!.getBankIncomeAndExpenditureAmount(cbo_id, mtg_no, fund_type)
    }

    fun getBankCodeIncomeAndExpenditureAmount(cbo_id: Long,mtg_no: Int,fund_type: Int,bankCode:String):Int{
        return incomeandExpenditureRepository!!.getBankCodeIncomeAndExpenditureAmount(cbo_id, mtg_no, fund_type,bankCode)
    }

    fun getShgTransactionsGrpAmount(cbo_id: Long,mtg_no: Int,bank_code:String,fund_type: Int): Int{
        return incomeandExpenditureRepository!!.getShgTransactionsGrpAmount(cbo_id, mtg_no, bank_code,fund_type)
    }

    internal fun getCoaSubHeadData(uid:List<Int>,type:String,languageCode:String): List<MstCOAEntity>?{
        return incomeandExpenditureRepository!!.getCoaSubHeadData(uid,type,languageCode)
    }

    internal  fun getInvestmentdata(mtg_guid: String, cbo_id: Long, mtg_no: Int,auid:Int,type:String,amount_to_from: Int): List<ShgFinancialTxnDetailEntity>?{
        return incomeandExpenditureRepository!!.getInvestmentdata(mtg_guid, cbo_id, mtg_no,auid,type,amount_to_from)
    }

    fun getInvestmentListByMtgNum(cbo_id: Long, mtg_no: Int,auid:List<Int>,type:String): LiveData<List<ShgFinancialTxnDetailEntity>?>{
        return incomeandExpenditureRepository!!.getInvestmentListByMtgNum(cbo_id, mtg_no,auid,type)
    }

    fun getUploadListData(mtg_no:Int,cbo_id: Long) : List<ShgFinancialTxnDetailEntity>{
        return incomeandExpenditureRepository!!.getUploadListData(mtg_no,cbo_id)
    }

    fun getAdjustmentCashCount(mtg_no:Int,cbo_id:Long):Int{
        return incomeandExpenditureRepository!!.getAdjustmentCashCount(mtg_no,cbo_id)
    }

    fun updateCashAdjustment(
        cbo_id: Long,
        mtg_no:Int,
        auid: Int,
        fund_type: Int?,
        amount_to_from: Int?,
        type: String?,
        amount: Int?,
        trans_date: Long?,
        date_realisation: Long?,
        mode_payment: Int?,
        bank_code: String?,
        transaction_no: String?,
        updatedby: String?,
        updatedon: Long?
    ) {
        incomeandExpenditureRepository!!.updateCashAdjustment(
            cbo_id,
            mtg_no,
            auid,
            fund_type,
            amount_to_from,
            type,
            amount,
            trans_date,
            date_realisation,
            mode_payment,
            bank_code,
            transaction_no,
            updatedby,
            updatedon
        )
    }

    fun getSingleTransferedBankList(
        mtg_guid: String,
        cbo_id: Long,
        mtg_no: Int,
        auid: Int
    ): List<ShgFinancialTxnDetailEntity>? {
        return incomeandExpenditureRepository!!.getSingleTransferedBankList(
            mtg_guid, cbo_id, mtg_no, auid
        )
    }

    fun getAllTransferedBankList(
        cbo_id: Long,
        mtg_no: Int,
        fund_type: Int
    ): LiveData<List<ShgFinancialTxnDetailEntity>?> {
        return incomeandExpenditureRepository!!.getAllTransferedBankList(
            cbo_id,
            mtg_no,
            fund_type
        )
    }

    fun getBankCodeIncomeAndExpendituretransferAmount(cbo_id: Long,mtg_no: Int,bank_code: String?,auid:List<Int>,type:String):Int{
        return incomeandExpenditureRepository!!.getBankCodeIncomeAndExpendituretransferAmount(cbo_id,mtg_no,bank_code,auid,type)
    }

    fun getBankTransferList(cbo_id: Long, mtg_no: Int, fund_type: Int): LiveData<List<BankTransactionModel>?>{
        return incomeandExpenditureRepository!!.getBankTransferList(cbo_id,mtg_no,fund_type)
    }
    fun getInvestmentAmount(cbo_id: Long,mtg_no: Int,type:String,auid:List<Int>): Int{
        return incomeandExpenditureRepository!!.getInvestmentAmount(cbo_id, mtg_no, type, auid)
    }

}