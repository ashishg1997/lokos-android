package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.microware.cdfi.api.meetingmodel.FundTypeModel
import com.microware.cdfi.entity.MstproductEntity
import com.microware.cdfi.repository.LoanProductRepository

class LoanProductViewModel:AndroidViewModel {

    var loanProductRepository:LoanProductRepository? = null

    constructor(application: Application):super(application){
        loanProductRepository = LoanProductRepository(application)
    }

    fun getFundTypeBySource_Receipt(source:Int?,recipent:Int?): List<FundTypeModel>? {
        return loanProductRepository!!.getFundTypeBySource_Receipt(source,recipent)
    }
    fun getFundTypeBySource_Receiptwithtypeid(source:Int?,recipent:Int?,typeId: Int?): List<FundTypeModel>? {
        return loanProductRepository!!.getFundTypeBySource_Receiptwithtypeid(source,recipent,typeId)
    }
    fun getFundType(source:Int?,recipent:Int?,typeId:Int?): String{
        return loanProductRepository!!.getFundType(source, recipent, typeId)
    }


    fun getFundTypeBySource(source:Int?,recipent:Int?): List<FundTypeModel>?{
        return loanProductRepository!!.getFundTypeBySource(source, recipent)
    }
}