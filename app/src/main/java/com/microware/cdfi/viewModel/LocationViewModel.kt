package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.entity.*
import com.microware.cdfi.repository.LocationRepository

class LocationViewModel : AndroidViewModel {

    var mRepository: LocationRepository? = null

    constructor(application: Application) : super(application) {
        mRepository = LocationRepository(application)
    }

    fun getState(): LiveData<List<StateEntity>> {
        return mRepository!!.getState()
    }

    fun getStateByStateCode(): List<StateEntity> {
        return mRepository!!.getStateByStateCode()
    }

    fun getStateByStateCode(stateCode: Int): List<StateEntity> {
        return mRepository!!.getStateByStateCode(stateCode)
    }

    fun getDistrictByStateCode(stateCode: Int): LiveData<List<DistrictEntity>> {
        return mRepository!!.getDistrictByStateCode(stateCode)
    }

    fun getDistrictData(stateCode: Int): List<DistrictEntity> {
        return mRepository!!.getDistrictData(stateCode)
    }

    fun getBlock_district_code(
        stateCode: Int,
        districtCode: Int
    ): LiveData<List<BlockEntity>> {
        return mRepository!!.getBlock_district_code(stateCode, districtCode)
    }

    fun getBlock_data(stateCode: Int, districtCode: Int): List<BlockEntity> {
        return mRepository!!.getBlock_data(stateCode, districtCode)
    }

    fun getPanchayatdataByPanchayatCode(
        stateCode: Int,
        district_code: Int,
        block_code: Int
    ): LiveData<List<PanchayatEntity>> {
        return mRepository!!.getPanchayatdataByPanchayatCode(stateCode, district_code, block_code)
    }

    fun getPanchayatByPanchayatCode(
        stateCode: Int,
        district_code: Int,
        block_code: Int
    ): List<PanchayatEntity> {
        return mRepository!!.getPanchayatByPanchayatCode(stateCode, district_code, block_code)
    }

    fun getVillage_by_panchayatcode(
        stateCode: Int,
        district_code: Int,
        block_code: Int,
        panchayat_code: Int
    ): LiveData<List<VillageEntity>> {
        return mRepository!!.getVillage_by_panchayatcode(
            stateCode,
            district_code,
            block_code,
            panchayat_code
        )
    }

    fun getVillagedata_by_panchayatCode(
        stateCode: Int,
        district_code: Int,
        block_code: Int,
        panchayat_code: Int
    ): List<VillageEntity> {
        return mRepository!!.getVillagedata_by_panchayatCode(
            stateCode,
            district_code,
            block_code,
            panchayat_code
        )
    }

    internal fun getVillageName(village_id: Int): String? {
        return mRepository?.getVillageName(village_id)
    }

    internal fun getPanchayatName(panchayat_id: Int): String? {
        return mRepository?.getPanchayatName(panchayat_id)
    }


}