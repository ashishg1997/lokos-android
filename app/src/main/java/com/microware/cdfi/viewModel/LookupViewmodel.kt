package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.microware.cdfi.entity.LookupEntity
import com.microware.cdfi.entity.subcommitee_masterEntity
import com.microware.cdfi.repository.LookupRepository

class LookupViewmodel : AndroidViewModel {
    var mRepository: LookupRepository? = null

    constructor(application: Application) : super(application) {
        mRepository = LookupRepository(application)
    }

    internal fun getlookup(flag: Int?,languageID:String?):List<LookupEntity>? {
       return mRepository?.getlookup(flag,languageID)
    }

    internal fun getlookupbycode_data(key1:Int?,languageID:String?):List<LookupEntity>? {
        return mRepository?.getlookupbycode_data(key1,languageID)
    }

    internal fun getlookupfromkeycode(flag: String?,languageID:String?):List<LookupEntity>? {
       return mRepository?.getlookupfromkeycode(flag,languageID)
    }

    internal fun getAddressType(kyc_code: String, addressType: Int?): String? {
        return mRepository?.getAddressType(kyc_code,addressType)
    }

    internal fun getlookupValue(flag:Int?,languageID:String?,keycode:Int?): String? {
        return mRepository?.getlookupValue(flag,languageID,keycode)
    }

    fun insert(lookupEntity: LookupEntity){
        return mRepository!!.insert(lookupEntity)
    }

    internal  fun getlookupMasterdata(key1:Int?,languageID:String?,code:List<Int>):List<LookupEntity>?{
        return mRepository!!.getlookupMasterdata(key1,languageID,code)
    }

    internal fun getlookupnotin(flag:Int?,notincode:Int,languageID:String?): List<LookupEntity>? {
        return mRepository!!.getlookupnotin(flag,notincode,languageID)
    }
    internal fun getDestination(key1: Int, language_id: String?, lookup_code: Int?): String? {
        return mRepository!!.getDestination(key1, language_id, lookup_code)
    }
    fun getplaceOfInvestment(key1:Int?,languageID:String?):List<LookupEntity>?{
        return mRepository!!.getplaceOfInvestment(key1, languageID)
    }
}