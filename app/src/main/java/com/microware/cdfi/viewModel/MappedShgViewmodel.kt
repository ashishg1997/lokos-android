package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.microware.cdfi.entity.MappedShgEntity
import com.microware.cdfi.repository.FedrationRepository
import com.microware.cdfi.repository.MappedSHGRepository

class MappedShgViewmodel : AndroidViewModel {

    var mappedSHGRepository: MappedSHGRepository? = null

    constructor(application: Application):super(application){
        mappedSHGRepository = MappedSHGRepository(application)
    }

    internal fun getShgNameList(vo_guid:String):List<MappedShgEntity>?{
        return mappedSHGRepository!!.getShgNameList(vo_guid)
    }
   internal fun getmeetingShgList(vo_guid:String):List<MappedShgEntity>?{
        return mappedSHGRepository!!.getmeetingShgList(vo_guid)
    }
    internal fun getShgName(shg_id: Long, vo_guid: String): String? {
        return mappedSHGRepository!!.getShgName(shg_id, vo_guid)
    }
    fun updateSettlementstatus(memid: Long,settlement: Int) {
        return mappedSHGRepository!!.updateSettlementstatus(
            memid,settlement)
    }
}