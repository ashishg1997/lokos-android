package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.microware.cdfi.entity.MappedVoEntity
import com.microware.cdfi.repository.MappedVoRepository

class MappedVoViewmodel : AndroidViewModel {

    var mappedVoRepository: MappedVoRepository? = null

    constructor(application: Application):super(application){
        mappedVoRepository = MappedVoRepository(application)
    }

    internal fun getVoNameList(clf_guid:String):List<MappedVoEntity>?{
        return mappedVoRepository!!.getVoNameList(clf_guid)
    }
}