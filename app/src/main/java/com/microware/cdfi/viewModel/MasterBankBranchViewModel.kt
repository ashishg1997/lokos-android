package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.microware.cdfi.entity.Bank_branchEntity
import com.microware.cdfi.repository.MasterBankBranchRepository

class MasterBankBranchViewModel: AndroidViewModel{

    var mRepository:  MasterBankBranchRepository? = null

    constructor(application: Application):super(application){
        mRepository = MasterBankBranchRepository(application)
    }

    fun BankBranchlistifsc(ifsccode:String): List<Bank_branchEntity>?{
        return mRepository!!.BankBranchlistifsc(ifsccode)
    }
    fun getBankMaster(bankCode:Int?): List<Bank_branchEntity>?{
        return mRepository!!.getBankBranch(bankCode)
    }
    fun getBranchname(bank_branch_code:Int): String{
        return mRepository!!.getBranchname(bank_branch_code)
    }


}