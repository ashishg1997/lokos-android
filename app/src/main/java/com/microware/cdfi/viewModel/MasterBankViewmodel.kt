package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.microware.cdfi.api.meetingmodel.BankListModel
import com.microware.cdfi.application.CDFIApplication
import com.microware.cdfi.entity.BankEntity
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.repository.MasterBankRepository

class MasterBankViewmodel : AndroidViewModel {

    var mRepository: MasterBankRepository? = null

    constructor(application: Application) : super(application) {
        mRepository = MasterBankRepository(application)
    }

    fun getBankMaster(): List<BankEntity>? {
        return mRepository!!.getBankMaster()
    }

    fun getBankwithcode(bankcode: String): List<BankEntity>? {
        return mRepository!!.getBankwithcode(bankcode)
    }

    internal fun getBankName(bank_id: Int): String? {
        return mRepository!!.getBankName(bank_id)
    }

    internal fun getAccountLength(bank_id: Int): String?{
        return mRepository!!.getAccountLength(bank_id)
    }
    fun getBankMaster1(cboGuid:String): List<BankListModel>? {
        return mRepository!!.getBankMaster1(cboGuid)
    }
}