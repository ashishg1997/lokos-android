package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.microware.cdfi.entity.DtMemSettlementEntity
import com.microware.cdfi.repository.MemSettlementRepository

class MemSettlementViewmodel: AndroidViewModel {

    var memSettlementRepository: MemSettlementRepository? = null

    constructor(application: Application):super(application){
        memSettlementRepository = MemSettlementRepository(application)
    }

    internal  fun getSettlementdata(memid: Long?,mtgno:Int): List<DtMemSettlementEntity>?{
        return memSettlementRepository!!.getSettlementdata(memid,mtgno)
    }


    fun insert(dtMemSettlementEntity: DtMemSettlementEntity){
         memSettlementRepository!!.insertSettlementData(dtMemSettlementEntity)
    }

    fun getTotalSettelMentAmt(mtgno:Int,cbo_id:Long):Int{
        return memSettlementRepository!!.getTotalSettelMentAmt(mtgno, cbo_id)
    }
}