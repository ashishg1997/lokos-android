package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.entity.member_addressEntity
import com.microware.cdfi.repository.MemberAddressRepository

class MemberAddressViewmodel : AndroidViewModel {

    var addressRepository: MemberAddressRepository? = null

    constructor(application: Application) : super(application) {
        addressRepository = MemberAddressRepository(application)
    }

    internal fun getAddressdetaildata(shg_guid: String?): List<member_addressEntity>? {
        return addressRepository!!.getAddressdetaildata(shg_guid)
    }

    internal fun getAddressdata(shg_guid: String?): LiveData<List<member_addressEntity>>? {
        return addressRepository!!.getAddressdata(shg_guid)
    }

   internal fun ggetAddressdatalist(member_guid: String?): List<member_addressEntity>?
    {
        return addressRepository!!.getAddressdatalist(member_guid)
    }
    internal fun getAddressdatalistcount(member_guid: String?): List<member_addressEntity>?
    {
        return addressRepository!!.getAddressdatalistcount(member_guid)
    }

    fun insert(addressEntity: member_addressEntity) {
        return addressRepository!!.insert(addressEntity)
    }

    fun updateAddressDetail(
        address_guid: String,
        address_type: Int,
        address_line1: String,
        address_line2: String,
        village: Int,
        panchayat_id: Int,
        landmark: String,
        state: Int,
        District: Int,
        postal_code: String,
        is_Active: Int,
        updated_date: Long,
        updated_by: String,
        addresslocation: Int,
        isCompleted: Int
    ) {
        addressRepository!!.updateAddressDetail(
            address_guid,
            address_type,
            address_line1,
            address_line2,
            village,
            panchayat_id,landmark,state,District,postal_code,is_Active,updated_date,updated_by,addresslocation,isCompleted
        )
    }
    fun deleteData(address_guid: String?){
        addressRepository!!.deleteData(address_guid)
    }
    fun deleteRecord(address_guid: String?){
        addressRepository!!.deleteRecord(address_guid)
    }

    fun updateAddressdata(
        address_guid: String,
        address_line1: String,
        address_line2: String,
        village: Int,
        panchayat_id: Int,
        block_id: Int,
        state: Int,
        District: Int,
        postal_code: String,
        is_Active: Int,
        updated_date: Long,
        updated_by: String,
        addresslocation: Int
    ){
        addressRepository!!.updateAddressdata(
            address_guid,
            address_line1,
            address_line2,
            village,
            panchayat_id,
            block_id,
            state,
            District,
            postal_code,
            is_Active,
            updated_date,
            updated_by,
            addresslocation
        )
    }

    fun getAddressByEntrySource(member_guid: String?,entrySource:Int): String?{
        return addressRepository!!.getAddressByEntrySource(member_guid,entrySource)
    }

    fun getCompletionCount(member_guid: String?):Int{
        return addressRepository!!.getCompletionCount(member_guid)
    }

    fun getVerificationCount(member_guid: String?):Int{
        return addressRepository!!.getVerificationCount(member_guid)
    }

    fun updateIsVerifed(member_guid: String?,updated_date: Long,updated_by: String){
        addressRepository!!.updateIsVerifed(member_guid,updated_date,updated_by)
    }
}