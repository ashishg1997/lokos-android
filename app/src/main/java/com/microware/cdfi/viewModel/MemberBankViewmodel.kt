package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.room.Query
import com.microware.cdfi.entity.MemberBankAccountEntity
import com.microware.cdfi.repository.MemberBankRepository

class MemberBankViewmodel : AndroidViewModel {

    var bankRepository: MemberBankRepository? = null

    constructor(application: Application):super(application){
        bankRepository = MemberBankRepository(application)
    }


    fun getBankdetaildata(memberGUID: String?): LiveData<List<MemberBankAccountEntity>>? {
        return bankRepository!!.getBankdetaildata(memberGUID)
    }

  fun getBankdetaildatalist(memberGUID: String?): List<MemberBankAccountEntity>? {
        return bankRepository!!.getBankdetaildatalist(memberGUID)
    }

    fun getBankdetaildatalistcount(memberGUID: String?): List<MemberBankAccountEntity>? {
        return bankRepository!!.getBankdetaildatalistcount(memberGUID)
    }
fun getBankdetaildefaultcount(memberGUID: String?): Int {
        return bankRepository!!.getBankdetaildefaultcount(memberGUID)
    }

    fun getBankdata(memberGUID: String?): List<MemberBankAccountEntity>? {
        return bankRepository!!.getBankdata(memberGUID)
    }

    fun insert(bankEntity: MemberBankAccountEntity){
        return bankRepository!!.insert(bankEntity)
    }

    fun updateBankDetail(
        bank_guid: String,
        account_no: String,
        bank_id: String,
        bank_account: Int,
        account_type: String,
        branchID: String,
        account_open_date: String,
        is_default_account: Int,
        status: Int,
        closing_date: String,
        gl_code: String,
        sameAsGroup: Int,
        nameinbankpassbook: String,
        is_edited: Int,
        updated_date: Long,
        updated_by: String,
        imageName: String,
        isCompleted: Int,
        ifsc_code:String
    ) {
        bankRepository!!.updateBankDetail(
            bank_guid,
            account_no,
            bank_id,
            bank_account,
            account_type,branchID,account_open_date,is_default_account,
            status,closing_date,gl_code,sameAsGroup,nameinbankpassbook,is_edited,updated_date,updated_by,imageName,isCompleted,ifsc_code
        )
    }

    fun getbank_acc_count(account_no:String,memberguid:String):Int{
        return bankRepository!!.getbank_acc_count(account_no,memberguid)
    }

    fun deleteData(bank_guid: String?){
        bankRepository!!.deleteData(bank_guid)
    }

    fun deleteRecord(bank_guid: String?){
        bankRepository!!.deleteRecord(bank_guid)
    }

    fun updateisedit( memberguid: String,last_uploaded_date:Long){
        bankRepository!!.updateisedit( memberguid,last_uploaded_date)
    }

    fun getCompletionCount(member_guid: String?):Int{
        return bankRepository!!.getCompletionCount(member_guid)
    }

    fun getVerificationCount(member_guid: String?):Int{
        return bankRepository!!.getVerificationCount(member_guid)
    }

    fun updateIsVerifed(member_guid: String?,updated_date: Long,updated_by: String){
        bankRepository!!.updateIsVerifed(member_guid,updated_date,updated_by)
    }

    fun getbank_acc_count2(account_no:String):Int{
        return bankRepository!!.getbank_acc_count2(account_no)
    }

    fun updateactivotaionstatus(memberbank_guid: String?, status: Int){
        bankRepository!!.updateactivotaionstatus(memberbank_guid,status)
    }
}