package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.entity.MemberDesignationEntity
import com.microware.cdfi.repository.MemberDesignationRepository

class MemberDesignationViewmodel : AndroidViewModel {
    var memberDesignationRepository: MemberDesignationRepository? = null

    constructor(application: Application) : super(application) {
        memberDesignationRepository = MemberDesignationRepository(application)
    }

    fun getMemberDesignationdata(shgGUID: String, memberGUID: String): MemberDesignationEntity {
        return memberDesignationRepository!!.getMemberDesignationdata(shgGUID, memberGUID)
    }

    fun getMemberDesignationdetaillist(shgGuid: String): List<MemberDesignationEntity> {
        return memberDesignationRepository!!.getMemberDesignationdetaillist(shgGuid)
    }
 fun getMemberDesignationdetaillistcount(shgGuid: String?): List<MemberDesignationEntity> {
        return memberDesignationRepository!!.getMemberDesignationdetaillistcount(shgGuid)
    }

    fun insert(memberDesignationEntity: MemberDesignationEntity) {
        return memberDesignationRepository!!.insert(memberDesignationEntity)
    }

    fun getMemberDesignation(shgGUID: String): LiveData<List<MemberDesignationEntity>>? {
        return memberDesignationRepository!!.getMemberDesignation(shgGUID)
    }

    fun getMembercount(memguid: String): Int {
        return memberDesignationRepository!!.getMembercount(memguid)
    }
   fun getsignatoryMembercount(memguid: String): Int {
        return memberDesignationRepository!!.getsignatoryMembercount(memguid)
    }
    fun getactivesignatorycount(shgGUID: String): Int {
        return memberDesignationRepository!!.getactivesignatorycount(shgGUID)
    }

    fun getMemberDesignationdata(shgGUID: String, id: Int?): List<MemberDesignationEntity> {
        return memberDesignationRepository!!.getMemberDesignationdata(shgGUID,id)
    }
}