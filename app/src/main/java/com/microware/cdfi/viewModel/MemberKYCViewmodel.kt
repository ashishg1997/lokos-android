package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.entity.Member_KYC_Entity
import com.microware.cdfi.repository.MemberKYCRepository

class MemberKYCViewmodel : AndroidViewModel {

    var kycRepository: MemberKYCRepository? = null

    constructor(application: Application):super(application){
        kycRepository = MemberKYCRepository(application)
    }

    internal fun getKycdetaildata(memberGUID: String?): LiveData<List<Member_KYC_Entity>>?{
        return kycRepository!!.getKycdetaildata(memberGUID)
    }

   internal fun getKycdetaildatalist(memberGUID: String?): List<Member_KYC_Entity>?{
        return kycRepository!!.getKycdetaildatalist(memberGUID)
    }

    internal fun getKycdetaildatalistcount(memberGUID: String?): List<Member_KYC_Entity>?{
        return kycRepository!!.getKycdetaildatalistcount(memberGUID)
    }

    internal fun getKycdetail(kycGUID: String?): List<Member_KYC_Entity>?{
        return kycRepository!!.getKycdetail(kycGUID)
    }

   internal fun getKycwithkycno(kyc_number: String?): List<Member_KYC_Entity>?{
        return kycRepository!!.getKycwithkycno(kyc_number)
    }

    fun insert(kycentity: Member_KYC_Entity){
        return kycRepository!!.insert(kycentity)
    }

    fun updateKycDetail(
        kyc_guid: String,
        kyc_type: Int,
        kyc_id: String,
        kyc_front_doc_orig_name: String,
        kyc_front_doc_encp_name: String,
        kyc_rear_doc_orig_name: String,
        kyc_rear_doc_encp_name: String,
        device: Int,
        is_edited: Int,
        updated_date: Long,
        updated_by: String,
        isCompleted: Int
    ) {
        kycRepository!!.updateKycDetail(
            kyc_guid,
            kyc_type,
            kyc_id,
            kyc_front_doc_orig_name,
            kyc_front_doc_encp_name,kyc_rear_doc_orig_name,kyc_rear_doc_encp_name,device,is_edited,updated_date,updated_by,isCompleted
        )
    }

    fun deleteData(kyc_guid: String?){
        kycRepository!!.deleteData(kyc_guid)
    }
    fun deleteRecord(kyc_guid: String?){
        kycRepository!!.deleteRecord(kyc_guid)
    }
    fun getKycEntrySource(memberGUID: String?,kyc_type: Int): Int?{
        return kycRepository!!.getKycEntrySource(memberGUID,kyc_type)
    }

    fun getKycGuidbyKycType(memberGUID: String?,kyc_type: Int):String?{
        return kycRepository!!.getKycGuidbyKycType(memberGUID,kyc_type)
    }

    fun updateKycNumber(
        kyc_guid: String,
        kyc_id: String,
        is_edited: Int,
        entrySource: Int,
        updated_date: Long,
        updated_by: String
    ){
        kycRepository!!.updateKycNumber(
            kyc_guid,
            kyc_id,
            is_edited,
            entrySource,
            updated_date,
            updated_by
        )
    }

    fun getCompletionCount(member_guid: String?):Int{
        return kycRepository!!.getCompletionCount(member_guid)
    }

    fun getVerificationCount(member_guid: String?):Int{
        return kycRepository!!.getVerificationCount(member_guid)
    }

    fun updateIsVerifed(member_guid: String?){
        kycRepository!!.updateIsVerifed(member_guid)
    }

    fun getKycCount(kycType:Int,kycNumber:String,member_guid: String):Int{
        return kycRepository!!.getKycCount(kycType, kycNumber, member_guid)
    }

    fun updateactivotaionstatus(memberkyc_guid: String?, status: Int){
        kycRepository!!.updateactivotaionstatus(memberkyc_guid,status)
    }

}