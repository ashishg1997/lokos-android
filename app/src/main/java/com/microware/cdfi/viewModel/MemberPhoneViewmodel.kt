package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.entity.MemberPhoneDetailEntity
import com.microware.cdfi.repository.MemberPhoneRepository

class MemberPhoneViewmodel :AndroidViewModel {

    var PhoneDetailRepository: MemberPhoneRepository? = null

    constructor(application: Application):super(application){
        PhoneDetailRepository = MemberPhoneRepository(application)
    }

    internal fun getphonedetaildata(memberphoneGuid: String?): List<MemberPhoneDetailEntity>?{
        return PhoneDetailRepository!!.getphonedetaildata(memberphoneGuid)
    }
internal fun getphonedetaillistdata(memberphoneGuid: String?): List<MemberPhoneDetailEntity>?{
        return PhoneDetailRepository!!.getphonedetaillistdata(memberphoneGuid)
    }

    fun getphoneData(memeberGUID: String?): LiveData<List<MemberPhoneDetailEntity>>? {
        return PhoneDetailRepository!!.getphoneData(memeberGUID)
    }

 fun  getphoneDatalist(memberGUID: String?): List<MemberPhoneDetailEntity>? {
        return PhoneDetailRepository!!.getphoneDatalist(memberGUID)
    }

fun  getphoneDatalistcount(memberGUID: String?): List<MemberPhoneDetailEntity>? {
        return PhoneDetailRepository!!.getphoneDatalistcount(memberGUID)
    }


    fun insert(Phoneentity: MemberPhoneDetailEntity){
        return PhoneDetailRepository!!.insert(Phoneentity)
    }

    fun updatePhoneDetail(
        phone_guid: String,
        phone_no: String,
        isDefault: Int,
        phone_ownership: String,
        phone_ownership_detail: String,
        valid_from: Long,
        valid_till: Long,
        device: Int,
        is_edited: Int,
        updated_date: Long,
        updated_by: String,
        isCompleted: Int
    ) {
        PhoneDetailRepository!!.updatePhoneDetail(
            phone_guid,
            phone_no,
            isDefault,
            phone_ownership,
            phone_ownership_detail,valid_from,valid_till,device,is_edited,updated_date,updated_by,isCompleted
        )
    }

    fun deleteData(phone_guid: String?){
        PhoneDetailRepository!!.deleteData(phone_guid)
    }
    fun deleteRecord(phone_guid: String?){
        PhoneDetailRepository!!.deleteRecord(phone_guid)
    }

    fun getCompletionCount(member_guid: String?):Int{
        return PhoneDetailRepository!!.getCompletionCount(member_guid)
    }

    fun getVerificationCount(member_guid: String?):Int{
        return PhoneDetailRepository!!.getVerificationCount(member_guid)
    }

    fun updateIsVerifed(member_guid: String?,updated_date: Long,updated_by: String){
        PhoneDetailRepository!!.updateIsVerifed(member_guid,updated_date,updated_by)
    }

    fun getPhoneCount(phone_no:String,member_guid: String?):Int{
        return PhoneDetailRepository!!.getPhoneCount(phone_no,member_guid)
    }
}