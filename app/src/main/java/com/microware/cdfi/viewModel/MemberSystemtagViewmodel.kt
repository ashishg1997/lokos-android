package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.entity.MemberSystemTagEntity
import com.microware.cdfi.repository.MemberSystemtagRepository

class MemberSystemtagViewmodel: AndroidViewModel {

    var memberSystemtagRepository: MemberSystemtagRepository? = null

    constructor(application: Application):super(application){
        memberSystemtagRepository = MemberSystemtagRepository(application)
    }

    fun getSystemtagdata(cbuguid: String?): LiveData<List<MemberSystemTagEntity>>? {
        return memberSystemtagRepository!!.getSystemtagdata(cbuguid)
    }
    fun getSystemtagdatalist(member_guid: String?): List<MemberSystemTagEntity>? {
        return memberSystemtagRepository!!.getSystemtagdatalist(member_guid)
    }
   fun getSystemtagdatalistcount(member_guid: String?): List<MemberSystemTagEntity>? {
        return memberSystemtagRepository!!.getSystemtagdatalistcount(member_guid)
    }
    fun getSystemtag(systemguid: String?): List<MemberSystemTagEntity>? {
        return memberSystemtagRepository!!.getSystemtag(systemguid)
    }

    fun insert(systemTagEntity: MemberSystemTagEntity) {
        memberSystemtagRepository!!.insert(systemTagEntity)
    }

    fun updateSystemTagdata(system_type:Int,system_id:String,systemtag_guid:String,updated_date:Long,updated_by:String){
        memberSystemtagRepository!!.updateSystemTagdata(system_type,system_id,systemtag_guid,updated_date,updated_by)
    }

    fun deleteData(systemtag_guid: String?){
        memberSystemtagRepository!!.deleteData(systemtag_guid)
    }

    fun deleteRecord(systemtag_guid: String?){
        memberSystemtagRepository!!.deleteRecord(systemtag_guid)
    }

    internal fun getSystemtagcount(systemtagid: String): Int{
        return memberSystemtagRepository!!.getSystemtagcount(systemtagid)
    }
}