package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.api.model.QueuestatusModel
import com.microware.cdfi.entity.MemberEntity
import com.microware.cdfi.repository.MemberRepository

class Memberviewmodel : AndroidViewModel {

    var mRepository: MemberRepository? = null

    constructor(application: Application) : super(application) {
        mRepository = MemberRepository(application)
    }

    internal fun getAllMember(): LiveData<List<MemberEntity>>? {
        return mRepository?.getAllMember()
    }

    internal fun getmember(memberGUID : String?): List<MemberEntity>? {
        return mRepository?.getmember(memberGUID)
    }
  internal fun getMemberdetail(memberGUID : String?): List<MemberEntity>? {
        return mRepository?.getMemberdetail(memberGUID)
    }
    internal fun getAllMember(shg_guid: String?): LiveData<List<MemberEntity>>? {
        return mRepository?.getAllMember(shg_guid)
    }
    internal fun getActiveMember(shg_guid: String?): LiveData<List<MemberEntity>>? {
        return mRepository?.getActiveMember(shg_guid)
    }
    internal fun getMemberuploadlist(shg_guid: String?): List<MemberEntity>? {
        return mRepository?.getMemberuploadlist(shg_guid)
    }
    internal fun getAllMemberlist(shg_guid: String?):List<MemberEntity>? {
        return mRepository?.getAllMemberlist(shg_guid)
    }

    internal fun getAllMemberDeslist(shg_guid: String?): List<MemberEntity>? {
        return mRepository!!.getAllMemberDeslist(shg_guid)
    }

    internal fun getcount(shg_guid: String?): Int {
        return mRepository?.getcount(shg_guid)!!
    }

  internal fun getseqno(shg_guid: String?): Int {
        return mRepository?.getseqno(shg_guid)!!
    }

    fun insert(memberEntity: MemberEntity) {
        mRepository!!.insert(memberEntity)
    }

   fun updateMemberDetail(
        member_guid: String,
        Seq_no: Int,
        member_name: String,
        member_name_local: String,
        Father_Husband: Int,
        Relation_Name: String,
        Relation_Name_Local: String,
        gender: Int,
        marital_status: Int,
        religion: Int,
        Social_Category: Int,
        TribalCategory: Int?,
        BPL: Int,
        BPL_Number: String,
        PIP_Category: Int,
        PIP_Date: Int,
        HighestEducationLevel: Int,
        DOB_Available: Int,
        DOB: Long,
        Age: Int,
        AgeAsOn: Long,
        Minority: Int,
        IsDisabled: Int,
        DisabilityDetails: String,
        WellbeingCategory: Int,
        PrimaryOccupation: Int,
        SecondaryOccupation:Int,
        TertiaryOccupation:Int,
        joining_date: Long,
        leaving_date: Long,
        ReasonForLeaving: Int,
        IfMinor_MemberReplaced: Int,
        GuardianName: String,
        GuardianName_Local: String,
        Guardian_Relation: Int,
        designation: Int,
        status: Int,
        house_hold_code: Int,
        head_house_hold: Int,
        Insurance: Int,
        marked_as_defaulter: Int,
        marked_as_defaulter_date: Int,
        RecordModified: String,
        updated_date: Long,
        updated_by: String,
        settlement_status: Int,
        imageName: String,
        isCompleted: Int
    ) {
        mRepository!!.updateMemberDetail(
            member_guid,
            Seq_no,
            member_name,
            member_name_local,
            Father_Husband,
            Relation_Name,
            Relation_Name_Local,
            gender,
            marital_status,
            religion,
            Social_Category,
            TribalCategory, BPL, BPL_Number, PIP_Category,PIP_Date,
            HighestEducationLevel,DOB_Available,DOB,Age,
            AgeAsOn,Minority,IsDisabled,DisabilityDetails,
            WellbeingCategory,PrimaryOccupation,SecondaryOccupation,
            TertiaryOccupation,joining_date,leaving_date,
            ReasonForLeaving,IfMinor_MemberReplaced,GuardianName,
            GuardianName_Local,Guardian_Relation
            ,designation,status,house_hold_code,head_house_hold,Insurance,marked_as_defaulter,
            marked_as_defaulter_date,RecordModified,updated_date,updated_by,settlement_status,imageName,isCompleted
        )
    }

    fun updateMemberDedupStatus(
        guid: String,
        code: String,
        activationStatus: Int,
        approve_status: Int,
        checker_remarks: String

    ){
        mRepository!!.updateMemberDedupStatus(
            guid,
            code,
            activationStatus,
            approve_status,
            checker_remarks

        )
    }

    fun updateMemberuploadStatus(
        guid: String, transactionid: String,
        lastupdatedate: Long)
    {
        mRepository!!.updateMemberuploadStatus(
            guid,transactionid,
            lastupdatedate)
    }
    fun getMemberlistdata(shgGUID: String?): LiveData<List<MemberEntity>>??{
        return mRepository!!.getMemberlistdata(shgGUID)
    }

    internal fun deleteMemberByMemberGuid(memberGUID: String?){
        mRepository!!.deleteMemberByMemberGuid(memberGUID)
    }

    internal fun deleteMemberAddressByMemberGuid(memberGUID: String?){
        mRepository!!.deleteMemberAddressByMemberGuid(memberGUID)
    }

    internal fun deleteMemberPhoneByMemberGuid(memberGUID: String?){
        mRepository!!.deleteMemberPhoneByMemberGuid(memberGUID)
    }

    internal fun deleteMemberBankByMemberGuid(memberGUID: String?){
        mRepository!!.deleteMemberBankByMemberGuid(memberGUID)
    }

    internal fun deleteMemberKycByMemberGuid(memberGUID: String?){
        mRepository!!.deleteMemberKycByMemberGuid(memberGUID)
    }

    internal fun deleteMemberSystemTagByMemberGuid(memberGUID: String?){
        mRepository!!.deleteMemberSystemTagByMemberGuid(memberGUID)
    }
    internal fun deleteMemberDataByMemberGuid(memberGUID: String?){
        mRepository!!.deleteMemberDataByMemberGuid(memberGUID)
    }

    internal fun deleteMemberAddressDataByMemberGuid(memberGUID: String?){
        mRepository!!.deleteMemberAddressDataByMemberGuid(memberGUID)
    }

    internal fun deleteMemberPhoneDataByMemberGuid(memberGUID: String?){
        mRepository!!.deleteMemberPhoneDataByMemberGuid(memberGUID)
    }

    internal fun deleteMemberBankDataByMemberGuid(memberGUID: String?){
        mRepository!!.deleteMemberBankDataByMemberGuid(memberGUID)
    }

    internal fun deleteMemberKycDataByMemberGuid(memberGUID: String?){
        mRepository!!.deleteMemberKycDataByMemberGuid(memberGUID)
    }

    internal fun deleteMemberSystemTagDataByMemberGuid(memberGUID: String?){
        mRepository!!.deleteMemberSystemTagDataByMemberGuid(memberGUID)
    }

    internal  fun getIsVerified(GUID:String?):Int{
        return  mRepository!!.getIsVerified(GUID)
    }

    internal  fun updateIsVerified(guid: String,is_verified:Int,updated_date: Long,updated_by: String){
        return   mRepository!!.updateIsVerified(guid,is_verified,updated_date,updated_by)
    }

    fun getCompletionCount(cbo_guid: String?):Int{
        return mRepository!!.getCompletionCount(cbo_guid)
    }

    fun getVerificationCount(cbo_guid: String?):Int{
        return mRepository!!.getVerificationCount(cbo_guid)
    }

    fun getFinalVerifyCount(member_guid: String?):Int{
        return mRepository!!.getFinalVerifyCount(member_guid)
    }

    fun updateIsVerifed(shgguid: String?,updated_date: Long,updated_by: String){
        mRepository!!.updateIsVerifed(shgguid,updated_date,updated_by)
    }

    internal fun getimage(member_id: Long?): String? {
        return mRepository?.getimage(member_id)
    }

    internal fun getOfficeBearerCount(shg_guid: String?,designationId:List<Int>): Int{
        return mRepository!!.getOfficeBearerCount(shg_guid,designationId)
    }

    fun getmemberList(shgGuid:String):List<QueuestatusModel>{
        return mRepository!!.getmemberList(shgGuid)
    }

    fun getMemberSummary(memberGUID: String?): LiveData<List<MemberEntity>>?{
        return mRepository!!.getMemberSummary(memberGUID)
    }
}