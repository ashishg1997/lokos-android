package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.entity.MstCOAEntity
import com.microware.cdfi.repository.MstCOARepository

class MstCOAViewmodel: AndroidViewModel {

    var mstCOARepository: MstCOARepository? = null

    constructor(application: Application):super(application){
        mstCOARepository = MstCOARepository(application)
    }

    internal fun getMstCOAdata(type: String,languageCode:String): List<MstCOAEntity>?{
        return mstCOARepository!!.getMstCOAdata(type,languageCode)
    }
    fun getMstCOAdatanotin(uid:List<Int>,type: String,languageCode:String): List<MstCOAEntity>?{
        return mstCOARepository!!.getMstCOAdatanotin(uid,type,languageCode)
    }
    internal fun getcoaValue(type:String?,languageID:String?,keycode:Int?): String? {
        return mstCOARepository?.getcoaValue(type,languageID,keycode)
    }

    fun insert(mstCOAEntity: MstCOAEntity){
        return mstCOARepository!!.insert(mstCOAEntity)
    }

    fun deleteRecord(uid: Int){
        mstCOARepository!!.deleteRecord(uid)
    }
     fun getCoaSubHeadlist(uid:List<Int>,languageCode:String): List<MstCOAEntity>?{
        return mstCOARepository!!.getCoaSubHeadlist(uid,languageCode)
    }
    internal fun getCoaSubHeadData(
        uid: List<Int>,
        type: String,
        languageCode: String
    ): List<MstCOAEntity>? {
        return mstCOARepository!!.getCoaSubHeadData(uid, type, languageCode)
    }
    fun getReceiptCoaSubHeadData(
        type: String,
        type1: String,
        languageCode: String
    ): List<MstCOAEntity>? {
        return mstCOARepository!!.getReceiptCoaSubHeadData(type, type1, languageCode)
    }
    fun getCoaSubAmount(uid:Int) : Int{
        return mstCOARepository!!.getCoaSubAmount(uid)
    }
    fun getCoaSubHeadname(auid:Int,languageCode:String): String{
        return mstCOARepository!!.getCoaSubHeadname(auid,languageCode)
    }

    fun getCoaSubAmountGroup(uid:Int,mtgNo:Int) : Int{
        return mstCOARepository!!.getCoaSubAmountGroup(uid, mtgNo)
    }

    internal fun getCOADesValue(languageID:String?,keycode:Int?): String?{
        return mstCOARepository!!.getCOADesValue( languageID, keycode)
    }
}