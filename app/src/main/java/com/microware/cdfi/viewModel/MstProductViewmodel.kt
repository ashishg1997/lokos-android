package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.microware.cdfi.entity.LoanProductEntity
import com.microware.cdfi.repository.MstProductRepository

class MstProductViewmodel : AndroidViewModel {

    var mstProductRepository: MstProductRepository? = null

    constructor(application: Application):super(application){
        mstProductRepository = MstProductRepository(application)
    }


    internal fun getproductlist(): List<LoanProductEntity>? {
        return mstProductRepository!!.getproductlist()
    }

}