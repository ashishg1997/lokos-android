package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.entity.MstVOCOAEntity
import com.microware.cdfi.repository.MstVOCOARepository

class MstVOCOAViewmodel: AndroidViewModel {

    var mstVOCOARepository: MstVOCOARepository? = null

    constructor(application: Application):super(application){
        mstVOCOARepository = MstVOCOARepository(application)
    }

    internal fun getMstCOAdata(type: String,languageCode:String): List<MstVOCOAEntity>?{
        return mstVOCOARepository!!.getMstCOAdata(type,languageCode)
    }

    internal fun getcoaValue(type:String?,languageID:String?,keycode:Int?): String? {
        return mstVOCOARepository?.getcoaValue(type,languageID,keycode)
    }

    fun insert(mstCOAEntity: MstVOCOAEntity){
        return mstVOCOARepository!!.insert(mstCOAEntity)
    }

    fun deleteRecord(uid: Int){
        mstVOCOARepository!!.deleteRecord(uid)
    }
     fun getCoaSubHeadlist(uid:List<Int>,languageCode:String): List<MstVOCOAEntity>?{
        return mstVOCOARepository!!.getCoaSubHeadlist(uid,languageCode)
    }
    internal fun getCoaSubHeadData(
        receipt:Int,
        type: List<String>,
        languageID: String?
    ): List<MstVOCOAEntity>? {
        return mstVOCOARepository!!.getCoaSubHeadData(receipt, type, languageID)
    }
    fun getReceiptCoaSubHeadData(
        type: String,
        type1: String,
        languageCode: String
    ): List<MstVOCOAEntity>? {
        return mstVOCOARepository!!.getReceiptCoaSubHeadData(type, type1, languageCode)
    }
    fun getCoaSubAmount(uid:Int) : Int{
        return mstVOCOARepository!!.getCoaSubAmount(uid)
    }

    fun getAllVoCoaSubHead(
        type: List<String>,
        languageCode: String
    ): LiveData<List<MstVOCOAEntity>?> {
        return mstVOCOARepository!!.getAllVoCoaSubHead(type, languageCode)
    }
fun getAlllist(
        type: List<String>,
        languageCode: String
    ): List<MstVOCOAEntity>? {
        return mstVOCOARepository!!.getAlllist(type, languageCode)
    }

    fun getCoaSubHeadname(auid:Int,languageCode:String): String{
        return mstVOCOARepository!!.getCoaSubHeadname(auid,languageCode)
    }
    fun getFundTypeId(receiptPayment:Int,type:String,uid:Int):String{
        return mstVOCOARepository!!.getFundTypeId(receiptPayment, type, uid)
    }
}