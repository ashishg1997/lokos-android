package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.microware.cdfi.repository.ResponseRepository

class ResponseViewModel : AndroidViewModel {

    var mRepository: ResponseRepository? = null

    constructor(application: Application) : super(application) {
        mRepository = ResponseRepository(application)
    }

    internal fun getResponseMsg(response_code :Int,language_id:String): String{
        return mRepository!!.getResponseMsg(response_code,language_id)
    }
}