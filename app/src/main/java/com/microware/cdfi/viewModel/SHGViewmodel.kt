package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.entity.SHGEntity
import com.microware.cdfi.repository.SHGRepository

class SHGViewmodel : AndroidViewModel {

    var mRepository: SHGRepository? = null

    constructor(application: Application) : super(application) {
        mRepository = SHGRepository(application)
    }

    internal fun getAllSHG(village_id:Int): LiveData<List<SHGEntity>>? {
        return mRepository?.getAllSHG(village_id)
    }
    internal fun getAll(): LiveData<List<SHGEntity>>? {
        return mRepository?.getAll()
    }

    internal fun getSHGdetail(ShgGUID: String?): List<SHGEntity>? {
        return mRepository?.getSHGdetail(ShgGUID)
    }
internal fun getSHG(ShgGUID: String?): List<SHGEntity>? {
        return mRepository?.getSHG(ShgGUID)
    }

    internal fun getShgCount(villageid:Int): Int {
        return mRepository!!.getShgCount(villageid)
    }

    internal fun getShgpendingCount(villageid:Int): Int {
        return mRepository!!.getShgpendingCount(villageid)
    }

    internal fun getShgactiveCount(villageid:Int): Int {
        return mRepository!!.getShgactiveCount(villageid)
    }
 internal fun getallShgCount(): Int {
        return mRepository!!.getallShgCount()
    }

    internal fun getallShgpendingCount(): Int {
        return mRepository!!.getallShgpendingCount()
    }

    internal fun getallShgactiveCount(): Int {
        return mRepository!!.getallShgactiveCount()
    }

       fun insert(shgEntity: SHGEntity) {
        mRepository!!.insert(shgEntity)
    }

    fun updateBasicDetail(
        GUID: String,
        HamletID: String,
        shg_name: String,
        shg_name_short_EN: String,
        shg_name_short_local: String,
        shg_type_code: Int,
        shg_name_local: String,
        composition: Int,
        shg_formation_date: Long,
        shg_revival_date: Long,
        shg_cooption_date: Long,
        SHG_PromotedBy: Int,
        SHG_RevivedBy: Int,
        meeting_frequency: Int,
        meeting_frequency_value: Int,
        meeting_on: Int,
        mode: Int,
        month_comp_saving: Int,
        is_bankaccount: Int,
        funding_agency_id: Int,
        parent_cbo_code: Int,
        parent_cbo_type: Int,
        Status: Int,
        is_active: Int,
        dedupl_status: Int,
        AccountBooksMaintained: Int,
        CashBookSTartDate: Int,
        BankBooKStartDate: Int,
        MembersLedgerStartDate: Int,
        Book4: Int,
        Book5: Int,
        Grade: String,
        GradingDoneOn: Int,
        GradeConfirmationStatus: String,
        bookkeeper_identified: Int,
        micro_plan_number: Int,
        WebDefaultChecker: Int,
        entry_source: Int,
        latitude: String,
        longitude: String,
        updated_date: Long,
        updated_by: String,
        primaryActivity: Int,
        secondaryActivity: Int,
        tertiaryActivity: Int,
        bookkeeper_name: String,
        bookkeeper_mobile: String,
        election_tenure: Int,
        ComsavingRoi: Double,
        voluntary_savings_interest: Double,
        is_volutary_saving: Int,
        saving_frequency: Int,
        tags: Int,
        promoter_name: String,
        promoter_code:Int?,
        imageName: String,
        shg_type_other: String,
        isVerified: Int,
        inactiveReason:Int

    ) {
        mRepository!!.updateBasicDetail(
            GUID,
            HamletID,
            shg_name,
            shg_name_short_EN,
            shg_name_short_local,
            shg_type_code,
            shg_name_local,
            composition,
            shg_formation_date,
            shg_revival_date,
            shg_cooption_date,
            SHG_PromotedBy,
            SHG_RevivedBy,
            meeting_frequency,
            meeting_frequency_value,
            meeting_on,
            mode,
            month_comp_saving,
            is_bankaccount,
            funding_agency_id,
            parent_cbo_code,
            parent_cbo_type,
            Status,
            is_active,
            dedupl_status,
            AccountBooksMaintained,
            CashBookSTartDate,
            BankBooKStartDate,
            MembersLedgerStartDate,
            Book4,
            Book5,
            Grade,
            GradingDoneOn,
            GradeConfirmationStatus,
            bookkeeper_identified,
            micro_plan_number,
            WebDefaultChecker,
            entry_source,
            latitude,
            longitude,
            updated_date,
            updated_by,
            primaryActivity,
            secondaryActivity,
            tertiaryActivity,
            bookkeeper_name,
            bookkeeper_mobile,
            election_tenure,
            ComsavingRoi,
            voluntary_savings_interest,
            is_volutary_saving,
            saving_frequency,
            tags,
            promoter_name,promoter_code,imageName,shg_type_other,isVerified,inactiveReason
        )
    }

    internal fun getAllSHGlist(): List<SHGEntity>? {
        return mRepository?.getAllSHGlist()
    }

    internal fun getAllSHGlistdata(): List<SHGEntity>? {
        return mRepository?.getAllSHGlistdata()
    }

    fun getPendingSHGlistdata(): List<SHGEntity>?{
        return mRepository?.getPendingSHGlistdata()
    }

    fun updateShguploadStatus(
        guid: String,
        remarks: String,
        user_id: String

    ) {
        mRepository!!.updateShguploadStatus(
            guid,
            remarks,
            user_id

        )
    }

    fun updateShgDedupStatus(
        guid: String,
        code: String,
        activationStatus: Int,
        approve_status: Int,
        checker_remarks: String



    ){
        mRepository!!.updateShgDedupStatus(
            guid,
            code,
            activationStatus,
            approve_status,
            checker_remarks

        )
    }
    fun getSHG_count(village_id:Int): Int{
        return mRepository!!.getSHG_count(village_id)
    }
 fun getlastdate(): Long{
        return mRepository!!.getlastdate()
    }
    fun getSHGlistdata(shgGUID: String?): LiveData<List<SHGEntity>>??{
        return mRepository!!.getSHGlistdata(shgGUID)
    }
    fun updateCheckerRemarks(checker_remark: String,shgGUID: String){
        mRepository!!.updateCheckerRemarks(checker_remark,shgGUID)
    }

    internal fun deleteShgByGuid(shgGUID: String?){
        return mRepository!!.deleteShgByGuid(shgGUID)
    }

    internal fun deleteBankByShgGuid(shgGUID: String?){
        return mRepository!!.deleteBankByShgGuid(shgGUID)
    }

    internal fun deletePhoneByShgGuid(shgGUID: String?){
        return mRepository!!.deletePhoneByShgGuid(shgGUID)
    }

    internal fun deleteAddressByShgGuid(shgGUID: String?){
        return mRepository!!.deleteAddressByShgGuid(shgGUID)
    }

    internal fun deleteSystemTagByShgGuid(shgGUID: String?){
        return mRepository!!.deleteSystemTagByShgGuid(shgGUID)
    }

    internal fun deleteDesignationByShgGuid(shgGUID: String?){
        return mRepository!!.deleteDesignationByShgGuid(shgGUID)
    }

    internal fun deleteMemberByShgGuid(shgGUID: String?){
        return mRepository!!.deleteMemberByShgGuid(shgGUID)
    }

    internal fun deleteMemberAddressByShgGuid(shgGUID: String?){
        return mRepository!!.deleteMemberAddressByShgGuid(shgGUID)
    }

    internal fun deleteMemberPhoneByShgGuid(shgGUID: String?){
        return mRepository!!.deleteMemberPhoneByShgGuid(shgGUID)
    }

    internal fun deleteMemberBankByShgGuid(shgGUID: String?){
        return mRepository!!.deleteMemberBankByShgGuid(shgGUID)
    }

    internal fun deleteMemberKycByShgGuid(shgGUID: String?){
        return mRepository!!.deleteMemberKycByShgGuid(shgGUID)
    }

    internal fun deleteMemberSystemTagByShgGuid(shgGUID: String?){
        return mRepository!!.deleteMemberSystemTagByShgGuid(shgGUID)
    }

    internal fun deleteShgDataByGuid(shgGUID: String?){
        return mRepository!!.deleteShgDataByGuid(shgGUID)
    }

    internal fun deleteBankDataByShgGuid(shgGUID: String?){
        return mRepository!!.deleteBankDataByShgGuid(shgGUID)
    }

    internal fun deletePhoneDataByShgGuid(shgGUID: String?){
        return mRepository!!.deletePhoneDataByShgGuid(shgGUID)
    }

    internal fun deleteAddressDataByShgGuid(shgGUID: String?){
        return mRepository!!.deleteAddressDataByShgGuid(shgGUID)
    }

    internal fun deleteSystemTagDataByShgGuid(shgGUID: String?){
        return mRepository!!.deleteSystemTagDataByShgGuid(shgGUID)
    }

    internal fun deleteDesignationDataByShgGuid(shgGUID: String?){
        return mRepository!!.deleteDesignationDataByShgGuid(shgGUID)
    }

    internal fun deleteMemberDataByShgGuid(shgGUID: String?){
        return mRepository!!.deleteMemberDataByShgGuid(shgGUID)
    }

    internal fun deleteMemberAddressDataByShgGuid(shgGUID: String?){
        return mRepository!!.deleteMemberAddressDataByShgGuid(shgGUID)
    }

    internal fun deleteMemberPhoneDataByShgGuid(shgGUID: String?){
        return mRepository!!.deleteMemberPhoneDataByShgGuid(shgGUID)
    }

    internal fun deleteMemberBankDataByShgGuid(shgGUID: String?){
        return mRepository!!.deleteMemberBankDataByShgGuid(shgGUID)
    }

    internal fun deleteMemberKycDataByShgGuid(shgGUID: String?){
        return mRepository!!.deleteMemberKycDataByShgGuid(shgGUID)
    }

    internal fun deleteMemberSystemTagDataByShgGuid(shgGUID: String?){
        return mRepository!!.deleteMemberSystemTagDataByShgGuid(shgGUID)
    }

    internal  fun getIsVerified(shgGUID:String?):Int{
        return  mRepository!!.getIsVerified(shgGUID)
    }

    fun getFinalVerification(shgGUID:String?):Int{
        return mRepository!!.getFinalVerification(shgGUID)
    }

    internal  fun updateIsVerified(guid: String,is_verified:Int,updated_date:Long,updated_by:String){
        mRepository!!.updateIsVerified(guid,is_verified,updated_date,updated_by)
    }

    internal  fun getIfscCode():List<String>{
        return mRepository!!.getIfscCode()
    }

    internal fun getBankCount(guid:String?):Int{
        return mRepository!!.getBankCount(guid)
    }

    internal fun getSignatoryCount(shgGUID: String?):Int{
        return mRepository!!.getSignatoryCount(shgGUID)
    }

    fun updateLockingFlag(isLocked:Int,isEdited:Int,shgGUID: String?){
        mRepository!!.updateLockingFlag(isLocked,isEdited,shgGUID)
    }
    fun updateMemberLockingFlag(isLocked:Int,isEdited:Int,shgGUID: String?){
        mRepository!!.updateMemberLockingFlag(isLocked,isEdited,shgGUID)
    }
    fun getLockingFlag(shgGUID:String?):Int{
        return mRepository!!.getLockingFlag(shgGUID)
    }

    fun getSHGname(ShgGUID: String?): String{
        return  mRepository!!.getSHGname(ShgGUID)
    }
}