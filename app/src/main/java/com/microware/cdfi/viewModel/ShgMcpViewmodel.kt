package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.microware.cdfi.entity.ShgMcpEntity
import com.microware.cdfi.repository.ShgMcpRepository

class ShgMcpViewmodel : AndroidViewModel {

    var shgMcpRepository: ShgMcpRepository? = null

    constructor(application: Application) : super(application) {
        shgMcpRepository = ShgMcpRepository(application)
    }

    fun getShgMcpDataAlllist(mcp_id: Long, cbo_id: Long, mem_id: Long): List<ShgMcpEntity>? {
        return shgMcpRepository!!.getShgMcpDataAlllist(mcp_id, cbo_id, mem_id)
    }

    fun getShgMcpDatalist(cbo_id: Long): List<ShgMcpEntity>? {
        return shgMcpRepository!!.getShgMcpDatalist(cbo_id)
    }

    internal fun gettotaldemand(cboid: Long): Int {
        return shgMcpRepository!!.gettotaldemand(cboid)
    }

    internal fun getCount(cboid: Long): Int {
        return shgMcpRepository!!.getCount(cboid)
    }

    fun insert(shgMcpEntity: ShgMcpEntity) {
        shgMcpRepository!!.insert(shgMcpEntity)
    }

    fun deleteAll() {
        return shgMcpRepository!!.deleteAll()
    }

    fun updateMcpPrepration(
        mcp_id: Long,
        cbo_id: Long,
        mem_id: Long,
        amt_demand: Int,
        tentative_date: Long,
        loan_product_id: Int?,
        loan_source: Int?,
        loan_purpose: Int?,
        loan_period: Int?,
        loan_requested_mtg_no: Int?,
        loan_requested_mtg_guid: String?,
        loan_sanctioned_mtg_no: Int?,
        loan_sanctioned_mtg_guid: String?,
        loan_request_priority : Int?,
        proposed_emi_amount : Int?,
        updatedby: String?,
        updatedon: Long?
    ) {
        shgMcpRepository!!.updateMcpPrepration(
            mcp_id,
            cbo_id,
            mem_id,
            amt_demand,
            tentative_date,
            loan_product_id,
            loan_source,
            loan_purpose,
            loan_period,
            loan_requested_mtg_no,
            loan_requested_mtg_guid,
            loan_sanctioned_mtg_no,
            loan_sanctioned_mtg_guid,
            loan_request_priority,
            proposed_emi_amount,
            updatedby,
            updatedon
        )
    }

    fun getUploadListData(mtg_no:Int,cbo_id:Long) : List<ShgMcpEntity>{
        return shgMcpRepository!!.getUploadListData(mtg_no,cbo_id)
    }


}