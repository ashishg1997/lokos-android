package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.api.model.EcScModelJoindata
import com.microware.cdfi.entity.VoShgMemberPhoneEntity
import com.microware.cdfi.entity.subcommitee_memberEntity
import com.microware.cdfi.repository.Subcommitee_memberRepository

class Subcommitee_memberViewModel:AndroidViewModel {

    var memberRepository:  Subcommitee_memberRepository? = null

    constructor(application: Application):super(application){
        memberRepository = Subcommitee_memberRepository(application)
    }

    fun insert(memberEntity : subcommitee_memberEntity){
        return memberRepository!!.insert(memberEntity)
    }

    fun getCommitteeMemberdetaildata(subcommitee_guid: String?): List<subcommitee_memberEntity>?{
        return memberRepository!!.getCommitteeMemberdetaildata(subcommitee_guid)
    }

    fun getCommitteeMemberdata(subcommitee_guid: String?,ec_member_code: Long?): List<subcommitee_memberEntity>?{
        return memberRepository!!.getCommitteeMemberdata(subcommitee_guid,ec_member_code)
    }

    fun getExecutiveMemberList(cbo_guid: String?): List<EcScModelJoindata>?{
        return memberRepository!!.getExecutiveMemberList(cbo_guid)
    }

    fun getECMemberList(cbo_guid: String?): List<EcScModelJoindata>?{
        return memberRepository!!.getECMemberList(cbo_guid)
    }

    fun updateScMemberdata(subcommitee_guid: String,
                           ec_member_code: Long,
                           fromdate:Long?,
                           todate:Long?,
                           status:Int,
                           is_active:Int,
                           entry_source:Int,
                           is_edited:Int,
                           updated_date:Long,
                           updated_by:String,
                           is_complete:Int){
        memberRepository!!.updateScMemberdata(subcommitee_guid,
            ec_member_code,
            fromdate,
            todate,
            status,
            is_active,
            entry_source,
            is_edited,
            updated_date,
            updated_by,
            is_complete)
    }

    fun getMemberPhone(member_id:Long):List<VoShgMemberPhoneEntity>{
        return memberRepository!!.getMemberPhone(member_id)
    }

    fun getScMembercount(ec_member_code: Long?):Int{
        return memberRepository!!.getScMembercount(ec_member_code)
    }

    fun getCommitteeMemberCount(subcommitee_guid: String?):Int{
        return memberRepository!!.getCommitteeMemberCount(subcommitee_guid)
    }
    fun getCLFExecutiveMemberList(cbo_guid: String?): List<EcScModelJoindata>?{
        return memberRepository!!.getCLFExecutiveMemberList(cbo_guid)
    }

    fun getScMembercountbySCGUID(ec_member_code: Long?,subcommitee_guid: String?):Int{
        return memberRepository!!.getScMembercountbySCGUID(ec_member_code,subcommitee_guid)
    }
}