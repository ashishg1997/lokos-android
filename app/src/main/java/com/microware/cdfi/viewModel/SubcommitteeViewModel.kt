package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.entity.subcommitee_memberEntity
import com.microware.cdfi.entity.subcommitteeEntity
import com.microware.cdfi.repository.SubcommitteeRepository

class SubcommitteeViewModel:AndroidViewModel {

    var subcommitteeRepository:  SubcommitteeRepository? = null

    constructor(application: Application):super(application){
        subcommitteeRepository = SubcommitteeRepository(application)
    }

    internal fun insert(subcommitteeEntity: subcommitteeEntity){
        return subcommitteeRepository!!.insert(subcommitteeEntity)
    }

    internal fun getCommitteedetaildata(subcommitee_guid: String?): List<subcommitteeEntity>?{
        return subcommitteeRepository!!.getCommitteedetaildata(subcommitee_guid)
    }

    internal fun getCommitteedata(cbo_guid: String?): LiveData<List<subcommitteeEntity>>?{
        return subcommitteeRepository!!.getCommitteedata(cbo_guid)
    }

    internal fun updateCommitteedata(subcommitee_guid:String,
                                     subcommitee_type_id:Int?,
                                     fromdate:Long?,
                                     todate:Long?,
                                     is_active:Int?,
                                     entry_source:Int?,
                                     is_edited:Int?,
                                     updated_date:Long?,
                                     updated_by:String?,
                                     status:Int){
        return subcommitteeRepository!!.updateCommitteedata(subcommitee_guid,
            subcommitee_type_id,
            fromdate,
            todate,
            is_active,
            entry_source,
            is_edited,
            updated_date,
            updated_by,
            status)
    }

    fun getCommitteePendingdata():List<subcommitteeEntity>?{
        return subcommitteeRepository!!.getCommitteePendingdata()
    }

    fun getCommitteeMemberPendingdata(subcommitee_guid: String?): List<subcommitee_memberEntity>?{
        return subcommitteeRepository!!.getCommitteeMemberPendingdata(subcommitee_guid)
    }

    fun updateuploadStatus(
        guid: String,
        lastupdatedate: Long
    ){
        subcommitteeRepository!!.updateuploadStatus(
            guid,lastupdatedate)
    }

    fun updateScuploadStatus(
        guid: String,
        lastupdatedate: Long
    ){
        subcommitteeRepository!!.updateScuploadStatus(
            guid,lastupdatedate)
    }

    fun updateScCompleteStatus(
        guid: String,
        lastupdatedate: Long
    ){
        subcommitteeRepository!!.updateScCompleteStatus(
            guid,lastupdatedate)
    }

    fun getScCount(scId: Int?,cboGuid:String?):Int{
        return subcommitteeRepository!!.getScCount(scId,cboGuid)
    }

    fun updateScMemberStatus(subcommitee_guid: String,
                             todate:Long?,
                             entry_source:Int,
                             is_edited:Int,
                             updated_date:Long,
                             updated_by:String){
        subcommitteeRepository!!.updateScMemberStatus(subcommitee_guid,
            todate,
            entry_source,
            is_edited,
            updated_date,
            updated_by)
    }
}