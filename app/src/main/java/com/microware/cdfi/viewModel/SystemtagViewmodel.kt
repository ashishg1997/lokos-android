package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.entity.SystemTagEntity
import com.microware.cdfi.repository.SystemtagRepository

class SystemtagViewmodel: AndroidViewModel {

    var systemtagRepository: SystemtagRepository? = null

    constructor(application: Application):super(application){
        systemtagRepository = SystemtagRepository(application)
    }

    fun getSystemtagdata(cbuguid: String?): LiveData<List<SystemTagEntity>>? {
        return systemtagRepository!!.getSystemtagdata(cbuguid)
    }
    fun getSystemtagdatalist(cbuguid: String?): List<SystemTagEntity>? {
        return systemtagRepository!!.getSystemtagdatalist(cbuguid)
    }
    fun getSystemtagdatalistcount(cbuguid: String?): List<SystemTagEntity>? {
        return systemtagRepository!!.getSystemtagdatalistcount(cbuguid)
    }
    fun getSystemtag(systemguid: String?): List<SystemTagEntity>? {
        return systemtagRepository!!.getSystemtag(systemguid)
    }

    fun insert(systemTagEntity: SystemTagEntity) {
        systemtagRepository!!.insert(systemTagEntity)
    }

    fun updateSystemTagdata(system_type:Int,system_id:String,systemtag_guid:String,updated_date:Long,updated_by:String){
        systemtagRepository!!.updateSystemTagdata(system_type,system_id,systemtag_guid,updated_date,updated_by)
    }

    fun deleteSystemtagData(systemtag_guid: String?){
        systemtagRepository!!.deleteSystemtagData(systemtag_guid)
    }

    fun deleteRecord(systemtag_guid: String?){
        systemtagRepository!!.deleteRecord(systemtag_guid)
    }

}