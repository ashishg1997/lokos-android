package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.entity.TransactionEntity
import com.microware.cdfi.repository.TransactionRepository

class TransactionViewmodel: AndroidViewModel {

    var transactionRepository: TransactionRepository? = null

    constructor(application: Application):super(application){
        transactionRepository = TransactionRepository(application)
    }

    internal fun gettransaction(): List<TransactionEntity>? {
        return transactionRepository!!.gettransaction()

    }

    internal fun getdatabytransaction(transactionno:String): List<TransactionEntity>?{
        return transactionRepository!!.getdatabytransaction(transactionno)
    }

    fun insert(transaction: TransactionEntity){
        return transactionRepository!!.insert(transaction)
    }

    fun deleteTransactionByGuid(shgGUID: String?){
        transactionRepository!!.deleteTransactionByGuid(shgGUID)
    }

    fun getUpdateQueueList(shgGuid:String) : LiveData<List<TransactionEntity>>{
        return transactionRepository!!.getUpdateQueueList(shgGuid)
    }
}