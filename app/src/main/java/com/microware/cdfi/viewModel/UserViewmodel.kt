package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.microware.cdfi.entity.UserEntity
import com.microware.cdfi.repository.UserRepository

class UserViewmodel: AndroidViewModel {

    var userRepository: UserRepository? = null

    constructor(application: Application):super(application){
        userRepository = UserRepository(application)
    }

    internal fun getUserData(): List<UserEntity>? {
        return userRepository!!.getUserData()
    }

    internal fun getUserCount(): Int {
        return userRepository!!.getUserCount()
    }

    internal fun getUserCountByUserId(userId:String): Int {
        return userRepository!!.getUserCountByUserId(userId)
    }
}