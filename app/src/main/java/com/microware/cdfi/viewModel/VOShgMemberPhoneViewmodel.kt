package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.microware.cdfi.entity.VoShgMemberEntity
import com.microware.cdfi.entity.VoShgMemberPhoneEntity
import com.microware.cdfi.repository.VOShgMemberPhoneRepository
import com.microware.cdfi.repository.VOShgMemberRepository

class VOShgMemberPhoneViewmodel : AndroidViewModel {

    var phoneRepository:  VOShgMemberPhoneRepository? = null

    constructor(application: Application):super(application){
        phoneRepository = VOShgMemberPhoneRepository(application)
    }

    fun getphonedetaillistdata(memberGuid:String):List<VoShgMemberPhoneEntity>?{
        return phoneRepository!!.getphonedetaillistdata(memberGuid)
    }
}