package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.microware.cdfi.entity.VoShgMemberEntity
import com.microware.cdfi.repository.VOShgMemberRepository

class VOShgMemberViewmodel : AndroidViewModel {

    var voShgMemberRepository:  VOShgMemberRepository? = null

    constructor(application: Application):super(application){
        voShgMemberRepository = VOShgMemberRepository(application)
    }

    fun getMemberList(shg_guid:String):List<VoShgMemberEntity>?{
        return voShgMemberRepository!!.getMemberList(shg_guid)
    }
}