package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.entity.VoCoaMappingEntity
import com.microware.cdfi.repository.VoCoaMappingRepository

class VoCoaMappingViewmodel: AndroidViewModel {

    var voCoaMappingRepository: VoCoaMappingRepository? = null

    constructor(application: Application):super(application){
        voCoaMappingRepository = VoCoaMappingRepository(application)
    }

    fun insert(voCoaMappingEntity: VoCoaMappingEntity){
        return voCoaMappingRepository!!.insert(voCoaMappingEntity)
    }

    fun getCoaMappinData(uid:Int,cboId:Long):List<VoCoaMappingEntity>{
        return voCoaMappingRepository!!.getCoaMappinData(uid,cboId)
    }
    fun getaccountno(uid:Int,cboId:Long):String {
        return voCoaMappingRepository!!.getaccountno(uid,cboId)
    }
    fun getcboData(cboId: Long): List<VoCoaMappingEntity> {
        return voCoaMappingRepository!!.getcboData(cboId)
    }
    fun getcboDatalist(cboId: Long): LiveData<List<VoCoaMappingEntity>> {
        return voCoaMappingRepository!!.getcboDatalist(cboId)
    }

    fun getBankCode(uid:Int,cboId:Long):String?{
        return voCoaMappingRepository!!.getBankCode(uid,cboId)
    }
}