package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.entity.ShgFinancialTxnVouchersEntity
import com.microware.cdfi.repository.VouchersRepository

class VouchersViewModel:AndroidViewModel {

    var vouchersRepository: VouchersRepository?=null

    constructor(application: Application) : super(application){
        vouchersRepository = VouchersRepository(application)
    }

    fun insert(vouchersEntity: ShgFinancialTxnVouchersEntity) {
        vouchersRepository!!.insert(vouchersEntity)
    }

    fun getVouchers():List<ShgFinancialTxnVouchersEntity>?{
        return vouchersRepository!!.getVouchers()
    }

    fun getMaxVoucherNum(mtgDate:Long): Int{
        return vouchersRepository!!.getMaxVoucherNum(mtgDate)
    }

    fun getDataList(mtgdate:Long, cbo_id: Long): LiveData<List<ShgFinancialTxnVouchersEntity>>?{
        return vouchersRepository!!.getDataList(mtgdate,cbo_id)
    }

    fun updateDateRelisation(date_realisation :Long,voucher_no: String, cboId: Long){
        vouchersRepository!!.updateDateRelisation(date_realisation ,voucher_no, cboId)
    }
    fun getVoucherUploadList(mtgNo:Int,cboId: Long):List<ShgFinancialTxnVouchersEntity>{
        return vouchersRepository!!.getVoucherUploadList(mtgNo,cboId)
    }
}