package com.microware.cdfi.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.entity.subcommitee_masterEntity
import com.microware.cdfi.repository.subcommitteeMasterRepository

class subcommitteeMasterViewModel :AndroidViewModel {

    var subcommitteeMasterRepository:  subcommitteeMasterRepository? = null

    constructor(application: Application):super(application){
        subcommitteeMasterRepository = subcommitteeMasterRepository(application)
    }

    fun getScMaster(languageID:String?):List<subcommitee_masterEntity>?{
        return  subcommitteeMasterRepository!!.getScMaster(languageID)
    }

    fun insert(subcommitee_masterEntity: subcommitee_masterEntity){
        return subcommitteeMasterRepository!!.insert(subcommitee_masterEntity)
    }

    fun getScMasterCount():Int{
        return  subcommitteeMasterRepository!!.getScMasterCount()
    }
}