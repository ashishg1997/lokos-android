package com.microware.cdfi.viewModel.voviewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.api.vomodel.VOBankTransactionModel
import com.microware.cdfi.entity.MstVOCOAEntity
import com.microware.cdfi.entity.voentity.VoFinTxnDetGrpEntity
import com.microware.cdfi.repository.vorepo.VoFinTxnDetGrpRepository


class VoFinTxnDetGrpViewModel : AndroidViewModel {
    var voFinTxnDetGrpRepository: VoFinTxnDetGrpRepository? = null

    constructor(application: Application) : super(application) {
        voFinTxnDetGrpRepository = VoFinTxnDetGrpRepository(application)
    }

    fun insertVoGroupLoanSchedule(VoFinTxnDetGrp: VoFinTxnDetGrpEntity?) {
        voFinTxnDetGrpRepository!!.insertVoGroupLoanSchedule(VoFinTxnDetGrp)
    }

    fun deleteVoFinTxnDetGrpData() {
        voFinTxnDetGrpRepository!!.deleteVoFinTxnDetGrpData()
    }

    fun getVoFinTxnDetGrpData(): LiveData<List<VoFinTxnDetGrpEntity>>? {
        return voFinTxnDetGrpRepository!!.getVoFinTxnDetGrpData()
    }

    fun getVoFinTxnDetGrpList(mtg_guid: String, cboid: Long): List<VoFinTxnDetGrpEntity>? {
        return voFinTxnDetGrpRepository!!.getVoFinTxnDetGrpList(mtg_guid, cboid)
    }

    fun deleteGrpFinancialTxnDetailData(mtg_no: Int, cbo_id: Long) {
        voFinTxnDetGrpRepository!!.deleteGrpFinancialTxnDetailData(mtg_no, cbo_id)
    }

    fun getAdjustmentCashCount(mtg_no: Int, cbo_id: Long): Int {
        return voFinTxnDetGrpRepository!!.getAdjustmentCashCount(mtg_no, cbo_id)
    }

    fun updateVoCashAdjustment(
        cbo_id: Long,
        mtg_no: Int,
        auid: Int,
        fund_type: Int?,
        amount_to_from: Int?,
        type: String?,
        amount: Int?,
        date_realisation: Long?,
        mode_payment: Int?,
        bank_code: String?,
        transaction_no: String?,
        updatedby: String?,
        updatedon: Long?
    ) {
        voFinTxnDetGrpRepository!!.updateVoCashAdjustment(
            cbo_id,
            mtg_no,
            auid,
            fund_type,
            amount_to_from,
            type,
            amount,
            date_realisation,
            mode_payment,
            bank_code,
            transaction_no,
            updatedby,
            updatedon
        )
    }


    fun getIncomeAndExpenditureAmount(
        cbo_id: Long,
        mtg_no: Int,
        modePayment: Int,
        fund_type: Int
    ): Int {
        return voFinTxnDetGrpRepository!!.getIncomeAndExpenditureAmount(
            cbo_id,
            mtg_no,
            modePayment,
            fund_type
        )
    }

    fun getMemberIncomeAndExpenditureAmount(cbo_id: Long, mtg_no: Int, fund_type: Int): Int {
        return voFinTxnDetGrpRepository!!.getMemberIncomeAndExpenditureAmount(
            cbo_id,
            mtg_no,
            fund_type
        )
    }

    fun getInvestmentListByMtgNum(
        cbo_id: Long,
        mtg_no: Int,
        auid: List<Int>,
        type: String
    ): LiveData<List<VoFinTxnDetGrpEntity>?> {
        return voFinTxnDetGrpRepository!!.getInvestmentListByMtgNum(cbo_id, mtg_no, auid, type)
    }

    fun getCoaSubHeadData(
        uid: List<Int>,
        type: String,
        languageCode: String
    ): List<MstVOCOAEntity>? {
        return voFinTxnDetGrpRepository!!.getCoaSubHeadData(uid, type, languageCode)
    }

    fun updateIncomeAndExpenditure(
        mtg_guid: String,
        cbo_id: Long,
        mtg_no: Int,
        auid: Int,
        fund_type: Int?,
        amount_to_from: Int?,
        type: String?,
        amount: Int?,
        date_realisation: Long?,
        mode_payment: Int?,
        bank_code: String?,
        transaction_no: String?,
        updatedby: String?,
        updatedon: Long?
    ) {
        voFinTxnDetGrpRepository!!.updateIncomeAndExpenditure(
            mtg_guid,
            cbo_id,
            mtg_no,
            auid,
            fund_type,
            amount_to_from,
            type,
            amount,
            date_realisation,
            mode_payment,
            bank_code,
            transaction_no,
            updatedby,
            updatedon
        )
    }

    internal fun getInvestmentdata(
        mtg_guid: String,
        cbo_id: Long,
        mtg_no: Int,
        auid: Int,
        type: String,
        amount_to_from: Int
    ): List<VoFinTxnDetGrpEntity>? {
        return voFinTxnDetGrpRepository!!.getInvestmentdata(
            mtg_guid,
            cbo_id,
            mtg_no,
            auid,
            type,
            amount_to_from
        )
    }

    internal fun getIncomeAndExpendituredata(
        mtg_guid: String,
        cbo_id: Long,
        mtg_no: Int,
        auid: Int,
        amount_to_from: Int
    ): List<VoFinTxnDetGrpEntity>? {
        return voFinTxnDetGrpRepository!!.getIncomeAndExpendituredata(
            mtg_guid,
            cbo_id,
            mtg_no,
            auid,
            amount_to_from
        )
    }

    fun getVoFinTxnDetGrpData(
        mtg_guid: String,
        cbo_id: Long,
        mtg_no: Int,
        fund_type: Int
    ): LiveData<List<VoFinTxnDetGrpEntity>>? {
        return voFinTxnDetGrpRepository!!.getVoFinTxnDetGrpData(mtg_guid, cbo_id, mtg_no, fund_type)
    }


    fun getVoFinTxnDetGrpListData(ntgNo: Int, cboid: Long): List<VoFinTxnDetGrpEntity>? {
        return voFinTxnDetGrpRepository!!.getVoFinTxnDetGrpListData(ntgNo, cboid)
    }

    fun getVoFinTxnDetgrpdatabyBank(
        cbo_id: Long,
        mtgno: Int,
        bankaccount: String,
        orgtype: Int,
        type: List<String>
    ): List<VoFinTxnDetGrpEntity>? {
        return voFinTxnDetGrpRepository!!.getVoFinTxnDetgrpdatabyBank(
            cbo_id,
            mtgno,
            bankaccount,
            orgtype,
            type
        )
    }

    fun updatefindetailgrp(
        cbo_id: Long,
        mtg_no: Int,
        bank_code: String?,
        mode_payment: Int?,
        transaction_no: String?,
        updatedby: String?,
        updatedon: Long?
    ) {
        voFinTxnDetGrpRepository!!.updatefindetailgrp(
            cbo_id,
            mtg_no,
            bank_code,
            mode_payment,
            transaction_no,
            updatedby,
            updatedon
        )
    }

    fun getIncomeAmount(
        cbo_id: Long,
        mtg_no: Int,
        modePayment: List<Int>,
        reciept: List<String>,
        bankcode: String
    ): Int {
        return voFinTxnDetGrpRepository!!.getIncomeAmount(
            cbo_id,
            mtg_no,
            modePayment,
            reciept,
            bankcode
        )

    }

    fun getpendingIncomeAmount(
        cbo_id: Long,
        mtg_no: Int,
        modePayment: Int,
        reciept: List<String>,
        bankcode: String,
        orgtype: Int
    ): Int {
        return voFinTxnDetGrpRepository!!.getpendingIncomeAmount(
            cbo_id,
            mtg_no,
            modePayment,
            reciept,
            bankcode,
            orgtype
        )

    }

    fun getSavingamount(mtgNO: Int, cboID: Long, auid: Int): List<VoFinTxnDetGrpEntity>? {
        return voFinTxnDetGrpRepository!!.getSavingamount(mtgNO, cboID, auid)
    }

    fun getAmount(uid: Int, mtgNo: Int, cboID: Long): Int {
        return voFinTxnDetGrpRepository!!.getAmount(uid, mtgNo, cboID)
    }

    fun getTotalFinTxnGrpAmount(mtgNo: Int, cboId: Long, orgType: Int, type: List<String>): Int {
        return voFinTxnDetGrpRepository!!.getTotalFinTxnGrpAmount(mtgNo, cboId, orgType, type)
    }

    fun getDebitAmount(cboId: Long, mtgNo: Int, type: String, bankCode: String): Int {
        return voFinTxnDetGrpRepository!!.getDebitAmount(cboId, mtgNo, type, bankCode)
    }

    fun gettxnkist(
        cboId: Long,
        mtgNo: Int,
        bankCode: String,
        txnno: String
    ): List<VoFinTxnDetGrpEntity>? {
        return voFinTxnDetGrpRepository!!.gettxnkist(cboId, mtgNo, bankCode, txnno)
    }

    fun getIncomeAmount(
        cbo_id: Long,
        mtg_no: Int,
        modePayment: List<Int>,
        reciept: List<String>
    ): Int {
        return voFinTxnDetGrpRepository!!.getIncomeAmount(cbo_id, mtg_no, modePayment, reciept)

    }

    fun getBankTransferList(
        cbo_id: Long,
        mtg_no: Int,
        fund_type: Int
    ): LiveData<List<VOBankTransactionModel>?> {
        return voFinTxnDetGrpRepository!!.getBankTransferList(cbo_id, mtg_no, fund_type)
    }

    fun getFinTxnDetailGrpAmount(
        cbodID: Long,
        mtgNo: Int,
        type: List<String>
    ): Int {
        return voFinTxnDetGrpRepository!!.getFinTxnDetailGrpAmount(cbodID, mtgNo, type)
    }

    fun getSavingDepositedAmount(mtgNo: Int, cboID: Long, type: List<String>): Int {
        return voFinTxnDetGrpRepository!!.getSavingDepositedAmount(mtgNo, cboID, type)
    }

    fun gettxnvoucherlist(cboId: Long, mtgNo: Int): List<VoFinTxnDetGrpEntity>? {
        return voFinTxnDetGrpRepository!!.gettxnvoucherlist(cboId, mtgNo)
    }
}