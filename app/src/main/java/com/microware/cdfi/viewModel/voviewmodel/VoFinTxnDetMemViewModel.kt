package com.microware.cdfi.viewModel.voviewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.entity.MstCOAEntity
import com.microware.cdfi.entity.voentity.VoFinTxnDetMemEntity
import com.microware.cdfi.repository.vorepo.VoFinTxnDetMemRepository

class VoFinTxnDetMemViewModel:AndroidViewModel {

    var voFinTxnDetMemRepository:  VoFinTxnDetMemRepository? = null

    constructor(application: Application):super(application){
        voFinTxnDetMemRepository = VoFinTxnDetMemRepository(application)
    }

    fun insertVoGroupLoanSchedule(VoFinTxnDetMem: VoFinTxnDetMemEntity?){
        voFinTxnDetMemRepository!!.insertVoGroupLoanSchedule(VoFinTxnDetMem)
    }
    fun deleteVoFinTxnDetMemData(){
        voFinTxnDetMemRepository!!.deleteVoFinTxnDetMemData()
    }

    fun getVoFinTxnDetMemData(): LiveData<List<VoFinTxnDetMemEntity>>?{
        return voFinTxnDetMemRepository!!.getVoFinTxnDetMemData()
    }
    fun getVoFinTxnDetMemList(mtg_guid:String, cbo_id: Long): List<VoFinTxnDetMemEntity>?{
        return  voFinTxnDetMemRepository!!.getVoFinTxnDetMemList(mtg_guid, cbo_id)
    }
    fun getVoFinTxnDetMemdata(
        cbo_id: Long,
        memid: Long,
        mtgno: Int,
        bankaccount: String,
        type: List<String>
    ): List<VoFinTxnDetMemEntity>?{
        return  voFinTxnDetMemRepository!!.getVoFinTxnDetMemdata(cbo_id, memid,mtgno,bankaccount,type)
    }

    fun deleteFinancialTransactionMemberDetailData(mtg_no: Int, cbo_id: Long) {
        voFinTxnDetMemRepository!!.deleteFinancialTransactionMemberDetailData(mtg_no, cbo_id)
    }

    fun getIncomeAmount(
        cbo_id: Long,
        mtg_no: Int,
        modePayment: List<Int>,
        reciept: List<String>,
        bankcode: String
    ): Int {
        return voFinTxnDetMemRepository!!.getIncomeAmount(cbo_id, mtg_no, modePayment,reciept,bankcode)

    }

    fun getpendingIncomeAmount(
        cbo_id: Long,
        memid: Long,
        mtg_no: Int,
        modePayment: Int,
        reciept: List<String>,
        bankcode: String
    ): Int {
        return voFinTxnDetMemRepository!!.getpendingIncomeAmount(cbo_id,memid, mtg_no, modePayment,reciept,bankcode)

    }

    fun getSavingamount(mtgNO:Int,cboID:Long,memId: Long, auid: Int): List<VoFinTxnDetMemEntity>? {
        return  voFinTxnDetMemRepository!!.getSavingamount(mtgNO,cboID,memId,auid)
    }

    fun getCoaSubHeadData(uid:List<Int>,type1:String,languageCode:String): List<MstCOAEntity>? {
        return voFinTxnDetMemRepository!!.getCoaSubHeadData(uid, type1, languageCode)
    }

    fun updateIncomeAndExpenditure(
        mtg_guid: String,
        cbo_id: Long,
        mtg_no:Int,
        auid: Int,
        type: String?,
        amount: Int?,
        date_realisation: Long?,
        mode_payment: Int?,
        bank_code: String?,
        transaction_no: String?,
        updatedby: String?,
        updatedon: Long?
    ) {
        voFinTxnDetMemRepository!!.updateIncomeAndExpenditure(
            mtg_guid,
            cbo_id,
            mtg_no,
            auid,
            type,
            amount,
            date_realisation,
            mode_payment,
            bank_code,
            transaction_no,
            updatedby,
            updatedon
        )
    }
    fun updatefindetailmem(
        cbo_id: Long,
        memid: Long,
        mtg_no: Int,
        bank_code: String?,
        mode_payment: Int?,
        transaction_no: String?,
        updatedby: String?,
        updatedon: Long?
    ){
        voFinTxnDetMemRepository!!.updatefindetailmem(
            cbo_id,
            memid,
            mtg_no,
            bank_code,
            mode_payment,
            transaction_no,
            updatedby,
            updatedon
        )
    }
    fun getIncomeAndExpendituredata(mtg_guid: String, cbo_id: Long, mtg_no: Int,auid:Int): List<VoFinTxnDetMemEntity>? {
        return voFinTxnDetMemRepository!!.getIncomeAndExpendituredata(mtg_guid, cbo_id, mtg_no, auid)
    }

    fun getIncomeAndExpenditureAlldata(
        mtg_guid: String,
        cbo_id: Long,
        mtg_no: Int
    ): LiveData<List<VoFinTxnDetMemEntity>?> {
        return voFinTxnDetMemRepository!!.getIncomeAndExpenditureAlldata(
            mtg_guid,
            cbo_id,
            mtg_no
        )
    }

    fun getsumvol(mtgno: Int, cbo_id: Long): Int {
        return voFinTxnDetMemRepository!!.getsumvol(mtgno, cbo_id)
    }
 fun getsumcomp(mtgno: Int, cbo_id: Long): Int {
        return voFinTxnDetMemRepository!!.getsumcomp(mtgno, cbo_id)
    }
    fun getFinTxnMemData(cboId: Long, memId: Long, mtgNo: Int): List<VoFinTxnDetMemEntity>? {
        return voFinTxnDetMemRepository!!.getFinTxnMemData(cboId,memId,mtgNo)
    }

    fun getauidlist(uid:Int,memId:Long, mtgNo: Int) : List<VoFinTxnDetMemEntity>? {
        return voFinTxnDetMemRepository!!.getauidlist(uid,memId,mtgNo)
    }

    fun getAmount(uid:Int,memId:Long,mtgNo:Int) : Int{
        return voFinTxnDetMemRepository!!.getAmount(uid, memId, mtgNo)
    }

    fun getpendingAmount(cboid: Long, memId: Long, mtgNo: Int, type: List<String>): Int {
        return voFinTxnDetMemRepository!!.getpendingAmount(cboid, memId, mtgNo, type)
    }

    fun getrecivedAmount(cboid: Long, memId: Long, mtgNo: Int, type: List<String>): Int {
        return voFinTxnDetMemRepository!!.getrecivedAmount(cboid, memId, mtgNo, type)
    }
    fun getTotalFinTxnMemAmount(mtgNo: Int, cboId: Long, orgType: Int, type: List<String>): Int {
        return voFinTxnDetMemRepository!!.getTotalFinTxnMemAmount(mtgNo, cboId, orgType,type)
    }

    fun getDebitAmount(cboId: Long, mtgNo: Int, type: String, bankCode: String): Int{
        return voFinTxnDetMemRepository!!.getDebitAmount(cboId, mtgNo, type, bankCode)
    }

    fun gettxnkist(cboId: Long, mtgNo: Int, bankCode: String, txnno: String): List<VoFinTxnDetMemEntity>? {
        return voFinTxnDetMemRepository!!.gettxnkist(cboId, mtgNo,  bankCode,txnno)
    }

    fun getAmountByAuid(uid: Int, memId: Long, mtgNo: Int, cboid: Long): Int {
        return voFinTxnDetMemRepository!!.getAmountByAuid(uid, memId, mtgNo, cboid)
    }
    fun getIncomeAmount(
        cbo_id: Long,
        mtg_no: Int,
        modePayment: List<Int>,
        reciept: List<String>
    ): Int {
        return voFinTxnDetMemRepository!!.getIncomeAmount(cbo_id, mtg_no, modePayment, reciept)

    }

    fun getFinTxnDetailMemAmount(
        cbodID: Long,
        mtgNo: Int,
        type: List<String>
    ):Int{
        return voFinTxnDetMemRepository!!.getFinTxnDetailMemAmount(cbodID, mtgNo, type)
    }
    fun gettxnvoucherlist(cboId: Long, mtgNo: Int): List<VoFinTxnDetMemEntity>? {
        return voFinTxnDetMemRepository!!.gettxnvoucherlist(cboId, mtgNo)
    }
}