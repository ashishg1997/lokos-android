package com.microware.cdfi.viewModel.voviewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.entity.DtMtgFinTxnEntity
import com.microware.cdfi.entity.voentity.VoFinTxnEntity
import com.microware.cdfi.repository.vorepo.VoFinTxnRepository

class VoFinTxnViewModel : AndroidViewModel {
    var voFinTxnRepository: VoFinTxnRepository? = null

    constructor(application: Application) : super(application) {
        voFinTxnRepository = VoFinTxnRepository(application)
    }

    fun insertVoFinTxn(VoFinTxn: VoFinTxnEntity?) {
        voFinTxnRepository!!.insertVoFinTxn(VoFinTxn)
    }

    fun deleteVoFinTxnData() {
        voFinTxnRepository!!.deleteVoFinTxnData()
    }

    fun getVoFinTxnData(): LiveData<List<VoFinTxnEntity>>? {
        return voFinTxnRepository!!.getVoFinTxnData()
    }

    fun getVoFinTxnList(): List<VoFinTxnEntity>? {
        return voFinTxnRepository!!.getVoFinTxnList()
    }

    internal fun getListDataByMtgnum(
        mtgno: Int,
        cbo_id: Long,
        accountNo: String
    ): List<VoFinTxnEntity> {
        return voFinTxnRepository!!.getListDataByMtgnum(mtgno, cbo_id, accountNo)
    }

    fun deleteVOFinancialTxnData(mtg_no: Int, cbo_id: Long) {
        voFinTxnRepository!!.deleteVOFinancialTxnData(mtg_no, cbo_id)
    }

    fun getListData(mtgno: Int, cbo_id: Long): List<VoFinTxnEntity> {
        return voFinTxnRepository!!.getListData(mtgno, cbo_id)
    }

    fun gettotalclosinginbank(mtgno: Int, cbo_id: Long, bankcode: String?): Int {
        return voFinTxnRepository!!.gettotalclosinginbank(mtgno, cbo_id, bankcode)
    }

    fun gettotalopeninginbank(mtgno: Int, cbo_id: Long, bankcode: String?): Int {
        return voFinTxnRepository!!.gettotalopeninginbank(mtgno, cbo_id, bankcode)
    }

    fun updateclosingbalbank(closingbal: Int, mtgno: Int, cbo_id: Long, accountno: String) {
        voFinTxnRepository!!.updateclosingbalbank(closingbal, mtgno, cbo_id, accountno)
    }
}