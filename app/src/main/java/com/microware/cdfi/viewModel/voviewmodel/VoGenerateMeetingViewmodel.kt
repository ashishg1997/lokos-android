package com.microware.cdfi.viewModel.voviewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.microware.cdfi.api.vomodel.VoLoanListModel
import com.microware.cdfi.api.vomodel.VoLoanRepaymentListModel
import com.microware.cdfi.api.vomodel.VoMeetingModel
import com.microware.cdfi.api.vomodel.VoShareCapitalModel
import com.microware.cdfi.entity.Cbo_bankEntity
import com.microware.cdfi.entity.voentity.VoFinTxnEntity
import com.microware.cdfi.entity.voentity.VoMtgDetEntity
import com.microware.cdfi.entity.voentity.VomtgEntity
import com.microware.cdfi.repository.vorepo.VoGenerateMeetingRepository

class VoGenerateMeetingViewmodel : AndroidViewModel {

    var vogenerateMeetingRepository:  VoGenerateMeetingRepository? = null

    constructor(application: Application):super(application){
        vogenerateMeetingRepository = VoGenerateMeetingRepository(application)
    }

    fun getmeetingCount(cbo_id: String): Int {
        return vogenerateMeetingRepository!!.getmeetingCount(cbo_id)
    }

    fun getMaxMeetingNum(shg_id: Long?): Int{
        return vogenerateMeetingRepository!!.getMaxMeetingNum(shg_id)
    }

    fun getMeetingCount(federationId: Long?): Int{
        return vogenerateMeetingRepository!!.getMeetingCount(federationId)
    }

    fun insert(vomtgEntity: VomtgEntity){
        return vogenerateMeetingRepository!!.insert(vomtgEntity)
    }

    fun openMeeting(flag: String, federationId: Long, mtgno: Int) {
        vogenerateMeetingRepository!!.openMeeting(flag, federationId, mtgno)
    }

    fun getgroupMeetingsdata(shg_code: String?): List<VoMeetingModel>? {
        return vogenerateMeetingRepository!!.getgroupMeetingsdata(shg_code)
    }

    internal fun getMtgByMtgnum(shg_id: Long?,mtgnum:Int): List<VoMtgDetEntity>{
        return vogenerateMeetingRepository!!.getMtgByMtgnum(shg_id,mtgnum)
    }

    fun insertvomtgDet(vomtgDetEntity: VoMtgDetEntity){
        return vogenerateMeetingRepository!!.insertvomtgDet(vomtgDetEntity)
    }

  /*  fun updateloandata1(mem_id: Long, mtgno: Int, loanno: Int, loanop: Int, loan_int_accrued: Int) {
        vogenerateMeetingRepository!!.updateloandata1(mem_id, mtgno, loanno, loanop,loan_int_accrued)
    }

    fun updateloandata2(mem_id: Long, mtgno: Int, loanno: Int, loanop: Int, loan_int_accrued: Int) {
        vogenerateMeetingRepository!!.updateloandata2(mem_id, mtgno, loanno, loanop,loan_int_accrued)
    }

    fun updateloandata3(mem_id: Long, mtgno: Int, loanno: Int, loanop: Int, loan_int_accrued: Int) {
        vogenerateMeetingRepository!!.updateloandata3(mem_id, mtgno, loanno, loanop,loan_int_accrued)
    }

    fun updateloandata4(mem_id: Long, mtgno: Int, loanno: Int, loanop: Int, loan_int_accrued: Int) {
        vogenerateMeetingRepository!!.updateloandata4(mem_id, mtgno, loanno, loanop,loan_int_accrued)
    }

    fun updateloandata5(mem_id: Long, mtgno: Int, loanno: Int, loanop: Int, loan_int_accrued: Int) {
        vogenerateMeetingRepository!!.updateloandata5(mem_id, mtgno, loanno, loanop,loan_int_accrued)
    }*/

    internal fun getBankdata(cbo_guid: String?): List<Cbo_bankEntity> {
        return vogenerateMeetingRepository!!.getBankdata(cbo_guid)
    }

    fun getMtglistdata(cbo_id: Long?): List<VomtgEntity>? {
        return vogenerateMeetingRepository!!.getMtglistdata(cbo_id)
    }

    fun getgroupMeetingsdatalist(cboType:Int): List<VoMeetingModel> {
        return vogenerateMeetingRepository!!.getgroupMeetingsdatalist(cboType)
    }

    fun returnmeetingstatus(federationId: Long, Mtgno: Int): String {
        return vogenerateMeetingRepository!!.returnmeetingstatus(federationId, Mtgno)
    }

    fun getMtglist(cbo_id: Long?): List<VomtgEntity>? {
        return vogenerateMeetingRepository!!.getMtglist(cbo_id)
    }

    fun closingMeeting(flag: String, mtgno: Int, cbo_id: Long, updatedOn: Long, updatedBy: String) {
        vogenerateMeetingRepository!!.closingMeeting(flag, mtgno, cbo_id, updatedOn, updatedBy)
    }

    fun deleteVo_MtgData(mtg_no: Int, cbo_id: Long) {
        vogenerateMeetingRepository!!.deleteVo_MtgData(mtg_no, cbo_id)
    }

    fun deleteVODetailData(mtg_no: Int, cbo_id: Long) {
        vogenerateMeetingRepository!!.deleteVODetailData(mtg_no, cbo_id)
    }

    fun getShareCapitalListByReceiptType(
        mtgNo: Int,
        cboId: Long,
        receiptType: Int
    ): List<VoShareCapitalModel> {
        return vogenerateMeetingRepository!!.getShareCapitalListByReceiptType(mtgNo, cboId, receiptType)
    }

    internal fun updateVoCutOffCash(
        cashInHand: Int,
        cashInTransit: Int,
        balanceDate: Long,
        closingcash: Int,
        mtgno: Int,
        cbo_id: Long,
        updatedOn: Long,
        updatedBy: String
    ) {
        vogenerateMeetingRepository!!.updateVoCutOffCash(
            cashInHand,
            cashInTransit,
            balanceDate,
            closingcash,
            mtgno,
            cbo_id,
            updatedOn,
            updatedBy
        )
    }

    internal fun getMeetingDetailData(mtgno: Int, cbo_id: Long): List<VomtgEntity> {
        return vogenerateMeetingRepository!!.getMeetingDetailData(mtgno, cbo_id)
    }

    internal fun getupdatedcash(mtgno: Int, cbo_id: Long): Int {
        return vogenerateMeetingRepository!!.getupdatedcash(mtgno, cbo_id)
    }

    fun getupdatedpeningcash(mtgno: Int, cbo_id: Long): Int {
        return vogenerateMeetingRepository!!.getupdatedpeningcash(mtgno, cbo_id)
    }

    fun gettotloan(mtgno: Int, cbo_id: Long): Int {
        return vogenerateMeetingRepository!!.gettotloan(mtgno, cbo_id)
    }

    fun gettotloanpaid(mtgno: Int, cbo_id: Long): Int {
        return vogenerateMeetingRepository!!.gettotloanpaid(mtgno, cbo_id)
    }
    fun getloanpaidbyauid(mtgno:Int,memid:Long,auid:Int): Int {
        return vogenerateMeetingRepository!!.getloanpaidbyauid(mtgno, memid,auid)
    }
    fun getgrouploanpaidbyauid(mtgno: Int, cboid: Long, auid: Int, LoanSource: Int): Int {
        return vogenerateMeetingRepository!!.getgrouploanpaidbyauid(mtgno, cboid,auid,LoanSource)
    }
   fun getgrouploanpaidbycbo(mtgno: Int, cboid: Long, LoanSource: Int): Int {
        return vogenerateMeetingRepository!!.getgrouploanpaidbycbo(mtgno, cboid,LoanSource)
    }
    fun gettotloangroup(mtgno: Int, cbo_id: Long): Int {
        return vogenerateMeetingRepository!!.gettotloangroup(mtgno, cbo_id)
    }

    fun gettotloanpaidgroup(mtgno: Int, cbo_id: Long): Int {
        return vogenerateMeetingRepository!!.gettotloanpaidgroup(mtgno, cbo_id)
    }

    fun updateclosingcash(closingcash: Int, mtgno: Int, cbo_id: Long) {
        vogenerateMeetingRepository!!.updateclosingcash(closingcash, mtgno, cbo_id)
    }

    internal fun getBankListDataByMtgnum(mtgno: Int, cbo_id: Long): List<VoFinTxnEntity> {
        return vogenerateMeetingRepository!!.getBankListDataByMtgnum(mtgno, cbo_id)
    }


    internal fun updateCutOffBankBalance(
        cbo_id: Long?,
        mtgno: Int?,
        bankcode: String?,
        zero_mtg_cash_bank: Int?,
        cheque_issued_not_realized: Int?,
        cheque_received_not_credited: Int?,
        closingbalance: Int?,
        balance_date: Long?,
        updatedby: String,
        updatedon: Long?,
        uploaded_by: String?
    ) {
        vogenerateMeetingRepository!!.updateCutOffBankBalance(
            cbo_id,
            mtgno,
            bankcode,
            zero_mtg_cash_bank,
            cheque_issued_not_realized,
            cheque_received_not_credited,
            closingbalance,
            balance_date,
            updatedby,
            updatedon,
            uploaded_by
        )
    }

    fun getTotalCompulsoryamount(mtgNo: Int, cboId: Long): Int? {
        return vogenerateMeetingRepository!!.getTotalCompulsoryamount(mtgNo,cboId)
    }

    fun getTotalVoluntaryamount(mtgNo: Int, cboId: Long): Int? {
        return vogenerateMeetingRepository!!.getTotalVoluntaryamount(mtgNo,cboId)
    }

    internal fun getCutOffClosedMemberLoanDataByMtgNum(
        mtgno: Int,
        cbo_id: Long,
        completionFlag: Boolean
    ): List<VoLoanListModel> {
        return vogenerateMeetingRepository!!.getCutOffClosedMemberLoanDataByMtgNum(mtgno, cbo_id, completionFlag)
    }

    internal fun getloanListDataMtgByMtgnum(mtgno: Int, cbo_id: Long, memId: Long,Loanproductid:Int): List<VoLoanListModel> {
        return vogenerateMeetingRepository!!.getloanListDataMtgByMtgnum(mtgno, cbo_id, memId,Loanproductid)
    }

    fun getCutOffTotalPresent(mtgno:Int,cbo_id:Long): Int{
        return vogenerateMeetingRepository!!.getCutOffTotalPresent(mtgno,cbo_id)
    }

    internal fun getCutOffMemberLoanAmount(cbo_id: Long,mtg_no: Int,completionFlag:Boolean):Int{
        return vogenerateMeetingRepository!!.getCutOffMemberLoanAmount(cbo_id,mtg_no,completionFlag)
    }

    fun getTotalAmountByReceiptType(mtg_guid: String, cbo_id: Long,mtg_no: Int,auid:Int): Int{
        return vogenerateMeetingRepository!!.getTotalAmountByReceiptType(mtg_guid,cbo_id,mtg_no,auid)
    }

    fun getTotalInvestmentByMtgNum(cbo_id: Long, mtg_no: Int,auid:List<Int>,type:String): Int{
        return vogenerateMeetingRepository!!.getTotalInvestmentByMtgNum(cbo_id,mtg_no,auid,type)
    }

    fun gettotalinbank(mtgno: Int, cbo_id: Long): Int {
        return vogenerateMeetingRepository!!.gettotalinbank(mtgno, cbo_id)
    }


    internal fun getBankdataWithAccountNo(
        cbo_guid: String?,
        bank_guid: String?
    ): List<Cbo_bankEntity> {
        return vogenerateMeetingRepository!!.getBankdataWithAccountNo(cbo_guid, bank_guid)
    }

    internal fun getListDatawithoutmember(mtgno: Int, cbo_id: Long, memid: Long): List<VoMtgDetEntity> {
        return vogenerateMeetingRepository!!.getListDatawithoutmember(mtgno, cbo_id,memid)
    }

    fun getClosedMemberLoanByMemberId(
        mtgno: Int,
        cbo_id: Long,
        completionFlag: Boolean,
        memberid: Long
    ): List<VoLoanListModel> {
        return vogenerateMeetingRepository!!.getClosedMemberLoanByMemberId(mtgno, cbo_id, completionFlag, memberid)
    }


    fun getVoMeetingList(): List<VomtgEntity>{
        return vogenerateMeetingRepository!!.getVoMeetingList()
    }
    internal  fun updateMeetinguploadStatus(
        shg_id: Long?,
        mtg_no: Int,
        lastupdatedate: Long
    ){
        vogenerateMeetingRepository!!.updateMeetinguploadStatus(
            shg_id,
            mtg_no,
            lastupdatedate
        )
    }
    internal fun getloanrepaymentList(mtgno:Int,memId:Long): List<VoLoanRepaymentListModel> {
        return vogenerateMeetingRepository!!.getloanrepaymentList(mtgno,memId)
    }


    fun getClosingBalanceCash(mtgNo: Int, cboId: Long): Int {
        return vogenerateMeetingRepository!!.getClosingBalanceCash(mtgNo,cboId)
    }

    fun getCashInTransit(mtgNo: Int, cboId: Long): Int {
        return vogenerateMeetingRepository!!.getCashInTransit(mtgNo,cboId)
    }


    fun getclosingbal(mtgno: Int, cbo_id: Long,accountno: String): Int {
        return vogenerateMeetingRepository!!.getclosingbal(mtgno, cbo_id,accountno)
    }
    internal fun getListinactivemember(mtgno: Int, cbo_id: Long): List<VoMtgDetEntity> {
        return vogenerateMeetingRepository!!.getListinactivemember(mtgno, cbo_id)
    }
    fun updatesavingwithdrawl(
        withdrawl: Int,
        mtgno: Int,
        cbo_id: Long,
        memid: Long,
        updated_by:String,
        updated_on:Long
    ) {
        vogenerateMeetingRepository!!.updatesavingwithdrawl(
            withdrawl,
            mtgno,
            cbo_id,
            memid,
            updated_by,
            updated_on
        )
    }
    fun getCompulsoryData(mtgno: Int, cbo_id: Long,mem_id: Long): List<VoMtgDetEntity>{
        return vogenerateMeetingRepository!!.getCompulsoryData(mtgno,cbo_id,mem_id)
    }
}