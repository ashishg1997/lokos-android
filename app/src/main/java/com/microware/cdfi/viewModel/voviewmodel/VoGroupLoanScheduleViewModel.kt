package com.microware.cdfi.viewModel.voviewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.entity.voentity.VoGroupLoanScheduleEntity
import com.microware.cdfi.repository.vorepo.VoGroupLoanScheduleRepository

class VoGroupLoanScheduleViewModel : AndroidViewModel {
    var voGroupLoanScheduleRepository: VoGroupLoanScheduleRepository? = null

    constructor(application: Application) : super(application) {
        voGroupLoanScheduleRepository = VoGroupLoanScheduleRepository(application)
    }

    fun insertVoGroupLoanSchedule(voGroupLoanSchduleEntity: VoGroupLoanScheduleEntity) {
        voGroupLoanScheduleRepository!!.insertVoGroupLoanSchedule(voGroupLoanSchduleEntity)
    }

    fun deleteVoGroupLoanScheduleData() {
        voGroupLoanScheduleRepository!!.deleteVoGroupLoanScheduleData()
    }

    fun getVoGroupLoanScheduleData(): LiveData<List<VoGroupLoanScheduleEntity>>? {
        return voGroupLoanScheduleRepository!!.getVoGroupLoanScheduleData()
    }

    fun getVoGroupLoanScheduleList(): List<VoGroupLoanScheduleEntity>? {
        return voGroupLoanScheduleRepository!!.getVoGroupLoanScheduleList()
    }

    fun getprincipaldemand(
        loanno:Int,
        cbo_id: Long,
        currentmtgdate: Long,
        lastmtgdate: Long
    ): Int {
        return voGroupLoanScheduleRepository!!.getprincipaldemand(
            loanno,
            cbo_id,
            currentmtgdate,
            lastmtgdate
        )
    }

    internal fun gettotaloutstanding(loanno:Int,cboid: Long): Int {
        return voGroupLoanScheduleRepository!!.gettotaloutstanding(loanno,cboid)
    }

    fun deleteGroupRepaidData(mtgdate: Long, mtgno: Int, cbo_id: Long) {
        voGroupLoanScheduleRepository!!.deleteGroupRepaidData(mtgdate, mtgno, cbo_id)
    }

    fun deleteGroupSubinstallmentData(mtgdate: Long, mtgno: Int, cbo_id: Long) {
        voGroupLoanScheduleRepository!!.deleteGroupSubinstallmentData(mtgdate, mtgno, cbo_id)
    }

    fun getPrincipalDemandByInstallmentNum(cboid: Long,mtgGuid: String,loanno: Int,installmentNo:Int):Int{
        return  voGroupLoanScheduleRepository!!.getPrincipalDemandByInstallmentNum(cboid,mtgGuid,loanno,installmentNo)
    }

    fun updateCutOffLoanSchedule(
        mtgGuid: String,
        cbo_id: Long,
        loanno: Int,
        installmentNo: Int,
        principalDemand: Int
    ) {
        voGroupLoanScheduleRepository!!.updateCutOffLoanSchedule(
            mtgGuid,
            cbo_id,
            loanno,
            installmentNo,
            principalDemand

        )
    }

    fun getGroupScheduledata(cboid: Long,mtgguid:String,loanno:Int): List<VoGroupLoanScheduleEntity>? {
        return voGroupLoanScheduleRepository!!.getGroupScheduledata(cboid,mtgguid,loanno)
    }

    fun updateLoanGroupSchedule(
        mtgguid: String,
        cboid: Long,
        loanno: Int,
        installmentno: Int,
        principaldemand: Int,
        loandemandos: Int,
        loanos: Int) {
        voGroupLoanScheduleRepository!!.updateLoanGroupSchedule(
            mtgguid,
            cboid,
            loanno,
            installmentno,
            principaldemand,
            loandemandos,
            loanos

        )
    }

    fun getLoanScheduleDetail(cboId: Long, loanNo: Int): List<VoGroupLoanScheduleEntity>? {
        return voGroupLoanScheduleRepository!!.getLoanScheduleDetail(cboId,loanNo)
    }
    internal fun getremaininginstallment(loanno: Int, cboid: Long): Int {
        return voGroupLoanScheduleRepository!!.getremaininginstallment(loanno, cboid)
    }

    fun deleterepaid(
        loanno: Int,
        shgId: Long,
        mtgdate: Long ,
        mtgno: Int  )
    {
        voGroupLoanScheduleRepository!!. deleterepaid(
            loanno,
            shgId,
            mtgdate,
            mtgno   )

    }

    fun deletesubinstallment(
        loanno: Int,
        shgId: Long,
        mtgdate: Long ,
        mtgno: Int  )
    {
        voGroupLoanScheduleRepository!!. deletesubinstallment(
            loanno,
            shgId,
            mtgdate,mtgno   )

    }

    fun getMemberScheduleloanwise(
        shgId: Long,
        loanno: Int,
        mtgno: Int
    ): List<VoGroupLoanScheduleEntity>? {
        return voGroupLoanScheduleRepository!!.getMemberScheduleloanwise(shgId,loanno,mtgno)
    }

    fun updaterepaid(
        loanno: Int,
        shgId: Long,
        installmentno: Int,
        sub_installment_no: Int,
        loanpaid: Int,
        mtgdate: Long,
        mtgno: Int,
        updatedby: String?,
        updatedon: Long
    ) {
        voGroupLoanScheduleRepository!!.updaterepaid(
            loanno,
            shgId,
            installmentno,
            sub_installment_no,
            loanpaid,
            mtgdate,mtgno,updatedby,updatedon
        )

    }
}