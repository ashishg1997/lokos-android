package com.microware.cdfi.viewModel.voviewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.entity.voentity.VoGroupLoanTxnEntity
import com.microware.cdfi.repository.vorepo.VoGroupLoanTxnRepository

class VoGroupLoanTxnViewModel:AndroidViewModel {
    var voGroupLoanTxnRepository:  VoGroupLoanTxnRepository? = null

    constructor(application: Application):super(application){
        voGroupLoanTxnRepository = VoGroupLoanTxnRepository(application)
    }

    fun insertVoGroupLoanTxn(voGroupLoanTxnEntity: VoGroupLoanTxnEntity){
        voGroupLoanTxnRepository!!.insertVoGroupLoanTxn(voGroupLoanTxnEntity)
    }

    fun deleteVoGroupLoanTxnData(){
        voGroupLoanTxnRepository!!.deleteVoGroupLoanTxnData()
    }

    fun getVoGroupLoanTxnData(): LiveData<List<VoGroupLoanTxnEntity>>?{
        return voGroupLoanTxnRepository!!.getVoGroupLoanTxnData()
    }

    fun   getVoGroupLoanTxnList(): List<VoGroupLoanTxnEntity>?{
        return voGroupLoanTxnRepository!!.getVoGroupLoanTxnList()
    }

    internal fun getListDataByMtgnum(mtgno:Int,cbo_aid:Long): List<VoGroupLoanTxnEntity> {
        return voGroupLoanTxnRepository!!.getListDataByMtgnum(mtgno,cbo_aid)
    }
     fun getListDataByLoansorce(mtgno:Int,cbo_aid:Long,LoanSorurce:Int): List<VoGroupLoanTxnEntity> {
        return voGroupLoanTxnRepository!!.getListDataByLoansorce(mtgno,cbo_aid,LoanSorurce)
    }
    fun deleteGroupLoanTxnData(mtg_no: Int, cbo_id: Long) {
        return voGroupLoanTxnRepository!!.deleteGroupLoanTxnData(mtg_no, cbo_id)
    }

    internal fun gettotalpaid(shgId: Long,mtgno: Int): Int {
        return voGroupLoanTxnRepository!!.gettotalpaid(shgId,mtgno)
    }

    fun getgrouploantxnlist(shgId: Long, mtgno: Int): List<VoGroupLoanTxnEntity>? {
        return voGroupLoanTxnRepository!!.getgrouploantxnlist(shgId,mtgno)
    }

    fun getgrouploandata(loanno: Int, mtgno: Int, shgid: Long): List<VoGroupLoanTxnEntity>? {
        return voGroupLoanTxnRepository!!.getgrouploandata(loanno,mtgno,shgid)
    }

    fun updateloanpaid(
        loanno: Int, mtgno: Int,
        shgID: Long,
        paid: Int,
        loanint: Int,
        mode: Int,
        bankaccount: String,
        transactionno: String,
        principaldemandcl: Int,
        completionflag: Boolean
    ) {
        voGroupLoanTxnRepository!!.updateloanpaid(loanno, mtgno, shgID, paid,loanint,
            mode,
            bankaccount,
            transactionno,principaldemandcl,completionflag
        )
    }
    internal fun getCutOffgroupLoanAmount(cbo_id: Long,mtg_no: Int):Int{
        return voGroupLoanTxnRepository!!.getCutOffgroupLoanAmount(cbo_id,mtg_no)
    }

    fun getgrptotloanpaidinbank(mtgno: Int, cbo_id: Long, account: String, modePayment: List<Int>): Int {
        return voGroupLoanTxnRepository!!.getgrptotloanpaidinbank(mtgno, cbo_id,account,modePayment)
    }

    fun getgrptotloanpaidinbank(modePayment: List<Int>,mtgno:Int,cbo_id:Long): Int {
        return voGroupLoanTxnRepository!!.getgrptotloanpaidinbank(modePayment,mtgno, cbo_id)
    }

    fun getRepaymentClfAmount(mtgno: Int,cbo_id: Long):Int{
        return voGroupLoanTxnRepository!!.getRepaymentClfAmount(mtgno, cbo_id)
    }

    fun getTotalRepaymentClfAmount(mtgno: Int,cbo_id: Long):Int{
        return voGroupLoanTxnRepository!!.getTotalRepaymentClfAmount(mtgno, cbo_id)
    }

}