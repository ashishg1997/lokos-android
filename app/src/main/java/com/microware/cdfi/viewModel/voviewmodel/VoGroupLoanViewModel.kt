package com.microware.cdfi.viewModel.voviewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.entity.voentity.VoGroupLoanEntity
import com.microware.cdfi.repository.vorepo.VoGroupLoanRepository

class VoGroupLoanViewModel : AndroidViewModel {
    var voGroupLoanRepository: VoGroupLoanRepository? = null

    constructor(application: Application) : super(application) {
        voGroupLoanRepository = VoGroupLoanRepository(application)
    }

    fun insertVoGroupLoan(voGroupLoanEntity: VoGroupLoanEntity) {
        voGroupLoanRepository!!.insertVoGroupLoan(voGroupLoanEntity)
    }

    fun deleteVoGroupLoanData() {
        voGroupLoanRepository!!.deleteVoGroupLoanData()
    }

    fun getVoGroupLoanData(): LiveData<List<VoGroupLoanEntity>>? {
        return voGroupLoanRepository!!.getVoGroupLoanData()
    }

    fun getVoGroupLoanList(): List<VoGroupLoanEntity>? {
        return voGroupLoanRepository!!.getVoGroupLoanList()
    }

    fun getinterestrate(loanno: Int): Double {
        return voGroupLoanRepository!!.getinterestrate(loanno)
    }

    fun deleteGroupLoanData(mtg_no: Int, cbo_id: Long) {
        return voGroupLoanRepository!!.deleteGroupLoanData(mtg_no, cbo_id)
    }

    fun getmaxLoanno(cboId: Long): Int {
        return voGroupLoanRepository!!.getmaxLoanno(cboId)
    }

    fun getListDataByMtgnum(mtgno: Int, cbo_aid: Long, LoanSource: Int, fundType: Int): List<VoGroupLoanEntity> {
        return voGroupLoanRepository!!.getListDataByMtgnum(mtgno, cbo_aid, LoanSource,fundType)
    }
 fun getListDatavoucher(mtgno: Int, cbo_aid: Long): List<VoGroupLoanEntity> {
        return voGroupLoanRepository!!.getListDatavoucher(mtgno, cbo_aid)
    }

    fun getLoandata(cbo_id: Long, loanno: Int, mtgno: Int): List<VoGroupLoanEntity>? {
        return voGroupLoanRepository!!.getLoandata(cbo_id, loanno, mtgno)
    }

    fun getLoandatabyloanno(cbo_id: Long, loanno: Int): List<VoGroupLoanEntity>? {
        return voGroupLoanRepository!!.getLoandatabyloanno(cbo_id, loanno)
    }

    fun getCutOffmaxLoanno(cbo_id: Long, mtg_no: Int): Int {
        return voGroupLoanRepository!!.getCutOffmaxLoanno(cbo_id, mtg_no)
    }

    fun getLoanDetail(cbo_id: Long, mtgNo: Int): List<VoGroupLoanEntity>? {
        return voGroupLoanRepository!!.getLoanDetail(cbo_id, mtgNo)
    }

    fun getLoanDetailData(loan_no: Int, cbo_id: Long, mtg_guid: String): List<VoGroupLoanEntity>? {
        return voGroupLoanRepository!!.getLoanDetailData(loan_no, cbo_id, mtg_guid)
    }

    fun gettotamt(loanno: Int, cboID: Long): Double {
        return voGroupLoanRepository!!.gettotamt(loanno, cboID)
    }

    fun getfundamt(cboid: Long, mtgNum: Int, fundType: Int, LoanSource: Int): Int {
        return voGroupLoanRepository!!.getfundamt(cboid, mtgNum, fundType, LoanSource)
    }

    fun gettotamtbyloansource(cboid: Long, mtgNum: Int, LoanSource: Int): Int {
        return voGroupLoanRepository!!.gettotamtbyloansource(cboid, mtgNum, LoanSource)
    }

    fun getgrouptotloanamtinbank(
        mtgno: Int,
        cbo_id: Long,
        code: String,
        modeofpayment: List<Int>
    ): Int {
        return voGroupLoanRepository!!.getgrouptotloanamtinbank(mtgno, cbo_id, code, modeofpayment)
    }

    fun getgrouptotloanamtinbank(modePayment: List<Int>, mtgno: Int, cbo_id: Long): Int {
        return voGroupLoanRepository!!.getgrouptotloanamtinbank(modePayment, mtgno, cbo_id)
    }

    fun getLoanRecAmount(loansource: Int, mtgno: Int, cbo_id: Long): Int{
        return voGroupLoanRepository!!.getLoanRecAmount(loansource, mtgno, cbo_id)
    }

     fun updateShgGroupLoan(cbo_id:Long,loan_no: Int, completionflag: Boolean){
        voGroupLoanRepository!!.updateShgGroupLoan(cbo_id,loan_no,completionflag)
    }

    fun getUploadGroupLoanList(cbo_id: Long) : List<VoGroupLoanEntity>{
        return voGroupLoanRepository!!.getUploadGroupLoanList( cbo_id)
    }
}