package com.microware.cdfi.viewModel.voviewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.entity.DtLoanApplicationEntity
import com.microware.cdfi.entity.voentity.VoLoanApplicationEntity
import com.microware.cdfi.repository.vorepo.VoLoanApplicationRepo

class VoLoanApplicationViewmodel : AndroidViewModel {

    var fedLoanApplicationRepo: VoLoanApplicationRepo? = null

    constructor(application: Application) : super(application) {
        fedLoanApplicationRepo = VoLoanApplicationRepo(application)
    }

    fun insertFedLoanApplicationData(fedLoanAppEntity: VoLoanApplicationEntity?) {
        fedLoanApplicationRepo!!.insertFedLoanApplicationData(fedLoanAppEntity)
    }

    fun deleteFedLoanApplicationData() {
        fedLoanApplicationRepo!!.deleteFedLoanApplicationData()
    }

    fun getFedLoanApplicationData(): LiveData<List<VoLoanApplicationEntity>>? {
        return fedLoanApplicationRepo!!.getFedLoanApplicationData()
    }

    fun getFedLoanApplicationDataAll(): List<VoLoanApplicationEntity>? {
        return fedLoanApplicationRepo!!.getFedLoanApplicationDataAll()
    }

    fun deleteVoLoanApplication(mtg_no: Int, cbo_id: Long) {
        fedLoanApplicationRepo!!.deleteVoLoanApplication(mtg_no, cbo_id)
    }

    internal fun getmaxloanno(cboid: Long): Int {
        return fedLoanApplicationRepo!!.getmaxloanno(cboid)
    }

    fun updateMemberloanapplicationdetail(
        loanapplication_id: Long,
        amt_disbursed: Int,
        amtsaction: Int,
        priorty: Int,
        approvalDate: Long,
        tentivedate: Long
    ) {
        fedLoanApplicationRepo!!.updateMemberloanapplicationdetail(
            loanapplication_id,
            amt_disbursed,
            amtsaction,
            priorty,
            approvalDate,
            tentivedate
        )
    }

    internal fun getmemberLoanApplication(loanapplication_id: Long,mtgNo:Int,shgid: Long): List<VoLoanApplicationEntity>? {
        return fedLoanApplicationRepo!!.getmemberLoanApplication(loanapplication_id,mtgNo,shgid)
    }

    fun getPriorityCount(priority: Int,mtg_no: Int,cbo_id: Long):Int{
        return fedLoanApplicationRepo!!.getPriorityCount(priority,mtg_no,cbo_id)
    }

    fun getTotalLoanCount(mtgNum:Int,mem_id: Long):Int{
        return fedLoanApplicationRepo!!.getTotalLoanCount(mtgNum,mem_id)
    }

    fun getLoanOutstanding(mtgNum:Int,shgid:Long,mem_id: Long):Int{
        return fedLoanApplicationRepo!!.getLoanOutstanding(mtgNum,shgid,mem_id)
    }

//replace//

    internal fun getLoanApplicationlist(mtgno: Int, cboid: Long,memId:Long,Loanproductid:Int): List<VoLoanApplicationEntity>? {
        return fedLoanApplicationRepo!!.getLoanApplicationlist(mtgno, cboid, memId,Loanproductid)
    }

    internal fun gettotaldemand(mtgno: Int, cboid: Long,memId:Long,Loanproductid:Int): Int {
        return fedLoanApplicationRepo!!.gettotaldemand(mtgno, cboid,memId,Loanproductid)
    }

    internal fun getLoanApplicationlist(mtgno: Int, cboid: Long): List<VoLoanApplicationEntity>? {
        return fedLoanApplicationRepo!!.getLoanApplicationlist(mtgno, cboid)
    }


    fun updatevoshgloanapplication(
        mtgguid: String,
        shgmemid: Long,
        void: Long,
        mtgno: Int,
        loanappid: Long,
        amt: Int


    ) {
        fedLoanApplicationRepo!!.updatevoshgloanapplication(
            mtgguid,
            shgmemid,
            void,
            mtgno,
            loanappid,
            amt
        )
    }
    fun getdemand(mtgno: Int,cboid:Long): Int{
        return fedLoanApplicationRepo!!.getdemand(mtgno, cboid)
    }

}