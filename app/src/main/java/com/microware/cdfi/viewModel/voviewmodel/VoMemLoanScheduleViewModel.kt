package com.microware.cdfi.viewModel.voviewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.entity.voentity.VoMemLoanScheduleEntity
import com.microware.cdfi.repository.vorepo.VoMemLoanScheduleRepository

class VoMemLoanScheduleViewModel:AndroidViewModel {
    var voMemLoanScheduleRepository:  VoMemLoanScheduleRepository? = null

    constructor(application: Application):super(application){
        voMemLoanScheduleRepository = VoMemLoanScheduleRepository(application)
    }

    fun insertVoMemLoanSchedule(voMemLoanScheduleEntity: VoMemLoanScheduleEntity){
        voMemLoanScheduleRepository!!.insertVoMemLoanSchedule(voMemLoanScheduleEntity)
    }

    fun deleteVoMemLoanScheduleData(){
        voMemLoanScheduleRepository!!.deleteVoMemLoanScheduleData()
    }

    fun getVoMemLoanScheduleData(): LiveData<List<VoMemLoanScheduleEntity>>?{
        return voMemLoanScheduleRepository!!.getVoMemLoanScheduleData()
    }

    fun   getVoMemLoanScheduleList(): List<VoMemLoanScheduleEntity>?{
        return voMemLoanScheduleRepository!!.getVoMemLoanScheduleList()
    }
    fun getprincipaldemand(
        loanNo: Int,
        mem_id: Long,
        currentmtgdate: Long,
        lastmtgdate: Long
    ): Int {
        return voMemLoanScheduleRepository!!.getprincipaldemand(
            loanNo,mem_id,
            currentmtgdate,
            lastmtgdate
        )
    }

    internal fun gettotaloutstanding(loanNo: Int,mem_id: Long): Int {
        return voMemLoanScheduleRepository!!.gettotaloutstanding(loanNo, mem_id)
    }

    fun deleteMemberRepaidData(mtgdate: Long, mtgno: Int, cbo_id: Long) {
        voMemLoanScheduleRepository!!.deleteMemberRepaidData(mtgdate, mtgno, cbo_id)
    }

    fun deleteMemberSubinstallmentData(mtgdate: Long, mtgno: Int, cbo_id: Long) {
        voMemLoanScheduleRepository!!.deleteMemberSubinstallmentData(mtgdate, mtgno, cbo_id)
    }

    fun getPrincipalDemandByInstallmentNum(memId: Long,mtgGuid: String,loanNo: Int,installmenNo:Int):Int{
        return  voMemLoanScheduleRepository!!.getPrincipalDemandByInstallmentNum(memId,mtgGuid,loanNo,installmenNo)
    }

    fun updateCutOffLoanMemberSchedule(
        mtgguid: String,
        memId: Long,
        loanno: Int,
        installmentno: Int,
        principaldemand: Int
    ) {
        voMemLoanScheduleRepository!!.updateCutOffLoanMemberSchedule(
            mtgguid,
            memId,
            loanno,
            installmentno,
            principaldemand
        )
    }

    internal fun getMemberScheduledata(
        mem_id: Long,
        mtgguid: String,
        loanno: Int
    ): List<VoMemLoanScheduleEntity>? {
        return voMemLoanScheduleRepository!!.getMemberScheduledata(mem_id, mtgguid, loanno)
    }

    fun updateLoanMemberSchedule(
        mtgguid: String,
        mem_id: Long,
        loanno: Int,
        installmentno: Int,
        principaldemand: Int,
        loandemandos: Int,
        loanos: Int

    ) {
        voMemLoanScheduleRepository!!.updateLoanMemberSchedule(
            mtgguid,
            mem_id,
            loanno,
            installmentno,
            principaldemand,
            loandemandos,
            loanos

        )
    }

    internal fun gettotalloanamt(loanno: Int, mem_id: Long, mtgguid: String): Int {
        return voMemLoanScheduleRepository!!.gettotalloanamt(loanno, mem_id, mtgguid)
    }
    fun getLoanScheduleDetail(cboId: Long, loanNo: Int): List<VoMemLoanScheduleEntity>? {
        return voMemLoanScheduleRepository!!.getLoanScheduleDetail(cboId,loanNo)
    }

    fun getnextdemand(mem_id: Long,loanno:Int): Int {
        return voMemLoanScheduleRepository!!.getnextdemand(mem_id,loanno)
    }

    internal fun getremaininginstallment(loanno: Int, mem_id: Long): Int {
        return voMemLoanScheduleRepository!!.getremaininginstallment(loanno, mem_id)
    }

    fun deleterepaid(
        loanno: Int,
        mem_id: Long,
        mtgdate: Long ,
        mtgno: Int  )
    {
        voMemLoanScheduleRepository!!. deleterepaid(
            loanno,
            mem_id,
            mtgdate,mtgno   )

    }
    fun deletesubinstallment(
        loanno: Int,
        mem_id: Long,
        mtgdate: Long ,
        mtgno: Int  )
    {
        voMemLoanScheduleRepository!!. deletesubinstallment(
            loanno,
            mem_id,
            mtgdate,mtgno   )

    }

    fun getMemberScheduleloanwise(
        mem_id: Long,
        loanno: Int,
        mtgno: Int
    ): List<VoMemLoanScheduleEntity>? {
        return voMemLoanScheduleRepository!!.getMemberScheduleloanwise(mem_id,loanno,mtgno)
    }

    fun updaterepaid(
        loanno: Int,
        mem_id: Long,
        installmentno: Int,
        subInstallmentNo: Int,
        loanpaid: Int,
        mtgdate: Long,
        mtgno: Int,
        updatedby: String?,
        updatedon: Long
    ) {
        voMemLoanScheduleRepository!!.updaterepaid(
            loanno,
            mem_id,
            installmentno,
            subInstallmentNo,
            loanpaid,
            mtgdate,mtgno,updatedby,updatedon
        )

    }
    fun gettotalloanoutstanding(mem_id: Long): Int {
        return voMemLoanScheduleRepository!!.gettotalloanoutstanding(mem_id)
    }

}