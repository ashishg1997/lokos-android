package com.microware.cdfi.viewModel.voviewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.api.vomodel.VoLoanRepaymentListModel
import com.microware.cdfi.entity.voentity.VoMemLoanTxnEntity
import com.microware.cdfi.repository.vorepo.VoMemLoanTxnRepository

class VoMemLoanTxnViewModel : AndroidViewModel {
    var voMemLoanTxnRepository: VoMemLoanTxnRepository? = null

    constructor(application: Application) : super(application) {
        voMemLoanTxnRepository = VoMemLoanTxnRepository(application)
    }

    fun insert(voMemLoanTxnEntity: VoMemLoanTxnEntity) {
        voMemLoanTxnRepository!!.insertVoMemLoanTxn(voMemLoanTxnEntity)
    }

    fun deleteVoMemLoanTxnData() {
        voMemLoanTxnRepository!!.deleteVoMemLoanTxnData()
    }

    fun getVoMemLoanTxnData(): LiveData<List<VoMemLoanTxnEntity>>? {
        return voMemLoanTxnRepository!!.getVoMemLoanTxnData()
    }

    fun getVoMemLoanTxnList(): List<VoMemLoanTxnEntity>? {
        return voMemLoanTxnRepository!!.getVoMemLoanTxnList()
    }

    internal fun getmeetingLoanTxnMemdata(
        cboid: Long,
        mem_id: Long,
        mtgno: Int
    ): List<VoMemLoanTxnEntity>? {
        return voMemLoanTxnRepository!!.getmeetingLoanTxnMemdata(cboid, mem_id, mtgno)
    }

    fun deleteMemberLoanTxnData(mtg_no: Int, cbo_id: Long) {
        voMemLoanTxnRepository!!.deleteMemberLoanTxnData(mtg_no, cbo_id)
    }

    fun getmemberloan(memid: Long, mtgno: Int): List<VoLoanRepaymentListModel>? {
        return voMemLoanTxnRepository!!.getmemberloan(memid,mtgno)
    }

    fun getmemberloantxnlist(memid: Long, mtgno: Int, fundtype: Int): List<VoMemLoanTxnEntity>? {
        return voMemLoanTxnRepository!!.getmemberloantxnlist(memid,mtgno,fundtype)
    }

    internal fun gettotalpaid(mem_id: Long,mtgno: Int, fundtype: Int): Int {
        return voMemLoanTxnRepository!!.gettotalpaid(mem_id,mtgno,fundtype)
    }

    fun getmemberloandata(loanno: Int, mtgno: Int, memid: Long): List<VoMemLoanTxnEntity>? {
        return voMemLoanTxnRepository!!.getmemberloandata(loanno,mtgno,memid)
    }

    fun updateloanpaid(
        loanno: Int, mtgno: Int, memid: Long, paid: Int, loanint: Int,
        mode: Int,
        bankaccount: String,
        transactionno: String,
        principaldemandcl: Int,
        completionflag: Boolean
    ) {
        voMemLoanTxnRepository!!.updateloanpaid(loanno, mtgno, memid, paid,loanint,
            mode,
            bankaccount,
            transactionno,principaldemandcl,completionflag
        )
    }

    fun getmemtotloanpaidinbank(mtgno: Int, cbo_id: Long, account: String, modePayment: List<Int>): Int {
        return voMemLoanTxnRepository!!.getmemtotloanpaidinbank(mtgno, cbo_id,account,modePayment)
    }
    fun getMemberLoanRepaymentAmount(cboid: Long, mtgno: Int,mem_id: Long):Int{
        return voMemLoanTxnRepository!!.getMemberLoanRepaymentAmount(cboid, mtgno,mem_id)
    }

    fun getMemberLoanRepaymentIntAmount(cboid: Long, mtgno: Int,mem_id: Long):Int{
        return voMemLoanTxnRepository!!.getMemberLoanRepaymentIntAmount(cboid, mtgno, mem_id)
    }

    fun getMemberLoanRepaymentOPAmount(cboid: Long, mtgno: Int,mem_id: Long):Int{
        return voMemLoanTxnRepository!!.getMemberLoanRepaymentOPAmount(cboid, mtgno, mem_id)
    }

    fun getmemtotloanpaidinbank(modePayment: List<Int>, mtgno: Int, cbo_id: Long): Int {
        return voMemLoanTxnRepository!!.getmemtotloanpaidinbank(modePayment, mtgno, cbo_id)
    }
    fun getTotalLoanRepayReceived(cboid: Long, mtgNo: Int):Int{
        return voMemLoanTxnRepository!!.getTotalLoanRepayReceived(cboid, mtgNo)
    }

    fun getLoanRepayReceived(cboid: Long, mtgNo: Int):Int{
        return voMemLoanTxnRepository!!.getLoanRepayReceived(cboid, mtgNo)
    }
     fun getListDataByMtgnum(mtgno:Int,cbo_aid:Long): List<VoMemLoanTxnEntity> {
        return voMemLoanTxnRepository!!.getListDataByMtgnum(mtgno,cbo_aid)
    }
    fun gettotalpaid1(mem_id: Long,mtgno: Int): Int {
        return voMemLoanTxnRepository!!.gettotalpaid1(mem_id,mtgno)
    }

    fun getmemberloantxnlist1(memid: Long, mtgno: Int): List<VoMemLoanTxnEntity>?{
        return voMemLoanTxnRepository!!.getmemberloantxnlist1(memid,mtgno)
    }
}