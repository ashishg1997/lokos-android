package com.microware.cdfi.viewModel.voviewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.entity.voentity.VoMemLoanEntity
import com.microware.cdfi.repository.vorepo.VoMemLoanRepository

class VoMemLoanViewModel: AndroidViewModel {
    var voMemLoanRepository:  VoMemLoanRepository? = null

    constructor(application: Application):super(application){
        voMemLoanRepository = VoMemLoanRepository(application)
    }

    fun insertVoMemLoan(VoMemLoanEntity: VoMemLoanEntity){
        voMemLoanRepository!!.insertVoMemLoan(VoMemLoanEntity)
    }

    fun deleteVoMemLoanData(){
        voMemLoanRepository!!.deleteVoMemLoanData()
    }

    fun getVoMemLoanData(): LiveData<List<VoMemLoanEntity>>?{
        return voMemLoanRepository!!.getVoMemLoanData()
    }

    fun   getVoMemLoanList(): List<VoMemLoanEntity>?{
        return voMemLoanRepository!!.getVoMemLoanList()
    }

    fun getinterestrate(loanNo:Int,memId: Long?): Double {
        return voMemLoanRepository!!.getinterestrate(loanNo,memId)
    }

    fun deleteMemberLoan(cbo_id: Long, mtg_no: Int) {
        return voMemLoanRepository!!.deleteMemberLoan(cbo_id, mtg_no)
    }

    fun getLoanDetailData(loan_no: Int,cbo_id: Long,mem_id: Long,mtg_guid:String): List<VoMemLoanEntity>?{
        return voMemLoanRepository!!.getLoanDetailData(loan_no,cbo_id,mem_id,mtg_guid)
    }

    fun getCutOffmaxLoanno(cbo_id: Long,mtg_no: Int): Int{
        return voMemLoanRepository!!.getCutOffmaxLoanno(cbo_id,mtg_no)
    }

    fun getLoanno(appid: Long): Int {
        return voMemLoanRepository!!.getLoanno(appid)
    }

    fun getLoanDetail(cbo_id: Long,mtgNo: Int): List<VoMemLoanEntity>?{
        return voMemLoanRepository!!.getLoanDetail(cbo_id,mtgNo)
    }


    fun getTotalClosedLoanAmount(mtgNum:Int,mem_id: Long):Int{
        return voMemLoanRepository!!.getTotalClosedLoanAmount(mtgNum, mem_id)
    }

    fun getTotalClosedLoanCount(mtgNum:Int,mem_id: Long):Int{
        return voMemLoanRepository!!.getTotalClosedLoanCount(mtgNum, mem_id)
    }

    fun getmaxLoanno(cbo_id: Long): Int {
        return voMemLoanRepository!!.getmaxLoanno(cbo_id)
    }

    fun getTotalLoanCount(mtgNum:Int,mem_id: Long):Int{
        return voMemLoanRepository!!.getTotalLoanCount(mtgNum, mem_id)
    }

    fun getTotalClosedLoanAmount(mtgNum: Int, mem_id: Long, completionFlag: Boolean): Int {
        return voMemLoanRepository!!.getTotalClosedLoanAmount(mtgNum, mem_id, completionFlag)
    }

    fun getTotalClosedLoanCount(mtgNum: Int, mem_id: Long, completionFlag: Boolean): Int {
        return voMemLoanRepository!!.getTotalClosedLoanCount(mtgNum, mem_id, completionFlag)
    }
    fun getTotalLoanAmount(mtgNum:Int,mem_id: Long):Int{
        return voMemLoanRepository!!.getTotalLoanAmount(mtgNum, mem_id)
    }

    fun getTotaloutstandingprinciple(mtgNum:Int,mem_id: Long):Int{
        return voMemLoanRepository!!.getTotaloutstandingprinciple(mtgNum, mem_id)
    }

    fun getAmountPaid(mtgNum:Int,mem_id: Long):Int{
        return voMemLoanRepository!!.getAmountPaid(mtgNum, mem_id)
    }

    fun gettotamt(loanno: Int,member_id:Long): Int {
        return voMemLoanRepository!!.gettotamt(loanno,member_id)
    }
    fun getfundamt(shgId:Long,mtgNum: Int,fundType:Int): Int {
        return voMemLoanRepository!!.getfundamt(shgId,mtgNum,fundType)
    }
    fun gettotcboamt(shgId:Long,mtgNum: Int): Int {
        return voMemLoanRepository!!.gettotcboamt(shgId,mtgNum)
    }

    fun getMemLoanDisbursedAmount(cbo_id: Long, mtg_no: Int, bank_code: String, modeofpayment: List<Int>):Int{
        return voMemLoanRepository!!.getMemLoanDisbursedAmount(cbo_id, mtg_no, bank_code,modeofpayment)
    }
    fun getloanListSummeryData(mtgno: Int, cbo_id: Long,mem_id: Long): List<VoMemLoanEntity>{
        return voMemLoanRepository!!.getloanListSummeryData(mtgno, cbo_id, mem_id)
    }

    fun getMemLoanDisbursedAmount(
        modePayment: List<Int>,
        cbo_id: Long,
        mtg_no: Int
    ): Int {
        return voMemLoanRepository!!.getMemLoanDisbursedAmount(modePayment, cbo_id, mtg_no)
    }
    fun getTotalClosedLoan(mtgNum:Int,mem_id: Long,completionFlag:Boolean):Int{
        return voMemLoanRepository!!.getTotalClosedLoan(mtgNum,mem_id,completionFlag)
    }

    fun getClosedLoanAmount(mtgNum:Int,mem_id: Long,completionFlag: Boolean):Int{
        return voMemLoanRepository!!.getClosedLoanAmount(mtgNum,mem_id,completionFlag)
    }
    internal  fun updateMemberLoanEditFlag(cbo_id:Long,mem_id:Long,loan_no:Int, completionflag: Boolean){
        voMemLoanRepository!!.updateMemberLoanEditFlag(cbo_id,mem_id,loan_no,completionflag)
    }

    fun getuploadlist(cbo_id: Long): List<VoMemLoanEntity>? {
        return voMemLoanRepository!!.getuploadlist(cbo_id)
    }
}