package com.microware.cdfi.viewModel.voviewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.microware.cdfi.entity.voentity.VoMemSettlementEntity
import com.microware.cdfi.repository.vorepo.VoMemSettlementRepository

class VoMemSettlementViewmodel: AndroidViewModel {

    var memSettlementRepository: VoMemSettlementRepository? = null

    constructor(application: Application):super(application){
        memSettlementRepository = VoMemSettlementRepository(application)
    }

    internal  fun getSettlementdata(memid: Long?,mtgno:Int): List<VoMemSettlementEntity>?{
        return memSettlementRepository!!.getSettlementdata(memid,mtgno)
    }


    fun insert(dtMemSettlementEntity: VoMemSettlementEntity){
         memSettlementRepository!!.insertSettlementData(dtMemSettlementEntity)
    }

    fun getSettlementuploaddata(cboId:Long?,memid: Long?,mtgno:Int): List<VoMemSettlementEntity>?{
        return memSettlementRepository!!.getSettlementuploaddata(cboId,memid,mtgno)
    }


}