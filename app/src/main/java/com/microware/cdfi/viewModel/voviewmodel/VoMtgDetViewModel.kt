package com.microware.cdfi.viewModel.voviewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.microware.cdfi.entity.voentity.VoMtgDetEntity
import com.microware.cdfi.repository.vorepo.VoMtgDetRepository

class VoMtgDetViewModel: AndroidViewModel {
    var voMtgDetRepository:  VoMtgDetRepository? = null

    constructor(application: Application):super(application){
        voMtgDetRepository = VoMtgDetRepository(application)
    }

    fun insertVoMtgDet(voMtgDetEntity: VoMtgDetEntity){
        voMtgDetRepository!!.insertVoMtgDet(voMtgDetEntity)
    }

    fun deleteVoMtgDetData(){
        voMtgDetRepository!!.deleteVoMtgDetData()
    }

    fun getVoMtgDetData(): LiveData<List<VoMtgDetEntity>>?{
        return voMtgDetRepository!!.getVoMtgDetData()
    }

    fun   getVoMtgDetList(): List<VoMtgDetEntity>?{
        return voMtgDetRepository!!.getVoMtgDetList()
    }

    internal fun getListDataMtgByMtgnum(mtgno: Int, cbo_id: Long): List<VoMtgDetEntity> {
        return voMtgDetRepository!!.getListDataMtgByMtgnum(mtgno, cbo_id)
    }

    fun updateAttendanceEC1(
        EC1: String,
        mtgno: Int,
        cbo_id: Long,
        memId: Long,
        updated_by: String,
        updated_on: Long
    ) {
        voMtgDetRepository!!.updateAttendanceEC1(
            EC1,
            mtgno,
            cbo_id,
            memId,
            updated_by,
            updated_on
        )
    }

    fun updateAttendanceEC2(
        EC2: String,
        mtgno: Int,
        cbo_id: Long,
        memId: Long,
        updated_by: String,
        updated_on: Long
    ) {
        voMtgDetRepository!!.updateAttendanceEC2(
            EC2,
            mtgno,
            cbo_id,
            memId,
            updated_by,
            updated_on
        )
    }

    fun updateAttendanceEC3(
        EC3: String,
        mtgno: Int,
        cbo_id: Long,
        memId: Long,
        updated_by: String,
        updated_on: Long
    ) {
        voMtgDetRepository!!.updateAttendanceEC3(
            EC3,
            mtgno,
            cbo_id,
            memId,
            updated_by,
            updated_on
        )
    }

    fun updateAttendanceOther(
        attendanceOther: Int,
        mtgno: Int,
        cbo_id: Long,
        memId: Long,
        updated_by: String,
        updated_on: Long
    ) {
        voMtgDetRepository!!.updateAttendanceOther(
            attendanceOther,
            mtgno,
            cbo_id,
            memId,
            updated_by,
            updated_on
        )
    }

    fun updateAttendance(
        attendance: String,
        mtgNo: Int,
        cboId: Long,
        memId: Long,
        updatedBy: String?,
        updatedOn: Long
    ) {
        voMtgDetRepository!!.updateAttendance(
            attendance,
            mtgNo,
            cboId,
            memId,
            updatedBy,
            updatedOn
        )
    }

    internal fun getTotalPresentAndAbsent(attendance: String, mtgNo: Int, cboId: Long): Int {
        return voMtgDetRepository!!.getTotalPresentAndAbsent(attendance, mtgNo, cboId)
    }

    fun updateCutOffAttendance(
        attendance: String,
        mtgNo: Int,
        cboId: Long,
        memId: Long,
        updatedBy: String?,
        updatedOn: Long
    ) {
        voMtgDetRepository!!.updateCutOffAttendance(
            attendance,
            mtgNo,
            cboId,
            memId,
            updatedBy,
            updatedOn
        )
    }

    fun getCutOffTotalPresent(mtgno: Int, cbo_id: Long): Int {
        return voMtgDetRepository!!.getCutOffTotalPresent(mtgno, cbo_id)
    }
    fun updateAttendanceEC4(
        EC4: String,
        mtgno: Int,
        cbo_id: Long,
        memId: Long,
        updated_by: String,
        updated_on: Long
    ) {
        voMtgDetRepository!!.updateAttendanceEC4(
            EC4,
            mtgno,
            cbo_id,
            memId,
            updated_by,
            updated_on
        )
    }

    fun updateAttendanceEC5(
        EC5: String,
        mtgno: Int,
        cbo_id: Long,
        memId: Long,
        updated_by: String,
        updated_on: Long
    ) {
        voMtgDetRepository!!.updateAttendanceEC5(
            EC5,
            mtgno,
            cbo_id,
            memId,
            updated_by,
            updated_on
        )
    }
    internal fun getshgsumattendance(mtgno: Int, cbo_id: Long): Int {
        return voMtgDetRepository!!.getshgsumattendance(mtgno, cbo_id)
    }

    fun getECPresent(mtgno: Int, cbo_id: Long, attendance: String): List<Int> {
        return voMtgDetRepository!!.getECPresent(mtgno, cbo_id, attendance)
    }

    internal fun getTotalPresentAbsent(mtgNo: Int, cboId: Long): Int {
        return voMtgDetRepository!!.getTotalPresentAbsent(mtgNo, cboId)
    }

    fun getECPresent(mtgno: Int, cbo_id: Long): Int {
        return voMtgDetRepository!!.getECPresent(mtgno, cbo_id)
    }
    fun getVolsaving(mem_id: Long, mtgno: Int): Int {
        return voMtgDetRepository!!.getVolsaving(mem_id, mtgno)
    }
    fun updateWithdrawal(mem_id: Long, mtgno: Int, Withdrawal: Int){
        voMtgDetRepository!!.updateWithdrawal(mem_id, mtgno,  Withdrawal)
    }
    fun updatevolclosing(mem_id: Long, mtgno: Int, sav_vol_cb: Int) {
        voMtgDetRepository!!.updatevolclosing(mem_id, mtgno,  sav_vol_cb)
    }
    fun updateCompclosing(mem_id: Long, mtgno: Int, sav_comp_cb: Int) {
        voMtgDetRepository!!.updateCompclosing(mem_id, mtgno,  sav_comp_cb)
    }
    fun getmemdata(memid: Long?, mtgnum: Int): List<VoMtgDetEntity> {
        return voMtgDetRepository!!.getmemdata(memid, mtgnum)
    }

    fun adjustCompclosing(mem_id: Long, mtgno: Int, sav_vol_cb: Int) {
        voMtgDetRepository!!.adjustCompclosing(mem_id, mtgno, sav_vol_cb)
    }

    fun adjustvolclosing(mem_id: Long, mtgno: Int, sav_comp_cb: Int) {
        voMtgDetRepository!!.adjustvolclosing(mem_id, mtgno, sav_comp_cb)
    }
    fun getVolclosing(mem_id: Long, mtgno: Int): Int {
        return voMtgDetRepository!!.getVolclosing(mem_id, mtgno)
    }
    fun getCompclosing(mem_id: Long, mtgno: Int): Int {
        return voMtgDetRepository!!.getCompclosing(mem_id, mtgno)
    }
    internal fun getListinactivemember(mtgno: Int, cbo_id: Long): List<VoMtgDetEntity> {
        return voMtgDetRepository!!.getListinactivemember(mtgno, cbo_id)
    }

    fun getCompulsoryData(mtgno: Int, cbo_id: Long,mem_id: Long): List<VoMtgDetEntity>{
        return voMtgDetRepository!!.getCompulsoryData(mtgno,cbo_id,mem_id)
    }

    fun updatesavingwithdrawl(
        withdrawl: Int,
        mtgno: Int,
        cbo_id: Long,
        memid: Long,
        updated_by:String,
        updated_on:Long
    ) {
        voMtgDetRepository!!.updatesavingwithdrawl(
            withdrawl,
            mtgno,
            cbo_id,
            memid,
            updated_by,
            updated_on
        )
    }
}